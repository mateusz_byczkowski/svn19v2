$/*-----------------------------------------------------------------------
$ * 
$ * File	: crtx_fld (cocrd.fd + coint.fd)
$ * 
$ * Author	: Stanislaw Namyslak (Tom Hay)
$ * 
$ * Created	: 21 May 92   14:43 
$ * 
$ * Purpose	: Card-based message field table definition 
$ * 
$ * Comments	: Tylko pola u�ywane przez serwery w CBD
$ *
$  Ident	: @(#) $Id: //depot/cortex/alpha/src/tuxfbuf/cocrd.fd#1$
$ * 	
$ *-----------------------------------------------------------------------
$ * Copyright (c) WBK SA , Nomad Software Ltd.					 
$ *-----------------------------------------------------------------------
$ * 30-11-2002 - Nowe pola zwiazane z IBKB 
$ *-----------------------------------------------------------------------*/

*base 12000

#name		relno	type	flags 	comment
#----		-----	----	-----	-------

# Cardholder identification
#-------------------------- 
C_PAN32		 0	string	-	1 primary account number (2 and 34)
C_CUSTID32       1      string  -       nr dowodu
C_BIRDAT32       2      string  -       data urodzenia
C_FNAME32        3      string  -       imie
C_LNAME32        4      string  -       nazwisko
C_EXPDAT32       5      string  -       data wygasniecia
C_A_AVLBAL32     6      float   -       saldo
C_A_CRDLIM32     7      float   -       limit
C_A_BLKAMT32     8      float   -       blokada
C_P_RODZ32       9      string  -       rodzaj karty
C_P_TYP32       10      string  -       typ karty


*base 12100
#name		relno	type	flags 	comment
#----		-----	----	-----	-------

# ATM informations
#-------------------------- 
C_TERMCODE       0    string  -      ATM code
C_BRNCODE        1      string  -       branch code
C_LOCATION       2      string  -       location
C_STATEREQ       3      string  -       status required
C_STATEACT       4      string  -       status actual
C_ONLINE         5      string  -       is online
C_REPLENDATE     6      long    -       last load date
C_REPLENTIME     7      long    -       last load time
C_CASS1DNOM      8      long    -       load banknote
C_CASS1LOAD      9      short   -       loaded nbr
C_CASS1DISP     10      short   -       payed nbr
C_CASS1TYPE     11      string  -       cassete type
C_CASS2DNOM     12      long    -       load banknote
C_CASS2LOAD     13      short   -       loaded nbr
C_CASS2DISP     14      short   -       payed nbr
C_CASS2TYPE     15      string  -       cassete type
C_CASS3DNOM     16      long    -       load banknote
C_CASS3LOAD     17      short   -       loaded nbr
C_CASS3DISP     18      short   -       payed nbr
C_CASS3TYPE     19      string  -       cassete type
C_CASS4DNOM     20      long    -       load banknote
C_CASS4LOAD     21      short   -       loaded nbr
C_CASS4DISP     22      short   -       payed nbr
C_CASS4TYPE     23      string  -       cassete type
C_DISPENSER     24      string  -       dispenser status
C_JOURNAL       25      string  -       journal status
C_RECEIPT       26      string  -       receipt status
C_CARDREADER    27      string  -       cardreader status
C_DOORS         28      string  -       doors status
C_STAMP         29      string  -       timestamp
C_REFRESH       30      long    -       refresh time
C_ACC_TIME      31      string  -       access time
C_CASH_LVL      32      long    -       cash level
C_LASTTXNDATE   33      long    -       last transaction date
C_LASTTXNTIME   34      long    -       last transaction time
C_OUTSOURCE     35      string  -       ATM in outsource flag
C_NR_ICBS       36      string  -       number ICBS


*base 90000
#name		relno	type	flags 	comment
#----		-----	----	-----	-------

CT_PAN             0      string  -       primary account number
CT_PRODDATE        1      string  -       production/renewal date
CT_RENEWCNT        2      short   -       renewal counter
CT_EXPDAT          4      string  -       data wygasniecia
CT_CIF             5      long    -       CIF wlasciciela karty 
CT_STATDATE        6      string  -       data statusu
CT_STATCODE        7      string  -       data statusu
CT_IBANACCNO       8      string  -       numer rachunku IBAN
CT_EMBOSS1         9      string  -       emboss 1
CT_EMBOSS2        10      string  -       emboss 2 
CT_GENSTAT        11      short   -       status uogolniony
CT_PRODNAME       12      string  -       nazwa produktu
CT_LIMITAMOUNT    13      double  -       kwota limitu
CT_LIMITPERIOD    14      short   -       okres limitu
CT_LIMITTYPE      15      short   -       typ limitu
CT_DIRECTION      16      short   -       kierunek odpytywania
CT_OPTION         17      long    -       opcje odpytywania
CT_DATEFROM       18      string  -       data od
CT_DATETO         19      string  -       data do
CT_TLOGID         20      long    -       tlogid
CT_COUNT          21      short   -       oczekiwana liczba rekordow
CT_POSTDATE       22      string  -       data ksiegowania
CT_DATELOCAL      23      string  -       data lokalna transakcji
CT_CRDACPTLOC     24      string  -       nazwa merchanta
CT_TXNTYPE        25      short   -       typ transakcji
CT_AMTTXN         26      double  -       kwota transakcji
CT_CURTXN         27      string  -       waluta transakcji
CT_AMTBILL        28      double  -       kwota transakcji w walucie rachunku
CT_CURBILL        29      string  -       waluta rachunku
CT_TXNCODE        30      short   -       kod transakcji (wg ISO)
CT_TOTALDEBIT     31      double  -       suma kwot ze zwroconych operacji (dla obciazen)
CT_TOTALCREDIT    32      double  -       suma kwot ze zwroconych operacji (dla uznan)
CT_POLICY_FLAG    33      long    -       flaga ubezpieczenia
CT_ADDR           34      string  -       adres - ulica+numer
CT_CITY           35      string  -       adres - miasto
CT_POSTCODE       36      string  -       kod pocztowy
CT_FIRSTNAME      37      string  -       imie
CT_LASTNAME       38      string  -       nazwisko
CT_AGREEMENT_FLAG 39      char    -       flaga umowy
CT_VPAN           40      string  -       wirtualny numer karty
CT_PAN_DISP       41      string  -       maskowany numer karty
CT_CUSTID         42      string  -       nr dowodu
CT_PINMAILER_REF  43      string  -       nr pinmailera
