<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns9 = "urn:be.services.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Pilot/GetContract/ResponseTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns7 = "http://jv.channel.cu.com.pl/cmf-product-types";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-party-types";
declare namespace ns3 = "urn:cif.entities.be.dcl";
declare namespace ns5 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns2 = "urn:baseentities.be.dcl";
declare namespace ns6 = "urn:applicationul.entities.be.dcl";
declare namespace ns8 = "urn:uldictionary.dictionaries.be.dcl";

declare function xf:ResponseTransform($getContractResponse1 as element(ns1:getContractResponse))
    as element(ns9:invokeResponse) {
        &lt;ns9:invokeResponse&gt;
            &lt;ns9:messageHelper&gt;
                &lt;ns2:MessageHelper&gt;
                    &lt;ns2:errorCode?&gt;&lt;/ns2:errorCode&gt;
                    &lt;ns2:errorType?&gt;&lt;/ns2:errorType&gt;
                    &lt;ns2:message?&gt;&lt;/ns2:message&gt;
                &lt;/ns2:MessageHelper&gt;
            &lt;/ns9:messageHelper&gt;
            &lt;ns9:customer&gt;
                &lt;ns3:Customer&gt;
                    &lt;ns3:customerNumber&gt;{
                    	let $clients :=
	                        for $client in $getContractResponse1/contract/client-list/contract-person
	                        where $client/@role = '1'
	                        return
	                            data($client/@client-code)
	                    return $clients[1]
                    }&lt;/ns3:customerNumber&gt;
                    &lt;ns3:policyContract&gt;
                        {
                            let $contract := $getContractResponse1/contract
                            return
                                &lt;ns6:PolicyContract&gt;
                                    &lt;ns6:proposalID&gt;{ xs:string( data($contract/proposal-number) ) }&lt;/ns6:proposalID&gt;
                                    &lt;ns6:policyID&gt;{ xs:string( data($contract/@contract-number) ) }&lt;/ns6:policyID&gt;
                                    &lt;ns6:proposalDate?&gt;{ data($contract/contract-dates/proposal) }&lt;/ns6:proposalDate&gt;
                                    &lt;ns6:policyDate?&gt;{ data($contract/contract-dates/accepted) }&lt;/ns6:policyDate&gt;
                                    &lt;ns6:cession&gt;{ if (fn:exists(data($contract/cession))) then 1 else 0 }&lt;/ns6:cession&gt;
                                    {
                                        for $coverages in $contract/coverages
                                        return
                                            &lt;ns6:policyCoverageList&gt;
                                                {
                                                    for $coverage in $coverages/coverage
                                                    return
                                                        &lt;ns6:PolicyCoverage&gt;
                                                            &lt;ns6:coverageID&gt;{ xs:string( data($coverage/@coverage-number) ) }&lt;/ns6:coverageID&gt;
                                                            &lt;ns6:owuSig&gt;{ xs:string( data($coverage/owu/signature) ) }&lt;/ns6:owuSig&gt;
                                                            &lt;ns6:regulationSig&gt;{ xs:string( data($coverage/regulation-sig) ) }&lt;/ns6:regulationSig&gt;
                                                            &lt;ns6:parametrsCardSig&gt;{ xs:string( data($coverage/param-card-sig) ) }&lt;/ns6:parametrsCardSig&gt;
                                                            &lt;ns6:coverageStartDate?&gt;{ data($coverage/dates/contract-start) }&lt;/ns6:coverageStartDate&gt;
                                                            &lt;ns6:premiumAmount&gt;{ xs:double( data($coverage/amount) ) }&lt;/ns6:premiumAmount&gt;
                                                            &lt;ns6:policyCoverageBenefitedList&gt;
                                                            {
                                                                for $beneficiary in $coverage/beneficiaries/beneficiary
                                                                return
                                                                    &lt;ns6:PolicyCoverageBenefited&gt;
                                                                        &lt;ns6:privatePerson&gt;{ xs:string( data($beneficiary/company-ind) ) }&lt;/ns6:privatePerson&gt;
                                                                        &lt;ns6:firstName?&gt;{ xs:string( data($beneficiary/name/first-name) ) }&lt;/ns6:firstName&gt;
                                                                        &lt;ns6:name?&gt;{ xs:string( data($beneficiary/name/last-name) ) }&lt;/ns6:name&gt;
                                                                        &lt;ns6:pesel?&gt;{ data($beneficiary/identifiers/pesel) }&lt;/ns6:pesel&gt;
                                                                        &lt;ns6:regon?&gt;{ data($beneficiary/identifiers/regon) }&lt;/ns6:regon&gt;
                                                                        &lt;ns6:birthDate?&gt;{ if(fn:contains($beneficiary/dob, '+')) then fn:substring-before(data($beneficiary/dob), '+') else data($beneficiary/dob) }&lt;/ns6:birthDate&gt;
                                                                        &lt;ns6:percent&gt;{ xs:double( data($beneficiary/split) ) }&lt;/ns6:percent&gt;
                                                                    &lt;/ns6:PolicyCoverageBenefited&gt;
                                                            }
                                                            &lt;/ns6:policyCoverageBenefitedList&gt;
                                                            &lt;ns6:policyCoverageFundSplitList&gt;
                                                            {
                                                                for $fund in $coverage/funds/contract-fund[1]/funds/fund
                                                                return
                                                                    &lt;ns6:PolicyCoverageFundSplit&gt;
                                                                        &lt;ns6:splitPercentage&gt;{ data($fund/split) }&lt;/ns6:splitPercentage&gt;
                                                                        &lt;ns6:fundID&gt;
                                                                        	&lt;ns8:UlFund&gt;
                                                                        		&lt;ns8:fundID&gt;{ xs:string( data($fund/@fund-name) ) }&lt;/ns8:fundID&gt;
                                                                        	&lt;/ns8:UlFund&gt;
                                                                        &lt;/ns6:fundID&gt;
                                                                    &lt;/ns6:PolicyCoverageFundSplit&gt;
                                                            }
                                                            &lt;/ns6:policyCoverageFundSplitList&gt;
                                                            &lt;ns6:policyCoverageFundList&gt;
                                                            {
                                                                for $fund-value in $coverage/values/fund-value
                                                                return
                                                                    &lt;ns6:PolicyCoverageFund&gt;
                                                                        &lt;ns6:fundUnit&gt;{ xs:double( data($fund-value/quantity) ) }&lt;/ns6:fundUnit&gt;
                                                                        &lt;ns6:fundID&gt;
	                                                                        &lt;ns8:UlFund&gt;
	                                                                        		&lt;ns8:fundID&gt;{ xs:string( data($fund-value/@fund-name) ) }&lt;/ns8:fundID&gt;
                                                                        	&lt;/ns8:UlFund&gt;
	                                                                    &lt;/ns6:fundID&gt;
                                                                    &lt;/ns6:PolicyCoverageFund&gt;
                                                            }
                                                            &lt;/ns6:policyCoverageFundList&gt;
                                                            &lt;ns6:coverageCategory&gt;
                                                                &lt;ns8:UlCoverageCategory&gt;
                                                                    &lt;ns8:ulCoverageCategory&gt;{ xs:string( data($coverage/@coverage-category) ) }&lt;/ns8:ulCoverageCategory&gt;
                                                                &lt;/ns8:UlCoverageCategory&gt;
                                                            &lt;/ns6:coverageCategory&gt;
                                                            &lt;ns6:coverageStatus&gt;
                                                                &lt;ns8:UlContractStatus&gt;
                                                                    &lt;ns8:ulContractStatus&gt;{ xs:string( data($coverage/status) ) }&lt;/ns8:ulContractStatus&gt;
                                                                &lt;/ns8:UlContractStatus&gt;
                                                            &lt;/ns6:coverageStatus&gt;
                                                        &lt;/ns6:PolicyCoverage&gt;
                                                }
                                            &lt;/ns6:policyCoverageList&gt;
                                    }
                                    &lt;ns6:policyContractAddressList&gt;
                                    {
                                    	let $persons :=
                                    		for $p in $contract/client-list/contract-person
                                    		where $p/@role = '1'
                                    		return
                                    			let $addresses := 
                                    				for $address in $p/address
                                    				where $address/@kind = '4'
                                    				return
	                                        			&lt;ns6:PolicyContractAddress&gt;
		                                                    &lt;ns6:addressType&gt;{ data($address/@kind) }&lt;/ns6:addressType&gt;
		                                                    &lt;ns6:addressName&gt;{ data($address/company-name) }&lt;/ns6:addressName&gt;
                                                            &lt;ns6:street&gt;{ xs:string( data($address/street) ) }&lt;/ns6:street&gt;
                                                            &lt;ns6:house&gt;{ xs:string( data($address/home) ) }&lt;/ns6:house&gt;
                                                            &lt;ns6:flat&gt;{ xs:string( data($address/flat) ) }&lt;/ns6:flat&gt;
                                                            &lt;ns6:city&gt;{ xs:string( data($address/city) ) }&lt;/ns6:city&gt;
                                                            &lt;ns6:zipCode&gt;{ xs:string( data($address/postal-code) ) }&lt;/ns6:zipCode&gt;
		                                                    &lt;ns6:state&gt;
		                                                        &lt;ns4:State&gt;
                                                                    &lt;ns4:state&gt;{ xs:string( data($address/county) ) }&lt;/ns4:state&gt;
		                                                        &lt;/ns4:State&gt;
		                                                    &lt;/ns6:state&gt;
		                                                    &lt;ns6:country&gt;
		                                                        &lt;ns4:CountryCode&gt;
                                                                    &lt;ns4:countryCode&gt;{ xs:string( data($address/country) ) }&lt;/ns4:countryCode&gt;
		                                                        &lt;/ns4:CountryCode&gt;
		                                                    &lt;/ns6:country&gt;
	                                        			&lt;/ns6:PolicyContractAddress&gt;
	                                        	return $addresses[1]
                                    	return $persons[1]
                                    }
                                    &lt;/ns6:policyContractAddressList&gt;
                                    &lt;ns6:policyContractAccountList&gt;
                                    {
                                        for $bank in $contract/banks/bank
                                        return
                                            &lt;ns6:PolicyContractAccount&gt;
                                                &lt;ns6:accountNo&gt;{ xs:string( data($bank/account) ) }&lt;/ns6:accountNo&gt;
                                                &lt;ns6:firstName&gt;{ xs:string( data($bank/recipient-name/first-name) ) }&lt;/ns6:firstName&gt;
                                                &lt;ns6:name&gt;{ xs:string( data($bank/recipient-name/last-name) ) }&lt;/ns6:name&gt;
                                                &lt;ns6:street&gt;{ xs:string( data($bank/recipient-address/street) ) }&lt;/ns6:street&gt;
                                                &lt;ns6:house&gt;{ xs:string( data($bank/recipient-address/home) ) }&lt;/ns6:house&gt;
                                                &lt;ns6:flat&gt;{ xs:string( data($bank/recipient-address/flat) ) }&lt;/ns6:flat&gt;
                                                &lt;ns6:zipCode&gt;{ data($bank/recipient-address/postal-code) }&lt;/ns6:zipCode&gt;
                                                &lt;ns6:city&gt;{ xs:string( data($bank/recipient-address/city) ) }&lt;/ns6:city&gt;
                                                &lt;ns6:accountType&gt;
                                                	&lt;ns8:UlAccountType&gt;
                                                		&lt;ns8:ulAccountType&gt;{ data($bank/@kind) }&lt;/ns8:ulAccountType&gt;
                                                	&lt;/ns8:UlAccountType&gt;
                                                &lt;/ns6:accountType&gt;
                                                &lt;ns6:country&gt;
                                                    &lt;ns4:CountryCode&gt;
                                                        &lt;ns4:countryCode&gt;{ xs:string( data($bank/recipient-address/country) ) }&lt;/ns4:countryCode&gt;
                                                    &lt;/ns4:CountryCode&gt;
                                                &lt;/ns6:country&gt;
                                            &lt;/ns6:PolicyContractAccount&gt;
                                    }
                                    &lt;/ns6:policyContractAccountList&gt;
                                    &lt;ns6:productCode&gt;
                                        &lt;ns8:UlParameters&gt;
                                            {
                                                for $product-type in $contract/@product-type
                                                return
                                                    &lt;ns8:productId&gt;{ xs:string( data($product-type) ) }&lt;/ns8:productId&gt;
                                            }
                                        &lt;/ns8:UlParameters&gt;
                                    &lt;/ns6:productCode&gt;
                                    &lt;ns6:policyStatus&gt;
                                        &lt;ns8:UlContractStatus&gt;
                                            &lt;ns8:ulContractStatus&gt;{ xs:string( data($contract/status) ) }&lt;/ns8:ulContractStatus&gt;
                                        &lt;/ns8:UlContractStatus&gt;
                                    &lt;/ns6:policyStatus&gt;
                                &lt;/ns6:PolicyContract&gt;
                        }
                    &lt;/ns3:policyContract&gt;
                &lt;/ns3:Customer&gt;
            &lt;/ns9:customer&gt;
        &lt;/ns9:invokeResponse&gt;
};

declare variable $getContractResponse1 as element(ns1:getContractResponse) external;

xf:ResponseTransform($getContractResponse1)</con:xquery>
</con:xqueryEntry>