<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/StreamLine/GetPolicyHistory/RequestTransform_nowe/";
declare namespace ns0 = "urn:baseentities.be.dcl";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns3 = "urn:applicationul.entities.be.dcl";
declare namespace ns2 = "urn:basedictionaries.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";
declare namespace ns6 = "urn:baseauxentities.be.dcl";
declare namespace ns7 = "urn:entities.be.dcl";

declare function xf:RequestTransform_nowe($invoke1 as element(ns5:invoke), $header1 as element(ns5:header))
    as element(ns-1:getContractFinancialOperationListRequest) {
        &lt;ns-1:getContractFinancialOperationListRequest&gt;
            &lt;envelope&gt;
            	&lt;request-time&gt;{ data($header1/ns5:msgHeader/ns5:timestamp) }&lt;/request-time&gt;
            	&lt;request-no&gt;{ data($header1/ns5:msgHeader/ns5:msgId) }&lt;/request-no&gt;
            	&lt;source-code&gt;{ data($invoke1/ns5:sourceCode/ns6:StringHolder/ns6:value) }&lt;/source-code&gt;
            	&lt;user-id&gt;{ data($header1/ns5:msgHeader/ns5:userId) }&lt;/user-id&gt;
                &lt;branch-id&gt;{ xs:long( data($header1/ns5:msgHeader/ns5:unitId) ) }&lt;/branch-id&gt;
                &lt;bar-code?&gt;{ data($invoke1/ns5:barCodeID/ns6:StringHolder/ns6:value) }&lt;/bar-code&gt;
            &lt;/envelope&gt;
            &lt;product-type&gt;{ data($invoke1/ns5:policyContract/ns3:PolicyContract/ns3:productCode/ns4:UlParameters/ns4:productId) }&lt;/product-type&gt;
            &lt;contract-number&gt;{ data($invoke1/ns5:policyContract/ns3:PolicyContract/ns3:policyID) }&lt;/contract-number&gt;
            {
            	for $date in $invoke1/ns5:startDate/ns6:DateHolder/ns6:value
            	return
            		&lt;date-from?&gt;{ fn:substring-before( data( concat($date, 'T') ), 'T' ) }&lt;/date-from&gt;
            }
            {
            	for $date in $invoke1/ns5:stopDate/ns6:DateHolder/ns6:value
            	return
            		&lt;date-to?&gt;{ fn:substring-before( data( concat($date, 'T') ), 'T' ) }&lt;/date-to&gt;
            }
            &lt;operation-type?&gt;{ 
            	let $operations :=
            		for $operation in $invoke1/ns5:policyContract/ns3:PolicyContract/ns3:policyOrderHistoryListList/ns3:PolicyOrderHistoryList
            		return
            			xs:int( data($operation/ns3:historyCode/ns4:UlHistoryCode/ns4:ulHistoryCode) )
            	return $operations[1]
            }&lt;/operation-type&gt;
        &lt;/ns-1:getContractFinancialOperationListRequest&gt;
};

declare variable $invoke1 as element(ns5:invoke) external;
declare variable $header1 as element(ns5:header) external;

xf:RequestTransform_nowe($invoke1, $header1)</con:xquery>
</con:xqueryEntry>