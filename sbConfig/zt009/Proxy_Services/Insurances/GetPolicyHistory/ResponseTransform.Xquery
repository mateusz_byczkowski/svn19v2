<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/StreamLine/GetPolicyHistory/ResponseTransform_nowe/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns1 = "urn:baseentities.be.dcl";
declare namespace ns3 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:applicationul.entities.be.dcl";
declare namespace cl = "http://sygnity.pl/functions";

declare function xf:cupCodeConvert($input as xs:string, $type as xs:string, $codes as element(cl:historyCodes)) as xs:string {
	let $translated :=
		for $e in $codes/cl:historyCode
		where $e/@cup-code = $input
		return
			if($type = 'CODE') then
				data($e/@nfe-code)
			else
				data($e/@nfe-type)
	return
		if($translated[1] != '') then
			$translated[1]
		else if($type = 'CODE') then
			'0'
		else
			''
};

declare function xf:ResponseTransform_nowe($getContractFinancialOperationListResponse1 as element(ns0:getContractFinancialOperationListResponse),
					$codes as element(cl:historyCodes))
    as element(ns4:invokeResponse) {
        &lt;ns4:invokeResponse&gt;
        	&lt;ns4:messageHelper&gt;
        		&lt;ns1:MessageHelper&gt;
        		&lt;/ns1:MessageHelper&gt;
        	&lt;/ns4:messageHelper&gt;
            &lt;ns4:policyContract&gt;
                &lt;ns2:PolicyContract&gt;
                    &lt;ns2:policyID&gt;{ xs:string( data($getContractFinancialOperationListResponse1/operations/@contract-number) ) }&lt;/ns2:policyID&gt;
                    &lt;ns2:policyOrderHistoryListList&gt;
                    {
                        for $operation in $getContractFinancialOperationListResponse1/operations/operation
                        return
                            &lt;ns2:PolicyOrderHistoryList&gt;
                                &lt;ns2:orderNumber&gt;{ xs:string( data($operation/order-no) ) }&lt;/ns2:orderNumber&gt;
                                &lt;ns2:operationDate&gt;{ data($operation/effective) }&lt;/ns2:operationDate&gt;
                                {
                                	for $data in $operation/execution
                                	where fn:string-length($data) &gt; 0
                                	return
                                		&lt;ns2:orderDate?&gt;{ fn:substring-before( data( $operation/execution ), 'T' ) }&lt;/ns2:orderDate&gt;
                                }
                                &lt;ns2:value&gt;{ xs:double( data($operation/value) ) }&lt;/ns2:value&gt;
                                &lt;ns2:operationId&gt;{ data($operation/@operation-id) }&lt;/ns2:operationId&gt;
                                &lt;ns2:historyCode&gt;
                                    &lt;ns3:UlHistoryCode&gt;
                                        &lt;ns3:ulHistoryCode&gt;{ xs:string( data($operation/operation-type) ) }&lt;/ns3:ulHistoryCode&gt;
                                    &lt;/ns3:UlHistoryCode&gt;
                                &lt;/ns2:historyCode&gt;
                                &lt;ns2:historyStatus&gt;
                                    &lt;ns3:UlHistoryStatus&gt;
                                        &lt;ns3:ulHistoryStatus&gt;{ xs:string( data($operation/status) ) }&lt;/ns3:ulHistoryStatus&gt;
                                    &lt;/ns3:UlHistoryStatus&gt;
                                &lt;/ns2:historyStatus&gt;
                                &lt;ns2:eventCode&gt;
                                	&lt;ns3:UlHistoryEventCode&gt;
                                		&lt;ns3:ulHistoryEventCode&gt;{ xf:cupCodeConvert(data($operation/operation-type), 'CODE', $codes) }&lt;/ns3:ulHistoryEventCode&gt;
                                	&lt;/ns3:UlHistoryEventCode&gt;
                                &lt;/ns2:eventCode&gt;
                                &lt;ns2:eventType&gt;
                                	&lt;ns3:UlHistoryEventType&gt;
                                		&lt;ns3:ulHistoryEventType&gt;{ xf:cupCodeConvert(data($operation/operation-type), 'TYPE', $codes) }&lt;/ns3:ulHistoryEventType&gt;
                                	&lt;/ns3:UlHistoryEventType&gt;
                                &lt;/ns2:eventType&gt;
                            &lt;/ns2:PolicyOrderHistoryList&gt;
                    }
                    &lt;/ns2:policyOrderHistoryListList&gt;
                    &lt;ns2:productCode&gt;
                        &lt;ns3:UlParameters&gt;
                            &lt;ns3:productId&gt;{ xs:string( data($getContractFinancialOperationListResponse1/operations/@product-type) ) }&lt;/ns3:productId&gt;
                        &lt;/ns3:UlParameters&gt;
                    &lt;/ns2:productCode&gt;
                &lt;/ns2:PolicyContract&gt;
            &lt;/ns4:policyContract&gt;
        &lt;/ns4:invokeResponse&gt;
};

declare variable $getContractFinancialOperationListResponse1 as element(ns0:getContractFinancialOperationListResponse) external;
declare variable $codes as element(cl:historyCodes) external;

xf:ResponseTransform_nowe($getContractFinancialOperationListResponse1, $codes)</con:xquery>
</con:xqueryEntry>