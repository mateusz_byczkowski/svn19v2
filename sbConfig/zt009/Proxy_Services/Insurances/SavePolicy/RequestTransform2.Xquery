<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/SaveULPolicy/RequestTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf-party-types";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns7 = "http://jv.channel.cu.com.pl/cmf-product-types";
declare namespace ns12 = "urn:be.services.dcl";
declare namespace ns5 = "urn:basedictionaries.be.dcl";
declare namespace ns6 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns11 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns9 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "urn:baseentities.be.dcl";
declare namespace ns3 = "urn:cif.entities.be.dcl";
declare namespace ns2 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns10 = "urn:applicationul.entities.be.dcl";
declare namespace ns8 = "urn:baseauxentities.be.dcl";
declare namespace ns13 = "urn:entities.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns12:invoke), $header1 as element(ns12:header))
    as element(ns9:addContractRequest) {
        &lt;ns9:addContractRequest&gt;
            &lt;envelope&gt;
                &lt;request-time&gt;{ data($header1/ns12:msgHeader/ns12:timestamp) }&lt;/request-time&gt;
                &lt;request-no&gt;{ data($header1/ns12:msgHeader/ns12:msgId) }&lt;/request-no&gt;
                &lt;source-code&gt;{ data($invoke1/ns12:sourceCode/ns8:StringHolder/ns8:value) }&lt;/source-code&gt;
                &lt;user-id&gt;{ data($header1/ns12:msgHeader/ns12:userId) }&lt;/user-id&gt;
                &lt;branch-id&gt;{ data($header1/ns12:msgHeader/ns12:unitId) }&lt;/branch-id&gt;
                &lt;bar-code?&gt;{ data($invoke1/ns12:barCodeID/ns8:StringHolder/ns8:value) }&lt;/bar-code&gt;
            &lt;/envelope&gt;
            &lt;save&gt;{ data($invoke1/ns12:mode/ns8:BooleanHolder/ns8:value) }&lt;/save&gt;
            {
                let $contract := $invoke1/ns12:customer/ns3:Customer/ns3:policyContract/ns10:PolicyContract
                return
		            &lt;contract product-type? = "{ data($invoke1/ns12:customer/ns3:Customer/ns3:policyContract/ns10:PolicyContract/ns10:productCode/ns11:UlParameters/ns11:productId) }"&gt;
		                &lt;proposal-number?&gt;{ data($contract/ns10:proposalID) }&lt;/proposal-number&gt;
		                &lt;contract-dates?&gt;
		                	&lt;contract-start?&gt;{ data($contract/ns10:policyStartDate) }&lt;/contract-start&gt;
		                	&lt;contract-end?&gt;{ data($contract/ns10:policyEndDate) }&lt;/contract-end&gt;
		                    &lt;proposal?&gt;{ data($contract/ns10:proposalDate) }&lt;/proposal&gt;
		                    &lt;accepted?&gt;{ data($contract/ns10:policyDate) }&lt;/accepted&gt;
		                &lt;/contract-dates&gt;
		                &lt;client-list?&gt;
                        {
                            let $addresses := 
                            	for $address in $contract/ns10:policyContractAddressList/ns10:PolicyContractAddress
	                            return
				                    &lt;contract-person client-code = "{ data($invoke1/ns12:customer/ns3:Customer/ns3:customerNumber) }"
				                                     role = "{ data($invoke1/ns12:customerRole/ns8:StringHolder/ns8:value) }"&gt;
		                                &lt;address kind = "{ $address/ns10:addressType }"&gt;
		                                    &lt;street?&gt;{ data($address/ns10:street) }&lt;/street&gt;
		                                    &lt;home?&gt;{ data($address/ns10:house) }&lt;/home&gt;
		                                    &lt;flat?&gt;{ data($address/ns10:flat) }&lt;/flat&gt;
		                                    &lt;postal-code?&gt;{ data($address/ns10:zipCode) }&lt;/postal-code&gt;
		                                    &lt;city?&gt;{ data($address/ns10:city) }&lt;/city&gt;
		                                    &lt;county?&gt;{ data($address/ns10:state/ns4:State/ns4:state) }&lt;/county&gt;
		                                    &lt;country?&gt;{ data($address/ns10:country/ns4:CountryCode/ns4:countryCode) }&lt;/country&gt;
		                                &lt;/address&gt;
		                                &lt;person&gt;
		                                	&lt;name&gt;
		                                		&lt;last-name&gt;{ data($address/ns10:addressName) }&lt;/last-name&gt;
		                                	&lt;/name&gt;
		                                &lt;/person&gt;
				                    &lt;/contract-person&gt;
	                        return $addresses[1]
                        }
		                &lt;/client-list&gt;
		                &lt;amount&gt;{ data($contract/ns10:insurancePremium) }&lt;/amount&gt;
		                &lt;status&gt;0&lt;/status&gt;
		                &lt;banks?&gt;
	                    {
	                        for $bank in $contract/ns10:policyContractAccountList/ns10:PolicyContractAccount
	                        return
	                            &lt;bank kind = "{ data($invoke1/ns12:accountType/ns8:StringHolder/ns8:value) }"&gt;
	                                &lt;account&gt;{ data($bank/ns10:accountNo) }&lt;/account&gt;
	                                &lt;recipient-name?&gt;
	                                    &lt;first-name?&gt;{ data($bank/ns10:firstName) }&lt;/first-name&gt;
	                                    &lt;last-name?&gt;{ data($bank/ns10:name) }&lt;/last-name&gt;
	                                &lt;/recipient-name&gt;
	                                &lt;recipient-address? kind?="{ data($invoke1/ns12:addressType/ns8:StringHolder/ns8:value) }"&gt;
	                                	&lt;street?&gt;{ data($bank/ns10:street) }&lt;/street&gt;
	                                	&lt;home?&gt;{ data($bank/ns10:house) }&lt;/home&gt;
	                                	&lt;flat?&gt;{ data($bank/ns10:flat) }&lt;/flat&gt;
	                                	&lt;postal-code?&gt;{ data($bank/ns10:zipCode) }&lt;/postal-code&gt;
	                                	&lt;city?&gt;{ data($bank/ns10:city) }&lt;/city&gt;
	                                	&lt;country?&gt;{ data($bank/ns10:country/ns4:CountryCode/ns4:countryCode) }&lt;/country&gt;
	                                &lt;/recipient-address&gt;
	                                &lt;standing-order-no?&gt;{ data($contract/ns10:transferOrderNumber) }&lt;/standing-order-no&gt;
	                            &lt;/bank&gt;
	                    }
		                &lt;/banks&gt;
		                &lt;coverages&gt;
		                {
		                	for $coverage in $contract/ns10:policyCoverageList/ns10:PolicyCoverage
		                	return 
			                    &lt;coverage client-code = "{ data($invoke1/ns12:customer/ns3:Customer/ns3:customerNumber) }"
			                              coverage-category = "{ data($coverage/ns10:coverageCategory/ns11:UlCoverageCategory/ns11:ulCoverageCategory) }"
			                              product-type? = "{ data($invoke1/ns12:customer/ns3:Customer/ns3:policyContract/ns10:PolicyContract/ns10:productCode/ns11:UlParameters/ns11:productId) }"&gt;
			                        &lt;level?&gt;{ data($coverage/ns10:variant/ns11:OpCoverageVariant/ns11:opCoverageVariant) }&lt;/level&gt;
			                        &lt;owu?&gt;
			                            &lt;signature?&gt;{ data($coverage/ns10:owuSig) }&lt;/signature&gt;
			                        &lt;/owu&gt;
			                        &lt;param-card-sig?&gt;{ data($coverage/ns10:parametrsCardSig) }&lt;/param-card-sig&gt;
			                        &lt;regulation-sig?&gt;{ data($coverage/ns10:regulationSig) }&lt;/regulation-sig&gt;
			                        &lt;dates?&gt;
			                        	&lt;proposal?&gt;{ data($contract/ns10:proposalDate) }&lt;/proposal&gt;
			                        &lt;/dates&gt;
			                        &lt;status&gt;0&lt;/status&gt;
			                        &lt;funds?&gt;
			                            &lt;contract-fund?&gt;
			                            	&lt;funds?&gt;
			                                {
			                                    for $fund in $coverage/ns10:policyCoverageFundSplitList/ns10:PolicyCoverageFundSplit
			                                    return
                                                    &lt;fund fund-name? = "{ data($fund/ns10:fundID/ns11:UlFund/ns11:fundID) }"&gt;
                                                        &lt;split&gt;{ data($fund/ns10:splitPercentage) }&lt;/split&gt;
                                                    &lt;/fund&gt;
			                                }
		                                	&lt;/funds&gt;
			                            &lt;/contract-fund&gt;
			                        &lt;/funds&gt;
	                                &lt;beneficiaries?&gt;
                                    {
                                        for $benefied in $coverage/ns10:policyCoverageBenefitedList/ns10:PolicyCoverageBenefited
                                        return
                                            &lt;beneficiary&gt;
                                                &lt;name&gt;
                                                    &lt;first-name?&gt;{ data($benefied/ns10:firstName) }&lt;/first-name&gt;
                                                    &lt;last-name&gt;{ data($benefied/ns10:name) }&lt;/last-name&gt;
                                                &lt;/name&gt;
                                                &lt;company-ind?&gt;{ data($benefied/ns10:privatePerson) }&lt;/company-ind&gt;
                                                &lt;dob?&gt;{ data($benefied/ns10:birthDate) }&lt;/dob&gt;
                                                &lt;identifiers&gt;
                                                    &lt;pesel?&gt;{ data($benefied/ns10:pesel) }&lt;/pesel&gt;
                                                    &lt;regon?&gt;{ data($benefied/ns10:regon) }&lt;/regon&gt;
                                                &lt;/identifiers&gt;
                                                &lt;split&gt;{ xs:float( data($benefied/ns10:percent) ) }&lt;/split&gt;
                                            &lt;/beneficiary&gt;
                                    }
	                                &lt;/beneficiaries&gt;
			                    &lt;/coverage&gt;
			            }
		                &lt;/coverages&gt;
		                &lt;sales-channel?&gt;{ data($invoke1/ns12:sourceCode/ns8:StringHolder/ns8:value) }&lt;/sales-channel&gt;
		            &lt;/contract&gt;
            }
        &lt;/ns9:addContractRequest&gt;
};

declare variable $invoke1 as element(ns12:invoke) external;
declare variable $header1 as element(ns12:header) external;

xf:RequestTransform($invoke1, $header1)</con:xquery>
</con:xqueryEntry>