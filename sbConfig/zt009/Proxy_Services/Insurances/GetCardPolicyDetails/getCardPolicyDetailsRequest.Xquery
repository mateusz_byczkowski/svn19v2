<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:insurancedict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:transferorder.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:insurance.entities.be.dcl";
declare namespace ns8="urn:be.services.dcl";
declare namespace ns9="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns8:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns8:msgHeader/ns8:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns8:msgHeader/ns8:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
,
&lt;NF_MSHEAD_UNITID?&gt;{data($parm/ns8:msgHeader/ns8:unitId)}&lt;/NF_MSHEAD_UNITID&gt;
,
&lt;NF_MSHEAD_USERID?&gt;{data($parm/ns8:msgHeader/ns8:userId)}&lt;/NF_MSHEAD_USERID&gt;
,
&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns8:msgHeader/ns8:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns8:msgHeader/ns8:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
,
&lt;NF_TRHEAD_TRANSID?&gt;{data($parm/ns8:transHeader/ns8:transId)}&lt;/NF_TRHEAD_TRANSID&gt;
};
declare function getFieldsFromInvoke($parm as element(ns8:invoke)) as element()*
{

&lt;NF_INSPAC_POLICYREFNUM?&gt;{data($parm/ns8:insurancePolicyAcc/ns7:InsurancePolicyAcc/ns7:policyRefNum)}&lt;/NF_INSPAC_POLICYREFNUM&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns8:header)}
    {getFieldsFromInvoke($body/ns8:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>