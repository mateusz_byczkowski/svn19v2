<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:insurance.entities.be.dcl";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:anyType) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};

declare function xf:getFields($parm as element(ns6:invoke), $msghead as element(ns6:msgHeader), $tranhead as element(ns6:transHeader))
	as element(fml:FML32) {

let $msgId:= $msghead/ns6:msgId
let $companyId:= $msghead/ns6:companyId
let $userId := $msghead/ns6:userId
let $appId:= $msghead/ns6:appId
let $unitId := $msghead/ns6:unitId
let $timestamp:= $msghead/ns6:timestamp

let $transId:=$tranhead/ns6:transId

return
  &lt;fml:FML32&gt;
     &lt;DC_TRN_ID?&gt;{data($transId)}&lt;/DC_TRN_ID&gt;
     &lt;DC_UZYTKOWNIK?&gt;{concat("SKP:", data($userId))}&lt;/DC_UZYTKOWNIK&gt;
     &lt;DC_ODDZIAL?&gt;{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL&gt;
     &lt;DC_IS_NR_REFF_POLISY?&gt;{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:policyRefNum)}&lt;/DC_IS_NR_REFF_POLISY&gt;
     &lt;DC_DATA_WAZNOSCI?&gt;{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:validityDate)}&lt;/DC_DATA_WAZNOSCI&gt;
     &lt;DC_IS_DNI_POWIAD_PRZED_WYGAS?&gt;{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:reminderDays)}&lt;/DC_IS_DNI_POWIAD_PRZED_WYGAS&gt;
     &lt;DC_IS_STALA_SKLADKA_UBEZP?&gt;{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:fixedPremium)}&lt;/DC_IS_STALA_SKLADKA_UBEZP&gt;
     &lt;DC_IS_DATA_NALEZNOSCI_SKLADKI?&gt;{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:premiumDueDate)}&lt;/DC_IS_DATA_NALEZNOSCI_SKLADKI&gt;
     &lt;DC_IS_NIETYPOWY_DZIEN_SKLADKI?&gt;{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:oddPremiumDay/ns3:SpecialDay/ns3:specialDay)}&lt;/DC_IS_NIETYPOWY_DZIEN_SKLADKI&gt;
	 {
     if (data($parm/ns6:account/ns1:Account/ns1:accountNumber)) 
      then &lt;DC_IS_NR_RACHUNKU_W_SYSTEMIE?&gt;{cutStr(data($parm/ns6:account/ns1:Account/ns1:accountNumber),12)}&lt;/DC_IS_NR_RACHUNKU_W_SYSTEMIE&gt;
     else()
	 }
  &lt;/fml:FML32&gt;
};

&lt;soap:Body&gt;
     { xf:getFields($body/ns6:invoke, $header/ns6:header/ns6:msgHeader, $header/ns6:header/ns6:transHeader) }
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>