<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/XMLExamples/bzwbk/ResponseTransform/";
declare namespace ns0 = "http://cu.com.pl/cmf-wsdl-types";
declare namespace ns1 = "http://cu.com.pl/cmf-holding-types";
declare namespace ns-1 = "http://cu.com.pl/cmf-biz-types";
declare namespace ns-3 = "urn:applicationul.entities.be.dcl";
declare namespace ns3 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace ns-4 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns-2 = "urn:baseentities.be.dcl";

declare function xf:ResponseTransform($getContractListResponse1 as element(ns3:getContractListResponse))
    as element(ns2:invokeResponse) {
        &lt;ns2:invokeResponse&gt;
            &lt;ns2:messageHelper&gt;
                &lt;ns-2:MessageHelper&gt;
                    &lt;ns-2:errorCode?&gt;&lt;/ns-2:errorCode&gt;
                    &lt;ns-2:errorType?&gt;&lt;/ns-2:errorType&gt;
                    &lt;ns-2:message?&gt;&lt;/ns-2:message&gt;
                &lt;/ns-2:MessageHelper&gt;
            &lt;/ns2:messageHelper&gt;
            {
                let $contracts := $getContractListResponse1/contracts
                return
                    &lt;ns2:policyList&gt;
                        {
                            for $contract in $contracts/contract
                            return
                                &lt;ns-3:PolicyList&gt;
                                    &lt;ns-3:proposalID&gt;{ xs:long( data($contract/proposal-number) ) }&lt;/ns-3:proposalID&gt;
                                    &lt;ns-3:policyID&gt;{ xs:long( data($contract/@contract-number) ) }&lt;/ns-3:policyID&gt;
                                    &lt;ns-3:productID&gt;{ xs:string( data($contract/@product-type) ) }&lt;/ns-3:productID&gt;
                                    &lt;ns-3:proposalDate&gt;{ data($contract/contract-dates/proposal) }&lt;/ns-3:proposalDate&gt;
                                    &lt;ns-3:accountNoPayment&gt;{ 
                                    	let $accounts :=
                                    		for $bank in $contract/banks/bank
                                    		where $bank/@kind = '1'
                                    		return
                                    			data($bank/account)
                                    	return $accounts[1]
                                    }&lt;/ns-3:accountNoPayment&gt;
                                    &lt;ns-3:policyValue&gt;{ xs:double( data($contract/amount) ) }&lt;/ns-3:policyValue&gt;
                                    &lt;ns-3:status&gt;
                                        &lt;ns-4:UlContractStatus&gt;
                                            &lt;ns-4:ulContractStatus&gt;{ xs:string( data($contract/status) ) }&lt;/ns-4:ulContractStatus&gt;
                                        &lt;/ns-4:UlContractStatus&gt;
                                    &lt;/ns-3:status&gt;
                                &lt;/ns-3:PolicyList&gt;
                        }
                    &lt;/ns2:policyList&gt;
            }
        &lt;/ns2:invokeResponse&gt;
};

declare variable $getContractListResponse1 as element(ns3:getContractListResponse) external;

xf:ResponseTransform($getContractListResponse1)</con:xquery>
</con:xqueryEntry>