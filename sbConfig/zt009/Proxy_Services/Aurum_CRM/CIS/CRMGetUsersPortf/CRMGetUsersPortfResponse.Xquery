<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetUsersPortfResponse($fml as element(fml:FML32))
	as element(m:CRMGetUsersPortfResponse) {
		&lt;m:CRMGetUsersPortfResponse&gt;
			{
				if($fml/fml:CI_NUMER_PACZKI)
					then &lt;m:NumerPaczki&gt;{ data($fml/fml:CI_NUMER_PACZKI) }&lt;/m:NumerPaczki&gt;
					else ()
			}
			{
				let $CI_SKP_PRACOWNIKA := $fml/CI_SKP_PRACOWNIKA

				for $it at $p in $fml/fml:CI_SKP_PRACOWNIKA
					return
						&lt;m:SkpPracownika&gt;{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:SkpPracownika&gt;
			}
			{
				let $DC_NUMER_ODDZIALU := $fml/DC_NUMER_ODDZIALU

				for $it at $p in $fml/fml:DC_NUMER_ODDZIALU
					return
						&lt;m:NumerOddzialu&gt;{ data($DC_NUMER_ODDZIALU[$p]) }&lt;/m:NumerOddzialu&gt;
			}
			{
				let $CI_ID_PORTFELA    := $fml/CI_ID_PORTFELA

				for $it at $p in $fml/fml:CI_ID_PORTFELA
					return
						&lt;m:IdPortfela&gt;{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela&gt;
			}
		&lt;/m:CRMGetUsersPortfResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetUsersPortfResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>