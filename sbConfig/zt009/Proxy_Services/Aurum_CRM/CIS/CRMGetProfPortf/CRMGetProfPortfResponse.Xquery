<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function xf:mapping($buf as element(fml:FML32), $req as element(fml:FML32))
	as element() {
		&lt;portfolio_profit_report&gt;
			{
				if($req/CI_ZNACZNIK_OKRESU)
					then &lt;period&gt;{ data($req/CI_ZNACZNIK_OKRESU) }&lt;/period&gt;
				else ()
			}
			{
				if($req/CI_DECYL)
					then &lt;decile&gt;{ data($req/CI_DECYL) }&lt;/decile&gt;
				else ()
			}
			{
				if($req/DC_TYP_KLIENTA)
					then &lt;cust_type_in&gt;{ data($req/DC_TYP_KLIENTA) }&lt;/cust_type_in&gt;
				else ()
			}
			{
				if($buf/CI_NUMER_PACZKI_STR)
					then &lt;package_no&gt;{ data($buf/CI_NUMER_PACZKI_STR) }&lt;/package_no&gt;
				else ()
			}
			&lt;portf_profitability&gt;
				{
					if($req/DC_TYP_KLIENTA) then
			   			&lt;cust_type code="{ data($req/DC_TYP_KLIENTA) }"&gt;
			   			{
			   				for $it at $p in $buf/DC_NUMER_KLIENTA
			   				return
								&lt;customer&gt;
									{
										if($buf/DC_NUMER_KLIENTA[$p])
											then &lt;param code="CIF"&gt;{ data($buf/DC_NUMER_KLIENTA[$p]) }&lt;/param&gt;
										else
											()
									}
									{	(: dla fizycznych imie -&gt; name :)
										if(data($buf/DC_IMIE[$p]) != "")
											then &lt;param code="NAME"&gt;{ data($buf/DC_IMIE[$p]) }&lt;/param&gt;
										else ()
									}
									{	(: dla prawnych nazwa pea -&gt; name :)
										if(data($buf/CI_NAZWA_PELNA[$p]) != "")
											then &lt;param code="NAME"&gt;{ data($buf/CI_NAZWA_PELNA[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/DC_NAZWISKO[$p])
											then &lt;param code="SURNAME"&gt;{ data($buf/DC_NAZWISKO[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_DECYL[$p])
											then &lt;param code="DECILE"&gt;{ data($buf/CI_DECYL[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_DOCHODY[$p])
											then &lt;param code="INCOME"&gt;{ data($buf/CI_DOCHODY[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_DOCHODY_ODSETKOWE[$p])
											then &lt;param code="INCOME_INT"&gt;{ data($buf/CI_DOCHODY_ODSETKOWE[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_DOCHODY_POZOSTALE[$p])
											then &lt;param code="INCOME_OTHER"&gt;{ data($buf/CI_DOCHODY_POZOSTALE[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_KOSZTY_BEZPOSREDNIE[$p])
											then &lt;param code="DIR_COSTS"&gt;{ data($buf/CI_KOSZTY_BEZPOSREDNIE[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_KOSZTY_POSREDNIE[$p])
											then &lt;param code="INDIR_COSTS"&gt;{ data($buf/CI_KOSZTY_POSREDNIE[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_KOSZTY_RYZYKA[$p])
											then &lt;param code="RISK_COSTS"&gt;{ data($buf/CI_KOSZTY_RYZYKA[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_WYNIK[$p])
											then &lt;param code="RESULT"&gt;{ data($buf/CI_WYNIK[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/DC_PRACOWNIK_BANKU[$p])
											then &lt;param code="EMPLOYEE_FLAG"&gt;{ data($buf/DC_PRACOWNIK_BANKU[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_VIP[$p])
											then &lt;param code="VIP"&gt;{ data($buf/CI_VIP[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_KLASA_OBSLUGI[$p])
											then &lt;param code="CI_KLASA_OBSLUGI"&gt;{ data($buf/CI_KLASA_OBSLUGI[$p]) }&lt;/param&gt;
										else ()
									}
								&lt;/customer&gt;
							}
			   			&lt;/cust_type&gt;
					else
						for $typ at $p in $buf/DC_TYP_KLIENTA
						return
				   			&lt;cust_type code="{ data($typ) }"&gt;
								&lt;customer&gt;
									&lt;!-- pola niezmapowane: CI_ID_PORTFELA, CI_ZNACZNIK_OKRESU --&gt;
									{
										if($buf/CI_DECYL[$p])
											then &lt;param code="DECILE"&gt;{ data($buf/CI_DECYL[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_DOCHODY[$p])
											then &lt;param code="INCOME"&gt;{ data($buf/CI_DOCHODY[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_KOSZTY_BEZPOSREDNIE[$p])
											then &lt;param code="DIR_COSTS"&gt;{ data($buf/CI_KOSZTY_BEZPOSREDNIE[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_KOSZTY_POSREDNIE[$p])
											then &lt;param code="INDIR_COSTS"&gt;{ data($buf/CI_KOSZTY_POSREDNIE[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_KOSZTY_RYZYKA[$p])
											then &lt;param code="RISK_COSTS"&gt;{ data($buf/CI_KOSZTY_RYZYKA[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_WYNIK[$p])
											then &lt;param code="RESULT"&gt;{ data($buf/CI_WYNIK[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/DC_PRACOWNIK_BANKU[$p])
											then &lt;param code="EMPLOYEE_FLAG"&gt;{ data($buf/DC_PRACOWNIK_BANKU[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_VIP[$p])
											then &lt;param code="VIP"&gt;{ data($buf/CI_VIP[$p]) }&lt;/param&gt;
										else ()
									}
									{
										if($buf/CI_KLASA_OBSLUGI[$p])
											then &lt;param code="KLASA_OBSLUGI"&gt;{ data($buf/CI_KLASA_OBSLUGI[$p]) }&lt;/param&gt;
										else ()
									}
								&lt;/customer&gt;
				   			&lt;/cust_type&gt;
				}
			&lt;/portf_profitability&gt;
		&lt;/portfolio_profit_report&gt;
};


declare variable $buf as element(fml:FML32) external;
declare variable $req as element(fml:FML32) external;

&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"&gt;
	&lt;m:CRMGetProfPortfResponse&gt;{ fn-bea:serialize(xf:mapping($buf, $req)) }&lt;/m:CRMGetProfPortfResponse&gt;
&lt;/soapenv:Body&gt;</con:xquery>
</con:xqueryEntry>