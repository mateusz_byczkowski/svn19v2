<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISGetCifRequest($req as element(m:CISGetCifRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE&gt;{ data($req/m:Imie) }&lt;/fml:DC_IMIE&gt;
					else ()
			}
			{
				if ($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO&gt;{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO&gt;
					else ()
			}
			{
				if ($req/m:NazwaPelna)
					then &lt;fml:CI_NAZWA_PELNA&gt;{ data($req/m:NazwaPelna) }&lt;/fml:CI_NAZWA_PELNA&gt;
					else ()
			}
			{
				if ($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON&gt;{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON&gt;
					else ()
			}
			{
				if ($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL&gt;{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL&gt;
					else ()
			}
			{
				if ($req/m:Nip)
					then &lt;fml:DC_NIP&gt;{ data($req/m:Nip) }&lt;/fml:DC_NIP&gt;
					else ()
			}
			{
				if ($req/m:NumerPaszportu)
					then &lt;fml:DC_NUMER_PASZPORTU&gt;{ data($req/m:NumerPaszportu) }&lt;/fml:DC_NUMER_PASZPORTU&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body&gt;
{ xf:mapCISGetCifRequest($body/m:CISGetCifRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>