<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace w = "urn:be.services.dcl";
declare namespace w1 = "urn:hlbsentities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare namespace urn = "urn:RatingWS";

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body&gt;
{
    let $req := $body/urn:saveDecisionResponse/return
    return
    
    &lt;w:invokeResponse xmlns:urn="urn:be.services.dcl"&gt;
      &lt;w:output xmlns:urn1="urn:hlbsentities.be.dcl"&gt;
        &lt;w1:RatingDecisionSWWROutput&gt;
          {if($req/status)
              then &lt;w1:status&gt;{ data($req/status) }&lt;/w1:status&gt;
              else ()
          }
          {if($req/errormessage)
              then &lt;w1:errorMessage&gt;{ data($req/errormessage) }&lt;/w1:errorMessage&gt;
              else ()
          }
        &lt;/w1:RatingDecisionSWWROutput&gt;
      &lt;/w:output&gt;
    &lt;/w:invokeResponse&gt;
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>