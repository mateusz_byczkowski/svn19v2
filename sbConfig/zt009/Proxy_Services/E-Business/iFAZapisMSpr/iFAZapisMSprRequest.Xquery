<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ARKA24/E-Business/messages/";
declare namespace xf = "http://bzwbk.com/services/ARKA24/E-Business/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapiFAZapisMSprRequest($req as element(m:iFAZapisMSprRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				&lt;fml:E_LOGIN_ID?&gt;{ data($req/m:LoginId) }&lt;/fml:E_LOGIN_ID&gt;
			}
			{
				&lt;fml:B_RODZ_DOK?&gt;{ data($req/m:RodzDok) }&lt;/fml:B_RODZ_DOK&gt;
			}
			{
				&lt;fml:B_NR_DOK?&gt;{ data($req/m:NrDok) }&lt;/fml:B_NR_DOK&gt;
			}
			{
				&lt;fml:E_CIF_NUMBER?&gt;{ data($req/m:CifNumber) }&lt;/fml:E_CIF_NUMBER&gt;
			}
			{
				&lt;fml:FA_KONWERSJA?&gt;{ data($req/m:Konwersja) }&lt;/fml:FA_KONWERSJA&gt;
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapiFAZapisMSprRequest($body/m:iFAZapisMSprRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>