<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "http://bzwbk.com/services/prime/";
declare namespace fault = "http://bzwbk.com/services/BZWBK24flow/faults/";

declare function xf:mapFault($fau as element(soap-env:Fault))
	as element(fml:FML32) {
	&lt;fml:FML32&gt;
		{
			if ($fau/detail/*/exceptionItem) then
			(
				&lt;fml:U_FAULT_STRING&gt;{ data($fau/detail/*/exceptionItem/errorDescription) }&lt;/fml:U_FAULT_STRING&gt;,
				&lt;fml:U_ERROR_CODE&gt;{ data($fau/detail/*/exceptionItem/errorCode1) }&lt;/fml:U_ERROR_CODE&gt;,
                                &lt;fml:U_ERROR_CODE&gt;{ data($fau/detail/*/exceptionItem/errorCode2) }&lt;/fml:U_ERROR_CODE&gt;
			) else (
				&lt;fml:U_FAULT_STRING&gt;{ data($fau/faultstring) }&lt;/fml:U_FAULT_STRING&gt;,
                               &lt;fml:U_ERROR_CODE&gt;30&lt;/fml:U_ERROR_CODE&gt;		
			)

		}
	&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapFault($body/soap-env:Fault) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>