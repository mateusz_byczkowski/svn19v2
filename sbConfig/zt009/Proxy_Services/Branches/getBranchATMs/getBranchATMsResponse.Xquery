<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2009-12-11
 :
 : wersja WSDLa: 24-03-2010 10:14:05
 :
 : $Proxy Services/Branches/getBranchATMs/getBranchATMsResponse.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getBranchATMs/getBranchATMsResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getBranchATMsResponse($fML321 as element(ns0:FML32))
    as element(ns5:invokeResponse)
{
	&lt;ns5:invokeResponse&gt;
        &lt;ns5:atmList&gt;{
           	for $i in 1 to count($fML321/ns0:NF_CURREC_CURRENCYCODE)
           	return
                &lt;ns1:CurrencyCash&gt;
                    &lt;ns1:amount&gt;{
						data($fML321/ns0:NF_CURRCA_AMOUNT[$i])
					}&lt;/ns1:amount&gt;
                    &lt;ns1:atm&gt;
                        &lt;ns4:ATM&gt;
                            &lt;ns4:atmNumber&gt;{
								data($fML321/ns0:NF_ATM_ATMNUMBER[$i])
							}&lt;/ns4:atmNumber&gt;
                            &lt;ns4:atmType&gt;
                                &lt;ns3:ATMType&gt;
                                    &lt;ns3:atmType&gt;{
										data($fML321/ns0:NF_ATMT_ATMTYP[$i])
									}&lt;/ns3:atmType&gt;
                                &lt;/ns3:ATMType&gt;
                            &lt;/ns4:atmType&gt;
                            &lt;ns4:till&gt;
                                &lt;ns4:Till&gt;
                                    &lt;ns4:tillID&gt;{
										data($fML321/ns0:NF_TILL_TILLID[$i])
									}&lt;/ns4:tillID&gt;
									
                                    &lt;ns4:tillName&gt;{
										data($fML321/ns0:NF_TILL_TILLNAME[$i])
									}&lt;/ns4:tillName&gt;
                                &lt;/ns4:Till&gt;
                            &lt;/ns4:till&gt;
                            &lt;ns4:teller&gt;
                                &lt;ns4:Teller&gt;
                                    &lt;ns4:tellerID&gt;{
										data($fML321/ns0:NF_TELLER_TELLERID[$i])
									}&lt;/ns4:tellerID&gt;
                                &lt;/ns4:Teller&gt;
                            &lt;/ns4:teller&gt;
                        &lt;/ns4:ATM&gt;
                    &lt;/ns1:atm&gt;
                    &lt;ns1:currency&gt;
                        &lt;ns2:CurrencyCode&gt;
                            &lt;ns2:currencyCode&gt;{
								data($fML321/ns0:NF_CURREC_CURRENCYCODE[$i])
							}&lt;/ns2:currencyCode&gt;
                        &lt;/ns2:CurrencyCode&gt;
                    &lt;/ns1:currency&gt;
               	&lt;/ns1:CurrencyCash&gt;	                    
        }&lt;/ns5:atmList&gt;
	&lt;/ns5:invokeResponse&gt;
};

&lt;soap-env:Body&gt;{
	xf:getBranchATMsResponse($fML321)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>