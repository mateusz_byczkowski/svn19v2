<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns4:header" location="getBranchATMs.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns4:invoke" location="getBranchATMs.WSDL" ::)
(:: pragma bea:global-element-return element="ns1:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @author  Tomasz Krajewski
 : @version 1.2
 : @since   2010-08-18
 :
 : wersja WSDLa: 18-08-2010 09:08:07
 :
 : $Proxy Services/Branches/getBranchATMs/getBranchATMsRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getBranchATMs/getBranchATMsRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:be.services.dcl";

declare variable $header1 as element(ns4:header) external;
declare variable $invoke1 as element(ns4:invoke) external;


(:~
 : @param invoke1 - operacja wejściowa
 : @param header1 - nagłówek wiadomości
 :
 : @return bufor XML/FML
 :)
declare function xf:getBranchATMsRequest($header1 as element(ns4:header),
										   $invoke1 as element(ns4:invoke))
    as element(ns0:FML32)
{
	&lt;ns0:FML32&gt;
	
		(:
		 : dane z naglowka
		 :)
    	&lt;ns0:NF_MSHEAD_MSGID?&gt;{
			data($header1/ns4:msgHeader/ns4:msgId)
		}&lt;/ns0:NF_MSHEAD_MSGID&gt;
		
		(:
		 : dane z operacji wejsciowej
		 :)
		{
			for $ATM in $invoke1/ns4:atm/ns3:ATM
			return
			(
				for $atmType in $ATM/ns3:atmType/ns2:ATMType/ns2:atmType
				return	
					&lt;ns0:NF_ATMT_ATMTYP&gt;{ data($atmType) }&lt;/ns0:NF_ATMT_ATMTYP&gt;
			)
		}	
        &lt;ns0:NF_BRANCC_BRANCHCODE?&gt;{
			data($invoke1/ns4:branchCode/ns1:BranchCode/ns1:branchCode)
		}&lt;/ns0:NF_BRANCC_BRANCHCODE&gt;
		
    &lt;/ns0:FML32&gt;
};

&lt;soap-env:Body&gt;{
	xf:getBranchATMsRequest($header1, $invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>