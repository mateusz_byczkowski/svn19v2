<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/epacksubsnew/messages/";
declare namespace xf = "http://bzwbk.com/services/epacksubsnew/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";


declare variable $body as element(soap-env:Body) external;

declare variable $req as element (m:ePackSubsNewRequest):=$body/m:ePackSubsNewRequest;

&lt;soap-env:Body&gt;	
		&lt;FML32&gt;
			{
				if($req/m:LoginId)
					then &lt;E_LOGIN_ID&gt;{ data($req/m:LoginId) }&lt;/E_LOGIN_ID&gt;
					else ()
			}
			{
				if($req/m:PackageType)
					then &lt;EV_PACKAGE_TYPE&gt;{ data($req/m:PackageType) }&lt;/EV_PACKAGE_TYPE&gt;
					else ()
			}
			{
				if($req/m:MarketingAgree)
					then &lt;EV_MARKETING_AGREE&gt;{ data($req/m:MarketingAgree) }&lt;/EV_MARKETING_AGREE&gt;
					else ()
			}
			{
				if($req/m:UserName)
					then &lt;U_USER_NAME&gt;{ data($req/m:UserName) }&lt;/U_USER_NAME&gt;
					else ()
			}
			{
				if($req/m:DelivTimeFrom)
					then &lt;EV_DELIV_TIME_FROM&gt;{ data($req/m:DelivTimeFrom) }&lt;/EV_DELIV_TIME_FROM&gt;
					else ()
			}
			{
				if($req/m:DelivTimeTo)
					then &lt;EV_DELIV_TIME_TO&gt;{ data($req/m:DelivTimeTo) }&lt;/EV_DELIV_TIME_TO&gt;
					else ()
			}
			{
			            for $email in $req/m:emailList/m:CustEmail return
				    &lt;E_CUST_EMAIL&gt;{ data($email) }&lt;/E_CUST_EMAIL&gt;
			}
			{
			            for $gsm in $req /m:gsmList/m:CustGsmno return
				    &lt;E_CUST_GSMNO&gt;{ data($gsm) }&lt;/E_CUST_GSMNO&gt;
			}
			
		&lt;/FML32&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>