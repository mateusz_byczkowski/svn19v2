<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapRow($areaId as xs:anyType,
                           $nrRachunku as xs:anyType,
                           $numerKlienta as xs:anyType,
			   $typRelacji as xs:anyType,
			   $procentZobPodatkowych as xs:anyType) as element()*{	   


          &lt;fml:DC_NR_RACHUNKU&gt;{xf:prepareShortAccountNumber($nrRachunku)}&lt;/fml:DC_NR_RACHUNKU&gt;,
          &lt;fml:DC_KOD_APLIKACJI&gt;{xf:prepareApplicationCode($areaId)}&lt;/fml:DC_KOD_APLIKACJI&gt;,
	  &lt;fml:DC_NUMER_KLIENTA&gt;{$numerKlienta}&lt;/fml:DC_NUMER_KLIENTA&gt;,
          &lt;fml:DC_RELACJA&gt;{$typRelacji }&lt;/fml:DC_RELACJA&gt;,
          &lt;fml:DC_TYP_RELACJI&gt;1&lt;/fml:DC_TYP_RELACJI&gt;,
	  &lt;fml:DC_PROCENT_ZOB_PODATKOWYCH&gt;{$procentZobPodatkowych}&lt;/fml:DC_PROCENT_ZOB_PODATKOWYCH&gt; 
};


declare function xf:mapNullRow() as element()*{	   
          &lt;fml:DC_NR_RACHUNKU nil="true"/&gt;, 
          &lt;fml:DC_KOD_APLIKACJI nil="true"/&gt;,
	  &lt;fml:DC_NUMER_KLIENTA nil="true"/&gt;,
          &lt;fml:DC_RELACJA nil="true"/&gt;,
          &lt;fml:DC_TYP_RELACJI nil="true"/&gt;,
	  &lt;fml:DC_PROCENT_ZOB_PODATKOWYCH nil="true"/&gt;
};

declare function xf:mapPackageFullRange($input as element(m:Paczka)) as element()*{
   let $nrRachunku:=data($input/m:NrRachunku)
   let $areaId:=data($input/m:ProductAreaId)
   return
   for $i in  (1 to 7)
     return  
        if (data($input/m:Relacja[$i])) then
	   xf:mapRow($areaId,$nrRachunku,data($input/m:Relacja/m:NumerKlienta),
	      data($input/m:Relacja/m:TypRelacji),data($input/m:Relacja/m:ProcentZobPodatkowych))
        else
	   xf:mapNullRow()
};

declare function xf:mapPackagePartialRange($input as element(m:Paczka)) as element()*{
   let $nrRachunku:=data($input/m:NrRachunku)
   let $areaId:=data($input/m:ProductAreaId)
   return
   for $x at $i in $input/m:Relacja
     return 
       xf:mapRow($areaId,$nrRachunku,data($x/m:NumerKlienta),data($x/m:TypRelacji),data($x/m:ProcentZobPodatkowych))
       
};

declare function xf:prepareShortAccountNumber($accountNumber as xs:anyType) as xs:string{
   let $accountNumberLength:= string-length($accountNumber)
   let $shortAccountNumber := substring($accountNumber,$accountNumberLength - 9)
   let $shortAccountNumberWithZeros := concat(substring("0000000000", string-length($shortAccountNumber) + 1) , $shortAccountNumber)
   return
       $shortAccountNumber
};


declare function xf:prepareApplicationCode($areaId as xs:anyType) as xs:string{
      if ($areaId = "2" or $areaId = "13")
         then "20"
     else if ($areaId = "10")
         then "30"
     else if ($areaId = "3")
         then "50"
     else "0"
};

declare function xf:mapCRMMngProdRelRequest($req as element(m:CRMMngProdRelRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:TrnId)
					then &lt;fml:DC_TRN_ID&gt;{ data($req/m:TrnId) }&lt;/fml:DC_TRN_ID&gt;
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK&gt;{concat("SKP:", data($req/m:Uzytkownik)) }&lt;/fml:DC_UZYTKOWNIK&gt;
					else ()
			}
			{
				if($req/m:Oddzial)
					then &lt;fml:DC_ODDZIAL&gt;{ data($req/m:Oddzial) }&lt;/fml:DC_ODDZIAL&gt;
					else ()
			}
			{
			  for $i in (1 to 4)
			   return
			     if ($req/m:Paczka[ $i + 1 ]) then
			       xf:mapPackageFullRange($req/m:Paczka[$i])
			     else
			       if ($req/m:Paczka[ $i ]) then
                                 xf:mapPackagePartialRange($req/m:Paczka[$i])
			       else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMMngProdRelRequest($body/m:CRMMngProdRelRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>