<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "urn:be.services.dcl";
declare namespace m1 = "urn:hlbsentities.be.dcl";
declare namespace w = "";
declare namespace u = "urn:ICBReportWebServices";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

&lt;soap-env:Body&gt;
{
    let $req := $body/m:invoke/m:input/m1:ConvertBIKReportInput
    return
    
    &lt;urn:convertBIKReport xmlns:urn="urn:ICBReportWebServices" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;
      {if($req/m1:reportBIK)
          then &lt;w:arg_0_1 xsi:type="xs:string"&gt;{data($req/m1:reportBIK)}&lt;/w:arg_0_1&gt;
          else ()
      }
      {if($req/m1:reportType)
          then &lt;w:arg_1_1 xsi:type="xs:int"&gt;{data($req/m1:reportType) }&lt;/w:arg_1_1&gt;
          else ()
      }
    &lt;/urn:convertBIKReport&gt;
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>