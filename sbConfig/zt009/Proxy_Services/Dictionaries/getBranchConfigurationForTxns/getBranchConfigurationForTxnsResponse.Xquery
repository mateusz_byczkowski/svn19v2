<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2010-02-19
 :
 : wersja WSDLa: 21-01-2010 16:00:20
 :
 : $Proxy Services/Dictionaries/getBranchConfigurationForTxns/getBranchConfigurationForTxnsResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Dictionaries/getBranchConfigurationForTxns/getBranchConfigurationForTxnsResponse/";
declare namespace ns0 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa - słownik konfiguracji oddziału
 :)
declare function xf:getBranchConfigurationForTxnsResponse($fML321 as element(ns1:FML32))
    as element(ns2:invokeResponse)
{
    &lt;ns2:invokeResponse&gt;
        &lt;ns2:dicts&gt;{
        
        	for $i in 1 to count($fML321/ns1:NF_BRACFT_BRANCHCODE)
        	return
	            &lt;ns0:BranchConfigurationForTxn&gt;

	                &lt;ns0:branchCode&gt;{
						data($fML321/ns1:NF_BRACFT_BRANCHCODE[$i])
					}&lt;/ns0:branchCode&gt;

					{
						let $dateFrom := $fML321/ns1:NF_BRACFT_DATEFROM[$i]
						return
							if (data($dateFrom)) then
			                	&lt;ns0:dateFrom&gt;{
			                		data($dateFrom)
			                	}&lt;/ns0:dateFrom&gt;
			                else ()
					}

					{
						let $dateTo := $fML321/ns1:NF_BRACFT_DATETO[$i]
						return
							if (data($dateTo)) then
				                &lt;ns0:dateTo&gt;{
									data($dateTo)
								}&lt;/ns0:dateTo&gt;
							else ()
					}

                	&lt;ns0:lastChangeUserSkp&gt;{
						data($fML321/ns1:NF_BRACFT_LASTCHANGEUSERSK[$i])
					}&lt;/ns0:lastChangeUserSkp&gt;

					{
						let $lastChangeDate := $fML321/ns1:NF_BRACFT_LASTCHANGEDATE[$i]
						return
							if (data($lastChangeDate)) then
				                &lt;ns0:lastChangeDate&gt;{
									data($lastChangeDate)
								}&lt;/ns0:lastChangeDate&gt;
							else ()
					}

                	&lt;ns0:treasuryFlag&gt;{
						xs:boolean(data($fML321/ns1:NF_BRACFT_TREASURYFLAG[$i]))
					}&lt;/ns0:treasuryFlag&gt;

                	&lt;ns0:cifForBranch&gt;{
						data($fML321/ns1:NF_BRACFT_CIFFORBRANCH[$i])
					}&lt;/ns0:cifForBranch&gt;

	                &lt;ns0:branchCostCenter&gt;{
						data($fML321/ns1:NF_BRACFT_BRANCHCOSTCENTER[$i])
					}&lt;/ns0:branchCostCenter&gt;

                	&lt;ns0:parentBranchCostCenter&gt;{
						data($fML321/ns1:NF_BRACFT_PARENTBRANCHCOST[$i])
					}&lt;/ns0:parentBranchCostCenter&gt;

            &lt;/ns0:BranchConfigurationForTxn&gt;
            
        }&lt;/ns2:dicts&gt;
    &lt;/ns2:invokeResponse&gt;
};

&lt;soap-env:Body&gt;{
	xf:getBranchConfigurationForTxnsResponse($fML321)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>