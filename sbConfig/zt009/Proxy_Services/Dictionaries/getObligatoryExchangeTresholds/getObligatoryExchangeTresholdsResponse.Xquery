<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(: Change log
v.1.1  2010-06-09 PKLI T47963 Zmiana typu pola currency na String, zmiana z kodu numerycznego na znakowy
:)

declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
(:1.1 declare namespace ns3="urn:dictionaries.be.dcl"; :)
declare namespace ns4="urn:operationsdictionary.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{

&lt;ns0:dicts&gt;
  {
    for $x at $occ in $parm/NF_OBLIET_LIMIT
    return
    &lt;ns4:ObligatoryExchangeTreshold&gt;
      &lt;ns4:currency?&gt;{data($parm/B_RET_DATA[$occ])}&lt;/ns4:currency&gt; (:1.1:)
      &lt;ns4:limit?&gt;{data($parm/NF_OBLIET_LIMIT[$occ])}&lt;/ns4:limit&gt;
(: 1.1    &lt;ns4:currency&gt;
        &lt;ns3:CurrencyCode&gt;
          &lt;ns3:currencyCode?&gt;{data($parm/B_KOD_PS[$occ])}&lt;/ns3:currencyCode&gt;
        &lt;/ns3:CurrencyCode&gt;
      &lt;/ns4:currency&gt; 1.1 :)
    &lt;/ns4:ObligatoryExchangeTreshold&gt;
  }
&lt;/ns0:dicts&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  {getElementsForDicts($parm)}
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>