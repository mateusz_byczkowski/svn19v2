<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2010-03-15 PKL NP1696: Usunięcie pól: description; Dodanie pól: nameSecond</con:description>
  <con:xquery>(: Change log
v.1.1 2010-03-15 PKL NP1696: Usunięcie pól: description; Dodanie pól: nameSecond
:)

declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace urn2="urn:entities.be.dcl";
declare namespace urn3="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function Num2Bool($dateIn as xs:string) as xs:boolean {
 if ($dateIn = "1") 
    then true()
    else false()
};

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
&lt;ns0:dicts&gt;
  {
    let $nazwa := $parm/B_NAZWA 
    let $nazwaCd := $parm/B_NAZWA_CD (: 1.1 :) 
    let $miasto := $parm/B_MIASTO 
    let $retData := $parm/B_RET_DATA 
    for $nrRach at $occ in $parm/B_NR_RACH
    return
    &lt;ns3:USAccount&gt;
      &lt;ns3:accountNumber?&gt;{data($nrRach)}&lt;/ns3:accountNumber&gt;
(:    &lt;ns3:description?&gt;{data($nazwa[$occ])}&lt;/ns3:description&gt;  1.1 :)
      &lt;ns3:name?&gt;{data($nazwa[$occ])}&lt;/ns3:name&gt;
      &lt;ns3:city?&gt;{data($miasto[$occ])}&lt;/ns3:city&gt;
      &lt;ns3:formType?&gt;{data($retData[$occ])}&lt;/ns3:formType&gt;
      &lt;ns3:nameSecond?&gt;{data($nazwaCd[$occ])}&lt;/ns3:nameSecond&gt; (: 1.1 :)
    &lt;/ns3:USAccount&gt;
  }
&lt;/ns0:dicts&gt;
};

declare function getElementsForBcd($parm as element(fml:FML32)) as element()
{
let $lastValue := data(fn:count($parm/B_NR_RACH))
return
&lt;ns0:bcd&gt;
  &lt;urn2:BusinessControlData&gt;
      &lt;urn2:pageControl&gt;
          &lt;urn3:PageControl&gt;
              &lt;urn3:hasNext?&gt;{Num2Bool(data($parm/B_OVERFLOW))}&lt;/urn3:hasNext&gt;
              &lt;urn3:navigationKeyValue?&gt;{data($parm/B_NR_RACH[$lastValue])}&lt;/urn3:navigationKeyValue&gt;
          &lt;/urn3:PageControl&gt;
      &lt;/urn2:pageControl&gt;
  &lt;/urn2:BusinessControlData&gt;
&lt;/ns0:bcd&gt;
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
&lt;ns0:invokeResponse&gt;
  {getElementsForDicts($parm)}
  {getElementsForBcd($parm)}
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>