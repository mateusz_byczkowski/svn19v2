<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soapenv:Body) external;

&lt;soapenv:Body&gt;
  &lt;urn:invokeResponse xmlns:urn="urn:be.services.dcl"/&gt;
&lt;/soapenv:Body&gt;</con:xquery>
</con:xqueryEntry>