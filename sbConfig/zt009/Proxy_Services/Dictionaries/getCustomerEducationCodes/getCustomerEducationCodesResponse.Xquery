<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
&lt;ns0:dicts&gt;
  {
    let $opis := $parm/CI_OPIS_WARTOSCI_SLOWNIKA
    for $wartosc at $occ in $parm/CI_WARTOSC_SLOWNIKA
    return
    &lt;ns3:CustomerEducationCode&gt;
      &lt;ns3:description?&gt;{data($opis[$occ])}&lt;/ns3:description&gt;
      {if (fn:string-length($wartosc)&gt;0)
        then &lt;ns3:customerEducationCode&gt;{data($wartosc)}&lt;/ns3:customerEducationCode&gt;
        else &lt;ns3:customerEducationCode&gt;{" "}&lt;/ns3:customerEducationCode&gt;
      }
    &lt;/ns3:CustomerEducationCode&gt;
  }
&lt;/ns0:dicts&gt;
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
&lt;ns0:invokeResponse&gt;
  {getElementsForDicts($parm)}
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>