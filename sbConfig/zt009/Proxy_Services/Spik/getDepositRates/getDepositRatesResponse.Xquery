<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getDepositRatesResponse/";
declare namespace dcl  = "urn:be.services.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "urn:filterandmessages.dictionaries.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";

declare function xf:getDepositRatesResponse($fml as element())
    as element(dcl:invokeResponse) {
        &lt;dcl:invokeResponse&gt;

            &lt;dcl:tableDate&gt;
            {
                if (data($fml/SP_DATE[1]))
                    then
                        &lt;ns3:DepositRate&gt;
                              &lt;ns3:date&gt; { fn:concat(fn:translate(data($fml/SP_DATE[1]),' ',''), ':00') } &lt;/ns3:date&gt;
                        &lt;/ns3:DepositRate&gt;
                    else ()
            }
            &lt;/dcl:tableDate&gt;

            &lt;dcl:bcd&gt;
              &lt;ns4:BusinessControlData&gt;
                &lt;ns4:pageControl&gt;
                  &lt;ns5:PageControl&gt;
                    &lt;ns5:hasNext&gt;false&lt;/ns5:hasNext&gt;
                    &lt;ns5:navigationKeyDefinition/&gt;
                    &lt;ns5:navigationKeyValue/&gt;
                  &lt;/ns5:PageControl&gt;
                &lt;/ns4:pageControl&gt;
              &lt;/ns4:BusinessControlData&gt;
            &lt;/dcl:bcd&gt;

            &lt;dcl:depositRatesList&gt;
            {
                for $i in 1 to count($fml/SP_WALUTA)
                return
                   &lt;ns3:DepositRate&gt;
                       {
                           if (data($fml/SP_MIN_PROG[$i]))
                               then
                                   &lt;ns3:thresholdMin&gt; { fn:translate(data($fml/SP_MIN_PROG[$i]),'.','') } &lt;/ns3:thresholdMin&gt;
                               else ()
                       }
                       {
                           if (data($fml/SP_MAX_PROG[$i]))
                               then
                                   &lt;ns3:thresholdMax&gt; { fn:translate(data($fml/SP_MAX_PROG[$i]),'.','') } &lt;/ns3:thresholdMax&gt;
                               else ()
                       }
                       {
                           if (data($fml/SP_KURS[$i]))
                               then
                                   &lt;ns3:rate&gt; { xs:double(data($fml/SP_KURS[$i])) } &lt;/ns3:rate&gt;
                               else ()
                       }
                       &lt;ns3:accountTerm&gt;
                       {
                           fn:concat(data($fml/SP_MIN_CZEST[$i]),
                                     data($fml/SP_MIN_OKRES[$i]),
                                     '-',
                                     data($fml/SP_MAX_CZEST[$i]),
                                     data($fml/SP_MAX_OKRES[$i]))
                       }
                       &lt;/ns3:accountTerm&gt;
                       {
                           if (data($fml/SP_MIN_CZEST[$i]))
                               then
                                   &lt;ns3:frequencyMin&gt; { data($fml/SP_MIN_CZEST[$i]) } &lt;/ns3:frequencyMin&gt;
                               else ()
                       }
                       {
                           if (data($fml/SP_MAX_CZEST[$i]))
                               then
                                   &lt;ns3:frequencyMax&gt; { data($fml/SP_MAX_CZEST[$i]) } &lt;/ns3:frequencyMax&gt;
                               else ()
                       }

                       &lt;ns3:currencyCode&gt;
                           &lt;ns1:CurrencyCode&gt;
                               &lt;ns1:currencyCode&gt; { data($fml/SP_WALUTA[$i]) } &lt;/ns1:currencyCode&gt;
                           &lt;/ns1:CurrencyCode&gt;
                       &lt;/ns3:currencyCode&gt;
                       {
                           if (data($fml/SP_MIN_OKRES[$i]))
                               then
                                   &lt;ns3:periodMin&gt;
                                       &lt;ns1:Period&gt;
                                           &lt;ns1:period&gt; { data($fml/SP_MIN_OKRES[$i]) } &lt;/ns1:period&gt;
                                       &lt;/ns1:Period&gt;
                                   &lt;/ns3:periodMin&gt;
                               else ()
                       }
                       {
                           if (data($fml/SP_MAX_OKRES[$i]))
                               then
                                   &lt;ns3:periodMax&gt;
                                       &lt;ns1:Period&gt;
                                           &lt;ns1:period&gt; { data($fml/SP_MAX_OKRES[$i]) } &lt;/ns1:period&gt;
                                       &lt;/ns1:Period&gt;
                                   &lt;/ns3:periodMax&gt;
                               else ()
                       }
                       {
                           if (data($fml/SP_ID_TABELI[$i]))
                               then
                                   &lt;ns3:idDepositRateTable&gt;
                                       &lt;ns2:DepositRateTables&gt;
                                           &lt;ns2:depositRateTables&gt; { data($fml/SP_ID_TABELI[$i]) } &lt;/ns2:depositRateTables&gt;
                                       &lt;/ns2:DepositRateTables&gt;
                                   &lt;/ns3:idDepositRateTable&gt;
                               else ()
                       }
                   &lt;/ns3:DepositRate&gt;
            }
            &lt;/dcl:depositRatesList&gt;
        &lt;/dcl:invokeResponse&gt;
};

declare variable $fml as element() external;

xf:getDepositRatesResponse($fml)</con:xquery>
</con:xqueryEntry>