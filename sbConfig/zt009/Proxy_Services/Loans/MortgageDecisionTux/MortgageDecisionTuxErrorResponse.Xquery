<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace dec = "http://bzwbk.com/dc/services/mortgage/decision";
declare namespace ctx = "http://www.bea.com/wli/sb/context";
declare namespace fml = "";

declare variable $body external;
declare variable $fault external;

declare function local:mapMortgageDecisionException($req as element(dec:MortgageDecisionException))
	as element(fml:FML32) {
	&lt;fml:FML32&gt;
		&lt;fml:B_STATUS&gt;ERROR&lt;/fml:B_STATUS&gt;
		&lt;fml:B_OPIS_BLEDU&gt;{ data($req/description) }&lt;/fml:B_OPIS_BLEDU&gt;
		&lt;fml:B_URCODE&gt;{ data($req/urCode) }&lt;/fml:B_URCODE&gt;
	&lt;/fml:FML32&gt;
};

declare function local:mapOtherFault($req as element(ctx:fault))
	as element(fml:FML32) {
	&lt;fml:FML32&gt;
		&lt;fml:B_STATUS&gt;ERROR&lt;/fml:B_STATUS&gt;
		&lt;fml:B_OPIS_BLEDU&gt;{ data($req/ctx:reason) }&lt;/fml:B_OPIS_BLEDU&gt;
		&lt;fml:B_URCODE&gt;0&lt;/fml:B_URCODE&gt;
	&lt;/fml:FML32&gt;
};

&lt;soap-env:Body&gt;
{ 
	if($body/soap-env:Fault)
	then
		local:mapMortgageDecisionException($body/soap-env:Fault/detail/dec:MortgageDecisionException) 
	else 
		local:mapOtherFault($fault)
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>