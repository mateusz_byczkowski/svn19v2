<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCustomerForAdvisoryRequest($req as element(m:GetCustomerForAdvisoryRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if ($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
				else ()
			}
			{
				if ($req/m:IdSpolki)
					then if (data($req/m:IdSpolki))
						then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
						else &lt;fml:CI_ID_SPOLKI&gt;{0}&lt;/fml:CI_ID_SPOLKI&gt;
				else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body&gt;
{ xf:mapGetCustomerForAdvisoryRequest($body/m:GetCustomerForAdvisoryRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>