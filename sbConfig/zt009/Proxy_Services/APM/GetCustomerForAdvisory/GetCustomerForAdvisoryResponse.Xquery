<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCustomerForAdvisoryResponse($fml as element(fml:FML32))
	as element(m:GetCustomerForAdvisoryResponse) {
		&lt;m:GetCustomerForAdvisoryResponse&gt;
			{
				let $CI_STATUS_AKT_DORADZTWA := $fml/fml:CI_STATUS_AKT_DORADZTWA return
					if($CI_STATUS_AKT_DORADZTWA)
						then &lt;m:StatusAktywnosciDoradztwa&gt;{ data($CI_STATUS_AKT_DORADZTWA) }&lt;/m:StatusAktywnosciDoradztwa&gt;
						else ()
			}
			{
				let $CI_PROFIL_INWESTYCYJNY := $fml/fml:CI_PROFIL_INWESTYCYJNY return
					if ($CI_PROFIL_INWESTYCYJNY)
						then &lt;m:ProfilInwestycyjny&gt;{ data($CI_PROFIL_INWESTYCYJNY) }&lt;/m:ProfilInwestycyjny&gt;
						else ()
			}
			{
				let $CI_DATA_AKT_DORADZTWA := $fml/fml:CI_DATA_AKT_DORADZTWA return
					if ($CI_DATA_AKT_DORADZTWA)
						then &lt;m:DataAktywacjiUslugi&gt;{ data($CI_DATA_AKT_DORADZTWA) }&lt;/m:DataAktywacjiUslugi&gt;
						else ()
			}
			{
				let $CI_DATA_ZAK_DORADZTWA := $fml/fml:CI_DATA_ZAK_DORADZTWA return
					if ($CI_DATA_ZAK_DORADZTWA)
						then &lt;m:DataOstatniejUslugi&gt;{ data($CI_DATA_ZAK_DORADZTWA) }&lt;/m:DataOstatniejUslugi&gt;
						else ()
			}
			{
				let $CI_ID_DORADZTWA := $fml/fml:CI_ID_DORADZTWA return
					if ($CI_ID_DORADZTWA)
						then &lt;m:IdentyfikatorDoradztwa&gt;{ data($CI_ID_DORADZTWA) }&lt;/m:IdentyfikatorDoradztwa&gt;
						else ()
			}
			{
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA return
					if ($CI_UDOSTEP_GRUPA)
						then &lt;m:UdostepGrupa&gt;{ data($CI_UDOSTEP_GRUPA) }&lt;/m:UdostepGrupa&gt;
						else ()
			}
			{
				let $DC_IMIE := $fml/fml:DC_IMIE return
					if ($DC_IMIE)
						then &lt;m:Imie&gt;{ data($DC_IMIE) }&lt;/m:Imie&gt;
						else ()
			}
			{
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO return
					if ($DC_NAZWISKO)
						then &lt;m:Nazwisko&gt;{ data($DC_NAZWISKO) }&lt;/m:Nazwisko&gt;
						else ()
			}
			{
				let $DC_DRUGIE_IMIE := $fml/fml:DC_DRUGIE_IMIE return
					if ($DC_DRUGIE_IMIE)
						then &lt;m:DrugieImie&gt;{ data($DC_DRUGIE_IMIE) }&lt;/m:DrugieImie&gt;
						else ()
			}
			{
				let $DC_TYP_ADRESU := $fml/fml:DC_TYP_ADRESU return
					if ($DC_TYP_ADRESU)
						then &lt;m:TypAdresu&gt;{ data($DC_TYP_ADRESU) }&lt;/m:TypAdresu&gt;
						else ()
			}
			{
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST return
					if ($DC_ULICA_DANE_PODST)
						then &lt;m:UlicaDanePodst&gt;{ data($DC_ULICA_DANE_PODST) }&lt;/m:UlicaDanePodst&gt;
						else ()
			}
			{
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST return
					if ($DC_NR_POSES_LOKALU_DANE_PODST)
						then &lt;m:NrPosesLokaluDanePodst&gt;{ data($DC_NR_POSES_LOKALU_DANE_PODST) }&lt;/m:NrPosesLokaluDanePodst&gt;
						else ()
			}
			{
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST return
					if ($DC_MIASTO_DANE_PODST)
						then &lt;m:MiastoDanePodst&gt;{ data($DC_MIASTO_DANE_PODST) }&lt;/m:MiastoDanePodst&gt;
						else ()
			}
			{
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST return
					if ($DC_KOD_POCZTOWY_DANE_PODST)
						then &lt;m:KodPocztowyDanePodst&gt;{ data($DC_KOD_POCZTOWY_DANE_PODST) }&lt;/m:KodPocztowyDanePodst&gt;
						else ()
			}
			{
				let $DC_WOJ_KRAJ_DANE_PODST := $fml/fml:DC_WOJ_KRAJ_DANE_PODST return
					if ($DC_WOJ_KRAJ_DANE_PODST)
						then &lt;m:Wojewodztwo&gt;{ data($DC_WOJ_KRAJ_DANE_PODST) }&lt;/m:Wojewodztwo&gt;
						else ()
			}
			{
				let $CI_KOD_KRAJU := $fml/fml:CI_KOD_KRAJU return
					if ($CI_KOD_KRAJU)
						then &lt;m:KodKraju&gt;{ (data($CI_KOD_KRAJU) cast as xs:decimal) cast as xs:short }&lt;/m:KodKraju&gt;
						else ()
			}
			{
				let $CI_KOD_KRAJU_KORESP := $fml/fml:CI_KOD_KRAJU_KORESP
				let $CI_DATA_OD := $fml/fml:CI_DATA_OD
				let $CI_DATA_DO := $fml/fml:CI_DATA_DO
				let $DC_IMIE_I_NAZWISKO_ALT := $fml/fml:DC_IMIE_I_NAZWISKO_ALT
				let $DC_ULICA_ADRES_ALT := $fml/fml:DC_ULICA_ADRES_ALT
				let $DC_NR_POSES_LOKALU_ADRES_ALT := $fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT
				let $DC_MIASTO_ADRES_ALT := $fml/fml:DC_MIASTO_ADRES_ALT
				let $DC_KOD_POCZTOWY_ADRES_ALT := $fml/fml:DC_KOD_POCZTOWY_ADRES_ALT
				let $DC_WOJEWODZTWO_KRAJ_ADRES_ALT := $fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT
				let $CI_TYP_ADRESU := $fml/fml:CI_TYP_ADRESU

				for $it at $p in $fml/fml:DC_IMIE_I_NAZWISKO_ALT
				return
					&lt;m:APMGetCustomerAdresAlt&gt;
						{
							if ($CI_KOD_KRAJU_KORESP[$p])
								then &lt;m:KodKrajuKoresp&gt;{ data($CI_KOD_KRAJU_KORESP[$p]) }&lt;/m:KodKrajuKoresp&gt;
							else ()
						}
						{
							if ($CI_DATA_OD[$p])
								then &lt;m:DataOd&gt;{ data($CI_DATA_OD[$p]) }&lt;/m:DataOd&gt;
							else ()
						}
						{
							if ($CI_DATA_DO[$p])
								then &lt;m:DataDo&gt;{ data($CI_DATA_DO[$p]) }&lt;/m:DataDo&gt;
							else ()
						}
						{
							if ($DC_IMIE_I_NAZWISKO_ALT[$p])
								then &lt;m:ImieINazwiskoAlt&gt;{ data($DC_IMIE_I_NAZWISKO_ALT[$p]) }&lt;/m:ImieINazwiskoAlt&gt;
							else ()
						}
						{
							if ($DC_ULICA_ADRES_ALT[$p])
								then &lt;m:UlicaAdresAlt&gt;{ data($DC_ULICA_ADRES_ALT[$p]) }&lt;/m:UlicaAdresAlt&gt;
							else ()
						}
						{
							if ($DC_NR_POSES_LOKALU_ADRES_ALT[$p])
								then &lt;m:NrPosesLokaluAdresAlt&gt;{ data($DC_NR_POSES_LOKALU_ADRES_ALT[$p]) }&lt;/m:NrPosesLokaluAdresAlt&gt;
							else ()
						}
						{
							if ($DC_MIASTO_ADRES_ALT[$p])
								then &lt;m:MiastoAdresAlt&gt;{ data($DC_MIASTO_ADRES_ALT[$p]) }&lt;/m:MiastoAdresAlt&gt;
							else ()
						}
						{
							if ($DC_KOD_POCZTOWY_ADRES_ALT[$p])
								then &lt;m:KodPocztowyAdresAlt&gt;{ data($DC_KOD_POCZTOWY_ADRES_ALT[$p]) }&lt;/m:KodPocztowyAdresAlt&gt;
							else ()
						}
						{
							if ($DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$p])
								then &lt;m:WojewodztwoKrajAdresAlt&gt;{ data($DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$p]) }&lt;/m:WojewodztwoKrajAdresAlt&gt;
							else ()
						}
						{
							if ($CI_TYP_ADRESU[$p])
								then &lt;m:TypAdresuKoresp&gt;{ data($CI_TYP_ADRESU[$p]) }&lt;/m:TypAdresuKoresp&gt;
							else ()
						}
					&lt;/m:APMGetCustomerAdresAlt&gt;
			}
			{
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL return
					if ($DC_NR_PESEL)
						then &lt;m:NrPesel&gt;{ data($DC_NR_PESEL) }&lt;/m:NrPesel&gt;
						else ()
			}
			{
				let $DC_REZ_NIEREZ := $fml/fml:DC_REZ_NIEREZ return
					if ($DC_REZ_NIEREZ)
						then &lt;m:RezNierez&gt;{ data($DC_REZ_NIEREZ) }&lt;/m:RezNierez&gt;
						else ()
			}
			{
				for $v in $fml/fml:CI_DOK_TOZSAMOSCI
				return
					&lt;m:DokTozsamosci&gt;{ data($v) }&lt;/m:DokTozsamosci&gt;
			}
			{
				for $v in $fml/fml:CI_SERIA_NR_DOK
				return
					&lt;m:SeriaNrDok&gt;{ data($v) }&lt;/m:SeriaNrDok&gt;
			}
			{
				let $DC_NR_DOWODU_REGON := $fml/fml:DC_NR_DOWODU_REGON return	
					if ($DC_NR_DOWODU_REGON)
						then &lt;m:NrDowoduRegon&gt;{ data($DC_NR_DOWODU_REGON) }&lt;/m:NrDowoduRegon&gt;
						else ()
			}
			{
				let $DC_NUMER_PASZPORTU := $fml/fml:DC_NUMER_PASZPORTU return
					if ($DC_NUMER_PASZPORTU)
						then &lt;m:NumerPaszportu&gt;{ data($DC_NUMER_PASZPORTU) }&lt;/m:NumerPaszportu&gt;
						else ()
			}
			{
				let $DC_NUMER_KLIENTA_REL := $fml/fml:DC_NUMER_KLIENTA_REL
				let $DC_TYP_RELACJI := $fml/fml:DC_TYP_RELACJI

				for $it at $p in $fml/fml:DC_NUMER_KLIENTA_REL
				return
					&lt;m:Relacje&gt;
						{
							if ($DC_NUMER_KLIENTA_REL[$p])
								then &lt;m:customerID&gt;{ data($DC_NUMER_KLIENTA_REL[$p]) }&lt;/m:customerID&gt;
								else ()
						}
						{
							if ($DC_TYP_RELACJI[$p])
								then &lt;m:relationType&gt;{ data($DC_TYP_RELACJI[$p]) }&lt;/m:relationType&gt;
								else ()
						}
					&lt;/m:Relacje&gt;
			}
			{
				let $DC_PLEC := $fml/fml:DC_PLEC return
					if ($DC_PLEC)
						then &lt;m:Plec&gt;{ data($DC_PLEC) }&lt;/m:Plec&gt;
						else ()
			}
			{
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA return
					if ($DC_TYP_KLIENTA)
						then &lt;m:TypKlienta&gt;{ data($DC_TYP_KLIENTA) }&lt;/m:TypKlienta&gt;
						else ()
			}
		&lt;/m:GetCustomerForAdvisoryResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapGetCustomerForAdvisoryResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>