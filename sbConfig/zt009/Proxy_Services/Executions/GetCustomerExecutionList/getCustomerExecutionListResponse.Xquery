<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:executiondict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace ns6="urn:executions.entities.be.dcl";
declare namespace ns7="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function sourceValue2Boolean ($parm as xs:anyType,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)&gt;5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };
 
declare function chgDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
 
    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)
 
    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 
 

};

declare function getElementsForExecutionList($parm as element(fml:FML32)) as element()
{

&lt;ns5:executionList&gt;
  {
    for $x at $occ in $parm/NF_EXECUT_EXECUTIONNUMBERL
    return
    &lt;ns6:Execution&gt;
      &lt;ns6:executionNumberLPP?&gt;{data($parm/NF_EXECUT_EXECUTIONNUMBERL[$occ])}&lt;/ns6:executionNumberLPP&gt;
      &lt;ns6:signature?&gt;{data($parm/NF_EXECUT_SIGNATURE[$occ])}&lt;/ns6:signature&gt;
      &lt;ns6:nameOfExecutor?&gt;{data($parm/NF_EXECUT_NAMEOFEXECUTOR[$occ])}&lt;/ns6:nameOfExecutor&gt;
      { insertDate(data($parm/NF_EXECUT_INCOMINGDATE[$occ]),"yyyy-MM-dd","ns6:incomingDate")}
      {
         if (data($parm/NF_EXECUT_FREEAMOUNTAVAILA[$occ]) = "-1") then
           ()
         else
           &lt;ns6:freeAmountAvailable?&gt;{sourceValue2Boolean (data($parm/NF_EXECUT_FREEAMOUNTAVAILA[$occ]),"1")}&lt;/ns6:freeAmountAvailable&gt;
      }
      &lt;ns6:totalExecutionAmount?&gt;{data($parm/NF_EXECUT_TOTALEXECUTIONAM[$occ])}&lt;/ns6:totalExecutionAmount&gt;
      &lt;ns6:executionStatus&gt;
        &lt;ns0:ExecutionStatus&gt;
          &lt;ns0:executionStatus?&gt;{data($parm/NF_EXECUS_EXECUTIONSTATUS[$occ])}&lt;/ns0:executionStatus&gt;
        &lt;/ns0:ExecutionStatus&gt;
      &lt;/ns6:executionStatus&gt;
    &lt;/ns6:Execution&gt;
  }
&lt;/ns5:executionList&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse&gt;
  {getElementsForExecutionList($parm)}
  &lt;ns5:bcd&gt;
    &lt;ns3:BusinessControlData&gt;
      &lt;ns3:pageControl&gt;
        &lt;ns7:PageControl&gt;
          &lt;ns7:hasNext?&gt;{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns7:hasNext&gt;
          &lt;ns7:navigationKeyDefinition?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns7:navigationKeyDefinition&gt;
          &lt;ns7:navigationKeyValue?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns7:navigationKeyValue&gt;
        &lt;/ns7:PageControl&gt;
      &lt;/ns3:pageControl&gt;
    &lt;/ns3:BusinessControlData&gt;
  &lt;/ns5:bcd&gt;
&lt;/ns5:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>