<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:executions.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns5="urn:filtersandmessages.entities.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:anyType,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if($parm = "false")
       then $falseval
    else $parm
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
,
&lt;NF_MSHEAD_UNITID?&gt;{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID&gt;
,
&lt;NF_MSHEAD_USERID?&gt;{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID&gt;
,
&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
,
&lt;NF_TRHEAD_TRANSID?&gt;{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID&gt;
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:freeAmountRepayment)) &gt; 0)
then
   &lt;NF_DEBTRD_FREEAMOUNTREPAYM?&gt;{boolean2SourceValue(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:freeAmountRepayment),"1","0")}&lt;/NF_DEBTRD_FREEAMOUNTREPAYM&gt;
else()
,
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:acaRepayment)) &gt; 0)
then
&lt;NF_DEBTRD_ACAREPAYMENT?&gt;{boolean2SourceValue(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:acaRepayment),"1","0")}&lt;/NF_DEBTRD_ACAREPAYMENT&gt;
else()
,
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:entireRepayment)) &gt; 0)
then
&lt;NF_DEBTRD_ENTIREREPAYMENT?&gt;{boolean2SourceValue(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:entireRepayment),"1","0")}&lt;/NF_DEBTRD_ENTIREREPAYMENT&gt;
else()
,
&lt;NF_DEBTRD_REPAYMENTAMOUNT?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:repaymentAmount)}&lt;/NF_DEBTRD_REPAYMENTAMOUNT&gt;
,
&lt;NF_DEBTRD_REPAYMENTDESCRIP?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:repaymentDescription)}&lt;/NF_DEBTRD_REPAYMENTDESCRIP&gt;
,
&lt;NF_DEBTRD_ISSUEDATE?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueDate)}&lt;/NF_DEBTRD_ISSUEDATE&gt;
,
&lt;NF_BRANCC_BRANCHCODE?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueBranch/ns4:BranchCode/ns4:branchCode)}&lt;/NF_BRANCC_BRANCHCODE&gt;
,
&lt;NF_BRANCC_DESCRIPTION?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueBranch/ns4:BranchCode/ns4:description)}&lt;/NF_BRANCC_DESCRIPTION&gt;
,
&lt;NF_BRANCC_FULLADDRESS?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueBranch/ns4:BranchCode/ns4:fullAddress)}&lt;/NF_BRANCC_FULLADDRESS&gt;
,
&lt;NF_CUSTOM_COMPANYNAME?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:companyName)}&lt;/NF_CUSTOM_COMPANYNAME&gt;
,
&lt;NF_CUSTOM_CUSTOMERNUMBER?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerNumber)}&lt;/NF_CUSTOM_CUSTOMERNUMBER&gt;
,
&lt;NF_ADDRES_FULLADDRESS?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:addressList/ns2:Address/ns2:fullAddress)}&lt;/NF_ADDRES_FULLADDRESS&gt;
,
for $x in data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:executionList/ns3:Execution/ns3:executionNumberLPP)
   return 
     &lt;NF_EXECUT_EXECUTIONNUMBERL?&gt;{$x}&lt;/NF_EXECUT_EXECUTIONNUMBERL&gt;
,
&lt;NF_CUSTOP_LASTNAME?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerPersonal/ns2:CustomerPersonal/ns2:lastName)}&lt;/NF_CUSTOP_LASTNAME&gt;
,
&lt;NF_CUSTOP_FIRSTNAME?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerPersonal/ns2:CustomerPersonal/ns2:firstName)}&lt;/NF_CUSTOP_FIRSTNAME&gt;
,
&lt;NF_CUSTOT_CUSTOMERTYPE?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerType/ns4:CustomerType/ns4:customerType)}&lt;/NF_CUSTOT_CUSTOMERTYPE&gt;
,
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountNumber)) &gt; 0) 
then
 &lt;NF_ACCOUN_ACCOUNTNUMBER?&gt;{cutStr(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountNumber),12)}&lt;/NF_ACCOUN_ACCOUNTNUMBER&gt;
else()

,
&lt;NF_BRANCC_BRANCHCODE?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountBranchNumber/ns4:BranchCode/ns4:branchCode)}&lt;/NF_BRANCC_BRANCHCODE&gt;
,
&lt;NF_BRANCC_DESCRIPTION?&gt;{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountBranchNumber/ns4:BranchCode/ns4:description)}&lt;/NF_BRANCC_DESCRIPTION&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>