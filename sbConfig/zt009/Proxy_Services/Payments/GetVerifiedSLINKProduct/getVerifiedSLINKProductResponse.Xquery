<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:operations.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn4 = "urn:swift.operations.entities.be.dcl";
declare namespace urn6 = "urn:transactionswiftdict.operationsdictionary.dictionaries.be.dcl";

declare function xf:mapGetSLINKProdResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
	
&lt;urn:invokeResponse&gt;
&lt;urn:transactionSwift&gt;
	&lt;urn4:TransactionSwift&gt;
		&lt;urn4:slinkProductNumber&gt;
			{ data($fml/fml:S_PRODUCT_ID)}	
		&lt;/urn4:slinkProductNumber&gt;
		&lt;urn4:valueDate&gt;
				{ data($fml/fml:S_TRN_DATE)}	
		&lt;/urn4:valueDate&gt;
		&lt;urn4:swiftFee&gt;
			&lt;urn4:SwiftFee&gt;
				&lt;urn4:slinkFeeId&gt;
					{ data($fml/fml:S_FEE_ID)}
				&lt;/urn4:slinkFeeId&gt;
				&lt;urn4:feeTotalAmount&gt;
					{ data($fml/fml:S_FEE_TOTAL_AMOUNT)}
				&lt;/urn4:feeTotalAmount&gt;

				&lt;urn4:swiftFeeBusinessList&gt;
                               {
                                        for $bank in $fml/fml:S_FML32 return
                                        (
					&lt;urn4:SwiftFeeBusiness&gt;
						&lt;urn4:feeAmount&gt;
							{ data($bank/S_FEE_AMOUNT)}
						&lt;/urn4:feeAmount&gt;						
						&lt;urn4:originCurrFeeAmount&gt;
							{ data($bank/fml:S_FEE_ORIGIN_AMOUNT)}
						&lt;/urn4:originCurrFeeAmount&gt;
						&lt;urn4:discountAmount&gt;
							{ data($bank/fml:S_DISCOUNT_AMOUNT)}
						&lt;/urn4:discountAmount&gt;
						&lt;urn4:originCurrFeeExchRate&gt;
							{ data($bank/fml:S_FEE_ORIGIN_RATE)}
						&lt;/urn4:originCurrFeeExchRate&gt;
						&lt;urn4:originCurrFeeExchRateCurrs&gt;
							{ data($bank/fml:S_FEE_ORIGIN_RATE_CURR)}
						&lt;/urn4:originCurrFeeExchRateCurrs&gt;
						&lt;urn4:discountCurrExchRate&gt;
							{ data($bank/fml:S_DISCOUNT_RATE)}
						&lt;/urn4:discountCurrExchRate&gt;
						&lt;urn4:discountCurrExchRateCurrs&gt;
							{ data($bank/fml:S_DISCOUNT_RATE_CURR)}
						&lt;/urn4:discountCurrExchRateCurrs&gt;
						&lt;urn4:chargedAccountNumber&gt;
							{ data($bank/fml:S_FEE_ACCOUNT_NO)}
						&lt;/urn4:chargedAccountNumber&gt;
						&lt;urn4:swiftFeeBusinessTitle&gt;  
							&lt;urn6:SwiftFeeBusinessTitle&gt;
								&lt;urn6:swiftFeeBusinessTitle&gt;
									{ data($bank/fml:S_FEE_TITLE)}
								&lt;/urn6:swiftFeeBusinessTitle&gt;
							&lt;/urn6:SwiftFeeBusinessTitle&gt;
						&lt;/urn4:swiftFeeBusinessTitle&gt;  
						&lt;urn4:originCurrFeeCurrency&gt; 
   							&lt;urn2:CurrencyCode&gt;
								&lt;urn2:currencyCode&gt;
									{ data($bank/fml:S_FEE_CURRENCY)}
								&lt;/urn2:currencyCode&gt;
							&lt;/urn2:CurrencyCode&gt;
						&lt;/urn4:originCurrFeeCurrency&gt;
						&lt;urn4:discountCurrency&gt;								
							&lt;urn2:CurrencyCode&gt;
								&lt;urn2:currencyCode&gt;
									{ data($bank/fml:S_DISCOUNT_CURRENCY)}
								&lt;/urn2:currencyCode&gt;
							&lt;/urn2:CurrencyCode&gt;
						&lt;/urn4:discountCurrency&gt;

						&lt;urn4:feeCurrency&gt;	
							&lt;urn2:CurrencyCode&gt;
								&lt;urn2:currencyCode&gt;
									{ data($bank/fml:S_FEE_CURRENCY)}
								&lt;/urn2:currencyCode&gt;
							&lt;/urn2:CurrencyCode&gt;
						&lt;/urn4:feeCurrency&gt;	
					&lt;/urn4:SwiftFeeBusiness&gt;
                                 )
                                 }
				&lt;/urn4:swiftFeeBusinessList&gt;					

				&lt;urn4:feeTotalAmountCurrency&gt;
					&lt;urn2:CurrencyCode&gt;         
						&lt;urn2:currencyCode&gt;
							{ data($fml/fml:S_FEE_TOTAL_AMOUNT_CURR)}
						&lt;/urn2:currencyCode&gt;						
					&lt;/urn2:CurrencyCode&gt;
				&lt;/urn4:feeTotalAmountCurrency&gt;	                             


			&lt;/urn4:SwiftFee&gt;
		&lt;/urn4:swiftFee&gt;
	&lt;/urn4:TransactionSwift&gt;
&lt;/urn:transactionSwift&gt;
&lt;/urn:invokeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapGetSLINKProdResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>