<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/GetCMFinance/messages/";
declare namespace xf = "http://bzwbk.com/services/GetCMFinance/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCMFinanceResponse($fml as element(fml:FML32))
	as element(m:GetCMFinanceResponse) {
		&lt;m:GetCMFinanceResponse&gt;
			{

				let $S_LIMIT_ID := $fml/fml:S_LIMIT_ID
				let $S_LIMIT_LEVEL := $fml/fml:S_LIMIT_LEVEL
				let $S_REK_ID := $fml/fml:S_REK_ID
				let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
				let $S_AMOUNT := $fml/fml:S_AMOUNT
				for $it at $p in $fml/fml:S_LIMIT_ID
				return
					&lt;m:GetCMFinanceList&gt;
					{
						if($S_LIMIT_ID[$p])
							then &lt;m:LimitId&gt;{ data($S_LIMIT_ID[$p]) }&lt;/m:LimitId&gt;
						else ()
					}
					{
						if($S_LIMIT_LEVEL[$p])
							then &lt;m:LimitLevel&gt;{ data($S_LIMIT_LEVEL[$p]) }&lt;/m:LimitLevel&gt;
						else ()
					}
					{
						if($S_REK_ID[$p])
							then &lt;m:RekId&gt;{ data($S_REK_ID[$p]) }&lt;/m:RekId&gt;
						else ()
					}
					{
						if($B_KOD_WALUTY[$p])
							then &lt;m:KodWaluty&gt;{ data($B_KOD_WALUTY[$p]) }&lt;/m:KodWaluty&gt;
						else ()
					}
					{
						if($S_AMOUNT[$p])
							then &lt;m:Amount&gt;{ data($S_AMOUNT[$p]) }&lt;/m:Amount&gt;
						else ()
					}
					&lt;/m:GetCMFinanceList&gt;
			}

		&lt;/m:GetCMFinanceResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapGetCMFinanceResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>