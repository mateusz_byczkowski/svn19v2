<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-10</con:description>
  <con:xquery>declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:baseauxentities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:entities.be.dcl";

declare namespace urn="urn:be.services.dcl"; 
declare namespace urn1="urn:accounts.entities.be.dcl"; 
declare namespace urn2="urn:dictionaries.be.dcl";
declare namespace urn3="urn:entities.be.dcl"; 
declare namespace urn4="urn:baseauxentities.be.dcl";

declare variable $body as element(soap:Body) external;

declare function convertActionCodeValue ($parm as xs:string,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "Y")
       then $trueval
       else $falseval
};


declare function getFieldsFromInvoke($parm as element(ns4:invoke)) as element()*
{
 (:
 let $blokada:=data($parm/fml:B_BLOKADA)
 let $blokada_len:=string-length($blokada)
 let $blokada_zero:=concat(substring("00000", 1+ $blokada_len),$blokada)
 let $typBlokady:=data($parm/fml:DC_TYP_BLOKADY[2])
 let $typBlokady_len:=string-length($typBlokady)
 let $typBlokady_zero:=concat(substring("00", 1+ $typBlokady_len),$typBlokady)
 let $key:=concat($blokada_zero,$typBlokady)
 let $spacje:='                                                                                             '
 :)
 let $pageControl:=$parm/ns4:bcd/ns5:BusinessControlData/ns5:pageControl/ns2:PageControl
 return (
&lt;fml:NF_HOLD_HOLDSTAT&gt;A&lt;/fml:NF_HOLD_HOLDSTAT&gt;
,
&lt;fml:NF_ACCOUN_ACCOUNTNUMBER?&gt;{data($parm/ns4:hold/urn1:Hold/urn1:tranAccount/urn1:TranAccount/urn1:account/urn1:Account/urn1:accountNumber)}&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER&gt;
,
&lt;fml:NF_ACCOUN_ACCOUNTIBAN?&gt;{data($parm/ns4:hold/urn1:Hold/urn1:tranAccount/urn1:TranAccount/urn1:account/urn1:Account/urn1:accountNumber)}&lt;/fml:NF_ACCOUN_ACCOUNTIBAN&gt;
,
&lt;fml:NF_HOLDT_HOLDTYPE?&gt;{data($parm/ns4:hold/urn1:Hold/urn1:holdType/urn2:HoldType/urn2:holdType)}&lt;/fml:NF_HOLDT_HOLDTYPE&gt;
,
&lt;fml:NF_PAGEC_ACTIONCODE&gt;{data($pageControl/ns2:actionCode)}&lt;/fml:NF_PAGEC_ACTIONCODE&gt;
,
&lt;fml:NF_PAGEC_PAGESIZE?&gt;{data($pageControl/ns2:pageSize)}&lt;/fml:NF_PAGEC_PAGESIZE&gt;
,
&lt;fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;{data($pageControl/ns2:navigationKeyValue)}&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;
,
&lt;fml:NF_PAGEC_REVERSEORDER&gt;1&lt;/fml:NF_PAGEC_REVERSEORDER&gt;
)
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
   {getFieldsFromInvoke($body/ns4:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>