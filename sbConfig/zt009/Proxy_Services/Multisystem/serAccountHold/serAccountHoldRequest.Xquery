<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-10</con:description>
  <con:xquery>declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:operations.entities.be.dcl";
declare namespace urn2="urn:accounts.entities.be.dcl";
declare namespace urn3="urn:dictionaries.be.dcl";
declare namespace urn4="urn:accountdict.dictionaries.be.dcl";
declare namespace urn5="urn:entities.be.dcl";
declare namespace urn6="urn:basedictionaries.be.dcl";

declare variable $body as element(soap:Body) external;

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm  = "1") then $trueval
       else $falseval
};

declare function defaultIfEmpty($parm as xs:string?,$defaultValue as xs:string) as xs:string{
    if ($parm)
       then insertDate($parm)
       else $defaultValue
};

declare function defaultIfFlagZero($parm as xs:string?,$defaultValue as xs:string, $flag as xs:string) as xs:string{
    if ($parm)
       then 
          if ($flag ="0") 
              then $defaultValue
          else
            insertDate($parm)
       else $defaultValue
};

declare function checkBalanceFlag($amount as xs:string,$flag as xs:string) as xs:string{
    if ($amount)
       then 
          if ($flag= "Y")
              then "99999999999.99"
          else
            $amount
       else ()
};

declare function defaultIfEmpty2($parm as xs:string?,$defaultValue as xs:string) as xs:string{
    if ($parm)
       then $parm
       else $defaultValue
};

declare function insertDate($value as xs:string?) as xs:string?{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "0001-01-01") and not ($value = "01-01-0001"))
            then chgDate($value)
        else() 
      else()
      };

declare function chgDate($value as xs:string) as xs:string
{
    let $year := substring($value, 1, 4)
    let $month := substring($value, 6, 2)
    let $day := substring($value, 9, 2)
return
     concat($day,concat("-",concat($month, concat("-", $year))))
};

&lt;soap:Body&gt;
&lt;fml:FML32&gt;
   &lt;fml:DC_TRN_ID&gt;{data($body/urn:invoke/urn:transaction/urn1:Transaction/urn1:transactionID)}&lt;/fml:DC_TRN_ID&gt;
   &lt;fml:DC_UZYTKOWNIK&gt;{data($body/urn:invoke/urn:user/urn5:User/urn5:userID)}&lt;/fml:DC_UZYTKOWNIK&gt; 
   &lt;fml:DC_ODDZIAL&gt;{data($body/urn:invoke/urn:user/urn5:User/urn5:branchCode/urn6:BaseBranchCode/urn6:branchCode)}&lt;/fml:DC_ODDZIAL&gt;
   &lt;fml:DC_NR_RACHUNKU&gt;{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:tranAccount/urn2:TranAccount/urn2:account/urn2:Account/urn2:accountNumber)}&lt;/fml:DC_NR_RACHUNKU&gt;
   &lt;fml:DC_KWOTA&gt;{checkBalanceFlag(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdAmount),boolean2SourceValue(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdBalanceFlag),"Y","N"))}&lt;/fml:DC_KWOTA&gt;
   &lt;fml:DC_NUMER_BLOKADY&gt;{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdNumber)}&lt;/fml:DC_NUMER_BLOKADY&gt;
   &lt;fml:DC_TYP_BLOKADY&gt;{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdType/urn3:HoldType/urn3:holdType)}&lt;/fml:DC_TYP_BLOKADY&gt; 
   &lt;fml:DC_DATA_WAZNOSCI&gt;{defaultIfFlagZero(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdExpirationDate),"31-12-2049",boolean2SourceValue(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdExpirationDateFlag),"0","1"))}&lt;/fml:DC_DATA_WAZNOSCI&gt;
   &lt;fml:DC_FLAGA_BLOKOWANIA_SALDA&gt;{boolean2SourceValue(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdBalanceFlag),"Y","N")}&lt;/fml:DC_FLAGA_BLOKOWANIA_SALDA&gt;
   &lt;fml:DC_OPIS_1&gt;{defaultIfEmpty2(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdDescription),"")}&lt;/fml:DC_OPIS_1&gt;
   &lt;fml:DC_OPIS_2&gt;{defaultIfEmpty2(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdDescription2),"")}&lt;/fml:DC_OPIS_2&gt;	
   &lt;fml:DC_NUMER_SERYJNY_FAKTURY&gt;{defaultIfEmpty2(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:firstChecqueNumber),"")}&lt;/fml:DC_NUMER_SERYJNY_FAKTURY&gt;
   &lt;fml:DC_DATA_CZEKU_FAKTURY&gt;{defaultIfEmpty(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:checqueRestrictDate),"")}&lt;/fml:DC_DATA_CZEKU_FAKTURY&gt; 
   &lt;fml:DC_NUMER_CZEKU&gt;{defaultIfEmpty2(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:lastChecqueNumber),"")}&lt;/fml:DC_NUMER_CZEKU&gt;
   &lt;fml:DC_WALUTA&gt;{defaultIfEmpty2(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdCurrencyCode/urn3:CurrencyCode/urn3:currencyCode),"PLN")}&lt;/fml:DC_WALUTA&gt; 
   &lt;fml:DC_UZYTKOWNIK_MODYFIKUJACY&gt;{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdCreateUserId)}&lt;/fml:DC_UZYTKOWNIK_MODYFIKUJACY&gt;
   &lt;fml:DC_UZYTKOWNIK_AUTORYZUJACY&gt;{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdAcceptUserId)}&lt;/fml:DC_UZYTKOWNIK_AUTORYZUJACY&gt; 
   &lt;fml:DC_FLAGA_DATY_WYGASNIECIA&gt;{boolean2SourceValue(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdExpirationDateFlag),"0","1")}&lt;/fml:DC_FLAGA_DATY_WYGASNIECIA&gt;
   &lt;fml:DC_TYP_ZMIANY&gt;{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdActionCode/urn4:HoldActionCode/urn4:holdActionCode)}&lt;/fml:DC_TYP_ZMIANY&gt;
   &lt;fml:DC_FLAGA_AUTORYZACJI&gt;O&lt;/fml:DC_FLAGA_AUTORYZACJI&gt;
   &lt;fml:DC_NUMER_AKCJI_NA_BLOKADZIE&gt;{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:actionNumberOnHold)}&lt;/fml:DC_NUMER_AKCJI_NA_BLOKADZIE&gt; 
&lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>