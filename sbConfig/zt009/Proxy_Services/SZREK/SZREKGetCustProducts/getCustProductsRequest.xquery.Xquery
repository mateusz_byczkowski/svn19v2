<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/esbmessages/";
declare namespace xf = "http://bzwbk.com/services/esbmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetCustProductsRequest($req as element(m:getCustProductsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			(:{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}:)
			{
				if($req/m:IdSpolki)
					then &lt;fml:NF_MSHEAD_COMPANYID&gt;{ data($req/m:IdSpolki) }&lt;/fml:NF_MSHEAD_COMPANYID&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER&gt;{ data($req/m:NumerKlienta) }&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER&gt;
					else ()
			}
			{
				if($req/m:ProductAreaId)
					then &lt;fml:NF_PRODUA_CODEPRODUCTAREA&gt;{ data($req/m:ProductAreaId) }&lt;/fml:NF_PRODUA_CODEPRODUCTAREA&gt;
					else ()
			}
			{
				if(string-length(data($req/m:ProductCategoryId))&gt;0 )
					then &lt;fml:NF_PRODUC_IDPRODUCTCATEGOR&gt;{ data($req/m:ProductCategoryId) }&lt;/fml:NF_PRODUC_IDPRODUCTCATEGOR&gt;
					else ()
			}
			(:{
				if($req/m:ProductAplication)
					then &lt;fml:CI_PRODUCT_APLICATION&gt;{ data($req/m:ProductAplication) }&lt;/fml:CI_PRODUCT_APLICATION&gt;
					else ()
			}:)
			(:{
				if($req/m:CiOpcja)
					then &lt;fml:NF_CTRL_OPTION&gt;{ data($req/m:CiOpcja) }&lt;/fml:NF_CTRL_OPTION&gt;
					else &lt;fml:NF_CTRL_OPTION&gt;0&lt;/fml:NF_CTRL_OPTION&gt;
			}:)
                        &lt;fml:NF_CTRL_OPTION&gt;2&lt;/fml:NF_CTRL_OPTION&gt;
                        &lt;fml:NF_PAGEC_PAGESIZE&gt;200&lt;/fml:NF_PAGEC_PAGESIZE&gt;
			&lt;fml:NF_PAGEC_ACTIONCODE&gt;F&lt;/fml:NF_PAGEC_ACTIONCODE&gt;
			&lt;fml:NF_PAGEC_REVERSEORDER&gt;0&lt;/fml:NF_PAGEC_REVERSEORDER&gt;
			&lt;fml:NF_PAGEC_NAVIGATIONKEYVALU/&gt;
			&lt;fml:NF_CTRL_ACTIVENONACTIVE&gt;0&lt;/fml:NF_CTRL_ACTIVENONACTIVE&gt;
			&lt;fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;0&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;
                        &lt;fml:NF_CTRL_SYSTEMS&gt;001002005&lt;/fml:NF_CTRL_SYSTEMS&gt;
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetCustProductsRequest($body/m:getCustProductsRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>