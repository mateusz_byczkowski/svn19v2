<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:be.services.dcl";
declare namespace ns8="urn:card.entities.be.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function chkUnitId($unitId as xs:anyType) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function chkField($value as xs:anyType,$length as xs:anyType,$fieldName as xs:string) as xs:anyType
{
   if($value) then
      if(string-length($value)&gt;$length) then
       element  {$fieldName}  {$value}   
      else()
   else ()
};


declare function boolean2SourceValue ($parm as xs:anyType,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm = "1") then $trueval
       else $falseval
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};


declare function xf:getFields($parm as element(ns7:invoke), $msghead as element(ns7:msgHeader), $tranhead as element(ns7:transHeader))
	as element(fml:FML32) {

let $msgId:= $msghead/ns7:msgId
let $companyId:= $msghead/ns7:companyId
let $userId := $msghead/ns7:userId
let $appId:= $msghead/ns7:appId
let $unitId := $msghead/ns7:unitId
let $timestamp:= $msghead/ns7:timestamp

let $transId:=$tranhead/ns7:transId

return
  &lt;fml:FML32&gt;
     &lt;DC_TRN_ID?&gt;{data($transId)}&lt;/DC_TRN_ID&gt;
     &lt;DC_UZYTKOWNIK?&gt;{concat("SKP:",data($userId))}&lt;/DC_UZYTKOWNIK&gt;
     &lt;DC_ODDZIAL?&gt;{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL&gt;
	 &lt;DC_TERMINAL_ID?&gt;ALSB&lt;/DC_TERMINAL_ID&gt;
	  {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:card/ns8:Card/ns8:bin),0,"DC_NR_KARTY_BIN")}
           {
	 if (string-length(data($parm/ns7:debitCard/ns8:DebitCard/ns8:tranAccount/ns2:TranAccount/ns2:account/ns2:Account/ns2:accountNumber)) &gt; 12) then
	 &lt;DC_RACHUNEK?&gt;{cutStr(data($parm/ns7:debitCard/ns8:DebitCard/ns8:tranAccount/ns2:TranAccount/ns2:account/ns2:Account/ns2:accountNumber),12)}&lt;/DC_RACHUNEK&gt;
	 else
	  chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:tranAccount/ns2:TranAccount/ns2:account/ns2:Account/ns2:accountNumber),0,"DC_RACHUNEK")
	 } 
         {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:card/ns8:Card/ns8:customer/ns3:Customer/ns3:customerNumber),0,"DC_NUMER_KLIENTA")}
         {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:card/ns8:Card/ns8:cardType),0,"DC_TYP_KARTY")}
	 {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:card/ns8:Card/ns8:embossName1),0,"DC_WYTLOCZONE_NA_KARCIE_I")}
         {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:card/ns8:Card/ns8:embossName2),0,"DC_WYTLOCZONE_NA_KARCIE_II")}
	 {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:pinMailer),0,"DC_IDENTYFIKATOR_PIN")}
	 {chkField(boolean2SourceValue(data($parm/ns7:debitCard/ns8:DebitCard/ns8:processingOptionFlag3),"1","0"),0,"DC_UMORZYC_OPLATE")}
	 {chkField(boolean2SourceValue(data($parm/ns7:debitCard/ns8:DebitCard/ns8:contract),"1","0"),0,"DC_STATUS_UMOWY")}
         {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:cashLimit),0,"DC_LIMIT_KARTY")}
	 {chkField(boolean2SourceValue(data($parm/ns7:debitCard/ns8:DebitCard/ns8:express),"1","0"),0,"DC_EKSPRES")}
	 {chkField(boolean2SourceValue(data($parm/ns7:debitCard/ns8:DebitCard/ns8:policy),"1","0"),0,"DC_ZALOZYC_POLISE")}
	 {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:addressFlag/ns1:CrdAddressFlag/ns1:crdAddressFlag),0,"DC_TYP_ADRESU")}
	 {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:cardAddressBranchNumber/ns4:BranchCode/ns4:branchCode),0,"DC_NUMER_ODDZIALU")}
	 {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:cycleLimit/ns1:CrdCycleLimit/ns1:crdCycleLimit),0,"DC_CYKL_LIMITU")}
	 &lt;DC_PROMOCJA&gt;0&lt;/DC_PROMOCJA&gt;
         {chkField(boolean2SourceValue(data($parm/ns7:debitCard/ns8:DebitCard/ns8:individualTranList),"30","0"),0,"DC_CYKL_ZESTAWIEN")}
         {chkField(boolean2SourceValue(data($parm/ns7:debitCard/ns8:DebitCard/ns8:collectiveTranList),"1","0"),0,"DC_ZEST_ZBIORCZE")}
         {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:expirationDate),0,"DC_DATA_WAZNOSCI")}
         {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:nextReissueDate),0,"DC_DATA_WZNOWIENIA")}
         {chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:nextCardFeeDate),0,"DC_DATA_NALICZENIA")}
{chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:debitCardLpInfo/ns8:DebitCardLpInfo/ns8:customerLpNumber),0,"DC_NR_PB2")}
{chkField(data($parm/ns7:debitCard/ns8:DebitCard/ns8:debitCardLpInfo/ns8:DebitCardLpInfo/ns8:otherCustomerLpNumber),0,"DC_NR_PB3")}   
{chkField(boolean2SourceValue(data($parm/ns7:debitCard/ns8:DebitCard/ns8:debitCardLpInfo/ns8:DebitCardLpInfo/ns8:processingApproval),"1","0"),0,"DC_FLAGA_MARKETINGU")}
{chkField(boolean2SourceValue(data($parm/ns7:debitCard/ns8:DebitCard/ns8:debitCardLpInfo/ns8:DebitCardLpInfo/ns8:forwardingDataApproval),"1","0"),0,"DC_FLAGA_ZGODY")}

  &lt;/fml:FML32&gt;
};

&lt;soap:Body&gt;
     { xf:getFields($body/ns7:invoke, $header/ns7:header/ns7:msgHeader, $header/ns7:header/ns7:transHeader) }
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>