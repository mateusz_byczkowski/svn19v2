<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:card.entities.be.dcl";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns2="urn:be.services.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:dictionaries.be.dcl";
declare namespace ns7="urn:accounts.entities.be.dcl";
declare namespace ns6="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns2:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns2:msgHeader/ns2:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns2:msgHeader/ns2:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
,
&lt;NF_MSHEAD_UNITID?&gt;{data($parm/ns2:msgHeader/ns2:unitId)}&lt;/NF_MSHEAD_UNITID&gt;
,
&lt;NF_MSHEAD_USERID?&gt;{data($parm/ns2:msgHeader/ns2:userId)}&lt;/NF_MSHEAD_USERID&gt;
,
&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns2:msgHeader/ns2:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns2:msgHeader/ns2:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
,
&lt;NF_TRHEAD_TRANSID?&gt;{data($parm/ns2:transHeader/ns2:transId)}&lt;/NF_TRHEAD_TRANSID&gt;
};
declare function getFieldsFromInvoke($parm as element(ns2:invoke)) as element()*
{

&lt;NF_DEBITC_VIRTUALCARDNBR?&gt;{data($parm/ns2:debitCard/ns0:DebitCard/ns0:virtualCardNbr)}&lt;/NF_DEBITC_VIRTUALCARDNBR&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns2:header)}
    {getFieldsFromInvoke($body/ns2:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>