<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns3="urn:accountdict.dictionaries.be.dcl";

declare function chkUnitId($unitId as xs:anyType) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};

declare function xf:prepareShortAccountNumber($accountNumber as xs:anyType) as xs:string{
   let $accountNumberLength:= string-length($accountNumber)
   let $shortAccountNumber := substring($accountNumber,$accountNumberLength - 11)
   let $shortAccountNumberWithZeros := concat(substring("000000000000", string-length($shortAccountNumber) + 1) , $shortAccountNumber)
   return
       $shortAccountNumber
};

declare function xf:mapChgTranAccountFeeRequest($req as element(ns1:Account), $reqF as element(ns1:Fee), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

let $msgId:= $msghead/dcl:transId
let $companyId:= $msghead/dcl:companyId
let $userId := $msghead/dcl:userId
let $appId:= $msghead/dcl:appId
let $unitId := $msghead/dcl:unitId
let $timestamp:= $msghead/dcl:timestamp

let $transId:=$tranhead/dcl:transId
let $skpOpener:=$req/ns1:skpOpener

let $accountNumber:= $req/ns1:accountNumber
let $prCode:= $req/ns1:tranAccount/ns1:TranAccount/ns1:promotionCode/ns2:PromotionCode/ns2:promotionCode
let $servChargeCode:= $req/ns1:tranAccount/ns1:TranAccount/ns1:serviceChargeCode              
let $feeFrequency:= $reqF/ns1:feeFrequency
let $nextFeeDate:= $reqF/ns1:nextFeeDate 
let $period:=$reqF/ns1:feePeriod/ns2:Period/ns2:period
let $feeType:=$reqF/ns1:feeType/ns3:FeeType/ns3:feeType
(: let $specialDay:=$reqF/ns1:feeSpecialDay/ns2:SpecialDay/ns2:specialDay :)
let $specialDay:=$reqF/ns1:feeSpecialDay
let $feeForServSumm:= $reqF/ns1:feeForServicesSummation

return
&lt;fml:FML32&gt;
  &lt;DC_TRN_ID?&gt;{data($transId)}&lt;/DC_TRN_ID&gt;
  &lt;DC_UZYTKOWNIK?&gt;{concat("SKP:",data($userId))}&lt;/DC_UZYTKOWNIK&gt;
  &lt;DC_ODDZIAL?&gt;{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL&gt;
  &lt;DC_NR_RACHUNKU?&gt;{xf:prepareShortAccountNumber($accountNumber)}&lt;/DC_NR_RACHUNKU&gt;
  {
   if(string-length(data($prCode)) &gt; 0) then 
     &lt;DC_PROMOCJA?&gt;{data($prCode)}&lt;/DC_PROMOCJA&gt;
   else()
  }
  &lt;DC_PLAN_OPLAT?&gt;{data($servChargeCode)}&lt;/DC_PLAN_OPLAT&gt;
  &lt;DC_OKRES_OPL_MANIP?&gt;{data($feeFrequency)}&lt;/DC_OKRES_OPL_MANIP&gt;
  &lt;DC_DATA_NAST_OPL_MANIP?&gt;{data($nextFeeDate)}&lt;/DC_DATA_NAST_OPL_MANIP&gt;
  &lt;DC_OKRES_NALICZ_OPLATY?&gt;{data($period)}&lt;/DC_OKRES_NALICZ_OPLATY&gt;
  &lt;DC_RODZAJ_OPL_MANIP?&gt;{data($feeType)}&lt;/DC_RODZAJ_OPL_MANIP&gt;
  {
   if(string-length(data($specialDay)) &gt; 0) then 
       &lt;DC_DZ_OPL_MANIPUL?&gt;{data($specialDay)}&lt;/DC_DZ_OPL_MANIPUL&gt;
   else()
  }
  &lt;DC_OPL_ZA_USL_PODSUMOWAN?&gt; {data($feeForServSumm)}&lt;/DC_OPL_ZA_USL_PODSUMOWAN&gt;
&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


&lt;soap-env:Body&gt;
{ xf:mapChgTranAccountFeeRequest($body/dcl:invoke/dcl:account/ns1:Account, $body/dcl:invoke/dcl:fee/ns1:Fee,  $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>