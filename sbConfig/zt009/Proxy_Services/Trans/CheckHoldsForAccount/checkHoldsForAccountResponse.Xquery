<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:filtersandmessages.entities.be.dcl";

declare function local:mapprepareCheckHoldsForAccountResponse($fml as element())  as element(m:invokeResponse) {
 &lt;m:invokeResponse&gt;
  &lt;m:response&gt;
   &lt;e:ResponseMessage&gt;
    {	
      if($fml/fml:NF_RESPOM_RESULT) then 
        &lt;e:result&gt; {
           if(data($fml/fml:NF_RESPOM_RESULT) = "1") then "true"									
           else "false"
         } &lt;/e:result&gt;
      else (),
      if($fml/fml:NF_RESPOM_ERRORCODE) then
         &lt;e:errorCode&gt;{ data($fml/fml:NF_RESPOM_ERRORCODE) }&lt;/e:errorCode&gt;
      else(),	
      if($fml/fml:NF_RESPOM_ERRORDESCRIPTION) then &lt;e:errorDescription&gt;{ data($fml/fml:NF_RESPOM_ERRORDESCRIPTION) }&lt;/e:errorDescription&gt;
      else()
    }
   &lt;/e:ResponseMessage&gt;
  &lt;/m:response&gt;
 &lt;/m:invokeResponse&gt;		
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ local:mapprepareCheckHoldsForAccountResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>