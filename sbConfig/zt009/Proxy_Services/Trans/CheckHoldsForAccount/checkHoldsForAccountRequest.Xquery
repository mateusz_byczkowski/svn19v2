<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:accounts.entities.be.dcl";
declare namespace e2 = "urn:accountdict.dictionaries.be.dcl";

declare function local:mapCheckHoldsForAccountRequest($req as element())  as element(fml:FML32) {
 &lt;fml:FML32&gt;
      &lt;fml:NF_ACCOUN_ACCOUNTNUMBER&gt;{ data($req/m:account/e:Account/e:accountNumber) }&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER&gt;
  &lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ local:mapCheckHoldsForAccountRequest($body/m:invoke) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>