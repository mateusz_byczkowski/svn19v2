<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForRelationships($parm as element(fml:FML32)) as element()
{

&lt;ns6:relationships&gt;
  {
    for $x at $occ in $parm/NF_ACCOUR_RELATIONSHIP
    return
    &lt;ns4:AccountRelationship&gt;
      &lt;ns4:relationship&gt;
        &lt;ns3:CustomerAccountRelationship&gt;
          &lt;ns3:customerAccountRelationship&gt;{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns3:customerAccountRelationship&gt;
        &lt;/ns3:CustomerAccountRelationship&gt;
      &lt;/ns4:relationship&gt;
      &lt;ns4:applicationNumber&gt;
        &lt;ns3:ApplicationNumber&gt;
          &lt;ns3:applicationNumber&gt;{data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ])}&lt;/ns3:applicationNumber&gt;
        &lt;/ns3:ApplicationNumber&gt;
      &lt;/ns4:applicationNumber&gt;
    &lt;/ns4:AccountRelationship&gt;
  }
&lt;/ns6:relationships&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns6:invokeResponse&gt;
  &lt;ns6:response&gt;
    &lt;ns2:ResponseMessage&gt;
      &lt;ns2:result&gt;true&lt;/ns2:result&gt;
    &lt;/ns2:ResponseMessage&gt;
  &lt;/ns6:response&gt;
  {getElementsForRelationships($parm)}
&lt;/ns6:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>