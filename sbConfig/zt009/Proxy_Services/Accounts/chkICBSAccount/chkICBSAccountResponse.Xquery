<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:transferorder.entities.be.dcl";

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml="";

declare variable $body as element(soap:Body) external;

&lt;soap:Body&gt;
  &lt;ns0:invokeResponse&gt;
    &lt;ns0:transferOrderTargetOut&gt;
      &lt;ns2:TransferOrderTarget&gt;
        &lt;ns2:accountExternalType&gt;
          {data($body/fml:FML32/NF_TRANOT_ACCOUNTEXTERNALT)}
        &lt;/ns2:accountExternalType&gt;
      &lt;/ns2:TransferOrderTarget&gt;
    &lt;/ns0:transferOrderTargetOut&gt;
  &lt;/ns0:invokeResponse&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>