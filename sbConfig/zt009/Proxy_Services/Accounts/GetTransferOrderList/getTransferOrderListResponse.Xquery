<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function sourceValue2Boolean($parm as xs:string?,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32), $occ as xs:integer) as element()
{

&lt;ns2:transferOrderTargetList&gt;
  {
    &lt;ns2:TransferOrderTarget&gt;
      &lt;ns2:amountTarget?&gt;{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}&lt;/ns2:amountTarget&gt;
      &lt;ns2:codeProfile?&gt;{data($parm/NF_TRANOT_CODEPROFILE[$occ])}&lt;/ns2:codeProfile&gt;
      &lt;ns2:transferOrderDescription?&gt;{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}&lt;/ns2:transferOrderDescription&gt;
      &lt;ns2:beneficiary1?&gt;{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}&lt;/ns2:beneficiary1&gt;
      &lt;ns2:beneficiary2?&gt;{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}&lt;/ns2:beneficiary2&gt;
      &lt;ns2:referenceNumber?&gt;{data($parm/NF_TRANOT_REFERENCENUMBER[$occ])}&lt;/ns2:referenceNumber&gt;
      &lt;ns2:currencyCode&gt;
        &lt;ns5:CurrencyCode&gt;
          &lt;ns5:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode&gt;
        &lt;/ns5:CurrencyCode&gt;
      &lt;/ns2:currencyCode&gt;
    &lt;/ns2:TransferOrderTarget&gt;
  }
&lt;/ns2:transferOrderTargetList&gt;
};
declare function getElementsForTransferOrderAnchorOut($parm as element(fml:FML32)) as element()
{

&lt;ns0:transferOrderAnchorOut&gt;
  {
    for $x at $occ in $parm/NF_TRANOA_TRANSFERORDERNUM
    return
    &lt;ns2:TransferOrderAnchor&gt;
      &lt;ns2:frequency?&gt;{data($parm/NF_TRANOA_FREQUENCY[$occ])}&lt;/ns2:frequency&gt;
	  { insertDate(data($parm/NF_TRANOA_PAYMENTDATE[$occ]),"yyyy-MM-dd","ns2:paymentDate")}
	  { insertDate(data($parm/NF_TRANOA_EXPIRATIONDATE[$occ]),"yyyy-MM-dd","ns2:expirationDate")}
      &lt;ns2:codeProfile?&gt;{data($parm/NF_TRANOA_CODEPROFILE[$occ])}&lt;/ns2:codeProfile&gt;
      &lt;ns2:nonBusinessDayRule?&gt;{sourceValue2Boolean(data($parm/NF_TRANOA_NONBUSINESSDAYRU[$occ]),"1")}&lt;/ns2:nonBusinessDayRule&gt;
      &lt;ns2:transferOrderNumber?&gt;{data($parm/NF_TRANOA_TRANSFERORDERNUM[$occ])}&lt;/ns2:transferOrderNumber&gt;
      &lt;ns2:charging?&gt;{sourceValue2Boolean(data($parm/NF_TRANOA_CHARGING[$occ]),"1")}&lt;/ns2:charging&gt;
      &lt;ns2:active?&gt;{sourceValue2Boolean(data($parm/NF_TRANOA_ACTIVE[$occ]),"1")}&lt;/ns2:active&gt;
	  { insertDate(data($parm/NF_TRANOA_FIRSTPAYMENTDATE[$occ]),"yyyy-MM-dd","ns2:firstPaymentDate")}
      &lt;ns2:referenceNumber?&gt;{data($parm/NF_TRANOA_REFERENCENUMBER[$occ])}&lt;/ns2:referenceNumber&gt;
      {getElementsForTransferOrderTargetList($parm, $occ)}
      &lt;ns2:account&gt;
        &lt;ns6:Account&gt;
          &lt;ns6:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns6:accountNumber&gt;
        &lt;/ns6:Account&gt;
      &lt;/ns2:account&gt;
      &lt;ns2:period&gt;
        &lt;ns5:Period&gt;
          &lt;ns5:period?&gt;{data($parm/NF_PERIOD_PERIOD[$occ])}&lt;/ns5:period&gt;
        &lt;/ns5:Period&gt;
      &lt;/ns2:period&gt;
    &lt;/ns2:TransferOrderAnchor&gt;
  }
&lt;/ns0:transferOrderAnchorOut&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  {getElementsForTransferOrderAnchorOut($parm)}
  &lt;ns0:bcd&gt;
    &lt;ns3:BusinessControlData&gt;
      &lt;ns3:pageControl&gt;
        &lt;ns4:PageControl&gt;
          &lt;ns4:hasNext?&gt;{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns4:hasNext&gt;
          &lt;ns4:navigationKeyDefinition?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns4:navigationKeyDefinition&gt;
          &lt;ns4:navigationKeyValue?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns4:navigationKeyValue&gt;
        &lt;/ns4:PageControl&gt;
      &lt;/ns3:pageControl&gt;
    &lt;/ns3:BusinessControlData&gt;
  &lt;/ns0:bcd&gt;
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>