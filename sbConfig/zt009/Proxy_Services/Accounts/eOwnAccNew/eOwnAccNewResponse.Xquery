<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeOwnAccNewResponse($fml as element(fml:FML32))
	as element(m:eOwnAccNewResponse) {
		&lt;m:eOwnAccNewResponse&gt;
			{
				if($fml/fml:E_ADMIN_MICRO_BRANCH)
					then &lt;m:AdminMicroBranch&gt;{ data($fml/fml:E_ADMIN_MICRO_BRANCH) }&lt;/m:AdminMicroBranch&gt;
					else ()
			}
			{
				if($fml/fml:U_USER_NAME)
					then &lt;m:UserName&gt;{ data($fml/fml:U_USER_NAME) }&lt;/m:UserName&gt;
					else ()
			}
			{
				if($fml/fml:B_SYS_MASK)
					then &lt;m:SysMask&gt;{ data($fml/fml:B_SYS_MASK) }&lt;/m:SysMask&gt;
					else ()
			}
			{

				let $B_SYS := $fml/fml:B_SYS
				let $B_BANK := $fml/fml:B_BANK
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				let $E_SEQ_NO := $fml/fml:E_SEQ_NO
				let $E_CHANNEL_MASK := $fml/fml:E_CHANNEL_MASK
				let $B_KOD_RACH := $fml/fml:B_KOD_RACH
				let $E_TIME_STAMP := $fml/fml:E_TIME_STAMP
				let $E_ACCOUNT_TYPE_OPTIONS := $fml/fml:E_ACCOUNT_TYPE_OPTIONS
				let $E_DR_TRN_MASK := $fml/fml:E_DR_TRN_MASK
				let $E_CR_TRN_MASK := $fml/fml:E_CR_TRN_MASK
				let $E_LOGIN_ID := $fml/fml:E_LOGIN_ID
				let $E_OPTIONS := $fml/fml:E_OPTIONS
				let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
				let $B_WALUTA := $fml/fml:B_WALUTA
				let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
				for $it at $p in $fml/fml:B_KOD_RACH
				return
					&lt;m:eOwnAccNewAccount&gt;
					{
						if($B_SYS[$p])
							then &lt;m:Sys&gt;{ data($B_SYS[$p]) }&lt;/m:Sys&gt;
						else ()
					}
					{
						if($B_BANK[$p])
							then &lt;m:Bank&gt;{ data($B_BANK[$p]) }&lt;/m:Bank&gt;
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then &lt;m:TypRach&gt;{ data($B_TYP_RACH[$p]) }&lt;/m:TypRach&gt;
						else ()
					}
					{
						if($E_SEQ_NO[$p])
							then &lt;m:SeqNo&gt;{ data($E_SEQ_NO[$p]) }&lt;/m:SeqNo&gt;
						else ()
					}
					{
						if($E_CHANNEL_MASK[$p])
							then &lt;m:ChannelMask&gt;{ data($E_CHANNEL_MASK[$p]) }&lt;/m:ChannelMask&gt;
						else ()
					}
					{
						if($B_KOD_RACH[$p])
							then &lt;m:KodRach&gt;{ data($B_KOD_RACH[$p]) }&lt;/m:KodRach&gt;
						else ()
					}
					{
						if($E_TIME_STAMP[$p])
							then &lt;m:TimeStamp&gt;{ data($E_TIME_STAMP[$p]) }&lt;/m:TimeStamp&gt;
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_OPTIONS[$p])
							then &lt;m:AccountTypeOptions&gt;{ data($E_ACCOUNT_TYPE_OPTIONS[$p]) }&lt;/m:AccountTypeOptions&gt;
						else ()
					}
					{
						if($E_DR_TRN_MASK[$p])
							then &lt;m:DrTrnMask&gt;{ data($E_DR_TRN_MASK[$p]) }&lt;/m:DrTrnMask&gt;
						else ()
					}
					{
						if($E_CR_TRN_MASK[$p])
							then &lt;m:CrTrnMask&gt;{ data($E_CR_TRN_MASK[$p]) }&lt;/m:CrTrnMask&gt;
						else ()
					}
					{
						if($E_LOGIN_ID[$p])
							then &lt;m:LoginId&gt;{ data($E_LOGIN_ID[$p]) }&lt;/m:LoginId&gt;
						else ()
					}
					{
						if($E_OPTIONS[$p])
							then &lt;m:Options&gt;{ data($E_OPTIONS[$p]) }&lt;/m:Options&gt;
						else ()
					}
					{
						if($B_DL_NR_RACH[$p])
							then &lt;m:DlNrRach&gt;{ data($B_DL_NR_RACH[$p]) }&lt;/m:DlNrRach&gt;
						else ()
					}
					{
						if($B_WALUTA[$p])
							then &lt;m:Waluta&gt;{ data($B_WALUTA[$p]) }&lt;/m:Waluta&gt;
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_NAME[$p])
							then &lt;m:AccountTypeName&gt;{ data($E_ACCOUNT_TYPE_NAME[$p]) }&lt;/m:AccountTypeName&gt;
						else ()
					}
					&lt;/m:eOwnAccNewAccount&gt;
			}

		&lt;/m:eOwnAccNewResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapeOwnAccNewResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>