<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:dictionaries.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns4:invokeResponse>
  <ns4:statementOut>
    <ns0:Statement>
      <ns0:accountInterestRate?>{data($parm/NF_STATEM_ACCOUNTINTERESTR)}</ns0:accountInterestRate>
      <ns0:account>
        <ns0:Account>
          <ns0:customerNumber?>{data($parm/NF_ACCOUN_CUSTOMERNUMBER)}</ns0:customerNumber>
          <ns0:accountBranchNumber>
            <ns1:BranchCode>
              <ns1:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE)}</ns1:branchCode>
            </ns1:BranchCode>
          </ns0:accountBranchNumber>
          <ns0:accountAddress>
            <ns0:AccountAddress>
              <ns0:name1?>{data($parm/NF_ACCOUA_NAME1)}</ns0:name1>
              <ns0:name2?>{data($parm/NF_ACCOUA_NAME2)}</ns0:name2>
              <ns0:street?>{data($parm/NF_ACCOUA_STREET)}</ns0:street>
              <ns0:houseFlatNumber?>{data($parm/NF_ACCOUA_HOUSEFLATNUMBER)}</ns0:houseFlatNumber>
              <ns0:city?>{data($parm/NF_ACCOUA_CITY)}</ns0:city>
              <ns0:stateCountry?>{data($parm/NF_ACCOUA_STATECOUNTRY)}</ns0:stateCountry>
              <ns0:zipCode?>{data($parm/NF_ACCOUA_ZIPCODE)}</ns0:zipCode>
            </ns0:AccountAddress>
          </ns0:accountAddress>
        </ns0:Account>
      </ns0:account>
    </ns0:Statement>
  </ns4:statementOut>
  <ns4:stringHolder>
    <ns5:StringHolder>
      <ns5:value?>{data($parm/NF_STRINH_VALUE)}</ns5:value>
    </ns5:StringHolder>
  </ns4:stringHolder>
  <ns4:integerHolder>
    <ns5:IntegerHolder>
      <ns5:value?>{data($parm/NF_INTEGH_VALUE)}</ns5:value>
    </ns5:IntegerHolder>
  </ns4:integerHolder>
</ns4:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>