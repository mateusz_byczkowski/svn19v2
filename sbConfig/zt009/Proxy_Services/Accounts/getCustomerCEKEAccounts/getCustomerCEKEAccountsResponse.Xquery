<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-13</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:accounts.entities.be.dcl";
declare namespace urn4 = "urn:dictionaresrepo.dictionaries.be.dcl";

declare function xf:mapgetCustomerCEKEAccountsResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		&lt;urn:invokeResponse&gt;
			&lt;urn:accounts&gt;
				{
					let $B_SYS := $fml/fml:B_SYS
					let $B_TYP_RACH := $fml/fml:B_TYP_RACH
					let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
					let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
					let $B_SKROT_OPISU := $fml/fml:B_SKROT_OPISU
					let $B_KOD_RACH := $fml/fml:B_KOD_RACH (: not used :)
					let $B_WALUTA := $fml/fml:B_WALUTA
					let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
					let $B_MIKROODDZIAL := $fml/fml:B_MIKROODDZIAL (: not used :)
					let $E_FEE_ALLOWED := $fml/fml:E_FEE_ALLOWED
					let $E_REGISTER_ACCOUNT := $fml/fml:E_REGISTER_ACCOUNT
					let $E_DR_CR_ACCOUNT := $fml/fml:E_DR_CR_ACCOUNT
					let $E_SEQ_NO := $fml/fml:E_SEQ_NO
					let $E_FEE_ENABLED := $fml/fml:E_FEE_ENABLED

					for $it at $p in $fml/fml:B_DL_NR_RACH
					return
					(
						&lt;urn1:OwnAccountCEKE&gt;
							{
								&lt;urn1:cekeAccountType?&gt;{data($B_TYP_RACH[$p])}&lt;/urn1:cekeAccountType&gt;
							}
							{
								if($fml/fml:E_REGISTER_ACCOUNT[$p])
								then (

										if (data($fml/fml:E_REGISTER_ACCOUNT[$p]) = "1") then
										(
											&lt;urn1:registerAccount&gt;true&lt;/urn1:registerAccount&gt;
										) else if (data($fml/fml:E_REGISTER_ACCOUNT[$p]) = "0") then
										(
											&lt;urn1:registerAccount&gt;false&lt;/urn1:registerAccount&gt;
										) else()

								)
								else ()
							}
							{
								if($fml/fml:E_DR_CR_ACCOUNT[$p])
									then (

											if (data($fml/fml:E_DR_CR_ACCOUNT[$p]) = "1") then
											(
												&lt;urn1:debitCreditAccount&gt;true&lt;/urn1:debitCreditAccount&gt;
											) else if (data($fml/fml:E_DR_CR_ACCOUNT[$p]) = "0") then
											(
												&lt;urn1:debitCreditAccount&gt;false&lt;/urn1:debitCreditAccount&gt;
											) else()

									)
									else ()
							}
							{
								if($E_FEE_ALLOWED[$p])
									then (

											if (data($E_FEE_ALLOWED[$p]) = "1") then
											(
												&lt;urn1:feeAllowedAccount&gt;true&lt;/urn1:feeAllowedAccount&gt;
											) else if (data($E_FEE_ALLOWED[$p]) = "0") then
											(
												&lt;urn1:feeAllowedAccount&gt;false&lt;/urn1:feeAllowedAccount&gt;
											) else()

									)
								else ()
							}
							
							{
								if ($E_SEQ_NO[$p]) then
								(
								if (data($E_SEQ_NO[$p]) != "0") then
								(
									&lt;urn1:seqNum&gt;{ data($E_SEQ_NO[$p]) }&lt;/urn1:seqNum&gt;
								)
								else
								(
									&lt;urn1:seqNum&gt;&lt;/urn1:seqNum&gt;
								)
								)
								else()	
							}
							
							
							
							{
								if($E_FEE_ENABLED[$p])
									then (
										if (data($E_FEE_ENABLED[$p]) = "1") then
										(
											&lt;urn1:flagFeeAccount&gt;true&lt;/urn1:flagFeeAccount&gt;
										) else if (data($E_FEE_ENABLED[$p]) = "0") then
										(
											&lt;urn1:flagFeeAccount&gt;false&lt;/urn1:flagFeeAccount&gt;
										) else ()
									)
									else ()
							}
							{
								if($B_SYS[$p])
									then
									(
										&lt;urn1:sourceSystem&gt;
											&lt;urn2:System&gt;
												{
													if (data($B_SYS[$p] = "1")) then
													(
														&lt;urn2:system&gt;5&lt;/urn2:system&gt;
													) else if (data($B_SYS[$p] = "3")) then
													(
														&lt;urn2:system&gt;1&lt;/urn2:system&gt;
													) else if (data($B_SYS[$p] = "4")) then
													(
														&lt;urn2:system&gt;7&lt;/urn2:system&gt;
													) else if (data($B_SYS[$p] = "5")) then
													(
														&lt;urn2:system&gt;9&lt;/urn2:system&gt;
													) else
													(
														&lt;urn2:system&gt;{ data($B_SYS[$p]) }&lt;/urn2:system&gt;
													)
												}
											&lt;/urn2:System&gt;
										&lt;/urn1:sourceSystem&gt;
									)
								else ()
							}
							{
								&lt;urn1:account&gt;
									&lt;urn3:Account&gt;
										{
											&lt;urn3:accountNumber?&gt;{ data($B_DL_NR_RACH[$p]) }&lt;/urn3:accountNumber&gt;
										}
										{
											&lt;urn3:accountName?&gt;{ data($E_ACCOUNT_TYPE_NAME[$p]) }&lt;/urn3:accountName&gt;
										}
										{
											&lt;urn3:accountDescription?&gt;{ data($B_SKROT_OPISU[$p]) }&lt;/urn3:accountDescription&gt;
										}
										{
											if ($B_WALUTA[$p]) then
											(
												&lt;urn3:currency&gt;
													&lt;urn2:CurrencyCode&gt;
														&lt;urn2:currencyCode&gt;{ data($B_KOD_WALUTY[$p]) }&lt;/urn2:currencyCode&gt;
													&lt;/urn2:CurrencyCode&gt;
												&lt;/urn3:currency&gt;
											) else ()
										}
										{
											if ($B_MIKROODDZIAL[$p]) then 
											(
											&lt;urn3:accountBranchNumber&gt;
												&lt;urn2:BranchCode&gt;
													&lt;urn2:branchCode&gt; { data($B_MIKROODDZIAL[$p]) }&lt;/urn2:branchCode&gt;
												&lt;/urn2:BranchCode&gt;
											&lt;/urn3:accountBranchNumber&gt;
											) else()
										}
										(: nothing to insert
										{
											&lt;urn3:accountStatus?&gt;
												&lt;urn4:AccountStatus?&gt;
													&lt;urn4:accountStatus?&gt;{ data() }&lt;/urn4:accountStatus&gt;
												&lt;/urn4:AccountStatus&gt;
											&lt;/urn3:accountStatus&gt;
										}
										:)
									&lt;/urn3:Account&gt;
								&lt;/urn1:account&gt;
							}
						&lt;/urn1:OwnAccountCEKE&gt;
					)
				}

			&lt;/urn:accounts&gt;
		&lt;/urn:invokeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetCustomerCEKEAccountsResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>