<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2010-03-17
 :
 : wersja WSDLa: 17-03-2010 13:33:37
 :
 : $Proxy Services/getDirectDebitTransactionList/getDirectDebitTransactionListResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getDirectDebitTransactionList/getDirectDebitTransactionListResponse/";
declare namespace ns0 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:entities.be.dcl";
declare namespace ns2 = "";
declare namespace ns3 = "urn:operations.entities.be.dcl";
declare namespace ns4 = "urn:be.services.dcl";
declare namespace ns5 = "urn:baseauxentities.be.dcl";

declare variable $fML321 as element(ns2:FML32) external;

declare function xf:getDirectDebitTransactionListResponse($fML321 as element(ns2:FML32))
    as element(ns4:invokeResponse)
{
    &lt;ns4:invokeResponse&gt;
    
		(:
		 : dane do stronicowania
		 :)
        &lt;ns4:bcd&gt;
            &lt;ns1:BusinessControlData&gt;
                &lt;ns1:pageControl?&gt;
                    &lt;ns5:PageControl?&gt;
	                    {
	                    	let $hasNext := data($fML321/ns2:NF_PAGEC_HASNEXT)
	                    	return
	                    		if ($hasNext) then
			                        &lt;ns5:hasNext&gt;{
										xs:boolean($hasNext)
									}&lt;/ns5:hasNext&gt;
	                    		else
									()
	                    }

                        &lt;ns5:navigationKeyDefinition?&gt;{
							data($fML321/ns2:NF_PAGEC_NAVIGATIONKEYDEFI)
						}&lt;/ns5:navigationKeyDefinition&gt;
						
                        &lt;ns5:navigationKeyValue?&gt;{
							data($fML321/ns2:NF_PAGEC_NAVIGATIONKEYVALU)
						}&lt;/ns5:navigationKeyValue&gt;
                    &lt;/ns5:PageControl&gt;
                &lt;/ns1:pageControl&gt;
            &lt;/ns1:BusinessControlData&gt;
        &lt;/ns4:bcd&gt;
        
        (:
         : dane wyjsciowe
         :)
        &lt;ns4:transactionList&gt;
        {
        	for $i in 1 to count($fML321/ns2:NF_TRANSA_TITLE)
        	return
	            &lt;ns3:Transaction&gt;

					{
	                	let $putDownDate := data($fML321/ns2:NF_TRANSA_PUTDOWNDATE[$i])
	                	return
	                		if ($putDownDate) then
	                			&lt;ns3:putDownDate&gt;{
									$putDownDate
								}&lt;/ns3:putDownDate&gt;
							else
								()
					}

	                &lt;ns3:title&gt;{
						data($fML321/ns2:NF_TRANSA_TITLE[$i])
					}&lt;/ns3:title&gt;

	                &lt;ns3:transactionWn&gt;
	                    &lt;ns3:TransactionWn&gt;
	                        &lt;ns3:accountNumber&gt;{
								data($fML321/ns2:NF_TRANSW_ACCOUNTNUMBER[$i])
							}&lt;/ns3:accountNumber&gt;

	                        &lt;ns3:name&gt;{
								data($fML321/ns2:NF_TRANSW_NAME[$i])
							}&lt;/ns3:name&gt;

	                        &lt;ns3:city/&gt;

	                        &lt;ns3:address&gt;{
								data($fML321/ns2:NF_TRANSW_ADDRESS[$i])
							}&lt;/ns3:address&gt;

	                        &lt;ns3:zipCode/&gt;

	                        &lt;ns3:nameSecond&gt;{
								data($fML321/ns2:NF_TRANSW_NAMESECOND[$i])
							}&lt;/ns3:nameSecond&gt;
	                    &lt;/ns3:TransactionWn&gt;
	                &lt;/ns3:transactionWn&gt;

	                &lt;ns3:transactionMa&gt;
	                    &lt;ns3:TransactionMa&gt;
	                        &lt;ns3:accountNumber&gt;{
								data($fML321/ns2:NF_TRANSM_ACCOUNTNUMBER[$i])
							}&lt;/ns3:accountNumber&gt;

	                        &lt;ns3:name&gt;{
								data($fML321/ns2:NF_TRANSM_NAME[$i])
							}&lt;/ns3:name&gt;

	                        &lt;ns3:city/&gt;

	                        &lt;ns3:address&gt;{
								data($fML321/ns2:NF_TRANSM_ADDRESS[$i])
							}&lt;/ns3:address&gt;

	                        &lt;ns3:zipCode/&gt;

	                        &lt;ns3:nameSecond&gt;{
								data($fML321/ns2:NF_TRANSM_NAMESECOND[$i])
							}&lt;/ns3:nameSecond&gt;
	                    &lt;/ns3:TransactionMa&gt;
	                &lt;/ns3:transactionMa&gt;,

	                &lt;ns3:directDebitCallOff&gt;
	                    &lt;ns3:DirectDebitCallOff&gt;
	                        &lt;ns3:originDDTxnAmount&gt;{
	                        	data($fML321/ns2:NF_DIRDCO_ORIGINDDTXNAMOUN[$i])
							}&lt;/ns3:originDDTxnAmount&gt;

	                        &lt;ns3:originDDTxnDate&gt;{
								data($fML321/ns2:NF_DIRDCO_ORIGINDDTXNDATE[$i])
							}&lt;/ns3:originDDTxnDate&gt;

	                        &lt;ns3:originDDTxnReferenceNr&gt;{
								data($fML321/ns2:NF_DIRDCO_ORIGINDDTXNREFER[$i])
							}&lt;/ns3:originDDTxnReferenceNr&gt;

	                        &lt;ns3:originDDTxnCurrency&gt;
	                            &lt;ns0:CurrencyCode&gt;
	                                &lt;ns0:currencyCode&gt;{
										data($fML321/ns2:NF_CURREC_CURRENCYCODE[$i])
									}&lt;/ns0:currencyCode&gt;
	                            &lt;/ns0:CurrencyCode&gt;
	                        &lt;/ns3:originDDTxnCurrency&gt;
	                    &lt;/ns3:DirectDebitCallOff&gt;
	                &lt;/ns3:directDebitCallOff&gt;
				&lt;/ns3:Transaction&gt;
        }
        &lt;/ns4:transactionList&gt;
    &lt;/ns4:invokeResponse&gt;
};

&lt;soap-env:Body&gt;{
	xf:getDirectDebitTransactionListResponse($fML321)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>