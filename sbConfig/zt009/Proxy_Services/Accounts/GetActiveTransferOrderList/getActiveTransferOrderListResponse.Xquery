<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:transferorder.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType
{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function sourceValue2Boolean ($parm as xs:anyType,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32), $occ as xs:integer) as element()
{

&lt;ns1:transferOrderTargetList&gt;
  {
   &lt;ns1:TransferOrderTarget&gt;
      &lt;ns1:amountTarget?&gt;{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}&lt;/ns1:amountTarget&gt;
      &lt;ns1:codeProfile?&gt;{data($parm/NF_TRANOT_CODEPROFILE[$occ])}&lt;/ns1:codeProfile&gt;
      &lt;ns1:transferOrderDescription?&gt;{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}&lt;/ns1:transferOrderDescription&gt;
      &lt;ns1:beneficiary1?&gt;{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}&lt;/ns1:beneficiary1&gt;
      &lt;ns1:beneficiary2?&gt;{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}&lt;/ns1:beneficiary2&gt;
      &lt;ns1:currencyCode&gt;
        &lt;ns3:CurrencyCode&gt;
          &lt;ns3:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns3:currencyCode&gt;
        &lt;/ns3:CurrencyCode&gt;
      &lt;/ns1:currencyCode&gt;
   &lt;/ns1:TransferOrderTarget&gt;
  }
&lt;/ns1:transferOrderTargetList&gt;
};
declare function getElementsForTransferOrder($parm as element(fml:FML32)) as element()
{

&lt;ns6:transferOrder&gt;
  {
    for $x at $occ in $parm/NF_TRANOA_TRANSFERORDERNUM
    return
    &lt;ns1:TransferOrderAnchor&gt;
      &lt;ns1:frequency?&gt;{data($parm/NF_TRANOA_FREQUENCY[$occ])}&lt;/ns1:frequency&gt;
      (:&lt;ns1:paymentDate?&gt;{data($parm/NF_TRANOA_PAYMENTDATE[$occ])}&lt;/ns1:paymentDate&gt;:)
      { insertDate(data($parm/NF_TRANOA_PAYMENTDATE[$occ]),"yyyy-MM-dd","ns1:paymentDate")}
      (:&lt;ns1:expirationDate?&gt;{data($parm/NF_TRANOA_EXPIRATIONDATE[$occ])}&lt;/ns1:expirationDate&gt;:)
      { insertDate(data($parm/NF_TRANOA_EXPIRATIONDATE[$occ]),"yyyy-MM-dd","ns1:expirationDate")}
      &lt;ns1:codeProfile?&gt;{data($parm/NF_TRANOA_CODEPROFILE[$occ])}&lt;/ns1:codeProfile&gt;
      &lt;ns1:nonBusinessDayRule?&gt;{sourceValue2Boolean(data($parm/NF_TRANOA_NONBUSINESSDAYRU[$occ]),"1")}&lt;/ns1:nonBusinessDayRule&gt;
      &lt;ns1:transferOrderNumber?&gt;{data($parm/NF_TRANOA_TRANSFERORDERNUM[$occ])}&lt;/ns1:transferOrderNumber&gt;
      (:&lt;ns1:charging?&gt;{data($parm/NF_TRANOA_CHARGING[$occ])}&lt;/ns1:charging&gt;:)
      &lt;ns1:charging?&gt;{sourceValue2Boolean(data($parm/NF_TRANOA_CHARGING[$occ]),"1")}&lt;/ns1:charging&gt;
      (:&lt;ns1:firstPaymentDate?&gt;{data($parm/NF_TRANOA_FIRSTPAYMENTDATE[$occ])}&lt;/ns1:firstPaymentDate&gt;:)
      { insertDate(data($parm/NF_TRANOA_FIRSTPAYMENTDATE[$occ]),"yyyy-MM-dd","ns1:firstPaymentDate")}
      {getElementsForTransferOrderTargetList($parm, $occ)}
      &lt;ns1:period&gt;
        &lt;ns3:Period&gt;
          &lt;ns3:period?&gt;{data($parm/NF_PERIOD_PERIOD[$occ])}&lt;/ns3:period&gt;
        &lt;/ns3:Period&gt;
      &lt;/ns1:period&gt;
    &lt;/ns1:TransferOrderAnchor&gt;
  }
&lt;/ns6:transferOrder&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns6:invokeResponse&gt;
  {getElementsForTransferOrder($parm)}
  &lt;ns6:bcd&gt;
    &lt;ns4:BusinessControlData&gt;
      &lt;ns4:pageControl&gt;
        &lt;ns7:PageControl&gt;
          (:&lt;ns7:hasNext?&gt;{data($parm/NF_PAGEC_HASNEXT)}&lt;/ns7:hasNext&gt;:)
          &lt;ns7:hasNext&gt;{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns7:hasNext&gt;
          &lt;ns7:navigationKeyDefinition?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns7:navigationKeyDefinition&gt;
          &lt;ns7:navigationKeyValue?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns7:navigationKeyValue&gt;
        &lt;/ns7:PageControl&gt;
      &lt;/ns4:pageControl&gt;
    &lt;/ns4:BusinessControlData&gt;
  &lt;/ns6:bcd&gt;
&lt;/ns6:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>