<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns1="urn:filterandmessages.dictionaries.be.dcl";
declare namespace ns0="urn:transferorder.entities.be.dcl";
declare namespace ns2="urn:be.services.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:dictionaries.be.dcl";
declare namespace ns5="urn:accounts.entities.be.dcl";
declare namespace ns7="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns8="urn:accountdict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function getElementsForHoldList($parm as element(fml:FML32)) as element()
{

<ns5:holdList>
  {
    for $x at $occ in $parm/NF_HOLD_HOLDAMOUNT
    return
    <ns5:Hold>
      <ns5:holdAmount?>{data($parm/NF_HOLD_HOLDAMOUNT[$occ])}</ns5:holdAmount>
      <ns5:holdCurrencyCode>
        <ns6:CurrencyCode>
          <ns6:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns6:currencyCode>
        </ns6:CurrencyCode>
      </ns5:holdCurrencyCode>
    </ns5:Hold>
  }
</ns5:holdList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns2:invokeResponse>
  <ns2:accountOut>
    <ns5:Account>
      {
      if (data($parm/NF_ACCOUN_ACCOUNTOPENDATE) != '0001-01-01') then
         <ns5:accountOpenDate>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_ACCOUN_ACCOUNTOPENDATE))}</ns5:accountOpenDate>
      else ()
      }
      <ns5:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE)}</ns5:currentBalance>
      <ns5:creditPrecentage?>{data($parm/NF_ACCOUN_CREDITPRECENTAGE)}</ns5:creditPrecentage>
(:     
      {
           if (data($parm/NF_ACCOUN_STATEMENTFORACCO) = 1) then 
               <ns5:statementForAccount>true</ns5:statementForAccount>
           else 
               <ns5:statementForAccount>false</ns5:statementForAccount>
      }
	 <ns5:statementForAccount?>{data($parm/NF_ACCOUN_STATEMENTFORACCO)}</ns5:statementForAccount> :)


      <ns5:productCode?>{data($parm/NF_ACCOUN_PRODUCTCODE)}</ns5:productCode>
      <ns5:timeAccount>
        <ns5:TimeAccount>
          <ns5:renewalFrequency?>{data($parm/NF_TIMEA_RENEWALFREQUENCY)}</ns5:renewalFrequency>
          <ns5:originalAmount?>{data($parm/NF_TIMEA_ORIGINALAMOUNT)}</ns5:originalAmount>
           {
           if (data($parm/NF_TIMEA_NEXTRENEWALMATURI) != '0001-01-01') then
              <ns5:nextRenewalMaturityDate>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_TIMEA_NEXTRENEWALMATURI))}</ns5:nextRenewalMaturityDate>
           else ()
           }
           {
           if (data($parm/NF_TIMEA_DISPOSITIONCODE) = 1) then 
               <ns5:dispositionCode>true</ns5:dispositionCode>
           else 
               <ns5:dispositionCode>false</ns5:dispositionCode>
           }
		  
	 (:    <ns5:dispositionCode?>{data($parm/NF_TIMEA_DISPOSITIONCODE)}</ns5:dispositionCode>   :)
          <ns5:interestPaymentFrequency?>{data($parm/NF_TIMEA_INTERESTPAYMENTFR)}</ns5:interestPaymentFrequency>
          {
          if (data($parm/NF_TIMEA_NEXTINRTERESTPAYM) != '0001-01-01') then
             <ns5:nextInrterestPaymentDate>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_TIMEA_NEXTINRTERESTPAYM))}</ns5:nextInrterestPaymentDate>
          else ()
          }
          {
           if (data($parm/NF_TIMEA_INTERESTDISPOSITI) = 1) then 
               <ns5:interestDisposition>true</ns5:interestDisposition>
           else 
               <ns5:interestDisposition>false</ns5:interestDisposition>
           }

   (:     <ns5:interestDisposition?>{data($parm/NF_TIMEA_INTERESTDISPOSITI)}</ns5:interestDisposition>   :)
          <ns5:nextInterestPaymentAmount?>{data($parm/NF_TIMEA_NEXTINTERESTPAYME)}</ns5:nextInterestPaymentAmount>
          <ns5:interestDue?>{data($parm/NF_TIMEA_INTERESTDUE)}</ns5:interestDue>
          <ns5:interestForPay?>{data($parm/NF_TIMEA_INTERESTFORPAY)}</ns5:interestForPay>
          <ns5:dailyInterest?>{data($parm/NF_TIMEA_DAILYINTEREST)}</ns5:dailyInterest>
          {
          if (data($parm/NF_TIMEA_LASTRENEWALDATE) != '0001-01-01') then
              <ns5:lastRenewalDate>{data($parm/NF_TIMEA_LASTRENEWALDATE)}</ns5:lastRenewalDate>
          else ()
          }
          {
          if (data($parm/NF_TIMEA_DATEMAYWITHDRAW) != '0001-01-01') then
             <ns5:dateMayWithdraw>{data($parm/NF_TIMEA_DATEMAYWITHDRAW)}</ns5:dateMayWithdraw>
          else ()
          }
          <ns5:withholdingThresholdAmount?>{data($parm/NF_TIMEA_WITHHOLDINGTHRESH)}</ns5:withholdingThresholdAmount>
          <ns5:currentInterestRate?>{data($parm/NF_TIMEA_CURRENTINTERESTRA)}</ns5:currentInterestRate>
          <ns5:indexVarianceAmount?>{data($parm/NF_TIMEA_INDEXVARIANCEAMOU)}</ns5:indexVarianceAmount>
   
		   {
           if (data($parm/NF_TIMEA_CLOSEOVERRIDEFLAG) = 1) then 
               <ns5:closeOverrideFlag>true</ns5:closeOverrideFlag>
           else 
               <ns5:closeOverrideFlag>false</ns5:closeOverrideFlag>
      }


   (:      <ns5:closeOverrideFlag?>{data($parm/NF_TIMEA_CLOSEOVERRIDEFLAG)}</ns5:closeOverrideFlag>    :)
          {
          if (data($parm/NF_TIMEA_ORIGINALDATEOFINS) != '0001-01-01') then
             <ns5:originalDateOfInstrument>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_TIMEA_ORIGINALDATEOFINS))}</ns5:originalDateOfInstrument>
          else ()
          }
          <ns5:balanceBucket?>{data($parm/NF_TIMEA_BALANCEBUCKET)}</ns5:balanceBucket>
{
     if (data($parm/NF_TIMEA_DATEOFMAINT) != '0001-01-01') then 
          <ns5:dateOfMaint>{data($parm/NF_TIMEA_DATEOFMAINT)}</ns5:dateOfMaint>
     else ()
}
          <ns5:holdAmount?>{data($parm/NF_HOLD_HOLDAMOUNT)}</ns5:holdAmount>
          <ns5:nextInterestAmountPLN?>{data($parm/NF_TIMEA_NEXTINTERESTAMOUN)}</ns5:nextInterestAmountPLN>
(:
          {getElementsForHoldList($parm)}
:)
          <ns5:tax>
            <ns5:Tax>
      {
           if (data($parm/NF_TAX_TAXFLAG) = 1) then 
               <ns5:taxFlag>true</ns5:taxFlag>
           else 
               <ns5:taxFlag>false</ns5:taxFlag>
      }
       (:       <ns5:taxFlag?>{data($parm/NF_TAX_TAXFLAG)}</ns5:taxFlag>         :)
              <ns5:taxPercentage?>{data($parm/NF_TAX_TAXPERCENTAGE)}</ns5:taxPercentage>
              <ns5:taxAmount?>{data($parm/NF_TAX_TAXAMOUNT)}</ns5:taxAmount>
            </ns5:Tax>
          </ns5:tax>
          <ns5:interestRateType>
            <ns8:InterestRateType>
              <ns8:interestRateType?>{data($parm/NF_INTERT_INTERESTRATETYPE)}</ns8:interestRateType>
            </ns8:InterestRateType>
          </ns5:interestRateType>
          <ns5:renewalID>
            <ns6:RenewalType>
              <ns6:renewalType?>{data($parm/NF_RENEWT_RENEWALTYPE)}</ns6:renewalType>
            </ns6:RenewalType>
          </ns5:renewalID>
          <ns5:renewalPeriod>
            <ns6:Period>
              <ns6:period?>{data($parm/NF_PERIOD_PERIOD[2])}</ns6:period>
            </ns6:Period>
          </ns5:renewalPeriod>
(:          <ns5:renewalSpecificDay>
            <ns6:SpecialDay>
              <ns6:specialDay?>{data($parm/NF_SPECID_SPECIALDAY[2])}</ns6:specialDay>
            </ns6:SpecialDay>
          </ns5:renewalSpecificDay>:)
          <ns5:interestPaymentPeriod>
            <ns6:Period>
              <ns6:period?>{data($parm/NF_PERIOD_PERIOD[1])}</ns6:period>
            </ns6:Period>
          </ns5:interestPaymentPeriod>
(:          <ns5:interestPaymentSpecificDay>
            <ns6:SpecialDay>
              <ns6:specialDay?>{data($parm/NF_SPECID_SPECIALDAY[1])}</ns6:specialDay>
            </ns6:SpecialDay>
          </ns5:interestPaymentSpecificDay>:)
          <ns5:competencyCode>
            <ns1:CompetencyCode>
              <ns1:competencyCode?>{data($parm/NF_COMPEC_COMPETENCYCODE)}</ns1:competencyCode>
            </ns1:CompetencyCode>
          </ns5:competencyCode>
        </ns5:TimeAccount>
      </ns5:timeAccount>
      <ns5:statementParameters>
        <ns5:StatementParameters>
          <ns5:frequency?>{data($parm/NF_STATEP_FREQUENCY)}</ns5:frequency>
{
     if (data($parm/NF_STATEP_NEXTPRINTOUTDATE) != '0001-01-01') then 
          <ns5:nextPrintoutDate>{data($parm/NF_STATEP_NEXTPRINTOUTDATE)}</ns5:nextPrintoutDate>
     else ()
}
      {
           if (data($parm/NF_STATEP_PRINTFLAG) = 1) then 
               <ns5:printFlag>true</ns5:printFlag>
           else 
               <ns5:printFlag>false</ns5:printFlag>
      }
          <ns5:cycle>
            <ns6:Period>
              <ns6:period?>{data($parm/NF_PERIOD_PERIOD[3])}</ns6:period>
            </ns6:Period>
          </ns5:cycle>
          <ns5:provideManner>
            <ns6:ProvideManner>
              <ns6:provideManner?>{data($parm/NF_PROVIM_PROVIDEMANNER)}</ns6:provideManner>
            </ns6:ProvideManner>
          </ns5:provideManner>
(:          <ns5:specialDay>
            <ns6:SpecialDay>
              <ns6:specialDay?>{data($parm/NF_SPECID_SPECIALDAY[3])}</ns6:specialDay>
            </ns6:SpecialDay>
          </ns5:specialDay>:)
        </ns5:StatementParameters>
      </ns5:statementParameters>
      <ns5:currency>
        <ns6:CurrencyCode>
          <ns6:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[1])}</ns6:currencyCode>
        </ns6:CurrencyCode>
      </ns5:currency>

      <ns5:accountStatus>
        <ns7:AccountStatus>
          <ns7:accountStatus?>{data($parm/NF_ACCOUS_ACCOUNTSTATUS)}</ns7:accountStatus>
        </ns7:AccountStatus>
      </ns5:accountStatus>

      <ns5:accountBranchNumber>
        <ns6:BranchCode>
          <ns6:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE)}</ns6:branchCode>
        </ns6:BranchCode>
      </ns5:accountBranchNumber>

      <ns5:accessType>
        <ns8:AccessType>
          <ns8:accessType?>{data($parm/NF_ACCEST_ACCESSTYPE)}</ns8:accessType>
        </ns8:AccessType>
      </ns5:accessType>

      <ns5:accountAddress>
        <ns5:AccountAddress>
		  <ns5:versionID?>{data($parm/NF_ACCOUA_VERSIONID)}</ns5:versionID>
          <ns5:name1?>{data($parm/NF_ACCOUA_NAME1)}</ns5:name1>
          <ns5:name2?>{data($parm/NF_ACCOUA_NAME2)}</ns5:name2>
          <ns5:street?>{data($parm/NF_ACCOUA_STREET)}</ns5:street>
          <ns5:houseFlatNumber?>{data($parm/NF_ACCOUA_HOUSEFLATNUMBER)}</ns5:houseFlatNumber>
          <ns5:city?>{data($parm/NF_ACCOUA_CITY)}</ns5:city>
          <ns5:stateCountry?>{data($parm/NF_ACCOUA_STATECOUNTRY)}</ns5:stateCountry>
          <ns5:zipCode?>{data($parm/NF_ACCOUA_ZIPCODE)}</ns5:zipCode>
          <ns5:accountAddressType?>{data($parm/NF_ACCOUA_ACCOUNTADDRESSTY)}</ns5:accountAddressType>
{
     if (data($parm/NF_ACCOUA_VALIDFROM) != '0001-01-01') then 
          <ns5:validFrom>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_ACCOUA_VALIDFROM))}</ns5:validFrom>
     else ()
}
{
     if (data($parm/NF_ACCOUA_VALIDTO) != '0001-01-01') then 
          <ns5:validTo>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_ACCOUA_VALIDTO))}</ns5:validTo>
     else ()
}
        </ns5:AccountAddress>
      </ns5:accountAddress>
    </ns5:Account>
  </ns2:accountOut>
  <ns2:renewalTransfer>
    <ns0:TransferOrderAnchor>
{
     if (data($parm/NF_TRANOA_TRANSFERORDERNUM[2]) != 0) then 
          <ns0:transferOrderNumber>{data($parm/NF_TRANOA_TRANSFERORDERNUM[2])}</ns0:transferOrderNumber>
     else ()
}
    </ns0:TransferOrderAnchor>
  </ns2:renewalTransfer>
  <ns2:interestTransfer>
    <ns0:TransferOrderAnchor>
{
     if (data($parm/NF_TRANOA_TRANSFERORDERNUM[1]) != 0) then 
          <ns0:transferOrderNumber>{data($parm/NF_TRANOA_TRANSFERORDERNUM[1])}</ns0:transferOrderNumber>
     else ()
}
    </ns0:TransferOrderAnchor>
  </ns2:interestTransfer>
</ns2:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>