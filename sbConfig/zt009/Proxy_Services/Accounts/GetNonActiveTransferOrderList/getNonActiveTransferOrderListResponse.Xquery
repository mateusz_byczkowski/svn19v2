<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:transferorder.entities.be.dcl";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType
{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function sourceValue2Boolean ($parm as xs:anyType,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32),$occ as xs:integer) as element()
{
&lt;ns0:transferOrderTargetList&gt;
      &lt;ns0:TransferOrderTarget&gt;
      &lt;ns0:amountTarget?&gt;{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}&lt;/ns0:amountTarget&gt;
      &lt;ns0:codeProfile?&gt;{data($parm/NF_TRANOT_CODEPROFILE[$occ])}&lt;/ns0:codeProfile&gt;
      &lt;ns0:transferOrderDescription?&gt;{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}&lt;/ns0:transferOrderDescription&gt;
      &lt;ns0:beneficiary1?&gt;{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}&lt;/ns0:beneficiary1&gt;
      &lt;ns0:beneficiary2?&gt;{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}&lt;/ns0:beneficiary2&gt;
      &lt;ns0:currencyCode&gt;
        &lt;ns5:CurrencyCode&gt;
          &lt;ns5:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode&gt;
        &lt;/ns5:CurrencyCode&gt;
      &lt;/ns0:currencyCode&gt;
    &lt;/ns0:TransferOrderTarget&gt;
&lt;/ns0:transferOrderTargetList&gt;
};
declare function getElementsForTransferOrder($parm as element(fml:FML32)) as element()
{

&lt;ns1:transferOrder&gt;
  {
    for $x at $occ in $parm/NF_TRANOA_TRANSFERORDERNUM
    return
    &lt;ns0:TransferOrderAnchor&gt;
      &lt;ns0:frequency?&gt;{data($parm/NF_TRANOA_FREQUENCY[$occ])}&lt;/ns0:frequency&gt;
      &lt;ns0:codeProfile?&gt;{data($parm/NF_TRANOA_CODEPROFILE[$occ])}&lt;/ns0:codeProfile&gt;
      &lt;ns0:transferOrderNumber?&gt;{data($parm/NF_TRANOA_TRANSFERORDERNUM[$occ])}&lt;/ns0:transferOrderNumber&gt;
      &lt;ns0:charging?&gt;{data($parm/NF_TRANOA_CHARGING[$occ])}&lt;/ns0:charging&gt;
      { insertDate(data($parm/NF_TRANOA_REVERSEDATE[$occ]),"yyyy-MM-dd","ns0:reverseDate")}
      {getElementsForTransferOrderTargetList($parm,$occ)}
      &lt;ns0:period&gt;
        &lt;ns5:Period&gt;
          &lt;ns5:period?&gt;{data($parm/NF_PERIOD_PERIOD2[$occ])}&lt;/ns5:period&gt;
        &lt;/ns5:Period&gt;
      &lt;/ns0:period&gt;
    &lt;/ns0:TransferOrderAnchor&gt;
  }
&lt;/ns1:transferOrder&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns1:invokeResponse&gt;
  {getElementsForTransferOrder($parm)}
  &lt;ns1:bcd&gt;
    &lt;ns7:BusinessControlData&gt;
      &lt;ns7:pageControl&gt;
        &lt;ns6:PageControl&gt;
          &lt;ns6:hasNext?&gt;{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns6:hasNext&gt;
          &lt;ns6:navigationKeyDefinition?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition&gt;
          &lt;ns6:navigationKeyValue?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue&gt;
        &lt;/ns6:PageControl&gt;
      &lt;/ns7:pageControl&gt;
    &lt;/ns7:BusinessControlData&gt;
  &lt;/ns1:bcd&gt;
&lt;/ns1:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>