<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-return element="ns2:invokeResponse" location="getSimulationForTimeAccount.wsdl"  ::)
(:: pragma bea:global-element-return parameter="$fMLOUT1" element="ns1:FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)



(:~
 :
 : @author  Grzegorz Strawa
 : @version 1.0
 : @since   2010-03-22
 :
 : wersja WSDLa: 03-12-2009 17:04:43
 :
 : $Proxy Services/Accounts/getSimulationForTimeAccount/getSimulationForTimeAccountResponse.xq$
 :
 :)
 
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/getSimulationForTimeAccount/getSimulationForTimeAccountResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:getSimulationForTimeAccountResponse($fMLOUT1 as element(ns1:FML32))
    as element(ns2:invokeResponse) {
        &lt;ns2:invokeResponse&gt;
            &lt;ns2:simulationForTimeAccount&gt;
                &lt;ns0:SimulationForTimeAccount&gt;
                 {
                   if (data($fMLOUT1/ns1:TR_KAPITAL)) then 
                    &lt;ns0:balance&gt;{ data($fMLOUT1/ns1:TR_KAPITAL) }&lt;/ns0:balance&gt; else ()
                  }
                   {
                   if (data($fMLOUT1/ns1:TR_ODSETKI_NALICZONE) ) then 
                    &lt;ns0:interestCalculate&gt;{ data($fMLOUT1/ns1:TR_ODSETKI_NALICZONE) }&lt;/ns0:interestCalculate&gt; else ()
                    }
                   {
                    if (data($fMLOUT1/ns1:TR_ODSETKI_UMORZONE) ) then 
                    &lt;ns0:interestLow&gt;{ data($fMLOUT1/ns1:TR_ODSETKI_UMORZONE) }&lt;/ns0:interestLow&gt; else ()
                    }
                   {
                    if (data($fMLOUT1/ns1:TR_ODSETKI_POTRACONE) ) then 
                    
                    &lt;ns0:interestForPay&gt;{ data($fMLOUT1/ns1:TR_ODSETKI_POTRACONE) }&lt;/ns0:interestForPay&gt; 
                    else ()
                   }
                    {
                    if (data($fMLOUT1/ns1:TR_NALEZNA_OPLATA) ) then 
                    &lt;ns0:interestPayed&gt;{ data($fMLOUT1/ns1:TR_NALEZNA_OPLATA) }&lt;/ns0:interestPayed&gt; 
                    else ()
                   }

                   {
                   if (data($fMLOUT1/ns1:TR_SALDO_DO_WYPLATY) ) then 
                    &lt;ns0:amountForPay&gt;{ data($fMLOUT1/ns1:TR_SALDO_DO_WYPLATY) }&lt;/ns0:amountForPay&gt; 
                    else ()
                   }

				   {               
                   if (data($fMLOUT1/ns1:TR_STAWKA_PODATKU) ) then 
                    &lt;ns0:tax&gt;{ data($fMLOUT1/ns1:TR_STAWKA_PODATKU) }&lt;/ns0:tax&gt; 
                    else ()
                   }
               
               
                   {
                    if (data($fMLOUT1/ns1:TR_DATA_OPER) ) then 
                   
                    &lt;ns0:icbsDateToClose&gt;{  
                        let $transactionDate := $fMLOUT1/ns1:TR_DATA_OPER
					return
						fn:concat(
							fn:substring(data($transactionDate ), 7, 4),
							'-',
							fn:substring(data($transactionDate ), 4, 2),
							'-',
							fn:substring(data($transactionDate ), 1, 2)
							)
						}&lt;/ns0:icbsDateToClose&gt;
					else ()
				}
                &lt;/ns0:SimulationForTimeAccount&gt;
            &lt;/ns2:simulationForTimeAccount&gt;
        &lt;/ns2:invokeResponse&gt;
};

declare variable $fMLOUT1 as element(ns1:FML32) external;

&lt;soap-env:Body&gt;
{
 xf:getSimulationForTimeAccountResponse($fMLOUT1)
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>