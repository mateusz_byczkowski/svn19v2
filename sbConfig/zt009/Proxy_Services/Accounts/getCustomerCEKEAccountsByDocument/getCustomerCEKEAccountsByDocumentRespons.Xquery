<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn3 = "urn:dictionaries.be.dcl";
declare namespace urn4 = "urn:accounts.entities.be.dcl";
declare namespace urn5 = "urn:dictionaresrepo.dictionaries.be.dcl";

declare function xf:mapgetCustomerCEKEAccountsByDocumentResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		&lt;urn:invokeResponse&gt;
			&lt;urn:ownAccountCEKEList&gt;
				{

					let $B_SYS := $fml/fml:B_SYS
					let $B_TYP_RACH := $fml/fml:B_TYP_RACH
					let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
					let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
					let $B_KOD_RACH := $fml/fml:B_KOD_RACH
					let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
					let $B_MIKROODDZIAL := $fml/fml:B_MIKROODDZIAL
					let $E_FEE_ALLOWED := $fml/fml:E_FEE_ALLOWED
					for $it at $p in $fml/fml:B_SYS
					return
						&lt;urn1:OwnAccountCEKE&gt;
						{
							&lt;urn1:cekeAccountType?&gt;{ data($B_TYP_RACH[$p]) }&lt;/urn1:cekeAccountType&gt;
						}
						{
							if($fml/fml:E_REGISTER_ACCOUNT[$p])
								then (

										if (data($fml/fml:E_REGISTER_ACCOUNT[$p]) = "1") then
										(
											&lt;urn1:registerAccount&gt;true&lt;/urn1:registerAccount&gt;
										) else if (data($fml/fml:E_REGISTER_ACCOUNT[$p]) = "0") then
										(
											&lt;urn1:registerAccount&gt;false&lt;/urn1:registerAccount&gt;
										) else()

								)
								else ()
						}
						{
							if($fml/fml:E_DR_CR_ACCOUNT[$p])
								then (

										if (data($fml/fml:E_DR_CR_ACCOUNT[$p]) = "1") then
										(
											&lt;urn1:debitCreditAccount&gt;true&lt;/urn1:debitCreditAccount&gt;
										) else if (data($fml/fml:E_DR_CR_ACCOUNT[$p]) = "0") then
										(
											&lt;urn1:debitCreditAccount&gt;false&lt;/urn1:debitCreditAccount&gt;
										) else()

								)
								else ()
						}
						{
							if($E_FEE_ALLOWED[$p])
								then (

										if (data($E_FEE_ALLOWED[$p]) = "1") then
										(
											&lt;urn1:feeAllowedAccount&gt;true&lt;/urn1:feeAllowedAccount&gt;
										) else if (data($E_FEE_ALLOWED[$p]) = "0") then
										(
											&lt;urn1:feeAllowedAccount&gt;false&lt;/urn1:feeAllowedAccount&gt;
										) else()

								)
							else ()
						}
						{
							if($B_SYS[$p])
								then
								(
									&lt;urn1:sourceSystem&gt;
										&lt;urn3:System&gt;
											{
												if (data($B_SYS[$p] = "1")) then
												(
													&lt;urn3:system&gt;5&lt;/urn3:system&gt;
												) else if (data($B_SYS[$p] = "3")) then
												(
													&lt;urn3:system&gt;1&lt;/urn3:system&gt;
												) else if (data($B_SYS[$p] = "4")) then
												(
													&lt;urn3:system&gt;7&lt;/urn3:system&gt;
												) else if (data($B_SYS[$p] = "5")) then
												(
													&lt;urn3:system&gt;9&lt;/urn3:system&gt;
												) else
												(
													&lt;urn3:system&gt;{ data($B_SYS[$p]) }&lt;/urn3:system&gt;
												)
											}
										&lt;/urn3:System&gt;
									&lt;/urn1:sourceSystem&gt;
								)
							else ()
						}


						{
							&lt;urn1:account&gt;
								&lt;urn4:Account&gt;
									{
										&lt;urn4:accountNumber?&gt;{ data($B_KOD_RACH[$p]) }&lt;/urn4:accountNumber&gt;
									}
									{
										&lt;urn4:accountDescription?&gt;{ data($E_ACCOUNT_TYPE_NAME[$p]) }&lt;/urn4:accountDescription&gt;
									}
									{
										&lt;urn4:accountIBAN?&gt;{ data($B_DL_NR_RACH[$p]) }&lt;/urn4:accountIBAN&gt;
									}
									{
										if($B_KOD_WALUTY[$p])
											then
											(
											&lt;urn4:currency&gt;
												&lt;urn3:CurrencyCode&gt;
													&lt;urn3:currencyCode&gt;{ data($B_KOD_WALUTY[$p]) }&lt;/urn3:currencyCode&gt;
												&lt;/urn3:CurrencyCode&gt;
											&lt;/urn4:currency&gt;
											)
										else ()
									}
									{
										if($B_MIKROODDZIAL[$p])
											then
											(
											&lt;urn4:accountBranchNumber&gt;
												&lt;urn3:BranchCode&gt;
													&lt;urn3:branchCode&gt;{ data($B_MIKROODDZIAL[$p]) }&lt;/urn3:branchCode&gt;
												&lt;/urn3:BranchCode&gt;
											&lt;/urn4:accountBranchNumber&gt;
											)
										else ()
									}
								&lt;/urn4:Account&gt;
							&lt;/urn1:account&gt;

						}

						&lt;/urn1:OwnAccountCEKE&gt;
				}
			&lt;/urn:ownAccountCEKEList&gt;
		&lt;/urn:invokeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetCustomerCEKEAccountsByDocumentResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>