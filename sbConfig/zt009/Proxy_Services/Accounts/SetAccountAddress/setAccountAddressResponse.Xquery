<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  &lt;ns0:response&gt;
    &lt;ns2:ResponseMessage&gt;
      &lt;ns2:result&gt;true&lt;/ns2:result&gt;
      (: &lt;ns2:errorCode?&gt;{data($parm/NF_RESPOM_ERRORCODE)}&lt;/ns2:errorCode&gt;
      &lt;ns2:errorDescription?&gt;{data($parm/NF_RESPOM_ERRORDESCRIPTION)}&lt;/ns2:errorDescription&gt; :)
    &lt;/ns2:ResponseMessage&gt;
  &lt;/ns0:response&gt;
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>