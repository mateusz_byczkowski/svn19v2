<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string,$dateFormat as xs:string) as xs:string{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "0001-01-01"))
            then fn-bea:date-to-string-with-format($dateFormat,fn-bea:date-from-string-with-format($dateFormat,$value))
        else ""
      else ""
};

declare function getElementsForOldList($parm as element(fml:FML32)) as element()
{
let $numberOfElements:=number($parm/NF_PAGECC_OPERATIONS[1]) 
return
&lt;ns0:oldList&gt;
  {
   for $x at $occ in $parm/NF_TRANOS_PAYMENTAMOUNT
    return
     if ($occ &lt;= $numberOfElements) then
    &lt;ns2:TransferOrderSchedule&gt;
{
       if ( insertDate(data($parm/NF_TRANOS_PAYMENTDATE[$occ]),"yyyy-MM-dd") != "") then
       &lt;ns2:paymentDate?&gt;{ insertDate(data($parm/NF_TRANOS_PAYMENTDATE[$occ]),"yyyy-MM-dd")}&lt;/ns2:paymentDate&gt;
       else()
}
      &lt;ns2:paymentAmount?&gt;{data($parm/NF_TRANOS_PAYMENTAMOUNT[$occ])}&lt;/ns2:paymentAmount&gt;
      &lt;ns2:paymentDescription?&gt;{data($parm/NF_TRANOS_PAYMENTDESCRIPTI[$occ])}&lt;/ns2:paymentDescription&gt;
    &lt;/ns2:TransferOrderSchedule&gt;
      else()
  }
&lt;/ns0:oldList&gt;
};
declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32),$occ as xsd:integer) as element()
{

&lt;ns2:transferOrderTargetList&gt;
(:  {
    for $x at $occ in $parm/NF_TRANOT_BENEFICIARY1
    return:)
    &lt;ns2:TransferOrderTarget&gt;
      &lt;ns2:amountTarget?&gt;{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}&lt;/ns2:amountTarget&gt;
      &lt;ns2:transferOrderDescription?&gt;{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}&lt;/ns2:transferOrderDescription&gt;
      &lt;ns2:beneficiary1?&gt;{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}&lt;/ns2:beneficiary1&gt;
      &lt;ns2:beneficiary2?&gt;{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}&lt;/ns2:beneficiary2&gt;
      &lt;ns2:beneficiary3?&gt;{data($parm/NF_TRANOT_BENEFICIARY3[$occ])}&lt;/ns2:beneficiary3&gt;
      &lt;ns2:beneficiary4?&gt;{data($parm/NF_TRANOT_BENEFICIARY4[$occ])}&lt;/ns2:beneficiary4&gt;
      &lt;ns2:account&gt;
        &lt;ns4:Account&gt;
          &lt;ns4:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns4:accountNumber&gt;
        &lt;/ns4:Account&gt;
      &lt;/ns2:account&gt;
      &lt;ns2:accountOutside&gt;
        &lt;ns2:AccountOutside&gt;
          &lt;ns2:accountNumber?&gt;{data($parm/NF_ACCOUO_ACCOUNTNUMBER[$occ])}&lt;/ns2:accountNumber&gt;
        &lt;/ns2:AccountOutside&gt;
      &lt;/ns2:accountOutside&gt;
      &lt;ns2:glaccount&gt;
        &lt;ns4:GLAccount&gt;
          &lt;ns4:accountGL?&gt;{data($parm/NF_GLA_ACCOUNTGL[$occ])}&lt;/ns4:accountGL&gt;
          &lt;ns4:currencyCode&gt;
            &lt;ns3:CurrencyCode&gt;
              &lt;ns3:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns3:currencyCode&gt;
            &lt;/ns3:CurrencyCode&gt;
          &lt;/ns4:currencyCode&gt;
        &lt;/ns4:GLAccount&gt;
      &lt;/ns2:glaccount&gt;
    &lt;/ns2:TransferOrderTarget&gt;
 (: }:)
&lt;/ns2:transferOrderTargetList&gt;
};
declare function getElementsForAnchorList($parm as element(fml:FML32)) as element()
{

&lt;ns0:anchorList&gt;
  {
    for $x at $occ in $parm/NF_CURREC_CURRENCYCODE
    return
      if (data($parm/NF_CTRL_OPTION[$occ]) = 1) then
      &lt;ns2:TransferOrderAnchor /&gt;
     else 
    &lt;ns2:TransferOrderAnchor&gt;
      &lt;ns2:frequency?&gt;{data($parm/NF_TRANOA_FREQUENCY[$occ])}&lt;/ns2:frequency&gt;
{
       if (insertDate(data($parm/NF_TRANOA_PAYMENTDATE[$occ]),"yyyy-MM-dd") != "") then
       &lt;ns2:paymentDate?&gt;{ insertDate(data($parm/NF_TRANOA_PAYMENTDATE[$occ]),"yyyy-MM-dd")}&lt;/ns2:paymentDate&gt;
       else()
}
{
       if (insertDate(data($parm/NF_TRANOA_EXPIRATIONDATE[$occ]),"yyyy-MM-dd") != "") then 
       &lt;ns2:expirationDate?&gt;{ insertDate(data($parm/NF_TRANOA_EXPIRATIONDATE[$occ]),"yyyy-MM-dd")}&lt;/ns2:expirationDate&gt;
       else()
}
      &lt;ns2:codeProfile?&gt;{data($parm/NF_TRANOA_CODEPROFILE[$occ])}&lt;/ns2:codeProfile&gt;
      &lt;ns2:nonBusinessDayRule?&gt;{data($parm/NF_TRANOA_NONBUSINESSDAYRU[$occ])}&lt;/ns2:nonBusinessDayRule&gt;
      {getElementsForTransferOrderTargetList($parm,$occ)}
      &lt;ns2:period&gt;
        &lt;ns3:Period&gt;
          &lt;ns3:period?&gt;{data($parm/NF_PERIOD_PERIOD[$occ])}&lt;/ns3:period&gt;
        &lt;/ns3:Period&gt;
      &lt;/ns2:period&gt;
      &lt;ns2:specificDay&gt;
         &lt;ns3:SpecialDay&gt;
             &lt;ns3:specialDay?&gt;{data($parm/NF_SPECID_SPECIALDAY[$occ])}&lt;/ns3:specialDay&gt;
         &lt;/ns3:SpecialDay&gt;
      &lt;/ns2:specificDay&gt;
      &lt;ns2:currencyCode&gt;
        &lt;ns3:CurrencyCode&gt;
          &lt;ns3:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns3:currencyCode&gt;
        &lt;/ns3:CurrencyCode&gt;
      &lt;/ns2:currencyCode&gt;
    &lt;/ns2:TransferOrderAnchor&gt;
  }
&lt;/ns0:anchorList&gt;
};
declare function getElementsForNewList($parm as element(fml:FML32)) as element()
{

&lt;ns0:newList&gt;
  {
   let $numberOfElements:=number($parm/NF_PAGECC_OPERATIONS[2]) 
    return
    for $x at $occ in $parm/NF_TRANOS_PAYMENTDATE
     return
     if ($occ &gt;  10) then
    &lt;ns2:TransferOrderSchedule&gt;
{
       if ( insertDate(data($parm/NF_TRANOS_PAYMENTDATE[$occ]),"yyyy-MM-dd")) then
       &lt;ns2:paymentDate?&gt;{ insertDate(data($parm/NF_TRANOS_PAYMENTDATE[$occ]),"yyyy-MM-dd")}&lt;/ns2:paymentDate&gt;
       else()
}
       &lt;ns2:paymentAmount?&gt;{data($parm/NF_TRANOS_PAYMENTAMOUNT[$occ])}&lt;/ns2:paymentAmount&gt;
      &lt;ns2:paymentDescription?&gt;{data($parm/NF_TRANOS_PAYMENTDESCRIPTI[$occ])}&lt;/ns2:paymentDescription&gt;
    &lt;/ns2:TransferOrderSchedule&gt;
    else()
  }
&lt;/ns0:newList&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  {
   if (data($parm/NF_CTRL_OPTION[1]) = 0) then
      getElementsForOldList($parm)
   else &lt;ns0:oldList /&gt;
   }
  {getElementsForAnchorList($parm)}
  {getElementsForNewList($parm)}
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>