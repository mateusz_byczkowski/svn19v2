<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns8:header" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns8:invoke" ::)
(:: pragma bea:global-element-return element="ns4:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:productstree.entities.be.dcl";
declare namespace ns4 = "";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns0 = "urn:accounts.entities.be.dcl";
declare namespace ns9 = "urn:baseauxentities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/getSimulationForTranAccount/getSimulationForTranAccount/";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:be.services.dcl";

declare function xf:getSimulationForTranAccount($header1 as element(ns8:header),
    $invoke1 as element(ns8:invoke))
    as element(ns4:FML32) {
        &lt;ns4:FML32&gt;
            &lt;ns4:TR_ID_OPER?&gt;{ data($header1/ns8:transHeader/ns8:transId) }&lt;/ns4:TR_ID_OPER&gt;
            &lt;ns4:TR_DATA_OPER?&gt;
                {
                    let $transactionDate  := ($invoke1/ns8:transaction/ns7:Transaction/ns7:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			&lt;/ns4:TR_DATA_OPER&gt;
            
            &lt;ns4:TR_ODDZ_KASY?&gt;{ 
            xs:short( data($invoke1/ns8:branchCode/ns2:BranchCode/ns2:branchCode) ) 
            }&lt;/ns4:TR_ODDZ_KASY&gt;
            
            &lt;ns4:TR_KASA?&gt;{ 
            xs:short( data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID) ) 
            }&lt;/ns4:TR_KASA&gt;
            
            &lt;ns4:TR_KASJER?&gt;{ 
            xs:short( data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID) ) 
            }&lt;/ns4:TR_KASJER&gt;
            
            &lt;ns4:TR_UZYTKOWNIK?&gt;{ 
            fn:concat("SKP:",data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:user/ns5:User/ns5:userID))
            }&lt;/ns4:TR_UZYTKOWNIK&gt;
            
            &lt;ns4:TR_MSG_ID?&gt;{ 
            data($header1/ns8:msgHeader/ns8:msgId) 
            }&lt;/ns4:TR_MSG_ID&gt;
            
            &lt;ns4:TR_FLAGA_AKCEPT&gt;
            O
            &lt;/ns4:TR_FLAGA_AKCEPT&gt;
            
            &lt;ns4:TR_TYP_KOM?&gt;{ 
            xs:short( data($invoke1/ns8:transaction/ns7:Transaction/ns7:csrMessageType/ns6:CsrMessageType/ns6:csrMessageType) ) 
            }&lt;/ns4:TR_TYP_KOM&gt;
            
            &lt;ns4:TR_KANAL&gt;
            0
            &lt;/ns4:TR_KANAL&gt;
            
            &lt;ns4:TR_RACH_NAD?&gt;{ 
            data($invoke1/ns8:account/ns0:Account/ns0:accountNumber) 
            }&lt;/ns4:TR_RACH_NAD&gt;
            &lt;ns4:TR_DATA_PRACY&gt;
                {
                    let $transactionDate  := ($invoke1/ns8:transaction/ns7:Transaction/ns7:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate ), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			&lt;/ns4:TR_DATA_PRACY&gt;
            &lt;ns4:TR_DATA_WALUTY?&gt;
                {
                    let $transactionDate  := ($invoke1/ns8:transaction/ns7:Transaction/ns7:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate ), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			&lt;/ns4:TR_DATA_WALUTY&gt;
            &lt;ns4:TR_CZAS_OPER?&gt;
                {
                    let $transactionTime  := ($header1/ns8:msgHeader/ns8:timestamp)  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
			&lt;/ns4:TR_CZAS_OPER&gt;
            &lt;ns4:TR_AKCEPTANT_SKP?&gt;{
             data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:user/ns5:User/ns5:userID) 
             }&lt;/ns4:TR_AKCEPTANT_SKP&gt;
             
            &lt;ns4:TR_UZYTKOWNIK_SKP?&gt;{ 
            data($header1/ns8:msgHeader/ns8:userId) 
            }&lt;/ns4:TR_UZYTKOWNIK_SKP&gt;
            
        &lt;/ns4:FML32&gt;
};

declare variable $header1 as element(ns8:header) external;
declare variable $invoke1 as element(ns8:invoke) external;

&lt;soap-env:Body&gt;{
xf:getSimulationForTranAccount($header1,
    $invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>