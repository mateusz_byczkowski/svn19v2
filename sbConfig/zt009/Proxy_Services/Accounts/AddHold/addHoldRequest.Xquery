<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accountdict.dictionaries.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function chkUnitId($unitId as xs:anyType) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function boolean2SourceValue ($parm as xs:anyType,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm  = "1") then $trueval
       else $falseval
};

declare function defaultIfEmpty($parm as xs:anyType,$defaultValue as xs:string) as xs:string{
    if ($parm)
       then insertDate($parm)
       else $defaultValue
};

declare function defaultIfFlagZero($parm as xs:anyType,$defaultValue as xs:string, $flag as xs:anyType) as xs:string{
    if ($parm)
       then 
          if ($flag ="0") 
              then $defaultValue
          else
            insertDate($parm)
       else $defaultValue
};

declare function checkBalanceFlag($amount as xs:anyType,$flag as xs:anyType) as xs:anyType{
    if ($amount)
       then 
          if ($flag= "Y")
              then "99999999999.99"
          else
            $amount
       else ()
};

declare function defaultIfEmpty2($parm as xs:anyType,$defaultValue as xs:string) as xs:string{
    if ($parm)
       then $parm
       else $defaultValue
};

declare function insertDate($value as xs:anyType) as xs:anyType{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "0001-01-01") and not ($value = "01-01-0001"))
            then chgDate($value)
        else() 
      else()
      };

declare function chgDate($value as xs:anyType) as xs:anyType
{
    let $year := substring($value, 1, 4)
    let $month := substring($value, 6, 2)
    let $day := substring($value, 9, 2)
return
     concat($day,concat("-",concat($month, concat("-", $year))))
};


declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{

&lt;DC_ODDZIAL?&gt;{chkUnitId(data($parm/ns6:msgHeader/ns6:unitId))}&lt;/DC_ODDZIAL&gt;
,
&lt;DC_UZYTKOWNIK?&gt;{data($parm/ns6:msgHeader/ns6:userId)}&lt;/DC_UZYTKOWNIK&gt;
,
&lt;DC_TRN_ID?&gt;{data($parm/ns6:transHeader/ns6:transId)}&lt;/DC_TRN_ID&gt;
,
&lt;DC_NUMER_AKCJI_NA_BLOKADZIE?&gt;{data($parm/ns6:transHeader/ns6:transId)}&lt;/DC_NUMER_AKCJI_NA_BLOKADZIE&gt;

};
declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{

&lt;DC_KWOTA?&gt;{checkBalanceFlag(data($parm/ns6:hold/ns1:Hold/ns1:holdAmount),boolean2SourceValue(data($parm/ns6:hold/ns1:Hold/ns1:holdBalanceFlag),"Y","N"))}&lt;/DC_KWOTA&gt;
,
&lt;DC_OPIS_1?&gt;{defaultIfEmpty2(data($parm/ns6:hold/ns1:Hold/ns1:holdDescription),"")}&lt;/DC_OPIS_1&gt;
,
&lt;DC_DATA_WAZNOSCI?&gt;{defaultIfFlagZero(data($parm/ns6:hold/ns1:Hold/ns1:holdExpirationDate),"31-12-2049",boolean2SourceValue(data($parm/ns6:hold/ns1:Hold/ns1:holdExpirationDateFlag),"0","1"))}&lt;/DC_DATA_WAZNOSCI&gt;
,
&lt;DC_NUMER_SERYJNY_FAKTURY?&gt;{defaultIfEmpty2(data($parm/ns6:hold/ns1:Hold/ns1:firstChecqueNumber),"")}&lt;/DC_NUMER_SERYJNY_FAKTURY&gt;
,
&lt;DC_NUMER_CZEKU?&gt;{defaultIfEmpty2(data($parm/ns6:hold/ns1:Hold/ns1:lastChecqueNumber),"")}&lt;/DC_NUMER_CZEKU&gt;
,
&lt;DC_FLAGA_BLOKOWANIA_SALDA?&gt;{boolean2SourceValue(data($parm/ns6:hold/ns1:Hold/ns1:holdBalanceFlag),"Y","N")}&lt;/DC_FLAGA_BLOKOWANIA_SALDA&gt;
,
&lt;DC_FLAGA_DATY_WYGASNIECIA?&gt;{boolean2SourceValue(data($parm/ns6:hold/ns1:Hold/ns1:holdExpirationDateFlag),"0","1")}&lt;/DC_FLAGA_DATY_WYGASNIECIA&gt;
,
&lt;DC_DATA_CZEKU_FAKTURY?&gt;{defaultIfEmpty(data($parm/ns6:hold/ns1:Hold/ns1:checqueRestrictDate),"")}&lt;/DC_DATA_CZEKU_FAKTURY&gt;
,
&lt;DC_UZYTKOWNIK_MODYFIKUJACY?&gt;{data($parm/ns6:hold/ns1:Hold/ns1:holdCreateUserId)}&lt;/DC_UZYTKOWNIK_MODYFIKUJACY&gt;
,
&lt;DC_UZYTKOWNIK_AUTORYZUJACY?&gt;{data($parm/ns6:hold/ns1:Hold/ns1:holdAcceptUserId)}&lt;/DC_UZYTKOWNIK_AUTORYZUJACY&gt;
,
&lt;DC_NUMER_BLOKADY?&gt;{data($parm/ns6:hold/ns1:Hold/ns1:holdNumber)}&lt;/DC_NUMER_BLOKADY&gt;
,
&lt;DC_OPIS_2?&gt;{defaultIfEmpty2(data($parm/ns6:hold/ns1:Hold/ns1:holdDescription2),"")}&lt;/DC_OPIS_2&gt;
,
&lt;DC_TYP_BLOKADY?&gt;{data($parm/ns6:hold/ns1:Hold/ns1:holdType/ns3:HoldType/ns3:holdType)}&lt;/DC_TYP_BLOKADY&gt;
,
&lt;DC_WALUTA?&gt;{defaultIfEmpty2(data($parm/ns6:hold/ns1:Hold/ns1:holdCurrencyCode/ns3:CurrencyCode/ns3:currencyCode),"PLN")}&lt;/DC_WALUTA&gt;
,
&lt;DC_TYP_ZMIANY?&gt;1&lt;/DC_TYP_ZMIANY&gt;
,
&lt;DC_FLAGA_AUTORYZACJI&gt;O&lt;/DC_FLAGA_AUTORYZACJI&gt;
,
&lt;DC_NR_RACHUNKU?&gt;{data($parm/ns6:account/ns1:Account/ns1:accountNumber)}&lt;/DC_NR_RACHUNKU&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvoke($body/ns6:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>