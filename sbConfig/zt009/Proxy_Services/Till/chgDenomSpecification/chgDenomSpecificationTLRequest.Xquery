<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="chgDenomSpecification.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="chgDenomSpecification.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns0:invokeResponse" location="chgDenomSpecification.WSDL" ::)
(:: pragma bea:global-element-return element="ns6:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns2 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns6 = "http://bzwbk.com/nfe/transactionLog";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/chgDenomSpecification/chgDenomSpecificationTLRequest/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:entities.be.dcl";

declare function xf:chgDenomSpecificationTLRequest($invoke1 as element(ns0:invoke),
    $header1 as element(ns0:header),
    $invokeResponse1 as element(ns0:invokeResponse),
         $SaveTransferResponse as element(FML32) ?)

    as element(ns6:transactionLogEntry) {
        &lt;ns6:transactionLogEntry&gt;
            {
                for $businessTransactionType in $invoke1/ns0:transaction/ns7:Transaction/ns7:businessTransactionType/ns5:BusinessTransactionType/ns5:businessTransactionType
                return
                    &lt;businessTransactionType&gt;{ data($businessTransactionType) }&lt;/businessTransactionType&gt;
            }
            {
                for $cashTransactionBasketID in $invoke1/ns0:transaction/ns7:Transaction/ns7:cashTransactionBasketID
                return
                    &lt;cashTransactionBasketID&gt;{ data($cashTransactionBasketID) }&lt;/cashTransactionBasketID&gt;
            }
            {
                for $csrMessageType in $invoke1/ns0:transaction/ns7:Transaction/ns7:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType
                return
                    &lt;csrMessageType&gt;{ data($csrMessageType) }&lt;/csrMessageType&gt;
            }
            {
                for $dtTransactionType in $invoke1/ns0:transaction/ns7:Transaction/ns7:dtTransactionType/ns5:DtTransactionType/ns5:dtTransactionType
                return
                    &lt;dtTransactionType&gt;{ data($dtTransactionType) }&lt;/dtTransactionType&gt;
            }
            &lt;executor&gt;
                {
                    for $branchCode in $invoke1/ns0:branchCode/ns4:BranchCode/ns4:branchCode
                    return
                        &lt;branchNumber&gt;{ xs:int( data($branchCode) ) }&lt;/branchNumber&gt;
                }
                {
                    for $userID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns8:User/ns8:userID
                    return
                        &lt;executorID&gt;{ data($userID) }&lt;/executorID&gt;
                }
                {
                    for $userFirstName in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns8:User/ns8:userFirstName
                    return
                        &lt;firstName&gt;{ data($userFirstName) }&lt;/firstName&gt;
                }
                {
                    for $userLastName in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns8:User/ns8:userLastName
                    return
                        &lt;lastName&gt;{ data($userLastName) }&lt;/lastName&gt;
                }
                {
                    for $tellerID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID
                    return
                        &lt;tellerID&gt;{ data($tellerID) }&lt;/tellerID&gt;
                }
                {
                    for $tillID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID
                    return
                        &lt;tillNumber&gt;{ data($tillID) }&lt;/tillNumber&gt;
                }
                &lt;userID&gt;{ data($SaveTransferResponse/TR_UZYTKOWNIK) }&lt;/userID&gt;

(:                {   :)
(:                    for $userID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns8:User/ns8:userID  :)
(:                    return   :)
(:                        &lt;userID&gt;{ data($userID) }&lt;/userID&gt;   :)
(:                }  :)
            &lt;/executor&gt;
            &lt;hlbsName&gt;chgDenomSpecification&lt;/hlbsName&gt;
            {
                for $putDownDate in $invoke1/ns0:transaction/ns7:Transaction/ns7:putDownDate
                return
                    &lt;putDownDate&gt;{ xs:date( data($putDownDate) ) }&lt;/putDownDate&gt;
            }
            &lt;timestamp&gt;{ data($header1/ns0:msgHeader/ns0:timestamp) }&lt;/timestamp&gt;
            {
                for $CurrencyCash in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:currencyCashList/ns1:CurrencyCash
                return
                    &lt;tlCurrencyCashList&gt;
                        {
                            for $currencyCode in $CurrencyCash/ns1:currency/ns4:CurrencyCode/ns4:currencyCode
                            return
                                &lt;currency&gt;{ data($currencyCode) }&lt;/currency&gt;
                        }
                        {
                            for $DenominationSpecification in $CurrencyCash/ns1:denominationSpecificationList/ns1:DenominationSpecification
                            return
                                &lt;tlDenominationSpecList?&gt;
                                    {
                                        let $denominationID := $DenominationSpecification/ns1:denomination/ns2:DenominationDefinition/ns2:denominationID
                                        let $itemsNumber := $DenominationSpecification/ns1:itemsNumber
                                        return
        									if (data($itemsNumber) != 0) then
                                        	(
                                        		&lt;denomination&gt;{ data($denominationID) }&lt;/denomination&gt;,
                                            	&lt;itemsNumber&gt;{ data($itemsNumber) }&lt;/itemsNumber&gt;
                                            )
                                            else
                                            ()
                                    }
                                &lt;/tlDenominationSpecList&gt;
                        }
                    &lt;/tlCurrencyCashList&gt;
            }
            {
                for $transactionDate in $invoke1/ns0:transaction/ns7:Transaction/ns7:transactionDate
                return
                    &lt;transactionDate&gt;{ xs:date(data($transactionDate) ) }&lt;/transactionDate&gt;
            }
            &lt;transactionID&gt;{ data($header1/ns0:transHeader/ns0:transId) }&lt;/transactionID&gt;
            {
                for $transactionStatus in $invokeResponse1/ns0:transactionOut/ns7:Transaction/ns7:transactionStatus/ns4:TransactionStatus/ns4:transactionStatus
                return
                    &lt;transactionStatus&gt;{ data($transactionStatus) }&lt;/transactionStatus&gt;
            }
        &lt;/ns6:transactionLogEntry&gt;
};

declare variable $invoke1 as element(ns0:invoke) external;
declare variable $header1 as element(ns0:header) external;
declare variable $invokeResponse1 as element(ns0:invokeResponse) external;
declare variable $SaveTransferResponse  as element(FML32) ? external;

&lt;soap-env:Body&gt;{
xf:chgDenomSpecificationTLRequest($invoke1,
    $header1,
    $invokeResponse1,
    $SaveTransferResponse)

}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>