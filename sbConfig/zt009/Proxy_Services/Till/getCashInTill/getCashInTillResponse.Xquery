<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Zmiana pola FML z NF_DENOMS_ITEMSNUMBER na NF_COLLAT_NUMBEROFUNITS.Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 : @author  Kacper Pawlaczyk
 : @version 1.5
 : @since   2010-08-03
 :
 : wersja WSDLa: 12-11-2009 11:07:43
 :
 : $Proxy Services/Till/getCashInTill/getCashInTillResponse.xq$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getCashInTill/getCashInTillResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:cash.operations.entities.be.dcl";
declare namespace ns3 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns5 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns6 = "urn:entities.be.dcl";
declare namespace ns7 = "urn:be.services.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getCashInTillResponse($fML321 as element(ns0:FML32))
    as element(ns7:invokeResponse)
{
    &lt;ns7:invokeResponse&gt;
        &lt;ns7:userTxnSessionOut&gt;
            &lt;ns5:UserTxnSession?&gt;
            
            	{
            		let $cashValNeeded := data($fML321/ns0:NF_USERTS_CASHVALIDATIONNE)
            		return
            			if ($cashValNeeded) then
			            	&lt;ns5:cashValidationNeeded&gt;{
								xs:boolean($cashValNeeded)
							}&lt;/ns5:cashValidationNeeded&gt;
            			else
							()
            	}

                &lt;ns5:currencyCashList&gt;{
                
					(:
					 : lista walut - distinct
					 :)
               		let $currencyList := fn:distinct-values($fML321/ns0:NF_CURREC_CURRENCYCODE)

					(:
					 : dla każdej różnej waluty
					 :)               		
					for $currencyCode in $currencyList
					return                
                        &lt;ns2:CurrencyCash&gt;
                           (:
							:
							: licznosc $fML321/ns0:NF_CURRCA_AMOUNT == licznosc $fML321/ns0:NF_CURREC_CURRENCYCODE
							:
							: Ponieważ kwoty dla dwóch różnych walut mogą się powtarzać,
							: szukamy pierwszego indeksu na liście wszystkich dla każdej z walut.
							: Znaleziony indeks jest również indeksem odpowiadającej walucie kwoty.
							:
							: Przykładowo:
							: NF_CURRCA_CURRENCYCODE    PLN
							: NF_CURRCA_CURRENCYCODE    PLN
							: NF_CURRCA_CURRENCYCODE    USD
							: NF_CURRCA_AMOUNT          1000.00 (dla PLN)
							: NF_CURRCA_AMOUNT          1000.00 (dla PLN)
							: NF_CURRCA_AMOUNT          1000.00 (dla USD)
							:
							: wowczas index-of dla $currencyCode == PLN zwroci (1,2) (wybieramy pierwsze wystapnienie - [1])
							:         index-of dla $currencyCode == USD zwroci (3)   (analogicznie)
							:
							: dodatkowo: brak tagu &lt;amount&gt; w przypadku, gdy backend zwroci &lt;NF_CURRCA_AMOUNT/&gt;
							:)
                        	{
                        		let $amountIndex := fn:index-of($fML321/ns0:NF_CURREC_CURRENCYCODE, $currencyCode)[1]
								return
									let $amount := data($fML321/ns0:NF_CURRCA_AMOUNT[xs:int($amountIndex)])
									return
										if ($amount) then
											&lt;ns2:amount&gt;{
												$amount
											}&lt;/ns2:amount&gt;
										else
											()
                        	}

                        	&lt;ns2:denominationSpecificationList&gt;{
								(:
								 : wszystkie wystąpienia NF_DENOMS_ITEMSNUMBER oraz NF_DENOMD_DENOMINATIONID
								 : na liscie wszystkich walut, ktore odpowiadaja
								 : obecnie rozważanej walucie (distinct)
								 :
								 : Przykladowo:
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE USD
								 : NF_CURRCA_CURRENCYCODE USD
								 :

								 : Pierwsze przejście:
								 :      NF_DENOMS_ITEMSNUMBER, NF_DENOMD_DENOMINATIONID z 1., 2. oraz 3. wystąpienia
								 : Drugie przejście:
								 :		NF_DENOMS_ITEMSNUMBER, NF_DENOMD_DENOMINATIONID z 4. oraz 5. wystąpienia
								 :)
								for $i in 1 to count($fML321/ns0:NF_CURREC_CURRENCYCODE)
								where (data($fML321/ns0:NF_CURREC_CURRENCYCODE[$i]) eq $currencyCode)
								return
	                                &lt;ns2:DenominationSpecification&gt;
	                                
                                		&lt;ns2:itemsNumber&gt;{
											data($fML321/ns0:NF_COLLAT_NUMBEROFUNITS[$i])
										}&lt;/ns2:itemsNumber&gt;
										
	                                    &lt;ns2:denomination&gt;
	                                        &lt;ns1:DenominationDefinition&gt;
                                        		&lt;ns1:denominationID&gt;{
													data($fML321/ns0:NF_DENOMD_DENOMINATIONID[$i])
												}&lt;/ns1:denominationID&gt;
	                                        &lt;/ns1:DenominationDefinition&gt;
	                                    &lt;/ns2:denomination&gt;
	                                    
	                                &lt;/ns2:DenominationSpecification&gt;
                        	}&lt;/ns2:denominationSpecificationList&gt;
                        	
                            &lt;ns2:currency&gt;
                                &lt;ns4:CurrencyCode&gt;
                                    &lt;ns4:currencyCode&gt;{
										data($currencyCode)
									}&lt;/ns4:currencyCode&gt;
                                &lt;/ns4:CurrencyCode&gt;
                            &lt;/ns2:currency&gt;
                    &lt;/ns2:CurrencyCash&gt;
                }&lt;/ns5:currencyCashList&gt;
                
                &lt;ns5:currencyReconciliationList?&gt;{
                	for $i in 1 to count($fML321/ns0:NF_CURREC_CURRENCYCODE2)
                	return
                        &lt;ns2:CurrencyReconciliation&gt;
                        
                            &lt;ns2:negativeReconciliationCounter&gt;{
								data($fML321/ns0:NF_CURRRE_NEGATIVERECONCIL[$i])
							}&lt;/ns2:negativeReconciliationCounter&gt;
							
                            &lt;ns2:currency&gt;
                                &lt;ns4:CurrencyCode&gt;
                                    &lt;ns4:currencyCode&gt;{
										data($fML321/ns0:NF_CURREC_CURRENCYCODE2[$i])
									}&lt;/ns4:currencyCode&gt;
                                &lt;/ns4:CurrencyCode&gt;
                            &lt;/ns2:currency&gt;
                            
                            &lt;ns2:reconciliationStatus&gt;
                                &lt;ns1:CurrencyReconcStatus&gt;
                                    &lt;ns1:reconParameterRead&gt;{
										data($fML321/ns0:NF_CURRRS_RECONPARAMETERRE[$i])
									}&lt;/ns1:reconParameterRead&gt;
									
                                    &lt;ns1:internalRecMKSStatusRead&gt;{
										data($fML321/ns0:NF_CURRRS_INTRECMKSSTAREA[$i])
									}&lt;/ns1:internalRecMKSStatusRead&gt;
                                &lt;/ns1:CurrencyReconcStatus&gt;
                            &lt;/ns2:reconciliationStatus&gt;
                        &lt;/ns2:CurrencyReconciliation&gt;	                            
                }&lt;/ns5:currencyReconciliationList&gt;
                
                (:
                 : 1 --&gt; O (sesja otwarta)
                 : 0 --&gt; C (sesja zamknięta)
                 :)
                {
					let $sessionStatus := data($fML321/ns0:NF_USETSS_USERTXNSESSIONST)
					return
						if ($sessionStatus) then
							&lt;ns5:sessionStatus&gt;
			                    &lt;ns3:UserTxnSessionStatus&gt;
									&lt;ns3:userTxnSessionStatus&gt;{
										if ($sessionStatus eq '1') then
											'O'
										else
											'C'
									}&lt;/ns3:userTxnSessionStatus&gt;
			                    &lt;/ns3:UserTxnSessionStatus&gt;
							&lt;/ns5:sessionStatus&gt;
						else
							()
				}

                &lt;ns5:user?&gt;
                    &lt;ns6:User?&gt;
                        &lt;ns6:userLastName?&gt;{
							data($fML321/ns0:NF_USER_USERLASTNAME)
						}&lt;/ns6:userLastName&gt;
						
                        &lt;ns6:userID?&gt;{
							data($fML321/ns0:NF_USER_USERID)
						}&lt;/ns6:userID&gt;
						
                        &lt;ns6:userFirstName?&gt;{
							data($fML321/ns0:NF_USER_USERFIRSTNAME)
						}&lt;/ns6:userFirstName&gt;
                    &lt;/ns6:User&gt;
                &lt;/ns5:user&gt;
                
                &lt;ns5:till?&gt;
                    &lt;ns5:Till?&gt;
                        &lt;ns5:tillID?&gt;{
							data($fML321/ns0:NF_TILL_TILLID)
						}&lt;/ns5:tillID&gt;
                    &lt;/ns5:Till&gt;
                &lt;/ns5:till&gt;
                
                &lt;ns5:teller?&gt;
                    &lt;ns5:Teller?&gt;
                        &lt;ns5:tellerID?&gt;{
							data($fML321/ns0:NF_TELLER_TELLERID)
						}&lt;/ns5:tellerID&gt;
                    &lt;/ns5:Teller&gt;
                &lt;/ns5:teller&gt;
                
            &lt;/ns5:UserTxnSession&gt;
        &lt;/ns7:userTxnSessionOut&gt;
    &lt;/ns7:invokeResponse&gt;
};

&lt;soap-env:Body&gt;{
	xf:getCashInTillResponse($fML321)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>