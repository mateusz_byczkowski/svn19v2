<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2009-12-17
 :
 : wersja WSDLa: 12-11-2009 11:07:43
 :
 : $Proxy Service/Till/addInternalCashTransport/addInternalCashTransportResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/addInternalCashTransport/addInternalCashTransportResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:internaltransports.operations.entities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace ns3 = "urn:internaltransportsdict.operationsdictionary.dictionaries.be.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:addInternalCashTransportResponse($fML321 as element(ns0:FML32))
    as element(ns2:invokeResponse)
{
    &lt;ns2:invokeResponse&gt;
        &lt;ns2:internalCashTransportOut&gt;
            &lt;ns1:InternalCashTransport?&gt;
                &lt;ns1:transportID?&gt;{
					data($fML321/ns0:NF_INTECT_TRANSPORTID)
				}&lt;/ns1:transportID&gt;
                &lt;ns1:transportStatus?&gt;
                    &lt;ns3:InternalCashTransportStatus?&gt;
                        &lt;ns3:internalCashTransportStatus?&gt;{
							data($fML321/ns0:NF_INTCTS_INTERNALCASHTRAN)
						}&lt;/ns3:internalCashTransportStatus&gt;
                    &lt;/ns3:InternalCashTransportStatus&gt;
                &lt;/ns1:transportStatus&gt;
                &lt;ns1:transportExtStatus?&gt;
                    &lt;ns3:InternalCashTranspExtStatus?&gt;
                        &lt;ns3:internalCashTranspExtStatus?&gt;{
							data($fML321/ns0:NF_INCTES_INTERNALCASHTRAN)
						}&lt;/ns3:internalCashTranspExtStatus&gt;
                    &lt;/ns3:InternalCashTranspExtStatus&gt;
                &lt;/ns1:transportExtStatus&gt;
            &lt;/ns1:InternalCashTransport&gt;
        &lt;/ns2:internalCashTransportOut&gt;
    &lt;/ns2:invokeResponse&gt;
};

&lt;soap-env:Body&gt;{
	xf:addInternalCashTransportResponse($fML321)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>