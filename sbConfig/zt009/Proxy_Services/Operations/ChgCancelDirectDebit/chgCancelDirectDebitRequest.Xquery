<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:directdebit.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:anyType) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};
declare function xf:getFields($parm as element(ns5:invoke), $msghead as element(ns5:msgHeader), $tranhead as element(ns5:transHeader)) as element(fml:FML32)
{
  let $msgId:= $msghead/ns5:msgId
  let $companyId:= $msghead/ns5:companyId
  let $userId := $msghead/ns5:userId
  let $appId:= $msghead/ns5:appId
  let $unitId := $msghead/ns5:unitId
  let $timestamp:= $msghead/ns5:timestamp

  let $transId:=$tranhead/ns5:transId

  return
  &lt;fml:FML32&gt;
     &lt;DC_TRN_ID?&gt;{data($transId)}&lt;/DC_TRN_ID&gt;
     &lt;DC_UZYTKOWNIK?&gt;{data($userId)}&lt;/DC_UZYTKOWNIK&gt;
     &lt;DC_ODDZIAL?&gt;{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL&gt;
     &lt;NF_DIRECD_DIRECTDEBITID?&gt;{data($parm/ns5:directDebit/ns2:DirectDebit/ns2:directDebitId)}&lt;/NF_DIRECD_DIRECTDEBITID&gt;
     &lt;NF_ACCOUN_ACCOUNTNUMBER?&gt;{data($parm/ns5:directDebit/ns2:DirectDebit/ns2:tranAccount/ns1:TranAccount/ns1:account/ns1:Account/ns1:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER&gt;
  &lt;/fml:FML32&gt;

};


&lt;soap:Body&gt;
     { xf:getFields($body/ns5:invoke, $header/ns5:header/ns5:msgHeader, $header/ns5:header/ns5:transHeader)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>