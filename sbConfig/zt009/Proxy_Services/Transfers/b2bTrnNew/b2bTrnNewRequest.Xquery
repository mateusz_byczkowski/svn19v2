<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "http://bzwbk.com/services/ceke/messages/";

declare function xf:map_b2bTrnNewRequest($req as element(urn:TrnNewRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				<fml:E_OWNER_ID?>{ data($req/urn:OwnerId) }</fml:E_OWNER_ID>
			}
			{
				<fml:E_EXT_TIME_STAMP?>{ data($req/urn:ExternalTimeStamp) }</fml:E_EXT_TIME_STAMP>
			}
			{
				<fml:E_EXT_ENC_DATA?>{ data($req/urn:ExternalSignature) }</fml:E_EXT_ENC_DATA>
			}
			{
				<fml:E_VARIANT?>{ data($req/urn:KeyVariant) }</fml:E_VARIANT>
			}
			{
				let $USER_ID := $req/urn:UserId
				let $TOKEN_SIGNATURE := $req/urn:TokenSignature
				let $TOKEN_DATETIME := $req/urn:TokenDatetime

				for $it at $p in $req/urn:UserId
				return
				(
					<fml:E_LOGIN_ID?>{ data($USER_ID[$p]) }</fml:E_LOGIN_ID>,
					<fml:E_SECURITY_CHECK?>{ data($TOKEN_SIGNATURE[$p]) }</fml:E_SECURITY_CHECK>,
					<fml:E_SECURITY_TIME?>{ data($TOKEN_DATETIME[$p]) }</fml:E_SECURITY_TIME>
				)
			}
			{

				let $IS_BIC := $req/urn:TrnData/urn:SwiftData/urn:BIC
				let $TRN_DATA := $req/urn:TrnData

				for $it at $p in $req/urn:TrnData
				return
				(
					<fml:B_DL_NR_RACH?>{ data($TRN_DATA[$p]/urn:SrcAccountNo) }</fml:B_DL_NR_RACH>,
					<fml:E_SRC_AMOUNT?>{ data($TRN_DATA[$p]/urn:SrcAmount) }</fml:E_SRC_AMOUNT>,
					<fml:B_DL_NR_RACH?>{ data($TRN_DATA[$p]/urn:DstAccountNo) }</fml:B_DL_NR_RACH>,
					<fml:E_DST_AMOUNT?>{ data($TRN_DATA[$p]/urn:DstAmount) }</fml:E_DST_AMOUNT>,
					<fml:B_WALUTA?>{ data($TRN_DATA[$p]/urn:DstCurrency) }</fml:B_WALUTA>,
					<fml:E_CUST_NAME?>{ data($TRN_DATA[$p]/urn:DstName) }</fml:E_CUST_NAME>,
					<fml:E_CUST_CITY?>{ data($TRN_DATA[$p]/urn:DstCity) }</fml:E_CUST_CITY>,
					<fml:E_CUST_ZIPCODE?>{ data($TRN_DATA[$p]/urn:DstZipCode) }</fml:E_CUST_ZIPCODE>,
					<fml:E_CUST_STREET?>{ data($TRN_DATA[$p]/urn:DstStreet) }</fml:E_CUST_STREET>,
					<fml:E_TRN_TITLE?>{ data($TRN_DATA[$p]/urn:Title) }</fml:E_TRN_TITLE>,

					if (data($TRN_DATA[$p]/urn:TrnType) = "0") then (: Elixir :)
					(
						<fml:E_TRN_TYPE>30</fml:E_TRN_TYPE>,
						<fml:E_TRANSFER_TYPE>1</fml:E_TRANSFER_TYPE>,

						if($IS_BIC) then (
							<fml:E_SWIFT_BIC></fml:E_SWIFT_BIC>,
							<fml:B_LP>0</fml:B_LP>,
							<fml:E_SWIFT_BRANCH_CODE></fml:E_SWIFT_BRANCH_CODE>,
							<fml:E_SWIFT_BRANCH_NAME></fml:E_SWIFT_BRANCH_NAME>,
							<fml:E_SWIFT_BANK_NAME></fml:E_SWIFT_BANK_NAME>,
							<fml:E_SWIFT_ADDRESS></fml:E_SWIFT_ADDRESS>,
							<fml:E_SWIFT_TOWN></fml:E_SWIFT_TOWN>,
							<fml:E_SWIFT_COUNTRY></fml:E_SWIFT_COUNTRY>,
							<fml:E_SWIFT_COST>0</fml:E_SWIFT_COST>,
							<fml:E_SWIFT_TYPE>0</fml:E_SWIFT_TYPE>,
							<fml:E_SWIFT_PRIORITY>0</fml:E_SWIFT_PRIORITY>,
							<fml:E_SWIFT_SPOT>0</fml:E_SWIFT_SPOT>
						) else ()
					)
					else if(data($TRN_DATA[$p]/urn:TrnType) = "1") then (: SWIFT :)
					(
						<fml:E_TRN_TYPE>12</fml:E_TRN_TYPE>,
						<fml:E_TRANSFER_TYPE>4</fml:E_TRANSFER_TYPE>,

						<fml:E_SWIFT_BIC?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:BIC) }</fml:E_SWIFT_BIC>,
						<fml:B_LP?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftBankLP) }</fml:B_LP>,
						<fml:E_SWIFT_BRANCH_CODE?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftBankBranchCode) }</fml:E_SWIFT_BRANCH_CODE>,
						<fml:E_SWIFT_BRANCH_NAME?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftBankBranchName) }</fml:E_SWIFT_BRANCH_NAME>,
						<fml:E_SWIFT_BANK_NAME?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftBankName) }</fml:E_SWIFT_BANK_NAME>,
						<fml:E_SWIFT_ADDRESS?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftBankAddress) }</fml:E_SWIFT_ADDRESS>,
						<fml:E_SWIFT_TOWN?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftBankTown) }</fml:E_SWIFT_TOWN>,
						<fml:E_SWIFT_COUNTRY?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftBankCountry) }</fml:E_SWIFT_COUNTRY>,
						<fml:E_SWIFT_COST?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftCost) }</fml:E_SWIFT_COST>,
						<fml:E_SWIFT_TYPE?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftType) }</fml:E_SWIFT_TYPE>,
						<fml:E_SWIFT_PRIORITY?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftPriority) }</fml:E_SWIFT_PRIORITY>,
						<fml:E_SWIFT_SPOT?>{ data($TRN_DATA[$p]/urn:SwiftData/urn:SwiftSpot) }</fml:E_SWIFT_SPOT>
					)
					else if (data($TRN_DATA[$p]/urn:TrnType) = "2") then (: walutowy wewnetrzny :)
					(
						<fml:E_TRN_TYPE>8</fml:E_TRN_TYPE>,
						<fml:E_TRANSFER_TYPE>1</fml:E_TRANSFER_TYPE>,

						if($IS_BIC) then (
							<fml:E_SWIFT_BIC></fml:E_SWIFT_BIC>,
							<fml:B_LP>0</fml:B_LP>,
							<fml:E_SWIFT_BRANCH_CODE></fml:E_SWIFT_BRANCH_CODE>,
							<fml:E_SWIFT_BRANCH_NAME></fml:E_SWIFT_BRANCH_NAME>,
							<fml:E_SWIFT_BANK_NAME></fml:E_SWIFT_BANK_NAME>,
							<fml:E_SWIFT_ADDRESS></fml:E_SWIFT_ADDRESS>,
							<fml:E_SWIFT_TOWN></fml:E_SWIFT_TOWN>,
							<fml:E_SWIFT_COUNTRY></fml:E_SWIFT_COUNTRY>,
							<fml:E_SWIFT_COST>0</fml:E_SWIFT_COST>,
							<fml:E_SWIFT_TYPE>0</fml:E_SWIFT_TYPE>,
							<fml:E_SWIFT_PRIORITY>0</fml:E_SWIFT_PRIORITY>,
							<fml:E_SWIFT_SPOT>0</fml:E_SWIFT_SPOT>
						) else ()
					)  
					else if (data($TRN_DATA[$p]/urn:TrnType) = "3") then (: Sorbnet :)
					(
						<fml:E_TRN_TYPE>30</fml:E_TRN_TYPE>,
						<fml:E_TRANSFER_TYPE>6</fml:E_TRANSFER_TYPE>,

						if($IS_BIC) then (
							<fml:E_SWIFT_BIC></fml:E_SWIFT_BIC>,
							<fml:B_LP>0</fml:B_LP>,
							<fml:E_SWIFT_BRANCH_CODE></fml:E_SWIFT_BRANCH_CODE>,
							<fml:E_SWIFT_BRANCH_NAME></fml:E_SWIFT_BRANCH_NAME>,
							<fml:E_SWIFT_BANK_NAME></fml:E_SWIFT_BANK_NAME>,
							<fml:E_SWIFT_ADDRESS></fml:E_SWIFT_ADDRESS>,
							<fml:E_SWIFT_TOWN></fml:E_SWIFT_TOWN>,
							<fml:E_SWIFT_COUNTRY></fml:E_SWIFT_COUNTRY>,
							<fml:E_SWIFT_COST>0</fml:E_SWIFT_COST>,
							<fml:E_SWIFT_TYPE>0</fml:E_SWIFT_TYPE>,
							<fml:E_SWIFT_PRIORITY>0</fml:E_SWIFT_PRIORITY>,
							<fml:E_SWIFT_SPOT>0</fml:E_SWIFT_SPOT>
						) else ()
					)
					else ()
				)
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_b2bTrnNewRequest($body/urn:TrnNewRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>