<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>declare namespace m = "urn:be.services.dcl";
declare namespace m1 = "urn:cif.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/"; 
declare namespace fml="";

declare variable $body external;
declare variable $header external;

&lt;soap-env:Body&gt;
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:customer/m1:Customer
    return
    &lt;fml:FML32&gt;
      &lt;B_MSHEAD_MSGID?&gt;{data($reqh/m:msgHeader/m:msgId)}&lt;/B_MSHEAD_MSGID&gt;
      &lt;B_NR_DOK?&gt;{data($req/m1:customerNumber)}&lt;/B_NR_DOK&gt;
    &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>