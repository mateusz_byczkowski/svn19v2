<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:ceke.entities.be.dcl";
declare namespace urn4 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn5 = "urn:accounts.entities.be.dcl";
declare namespace urn6 = "urn:entities.be.dcl";

declare function xf:mapsaveCustomerCEKEResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		&lt;urn:invokeResponse&gt;
			&lt;urn:customerCEKEOut&gt;
				&lt;urn3:CustomerCEKE&gt;
				{
					&lt;urn3:nik?&gt;{ data($fml/fml:E_LOGIN_ID) }&lt;/urn3:nik&gt;
				}
                                                                {
					&lt;urn3:limitSMSPrepaid?&gt;{ data($fml/fml:E_CHANNEL_LIMIT) }&lt;/urn3:limitSMSPrepaid&gt;
				}
				{
					&lt;urn3:contractVersion?&gt;{data($fml/fml:E_CUST_REPORT_VERSION)}&lt;/urn3:contractVersion&gt;
				}
				&lt;/urn3:CustomerCEKE&gt;
			&lt;/urn:customerCEKEOut&gt;
		&lt;/urn:invokeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapsaveCustomerCEKEResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>