<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/GetCustomerFolders/RequestTransform2/";
declare namespace ns0 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns1 = "urn:entities.be.dcl";
declare namespace ns-1 = "urn:cif.entities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace ns3 = "urn:baseauxentities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";

declare function xf:RequestTransform2($invoke1 as element(ns2:invoke))
    as element(ns0:CRMGetCustCorpsRequest) {
        let $invoke := $invoke1
        return
            &lt;ns0:CRMGetCustCorpsRequest&gt;
                {
                    for $customerNumber in $invoke/ns2:customer/ns-1:Customer/ns-1:customerNumber
                    return
                        &lt;ns0:NumerKlienta&gt;{ data($customerNumber) }&lt;/ns0:NumerKlienta&gt;
                }
                {
                    for $value in $invoke/ns2:userCompanyTypeList/ns-1:CustomerFolders
                    return
                        &lt;ns0:IdSpolki&gt;{ xs:short( data($value/ns-1:companyID/ns4:CompanyType/ns4:companyType) ) }&lt;/ns0:IdSpolki&gt;
                }
            &lt;/ns0:CRMGetCustCorpsRequest&gt;
};

declare variable $invoke1 as element(ns2:invoke) external;

xf:RequestTransform2($invoke1)</con:xquery>
</con:xqueryEntry>