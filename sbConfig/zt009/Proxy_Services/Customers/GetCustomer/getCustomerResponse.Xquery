<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>T50787 kada</con:description>
  <con:xquery>declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:dictionaries.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace cer="http://bzwbk.com/services/cerber";
declare variable $body as element(soap:Body) external;
declare variable $cerberBody as element(cer:SearchUsersResponse) external;
declare variable $getCustCertBody as element(FML32) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)&gt;5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
};


declare function chgDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
 
    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)
 
    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 
};

declare function sourceValue2Boolean ($parm as xs:string?,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForCustomerEmploymentInfoList($parm as element(fml:FML32)) as element()
{

&lt;ns2:customerEmploymentInfoList&gt;
  {
    for $x at $occ in $parm/DC_WYKONYWANA_PRACA

    return
    &lt;ns2:CustomerEmploymentInfo&gt;
      &lt;ns2:employment&gt;{data($parm/DC_WYKONYWANA_PRACA[occ])}&lt;/ns2:employment&gt;
    &lt;/ns2:CustomerEmploymentInfo&gt;
  }
&lt;/ns2:customerEmploymentInfoList&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32), $cerRes as element(cer:SearchUsersResponse), $getCustCert as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse&gt;
  &lt;ns4:documentsOut&gt;
    &lt;ns2:Document&gt;
      &lt;ns2:documentNumber&gt;{data($parm/CI_SERIA_NR_DOK)}&lt;/ns2:documentNumber&gt;
      &lt;ns2:primaryDocument&gt;{sourceValue2Boolean (data($parm/CI_DOK_TOZSAMOSCI),"1")}&lt;/ns2:primaryDocument&gt;
    &lt;/ns2:Document&gt;
  &lt;/ns4:documentsOut&gt;
  &lt;ns4:customerOut&gt;
    &lt;ns2:Customer&gt;
      &lt;ns2:companyName&gt;{data($parm/CI_NAZWA_PELNA)}&lt;/ns2:companyName&gt;
      &lt;ns2:customerNumber&gt;{data($parm/DC_NUMER_KLIENTA)}&lt;/ns2:customerNumber&gt;
      { insertDate(data($parm/CI_DATA_WPROWADZENIA),"yyyy-MM-dd","ns2:dateCustomerOpened")}
      &lt;ns2:resident&gt;{sourceValue2Boolean (data($parm/DC_REZ_NIEREZ),"1")}&lt;/ns2:resident&gt;
      &lt;ns2:taxID&gt;{data($parm/DC_NIP)}&lt;/ns2:taxID&gt;
      &lt;ns2:shortName&gt;{data($parm/DC_NAZWA_SKROCONA)}&lt;/ns2:shortName&gt;
      &lt;ns2:certificate&gt;{sourceValue2Boolean (data($getCustCert/NF_CUSTOM_CERTIFICATE),"1")}&lt;/ns2:certificate&gt;
      &lt;ns2:phoneNo&gt;{data($parm/DC_NR_TELEFONU)}&lt;/ns2:phoneNo&gt;
      &lt;ns2:mobileNo&gt;{data($parm/DC_NR_TELEF_KOMORKOWEGO)}&lt;/ns2:mobileNo&gt;
      &lt;ns2:email&gt;{data($parm/DC_ADRES_E_MAIL)}&lt;/ns2:email&gt;
      &lt;ns2:portfolio&gt;{fn:concat(data($cerRes/cer:User/Name), ' ', data($cerRes/cer:User/Surname))}&lt;/ns2:portfolio&gt;
      &lt;ns2:fax&gt;{data($parm/DC_NUMER_FAKSU)}&lt;/ns2:fax&gt;
      &lt;ns2:debtorTaxID&gt;{data($getCustCert/NF_DIRECD_DEBTORTAXID)}&lt;/ns2:debtorTaxID&gt;
      {getElementsForCustomerEmploymentInfoList($parm)}

      { 
      if (data($parm/DC_TYP_KLIENTA)= 'P') then
      &lt;ns2:customerFirm&gt;
        &lt;ns2:CustomerFirm&gt;
          &lt;ns2:numberOfEmployees?&gt;{data($parm/DC_LICZBA_ZATRUDNIONYCH)}&lt;/ns2:numberOfEmployees&gt;
          &lt;ns2:regon?&gt;{data($parm/DC_NR_DOWODU_REGON)}&lt;/ns2:regon&gt;
          &lt;ns2:krs?&gt;{data($parm/CI_KRS)}&lt;/ns2:krs&gt;
          &lt;ns2:legalForm&gt;
            &lt;ns1:CustomerLegalForm&gt;
              &lt;ns1:customerLegalForm&gt;{data($parm/CI_FOP)}&lt;/ns1:customerLegalForm&gt;
            &lt;/ns1:CustomerLegalForm&gt;
          &lt;/ns2:legalForm&gt;
        &lt;/ns2:CustomerFirm&gt;
      &lt;/ns2:customerFirm&gt;
      else ()
      }
      &lt;ns2:customerFinancialInfo&gt;
        &lt;ns2:CustomerFinancialInfo&gt;
          &lt;ns2:taxPercent&gt;{data($parm/CI_PROCENT_PODATKU)}&lt;/ns2:taxPercent&gt;
        &lt;/ns2:CustomerFinancialInfo&gt;
      &lt;/ns2:customerFinancialInfo&gt;

       { 
        if (data($parm/DC_TYP_KLIENTA)= 'F') then
      &lt;ns2:customerPersonal&gt;
        &lt;ns2:CustomerPersonal&gt;
         &lt;ns2:customerNumber?&gt;{data($parm/DC_NUMER_KLIENTA)}&lt;/ns2:customerNumber&gt;
         &lt;ns2:workInBankStatus?&gt;{sourceValue2Boolean(data($parm/DC_PRACOWNIK_BANKU),"1") }&lt;/ns2:workInBankStatus&gt;
         &lt;ns2:birthPlace?&gt;{data($parm/DC_MIEJSCE_URODZENIA)}&lt;/ns2:birthPlace&gt;
         &lt;ns2:motherMaidenName?&gt;{data($parm/DC_NAZWISKO_PANIENSKIE_MATKI)}&lt;/ns2:motherMaidenName&gt;
         &lt;ns2:fatherName?&gt;{data($parm/DC_IMIE_OJCA)}&lt;/ns2:fatherName&gt;
         &lt;ns2:numberOfPersonsHousehold?&gt;{data($parm/DC_LICZBA_OS_WE_WSP_GOSP_D)}&lt;/ns2:numberOfPersonsHousehold&gt;
         &lt;ns2:workplace?&gt;{data($parm/CI_NAZWA_PRACODAWCY)}&lt;/ns2:workplace&gt;
         { insertDate(data($parm/DC_DATA_URODZENIA),"yyyy-MM-dd","ns2:dateOfBirth")}
         &lt;ns2:lastName&gt;{data($parm/DC_NAZWISKO)}&lt;/ns2:lastName&gt;
         &lt;ns2:secondName?&gt;{data($parm/DC_DRUGIE_IMIE)}&lt;/ns2:secondName&gt;
         &lt;ns2:pesel?&gt;{data($parm/DC_NR_PESEL)}&lt;/ns2:pesel&gt;
         &lt;ns2:firstName&gt;{data($parm/DC_IMIE)}&lt;/ns2:firstName&gt;
         { insertDate(data($parm/DC_DATA_SMIERCI),"yyyy-MM-dd","ns2:dateOfDeath")}
         &lt;ns2:identityCardNumber?&gt;{data($parm/DC_NR_DOWODU_REGON)}&lt;/ns2:identityCardNumber&gt;
         &lt;ns2:passportNumber?&gt;{data($parm/DC_NUMER_PASZPORTU)}&lt;/ns2:passportNumber&gt;
         &lt;ns2:citizenship&gt;
           &lt;ns1:CitizenshipCode&gt;
             (:&lt;ns1:citizenshipCode?&gt;{data($parm/CI_OBYWATELSTWO)}&lt;/ns1:citizenshipCode&gt;:)(:T48111:)
             &lt;ns1:isoCitizenshipCode?&gt;{data($parm/CI_OBYWATELSTWO)}&lt;/ns1:isoCitizenshipCode&gt; (:T48111:)
           &lt;/ns1:CitizenshipCode&gt;
         &lt;/ns2:citizenship&gt;
        &lt;/ns2:CustomerPersonal&gt;
      &lt;/ns2:customerPersonal&gt;
      else ()
      }

      &lt;ns2:branchOfOwnership&gt;
        &lt;ns1:BranchCode&gt;
          &lt;ns1:branchCode?&gt;{data($parm/DC_NUMER_ODDZIALU)}&lt;/ns1:branchCode&gt;
        &lt;/ns1:BranchCode&gt;
      &lt;/ns2:branchOfOwnership&gt;

      &lt;ns2:customerType&gt;
        &lt;ns1:CustomerType&gt;
          &lt;ns1:customerType?&gt;{data($parm/DC_TYP_KLIENTA)}&lt;/ns1:customerType&gt;
        &lt;/ns1:CustomerType&gt;
      &lt;/ns2:customerType&gt;

      &lt;ns2:customerSegment&gt;
        &lt;ns1:CustomerSegment&gt;
          &lt;ns1:customerSegment&gt;{data($parm/CI_KLASA_OBSLUGI)}&lt;/ns1:customerSegment&gt;
        &lt;/ns1:CustomerSegment&gt;
      &lt;/ns2:customerSegment&gt;

      &lt;ns2:processingApproval&gt;
        &lt;ns1:CustomerProcessingApproval&gt;
          &lt;ns1:customerProcessingApproval&gt;{data($parm/CI_UDOSTEP_GRUPA)}&lt;/ns1:customerProcessingApproval&gt;
        &lt;/ns1:CustomerProcessingApproval&gt;
      &lt;/ns2:processingApproval&gt;

      &lt;ns2:spw&gt;
        &lt;ns1:SpwCode&gt;
          &lt;ns1:spwCode?&gt;{data($parm/DC_SPW)}&lt;/ns1:spwCode&gt;
        &lt;/ns1:SpwCode&gt;
      &lt;/ns2:spw&gt;

      &lt;ns2:customerLegalInfo&gt;
        &lt;ns2:CustomerLegalInfo&gt;
          &lt;ns2:registryNo?&gt;{data($parm/CI_NUMER_W_REJESTRZE)}&lt;/ns2:registryNo&gt;
          &lt;ns2:registryName?&gt;{data($parm/CI_NAZWA_ORG_REJESTR)}&lt;/ns2:registryName&gt;
          &lt;ns2:initialCapital?&gt;{data($parm/CI_KAPITAL_ZALOZYCIELSKI)}&lt;/ns2:initialCapital&gt;
          &lt;ns2:initialCapitalPaid?&gt;{data($parm/CI_KAPITAL_ZALOZ_OPLACONY)}&lt;/ns2:initialCapitalPaid&gt;
        &lt;/ns2:CustomerLegalInfo&gt;
      &lt;/ns2:customerLegalInfo&gt;

    &lt;/ns2:Customer&gt;
  &lt;/ns4:customerOut&gt;
&lt;/ns4:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32, $cerberBody, $getCustCertBody)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>