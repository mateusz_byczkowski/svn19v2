<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbICBSChkDCCustomerRequest($req as element(m:bICBSChkDCCustomerRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL&gt;{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL&gt;
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE&gt;{ data($req/m:Imie) }&lt;/fml:DC_IMIE&gt;
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON&gt;{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON&gt;
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO&gt;{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO&gt;
					else ()
			}
			{
				if($req/m:DrugieImie)
					then &lt;fml:DC_DRUGIE_IMIE&gt;{ data($req/m:DrugieImie) }&lt;/fml:DC_DRUGIE_IMIE&gt;
					else ()
			}
			{
				if($req/m:NrPosesLokaluDanePodst)
					then &lt;fml:DC_NR_POSES_LOKALU_DANE_PODST&gt;{ data($req/m:NrPosesLokaluDanePodst) }&lt;/fml:DC_NR_POSES_LOKALU_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then &lt;fml:DC_MIASTO_DANE_PODST&gt;{ data($req/m:MiastoDanePodst) }&lt;/fml:DC_MIASTO_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then &lt;fml:DC_KOD_POCZTOWY_DANE_PODST&gt;{ data($req/m:KodPocztowyDanePodst) }&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:AdresOd)
					then &lt;fml:DC_ADRES_OD&gt;{ data($req/m:AdresOd) }&lt;/fml:DC_ADRES_OD&gt;
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then &lt;fml:DC_ULICA_DANE_PODST&gt;{ data($req/m:UlicaDanePodst) }&lt;/fml:DC_ULICA_DANE_PODST&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapbICBSChkDCCustomerRequest($body/m:bICBSChkDCCustomerRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>