<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:hlbsentities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

&lt;soap-env:Body&gt;
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:document/m1:CheckDocumentInMIGInput

    let $msgId := $reqh/m:msgHeader/m:msgId
    let $userId  := $reqh/m:msgHeader/m:userId
    let $unitId  := $reqh/m:msgHeader/m:unitId
    return

    &lt;fml:FML32&gt;
      &lt;fml:DC_MSHEAD_MSGID?&gt;{ data($msgId) }&lt;/fml:DC_MSHEAD_MSGID&gt;
      &lt;fml:DC_UZYTKOWNIK?&gt;{concat("SKP:", data($userId)) }&lt;/fml:DC_UZYTKOWNIK&gt;
      &lt;fml:DC_KOD_JEDNOSTKI?&gt;{ data($unitId) }&lt;/fml:DC_KOD_JEDNOSTKI&gt;
      &lt;fml:DC_KOD_APLIKACJI&gt;90&lt;/fml:DC_KOD_APLIKACJI&gt;

      {if($req/m1:documentID)
         then &lt;fml:CI_DOK_TOZSAMOSCI&gt;{ data($req/m1:documentID) }&lt;/fml:CI_DOK_TOZSAMOSCI&gt;
         else ()
      }
      {if($req/m1:documentNumber)
           then &lt;fml:CI_SERIA_NR_DOK&gt;{ data($req/m1:documentNumber) }&lt;/fml:CI_SERIA_NR_DOK&gt;
           else ()
      }
    &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>