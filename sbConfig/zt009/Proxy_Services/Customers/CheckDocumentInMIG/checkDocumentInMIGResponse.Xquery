<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "urn:be.services.dcl";
declare namespace m1 = "urn:hlbsentities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://bzwbk.com/services/mappings/";

declare variable $body as element(soap-env:Body) external;
declare variable $fml := $body/FML32;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))&gt;0)
    then true()
    else false()
};

&lt;soapenv:Body  xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"&gt; 
  &lt;urn:invokeResponse xmlns:urn="urn:be.services.dcl"&gt; 
    &lt;urn:documentStatus xmlns:urn1="urn:hlbsentities.be.dcl"&gt; 
      &lt;urn1:CheckDocumentInMIGOutput&gt;
        {if($fml/CI_STATUS and xf:isData($fml/CI_STATUS) and data($fml/CI_STATUS)="W")
             then &lt;urn1:status&gt;true&lt;/urn1:status&gt;
             else &lt;urn1:status&gt;false&lt;/urn1:status&gt;
        }
      &lt;/urn1:CheckDocumentInMIGOutput&gt;
    &lt;/urn:documentStatus&gt; 
  &lt;/urn:invokeResponse&gt; 
&lt;/soapenv:Body&gt;</con:xquery>
</con:xqueryEntry>