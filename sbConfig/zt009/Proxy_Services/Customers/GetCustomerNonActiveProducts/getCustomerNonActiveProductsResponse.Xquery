<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1.1  2009-06-03  PKL  TEET 37683
v.1.1.2  2009-06-10  PKL  TEET 37994
v.1.1.3  2009-06-17  KADA CR61</con:description>
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:card.entities.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns7="urn:dictionaries.be.dcl";
declare namespace ns8="urn:insurance.entities.be.dcl";
declare namespace ns9="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml="";
declare namespace ns10="urn:errors.hlbsentities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:anyType,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
    if ($value)
      then if(string-length($value)&gt;5)
          then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
      else() 
    else()
};

(:
declare function getElementsForInsurances($parm as element(fml:FML32)) as element()
{

&lt;ns0:insurances&gt;
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="4") then 
    &lt;ns8:Insurance&gt;
      &lt;ns8:insuranceNumber?&gt;{data($parm/NF_INSURA_INSURANCENUMBER[$occ])}&lt;/ns8:insuranceNumber&gt;
      &lt;ns8:insuranceDescription?&gt;{data($parm/NF_INSURA_INSURANCEDESCRIP[$occ])}&lt;/ns8:insuranceDescription&gt;
       { insertDate(data($parm/NF_INSURA_LASTINSURANCEPRE[$occ]),"yyyy-MM-dd","ns8:lastInsurancePremiumDate")}
       { insertDate(data($parm/NF_INSURA_INSURANCEOPENDAT[$occ]),"yyyy-MM-dd","ns8:insuranceOpenDate")}
        &lt;/ns8:Insurance&gt;
    else ()
  }
&lt;/ns0:insurances&gt;
};
:)

declare function getElementsForAccountRelationshipList($parm as element(fml:FML32)) as element()
{

&lt;ns9:accountRelationshipList&gt;
  {
    for $x at $occ in $parm/XXXXX
    return
    &lt;ns4:AccountRelationship&gt;
      &lt;ns4:relationship&gt;
        &lt;ns7:CustomerAccountRelationship&gt;
        &lt;/ns7:CustomerAccountRelationship&gt;
      &lt;/ns4:relationship&gt;
    &lt;/ns4:AccountRelationship&gt;
  }
&lt;/ns9:accountRelationshipList&gt;
};
declare function getElementsForAccounts($parm as element(fml:FML32)) as element()
{
&lt;ns0:accounts&gt;
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="2" or data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="10"
          or data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="13") then
    &lt;ns9:Account&gt;
      &lt;ns9:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns9:accountNumber&gt;
     { insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns9:accountOpenDate")}
        &lt;ns9:currentBalance?&gt;{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns9:currentBalance&gt;
      &lt;ns9:accountDescription?&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns9:accountDescription&gt; 
      &lt;ns9:codeProductSystem?&gt;{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}&lt;/ns9:codeProductSystem&gt;
      &lt;ns9:accountRelationshipList&gt;
        &lt;ns4:AccountRelationship&gt;
          &lt;ns4:relationship&gt;
             &lt;ns7:CustomerAccountRelationship&gt;
                &lt;ns7:customerAccountRelationship&gt;{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns7:customerAccountRelationship&gt;
             &lt;/ns7:CustomerAccountRelationship&gt;
             &lt;/ns4:relationship&gt;
        &lt;/ns4:AccountRelationship&gt;
      &lt;/ns9:accountRelationshipList&gt;
          {
           if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="10") then
             &lt;ns9:timeAccount&gt;
               &lt;ns9:TimeAccount&gt;
                   { insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]),"yyyy-MM-dd","ns9:dateOfMaint")}
               &lt;/ns9:TimeAccount&gt;
             &lt;/ns9:timeAccount&gt;
           else
             &lt;ns9:tranAccount&gt;
                &lt;ns9:TranAccount&gt;
                   { insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]),"yyyy-MM-dd","ns9:dateOfLastActivity")}
                   (: &lt;ns9:dateOfLastActivity?&gt;{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]))}&lt;/ns9:dateOfLastActivity&gt; :)
                 &lt;/ns9:TranAccount&gt;
             &lt;/ns9:tranAccount&gt;
          }
      &lt;ns9:accountType&gt;
        &lt;ns7:AccountType&gt;
          &lt;ns7:accountType?&gt;{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns7:accountType&gt;
        &lt;/ns7:AccountType&gt;
      &lt;/ns9:accountType&gt;
      &lt;ns9:currency&gt;
        &lt;ns7:CurrencyCode&gt;
          &lt;ns7:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns7:currencyCode&gt;
        &lt;/ns7:CurrencyCode&gt;
      &lt;/ns9:currency&gt;
      {getElementsForProductDefinition($parm, $occ)}
    &lt;/ns9:Account&gt;
    else()
  }
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="9" ) then
    &lt;ns9:Account&gt;
      &lt;ns9:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns9:accountNumber&gt;
     { insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns9:accountOpenDate")}
      &lt;ns9:currentBalance?&gt;{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns9:currentBalance&gt;
      &lt;ns9:accountDescription?&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns9:accountDescription&gt; 
      &lt;ns9:codeProductSystem?&gt;{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}&lt;/ns9:codeProductSystem&gt;
      &lt;ns9:tranAccount&gt;
        &lt;ns9:TranAccount&gt;
           { insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]),"yyyy-MM-dd","ns9:dateOfLastActivity")}
           &lt;/ns9:TranAccount&gt;
      &lt;/ns9:tranAccount&gt;
      &lt;ns9:accountType&gt;
        &lt;ns7:AccountType&gt;
          &lt;ns7:accountType?&gt;{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns7:accountType&gt;
        &lt;/ns7:AccountType&gt;
      &lt;/ns9:accountType&gt;
      &lt;ns9:currency&gt;
        &lt;ns7:CurrencyCode&gt;
          &lt;ns7:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns7:currencyCode&gt;
        &lt;/ns7:CurrencyCode&gt;
      &lt;/ns9:currency&gt;
      {getElementsForProductDefinition($parm, $occ)}
    &lt;/ns9:Account&gt;
    else()
  }
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="3"  ) then
    &lt;ns9:Account&gt;
      &lt;ns9:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns9:accountNumber&gt;
     { insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns9:accountOpenDate")}
      &lt;ns9:currentBalance?&gt;{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns9:currentBalance&gt;
      &lt;ns9:accountDescription?&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns9:accountDescription&gt; 
      &lt;ns9:codeProductSystem?&gt;{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}&lt;/ns9:codeProductSystem&gt;
      &lt;ns9:accountType&gt;
        &lt;ns7:AccountType&gt;
          &lt;ns7:accountType?&gt;{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns7:accountType&gt;
        &lt;/ns7:AccountType&gt;
      &lt;/ns9:accountType&gt;
      &lt;ns9:currency&gt;
        &lt;ns7:CurrencyCode&gt;
          &lt;ns7:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns7:currencyCode&gt;
        &lt;/ns7:CurrencyCode&gt;
      &lt;/ns9:currency&gt;
      {getElementsForProductDefinition($parm, $occ)}
    &lt;/ns9:Account&gt;
    else()
  }


&lt;/ns0:accounts&gt;
};
declare function getElementsForCards($parm as element(fml:FML32)) as element()
{

&lt;ns0:cards&gt;
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
     if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="1") then
    &lt;ns3:Card&gt;
      &lt;ns3:cardName?&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns3:cardName&gt;
      &lt;ns3:debitCard&gt;
        &lt;ns3:DebitCard&gt;
          &lt;ns3:cardNbr?&gt;{data($parm/NF_DEBITC_CARDNBR[$occ])}&lt;/ns3:cardNbr&gt;
          &lt;ns3:virtualCardNbr?&gt;{data($parm/NF_DEBITC_VIRTUALCARDNBR[$occ])}&lt;/ns3:virtualCardNbr&gt;
          &lt;ns3:tranAccount&gt;
            &lt;ns9:TranAccount&gt;
              &lt;ns9:account&gt;
                &lt;ns9:Account&gt;
                  &lt;ns9:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns9:accountNumber&gt;
                  &lt;ns9:currency&gt;
                     &lt;ns7:CurrencyCode&gt;
                      &lt;ns7:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns7:currencyCode&gt;
                     &lt;/ns7:CurrencyCode&gt;
                   &lt;/ns9:currency&gt;
                 &lt;/ns9:Account&gt;
               &lt;/ns9:account&gt;
             &lt;/ns9:TranAccount&gt;
           &lt;/ns3:tranAccount&gt;
         &lt;/ns3:DebitCard&gt;
       &lt;/ns3:debitCard&gt;
    &lt;/ns3:Card&gt;
         else ()
  }
&lt;/ns0:cards&gt;
};
declare function getElementsForCollaterals($parm as element(fml:FML32)) as element()
{

&lt;ns0:collaterals&gt;
  {
    for $x at $occ in $parm/NF_COLLAT_COLLATERALVALUE
    return
    if (string-length($parm/NF_COLLAT_COLLATERALVALUE[$occ])&gt;0) then
    &lt;ns9:Collateral&gt;      
      &lt;ns9:collateralDescription?&gt;{data($parm/NF_COLLAT_COLLATERALDESCRI[$occ])}&lt;/ns9:collateralDescription&gt;
      &lt;ns9:collateralValue?&gt;{data($parm/NF_COLLAT_COLLATERALVALUE[$occ])}&lt;/ns9:collateralValue&gt;
      &lt;ns9:collateralItemNumber?&gt;{data($parm/NF_COLLAT_COLLATERALITEMNU[$occ])}&lt;/ns9:collateralItemNumber&gt;
      &lt;ns9:lastMaintenance?&gt;{data($parm/NF_COLLAT_LASTMAINTENANCE[$occ])}&lt;/ns9:lastMaintenance&gt;
      &lt;ns9:currencyCode&gt;
        &lt;ns7:CurrencyCode&gt;
          &lt;ns7:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns7:currencyCode&gt;
        &lt;/ns7:CurrencyCode&gt;
      &lt;/ns9:currencyCode&gt;
    &lt;/ns9:Collateral&gt;
    else ()
  }
&lt;/ns0:collaterals&gt;
};

(:
declare function getElementsForProductDefinition($parm as element(fml:FML32)) as element()
{

&lt;ns0:productDefinition&gt;
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
    &lt;ns1:ProductDefinition&gt;
      &lt;ns1:idProductGroup?&gt;{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup&gt;
      &lt;ns1:idProductDefinition?&gt;{data($parm/PT_ID_DEFINITION[$occ])}&lt;/ns1:idProductDefinition&gt;
    &lt;/ns1:ProductDefinition&gt;
  }
&lt;/ns0:productDefinition&gt;
};
:)

declare function getElementsForProductDefinition($parm as element(fml:FML32), $occ as xs:integer) as element()
{
&lt;ns9:productDefinition&gt;
    &lt;ns1:ProductDefinition&gt;
      &lt;ns1:idProductGroup?&gt;{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup&gt;
      &lt;ns1:idProductDefinition?&gt;{data($parm/PT_ID_DEFINITION[$occ])}&lt;/ns1:idProductDefinition&gt;
    &lt;/ns1:ProductDefinition&gt;
&lt;/ns9:productDefinition&gt;
};


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  (:{getElementsForInsurances($parm)}:)
  (:{getElementsForProductDefinition($parm)}:)
  {getElementsForAccounts($parm)}
  &lt;ns0:bcd&gt;
    &lt;ns5:BusinessControlData&gt;
      &lt;ns5:pageControl&gt;
        &lt;ns6:PageControl&gt;
          &lt;ns6:hasNext?&gt;{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns6:hasNext&gt;
          &lt;ns6:navigationKeyDefinition?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition&gt;
          &lt;ns6:navigationKeyValue?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue&gt;
        &lt;/ns6:PageControl&gt;
      &lt;/ns5:pageControl&gt;
    &lt;/ns5:BusinessControlData&gt;
  &lt;/ns0:bcd&gt;
  {getElementsForCards($parm)}
  {getElementsForCollaterals($parm)}
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>