<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace wsdl="http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace xf="http://jv.channel.cu.com.pl/cmf/functions";
declare namespace ser-root="http://jv.channel.cu.com.pl/webservice/";
declare namespace wsdl-jv="http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace fml = "";

declare variable $timestamp external;

declare function xf:mapErrorCode($errorCode as xs:string) as xs:string
{
	if ($errorCode = '1003') then
		'106'
	else if ($errorCode = '1001') then
		'107'
        else if ($errorCode = '1') then
                '102'
	else
		$errorCode
};

declare function xf:mapResponse($resp as element(wsdl:addClientJVResponse))
		as element (fml:FML32)
{
	&lt;fml:FML32&gt;
		&lt;fml:CI_NR_ZRODLOWY_KLIENTA&gt; { data($resp/client-code) } &lt;/fml:CI_NR_ZRODLOWY_KLIENTA&gt;
		&lt;fml:CI_CZAS_AKTUALIZACJI&gt;{ data($timestamp) }&lt;/fml:CI_CZAS_AKTUALIZACJI&gt;
	&lt;/fml:FML32&gt;
};

declare function xf:mapFault($fault as element(soap-env:Fault))
		as element (fml:FML32)
{
	&lt;fml:FML32&gt;
	{
		if (data($fault/detail/ser-root:faultResponse/wsdl-jv:faultResponse/status/@error-code)) then
			&lt;fml:CI_KOD_BLEDU&gt;
			{
				xf:mapErrorCode($fault/detail/ser-root:faultResponse/wsdl-jv:faultResponse/status/@error-code)
			}
			&lt;/fml:CI_KOD_BLEDU&gt;
		else ()
	}
	{
		if (data($fault/detail/ser-root:faultResponse/wsdl-jv:faultResponse/status/error-msg)) then
			&lt;fml:DC_OPIS_BLEDU&gt;
			{
				data($fault/detail/ser-root:faultResponse/wsdl-jv:faultResponse/status/error-msg)
			}
			&lt;/fml:DC_OPIS_BLEDU&gt;
		else ()
	}
	&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{
	if (boolean($body/wsdl:addClientJVResponse)) then (
 		xf:mapResponse($body/wsdl:addClientJVResponse)
 	) else if (boolean($body/soap-env:Fault)) then (
 		xf:mapFault($body/soap-env:Fault)
 	) else ()

}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>