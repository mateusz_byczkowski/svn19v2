<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:hlbsentities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://bzwbk.com/services/mappings/";

declare variable $body as element(soap-env:Body) external;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))&gt;0)
    then true()
    else false()
};

&lt;soapenv:Body  xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"&gt; 
  {
  let $req := $body/m:invoke/m:customer/m1:CheckCustomerInMIGInput
    
  return
    &lt;urn:invokeResponse xmlns:urn="urn:be.services.dcl"&gt; 
      &lt;urn:customerStatus xmlns:urn1="urn:hlbsentities.be.dcl"&gt; 
        &lt;urn1:CheckCustomerInMIGOutput&gt; 
          {if(($req/m1:documentNumber and fn:contains(",DD9950289,DD9950289,ABF764773,AJL521424,AAE291299,AWT123456,DD9682040,AJX609449,", concat(",", $req/m1:documentNumber, ",")))
             or ($req/m1:pesel and fn:contains(",80030304185,77050803075,83101408124,86041811168,83032816047,76081868882,72041224684,85042301629,", concat(",", $req/m1:pesel, ",")))
             or ($req/m1:regon and fn:contains(",012563538,091365937,092541968,799654369,747131169,187378727,868187383,679518610,633424017,108609312,", concat(",", $req/m1:regon, ","))))
              then &lt;urn1:status&gt;true&lt;/urn1:status&gt;
              else &lt;urn1:status&gt;false&lt;/urn1:status&gt;
          }
        &lt;/urn1:CheckCustomerInMIGOutput&gt; 
      &lt;/urn:customerStatus&gt; 
    &lt;/urn:invokeResponse&gt; 
  }
&lt;/soapenv:Body&gt;</con:xquery>
</con:xqueryEntry>