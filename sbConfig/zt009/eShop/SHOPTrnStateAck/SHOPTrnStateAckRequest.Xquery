<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://eshop.com/services/TrnStateAck/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapSHOPTrnAckRequest($fml as element(fml:FML32))
	as element(m:TrnStateAckReq) {
		&lt;m:TrnStateAckReq&gt;
		
                                 &lt;m:shopId&gt;{data($fml/fml:E_SHOP_ID)}&lt;/m:shopId&gt;
		 &lt;m:shopTransId&gt;{data($fml/fml:E_SHOP_TRANS_ID)}&lt;/m:shopTransId&gt;
                                 &lt;m:trnState&gt;{data($fml/fml:E_TRN_STATE)}&lt;/m:trnState&gt;
		

		&lt;/m:TrnStateAckReq&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapSHOPTrnAckRequest($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>