<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace cer="http://bzwbk.com/services/cerber";
declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";

declare variable $body as element(soap-env:Body) external;

declare function local:UnitBaseContents($unit as element()) as element()* {
    &lt;AddressCity&gt;{ data($unit/addressCity) }&lt;/AddressCity&gt;,
    &lt;AddressNo&gt;{ data($unit/addressNo) }&lt;/AddressNo&gt;,
    &lt;AddressState&gt;{ data($unit/addressState) }&lt;/AddressState&gt;,
    &lt;AddressStreet&gt;{ data($unit/addressStreet) }&lt;/AddressStreet&gt;,
    &lt;AddressZip&gt;{ data($unit/addressZip) }&lt;/AddressZip&gt;,
    &lt;IcbsNo?&gt;{ data($unit/icbsNo) }&lt;/IcbsNo&gt;, 
    &lt;KirNo?&gt;{ data($unit/kirNo) }&lt;/KirNo&gt;,
    &lt;Name&gt;{ data($unit/name) }&lt;/Name&gt;,
    &lt;FunctionId?&gt;{ data($unit/functionId) }&lt;/FunctionId&gt;,
    &lt;Function&gt;{ data($unit/function) }&lt;/Function&gt;,
    &lt;ChiefId?&gt;{ data($unit/chiefId) }&lt;/ChiefId&gt;,
    &lt;CompanyId?&gt;{ data($unit/companyId) }&lt;/CompanyId&gt;,
    &lt;ParentIcbsNo?&gt;{ data($unit/parentIcbsNo) }&lt;/ParentIcbsNo&gt;
};

declare function local:Units($unit as element()) as element() {
    &lt;Units&gt;
        {
            local:UnitBaseContents($unit)
        }
        &lt;CashierNo?&gt;{ data($unit/cashierNo)}&lt;/CashierNo&gt;
        &lt;TeamId?&gt;{data($unit/teamId)}&lt;/TeamId&gt;
        &lt;IsDelegation&gt;{data($unit/isDelegation)}&lt;/IsDelegation&gt;
        &lt;Since?&gt;{data($unit/since)}&lt;/Since&gt;
        &lt;Until?&gt;{data($unit/until)}&lt;/Until&gt;
        &lt;IsMasterUnit&gt;{data($unit/isMasterUnit)}&lt;/IsMasterUnit&gt;
        &lt;WorkWithOwnWallet&gt;{data($unit/workWithOwnWallet)}&lt;/WorkWithOwnWallet&gt;
        {
            for $a in $unit/attributes/attribute
            return
                &lt;Attributes&gt;
                    &lt;Name&gt;{data($a/name)}&lt;/Name&gt;
                    &lt;Value&gt;{data($a/value)}&lt;/Value&gt;
                &lt;/Attributes&gt;
        }
        {
            for $p in $unit/permissions/string
            return
                &lt;Permissions&gt;{data($p)}&lt;/Permissions&gt;
        }
        {
            for $s in $unit/substitutions/substitution
            return
                &lt;Substitutions&gt;
                    &lt;Id&gt;{ data($s/id) }&lt;/Id&gt;
                    &lt;Since?&gt;{data($s/since)}&lt;/Since&gt;
                    &lt;SubstitutedUserId&gt;{data($s/substitutedUserId)}&lt;/SubstitutedUserId&gt;
                    &lt;SubstitutionForApplication&gt;{data($s/substitutionForApplication)}&lt;/SubstitutionForApplication&gt;
                    &lt;UnitId&gt;{data($s/unitId)}&lt;/UnitId&gt;
                    &lt;Until?&gt;{data($s/until)}&lt;/Until&gt;
                    &lt;UserId&gt;{data($s/userId)}&lt;/UserId&gt;
                    &lt;CreatedBy&gt;{ data($s/createdBy) }&lt;/CreatedBy&gt;
                &lt;/Substitutions&gt;
        }
    &lt;/Units&gt;
};

declare function local:UserBaseContents($user as element()) as element()* {
    &lt;Email&gt;{ data($user/email) }&lt;/Email&gt;,
    &lt;Func&gt;{ data($user/function) }&lt;/Func&gt;,
    &lt;Gender&gt;{ data($user/gender) }&lt;/Gender&gt;,
    &lt;LastIncorrectLoginApp?&gt;{ data($user/lastIncorrectLoginApp) }&lt;/LastIncorrectLoginApp&gt;,
    &lt;LastIncorrectLoginDate?&gt;{ data($user/lastIncorrectLoginDate) }&lt;/LastIncorrectLoginDate&gt;,
    &lt;LastLoggedInApplication?&gt;{ data($user/lastLoggedInApplication) }&lt;/LastLoggedInApplication&gt;,
    &lt;LastLoggedInDate?&gt;{ data($user/lastLoggedInDate) }&lt;/LastLoggedInDate&gt;,
    &lt;Login&gt;{ data($user/login) }&lt;/Login&gt;,
    &lt;MobileNo&gt;{ data($user/mobileNo) }&lt;/MobileNo&gt;,
    &lt;Name&gt;{ data($user/name) }&lt;/Name&gt;,
    &lt;Password?&gt;{ data($user/password) }&lt;/Password&gt;,
    &lt;PhoneNo&gt;{ data($user/phoneNo) }&lt;/PhoneNo&gt;,
    &lt;Position?&gt;{ data($user/position) }&lt;/Position&gt;,
    &lt;PositionId?&gt;{ data($user/positionId) }&lt;/PositionId&gt;,
    &lt;Skp&gt;{ data($user/skp) }&lt;/Skp&gt;,
    &lt;Surname&gt;{ data($user/surename) }&lt;/Surname&gt;
};

declare function local:UserContents($user as element()) as element()* {
    local:UserBaseContents($user),
    for $a in $user/attributes/attribute
    return
        &lt;Attributes&gt;
            &lt;Name&gt;{data($a/name)}&lt;/Name&gt;
            &lt;Value&gt;{data($a/value)}&lt;/Value&gt;
        &lt;/Attributes&gt;,
    for $a in $user/positionAttributes/attribute
    return
        &lt;PositionAttributes&gt;
            &lt;Name&gt;{data($a/name)}&lt;/Name&gt;
            &lt;Value&gt;{data($a/value)}&lt;/Value&gt;
        &lt;/PositionAttributes&gt;,
    for $p in $user/permissions/string
    return
        &lt;Permissions&gt;{data($p)}&lt;/Permissions&gt;,
    for $p in $user/positionPermissions/string
    return
        &lt;PositionPermissions&gt;{data($p)}&lt;/PositionPermissions&gt;,
    for $u in $user/units/unit
    return
        local:Units($u)
};

declare function local:UserExtendedContents($user as element()) as element()* {
    local:UserContents($user),
    for $g in $user/groups/group
    return
        &lt;Groups&gt;
            {
                for $a in $g/attributes/attribute
                return
                    &lt;Attributes&gt;
                        &lt;Name&gt;{data($a/name)}&lt;/Name&gt;
                        &lt;Value&gt;{data($a/value)}&lt;/Value&gt;
                    &lt;/Attributes&gt;
            }
            &lt;Name&gt;{data($g/name)}&lt;/Name&gt;
            {
                for $p in $g/permissions/string
                return
                    &lt;Permissions&gt;{data($p)}&lt;/Permissions&gt;
            }
        &lt;/Groups&gt;,
    for $g in $user/positionGroups/group
    return
        &lt;PositionGroups&gt;
            {
                for $a in $g/attributes/attribute
                return
                    &lt;Attributes&gt;
                        &lt;Name&gt;{data($a/name)}&lt;/Name&gt;
                        &lt;Value&gt;{data($a/value)}&lt;/Value&gt;
                    &lt;/Attributes&gt;
            }
            &lt;Name&gt;{data($g/name)}&lt;/Name&gt;
            {
                for $p in $g/permissions/string
                return
                    &lt;Permissions&gt;{data($p)}&lt;/Permissions&gt;
            }
        &lt;/PositionGroups&gt;
};

declare function local:GetUserDataResponse($resp as element(cer:getUserDataResponse)) as element() {
    if($resp/user) then
        &lt;cer:GetUserDataResponse&gt;
        {
            local:UserContents($resp/user)
        }
        &lt;/cer:GetUserDataResponse&gt;
    else
        $resp
};

declare function local:GetUserDataWithHashResponse($resp as element(cer:getUserDataWithHashResponse)) as element() {
    if($resp/user) then
        &lt;cer:GetUserDataWithHashResponse&gt;
        {
            local:UserContents($resp/user)
        }
        &lt;/cer:GetUserDataWithHashResponse&gt;
    else
        $resp
};


declare function local:GetUserDataExtendedResponse($resp as element(cer:getUserDataExtendedResponse)) as element() {
    if($resp/userExtended) then
        &lt;cer:GetUserDataExtendedResponse&gt;
        {
            local:UserExtendedContents($resp/userExtended)
        }
        &lt;/cer:GetUserDataExtendedResponse&gt;
    else
        $resp
};

declare function local:GetUserDataWithoutPassResponse($resp as element(cer:getUserDataWithoutPassResponse)) as element() {
    if($resp/user) then
        &lt;cer:GetUserDataWithoutPassResponse&gt;
        {
            local:UserContents($resp/user)
        }
        &lt;/cer:GetUserDataWithoutPassResponse&gt;
    else
        $resp
};

declare function local:GetUserDataWithoutPassForAppResponse($resp as element(cer:getUserDataWithoutPassForAppResponse)) as element() {
    if($resp/user) then
        &lt;cer:GetUserDataWithoutPassForAppResponse&gt;
        {
            local:UserContents($resp/user)
        }
        &lt;/cer:GetUserDataWithoutPassForAppResponse&gt;
    else
        $resp
};

declare function local:GetUserDataWithoutPassForAppsResponse($resp as element(cer:getUserDataWithoutPassForAppsResponse)) as element() {
    if($resp/user-array/user) then
        &lt;cer:GetUserDataWithoutPassForAppsResponse&gt;
        {
            local:UserContents($resp/user-array/user)
        }
        &lt;/cer:GetUserDataWithoutPassForAppsResponse&gt;
    else
        $resp
};

declare function local:GetUserDataForAppsResponse($resp as element(cer:getUserDataForAppsResponse)) as element() {
    if($resp/user-array/user) then
        &lt;cer:GetUserDataForAppsResponse&gt;
        {
            local:UserContents($resp/user-array/user)
        }
        &lt;/cer:GetUserDataForAppsResponse&gt;
    else
        $resp
};

declare function local:GetUsersDataWithoutPassResponse($resp as element()) as element() {
    if($resp/user-array) then
        &lt;cer:GetUsersDataWithoutPassResponse&gt;
        {
            for $user in $resp/user-array/user
            return
                &lt;cer:User&gt;{ local:UserContents($user) }&lt;/cer:User&gt;
        }
        &lt;/cer:GetUsersDataWithoutPassResponse&gt;
    else
        $resp
};

declare function local:FindUsersMasterUnitsICBSNoResponse($resp as element()) as element() {
    if($resp/list) then
        &lt;cer:FindUsersMasterUnitsICBSNoResponse&gt;
        {
            for $icbsNo in $resp/list/*
            return
            typeswitch($icbsNo)
                case $val as element(int) return element cer:IcbsNumber { data($val) }
                default return &lt;cer:IcbsNumber xsi:nil="true" /&gt;
        }
        &lt;/cer:FindUsersMasterUnitsICBSNoResponse&gt;
    else
        $resp

};

declare function local:SearchUsersResponse($resp as element()) as element() {
    if($resp/list) then
        &lt;cer:SearchUsersResponse&gt;
        {
            for $user in $resp/list/userBaseImpl
            return 
                &lt;cer:User&gt;{ local:UserBaseContents($user) }&lt;/cer:User&gt;
        }
        &lt;/cer:SearchUsersResponse&gt;
    else
        $resp
};

declare function local:GetDirectSuperiorUserResponse($resp as element()) as element() {
    &lt;cer:GetDirectSuperiorUserResponse&gt;
    {
        if($resp/userBaseImpl) then
            local:UserBaseContents($resp/userBaseImpl)
        else
            ()
    }
    &lt;/cer:GetDirectSuperiorUserResponse&gt;
};

declare function local:GetDirectSuperiorUsersResponse($resp as element()) as element() {
    if($resp/list) then
        &lt;cer:GetDirectSuperiorUsersResponse&gt;
        {
            for $user in $resp/list/(userBaseImpl|null)
            return
            (
                if(name($user) = "null") then
                    &lt;cer:User xsi:nil="true" /&gt;
                else
                    &lt;cer:User&gt;{ local:UserContents($user) }&lt;/cer:User&gt;
            )
        }
        &lt;/cer:GetDirectSuperiorUsersResponse&gt;
    else
        $resp
};

declare function local:GetUserPermissionUnitInfoResponse($resp as element()) as element() {
    if($resp/linked-list) then
        &lt;cer:GetUserPermissionUnitInfoResponse&gt;
        {
            for $upui in $resp/linked-list/userPermissionUnitInfo
            return
                &lt;cer:userPermissionUnitInfo&gt;
                    &lt;SKP&gt;{ data($upui/skp) }&lt;/SKP&gt;
                    &lt;PermissionName&gt;{ data($upui/permissionName) }&lt;/PermissionName&gt;
                    &lt;IcbsNo?&gt;{ data($upui/icbsNo) }&lt;/IcbsNo&gt;
                &lt;/cer:userPermissionUnitInfo&gt;
        }
        &lt;/cer:GetUserPermissionUnitInfoResponse&gt;
    else
        $resp
};


&lt;soap-env:Body&gt;
{
    typeswitch($body/*)
        case $resp as element(cer:getUserDataResponse) return local:GetUserDataResponse($resp)
        case $resp as element(cer:getUserDataWithHashResponse) return local:GetUserDataWithHashResponse($resp)
        case $resp as element(cer:getUserDataExtendedResponse) return local:GetUserDataExtendedResponse($resp)
        case $resp as element(cer:getUserDataWithoutPassResponse) return local:GetUserDataWithoutPassResponse($resp)
        case $resp as element(cer:getUserDataWithoutPassForAppResponse) return local:GetUserDataWithoutPassForAppResponse($resp)
        case $resp as element(cer:getUserDataWithoutPassForAppsResponse) return local:GetUserDataWithoutPassForAppsResponse($resp)
        case $resp as element(cer:getUserDataForAppsResponse) return local:GetUserDataForAppsResponse($resp)
        case $resp as element(cer:getUsersDataWithoutPassResponse) return local:GetUsersDataWithoutPassResponse($resp)
        case $resp as element(cer:findUsersMasterUnitsICBSnoResponse) return local:FindUsersMasterUnitsICBSNoResponse($resp)
        case $resp as element(cer:searchUsersResponse) return local:SearchUsersResponse($resp)
        case $resp as element(cer:getDirectSuperiorUserResponse) return local:GetDirectSuperiorUserResponse($resp) 
        case $resp as element(cer:getDirectSuperiorUsersResponse) return local:GetDirectSuperiorUsersResponse($resp)        
        case $resp as element(cer:getUserPermissionUnitInfoResponse) return local:GetUserPermissionUnitInfoResponse($resp)      
        default $resp return $resp
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>