<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetDictionaryRequest($req as element(m:GetDictionaryRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:Identyfikator1)
					then &lt;fml:B_IDENTYFIKATOR1&gt;{ data($req/m:Identyfikator1) }&lt;/fml:B_IDENTYFIKATOR1&gt;
					else ()
			}
			{
				if($req/m:Identyfikator2)
					then &lt;fml:B_IDENTYFIKATOR2&gt;{ data($req/m:Identyfikator2) }&lt;/fml:B_IDENTYFIKATOR2&gt;
					else ()
			}
			{
				if($req/m:Lp)
					then &lt;fml:B_LP&gt;{ data($req/m:Lp) }&lt;/fml:B_LP&gt;
					else ()
			}
			{
				if($req/m:KodPs)
					then &lt;fml:B_KOD_PS&gt;{ data($req/m:KodPs) }&lt;/fml:B_KOD_PS&gt;
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:B_OPCJA&gt;{ data($req/m:Opcja) }&lt;/fml:B_OPCJA&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapGetDictionaryRequest($body/m:GetDictionaryRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>