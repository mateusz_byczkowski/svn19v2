<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetDictionaryResponse($fml as element(fml:FML32))
	as element(m:GetDictionaryResponse) {
		&lt;m:GetDictionaryResponse&gt;
			{

				let $B_IDENTYFIKATOR2 := $fml/fml:B_IDENTYFIKATOR2
				let $B_KOD_PS := $fml/fml:B_KOD_PS
				let $B_NAZWA := $fml/fml:B_NAZWA
				let $B_WALUTA := $fml/fml:B_WALUTA
				let $B_RET_DATA := $fml/fml:B_RET_DATA
				let $B_STOPA := $fml/fml:B_STOPA
				let $B_STOPA_IND := $fml/fml:B_STOPA_IND
				for $it at $p in $fml/fml:B_KOD_PS
				return
					&lt;m:Entry&gt;
					{
						if($B_IDENTYFIKATOR2[$p])
							then &lt;m:Identyfikator2&gt;{ data($B_IDENTYFIKATOR2[$p]) }&lt;/m:Identyfikator2&gt;
						else ()
					}
					{
						if($B_KOD_PS[$p])
							then &lt;m:KodPs&gt;{ data($B_KOD_PS[$p]) }&lt;/m:KodPs&gt;
						else ()
					}
					{
						if($B_NAZWA[$p])
							then &lt;m:Nazwa&gt;{ data($B_NAZWA[$p]) }&lt;/m:Nazwa&gt;
						else ()
					}
					{
						if($B_WALUTA[$p])
							then &lt;m:Waluta&gt;{ data($B_WALUTA[$p]) }&lt;/m:Waluta&gt;
						else ()
					}
					{
						if($B_RET_DATA[$p])
							then &lt;m:RetData&gt;{ data($B_RET_DATA[$p]) }&lt;/m:RetData&gt;
						else ()
					}
					{
						if($B_STOPA[$p])
							then &lt;m:Stopa&gt;{ data($B_STOPA[$p]) }&lt;/m:Stopa&gt;
						else ()
					}
					{
						if($B_STOPA_IND[$p])
							then &lt;m:StopaInd&gt;{ data($B_STOPA_IND[$p]) }&lt;/m:StopaInd&gt;
						else ()
					}
					&lt;/m:Entry&gt;
			}

		&lt;/m:GetDictionaryResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapGetDictionaryResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>