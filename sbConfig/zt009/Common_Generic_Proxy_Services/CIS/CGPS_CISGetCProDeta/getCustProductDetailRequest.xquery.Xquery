<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1  2009-12-19  PKL  NP2036  Dodanie obsługi filtra CI_ID_SYS (SystemId)</con:description>
  <con:xquery>(: Change Log 
v.1.1  2009-12-19  PKL  NP2036  Dodanie obsługi filtra CI_ID_SYS (SystemId)
:)


declare namespace m = "http://bzwbk.com/services/esbmessages/";
declare namespace xf = "http://bzwbk.com/services/esbmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetCustProductDetailRequest($req as element(m:getCustProductDetailRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:NrRachunku)
					then &lt;fml:DC_NR_RACHUNKU&gt;{ data($req/m:NrRachunku) }&lt;/fml:DC_NR_RACHUNKU&gt;
					else ()
			}
			{
				if($req/m:NrAlternatywny)
					then &lt;fml:DC_NR_RACHUNKU_W_SYSTEMIE&gt;{ data($req/m:NrAlternatywny) }&lt;/fml:DC_NR_RACHUNKU_W_SYSTEMIE&gt;
					else ()
			}
			{
				if($req/m:ProductAreaId)
					then &lt;fml:CI_PRODUCT_AREA_ID&gt;{ data($req/m:ProductAreaId) }&lt;/fml:CI_PRODUCT_AREA_ID&gt;
					else ()
			}
			{
				if($req/m:ProductCategoryId)
					then &lt;fml:CI_PRODUCT_CATEGORY_ID&gt;{ data($req/m:ProductCategoryId) }&lt;/fml:CI_PRODUCT_CATEGORY_ID&gt;
					else ()
			}
			{
				if($req/m:CiOpcja)
					then &lt;fml:CI_OPCJA&gt;{ data($req/m:CiOpcja) }&lt;/fml:CI_OPCJA&gt;
					else &lt;fml:CI_OPCJA&gt;0&lt;/fml:CI_OPCJA&gt;
			}
(:1.1:)		{
(:1.1:)				if($req/m:SystemId)
(:1.1:)				then &lt;fml:CI_ID_SYS&gt;{ data($req/m:SystemId) }&lt;/fml:CI_ID_SYS&gt;
(:1.1:)				else &lt;fml:CI_ID_SYS&gt;0&lt;/fml:CI_ID_SYS&gt;
(:1.1:)		}
                         
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetCustProductDetailRequest($body/m:getCustProductDetailRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>