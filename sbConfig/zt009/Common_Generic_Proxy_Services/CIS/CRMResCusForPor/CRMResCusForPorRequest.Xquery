<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMResCusForPorRequest($req as element(m:CRMResCusForPorRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				if($req/m:IdOperacji)
					then &lt;fml:CI_ID_OPERACJI&gt;{ data($req/m:IdOperacji) }&lt;/fml:CI_ID_OPERACJI&gt;
					else ()
			}
			{
				for $v in $req/m:NumerKlienta
				return
					&lt;fml:DC_NUMER_KLIENTA&gt;{ data($v) }&lt;/fml:DC_NUMER_KLIENTA&gt;
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA&gt;{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMResCusForPorRequest($body/m:CRMResCusForPorRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>