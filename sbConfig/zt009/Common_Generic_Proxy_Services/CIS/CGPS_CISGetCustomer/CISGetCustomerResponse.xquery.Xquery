<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";



declare function xf:mapCISGetCustomerResponse($fml as element(fml:FML32))
	as element(m:CISGetCustomerResponse) {
        
		&lt;m:CISGetCustomerResponse&gt;
			{
				if($fml/fml:DC_NUMER_KLIENTA)
					then &lt;m:NumerKlienta&gt;{ data($fml/fml:DC_NUMER_KLIENTA) }&lt;/m:NumerKlienta&gt;
					else ()
			}
			{
				if($fml/fml:CI_NAZWA_PELNA)
					then &lt;m:NazwaPelna&gt;{ data($fml/fml:CI_NAZWA_PELNA) }&lt;/m:NazwaPelna&gt;
					else ()
			}
			{
				if($fml/fml:CI_UDOSTEP_GRUPA)
					then &lt;m:UdostepGrupa&gt;{ data($fml/fml:CI_UDOSTEP_GRUPA) }&lt;/m:UdostepGrupa&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then &lt;m:NrDowoduRegon&gt;{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:NrDowoduRegon&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then &lt;m:NrPesel&gt;{ data($fml/fml:DC_NR_PESEL) }&lt;/m:NrPesel&gt;
					else ()
			}
			{
				if($fml/fml:DC_TYP_KLIENTA)
					then &lt;m:TypKlienta&gt;{ data($fml/fml:DC_TYP_KLIENTA) }&lt;/m:TypKlienta&gt;
					else ()
			}
			{
				if($fml/fml:CI_STATUS_GIODO)
					then &lt;m:StatusGiodo&gt;{ data($fml/fml:CI_STATUS_GIODO) }&lt;/m:StatusGiodo&gt;
					else ()
			}
			{
				if($fml/fml:DC_REZ_NIEREZ)
					then &lt;m:RezNierez&gt;{ data($fml/fml:DC_REZ_NIEREZ) }&lt;/m:RezNierez&gt;
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then &lt;m:Nip&gt;{ data($fml/fml:DC_NIP) }&lt;/m:Nip&gt;
					else ()
			}
			{
				if($fml/fml:CI_GRUPA_KLIENTOW)
					then &lt;m:GrupaKlientow&gt;{ data($fml/fml:CI_GRUPA_KLIENTOW) }&lt;/m:GrupaKlientow&gt;
					else ()
			}
			{
				if($fml/fml:DC_PRACOWNIK_BANKU)
					then &lt;m:PracownikBanku&gt;{ data($fml/fml:DC_PRACOWNIK_BANKU) }&lt;/m:PracownikBanku&gt;
					else ()
			}
			{
				if($fml/fml:DC_ULICA_DANE_PODST)
					then &lt;m:UlicaDanePodst&gt;{ data($fml/fml:DC_ULICA_DANE_PODST) }&lt;/m:UlicaDanePodst&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST)
					then &lt;m:NrPosesLokaluDanePodst&gt;{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST) }&lt;/m:NrPosesLokaluDanePodst&gt;
					else ()
			}
			{
				if($fml/fml:DC_MIASTO_DANE_PODST)
					then &lt;m:MiastoDanePodst&gt;{ data($fml/fml:DC_MIASTO_DANE_PODST) }&lt;/m:MiastoDanePodst&gt;
					else ()
			}
			{
				if($fml/fml:DC_KOD_POCZTOWY_DANE_PODST)
					then &lt;m:KodPocztowyDanePodst&gt;{ data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST) }&lt;/m:KodPocztowyDanePodst&gt;
					else ()
			}
			{
				if($fml/fml:DC_ADRES_E_MAIL)
					then &lt;m:AdresEMail&gt;{ data($fml/fml:DC_ADRES_E_MAIL) }&lt;/m:AdresEMail&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_KIER_TELEFONU)
					then &lt;m:NrKierTelefonu&gt;{ data($fml/fml:DC_NR_KIER_TELEFONU) }&lt;/m:NrKierTelefonu&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_TELEFONU)
					then &lt;m:NrTelefonu&gt;{ data($fml/fml:DC_NR_TELEFONU) }&lt;/m:NrTelefonu&gt;
					else ()
			}
			{
				if($fml/fml:DC_NUMER_FAKSU)
					then &lt;m:NumerFaksu&gt;{ data($fml/fml:DC_NUMER_FAKSU) }&lt;/m:NumerFaksu&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_TELEF_KOMORKOWEGO)
					then &lt;m:NrTelefKomorkowego&gt;{ data($fml/fml:DC_NR_TELEF_KOMORKOWEGO) }&lt;/m:NrTelefKomorkowego&gt;
					else ()
			}
			{
				if($fml/fml:DC_WOJEWODZTWO)
					then &lt;m:Wojewodztwo&gt;{ data($fml/fml:DC_WOJEWODZTWO) }&lt;/m:Wojewodztwo&gt;
					else ()
			}
			{
				if($fml/fml:DC_KOD_KRAJU)
					then &lt;m:KodKraju&gt;{ data($fml/fml:DC_KOD_KRAJU) }&lt;/m:KodKraju&gt;
					else ()
			}
			{
				if($fml/fml:DC_NUMER_ODDZIALU)
					then if (data($fml/fml:DC_NUMER_ODDZIALU) = '997')
                                                   then &lt;m:NumerOddzialu&gt;10066&lt;/m:NumerOddzialu&gt; 
                                         else if (data($fml/fml:DC_NUMER_ODDZIALU) = '996')
                                                   then &lt;m:NumerOddzialu&gt;10072&lt;/m:NumerOddzialu&gt; 
                                         else if (data($fml/fml:DC_NUMER_ODDZIALU) = '995')
                                                   then &lt;m:NumerOddzialu&gt;10017&lt;/m:NumerOddzialu&gt; 
                                         else if (data($fml/fml:DC_NUMER_ODDZIALU) = '992')
                                                   then &lt;m:NumerOddzialu&gt;10076&lt;/m:NumerOddzialu&gt;          
                                         else  &lt;m:NumerOddzialu&gt;{ data($fml/fml:DC_NUMER_ODDZIALU) }&lt;/m:NumerOddzialu&gt;
					else ()
			}
			{
				if($fml/fml:DC_SPW)
					then &lt;m:Spw&gt;{ data($fml/fml:DC_SPW) }&lt;/m:Spw&gt;
					else ()
			}
			{
				if($fml/fml:DC_NAZWA_SKROCONA)
					then &lt;m:NazwaSkrocona&gt;{ data($fml/fml:DC_NAZWA_SKROCONA) }&lt;/m:NazwaSkrocona&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_WPROWADZENIA)
					then &lt;m:DataWprowadzenia&gt;{ data($fml/fml:CI_DATA_WPROWADZENIA) }&lt;/m:DataWprowadzenia&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_STATUSU_GIODU)
					then &lt;m:DataStatusuGiodu&gt;{ data($fml/fml:CI_DATA_STATUSU_GIODU) }&lt;/m:DataStatusuGiodu&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_UDOSTEP_GRUPA)
					then &lt;m:DataUdostepGrupa&gt;{ data($fml/fml:CI_DATA_UDOSTEP_GRUPA) }&lt;/m:DataUdostepGrupa&gt;
					else ()
			}
			{
				if($fml/fml:CI_ZGODA_NA_KORESP)
					then &lt;m:ZgodaNaKoresp&gt;{ data($fml/fml:CI_ZGODA_NA_KORESP) }&lt;/m:ZgodaNaKoresp&gt;
					else ()
			}
			{
				if($fml/fml:CI_UDOSTEP_GRUPA)
					(: then &lt;m:ZgodaNaKontakt&gt;{ data($fml/fml:CI_ZGODA_NA_KONTAKT) }&lt;/m:ZgodaNaKontakt&gt;
                                         Zmiana pola na zawierające zgodę na przetwarzanie danych :)
                                            then if (data($fml/fml:CI_UDOSTEP_GRUPA ) = "99999") then 
                                                &lt;m:ZgodaNaKontakt&gt;9&lt;/m:ZgodaNaKontakt&gt;
                                           else
                                                &lt;m:ZgodaNaKontakt&gt;{ data($fml/fml:CI_UDOSTEP_GRUPA) }&lt;/m:ZgodaNaKontakt&gt;
					else ()
			}
			{
				if($fml/fml:CI_GIIF)
					then &lt;m:Giif&gt;{ data($fml/fml:CI_GIIF) }&lt;/m:Giif&gt;
					else ()
			}
			{
				if($fml/fml:DC_KARTA_WZOROW_PODPISOW)
					then &lt;m:KartaWzorowPodpisow&gt;{ data($fml/fml:DC_KARTA_WZOROW_PODPISOW) }&lt;/m:KartaWzorowPodpisow&gt;
					else ()
			}
			{
				if($fml/fml:DC_PREF_KONTAKT)
					then &lt;m:PrefKontakt&gt;{ data($fml/fml:DC_PREF_KONTAKT) }&lt;/m:PrefKontakt&gt;
					else ()
			}
			{
				if($fml/fml:CI_RYZYKO_NBP)
					then &lt;m:RyzykoNbp&gt;{ data($fml/fml:CI_RYZYKO_NBP) }&lt;/m:RyzykoNbp&gt;
					else ()
			}
			{
				if($fml/fml:CI_RYZYKO_BZWBK)
					then &lt;m:RyzykoBzwbk&gt;{ data($fml/fml:CI_RYZYKO_BZWBK) }&lt;/m:RyzykoBzwbk&gt;
					else ()
			}
			{
				if($fml/fml:CI_MAX_RYZYKO)
					then &lt;m:MaxRyzyko&gt;{ data($fml/fml:CI_MAX_RYZYKO) }&lt;/m:MaxRyzyko&gt;
					else ()
			}
			{
				if($fml/fml:CI_RYZYKO_INNE)
					then &lt;m:RyzykoInne&gt;{ data($fml/fml:CI_RYZYKO_INNE) }&lt;/m:RyzykoInne&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_RYZYKA)
					then &lt;m:DataRyzyka&gt;{ data($fml/fml:CI_DATA_RYZYKA) }&lt;/m:DataRyzyka&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_RYZYKA_INNEGO)
					then &lt;m:DataRyzykaInnego&gt;{ data($fml/fml:CI_DATA_RYZYKA_INNEGO) }&lt;/m:DataRyzykaInnego&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_OPOZNIENIA)
					then &lt;m:DataOpoznienia&gt;{ data($fml/fml:CI_DATA_OPOZNIENIA) }&lt;/m:DataOpoznienia&gt;
					else ()
			}
			{
				if($fml/fml:CI_CZAS_OPOZNIENIA_DNI)
					then &lt;m:CzasOpoznieniaDni&gt;{ data($fml/fml:CI_CZAS_OPOZNIENIA_DNI) }&lt;/m:CzasOpoznieniaDni&gt;
					else ()
			}
			{
				if($fml/fml:CI_LICZNIK_OPOZNIEN)
					then &lt;m:LicznikOpoznien&gt;{ data($fml/fml:CI_LICZNIK_OPOZNIEN) }&lt;/m:LicznikOpoznien&gt;
					else ()
			}
			{
				if($fml/fml:CI_GRADING)
					then &lt;m:Grading&gt;{ data($fml/fml:CI_GRADING) }&lt;/m:Grading&gt;

					else ()
			}
			{
				if($fml/fml:CI_SCORING)
					then &lt;m:Scoring&gt;{ data($fml/fml:CI_SCORING) }&lt;/m:Scoring&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRACOWNIK_WPROW)
					then &lt;m:PracownikWprow&gt;{ data($fml/fml:CI_PRACOWNIK_WPROW) }&lt;/m:PracownikWprow&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRACOWNIK_ZMIEN)
					then &lt;m:PracownikZmien&gt;{ data($fml/fml:CI_PRACOWNIK_ZMIEN) }&lt;/m:PracownikZmien&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_MODYFIKACJI)
					then &lt;m:DataModyfikacji&gt;{ data($fml/fml:CI_DATA_MODYFIKACJI) }&lt;/m:DataModyfikacji&gt;
					else ()
			}
			{
				if($fml/fml:DC_URZEDNIK_1)
					then &lt;m:Urzednik1&gt;{ data($fml/fml:DC_URZEDNIK_1) }&lt;/m:Urzednik1&gt;
					else ()
			}
			{
				if($fml/fml:DC_URZEDNIK_2)
					then &lt;m:Urzednik2&gt;{ data($fml/fml:DC_URZEDNIK_2) }&lt;/m:Urzednik2&gt;
					else ()
			}
			{
				if($fml/fml:DC_URZEDNIK_3)
					then &lt;m:Urzednik3&gt;{ data($fml/fml:DC_URZEDNIK_3) }&lt;/m:Urzednik3&gt;
					else ()
			}
			{
				if($fml/fml:DC_SEGMENT_MARKETINGOWY)
					then &lt;m:SegmentMarketingowy&gt;{ data($fml/fml:DC_SEGMENT_MARKETINGOWY) }&lt;/m:SegmentMarketingowy&gt;
					else ()
			}
			{
				if($fml/fml:CI_PODSEGMENT_MARK)
					then &lt;m:PodsegmentMark&gt;{ data($fml/fml:CI_PODSEGMENT_MARK) }&lt;/m:PodsegmentMark&gt;
					else ()
			}
			{
				if($fml/fml:CI_KLASA_OBSLUGI)
					then &lt;m:KlasaObslugi&gt;{ data($fml/fml:CI_KLASA_OBSLUGI) }&lt;/m:KlasaObslugi&gt;
					else ()
			}
			{
				if($fml/fml:CI_DECYZJA)
					then &lt;m:Decyzja&gt;{ data($fml/fml:CI_DECYZJA) }&lt;/m:Decyzja&gt;
					else ()
			}
			{
				if($fml/fml:DC_ZRODLO_DANYCH)
					then &lt;m:ZrodloDanych&gt;{ data($fml/fml:DC_ZRODLO_DANYCH) }&lt;/m:ZrodloDanych&gt;
					else ()
			}
			{
				if($fml/fml:CI_KANALY_EL)
					then &lt;m:KanalyEl&gt;{ data($fml/fml:CI_KANALY_EL) }&lt;/m:KanalyEl&gt;
					else ()
			}
			{
				if($fml/fml:DC_PAKIET)
					then &lt;m:Pakiet&gt;{ data($fml/fml:DC_PAKIET) }&lt;/m:Pakiet&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_OST_KONTAKTU)
					then &lt;m:DataOstKontaktu&gt;{ data($fml/fml:CI_DATA_OST_KONTAKTU) }&lt;/m:DataOstKontaktu&gt;
					else ()
			}
			{
				if($fml/fml:CI_POSIADANIE_KONTA_OS)
					then &lt;m:PosiadanieKontaOs&gt;{ data($fml/fml:CI_POSIADANIE_KONTA_OS) }&lt;/m:PosiadanieKontaOs&gt;
					else ()
			}
			{
				if($fml/fml:CI_KONTO_Z_LIMITEM)
					then &lt;m:KontoZLimitem&gt;{ data($fml/fml:CI_KONTO_Z_LIMITEM) }&lt;/m:KontoZLimitem&gt;
					else ()
			}
			{
				if($fml/fml:CI_WYSOKOSC_LIMITU)
					then &lt;m:WysokoscLimitu&gt;{ data($fml/fml:CI_WYSOKOSC_LIMITU) }&lt;/m:WysokoscLimitu&gt;
					else ()
			}
			{
				if($fml/fml:CI_LOKATY_BANKOWE)
					then &lt;m:LokatyBankowe&gt;{ data($fml/fml:CI_LOKATY_BANKOWE) }&lt;/m:LokatyBankowe&gt;
					else ()
			}
			{
				if($fml/fml:CI_RACHUNEK_INWEST)
					then &lt;m:RachunekInwest&gt;{ data($fml/fml:CI_RACHUNEK_INWEST) }&lt;/m:RachunekInwest&gt;
					else ()
			}
			{
				if($fml/fml:CI_WYSOKOSC_ZADLUZENIA)
					then &lt;m:WysokoscZadluzenia&gt;{ data($fml/fml:CI_WYSOKOSC_ZADLUZENIA) }&lt;/m:WysokoscZadluzenia&gt;
					else ()
			}
			{
				if($fml/fml:CI_MIESIECZNA_RATA)
					then &lt;m:MiesiecznaRata&gt;{ data($fml/fml:CI_MIESIECZNA_RATA) }&lt;/m:MiesiecznaRata&gt;
					else ()
			}
			{
				if($fml/fml:CI_KREDYT_OBSLUG_SAM)
					then &lt;m:KredytObslugSam&gt;{ data($fml/fml:CI_KREDYT_OBSLUG_SAM) }&lt;/m:KredytObslugSam&gt;
					else ()
			}
			{
				if($fml/fml:CI_WYSOKOSC_PORECZENIA)
					then &lt;m:WysokoscPoreczenia&gt;{ data($fml/fml:CI_WYSOKOSC_PORECZENIA) }&lt;/m:WysokoscPoreczenia&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_WYGASNIECIA)
					then &lt;m:DataWygasniecia&gt;{ data($fml/fml:CI_DATA_WYGASNIECIA) }&lt;/m:DataWygasniecia&gt;
					else ()
			}
			{
				if($fml/fml:CI_ZOBOW_PLAC_PORECZ)
					then &lt;m:ZobowPlacPorecz&gt;{ data($fml/fml:CI_ZOBOW_PLAC_PORECZ) }&lt;/m:ZobowPlacPorecz&gt;
					else ()
			}
			{
				if($fml/fml:CI_WINDYKACJA)
					then &lt;m:Windykacja&gt;{ data($fml/fml:CI_WINDYKACJA) }&lt;/m:Windykacja&gt;
					else ()
			}
			{
				if($fml/fml:CI_KWOTA_WINDYKACJI)
					then &lt;m:KwotaWindykacji&gt;{ data($fml/fml:CI_KWOTA_WINDYKACJI) }&lt;/m:KwotaWindykacji&gt;
					else ()
			}
			{
				if($fml/fml:CI_NIK)
					then &lt;m:Nik&gt;{ data($fml/fml:CI_NIK) }&lt;/m:Nik&gt;
					else ()
			}
			{
				if($fml/fml:CI_RELACJA)
					then &lt;m:Relacja&gt;{ data($fml/fml:CI_RELACJA) }&lt;/m:Relacja&gt;
					else ()
			}
			{
				if($fml/fml:CI_DECYL)
					then &lt;m:Decyl&gt;{ data($fml/fml:CI_DECYL) }&lt;/m:Decyl&gt;
					else ()
			}
			{
				if($fml/fml:CI_LISTA_KONTAKTOW)
					then &lt;m:ListaKontaktow&gt;{ data($fml/fml:CI_LISTA_KONTAKTOW) }&lt;/m:ListaKontaktow&gt;
					else ()
			}
			{
				if($fml/fml:CI_STATUS_AKTYWNOSCI)
					then &lt;m:StatusAktywnosci&gt;{ data($fml/fml:CI_STATUS_AKTYWNOSCI) }&lt;/m:StatusAktywnosci&gt;
					else ()
			}
			{
				if($fml/fml:CI_DANE_MARKETINGOWE)
					then &lt;m:DaneMarketingowe&gt;{ data($fml/fml:CI_DANE_MARKETINGOWE) }&lt;/m:DaneMarketingowe&gt;
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA)
					then &lt;m:SkpPracownika&gt;{ data($fml/fml:CI_SKP_PRACOWNIKA) }&lt;/m:SkpPracownika&gt;
					else ()
			}
			{
				if($fml/fml:CI_SKP_ZASTEPCY)
					then &lt;m:SkpZastepcy&gt;{ data($fml/fml:CI_SKP_ZASTEPCY) }&lt;/m:SkpZastepcy&gt;
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA_UPR)
					then &lt;m:SkpPracownikaUpr&gt;{ data($fml/fml:CI_SKP_PRACOWNIKA_UPR) }&lt;/m:SkpPracownikaUpr&gt;
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA_CBK)
					then &lt;m:SkpPracownikaCbk&gt;{ data($fml/fml:CI_SKP_PRACOWNIKA_CBK) }&lt;/m:SkpPracownikaCbk&gt;
					else ()
			}
			{
				if($fml/fml:DC_IMIE)
					then &lt;m:Imie&gt;{ data($fml/fml:DC_IMIE) }&lt;/m:Imie&gt;
					else ()
			}
			{
				if($fml/fml:DC_NAZWISKO)
					then &lt;m:Nazwisko&gt;{ data($fml/fml:DC_NAZWISKO) }&lt;/m:Nazwisko&gt;
					else ()
			}
			{
				if($fml/fml:DC_NUMER_PASZPORTU)
					then &lt;m:NumerPaszportu&gt;{ data($fml/fml:DC_NUMER_PASZPORTU) }&lt;/m:NumerPaszportu&gt;
					else ()
			}
			{
				if($fml/fml:CI_DOK_TOZSAMOSCI)
					then &lt;m:DokTozsamosci&gt;{ data($fml/fml:CI_DOK_TOZSAMOSCI) }&lt;/m:DokTozsamosci&gt;
					else ()
			}
			{
				if($fml/fml:CI_SERIA_NR_DOK)
					then &lt;m:SeriaNrDok&gt;{ data($fml/fml:CI_SERIA_NR_DOK) }&lt;/m:SeriaNrDok&gt;
					else ()
			}
			{
				if($fml/fml:CI_WYDANY_PRZEZ)
					then &lt;m:WydanyPrzez&gt;{ data($fml/fml:CI_WYDANY_PRZEZ) }&lt;/m:WydanyPrzez&gt;
					else ()
			}
			{
				if($fml/fml:CI_SPRZECIW_PRZETW_DANYCH)
					then &lt;m:SprzeciwPrzetwDanych&gt;{ data($fml/fml:CI_SPRZECIW_PRZETW_DANYCH) }&lt;/m:SprzeciwPrzetwDanych&gt;
					else ()
			}
			{
				if($fml/fml:CI_NR_LEGITYMACJI)
					then &lt;m:NrLegitymacji&gt;{ data($fml/fml:CI_NR_LEGITYMACJI) }&lt;/m:NrLegitymacji&gt;
					else ()
			}
			{
				if($fml/fml:CI_NR_PRAWA_JAZDY)
					then &lt;m:NrPrawaJazdy&gt;{ data($fml/fml:CI_NR_PRAWA_JAZDY) }&lt;/m:NrPrawaJazdy&gt;
					else ()
			}
			{
				if($fml/fml:DC_PLEC)
					then &lt;m:Plec&gt;{ data($fml/fml:DC_PLEC) }&lt;/m:Plec&gt;
					else ()
			}
			{
				if($fml/fml:DC_TYTUL)
					then &lt;m:Tytul&gt;{ data($fml/fml:DC_TYTUL) }&lt;/m:Tytul&gt;
					else ()
			}
			{
				if($fml/fml:DC_DRUGIE_IMIE)
					then &lt;m:DrugieImie&gt;{ data($fml/fml:DC_DRUGIE_IMIE) }&lt;/m:DrugieImie&gt;
					else ()
			}
			{
				if($fml/fml:DC_IMIE_OJCA)
					then &lt;m:ImieOjca&gt;{ data($fml/fml:DC_IMIE_OJCA) }&lt;/m:ImieOjca&gt;
					else ()
			}
			{
				if($fml/fml:DC_NAZWISKO_PANIENSKIE_MATKI)
					then &lt;m:NazwiskoPanienskieMatki&gt;{ data($fml/fml:DC_NAZWISKO_PANIENSKIE_MATKI) }&lt;/m:NazwiskoPanienskieMatki&gt;
					else ()
			}
			{
				if($fml/fml:DC_DATA_URODZENIA)
					then &lt;m:DataUrodzenia&gt;{ data($fml/fml:DC_DATA_URODZENIA) }&lt;/m:DataUrodzenia&gt;
					else ()
			}
			{
				if($fml/fml:DC_MIEJSCE_URODZENIA)
					then &lt;m:MiejsceUrodzenia&gt;{ data($fml/fml:DC_MIEJSCE_URODZENIA) }&lt;/m:MiejsceUrodzenia&gt;
					else ()
			}
			{
				if($fml/fml:DC_STAN_CYWILNY)
					then &lt;m:StanCywilny&gt;{ data($fml/fml:DC_STAN_CYWILNY) }&lt;/m:StanCywilny&gt;
					else ()
			}
			{
				if($fml/fml:DC_KOD_JEZYKA)
					then &lt;m:KodJezyka&gt;{ data($fml/fml:DC_KOD_JEZYKA) }&lt;/m:KodJezyka&gt;
					else ()
			}
			{
				if($fml/fml:CI_HASLO_W_CC)
					then &lt;m:HasloWCc&gt;{ data($fml/fml:CI_HASLO_W_CC) }&lt;/m:HasloWCc&gt;
					else ()
			}
			{
				if($fml/fml:CI_UCZEN)
					then &lt;m:Uczen&gt;{ data($fml/fml:CI_UCZEN) }&lt;/m:Uczen&gt;
					else ()
			}
			{
				if($fml/fml:DC_UCZELNIA)
					then &lt;m:Uczelnia&gt;{ data($fml/fml:DC_UCZELNIA) }&lt;/m:Uczelnia&gt;
					else ()
			}
			{
				if($fml/fml:DC_PLANOW_DATA_UKON_SZK)
					then &lt;m:PlanowDataUkonSzk&gt;{ data($fml/fml:DC_PLANOW_DATA_UKON_SZK) }&lt;/m:PlanowDataUkonSzk&gt;
					else ()
			}
			{
				if($fml/fml:CI_STANOWISKO)
					then &lt;m:Stanowisko&gt;{ data($fml/fml:CI_STANOWISKO) }&lt;/m:Stanowisko&gt;
					else ()
			}
			{
				if($fml/fml:DC_SLUZBA_WOJSKOWA)
					then &lt;m:SluzbaWojskowa&gt;{ data($fml/fml:DC_SLUZBA_WOJSKOWA) }&lt;/m:SluzbaWojskowa&gt;
					else ()
			}
			{
				if($fml/fml:CI_ZATRUDNIENIE)
					then &lt;m:Zatrudnienie&gt;{ data($fml/fml:CI_ZATRUDNIENIE) }&lt;/m:Zatrudnienie&gt;
					else ()
			}
			{
				if($fml/fml:DC_WSPOLNOTA_MAJATK)
					then &lt;m:WspolnotaMajatk&gt;{ data($fml/fml:DC_WSPOLNOTA_MAJATK) }&lt;/m:WspolnotaMajatk&gt;
					else ()
			}
			{
				if($fml/fml:DC_LICZBA_OS_WE_WSP_GOSP_D)
					then &lt;m:LiczbaOsWeWspGospD&gt;{ data($fml/fml:DC_LICZBA_OS_WE_WSP_GOSP_D) }&lt;/m:LiczbaOsWeWspGospD&gt;
					else ()
			}
			{
				if($fml/fml:DC_LICZBA_OSOB_NA_UTRZ)
					then &lt;m:LiczbaOsobNaUtrz&gt;{ data($fml/fml:DC_LICZBA_OSOB_NA_UTRZ) }&lt;/m:LiczbaOsobNaUtrz&gt;
					else ()
			}
			{
				if($fml/fml:DC_POSIADA_DOM_MIESZK)
					then &lt;m:PosiadaDomMieszk&gt;{ data($fml/fml:DC_POSIADA_DOM_MIESZK) }&lt;/m:PosiadaDomMieszk&gt;
					else ()
			}
			{
				if($fml/fml:DC_WARUNKI_MIESZKANIOWE)
					then &lt;m:WarunkiMieszkaniowe&gt;{ data($fml/fml:DC_WARUNKI_MIESZKANIOWE) }&lt;/m:WarunkiMieszkaniowe&gt;
					else ()
			}
			{
				if($fml/fml:CI_ZAM_LOKAL)
					then &lt;m:ZamLokal&gt;{ data($fml/fml:CI_ZAM_LOKAL) }&lt;/m:ZamLokal&gt;
					else ()
			}
			{
				if($fml/fml:CI_ZAM_LOKAL_INNE)
					then &lt;m:ZamLokalInne&gt;{ data($fml/fml:CI_ZAM_LOKAL_INNE) }&lt;/m:ZamLokalInne&gt;
					else ()
			}
			{
				if($fml/fml:CI_LOKAL_OBC_HIPOTEKA)
					then &lt;m:LokalObcHipoteka&gt;{ data($fml/fml:CI_LOKAL_OBC_HIPOTEKA) }&lt;/m:LokalObcHipoteka&gt;
					else ()
			}
			{
				if($fml/fml:CI_WARTOSC_HIPOTEKI)
					then &lt;m:WartoscHipoteki&gt;{ data($fml/fml:CI_WARTOSC_HIPOTEKI) }&lt;/m:WartoscHipoteki&gt;
					else ()
			}
			{
				if($fml/fml:DC_WYKSZTALCENIE)
					then &lt;m:Wyksztalcenie&gt;{ data($fml/fml:DC_WYKSZTALCENIE) }&lt;/m:Wyksztalcenie&gt;
					else ()
			}
			{
				if($fml/fml:DC_KOD_ZAWODU)
					then &lt;m:KodZawodu&gt;{ data($fml/fml:DC_KOD_ZAWODU) }&lt;/m:KodZawodu&gt;
					else ()
			}
			{
				if($fml/fml:DC_MIEJSCE_ZATRUDNIENIA)
					then &lt;m:MiejsceZatrudnienia&gt;{ data($fml/fml:DC_MIEJSCE_ZATRUDNIENIA) }&lt;/m:MiejsceZatrudnienia&gt;
					else ()
			}
			{
				if($fml/fml:DC_MIES_DOCHOD_NETTO_W_PLN)
					then &lt;m:MiesDochodNettoWPln&gt;{ data($fml/fml:DC_MIES_DOCHOD_NETTO_W_PLN) }&lt;/m:MiesDochodNettoWPln&gt;
					else ()
			}
			{
				if($fml/fml:CI_SPOS_OTRZYM_DOCH)
					then &lt;m:SposOtrzymDoch&gt;{ data($fml/fml:CI_SPOS_OTRZYM_DOCH) }&lt;/m:SposOtrzymDoch&gt;
					else ()
			}
			{
				if($fml/fml:CI_DOD_DOCHOD_MIES_NETTO)
					then &lt;m:DodDochodMiesNetto&gt;{ data($fml/fml:CI_DOD_DOCHOD_MIES_NETTO) }&lt;/m:DodDochodMiesNetto&gt;
					else ()
			}
			{
				if($fml/fml:CI_ZRODLO_DODAT_DOCH)
					then &lt;m:ZrodloDodatDoch&gt;{ data($fml/fml:CI_ZRODLO_DODAT_DOCH) }&lt;/m:ZrodloDodatDoch&gt;
					else ()
			}
			{
				if($fml/fml:CI_RODZAJ_OBCIAZENIA)
					then &lt;m:RodzajObciazenia&gt;{ data($fml/fml:CI_RODZAJ_OBCIAZENIA) }&lt;/m:RodzajObciazenia&gt;
					else ()
			}
			{
				if($fml/fml:CI_RODZAJ_OBC_INNE)
					then &lt;m:RodzajObcInne&gt;{ data($fml/fml:CI_RODZAJ_OBC_INNE) }&lt;/m:RodzajObcInne&gt;
					else ()
			}
			{
				if($fml/fml:CI_LACZNA_WYS_OBC)
					then &lt;m:LacznaWysObc&gt;{ data($fml/fml:CI_LACZNA_WYS_OBC) }&lt;/m:LacznaWysObc&gt;
					else ()
			}
			{
				if($fml/fml:CI_POLISY_NA_ZYCIE)
					then &lt;m:PolisyNaZycie&gt;{ data($fml/fml:CI_POLISY_NA_ZYCIE) }&lt;/m:PolisyNaZycie&gt;
					else ()
			}
			{
				if($fml/fml:CI_LICZBA_POLIS)
					then &lt;m:LiczbaPolis&gt;{ data($fml/fml:CI_LICZBA_POLIS) }&lt;/m:LiczbaPolis&gt;
					else ()
			}
			{
				if($fml/fml:CI_NAZWA_TOW_UBEZP)
					then &lt;m:NazwaTowUbezp&gt;{ data($fml/fml:CI_NAZWA_TOW_UBEZP) }&lt;/m:NazwaTowUbezp&gt;
					else ()
			}
			{
				if($fml/fml:CI_SUMA_POLIS_NA_ZYCIE)
					then &lt;m:SumaPolisNaZycie&gt;{ data($fml/fml:CI_SUMA_POLIS_NA_ZYCIE) }&lt;/m:SumaPolisNaZycie&gt;
					else ()
			}
			{
				if($fml/fml:CI_AKTUALNE_ZADL_DZIALAL)
					then &lt;m:AktualneZadlDzialal&gt;{ data($fml/fml:CI_AKTUALNE_ZADL_DZIALAL) }&lt;/m:AktualneZadlDzialal&gt;
					else ()
			}
			{
				if($fml/fml:DC_KOD_EKD_PRACODAW)
					then &lt;m:KodEkdPracodaw&gt;{ data($fml/fml:DC_KOD_EKD_PRACODAW) }&lt;/m:KodEkdPracodaw&gt;
					else ()
			}
			{
				if($fml/fml:DC_DATA_PODJECIA_PRACY)
					then &lt;m:DataPodjeciaPracy&gt;{ data($fml/fml:DC_DATA_PODJECIA_PRACY) }&lt;/m:DataPodjeciaPracy&gt;
					else ()
			}
			{
				if($fml/fml:DC_DATA_SMIERCI)
					then &lt;m:DataSmierci&gt;{ data($fml/fml:DC_DATA_SMIERCI) }&lt;/m:DataSmierci&gt;
					else ()
			}
			{
				if($fml/fml:DC_UBEZP_OD_NASTEPSTW_NW)
					then &lt;m:UbezpOdNastepstwNw&gt;{ data($fml/fml:DC_UBEZP_OD_NASTEPSTW_NW) }&lt;/m:UbezpOdNastepstwNw&gt;
					else ()
			}
			{
				if($fml/fml:DC_UBEZPIECZENIE_EMERYTALNE)
					then &lt;m:UbezpieczenieEmerytalne&gt;{ data($fml/fml:DC_UBEZPIECZENIE_EMERYTALNE) }&lt;/m:UbezpieczenieEmerytalne&gt;
					else ()
			}
			{
				if($fml/fml:DC_UBEZP_MAJATKOWE_MIES)
					then &lt;m:UbezpMajatkoweMies&gt;{ data($fml/fml:DC_UBEZP_MAJATKOWE_MIES) }&lt;/m:UbezpMajatkoweMies&gt;
					else ()
			}
			{
				if($fml/fml:DC_UBEZPIECZENIA_INNE)
					then &lt;m:UbezpieczeniaInne&gt;{ data($fml/fml:DC_UBEZPIECZENIA_INNE) }&lt;/m:UbezpieczeniaInne&gt;
					else ()
			}
			{
				if($fml/fml:CI_KRS)
					then &lt;m:Krs&gt;{ data($fml/fml:CI_KRS) }&lt;/m:Krs&gt;
					else ()
			}
			{
				if($fml/fml:DC_EKD)
					then &lt;m:Ekd&gt;{ data($fml/fml:DC_EKD) }&lt;/m:Ekd&gt;
					else ()
			}
			{
				if($fml/fml:CI_LICZBA_PLACOWEK)
					then &lt;m:LiczbaPlacowek&gt;{ data($fml/fml:CI_LICZBA_PLACOWEK) }&lt;/m:LiczbaPlacowek&gt;
					else ()
			}
			{
				if($fml/fml:DC_LICZBA_ZATRUDNIONYCH)
					then &lt;m:LiczbaZatrudnionych&gt;{ data($fml/fml:DC_LICZBA_ZATRUDNIONYCH) }&lt;/m:LiczbaZatrudnionych&gt;
					else ()
			}
			{
				if($fml/fml:DC_NAZWISKO_OSOBY_KONTAKTOWEJ)
					then &lt;m:NazwiskoOsobyKontaktowej&gt;{ data($fml/fml:DC_NAZWISKO_OSOBY_KONTAKTOWEJ) }&lt;/m:NazwiskoOsobyKontaktowej&gt;
					else ()
			}
			{
				if($fml/fml:DC_TYTUL_OSOBY_KONTAKTOWEJ)
					then &lt;m:TytulOsobyKontaktowej&gt;{ data($fml/fml:DC_TYTUL_OSOBY_KONTAKTOWEJ) }&lt;/m:TytulOsobyKontaktowej&gt;
					else ()
			}
			{
				if($fml/fml:DC_DATA_BANKRUCTWA)
					then &lt;m:DataBankructwa&gt;{ data($fml/fml:DC_DATA_BANKRUCTWA) }&lt;/m:DataBankructwa&gt;
					else ()
			}
			{
				if($fml/fml:CI_STRONA_WWW)
					then &lt;m:StronaWww&gt;{ data($fml/fml:CI_STRONA_WWW) }&lt;/m:StronaWww&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_ROZP_DZIAL)
					then &lt;m:DataRozpDzial&gt;{ data($fml/fml:CI_DATA_ROZP_DZIAL) }&lt;/m:DataRozpDzial&gt;
					else ()
			}
			{
				if($fml/fml:CI_DANE_FINANSOWE)
					then &lt;m:DaneFinansowe&gt;{ data($fml/fml:CI_DANE_FINANSOWE) }&lt;/m:DaneFinansowe&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_SPRAWOZDANIA)
					then &lt;m:DataSprawozdania&gt;{ data($fml/fml:CI_DATA_SPRAWOZDANIA) }&lt;/m:DataSprawozdania&gt;
					else ()
			}
			{
				if($fml/fml:CI_KWOTA_ZADLUZENIA)
					then &lt;m:KwotaZadluzenia&gt;{ data($fml/fml:CI_KWOTA_ZADLUZENIA) }&lt;/m:KwotaZadluzenia&gt;
					else ()
			}
			{
				if($fml/fml:DC_JEDNOSTKA_KORPORACYJNA)
					then &lt;m:JednostkaKorporacyjna&gt;{ data($fml/fml:DC_JEDNOSTKA_KORPORACYJNA) }&lt;/m:JednostkaKorporacyjna&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRZYNAL_CENTR_KORP)
					then &lt;m:PrzynalCentrKorp&gt;{ data($fml/fml:CI_PRZYNAL_CENTR_KORP) }&lt;/m:PrzynalCentrKorp&gt;
					else ()
			}
			{
				if($fml/fml:CI_TYP_PODMIOTU)
					then &lt;m:TypPodmiotu&gt;{ data($fml/fml:CI_TYP_PODMIOTU) }&lt;/m:TypPodmiotu&gt;
					else ()
			}
			{
				if($fml/fml:CI_NUMER_SWIFT)
					then &lt;m:NumerSwift&gt;{ data($fml/fml:CI_NUMER_SWIFT) }&lt;/m:NumerSwift&gt;
					else ()
			}
			{
				if($fml/fml:CI_NUMER_W_REJESTRZE)
					then &lt;m:NumerWRejestrze&gt;{ data($fml/fml:CI_NUMER_W_REJESTRZE) }&lt;/m:NumerWRejestrze&gt;
					else ()
			}
			{
				if($fml/fml:CI_NAZWA_ORG_REJESTR)
					then &lt;m:NazwaOrgRejestr&gt;{ data($fml/fml:CI_NAZWA_ORG_REJESTR) }&lt;/m:NazwaOrgRejestr&gt;
					else ()
			}
			{
				if($fml/fml:CI_KRAJ_ORG_REJESTR)
					then &lt;m:KrajOrgRejestr&gt;{ data($fml/fml:CI_KRAJ_ORG_REJESTR) }&lt;/m:KrajOrgRejestr&gt;
					else ()
			}
			{
				if($fml/fml:CI_MIASTO_ORG_REJESTR)
					then &lt;m:MiastoOrgRejestr&gt;{ data($fml/fml:CI_MIASTO_ORG_REJESTR) }&lt;/m:MiastoOrgRejestr&gt;
					else ()
			}
			{
				if($fml/fml:CI_KOD_POCZT_ORG_REJESTR)
					then &lt;m:KodPocztOrgRejestr&gt;{ data($fml/fml:CI_KOD_POCZT_ORG_REJESTR) }&lt;/m:KodPocztOrgRejestr&gt;
					else ()
			}
			{
				if($fml/fml:CI_ULICA_ORG_REJESTR)
					then &lt;m:UlicaOrgRejestr&gt;{ data($fml/fml:CI_ULICA_ORG_REJESTR) }&lt;/m:UlicaOrgRejestr&gt;
					else ()
			}
			{
				if($fml/fml:CI_NR_POSES_LOKALU_ORG_REJ)
					then &lt;m:NrPosesLokaluOrgRej&gt;{ data($fml/fml:CI_NR_POSES_LOKALU_ORG_REJ) }&lt;/m:NrPosesLokaluOrgRej&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_WPISU_DO_REJESTR)
					then &lt;m:DataWpisuDoRejestr&gt;{ data($fml/fml:CI_DATA_WPISU_DO_REJESTR) }&lt;/m:DataWpisuDoRejestr&gt;
					else ()
			}
			{
				if($fml/fml:CI_STATUS_SIEDZIBY)
					then &lt;m:StatusSiedziby&gt;{ data($fml/fml:CI_STATUS_SIEDZIBY) }&lt;/m:StatusSiedziby&gt;
					else ()
			}
			{
				if($fml/fml:CI_AUDYTOR)
					then &lt;m:Audytor&gt;{ data($fml/fml:CI_AUDYTOR) }&lt;/m:Audytor&gt;
					else ()
			}
			{
				if($fml/fml:CI_KAPITAL_ZALOZYCIELSKI)
					then &lt;m:KapitalZalozycielski&gt;{ data($fml/fml:CI_KAPITAL_ZALOZYCIELSKI) }&lt;/m:KapitalZalozycielski&gt;
					else ()
			}
			{
				if($fml/fml:CI_WARTOSC_AKCJI)
					then &lt;m:WartoscAkcji&gt;{ data($fml/fml:CI_WARTOSC_AKCJI) }&lt;/m:WartoscAkcji&gt;
					else ()
			}
			{
				if($fml/fml:CI_WARTOSC_NIERUCHOMOSCI)
					then &lt;m:WartoscNieruchomosci&gt;{ data($fml/fml:CI_WARTOSC_NIERUCHOMOSCI) }&lt;/m:WartoscNieruchomosci&gt;
					else ()
			}
			{
				if($fml/fml:CI_WARTOSC_SRODKOW_MATER)
					then &lt;m:WartoscSrodkowMater&gt;{ data($fml/fml:CI_WARTOSC_SRODKOW_MATER) }&lt;/m:WartoscSrodkowMater&gt;
					else ()
			}
			{
				if($fml/fml:CI_INNE_AKTYWA)
					then &lt;m:InneAktywa&gt;{ data($fml/fml:CI_INNE_AKTYWA) }&lt;/m:InneAktywa&gt;
					else ()
			}
			{
				if($fml/fml:CI_ZOBOWIAZANIA_PODATKOWE)
					then &lt;m:ZobowiazaniaPodatkowe&gt;{ data($fml/fml:CI_ZOBOWIAZANIA_PODATKOWE) }&lt;/m:ZobowiazaniaPodatkowe&gt;
					else ()
			}
			{
				if($fml/fml:CI_INNE_ZOBOWIAZANIA)
					then &lt;m:InneZobowiazania&gt;{ data($fml/fml:CI_INNE_ZOBOWIAZANIA) }&lt;/m:InneZobowiazania&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_AKTUAL_DANYCH_FIN)
					then &lt;m:DataAktualDanychFin&gt;{ data($fml/fml:CI_DATA_AKTUAL_DANYCH_FIN) }&lt;/m:DataAktualDanychFin&gt;
					else ()
			}
			{
                          for $x at $occ in $fml/CI_TYP_ADRESU where data($x) = "2"
                                  return
                                     &lt;m:CISGetCustomerCustomerAdresAlt&gt;
						&lt;m:ImieINazwiskoAlt&gt;{ data($fml/fml:DC_IMIE_I_NAZWISKO_ALT[$occ]) }&lt;/m:ImieINazwiskoAlt&gt;
				                &lt;m:UlicaAdresAlt&gt;{ data($fml/fml:DC_ULICA_ADRES_ALT[$occ]) }&lt;/m:UlicaAdresAlt&gt;
						&lt;m:NrPosesLokaluAdresAlt&gt;{ data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT[$occ]) }&lt;/m:NrPosesLokaluAdresAlt&gt;
						&lt;m:MiastoAdresAlt&gt;{ data($fml/fml:DC_MIASTO_ADRES_ALT[$occ]) }&lt;/m:MiastoAdresAlt&gt;
						&lt;m:KodPocztowyAdresAlt&gt;{ data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT[$occ]) }&lt;/m:KodPocztowyAdresAlt&gt;
						&lt;m:WojewodztwoKrajAdresAlt&gt;{ data($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$occ]) }&lt;/m:WojewodztwoKrajAdresAlt&gt;
				      &lt;/m:CISGetCustomerCustomerAdresAlt&gt;
			}


		&lt;/m:CISGetCustomerResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCISGetCustomerResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>