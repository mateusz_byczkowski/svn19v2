<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/esbmessages/";
declare namespace xf = "http://bzwbk.com/services/esbmessages/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISGetCustByProdResponse($fml as element(fml:FML32))
	as element(m:CISGetCustByProdResponse) {
		&lt;m:CISGetCustByProdResponse&gt;
			{
				if($fml/fml:CI_PRODUCT_AREA_ID)
					then &lt;m:CI_PRODUCT_AREA_ID&gt;{ data($fml/fml:CI_PRODUCT_AREA_ID) }&lt;/m:CI_PRODUCT_AREA_ID&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_CATEGORY_ID)
					then &lt;m:CI_PRODUCT_CATEGORY_ID&gt;{ data($fml/fml:CI_PRODUCT_CATEGORY_ID) }&lt;/m:CI_PRODUCT_CATEGORY_ID&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ID)
					then &lt;m:CI_PRODUCT_ID&gt;{ data($fml/fml:CI_PRODUCT_ID) }&lt;/m:CI_PRODUCT_ID&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_CODE)
					then &lt;m:CI_PRODUCT_CODE&gt;{ data($fml/fml:CI_PRODUCT_CODE) }&lt;/m:CI_PRODUCT_CODE&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_NAME_EN)
					then &lt;m:CI_PRODUCT_NAME_EN&gt;{ data($fml/fml:CI_PRODUCT_NAME_EN) }&lt;/m:CI_PRODUCT_NAME_EN&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_NAME_PL)
					then &lt;m:CI_PRODUCT_NAME_PL&gt;{ data($fml/fml:CI_PRODUCT_NAME_PL) }&lt;/m:CI_PRODUCT_NAME_PL&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_RACHUNKU)
					then &lt;m:DC_NR_RACHUNKU&gt;{ data($fml/fml:DC_NR_RACHUNKU) }&lt;/m:DC_NR_RACHUNKU&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_RACHUNKU_W_SYSTEMIE)
					then &lt;m:DC_NR_ALTERNATYWNY&gt;{ data($fml/fml:DC_NR_RACHUNKU_W_SYSTEMIE) }&lt;/m:DC_NR_ALTERNATYWNY&gt;
					else ()
			}
			{
				if($fml/fml:B_POJEDYNCZY)
					then  if(data($fml/fml:B_POJEDYNCZY)="T" or data($fml/fml:B_POJEDYNCZY)="N") 
                                                     then &lt;m:B_POJEDYNCZY&gt;{ data($fml/fml:B_POJEDYNCZY) }&lt;/m:B_POJEDYNCZY&gt;
					         else &lt;m:B_POJEDYNCZY&gt;N&lt;/m:B_POJEDYNCZY&gt;
                                        else &lt;m:B_POJEDYNCZY&gt;N&lt;/m:B_POJEDYNCZY&gt;
			}
			{
				if($fml/fml:CI_PRODUCT_ATT1)
					then &lt;m:CI_PRODUCT_ATT1&gt;{ data($fml/fml:CI_PRODUCT_ATT1) }&lt;/m:CI_PRODUCT_ATT1&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT2)
					then &lt;m:CI_PRODUCT_ATT2&gt;{ data($fml/fml:CI_PRODUCT_ATT2) }&lt;/m:CI_PRODUCT_ATT2&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT3)
					then &lt;m:CI_PRODUCT_ATT3&gt;{ data($fml/fml:CI_PRODUCT_ATT3) }&lt;/m:CI_PRODUCT_ATT3&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT4)
					then &lt;m:CI_PRODUCT_ATT4&gt;{ data($fml/fml:CI_PRODUCT_ATT4) }&lt;/m:CI_PRODUCT_ATT4&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT5)
					then &lt;m:CI_PRODUCT_ATT5&gt;{ data($fml/fml:CI_PRODUCT_ATT5) }&lt;/m:CI_PRODUCT_ATT5&gt;
					else ()
			}
			{
 			if($fml/fml:DC_NUMER_KLIENTA)
			   then &lt;m:DC_NUMER_KLIENTA&gt;{ data($fml/fml:DC_NUMER_KLIENTA) }&lt;/m:DC_NUMER_KLIENTA&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:CI_NAZWA_PELNA)
			   then &lt;m:CI_NAZWA_PELNA&gt;{ data($fml/fml:CI_NAZWA_PELNA) }&lt;/m:CI_NAZWA_PELNA&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_ULICA_DANE_PODST)
			   then &lt;m:DC_ULICA_DANE_PODST&gt;{ data($fml/fml:DC_ULICA_DANE_PODST) }&lt;/m:DC_ULICA_DANE_PODST&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST)
			   then &lt;m:DC_NR_POSES_LOKALU_DANE_PODST&gt;{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST) }&lt;/m:DC_NR_POSES_LOKALU_DANE_PODST&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_MIASTO_DANE_PODST)
			   then &lt;m:DC_MIASTO_DANE_PODST&gt;{ data($fml/fml:DC_MIASTO_DANE_PODST) }&lt;/m:DC_MIASTO_DANE_PODST&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_WOJ_KRAJ_DANE_PODST)
			   then &lt;m:DC_WOJ_KRAJ_DANE_PODST&gt;{ data($fml/fml:DC_WOJ_KRAJ_DANE_PODST) }&lt;/m:DC_WOJ_KRAJ_DANE_PODST&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NUMER_ODDZIALU)
			   then &lt;m:DC_NUMER_ODDZIALU&gt;{ data($fml/fml:DC_NUMER_ODDZIALU) }&lt;/m:DC_NUMER_ODDZIALU&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_IMIE)
			   then &lt;m:DC_IMIE&gt;{ data($fml/fml:DC_IMIE) }&lt;/m:DC_IMIE&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NAZWISKO)
			   then &lt;m:DC_NAZWISKO&gt;{ data($fml/fml:DC_NAZWISKO) }&lt;/m:DC_NAZWISKO&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:CI_STATUS_GIODO)
			   then &lt;m:CI_STATUS_GIODO&gt;{ data($fml/fml:CI_STATUS_GIODO) }&lt;/m:CI_STATUS_GIODO&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:CI_UDOSTEP_GRUPA)
			   then &lt;m:CI_UDOSTEP_GRUPA&gt;{ data($fml/fml:CI_UDOSTEP_GRUPA) }&lt;/m:CI_UDOSTEP_GRUPA&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NR_DOWODU_REGON)
			   then &lt;m:DC_NR_DOWODU_REGON&gt;{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:DC_NR_DOWODU_REGON&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NR_PESEL)
			   then &lt;m:DC_NR_PESEL&gt;{ data($fml/fml:DC_NR_PESEL) }&lt;/m:DC_NR_PESEL&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:CI_WIECEJ_ADDR_KORESP)
			   then &lt;m:CI_WIECEJ_ADR_KORESP&gt;{ data($fml/fml:CI_WIECEJ_ADDR_KORESP) }&lt;/m:CI_WIECEJ_ADR_KORESP&gt;
			   else &lt;m:CI_WIECEJ_ADR_KORESP&gt;N&lt;/m:CI_WIECEJ_ADR_KORESP&gt;
		   	} 
			{
 			if($fml/fml:DC_IMIE_I_NAZWISKO_ALT)
			   then &lt;m:DC_IMIE_I_NAZWISKO_ALT&gt;{ data($fml/fml:DC_IMIE_I_NAZWISKO_ALT) }&lt;/m:DC_IMIE_I_NAZWISKO_ALT&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_ULICA_ADRES_ALT)
			   then &lt;m:DC_ULICA_ADRES_ALT&gt;{ data($fml/fml:DC_ULICA_ADRES_ALT) }&lt;/m:DC_ULICA_ADRES_ALT&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT)
			   then &lt;m:DC_NR_POSES_LOKALU_ADRES_ALT&gt;{ data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT) }&lt;/m:DC_NR_POSES_LOKALU_ADRES_ALT&gt;
			   else ()
		   	} 
			{
 			if($fml/fml:DC_MIASTO_ADRES_ALT)
			   then &lt;m:DC_MIASTO_ADRES_ALT&gt;{ data($fml/fml:DC_MIASTO_ADRES_ALT) }&lt;/m:DC_MIASTO_ADRES_ALT&gt;
			   else ()
		   	} 
		        {
 			if($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT)
			   then &lt;m:DC_KOD_POCZTOWY_ADRES_ALT&gt;{ data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT) }&lt;/m:DC_KOD_POCZTOWY_ADRES_ALT&gt;
			   else ()
		   	} 
		        {
 			if($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT)
			   then &lt;m:DC_WOJEWODZTWO_KRAJ_ADRES_ALT&gt;{ data($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT) }&lt;/m:DC_WOJEWODZTWO_KRAJ_ADRES_ALT&gt;
			   else ()
		   	} 
		        {
 			if($fml/fml:DC_NR_TELEFONU_KOMORKOWEGO)
			   then &lt;m:DC_NR_TELEFONU_KOMORKOWEGO&gt;{ data($fml/fml:DC_NR_TELEFONU_KOMORKOWEGO) }&lt;/m:DC_NR_TELEFONU_KOMORKOWEGO&gt;
			   else ()
		   	} 
		        {
 			if($fml/fml:DC_NR_TELEFONU)
			   then &lt;m:DC_NR_TELEFONU&gt;{ data($fml/fml:DC_NR_TELEFONU) }&lt;/m:DC_NR_TELEFONU&gt;
			   else ()
		   	} 
		        {
 			if($fml/fml:DC_ADRES_EMAIL)
			   then &lt;m:DC_ADRES_EMAIL&gt;{ data($fml/fml:DC_ADRES_EMAIL) }&lt;/m:DC_ADRES_EMAIL&gt;
			   else ()
		   	} 
		        {
 			if($fml/fml:CI_RELACJA)
			   then &lt;m:CI_RELACJA&gt;{ data($fml/fml:CI_RELACJA) }&lt;/m:CI_RELACJA&gt;
			   else ()
		   	} 
		        {
 			if($fml/fml:CI_KLASA_OBSLUGI)
			   then &lt;m:CI_KLASA_OBSLUGI&gt;{ data($fml/fml:CI_KLASA_OBSLUGI) }&lt;/m:CI_KLASA_OBSLUGI&gt;
			   else ()
		   	} 
		        
 
                        
                         
		&lt;/m:CISGetCustByProdResponse&gt;
};
declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCISGetCustByProdResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>