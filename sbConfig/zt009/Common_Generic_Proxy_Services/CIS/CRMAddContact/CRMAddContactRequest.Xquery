<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddContactRequest($req as element(m:CRMAddContactRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA&gt;{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA&gt;
					else ()
			}
			{
				if($req/m:SkpPracownikaRej)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ&gt;{ data($req/m:SkpPracownikaRej) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ&gt;
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				if($req/m:NumerKlientaWew)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlientaWew) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:CI_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:CI_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:DataKont)
					then &lt;fml:CI_DATA_KONT&gt;{ data($req/m:DataKont) }&lt;/fml:CI_DATA_KONT&gt;
					else ()
			}
			{
				if($req/m:CelKont)
					then &lt;fml:CI_CEL_KONT&gt;{ data($req/m:CelKont) }&lt;/fml:CI_CEL_KONT&gt;
					else ()
			}
			{
				if($req/m:TypKont)
					then &lt;fml:CI_TYP_KONT&gt;{ data($req/m:TypKont) }&lt;/fml:CI_TYP_KONT&gt;
					else ()
			}
			{
				if($req/m:RodzajKont)
					then &lt;fml:CI_RODZAJ_KONT&gt;{ data($req/m:RodzajKont) }&lt;/fml:CI_RODZAJ_KONT&gt;
					else ()
			}
			{
				if($req/m:IdZws)
					then &lt;fml:CI_ID_ZWS&gt;{ data($req/m:IdZws) }&lt;/fml:CI_ID_ZWS&gt;
					else ()
			}
			{
				if($req/m:IdKamp)
					then &lt;fml:CI_ID_KAMP&gt;{ data($req/m:IdKamp) }&lt;/fml:CI_ID_KAMP&gt;
					else ()
			}
			{
				if($req/m:SposKont)
					then &lt;fml:CI_SPOS_KONT&gt;{ data($req/m:SposKont) }&lt;/fml:CI_SPOS_KONT&gt;
					else ()
			}
			{
				if($req/m:MiejsceSpotkania)
					then &lt;fml:CI_MIEJSCE_SPOTKANIA&gt;{ data($req/m:MiejsceSpotkania) }&lt;/fml:CI_MIEJSCE_SPOTKANIA&gt;
					else ()
			}
			{
				if($req/m:WynikKontaktu)
					then &lt;fml:CI_WYNIK_KONTAKTU&gt;{ data($req/m:WynikKontaktu) }&lt;/fml:CI_WYNIK_KONTAKTU&gt;
					else ()
			}
			{
				if($req/m:NotatKont)
					then &lt;fml:CI_NOTAT_KONT&gt;{ data($req/m:NotatKont) }&lt;/fml:CI_NOTAT_KONT&gt;
					else ()
			}
			{
				if($req/m:Produkty)
					then &lt;fml:CI_PRODUKTY&gt;{ data($req/m:Produkty) }&lt;/fml:CI_PRODUKTY&gt;
					else ()
			}
			{
				if($req/m:NrOddzialu)
					then &lt;fml:CI_NR_ODDZIALU&gt;{ data($req/m:NrOddzialu) }&lt;/fml:CI_NR_ODDZIALU&gt;
					else ()
			}
			{
				if($req/m:IdZadania)
					then &lt;fml:CI_ID_ZADANIA&gt;{ data($req/m:IdZadania) }&lt;/fml:CI_ID_ZADANIA&gt;
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA&gt;{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMAddContactRequest($body/m:CRMAddContactRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>