<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductDetail($fml as element(fml:FML32))
	as element(m:PRIMEGetCustProductDetail) {
		&lt;m:PRIMEGetCustProductDetail&gt;
			{
				if($fml/fml:CI_ID_WEW_PRAC)
					then &lt;m:CI_ID_WEW_PRAC&gt;{ data($fml/fml:CI_ID_WEW_PRAC) }&lt;/m:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($fml/fml:CI_ID_SPOLKI)
					then &lt;m:CI_ID_SPOLKI&gt;{ data($fml/fml:CI_ID_SPOLKI) }&lt;/m:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($fml/fml:DC_NUMER_KLIENTA)
					then &lt;m:DC_NUMER_KLIENTA&gt;{ data($fml/fml:DC_NUMER_KLIENTA) }&lt;/m:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then &lt;m:DC_NR_DOWODU_REGON&gt;{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:DC_NR_DOWODU_REGON&gt;
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then &lt;m:DC_NIP&gt;{ data($fml/fml:DC_NIP) }&lt;/m:DC_NIP&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then &lt;m:DC_NR_PESEL&gt;{ data($fml/fml:DC_NR_PESEL) }&lt;/m:DC_NR_PESEL&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_RACHUNKU_W_SYSTEMIE and string-length(data($fml/fml:DC_NR_RACHUNKU_W_SYSTEMIE))&gt;0)
					then &lt;m:DC_NR_RACHUNKU&gt;{ concat("V",data($fml/fml:DC_NR_RACHUNKU_W_SYSTEMIE)) }&lt;/m:DC_NR_RACHUNKU&gt;
					else  &lt;m:DC_NR_RACHUNKU&gt;{ data($fml/fml:DC_NR_RACHUNKU) }&lt;/m:DC_NR_RACHUNKU&gt;
			}
			{
				if($fml/fml:CI_OPCJA)
					then &lt;m:CI_OPCJA&gt;{ data($fml/fml:CI_OPCJA) }&lt;/m:CI_OPCJA&gt;
					else ()
			}
			{
				if($fml/fml:CI_ID_SYS)
					then &lt;m:CI_ID_SYS&gt;{ data($fml/fml:CI_ID_SYS) }&lt;/m:CI_ID_SYS&gt;
					else ()
			}
		&lt;/m:PRIMEGetCustProductDetail&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapPRIMEGetCustProductDetail($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>