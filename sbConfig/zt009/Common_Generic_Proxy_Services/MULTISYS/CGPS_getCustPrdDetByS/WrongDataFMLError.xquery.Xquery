<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace fml="";

declare variable $body external;


&lt;soap-env:Body&gt;
            &lt;FML32&gt; 
              &lt;TPAERRNO&gt;11&lt;/TPAERRNO&gt;
              &lt;TPAURCODE&gt;103&lt;/TPAURCODE&gt;
            &lt;/FML32&gt; 
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>