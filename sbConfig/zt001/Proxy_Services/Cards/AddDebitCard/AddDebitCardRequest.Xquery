<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbICBSAddDebitCardRequest($req as element(m:bICBSAddDebitCardRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:TrnId)
					then &lt;fml:DC_TRN_ID>{ data($req/m:TrnId) }&lt;/fml:DC_TRN_ID>
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK>{ data($req/m:Uzytkownik) }&lt;/fml:DC_UZYTKOWNIK>
					else ()
			}
			{
				if($req/m:Oddzial)
					then &lt;fml:DC_ODDZIAL>{ data($req/m:Oddzial) }&lt;/fml:DC_ODDZIAL>
					else ()
			}
			{
				if($req/m:NrKartyBin)
					then &lt;fml:DC_NR_KARTY_BIN>{ data($req/m:NrKartyBin) }&lt;/fml:DC_NR_KARTY_BIN>
					else ()
			}
			{
				if($req/m:TypKarty)
					then &lt;fml:DC_TYP_KARTY>{ data($req/m:TypKarty) }&lt;/fml:DC_TYP_KARTY>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:Rachunek)
					then &lt;fml:DC_RACHUNEK>{ data($req/m:Rachunek) }&lt;/fml:DC_RACHUNEK>
					else ()
			}
			{
				if($req/m:WytloczoneNaKarcieI)
					then &lt;fml:DC_WYTLOCZONE_NA_KARCIE_I>{ data($req/m:WytloczoneNaKarcieI) }&lt;/fml:DC_WYTLOCZONE_NA_KARCIE_I>
					else ()
			}
			{
				if($req/m:WytloczoneNaKarcieIi)
					then &lt;fml:DC_WYTLOCZONE_NA_KARCIE_II>{ data($req/m:WytloczoneNaKarcieIi) }&lt;/fml:DC_WYTLOCZONE_NA_KARCIE_II>
					else ()
			}
			{
				if($req/m:IdentyfikatorPin)
					then &lt;fml:DC_IDENTYFIKATOR_PIN>{ data($req/m:IdentyfikatorPin) }&lt;/fml:DC_IDENTYFIKATOR_PIN>
					else ()
			}
			{
				if($req/m:ZalozycPolise)
					then &lt;fml:DC_ZALOZYC_POLISE>{ data($req/m:ZalozycPolise) }&lt;/fml:DC_ZALOZYC_POLISE>
					else ()
			}
			{
				if($req/m:LimitKarty)
					then &lt;fml:DC_LIMIT_KARTY>{ data($req/m:LimitKarty) }&lt;/fml:DC_LIMIT_KARTY>
					else ()
			}
			{
				if($req/m:StatusUmowy)
					then &lt;fml:DC_STATUS_UMOWY>{ data($req/m:StatusUmowy) }&lt;/fml:DC_STATUS_UMOWY>
					else ()
			}
			{
				if($req/m:TerminalId)
					then &lt;fml:DC_TERMINAL_ID>{ data($req/m:TerminalId) }&lt;/fml:DC_TERMINAL_ID>
					else ()
			}
			{
				if($req/m:UmorzycOplate)
					then &lt;fml:DC_UMORZYC_OPLATE>{ data($req/m:UmorzycOplate) }&lt;/fml:DC_UMORZYC_OPLATE>
					else ()
			}
			{
				if($req/m:CyklZestawien)
					then &lt;fml:DC_CYKL_ZESTAWIEN>{ data($req/m:CyklZestawien) }&lt;/fml:DC_CYKL_ZESTAWIEN>
					else ()
			}
			{
				if($req/m:Ekspres)
					then &lt;fml:DC_EKSPRES>{ data($req/m:Ekspres) }&lt;/fml:DC_EKSPRES>
					else ()
			}
			{
				if($req/m:TypAdresu)
					then &lt;fml:DC_TYP_ADRESU>{ data($req/m:TypAdresu) }&lt;/fml:DC_TYP_ADRESU>
					else ()
			}
			{
				if($req/m:Promocja)
					then &lt;fml:DC_PROMOCJA>{ data($req/m:Promocja) }&lt;/fml:DC_PROMOCJA>
					else ()
			}
			{
				if($req/m:ZestZbiorcze)
					then &lt;fml:DC_ZEST_ZBIORCZE>{ data($req/m:ZestZbiorcze) }&lt;/fml:DC_ZEST_ZBIORCZE>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbICBSAddDebitCardRequest($body/m:bICBSAddDebitCardRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>