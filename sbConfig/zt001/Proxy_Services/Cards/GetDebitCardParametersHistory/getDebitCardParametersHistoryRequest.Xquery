<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:card.entities.be.dcl";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:be.services.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:dictionaries.be.dcl";
declare namespace ns5="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7="urn:baseauxentities.be.dcl";
declare namespace ns8="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};


declare function getFieldsFromHeader($parm as element(ns2:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns2:msgHeader/ns2:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns2:msgHeader/ns2:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns2:msgHeader/ns2:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns2:msgHeader/ns2:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns2:msgHeader/ns2:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns2:msgHeader/ns2:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns2:transHeader/ns2:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns2:invoke)) as element()*
{

&lt;NF_DEBITC_VIRTUALCARDNBR?>{data($parm/ns2:debitCard/ns0:DebitCard/ns0:virtualCardNbr)}&lt;/NF_DEBITC_VIRTUALCARDNBR>
,
&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns2:bcd/ns8:BusinessControlData/ns8:pageControl/ns7:PageControl/ns7:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns2:bcd/ns8:BusinessControlData/ns8:pageControl/ns7:PageControl/ns7:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
&lt;NF_PAGEC_REVERSEORDER?>{boolean2SourceValue(data($parm/ns2:bcd/ns8:BusinessControlData/ns8:pageControl/ns7:PageControl/ns7:reverseOrder),"1","0")}&lt;/NF_PAGEC_REVERSEORDER>
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns2:bcd/ns8:BusinessControlData/ns8:pageControl/ns7:PageControl/ns7:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU>{data($parm/ns2:bcd/ns8:BusinessControlData/ns8:pageControl/ns7:PageControl/ns7:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
,
(:&lt;NF_FICPCH_FROMDATEFILTER?>{fn-bea:date-to-string-with-format("yyyy-MM-dd",data($parm/ns0:filter/ns6:FilterChgParamCardHistory/ns6:fromDateFilter))}&lt;/NF_FICPCH_FROMDATEFILTER>:)

	if (data($parm/ns2:filter/ns5:FilterChgParamCardHistory/ns5:fromDateFilter))
	then &lt;NF_FICPCH_FROMDATEFILTER>{fn-bea:date-to-string-with-format("yyyy-MM-dd",data($parm/ns2:filter/ns5:FilterChgParamCardHistory/ns5:fromDateFilter))}&lt;/NF_FICPCH_FROMDATEFILTER>
	else &lt;NF_FICPCH_FROMDATEFILTER>0001-01-01&lt;/NF_FICPCH_FROMDATEFILTER>
,
(:&lt;NF_FICPCH_TODATEFILTER?>{fn-bea:date-to-string-with-format("yyyy-MM-dd",data($parm/ns0:filter/ns6:FilterChgParamCardHistory/ns6:toDateFilter))}&lt;/NF_FICPCH_TODATEFILTER>:)

	if (data($parm/ns2:filter/ns5:FilterChgParamCardHistory/ns5:toDateFilter))
	then &lt;NF_FICPCH_TODATEFILTER>{fn-bea:date-to-string-with-format("yyyy-MM-dd",data($parm/ns2:filter/ns5:FilterChgParamCardHistory/ns5:toDateFilter))}&lt;/NF_FICPCH_TODATEFILTER>
	else &lt;NF_FICPCH_TODATEFILTER>2099-12-31&lt;/NF_FICPCH_TODATEFILTER>
,
(:&lt;NF_CRDCT_CRDCHANGETYPE?>{data($parm/ns2:filter/ns5:FilterChgParamCardHistory/ns5:changeTypeFilter/ns1:CrdChangeType/ns1:crdChangeType)}&lt;/NF_CRDCT_CRDCHANGETYPE>:)

	if (data($parm/ns2:filter/ns5:FilterChgParamCardHistory/ns5:changeTypeFilter/ns1:CrdChangeType/ns1:crdChangeType))
	then &lt;NF_CRDCT_CRDCHANGETYPE>{data($parm/ns2:filter/ns5:FilterChgParamCardHistory/ns5:changeTypeFilter/ns1:CrdChangeType/ns1:crdChangeType)}&lt;/NF_CRDCT_CRDCHANGETYPE>
	else &lt;NF_CRDCT_CRDCHANGETYPE> &lt;/NF_CRDCT_CRDCHANGETYPE>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns2:header)}
    {getFieldsFromInvoke($body/ns2:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>