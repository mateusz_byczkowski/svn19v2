<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:crddict.dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";
declare namespace ns4="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForValidationErrorList($parm as element(fml:FML32)) as element()
{

&lt;ns3:validationErrorList>
  {
    for $x at $occ in $parm/NF_CSDIVE_CRDSDIVALIDATION
    return
    &lt;ns0:CrdSDIValidationError>
      &lt;ns0:crdSDIValidationError?>{data($parm/NF_CSDIVE_CRDSDIVALIDATION[$occ])}&lt;/ns0:crdSDIValidationError>
    &lt;/ns0:CrdSDIValidationError>
  }
&lt;/ns3:validationErrorList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
    &lt;ns3:invokeResponse>
     &lt;ns3:isSDIValidationOK>
       &lt;ns5:BooleanHolder>
          &lt;ns5:value?>{sourceValue2Boolean (data($parm/NF_BOOLEH_VALUE),"1")}&lt;/ns5:value>
       &lt;/ns5:BooleanHolder>
     &lt;/ns3:isSDIValidationOK>
     {getElementsForValidationErrorList($parm)}
   &lt;/ns3:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>