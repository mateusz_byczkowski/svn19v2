<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$3.2011-05-13</con:description>
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="urn:errors.hlbsentities.be.dcl";
declare namespace urn3="urn:be.services.dcl";
declare namespace fml = "";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace urn = "urn:be.services.dcl";

declare variable $fault external;
declare variable $headerCache external;
declare variable $body external;

declare function local:fault($faultString as xs:string, $detail as element()*) as element(soap-env:Fault) {
		&lt;soap-env:Fault>
			&lt;faultcode>soapenv:Server.userException&lt;/faultcode> 
			&lt;faultstring>{ $faultString }&lt;/faultstring> 
			&lt;detail>{ $detail }&lt;/detail>
		&lt;/soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string, $errorDescription as xs:string) as element()* {
         &lt;f:exceptionItem>
         	&lt;f:errorCode1>{ $errorCode1 }&lt;/f:errorCode1>
         	&lt;f:errorCode2>{ $errorCode2 }&lt;/f:errorCode2>
        	&lt;f:errorDescription>{ $errorDescription }&lt;/f:errorDescription>
         &lt;/f:exceptionItem>

};

declare function local:getShortUrcode($urcode as xs:string) as xs:string
{
  if (fn:string-length($urcode) = 5) 
      then
      (
	  fn:substring($urcode,4,2)	 
      )
      else
     (
       $urcode
     )
};

declare function local:getLongUrcode($urcode as xs:string) as xs:string
{
  if (fn:string-length($urcode) = 5) 
      then
      (	
	  fn:substring($urcode,1,3)
       )
       else
       (
	    if (fn:string-length($urcode) = 3)
               then 
               (
                     fn:substring($urcode,1,3)
               ) 
               else
               (
                     $urcode
               )
       )
   
};

declare function local:getErrorDescription($fml as element()) as xs:string { 
    if ($fml/ERROR_DESCRIPTION[1])
       then  data($fml/ERROR_DESCRIPTION[1])
    else  ""
       
};

&lt;soap-env:Body>
{
	(: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
	let $reason := fn:substring-before(fn:substring-after(fn:substring-before($fault/ctx:reason, ":"), "("), ")")
	let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($fault/ctx:reason, ":"), ":"), ":")
	let $messageID := data($headerCache/urn3:header/urn3:msgHeader/urn3:msgId)
        let $shortUrcode:=string(local:getShortUrcode($urcode))
        let $longUrcode:=string(local:getLongUrcode($urcode))
     
     (:let $errorDescriptiona :=  getErrorDescription($body/fml:FML32):)
     let $errorDescriptiona := local:getErrorDescription($body/FML32)
     let $errorDescription := translate($errorDescriptiona, "za§˘’Ťg‘žl†ja¦¤ZAˇŁś•G?LŹJAĄ;&amp;#346;", "zażółćgęŁląjaźńZAŻÓśĆGĘLĄJAŃŚ")

  
	return
		if($reason = "13") then
			local:fault("f:TimeoutException", element f:TimeoutException { local:errors($reason, $urcode, $messageID) })
		else if($reason = "11") then
			if($longUrcode = "101") then
				local:fault("f:ErrBufferFillException", element f:ErrBufferFillException { local:errors($reason, $urcode, $messageID) })
			else if($longUrcode = "102") then
				local:fault("f:CbdErrGetInputException", element f:CbdErrGetInputException { local:errors($reason, $urcode, $messageID) })
			else if($longUrcode = "103") then
				local:fault("f:ErrNoDataException", element f:ErrNoDataException { local:errors($reason, $urcode, $messageID) })
			else
                            local:fault("f:ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode,  $errorDescription) })                       
            else
				local:fault("f:ServiceException", element f:ServiceException { local:errors($reason, $urcode, concat($messageID, concat($shortUrcode, concat($longUrcode, $errorDescription)))) })                       				

                            

}

&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>