<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-13</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:operations.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn4 = "urn:swift.operations.entities.be.dcl";
declare namespace urn6 = "urn:transactionswiftdict.operationsdictionary.dictionaries.be.dcl";

declare function xf:mapGetSLINKProdResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
	
&lt;urn:invokeResponse>
&lt;urn:transactionSwift>
	&lt;urn4:TransactionSwift>
		&lt;urn4:slinkProductNumber>
			{ data($fml/fml:S_PRODUCT_ID)}	
		&lt;/urn4:slinkProductNumber>
		&lt;urn4:valueDate>
				{ data($fml/fml:S_TRN_DATE)}	
		&lt;/urn4:valueDate>
		&lt;urn4:swiftFee>
			&lt;urn4:SwiftFee>
				&lt;urn4:slinkFeeId>
					{ data($fml/fml:S_FEE_ID)}
				&lt;/urn4:slinkFeeId>
				&lt;urn4:feeTotalAmount>
					{ data($fml/fml:S_FEE_TOTAL_AMOUNT)}
				&lt;/urn4:feeTotalAmount>

				&lt;urn4:swiftFeeBusinessList>
                               {
                                        for $bank in $fml/fml:S_FML32 return
                                        (
					&lt;urn4:SwiftFeeBusiness>
						&lt;urn4:feeAmount>
							{ data($bank/S_FEE_AMOUNT)}
						&lt;/urn4:feeAmount>						
						&lt;urn4:originCurrFeeAmount>
							{ data($bank/fml:S_FEE_ORIGIN_AMOUNT)}
						&lt;/urn4:originCurrFeeAmount>
						&lt;urn4:discountAmount>
							{ data($bank/fml:S_DISCOUNT_AMOUNT)}
						&lt;/urn4:discountAmount>
						&lt;urn4:originCurrFeeExchRate>
							{ data($bank/fml:S_FEE_ORIGIN_RATE)}
						&lt;/urn4:originCurrFeeExchRate>
						&lt;urn4:originCurrFeeExchRateCurrs>
							{ data($bank/fml:S_FEE_ORIGIN_RATE_CURR)}
						&lt;/urn4:originCurrFeeExchRateCurrs>
						&lt;urn4:discountCurrExchRate>
							{ data($bank/fml:S_DISCOUNT_RATE)}
						&lt;/urn4:discountCurrExchRate>
						&lt;urn4:discountCurrExchRateCurrs>
							{ data($bank/fml:S_DISCOUNT_RATE_CURR)}
						&lt;/urn4:discountCurrExchRateCurrs>
						&lt;urn4:chargedAccountNumber>
							{ data($bank/fml:S_FEE_ACCOUNT_NO)}
						&lt;/urn4:chargedAccountNumber>
						&lt;urn4:swiftFeeBusinessTitle>  
							&lt;urn6:SwiftFeeBusinessTitle>
								&lt;urn6:swiftFeeBusinessTitle>
									{ data($bank/fml:S_FEE_TITLE)}
								&lt;/urn6:swiftFeeBusinessTitle>
							&lt;/urn6:SwiftFeeBusinessTitle>
						&lt;/urn4:swiftFeeBusinessTitle>  
						&lt;urn4:originCurrFeeCurrency> 
   							&lt;urn2:CurrencyCode>
								&lt;urn2:currencyCode>
									{ data($bank/fml:S_FEE_CURRENCY)}
								&lt;/urn2:currencyCode>
							&lt;/urn2:CurrencyCode>
						&lt;/urn4:originCurrFeeCurrency>
						&lt;urn4:discountCurrency>								
							&lt;urn2:CurrencyCode>
								&lt;urn2:currencyCode>
									{ data($bank/fml:S_DISCOUNT_CURRENCY)}
								&lt;/urn2:currencyCode>
							&lt;/urn2:CurrencyCode>
						&lt;/urn4:discountCurrency>

						&lt;urn4:feeCurrency>	
							&lt;urn2:CurrencyCode>
								&lt;urn2:currencyCode>
									{ data($bank/fml:S_FEE_CURRENCY)}
								&lt;/urn2:currencyCode>
							&lt;/urn2:CurrencyCode>
						&lt;/urn4:feeCurrency>	
					&lt;/urn4:SwiftFeeBusiness>
                                 )
                                 }
				&lt;/urn4:swiftFeeBusinessList>					

				&lt;urn4:feeTotalAmountCurrency>
					&lt;urn2:CurrencyCode>         
						&lt;urn2:currencyCode>
							{ data($fml/fml:S_FEE_TOTAL_AMOUNT_CURR)}
						&lt;/urn2:currencyCode>						
					&lt;/urn2:CurrencyCode>
				&lt;/urn4:feeTotalAmountCurrency>	                             


			&lt;/urn4:SwiftFee>
		&lt;/urn4:swiftFee>
	&lt;/urn4:TransactionSwift>
&lt;/urn:transactionSwift>
&lt;/urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetSLINKProdResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>