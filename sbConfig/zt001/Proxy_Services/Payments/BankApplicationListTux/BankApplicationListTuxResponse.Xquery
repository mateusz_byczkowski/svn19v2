<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsd="http://www.w3.org/2001/XMLSchema";
declare namespace f="http://bzwbk.com/services/BZWBK24flow/faults/";
declare default element namespace "http://www.softax.com.pl/ebppml";

declare function xf:map_getBankApplicationListTuxResponse ($rpl as element(soapenv:Body)) as element(fml:FML32) {
	
	&lt;fml:FML32>
	{
		if (data($rpl/application-list-reply/reply/status/result) = "OK") then (
			for $it at $p in $rpl/application-list-reply/reply/application-list/application
			return (

				&lt;fml:EBPP_APPLICATION_ID>{ data($it/application-id) }&lt;/fml:EBPP_APPLICATION_ID>,
				&lt;fml:EBPP_APP_STATUS>{ data($it/status) }&lt;/fml:EBPP_APP_STATUS>,
				
				
				&lt;fml:EBPP_CREDITOR_ID>{ data($it/creditor-id) }&lt;/fml:EBPP_CREDITOR_ID>,
				&lt;fml:EBPP_DEBTOR_ID>{ data($it/debtor-id) }&lt;/fml:EBPP_DEBTOR_ID>,
				&lt;fml:EBPP_TITLE_ID>{ data($it/title-id) }&lt;/fml:EBPP_TITLE_ID>,
				&lt;fml:EBPP_TITLE>{ data($it/title) }&lt;/fml:EBPP_TITLE>,
				
				&lt;fml:EBPP_VALID_FROM>{ data($it/validity/@since) }&lt;/fml:EBPP_VALID_FROM>,
				&lt;fml:EBPP_VALID_TO>{ data($it/validity/@to) }&lt;/fml:EBPP_VALID_TO>,
				
				if (data($it/amount)) then (
					&lt;fml:EBPP_AMOUNT>{ data($it/amount) }&lt;/fml:EBPP_AMOUNT>
				) else ( &lt;fml:EBPP_AMOUNT>0&lt;/fml:EBPP_AMOUNT> )
				,
				&lt;fml:EBPP_CURRENCY>{ data($it/amount/@curr) }&lt;/fml:EBPP_CURRENCY>,
				&lt;fml:EBPP_AGR_NAME>{ data($it/name) }&lt;/fml:EBPP_AGR_NAME>,
				
				&lt;fml:EBPP_EFFICIENT_FROM>{ data($it/efficiency/@since) }&lt;/fml:EBPP_EFFICIENT_FROM>,
				&lt;fml:EBPP_EFFICIENT_TO>{ data($it/efficiency/@to) }&lt;/fml:EBPP_EFFICIENT_TO>,
				
				&lt;fml:EBPP_EXPIRATION_DATE>{ data($it/expire-date) }&lt;/fml:EBPP_EXPIRATION_DATE>,
				&lt;fml:EBPP_SECRET>{ data($it/secret) }&lt;/fml:EBPP_SECRET>
			),
			if (xs:int(data($rpl/fml:E_REC_COUNT)) > 0) then (
				&lt;fml:E_REC_COUNT>{ xs:int(data($rpl/fml:E_REC_COUNT)) - fn:count($rpl/application-list-reply/reply/application-list/application) }&lt;/fml:E_REC_COUNT>
			) else (
			&lt;fml:E_REC_COUNT>{ xs:int(data($rpl/fml:E_REC_COUNT)) + fn:count($rpl/application-list-reply/reply/application-list/application) }&lt;/fml:E_REC_COUNT>
			)
		) else (
		&lt;fml:ERROR_MESSAGE>{ data($rpl/application-list-reply/reply/status/err-code)  }&lt;/fml:ERROR_MESSAGE>,
		&lt;fml:ERROR_DESCRIPTION>{ data($rpl/application-list-reply/reply/status/err-message) }&lt;/fml:ERROR_DESCRIPTION>
		)		
	}
	&lt;/fml:FML32>
	
	
};

declare variable $reply as element(soapenv:Body) external;
&lt;soapenv:Body>
{ xf:map_getBankApplicationListTuxResponse($reply) }
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>