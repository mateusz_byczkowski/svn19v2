<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-13</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/GetCMBalance/messages/";
declare namespace xf = "http://bzwbk.com/services/GetCMBalance/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCMBalanceResponse($fml as element(fml:FML32))
	as element(m:GetCMBalanceResponse) {
		&lt;m:GetCMBalanceResponse>
			{

				let $S_ACC_GRP_ID := $fml/fml:S_ACC_GRP_ID
				let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
				let $S_AMOUNT := $fml/fml:S_AMOUNT
				for $it at $p in $fml/fml:S_ACC_GRP_ID
				return
					&lt;m:GetCMBalanceList>
					{
						if($S_ACC_GRP_ID[$p])
							then &lt;m:AccGrpId>{ data($S_ACC_GRP_ID[$p]) }&lt;/m:AccGrpId>
						else ()
					}
					{
						if($B_KOD_WALUTY[$p])
							then &lt;m:KodWaluty>{ data($B_KOD_WALUTY[$p]) }&lt;/m:KodWaluty>
						else ()
					}
					{
						if($S_AMOUNT[$p])
							then &lt;m:Amount>{ data($S_AMOUNT[$p]) }&lt;/m:Amount>
						else ()
					}
					&lt;/m:GetCMBalanceList>
			}

		&lt;/m:GetCMBalanceResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetCMBalanceResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>