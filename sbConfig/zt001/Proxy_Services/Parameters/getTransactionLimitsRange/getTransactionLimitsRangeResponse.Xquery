<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:baseauxentities.be.dcl";
declare namespace urn2 = "urn:ceke.entities.be.dcl";
declare namespace urn3 = "urn:cekedict.dictionaries.be.dcl";

declare function xf:mapgetTransactionLimitsRangeResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		&lt;urn:invokeResponse>
			&lt;urn:customerCEKEOut>
				&lt;urn2:CustomerCEKE>



					{
					for $limitPIN at $p in $fml/fml:E_LIMIT return
					(
						if($p = 1)
						then (
						&lt;urn2:limitPINMin>{ data($limitPIN)}&lt;/urn2:limitPINMin>
						)
						else if ($p = 2) then (
						&lt;urn2:limitPINMax>{ data($limitPIN)}&lt;/urn2:limitPINMax>
						) else ()
					)
					}

					{
					for $limitTokenSMS at $q in $fml/fml:E_HI_LIMIT return
					(
						if($q = 1)
						then (
						&lt;urn2:limitTokenSMSMin>{ data($limitTokenSMS)}&lt;/urn2:limitTokenSMSMin>
						)
						else if ($q = 2) then (
						&lt;urn2:limitTokenSMSMax>{ data($limitTokenSMS)}&lt;/urn2:limitTokenSMSMax>
						) else ()
					)
					}
				&lt;/urn2:CustomerCEKE>
			&lt;/urn:customerCEKEOut>
		&lt;/urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetTransactionLimitsRangeResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>