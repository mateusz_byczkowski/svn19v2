<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns="http://bzwbk.com/services/icbs/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns:getICBSPSDateResponse>
	&lt;ns:currentProcessingDate>{fn-bea:date-from-string-with-format("dd-MM-yyyy", data($parm/B_DATA_OPER))}&lt;/ns:currentProcessingDate>
	&lt;ns:nextProcessingDate>{fn-bea:date-from-string-with-format("dd-MM-yyyy", data($parm/B_D_NAST_OPER))}&lt;/ns:nextProcessingDate>
	&lt;ns:state>{data($parm/B_STAN_PS)}&lt;/ns:state>
&lt;/ns:getICBSPSDateResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>