<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace tns="urn:services.dcl";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn="urn:be.services.dcl";

declare function xf:map_getPSDate2Request($req as element(urn:invoke))
	as element(fml:FML32) {
		&lt;fml:FML32>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_getPSDate2Request($body/urn:invoke) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>