<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.5
 : @since   2009-12-14
 :
 : wersja WSDLa: 17-09-2009 12:12:51
 : 
 : $Proxy Services/Parameters/getCurrencyRates2/getCurrencyRatesResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getCurrencyRates/getCurrencyRatesResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:
 : @param $stringToPad lancuch wejsciowy
 : @param $padChar     znaki wiodace, ktorymi dopelniamy
 : @param $length      docelowa dlugosc lancucha
 :
 : @return lancuch o dlugosci $length z wiodącymi znakami $padChar
 :
 :)
declare function xf:transformTime($stringToPad as xs:string,
									$padChar as xs:string,
							 	    $length as xs:integer)
	as xs:string
{
	let $inputLength := fn:string-length($stringToPad)
	return
		fn:concat(for $i in (1 to $length - $inputLength) return $padChar,
					   $stringToPad)
};

(:~
 : @param $fML321 - bufor XML/FML
 :
 : @return invokeResponse - operacja wyjściowa
 :)
declare function xf:getCurrencyRatesResponse($fML321 as element(ns0:FML32))
    as element(ns3:invokeResponse)
{
	&lt;ns3:invokeResponse>
		&lt;ns3:rateTableList>
		{
			for $i in 1 to count($fML321/ns0:NF_CURREC_CURRENCYCODE)
			return
				&lt;ns1:RateTable>
					&lt;ns1:averageRate>{
						data($fML321/ns0:NF_RATET_AVERAGERATE[$i])
					}&lt;/ns1:averageRate>
					
					&lt;ns1:transferBuyRate>{
						data($fML321/ns0:NF_RATET_TRANSFERBUYRATE[$i])
					}&lt;/ns1:transferBuyRate>
					
					&lt;ns1:transferBuyRatePref>{
						data($fML321/ns0:NF_RATET_TRANSFERBUYRATEPR[$i])
					}&lt;/ns1:transferBuyRatePref>
					
					&lt;ns1:transferSellRate>{
						data($fML321/ns0:NF_RATET_TRANSFERSELLRATE[$i])
					}&lt;/ns1:transferSellRate>
					
					&lt;ns1:transferSellRatePref>{
						data($fML321/ns0:NF_RATET_TRANSFERSELLRATEP[$i])
					}&lt;/ns1:transferSellRatePref>
					
					&lt;ns1:transferBuyRateNote>{
						data($fML321/ns0:NF_RATET_TRANSFERBUYRATENO[$i])
					}&lt;/ns1:transferBuyRateNote>
					
					&lt;ns1:transferSellRateNote>{
						data($fML321/ns0:NF_RATET_TRANSFERSELLRATEN[$i])
					}&lt;/ns1:transferSellRateNote>
					
					(:
					 : czas ma byc ustawiony jako czas bieżący, systemowy;
					 : jezeli byłoby to niemożliwe,
					 : to 23:59:59 a nie 00:00:00
					 : (bo ma być znajdowana ostatnia tabela załadowana danego dnia)
					 :)
					{
						let $currencyDate := $fML321/ns0:NF_RATET_CURRENCYDT[$i]
						return
							if (data($currencyDate)) then
								&lt;ns1:currencyDate>{
									let $time := data($fML321/ns0:NF_RATET_CURRENCYTM[$i])
									return
										if (data($time)) then
											let $currencyTime := xf:transformTime($time, '0', 6)
											return
												fn:concat(data($currencyDate),
														  'T',
														  fn:substring($currencyTime, 1, 2),
														  ':',
														  fn:substring($currencyTime, 3, 2),
														  ':',
														  fn:substring($currencyTime, 5, 2))
										else
											fn:concat(data($currencyDate),'T23:59:59')
								}&lt;/ns1:currencyDate>
							else
								()
					}

					&lt;ns1:averageRateNBP>{
						data($fML321/ns0:NF_RATET_AVERAGERATENBP[$i])
					}&lt;/ns1:averageRateNBP>
					
					&lt;ns1:westernUnionRate>{
						data($fML321/ns0:NF_RATET_WESTERNUNIONRATE[$i])
					}&lt;/ns1:westernUnionRate>
					
					&lt;ns1:currencyCode>
						&lt;ns2:CurrencyCode>
							&lt;ns2:currencyCode>{
								data($fML321/ns0:NF_CURREC_CURRENCYCODE[$i])
							}&lt;/ns2:currencyCode>
						&lt;/ns2:CurrencyCode>
					&lt;/ns1:currencyCode>
				&lt;/ns1:RateTable>
		}
		&lt;/ns3:rateTableList>
	&lt;/ns3:invokeResponse>
};

&lt;soap-env:Body>{
	xf:getCurrencyRatesResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>