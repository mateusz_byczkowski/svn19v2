<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace msg = "http://bzwbk.com/services/ceke/messages/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;
 
declare variable $req  as element(msg:GetExRatesRequest):=$body/msg:GetExRatesRequest;

&lt;soap-env:Body>
	&lt;FML32>
	{
		if($req/NrTabeli)
			then &lt;B_NR_TABELI>{ data($req/NrTabeli) }&lt;/B_NR_TABELI>
			else ()
	}
	&lt;/FML32>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>