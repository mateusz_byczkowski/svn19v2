<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$3.2010-10-28</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:accounts.entities.be.dcl";
declare namespace urn2="urn:accountstat.accounts.entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace fml="";

declare variable $body external;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

declare function local:getElement($parm as element(fml:FML32), $licz as xs:integer) as element()*
{
  if (data($parm/B_LIMIT_KWOTA[$licz]) and xf:isData(data($parm/B_LIMIT_KWOTA[$licz])))
    then 
      &lt;urn1:TranAccountTurnover>
        &lt;urn1:monthLimit>{data($parm/B_LIMIT_KWOTA[$licz])}&lt;/urn1:monthLimit>
        &lt;urn1:month>{data($parm/B_MIESIAC[$licz])}&lt;/urn1:month>
        &lt;urn1:year>{data($parm/B_ROK[$licz])}&lt;/urn1:year>
        &lt;urn1:monthRevenue>{round-half-to-even(data($parm/B_OBROT_WN[$licz]) + data($parm/B_OBROT_MA[$licz]),2)}&lt;/urn1:monthRevenue>
        &lt;urn1:montLoad>{data($parm/B_OBROT_WN[$licz])}&lt;/urn1:montLoad>
        &lt;urn1:monthCredit>{data($parm/B_OBROT_MA[$licz])}&lt;/urn1:monthCredit>
        &lt;urn1:monthBalance>{round-half-to-even(data($parm/B_AVG_SALDO_MA[$licz]) + data($parm/B_AVG_SALDO_WN[$licz]),2)}&lt;/urn1:monthBalance>
      &lt;/urn1:TranAccountTurnover>
    else()
};

declare function getInvokeResponse($parm as element(fml:FML32)) as element()*
{
    let $fml := $body/FML32
    let $B_KOD_RACH_S := $fml/fml:B_KOD_RACH_S
    let $B_KK_SALDO := $fml/fml:B_KK_SALDO
    let $B_OBROTY_DZIEN_UWN := $fml/fml:B_OBROTY_DZIEN_UWN
    let $B_OBROTY_DZIEN_UMA := $fml/fml:B_OBROTY_DZIEN_UMA
    let $B_LICZBA_REK := $fml/fml:B_LICZBA_REK

    return
      &lt;urn:invokeResponse>
        &lt;urn:accountsListOut>
        {
        for $x at $occ in $B_KOD_RACH_S
		  
          let $rek := xs:int($B_LICZBA_REK[$occ])                           
          let $licz := xs:int(sum($B_LICZBA_REK[position() &lt; $occ]))  

          return
          &lt;urn1:Account>
            &lt;urn1:accountNumber>{data($B_KOD_RACH_S[$occ])}&lt;/urn1:accountNumber>
            &lt;urn1:tranAccount>
              &lt;urn1:TranAccount>
                &lt;urn1:tranAccountTurnoverList>		
                {
                   for $n in $licz+1 to $licz+$rek
                   return                        
                      local:getElement($parm, $n)	      
                }
                &lt;/urn1:tranAccountTurnoverList>   
              &lt;/urn1:TranAccount>
            &lt;/urn1:tranAccount>
            &lt;urn1:accountPeriodStat>
              &lt;urn2:AccountPeriodStat>
                &lt;urn2:avaragePeriodBalance>{data($B_KK_SALDO[$occ])}&lt;/urn2:avaragePeriodBalance>
                &lt;urn2:debitTurnover>{data($B_OBROTY_DZIEN_UWN[$occ])}&lt;/urn2:debitTurnover>
                &lt;urn2:creditTurnover>{data($B_OBROTY_DZIEN_UMA[$occ])}&lt;/urn2:creditTurnover>
              &lt;/urn2:AccountPeriodStat>
            &lt;/urn1:accountPeriodStat>
          &lt;/urn1:Account>
        }
        &lt;/urn:accountsListOut>
      &lt;/urn:invokeResponse>
};
  
&lt;soap:Body>
  {getInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>