<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-11-29</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf="http://bzwbk.com/services/prime/functions";
declare namespace fml = "";
declare namespace fault="http://bzwbk.com/services/cerber/faults";
declare namespace m="http://bzwbk.com/services/cerber";

declare variable $body as element(soap-env:Body) external;


declare function xf:mapResponse($resp as element (m:GetUserDataWithoutPassResponse))
	as element (FML32) 
{
    &lt;FML32>

      {
        for $rights in $resp/Permissions return
        (
            &lt;U_RIGHTS_ID>{ data($rights) }&lt;/U_RIGHTS_ID>
        )
      }
      {      
          for $attr in $resp/Attributes return
          (
              &lt;U_USER_ATTR_NAME>{ data($attr/Name) }&lt;/U_USER_ATTR_NAME>
          )
      }
      {
          for $attr in $resp/Attributes return
          (
              &lt;U_USER_ATTR_VALUE>{ data($attr/Value) }&lt;/U_USER_ATTR_VALUE>
          )
      }

    &lt;U_ORG_UNIT>{data($resp/Units[IsMasterUnit='T']/IcbsNo)}&lt;/U_ORG_UNIT>
    &lt;U_ERROR_CODE>0&lt;/U_ERROR_CODE>
    &lt;/FML32>
};


declare function xf:getErrorCode ($fau as element(soap-env:Fault)) as xs:integer
{
      if (boolean($fau/detail/fault:InvalidUserPasswordException)) then  110
     else    if (boolean($fau/detail/fault:IllegalApplicationAccessException)) then   127
     else    99
    };

declare function xf:mapFault($fau as element(soap-env:Fault))
    as element (FML32) 
{
    &lt;FML32>
      &lt;U_ERROR_CODE>{xf:getErrorCode($fau)}&lt;/U_ERROR_CODE>
    &lt;/FML32>
};



&lt;soap-env:Body>
{
    if (boolean($body/m:GetUserDataWithoutPassResponse)) then 
    (
         xf:mapResponse($body/m:GetUserDataWithoutPassResponse)
    ) else if (boolean($body/soap-env:Fault)) then 
    (
         xf:mapFault($body/soap-env:Fault)
    ) 
    else ()
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>