<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.5
 : @since   2009-12-15
 :
 : wersja WSDLa: 04-02-2010 15:47:16
 :
 : $Proxy Services/Till/getTillCashLimits/getTillCashLimitsResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getTillCashLimits/getTillCashLimitsResponse/";
declare namespace ns0 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";
declare namespace ns6 = "urn:baseauxentities.be.dcl";

declare variable $fML321 as element(ns2:FML32) external;

(:~
 : @param $fML321 bufor FML/XML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getTillCashLimitsResponse($fML321 as element(ns2:FML32))
    as element(ns5:invokeResponse)
{
    &lt;ns5:invokeResponse>

		(:
		 : dane wyjściowe
		 :)
        &lt;ns5:userTxnSessionOut>{
        
	        for $i in 1 to count($fML321/ns2:NF_USERTS_SESSIONDATE)
	        return
	            &lt;ns3:UserTxnSession>
	            
	            	{
	            		let $sessionDate := $fML321/ns2:NF_USERTS_SESSIONDATE[$i]
	            		return
		               		if (data($sessionDate)) then
			            		&lt;ns3:sessionDate>{	
		    	            		data($sessionDate)
								}&lt;/ns3:sessionDate>
		            	    else ()
					}
				
                    (:
                     : status stanowiska
                     :
                     : - 0 --> C (zamknięte)
                     : - 1 --> O (otwarte)
                     :)
                    {
                    	let $sessionStatus := $fML321/ns2:NF_USETSS_USERTXNSESSIONST[$i]
                    	return
                    		if (data($sessionStatus)) then
                    			&lt;ns3:sessionStatus>
                    				&lt;ns1:UserTxnSessionStatus>
                    					&lt;ns1:userTxnSessionStatus>{
                    						if (data($sessionStatus) eq '1') then
                    							'O'
                    						else
                    							'C'
                    					}&lt;/ns1:userTxnSessionStatus>
                    				&lt;/ns1:UserTxnSessionStatus>
                    			&lt;/ns3:sessionStatus>
                    		else ()
                    }
	
	               	&lt;ns3:user>
	                   	&lt;ns4:User>
							&lt;ns4:userLastName>{
								data($fML321/ns2:NF_USER_USERLASTNAME[$i])
							}&lt;/ns4:userLastName>
							
							&lt;ns4:userID>{
								data($fML321/ns2:NF_USER_USERID[$i])
							}&lt;/ns4:userID>
							
							&lt;ns4:userFirstName>{
								data($fML321/ns2:NF_USER_USERFIRSTNAME[$i])
							}&lt;/ns4:userFirstName>
	                   	&lt;/ns4:User>
	               	&lt;/ns3:user>

	               	&lt;ns3:till>
						&lt;ns3:Till>
							&lt;ns3:tillID>{
								data($fML321/ns2:NF_TILL_TILLID[$i])
							}&lt;/ns3:tillID>
							
							&lt;ns3:tillCashLimit>
								&lt;ns3:TillCashLimits>
									&lt;ns3:localAmount>{
										data($fML321/ns2:NF_TILLCL_LOCALAMOUNT[$i])
									}&lt;/ns3:localAmount>
									
									&lt;ns3:foreignAmount>{
										data($fML321/ns2:NF_TILLCL_FOREIGNAMOUNT[$i])
									}&lt;/ns3:foreignAmount>
									
									{
										let $lastChangeDate := $fML321/ns2:NF_TILLCL_LASTCHANGEDATE[$i]
										return
											if (data($lastChangeDate)) then
												&lt;ns3:lastChangeDate>{
													fn:concat(fn:substring(data($lastChangeDate), 1, 10),
															  'T',
															  fn:substring(data($lastChangeDate), 12, 2),
															  ':',
															  fn:substring(data($lastChangeDate), 15, 2),
															  ':',
															  fn:substring(data($lastChangeDate), 18, 2))
												}&lt;/ns3:lastChangeDate>
											else ()	
									}
									
									&lt;ns3:lastChangeUser>{
										data($fML321/ns2:NF_TILLCL_LASTCHANGEUSER[$i])
									}&lt;/ns3:lastChangeUser>
									
									&lt;ns3:localCurrency>
										&lt;ns0:CurrencyCode>
											&lt;ns0:currencyCode>{
												data($fML321/ns2:NF_CURREC_CURRENCYCODE[$i])
											}&lt;/ns0:currencyCode>
										&lt;/ns0:CurrencyCode>
									&lt;/ns3:localCurrency>
									
									&lt;ns3:foreignCurrency>
										&lt;ns0:CurrencyCode>
											&lt;ns0:currencyCode>{
												data($fML321/ns2:NF_CURREC_CURRENCYCODE2[$i])
											}&lt;/ns0:currencyCode>
										&lt;/ns0:CurrencyCode>
									&lt;/ns3:foreignCurrency>
								&lt;/ns3:TillCashLimits>
							&lt;/ns3:tillCashLimit>
						&lt;/ns3:Till>
					&lt;/ns3:till>
	
	               	&lt;ns3:teller>
	                   	&lt;ns3:Teller>
							&lt;ns3:tellerID>{
								data($fML321/ns2:NF_TELLER_TELLERID[$i])
							}&lt;/ns3:tellerID>
						&lt;/ns3:Teller>
					&lt;/ns3:teller>
					
				&lt;/ns3:UserTxnSession>
			
        }&lt;/ns5:userTxnSessionOut>
        
        (:
         : dane do stronicowania
         :)
        &lt;ns5:bcd>
            &lt;ns4:BusinessControlData>
                &lt;ns4:pageControl?>
                    &lt;ns6:PageControl?>
                    	{
                    		let $hasNext := $fML321/ns2:NF_PAGEC_HASNEXT
                    		return
                    			if (data($hasNext)) then
									&lt;ns6:hasNext>{
			                    		xs:boolean(data($fML321/ns2:NF_PAGEC_HASNEXT))
			                    	}&lt;/ns6:hasNext>
                    			else ()
                    	}
                    	
                        &lt;ns6:navigationKeyDefinition?>{
							data($fML321/ns2:NF_PAGEC_NAVIGATIONKEYDEFI)
						}&lt;/ns6:navigationKeyDefinition>
						
                        &lt;ns6:navigationKeyValue?>{
							data($fML321/ns2:NF_PAGEC_NAVIGATIONKEYVALU)
						}&lt;/ns6:navigationKeyValue>
						
                    &lt;/ns6:PageControl>
                &lt;/ns4:pageControl>
            &lt;/ns4:BusinessControlData>
        &lt;/ns5:bcd>
        
    &lt;/ns5:invokeResponse>
};

&lt;soap-env:Body>{
	xf:getTillCashLimitsResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>