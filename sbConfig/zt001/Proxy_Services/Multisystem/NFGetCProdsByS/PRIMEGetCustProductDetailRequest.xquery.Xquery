<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductDetail($fml as element(fml:FML32))
	as element(m:PRIMEGetCustProductDetail) {
		&lt;m:PRIMEGetCustProductDetail>
			(:{
				if($fml/fml:CI_ID_WEW_PRAC)
					then &lt;m:CI_ID_WEW_PRAC>{ data($fml/fml:CI_ID_WEW_PRAC) }&lt;/m:CI_ID_WEW_PRAC>
					else ()
			}:)
			{
				if($fml/fml:NF_MSHEAD_COMPANYID)
					then &lt;m:CI_ID_SPOLKI>{ data($fml/fml:NF_MSHEAD_COMPANYID) }&lt;/m:CI_ID_SPOLKI>
					else ()
			}
			{
				if($fml/fml:NF_CUSTOM_CUSTOMERNUMBER)
					then &lt;m:DC_NUMER_KLIENTA>{ data($fml/fml:NF_CUSTOM_CUSTOMERNUMBER) }&lt;/m:DC_NUMER_KLIENTA>
					else ()
			}
			(:{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then &lt;m:DC_NR_DOWODU_REGON>{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then &lt;m:DC_NIP>{ data($fml/fml:DC_NIP) }&lt;/m:DC_NIP>
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then &lt;m:DC_NR_PESEL>{ data($fml/fml:DC_NR_PESEL) }&lt;/m:DC_NR_PESEL>
					else ()
			}:)
			{
				if($fml/fml:NF_ACCOUN_ACCOUNTNUMBER)
					then &lt;m:DC_NR_RACHUNKU>{ data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER) }&lt;/m:DC_NR_RACHUNKU>
                                       else ()
			}
			{
				if($fml/fml:NF_CTRL_OPTION)
					then &lt;m:CI_OPCJA>{ data($fml/fml:NF_CTRL_OPTION) }&lt;/m:CI_OPCJA>
					else ()
			}
			{
				if($fml/fml:NF_CTRL_SYSTEMID)
					then &lt;m:CI_ID_SYS>{ data($fml/fml:NF_CTRL_SYSTEMID) }&lt;/m:CI_ID_SYS>
					else ()
			}


		&lt;/m:PRIMEGetCustProductDetail>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapPRIMEGetCustProductDetail($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>