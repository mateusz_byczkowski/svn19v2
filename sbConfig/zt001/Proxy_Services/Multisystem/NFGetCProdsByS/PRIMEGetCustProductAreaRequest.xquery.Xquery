<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2009-08-26 PKL NP1995 Dodanie pól ProductStat i ProductStatusCode</con:description>
  <con:xquery>(: Change Log :)
(: v.1.1.0 2009-08-26 PKL NP1995 Dodanie pól ProductStat i ProductStatusCode :)

declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductsArea($fml as element(fml:FML32))
	as element(m:PRIMEGetCustProductsArea) {
                let $cif := data($fml/fml:NF_CUSTOM_CUSTOMERNUMBER)
                let $cifWithoutZeros := replace($cif,'^0+','')
                return
		&lt;m:PRIMEGetCustProductsArea>
			{
				if($fml/fml:NF_MSHEAD_USERID)
					then &lt;m:CI_ID_WEW_PRAC>{ data($fml/fml:NF_MSHEAD_USERID) }&lt;/m:CI_ID_WEW_PRAC>
					else  &lt;m:CI_ID_WEW_PRAC>1&lt;/m:CI_ID_WEW_PRAC>
			}
			{
				if($fml/fml:NF_MSHEAD_COMPANYID)
					then &lt;m:CI_ID_SPOLKI>{ data($fml/fml:NF_MSHEAD_COMPANYID) }&lt;/m:CI_ID_SPOLKI>
					else ()
			}
			{
				if($fml/fml:NF_CUSTOM_CUSTOMERNUMBER)
					then &lt;m:DC_NUMER_KLIENTA>{ $cif }&lt;/m:DC_NUMER_KLIENTA>
					else ()
			}
			(:{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then &lt;m:DC_NR_DOWODU_REGON>{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then &lt;m:DC_NIP>{ data($fml/fml:DC_NIP) }&lt;/m:DC_NIP>
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then &lt;m:DC_NR_PESEL>{ data($fml/fml:DC_NR_PESEL) }&lt;/m:DC_NR_PESEL>
					else ()
			}:)
			{
				if($fml/fml:NF_PRODUA_CODEPRODUCTAREA)
					then &lt;m:CI_PRODUCT_AREA_CODE>{ data($fml/fml:NF_PRODUA_CODEPRODUCTAREA) }&lt;/m:CI_PRODUCT_AREA_CODE>
					else 	&lt;m:CI_PRODUCT_AREA_CODE>0&lt;/m:CI_PRODUCT_AREA_CODE>
			}
(: NP1995 początek :)		{
				if($fml/fml:NF_CTRL_ACTIVENONACTIVE)
					then &lt;m:NF_CTRL_ACTIVENONACTIVE>{ data($fml/fml:NF_CTRL_ACTIVENONACTIVE) }&lt;/m:NF_CTRL_ACTIVENONACTIVE>
					else 	&lt;m:NF_CTRL_ACTIVENONACTIVE>0&lt;/m:NF_CTRL_ACTIVENONACTIVE>
(: NP1995 koniec :)		}
                       (:{
				if($fml/fml:CI_OPCJA)
					then &lt;m:CI_OPCJA>{ data($fml/fml:CI_OPCJA) }&lt;/m:CI_OPCJA>
					else ()
			}:)
                      &lt;m:CI_OPCJA>1&lt;/m:CI_OPCJA>


		&lt;/m:PRIMEGetCustProductsArea>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapPRIMEGetCustProductsArea($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>