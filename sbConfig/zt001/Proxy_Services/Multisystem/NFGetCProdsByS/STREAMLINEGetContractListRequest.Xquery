<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.2  2010-05-06 PKLI NP1972_3 CR2 Filtrowanie produktów Streamline na podstawie statusu aktywności
v.1.1  2009-11-25 PKLI NP1972_1 Obsługa pól &lt;factory> i &lt;is-group></con:description>
  <con:xquery>(: Log Zmian: 
==================================
v.1.1  2009-11-25 PKLI NP1972_1 Obsługa pól &lt;factory> i &lt;is-group>
v.1.2  2010-05-06 PKLI NP1972_3 CR2 Filtrowanie produktów Streamline na podstawie statusu aktywności
:)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
    &lt;wsdl:getContractListRequest xmlns:wsdl="http://jv.channel.cu.com.pl/cmf/wsdl-jv">
        &lt;envelope>
            &lt;request-time>{current-dateTime()}&lt;/request-time>
            &lt;request-no?>{data($body/FML32/NF_MSHEAD_MSGID)}&lt;/request-no>
            &lt;source-code>placowka banku&lt;/source-code>
            &lt;user-id?>{data($body/FML32/NF_MSHEAD_USERID)}&lt;/user-id>
            &lt;branch-id?>{data($body/FML32/NF_MSHEAD_UNITID)}&lt;/branch-id>
            (:&lt;bar-code>string&lt;/bar-code>:)
        &lt;/envelope>
        &lt;client-code>{data($body/FML32/NF_CUSTOM_CUSTOMERNUMBER)}&lt;/client-code>
        (:&lt;product-type>string&lt;/product-type>:)
        (:&lt;category>string&lt;/category>:)
(: v.1.1 start:)
        {
 	if ($body/FML32/NF_CTRL_SYSTEMID and data($body/FML32/NF_CTRL_SYSTEMID)=10)
          then &lt;factory>JVL&lt;/factory>
          elseif ($body/FML32/NF_CTRL_SYSTEMID and data($body/FML32/NF_CTRL_SYSTEMID)=11)
            then   &lt;factory>JVG&lt;/factory>
            else() 
        }
        {
 	if ($body/FML32/NF_CTRL_AREAS and data($body/FML32/NF_CTRL_AREAS)=94)
          then &lt;is-group>true&lt;/is-group>
          elseif  ($body/FML32/NF_CTRL_AREAS and data($body/FML32/NF_CTRL_AREAS)=84)
             then &lt;is-group>false&lt;/is-group>
             else() 
        }
(:v.1.1 end:)
(: v.1.2 start :)
        {
           if ($body/FML32/NF_CTRL_ACTIVENONACTIVE and data($body/FML32/NF_CTRL_ACTIVENONACTIVE)=1)
           then &lt;general-status>2&lt;/general-status>
           elseif ($body/FML32/NF_CTRL_ACTIVENONACTIVE and data($body/FML32/NF_CTRL_ACTIVENONACTIVE)=2)
             then &lt;general-status>4&lt;/general-status>
            else() 
        }
(: v.1.2 end :)



    &lt;/wsdl:getContractListRequest>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>