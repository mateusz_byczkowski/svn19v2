<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeGetCustProdArea($fml as element(fml:FML32))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($fml/fml:NF_MSHEAD_USERID)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($fml/fml:NF_MSHEAD_USERID) }&lt;/fml:CI_ID_WEW_PRAC>
					else  &lt;fml:CI_ID_WEW_PRAC>1&lt;/fml:CI_ID_WEW_PRAC>
			}
			{
				if($fml/fml:NF_MSHEAD_COMPANYID)
					then &lt;fml:CI_ID_SPOLKI>{ data($fml/fml:NF_MSHEAD_COMPANYID) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($fml/fml:NF_CUSTOM_CUSTOMERNUMBER)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($fml/fml:NF_CUSTOM_CUSTOMERNUMBER) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			(:{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then &lt;m:DC_NR_DOWODU_REGON>{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then &lt;m:DC_NIP>{ data($fml/fml:DC_NIP) }&lt;/m:DC_NIP>
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then &lt;m:DC_NR_PESEL>{ data($fml/fml:DC_NR_PESEL) }&lt;/m:DC_NR_PESEL>
					else ()
			}:)
			{
				if($fml/fml:NF_PRODUA_CODEPRODUCTAREA)
					then &lt;fml:CI_PRODUCT_AREA_CODE>{ data($fml/fml:NF_PRODUA_CODEPRODUCTAREA) }&lt;/fml:CI_PRODUCT_AREA_CODE>
					else 	&lt;fml:CI_PRODUCT_AREA_CODE>0&lt;/fml:CI_PRODUCT_AREA_CODE>
			}
                       (:{
				if($fml/fml:CI_OPCJA)
					then &lt;fml:CI_OPCJA>{ data($fml/fml:CI_OPCJA) }&lt;/fml:CI_OPCJA>
					else ()
			}:)
                      &lt;fml:CI_OPCJA>1&lt;/fml:CI_OPCJA>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapeGetCustProdArea($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>