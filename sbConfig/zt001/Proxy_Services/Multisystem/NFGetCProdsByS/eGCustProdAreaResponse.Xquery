<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Response dla systemu CEKEVersion.$1.2011-02-02</con:description>
  <con:xquery>(: Log Zmian: 
==================================
v.1.1  2010-06-08 PKLI T45137 Ustawienie statusu 'Produkt aktywny' dla wszystkich usług elektronicznych z systemu CEKE           
v.1.2 2011-02-02 PKL T52074 Błąd w obsłudze stronicowania dla PRIME, CEKE i STRL

:)

declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCEKEStatus($area as xs:string,$status as xs:string) as xs:string{
   (:   kanały elektroniczne :)
   if ($area = "7" )
     then if ($status = "1")
            then "1"
          else if ($status="0")
            then "4"
          else ""
   else ""
};
	
declare function xf:calculatePageStart ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then 1
else if ($actioncode="N")
(: 1.2  then xs:integer(min(($records  , $oldstop + 1)))  :)  
           then xs:integer(min(($records + 1, $oldstop + 1))) (:1.2:)

else if ($actioncode="P")
   then max((1,$oldstart - $pagesize)) 
else if ($actioncode="L")
(: 1.2  then max((1,$records  - $pagesize)) :)
           then max((1,$records  - $pagesize + 1)) (:1.2:)

else 0
};

declare function xf:calculatePageStop ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then xs:integer(min(($pagesize,$records)))
else if ($actioncode="N")
   then xs:integer(min(($pagesize + $oldstop,$records))) 
else if ($actioncode="P")
(: 1.2  then max((1,$oldstart - 1)) :)
           then max((0,$oldstart - 1)) (:1.2:)
else if ($actioncode="L")
   then $records
else xs:integer(0)
};


declare function xf:calculateHasNext( $pagestart as xs:integer,$pagestop as xs:integer, 
                                                     $records as xs:integer, $actioncode as xs:string) as xs:integer{
(: 1.2   if ($actioncode != "P")  :)
    if ($actioncode != "P" and $actioncode != "L")  (:1.2:)
     then  if ($pagestop &lt; $records)
           then 1
           else 0
    else if ($pagestart > 1)
           then 1
           else 0 
};

declare function xf:mapProduct($bdy as element(fml:FML32),$ind as xs:integer,$area as xs:string)  as element()* {

         if ($bdy/fml:DC_NR_RACHUNKU[$ind])
             then &lt;fml:NF_ACCOUN_ACCOUNTNUMBER>{data($bdy/fml:DC_NR_RACHUNKU[$ind])}&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER>
             else &lt;fml:NF_ACCOUN_ACCOUNTNUMBER nill="true"/>
,
         if ($bdy/fml:CI_PRODUCT_ATT1[$ind] and string-length(data($bdy/fml:CI_PRODUCT_ATT1[$ind]))>0)
             then &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT>{data($bdy/fml:CI_PRODUCT_ATT1[$ind])}&lt;/fml:NF_ATTRPG_FIRSTPRODUCTFEAT>
             else &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT nill="true"/>
,
         if ($bdy/fml:CI_PRODUCT_ATT2[$ind] and string-length(data($bdy/fml:CI_PRODUCT_ATT2[$ind]))>0)
             then &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA>{data($bdy/fml:CI_PRODUCT_ATT2[$ind])}&lt;/fml:NF_ATTRPG_SECONDPRODUCTFEA>
             else &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA nill="true"/>
,
         if ($bdy/fml:CI_PRODUCT_ATT3[$ind] and string-length(data($bdy/fml:CI_PRODUCT_ATT3[$ind]))>0)
             then &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT>{data($bdy/fml:CI_PRODUCT_ATT3[$ind])}&lt;/fml:NF_ATTRPG_THIRDPRODUCTFEAT>
             else &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT nill="true"/>
,
         if ($bdy/fml:CI_PRODUCT_ATT4[$ind] and string-length(data($bdy/fml:CI_PRODUCT_ATT4[$ind]))>0)
             then &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA>{data($bdy/fml:CI_PRODUCT_ATT4[$ind])}&lt;/fml:NF_ATTRPG_FOURTHPRODUCTFEA>
             else &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA nill="true"/>
,
         if ($bdy/fml:CI_PRODUCT_ATT5[$ind] and string-length(data($bdy/fml:CI_PRODUCT_ATT5[$ind]))>0)
             then &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT>{data($bdy/fml:CI_PRODUCT_ATT5[$ind])}&lt;/fml:NF_ATTRPG_FIFTHPRODUCTFEAT>
             else &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT nill="true"/>
,
         if ($bdy/fml:B_KOD_WALUTY[$ind] and string-length(data($bdy/fml:B_KOD_WALUTY[$ind]))>0)
             then &lt;fml:NF_CURREC_CURRENCYCODE>{data($bdy/fml:B_KOD_WALUTY[$ind])}&lt;/fml:NF_CURREC_CURRENCYCODE>
             else &lt;fml:NF_CURREC_CURRENCYCODE nill="true"/>
,
         if ($bdy/fml:CI_RACHUNEK_ADR_ALT[$ind] and string-length(data($bdy/fml:CI_RACHUNEK_ADR_ALT[$ind]))>0)
             then &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE>{data($bdy/fml:CI_RACHUNEK_ADR_ALT[$ind])}&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE>
             else &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE>0&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE>
,
       (:  if ($bdy/fml:CI_RELACJA[$ind] and string-length(data($bdy/fml:CI_RELACJA[$ind]))>0)
             then &lt;fml:CI_RELACJA>{data($bdy/fml:CI_RELACJA[$ind])}&lt;/fml:CI_RELACJA>
             else &lt;fml:CI_RELACJA nill="true"/>
,:)
         if ($bdy/fml:B_SALDO[$ind] and string-length(data($bdy/fml:B_SALDO[$ind]))>0)
             then &lt;fml:NF_ACCOUN_CURRENTBALANCE>{data($bdy/fml:B_SALDO[$ind])}&lt;/fml:NF_ACCOUN_CURRENTBALANCE>
             else &lt;fml:NF_ACCOUN_CURRENTBALANCE nill="true"/>
,
         if ($bdy/fml:B_DOST_SRODKI[$ind] and string-length(data($bdy/fml:B_DOST_SRODKI[$ind]))>0)
             then &lt;fml:NF_ACCOUN_AVAILABLEBALANCE>{data($bdy/fml:B_DOST_SRODKI[$ind])}&lt;/fml:NF_ACCOUN_AVAILABLEBALANCE>
             else &lt;fml:NF_ACCOUN_AVAILABLEBALANCE nill="true"/>
,
         if ($bdy/fml:B_BLOKADA[$ind] and string-length(data($bdy/fml:B_BLOKADA[$ind]))>0)
             then &lt;fml:NF_HOLD_HOLDAMOUNT>{data($bdy/fml:B_BLOKADA[$ind])}&lt;/fml:NF_HOLD_HOLDAMOUNT>
             else &lt;fml:NF_HOLD_HOLDAMOUNT nill="true"/>
,
         if ($bdy/fml:B_D_OTWARCIA[$ind] and string-length(data($bdy/fml:B_D_OTWARCIA[$ind]))>0)
             then &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE>{data($bdy/fml:B_D_OTWARCIA[$ind])}&lt;/fml:NF_ACCOUN_ACCOUNTOPENDATE>
             else &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE nill="true"/>
,
         if ($bdy/fml:B_DATA_OST_OPER[$ind] and string-length(data($bdy/fml:B_DATA_OST_OPER[$ind]))>0)
             then &lt;fml:NF_TRANA_DATEOFLASTACTIVIT>{data($bdy/fml:B_DATA_OST_OPER[$ind])}&lt;/fml:NF_TRANA_DATEOFLASTACTIVIT>
             else &lt;fml:NF_TRANA_DATEOFLASTACTIVIT nill="true"/>
,
         if ($bdy/fml:B_LIMIT1[$ind] and string-length(data($bdy/fml:B_LIMIT1[$ind]))>0)
             then &lt;fml:NF_TRAACA_LIMITAMOUNT>{data($bdy/fml:B_LIMIT1[$ind])}&lt;/fml:NF_TRAACA_LIMITAMOUNT>
             else &lt;fml:NF_TRAACA_LIMITAMOUNT nill="true"/>
,
         if ($bdy/fml:CI_PRODUCT_NAME_ORYGINAL[$ind] and string-length(data($bdy/fml:CI_PRODUCT_NAME_ORYGINAL[$ind]))>0)
             then &lt;fml:NF_ACCOUN_ACCOUNTNAME>{data($bdy/fml:CI_PRODUCT_NAME_ORYGINAL[$ind])}&lt;/fml:NF_ACCOUN_ACCOUNTNAME>
             else &lt;fml:NF_ACCOUN_ACCOUNTNAME nill="true"/>
,
         if ($bdy/fml:B_POJEDYNCZY[$ind] and string-length(data($bdy/fml:B_POJEDYNCZY[$ind]))>0)
             then &lt;fml:NF_IS_COOWNER>{data($bdy/fml:B_POJEDYNCZY[$ind])}&lt;/fml:NF_IS_COOWNER>
             else &lt;fml:NF_IS_COOWNER>N&lt;/fml:NF_IS_COOWNER>
  (:,     
         if ($bdy/fml:CI_STATUS[$ind] and string-length(data($bdy/fml:CI_STATUS[$ind]))>0)
             then &lt;fml:CI_STATUS>{xf:mapCEKEStatus($area,data($bdy/fml:CI_STATUS[$ind]))}&lt;/fml:CI_STATUS>
             else &lt;fml:CI_STATUS nill="true"/>:)
,        
         &lt;NF_PRODUA_CODEPRODUCTAREA>8&lt;/NF_PRODUA_CODEPRODUCTAREA>
,        
         &lt;NF_PRODUD_SORCEPRODUCTCODE nil="true"/>
,
         &lt;NF_CTRL_SYSTEMID>5&lt;/NF_CTRL_SYSTEMID>
(:1.1:)
, 
          &lt;NF_PRODUCT_STAT>T&lt;/NF_PRODUCT_STAT>
,
          &lt;NF_PRODUCT_STATUSCODE nil="true"/>
(:1.1:)
};

declare function xf:mapICBSGetCProAreaResponse ($bdy as element(fml:FML32),$area as xs:string,
      $pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
                                      $actioncode as xs:string )
	as element(fml:FML32){
        let $records:=count( $bdy/fml:DC_NR_RACHUNKU)
        let $start:=xf:calculatePageStart($pagesize,$oldstart,$oldstop,$actioncode,$records)
        let $stop:=xf:calculatePageStop($pagesize,$oldstart,$oldstop,$actioncode,$records)
        let $newnavkey:=concat("012:",string($start),":",string($stop))
        let $hasnext:=xf:calculateHasNext($start,$stop,$records,$actioncode)
        return
           &lt;fml:FML32>
                     (:1.2 start:)
                     {
                             if ($start > $records or $stop &lt;= 0) 
                             then (
		  &lt;NF_PAGEC_NAVIGATIONKEYVALU>012&lt;/NF_PAGEC_NAVIGATIONKEYVALU>,
		  &lt;NF_PAGEC_HASNEXT>0&lt;/NF_PAGEC_HASNEXT>,
		  &lt;NF_PAGECC_OPERATIONS>0&lt;/NF_PAGECC_OPERATIONS>
                                    )
                             else (  
              &lt;NF_PAGEC_NAVIGATIONKEYVALU>{$newnavkey}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>,
              &lt;NF_PAGEC_HASNEXT>{$hasnext}&lt;/NF_PAGEC_HASNEXT>,
              &lt;NF_PAGECC_OPERATIONS>{max((0,$stop - $start+1))}&lt;/NF_PAGECC_OPERATIONS>,
             
                for $idx in ($start to $stop)

                   return
                      xf:mapProduct($bdy,$idx,$area)
                 ) 
              }   (:1.2 koniec :)
           &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $area as xs:string external;
declare variable $pagesize as xs:integer external;
declare variable $oldstart  as xs:integer external;
declare variable $oldstop  as xs:integer external;
declare variable $actioncode  as xs:string external;
&lt;soap-env:Body>
{xf:mapICBSGetCProAreaResponse($body/fml:FML32,$area,$pagesize,$oldstart,$oldstop,$actioncode)}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>