<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2010-05-06 PKLI NP1972_3 CR2 Filtrowanie produktów Streamline na podstawie statusu aktywności</con:description>
  <con:xquery>declare namespace wsdl-jv = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace cmf-holding = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace cmf-biz = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function zwrocKilka($bdy as element(contract)) as element()*{
                 &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE>1&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE>,
                 &lt;fml:NF_ACCOUA_NAME1>{substring(data($bdy/client-list/contract-person/person/name/last-name), 1,40)}&lt;/fml:NF_ACCOUA_NAME1>,
                 &lt;fml:NF_ACCOUA_NAME2>{substring(data($bdy/client-list/contract-person/person/name/last-name),41,40)}&lt;/fml:NF_ACCOUA_NAME2>,
                 &lt;fml:NF_ACCOUA_STREET>{data($bdy/client-list/contract-person/address[@kind="4"]/street)}&lt;/fml:NF_ACCOUA_STREET>,
                  if ($bdy/client-list/contract-person/address[@kind="4"]/flat)
                  then &lt;fml:NF_ACCOUA_HOUSEFLATNUMBER>{
                         concat( data($bdy/client-list/contract-person/address[@kind="4"]/home),
                                 concat( "/", data($bdy/client-list/contract-person/address[@kind="4"]/flat)))
                         }&lt;/fml:NF_ACCOUA_HOUSEFLATNUMBER>
                 else &lt;fml:NF_ACCOUA_HOUSEFLATNUMBER>{data($bdy/client-list/contract-person/address[@kind="4"]/home)}&lt;/fml:NF_ACCOUA_HOUSEFLATNUMBER>
                 ,
                 &lt;fml:NF_ACCOUA_CITY>{data($bdy/client-list/contract-person/address[@kind="4"]/city)}&lt;/fml:NF_ACCOUA_CITY>,
                 &lt;fml:NF_ACCOUA_STATECOUNTRY>{data($bdy/client-list/contract-person/address[@kind="4"]/country)}&lt;/fml:NF_ACCOUA_STATECOUNTRY>,
                 &lt;fml:NF_ACCOUA_ZIPCODE>{data($bdy/client-list/contract-person/address[@kind="4"]/postal-code)}&lt;/fml:NF_ACCOUA_ZIPCODE>
};



declare variable $body as element(soap-env:Body) external;
declare variable $policy_id as xs:string external;
declare variable $system_id as xs:string external;

let $bdy:=$body/wsdl-jv:getContractResponse/contract

return

&lt;soap-env:Body>
        &lt;fml:FML32>	
            &lt;NF_PRODUD_SORCEPRODUCTCODE nill="true"/>
	     &lt;NF_ACCOUN_ACCOUNTNUMBER  nil="true" />
            &lt;NF_TRAACA_LIMITAMOUNT nil="true"/>
            &lt;NF_CURREC_CURRENCYCODE>PLN&lt;/NF_CURREC_CURRENCYCODE>
            &lt;NF_ACCOUN_CURRENTBALANCE?>{data($bdy/amount)}&lt;/NF_ACCOUN_CURRENTBALANCE>
            &lt;NF_ACCOUN_ACCOUNTOPENDATE?>{data($bdy/contract-dates/contract-start)}&lt;/NF_ACCOUN_ACCOUNTOPENDATE>
            &lt;NF_TIMEA_LASTINTERESTPAMD nil="true"/>
            &lt;NF_TIMEA_NEXTRENEWALMATURI nil="true"/>
            &lt;NF_INSURA_ID>{$policy_id}&lt;/NF_INSURA_ID>
            &lt;NF_CUSTOM_CUSTOMERNUMBER?>{data($bdy/client-list/contract-person/@client-code)}&lt;/NF_CUSTOM_CUSTOMERNUMBER>
            &lt;NF_ACCOUN_AVAILABLEBALANCE nil="true"/>
            &lt;NF_HOLD_HOLDAMOUNT nil="true"/>
            &lt;NF_ACCOUN_CREDITPRECENTAGE nil="true"/>

            &lt;NF_CTRL_SYSTEMID>{$system_id}&lt;/NF_CTRL_SYSTEMID>
            &lt;NF_IS_COOWNER>N&lt;/NF_IS_COOWNER>

            &lt;NF_ATTRPG_FIRSTPRODUCTFEAT>{data($bdy/@product-type)}&lt;/NF_ATTRPG_FIRSTPRODUCTFEAT>
            &lt;NF_ATTRPG_SECONDPRODUCTFEA nil="true"/>
            &lt;NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/>
            &lt;NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/>
            &lt;NF_ATTRPG_FIFTHPRODUCTFEAT>PLN&lt;/NF_ATTRPG_FIFTHPRODUCTFEAT>
            &lt;NF_PRODUA_CODEPRODUCTAREA>4&lt;/NF_PRODUA_CODEPRODUCTAREA>
(: v.1.1 start :)
            {
            if (data($bdy/status)="0" or data($bdy/status)="1")
            then &lt;NF_PRODUCT_STAT>T&lt;/NF_PRODUCT_STAT>
            else  &lt;NF_PRODUCT_STAT>N&lt;/NF_PRODUCT_STAT> (: statusy 2, 3, 5, 7, 8 i pozostałe :)
            }
(: v.1.1 end :)
            {
            if (data($bdy/client-list/contract-person/address/@kind)="4")
            then zwrocKilka($bdy)
            else &lt;NF_ACCOUN_ALTERNATIVEADDRE>0&lt;/NF_ACCOUN_ALTERNATIVEADDRE>
            }
            &lt;NF_PAGEC_HASNEXT>0&lt;/NF_PAGEC_HASNEXT>
            &lt;NF_PAGEC_NAVIGATIONKEYVALU>12:000000000000&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
            &lt;NF_PAGEC_PAGESIZE>1&lt;/NF_PAGEC_PAGESIZE>
        &lt;/fml:FML32>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>