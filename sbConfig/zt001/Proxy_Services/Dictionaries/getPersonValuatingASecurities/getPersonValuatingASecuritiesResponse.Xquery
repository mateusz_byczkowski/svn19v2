<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace urn2="urn:entities.be.dcl";
declare namespace urn3="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function Num2Bool($dateIn as xs:string) as xs:boolean {
 if ($dateIn = "1") 
    then true()
    else false()
};

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
&lt;ns0:dicts>
  {
    let $nazwa := $parm/B_NAZWA
    for $kod at $occ in $parm/B_KOD_PS
    return
    &lt;ns3:PersonValuatingASecurity>
      &lt;ns3:personValuatingASecurity?>{data($kod)}&lt;/ns3:personValuatingASecurity>
      &lt;ns3:description?>{data($nazwa[$occ])}&lt;/ns3:description>
    &lt;/ns3:PersonValuatingASecurity>
  }
&lt;/ns0:dicts>
};

declare function getElementsForBcd($parm as element(fml:FML32)) as element()
{
let $lastValue := data(fn:count($parm/B_KOD_PS))
return
&lt;ns0:bcd>
  &lt;urn2:BusinessControlData>
      &lt;urn2:pageControl>
          &lt;urn3:PageControl>
              &lt;urn3:hasNext?>{Num2Bool(data($parm/B_OVERFLOW))}&lt;/urn3:hasNext>
              &lt;urn3:navigationKeyValue?>{data($parm/B_KOD_PS[$lastValue])}&lt;/urn3:navigationKeyValue>
          &lt;/urn3:PageControl>
      &lt;/urn2:pageControl>
  &lt;/urn2:BusinessControlData>
&lt;/ns0:bcd>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
&lt;ns0:invokeResponse>
  {getElementsForDicts($parm)}
  {getElementsForBcd($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>