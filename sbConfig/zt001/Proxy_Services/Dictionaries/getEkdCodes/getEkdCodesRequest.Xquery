<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns1="urn:be.services.dcl";
declare namespace urn1="urn:entities.be.dcl";
declare namespace urn2="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

&lt;soap:Body>
  &lt;fml:FML32>
      &lt;CI_MSHEAD_MSGID?>{data($header/ns1:header/ns1:msgHeader/ns1:msgId)}&lt;/CI_MSHEAD_MSGID>
      &lt;CI_ID_SLOWNIKA>18&lt;/CI_ID_SLOWNIKA>
      &lt;CI_WARTOSC_SLOWNIKA?>{data($body/ns1:invoke/ns1:bcd/urn1:BusinessControlData/urn1:pageControl/urn2:PageControl/urn2:navigationKeyValue)}&lt;/CI_WARTOSC_SLOWNIKA>
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>