<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace mes="http://bzwbk.com/services/messages/";

declare variable $body as element(soap-env:Body) external;

let $currencyCode:=data($body/mes:GetRateTableForAdvisoryRequest/mes:currencyCode)
let $currencyDate:=data($body/mes:GetRateTableForAdvisoryRequest/mes:currencyDate)

return
&lt;soap-env:Body>
  &lt;FML32>
       {
          if ($currencyCode)
              then  &lt;NF_CURREC_CURRENCYCODE>{$currencyCode}&lt;/NF_CURREC_CURRENCYCODE>
              else    &lt;NF_CURREC_CURRENCYCODE>ALL&lt;/NF_CURREC_CURRENCYCODE>
       }
       &lt;NF_CURREC_NUMERICCODE nil="true"/>
       {
           if ($currencyDate)
              then &lt;NF_RATET_CURRENCYDT>{fn-bea:dateTime-to-string-with-format("yyyy-MM-dd",$currencyDate)}&lt;/NF_RATET_CURRENCYDT>
           else  &lt;NF_RATET_CURRENCYDT>{fn-bea:date-to-string-with-format("yyyy-MM-dd",current-date())}&lt;/NF_RATET_CURRENCYDT>
       }
       {
           if ($currencyDate and not( fn-bea:dateTime-to-string-with-format("HHmmss",$currencyDate) = "000000") )
              then &lt;NF_RATET_CURRENCYTM?>{fn-bea:dateTime-to-string-with-format("HHmmss",$currencyDate)}&lt;/NF_RATET_CURRENCYTM>
           else &lt;NF_RATET_CURRENCYTM>0&lt;/NF_RATET_CURRENCYTM>
       }
      
  &lt;/FML32>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>