<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/slowniki/services/dictionary/infobase";



declare function xf:map_GetDictionaryInfoRequest($fml as element(fml:FML32))
	as element(urn:dictionaryInfoRequest) {
		&lt;urn:dictionaryInfoRequest>
			 &lt;dictionaryName>{ data($fml/fml:PT_POLISH_NAME) }&lt;/dictionaryName>
  		&lt;/urn:dictionaryInfoRequest>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_GetDictionaryInfoRequest($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>