<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDateTime($value1 as xs:string,$value2 as xs:string,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
    let $val2withZeros:=concat(substring("000000",1,6 - string-length($value2) ),$value2)
    let $input:=concat($value1,$val2withZeros)
    return
      if ($value1)
        then if(string-length($value1)>5 and not ($value1 = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$input)}
        else() 
      else()
};

declare function getElementsForRateTableList($parm as element(fml:FML32)) as element()
{

&lt;ns4:rateTableList>
  {
    for $x at $occ in $parm/NF_CURREC_CURRENCYCODE
    return
    &lt;ns0:RateTable>
      &lt;ns0:averageRate?>{data($parm/NF_RATET_AVERAGERATE[$occ])}&lt;/ns0:averageRate>
      &lt;ns0:transferBuyRate?>{data($parm/NF_RATET_TRANSFERBUYRATE[$occ])}&lt;/ns0:transferBuyRate>
      &lt;ns0:transferBuyRatePref?>{data($parm/NF_RATET_TRANSFERBUYRATEPR[$occ])}&lt;/ns0:transferBuyRatePref>
      &lt;ns0:transferSellRate?>{data($parm/NF_RATET_TRANSFERSELLRATE[$occ])}&lt;/ns0:transferSellRate>
      &lt;ns0:transferSellRatePref?>{data($parm/NF_RATET_TRANSFERSELLRATEP[$occ])}&lt;/ns0:transferSellRatePref>
      &lt;ns0:transferBuyRateNote?>{data($parm/NF_RATET_TRANSFERBUYRATENO[$occ])}&lt;/ns0:transferBuyRateNote>
      &lt;ns0:transferSellRateNote?>{data($parm/NF_RATET_TRANSFERSELLRATEN[$occ])}&lt;/ns0:transferSellRateNote>
      {insertDateTime(data($parm/NF_RATET_CURRENCYDT[$occ]),data($parm/NF_RATET_CURRENCYTM[$occ]), "yyyy-MM-ddHHmmss","ns0:currencyDate")}
      (:&lt;ns0:currencyDate?>{data($parm/NF_RATET_CURRENCYDATE[$occ])}&lt;/ns0:currencyDate>:)
      &lt;ns0:averageRateNBP?>{data($parm/NF_RATET_AVERAGERATENBP[$occ])}&lt;/ns0:averageRateNBP>
      &lt;ns0:currencyCode>
        &lt;ns2:CurrencyCode>
          &lt;ns2:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns2:currencyCode>
        &lt;/ns2:CurrencyCode>
      &lt;/ns0:currencyCode>
    &lt;/ns0:RateTable>
  }
&lt;/ns4:rateTableList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  {getElementsForRateTableList($parm)}
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>