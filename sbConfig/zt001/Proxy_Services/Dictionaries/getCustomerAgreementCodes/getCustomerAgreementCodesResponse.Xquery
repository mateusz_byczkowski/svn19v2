<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="urn:cifdict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
&lt;ns0:dicts>
  {
    let $opis := $parm/CI_OPIS_WARTOSCI_SLOWNIKA
    for $wartosc at $occ in $parm/CI_WARTOSC_SLOWNIKA
    return
    &lt;ns1:CustomerAgreementCode>
      {if (fn:string-length($wartosc)>0)
        then &lt;ns1:customerAgreementCode>{data($wartosc)}&lt;/ns1:customerAgreementCode>
        else &lt;ns1:customerAgreementCode>{" "}&lt;/ns1:customerAgreementCode>
      }
      &lt;ns1:description?>{data($opis[$occ])}&lt;/ns1:description>
    &lt;/ns1:CustomerAgreementCode>
  }
&lt;/ns0:dicts>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
&lt;ns0:invokeResponse>
  {getElementsForDicts($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>