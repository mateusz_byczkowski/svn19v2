<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/test/testret/";
declare namespace ns0 = "urn:RatingWS";
declare namespace ns1 = "urn:hlbsentities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(SOAP-ENV:Body) external;

declare function xf:testret($sMEProcessPathResponse1 as element())
    as element(ns2:invokeResponse) {
        &lt;ns2:invokeResponse>
            &lt;ns2:getSMEProcessPathOutput>
                &lt;ns1:GetSMEProcessPathOutput>
                    &lt;ns1:decisionResp>{ data($sMEProcessPathResponse1/return/decisionResp) }&lt;/ns1:decisionResp>
                    &lt;ns1:requiredDoc>{ data($sMEProcessPathResponse1/return/requiredDoc) }&lt;/ns1:requiredDoc>
                    &lt;ns1:nextStep>{ data($sMEProcessPathResponse1/return/nextStep) }&lt;/ns1:nextStep>
                    &lt;ns1:destQueue>{ data($sMEProcessPathResponse1/return/destQueue) }&lt;/ns1:destQueue>
                    &lt;ns1:message>{ data($sMEProcessPathResponse1/return/message) }&lt;/ns1:message>
                &lt;/ns1:GetSMEProcessPathOutput>
            &lt;/ns2:getSMEProcessPathOutput>
        &lt;/ns2:invokeResponse>
};


&lt;soap:Body>
{xf:testret($body/ns0:getSMEProcessPathResponse)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>