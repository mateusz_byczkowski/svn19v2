<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getDepositRatesResponse/";
declare namespace dcl  = "urn:be.services.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "urn:filterandmessages.dictionaries.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";

declare function xf:getDepositRatesResponse($fml as element())
    as element(dcl:invokeResponse) {
        &lt;dcl:invokeResponse>

            &lt;dcl:tableDate>
            {
                if (data($fml/SP_DATE[1]))
                    then
                        &lt;ns3:DepositRate>
                              &lt;ns3:date> { fn:concat(fn:translate(data($fml/SP_DATE[1]),' ',''), ':00') } &lt;/ns3:date>
                        &lt;/ns3:DepositRate>
                    else ()
            }
            &lt;/dcl:tableDate>

            &lt;dcl:bcd>
              &lt;ns4:BusinessControlData>
                &lt;ns4:pageControl>
                  &lt;ns5:PageControl>
                    &lt;ns5:hasNext>false&lt;/ns5:hasNext>
                    &lt;ns5:navigationKeyDefinition/>
                    &lt;ns5:navigationKeyValue/>
                  &lt;/ns5:PageControl>
                &lt;/ns4:pageControl>
              &lt;/ns4:BusinessControlData>
            &lt;/dcl:bcd>

            &lt;dcl:depositRatesList>
            {
                for $i in 1 to count($fml/SP_WALUTA)
                return
                   &lt;ns3:DepositRate>
                       {
                           if (data($fml/SP_MIN_PROG[$i]))
                               then
                                   &lt;ns3:thresholdMin> { fn:translate(data($fml/SP_MIN_PROG[$i]),'.','') } &lt;/ns3:thresholdMin>
                               else ()
                       }
                       {
                           if (data($fml/SP_MAX_PROG[$i]))
                               then
                                   &lt;ns3:thresholdMax> { fn:translate(data($fml/SP_MAX_PROG[$i]),'.','') } &lt;/ns3:thresholdMax>
                               else ()
                       }
                       {
                           if (data($fml/SP_KURS[$i]))
                               then
                                   &lt;ns3:rate> { xs:double(data($fml/SP_KURS[$i])) } &lt;/ns3:rate>
                               else ()
                       }
                       &lt;ns3:accountTerm>
                       {
                           fn:concat(data($fml/SP_MIN_CZEST[$i]),
                                     data($fml/SP_MIN_OKRES[$i]),
                                     '-',
                                     data($fml/SP_MAX_CZEST[$i]),
                                     data($fml/SP_MAX_OKRES[$i]))
                       }
                       &lt;/ns3:accountTerm>
                       {
                           if (data($fml/SP_MIN_CZEST[$i]))
                               then
                                   &lt;ns3:frequencyMin> { data($fml/SP_MIN_CZEST[$i]) } &lt;/ns3:frequencyMin>
                               else ()
                       }
                       {
                           if (data($fml/SP_MAX_CZEST[$i]))
                               then
                                   &lt;ns3:frequencyMax> { data($fml/SP_MAX_CZEST[$i]) } &lt;/ns3:frequencyMax>
                               else ()
                       }

                       &lt;ns3:currencyCode>
                           &lt;ns1:CurrencyCode>
                               &lt;ns1:currencyCode> { data($fml/SP_WALUTA[$i]) } &lt;/ns1:currencyCode>
                           &lt;/ns1:CurrencyCode>
                       &lt;/ns3:currencyCode>
                       {
                           if (data($fml/SP_MIN_OKRES[$i]))
                               then
                                   &lt;ns3:periodMin>
                                       &lt;ns1:Period>
                                           &lt;ns1:period> { data($fml/SP_MIN_OKRES[$i]) } &lt;/ns1:period>
                                       &lt;/ns1:Period>
                                   &lt;/ns3:periodMin>
                               else ()
                       }
                       {
                           if (data($fml/SP_MAX_OKRES[$i]))
                               then
                                   &lt;ns3:periodMax>
                                       &lt;ns1:Period>
                                           &lt;ns1:period> { data($fml/SP_MAX_OKRES[$i]) } &lt;/ns1:period>
                                       &lt;/ns1:Period>
                                   &lt;/ns3:periodMax>
                               else ()
                       }
                       {
                           if (data($fml/SP_ID_TABELI[$i]))
                               then
                                   &lt;ns3:idDepositRateTable>
                                       &lt;ns2:DepositRateTables>
                                           &lt;ns2:depositRateTables> { data($fml/SP_ID_TABELI[$i]) } &lt;/ns2:depositRateTables>
                                       &lt;/ns2:DepositRateTables>
                                   &lt;/ns3:idDepositRateTable>
                               else ()
                       }
                   &lt;/ns3:DepositRate>
            }
            &lt;/dcl:depositRatesList>
        &lt;/dcl:invokeResponse>
};

declare variable $fml as element() external;

xf:getDepositRatesResponse($fml)</con:xquery>
</con:xqueryEntry>