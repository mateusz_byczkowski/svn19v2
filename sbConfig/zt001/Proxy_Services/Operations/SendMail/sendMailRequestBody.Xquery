<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace urn="urn:be.services.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace email="http://www.bea.com/wli/sb/transports/email";
declare namespace urn1="urn:filtersandmessages.entities.be.dcl";

declare variable $body as element(soap:Body) external;

declare function prepareMailRequestBody($req as element(urn1:Mail))  {
   let $body := $req/urn1:body
   return data($body)
};


&lt;soap:Body>
{prepareMailRequestBody($body/urn:invoke/urn:mail/urn1:Mail)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>