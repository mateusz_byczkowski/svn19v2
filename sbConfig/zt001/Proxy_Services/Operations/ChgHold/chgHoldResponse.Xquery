<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:accountdict.dictionaries.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns6:invokeResponse>
  &lt;ns6:response>
    &lt;ns0:ResponseMessage>
      &lt;ns0:result> 
       {
        if (data($parm/DC_NUMER_BLOKADY) != "0")
        then "true"
        else "false"
       }
     &lt;/ns0:result>
    &lt;/ns0:ResponseMessage>
  &lt;/ns6:response>
  &lt;ns6:holdOut>
    &lt;ns2:Hold>
      &lt;ns2:holdCreateDate?>{data($parm/DC_DATA_WPROWADZENIA)}&lt;/ns2:holdCreateDate>
      &lt;ns2:holdAcceptDate?>{data($parm/DC_DATA_POTWIERDZ)}&lt;/ns2:holdAcceptDate>
    &lt;/ns2:Hold>
  &lt;/ns6:holdOut>
&lt;/ns6:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>