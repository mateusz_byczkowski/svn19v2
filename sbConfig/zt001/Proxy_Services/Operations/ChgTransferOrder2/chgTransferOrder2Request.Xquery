<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:transferOrderScheduleData($x as element(ns2:TransferOrderSchedule)) as element()*{
&lt;NF_TRANOS_PAYMENTDATE?>{data($x/ns2:paymentDate)}&lt;/NF_TRANOS_PAYMENTDATE>,
&lt;NF_TRANOS_PAYMENTAMOUNT?>{data($x/ns2:paymentAmount)}&lt;/NF_TRANOS_PAYMENTAMOUNT>,
&lt;NF_TRANOS_PAYMENTDESCRIPTI?>{data($x/ns2:paymentDescription)}&lt;/NF_TRANOS_PAYMENTDESCRIPTI>,
&lt;NF_TRANOS_RELATIVERECORDNU?>{data($x/ns2:relativeRecordNumer)}&lt;/NF_TRANOS_RELATIVERECORDNU>
};

declare function xf:getFields($parm as element(ns6:invoke), $msghead as element(ns6:msgHeader), $tranhead as element(ns6:transHeader)) as element(fml:FML32) 
{
  let $msgId:= $msghead/ns6:msgId
  let $companyId:= $msghead/ns6:companyId
  let $userId := $msghead/ns6:userId
  let $appId:= $msghead/ns6:appId
  let $unitId := $msghead/ns6:unitId
  let $timestamp:= $msghead/ns6:timestamp

  let $transId:=$tranhead/ns6:transId

  return
  &lt;fml:FML32>
     &lt;DC_TRN_ID?>{data($transId)}&lt;/DC_TRN_ID>
     (:&lt;DC_UZYTKOWNIK?>{data($userId)}&lt;/DC_UZYTKOWNIK>:)
     &lt;DC_UZYTKOWNIK?>{concat("SKP:", data($userId))}&lt;/DC_UZYTKOWNIK>
     &lt;DC_ODDZIAL?>{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL>
     &lt;NF_TRANOA_FREQUENCY?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:frequency)}&lt;/NF_TRANOA_FREQUENCY>
     &lt;NF_TRANOA_PAYMENTDATE?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:paymentDate)}&lt;/NF_TRANOA_PAYMENTDATE>
     &lt;NF_TRANOA_EXPIRATIONDATE?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:expirationDate)}&lt;/NF_TRANOA_EXPIRATIONDATE>
     &lt;NF_TRANOA_CODEPROFILE?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:codeProfile)}&lt;/NF_TRANOA_CODEPROFILE>
    
{if ($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:nonBusinessDayRule)
                 then  if (data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:nonBusinessDayRule) = "true")
                   then &lt;NF_TRANOA_NONBUSINESSDAYRU?>1&lt;/NF_TRANOA_NONBUSINESSDAYRU>
                   else &lt;NF_TRANOA_NONBUSINESSDAYRU?>0&lt;/NF_TRANOA_NONBUSINESSDAYRU>
                 else()
}

     &lt;NF_TRANOA_TRANSFERORDERNUM?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderNumber)}&lt;/NF_TRANOA_TRANSFERORDERNUM>
     &lt;NF_ACCOUN_CUSTOMERNUMBER?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:referenceNumber)}&lt;/NF_ACCOUN_CUSTOMERNUMBER>
     &lt;NF_TRANOT_AMOUNTTARGET?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:amountTarget)}&lt;/NF_TRANOT_AMOUNTTARGET>
     &lt;NF_TRANOT_CODEPROFILE?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:codeProfile)}&lt;/NF_TRANOT_CODEPROFILE>
     &lt;NF_TRANOT_TRANSFERORDERDES?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:transferOrderDescription)}&lt;/NF_TRANOT_TRANSFERORDERDES>
     &lt;NF_TRANOT_BENEFICIARY1?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:beneficiary1)}&lt;/NF_TRANOT_BENEFICIARY1>
     &lt;NF_TRANOT_BENEFICIARY2?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:beneficiary2)}&lt;/NF_TRANOT_BENEFICIARY2>
     &lt;NF_TRANOT_BENEFICIARY3?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:beneficiary3)}&lt;/NF_TRANOT_BENEFICIARY3>
     &lt;NF_TRANOT_BENEFICIARY4?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:beneficiary4)}&lt;/NF_TRANOT_BENEFICIARY4>
     &lt;NF_INSURA_INSURANCENUMBER?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:referenceNumber)}&lt;/NF_INSURA_INSURANCENUMBER>
     &lt;NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:account/ns1:Account/ns1:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER>
     &lt;NF_ACCOUO_ACCOUNTNUMBER?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:accountOutside/ns2:AccountOutside/ns2:accountNumber)}&lt;/NF_ACCOUO_ACCOUNTNUMBER>
     &lt;NF_GLA_COSTCENTERGL?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:glaccount/ns1:GLAccount/ns1:costCenterGL)}&lt;/NF_GLA_COSTCENTERGL>

{
if(string-length(data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:glaccount/ns1:GLAccount/ns1:accountGL))>0)
 then
     &lt;NF_GLA_ACCOUNTGL?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:glaccount/ns1:GLAccount/ns1:accountGL)}&lt;/NF_GLA_ACCOUNTGL>
 else()
}
     &lt;NF_CURREC_CURRENCYCODE2?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:glaccount/ns1:GLAccount/ns1:currencyCode/ns4:CurrencyCode/ns4:currencyCode)}&lt;/NF_CURREC_CURRENCYCODE2>
     &lt;NF_CURREC_CURRENCYCODE?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderTargetList/ns2:TransferOrderTarget/ns2:currencyCode/ns4:CurrencyCode/ns4:currencyCode)}&lt;/NF_CURREC_CURRENCYCODE>
    (: Tego ma tu nie byc docelowo, numeru rachunku zleceniodawcy nie mozna i tak zmienic :) 
    (: &lt;NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:account/ns1:Account/ns1:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER>:)
     &lt;NF_PERIOD_PERIOD?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:period/ns4:Period/ns4:period)}&lt;/NF_PERIOD_PERIOD>
     &lt;NF_SPECID_SPECIALDAY?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:specificDay/ns4:SpecialDay/ns4:specialDay)}&lt;/NF_SPECID_SPECIALDAY>
     &lt;NF_CURREC_CURRENCYCODE2?>{data($parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:currencyCode/ns4:CurrencyCode/ns4:currencyCode)}&lt;/NF_CURREC_CURRENCYCODE2>
      { 
         for $x in $parm/ns6:transferOrder/ns2:TransferOrderAnchor/ns2:transferOrderScheduleList/ns2:TransferOrderSchedule
            return
                xf:transferOrderScheduleData($x)
       }
&lt;/fml:FML32>
};

&lt;soap:Body>
     { xf:getFields($body/ns6:invoke, $header/ns6:header/ns6:msgHeader, $header/ns6:header/ns6:transHeader)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>