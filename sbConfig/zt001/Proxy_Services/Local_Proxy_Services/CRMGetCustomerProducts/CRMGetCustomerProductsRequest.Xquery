<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.2  2009-11-10 PKLI T43680 Usunięcie funkcji count()
v.1.1  2009-11-10 PKLI NP2036 Dodanie filtra NF_CTRL_COMPANIES</con:description>
  <con:xquery>(: Log Zmian: 
==================================
v.1.1  2009-11-10 PKLI NP2036 Dodanie filtra NF_CTRL_COMPANIES
v.1.2  2009-11-10 PKLI T43680 Usunięcie funkcji count()                                
:)


declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:getCompanyIds($req as element(m:GetCustomerProductsRequest), $idx as xs:integer, $res as xs:string)as xs:string{
   let $len:=string-length(data($req/m:IdSpolki[$idx]))
   let $value:=concat(substring("00",1,2 - $len),data($req/m:IdSpolki[$idx]))
   return
     if ($len > 0 and $len &lt;=2)
         then xf:getCompanyIds($req,$idx+1,concat($res,$value))
         else $res
};

declare function xf:getCompanyFiltrIds($req as element(m:GetCustomerProductsRequest), $idx as xs:integer, $res as xs:string)as xs:string{
   let $len:=string-length(data($req/m:IdSpolkiFiltr[$idx]))
   let $value:=concat(substring("00",1,2 - $len),data($req/m:IdSpolkiFiltr[$idx]))
   return
     if ($len > 0 and $len &lt;=2)
         then xf:getCompanyFiltrIds($req,$idx+1,concat($res,$value))
         else $res
};

declare function xf:mappGetCustomerProductsRequest($req as element(m:GetCustomerProductsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:NF_MSHEAD_USERID>{ data($req/m:IdWewPrac) }&lt;/fml:NF_MSHEAD_USERID>
					else ()
			}
			{
                                (: if(count($req/m:IdSpolki))    //1.2	 :)
				if($req/m:IdSpolki)  (: 1.2 :)
					then &lt;fml:NF_MSHEAD_COMPANYID>{ xf:getCompanyIds($req,1,"") }&lt;/fml:NF_MSHEAD_COMPANYID>
					else ()
			}
			{
				(: if(count($req/m:IdSpolkiFiltr))   //1.2	 :)
                                 if($req/m:IdSpolkiFiltr) (: 1.2 :)
					then &lt;fml:NF_CTRL_COMPANIES>{ xf:getCompanyFiltrIds($req,1,"") }&lt;/fml:NF_CTRL_COMPANIES>
					else ()
			}

			{
				if($req/m:NumerKlienta)
					then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER>{ data($req/m:NumerKlienta) }&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER>
					else ()
			}
			{
				if($req/m:ProductAreaId)
					then &lt;fml:NF_PRODUA_CODEPRODUCTAREA>{ data($req/m:ProductAreaId) }&lt;/fml:NF_PRODUA_CODEPRODUCTAREA>
					else ()
			}
			(:{
				if($req/m:ProductCategoryId)
					then &lt;fml:CI_PRODUCT_CATEGORY_ID>{ data($req/m:ProductCategoryId) }&lt;/fml:CI_PRODUCT_CATEGORY_ID>
					else ()
			}
			{
				if($req/m:ProductAplication)
					then &lt;fml:CI_PRODUCT_APPLICATION>{ data($req/m:ProductAplication) }&lt;/fml:CI_PRODUCT_APPLICATION>
					else ()
			}:)
			(:{
				if($req/m:Opcja)
					then &lt;fml:NF_CTRL_OPTION>{ data($req/m:Opcja) }&lt;/fml:NF_CTRL_OPTION>
					else ()
			}:)
			{
				if($req/m:LiczbaOper)
					then &lt;fml:NF_PAGEC_PAGESIZE>{ data($req/m:LiczbaOper) }&lt;/fml:NF_PAGEC_PAGESIZE>
					else ()
			}
			{
				if($req/m:KluczNawigacyjny)
					then &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU>{ data($req/m:KluczNawigacyjny) }&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU>
					else  ()
			}
			{
				if($req/m:KodAkcji)
					then &lt;fml:NF_PAGEC_ACTIONCODE>{data($req/m:KodAkcji)}&lt;/fml:NF_PAGEC_ACTIONCODE>
					else  &lt;fml:NF_PAGEC_ACTIONCODE>F&lt;/fml:NF_PAGEC_ACTIONCODE>
			}
                       &lt;NF_CTRL_OPTION>0&lt;/NF_CTRL_OPTION>
                       &lt;NF_ACCOUN_ALTERNATIVEADDRE>0&lt;/NF_ACCOUN_ALTERNATIVEADDRE>
                       &lt;NF_PAGEC_REVERSEORDER>0&lt;/NF_PAGEC_REVERSEORDER>
                       &lt;NF_CTRL_ACTIVENONACTIVE>0&lt;/NF_CTRL_ACTIVENONACTIVE>
                       &lt;NF_CTRL_SHOWCURRBAL>2&lt;/NF_CTRL_SHOWCURRBAL>
                       &lt;NF_PRODUG_VISIBILITYCRM>1&lt;/NF_PRODUG_VISIBILITYCRM>

		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappGetCustomerProductsRequest($body/m:GetCustomerProductsRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>