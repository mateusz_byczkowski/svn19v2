<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
   let $length:= string-length($sourceString)
   let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
   let $accountNumber :=  substring($sourceString, $startPos)
   return
     $accountNumber
};

declare function xf:mapCRMDelCustProdAddrRequest($req as element(m:CRMDelCustProdAddrRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:TrnId)
					then &lt;fml:DC_TRN_ID>{ data($req/m:TrnId) }&lt;/fml:DC_TRN_ID>
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK>{ concat("SKP:",data($req/m:Uzytkownik)) }&lt;/fml:DC_UZYTKOWNIK>
					else ()
			}
			{
				if($req/m:Oddzial)
					then &lt;fml:DC_ODDZIAL>{ data($req/m:Oddzial) }&lt;/fml:DC_ODDZIAL>
					else ()
			}
			{
				if($req/m:NumerProduktu)
					then &lt;fml:DC_NUMER_PRODUKTU>{ data($req/m:NumerProduktu) }&lt;/fml:DC_NUMER_PRODUKTU>
					else ()
			}
			{
				if($req/m:NrRachunku)
					then &lt;fml:DC_NR_RACHUNKU>{ cutStr(data($req/m:NrRachunku),10) }&lt;/fml:DC_NR_RACHUNKU>
					else ()
			}
                        &lt;DC_TYP_ZMIANY>D&lt;/DC_TYP_ZMIANY>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMDelCustProdAddrRequest($body/m:CRMDelCustProdAddrRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>