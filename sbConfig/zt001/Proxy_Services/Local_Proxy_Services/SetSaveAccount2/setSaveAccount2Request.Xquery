<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";

declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};

declare function chkUnitId($unitId as xs:string) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else()
};

declare function xf:mapSetSaveAccountRequest($req as element(ns1:Account), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

	let $unitId := $msghead/dcl:unitId
    let $transId:=$tranhead/dcl:transId

	let $customerNumber:= $req/ns1:customerNumber
    let $skpOpener:=$req/ns1:skpOpener
    let $productCode:=$req/ns1:productCode
    let $officerCode:=$req/ns1:officerCode
        
    (: encja TranAccount :)
    let $serviceChargeCode:=$req/ns1:tranAccount/ns1:TranAccount/ns1:serviceChargeCode
    let $interestPlanNumber:=$req/ns1:tranAccount/ns1:TranAccount/ns1:interestPlanNumber
    let $interestTransferAccount:=$req/ns1:tranAccount/ns1:TranAccount/ns1:interestTransferAccount

	(: Encja accountBranchNumber :)
    let $accountBranchNumber:=$req/ns1:accountBranchNumber/ns2:BranchCode/ns2:branchCode

	(: encja statementParameters :)
	let $cycle:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:cycle/ns2:Period/ns2:period
	let $frequency:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:frequency
	let $nextPrintoutDate:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:nextPrintoutDate
	(: let $specialDay:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:specialDay/ns2:SpecialDay/ns2:specialDay :)
	let $specialDay:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:specialDay
	let $provideManner:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:provideManner/ns2:ProvideManner/ns2:provideManner
	let $aggregateTransaction:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:aggregateTransaction
	
	return
          &lt;fml:FML32>
            &lt;DC_TRN_ID?>{data($transId)}&lt;/DC_TRN_ID>
            &lt;DC_UZYTKOWNIK?>{concat("SKP:",data($skpOpener))}&lt;/DC_UZYTKOWNIK>
            &lt;DC_ODDZIAL?>{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL>
            &lt;DC_NUMER_KLIENTA?>{data($customerNumber)}&lt;/DC_NUMER_KLIENTA>
            &lt;DC_RELACJA>SOW&lt;/DC_RELACJA>
            &lt;DC_TYP_RELACJI>1&lt;/DC_TYP_RELACJI>
            &lt;DC_PROCENT_ZOB_PODATKOWYCH>100.0000&lt;/DC_PROCENT_ZOB_PODATKOWYCH>
            &lt;DC_NUMER_PRODUKTU?>{data($productCode)}&lt;/DC_NUMER_PRODUKTU>
            &lt;DC_NUMER_ODDZIALU?>{data($accountBranchNumber)}&lt;/DC_NUMER_ODDZIALU>
            &lt;DC_RODZAJ_RACHUNKU>DM&lt;/DC_RODZAJ_RACHUNKU>
            
            &lt;DC_PLAN_OPLAT?>{data($serviceChargeCode)}&lt;/DC_PLAN_OPLAT>
            &lt;DC_PLAN_ODSETKOWY?>{data($interestPlanNumber)}&lt;/DC_PLAN_ODSETKOWY>
            
			&lt;DC_KOD_CYKLU_WYCIAGOW?>{data($cycle)}&lt;/DC_KOD_CYKLU_WYCIAGOW>
			&lt;DC_LICZBA_OKRESOW_CYKL_WYCIAG?>{data($frequency)}&lt;/DC_LICZBA_OKRESOW_CYKL_WYCIAG>
            {
               if ( string-length($officerCode)>0 )
                 then &lt;DC_KOD_PRACOWNIKA>{data($officerCode)}&lt;/DC_KOD_PRACOWNIKA>
                 else   &lt;DC_KOD_PRACOWNIKA>DCL&lt;/DC_KOD_PRACOWNIKA>                 
             } 
			{
				if ($nextPrintoutDate)
					then 
						&lt;DC_DATA_NASTEPNEGO_WYCIAGU?>{xf:DateTime2CYMD(data($nextPrintoutDate))}&lt;/DC_DATA_NASTEPNEGO_WYCIAGU>					
					else ()
			}
			&lt;DC_OKRESLONY_DZIEN_WYCIAGU?>{data($specialDay)}&lt;/DC_OKRESLONY_DZIEN_WYCIAGU>
			&lt;DC_KOD_SPECJALNYCH_INSTRUKCJI?>{data($provideManner)}&lt;/DC_KOD_SPECJALNYCH_INSTRUKCJI>			
			{
				if ($aggregateTransaction)
                 then  if (data($aggregateTransaction) = "true")
                   then &lt;DC_ZBIJANIE_TRANSAKCJI>1&lt;/DC_ZBIJANIE_TRANSAKCJI>
                   else &lt;DC_ZBIJANIE_TRANSAKCJI>0&lt;/DC_ZBIJANIE_TRANSAKCJI>
                 else()
			}			
          &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


&lt;soap-env:Body>
{ xf:mapSetSaveAccountRequest($body/dcl:invoke/dcl:account/ns1:Account, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>