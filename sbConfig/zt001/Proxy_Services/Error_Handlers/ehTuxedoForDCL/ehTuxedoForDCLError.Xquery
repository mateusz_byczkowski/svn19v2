<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2011-02-15</con:description>
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace urn3="urn:be.services.dcl";
declare variable $body external;
declare variable $fault external;
declare variable $headerCache external;

declare function local:fault($faultString as xs:string, $detail as element()?) as element(soap-env:Fault) {
		&lt;soap-env:Fault>
			&lt;faultcode>soapenv:Server.userException&lt;/faultcode> 
			&lt;faultstring>{ $faultString }&lt;/faultstring> 
			&lt;detail>{ $detail }&lt;/detail>
		&lt;/soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string,
        $errorDescription as xs:string,$errorCodeFromBS as xs:string) as element(err:exceptionItem) {
   &lt;err:exceptionItem>
	&lt;err:errorCode1>{ $errorCode1 }&lt;/err:errorCode1>
	&lt;err:errorCode2>{ $errorCode2 }&lt;/err:errorCode2>
        {
           if (string-length($errorCodeFromBS)>0) 
               then   &lt;err:errorDescription>{ concat($errorCodeFromBS," : ",$errorDescription) }&lt;/err:errorDescription>
               else    &lt;err:errorDescription>{ $errorDescription }&lt;/err:errorDescription>
        }
   &lt;/err:exceptionItem>
};

&lt;soap-env:Body>
	{

               
		let $reason := fn:substring-after(fn:substring-before(data($fault/ctx:reason), "):"), "(")
		let $urcode := fn:substring-before(fn:substring-after(fn:substring-after(data($fault/ctx:reason),":"),":"),":")
		let $timeoutDesc := "Wysołanie usługi przekroczyło dopuszczalny czas odpowiedzi"
		let $wrongInputDesc := "Błędne dane wejściowe"
		let $dcInterfaceDownDesc := "Interfejs DC ICBS jest wyłączony - przetwarzanie niemożliwe"
		let $fmlBufferDesc := "Błąd bufora FML" 
		let $defaultUrcodeDesc := "Błąd wywołania usługi"
		let $defaultDesc := "Krytyczny bład usługi w systemie źródłowym"
		let $alsbErrorDesc := "Wystąpił bład w usłudze proxy na ALSB"
                

		 (: --------DOKLEJANIE NAGLOWKA DO OPISU BLEDU----------------- :)
                 let $messageID := data($headerCache/urn3:header/urn3:msgHeader/urn3:msgId)
                  
               
		return
                  if (fn:substring(data($fault/ctx:errorCode),1,6) = "BEA-38") then 
                    if($reason = "13") then
		       local:fault("err:TimeoutException", element err:TimeoutException { local:errors($reason, $urcode, concat($timeoutDesc, concat(" (",concat($messageID, ")"))),"") })
		    else  if($reason = "11") then
		      if($urcode = "100") then
(:T42213:)         (:local:fault("err:SystemException", element err:SystemException { local:errors($reason, $urcode, concat($dcInterfaceDownDesc, concat(' (',concat($messageID, ')'))),"") }) :)
(:T42213:)         (: local:fault("err:ServiceException", element err:ServiceException { local:errors($reason, $urcode, concat($dcInterfaceDownDesc, concat(' (',concat($messageID, ')'))),"") }) :)
(:T42213:)         local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, concat($dcInterfaceDownDesc, concat(' (',concat($messageID, ')'))),"") })
                      else if($urcode = "101") then
                        local:fault("err:SystemException", element err:SystemException { local:errors($reason, $urcode, concat($fmlBufferDesc, concat(' (',concat($messageID, ')'))),"") })
                      else if($urcode = "102") then
                        local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, concat($wrongInputDesc, concat(' (',concat($messageID, ')'))),"") })
                      else
                        local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, concat($defaultUrcodeDesc, concat(' (',concat($messageID, ')'))),"") })
                    else
                      local:fault("err:ServiceException", element err:ServiceException { local:errors($reason, $urcode, concat($defaultDesc, concat(' (',concat($messageID, ')'))),"")})

                 else
                    local:fault("err:SystemException", element err:SystemException { local:errors("0","0",concat($alsbErrorDesc, concat(' (',concat($messageID, ')'))),"" )})
             }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>