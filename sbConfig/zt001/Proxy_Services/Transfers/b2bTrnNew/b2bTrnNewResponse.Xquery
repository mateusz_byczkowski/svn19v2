<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/ceke/messages/";



declare function xf:map_b2bTrnNewResponse($fml as element(fml:FML32))
	as element(urn:TrnNewResponse) {
		&lt;urn:TrnNewResponse>
			{
				let $DATE := $fml/fml:E_TRN_DATE

				for $it1 at $p1 in $fml/fml:E_TRN_DATE
				return
				(
					if($DATE[$p1] and data($DATE[$p1])) then
					(
						&lt;urn:TrnDate>{ concat(substring(data($DATE[$p1]),7,4),'-', substring(data($DATE[$p1]),4,2),'-',substring(data($DATE[$p1]),1,2))}&lt;/urn:TrnDate>
					) else (
						&lt;urn:TrnDate xsi:nil="true">&lt;/urn:TrnDate>
					)
				),

				let $ID := $fml/fml:E_TRN_ID

				for $it2 at $p2 in $fml/fml:E_TRN_ID
				return
				(
					if($ID[$p2] and data($ID[$p2])) then
					(
						if(data($ID[$p2] = 0)) then
						(
							&lt;urn:TrnId xsi:nil="true">&lt;/urn:TrnId>
						) else (
							&lt;urn:TrnId>{ data($ID[$p2]) }&lt;/urn:TrnId>
						)
					) else
					(
						&lt;urn:TrnId xsi:nil="true">&lt;/urn:TrnId>
					)
				),

				let $STATE := $fml/fml:E_TRN_STATE

				for $it3 at $p3 in $fml/fml:E_TRN_STATE
				return
				(
					if($STATE[$p3] and data($STATE[$p3])) then
					(
						&lt;urn:TrnState>{ data($STATE[$p3]) }&lt;/urn:TrnState>
					) else
					(
						&lt;urn:TrnState xsi:nil="true">&lt;/urn:TrnState>
					)
				),

				let $ERROR := $fml/fml:E_SEND_ERROR

				for $it4 at $p4 in $fml/fml:E_SEND_ERROR
				return
				(
					if($ERROR[$p4] and data($ERROR[$p4])) then
						(
							if(data($ERROR[$p4]) = 0) then
							(
								&lt;urn:TrnError>0&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 13) then
							(
								&lt;urn:TrnError>13&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 16) then
							(
								&lt;urn:TrnError>16&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 17) then
							(
								&lt;urn:TrnError>17&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 21) then
							(
								&lt;urn:TrnError>21&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 23) then
							(
								&lt;urn:TrnError>23&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 24) then
							(
								&lt;urn:TrnError>24&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 27) then
							(
								&lt;urn:TrnError>27&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 42) then
							(
								&lt;urn:TrnError>42&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 43) then
							(
								&lt;urn:TrnError>43&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 51) then
							(
								&lt;urn:TrnError>51&lt;/urn:TrnError>
							) else if(data($ERROR[$p4]) = 52) then
							(
								&lt;urn:TrnError>52&lt;/urn:TrnError>
							) else
							(
								&lt;urn:TrnError>1001&lt;/urn:TrnError>
							)
					) else (
						&lt;urn:TrnError xsi:nil="true">&lt;/urn:TrnError>
					)
				)


			}
		&lt;/urn:TrnNewResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_b2bTrnNewResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>