<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$5.2011-05-13</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:operations.entities.be.dcl";
declare namespace urn2="urn:dictionaries.be.dcl";
declare namespace urn3="urn:operationsdictionary.dictionaries.be.dcl";
declare namespace urn4="urn:basedictionaries.be.dcl";

declare namespace xf="urn:be.services.dcl";


declare variable $body external;
declare variable $header external;


declare variable $trans as element (urn1:Transaction):=$body/urn:invoke/urn:transaction/urn1:Transaction;
declare variable $transWn as element (urn1:TransactionWn):=$trans/urn1:transactionWn/urn1:TransactionWn;
declare variable $transMa as element (urn1:TransactionMa):=$trans/urn1:transactionMa/urn1:TransactionMa;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string
{
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:dateTime) as xs:string 
{
       fn:concat (xf:convertTo2CharString(fn:day-from-dateTime($dateIn)),"-",
                          xf:convertTo2CharString(fn:month-from-dateTime($dateIn)),"-",
                          fn:string(fn:year-from-dateTime($dateIn)))
};

declare function xf:mapTime($dateTimeIn as xs:dateTime) as xs:string 
{
     let $string := $dateTimeIn cast as xs:string
     return
       fn:concat (fn:substring($string,12,2),fn:substring($string,15,2),fn:substring($string,18,2))
};
 
 declare function xf:round ( $value as xs:double)  as xs:double {
    fn:round-half-to-even($value,  6)
};

declare function xf:isNumeric ( $value as xs:string)  as xs:boolean {
       
   string(number($value)) != 'NaN'
 } ;

declare function xf:createUserId ( $userId as xs:string)  as xs:string {
    if (xf:isNumeric ($userId))
            then fn:concat("SKP:",$userId) 
    else
            $userId
};

&lt;soap-env:Body>
    &lt;FML32>
    &lt;!--  header -->
        &lt;TR_ID_OPER?>{data($trans/urn1:transactionID)}&lt;/TR_ID_OPER>
        &lt;TR_DATA_OPER?> {xf:mapDate( $trans/urn1:putDownDate)}&lt;/TR_DATA_OPER>
        &lt;TR_CZAS_OPER?> {xf:mapTime( $trans/urn1:putDownDate)}&lt;/TR_CZAS_OPER>
        &lt;TR_ODDZ_KASY?>{data($trans/urn1:branchCode/urn4:BaseBranchCode/urn4:branchCode)}&lt;/TR_ODDZ_KASY>
        &lt;TR_KASA?>{data($trans/urn1:tillNumber)}&lt;/TR_KASA>
        &lt;TR_KASJER?>{data($trans/urn1:tellerID)}&lt;/TR_KASJER>
 
        &lt;TR_UZYTKOWNIK>
        {if ($header/urn:header/urn:msgHeader/urn:userId) then
               let  $userId:=data($header/urn:header/urn:msgHeader/urn:userId)
               return  xf:createUserId ($userId)
         else
               let  $userId:=data($body/urn:header/urn:msgHeader/urn:userId)
               return xf:createUserId ($userId)
        }
        &lt;/TR_UZYTKOWNIK>

        &lt;TR_FLAGA_AKCEPT?>{data($trans/urn1:acceptanceFlag)}&lt;/TR_FLAGA_AKCEPT>
    
    &lt;!--  main  -->
    
        &lt;TR_TYTUL?>{data($trans/urn1:title)}&lt;/TR_TYTUL>
        &lt;TR_TYP_KOM?>{data($trans/urn1:internalTransactionType/urn3:InternalTransactionType/urn3:internalType)}&lt;/TR_TYP_KOM>
        &lt;TR_KANAL?>{data($trans/urn1:externalTransactionType/urn3:ExternalTransactionType/urn3:externalType)}&lt;/TR_KANAL>
        &lt;TR_DATA_WALUTY?> {xf:mapDate( $trans/urn1:transactionDate)}&lt;/TR_DATA_WALUTY>
        
    &lt;!--  oplaty  -->

        {for $fee at $i in $trans/urn1:fees/urn1:feeAmount  return
            (element {fn:concat('TR_OPLATA_',xs:string($i))} {data($fee)})
         }

    &lt;!--  WN -->
         
        &lt;TR_RACH_NAD?>{data($transWn/urn1:accountNumber)}&lt;/TR_RACH_NAD>
        { if  ($transWn/urn1:amountWn) then
        &lt;TR_KWOTA_NAD>{xf:round($transWn/urn1:amountWn)}&lt;/TR_KWOTA_NAD>
        else()
        }
        &lt;TR_WALUTA_NAD?>{data($transWn/urn1:currencyCode/urn2:CurrencyCode/urn2:currencyCode)}&lt;/TR_WALUTA_NAD>
        { if  ($transWn/urn1:rate) then
        &lt;TR_KURS_NAD>{xf:round($transWn/urn1:rate)}&lt;/TR_KURS_NAD>
        else()
        }
        { if (fn:string-length($transWn/urn1:name)>0) then
        &lt;TR_NAZWA_NAD>{fn:substring($transWn/urn1:name,1,35)}&lt;/TR_NAZWA_NAD>
        else ()
        }
        { if (fn:string-length($transWn/urn1:name)>35) then
        &lt;TR_NAZWA_NAD_2>{fn:substring($transWn/urn1:name,36,35)}&lt;/TR_NAZWA_NAD_2>
        else ()
        }

        &lt;TR_MIEJSCOWOSC_NAD?>{data($transWn/urn1:city)}&lt;/TR_MIEJSCOWOSC_NAD>
        &lt;TR_ULICA_NAD?>{data($transWn/urn1:adsress)}&lt;/TR_ULICA_NAD>
        &lt;TR_KOD_POCZT_NAD?>{data($transWn/urn1:zipCode)}&lt;/TR_KOD_POCZT_NAD>

        &lt;!--  MA -->
        
        &lt;TR_RACH_ADR?>{data($transMa/urn1:accountNumber)}&lt;/TR_RACH_ADR>
        { if  ($transMa/urn1:amountMa) then
        &lt;TR_KWOTA_ADR>{xf:round($transMa/urn1:amountMa)}&lt;/TR_KWOTA_ADR>
        else()
        }
     
        &lt;TR_WALUTA_ADR?>{data($transMa/urn1:currencyCode/urn2:CurrencyCode/urn2:currencyCode)}&lt;/TR_WALUTA_ADR>
        { if  ($transMa/urn1:rate) then
        &lt;TR_KURS_ADR>{xf:round($transMa/urn1:rate)}&lt;/TR_KURS_ADR>
        else()
        }
        { if (fn:string-length($transMa/urn1:name)>0) then
        &lt;TR_NAZWA_ADR>{fn:substring($transMa/urn1:name,1,35)}&lt;/TR_NAZWA_ADR>
        else ()
        }
        { if (fn:string-length($transMa/urn1:name)>35) then
        &lt;TR_NAZWA_ADR_2>{fn:substring($transMa/urn1:name,36,35)}&lt;/TR_NAZWA_ADR_2>
        else ()
        }
        
        &lt;TR_MIEJSCOWOSC_ADR?>{data($transMa/urn1:city)}&lt;/TR_MIEJSCOWOSC_ADR>
        &lt;TR_ULICA_ADR?>{data($transMa/urn1:address)}&lt;/TR_ULICA_ADR>
        &lt;TR_KOD_POCZT_ADR?>{data($transMa/urn1:zipCode)}&lt;/TR_KOD_POCZT_ADR>
        
        &lt;!--  ZUS -->
        {
        if ($trans/urn1:transactionZUS) then
            let  $transZUS:=$trans/urn1:transactionZUS/urn1:TransactionZUS
            return
                (&lt;TR_ZUS_NIP?>{data($transZUS/urn1:nip)}&lt;/TR_ZUS_NIP>,
                &lt;TR_ZUS_TYP_IDENTYF?>{data($transZUS/urn1:idType/urn3:IDType/urn3:idType)}&lt;/TR_ZUS_TYP_IDENTYF>,
                &lt;TR_ZUS_IDENTYF_DOD?>{data($transZUS/urn1:idValue)}&lt;/TR_ZUS_IDENTYF_DOD>,
                &lt;TR_ZUS_TYP_PLAT?>{data($transZUS/urn1:paymentType/urn3:ZUSPaymentType/urn3:paymentType)}&lt;/TR_ZUS_TYP_PLAT>,
                &lt;TR_ZUS_NR_DEKLAR?>{data($transZUS/urn1:declarationNumber)}&lt;/TR_ZUS_NR_DEKLAR>,
                &lt;TR_ZUS_DATA_DEKLAR?>{data($transZUS/urn1:declarationDate)}&lt;/TR_ZUS_DATA_DEKLAR>,
                &lt;TR_ZUS_NAZWA_SKR_POD?>{data($transZUS/urn1:name)}&lt;/TR_ZUS_NAZWA_SKR_POD>)
        else()
        }

        &lt;!--  US -->
        {
        if ($trans/urn1:transactionUS) then
            let  $transUS:=$trans/urn1:transactionUS/urn1:TransctionUS
            return
            (&lt;TR_US_TYP_IDENTYF?>{data($transUS/urn1:idType/urn3:IDType/urn3:idType)}&lt;/TR_US_TYP_IDENTYF>,
            &lt;TR_US_TYP_OKRESU?>{data($transUS/urn1:periodType/urn3:USPeriodType/urn3:periodType)}&lt;/TR_US_TYP_OKRESU>,
            &lt;TR_US_OKRES?>{data($transUS/urn1:period/urn3:USPeriod/urn3:period)}&lt;/TR_US_OKRES>,
            &lt;TR_US_SYMBOL_FORM?>{data($transUS/urn1:symbolForm/urn3:SymbolForm/urn3:symbolForm)}&lt;/TR_US_SYMBOL_FORM>,
            &lt;TR_US_IDENTYF_ZOBOW?>{data($transUS/urn1:commitmentID)}&lt;/TR_US_IDENTYF_ZOBOW>,
            &lt;TR_US_OPIS_ZOBOW?>{data($transUS/urn1:commitmentDesc)}&lt;/TR_US_OPIS_ZOBOW>,
            &lt;TR_US_NAZWA_SKR_POD?>{data($transUS/urn1:name)}&lt;/TR_US_NAZWA_SKR_POD>,
            &lt;TR_US_IDENTYF_DOD?>{data($transUS/urn1:idValue)}&lt;/TR_US_IDENTYF_DOD>)
        else()
        }
        &lt;!-- Dysponenci -->

     {  for $dysp  in $trans/urn1:disposerList/urn1:Disposer    return
                 (
                 &lt;TR_DYSP_CIF?>{data($dysp/urn1:cif)}&lt;/TR_DYSP_CIF>,
                 &lt;TR_DYSP_NAZWA?>{data($dysp/urn1:firstName),data($dysp/urn1:lastName)}&lt;/TR_DYSP_NAZWA>,
                  &lt;TR_DYSP_OBYWATELSTWO?>{data($dysp/urn1:citizenship/urn2:CountryCode/urn2:countryCode)}&lt;/TR_DYSP_OBYWATELSTWO>,
                  &lt;TR_DYSP_KRAJ?>{data($dysp/urn1:countryCode/urn2:CountryCode/urn2:countryCode)}&lt;/TR_DYSP_KRAJ>,
                  &lt;TR_DYSP_MIEJSCOWOSC?>{data($dysp/urn1:city)}&lt;/TR_DYSP_MIEJSCOWOSC>,
                  &lt;TR_DYSP_KOD_POCZT?>{data($dysp/urn1:zipCode)}&lt;/TR_DYSP_KOD_POCZT>,
                  &lt;TR_DYSP_ULICA?>{data($dysp/urn1:address)}&lt;/TR_DYSP_ULICA>,
                  &lt;TR_DYSP_PESEL?>{data($dysp/urn1:pesel)}&lt;/TR_DYSP_PESEL>,
                  &lt;TR_DYSP_NR_PASZPORTU?>{data($dysp/urn1:passportNumber)}&lt;/TR_DYSP_NR_PASZPORTU>,
                  &lt;TR_DYSP_NR_DOWOD?>{data($dysp/urn1:documentNumber)}&lt;/TR_DYSP_NR_DOWOD>
                 )
             }


    &lt;/FML32>
    
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>