<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForCollateral($parm as element(fml:FML32)) as element()
{

&lt;ns5:collateral>
  {
    for $x at $occ in $parm/NF_COLLCO_COLLATERALCODE
    return
    &lt;ns0:Collateral>
      &lt;ns0:collateralDescription?>{data($parm/NF_COLLAT_COLLATERALDESCRI[$occ])}&lt;/ns0:collateralDescription>
      &lt;ns0:collateralItemNumber?>{data($parm/NF_COLLAT_COLLATERALITEMNU[$occ])}&lt;/ns0:collateralItemNumber>
      &lt;ns0:collateralCode>
        &lt;ns2:CollateralCode>
          &lt;ns2:collateralCode?>{data($parm/NF_COLLCO_COLLATERALCODE[$occ])}&lt;/ns2:collateralCode>
        &lt;/ns2:CollateralCode>
      &lt;/ns0:collateralCode>
    &lt;/ns0:Collateral>
  }
&lt;/ns5:collateral>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse>
  {getElementsForCollateral($parm)}
&lt;/ns5:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>