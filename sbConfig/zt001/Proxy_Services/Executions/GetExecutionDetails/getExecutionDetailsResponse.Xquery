<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace ns0="urn:executiondict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace ns6="urn:executions.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForAddressList($parm as element(fml:FML32)) as element()
{

&lt;ns1:addressList>
  {
    for $x at $occ in $parm/NF_ADDRET_ADDRESSTYPE
    return
    &lt;ns1:Address>
      &lt;ns1:houseFlatNumber?>{data($parm/NF_ADDRES_HOUSEFLATNUMBER[$occ])}&lt;/ns1:houseFlatNumber>
      &lt;ns1:city?>{data($parm/NF_ADDRES_CITY[$occ])}&lt;/ns1:city>
      &lt;ns1:street?>{data($parm/NF_ADDRES_STREET[$occ])}&lt;/ns1:street>
      &lt;ns1:zipCode?>{data($parm/NF_ADDRES_ZIPCODE[$occ])}&lt;/ns1:zipCode>
      &lt;ns1:addressType>
        &lt;ns3:AddressType>
          &lt;ns3:addressType?>1&lt;/ns3:addressType>
        &lt;/ns3:AddressType>
      &lt;/ns1:addressType>
    &lt;/ns1:Address>
  }
&lt;/ns1:addressList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse>
  &lt;ns5:executionOut>
    &lt;ns6:Execution>
      &lt;ns6:executionNumberLPP?>{data($parm/NF_EXECUT_EXECUTIONNUMBERL)}&lt;/ns6:executionNumberLPP>
      &lt;ns6:signature?>{data($parm/NF_EXECUT_SIGNATURE)}&lt;/ns6:signature>
      &lt;ns6:nameOfExecutor?>{data($parm/NF_EXECUT_NAMEOFEXECUTOR)}&lt;/ns6:nameOfExecutor>
      &lt;ns6:nameOfHolder?>{data($parm/NF_EXECUT_NAMEOFHOLDER)}&lt;/ns6:nameOfHolder>
      { insertDate(data($parm/NF_EXECUT_STARTEXECUTIONDA),"yyyy-MM-dd","ns6:startExecutionDate")}
      { insertDate(data($parm/NF_EXECUT_INCOMINGDATE),"yyyy-MM-dd","ns6:incomingDate")}
      { insertDate(data($parm/NF_EXECUT_EXECUTIONCLOSEDA),"yyyy-MM-dd","ns6:executionCloseDate")}
      {
         if (data($parm/NF_EXECUT_FREEAMOUNTAVAILA) = "-1") then
           ()
         else
           &lt;ns6:freeAmountAvailable?>{sourceValue2Boolean (data($parm/NF_EXECUT_FREEAMOUNTAVAILA),"1")}&lt;/ns6:freeAmountAvailable>
      }
      &lt;ns6:totalExecutionAmount?>{data($parm/NF_EXECUT_TOTALEXECUTIONAM)}&lt;/ns6:totalExecutionAmount>
      &lt;ns6:executionAmount?>{data($parm/NF_EXECUT_EXECUTIONAMOUNT)}&lt;/ns6:executionAmount>
      &lt;ns6:executionExpense?>{data($parm/NF_EXECUT_EXECUTIONEXPENSE)}&lt;/ns6:executionExpense>
      &lt;ns6:otherExecutionExpense?>{data($parm/NF_EXECUT_OTHEREXECUTIONEX)}&lt;/ns6:otherExecutionExpense>
      &lt;ns6:executorInterest?>{data($parm/NF_EXECUT_EXECUTORINTEREST)}&lt;/ns6:executorInterest>
      &lt;ns6:bankInterest?>{data($parm/NF_EXECUT_BANKINTEREST)}&lt;/ns6:bankInterest>
      &lt;ns6:transferFee?>{data($parm/NF_EXECUT_TRANSFERFEE)}&lt;/ns6:transferFee>
      &lt;ns6:executorCharge?>{data($parm/NF_EXECUT_EXECUTORCHARGE)}&lt;/ns6:executorCharge>
      &lt;ns6:executionStatus>
        &lt;ns0:ExecutionStatus>
          &lt;ns0:executionStatus?>{data($parm/NF_EXECUS_EXECUTIONSTATUS)}&lt;/ns0:executionStatus>
        &lt;/ns0:ExecutionStatus>
      &lt;/ns6:executionStatus>
      &lt;ns6:customer>
        &lt;ns1:Customer>
          &lt;ns1:companyName?>{data($parm/NF_CUSTOM_COMPANYNAME)}&lt;/ns1:companyName>
          &lt;ns1:customerNumber?>{data($parm/NF_CUSTOM_CUSTOMERNUMBER)}&lt;/ns1:customerNumber>
          {getElementsForAddressList($parm)}
          &lt;ns1:customerPersonal>
            &lt;ns1:CustomerPersonal>
              &lt;ns1:lastName?>{data($parm/NF_CUSTOP_LASTNAME)}&lt;/ns1:lastName>
              &lt;ns1:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME)}&lt;/ns1:firstName>
            &lt;/ns1:CustomerPersonal>
          &lt;/ns1:customerPersonal>
          &lt;ns1:customerType>
            &lt;ns3:CustomerType>
              &lt;ns3:customerType?>{data($parm/NF_CUSTOT_CUSTOMERTYPE)}&lt;/ns3:customerType>
            &lt;/ns3:CustomerType>
          &lt;/ns1:customerType>
        &lt;/ns1:Customer>
      &lt;/ns6:customer>
      &lt;ns6:executionPaymentSummary>
        &lt;ns6:ExecutionPaymentSummary>
          &lt;ns6:executionAmount?>{data($parm/NF_EXECPS_EXECUTIONAMOUNT)}&lt;/ns6:executionAmount>
          &lt;ns6:executionExpense?>{data($parm/NF_EXECPS_EXECUTIONEXPENSE)}&lt;/ns6:executionExpense>
          &lt;ns6:otherExecutionExpense?>{data($parm/NF_EXECPS_OTHEREXECUTIONEX)}&lt;/ns6:otherExecutionExpense>
          &lt;ns6:executorInterest?>{data($parm/NF_EXECPS_EXECUTORINTEREST)}&lt;/ns6:executorInterest>
          &lt;ns6:bankInterest?>{data($parm/NF_EXECPS_BANKINTEREST)}&lt;/ns6:bankInterest>
          &lt;ns6:transferFee?>{data($parm/NF_EXECPS_TRANSFERFEE)}&lt;/ns6:transferFee>
          &lt;ns6:executorCharge?>{data($parm/NF_EXECPS_EXECUTORCHARGE)}&lt;/ns6:executorCharge>
          &lt;ns6:totalPayment?>{data($parm/NF_EXECPS_TOTALPAYMENT)}&lt;/ns6:totalPayment>
        &lt;/ns6:ExecutionPaymentSummary>
      &lt;/ns6:executionPaymentSummary>
    &lt;/ns6:Execution>
  &lt;/ns5:executionOut>
&lt;/ns5:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>