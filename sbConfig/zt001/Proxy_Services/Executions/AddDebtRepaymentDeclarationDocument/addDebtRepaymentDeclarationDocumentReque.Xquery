<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:executions.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns5="urn:filtersandmessages.entities.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if($parm = "false")
       then $falseval
    else $parm
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:freeAmountRepayment)) > 0)
then
   &lt;NF_DEBTRD_FREEAMOUNTREPAYM?>{boolean2SourceValue(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:freeAmountRepayment),"1","0")}&lt;/NF_DEBTRD_FREEAMOUNTREPAYM>
else()
,
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:acaRepayment)) > 0)
then
&lt;NF_DEBTRD_ACAREPAYMENT?>{boolean2SourceValue(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:acaRepayment),"1","0")}&lt;/NF_DEBTRD_ACAREPAYMENT>
else()
,
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:entireRepayment)) > 0)
then
&lt;NF_DEBTRD_ENTIREREPAYMENT?>{boolean2SourceValue(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:entireRepayment),"1","0")}&lt;/NF_DEBTRD_ENTIREREPAYMENT>
else()
,
&lt;NF_DEBTRD_REPAYMENTAMOUNT?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:repaymentAmount)}&lt;/NF_DEBTRD_REPAYMENTAMOUNT>
,
&lt;NF_DEBTRD_REPAYMENTDESCRIP?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:repaymentDescription)}&lt;/NF_DEBTRD_REPAYMENTDESCRIP>
,
&lt;NF_DEBTRD_ISSUEDATE?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueDate)}&lt;/NF_DEBTRD_ISSUEDATE>
,
&lt;NF_BRANCC_BRANCHCODE?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueBranch/ns4:BranchCode/ns4:branchCode)}&lt;/NF_BRANCC_BRANCHCODE>
,
&lt;NF_BRANCC_DESCRIPTION?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueBranch/ns4:BranchCode/ns4:description)}&lt;/NF_BRANCC_DESCRIPTION>
,
&lt;NF_BRANCC_FULLADDRESS?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueBranch/ns4:BranchCode/ns4:fullAddress)}&lt;/NF_BRANCC_FULLADDRESS>
,
&lt;NF_CUSTOM_COMPANYNAME?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:companyName)}&lt;/NF_CUSTOM_COMPANYNAME>
,
&lt;NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerNumber)}&lt;/NF_CUSTOM_CUSTOMERNUMBER>
,
&lt;NF_ADDRES_FULLADDRESS?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:addressList/ns2:Address/ns2:fullAddress)}&lt;/NF_ADDRES_FULLADDRESS>
,
for $x in data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:executionList/ns3:Execution/ns3:executionNumberLPP)
   return 
     &lt;NF_EXECUT_EXECUTIONNUMBERL?>{$x}&lt;/NF_EXECUT_EXECUTIONNUMBERL>
,
&lt;NF_CUSTOP_LASTNAME?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerPersonal/ns2:CustomerPersonal/ns2:lastName)}&lt;/NF_CUSTOP_LASTNAME>
,
&lt;NF_CUSTOP_FIRSTNAME?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerPersonal/ns2:CustomerPersonal/ns2:firstName)}&lt;/NF_CUSTOP_FIRSTNAME>
,
&lt;NF_CUSTOT_CUSTOMERTYPE?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerType/ns4:CustomerType/ns4:customerType)}&lt;/NF_CUSTOT_CUSTOMERTYPE>
,
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountNumber)) > 0) 
then
 &lt;NF_ACCOUN_ACCOUNTNUMBER?>{cutStr(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountNumber),12)}&lt;/NF_ACCOUN_ACCOUNTNUMBER>
else()

,
&lt;NF_BRANCC_BRANCHCODE?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountBranchNumber/ns4:BranchCode/ns4:branchCode)}&lt;/NF_BRANCC_BRANCHCODE>
,
&lt;NF_BRANCC_DESCRIPTION?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountBranchNumber/ns4:BranchCode/ns4:description)}&lt;/NF_BRANCC_DESCRIPTION>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>