<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:filtersandmessages.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

<soap-env:Body>
	<m:invokeResponse>
		<m:response>
			<e:ResponseMessage>
				<e:result>true</e:result>
			</e:ResponseMessage>
		</m:response>
	</m:invokeResponse>		
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>