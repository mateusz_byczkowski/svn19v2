<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/prime/";



declare function xf:map_GetCustomerDetailsRequest($fml as element(fml:FML32))
	as element(urn:GetCustomerDetails) {
		&lt;urn:GetCustomerDetails>
			&lt;urn:peopleSerno>{ data($fml/fml:CI_NR_ZRODLOWY_KLIENTA)}&lt;/urn:peopleSerno>
		&lt;/urn:GetCustomerDetails>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_GetCustomerDetailsRequest($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>