<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(: Log Zmian: 
v.1.0  2010-08-19  PKLI NP1836 Utworzenie
:)

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:getElementsForProductsList($fml as element(fml:FML32)) as element()* {
  let $NF_ACCOUN_ACCOUNTNUMBER  := $fml/fml:NF_ACCOUN_ACCOUNTNUMBER 
  let $NF_ACCOUN_ACCOUNTNAME := $fml/fml:NF_ACCOUN_ACCOUNTNAME
  let $NF_ACCOUN_ACCOUNTDESCRIPTI := $fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI
  let $NF_CURREC_CURRENCYCODE := $fml/fml:NF_CURREC_CURRENCYCODE
  let $NF_ACCOUR_RELATIONSHIP := $fml/fml:NF_ACCOUR_RELATIONSHIP           
  let $NF_APPLIN_APPLICATIONNUMBE:= $fml/fml:NF_APPLIN_APPLICATIONNUMBE 
  let $NF_ACCOUN_PRODUCTCODE := $fml/fml:NF_ACCOUN_PRODUCTCODE
  let $NF_BRANCC_BRANCHCODE := $fml/fml:NF_BRANCC_BRANCHCODE
  let $NF_ACCOUT_ACCOUNTTYPE := $fml/fml:NF_ACCOUT_ACCOUNTTYPE
  let $NF_ACCOUN_CURRENTBALANCE := $fml/fml:NF_ACCOUN_CURRENTBALANCE
  let $NF_ACCOUN_CURRENTBALANCEPL:= $fml/fml:NF_ACCOUN_CURRENTBALANCEPL
  let $NF_ACCOUN_AVAILABLEBALANCE:=$fml/fml:NF_ACCOUN_AVAILABLEBALANCE
  let $NF_PRODUCT_STATUSCODE:=$fml/fml:NF_PRODUCT_STATUSCODE
  let $NF_ACCOTF_ACCOUNTTYPEFLAG:= $fml/fml:NF_ACCOTF_ACCOUNTTYPEFLAG
  let $NF_PRODAT_IDPRODUCTDEFINIT := $fml/fml:NF_PRODAT_IDPRODUCTDEFINIT
  let $PT_ID_GROUP := $fml/fml:PT_ID_GROUP
  let $NF_PRODUCT_STAT := $fml/fml:NF_PRODUCT_STAT
  let $NF_PRODUA_CODEPRODUCTAREA := $fml/fml:NF_PRODUA_CODEPRODUCTAREA	

  for $it at $p in $fml/fml:NF_ACCOUN_ACCOUNTNUMBER return
    &lt;m:product>	
     {
        if($fml/fml:NF_ACCOUN_ACCOUNTNUMBER[$p])
        then &lt;m:accountNumber>{ data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER[$p]) }&lt;/m:accountNumber>
        else &lt;m:accountNumber/>
      }
     {
        if($fml/fml:NF_ACCOUN_ACCOUNTNAME[$p])
        then &lt;m:accountName>{ data($fml/fml:NF_ACCOUN_ACCOUNTNAME[$p]) }&lt;/m:accountName>
        else  &lt;m:accountName/>
      }
     {
        if($fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI[$p])
        then &lt;m:accountDescription>{ data($fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI[$p]) }&lt;/m:accountDescription>
        else &lt;m:accountDescription/>
      }
     {
        if($fml/fml:NF_CURREC_CURRENCYCODE[$p])
        then &lt;m:currencyCode>{ data($fml/fml:NF_CURREC_CURRENCYCODE[$p]) }&lt;/m:currencyCode>
        else &lt;m:currencyCode/>
      }
     {
        if($fml/fml:NF_ACCOUR_RELATIONSHIP[$p])
        then &lt;m:relationship>{ data($fml/fml:NF_ACCOUR_RELATIONSHIP[$p]) }&lt;/m:relationship>
        else &lt;m:relationship/>
      }
     {
        if($fml/fml:NF_APPLIN_APPLICATIONNUMBE[$p])
        then &lt;m:codeProductSystem>{ data($fml/fml:NF_APPLIN_APPLICATIONNUMBE[$p]) }&lt;/m:codeProductSystem>
        else &lt;m:codeProductSystem>0&lt;/m:codeProductSystem>
      }
     {
        if($fml/fml:NF_ACCOUN_PRODUCTCODE[$p])
        then &lt;m:productCode>{ data($fml/fml:NF_ACCOUN_PRODUCTCODE[$p]) }&lt;/m:productCode>
        else &lt;m:productCode/>
      }
     {
        if($fml/fml:NF_BRANCC_BRANCHCODE[$p])
        then &lt;m:accountBranchNumber>{ data($fml/fml:NF_BRANCC_BRANCHCODE[$p]) }&lt;/m:accountBranchNumber>
        else &lt;m:accountBranchNumber>0&lt;/m:accountBranchNumber>
      }
     {
        if($fml/fml:NF_ACCOUT_ACCOUNTTYPE[$p])
        then &lt;m:accountType>{ data($fml/fml:NF_ACCOUT_ACCOUNTTYPE[$p]) }&lt;/m:accountType>
        else &lt;m:accountType/>
      }
     {
        if($fml/fml:NF_ACCOUN_CURRENTBALANCE[$p])
        then &lt;m:currentBalance>{ data($fml/fml:NF_ACCOUN_CURRENTBALANCE[$p]) }&lt;/m:currentBalance>
        else &lt;m:currentBalance>0&lt;/m:currentBalance>
      }
     {
        if($fml/fml:NF_ACCOUN_CURRENTBALANCEPL[$p])
        then &lt;m:currentBalancePLN>{ data($fml/fml:NF_ACCOUN_CURRENTBALANCEPL[$p]) }&lt;/m:currentBalancePLN>
        else &lt;m:currentBalancePLN>0&lt;/m:currentBalancePLN>
      }
     {
        if($fml/fml:NF_ACCOUN_AVAILABLEBALANCE[$p])
        then &lt;m:availableBalance>{ data($fml/fml:NF_ACCOUN_AVAILABLEBALANCE[$p]) }&lt;/m:availableBalance>
        else &lt;m:availableBalance>0&lt;/m:availableBalance>
      }
     {
        if($fml/fml:NF_PRODUCT_STATUSCODE[$p])
        then &lt;m:accountStatus>{ data($fml/fml:NF_PRODUCT_STATUSCODE[$p]) }&lt;/m:accountStatus>
        else &lt;m:accountStatus/>
      }
     {
        if($fml/fml:NF_FEET_FEETYPE[$p])
        then &lt;m:feeType>{ data($fml/fml:NF_FEET_FEETYPE[$p]) }&lt;/m:feeType>
        else &lt;m:feeType>0&lt;/m:feeType>
      }
     {
        if($fml/fml:NF_ACCOTF_ACCOUNTTYPEFLAG[$p])
        then &lt;m:accountTypeFlag>{ data($fml/fml:NF_ACCOTF_ACCOUNTTYPEFLAG[$p]) }&lt;/m:accountTypeFlag>
        else &lt;m:accountTypeFlag/>
      }
     {
        if($fml/fml:NF_PRODAT_IDPRODUCTDEFINIT[$p])
        then &lt;m:idProductDefinition>{ data($fml/fml:NF_PRODAT_IDPRODUCTDEFINIT[$p]) }&lt;/m:idProductDefinition>
        else &lt;m:idProductDefinition>0&lt;/m:idProductDefinition>
      }
     {
        if($fml/fml:PT_ID_GROUP[$p])
        then &lt;m:idProductGroup>{ data($fml/fml:PT_ID_GROUP[$p]) }&lt;/m:idProductGroup>
        else &lt;m:idProductGroup>0&lt;/m:idProductGroup>
      }
     {
        if($fml/fml:NF_PRODUCT_STAT[$p])
        then &lt;m:accountActiveStatus>{ data($fml/fml:NF_PRODUCT_STAT[$p]) }&lt;/m:accountActiveStatus>
        else &lt;m:accountActiveStatus/>
      }
     {
        if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p])
        then &lt;m:codeProductArea>{ data($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p]) }&lt;/m:codeProductArea>
        else &lt;m:codeProductArea>0&lt;/m:codeProductArea>
      }

    &lt;/m:product>
			
};

declare function xf:getElementsForPageControl($fml as element(fml:FML32)) as element()* {
    &lt;m:pageControl>	
      {
        if($fml/fml:NF_PAGECC_OPERATIONS)
        then &lt;m:Operations>{ data($fml/fml:NF_PAGECC_OPERATIONS) }&lt;/m:Operations>
        else ()
      }
      {
        if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
        then &lt;m:navigationKeyValue>{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/m:navigationKeyValue>
        else ()
      }
      {
        if($fml/fml:NF_PAGEC_HASNEXT)
        then &lt;m:hasNext>{ data($fml/fml:NF_PAGEC_HASNEXT) }&lt;/m:hasNext>
        else ()
      }

   &lt;/m:pageControl>	
};     

declare function xf:getElementsForServiceWarningsList($fml as element(fml:FML32)) as element()* {
  let $NF_SERVIW_WARNINGCODE1 := $fml/fml:NF_SERVIW_WARNINGCODE1
  let $NF_SERVIW_WARNINGCODE2 := $fml/fml:NF_SERVIW_WARNINGCODE2
  let $NF_SERVIW_WARNINGUSERVISIB := $fml/fml:NF_SERVIW_WARNINGUSERVISIB
  let $NF_SERVIW_WARNINGDESCRIPTI := $fml/fml:NF_SERVIW_WARNINGDESCRIPTI 
 
  for $it at $p in $fml/fml:NF_SERVIW_WARNINGCODE1 return
    &lt;m:serviceWarning>	
      {
        if($NF_SERVIW_WARNINGCODE1[$p])
        then &lt;m:WarningCode1>{ data($NF_SERVIW_WARNINGCODE1[$p]) }&lt;/m:WarningCode1>
        else ()
      }
      {
        if($NF_SERVIW_WARNINGCODE2[$p])
        then &lt;m:WarningCode2>{ data($NF_SERVIW_WARNINGCODE2[$p]) }&lt;/m:WarningCode2>
        else ()
      }
      {
        if($NF_SERVIW_WARNINGUSERVISIB[$p])
        then &lt;m:WarningUserVisibility>{ data($NF_SERVIW_WARNINGUSERVISIB[$p]) }&lt;/m:WarningUserVisibility>
        else ()
      }
      {
        if($NF_SERVIW_WARNINGDESCRIPTI[$p])
        then &lt;m:WarningDescription>{ data($NF_SERVIW_WARNINGDESCRIPTI[$p]) }&lt;/m:WarningDescription>
        else ()
      } 
    &lt;/m:serviceWarning>	
};        
        
declare function xf:mappGetCustomerProductsResponse($fml as element(fml:FML32)) as element() {
  &lt;m:GetCustomerProductsResponse>
     {xf:getElementsForProductsList($fml)} 
     {xf:getElementsForPageControl($fml)} 
     {xf:getElementsForServiceWarningsList($fml)} 
  &lt;/m:GetCustomerProductsResponse>        
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappGetCustomerProductsResponse($body/fml:FML32) }

&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>