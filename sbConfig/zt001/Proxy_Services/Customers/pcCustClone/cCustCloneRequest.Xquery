<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-10</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappcCustCloneRequest($req as element(m:cCustCloneRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:IdSys)
                                    then &lt;fml:CI_ID_SYS>{ data($req/m:IdSys) }&lt;/fml:CI_ID_SYS>
		                 else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappcCustCloneRequest($body/m:cCustCloneRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>