<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>(:Change log 
v.1.0  2010-06-28  PKL  NP2042 Utworzenie 
v.1.1  2010-09-02  PKL  NP2042 CR1 Rozszerzenie usługi o parametr wyjściowy LoanAccount.faceAmount 

:)

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn= "urn:be.services.dcl";
declare namespace urn1="urn:productstree.entities.be.dcl";
declare namespace urn2="urn:accounts.entities.be.dcl";
declare namespace urn3="urn:dictionaries.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
  &lt;urn:invokeResponse>
    &lt;urn:productDefinition>
        &lt;urn1:ProductDefinition>   
        &lt;urn1:idProductGroup?>{data($parm/PT_ID_GROUP)}&lt;/urn1:idProductGroup>
        &lt;urn1:idProductDefinition?>{data($parm/CI_PRODUCT_ID)}&lt;/urn1:idProductDefinition>
        &lt;/urn1:ProductDefinition>
    &lt;/urn:productDefinition>
    &lt;urn:accountOut>
        &lt;urn2:Account>
            &lt;urn2:accountOpenDate?>{data($parm/B_D_OTWARCIA)}&lt;/urn2:accountOpenDate>
            &lt;urn2:currentBalance?>{data($parm/B_SALDO)}&lt;/urn2:currentBalance>
            &lt;urn2:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI)}&lt;/urn2:accountDescription>
            &lt;urn2:currentBalancePLN?>{data($parm/NF_ACCOUN_CURRENTBALANCEPL)}&lt;/urn2:currentBalancePLN>
            &lt;urn2:loanAccount>
                &lt;urn2:LoanAccount>
(:1.1:)       &lt;urn2:faceAmount?>{data($parm/B_KREDYT_PRZYZN)}&lt;/urn2:faceAmount>
                &lt;urn2:payoff?>{data($parm/NF_LOANA_PAYOFF)}&lt;/urn2:payoff>
                &lt;urn2:accountToPayObligations?>{data($parm/NF_LOANA_ACCOUNTTOPAYOBLIG)}&lt;/urn2:accountToPayObligations>
                &lt;/urn2:LoanAccount>
            &lt;/urn2:loanAccount>
            &lt;urn2:currency>
                &lt;urn3:CurrencyCode>
                    &lt;urn3:currencyCode?>{data($parm/B_KOD_WALUTY)}&lt;/urn3:currencyCode>
                &lt;/urn3:CurrencyCode>
            &lt;/urn2:currency>
        &lt;/urn2:Account>
    &lt;/urn:accountOut>
  &lt;/urn:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>