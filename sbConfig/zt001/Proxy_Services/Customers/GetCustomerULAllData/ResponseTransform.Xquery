<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/GetCustomerAllData/ResponseTransform2/";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace ns-1 = "urn:baseentities.be.dcl";
declare namespace ns1 = "urn:basedictionaries.be.dcl";
declare namespace ns3 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns6 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace ns7 = "urn:cifdict.dictionaries.be.dcl";


declare function local:intToBoolean($tn as xs:string?)
   as xs:string {
if($tn) then
	if ($tn eq '') then ''
	else xs:string($tn ne '0')
else ''
};

declare function local:DMYToYMD($dateDMY as xs:string?)
   as xs:string {
   if($dateDMY) then
	   if ($dateDMY eq '') then ''
	   else fn:string-join((fn:substring($dateDMY, 7, 4), fn:substring($dateDMY, 4, 2),fn:substring($dateDMY, 1, 2)), '-')
   else ''
};


declare function xf:ResponseTransform2($cISGetCustomerResponse1 as element(ns3:CISGetCustomerResponse),
    									 $invoke1 as element(ns4:invoke) )
    as element(ns4:invokeResponse) {
        &lt;ns4:invokeResponse>
        
            &lt;ns4:messageHelper>
                &lt;ns-1:MessageHelper>
                    &lt;ns-1:errorCode?>&lt;/ns-1:errorCode>
                    &lt;ns-1:message?>&lt;/ns-1:message>
                &lt;/ns-1:MessageHelper>
            &lt;/ns4:messageHelper>

            &lt;ns4:customer>
                &lt;ns0:Customer>

                	{
                	for $RezNierez in $cISGetCustomerResponse1/ns3:RezNierez
                	where $RezNierez != ''
                	return
                    	&lt;ns0:resident?>{ local:intToBoolean( data($RezNierez) ) }&lt;/ns0:resident>
					}
                    &lt;ns0:taxID?>{ data($cISGetCustomerResponse1/ns3:Nip) }&lt;/ns0:taxID>
                    &lt;ns0:phoneNo?>{ data($cISGetCustomerResponse1/ns3:NrTelefonu) }&lt;/ns0:phoneNo>
                    &lt;ns0:mobileNo?>{ data($cISGetCustomerResponse1/ns3:NrTelefKomorkowego) }&lt;/ns0:mobileNo>
                    &lt;ns0:email?>{ data($cISGetCustomerResponse1/ns3:AdresEMail) }&lt;/ns0:email>
                    {
                   for $companyType in $invoke1/ns4:customer/ns0:Customer/ns0:companyID/ns2:CompanyType/ns2:companyType 
                    where $companyType = 1
                     return
                     &lt;ns0:businessNo?>{data($cISGetCustomerResponse1/ns3:NrTelefonuPrac)}&lt;/ns0:businessNo>
                    }
                   {
                   for $companyType in $invoke1/ns4:customer/ns0:Customer/ns0:companyID/ns2:CompanyType/ns2:companyType 
                    where $companyType = 7
                     return
                     &lt;ns0:businessNo?>{data($cISGetCustomerResponse1/ns3:NumerTelefonuPraca)}&lt;/ns0:businessNo>                                                            
                    }
                    &lt;ns0:agreementBankInfo>{ local:intToBoolean( data($cISGetCustomerResponse1/ns3:ZgodaTajemBank) ) }&lt;/ns0:agreementBankInfo>
                    &lt;ns0:addressList>
                        &lt;ns0:Address>
                            &lt;ns0:houseFlatNumber?>{ data($cISGetCustomerResponse1/ns3:NrPosesLokaluDanePodst) }&lt;/ns0:houseFlatNumber>
                            &lt;ns0:city?>{ data($cISGetCustomerResponse1/ns3:MiastoDanePodst) }&lt;/ns0:city>
                            {
                            for $AdresOd in $cISGetCustomerResponse1/ns3:AdresOd
                            where $AdresOd!= ''
                            return
                            	&lt;ns0:validFrom>{local:DMYToYMD( data($AdresOd))} &lt;/ns0:validFrom>
                            }
                            {
                            for $AdresDo in $cISGetCustomerResponse1/ns3:AdresDo
                            where $AdresDo!= ''
                            return
                            	&lt;ns0:validTo?>{ local:DMYToYMD( data($AdresDo) ) }&lt;/ns0:validTo>
                            }
                            &lt;ns0:street?>{ data($cISGetCustomerResponse1/ns3:UlicaDanePodst) }&lt;/ns0:street>
                            &lt;ns0:zipCode?>{ data($cISGetCustomerResponse1/ns3:KodPocztowyDanePodst) }&lt;/ns0:zipCode>
                        	
                        	&lt;ns0:addressType>
                        		&lt;ns2:AddressType>
                        			&lt;ns2:addressType>1&lt;/ns2:addressType>
                    			&lt;/ns2:AddressType>
                        	&lt;/ns0:addressType>
                            {
                            	for $wojew in $cISGetCustomerResponse1/ns3:Wojewodztwo
                            	return
		                            &lt;ns0:state>
		                                &lt;ns2:State>
			                                &lt;ns2:state>{ data($wojew) }&lt;/ns2:state>
		                                &lt;/ns2:State>
		                            &lt;/ns0:state>
                            }
                            {
                            	for $kraj in $cISGetCustomerResponse1/ns3:KodKraju
                            	return
		                            &lt;ns0:countryId>
		                                &lt;ns2:CountryCode>
			                                &lt;ns2:isoCountryCode>{ xs:string( data($kraj) ) }&lt;/ns2:isoCountryCode>
		                                &lt;/ns2:CountryCode>
		                            &lt;/ns0:countryId>
                            }
                            {
                            	for $typAdresu in $cISGetCustomerResponse1/ns3:TypAdresu
                            	return
		                            &lt;ns0:typeOfLocation>
		                            	&lt;ns2:AddressTypeOfLocation>
		                            		&lt;ns2:addressTypeOfLocation>{data($typAdresu)}&lt;/ns2:addressTypeOfLocation>
		                            	&lt;/ns2:AddressTypeOfLocation>
		                            &lt;/ns0:typeOfLocation>
                            }
                        &lt;/ns0:Address>
                        {
                        	for $adresKoresp in $cISGetCustomerResponse1/ns3:CISGetCustomerAdresAlt
                        	return
		                        &lt;ns0:Address>
		                            &lt;ns0:houseFlatNumber?>{ data($adresKoresp/ns3:NrPosesLokaluAdresAlt) }&lt;/ns0:houseFlatNumber>
		                            &lt;ns0:city?>{ data($adresKoresp/ns3:MiastoAdresAlt) }&lt;/ns0:city>
		                            {
		                            for $validFrom in $adresKoresp/ns3:DataOd
		                            where $validFrom!=''
		                            return
		                            	&lt;ns0:validFrom>{ local:DMYToYMD( data($validFrom) ) }&lt;/ns0:validFrom>
		                            }
             						{
		                            for $validTo in $adresKoresp/ns3:DataDo
		                            where $validTo!=''
		                            return
		                            	&lt;ns0:validTo>{ local:DMYToYMD( data($validTo) ) }&lt;/ns0:validTo>
		                            }
		                            &lt;ns0:street?>{ data($adresKoresp/ns3:UlicaAdresAlt) }&lt;/ns0:street>
		                            &lt;ns0:zipCode?>{ data($adresKoresp/ns3:KodPocztowyAdresAlt) }&lt;/ns0:zipCode>
		                        	&lt;ns0:addressType>
		                        		&lt;ns2:AddressType>
		                        			&lt;ns2:addressType>2&lt;/ns2:addressType>
		                    			&lt;/ns2:AddressType>
		                        	&lt;/ns0:addressType>
		                            {
		                            	for $wojew in $adresKoresp/ns3:WojewodztwoKrajAdresAlt
		                            	return
				                            &lt;ns0:state>
				                                &lt;ns2:State>
					                                &lt;ns2:state>{ data($wojew) }&lt;/ns2:state>
				                                &lt;/ns2:State>
				                            &lt;/ns0:state>
		                            }
		                            {
		                            	for $kraj in $adresKoresp/ns3:KodKrajuKoresp
		                            	return
				                            &lt;ns0:countryId>
				                                &lt;ns2:CountryCode>
					                                &lt;ns2:isoCountryCode>{ xs:string( data($kraj) ) }&lt;/ns2:isoCountryCode>
				                                &lt;/ns2:CountryCode>
				                            &lt;/ns0:countryId>
		                            }
		                            {
		                            	for $typAdresu in $adresKoresp/ns3:TypAdresuKoresp
		                            	return
				                            &lt;ns0:typeOfLocation>
				                            	&lt;ns2:AddressTypeOfLocation>
				                            		&lt;ns2:addressTypeOfLocation>{data($typAdresu)}&lt;/ns2:addressTypeOfLocation>
				                            	&lt;/ns2:AddressTypeOfLocation>
				                            &lt;/ns0:typeOfLocation>
		                            }
		                        &lt;/ns0:Address>
                        }
                    &lt;/ns0:addressList>
                    {
	                    for $SeriaNrDok in $cISGetCustomerResponse1/ns3:SeriaNrDok
	                    return
	                    	&lt;ns0:documentList>
	            				&lt;ns0:Document>
	                        		&lt;ns0:documentNumber>{ data($SeriaNrDok) }&lt;/ns0:documentNumber>
		                            {
		                            	for $DokTozsamosci in $cISGetCustomerResponse1/ns3:DokTozsamosci
		                            	return
			                                &lt;ns0:documentType>
	                                			&lt;ns2:CustomerDocumentType>
			                                    	&lt;ns2:customerDocumentType?>{ data($DokTozsamosci) }&lt;/ns2:customerDocumentType>
	                                			&lt;/ns2:CustomerDocumentType>
	                            			&lt;/ns0:documentType>
                            		}
                        		&lt;/ns0:Document>
                    		&lt;/ns0:documentList>
                    }


                    {
                        for $KodZawodu in $cISGetCustomerResponse1/ns3:KodZawoduWyuczonego
                        return
                        	&lt;ns0:customerEmploymentInfoList>
                        		&lt;ns0:CustomerEmploymentInfo>
                            		&lt;ns0:occupationCode>
                                		&lt;ns2:CustomerOccupationCode>
                            				&lt;ns2:customerOccupationCode>{ xs:string( data($KodZawodu) ) }&lt;/ns2:customerOccupationCode>
                                		&lt;/ns2:CustomerOccupationCode>
                            		&lt;/ns0:occupationCode>
                        		&lt;/ns0:CustomerEmploymentInfo>
                    		&lt;/ns0:customerEmploymentInfoList>
                    }
                    &lt;ns0:customerInsuranceList>
                        &lt;ns0:CustomerInsurance>
                        	{
	                        	let $policyInvest := $cISGetCustomerResponse1/ns3:UbezpFundInwest
	                        	return
		                        	if ($policyInvest != '') then
			                        	&lt;ns0:policyInvest?>{ local:intToBoolean( data($policyInvest) ) }&lt;/ns0:policyInvest>
			                        else ''
	                        }
                        	{
	                        	let $policyLife := $cISGetCustomerResponse1/ns3:UbezpieczenieNaZycie
	                        	return
		                        	if ($policyLife != '') then
			                        	&lt;ns0:policyLife?>{ local:intToBoolean( data($policyLife) ) }&lt;/ns0:policyLife>
			                        else ''
	                        }
                       		{
	                        	let $policyOther := $cISGetCustomerResponse1/ns3:UbezpieczeniaInne
	                        	return
		                        	if ($policyOther != '') then
			                        	&lt;ns0:policyOther?>{ local:intToBoolean( data($policyOther) ) }&lt;/ns0:policyOther>
			                        else ''
	                        }
	                        {
	                        let $policyTotalAmount := $cISGetCustomerResponse1/ns3:SumaPolisNaZycie
	                        return 
	                        	if ($policyTotalAmount != '') then
	                        		&lt;ns0:policyTotalAmount?>{ xs:double( data($policyTotalAmount) ) }&lt;/ns0:policyTotalAmount>
	                        	else ''
	                        }
	                        {
	                        let $policyPremiumSum := $cISGetCustomerResponse1/ns3:SumaSkladekPolis
	                        return 
	                        	if ($policyPremiumSum != '') then
	                        		&lt;ns0:policyPremiumSum?>{ xs:double( data($policyPremiumSum) ) }&lt;/ns0:policyPremiumSum>
	                        	else ''
	                        }
	                        {
	                        let $policyNo := $cISGetCustomerResponse1/ns3:LiczbaPolis
	                        return 
	                        	if ($policyNo != '') then
	                        		&lt;ns0:policyNo?>{ xs:int( data($policyNo) ) }&lt;/ns0:policyNo>
	                        	else ''
	                        }
	                        &lt;ns0:policyInvestName?>
	                        	&lt;ns6:InsurerName?>
	                        		&lt;ns6:insurerName?>{ data($cISGetCustomerResponse1/ns3:NazwaUbezpFundInwest) }&lt;/ns6:insurerName>
	                        	&lt;/ns6:InsurerName>
	                        &lt;/ns0:policyInvestName>
	                        &lt;ns0:policyLifeName?>
	                        	&lt;ns6:InsurerName?>
	                        		&lt;ns6:insurerName?>{ data($cISGetCustomerResponse1/ns3:UbezpNaZycie) }&lt;/ns6:insurerName>
	                        	&lt;/ns6:InsurerName>
	                        &lt;/ns0:policyLifeName>
	                        &lt;ns0:policyOtherName?>
	                        	&lt;ns6:InsurerName?>
	                        		&lt;ns6:insurerName?>{ data($cISGetCustomerResponse1/ns3:UbezpInne) }&lt;/ns6:insurerName>
	                        	&lt;/ns6:InsurerName>
	                        &lt;/ns0:policyOtherName>	                        
                        &lt;/ns0:CustomerInsurance>
                    &lt;/ns0:customerInsuranceList>
                    &lt;ns0:customerAdditionalInfo>
                    	 &lt;ns0:CustomerAdditionalInfo>
                            {
                            for $KodJezyka in $cISGetCustomerResponse1/ns3:KodJezyka
                            return
	                            &lt;ns0:language>
	                                &lt;ns1:Language>
                                        &lt;ns1:languageID>{ data($KodJezyka) }&lt;/ns1:languageID>
	                                &lt;/ns1:Language>
	                            &lt;/ns0:language>                                    
                            }
                            {
                            for $ZrodloDanych in $cISGetCustomerResponse1/ns3:ZrodloDanych 
                            return 
	                            &lt;ns0:dataSource>
		                         	&lt;ns2:DataSource>
		                         		&lt;ns2:dataSource>{ data($ZrodloDanych) }&lt;/ns2:dataSource>
		                         	&lt;/ns2:DataSource>
		                         &lt;/ns0:dataSource>
	                       }
                    	 &lt;/ns0:CustomerAdditionalInfo>
                    &lt;/ns0:customerAdditionalInfo>
                    {
                    for $ZrodloDochodu in $cISGetCustomerResponse1/ns3:ZrodloDochodu
                    where $ZrodloDochodu != ''
                    return                    
                    	&lt;ns0:customerFinancialInfo>
                	 		&lt;ns0:CustomerFinancialInfo>
	                            &lt;ns0:sourceOfIncomeCode>
	                                &lt;ns2:SourceOfIncomeCode>
	                                    &lt;ns2:sourceOfIncomeCode>{ data($ZrodloDochodu) }&lt;/ns2:sourceOfIncomeCode>
	                                &lt;/ns2:SourceOfIncomeCode>
	                            &lt;/ns0:sourceOfIncomeCode>
                           	 &lt;/ns0:CustomerFinancialInfo>
            			&lt;/ns0:customerFinancialInfo>
               		}
                    &lt;ns0:customerPersonal>
                        &lt;ns0:CustomerPersonal>
                            &lt;ns0:birthPlace?>{ data($cISGetCustomerResponse1/ns3:MiejsceUrodzenia) }&lt;/ns0:birthPlace>
                            &lt;ns0:motherMaidenName?>{ data($cISGetCustomerResponse1/ns3:NazwiskoPanienskieMatki) }&lt;/ns0:motherMaidenName>
                            {
                            for $dateOfBirth in $cISGetCustomerResponse1/ns3:DataUrodzenia
                            where $dateOfBirth!=''
                            return
                            	&lt;ns0:dateOfBirth>{local:DMYToYMD( data($dateOfBirth) ) }&lt;/ns0:dateOfBirth>
                            }
                            &lt;ns0:lastName?>{ data($cISGetCustomerResponse1/ns3:Nazwisko) }&lt;/ns0:lastName>
                            &lt;ns0:secondName?>{ data($cISGetCustomerResponse1/ns3:DrugieImie) }&lt;/ns0:secondName>
                            &lt;ns0:pesel?>{ data($cISGetCustomerResponse1/ns3:NrPesel) }&lt;/ns0:pesel>
                            &lt;ns0:firstName?>{ data($cISGetCustomerResponse1/ns3:Imie) }&lt;/ns0:firstName>
                            &lt;ns0:identityCardNumber?>{ data($cISGetCustomerResponse1/ns3:NrDowoduRegon) }&lt;/ns0:identityCardNumber>
                            &lt;ns0:passportNumber?>{ data($cISGetCustomerResponse1/ns3:NumerPaszportu) }&lt;/ns0:passportNumber>
                            

                            {
                                for $Plec in $cISGetCustomerResponse1/ns3:Plec
                                return
		                            &lt;ns0:sexCode>
		                                &lt;ns2:CustomerSexCode>
		                                    &lt;ns2:customerSexCode>{ data($Plec) }&lt;/ns2:customerSexCode>
		                                &lt;/ns2:CustomerSexCode>
		                            &lt;/ns0:sexCode>
                           }
                           {
		                        for $KrajPochodzenia in $cISGetCustomerResponse1/ns3:KrajPochodzenia
		                        return
		                            &lt;ns0:countryOfOrigin>
		                                &lt;ns2:CountryCode>
                                            &lt;ns2:isoCountryCode>{ data($KrajPochodzenia) }&lt;/ns2:isoCountryCode>
                                		&lt;/ns2:CountryCode>
                            		&lt;/ns0:countryOfOrigin>
                            }
                            {
	                            for $Obywatelstwo in $cISGetCustomerResponse1/ns3:Obywatelstwo
	                            return
		                            &lt;ns0:citizenship>
		                            	&lt;ns2:CitizenshipCode>
		                            		&lt;ns2:isoCitizenshipCode>{ data($Obywatelstwo) }&lt;/ns2:isoCitizenshipCode>
		                        		&lt;/ns2:CitizenshipCode>
		                            &lt;/ns0:citizenship>
                            }
                            {
	                            for $KrajZamieszkania in $cISGetCustomerResponse1/ns3:KrajZamieszkania
	                            return
                            		&lt;ns0:countryOfResidence>
                                		&lt;ns2:CountryCode>
                                            &lt;ns2:isoCountryCode>{ data($KrajZamieszkania) }&lt;/ns2:isoCountryCode>
                                		&lt;/ns2:CountryCode>
                            		&lt;/ns0:countryOfResidence>
                            }


                        &lt;/ns0:CustomerPersonal>
                    &lt;/ns0:customerPersonal>
                    {
	                    for $NumerOddzialu in $cISGetCustomerResponse1/ns3:NumerOddzialu
	                    return
		                    &lt;ns0:branchOfOwnership>
		                        &lt;ns2:BranchCode>
                                    &lt;ns2:branchCode>{ xs:string( data($NumerOddzialu) ) }&lt;/ns2:branchCode>
		                        &lt;/ns2:BranchCode>
		                    &lt;/ns0:branchOfOwnership>
                    }
                    {
	                    for $TypKlienta in $cISGetCustomerResponse1/ns3:TypKlienta
	                    return
		                    &lt;ns0:customerType>
		                        &lt;ns2:CustomerType>
                                    &lt;ns2:customerType>{ xs:string( data($TypKlienta) ) }&lt;/ns2:customerType>
		                        &lt;/ns2:CustomerType>
		                    &lt;/ns0:customerType>
                    }
                    {
                    	for $approval in $cISGetCustomerResponse1/ns3:UdostepGrupa
                    	where $approval != ''
                    	return
		                    &lt;ns0:processingApproval?>
		                    	&lt;ns2:CustomerProcessingApproval?>
		                    		&lt;ns2:customerProcessingApproval?>{ data($approval) }&lt;/ns2:customerProcessingApproval>
		                    	&lt;/ns2:CustomerProcessingApproval>
		                    &lt;/ns0:processingApproval>
                    }
                    {
	                    for $KartaWzorowPodpisow in $cISGetCustomerResponse1/ns3:KartaWzorowPodpisow
	                    return
		                    &lt;ns0:signatureCardBranchId>
		                        &lt;ns2:BranchCode>
                                    &lt;ns2:branchCode>{ data($KartaWzorowPodpisow) }&lt;/ns2:branchCode>
		                        &lt;/ns2:BranchCode>
		                    &lt;/ns0:signatureCardBranchId>
                    }
                    {
                    	for $approval in $cISGetCustomerResponse1/ns3:ZgodaInfHandlowa
                    	where $approval != ''
                    	return
		                    &lt;ns0:agreementCode?>
		                    	&lt;ns7:CustomerAgreementCode?>
		                    		&lt;ns7:customerAgreementCode?>{ data($approval) }&lt;/ns7:customerAgreementCode>
		                    	&lt;/ns7:CustomerAgreementCode>
		                    &lt;/ns0:agreementCode>
                    }
                &lt;/ns0:Customer>
            &lt;/ns4:customer>
        &lt;/ns4:invokeResponse>
};
declare variable $invoke1 as element(ns4:invoke)  external;
declare variable $cISGetCustomerResponse1 as element(ns3:CISGetCustomerResponse) external;

xf:ResponseTransform2($cISGetCustomerResponse1, $invoke1)</con:xquery>
</con:xqueryEntry>