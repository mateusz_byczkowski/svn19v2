<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace m="urn:be.services.dcl";
declare namespace m1="urn:cif.entities.be.dcl";
declare namespace xf="urn:be.services.dcl/mappings";

declare variable $body external;

declare function m:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

declare function xf:mapDate($dateIn as xs:string) as xs:string {
     fn:concat (fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2))
};

&lt;soap-env:Body> 
  {
  let $fml as element(FML32):=$body/FML32
  return
  
  &lt;m:invokeResponse xmlns:urn="urn:be.services.dcl"> 
    &lt;m:shareholderList xmlns:urn1="urn:cif.entities.be.dcl"> 
      {if($fml/DC_NUMER_KLIENTA)
        then 
          for $x at $i in $fml/DC_NUMER_KLIENTA
            return
              &lt;m1:Customer>
                {if($fml/CI_NAZWA_PELNA[$i] and m:isData($fml/CI_NAZWA_PELNA[$i]))
                    then &lt;m1:companyName>{data($fml/CI_NAZWA_PELNA[$i])}&lt;/m1:companyName>
                    else ()
                }
                {if($fml/DC_NUMER_KLIENTA[$i] and m:isData($fml/DC_NUMER_KLIENTA[$i]))
                    then &lt;m1:customerNumber>{data($fml/DC_NUMER_KLIENTA[$i])}&lt;/m1:customerNumber>
                    else ()
                }
                {if($fml/DC_DATA_WPROWADZENIA[$i] and m:isData($fml/DC_DATA_WPROWADZENIA[$i]))
                    then &lt;m1:dateCustomerOpened>{xf:mapDate($fml/DC_DATA_WPROWADZENIA[$i])}&lt;/m1:dateCustomerOpened>
                    else ()
                }
                {if($fml/CI_DATA_AKTUALIZACJI[$i] and m:isData($fml/CI_DATA_AKTUALIZACJI[$i]))
                    then &lt;m1:fileMaintenanceLastDate>{xf:mapDate($fml/CI_DATA_AKTUALIZACJI[$i])}&lt;/m1:fileMaintenanceLastDate>
                    else ()
                }
                {if($fml/DC_REZ_NIEREZ[$i] and m:isData($fml/DC_REZ_NIEREZ[$i]))
                    then &lt;m1:resident>{fn:boolean(xs:integer($fml/DC_REZ_NIEREZ[$i]))}&lt;/m1:resident>
                    else ()
                }
                {if($fml/DC_NIP[$i] and m:isData($fml/DC_NIP[$i]))
                    then &lt;m1:taxID>{data($fml/DC_NIP[$i])}&lt;/m1:taxID>
                    else ()
                }
                {if($fml/DC_NAZWA_SKROCONA[$i] and m:isData($fml/DC_NAZWA_SKROCONA[$i]))
                    then &lt;m1:shortName>{data($fml/DC_NAZWA_SKROCONA[$i])}&lt;/m1:shortName>
                    else (if($fml/CI_NAZWA_PELNA[$i] and m:isData($fml/CI_NAZWA_PELNA[$i]))
                          then &lt;m1:shortName>{data($fml/CI_NAZWA_PELNA[$i])}&lt;/m1:shortName>
                          else ()
                         )
                }
                {if($fml/CI_DECYZJA[$i] and m:isData($fml/CI_DECYZJA[$i]))
                    then &lt;m1:crmDecision>{data($fml/CI_DECYZJA[$i])}&lt;/m1:crmDecision>
                    else ()
                }
                {if($fml/CI_DATA_NIP[$i] and m:isData($fml/CI_DATA_NIP[$i]))
                    then &lt;m1:taxIdDate>{xf:mapDate($fml/CI_DATA_NIP[$i])}&lt;/m1:taxIdDate>
                    else ()
                }
                &lt;m1:certificate>false&lt;/m1:certificate>
              &lt;/m1:Customer> 
        else ()
      }
    &lt;/m:shareholderList>
  &lt;/m:invokeResponse> 
  }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>