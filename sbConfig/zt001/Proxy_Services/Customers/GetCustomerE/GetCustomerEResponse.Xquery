<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCustomerEResponse($fml as element(fml:FML32))
	as element(m:GetCustomerEResponse) {
		&lt;m:GetCustomerEResponse>
			{

				let $B_ID_ODDZ := $fml/fml:B_ID_ODDZ
				let $B_SYS := $fml/fml:B_SYS
				let $B_MIKROODDZIAL := $fml/fml:B_MIKROODDZIAL
				let $B_NAZWA_KORESP := $fml/fml:B_NAZWA_KORESP
				let $B_NAZWA_KLIENTA := $fml/fml:B_NAZWA_KLIENTA
				let $B_UL_KORESP := $fml/fml:B_UL_KORESP
				let $B_M_KORESP := $fml/fml:B_M_KORESP
				let $B_KOD_KORESP := $fml/fml:B_KOD_KORESP
				let $B_UL_ZAM := $fml/fml:B_UL_ZAM
				let $B_M_ZAM := $fml/fml:B_M_ZAM
				let $B_KOD_POCZT := $fml/fml:B_KOD_POCZT
				let $B_TELEFON := $fml/fml:B_TELEFON
				let $B_D_URODZENIA := $fml/fml:B_D_URODZENIA
				let $B_M_URODZENIA := $fml/fml:B_M_URODZENIA
				let $E_CIF_OPTIONS := $fml/fml:E_CIF_OPTIONS
				let $E_CUSTOMER_TYPE := $fml/fml:E_CUSTOMER_TYPE
				let $B_RODZ_DOK := $fml/fml:B_RODZ_DOK
				let $B_NR_DOK := $fml/fml:B_NR_DOK
				let $B_STATUS := $fml/fml:B_STATUS
				let $B_KOD_KLIENTA := $fml/fml:B_KOD_KLIENTA
				for $it at $p in $fml/fml:B_KOD_KLIENTA
				return
					&lt;m:GetCustomerE>
					{
						if($B_ID_ODDZ[$p])
							then &lt;m:IdOddz>{ data($B_ID_ODDZ[$p]) }&lt;/m:IdOddz>
						else ()
					}
					{
						if($B_SYS[$p])
							then &lt;m:Sys>{ data($B_SYS[$p]) }&lt;/m:Sys>
						else ()
					}
					{
						if($B_MIKROODDZIAL[$p])
							then &lt;m:Mikrooddzial>{ data($B_MIKROODDZIAL[$p]) }&lt;/m:Mikrooddzial>
						else ()
					}
					{
						if($B_NAZWA_KORESP[$p])
							then &lt;m:NazwaKoresp>{ data($B_NAZWA_KORESP[$p]) }&lt;/m:NazwaKoresp>
						else ()
					}
					{
						if($B_NAZWA_KLIENTA[$p])
							then &lt;m:NazwaKlienta>{ data($B_NAZWA_KLIENTA[$p]) }&lt;/m:NazwaKlienta>
						else ()
					}
					{
						if($B_UL_KORESP[$p])
							then &lt;m:UlKoresp>{ data($B_UL_KORESP[$p]) }&lt;/m:UlKoresp>
						else ()
					}
					{
						if($B_M_KORESP[$p])
							then &lt;m:MKoresp>{ data($B_M_KORESP[$p]) }&lt;/m:MKoresp>
						else ()
					}
					{
						if($B_KOD_KORESP[$p])
							then &lt;m:KodKoresp>{ data($B_KOD_KORESP[$p]) }&lt;/m:KodKoresp>
						else ()
					}
					{
						if($B_UL_ZAM[$p])
							then &lt;m:UlZam>{ data($B_UL_ZAM[$p]) }&lt;/m:UlZam>
						else ()
					}
					{
						if($B_M_ZAM[$p])
							then &lt;m:MZam>{ data($B_M_ZAM[$p]) }&lt;/m:MZam>
						else ()
					}
					{
						if($B_KOD_POCZT[$p])
							then &lt;m:KodPoczt>{ data($B_KOD_POCZT[$p]) }&lt;/m:KodPoczt>
						else ()
					}
					{
						if($B_TELEFON[$p])
							then &lt;m:Telefon>{ data($B_TELEFON[$p]) }&lt;/m:Telefon>
						else ()
					}
					{
						if($B_D_URODZENIA[$p])
							then &lt;m:DUrodzenia>{ data($B_D_URODZENIA[$p]) }&lt;/m:DUrodzenia>
						else ()
					}
					{
						if($B_M_URODZENIA[$p])
							then &lt;m:MUrodzenia>{ data($B_M_URODZENIA[$p]) }&lt;/m:MUrodzenia>
						else ()
					}
					{
						if($E_CIF_OPTIONS[$p])
							then &lt;m:CifOptions>{ data($E_CIF_OPTIONS[$p]) }&lt;/m:CifOptions>
						else ()
					}
					{
						if($E_CUSTOMER_TYPE[$p])
							then &lt;m:CustomerType>{ data($E_CUSTOMER_TYPE[$p]) }&lt;/m:CustomerType>
						else ()
					}
					{
						if($B_RODZ_DOK[$p])
							then &lt;m:RodzDok>{ data($B_RODZ_DOK[$p]) }&lt;/m:RodzDok>
						else ()
					}
					{
						if($B_NR_DOK[$p])
							then &lt;m:NrDok>{ data($B_NR_DOK[$p]) }&lt;/m:NrDok>
						else ()
					}
					{
						if($B_STATUS[$p])
							then &lt;m:Status>{ data($B_STATUS[$p]) }&lt;/m:Status>
						else ()
					}
					{
						if($B_KOD_KLIENTA[$p])
							then &lt;m:KodKlienta>{ data($B_KOD_KLIENTA[$p]) }&lt;/m:KodKlienta>
						else ()
					}
					&lt;/m:GetCustomerE>
			}

		&lt;/m:GetCustomerEResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetCustomerEResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>