<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:hlbsentities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

&lt;soap-env:Body>
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:document/m1:CheckDocumentInMIGInput

    let $msgId := $reqh/m:msgHeader/m:msgId
    let $userId  := $reqh/m:msgHeader/m:userId
    let $unitId  := $reqh/m:msgHeader/m:unitId
    return

    &lt;fml:FML32>
      &lt;fml:DC_MSHEAD_MSGID?>{ data($msgId) }&lt;/fml:DC_MSHEAD_MSGID>
      &lt;fml:DC_UZYTKOWNIK?>{concat("SKP:", data($userId)) }&lt;/fml:DC_UZYTKOWNIK>
      &lt;fml:DC_KOD_JEDNOSTKI?>{ data($unitId) }&lt;/fml:DC_KOD_JEDNOSTKI>
      &lt;fml:DC_KOD_APLIKACJI>90&lt;/fml:DC_KOD_APLIKACJI>

      {if($req/m1:documentID)
         then &lt;fml:CI_DOK_TOZSAMOSCI>{ data($req/m1:documentID) }&lt;/fml:CI_DOK_TOZSAMOSCI>
         else ()
      }
      {if($req/m1:documentNumber)
           then &lt;fml:CI_SERIA_NR_DOK>{ data($req/m1:documentNumber) }&lt;/fml:CI_SERIA_NR_DOK>
           else ()
      }
    &lt;/fml:FML32>
  }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>