<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:cif.entities.be.dcl";
declare namespace e2 = "urn:filtersandmessages.entities.be.dcl";
declare namespace e3 = "urn:dictionaries.be.dcl";

declare function local:mapGetCustomerEmploymentAddressResponse($fml as element())
    as element(m:invokeResponse) {
        <m:invokeResponse>
			<m:response>
				<e2:ResponseMessage>
					<e2:result>true</e2:result>
				</e2:ResponseMessage>
			</m:response>	
			<m:address>
				<e:Address>
		          <e:houseFlatNumber>{ data($fml/fml:CI_NUMER_POSESJI_PRAC) }</e:houseFlatNumber>	
		          <e:city>{ data($fml/fml:CI_MIASTO_PRACODAWCY) }</e:city>	
		          <e:street>{ data($fml/fml:CI_ULICA_PRACODAWCY) }</e:street>	
		          <e:zipCode>{ data($fml/fml:CI_KOD_POCZT_PRAC) }</e:zipCode>	
		          <e:countryId>
		          	<e3:CountryCode>
		          		<e3:countryCode>{ data($fml/fml:CI_KOD_KRAJU_PRAC) }</e3:countryCode>
		          	</e3:CountryCode>
		          </e:countryId>
				</e:Address>
			</m:address>
        </m:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ local:mapGetCustomerEmploymentAddressResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>