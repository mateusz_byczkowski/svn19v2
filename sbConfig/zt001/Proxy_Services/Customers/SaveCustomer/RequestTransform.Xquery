<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/SaveCustomer/RequestTransform2/";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace ns-1 = "urn:baseentities.be.dcl";
declare namespace ns1 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "urn:basedictionaries.be.dcl";
declare namespace ns5 = "urn:baseauxentities.be.dcl";
declare namespace ns6 = "urn:entities.be.dcl";
declare namespace ns7 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace ns8 = "urn:cifdict.dictionaries.be.dcl";

declare function local:booleanToInt($tn as xs:boolean?) as xs:integer {
  if($tn) then 1
  else 0
};

declare function local:booleanToString($tn as xs:boolean?) as xs:string {
  if($tn) then '1'
  else '0'
};

declare function local:YMDToDMY($dateYMD as xs:string?) as xs:string {
   if($dateYMD) then
	   if ($dateYMD eq '') then ''
	   else fn:string-join((fn:substring($dateYMD, 9, 2), fn:substring($dateYMD, 6, 2),fn:substring($dateYMD, 1, 4)), '-')
   else ''
};

declare function xf:RequestTransform2($invoke1 as element(ns4:invoke), $header1 as element(ns4:header))
    as element(ns1:pcCustNewRequest) {
        &lt;ns1:pcCustNewRequest>
            &lt;ns1:PracownikWprow?>{ data($header1/ns4:msgHeader/ns4:userId) }&lt;/ns1:PracownikWprow>
            &lt;ns1:NumerKlienta?>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerNumber) }&lt;/ns1:NumerKlienta>
            &lt;ns1:IdSpolki>{ data($invoke1/ns4:customer/ns0:Customer/ns0:companyID/ns3:CompanyType/ns3:companyType) }&lt;/ns1:IdSpolki>
            {
                for $customerType in $invoke1/ns4:customer/ns0:Customer/ns0:customerType/ns3:CustomerType/ns3:customerType
                return
                    &lt;ns1:TypKlienta>{ data($customerType) }&lt;/ns1:TypKlienta>
            }
            {
                for $agreementMarketing in $invoke1/ns4:customer/ns0:Customer/ns0:processingApproval/ns3:CustomerProcessingApproval/ns3:customerProcessingApproval
                return
                    &lt;ns1:UdostepGrupa?>{ data($agreementMarketing) }&lt;/ns1:UdostepGrupa>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:UlicaDanePodst>{ data($adresy[1]/ns0:street) }&lt;/ns1:UlicaDanePodst>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:NrPosesLokaluDanePodst>{ data($adresy[1]/ns0:houseFlatNumber) }&lt;/ns1:NrPosesLokaluDanePodst>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:KodPocztowyDanePodst>{ data($adresy[1]/ns0:zipCode) }&lt;/ns1:KodPocztowyDanePodst>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:MiastoDanePodst>{ data($adresy[1]/ns0:city) }&lt;/ns1:MiastoDanePodst>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:WojKrajDanePodst>{ data($adresy[1]/ns0:state/ns3:State/ns3:state) }&lt;/ns1:WojKrajDanePodst>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:KodKraju>{ data($adresy[1]/ns0:countryId/ns3:CountryCode/ns3:isoCountryCode) }&lt;/ns1:KodKraju>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:TypAdresu>{ data($adresy[1]/ns0:typeOfLocation/ns3:AddressTypeOfLocation/ns3:addressTypeOfLocation) }&lt;/ns1:TypAdresu>
            }
            &lt;ns1:RezNierez>{ local:booleanToInt($invoke1/ns4:customer/ns0:Customer/ns0:resident) }&lt;/ns1:RezNierez>
            &lt;ns1:NrDowoduRegon>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:identityCardNumber) }&lt;/ns1:NrDowoduRegon>
            &lt;ns1:NrPesel>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:pesel) }&lt;/ns1:NrPesel>
            &lt;ns1:Nip>{ data($invoke1/ns4:customer/ns0:Customer/ns0:taxID) }&lt;/ns1:Nip>
            {
                for $dateAgreementMarketing in $invoke1/ns4:dateOfAgreement/ns5:DateHolder/ns5:value
                return
                    &lt;ns1:DataUdostepGrupa?>{ local:YMDToDMY( data($dateAgreementMarketing) ) }&lt;/ns1:DataUdostepGrupa>
            }
            {
                for $agreementEmailMarketing in $invoke1/ns4:customer/ns0:Customer/ns0:agreementCode/ns8:CustomerAgreementCode/ns8:customerAgreementCode
                return
                    &lt;ns1:ZgodaInfHandlowa?>{ data($agreementEmailMarketing) }&lt;/ns1:ZgodaInfHandlowa>
            }
            {
                for $dateAgreementEmailMarketing in $invoke1/ns4:dateOfAgreement/ns5:DateHolder/ns5:value
                return
                    &lt;ns1:DataZgodyInfHandlowej?>{ local:YMDToDMY($dateAgreementEmailMarketing) }&lt;/ns1:DataZgodyInfHandlowej>
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:AdresOd>{ local:YMDToDMY(data($adresy[1]/ns0:validFrom)) }&lt;/ns1:AdresOd>
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:AdresDo>{ local:YMDToDMY( data($adresy[1]/ns0:validTo) ) }&lt;/ns1:AdresDo>
            }
            &lt;ns1:KrajPochodzenia>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:countryOfOrigin/ns3:CountryCode/ns3:isoCountryCode) }&lt;/ns1:KrajPochodzenia>
            &lt;ns1:AdresEMail>{ data($invoke1/ns4:customer/ns0:Customer/ns0:email) }&lt;/ns1:AdresEMail>
            &lt;ns1:NrTelefonu>{ data($invoke1/ns4:customer/ns0:Customer/ns0:phoneNo) }&lt;/ns1:NrTelefonu>
            &lt;ns1:NrTelefKomorkowego>{ data($invoke1/ns4:customer/ns0:Customer/ns0:mobileNo) }&lt;/ns1:NrTelefKomorkowego>
            &lt;ns1:NumerTelefonuPraca>{ data($invoke1/ns4:customer/ns0:Customer/ns0:businessNo) }&lt;/ns1:NumerTelefonuPraca>
            &lt;ns1:KartaWzorowPodpisow>{ data($invoke1/ns4:customer/ns0:Customer/ns0:signatureCardBranchId/ns3:BranchCode/ns3:branchCode) }&lt;/ns1:KartaWzorowPodpisow>
            &lt;ns1:NrOddzialu>{ xs:long( data($invoke1/ns4:customer/ns0:Customer/ns0:branchOfOwnership/ns3:BranchCode/ns3:branchCode) ) }&lt;/ns1:NrOddzialu>
            {
                for $customerDocumentType in $invoke1/ns4:customer/ns0:Customer/ns0:documentList/ns0:Document[1]/ns0:documentType/ns3:CustomerDocumentType/ns3:customerDocumentType
                return
                    &lt;ns1:DokTozsamosci>{ data($customerDocumentType) }&lt;/ns1:DokTozsamosci>
            }
            {
                for $documentNumber in $invoke1/ns4:customer/ns0:Customer/ns0:documentList/ns0:Document[1]/ns0:documentNumber
                return
                    &lt;ns1:SeriaNrDok>{ data($documentNumber) }&lt;/ns1:SeriaNrDok>
            }
            &lt;ns1:NumerPaszportu>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:passportNumber) }&lt;/ns1:NumerPaszportu>
            &lt;ns1:Plec>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:sexCode/ns3:CustomerSexCode/ns3:customerSexCode) }&lt;/ns1:Plec>
            &lt;ns1:Imie>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:firstName) }&lt;/ns1:Imie>
            &lt;ns1:DrugieImie>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:secondName) }&lt;/ns1:DrugieImie>
            &lt;ns1:Nazwisko>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:lastName) }&lt;/ns1:Nazwisko>
            &lt;ns1:NazwiskoPanienskieMatki>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:motherMaidenName) }&lt;/ns1:NazwiskoPanienskieMatki>
            &lt;ns1:Obywatelstwo>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:citizenship/ns3:CitizenshipCode/ns3:isoCitizenshipCode) }&lt;/ns1:Obywatelstwo>
            &lt;ns1:DataUrodzenia>{ local:YMDToDMY( data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:dateOfBirth) ) }&lt;/ns1:DataUrodzenia>
            &lt;ns1:MiejsceUrodzenia>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:birthPlace) }&lt;/ns1:MiejsceUrodzenia>
            &lt;ns1:KodJezyka>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerAdditionalInfo/ns0:CustomerAdditionalInfo/ns0:language/ns2:Language/ns2:languageID) }&lt;/ns1:KodJezyka>
            &lt;ns1:ZrodloDanych>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerAdditionalInfo/ns0:CustomerAdditionalInfo/ns0:dataSource/ns3:DataSource/ns3:dataSource) }&lt;/ns1:ZrodloDanych>
            {
                for $policyNo in $invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyNo
                return
                    &lt;ns1:LiczbaPolis>{ xs:short( data($policyNo) ) }&lt;/ns1:LiczbaPolis>
            }
            {
                for $policyTotalAmount in $invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyTotalAmount
                return
                    &lt;ns1:SumaPolisNaZycie>{ xs:string( data($policyTotalAmount) ) }&lt;/ns1:SumaPolisNaZycie>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:NrPosesLokaluAdresAlt>{ data($adresy[1]/ns0:houseFlatNumber) }&lt;/ns1:NrPosesLokaluAdresAlt>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:KodPocztowyAdresAlt>{ data($adresy[1]/ns0:zipCode) }&lt;/ns1:KodPocztowyAdresAlt>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:WojewodztwoKrajAdresAlt>{ data($adresy[1]/ns0:state/ns3:State/ns3:state) }&lt;/ns1:WojewodztwoKrajAdresAlt>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:KodKrajuKoresp>{ data($adresy[1]/ns0:countryId/ns3:CountryCode/ns3:isoCountryCode) }&lt;/ns1:KodKrajuKoresp>
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:DataOd>{ local:YMDToDMY( data($adresy[1]/ns0:validFrom) ) }&lt;/ns1:DataOd>
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:DataDo>{ local:YMDToDMY( data($adresy[1]/ns0:validTo) ) }&lt;/ns1:DataDo>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:TypAdresuKoresp>{ data($adresy[1]/ns0:typeOfLocation/ns3:AddressTypeOfLocation/ns3:addressTypeOfLocation) }&lt;/ns1:TypAdresuKoresp>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:MiastoAdresAlt>{ data($adresy[1]/ns0:city) }&lt;/ns1:MiastoAdresAlt>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:UlicaAdresAlt>{ data($adresy[1]/ns0:street) }&lt;/ns1:UlicaAdresAlt>
            }
            &lt;ns1:KrajZamieszkania>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:countryOfResidence/ns3:CountryCode/ns3:isoCountryCode) }&lt;/ns1:KrajZamieszkania>
            &lt;ns1:ZrodloDochodu>{ xs:string( data($invoke1/ns4:customer/ns0:Customer/ns0:customerFinancialInfo/ns0:CustomerFinancialInfo/ns0:sourceOfIncomeCode/ns3:SourceOfIncomeCode/ns3:sourceOfIncomeCode) ) }&lt;/ns1:ZrodloDochodu>
            {
                let $policyLife := $invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyLife
                return
                    &lt;ns1:UbezpieczenieNaZycie>{ local:booleanToString($policyLife) }&lt;/ns1:UbezpieczenieNaZycie>
            }
            {
                let $policyOther := $invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyOther
                return
                    &lt;ns1:UbezpieczeniaInne>{ local:booleanToString($policyOther) }&lt;/ns1:UbezpieczeniaInne>
            }
            &lt;ns1:UbezpNaZycie?>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyLifeName/ns7:InsurerName/ns7:insurerName) }&lt;/ns1:UbezpNaZycie>
            &lt;ns1:UbezpInne?>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyOtherName/ns7:InsurerName/ns7:insurerName) }&lt;/ns1:UbezpInne>
            {
                for $customerOccupationCode in $invoke1/ns4:customer/ns0:Customer/ns0:customerEmploymentInfoList/ns0:CustomerEmploymentInfo[1]/ns0:occupationCode/ns3:CustomerOccupationCode/ns3:customerOccupationCode
                where $customerOccupationCode != ''
                return
                    &lt;ns1:KodZawoduWyuczonego>{ xs:short(data($customerOccupationCode)) }&lt;/ns1:KodZawoduWyuczonego>
            }
            {
                for $agreementBankConfInfo in $invoke1/ns4:customer/ns0:Customer/ns0:agreementBankInfo
                return
                    &lt;ns1:ZgodaTajemBank?>{ local:booleanToString($agreementBankConfInfo) }&lt;/ns1:ZgodaTajemBank>
            }
            {
                for $dateAgreementBankConfInfo in $invoke1/ns4:dateOfAgreement/ns5:DateHolder/ns5:value
                return
                    &lt;ns1:DataZgodyTajemBank?>{ local:YMDToDMY($dateAgreementBankConfInfo) }&lt;/ns1:DataZgodyTajemBank>
            }
            &lt;ns1:UbezpFundInwest?>{ local:booleanToString($invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyInvest) }&lt;/ns1:UbezpFundInwest>
            &lt;ns1:NazwaUbezpFundInwest?>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyInvestName) }&lt;/ns1:NazwaUbezpFundInwest>
            &lt;ns1:SumaSkladekPolis?>{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyPremiumSum) }&lt;/ns1:SumaSkladekPolis>

        &lt;/ns1:pcCustNewRequest>
};

declare variable $invoke1 as element(ns4:invoke) external;
declare variable $header1 as element(ns4:header) external;

xf:RequestTransform2($invoke1, $header1)</con:xquery>
</con:xqueryEntry>