<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForAddresses($parm as element(fml:FML32)) as element()
{

<ns0:addresses>
  {
   (: adres podstawowy :)
   if ($parm/DC_ULICA_DANE_PODST) then
    <ns2:Address>
      <ns2:houseFlatNumber?>{data($parm/DC_NR_POSES_LOKALU_DANE_PODST)}</ns2:houseFlatNumber>
      <ns2:city?>{data($parm/DC_MIASTO_DANE_PODST)}</ns2:city>
      (:<ns2:validFrom?>{data($parm/NF_ADDRES_VALIDFROM[$occ])}</ns2:validFrom>:)
      <ns2:county?>{data($parm/DC_WOJEWODZTWO)}</ns2:county>
      (:<ns2:validTo?>{data($parm/NF_ADDRES_VALIDTO[$occ])}</ns2:validTo>:)
      <ns2:street?>{data($parm/DC_ULICA_DANE_PODST)}</ns2:street>
      <ns2:zipCode?>{data($parm/DC_KOD_POCZTOWY_DANE_PODST)}</ns2:zipCode>
      <ns2:post?>{data($parm/DC_MIASTO_DANE_PODST)}</ns2:post>
      <ns2:name?>{data($parm/CI_NAZWA_PELNA)}</ns2:name>
      (:<ns2:local?>{data($parm/NF_ADDRES_LOCAL[$occ])}</ns2:local>:)
      <ns2:addressType>
        <ns3:AddressType>
          <ns3:addressType?>1</ns3:addressType>
        </ns3:AddressType>
      </ns2:addressType>
      <ns2:state>
        <ns3:State>
               {
            if (data($parm/DC_KOD_KRAJU) = "616") then
               <ns3:state?>{data($parm/DC_WOJEWODZTWO)}</ns3:state>  
              else ()
              }
(:          <ns3:state?>{data($parm/NF_STATE_STATE[$occ])}</ns3:state>:)
        </ns3:State>
      </ns2:state>
      <ns2:countryId>
        <ns3:CountryCode>
          <ns3:isoCountryCode?>{data($parm/DC_KOD_KRAJU)}</ns3:isoCountryCode>
        </ns3:CountryCode>
      </ns2:countryId>
    </ns2:Address>
    else()
  }
  {
   (: adres korespondencyjny:)
  for $x at $occ in $parm/CI_TYP_ADRESU where data($x) = "2"
    return
      <ns2:Address>
         <ns2:houseFlatNumber?>{data($parm/DC_NR_POSES_LOKALU_ADRES_ALT[$occ])}</ns2:houseFlatNumber>
         <ns2:city?>{data($parm/DC_MIASTO_ADRES_ALT[$occ])}</ns2:city>
      (:<ns2:validFrom?>{data($parm/NF_ADDRES_VALIDFROM[$occ])}</ns2:validFrom>:)
         <ns2:county?>{data($parm/DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$occ])}</ns2:county>
  (:    <ns2:validTo?>{data($parm/NF_ADDRES_VALIDTO[$occ])}</ns2:validTo>:)
         <ns2:street?>{data($parm/DC_ULICA_ADRES_ALT[$occ])}</ns2:street>
         <ns2:zipCode?>{data($parm/DC_KOD_POCZTOWY_ADRES_ALT[$occ])}</ns2:zipCode>
 (:     <ns2:countryId?>{data($parm/XXXXXXXXXXX[$occ])}</ns2:countryId>:)
         <ns2:post?>{data($parm/DC_MIASTO_ADRES_ALT[$occ])}</ns2:post>
         <ns2:name?>{data($parm/DC_IMIE_I_NAZWISKO_ALT[$occ])}</ns2:name>
   (:   <ns2:local?>{data($parm/NF_ADDRES_LOCAL[$occ])}</ns2:local>:)
      <ns2:addressType>
        <ns3:AddressType>
           <ns3:addressType?>2</ns3:addressType> 
        </ns3:AddressType>
      </ns2:addressType>
      <ns2:state>
        <ns3:State>
               {
            if (data($parm/CI_KOD_KRAJU_KORESP[$occ]) = "616") then
               <ns3:state?>{data($parm/DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$occ])}</ns3:state>  
            else
              if (data($parm/DC_TYP_ADRESU[$occ]) != "1") then
                 <ns3:state?>{data($parm/DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$occ])}</ns3:state>  
              else ()
              }
(:          <ns3:state?>{data($parm/NF_STATE_STATE[$occ])}</ns3:state>:)
        </ns3:State>
      </ns2:state>
        <ns2:countryId>
        <ns3:CountryCode>
            {
            if (data($parm/CI_KOD_KRAJU_KORESP[$occ]) != "0") then
               <ns3:isoCountryCode?>{data($parm/CI_KOD_KRAJU_KORESP[$occ])}</ns3:isoCountryCode>
            else
              if (data($parm/DC_TYP_ADRESU[$occ]) != "1") then
                 <ns3:isoCountryCode>616</ns3:isoCountryCode>
              else
                 <ns3:isoCountryCode?>{data($parm/CI_KOD_KRAJU_KORESP[$occ])}</ns3:isoCountryCode>
              }
        </ns3:CountryCode>
      </ns2:countryId> 
    </ns2:Address>
}
</ns0:addresses> 

};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForAddresses($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>