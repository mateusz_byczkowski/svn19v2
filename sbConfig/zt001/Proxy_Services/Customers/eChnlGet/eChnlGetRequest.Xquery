<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeChnlGetRequest($req as element(m:eChnlGetRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:LoginId)
					then &lt;fml:E_LOGIN_ID>{ data($req/m:LoginId) }&lt;/fml:E_LOGIN_ID>
					else ()
			}
			{
				if($req/m:ChannelType)
					then &lt;fml:E_CHANNEL_TYPE>{ data($req/m:ChannelType) }&lt;/fml:E_CHANNEL_TYPE>
					else ()
			}
			{
				if($req/m:UserName)
					then &lt;fml:U_USER_NAME>{ data($req/m:UserName) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/m:AdminMicroBranch)
					then &lt;fml:E_ADMIN_MICRO_BRANCH>{ data($req/m:AdminMicroBranch) }&lt;/fml:E_ADMIN_MICRO_BRANCH>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapeChnlGetRequest($body/m:eChnlGetRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>