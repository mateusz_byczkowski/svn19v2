<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeChnlGetResponse($fml as element(fml:FML32))
	as element(m:eChnlGetResponse) {
		&lt;m:eChnlGetResponse>
			{

				let $E_LOGIN_ID := $fml/fml:E_LOGIN_ID
				let $E_CHANNEL_LOGIN_ID := $fml/fml:E_CHANNEL_LOGIN_ID
				let $E_CHANNEL_TYPE := $fml/fml:E_CHANNEL_TYPE
				let $E_TRIES := $fml/fml:E_TRIES
				let $E_TRIES_ALLOWED := $fml/fml:E_TRIES_ALLOWED
				let $E_STATUS := $fml/fml:E_STATUS
				let $E_LOGIN_COUNT := $fml/fml:E_LOGIN_COUNT
				let $E_BAD_LOGIN_COUNT := $fml/fml:E_BAD_LOGIN_COUNT
				let $E_LAST_LOGIN := $fml/fml:E_LAST_LOGIN
				let $E_LAST_BAD_LOGIN := $fml/fml:E_LAST_BAD_LOGIN
				let $E_EXPIRY := $fml/fml:E_EXPIRY
				let $E_LAST_CHANGED := $fml/fml:E_LAST_CHANGED
				let $E_CHANGE_PERIOD := $fml/fml:E_CHANGE_PERIOD
				let $E_TRN_MASK := $fml/fml:E_TRN_MASK
				let $E_CHANNEL_LIMIT := $fml/fml:E_CHANNEL_LIMIT
				let $E_CHANNEL_CYCLE := $fml/fml:E_CHANNEL_CYCLE
				let $E_CHANNEL_CYCLE_BEGIN := $fml/fml:E_CHANNEL_CYCLE_BEGIN
				let $E_CHANNEL_AMOUNT_REMAINING := $fml/fml:E_CHANNEL_AMOUNT_REMAINING
				let $E_TIME_STAMP := $fml/fml:E_TIME_STAMP
				let $E_CHANNEL_OPTIONS := $fml/fml:E_CHANNEL_OPTIONS
				let $E_ALLOWED_LOGON_SECURITY := $fml/fml:E_ALLOWED_LOGON_SECURITY
				for $it at $p in $fml/fml:E_LOGIN_ID
				return
					&lt;m:eChnlGet>
					{
						if($E_LOGIN_ID[$p])
							then &lt;m:LoginId>{ data($E_LOGIN_ID[$p]) }&lt;/m:LoginId>
						else ()
					}
					{
						if($E_CHANNEL_LOGIN_ID[$p])
							then &lt;m:ChannelLoginId>{ data($E_CHANNEL_LOGIN_ID[$p]) }&lt;/m:ChannelLoginId>
						else ()
					}
					{
						if($E_CHANNEL_TYPE[$p])
							then &lt;m:ChannelType>{ data($E_CHANNEL_TYPE[$p]) }&lt;/m:ChannelType>
						else ()
					}
					{
						if($E_TRIES[$p])
							then &lt;m:Tries>{ data($E_TRIES[$p]) }&lt;/m:Tries>
						else ()
					}
					{
						if($E_TRIES_ALLOWED[$p])
							then &lt;m:TriesAllowed>{ data($E_TRIES_ALLOWED[$p]) }&lt;/m:TriesAllowed>
						else ()
					}
					{
						if($E_STATUS[$p])
							then &lt;m:Status>{ data($E_STATUS[$p]) }&lt;/m:Status>
						else ()
					}
					{
						if($E_LOGIN_COUNT[$p])
							then &lt;m:LoginCount>{ data($E_LOGIN_COUNT[$p]) }&lt;/m:LoginCount>
						else ()
					}
					{
						if($E_BAD_LOGIN_COUNT[$p])
							then &lt;m:BadLoginCount>{ data($E_BAD_LOGIN_COUNT[$p]) }&lt;/m:BadLoginCount>
						else ()
					}
					{
						if($E_LAST_LOGIN[$p])
							then &lt;m:LastLogin>{ data($E_LAST_LOGIN[$p]) }&lt;/m:LastLogin>
						else ()
					}
					{
						if($E_LAST_BAD_LOGIN[$p])
							then &lt;m:LastBadLogin>{ data($E_LAST_BAD_LOGIN[$p]) }&lt;/m:LastBadLogin>
						else ()
					}
					{
						if($E_EXPIRY[$p])
							then &lt;m:Expiry>{ data($E_EXPIRY[$p]) }&lt;/m:Expiry>
						else ()
					}
					{
						if($E_LAST_CHANGED[$p])
							then &lt;m:LastChanged>{ data($E_LAST_CHANGED[$p]) }&lt;/m:LastChanged>
						else ()
					}
					{
						if($E_CHANGE_PERIOD[$p])
							then &lt;m:ChangePeriod>{ data($E_CHANGE_PERIOD[$p]) }&lt;/m:ChangePeriod>
						else ()
					}
					{
						if($E_TRN_MASK[$p])
							then &lt;m:TrnMask>{ data($E_TRN_MASK[$p]) }&lt;/m:TrnMask>
						else ()
					}
					{
						if($E_CHANNEL_LIMIT[$p])
							then &lt;m:ChannelLimit>{ data($E_CHANNEL_LIMIT[$p]) }&lt;/m:ChannelLimit>
						else ()
					}
					{
						if($E_CHANNEL_CYCLE[$p])
							then &lt;m:ChannelCycle>{ data($E_CHANNEL_CYCLE[$p]) }&lt;/m:ChannelCycle>
						else ()
					}
					{
						if($E_CHANNEL_CYCLE_BEGIN[$p])
							then &lt;m:ChannelCycleBegin>{ data($E_CHANNEL_CYCLE_BEGIN[$p]) }&lt;/m:ChannelCycleBegin>
						else ()
					}
					{
						if($E_CHANNEL_AMOUNT_REMAINING[$p])
							then &lt;m:ChannelAmountRemaining>{ data($E_CHANNEL_AMOUNT_REMAINING[$p]) }&lt;/m:ChannelAmountRemaining>
						else ()
					}
					{
						if($E_TIME_STAMP[$p])
							then &lt;m:TimeStamp>{ data($E_TIME_STAMP[$p]) }&lt;/m:TimeStamp>
						else ()
					}
					{
						if($E_CHANNEL_OPTIONS[$p])
							then &lt;m:ChannelOptions>{ data($E_CHANNEL_OPTIONS[$p]) }&lt;/m:ChannelOptions>
						else ()
					}
					{
						if($E_ALLOWED_LOGON_SECURITY[$p])
							then &lt;m:AllowedLogonSecurity>{ data($E_ALLOWED_LOGON_SECURITY[$p]) }&lt;/m:AllowedLogonSecurity>
						else ()
					}
					&lt;/m:eChnlGet>
			}

		&lt;/m:eChnlGetResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapeChnlGetResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>