<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-10</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function insertDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {
      if ($value)
        then if(string-length($value)>5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)>5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };
 
declare function chgDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {
 
    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)
 
    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 
};

declare function xf:mapgetCustProdsForAdvisoryResponse($fml as element(fml:FML32))
  as element(m:getCustProdsForAdvisoryResponse) {
    &lt;m:getCustProdsForAdvisoryResponse>
      {for $it at $p in $fml/fml:NF_PRODUA_CODEPRODUCTAREA
        return
        &lt;m:CUSTOMER_PRODUCTS>
          {if($fml/fml:NF_PRODAT_IDPRODUCTDEFINIT[$p])
            then &lt;m:ProdatIdproductdefinit>{ data($fml/fml:NF_PRODAT_IDPRODUCTDEFINIT[$p]) }&lt;/m:ProdatIdproductdefinit>
            else ()
          }
          {if($fml/fml:PT_ID_GROUP[$p])
            then &lt;m:ProgahIdproductgroup>{ data($fml/fml:PT_ID_GROUP[$p]) }&lt;/m:ProgahIdproductgroup>
            else ()
          }
          {if($fml/fml:NF_PRODUG_RECEIVER[$p])
            then &lt;m:ProdugReceiver>{ data($fml/fml:NF_PRODUG_RECEIVER[$p]) }&lt;/m:ProdugReceiver>
            else ()
          }
          {if($fml/fml:NF_ACCOUR_RELATIONSHIP[$p])
            then &lt;m:AccourRelationship>{ data($fml/fml:NF_ACCOUR_RELATIONSHIP[$p]) }&lt;/m:AccourRelationship>
            else ()
          }
          {if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p])
            then &lt;m:ProduaCodeproductarea>{ data($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p]) }&lt;/m:ProduaCodeproductarea>
            else ()
          }
          {if($fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI[$p])
            then &lt;m:AccounAccountdescripti>{ data($fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI[$p]) }&lt;/m:AccounAccountdescripti>
            else ()
          }
          {if($fml/fml:NF_ACCOUN_ACCOUNTNUMBER[$p])
            then &lt;m:AccounAccountnumber>{ fn:substring(data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER[$p]), fn:string-length(data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER[$p]))-9, 10) }&lt;/m:AccounAccountnumber>
            else ()
          }
          {if($fml/fml:NF_CURREC_CURRENCYCODE[$p])
            then &lt;m:CurrecCurrencycode>{ data($fml/fml:NF_CURREC_CURRENCYCODE[$p]) }&lt;/m:CurrecCurrencycode>
            else ()
          }
          {if($fml/fml:NF_ACCOUN_CURRENTBALANCE[$p])
            then &lt;m:AccounCurrentbalance>{ data($fml/fml:NF_ACCOUN_CURRENTBALANCE[$p]) }&lt;/m:AccounCurrentbalance>
            else ()
          }
          {if($fml/fml:NF_ACCOUN_CURRENTBALANCEPL[$p])
            then &lt;m:AccounCurrentbalancepl>{ data($fml/fml:NF_ACCOUN_CURRENTBALANCEPL[$p]) }&lt;/m:AccounCurrentbalancepl>
            else ()
          }
          {if($fml/fml:NF_ACCOUN_DEBITPERCENTAGE[$p])
            then &lt;m:AccounPercentage>{ data($fml/fml:NF_ACCOUN_DEBITPERCENTAGE[$p]) }&lt;/m:AccounPercentage>
            else ()
          }
          {if($fml/fml:NF_PRODUCT_STAT[$p])
            then &lt;m:ProductStat>{ data($fml/fml:NF_PRODUCT_STAT[$p]) }&lt;/m:ProductStat>
            else ()
          }
          { insertDate(data($fml/fml:NF_ACCOUN_ACCOUNTOPENDATE[$p]),"yyyy-MM-dd","m:AccounAccountopendate")
          }
          {if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] = 10)
            then insertDate(data($fml/fml:NF_TIMEA_NEXTRENEWALMATURI[$p]),"yyyy-MM-dd","m:AccounAccountclosedate")
            else if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] = 3) 
              then insertDate(data($fml/fml:NF_LOANA_NEXTMATURITYDATE[$p]),"yyyy-MM-dd","m:AccounAccountclosedate")
              else ()
          }
         {if($fml/fml:NF_PERIOD_PERIOD[$p])
            then &lt;m:AccounRenewalperiod>{ data($fml/fml:NF_PERIOD_PERIOD[$p]) }&lt;/m:AccounRenewalperiod>
            else ()
          }
          {if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] = 10)
            then if($fml/fml:NF_TIMEA_RENEWALFREQUENCY[$p])
                    then &lt;m:AccounRenewalfrequency>{ data($fml/fml:NF_TIMEA_RENEWALFREQUENCY[$p]) }&lt;/m:AccounRenewalfrequency>
                    else ()
            else if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] = 3)
                    then &lt;m:AccounRenewalfrequency>{ data($fml/fml:NF_LOANA_MATURITYDATE2[$p]) }&lt;/m:AccounRenewalfrequency>
                    else &lt;m:AccounRenewalfrequency>0&lt;/m:AccounRenewalfrequency>
          }
          {if($fml/fml:NF_RENEWT_RENEWALTYPE[$p])
            then &lt;m:TimeaRenewaltype>{ data($fml/fml:NF_RENEWT_RENEWALTYPE[$p]) }&lt;/m:TimeaRenewaltype>
            else ()
          }
          {if($fml/fml:NF_TIMEA_INTERESTDISPOSITI[$p])
            then &lt;m:TimeaInterestdispositi>{ data($fml/fml:NF_TIMEA_INTERESTDISPOSITI[$p]) }&lt;/m:TimeaInterestdispositi>
            else ()
          }
          {if($fml/fml:NF_LOANAS_PAYMNTAMOUNTORPC[$p])
            then &lt;m:LoanasPaymntamountorpc>{ data($fml/fml:NF_LOANAS_PAYMNTAMOUNTORPC[$p]) }&lt;/m:LoanasPaymntamountorpc>
            else ()
          }
          {if($fml/fml:NF_LOANAS_PAYMNTAMOUNTORPCPL[$p])
            then &lt;m:LoanasPaymntamountorpcpl>{ data($fml/fml:NF_LOANAS_PAYMNTAMOUNTORPCPL[$p]) }&lt;/m:LoanasPaymntamountorpcpl>
            else ()
          }
          {if($fml/fml:NF_LOANAS_INTERESTPAYMENT[$p])
            then &lt;m:LoanasInterestpayment>{ data($fml/fml:NF_LOANAS_INTERESTPAYMENT[$p]) }&lt;/m:LoanasInterestpayment>
            else ()
          }
          {if($fml/fml:NF_COOWNERS_COUNT[$p])
            then &lt;m:CoownersCount>{ data($fml/fml:NF_COOWNERS_COUNT[$p]) }&lt;/m:CoownersCount>
            else ()
          }
        &lt;/m:CUSTOMER_PRODUCTS>
      }
      {if($fml/fml:NF_PAGECC_OPERATIONS)
        then &lt;m:PageccOperations>{ data($fml/fml:NF_PAGECC_OPERATIONS) }&lt;/m:PageccOperations>
        else ()
      }
      {if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
        then &lt;m:PagecNavigationkeyvalu>{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/m:PagecNavigationkeyvalu>
        else ()
      }
      {if($fml/fml:NF_PAGEC_HASNEXT)
        then &lt;m:PagecHasnext>{ data($fml/fml:NF_PAGEC_HASNEXT) }&lt;/m:PagecHasnext>
        else ()
      }
      {if($fml/fml:NF_RESPOM_ERRORCODE)
        then &lt;m:ErrorCode>{ data($fml/fml:NF_RESPOM_ERRORCODE) }&lt;/m:ErrorCode>
        else ()
      }
      {if($fml/fml:NF_RESPOM_ERRORDESCRIPTION)
        then &lt;m:ErrorDescription>{ data($fml/fml:NF_RESPOM_ERRORDESCRIPTION) }&lt;/m:ErrorDescription>
        else ()
      }
    &lt;/m:getCustProdsForAdvisoryResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetCustProdsForAdvisoryResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>