<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>PT58 - dodanie Body.Version.$1.2011-05-24</con:description>
  <con:xquery>declare namespace ns4 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/SetCustDataShare/RequestTransform2/";
declare namespace ns0 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns-1 = "urn:cif.entities.be.dcl";
declare namespace ns1 = "urn:basedictionaries.be.dcl";
declare namespace ns3 = "urn:entities.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns5 = "urn:baseauxentities.be.dcl";
declare namespace ns6 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace ns7 = "urn:cifdict.dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function local:booleanToString($tn as xs:boolean?) as xs:string {
  if($tn) then '1'
  else '0'
};

declare function local:YMDToDMY($dateYMD as xs:string?) as xs:string {
   if($dateYMD) then
	   if ($dateYMD eq '') then ''
	   else fn:string-join((fn:substring($dateYMD, 9, 2), fn:substring($dateYMD, 6, 2),fn:substring($dateYMD, 1, 4)), '-')
   else ''
};

declare function xf:RequestTransform2($invoke1 as element(ns4:invoke), $header1 as element(ns4:header))
    as element(ns0:pcCustDataShareRequest) {
        &lt;ns0:pcCustDataShareRequest>
            {
                for $customerNumber in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerNumber
                where fn:string-length($customerNumber) > 0
                return
                    &lt;ns0:NumerKlienta?>{ data($customerNumber) }&lt;/ns0:NumerKlienta>
            }
            {
                for $value in $invoke1/ns4:actualDate/ns5:DateHolder/ns5:value
                where fn:string-length($value) > 0
                return
                    &lt;ns0:DataPrzekazania?>{ local:YMDToDMY( data($value) ) }&lt;/ns0:DataPrzekazania>
            }
            {
                for $companyType in $invoke1/ns4:customer/ns-1:Customer/ns-1:companyID/ns2:CompanyType/ns2:companyType
                where fn:string-length($companyType) > 0
                return
                    &lt;ns0:IdSpolki?>{ xs:short( data($companyType) ) }&lt;/ns0:IdSpolki>
            }
            {
            	for $destCompanyType in $invoke1/ns4:companyIdDest/ns-1:CustomerFolders/ns-1:companyID/ns2:CompanyType/ns2:companyType
                where fn:string-length($destCompanyType) > 0
            	return 
            		&lt;ns0:IdSpolki?>{ data($destCompanyType) }&lt;/ns0:IdSpolki>
            }
            {
                for $value in $invoke1/ns4:BakubaCompanyDesc/ns5:StringHolder/ns5:value
                return
                    &lt;ns0:OpisPodmiotu>{ data($value) }&lt;/ns0:OpisPodmiotu>
            }
            {
                for $value in $invoke1/ns4:BakubaComment/ns5:StringHolder/ns5:value
                return
                    &lt;ns0:Uwagi>{ data($value) }&lt;/ns0:Uwagi>
            }
            {
                for $userID in $header1/ns4:msgHeader/ns4:userId
                where fn:string-length($userID) > 0
                return
                    &lt;ns0:PracownikWprow?>{ data($userID) }&lt;/ns0:PracownikWprow>
            }
            {
                for $agreementMarketing in $invoke1/ns4:customer/ns-1:Customer/ns-1:processingApproval/ns2:CustomerProcessingApproval/ns2:customerProcessingApproval
                where fn:string-length($agreementMarketing) > 0
                return
                    &lt;ns0:UdostepGrupa?>{ data($agreementMarketing) }&lt;/ns0:UdostepGrupa>
            }
            {
                for $agreementEmailMarketing in $invoke1/ns4:customer/ns-1:Customer/ns-1:agreementCode/ns7:CustomerAgreementCode/ns7:customerAgreementCode
                where fn:string-length($agreementEmailMarketing) > 0
                return
                    &lt;ns0:ZgodaInfHandlowa?>{ data($agreementEmailMarketing) }&lt;/ns0:ZgodaInfHandlowa>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
  	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = '1'
                 		and fn:string-length($adres/ns-1:street) > 0
  	                return $adres
                return
                    &lt;ns0:UlicaDanePodst?>{ data($adresy[1]/ns-1:street) }&lt;/ns0:UlicaDanePodst>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
  	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = '1'
  	                	and fn:string-length($adres/ns-1:houseFlatNumber) > 0
  	                return $adres
                return
                    &lt;ns0:NrPosesLokaluDanePodst?>{ data($adresy[1]/ns-1:houseFlatNumber) }&lt;/ns0:NrPosesLokaluDanePodst>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
  	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = '1'
  	                	and fn:string-length($adres/ns-1:zipCode) > 0
  	                return $adres
                return
                    &lt;ns0:KodPocztowyDanePodst?>{ data($adresy[1]/ns-1:zipCode) }&lt;/ns0:KodPocztowyDanePodst>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
  	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = '1'
  	                	and fn:string-length($adres/ns-1:city) > 0
  	                return $adres
                return
                    &lt;ns0:MiastoDanePodst?>{ data($adresy[1]/ns-1:city) }&lt;/ns0:MiastoDanePodst>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
  	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = '1'
  	                   and fn:string-length($adres/ns-1:state) > 0
  	                return $adres
                return
                    &lt;ns0:WojKrajDanePodst?>{ data($adresy[1]/ns-1:state) }&lt;/ns0:WojKrajDanePodst>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
  	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = '1'
  	                	and fn:string-length($adres/ns-1:countryId/ns2:CountryCode/ns2:isoCountryCode) > 0
  	                return $adres
                return
                    &lt;ns0:KodKraju?>{ data($adresy[1]/ns-1:countryId/ns2:CountryCode/ns2:isoCountryCode) }&lt;/ns0:KodKraju>
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
  	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = '1'
  	                	and fn:string-length($adres/ns-1:validFrom) > 0
  	                return $adres
                return
                    &lt;ns0:AdresOd?>{ local:YMDToDMY( data($adresy[1]/ns-1:validFrom) ) }&lt;/ns0:AdresOd>
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
  	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = '1'
  	                   and fn:string-length($adres/ns-1:validTo) > 0
  	                return $adres
                return
                    &lt;ns0:AdresDo?>{ local:YMDToDMY( data($adresy[1]/ns-1:validTo) ) }&lt;/ns0:AdresDo>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
  	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = '1'
  	                  and fn:string-length($adres/ns-1:typeOfLocation/ns2:AddressTypeOfLocation/ns2:addressTypeOfLocation) > 0
  	                return $adres
                return
                    &lt;ns0:TypAdresu?>{ data($adresy[1]/ns-1:typeOfLocation/ns2:AddressTypeOfLocation/ns2:addressTypeOfLocation) }&lt;/ns0:TypAdresu>
            }
            {
                for $resident in $invoke1/ns4:customer/ns-1:Customer/ns-1:resident
                where fn:string-length($resident) > 0
                return
                    &lt;ns0:RezNierez?>{ local:booleanToString( data($resident) ) }&lt;/ns0:RezNierez>
            }
            {
                for $countryCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:countryOfOrigin/ns2:CountryCode/ns2:isoCountryCode
                where fn:string-length($countryCode) > 0
                return
                    &lt;ns0:KrajPochodzenia?>{ data($countryCode) }&lt;/ns0:KrajPochodzenia>
            }
            {
                for $email in $invoke1/ns4:customer/ns-1:Customer/ns-1:email
                where fn:string-length($email) > 0
                return
                    &lt;ns0:AdresEMail?>{ data($email) }&lt;/ns0:AdresEMail>
            }
            {
                for $phoneNo in $invoke1/ns4:customer/ns-1:Customer/ns-1:phoneNo
                where fn:string-length($phoneNo) > 0
                return
                    &lt;ns0:NrTelefonu?>{ data($phoneNo) }&lt;/ns0:NrTelefonu>
            }
            {
                for $mobileNo in $invoke1/ns4:customer/ns-1:Customer/ns-1:mobileNo
                where fn:string-length($mobileNo) > 0
                return
                    &lt;ns0:NrTelefKomorkowego?>{ data($mobileNo) }&lt;/ns0:NrTelefKomorkowego>
            }
            {
                for $businessNo in $invoke1/ns4:customer/ns-1:Customer/ns-1:businessNo
                where fn:string-length($businessNo) > 0
                return
                    &lt;ns0:NumerTelefonuPraca?>{ data($businessNo) }&lt;/ns0:NumerTelefonuPraca>
            }
            {
                for $identityCardNumber in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:identityCardNumber
                where fn:string-length($identityCardNumber) > 0
                return
                    &lt;ns0:NrDowoduRegon?>{ data($identityCardNumber) }&lt;/ns0:NrDowoduRegon>
            }
            {
                for $pesel in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:pesel
                where fn:string-length($pesel) > 0
                return
                    &lt;ns0:NrPesel?>{ data($pesel) }&lt;/ns0:NrPesel>
            }
            {
                for $taxID in $invoke1/ns4:customer/ns-1:Customer/ns-1:taxID
                where fn:string-length($taxID)
                return
                    &lt;ns0:Nip?>{ data($taxID) }&lt;/ns0:Nip>
            }
            {
                for $branchCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:signatureCardBranchId/ns2:BranchCode/ns2:branchCode
                where fn:string-length($branchCode) > 0
                return
                    &lt;ns0:KartaWzorowPodpisow?>{ data($branchCode) }&lt;/ns0:KartaWzorowPodpisow>
            }
            {
                for $branchRoutingNumber in $invoke1/ns4:customer/ns-1:Customer/ns-1:branchOfOwnership/ns2:BranchCode/ns2:branchCode
                where fn:string-length($branchRoutingNumber) > 0
                return
                    &lt;ns0:NrOddzialu?>{ xs:long( data($branchRoutingNumber) ) }&lt;/ns0:NrOddzialu>
            }
            {
                for $dataSource in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerAdditionalInfo/ns-1:CustomerAdditionalInfo/ns-1:dataSource/ns2:DataSource/ns2:dataSource
                where fn:string-length($dataSource) > 0
                return
                    &lt;ns0:ZrodloDanych?>{ data($dataSource) }&lt;/ns0:ZrodloDanych>
            }
            {
            	for $zgoda in $invoke1/ns4:customer/ns-1:Customer/ns-1:agreementBankInfo
            	where fn:string-length($zgoda) > 0
            	return
            		&lt;ns0:ZgodaTajemBank>{ local:booleanToString( data($zgoda) ) }&lt;/ns0:ZgodaTajemBank>
            }
            {
                for $customerDocumentType in $invoke1/ns4:customer/ns-1:Customer/ns-1:documentList/ns-1:Document[1]/ns-1:documentType/ns2:CustomerDocumentType/ns2:customerDocumentType
                where fn:string-length($customerDocumentType) > 0
                return
                    &lt;ns0:DokTozsamosci?>{ data($customerDocumentType) }&lt;/ns0:DokTozsamosci>
            }
            {
                for $documentNumber in $invoke1/ns4:customer/ns-1:Customer/ns-1:documentList/ns-1:Document[1]/ns-1:documentNumber
                where fn:string-length($documentNumber) > 0
                return
                    &lt;ns0:SeriaNrDok?>{ data($documentNumber) }&lt;/ns0:SeriaNrDok>
            }
            {
            	for $document in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:passportNumber
                where fn:string-length($document) > 0
            	return
            		&lt;ns0:NumerPaszportu?>{ data($document) }&lt;/ns0:NumerPaszportu>
            }
            {
                for $customerSexCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:sexCode/ns2:CustomerSexCode/ns2:customerSexCode
                where fn:string-length($customerSexCode) > 0
                return
                    &lt;ns0:Plec?>{ data($customerSexCode) }&lt;/ns0:Plec>
            }
            {
                for $firstName in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:firstName
                where fn:string-length($firstName) > 0
                return
                    &lt;ns0:Imie?>{ data($firstName) }&lt;/ns0:Imie>
            }
            {
                for $secondName in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:secondName
                where fn:string-length($secondName) > 0
                return
                    &lt;ns0:DrugieImie?>{ data($secondName) }&lt;/ns0:DrugieImie>
            }
            {
                for $lastName in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:lastName
                where fn:string-length($lastName) > 0
                return
                    &lt;ns0:Nazwisko?>{ data($lastName) }&lt;/ns0:Nazwisko>
            }
            {
                for $motherMaidenName in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:motherMaidenName
                where fn:string-length($motherMaidenName) > 0
                return
                    &lt;ns0:NazwiskoPanienskieMatki?>{ data($motherMaidenName) }&lt;/ns0:NazwiskoPanienskieMatki>
            }
            {
                for $citizenshipCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:citizenship/ns2:CitizenshipCode/ns2:isoCitizenshipCode
                where fn:string-length($citizenshipCode) > 0
                return
                    &lt;ns0:Obywatelstwo?>{ data($citizenshipCode) }&lt;/ns0:Obywatelstwo>
            }
            {
                for $dateOfBirth in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:dateOfBirth
                where fn:string-length($dateOfBirth) > 0
                return
                    &lt;ns0:DataUrodzenia?>{ local:YMDToDMY( data($dateOfBirth) ) }&lt;/ns0:DataUrodzenia>
            }
            {
                for $birthPlace in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:birthPlace
                where fn:string-length($birthPlace) > 0
                return
                    &lt;ns0:MiejsceUrodzenia?>{ data($birthPlace) }&lt;/ns0:MiejsceUrodzenia>
            }
            {
                for $languageID in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerAdditionalInfo/ns-1:CustomerAdditionalInfo/ns-1:language/ns1:Language/ns1:languageID
                where fn:string-length($languageID) > 0
                return
                    &lt;ns0:KodJezyka?>{ data($languageID) }&lt;/ns0:KodJezyka>
            }
            {
                for $customerOccupationCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerEmploymentInfoList/ns-1:CustomerEmploymentInfo[1]/ns-1:occupationCode/ns2:CustomerOccupationCode/ns2:customerOccupationCode
                where fn:string-length($customerOccupationCode) > 0
                return
                    &lt;ns0:KodZawoduWyuczonego?>{ xs:short( data($customerOccupationCode) ) }&lt;/ns0:KodZawoduWyuczonego>
            }
            {
                for $sourceOfIncome in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerFinancialInfo/ns-1:CustomerFinancialInfo/ns-1:sourceOfIncomeCode/ns2:SourceOfIncomeCode/ns2:sourceOfIncomeCode
                where fn:string-length($sourceOfIncome) > 0
                return
                    &lt;ns0:ZrodloDochodu?>{ data($sourceOfIncome) }&lt;/ns0:ZrodloDochodu>
            }
            {
                for $policyNo in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyNo
                where fn:string-length($policyNo) > 0
                return
                    &lt;ns0:LiczbaPolis?>{ xs:short( data($policyNo) ) }&lt;/ns0:LiczbaPolis>
            }
            {
                for $policyTotalAmount in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyTotalAmount
                where fn:string-length($policyTotalAmount) > 0
                return
                    &lt;ns0:SumaPolisNaZycie?>{ xs:string( data($policyTotalAmount) ) }&lt;/ns0:SumaPolisNaZycie>
            }
            {
                for $policyLife in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyLife
                where fn:string-length($policyLife) > 0
                return
                    &lt;ns0:UbezpieczenieNaZycie?>{ local:booleanToString( data($policyLife) ) }&lt;/ns0:UbezpieczenieNaZycie>
            }
            {
                for $policyOther in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyOther
                where fn:string-length($policyOther) > 0
                return
                    &lt;ns0:UbezpieczeniaInne?>{ local:booleanToString( data($policyOther) ) }&lt;/ns0:UbezpieczeniaInne>
            }
            {
                for $policyLifeName in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyLifeName/ns6:InsurerName/ns6:insurerName
                where fn:string-length($policyLifeName) > 0
                return
                    &lt;ns0:UbezpNaZycie?>{ data($policyLifeName) }&lt;/ns0:UbezpNaZycie>
            }
            {
                for $policyOtherName in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyOtherName/ns6:InsurerName/ns6:insurerName
                where fn:string-length($policyOtherName) > 0
                return
                    &lt;ns0:UbezpInne?>{ data($policyOtherName) }&lt;/ns0:UbezpInne>
            }
            {
                for $value in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyInvest
                where fn:string-length($value) > 0
                return
		            &lt;ns0:UbezpFundInwest?>{ local:booleanToString($value) }&lt;/ns0:UbezpFundInwest>
		    }
		    {
		    	for $value in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyInvestName/ns6:InsurerName/ns6:insurerName
		    	where fn:string-length($value) > 0
		    	return
		            &lt;ns0:NazwaFundInwest?>{ data($value) }&lt;/ns0:NazwaFundInwest>
            }
            {
		    	for $value in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyPremiumSum
		    	where fn:string-length($value) > 0
		    	return
		            &lt;ns0:SumaSkladekPolis?>{ data($value) }&lt;/ns0:SumaSkladekPolis>
		    }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
    	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
    	                   and fn:string-length($adres/ns-1:street) > 0
    	                return $adres
                return
                    &lt;ns0:UlicaAdresAlt?>{ data($adresy[1]/ns-1:street) }&lt;/ns0:UlicaAdresAlt>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
    	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
    	                  and fn:string-length($adres/ns-1:houseFlatNumber) > 0
    	                return $adres
                return
                    &lt;ns0:NrPosesLokaluAdresAlt?>{ data($adresy[1]/ns-1:houseFlatNumber) }&lt;/ns0:NrPosesLokaluAdresAlt>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
    	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
    	                  and fn:string-length($adres/ns-1:zipCode) > 0
    	                return $adres
                return
                    &lt;ns0:KodPocztowyAdresAlt?>{ data($adresy[1]/ns-1:zipCode) }&lt;/ns0:KodPocztowyAdresAlt>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
    	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
    	                  and fn:string-length($adres/ns-1:city) > 0
    	                return $adres
                return
                    &lt;ns0:MiastoAdresAlt?>{ data($adresy[1]/ns-1:city) }&lt;/ns0:MiastoAdresAlt>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
    	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
    	                  and fn:string-length($adres/ns-1:state/ns2:State/ns2:state) > 0
    	                return $adres
                return
                    &lt;ns0:WojewodztwoKrajAdresAlt?>{ data($adresy[1]/ns-1:state/ns2:State/ns2:state) }&lt;/ns0:WojewodztwoKrajAdresAlt>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
    	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
    	                  and fn:string-length($adres/ns-1:countryId/ns2:CountryCode/ns2:isoCountryCode) > 0
    	                return $adres
                return
                    &lt;ns0:KodKrajuKoresp?>{ data($adresy[1]/ns-1:countryId/ns2:CountryCode/ns2:isoCountryCode) }&lt;/ns0:KodKrajuKoresp>
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
    	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
    	                   and fn:string-length($adres/ns-1:validFrom) > 0
    	                return $adres
                return
                    &lt;ns0:DataOd?>{ local:YMDToDMY( data($adresy[1]/ns-1:validFrom) ) }&lt;/ns0:DataOd>
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
    	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
    	                  and fn:string-length($adres/ns-1:validTo) > 0
    	                return $adres
                return
                    &lt;ns0:DataDo?>{ local:YMDToDMY( data($adresy[1]/ns-1:validTo) ) }&lt;/ns0:DataDo>
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
    	                where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
    	                  and fn:string-length($adres/ns-1:typeOfLocation/ns2:AddressTypeOfLocation/ns2:addressTypeOfLocation) > 0
    	                return $adres
                return
                    &lt;ns0:TypAdresuKoresp?>{ data($adresy[1]/ns-1:typeOfLocation/ns2:AddressTypeOfLocation/ns2:addressTypeOfLocation) }&lt;/ns0:TypAdresuKoresp>
            }
            {
                for $countryCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:countryOfResidence/ns2:CountryCode/ns2:isoCountryCode
                where fn:string-length($countryCode) > 0
                return
                    &lt;ns0:KrajZamieszkania?>{ data($countryCode) }&lt;/ns0:KrajZamieszkania>
            }
            {
            	for $dataSmierci in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:dateOfDeath
            	where fn:string-length($dataSmierci) > 0
            	return
            		&lt;ns0:DataSmierci?>{ local:YMDToDMY( data($dataSmierci) ) }&lt;/ns0:DataSmierci>
            }
        &lt;/ns0:pcCustDataShareRequest>
};

declare variable $invoke1 as element(ns4:invoke) external;
declare variable $header1 as element(ns4:header) external;

&lt;soap-env:Body>{
xf:RequestTransform2($invoke1, $header1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>