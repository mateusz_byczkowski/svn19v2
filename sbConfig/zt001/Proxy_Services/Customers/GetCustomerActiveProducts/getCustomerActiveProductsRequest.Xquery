<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2011-05-10</con:description>
  <con:xquery>(: Change Log 
v.1.1 2011-03-23 PK  PT58 Content Streaming
:)

declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:card.entities.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns7="urn:dictionaries.be.dcl";
declare namespace ns8="urn:insurance.entities.be.dcl";
declare namespace ns9="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml="";
declare namespace ns10="urn:errors.hlbsentities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



declare function boolean2SourceValue($parm as xs:string*,$trueval as xs:string,$falseval as xs:string) as xs:string* {
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>
(:v.1.1 Start:)
,
&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>
(:v.1.1 Koniec:)
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

&lt;NF_PRODUA_CODEPRODUCTAREA?>{data($parm/ns0:productArea/ns1:ProductArea/ns1:codeProductArea)}&lt;/NF_PRODUA_CODEPRODUCTAREA>
,
&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
&lt;NF_PAGEC_REVERSEORDER?>{boolean2SourceValue(data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:reverseOrder),"1","0")}&lt;/NF_PAGEC_REVERSEORDER>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
,
&lt;NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns0:customer/ns4:Customer/ns4:customerNumber)}&lt;/NF_CUSTOM_CUSTOMERNUMBER>
,
&lt;NF_CTRL_ACTIVENONACTIVE>1&lt;/NF_CTRL_ACTIVENONACTIVE>
,
&lt;NF_ACCOUN_ALTERNATIVEADDRE>0&lt;/NF_ACCOUN_ALTERNATIVEADDRE>
,
&lt;NF_CTRL_OPTION>5&lt;/NF_CTRL_OPTION>
, 
&lt;NF_CTRL_SYSTEMS>001&lt;/NF_CTRL_SYSTEMS>
,
&lt;NF_CTRL_SHOWCURRBAL?>{data($parm/ns0:showCurrentBalance/ns6:IntegerHolder/ns6:value)}&lt;/NF_CTRL_SHOWCURRBAL>
,
&lt;NF_PRODUG_VISIBILITYDCL>1&lt;/NF_PRODUG_VISIBILITYDCL>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>