<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:bool2short($bool as xs:boolean) as xs:string {
	if ($bool)
		then "1"
		else "0"
};

declare function getFieldsFromHeader($parm as element(ns5:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns5:msgHeader/ns5:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns5:msgHeader/ns5:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns5:msgHeader/ns5:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns5:msgHeader/ns5:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns5:msgHeader/ns5:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns5:msgHeader/ns5:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns5:transHeader/ns5:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns5:invoke)) as element()*
{

&lt;NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns5:account/ns0:Account/ns0:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER>
,
&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns5:bcd/ns3:BusinessControlData/ns3:pageControl/ns6:PageControl/ns6:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns5:bcd/ns3:BusinessControlData/ns3:pageControl/ns6:PageControl/ns6:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
    if (data($parm/ns5:bcd/ns3:BusinessControlData/ns3:pageControl/ns6:PageControl/ns6:reverseOrder))
      then 	
        &lt;NF_PAGEC_REVERSEORDER?>{xf:bool2short(data($parm/ns5:bcd/ns3:BusinessControlData/ns3:pageControl/ns6:PageControl/ns6:reverseOrder))}&lt;/NF_PAGEC_REVERSEORDER>
    else()
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns5:bcd/ns3:BusinessControlData/ns3:pageControl/ns6:PageControl/ns6:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns5:bcd/ns3:BusinessControlData/ns3:pageControl/ns6:PageControl/ns6:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns5:header)}
    {getFieldsFromInvoke($body/ns5:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>