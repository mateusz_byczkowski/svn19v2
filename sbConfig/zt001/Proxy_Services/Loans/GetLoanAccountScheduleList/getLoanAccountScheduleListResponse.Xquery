<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:short2bool($short as xs:int) as xs:string {
	if ($short >= 1)
		then "true"
		else "false"
};

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

declare function getElementsForSchedules($parm as element(fml:FML32)) as element()
{

&lt;ns4:schedules>
  {
    for $x at $occ in $parm/NF_LOANAS_PAYMENTDATE
    return
    &lt;ns0:LoanAccountSchedule>
      {if (data($parm/NF_LOANAS_PAYMENTDATE[$occ]) and xf:isData(data($parm/NF_LOANAS_PAYMENTDATE[$occ]))) 
      	then &lt;ns0:paymentDate?>{data($parm/NF_LOANAS_PAYMENTDATE[$occ])}&lt;/ns0:paymentDate>
      	else ()
      }
      {if (data($parm/NF_LOANAS_TOTALPAYMENT[$occ]) and xf:isData(data($parm/NF_LOANAS_TOTALPAYMENT[$occ])))
      	then &lt;ns0:totalPayment?>{data($parm/NF_LOANAS_TOTALPAYMENT[$occ])}&lt;/ns0:totalPayment>
      	else ()
      }
      {if (data($parm/NF_LOANAS_PRINCIPALPAYMENT[$occ]) and xf:isData(data($parm/NF_LOANAS_PRINCIPALPAYMENT[$occ])))
                then &lt;ns0:principalPayment?>{data($parm/NF_LOANAS_PRINCIPALPAYMENT[$occ])}&lt;/ns0:principalPayment>
      	else ()
      }      
      {if (data($parm/NF_LOANAS_INTERESTPAYMENT[$occ]) and xf:isData(data($parm/NF_LOANAS_INTERESTPAYMENT[$occ])))
      	then &lt;ns0:interestPayment?>{data($parm/NF_LOANAS_INTERESTPAYMENT[$occ])}&lt;/ns0:interestPayment>
      	else ()
      }
      {if (data($parm/NF_LOANAS_PRINCIPALBALANCE[$occ]) and xf:isData(data($parm/NF_LOANAS_PRINCIPALBALANCE[$occ])))
      	then &lt;ns0:principalBalance?>{data($parm/NF_LOANAS_PRINCIPALBALANCE[$occ])}&lt;/ns0:principalBalance>
      	else ()
      }
    &lt;/ns0:LoanAccountSchedule>
  }
&lt;/ns4:schedules>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  {getElementsForSchedules($parm)}
  &lt;ns4:bcd>
    &lt;ns2:BusinessControlData>
      &lt;ns2:pageControl>
        &lt;ns5:PageControl>
          &lt;ns5:hasNext?>{xf:short2bool(data($parm/NF_PAGEC_HASNEXT))}&lt;/ns5:hasNext>
          &lt;ns5:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns5:navigationKeyDefinition>
          &lt;ns5:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns5:navigationKeyValue>
        &lt;/ns5:PageControl>
      &lt;/ns2:pageControl>
    &lt;/ns2:BusinessControlData>
  &lt;/ns4:bcd>
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>