<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn ="urn:be.services.dcl";
declare namespace urn1="urn:insurance.entities.be.dcl";
declare namespace urn2="urn:insurancedict.dictionaries.be.dcl";
declare namespace urn3="urn:productstree.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInsurancePackageAccList($parm as element(fml:FML32)) as element() {
  &lt;urn:insurancePackageAccList>
    {
      for $x at $occ in $parm/NF_INSUPA_NUMBER
      return
        &lt;urn1:InsurancePackageAcc>
          &lt;urn1:type?>{data($parm/NF_INSUPA_TYPE[$occ])}&lt;/urn1:type>
          &lt;urn1:number?>{data($parm/NF_INSUPA_NUMBER[$occ])}&lt;/urn1:number>
          &lt;urn1:name?>{data($parm/NF_INSUPA_NAME[$occ])}&lt;/urn1:name>
          &lt;urn1:status?>
            &lt;urn2:InsurancePackageStatus>
              &lt;urn2:insurancePackageStatus>{data($parm/NF_INSUPA_STATUS[$occ])}&lt;/urn2:insurancePackageStatus>
            &lt;/urn2:InsurancePackageStatus>
          &lt;/urn1:status>
        &lt;/urn1:InsurancePackageAcc>
    }
  &lt;/urn:insurancePackageAccList>
};

declare function getElementsForInsurancePolicyAccList($parm as element(fml:FML32)) as element() {
  &lt;urn:insurancePolicyAccList>
  {
    for $x at $occ in $parm/NF_INSPAC_POLICYREFNUM
    return
      &lt;urn1:InsurancePolicyAcc>
        &lt;urn1:policyRefNum?>{data($parm/NF_INSPAC_POLICYREFNUM[$occ])}&lt;/urn1:policyRefNum>
        &lt;urn1:insuranceDescription?>{data($parm/NF_INSPAC_INSURANCEDESCRIP[$occ])}&lt;/urn1:insuranceDescription>
        &lt;urn1:icbsProductNumber?>{data($parm/NF_INSPAC_ICBSPRODUCTNUMBE[$occ])}&lt;/urn1:icbsProductNumber>
        &lt;urn1:productDefinition>
          &lt;urn3:ProductDefinition>
            &lt;urn3:idProductDefinition?>{data($parm/NF_PRODUD_IDPRODUCTDEFINIT[$occ])}&lt;/urn3:idProductDefinition>
          &lt;/urn3:ProductDefinition>
        &lt;/urn1:productDefinition>
        &lt;urn1:status>
          &lt;urn2:InsurancePolicyStatus>
            &lt;urn2:insurancePolicyStatus?>{data($parm/NF_INSPST_INSURANCEPOLICYS[$occ])}&lt;/urn2:insurancePolicyStatus>
          &lt;/urn2:InsurancePolicyStatus>
        &lt;/urn1:status>
      &lt;/urn1:InsurancePolicyAcc>
  }
&lt;/urn:insurancePolicyAccList>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()* {
  &lt;urn:invokeResponse>
    {getElementsForInsurancePackageAccList($parm)}
    {getElementsForInsurancePolicyAccList($parm)}
  &lt;/urn:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>