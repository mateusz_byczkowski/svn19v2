<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:cif.entities.be.dcl";
declare namespace m3 = "urn:dictionaries.be.dcl";
declare namespace m4 = "urn:accountdict.dictionaries.be.dcl";
declare namespace m5 = "urn:basedictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string

  return 
    if ($value &lt; 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
    fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-date($dateIn)))
};

declare function xf:booleanTo01($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "1"
    else "0"
};

declare function xf:shortAccount($account as xs:string) as xs:string {
    if (fn:string-length($account) > 12)
      then fn:substring($account, (fn:string-length($account))-11)
      else $account
};

&lt;soap-env:Body>
  {
    let $reqh  := $header/m:header
    let $req   := $body/m:invoke/m:account/m1:Account
    let $req1  := $req/m1:loanAccount/m1:LoanAccount
    let $req2  := $req1/m1:loanAccountScheduleList/m1:LoanAccountSchedule/m1:loanAccountScheduleRuleList
    let $req3  := $req/m1:accountAddress/m1:AccountAddress

    return

    &lt;fml:FML32>
      {if($reqh/m:msgHeader/m:msgId)
          then &lt;fml:DC_MSHEAD_MSGID>{ data($reqh/m:msgHeader/m:msgId) }&lt;/fml:DC_MSHEAD_MSGID>
          else ()
      }
      {if($reqh/m:transHeader/m:transId)
          then &lt;fml:DC_TRN_ID>{ data($reqh/m:transHeader/m:transId) }&lt;/fml:DC_TRN_ID>
          else ()
      }
      {if($reqh/m:msgHeader/m:userId)
          then &lt;fml:DC_UZYTKOWNIK>{concat("SKP:", data($reqh/m:msgHeader/m:userId)) }&lt;/fml:DC_UZYTKOWNIK>
          else ()
      }
      {if($reqh/m:msgHeader/m:unitId)
          then 
              if($reqh/m:msgHeader/m:unitId &lt; 1000)
                then &lt;fml:DC_ODDZIAL>{ data($reqh/m:msgHeader/m:unitId) }&lt;/fml:DC_ODDZIAL>
                else &lt;fml:DC_ODDZIAL>0&lt;/fml:DC_ODDZIAL>
          else ()
      }

      {if($req/m1:accountOpenDate and fn:string-length($req/m1:accountOpenDate)>0)
          then &lt;fml:DC_DATA_OTWARCIA_RACHUNKU>{ xf:mapDate($req/m1:accountOpenDate) }&lt;/fml:DC_DATA_OTWARCIA_RACHUNKU>
          else ()
      }
      {for $it in $req/m1:indirectCommitmenntList/m1:IndirectCommitment
         return
         if($it/m1:typeIndirectCommitment)
           then &lt;fml:DC_TYP_POSRED_ZOBOWIAZANIA>{ data($it/m1:typeIndirectCommitment) }&lt;/fml:DC_TYP_POSRED_ZOBOWIAZANIA>
           else ()
      }  
      {for $it in $req/m1:indirectCommitmenntList/m1:IndirectCommitment
         return
         if($it/m1:amountMaxIndirectCommitment)
           then &lt;fml:DC_MAX_KWOTA_PORECZENIA>{ data($it/m1:amountMaxIndirectCommitment) }&lt;/fml:DC_MAX_KWOTA_PORECZENIA>
           else ()
      }
      {for $it in $req/m1:indirectCommitmenntList/m1:IndirectCommitment
         return
         if($it/m1:indirectCommitmentPercent)
           then &lt;fml:DC_PROCENT_ZOBOWIAZANIA>{ data($it/m1:indirectCommitmentPercent) }&lt;/fml:DC_PROCENT_ZOBOWIAZANIA>
           else ()
      }
      {for $it in $req/m1:indirectCommitmenntList/m1:IndirectCommitment
         return
         if($it/m1:customer/m2:Customer/m2:customerNumber)
           then &lt;fml:DC_NUMER_PORECZYCIELA_Z_CIF>{ data($it/m1:customer/m2:Customer/m2:customerNumber) }&lt;/fml:DC_NUMER_PORECZYCIELA_Z_CIF>
           else ()
      }
      {for $it in $req/m1:accountRelationshipList/m2:AccountRelationship
         return
         if($it/m2:customer/m2:Customer/m2:customerNumber)
           then &lt;fml:DC_NUMER_KLIENTA_REL>{ data($it/m2:customer/m2:Customer/m2:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA_REL>
           else ()
      }
      {for $it in $req/m1:accountRelationshipList/m2:AccountRelationship
         return
         if($it/m2:relationship/m3:CustomerAccountRelationship/m3:customerAccountRelationship)
           then &lt;fml:DC_RELACJA>{ data($it/m2:relationship/m3:CustomerAccountRelationship/m3:customerAccountRelationship) }&lt;/fml:DC_RELACJA>
           else ()
      }
      {for $it in $req/m1:accountRelationshipList/m2:AccountRelationship
         return
         if($it/m2:relationship/m3:CustomerAccountRelationship/m3:customerAccountRelationship='IND')
           then &lt;fml:DC_TYP_RELACJI>2&lt;/fml:DC_TYP_RELACJI>
           else &lt;fml:DC_TYP_RELACJI>1&lt;/fml:DC_TYP_RELACJI>
      }

      {for $it in $req/m1:accountRelationshipList/m2:AccountRelationship
         return
         if(fn:contains("SOW JAF JOF OWN",$it/m2:relationship/m3:CustomerAccountRelationship/m3:customerAccountRelationship))
           then &lt;fml:DC_PROCENT_ZOB_PODATKOWYCH>100&lt;/fml:DC_PROCENT_ZOB_PODATKOWYCH>
           else &lt;fml:DC_PROCENT_ZOB_PODATKOWYCH>0&lt;/fml:DC_PROCENT_ZOB_PODATKOWYCH>
      }
      {if($req1/m1:interestGuaranteeCode)
          then &lt;fml:DC_KOD_STOPY>{ data($req1/m1:interestGuaranteeCode) }&lt;/fml:DC_KOD_STOPY>
          else ()
      }
      {if($req1/m1:interestRate)
          then &lt;fml:DC_STOPA_PROCENTOWA>{ data($req1/m1:interestRate) }&lt;/fml:DC_STOPA_PROCENTOWA>
          else ()
      }
      {if($req1/m1:indexRateNumber)
          then &lt;fml:DC_INDEKS_STOPY>{ data($req1/m1:indexRateNumber) }&lt;/fml:DC_INDEKS_STOPY>
          else ()
      }
      {if($req1/m1:varianceFromIndex)
          then &lt;fml:DC_MARZA>{ data($req1/m1:varianceFromIndex) }&lt;/fml:DC_MARZA>
          else ()
      }
      {if($req1/m1:rateReviewPeriod)
          then &lt;fml:DC_CZEST_OKRES_SD_OKRES>{ data($req1/m1:rateReviewPeriod) }&lt;/fml:DC_CZEST_OKRES_SD_OKRES>
          else ()
      }
      {if($req1/m1:rateReviewFrequency)
          then &lt;fml:DC_CZEST_OKRES_SD_CZEST>{ data($req1/m1:rateReviewFrequency) }&lt;/fml:DC_CZEST_OKRES_SD_CZEST>
          else ()
      }
      {if($req1/m1:nextRateReviewChangeDate and fn:string-length($req1/m1:nextRateReviewChangeDate)>0)
          then &lt;fml:DC_DATA_NASTEPNEJ_ZMIANY>{ xf:mapDate($req1/m1:nextRateReviewChangeDate) }&lt;/fml:DC_DATA_NASTEPNEJ_ZMIANY>
          else ()
      }
      {if($req1/m1:faceAmount)
          then &lt;fml:DC_KWOTA_KREDYTU>{ data($req1/m1:faceAmount) }&lt;/fml:DC_KWOTA_KREDYTU>
          else ()
      }
      {if($req1/m1:productpppType)
          then &lt;fml:DC_NUMER_PRODUKTU>{ data($req1/m1:productpppType) }&lt;/fml:DC_NUMER_PRODUKTU>
          else ()
      }
      {if($req1/m1:officerCode)
          then &lt;fml:DC_KOD_PRACOWNIKA>{ data($req1/m1:officerCode) }&lt;/fml:DC_KOD_PRACOWNIKA>
          else ()
      }
      {if($req1/m1:maturityDate and fn:string-length($req1/m1:maturityDate)>0)
          then &lt;fml:DC_DATA_WYMAGALNOSCI_KREDYTU>{ xf:mapDate($req1/m1:maturityDate) }&lt;/fml:DC_DATA_WYMAGALNOSCI_KREDYTU>
          else ()
      }
      {if($req1/m1:adjustAvail and $req1/m1:adjustAvail = true())
          then &lt;fml:DC_REWOLWING>{ xf:booleanTo01($req1/m1:adjustAvail) }&lt;/fml:DC_REWOLWING>
          else ()
      }
      {if($req1/m1:smsa)
          then &lt;fml:DC_SC_RESTR>{ data($req1/m1:smsa) }&lt;/fml:DC_SC_RESTR>
          else ()
      }
      {if($req1/m1:indexRateMultiplier)
          then &lt;fml:DC_MNOZNIK_INDEKS_MARZA>{ data($req1/m1:indexRateMultiplier) }&lt;/fml:DC_MNOZNIK_INDEKS_MARZA>
          else ()
      }
      {if($req1/m1:rateChangeAtStartDateFlag and fn:string-length($req1/m1:rateChangeAtStartDateFlag)>0)
          then &lt;fml:DC_ZZ_STOPY_PRZY_DACIE_URUCH>{ xf:booleanTo01($req1/m1:rateChangeAtStartDateFlag) }&lt;/fml:DC_ZZ_STOPY_PRZY_DACIE_URUCH>
          else ()
      }
      {if($req1/m1:rateReviewSpDay)
          then &lt;fml:DC_CZEST_OKRES_SD_DZIEN>{ data($req1/m1:rateReviewSpDay) }&lt;/fml:DC_CZEST_OKRES_SD_DZIEN>
          else ()
      }
      {if($req1/m1:timesToUse)
          then &lt;fml:DC_LICZBA_RAT_ODS>{ data($req1/m1:timesToUse) }&lt;/fml:DC_LICZBA_RAT_ODS>
          else ()
      }
      {if($req1/m1:interestPeriod)
          then &lt;fml:DC_OKRES_SPLAT_ODS>{ data($req1/m1:interestPeriod) }&lt;/fml:DC_OKRES_SPLAT_ODS>
          else ()
      }
      {if($req1/m1:interestFrequency)
          then &lt;fml:DC_CZEST_SPLAT_ODS>{ data($req1/m1:interestFrequency) }&lt;/fml:DC_CZEST_SPLAT_ODS>
          else ()
      }
      {if($req1/m1:firstInterestBillingDate and fn:string-length($req1/m1:firstInterestBillingDate)>0)
          then &lt;fml:DC_DATA_PIERWSZEJ_PLAT_ODS>{ xf:mapDate($req1/m1:firstInterestBillingDate) }&lt;/fml:DC_DATA_PIERWSZEJ_PLAT_ODS>
        else ()
      }
      {if($req1/m1:specificDayOfMonthToBill)
          then &lt;fml:DC_OKRESLONY_DZIEN_ODS>{ data($req1/m1:specificDayOfMonthToBill) }&lt;/fml:DC_OKRESLONY_DZIEN_ODS>
          else ()
      }
      {if($req1/m1:rateChangesWindexStartDate)
          then &lt;fml:DC_ZM_STOPY_DATA_OBOWIAZ_INDE>{ xf:mapDate($req1/m1:rateChangesWindexStartDate) }&lt;/fml:DC_ZM_STOPY_DATA_OBOWIAZ_INDE>
          else ()
      }
      {if($req1/m1:targetProfileNumber)
          then &lt;fml:DC_NR_PROFILU_ZLECENIOBIORCY>{ data($req1/m1:targetProfileNumber) }&lt;/fml:DC_NR_PROFILU_ZLECENIOBIORCY>
          else ()
      }
      {if($req1/m1:icbsAccountNumber)
          then &lt;fml:DC_NR_RACHUNKU_W_SYSTEMIE>{ xf:shortAccount($req1/m1:icbsAccountNumber) }&lt;/fml:DC_NR_RACHUNKU_W_SYSTEMIE>
          else ()
      }
      {if($req1/m1:debitcode)
          then &lt;fml:DC_KOD_STOPY_OBCIAZENIA>{ data($req1/m1:debitcode) }&lt;/fml:DC_KOD_STOPY_OBCIAZENIA>
          else ()
      }
      {if($req1/m1:creditcode)
          then &lt;fml:DC_KOD_STOPY_UZNANIA>{ data($req1/m1:creditcode) }&lt;/fml:DC_KOD_STOPY_UZNANIA>
          else ()
      }
      {if($req1/m1:rateChangeCode)
          then &lt;fml:DC_KOD_KURSU_WYMIANY>{ data($req1/m1:rateChangeCode) }&lt;/fml:DC_KOD_KURSU_WYMIANY>
          else ()
      }


      {for $it in $req2/m1:LoanAccountScheduleRule
         return
         if($it/m1:paymentFrequency)
           then &lt;fml:DC_CZEST_SPLAT>{ data($it/m1:paymentFrequency) }&lt;/fml:DC_CZEST_SPLAT>
           else ()
      }
      {for $it in $req2/m1:LoanAccountScheduleRule
         return
         if($it/m1:pmtInSched)
           then &lt;fml:DC_LICZBA_RAT_HARM>{ data($it/m1:pmtInSched) }&lt;/fml:DC_LICZBA_RAT_HARM>
           else ()
      }
      {for $it in $req2/m1:LoanAccountScheduleRule
         return
         if($it/m1:paymntAmountOrPctg)
           then &lt;fml:DC_KWOTA_RATY_LUB_PROCENT_999>{ data($it/m1:paymntAmountOrPctg) }&lt;/fml:DC_KWOTA_RATY_LUB_PROCENT_999>
           else ()
      }
      {for $it in $req2/m1:LoanAccountScheduleRule
         return
         if($it/m1:firstPmtDate and fn:string-length($it/m1:firstPmtDate)>0)
           then &lt;fml:DC_DATA_PIERWSZEJ_PLATNOSCI>{ xf:mapDate($it/m1:firstPmtDate) }&lt;/fml:DC_DATA_PIERWSZEJ_PLATNOSCI>
           else ()
      }
      {for $it in $req2/m1:LoanAccountScheduleRule
        return
        if($it/m1:paymentPeriod/m3:Period/m3:period)
          then &lt;fml:DC_OKRES_SPLAT>{ data($it/m1:paymentPeriod/m3:Period/m3:period) }&lt;/fml:DC_OKRES_SPLAT>
          else ()
      }
      {for $it in $req2/m1:LoanAccountScheduleRule
         return
         if($it/m1:paymentType/m4:CreditSchedulePaymentType/m4:creditSchedulePaymentType)
           then &lt;fml:DC_SPOSOB_PLATN>{ data($it/m1:paymentType/m4:CreditSchedulePaymentType/m4:creditSchedulePaymentType) }&lt;/fml:DC_SPOSOB_PLATN>
           else ()
      }
      {for $it in $req2/m1:LoanAccountScheduleRule
         return
         if($it/m1:specificDay/m3:SpecialDay/m3:specialDay)
           then &lt;fml:DC_SPECYFICZNY_DZIEN_SPLATY>{ data($it/m1:specificDay/m3:SpecialDay/m3:specialDay) }&lt;/fml:DC_SPECYFICZNY_DZIEN_SPLATY>
           else ()
      }

    
      {for $it in $req1/m1:loanFeeList/m1:LoanFee
         return
         if($it/m1:feeAmount)
          then &lt;fml:DC_KWOTA_PROCENT_PROWIZJI_KWOT>{ data($it/m1:feeAmount) }&lt;/fml:DC_KWOTA_PROCENT_PROWIZJI_KWOT>
          else ()
      }
      {for $it in $req1/m1:loanFeeList/m1:LoanFee
         return
         if($it/m1:feePercentage)
           then &lt;fml:DC_KWOTA_PROCENT_PROWIZJI_PROC>{ data($it/m1:feePercentage) }&lt;/fml:DC_KWOTA_PROCENT_PROWIZJI_PROC>
           else ()
      }
      {for $it in $req1/m1:loanFeeList/m1:LoanFee
         return
         if($it/m1:feeNumber)
           then &lt;fml:DC_NUMER_PROWIZJI>{ data($it/m1:feeNumber) }&lt;/fml:DC_NUMER_PROWIZJI>
           else ()
      }
      {for $it in $req1/m1:loanFeeList/m1:LoanFee
         return
         if($it/m1:feeStartDate and fn:string-length($it/m1:feeStartDate)>0)
           then &lt;fml:DC_DATA_NALICZENIA>{ xf:mapDate($it/m1:feeStartDate) }&lt;/fml:DC_DATA_NALICZENIA>
           else ()
      }
      {for $it in $req1/m1:loanFeeList/m1:LoanFee
         return
         if($it/m1:feePeriod)
           then &lt;fml:DC_OKRES_NALICZENIA>{ data($it/m1:feePeriod) }&lt;/fml:DC_OKRES_NALICZENIA>
           else ()
      }
      {for $it in $req1/m1:loanFeeList/m1:LoanFee
         return
         if($it/m1:feeFrequency)
           then &lt;fml:DC_CZESTOT_NALICZENIA>{ data($it/m1:feeFrequency) }&lt;/fml:DC_CZESTOT_NALICZENIA>
           else ()
      }
      {for $it in $req1/m1:loanFeeList/m1:LoanFee
         return
         if($it/m1:feeBillCode)
           then &lt;fml:DC_SPOSOB_SPLATY>{ data($it/m1:feeBillCode) }&lt;/fml:DC_SPOSOB_SPLATY>
           else ()
      }
      {for $it in $req1/m1:loanFeeList/m1:LoanFee
         return
         if($it/m1:feeSpecificDay)
           then &lt;fml:DC_OKR_DZIEN_NALICZENIA>{ data($it/m1:feeSpecificDay) }&lt;/fml:DC_OKR_DZIEN_NALICZENIA>
           else ()
      }
      {for $it in $req1/m1:loanFeeList/m1:LoanFee
         return
         if($it/m1:feeCalculationCode/m3:FeeChargeCode/m3:feeChargeCode)
           then &lt;fml:DC_KOD_NALICZENIA>{ data($it/m1:feeCalculationCode/m3:FeeChargeCode/m3:feeChargeCode) }&lt;/fml:DC_KOD_NALICZENIA>
           else ()
      }
      {for $it in $req1/m1:loanFeeList/m1:LoanFee
         return
         if($it/m1:feeStartCode/m3:FeeCalculateMethod/m3:feeCalculateMethod)
           then &lt;fml:DC_SPOSOB_NALICZENIA>{ data($it/m1:feeStartCode/m3:FeeCalculateMethod/m3:feeCalculateMethod) }&lt;/fml:DC_SPOSOB_NALICZENIA>
           else ()
      }
      
      
      {if($req1/m1:branchNumber/m5:BaseBranchCode/m5:branchCode)
          then &lt;fml:DC_NUMER_ODDZIALU>{ data($req1/m1:branchNumber/m5:BaseBranchCode/m5:branchCode) }&lt;/fml:DC_NUMER_ODDZIALU>
          else ()
      }
      {if($req1/m1:censusTract/m3:CreditObject/m3:object)
          then &lt;fml:DC_CEL_KREDYTOWANIA>{ data($req1/m1:censusTract/m3:CreditObject/m3:object) }&lt;/fml:DC_CEL_KREDYTOWANIA>
          else ()
      }


      {if($req3/m1:name1)
          then &lt;fml:DC_IMIE_I_NAZWISKO_ALT>{ data($req3/m1:name1) }&lt;/fml:DC_IMIE_I_NAZWISKO_ALT>
          else ()
      }
      {if($req3/m1:name2)
          then &lt;fml:DC_IMIE_I_NAZWISKO_ALT_C_D>{ data($req3/m1:name2) }&lt;/fml:DC_IMIE_I_NAZWISKO_ALT_C_D>
          else ()
      }
      {if($req3/m1:street)
          then &lt;fml:DC_ULICA_ADRES_ALT>{ data($req3/m1:street) }&lt;/fml:DC_ULICA_ADRES_ALT>
          else ()
      }
      {if($req3/m1:houseFlatNumber)
          then &lt;fml:DC_NR_POSES_LOKALU_ADRES_ALT>{ data($req3/m1:houseFlatNumber) }&lt;/fml:DC_NR_POSES_LOKALU_ADRES_ALT>
          else ()
      }
      {if($req3/m1:city)
          then &lt;fml:DC_MIASTO_ADRES_ALT>{ data($req3/m1:city) }&lt;/fml:DC_MIASTO_ADRES_ALT>
          else ()
      }
      {if($req3/m1:city)
          then &lt;fml:DC_RODZAJ_RACHUNKU>50&lt;/fml:DC_RODZAJ_RACHUNKU>
          else()
      }
      {if($req3/m1:stateCountry)
          then &lt;fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{ data($req3/m1:stateCountry) }&lt;/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
          else ()
      }
      {if($req3/m1:zipCode)
          then &lt;fml:DC_KOD_POCZTOWY_ADRES_ALT>{ data($req3/m1:zipCode) }&lt;/fml:DC_KOD_POCZTOWY_ADRES_ALT>
          else ()
      }
      {if($req3/m1:accountAddressType)
          then &lt;fml:DC_TYP_ADRESU>{ data($req3/m1:accountAddressType) }&lt;/fml:DC_TYP_ADRESU>
          else ()
      }
      {if($req3/m1:validFrom and fn:string-length($req3/m1:validFrom)>0)
          then &lt;fml:DC_DATA_WPROWADZENIA>{ xf:mapDate($req3/m1:validFrom) }&lt;/fml:DC_DATA_WPROWADZENIA>
          else ()
      }
      {if($req3/m1:validTo and fn:string-length($req3/m1:validTo)>0)
          then &lt;fml:DC_DATA_KONCOWA>{ xf:mapDate($req3/m1:validTo) }&lt;/fml:DC_DATA_KONCOWA>
          else ()
      }
      {if($req3/m1:deleteWhenExpired and fn:string-length($req3/m1:deleteWhenExpired)>0)
          then &lt;fml:DC_KASOWAC_PRZY_WYGASNIECIU>{ xf:booleanTo01($req3/m1:deleteWhenExpired) }&lt;/fml:DC_KASOWAC_PRZY_WYGASNIECIU>
          else ()
      }

      &lt;fml:DC_NR_PROFILU_STALEGO_ZLEC>2&lt;/fml:DC_NR_PROFILU_STALEGO_ZLEC>
    &lt;/fml:FML32>
  }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>