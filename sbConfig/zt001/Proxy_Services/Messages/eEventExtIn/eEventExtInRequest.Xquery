<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cekemessages/";
declare namespace xf = "http://bzwbk.com/services/cekemappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeEventExtInRequest($req as element(m:eEventExtInRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				for $event in $req/m:event
				return
					(&lt;fml:EV_TYPE>{ data($event/m:type) }&lt;/fml:EV_TYPE>,
					&lt;fml:EV_KEY>{ data($event/m:key) }&lt;/fml:EV_KEY>,
					&lt;fml:EV_CHANNEL_TYPE>{ data($event/m:channelType) }&lt;/fml:EV_CHANNEL_TYPE>,
					&lt;fml:EV_SEND_ADDRESS>{ data($event/m:sendAddress) }&lt;/fml:EV_SEND_ADDRESS>,
					&lt;fml:EV_CONTENT>{ data($event/m:content) }&lt;/fml:EV_CONTENT>)
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapeEventExtInRequest($body/m:eEventExtInRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>