<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:shortAccount($account as xs:string) as xs:string {
    if (fn:string-length($account) > 12)
      then fn:substring($account, (fn:string-length($account))-11)
      else $account
};

  
let $reqh := $header/m:header
	
let $PledgeCollateralToAccount := $body/m:invoke/m:pledgeCollateral/m1:PledgeCollateralToAccount
let $Collateral := $PledgeCollateralToAccount/m1:collateral/m1:Collateral
let $Account := $PledgeCollateralToAccount/m1:account/m1:Account
let $PledgedToApplication := $PledgeCollateralToAccount/m1:pledgedToApplication/m2:PledgedToApplication
let $PledgingRule := $PledgeCollateralToAccount/m1:pledgingRule/m2:PledgingRule
let $CurrencyRateCode := $PledgeCollateralToAccount/m1:crossCurrencyRateCode/m2:CurrencyRateCode
let $TranAccountACA := $Account/m1:tranAccount/m1:TranAccount/m1:tranAccountACAList/m1:TranAccountACA
	
let $account_maximumPledgeAmount := $PledgeCollateralToAccount/m1:maximumPledgeAmount
let $pledgingPriority := $PledgeCollateralToAccount/m1:pledgingPriority
let $kolejnosc := if ($pledgingPriority) then $pledgingPriority else 0
let $collateralItemNumber := $Collateral/m1:collateralItemNumber
let $accountNumber := if($Account/m1:accountNumber) then xf:shortAccount($Account/m1:accountNumber) else ()
let $customerNumber := $Account/m1:customerNumber
let $pledgedToApplication := $PledgedToApplication/m2:pledgedToApplication
let $pledgingRule := $PledgingRule/m2:pledgingRule
let $currencyRateCode := $CurrencyRateCode/m2:currencyRateCode
let $aca_maximumPledgeAmount := $TranAccountACA/m1:maximumPledgeAmount
let $aca_subaccountNumber := $TranAccountACA/m1:subaccountNumber

let $msgId := $reqh/m:msgHeader/m:msgId
let $transId := $reqh/m:transHeader/m:transId
let $userId  := $reqh/m:msgHeader/m:userId
let $unitId  := $reqh/m:msgHeader/m:unitId

return
	
&lt;soap-env:Body>   
 
   &lt;fml:FML32>
      &lt;fml:DC_MSHEAD_MSGID?>{ data($msgId) }&lt;/fml:DC_MSHEAD_MSGID> 
      &lt;fml:DC_TRN_ID?>{ data($transId) }&lt;/fml:DC_TRN_ID>
      &lt;fml:DC_UZYTKOWNIK?>{concat("SKP:", data($userId)) }&lt;/fml:DC_UZYTKOWNIK>

      {
        if($unitId) then 
           if($unitId &lt; 1000)
             then &lt;fml:DC_ODDZIAL>{ data($unitId) }&lt;/fml:DC_ODDZIAL>
             else &lt;fml:DC_ODDZIAL>0&lt;/fml:DC_ODDZIAL>
        else ()
      }

      {
        if($TranAccountACA/m1:subaccountNumber)then
           for $tran at $i in $TranAccountACA return
           (
            &lt;fml:DC_NR_LINII_KREDYTOWEJ_ACA>{ data($aca_subaccountNumber[$i]) }&lt;/fml:DC_NR_LINII_KREDYTOWEJ_ACA>,
            &lt;fml:DC_MAKS_KWOTA_ZABEZPIECZONA>{ data($aca_maximumPledgeAmount[$i]) }&lt;/fml:DC_MAKS_KWOTA_ZABEZPIECZONA>,
            &lt;fml:DC_KOLEJNOSC_WYKORZYSTANIA?>{ if ($kolejnosc = 0) then 0 else xs:int($kolejnosc + $i - 1) }&lt;/fml:DC_KOLEJNOSC_WYKORZYSTANIA>,
            &lt;fml:DC_ID_ZABEZPIECZENIA?>{ data($collateralItemNumber) }&lt;/fml:DC_ID_ZABEZPIECZENIA>,
            &lt;fml:DC_NR_RACHUNKU?>{ data($accountNumber) }&lt;/fml:DC_NR_RACHUNKU>,
            &lt;fml:DC_NUMER_KLIENTA?>{ data($customerNumber) }&lt;/fml:DC_NUMER_KLIENTA>,
            &lt;fml:DC_NR_PODSYSTEMU?>{ data($pledgedToApplication) }&lt;/fml:DC_NR_PODSYSTEMU>,
            &lt;fml:DC_SPOSOB_ZABEZPIECZENIA?>{ data($pledgingRule) }&lt;/fml:DC_SPOSOB_ZABEZPIECZENIA>,
            &lt;fml:DC_KURS_WYCENY_ZABEZPIECZENIA?>{ data($currencyRateCode) }&lt;/fml:DC_KURS_WYCENY_ZABEZPIECZENIA>
           )
        else
        (
            &lt;fml:DC_MAKS_KWOTA_ZABEZPIECZONA?>{ data($account_maximumPledgeAmount) }&lt;/fml:DC_MAKS_KWOTA_ZABEZPIECZONA>,
            &lt;fml:DC_KOLEJNOSC_WYKORZYSTANIA?>{ data($pledgingPriority) }&lt;/fml:DC_KOLEJNOSC_WYKORZYSTANIA>,
            &lt;fml:DC_ID_ZABEZPIECZENIA?>{ data($collateralItemNumber) }&lt;/fml:DC_ID_ZABEZPIECZENIA>,
            &lt;fml:DC_NR_RACHUNKU?> { data($accountNumber) }&lt;/fml:DC_NR_RACHUNKU>,
            &lt;fml:DC_NUMER_KLIENTA?>{ data($customerNumber) }&lt;/fml:DC_NUMER_KLIENTA>,
            &lt;fml:DC_NR_PODSYSTEMU?>{ data($pledgedToApplication) }&lt;/fml:DC_NR_PODSYSTEMU>,
            &lt;fml:DC_SPOSOB_ZABEZPIECZENIA?>{ data($pledgingRule) }&lt;/fml:DC_SPOSOB_ZABEZPIECZENIA>,
            &lt;fml:DC_KURS_WYCENY_ZABEZPIECZENIA?>{ data($currencyRateCode) }&lt;/fml:DC_KURS_WYCENY_ZABEZPIECZENIA>
        )
      }      
	 
   &lt;/fml:FML32>
	  
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>