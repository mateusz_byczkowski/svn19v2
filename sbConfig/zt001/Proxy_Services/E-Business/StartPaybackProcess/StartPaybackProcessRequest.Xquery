<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace op = "http://service.ws.flow.pbpolsoft.com.pl";
declare namespace tns1 = "http://bzwbk.com/services/BZWBK24flow/";
declare namespace tns3 = "http://bzwbk.com/services/BZWBK24flow/messages/";
declare namespace tns2 = "urn:pbpolsoft.com.pl";
declare namespace xsd="http://www.w3.org/2001/XMLSchema";
declare namespace soapenc = "http://schemas.xmlsoap.org/soap/encoding/";


declare function xf:transformBody ($req as element(soapenv:Body)) as element(soapenv:Body) {
	
	&lt;soapenv:Body>
	{		
			if($req/tns1:startNP2049)
			then 	&lt;op:startNP2049>&lt;application href="#id0" />&lt;/op:startNP2049>
			else 	&lt;op:startNP2049>&lt;application href="#id0" />&lt;/op:startNP2049>
			
	}
        &lt;multiRef id="id0" xsi:type="tns2:PaybackApplicationBean" xmlns:tns2="urn:pbpolsoft.com.pl">
        {
                if ($req/*/tns3:crwId)
			then &lt;crwId xsi:type="soapenc:string">{data($req/*/tns3:crwId)}&lt;/crwId>
			else()
        }
        &lt;/multiRef>
	
	
	&lt;/soapenv:Body>
};

	declare variable $body as element(soapenv:Body) external;
	xf:transformBody($body)</con:xquery>
</con:xqueryEntry>