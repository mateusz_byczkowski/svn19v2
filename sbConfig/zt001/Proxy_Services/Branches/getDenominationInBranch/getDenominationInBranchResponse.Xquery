<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @author  Tomasz Krajewski
 : @version 1.2
 : @since   2010-08-13
 :
 : wersja WSDLa: 12-11-2009 11:07:43
 :
 : $Proxy Services/Branches/getDenominationInBranch/getDenominationInBranchResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getDenominationInBranch/getDenominationInBranchResponse/";
declare namespace ns0 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns2 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";
declare namespace ns6 = "";

declare variable $fML321 as element(ns6:FML32) external;

(:~
 : @param $fML321 - bufor FML/XML
 :
 : @return invokeResponse - operacja wyjściowa
 :)
declare function xf:getDenominationInBranchResponse($fML321 as element(ns6:FML32))
    as element(ns5:invokeResponse)
{
	&lt;ns5:invokeResponse>
        &lt;ns5:totalDenomSpec>
			&lt;ns1:DenominationSpecification?>
				&lt;ns1:itemsNumber?>{
				(: v1.2 Zmiana pola NF_DENOMS_ITEMSNUMBER na NF_COLLAT_NUMBEROFUNITS :)
					data($fML321/ns6:NF_COLLAT_NUMBEROFUNITS[1])
				}&lt;/ns1:itemsNumber>
				
				&lt;ns1:denomination?>
					&lt;ns0:DenominationDefinition?>
						&lt;ns0:denominationID?>{
							data($fML321/ns6:NF_DENOMD_DENOMINATIONID[1])
						}&lt;/ns0:denominationID>
					&lt;/ns0:DenominationDefinition>
				&lt;/ns1:denomination>
			&lt;/ns1:DenominationSpecification>
		&lt;/ns5:totalDenomSpec>
		
		&lt;ns5:denominationSpecificationList>{
			(: v1.2 Zmiana pola NF_DENOMS_ITEMSNUMBER na NF_COLLAT_NUMBEROFUNITS :)
			for $i in 2 to count($fML321/ns6:NF_COLLAT_NUMBEROFUNITS)
		    return
				&lt;ns1:DenominationSpecification>
                    &lt;ns1:itemsNumber>{
						data($fML321/ns6:NF_COLLAT_NUMBEROFUNITS[$i])
					}&lt;/ns1:itemsNumber>
					
                    &lt;ns1:currencyCash>
                        &lt;ns1:CurrencyCash>

                            &lt;ns1:userTxnSession>
                                &lt;ns3:UserTxnSession>
									(:
									 : status oddziału
									 :
									 : 0 --> C (zamknięty)
									 : 1 --> O (otwarty)
									 :)
		                        	{
		                        		let $sessionStatus := data($fML321/ns6:NF_USETSS_USERTXNSESSIONST[$i])
		                        		return
		                        			if ($sessionStatus) then
		                        				&lt;ns3:sessionStatus>
													&lt;ns2:UserTxnSessionStatus>
														&lt;ns2:userTxnSessionStatus>{
															if ($sessionStatus eq '1') then
																'O'
															else
																'C'
														}&lt;/ns2:userTxnSessionStatus>
													&lt;/ns2:UserTxnSessionStatus>
												&lt;/ns3:sessionStatus>
		                        			else
												()
		                        	}
                                    
                                    &lt;ns3:user>
                                        &lt;ns4:User>
                                            &lt;ns4:userLastName>{
												data($fML321/ns6:NF_USER_USERLASTNAME[$i])
											}&lt;/ns4:userLastName>
											
                                            &lt;ns4:userID>{
												data($fML321/ns6:NF_USER_USERID[$i])
											}&lt;/ns4:userID>
											
                                            &lt;ns4:userFirstName>{
												data($fML321/ns6:NF_USER_USERFIRSTNAME[$i])
											}&lt;/ns4:userFirstName>
                                        &lt;/ns4:User>
                                    &lt;/ns3:user>
                                    
                                    &lt;ns3:till>
                                        &lt;ns3:Till>
                                            &lt;ns3:tillID>{
												data($fML321/ns6:NF_TILL_TILLID[$i])
											}&lt;/ns3:tillID>
                                        &lt;/ns3:Till>
                                    &lt;/ns3:till>
                                    
                                    &lt;ns3:teller>
                                        &lt;ns3:Teller>
                                            &lt;ns3:tellerID>{
												data($fML321/ns6:NF_TELLER_TELLERID[$i])
											}&lt;/ns3:tellerID>
                                        &lt;/ns3:Teller>
                                    &lt;/ns3:teller>

                                &lt;/ns3:UserTxnSession>
                            &lt;/ns1:userTxnSession>
                        &lt;/ns1:CurrencyCash>
                    &lt;/ns1:currencyCash>
                &lt;/ns1:DenominationSpecification>
                
		}&lt;/ns5:denominationSpecificationList>
	&lt;/ns5:invokeResponse>
};

&lt;soap-env:Body>{
	xf:getDenominationInBranchResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>