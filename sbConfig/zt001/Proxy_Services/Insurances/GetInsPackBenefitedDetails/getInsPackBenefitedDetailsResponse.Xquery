<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:insurance.entities.be.dcl";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://bzwbk.com/services/mappings/";

declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns1:invokeResponse>
  &lt;ns1:insurancePackBenefitedOut>
    &lt;ns0:InsPackBenefited>
      &lt;ns0:name?>{data($parm/NF_INSPB_NAME)}&lt;/ns0:name>
      &lt;ns0:firstName?>{data($parm/NF_INSPB_FIRSTNAME)}&lt;/ns0:firstName>
      &lt;ns0:secondName?>{data($parm/NF_INSPB_SECONDNAME)}&lt;/ns0:secondName>
      &lt;ns0:street?>{data($parm/NF_INSPB_STREET)}&lt;/ns0:street>
      &lt;ns0:streetNum?>{data($parm/NF_INSPB_STREETNUM)}&lt;/ns0:streetNum>
      &lt;ns0:zipCode?>{data($parm/NF_INSPB_ZIPCODE)}&lt;/ns0:zipCode>
      &lt;ns0:city?>{data($parm/NF_INSPB_CITY)}&lt;/ns0:city>

      {if (data($parm/NF_INSPB_BIRTHDATE) and xf:isData(data($parm/NF_INSPB_BIRTHDATE)))
          then &lt;ns0:birthDate>{data($parm/NF_INSPB_BIRTHDATE)}&lt;/ns0:birthDate>
          else ()
      }

      &lt;ns0:pesel?>{data($parm/NF_INSPB_PESEL)}&lt;/ns0:pesel>
      &lt;ns0:benefitPercent?>{data($parm/NF_INSPB_BENEFITPERCENT)}&lt;/ns0:benefitPercent>
    &lt;/ns0:InsPackBenefited>
  &lt;/ns1:insurancePackBenefitedOut>
&lt;/ns1:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>