<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-08</con:description>
  <con:xquery>declare namespace crw = "http://bzwbk.com/crw/services/quickwin/";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn = "urn:be.services.dcl";

declare namespace urn1 = "urn:accounts.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:productstree.entities.be.dcl";
declare namespace urn4 = "urn:insurance.entities.be.dcl";
declare namespace urn5 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace urn6 = "urn:cif.entities.be.dcl";
declare namespace urn7 = "urn:filtersandmessages.entities.be.dcl";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare variable $body as element(soap:Body) external;


declare function local:datetime2date($x as xs:dateTime) as xs:string {
	let $m := 
		if (fn:month-from-dateTime($x) &lt; 10) then
			fn:concat('0', xs:string(fn:month-from-dateTime($x)))
		else
			xs:string(fn:month-from-dateTime($x))

	let $d := 
		if (fn:day-from-dateTime($x) &lt; 10) then
			fn:concat('0', xs:string(fn:day-from-dateTime($x)))
		else
			xs:string(fn:day-from-dateTime($x))

	return fn:concat(xs:string(fn:year-from-dateTime($x)), '-', $m, '-', $d)
};



declare function local:readApplicationResponse($resp as element(crw:readApplicationResponse))
    as element(urn:invokeResponse) {
        &lt;urn:invokeResponse>
         &lt;urn:linkedAccount>
            &lt;urn1:Account>
               &lt;urn1:accountNumber?>{ data($resp/application/linkedAccount/accountNumber) }&lt;/urn1:accountNumber>
               &lt;urn1:accountDescription?>{ data($resp/application/linkedAccount/accountDescription) }&lt;/urn1:accountDescription>
               &lt;urn1:loanAccount?>
                  &lt;urn1:LoanAccount?>
                     &lt;urn1:faceAmount?>{ data($resp/application/linkedAccount/faceAmount) }&lt;/urn1:faceAmount>
                  &lt;/urn1:LoanAccount>
               &lt;/urn1:loanAccount>
               &lt;urn1:tranAccount?>
                  &lt;urn1:TranAccount?>
                     &lt;urn1:totalLimitAmount?>{ data($resp/application/linkedAccount/totalLimitAmount) }&lt;/urn1:totalLimitAmount>
                  &lt;/urn1:TranAccount>
               &lt;/urn1:tranAccount>
               &lt;urn1:currency?>
                  &lt;urn2:CurrencyCode?>
                     &lt;urn2:currencyCode?>{ data($resp/application/linkedAccount/currency) }&lt;/urn2:currencyCode>
                  &lt;/urn2:CurrencyCode>
               &lt;/urn1:currency>
               &lt;urn1:productDefinition?>
                  &lt;urn3:ProductDefinition?>
                     &lt;urn3:idProductDefinition?>{ data($resp/application/linkedAccount/idProductDefinition) }&lt;/urn3:idProductDefinition>
                  &lt;/urn3:ProductDefinition>
               &lt;/urn1:productDefinition>
            &lt;/urn1:Account>
         &lt;/urn:linkedAccount>


         &lt;urn:insurancePolicyAcc>
            &lt;urn4:InsurancePolicyAcc>
				{ if (data($resp/application/actualDate) ne '') then
               &lt;urn4:actualDate?>{ local:datetime2date(data($resp/application/actualDate)) }&lt;/urn4:actualDate>
			   else ''
				}
				{ if (data($resp/application/startDate) ne '') then
               &lt;urn4:startDate?>{ local:datetime2date(data($resp/application/startDate)) }&lt;/urn4:startDate>
			   else ''
				}
				{ if (data($resp/application/validityDate) ne '') then
               &lt;urn4:validityDate?>{ local:datetime2date(data($resp/application/validityDate)) }&lt;/urn4:validityDate>
			   else ''
				}				
               &lt;urn4:fixedPremium?>{ data($resp/application/fixedPremium) }&lt;/urn4:fixedPremium>
				{ if (data($resp/application/premiumDueDate) ne '') then
               &lt;urn4:premiumDueDate?>{ local:datetime2date(data($resp/application/premiumDueDate)) }&lt;/urn4:premiumDueDate>
			   else ''
				}
               &lt;urn4:premiumFrequency?>{ data($resp/application/premiumFrequency) }&lt;/urn4:premiumFrequency>
               &lt;urn4:externalPolices?>{ data($resp/application/externalPolices) }&lt;/urn4:externalPolices>
               &lt;urn4:saleEmployeeID?>{ data($resp/application/saleEmployeeID) }&lt;/urn4:saleEmployeeID>
               &lt;urn4:insuranceDescription?>{ data($resp/application/insuranceDescription) }&lt;/urn4:insuranceDescription>
               &lt;urn4:icbsProductNumber?>{ data($resp/application/icbsProductNumber) }&lt;/urn4:icbsProductNumber>
				&lt;urn4:accountRelationshipList>
			   {for $role in $resp/application/customerRoles
				return
					&lt;urn6:AccountRelationship>
					   &lt;urn6:customer>
						  &lt;urn6:Customer>
							 &lt;urn6:companyName?>{ data($role/customer/companyName) }&lt;/urn6:companyName>
							 &lt;urn6:customerNumber?>{ data($role/customer/customerNumber) }&lt;/urn6:customerNumber>
							 &lt;urn6:documentList>
								&lt;urn6:Document>
								   &lt;urn6:documentNumber?>{ data($role/customer/documentNumber) }&lt;/urn6:documentNumber>
								   &lt;urn6:documentType>
									  &lt;urn2:CustomerDocumentType>
										 &lt;urn2:customerDocumentType?>{ data($role/customer/documentType) }&lt;/urn2:customerDocumentType>
									  &lt;/urn2:CustomerDocumentType>
								   &lt;/urn6:documentType>
								&lt;/urn6:Document>
							 &lt;/urn6:documentList>
							 &lt;urn6:customerPersonal>
								&lt;urn6:CustomerPersonal>
								   &lt;urn6:lastName?>{ data($role/customer/lastName) }&lt;/urn6:lastName>
								   &lt;urn6:firstName?>{ data($role/customer/firstName) }&lt;/urn6:firstName>
								&lt;/urn6:CustomerPersonal>
							 &lt;/urn6:customerPersonal>
						  &lt;/urn6:Customer>
					   &lt;/urn6:customer>
					   &lt;urn6:relationship>
						  &lt;urn2:CustomerAccountRelationship>
							 &lt;urn2:customerAccountRelationship?>{ data($role/relationship) }&lt;/urn2:customerAccountRelationship>
						  &lt;/urn2:CustomerAccountRelationship>
					   &lt;/urn6:relationship>
					&lt;/urn6:AccountRelationship>
				}
				 &lt;/urn4:accountRelationshipList>
				 &lt;urn4:additionalInfoList?>
				 {for $info in $resp/application/additionalInfos
				return
						&lt;urn7:AdditionalInfo?>
						   &lt;urn7:value?>{ data($info/value) }&lt;/urn7:value>
						   &lt;urn7:additionalInfoDefinition?>
							  &lt;urn2:AdditionalInfoDefinition?>
								 &lt;urn2:additionalInfoDefinition?>{ data($info/definition) }&lt;/urn2:additionalInfoDefinition>
							  &lt;/urn2:AdditionalInfoDefinition>
						   &lt;/urn7:additionalInfoDefinition>
						&lt;/urn7:AdditionalInfo>
				}
				&lt;/urn4:additionalInfoList>

		
               &lt;urn4:currency>
                  &lt;urn2:CurrencyCode>
                     &lt;urn2:currencyCode?>{ data($resp/application/currency) }&lt;/urn2:currencyCode>
                  &lt;/urn2:CurrencyCode>
               &lt;/urn4:currency>
               &lt;urn4:premiumPeriod>
                  &lt;urn2:Period>
                     &lt;urn2:period?>{ data($resp/application/premiumPeriod) }&lt;/urn2:period>
                  &lt;/urn2:Period>
               &lt;/urn4:premiumPeriod>
               &lt;urn4:oddPremiumDay>
                  &lt;urn2:SpecialDay>
                     &lt;urn2:specialDay?>{ data($resp/application/oddPremiumDay) }&lt;/urn2:specialDay>
                  &lt;/urn2:SpecialDay>
               &lt;/urn4:oddPremiumDay>
				&lt;urn4:insPolicyApplication>
					&lt;urn4:InsPolicyApplication>
						   &lt;urn4:applicationNumber?>{ data($resp/application/number) }&lt;/urn4:applicationNumber>
						   &lt;urn4:owuSignature?>{ data($resp/application/owuSignature) }&lt;/urn4:owuSignature>
						   &lt;urn4:loanApplicationNum?>{ data($resp/application/loanApplicationNum) }&lt;/urn4:loanApplicationNum>
						   &lt;urn4:applicationStatus?>
							  &lt;urn5:InsPolicyAppStatus?>
								 &lt;urn5:insPolicyAppStatus?>{ data($resp/application/applicationStatus) }&lt;/urn5:insPolicyAppStatus>
							  &lt;/urn5:InsPolicyAppStatus>
						   &lt;/urn4:applicationStatus>
					&lt;/urn4:InsPolicyApplication>
				&lt;/urn4:insPolicyApplication>
            &lt;/urn4:InsurancePolicyAcc>
         &lt;/urn:insurancePolicyAcc>

         &lt;urn:maintenanceAccount>
            &lt;urn1:Account>
               &lt;urn1:accountNumber?>{ data($resp/application/maintenanceAccount/accountNumber) }&lt;/urn1:accountNumber>
               &lt;urn1:accountDescription?>{ data($resp/application/maintenanceAccount/accountDescription) }&lt;/urn1:accountDescription>
               &lt;urn1:currency?>
                  &lt;urn2:CurrencyCode?>
                     &lt;urn2:currencyCode?>{ data($resp/application/maintenanceAccount/currency) }&lt;/urn2:currencyCode>
                  &lt;/urn2:CurrencyCode>
               &lt;/urn1:currency>
               &lt;urn1:productDefinition?>
                  &lt;urn3:ProductDefinition?>
                     &lt;urn3:idProductDefinition?>{ data($resp/application/maintenanceAccount/idProductDefinition) }&lt;/urn3:idProductDefinition>
                  &lt;/urn3:ProductDefinition>
               &lt;/urn1:productDefinition>
            &lt;/urn1:Account>
         &lt;/urn:maintenanceAccount>

        &lt;/urn:invokeResponse>
};



declare function local:createApplicationResponse($resp as element(crw:createApplicationResponse))
    as element(urn:invokeResponse) {
        &lt;urn:invokeResponse>
			&lt;urn:insPolicyApplication>
				&lt;urn4:InsPolicyApplication>
					&lt;urn4:applicationNumber>{ data($resp/number) }&lt;/urn4:applicationNumber>
				&lt;/urn4:InsPolicyApplication>
			&lt;/urn:insPolicyApplication>
		&lt;/urn:invokeResponse>
};


declare function local:emptyResponse() as element(urn:invokeResponse) {
	&lt;urn:invokeResponse>
	&lt;/urn:invokeResponse>
};

declare function local:faultMapping($fault as element(soap:Fault)) as element(soap:Fault) {
      &lt;soap:Fault>
         &lt;faultcode>{ data($fault/faultcode) }&lt;/faultcode>
         &lt;faultstring>{ data($fault/faultstring) }&lt;/faultstring>
         &lt;detail>
            &lt;err:ServiceFailException xmlns:err="urn:errors.hlbsentities.be.dcl">
               &lt;err:exceptionItem>
                  &lt;err:errorCode1>0&lt;/err:errorCode1>
                  &lt;err:errorCode2>0&lt;/err:errorCode2>
                  &lt;err:errorDescription>{ data($fault/faultstring) }&lt;/err:errorDescription>
               &lt;/err:exceptionItem>
            &lt;/err:ServiceFailException>
         &lt;/detail>
      &lt;/soap:Fault>
};



declare function local:unknownResponse() as element(soap:Fault) {
      &lt;soap:Fault>
         &lt;faultcode>UNKNOWN&lt;/faultcode>
         &lt;faultstring>Nieznany błąd systemowy na ALSB&lt;/faultstring>
         &lt;detail>
            &lt;err:ServiceFailException xmlns:err="urn:errors.hlbsentities.be.dcl">
               &lt;err:exceptionItem>
                  &lt;err:errorCode1>0&lt;/err:errorCode1>
                  &lt;err:errorCode2>0&lt;/err:errorCode2>
                  &lt;err:errorDescription>Nieznany błąd systemowy na ALSB&lt;/err:errorDescription>
               &lt;/err:exceptionItem>
            &lt;/err:ServiceFailException>
         &lt;/detail>
      &lt;/soap:Fault>
};



typeswitch($body/*)
	case $resp as element(crw:readApplicationResponse) return local:readApplicationResponse($resp)
	case $resp as element(crw:createApplicationResponse) return local:createApplicationResponse($resp)
	case $resp as element(crw:updateApplicationResponse ) return local:emptyResponse()
	case $resp as element(crw:updateApplicationNotNullResponse ) return local:emptyResponse()
	case $resp as element(soap:Fault) return local:faultMapping($resp)
	default $resp return local:unknownResponse()</con:xquery>
</con:xqueryEntry>