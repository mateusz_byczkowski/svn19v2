<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace ns0="urn:insurancedict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:insurance.entities.be.dcl";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string
{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse>
  &lt;ns5:insurancePolicyAccOut>
    &lt;ns4:InsurancePolicyAcc>
      &lt;ns4:policyRefNum?>{data($parm/NF_INSPAC_POLICYREFNUM)}&lt;/ns4:policyRefNum>
      &lt;ns4:policyNum?>{data($parm/NF_INSPAC_POLICYNUM)}&lt;/ns4:policyNum>
      {insertDate(data($parm/NF_INSPAC_PREMIUMDUEDATE),"yyyy-MM-dd","ns4:premiumDueDate")}
      &lt;ns4:premiumFrequency?>{data($parm/NF_INSPAC_PREMIUMFREQUENCY)}&lt;/ns4:premiumFrequency>
      {
       if(string-length(data($parm/NF_INSPAC_MANDATORYFLAG)) >0)
       then  
         &lt;ns4:mandatoryFlag?>{sourceValue2Boolean(data($parm/NF_INSPAC_MANDATORYFLAG),"1")}&lt;/ns4:mandatoryFlag> 
       else() 
      }
      &lt;ns4:premiumPeriod>
        &lt;ns2:Period>
          &lt;ns2:period?>{data($parm/NF_PERIOD_PERIOD)}&lt;/ns2:period>
        &lt;/ns2:Period>
      &lt;/ns4:premiumPeriod>
      &lt;ns4:oddPremiumDay>
        &lt;ns2:SpecialDay>
          &lt;ns2:specialDay?>{data($parm/NF_SPECID_SPECIALDAY)}&lt;/ns2:specialDay>
        &lt;/ns2:SpecialDay>
      &lt;/ns4:oddPremiumDay>
      &lt;ns4:closingMethod>
        &lt;ns0:InsuranceClosingMethod>
          {
           if(string-length(data($parm/NF_INSCME_INSURANCECLOSING))>0)
           then
             &lt;ns0:insuranceClosingMethod?>{data($parm/NF_INSCME_INSURANCECLOSING)}&lt;/ns0:insuranceClosingMethod>
           else
             &lt;ns0:insuranceClosingMethod?>0&lt;/ns0:insuranceClosingMethod>
          }
        &lt;/ns0:InsuranceClosingMethod>
      &lt;/ns4:closingMethod>
    &lt;/ns4:InsurancePolicyAcc>
  &lt;/ns5:insurancePolicyAccOut>
&lt;/ns5:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>