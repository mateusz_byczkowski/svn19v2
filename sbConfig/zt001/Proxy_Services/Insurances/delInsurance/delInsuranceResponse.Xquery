<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-08</con:description>
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:insurance.entities.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  &lt;ns4:response>
    &lt;ns0:ResponseMessage>
    {
      if (data($parm/DC_OPIS_BLEDU[1])) then
      &lt;ns0:result?>false&lt;/ns0:result>
      else
       &lt;ns0:result?>true&lt;/ns0:result>
    }
    &lt;/ns0:ResponseMessage>
  &lt;/ns4:response>
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>