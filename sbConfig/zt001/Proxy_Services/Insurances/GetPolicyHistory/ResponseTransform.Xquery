<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/StreamLine/GetPolicyHistory/ResponseTransform_nowe/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns1 = "urn:baseentities.be.dcl";
declare namespace ns3 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:applicationul.entities.be.dcl";
declare namespace cl = "http://sygnity.pl/functions";

declare function xf:cupCodeConvert($input as xs:string, $type as xs:string, $codes as element(cl:historyCodes)) as xs:string {
	let $translated :=
		for $e in $codes/cl:historyCode
		where $e/@cup-code = $input
		return
			if($type = 'CODE') then
				data($e/@nfe-code)
			else
				data($e/@nfe-type)
	return
		if($translated[1] != '') then
			$translated[1]
		else if($type = 'CODE') then
			'0'
		else
			''
};

declare function xf:ResponseTransform_nowe($getContractFinancialOperationListResponse1 as element(ns0:getContractFinancialOperationListResponse),
					$codes as element(cl:historyCodes))
    as element(ns4:invokeResponse) {
        &lt;ns4:invokeResponse>
        	&lt;ns4:messageHelper>
        		&lt;ns1:MessageHelper>
        		&lt;/ns1:MessageHelper>
        	&lt;/ns4:messageHelper>
            &lt;ns4:policyContract>
                &lt;ns2:PolicyContract>
                    &lt;ns2:policyID>{ xs:string( data($getContractFinancialOperationListResponse1/operations/@contract-number) ) }&lt;/ns2:policyID>
                    &lt;ns2:policyOrderHistoryListList>
                    {
                        for $operation in $getContractFinancialOperationListResponse1/operations/operation
                        return
                            &lt;ns2:PolicyOrderHistoryList>
                                &lt;ns2:orderNumber>{ xs:string( data($operation/order-no) ) }&lt;/ns2:orderNumber>
                                &lt;ns2:operationDate>{ data($operation/effective) }&lt;/ns2:operationDate>
                                {
                                	for $data in $operation/execution
                                	where fn:string-length($data) > 0
                                	return
                                		&lt;ns2:orderDate?>{ fn:substring-before( data( $operation/execution ), 'T' ) }&lt;/ns2:orderDate>
                                }
                                &lt;ns2:value>{ xs:double( data($operation/value) ) }&lt;/ns2:value>
                                &lt;ns2:operationId>{ data($operation/@operation-id) }&lt;/ns2:operationId>
                                &lt;ns2:historyCode>
                                    &lt;ns3:UlHistoryCode>
                                        &lt;ns3:ulHistoryCode>{ xs:string( data($operation/operation-type) ) }&lt;/ns3:ulHistoryCode>
                                    &lt;/ns3:UlHistoryCode>
                                &lt;/ns2:historyCode>
                                &lt;ns2:historyStatus>
                                    &lt;ns3:UlHistoryStatus>
                                        &lt;ns3:ulHistoryStatus>{ xs:string( data($operation/status) ) }&lt;/ns3:ulHistoryStatus>
                                    &lt;/ns3:UlHistoryStatus>
                                &lt;/ns2:historyStatus>
                                &lt;ns2:eventCode>
                                	&lt;ns3:UlHistoryEventCode>
                                		&lt;ns3:ulHistoryEventCode>{ xf:cupCodeConvert(data($operation/operation-type), 'CODE', $codes) }&lt;/ns3:ulHistoryEventCode>
                                	&lt;/ns3:UlHistoryEventCode>
                                &lt;/ns2:eventCode>
                                &lt;ns2:eventType>
                                	&lt;ns3:UlHistoryEventType>
                                		&lt;ns3:ulHistoryEventType>{ xf:cupCodeConvert(data($operation/operation-type), 'TYPE', $codes) }&lt;/ns3:ulHistoryEventType>
                                	&lt;/ns3:UlHistoryEventType>
                                &lt;/ns2:eventType>
                            &lt;/ns2:PolicyOrderHistoryList>
                    }
                    &lt;/ns2:policyOrderHistoryListList>
                    &lt;ns2:productCode>
                        &lt;ns3:UlParameters>
                            &lt;ns3:productId>{ xs:string( data($getContractFinancialOperationListResponse1/operations/@product-type) ) }&lt;/ns3:productId>
                        &lt;/ns3:UlParameters>
                    &lt;/ns2:productCode>
                &lt;/ns2:PolicyContract>
            &lt;/ns4:policyContract>
        &lt;/ns4:invokeResponse>
};

declare variable $getContractFinancialOperationListResponse1 as element(ns0:getContractFinancialOperationListResponse) external;
declare variable $codes as element(cl:historyCodes) external;

xf:ResponseTransform_nowe($getContractFinancialOperationListResponse1, $codes)</con:xquery>
</con:xqueryEntry>