<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:insurance.entities.be.dcl";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForBenefitedList($parm as element(fml:FML32)) as element()
{

&lt;ns1:benefitedList>
  {
    for $x at $occ in $parm/NF_INSPB_NAME
    return
    &lt;ns0:InsPackBenefited>
      &lt;ns0:name?>{data($parm/NF_INSPB_NAME[$occ])}&lt;/ns0:name>
      &lt;ns0:firstName?>{data($parm/NF_INSPB_FIRSTNAME[$occ])}&lt;/ns0:firstName>
      &lt;ns0:benefitPercent?>{data($parm/NF_INSPB_BENEFITPERCENT[$occ])}&lt;/ns0:benefitPercent>
    &lt;/ns0:InsPackBenefited>
  }
&lt;/ns1:benefitedList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns1:invokeResponse>
  &lt;ns1:bcd>
    &lt;ns5:BusinessControlData>
      &lt;ns5:pageControl>
        &lt;ns4:PageControl>
          &lt;ns4:hasNext?>{data($parm/NF_PAGEC_HASNEXT)}&lt;/ns4:hasNext>
          &lt;ns4:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns4:navigationKeyDefinition>
          &lt;ns4:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns4:navigationKeyValue>
        &lt;/ns4:PageControl>
      &lt;/ns5:pageControl>
    &lt;/ns5:BusinessControlData>
  &lt;/ns1:bcd>
  {getElementsForBenefitedList($parm)}
&lt;/ns1:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>