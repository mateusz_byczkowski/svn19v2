<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:applicationul.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/StreamLine/SaveSwitch/RequestTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns1 = "urn:baseentities.be.dcl";
declare namespace ns7 = "urn:baseauxentities.be.dcl";
declare namespace ns3 = "urn:basedictionaries.be.dcl";
declare namespace ns2 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns5 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns8 = "urn:entities.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns6:invoke), $header1 as element(ns6:header))
    as element(ns0:performContractSwitchRequest) {
        &lt;ns0:performContractSwitchRequest>
            &lt;envelope>
            	&lt;request-time>{ data($header1/ns6:msgHeader/ns6:timestamp) }&lt;/request-time>
            	&lt;request-no>{ data($header1/ns6:msgHeader/ns6:msgId) }&lt;/request-no>
            	&lt;source-code>{ data($invoke1/ns6:sourceCode/ns7:StringHolder/ns7:value) }&lt;/source-code>
            	&lt;user-id>{ data($header1/ns6:msgHeader/ns6:userId) }&lt;/user-id>
                &lt;branch-id>{ data($header1/ns6:msgHeader/ns6:unitId) }&lt;/branch-id>
                &lt;bar-code?>{ data($invoke1/ns6:barCodeID/ns7:StringHolder/ns7:value) }&lt;/bar-code>
            &lt;/envelope>
            &lt;save>{ data($invoke1/ns6:mode/ns7:BooleanHolder/ns7:value) }&lt;/save>
            &lt;product-type>{ data($invoke1/ns6:policyCoverage/ns4:PolicyCoverage/ns4:policyContract/ns4:PolicyContract/ns4:productCode/ns5:UlParameters/ns5:productId) }&lt;/product-type>
            &lt;contract-number>{ data($invoke1/ns6:policyCoverage/ns4:PolicyCoverage/ns4:policyContract/ns4:PolicyContract/ns4:policyID) }&lt;/contract-number>
            &lt;coverage-number>{ data($invoke1/ns6:policyCoverage/ns4:PolicyCoverage/ns4:coverageID) }&lt;/coverage-number>
            &lt;fund-from>
            {
            	let $order := $invoke1/ns6:policyCoverage/ns4:PolicyCoverage/ns4:policyContract/ns4:PolicyContract/ns4:policyOrder/ns4:PolicyOrder
            	return
	                for $PolicyOrderFundFrom in $order/ns4:policyOrderFundFromList/ns4:PolicyOrderFundFrom
	                return
	                    &lt;fund-change fund-name = "{ data($PolicyOrderFundFrom/ns4:fundID/ns5:UlFund/ns5:fundID) }">
	                        &lt;value?>{ data($PolicyOrderFundFrom/ns4:value) }&lt;/value>
	                        &lt;percentage?>{ data($PolicyOrderFundFrom/ns4:percentage) }&lt;/percentage>
	                    &lt;/fund-change>
            }
            &lt;/fund-from>
            &lt;fund-to>
            {
            	let $order := $invoke1/ns6:policyCoverage/ns4:PolicyCoverage/ns4:policyContract/ns4:PolicyContract/ns4:policyOrder/ns4:PolicyOrder
            	return
	                for $PolicyOrderFundTo in $order/ns4:policyOrderFundToList/ns4:PolicyOrderFundTo
	                return
	                    &lt;fund-change fund-name = "{ data($PolicyOrderFundTo/ns4:fundID/ns5:UlFund/ns5:fundID) }">
	                        &lt;percentage>{ xs:decimal( data($PolicyOrderFundTo/ns4:percentage) ) }&lt;/percentage>
	                    &lt;/fund-change>
            }
            &lt;/fund-to>
        &lt;/ns0:performContractSwitchRequest>
};

declare variable $invoke1 as element(ns6:invoke) external;
declare variable $header1 as element(ns6:header) external;

xf:RequestTransform($invoke1, $header1)</con:xquery>
</con:xqueryEntry>