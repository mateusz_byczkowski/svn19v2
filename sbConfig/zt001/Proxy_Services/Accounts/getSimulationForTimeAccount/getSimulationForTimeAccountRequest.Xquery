<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns8:header" location="getSimulationForTimeAccount.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns8:invoke" location="getSimulationForTimeAccount.wsdl" ::)
(:: pragma bea:global-element-return element="ns4:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)


(:~
 :
 : @author  Grzegorz Strawa
 : @version 1.0
 : @since   2010-03-11
 :
 : wersja WSDLa: 03-12-2009 17:04:43
 :
 : $Proxy Services/Account/getSimulationForTimeAccount/getSimulationForTimeAccountRequest.xq$
 :
 :)

declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/getSimulationForTimeAccount/getSimulationForTimeAccountRequest/";
declare namespace ns0 = "urn:accounts.entities.be.dcl";
declare namespace ns1 = "urn:productstree.entities.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:be.services.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";


declare function xf:getSimulationForTimeAccountRequest($header1 as element(ns8:header),
    $invoke1 as element(ns8:invoke))
    as element(ns4:FML32) {
        &lt;ns4:FML32>
            &lt;ns4:TR_ID_OPER>{ data($header1/ns8:transHeader/ns8:transId) }&lt;/ns4:TR_ID_OPER>
            &lt;ns4:TR_DATA_OPER>{ 
            let $transactionDate := $invoke1/ns8:transaction/ns7:Transaction/ns7:transactionDate
					return
						fn:concat(
							fn:substring(data($transactionDate ), 9, 2),
							'-',
							fn:substring(data($transactionDate ), 6, 2),
							'-',
							fn:substring(data($transactionDate ), 1, 4)
							)
            }&lt;/ns4:TR_DATA_OPER>
            &lt;ns4:TR_ODDZ_KASY?>{ 
            xs:short( data($invoke1/ns8:branchCode/ns2:BranchCode/ns2:branchCode) ) 
            }&lt;/ns4:TR_ODDZ_KASY>
            
            &lt;ns4:TR_KASA?>{ 
            xs:short( data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID) ) 
            }&lt;/ns4:TR_KASA>
            
            &lt;ns4:TR_KASJER?>{ 
            xs:short( data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID) ) 
            }&lt;/ns4:TR_KASJER>
            
            &lt;ns4:TR_UZYTKOWNIK?>{ 
            fn:concat("SKP:",data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:user/ns5:User/ns5:userID) ) 
            }&lt;/ns4:TR_UZYTKOWNIK>
            
            &lt;ns4:TR_ID_GR_OPER?>{ 
            data($invoke1/ns8:transaction/ns7:Transaction/ns7:transactionGroupID) 
            }&lt;/ns4:TR_ID_GR_OPER>
            
            &lt;ns4:TR_MSG_ID?>{ 
            data($header1/ns8:msgHeader/ns8:msgId) 
            }&lt;/ns4:TR_MSG_ID>
            
    		&lt;ns4:TR_FLAGA_AKCEPT?>
    		O
    		&lt;/ns4:TR_FLAGA_AKCEPT>
    		
            &lt;ns4:TR_TYP_KOM?>{ 
            xs:short( data($invoke1/ns8:transaction/ns7:Transaction/ns7:csrMessageType/ns6:CsrMessageType/ns6:csrMessageType) ) 
            }&lt;/ns4:TR_TYP_KOM>
            
            &lt;ns4:TR_KANAL?>
            0
            &lt;/ns4:TR_KANAL>
            
        	&lt;ns4:TR_RACH_NAD?>{ 
        	data($invoke1/ns8:account/ns0:Account/ns0:accountNumber) 
        	}&lt;/ns4:TR_RACH_NAD>
        	
 		    &lt;ns4:TR_DATA_PRACY?>{ 
             let $transactionDate := $header1/ns8:msgHeader/ns8:timestamp
			return
						fn:concat(
							fn:substring(data($transactionDate ), 9, 2),
							'-',
							fn:substring(data($transactionDate ), 6, 2),
							'-',
							fn:substring(data($transactionDate ), 1, 4)
							)
            }&lt;/ns4:TR_DATA_PRACY>
            
            &lt;ns4:TR_CZAS_OPER?>{ 
             let $transactionTime:= $header1/ns8:msgHeader/ns8:timestamp
			return
				fn:replace(
				fn:substring(data($transactionTime), 12, 8)							
				 ,':','')
                   }&lt;/ns4:TR_CZAS_OPER>
                   
    		&lt;ns4:TR_AKCEPTANT_SKP?>{ 
    		data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:user/ns5:User/ns5:userID) 
    		}&lt;/ns4:TR_AKCEPTANT_SKP>
    		
         	&lt;ns4:TR_UZYTKOWNIK_SKP?>{
         	 data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:user/ns5:User/ns5:userID)  
         	 }&lt;/ns4:TR_UZYTKOWNIK_SKP>
        &lt;/ns4:FML32>

};

declare variable $header1 as element(ns8:header) external;
declare variable $invoke1 as element(ns8:invoke) external;

&lt;soap-env:Body>
{
xf:getSimulationForTimeAccountRequest($header1,
    $invoke1)
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>