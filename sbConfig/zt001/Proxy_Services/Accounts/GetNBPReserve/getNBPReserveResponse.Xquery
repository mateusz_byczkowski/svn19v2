<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:accounts.entities.be.dcl";
declare namespace e2 = "urn:accountdict.dictionaries.be.dcl";

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
   if ($value)
     then if(string-length($value)>5 and not ($value = "0001-01-01"))
         then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
     else() 
   else()
};

declare function local:mapGetNBPReserveResponse($fml as element())
    as element(m:invokeResponse) {
		&lt;m:invokeResponse>
			&lt;m:nbpReserve>
				&lt;e:NBPReserve>
                                                                                { insertDate(data($fml/NF_NBPR_DATEOFPDOSTATUS),"yyyy-MM-dd","e:dateOfPDOStatus")}
					&lt;e:currentReserve>{ data($fml/fml:NF_NBPR_CURRENTRESERVE) }&lt;/e:currentReserve>
					&lt;e:previousReserve>{ data($fml/fml:NF_NBPR_PREVIOUSRESERVE) }&lt;/e:previousReserve>
					&lt;e:provMadeYTD_T>{ data($fml/fml:NF_NBPR_PROVMADEYTD_T) }&lt;/e:provMadeYTD_T>
					&lt;e:provCancYTD_T>{ data($fml/fml:NF_NBPR_PROVCANCYTD_T) }&lt;/e:provCancYTD_T>
					&lt;e:provMadeYTD_N>{ data($fml/fml:NF_NBPR_PROVMADEYTD_N) }&lt;/e:provMadeYTD_N>
					&lt;e:provCancYTD_N>{ data($fml/fml:NF_NBPR_PROVCANCYTD_N) }&lt;/e:provCancYTD_N>
					&lt;e:overdrawnDays>{ data($fml/fml:NF_NBPR_OVERDRAWNDAYS) }&lt;/e:overdrawnDays>
					&lt;e:pdoStatus>
						&lt;e2:PdoStatus>
							&lt;e2:pdoStatus>{ data($fml/fml:NF_PDOS_PDOSTATUS[1]) }&lt;/e2:pdoStatus>
						&lt;/e2:PdoStatus>
					&lt;/e:pdoStatus>
					&lt;e:customerRiskCode>
						&lt;e2:PdoStatus>
							&lt;e2:pdoStatus>{ data($fml/fml:NF_PDOS_PDOSTATUS[2]) }&lt;/e2:pdoStatus>
						&lt;/e2:PdoStatus>
					&lt;/e:customerRiskCode>
					&lt;e:provisionCalculationFlag>
						&lt;e2:ProvisionCalculationFlag>
							&lt;e2:provisionCalculationFlag>{ data($fml/fml:NF_PROVCF_PROVISIONCALCULA) }&lt;/e2:provisionCalculationFlag>
						&lt;/e2:ProvisionCalculationFlag>
					&lt;/e:provisionCalculationFlag>
				&lt;/e:NBPReserve>
			&lt;/m:nbpReserve>
		&lt;/m:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ local:mapGetNBPReserveResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>