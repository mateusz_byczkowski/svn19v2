<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace tem = "http://tempuri.org/";
declare namespace wcf="http://schemas.datacontract.org/2004/07/WcfServiceLibrary.Struktures";


declare function xf:map_manCustProdAddrRequest($fml as element(fml:FML32))
    as element(tem:manageCardProductAdr) {
        &lt;tem:manageCardProductAdr>
            &lt;tem:input>
                &lt;wcf:trnId>{ data($fml/fml:DC_TRN_ID) }&lt;/wcf:trnId>
                &lt;wcf:uzytkownik>{ data($fml/fml:DC_UZYTKOWNIK) }&lt;/wcf:uzytkownik>
                &lt;wcf:oddzial>{ data($fml/fml:DC_ODDZIAL) }&lt;/wcf:oddzial>
                &lt;wcf:typZmiany>{ data($fml/fml:DC_TYP_ZMIANY) }&lt;/wcf:typZmiany>
                {
                    if($fml/fml:DC_NR_RACHUNKU)
                    then    &lt;wcf:nrRachunku>{ data($fml/fml:DC_NR_RACHUNKU) }&lt;/wcf:nrRachunku>
                    else   
                    (            
                        if(($fml/fml:DC_NUMER_KLIENTA) and ($fml/fml:DC_TYP_ZMIANY='D'))
                        then    &lt;wcf:numerKlienta>{ data($fml/fml:DC_NUMER_KLIENTA) }&lt;/wcf:numerKlienta>
                        else()
                    )                   
                }
                {
                    if($fml/fml:DC_IMIE_I_NAZWISKO_ALT)
                    then    &lt;wcf:imieINazwiskoAlt>{ data($fml/fml:DC_IMIE_I_NAZWISKO_ALT) }&lt;/wcf:imieINazwiskoAlt>
                    else()
                }
                {
                    if($fml/fml:DC_ULICA_ADRES_ALT)
                    then    &lt;wcf:UlicaAdresAlt>{ data($fml/fml:DC_ULICA_ADRES_ALT) }&lt;/wcf:UlicaAdresAlt>
                    else()
                }
                {
                    if($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT)
                    then    &lt;wcf:NrPosesLokaluAdresAlt>{ data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT) }&lt;/wcf:NrPosesLokaluAdresAlt>
                    else()
                }
                {
                    if($fml/fml:DC_MIASTO_ADRES_ALT)
                    then    &lt;wcf:miastAdresAlt>{ data($fml/fml:DC_MIASTO_ADRES_ALT) }&lt;/wcf:miastAdresAlt>
                    else()
                }
                {
                    if($fml/fml:CI_KOD_KRAJU_KORESP)
                    then    &lt;wcf:wojewodztwoKrajAdresAlt>{ data($fml/fml:CI_KOD_KRAJU_KORESP) }&lt;/wcf:wojewodztwoKrajAdresAlt>
                    else()
                }
                {
                    if($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT)
                    then    &lt;wcf:kodPocztowyAdresAlt>{ data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT) }&lt;/wcf:kodPocztowyAdresAlt>
                    else()
                }
                {
                    if($fml/fml:DC_DATA_WPROWADZENIA)
                    then    &lt;wcf:dataWprowadzenia>{ data($fml/fml:DC_DATA_WPROWADZENIA) }&lt;/wcf:dataWprowadzenia>
                    else()
                }
                {
                    if($fml/fml:DC_DATA_KONCOWA)
                    then    &lt;wcf:dataKoncowa>{ data($fml/fml:DC_DATA_KONCOWA) }&lt;/wcf:dataKoncowa>
                    else()
                }
                {
                    if($fml/fml:DC_KASOWAC_PRZY_WYGASNIECIU)
                    then    &lt;wcf:kasowacPrzyWygasnieciu>{ data($fml/fml:DC_KASOWAC_PRZY_WYGASNIECIU) }&lt;/wcf:kasowacPrzyWygasnieciu>
                    else()
                }
            &lt;/tem:input>
        &lt;/tem:manageCardProductAdr>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_manCustProdAddrRequest($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>