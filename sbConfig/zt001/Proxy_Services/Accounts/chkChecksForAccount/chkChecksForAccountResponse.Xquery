<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.4
 : @since   2009-12-14
 :
 : wersja WSDLa: 30-07-2009 16:08:17
 :
 : $Proxy Services/Accounts/chkChecksForAccount/chkChecksForAccountResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/chkChecksForAccount/chkChecksForAccountResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:~
 : @param $fML321 - bufor XML/FML
 :
 : @return invokeRespone - operacja wyjściowa
 :)
declare function xf:chkChecksForAccountResponse($fML321 as element(ns1:FML32))
    as element(ns2:invokeResponse)
{
	&lt;ns2:invokeResponse>
		&lt;ns2:response>
			&lt;ns0:ResponseMessage>{
				let $result := data($fML321/ns1:NF_RESPOM_RESULT)
				return
					if ($result) then
						&lt;ns0:result>{
							xs:boolean($result)
						}&lt;/ns0:result>
					else
						()
			}&lt;/ns0:ResponseMessage>
		&lt;/ns2:response>
	&lt;/ns2:invokeResponse>
};

&lt;soap-env:Body>{
	xf:chkChecksForAccountResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>