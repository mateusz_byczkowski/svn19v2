<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="postCancelDirectDebit.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="postCancelDirectDebit.wsdl" ::)
(:: pragma bea:global-element-return element="ns5:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:acceptance.entities.be.dcl";
declare namespace ns3 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns5 = "";
declare namespace ns6 = "urn:operations.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/postCancelDirectDebit/postCancelDirectDebit/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns8 = "urn:entities.be.dcl";

declare function local:dataToSV($data as xs:string) as xs:string
{
let $data:=$data
return
   if ($data) then
fn:concat(fn:substring($data, 9, 2),'-',fn:substring($data, 6, 2),'-',fn:substring($data, 1, 4))
else("")

};



declare function xf:postCancelDirectDebit($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke))
    as element(ns5:FML32) {
        &lt;ns5:FML32>

            &lt;ns5:TR_ID_OPER?>{ 
            data($header1/ns0:transHeader/ns0:transId) 
            }&lt;/ns5:TR_ID_OPER>
             &lt;ns5:TR_DATA_OPER?>{local:dataToSV(xs:string( data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionDate)))}&lt;/ns5:TR_DATA_OPER>
             
 (:           &lt;ns5:TR_DATA_OPER?> :)
  (:              { :)
   (:                 let $transactionDate  := ($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionDate) :)  
    (:                return :)
     (:                   (fn:concat( :)
      (:                  fn:substring(data($transactionDate), 9, 2), :)
       (:                 '-', :)
        (:                fn:substring(data($transactionDate ), 6, 2), :)
         (:               '-', :)
          (:              fn:substring(data($transactionDate ), 1, 4) :)
           (:             )):)
            (:    } :)
			(:	&lt;/ns5:TR_DATA_OPER> :)

            &lt;ns5:TR_ODDZ_KASY?>{ 
            xs:short( data($invoke1/ns0:branchCode/ns2:BranchCode/ns2:branchCode) ) 
            }&lt;/ns5:TR_ODDZ_KASY>
            
            &lt;ns5:TR_KASA?>{ 
            xs:short( data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:till/ns1:Till/ns1:tillID) ) 
            }&lt;/ns5:TR_KASA>
            
            &lt;ns5:TR_KASJER?>{ 
            xs:short( data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:teller/ns1:Teller/ns1:tellerID) ) 
            }&lt;/ns5:TR_KASJER>
            
            &lt;ns5:TR_UZYTKOWNIK?>{ 
            data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns8:User/ns8:userID) 
            }&lt;/ns5:TR_UZYTKOWNIK>
            
            &lt;ns5:TR_ID_GR_OPER?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:cashTransactionBasketID) 
            }&lt;/ns5:TR_ID_GR_OPER>
            
            &lt;ns5:TR_MSG_ID?>{ 
            data($header1/ns0:msgHeader/ns0:msgId) 
            }&lt;/ns5:TR_MSG_ID>
            
            
            &lt;ns5:TR_FLAGA_AKCEPT?>
                {
                    if (fn:boolean(data($invoke1/ns0:acceptanceForBE/ns6:AcceptanceForBE/ns6:flag)) = fn:boolean("true")) then
                        (data($invoke1/ns0:acceptanceForBE/ns6:AcceptanceForBE/ns6:flag))
                    else 
                        'T'
                }
			&lt;/ns5:TR_FLAGA_AKCEPT>
            
            &lt;ns5:TR_AKCEPTANT?>{
            data($invoke1/ns0:acceptTask/ns4:AcceptTask/ns4:acceptor) 
            }&lt;/ns5:TR_AKCEPTANT>
            
            &lt;ns5:TR_TYP_KOM?>{ 
            xs:short( data($invoke1/ns0:transaction/ns6:Transaction/ns6:csrMessageType/ns3:CsrMessageType/ns3:csrMessageType) ) 
            }&lt;/ns5:TR_TYP_KOM>
            
            &lt;ns5:TR_KANAL?>{ 
            xs:short( data($invoke1/ns0:transaction/ns6:Transaction/ns6:extendedCSRMessageType/ns3:ExtendedCSRMessageType/ns3:extendedCSRMessageType) ) 
            }&lt;/ns5:TR_KANAL>
            
              &lt;ns5:TR_CZAS_OPER?>
                {
                    let $transactionTime  := ($header1/ns0:msgHeader/ns0:timestamp)
  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
				&lt;/ns5:TR_CZAS_OPER>
 

            &lt;ns5:TR_RACH_NAD?>{ 
             data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:accountNumber)
             }&lt;/ns5:TR_RACH_NAD>
             
            &lt;ns5:TR_KWOTA_NAD?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:amountWn) 
            }&lt;/ns5:TR_KWOTA_NAD>
             
            &lt;ns5:TR_WALUTA_NAD?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:currencyCode/ns2:CurrencyCode/ns2:currencyCode)
            }&lt;/ns5:TR_WALUTA_NAD>
            
            &lt;ns5:TR_KURS_NAD?>{
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:rate) 
            }&lt;/ns5:TR_KURS_NAD>
            
            
            &lt;ns5:TR_NAZWA_NAD?>{
            fn-bea:trim-right( concat(data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:name)," ",data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:nameSecond)))
            }&lt;/ns5:TR_NAZWA_NAD>
            
            &lt;ns5:TR_MIEJSCOWOSC_NAD?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:city) 
            }&lt;/ns5:TR_MIEJSCOWOSC_NAD>
            
			&lt;ns5:TR_ULICA_NAD?>{ 
			data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:address)  
			}&lt;/ns5:TR_ULICA_NAD>
                        			
            &lt;ns5:TR_KOD_POCZT_NAD?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:zipCode)  
            }&lt;/ns5:TR_KOD_POCZT_NAD>
            
            &lt;ns5:TR_ROWN_PLN_NAD?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:amountWnEquiv) 
            }&lt;/ns5:TR_ROWN_PLN_NAD>
                        
			&lt;ns5:TR_RACH_ADR?>{ 
			data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:accountNumber)  
			}&lt;/ns5:TR_RACH_ADR>
			
            &lt;ns5:TR_KWOTA_ADR?>{ 
             data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:amountMa)
             }&lt;/ns5:TR_KWOTA_ADR>
            
            &lt;ns5:TR_WALUTA_ADR?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:currencyCode/ns2:CurrencyCode/ns2:currencyCode) 
            }&lt;/ns5:TR_WALUTA_ADR>
			                        
            &lt;ns5:TR_KURS_ADR?>{  
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:rate) 
            }&lt;/ns5:TR_KURS_ADR>
            
            &lt;ns5:TR_NAZWA_ADR?>{ 
            fn-bea:trim-right( concat( data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:name)," ",data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:nameSecond) )) 
            }&lt;/ns5:TR_NAZWA_ADR>
            
            &lt;ns5:TR_MIEJSCOWOSC_ADR?>{
             data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:city)  
            }&lt;/ns5:TR_MIEJSCOWOSC_ADR>            
            
            &lt;ns5:TR_ULICA_ADR?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:address) 
            }&lt;/ns5:TR_ULICA_ADR>
            
            &lt;ns5:TR_KOD_POCZT_ADR?>{
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:zipCode)  
            }&lt;/ns5:TR_KOD_POCZT_ADR>
            
            &lt;ns5:TR_ROWN_PLN_ADR?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:amountMaEquiv)  
            }&lt;/ns5:TR_ROWN_PLN_ADR>
            
             &lt;ns5:TR_TYTUL?>{
             data($invoke1/ns0:transaction/ns6:Transaction/ns6:title)
             }&lt;/ns5:TR_TYTUL>
             
            &lt;ns5:TR_AKCEPTANT_SKP?>{
             data($invoke1/ns0:acceptTask/ns4:AcceptTask/ns4:acceptor) 
             }&lt;/ns5:TR_AKCEPTANT_SKP>
             
            &lt;ns5:TR_UZYTKOWNIK_SKP?>{ 
            data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns8:User/ns8:userID) 
            }&lt;/ns5:TR_UZYTKOWNIK_SKP>
            
            &lt;ns5:TR_NR_REF_ODWOL_PZ?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:directDebitCallOff/ns6:DirectDebitCallOff/ns6:originDDTxnReferenceNr) 
            }&lt;/ns5:TR_NR_REF_ODWOL_PZ>
            
        	&lt;ns5:TR_DATA_ODWOL_PZ?>{
        	 local:dataToSV(xs:string( data($invoke1/ns0:transaction/ns6:Transaction/ns6:directDebitCallOff/ns6:DirectDebitCallOff/ns6:originDDTxnDate) )) 
        	 }&lt;/ns5:TR_DATA_ODWOL_PZ>
        	 
        &lt;/ns5:FML32>
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;

&lt;soap-env:Body>{
xf:postCancelDirectDebit($header1,
    $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>