<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns4="urn:filtersandmessages.entities.be.dcl";
declare namespace ns6="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:response>
    &lt;ns4:ResponseMessage>
      &lt;ns4:result>true&lt;/ns4:result>
      &lt;ns4:errorCode>{data($parm/NF_RESPOM_ERRORCODE)}&lt;/ns4:errorCode>
      &lt;ns4:errorDescription>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}&lt;/ns4:errorDescription>
    &lt;/ns4:ResponseMessage>
  &lt;/ns0:response>
  &lt;ns0:relationship>
    &lt;ns2:AccountRelationship>
          {
            if($parm/NF_ACCOUR_RELATIONSHIP[1]) then
      &lt;ns2:relationship>{data($parm/NF_ACCOUR_RELATIONSHIP[1])}&lt;/ns2:relationship>
            else ()
          }
      &lt;ns2:applicationNumber>
        &lt;ns3:ApplicationNumber>
          {
            if($parm/NF_APPLIN_APPLICATIONNUMBE[1]) then
 &lt;ns3:applicationNumber>{data($parm/NF_APPLIN_APPLICATIONNUMBE[1])}&lt;/ns3:applicationNumber>
            else ()
          }
        &lt;/ns3:ApplicationNumber>
      &lt;/ns2:applicationNumber>
    &lt;/ns2:AccountRelationship>
  &lt;/ns0:relationship>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>