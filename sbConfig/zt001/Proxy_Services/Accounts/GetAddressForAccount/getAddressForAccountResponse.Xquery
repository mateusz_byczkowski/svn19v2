<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-10</con:description>
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:response>
    &lt;ns2:ResponseMessage>
      &lt;ns2:result>{data($parm/NF_RESPOM_RESULT)}&lt;/ns2:result>
      &lt;ns2:errorCode>{data($parm/NF_RESPOM_ERRORCODE)}&lt;/ns2:errorCode>
      &lt;ns2:errorDescription>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}&lt;/ns2:errorDescription>
    &lt;/ns2:ResponseMessage>
  &lt;/ns0:response>
  &lt;ns0:accountAddress>
    &lt;ns3:AccountAddress>
      &lt;ns3:name1>{data($parm/NF_ACCOUA_NAME1)}&lt;/ns3:name1>
      &lt;ns3:name2>{data($parm/NF_ACCOUA_NAME2)}&lt;/ns3:name2>
      &lt;ns3:street>{data($parm/NF_ACCOUA_STREET)}&lt;/ns3:street>
      &lt;ns3:houseFlatNumber>{data($parm/NF_ACCOUA_HOUSEFLATNUMBER)}&lt;/ns3:houseFlatNumber>
      &lt;ns3:city>{data($parm/NF_ACCOUA_CITY)}&lt;/ns3:city>
      &lt;ns3:stateCountry>{data($parm/NF_ACCOUA_STATECOUNTRY)}&lt;/ns3:stateCountry>
      &lt;ns3:zipCode>{data($parm/NF_ACCOUA_ZIPCODE)}&lt;/ns3:zipCode>
      &lt;ns3:accountAddressType>{data($parm/NF_ACCOUA_ACCOUNTADDRESSTY)}&lt;/ns3:accountAddressType>
      { insertDate(data($parm/NF_ACCOUA_VALIDFROM),"yyyy-MM-dd","ns3:validFrom")}
      { insertDate(data($parm/NF_ACCOUA_VALIDTO),"yyyy-MM-dd","ns3:validTo")}
	  {
      if (data($parm/NF_ACCOUA_DELETEWHENEXPIRE) = 1)
           then &lt;ns3:deleteWhenExpired>true&lt;/ns3:deleteWhenExpired>
           else &lt;ns3:deleteWhenExpired>false&lt;/ns3:deleteWhenExpired>
      }
	  (: &lt;ns3:deleteWhenExpired>{data($parm/NF_ACCOUA_DELETEWHENEXPIRE)}&lt;/ns3:deleteWhenExpired> :)
    &lt;/ns3:AccountAddress>
  &lt;/ns0:accountAddress>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>