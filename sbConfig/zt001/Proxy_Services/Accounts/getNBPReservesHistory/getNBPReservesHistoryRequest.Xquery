<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-11-18</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappgetNBPReservesHistoryRequest($req as element(m:getNBPReservesHistoryRequest))
	as element(fml:FML32) {
  &lt;fml:FML32>
  {
    if($req/m:accountNumber)
      then &lt;fml:NF_ACCOUN_ACCOUNTNUMBER>{ data($req/m:accountNumber) }&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER>
    else (),
    if($req/m:pageSize)
      then &lt;fml:NF_PAGEC_PAGESIZE>{ data($req/m:pageSize) }&lt;/fml:NF_PAGEC_PAGESIZE>
    else (),
    if($req/m:actionCode)
      then &lt;fml:NF_PAGEC_ACTIONCODE>{ data($req/m:actionCode) }&lt;/fml:NF_PAGEC_ACTIONCODE>
    else (),
    if($req/m:navigationKeyValue)
      then &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU>{ data($req/m:navigationKeyValue) }&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU>
    else (&lt;fml:NF_PAGEC_NAVIGATIONKEYVALU/>)
  }
  &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappgetNBPReservesHistoryRequest($body/m:getNBPReservesHistoryRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>