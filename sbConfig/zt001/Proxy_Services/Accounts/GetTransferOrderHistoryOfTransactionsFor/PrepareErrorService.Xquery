<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-15</con:description>
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare variable $body external;
declare variable $headerCache external;
declare namespace urn3="urn:be.services.dcl";

declare function local:fault($faultString as xs:string, $detail as xs:string) as element(soap-env:Fault) {
		&lt;soap-env:Fault>
			&lt;faultcode>soapenv:Server.userException&lt;/faultcode> 
			&lt;faultstring>{ $faultString }&lt;/faultstring> 
			&lt;detail>{ $detail }&lt;/detail>
		&lt;/soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string,
        $errorDescription as xs:string,$errorCodeFromBS as xs:string) as element(err:exceptionItem) {
   &lt;err:exceptionItem>
	&lt;err:errorCode1>{ $errorCode1 }&lt;/err:errorCode1>
	&lt;err:errorCode2>{ $errorCode2 }&lt;/err:errorCode2>
        {
           if (string-length($errorCodeFromBS)>0) 
               then   &lt;err:errorDescription>{ concat($errorCodeFromBS," : ",$errorDescription) }&lt;/err:errorDescription>
               else    &lt;err:errorDescription>{ $errorDescription }&lt;/err:errorDescription>
        }
   &lt;/err:exceptionItem>
};

&lt;soap-env:Body>
	{
		let $reason := "11"
		let $urcode := "1000"
		
		let $errorDescriptiona :=data($body/FML32/NF_ERROR_DESCRIPTION[1])
		let $errorCode:=data($body/FML32/NF_ERROR_CODE[1])
		let $errorDescriptionDC:=data($body/FML32/DC_OPIS_BLEDU[1])
                let $messageID := data($headerCache/urn3:header/urn3:msgHeader/urn3:msgId)
                           let $errorDescription := translate($errorDescriptiona, "za§˘’Ťg‘žl†ja¦¤", "zażółćgęśląjaźń")

		return
                       if (string-length($errorDescription)>0 or string-length($errorCode)>0) then
                          local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, concat($errorDescription, concat(' (',concat($messageID, ')'))),$errorCode)})
                       else if (string-length($errorDescriptionDC)>0) then
                          local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, concat($errorDescriptionDC, concat(' (',concat($messageID, ')'))),"") })
                       else()
                         }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>