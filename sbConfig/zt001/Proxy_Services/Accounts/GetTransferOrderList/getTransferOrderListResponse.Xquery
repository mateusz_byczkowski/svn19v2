<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function sourceValue2Boolean($parm as xs:string?,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32), $occ as xs:integer) as element()
{

&lt;ns2:transferOrderTargetList>
  {
    &lt;ns2:TransferOrderTarget>
      &lt;ns2:amountTarget?>{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}&lt;/ns2:amountTarget>
      &lt;ns2:codeProfile?>{data($parm/NF_TRANOT_CODEPROFILE[$occ])}&lt;/ns2:codeProfile>
      &lt;ns2:transferOrderDescription?>{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}&lt;/ns2:transferOrderDescription>
      &lt;ns2:beneficiary1?>{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}&lt;/ns2:beneficiary1>
      &lt;ns2:beneficiary2?>{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}&lt;/ns2:beneficiary2>
      &lt;ns2:referenceNumber?>{data($parm/NF_TRANOT_REFERENCENUMBER[$occ])}&lt;/ns2:referenceNumber>
      &lt;ns2:currencyCode>
        &lt;ns5:CurrencyCode>
          &lt;ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode>
        &lt;/ns5:CurrencyCode>
      &lt;/ns2:currencyCode>
    &lt;/ns2:TransferOrderTarget>
  }
&lt;/ns2:transferOrderTargetList>
};
declare function getElementsForTransferOrderAnchorOut($parm as element(fml:FML32)) as element()
{

&lt;ns0:transferOrderAnchorOut>
  {
    for $x at $occ in $parm/NF_TRANOA_TRANSFERORDERNUM
    return
    &lt;ns2:TransferOrderAnchor>
      &lt;ns2:frequency?>{data($parm/NF_TRANOA_FREQUENCY[$occ])}&lt;/ns2:frequency>
	  { insertDate(data($parm/NF_TRANOA_PAYMENTDATE[$occ]),"yyyy-MM-dd","ns2:paymentDate")}
	  { insertDate(data($parm/NF_TRANOA_EXPIRATIONDATE[$occ]),"yyyy-MM-dd","ns2:expirationDate")}
      &lt;ns2:codeProfile?>{data($parm/NF_TRANOA_CODEPROFILE[$occ])}&lt;/ns2:codeProfile>
      &lt;ns2:nonBusinessDayRule?>{sourceValue2Boolean(data($parm/NF_TRANOA_NONBUSINESSDAYRU[$occ]),"1")}&lt;/ns2:nonBusinessDayRule>
      &lt;ns2:transferOrderNumber?>{data($parm/NF_TRANOA_TRANSFERORDERNUM[$occ])}&lt;/ns2:transferOrderNumber>
      &lt;ns2:charging?>{sourceValue2Boolean(data($parm/NF_TRANOA_CHARGING[$occ]),"1")}&lt;/ns2:charging>
      &lt;ns2:active?>{sourceValue2Boolean(data($parm/NF_TRANOA_ACTIVE[$occ]),"1")}&lt;/ns2:active>
	  { insertDate(data($parm/NF_TRANOA_FIRSTPAYMENTDATE[$occ]),"yyyy-MM-dd","ns2:firstPaymentDate")}
      &lt;ns2:referenceNumber?>{data($parm/NF_TRANOA_REFERENCENUMBER[$occ])}&lt;/ns2:referenceNumber>
      {getElementsForTransferOrderTargetList($parm, $occ)}
      &lt;ns2:account>
        &lt;ns6:Account>
          &lt;ns6:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns6:accountNumber>
        &lt;/ns6:Account>
      &lt;/ns2:account>
      &lt;ns2:period>
        &lt;ns5:Period>
          &lt;ns5:period?>{data($parm/NF_PERIOD_PERIOD[$occ])}&lt;/ns5:period>
        &lt;/ns5:Period>
      &lt;/ns2:period>
    &lt;/ns2:TransferOrderAnchor>
  }
&lt;/ns0:transferOrderAnchorOut>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  {getElementsForTransferOrderAnchorOut($parm)}
  &lt;ns0:bcd>
    &lt;ns3:BusinessControlData>
      &lt;ns3:pageControl>
        &lt;ns4:PageControl>
          &lt;ns4:hasNext?>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns4:hasNext>
          &lt;ns4:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns4:navigationKeyDefinition>
          &lt;ns4:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns4:navigationKeyValue>
        &lt;/ns4:PageControl>
      &lt;/ns3:pageControl>
    &lt;/ns3:BusinessControlData>
  &lt;/ns0:bcd>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>