<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:
 : 
 : @author  Kacper Pawlaczyk
 : @version 1.2
 : @since   2010-03-11
 :
 : wersja WSDLa: 08-01-2010 13:09:21
 :
 : $Proxy Services/Accounts/addToClosingAccountsTable/addToClosingAccountsTableResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/addToClosingAccountsTable/addToClosingAccountsTableResponse/";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns1 = "";

declare variable $fML321 as element(ns1:FML32) external;

declare function xf:addToClosingAccountsTableResponse($fML321 as element(ns1:FML32))
    as element(ns0:invokeResponse)
{
	&lt;ns0:invokeResponse/>
};

&lt;soap-env:Body>{
	xf:addToClosingAccountsTableResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>