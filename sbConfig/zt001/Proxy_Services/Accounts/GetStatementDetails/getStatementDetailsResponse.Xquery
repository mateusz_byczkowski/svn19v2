<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:operations.entities.be.dcl";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };

      
declare function checkNull($value as xs:string?,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>1 and not ($value = "0.00"))
            then element  {$fieldName}  {$value}
        else() 
      else()
      };

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function sourceValue2BooleanS($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForTransactionList($parm as element(fml:FML32)) as element()
{

&lt;ns6:transactionList>
  {
    for $x at $occ in $parm/NF_TRANSA_PUTDOWNDATE
    return
    &lt;ns5:Transaction>
      &lt;ns5:putDownDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_TRANSA_PUTDOWNDATE[$occ]))}&lt;/ns5:putDownDate>
      {
        if(data($parm/NF_TRANSA_TRANSACTIONDATE[$occ]))
		then insertDate(data($parm/NF_TRANSA_TRANSACTIONDATE[$occ]),"yyyy-MM-dd","ns5:transactionDate")
	    else ()
	  }	
	  {
        if(data($parm/NF_TRANSA_TITLE[$occ]))
		then &lt;ns5:title?>{data($parm/NF_TRANSA_TITLE[$occ])}&lt;/ns5:title>
	    else ()
	  }	     
      {
        if (data($parm/NF_TRANSA_TRANSACTIONFLAG[$occ])) 
        then &lt;ns5:transactionFlag?>{sourceValue2Boolean(data($parm/NF_TRANSA_TRANSACTIONFLAG[$occ]),"1")}&lt;/ns5:transactionFlag>
        else()
      }
	  {
        if (data($parm/NF_TRANSA_CURRENTBALANCE[$occ])) 
        then &lt;ns5:currentBalance?>{data($parm/NF_TRANSA_CURRENTBALANCE[$occ])}&lt;/ns5:currentBalance>
        else()
      }     
      &lt;ns5:transactionWn>
        &lt;ns5:TransactionWn>
		  {
           if (data($parm/NF_TRANSW_ACCOUNTNUMBER[$occ])) 
           then checkNull(data($parm/NF_TRANSW_ACCOUNTNUMBER[$occ]), "ns5:accountNumber")
           else()
          }
		  {
           if (data($parm/NF_TRANSW_AMOUNTWN[$occ])) 
           then checkNull(data($parm/NF_TRANSW_AMOUNTWN[$occ]), "ns5:amountWn")
           else()
          }          
          &lt;ns5:currencyCode>
            &lt;ns2:CurrencyCode>
			  {
               if (data($parm/NF_CURREC_CURRENCYCODE[$occ])) 
               then checkNull(data($parm/NF_CURREC_CURRENCYCODE[$occ]), "ns2:currencyCode")
               else()
              }
            &lt;/ns2:CurrencyCode>
          &lt;/ns5:currencyCode>
        &lt;/ns5:TransactionWn>
      &lt;/ns5:transactionWn>
      &lt;ns5:transactionMa>
        &lt;ns5:TransactionMa>
		  {
               if (data($parm/NF_TRANSM_ACCOUNTNUMBER[$occ])) 
               then checkNull(data($parm/NF_TRANSM_ACCOUNTNUMBER[$occ]), "ns5:accountNumber")
               else()
          }
		  {
               if (data($parm/NF_TRANSM_AMOUNTMA[$occ])) 
               then checkNull(data($parm/NF_TRANSM_AMOUNTMA[$occ]), "ns5:amountMa")
               else()
          }
          &lt;ns5:currencyCode>
            &lt;ns2:CurrencyCode>
			  {
               if (data($parm/NF_CURREC_CURRENCYCODE2[$occ])) 
               then checkNull(data($parm/NF_CURREC_CURRENCYCODE2[$occ]), "ns2:currencyCode")
               else()
              }
            &lt;/ns2:CurrencyCode>
          &lt;/ns5:currencyCode>
        &lt;/ns5:TransactionMa>
      &lt;/ns5:transactionMa>
      &lt;ns5:transactionOnStatement>
        &lt;ns5:TransactionOnStatement>
		  {
               if (data($parm/NF_TRAOST_TRANSACTIONKIND[$occ])) 
               then checkNull(data($parm/NF_TRAOST_TRANSACTIONKIND[$occ]), "ns5:transactionKind")
               else()
          }
		  {
               if (data($parm/NF_TRAOST_NAMEANDADDRESS[$occ])) 
               then checkNull(data($parm/NF_TRAOST_NAMEANDADDRESS[$occ]), "ns5:nameAndAddress")
               else()
          }
		  {
               if (data($parm/NF_TRAOST_RATES[$occ])) 
               then checkNull(data($parm/NF_TRAOST_RATES[$occ]), "ns5:rates")
               else()
          }
        &lt;/ns5:TransactionOnStatement>
      &lt;/ns5:transactionOnStatement>
    &lt;/ns5:Transaction>
  }
&lt;/ns6:transactionList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns6:invokeResponse>
  &lt;ns6:bcd>
    &lt;ns3:BusinessControlData>
      &lt;ns3:pageControl>
        &lt;ns7:PageControl>
          &lt;ns7:hasNext?>{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns7:hasNext>
          &lt;ns7:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns7:navigationKeyDefinition>
          &lt;ns7:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns7:navigationKeyValue>
        &lt;/ns7:PageControl>
      &lt;/ns3:pageControl>
    &lt;/ns3:BusinessControlData>
  &lt;/ns6:bcd>
  {getElementsForTransactionList($parm)}
&lt;/ns6:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>