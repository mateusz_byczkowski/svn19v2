<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2011-05-16</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author	Kacper Pawlaczyk
 : @author	Tomasz Krajwski
 : @version	1.7
 : @since	2011-03-01
 :
 : wersja WSDLa: 15-02-2010 13:53:15
 :
 : $Proxy Services/Accounts/getAccountDetails/PTGetPrBranchRequest.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts_svn/getAccountDetails/PTGetPrBranchRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;
declare variable $body as element() external;
declare variable $header1 as element(ns2:header)? external;

declare function xf:PTGetPrBranchRequest($fML321 as element(ns1:FML32),$header1 as element(ns2:header)?)
    as element(ns0:FML32)
{
	&lt;ns0:FML32>
		(: v1.7 - dodanie callID :)
		{
			let $callId := data($header1/ns2:msgHeader/ns2:msgId)
			return
				if ($callId)
				then
				(
					&lt;ns0:BLIB_CALL_ID>{
						$callId
					}&lt;/ns0:BLIB_CALL_ID>
				)
				else
				()
		}
		
		&lt;ns0:PT_FIRST_PRODUCT_FEATURE>{
			data($fML321/ns1:PT_FIRST_PRODUCT_FEATURE)
		}&lt;/ns0:PT_FIRST_PRODUCT_FEATURE>

		&lt;ns0:PT_SECOND_PRODUCT_FEATURE>{
			data($fML321/ns1:PT_SECOND_PRODUCT_FEATURE)
		}&lt;/ns0:PT_SECOND_PRODUCT_FEATURE>

		&lt;ns0:PT_THIRD_PRODUCT_FEATURE>{
			data($fML321/ns1:PT_THIRD_PRODUCT_FEATURE)
		}&lt;/ns0:PT_THIRD_PRODUCT_FEATURE>

		&lt;ns0:PT_FOURTH_PRODUCT_FEATURE>{
			data($fML321/ns1:PT_FOURTH_PRODUCT_FEATURE)
		}&lt;/ns0:PT_FOURTH_PRODUCT_FEATURE>

		&lt;ns0:PT_FIFTH_PRODUCT_FEATURE>{
			data($fML321/ns1:PT_FIFTH_PRODUCT_FEATURE)
		}&lt;/ns0:PT_FIFTH_PRODUCT_FEATURE>
		
		(:
		 : 1 - ICBS
		 :)
		&lt;ns0:PT_CODE_PRODUCT_SYSTEM>1&lt;/ns0:PT_CODE_PRODUCT_SYSTEM>
		
		&lt;ns0:PT_SOURCE_PRODUCT_CODE>{
			data($fML321/ns1:PT_SOURCE_PRODUCT_CODE)
		}&lt;/ns0:PT_SOURCE_PRODUCT_CODE>

	&lt;/ns0:FML32>
};

let $fML321 := $body/FML32
return
	xf:PTGetPrBranchRequest($fML321, $header1)</con:xquery>
</con:xqueryEntry>