<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2010-03-17
 :
 : wersja WSDLa: 17-03-2010 13:33:37
 :
 : $Proxy Services/Acccounts/getDirectDebitTransactionList/getDirectDebitTransactionListRequest.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getDirectDebitTransactionList/getDirectDebitTransactionListRequest/";
declare namespace ns0 = "urn:accounts.entities.be.dcl";
declare namespace ns1 = "urn:cif.entities.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:entities.be.dcl";
declare namespace ns5 = "";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns7 = "urn:baseauxentities.be.dcl";

declare variable $header1 as element(ns6:header) external;
declare variable $invoke1 as element(ns6:invoke) external;

(:
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:getDirectDebitTransactionListRequest($header1 as element(ns6:header),
														   $invoke1 as element(ns6:invoke))
    as element(ns5:FML32)
{
	&lt;ns5:FML32>

		(:
		 : dane z nagłówka
		 :)
		&lt;ns5:NF_MSHEAD_MSGID?>{
			data($header1/ns6:msgHeader/ns6:msgId)
		}&lt;/ns5:NF_MSHEAD_MSGID>

		(:
		 : dane wejsciowe - numer rachunku
		 9/06/2010 - RK - obciecie num. rachunku do 12 znakow (wymaganie TUXEDO)
		 :)
		 {
		 	let $accountNumber := data($invoke1/ns6:account/ns0:Account/ns0:accountNumber)
		 	return
		 		if ($accountNumber ) then
		 			&lt;ns5:NF_ACCOUN_ACCOUNTNUMBER?>{
						fn:substring($accountNumber , ((fn:string-length($accountNumber))-11))
					}&lt;/ns5:NF_ACCOUN_ACCOUNTNUMBER>
				else
					()
		}

		(:
		 : dane do stronicowania
		 :)
		&lt;ns5:NF_PAGEC_ACTIONCODE?>{
			data($invoke1/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:actionCode)
		}&lt;/ns5:NF_PAGEC_ACTIONCODE>

		&lt;ns5:NF_PAGEC_PAGESIZE?>{
			data($invoke1/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:pageSize)
		}&lt;/ns5:NF_PAGEC_PAGESIZE>
		
		{
			let $reverseOrder := data($invoke1/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:reverseOrder)
			return
				if ($reverseOrder) then
					&lt;ns5:NF_PAGEC_REVERSEORDER>{
						if ($reverseOrder eq 'true') then
							'1'
						else
							'0'
					}&lt;/ns5:NF_PAGEC_REVERSEORDER>
				else ()
		}

		&lt;ns5:NF_PAGEC_NAVIGATIONKEYDEFI?>{
			data($invoke1/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:navigationKeyDefinition)
		}&lt;/ns5:NF_PAGEC_NAVIGATIONKEYDEFI>
		
		&lt;ns5:NF_PAGEC_NAVIGATIONKEYVALU?>{
			data($invoke1/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:navigationKeyValue)
		}&lt;/ns5:NF_PAGEC_NAVIGATIONKEYVALU>
            
		(:
		 : dane wejsciowe - cd.
		 :)
		&lt;ns5:NF_SPWC_SPWCODE?>{
			data($invoke1/ns6:customer/ns1:Customer/ns1:spw/ns2:SpwCode/ns2:spwCode)
		}&lt;/ns5:NF_SPWC_SPWCODE>
		
		&lt;ns5:NF_USERTS_SESSIONDATE?>{
			data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:sessionDate)
		}&lt;/ns5:NF_USERTS_SESSIONDATE>
		
	&lt;/ns5:FML32>
};

&lt;soap-env:Body>{
	xf:getDirectDebitTransactionListRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>