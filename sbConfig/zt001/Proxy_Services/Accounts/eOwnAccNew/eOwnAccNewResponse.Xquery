<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeOwnAccNewResponse($fml as element(fml:FML32))
	as element(m:eOwnAccNewResponse) {
		&lt;m:eOwnAccNewResponse>
			{
				if($fml/fml:E_ADMIN_MICRO_BRANCH)
					then &lt;m:AdminMicroBranch>{ data($fml/fml:E_ADMIN_MICRO_BRANCH) }&lt;/m:AdminMicroBranch>
					else ()
			}
			{
				if($fml/fml:U_USER_NAME)
					then &lt;m:UserName>{ data($fml/fml:U_USER_NAME) }&lt;/m:UserName>
					else ()
			}
			{
				if($fml/fml:B_SYS_MASK)
					then &lt;m:SysMask>{ data($fml/fml:B_SYS_MASK) }&lt;/m:SysMask>
					else ()
			}
			{

				let $B_SYS := $fml/fml:B_SYS
				let $B_BANK := $fml/fml:B_BANK
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				let $E_SEQ_NO := $fml/fml:E_SEQ_NO
				let $E_CHANNEL_MASK := $fml/fml:E_CHANNEL_MASK
				let $B_KOD_RACH := $fml/fml:B_KOD_RACH
				let $E_TIME_STAMP := $fml/fml:E_TIME_STAMP
				let $E_ACCOUNT_TYPE_OPTIONS := $fml/fml:E_ACCOUNT_TYPE_OPTIONS
				let $E_DR_TRN_MASK := $fml/fml:E_DR_TRN_MASK
				let $E_CR_TRN_MASK := $fml/fml:E_CR_TRN_MASK
				let $E_LOGIN_ID := $fml/fml:E_LOGIN_ID
				let $E_OPTIONS := $fml/fml:E_OPTIONS
				let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
				let $B_WALUTA := $fml/fml:B_WALUTA
				let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
				for $it at $p in $fml/fml:B_KOD_RACH
				return
					&lt;m:eOwnAccNewAccount>
					{
						if($B_SYS[$p])
							then &lt;m:Sys>{ data($B_SYS[$p]) }&lt;/m:Sys>
						else ()
					}
					{
						if($B_BANK[$p])
							then &lt;m:Bank>{ data($B_BANK[$p]) }&lt;/m:Bank>
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then &lt;m:TypRach>{ data($B_TYP_RACH[$p]) }&lt;/m:TypRach>
						else ()
					}
					{
						if($E_SEQ_NO[$p])
							then &lt;m:SeqNo>{ data($E_SEQ_NO[$p]) }&lt;/m:SeqNo>
						else ()
					}
					{
						if($E_CHANNEL_MASK[$p])
							then &lt;m:ChannelMask>{ data($E_CHANNEL_MASK[$p]) }&lt;/m:ChannelMask>
						else ()
					}
					{
						if($B_KOD_RACH[$p])
							then &lt;m:KodRach>{ data($B_KOD_RACH[$p]) }&lt;/m:KodRach>
						else ()
					}
					{
						if($E_TIME_STAMP[$p])
							then &lt;m:TimeStamp>{ data($E_TIME_STAMP[$p]) }&lt;/m:TimeStamp>
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_OPTIONS[$p])
							then &lt;m:AccountTypeOptions>{ data($E_ACCOUNT_TYPE_OPTIONS[$p]) }&lt;/m:AccountTypeOptions>
						else ()
					}
					{
						if($E_DR_TRN_MASK[$p])
							then &lt;m:DrTrnMask>{ data($E_DR_TRN_MASK[$p]) }&lt;/m:DrTrnMask>
						else ()
					}
					{
						if($E_CR_TRN_MASK[$p])
							then &lt;m:CrTrnMask>{ data($E_CR_TRN_MASK[$p]) }&lt;/m:CrTrnMask>
						else ()
					}
					{
						if($E_LOGIN_ID[$p])
							then &lt;m:LoginId>{ data($E_LOGIN_ID[$p]) }&lt;/m:LoginId>
						else ()
					}
					{
						if($E_OPTIONS[$p])
							then &lt;m:Options>{ data($E_OPTIONS[$p]) }&lt;/m:Options>
						else ()
					}
					{
						if($B_DL_NR_RACH[$p])
							then &lt;m:DlNrRach>{ data($B_DL_NR_RACH[$p]) }&lt;/m:DlNrRach>
						else ()
					}
					{
						if($B_WALUTA[$p])
							then &lt;m:Waluta>{ data($B_WALUTA[$p]) }&lt;/m:Waluta>
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_NAME[$p])
							then &lt;m:AccountTypeName>{ data($E_ACCOUNT_TYPE_NAME[$p]) }&lt;/m:AccountTypeName>
						else ()
					}
					&lt;/m:eOwnAccNewAccount>
			}

		&lt;/m:eOwnAccNewResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapeOwnAccNewResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>