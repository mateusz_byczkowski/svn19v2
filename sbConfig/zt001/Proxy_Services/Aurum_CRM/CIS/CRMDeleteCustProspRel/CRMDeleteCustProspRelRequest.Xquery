<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMDeleteCustProspRelRequest($req as element(m:CRMDeleteCustProspRelRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:CustCif)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:CustCif) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:RelType)
					then &lt;fml:DC_TYP_RELACJI>{ data($req/m:RelType) }&lt;/fml:DC_TYP_RELACJI>
					else ()
			}
			{
				if($req/m:EmpSkpNo)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:EmpSkpNo) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
                        {
                                if($req/m:NumerKlienta)
                                         then &lt;fml:CI_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:CI_NUMER_KLIENTA>
                                         else ()
                        }
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMDeleteCustProspRelRequest($body/m:CRMDeleteCustProspRelRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>