<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function xf:mapCRMGetCustProdsRequest($req as element(m:CRMGetCustProdsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:PoziomAgregacji)
					then &lt;fml:CI_POZIOM_AGREGACJI>{ data($req/m:PoziomAgregacji) }&lt;/fml:CI_POZIOM_AGREGACJI>
					else ()
			}
			{
				if($req/m:KodProduktu)
					then &lt;fml:CI_KOD_PRODUKTU>{ data($req/m:KodProduktu) }&lt;/fml:CI_KOD_PRODUKTU>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $req as element(m:CRMGetCustProdsRequest) external;
&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
{ xf:mapCRMGetCustProdsRequest($req) }
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>