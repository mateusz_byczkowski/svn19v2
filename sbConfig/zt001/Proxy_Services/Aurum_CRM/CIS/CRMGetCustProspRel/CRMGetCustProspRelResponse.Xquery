<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustProspRelResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustProspRelResponse) {
		&lt;m:CRMGetCustProspRelResponse>
			{

				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $CI_NUMER_KLIENTA := $fml/fml:CI_NUMER_KLIENTA
                                let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_DRUGIE_IMIE := $fml/fml:DC_DRUGIE_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $DC_NR_DOWODU_REGON := $fml/fml:DC_NR_DOWODU_REGON
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL
				let $DC_NUMER_PASZPORTU := $fml/fml:DC_NUMER_PASZPORTU
				let $DC_DATA_URODZENIA := $fml/fml:DC_DATA_URODZENIA
				let $DC_TYP_ADRESU := $fml/fml:DC_TYP_ADRESU
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST
				let $DC_WOJ_KRAJ_DANE_PODST := $fml/fml:DC_WOJ_KRAJ_DANE_PODST
				let $CI_KOD_KRAJU := $fml/fml:CI_KOD_KRAJU
				let $DC_TYP_RELACJI := $fml/fml:DC_TYP_RELACJI
				let $CI_PARAMETRY_RELACJI := $fml/fml:CI_PARAMETRY_RELACJI
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:ProspRelData>
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:CustCif>{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:CustCif>
						else ()
					}
					{
						if($CI_NUMER_KLIENTA[$p])
							then &lt;m:ProspId>{ data($CI_NUMER_KLIENTA[$p]) }&lt;/m:ProspId>
						else ()
					}
					{
						if ($CI_SKP_PRACOWNIKA[$p])
                                                        then &lt;m:EmpSkpNo>{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:EmpSkpNo>
                                                else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:RelFirstName>{ data($DC_IMIE[$p]) }&lt;/m:RelFirstName>
						else ()
					}
					{
						if($DC_DRUGIE_IMIE[$p])
							then &lt;m:RelSecondName>{ data($DC_DRUGIE_IMIE[$p]) }&lt;/m:RelSecondName>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:RelLastName>{ data($DC_NAZWISKO[$p]) }&lt;/m:RelLastName>
						else ()
					}
					{
						if($DC_NR_DOWODU_REGON[$p])
							then &lt;m:RelIdCardNo>{ data($DC_NR_DOWODU_REGON[$p]) }&lt;/m:RelIdCardNo>
						else ()
					}
					{
						if($DC_NUMER_PASZPORTU[$p])
							then &lt;m:RelPassportNo>{ data($DC_NUMER_PASZPORTU[$p]) }&lt;/m:RelPassportNo>
						else ()
					}
					{
						if($DC_NR_PESEL[$p])
							then &lt;m:RelPesel>{ data($DC_NR_PESEL[$p]) }&lt;/m:RelPesel>
						else ()
					}
					{
						if($DC_DATA_URODZENIA[$p])
							then &lt;m:RelBirthDate>{ data($DC_DATA_URODZENIA[$p]) }&lt;/m:RelBirthDate>
						else ()
					}
					{
						if($DC_TYP_ADRESU[$p])
							then &lt;m:RelAddressType>{ data($DC_TYP_ADRESU[$p]) }&lt;/m:RelAddressType>
						else ()
					}
					{
						if($DC_ULICA_DANE_PODST[$p])
							then &lt;m:RelAddressStreet>{ data($DC_ULICA_DANE_PODST[$p]) }&lt;/m:RelAddressStreet>
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_DANE_PODST[$p])
							then &lt;m:RelAddressHouseFlatNo>{ data($DC_NR_POSES_LOKALU_DANE_PODST[$p]) }&lt;/m:RelAddressHouseFlatNo>
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_DANE_PODST[$p])
							then &lt;m:RelAddressPostCode>{ data($DC_KOD_POCZTOWY_DANE_PODST[$p]) }&lt;/m:RelAddressPostCode>
						else ()
					}
					{
						if($DC_MIASTO_DANE_PODST[$p])
							then &lt;m:RelAddressCity>{ data($DC_MIASTO_DANE_PODST[$p]) }&lt;/m:RelAddressCity>
						else ()
					}
					{
						if($DC_WOJ_KRAJ_DANE_PODST[$p])
							then &lt;m:RelAddressCounty>{ data($DC_WOJ_KRAJ_DANE_PODST[$p]) }&lt;/m:RelAddressCounty>
						else ()
					}
					{
						if($CI_KOD_KRAJU[$p])
							then &lt;m:RelAddressCountry>{ data($CI_KOD_KRAJU[$p]) }&lt;/m:RelAddressCountry>
						else ()
					}
					{
						if($DC_TYP_RELACJI[$p])
							then &lt;m:RelType>{ data($DC_TYP_RELACJI[$p]) }&lt;/m:RelType>
						else ()
					}
					{
						if($CI_PARAMETRY_RELACJI[$p])
							then &lt;m:RelDetails>{ fn:replace(data($CI_PARAMETRY_RELACJI[$p]), "&amp;", "&amp;amp;") }&lt;/m:RelDetails>
						else ()
					}
					&lt;/m:ProspRelData>
			}

		&lt;/m:CRMGetCustProspRelResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetCustProspRelResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>