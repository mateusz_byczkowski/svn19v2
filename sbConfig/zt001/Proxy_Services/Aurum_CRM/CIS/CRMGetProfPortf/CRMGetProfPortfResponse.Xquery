<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function xf:mapping($buf as element(fml:FML32), $req as element(fml:FML32))
	as element() {
		&lt;portfolio_profit_report>
			{
				if($req/CI_ZNACZNIK_OKRESU)
					then &lt;period>{ data($req/CI_ZNACZNIK_OKRESU) }&lt;/period>
				else ()
			}
			{
				if($req/CI_DECYL)
					then &lt;decile>{ data($req/CI_DECYL) }&lt;/decile>
				else ()
			}
			{
				if($req/DC_TYP_KLIENTA)
					then &lt;cust_type_in>{ data($req/DC_TYP_KLIENTA) }&lt;/cust_type_in>
				else ()
			}
			{
				if($buf/CI_NUMER_PACZKI_STR)
					then &lt;package_no>{ data($buf/CI_NUMER_PACZKI_STR) }&lt;/package_no>
				else ()
			}
			&lt;portf_profitability>
				{
					if($req/DC_TYP_KLIENTA) then
			   			&lt;cust_type code="{ data($req/DC_TYP_KLIENTA) }">
			   			{
			   				for $it at $p in $buf/DC_NUMER_KLIENTA
			   				return
								&lt;customer>
									{
										if($buf/DC_NUMER_KLIENTA[$p])
											then &lt;param code="CIF">{ data($buf/DC_NUMER_KLIENTA[$p]) }&lt;/param>
										else
											()
									}
									{	(: dla fizycznych imie -> name :)
										if(data($buf/DC_IMIE[$p]) != "")
											then &lt;param code="NAME">{ data($buf/DC_IMIE[$p]) }&lt;/param>
										else ()
									}
									{	(: dla prawnych nazwa pea -> name :)
										if(data($buf/CI_NAZWA_PELNA[$p]) != "")
											then &lt;param code="NAME">{ data($buf/CI_NAZWA_PELNA[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/DC_NAZWISKO[$p])
											then &lt;param code="SURNAME">{ data($buf/DC_NAZWISKO[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_DECYL[$p])
											then &lt;param code="DECILE">{ data($buf/CI_DECYL[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_DOCHODY[$p])
											then &lt;param code="INCOME">{ data($buf/CI_DOCHODY[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_DOCHODY_ODSETKOWE[$p])
											then &lt;param code="INCOME_INT">{ data($buf/CI_DOCHODY_ODSETKOWE[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_DOCHODY_POZOSTALE[$p])
											then &lt;param code="INCOME_OTHER">{ data($buf/CI_DOCHODY_POZOSTALE[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_KOSZTY_BEZPOSREDNIE[$p])
											then &lt;param code="DIR_COSTS">{ data($buf/CI_KOSZTY_BEZPOSREDNIE[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_KOSZTY_POSREDNIE[$p])
											then &lt;param code="INDIR_COSTS">{ data($buf/CI_KOSZTY_POSREDNIE[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_KOSZTY_RYZYKA[$p])
											then &lt;param code="RISK_COSTS">{ data($buf/CI_KOSZTY_RYZYKA[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_WYNIK[$p])
											then &lt;param code="RESULT">{ data($buf/CI_WYNIK[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/DC_PRACOWNIK_BANKU[$p])
											then &lt;param code="EMPLOYEE_FLAG">{ data($buf/DC_PRACOWNIK_BANKU[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_VIP[$p])
											then &lt;param code="VIP">{ data($buf/CI_VIP[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_KLASA_OBSLUGI[$p])
											then &lt;param code="CI_KLASA_OBSLUGI">{ data($buf/CI_KLASA_OBSLUGI[$p]) }&lt;/param>
										else ()
									}
								&lt;/customer>
							}
			   			&lt;/cust_type>
					else
						for $typ at $p in $buf/DC_TYP_KLIENTA
						return
				   			&lt;cust_type code="{ data($typ) }">
								&lt;customer>
									&lt;!-- pola niezmapowane: CI_ID_PORTFELA, CI_ZNACZNIK_OKRESU -->
									{
										if($buf/CI_DECYL[$p])
											then &lt;param code="DECILE">{ data($buf/CI_DECYL[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_DOCHODY[$p])
											then &lt;param code="INCOME">{ data($buf/CI_DOCHODY[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_KOSZTY_BEZPOSREDNIE[$p])
											then &lt;param code="DIR_COSTS">{ data($buf/CI_KOSZTY_BEZPOSREDNIE[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_KOSZTY_POSREDNIE[$p])
											then &lt;param code="INDIR_COSTS">{ data($buf/CI_KOSZTY_POSREDNIE[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_KOSZTY_RYZYKA[$p])
											then &lt;param code="RISK_COSTS">{ data($buf/CI_KOSZTY_RYZYKA[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_WYNIK[$p])
											then &lt;param code="RESULT">{ data($buf/CI_WYNIK[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/DC_PRACOWNIK_BANKU[$p])
											then &lt;param code="EMPLOYEE_FLAG">{ data($buf/DC_PRACOWNIK_BANKU[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_VIP[$p])
											then &lt;param code="VIP">{ data($buf/CI_VIP[$p]) }&lt;/param>
										else ()
									}
									{
										if($buf/CI_KLASA_OBSLUGI[$p])
											then &lt;param code="KLASA_OBSLUGI">{ data($buf/CI_KLASA_OBSLUGI[$p]) }&lt;/param>
										else ()
									}
								&lt;/customer>
				   			&lt;/cust_type>
				}
			&lt;/portf_profitability>
		&lt;/portfolio_profit_report>
};


declare variable $buf as element(fml:FML32) external;
declare variable $req as element(fml:FML32) external;

&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	&lt;m:CRMGetProfPortfResponse>{ fn-bea:serialize(xf:mapping($buf, $req)) }&lt;/m:CRMGetProfPortfResponse>
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>