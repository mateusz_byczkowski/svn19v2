<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMSaveQuestionnaireAnswersRequest($req as element(m:CRMSaveQuestionnaireAnswersRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:QuestId)
					then &lt;fml:CI_ID_ANKIETY>{ data($req/m:QuestId) }&lt;/fml:CI_ID_ANKIETY>
					else ()
			}
			{
				if($req/m:CustCif)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:CustCif) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:CompanyId)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:CompanyId) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:UsrSkpNo)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:UsrSkpNo) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:XmlQuestionnaire)
					then &lt;fml:CI_ANKIETA_XML>{ data($req/m:XmlQuestionnaire) }&lt;/fml:CI_ANKIETA_XML>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMSaveQuestionnaireAnswersRequest($body/m:CRMSaveQuestionnaireAnswersRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>