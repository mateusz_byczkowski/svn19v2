<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetFlagHistoryRequest($req as element(m:CRMGetFlagHistoryRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IcbsNo)
					then &lt;fml:CI_NUMER_ODDZIALU>{ data($req/m:IcbsNo) }&lt;/fml:CI_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:CompanyId)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:CompanyId) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:AccDateFrom)
					then &lt;fml:CI_DATA_OD>{ data($req/m:AccDateFrom) }&lt;/fml:CI_DATA_OD>
					else ()
			}
			{
				if($req/m:AccDateTo)
					then &lt;fml:CI_DATA_DO>{ data($req/m:AccDateTo) }&lt;/fml:CI_DATA_DO>
					else ()
			}
			{
				if($req/m:CustCif)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:CustCif) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:SetUserId)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:SetUserId) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:AccUserId)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:AccUserId) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:FlagType)
					then &lt;fml:CI_TYP>{ data($req/m:FlagType) }&lt;/fml:CI_TYP>
					else ()
			}
			{
				if($req/m:SortBy)
					then &lt;fml:CI_SORTOWANIE>{ data($req/m:SortBy) }&lt;/fml:CI_SORTOWANIE>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetFlagHistoryRequest($body/m:CRMGetFlagHistoryRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>