<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMModLFRFlagRequest($req as element(m:modLFRFlagRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:EmpSKPNo)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:EmpSKPNo) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:CustCIF)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:CustCIF) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:FlagValue)
					then &lt;fml:CI_LFR>{ data($req/m:FlagValue) }&lt;/fml:CI_LFR>
					else ()
			}			
			{
				if($req/m:CompanyId)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:CompanyId) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:BranchId)
					then &lt;fml:CI_NUMER_ODDZIALU>{ data($req/m:BranchId) }&lt;/fml:CI_NUMER_ODDZIALU>
					else ()
			}

                        &lt;fml:CI_SKP_PRACOWNIKA/>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMModLFRFlagRequest($body/m:modLFRFlagRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>