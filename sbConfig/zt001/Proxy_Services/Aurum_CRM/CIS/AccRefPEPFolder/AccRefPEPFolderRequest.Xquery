<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Zmiana w polu CI_PEP z wartości Y na 1Version.$1.2010-09-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapAccRefPEPFolder($req as element(m:accRefPEPFolderRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			&lt;fml:CI_SKP_PRACOWNIKA>{data($req/m:SupSKPNo)}&lt;/fml:CI_SKP_PRACOWNIKA>
			&lt;fml:CI_SKP_PRACOWNIKA_REJ>{data($req/m:EmpSKPNo)}&lt;/fml:CI_SKP_PRACOWNIKA_REJ>
			&lt;fml:CI_NUMER_ODDZIALU>{data($req/m:OfficerBranchId)}&lt;/fml:CI_NUMER_ODDZIALU>
                        &lt;fml:DC_NUMER_KLIENTA>{data($req/m:PepCIF)}&lt;/fml:DC_NUMER_KLIENTA>
                        &lt;fml:CI_ID_SPOLKI>1&lt;/fml:CI_ID_SPOLKI>
                        &lt;fml:CI_PEP>1&lt;/fml:CI_PEP>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapAccRefPEPFolder($body/m:accRefPEPFolderRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>