<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetProspFolderRequest($req as element(m:CRMGetProspFolderRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>

                   {
                    if($req/m:CustId)
			then &lt;fml:CI_NUMER_KLIENTA>{data($req/m:CustId)}&lt;/fml:CI_NUMER_KLIENTA>
                        else ()
                    }
                   {
                    if($req/m:CompanyId)
			then &lt;fml:CI_ID_SPOLKI>{data($req/m:CompanyId)}&lt;/fml:CI_ID_SPOLKI>
                        else ()
                    }
                   {
                    if($req/m:EmpSkpNo)
			then &lt;fml:CI_SKP_PRACOWNIKA>{data($req/m:EmpSkpNo)}&lt;/fml:CI_SKP_PRACOWNIKA>
                        else ()
                    }
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetProspFolderRequest($body/m:CRMGetProspFolderRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>