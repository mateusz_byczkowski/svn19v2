<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCustomerRequest($req as element(m:GetCustomerRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			&lt;fml:B_RODZ_DOK?>{ data($req/m:RodzDok) }&lt;/fml:B_RODZ_DOK>
			&lt;fml:B_NR_DOK?>{ data($req/m:NrDok) }&lt;/fml:B_NR_DOK>
			{
				if(data($req/m:Sys) = '3')
					then if(string-length(data($req/m:DlNrRach)) > 10)
						then &lt;fml:B_DL_NR_RACH>{ data($req/m:DlNrRach) }&lt;/fml:B_DL_NR_RACH>
						else &lt;fml:B_KOD_RACH?>{ data($req/m:DlNrRach) }&lt;/fml:B_KOD_RACH>
					else &lt;fml:B_DL_NR_RACH?>{ data($req/m:DlNrRach) }&lt;/fml:B_DL_NR_RACH>
			}
			&lt;fml:B_OPCJA?>{ data($req/m:Opcja) }&lt;/fml:B_OPCJA>
			&lt;fml:B_ID_ODDZ?>{ data($req/m:IdOddz) }&lt;/fml:B_ID_ODDZ>
			&lt;fml:E_CUSTOMER_TYPE?>{ data($req/m:CustomerType) }&lt;/fml:E_CUSTOMER_TYPE>
			&lt;fml:E_CIF_NUMBER?>{ data($req/m:CifNumber) }&lt;/fml:E_CIF_NUMBER>
			&lt;fml:E_CIF_OPTIONS?>{ data($req/m:CifOptions) }&lt;/fml:E_CIF_OPTIONS>
			&lt;fml:B_SYS?>{ data($req/m:Sys) }&lt;/fml:B_SYS>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetCustomerRequest($body/m:GetCustomerRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>