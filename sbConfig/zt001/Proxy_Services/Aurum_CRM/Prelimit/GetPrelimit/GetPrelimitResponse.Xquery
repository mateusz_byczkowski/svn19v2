<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/prl/messages/";
declare namespace xf = "http://bzwbk.com/services/prl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetPrelimitResponse($fml as element(fml:FML32))
	as element(m:GetPrelimitResponse) {
		&lt;m:GetPrelimitResponse>
			{
				let $DC_FLAGA_PRL := $fml/fml:DC_FLAGA_PRL
				let $DC_KWOTA_PRELIMITU := $fml/fml:DC_KWOTA_PRELIMITU
				let $DC_MIN_CENA := $fml/fml:DC_MIN_CENA
				let $DC_NR_RACHUNKU := $fml/fml:DC_NR_RACHUNKU
				let $DC_LIMIT_BIEZ := $fml/fml:DC_LIMIT_BIEZ
				let $DC_DATA_KARENCJI := $fml/fml:DC_DATA_KARENCJI
				let $DC_KOD_WYKLUCZENIA := $fml/fml:DC_KOD_WYKLUCZENIA
				let $DC_PRAWDOPODOBIENSTWO := $fml/fml:DC_PRAWDOPODOBIENSTWO
				let $DC_FLAGA_MARKETINGU := $fml/fml:DC_FLAGA_MARKETINGU
				let $DC_FLAGA_KLIENTA := $fml/fml:DC_FLAGA_KLIENTA
				let $DC_DATA_WAZNOSCI := $fml/fml:DC_DATA_WAZNOSCI
				let $DC_PRAWDOPOD_OPIS := $fml/fml:DC_PRAWDOPOD_OPIS
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL
				let $DC_NR_DOWODU_REGON := $fml/fml:DC_NR_DOWODU_REGON
				let $DC_NUMER_PASZPORTU := $fml/fml:DC_NUMER_PASZPORTU
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA
				let $DC_STATUS_KLIENTA := $fml/fml:DC_STATUS_KLIENTA
				let $DC_SUGEROWANA_CENA := $fml/fml:DC_SUGEROWANA_CENA
				let $DC_MAKS_CENA := $fml/fml:DC_MAKS_CENA
				for $it at $p in $fml/fml:DC_FLAGA_PRL
				return
					&lt;m:GetPrelimitPrelimit>
						&lt;m:FlagaPrl?>{ data($DC_FLAGA_PRL[$p]) }&lt;/m:FlagaPrl>
						&lt;m:KwotaPrelimitu?>{ data($DC_KWOTA_PRELIMITU[$p]) }&lt;/m:KwotaPrelimitu>
						&lt;m:MinCena?>{ data($DC_MIN_CENA[$p]) }&lt;/m:MinCena>
						&lt;m:NrRachunku?>{ data($DC_NR_RACHUNKU[$p]) }&lt;/m:NrRachunku>
						&lt;m:LimitBiez?>{ data($DC_LIMIT_BIEZ[$p]) }&lt;/m:LimitBiez>
						&lt;m:DataKarencji?>{ data($DC_DATA_KARENCJI[$p]) }&lt;/m:DataKarencji>
						&lt;m:KodWykluczenia?>{ data($DC_KOD_WYKLUCZENIA[$p]) }&lt;/m:KodWykluczenia>
						&lt;m:Prawdopodobienstwo?>{ data($DC_PRAWDOPODOBIENSTWO[$p]) }&lt;/m:Prawdopodobienstwo>
						&lt;m:FlagaMarketingu?>{ data($DC_FLAGA_MARKETINGU[$p]) }&lt;/m:FlagaMarketingu>
						&lt;m:FlagaKlienta?>{ data($DC_FLAGA_KLIENTA[$p]) }&lt;/m:FlagaKlienta>
						&lt;m:DataWaznosci?>{ data($DC_DATA_WAZNOSCI[$p]) }&lt;/m:DataWaznosci>
						&lt;m:PrawdopodOpis?>{ data($DC_PRAWDOPOD_OPIS[$p]) }&lt;/m:PrawdopodOpis>
						&lt;m:NrPesel?>{ data($DC_NR_PESEL[$p]) }&lt;/m:NrPesel>
						&lt;m:NrDowoduRegon?>{ data($DC_NR_DOWODU_REGON[$p]) }&lt;/m:NrDowoduRegon>
						&lt;m:NumerPaszportu?>{ data($DC_NUMER_PASZPORTU[$p]) }&lt;/m:NumerPaszportu>
						&lt;m:TypKlienta?>{ data($DC_TYP_KLIENTA[$p]) }&lt;/m:TypKlienta>
						&lt;m:StatusKlienta?>{ data($DC_STATUS_KLIENTA[$p]) }&lt;/m:StatusKlienta>
						&lt;m:SugerowanaCena?>{ data($DC_SUGEROWANA_CENA[$p]) }&lt;/m:SugerowanaCena>
						&lt;m:MaksCena?>{ data($DC_MAKS_CENA[$p]) }&lt;/m:MaksCena>
					&lt;/m:GetPrelimitPrelimit>
			}

		&lt;/m:GetPrelimitResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetPrelimitResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>