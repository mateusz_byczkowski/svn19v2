<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "urn:be.services.dcl";
declare namespace m1 = "urn:hlbsentities.be.dcl";
declare namespace w = "";
declare namespace u = "urn:RatingWS";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

&lt;soap-env:Body>
{
    let $req := $body/m:invoke/m:input/m1:RatingDecisionSWWRInput
    return
    
    &lt;urn:saveDecision xmlns:urn="urn:RatingWS" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      {if($req/m1:timeStamp)
          then &lt;w:timestamp xsi:type="xs:string">{ data($req/m1:timeStamp) }&lt;/w:timestamp>
          else ()
      }
      {if($req/m1:userId)
          then &lt;w:userId xsi:type="xs:string">{ data($req/m1:userId) }&lt;/w:userId>
          else ()
      }
      {if($req/m1:swwrId)
          then &lt;w:swwrId xsi:type="xs:int">{ data($req/m1:swwrId) }&lt;/w:swwrId>
          else ()
      }
      {if($req/m1:extId)
          then &lt;w:extId xsi:type="xs:int">{ data($req/m1:extId) }&lt;/w:extId>
          else ()
      }
      {if($req/m1:extType)
          then &lt;w:extType xsi:type="xs:string">{ data($req/m1:extType) }&lt;/w:extType>
          else ()
      }
      {if($req/m1:oper)
          then &lt;w:oper xsi:type="xs:string">{ data($req/m1:oper) }&lt;/w:oper>
          else ()
      }
      {if($req/m1:comment)
          then &lt;w:comment xsi:type="xs:string">{ data($req/m1:comment) }&lt;/w:comment>
          else ()
      }
      {if($req/m1:vars)
          then &lt;w:vars xsi:type="xs:string">{ data($req/m1:vars) }&lt;/w:vars>
          else ()
      }
    &lt;/urn:saveDecision>
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>