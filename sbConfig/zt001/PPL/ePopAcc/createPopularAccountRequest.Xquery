<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapcreatePopularAccountRequest($req as element(m:createPopularAccount))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/userDescription/microbranch)
					then &lt;fml:E_MICRO_BRANCH>{ data($req/userDescription/microbranch) }&lt;/fml:E_MICRO_BRANCH>
					else ()
			}
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME>{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/popularAccount/accountNo)
					then &lt;fml:B_DL_NR_RACH>{ data($req/popularAccount/accountNo) }&lt;/fml:B_DL_NR_RACH>
					else ()
			}
			{
				if($req/popularAccount/info)
					then &lt;fml:E_POPACC_INFO>{ data($req/popularAccount/info) }&lt;/fml:E_POPACC_INFO>
					else ()
			}
			{
				if($req/popularAccount/validTo)
					then &lt;fml:E_VALID_TO>{ data($req/popularAccount/validTo) }&lt;/fml:E_VALID_TO>
					else ()
			}
			{
				if($req/popularAccount/validFrom)
					then &lt;fml:E_VALID_FROM>{ data($req/popularAccount/validFrom) }&lt;/fml:E_VALID_FROM>
					else ()
			}
			{
				if($req/popularAccount/description)
					then &lt;fml:E_TRN_TITLE>{ data($req/popularAccount/description) }&lt;/fml:E_TRN_TITLE>
					else ()
			}
			{
				if($req/popularAccount/branchCode)
					then &lt;fml:E_BRANCH_CODE>{ data($req/popularAccount/branchCode) }&lt;/fml:E_BRANCH_CODE>
					else ()
			}
			{
				if($req/popularAccount/currency)
					then &lt;fml:B_WALUTA>{ data($req/popularAccount/currency) }&lt;/fml:B_WALUTA>
					else ()
			}
			{
				if($req/popularAccount/zipCode)
					then &lt;fml:B_KOD_POCZT>{ data($req/popularAccount/zipCode) }&lt;/fml:B_KOD_POCZT>
					else ()
			}
			{
				if($req/popularAccount/city)
					then &lt;fml:B_M_ZAM>{ data($req/popularAccount/city) }&lt;/fml:B_M_ZAM>
					else ()
			}
			{
				if($req/popularAccount/street)
					then &lt;fml:B_UL_ZAM>{ data($req/popularAccount/street) }&lt;/fml:B_UL_ZAM>
					else ()
			}
			{
				if($req/popularAccount/name)
					then &lt;fml:B_NAZWA_KLIENTA>{ data($req/popularAccount/name) }&lt;/fml:B_NAZWA_KLIENTA>
					else ()
			}
			{
				if($req/popularAccount/transferType)
					then &lt;fml:E_TRANSFER_TYPE>{ data($req/popularAccount/transferType) }&lt;/fml:E_TRANSFER_TYPE>
					else ()
			}
			{
				if($req/popularAccount/options)
					then &lt;fml:E_POPACC_OPTIONS>{ data($req/popularAccount/options) }&lt;/fml:E_POPACC_OPTIONS>
					else ()
			}
			{
				if($req/popularAccount/timestamp)
					then &lt;fml:E_TIME_STAMP>{ data($req/popularAccount/timestamp) }&lt;/fml:E_TIME_STAMP>
					else ()
			}
			{
				if($req/popularAccount/accountTypeId)
					then &lt;fml:B_TYP_RACH>{ data($req/popularAccount/accountTypeId) }&lt;/fml:B_TYP_RACH>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapcreatePopularAccountRequest($body/m:createPopularAccount) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>