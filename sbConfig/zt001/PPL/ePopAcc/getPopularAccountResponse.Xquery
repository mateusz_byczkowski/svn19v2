<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetPopularAccountResponse($fml as element(fml:FML32))
	as element(m:getPopularAccountResponse) {
		&lt;m:getPopularAccountResponse>
		&lt;getPopularAccountReturn>
			{
				if($fml/fml:B_DL_NR_RACH)
					then &lt;accountNo>{ data($fml/fml:B_DL_NR_RACH) }&lt;/accountNo>
					else ()
			}
			{
				if($fml/fml:E_POPACC_INFO)
					then &lt;info>{ data($fml/fml:E_POPACC_INFO) }&lt;/info>
					else ()
			}
			{
				if($fml/fml:E_VALID_TO)
					then &lt;validTo>{ data($fml/fml:E_VALID_TO) }&lt;/validTo>
					else ()
			}
			{
				if($fml/fml:E_VALID_FROM)
					then &lt;validFrom>{ data($fml/fml:E_VALID_FROM) }&lt;/validFrom>
					else ()
			}
			{
				if($fml/fml:E_TRN_TITLE)
					then &lt;description>{ data($fml/fml:E_TRN_TITLE) }&lt;/description>
					else ()
			}
			{
				if($fml/fml:E_BRANCH_CODE)
					then &lt;branchCode>{ data($fml/fml:E_BRANCH_CODE) }&lt;/branchCode>
					else ()
			}
			{
				if($fml/fml:B_WALUTA)
					then &lt;currency>{ data($fml/fml:B_WALUTA) }&lt;/currency>
					else ()
			}
			{
				if($fml/fml:B_KOD_POCZT)
					then &lt;zipCode>{ data($fml/fml:B_KOD_POCZT) }&lt;/zipCode>
					else ()
			}
			{
				if($fml/fml:B_M_ZAM)
					then &lt;city>{ data($fml/fml:B_M_ZAM) }&lt;/city>
					else ()
			}
			{
				if($fml/fml:B_UL_ZAM)
					then &lt;street>{ data($fml/fml:B_UL_ZAM) }&lt;/street>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KLIENTA)
					then &lt;name>{ data($fml/fml:B_NAZWA_KLIENTA) }&lt;/name>
					else ()
			}
			{
				if($fml/fml:E_TRANSFER_TYPE)
					then &lt;transferType>{ data($fml/fml:E_TRANSFER_TYPE) }&lt;/transferType>
					else ()
			}
			{
				if($fml/fml:E_POPACC_OPTIONS)
					then &lt;options>{ data($fml/fml:E_POPACC_OPTIONS) }&lt;/options>
					else ()
			}
			{
				if($fml/fml:E_TIME_STAMP)
					then &lt;timestamp>{ data($fml/fml:E_TIME_STAMP) }&lt;/timestamp>
					else ()
			}
			{
				if($fml/fml:B_TYP_RACH)
					then &lt;accountTypeId>{ data($fml/fml:B_TYP_RACH) }&lt;/accountTypeId>
					else ()
			}
		&lt;/getPopularAccountReturn>
		&lt;/m:getPopularAccountResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetPopularAccountResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>