<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetFirstContractDescriptionsRequest($req as element(m:getFirstContractDescriptions))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/date)
					then &lt;fml:E_CUST_REPORT_DATE>{ data($req/date) }&lt;/fml:E_CUST_REPORT_DATE>
					else ()
			}
			{
				if($req/status)
					then &lt;fml:E_CUST_REPORT_STATUS>{ data($req/status) }&lt;/fml:E_CUST_REPORT_STATUS>
					else ()
			}
			{
				if($req/contractDescriptionsNo)
					then &lt;fml:E_REC_COUNT>{ data($req/contractDescriptionsNo) }&lt;/fml:E_REC_COUNT>
					else ()
			}
			&lt;fml:E_LOGIN_ID>0&lt;/fml:E_LOGIN_ID>
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME>{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/userDescription/microbranch)
					then &lt;fml:U_MICROBRANCH>{ data($req/userDescription/microbranch) }&lt;/fml:U_MICROBRANCH>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetFirstContractDescriptionsRequest($body/m:getFirstContractDescriptions) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>