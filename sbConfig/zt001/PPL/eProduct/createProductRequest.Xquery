<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapcreateProductRequest($req as element(m:createProduct))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/userDescription/microbranch)
					then &lt;fml:E_MICRO_BRANCH>{ data($req/userDescription/microbranch) }&lt;/fml:E_MICRO_BRANCH>
					else ()
			}
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME>{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/product/serviceCode)
					then &lt;fml:B_KOD_USLUGI>{ data($req/product/serviceCode) }&lt;/fml:B_KOD_USLUGI>
					else ()
			}
			{
				if($req/product/paymentPlan)
					then &lt;fml:B_PLAN_OPLAT>{ data($req/product/paymentPlan) }&lt;/fml:B_PLAN_OPLAT>
					else ()
			}
			{
				if($req/product/productKindNo)
					then &lt;fml:B_IRODZAJ_RACH>{ data($req/product/productKindNo) }&lt;/fml:B_IRODZAJ_RACH>
					else ()
			}
			{
				if($req/product/subtype)
					then &lt;fml:B_ITYP>{ data($req/product/subtype) }&lt;/fml:B_ITYP>
					else ()
			}
			{
				if($req/product/description)
					then &lt;fml:B_OPIS_RACH>{ data($req/product/description) }&lt;/fml:B_OPIS_RACH>
					else ()
			}
			{
				if($req/product/shortDescription)
					then &lt;fml:B_SKROT_OPISU>{ data($req/product/shortDescription) }&lt;/fml:B_SKROT_OPISU>
					else ()
			}
			{
				if($req/product/accountTypeId)
					then &lt;fml:B_TYP_RACH>{ data($req/product/accountTypeId) }&lt;/fml:B_TYP_RACH>
					else ()
			}
			{
				if($req/product/productKind)
					then &lt;fml:B_RODZAJ_RACH>{ data($req/product/productKind) }&lt;/fml:B_RODZAJ_RACH>
					else ()
			}
			{
				if($req/product/customerType)
					then if (fn:string($req/product/customerType) != '')
						then &lt;fml:B_OSOBA>{ data($req/product/customerType) }&lt;/fml:B_OSOBA>
						else &lt;fml:B_OSOBA nil="true"/>
					else ()
			}
			{
				if($req/product/elOption)
					then &lt;fml:B_OPCJE_EL>{ data($req/product/elOption) }&lt;/fml:B_OPCJE_EL>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapcreateProductRequest($body/m:createProduct) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>