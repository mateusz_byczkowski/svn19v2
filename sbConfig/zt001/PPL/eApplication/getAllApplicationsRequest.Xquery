<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetAllApplicationsRequest($req as element(m:getAllApplications))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/pesel)
					then &lt;fml:B_NR_DOK>{ data($req/pesel) }&lt;/fml:B_NR_DOK>
					else ()
			}
			{
				if($req/cif)
					then &lt;fml:E_CIF_NUMBER>{ data($req/cif) }&lt;/fml:E_CIF_NUMBER>
					else ()
			}
			&lt;fml:B_RODZ_DOK>S&lt;/fml:B_RODZ_DOK>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetAllApplicationsRequest($body/m:getAllApplications) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>