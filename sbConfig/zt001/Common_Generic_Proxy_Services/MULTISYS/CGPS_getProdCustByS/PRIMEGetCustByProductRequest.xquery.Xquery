<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustByProduct($fml as element(fml:FML32))
	as element(m:PRIMEGetCustByProduct) {
		&lt;m:PRIMEGetCustByProduct>
			{
				if($fml/fml:CI_ID_WEW_PRAC)
					then &lt;m:CI_ID_WEW_PRAC>{ data($fml/fml:CI_ID_WEW_PRAC) }&lt;/m:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($fml/fml:CI_ID_SPOLKI)
					then &lt;m:CI_ID_SPOLKI>{ data($fml/fml:CI_ID_SPOLKI) }&lt;/m:CI_ID_SPOLKI>
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_TYPE)
					then &lt;m:CI_PRODUCT_TYPE>{ data($fml/fml:CI_PRODUCT_TYPE) }&lt;/m:CI_PRODUCT_TYPE>
					else ()
			}
			{
				if($fml/fml:DC_NR_RACHUNKU)
					then &lt;m:DC_NR_RACHUNKU>{ data($fml/fml:DC_NR_RACHUNKU) }&lt;/m:DC_NR_RACHUNKU>
					else ()
			}
			{
				if($fml/fml:CI_ID_SYS)
					then &lt;m:CI_ID_SYS>{ data($fml/fml:CI_ID_SYS) }&lt;/m:CI_ID_SYS>
					else ()
			}
			{
				if($fml/fml:CI_OPCJA)
					then &lt;m:CI_OPCJA>{ data($fml/fml:CI_OPCJA) }&lt;/m:CI_OPCJA>
					else ()
			}			
		&lt;/m:PRIMEGetCustByProduct>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapPRIMEGetCustByProduct($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>