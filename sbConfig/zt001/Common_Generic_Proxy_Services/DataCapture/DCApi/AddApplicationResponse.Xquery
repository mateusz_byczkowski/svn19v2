<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-10</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace dcap = "http://bzwbk.com/services/dcapi/";
declare namespace s = "http://bzwbk.com/services/dcapi/structs/";

declare function local:fault($faultString as xs:string, $detail as element()) as element(soap-env:Fault) {
	&lt;soap-env:Fault>
		&lt;faultcode>soapenv:Server.userException&lt;/faultcode> 
		&lt;faultstring>{ $faultString }&lt;/faultstring> 
		&lt;detail>{ $detail }&lt;/detail>
	&lt;/soap-env:Fault>
};

declare function local:ValidationException($ve as element()) as element() {
	&lt;s:ValidationException>
	{
		$ve/*
	}
	&lt;/s:ValidationException>
};

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
{
	typeswitch($body/dcap:addApplicationResponse/*)
	case $m as element(validationException)
		return local:fault("com.bzwbk.services.dcapi.structs.ValidationException", local:ValidationException($m))
	case element(internalApiException)
		return local:fault("InternalApiException", element s:InternalApiException {})
	case element(result)
		return $body/dcap:addApplicationResponse
	default
		return &lt;error>unknown result type&lt;/error>
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>