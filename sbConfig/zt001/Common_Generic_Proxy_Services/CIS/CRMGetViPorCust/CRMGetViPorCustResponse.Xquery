<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetViPorCustResponse($fml as element(fml:FML32))
	as element(m:CRMGetViPorCustResponse) {
		&lt;m:CRMGetViPorCustResponse>
			{
				let $CI_NUMER_PACZKI := $fml/fml:CI_NUMER_PACZKI return
					if($CI_NUMER_PACZKI)
						then &lt;m:NumerPaczki>{ data($CI_NUMER_PACZKI) }&lt;/m:NumerPaczki>
						else ()
			}
			{
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST
				let $DC_NUMER_ODDZIALU := $fml/fml:DC_NUMER_ODDZIALU
				let $CI_STATUS_GIODO := $fml/fml:CI_STATUS_GIODO
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $CI_WIECEJ_ADDR_KORESP := $fml/fml:CI_WIECEJ_ADDR_KORESP
				let $DC_IMIE_I_NAZWISKO_ALT := $fml/fml:DC_IMIE_I_NAZWISKO_ALT
				let $DC_ULICA_ADRES_ALT := $fml/fml:DC_ULICA_ADRES_ALT
				let $DC_NR_POSES_LOKALU_ADRES_ALT := $fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT
				let $DC_MIASTO_ADRES_ALT := $fml/fml:DC_MIASTO_ADRES_ALT
				let $DC_KOD_POCZTOWY_ADRES_ALT := $fml/fml:DC_KOD_POCZTOWY_ADRES_ALT
				let $DC_NR_TELEFONU := $fml/fml:DC_NR_TELEFONU
				let $DC_NR_TELEF_KOMORKOWEGO := $fml/fml:DC_NR_TELEF_KOMORKOWEGO
				let $CI_NR_TELEFONU_SMS := $fml/fml:CI_NR_TELEFONU_SMS
				let $DC_ADRES_E_MAIL := $fml/fml:DC_ADRES_E_MAIL
				let $CI_RELACJA := $fml/fml:CI_RELACJA
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $CI_STATUS := $fml/fml:CI_STATUS
				let $CI_KLASA_OBSLUGI_AKT := $fml/fml:CI_KLASA_OBSLUGI_AKT
				let $CI_SKP_PRACOWNIKA_REJ := $fml/fml:CI_SKP_PRACOWNIKA_REJ
				let $CI_TYP_ZMIANY := $fml/fml:CI_TYP_ZMIANY
				let $CI_VIP := $fml/fml:CI_VIP
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:CRMGetViPorCustKlient>
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko>{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko>
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie>{ data($DC_IMIE[$p]) }&lt;/m:Imie>
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna>{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna>
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI[$p])
							then &lt;m:KlasaObslugi>{ data($CI_KLASA_OBSLUGI[$p]) }&lt;/m:KlasaObslugi>
						else ()
					}
					{
						if($DC_ULICA_DANE_PODST[$p])
							then &lt;m:UlicaDanePodst>{ data($DC_ULICA_DANE_PODST[$p]) }&lt;/m:UlicaDanePodst>
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_DANE_PODST[$p])
							then &lt;m:NrPosesLokaluDanePodst>{ data($DC_NR_POSES_LOKALU_DANE_PODST[$p]) }&lt;/m:NrPosesLokaluDanePodst>
						else ()
					}
					{
						if($DC_MIASTO_DANE_PODST[$p])
							then &lt;m:MiastoDanePodst>{ data($DC_MIASTO_DANE_PODST[$p]) }&lt;/m:MiastoDanePodst>
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_DANE_PODST[$p])
							then &lt;m:KodPocztowyDanePodst>{ data($DC_KOD_POCZTOWY_DANE_PODST[$p]) }&lt;/m:KodPocztowyDanePodst>
						else ()
					}
					{
						if($DC_NUMER_ODDZIALU[$p])
							then &lt;m:NumerOddzialu>{ data($DC_NUMER_ODDZIALU[$p]) }&lt;/m:NumerOddzialu>
						else ()
					}
					{
						if($CI_STATUS_GIODO[$p])
							then &lt;m:StatusGiodo>{ data($CI_STATUS_GIODO[$p]) }&lt;/m:StatusGiodo>
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then &lt;m:UdostepGrupa>{ data($CI_UDOSTEP_GRUPA[$p]) }&lt;/m:UdostepGrupa>
						else ()
					}
					{
						if($CI_WIECEJ_ADDR_KORESP[$p])
							then &lt;m:WiecejAddrKoresp>{ data($CI_WIECEJ_ADDR_KORESP[$p]) }&lt;/m:WiecejAddrKoresp>
						else ()
					}
					{
						if($DC_IMIE_I_NAZWISKO_ALT[$p])
							then &lt;m:ImieINazwiskoAlt>{ data($DC_IMIE_I_NAZWISKO_ALT[$p]) }&lt;/m:ImieINazwiskoAlt>
						else ()
					}
					{
						if($DC_ULICA_ADRES_ALT[$p])
							then &lt;m:UlicaAdresAlt>{ data($DC_ULICA_ADRES_ALT[$p]) }&lt;/m:UlicaAdresAlt>
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_ADRES_ALT[$p])
							then &lt;m:NrPosesLokaluAdresAlt>{ data($DC_NR_POSES_LOKALU_ADRES_ALT[$p]) }&lt;/m:NrPosesLokaluAdresAlt>
						else ()
					}
					{
						if($DC_MIASTO_ADRES_ALT[$p])
							then &lt;m:MiastoAdresAlt>{ data($DC_MIASTO_ADRES_ALT[$p]) }&lt;/m:MiastoAdresAlt>
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_ADRES_ALT[$p])
							then &lt;m:KodPocztowyAdresAlt>{ data($DC_KOD_POCZTOWY_ADRES_ALT[$p]) }&lt;/m:KodPocztowyAdresAlt>
						else ()
					}
					{
						if($DC_NR_TELEFONU[$p])
							then &lt;m:NrTelefonu>{ data($DC_NR_TELEFONU[$p]) }&lt;/m:NrTelefonu>
						else ()
					}
					{
						if($DC_NR_TELEF_KOMORKOWEGO[$p])
							then &lt;m:NrTelefKomorkowego>{ data($DC_NR_TELEF_KOMORKOWEGO[$p]) }&lt;/m:NrTelefKomorkowego>
						else ()
					}
					{
						if($CI_NR_TELEFONU_SMS[$p])
							then &lt;m:NrTelefonuSms>{ data($CI_NR_TELEFONU_SMS[$p]) }&lt;/m:NrTelefonuSms>
						else ()
					}
					{
						if($DC_ADRES_E_MAIL[$p])
							then &lt;m:AdresEMail>{ data($DC_ADRES_E_MAIL[$p]) }&lt;/m:AdresEMail>
						else ()
					}
					{
						if($CI_RELACJA[$p])
							then &lt;m:Relacja>{ data($CI_RELACJA[$p]) }&lt;/m:Relacja>
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then &lt;m:IdPortfela>{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela>
						else ()
					}
					{
						if($CI_STATUS[$p])
							then &lt;m:Status>{ data($CI_STATUS[$p]) }&lt;/m:Status>
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI_AKT[$p])
							then &lt;m:KlasaObslugiAkt>{ data($CI_KLASA_OBSLUGI_AKT[$p]) }&lt;/m:KlasaObslugiAkt>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_REJ[$p])
							then &lt;m:SkpPracownikaRej>{ data($CI_SKP_PRACOWNIKA_REJ[$p]) }&lt;/m:SkpPracownikaRej>
						else ()
					}
					{
						if($CI_TYP_ZMIANY[$p])
							then &lt;m:TypZmiany>{ data($CI_TYP_ZMIANY[$p]) }&lt;/m:TypZmiany>
						else ()
					}
					{
						if($CI_VIP[$p])
							then &lt;m:Vip>{ data($CI_VIP[$p]) }&lt;/m:Vip>
						else ()
					}
					&lt;/m:CRMGetViPorCustKlient>
			}
		&lt;/m:CRMGetViPorCustResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetViPorCustResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>