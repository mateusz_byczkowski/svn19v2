<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetDictCatResponse($fml as element(fml:FML32))
	as element(m:CRMGetDictCatResponse) {
		&lt;m:CRMGetDictCatResponse>
			{

				let $CI_ID_SLOWNIKA := $fml/fml:CI_ID_SLOWNIKA
				let $CI_NAZWA_SLOWNIKA := $fml/fml:CI_NAZWA_SLOWNIKA
				for $it at $p in $fml/fml:CI_ID_SLOWNIKA
				return
					&lt;m:CRMGetDictCatKategoria>
					{
						if($CI_ID_SLOWNIKA[$p])
							then &lt;m:IdSlownika>{ data($CI_ID_SLOWNIKA[$p]) }&lt;/m:IdSlownika>
						else ()
					}
					{
						if($CI_NAZWA_SLOWNIKA[$p])
							then &lt;m:NazwaSlownika>{ data($CI_NAZWA_SLOWNIKA[$p]) }&lt;/m:NazwaSlownika>
						else ()
					}
					&lt;/m:CRMGetDictCatKategoria>
			}

		&lt;/m:CRMGetDictCatResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetDictCatResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>