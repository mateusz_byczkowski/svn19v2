<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:T40586CR	MMa  2010-01-04  Teet 40586 CR :)

declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustomersResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustomersResponse) {
		&lt;m:CRMGetCustomersResponse>
			{
				if($fml/fml:CI_NUMER_PACZKI)
					then &lt;m:NumerPaczki>{ data($fml/fml:CI_NUMER_PACZKI) }&lt;/m:NumerPaczki>
					else ()
			}
(:T40586CR - start:)
			{
				if($fml/fml:CI_NUMER_PACZKI_STR)
					then &lt;m:NumerPaczkiStr>{ data($fml/fml:CI_NUMER_PACZKI_STR) }&lt;/m:NumerPaczkiStr>
					else ()
			}
			{
				if($fml/fml:CI_CONT)
					then &lt;m:Cont>{ data($fml/fml:CI_CONT) }&lt;/m:Cont>
					else ()
			}
(:T40586CR - koniec:)
			{

				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST
				let $DC_NUMER_ODDZIALU := $fml/fml:DC_NUMER_ODDZIALU
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_STATUS_GIODO := $fml/fml:CI_STATUS_GIODO
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL
				let $DC_NR_DOWODU_REGON := $fml/fml:DC_NR_DOWODU_REGON
				let $CI_WIECEJ_ADDR_KORESP := $fml/fml:CI_WIECEJ_ADDR_KORESP
				let $DC_IMIE_I_NAZWISKO_ALT := $fml/fml:DC_IMIE_I_NAZWISKO_ALT
				let $DC_ULICA_ADRES_ALT := $fml/fml:DC_ULICA_ADRES_ALT
				let $DC_NR_POSES_LOKALU_ADRES_ALT := $fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT
				let $DC_MIASTO_ADRES_ALT := $fml/fml:DC_MIASTO_ADRES_ALT
				let $DC_KOD_POCZTOWY_ADRES_ALT := $fml/fml:DC_KOD_POCZTOWY_ADRES_ALT
				let $DC_NR_TELEFONU := $fml/fml:DC_NR_TELEFONU
				let $DC_NR_TELEF_KOMORKOWEGO := $fml/fml:DC_NR_TELEF_KOMORKOWEGO
				let $CI_NR_TELEFONU_SMS := $fml/fml:CI_NR_TELEFONU_SMS
				let $DC_ADRES_E_MAIL := $fml/fml:DC_ADRES_E_MAIL
				let $CI_RELACJA := $fml/fml:CI_RELACJA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $CI_CESJA_UPRAWNIEN := $fml/fml:CI_CESJA_UPRAWNIEN
				let $CI_SKP_PRACOWNIKA_UPR := $fml/fml:CI_SKP_PRACOWNIKA_UPR
				let $CI_DATA_CESJI_UPRAWNIEN := $fml/fml:CI_DATA_CESJI_UPRAWNIEN
				let $CI_KLIENT_ZNANY := $fml/fml:CI_KLIENT_ZNANY
				let $DC_NIP := $fml/fml:DC_NIP
				let $DC_SEGMENT_MARKETINGOWY := $fml/fml:DC_SEGMENT_MARKETINGOWY
				let $DC_PODSEGMENT_MARK := $fml/fml:DC_PODSEGMENT_MARK
				let $CI_NIK := $fml/fml:CI_NIK

				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:CRMGetCustomersCRMKlient>
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta>
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna>{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna>
						else ()
					}
					{
						if($DC_ULICA_DANE_PODST[$p])
							then &lt;m:UlicaDanePodst>{ data($DC_ULICA_DANE_PODST[$p]) }&lt;/m:UlicaDanePodst>
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_DANE_PODST[$p])
							then &lt;m:NrPosesLokaluDanePodst>{ data($DC_NR_POSES_LOKALU_DANE_PODST[$p]) }&lt;/m:NrPosesLokaluDanePodst>
						else ()
					}
					{
						if($DC_MIASTO_DANE_PODST[$p])
							then &lt;m:MiastoDanePodst>{ data($DC_MIASTO_DANE_PODST[$p]) }&lt;/m:MiastoDanePodst>
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_DANE_PODST[$p])
							then &lt;m:KodPocztowyDanePodst>{ data($DC_KOD_POCZTOWY_DANE_PODST[$p]) }&lt;/m:KodPocztowyDanePodst>
						else ()
					}
					{
						if($DC_NUMER_ODDZIALU[$p])
						     then if (data($DC_NUMER_ODDZIALU[$p]) = '997')
                                                                then &lt;m:NumerOddzialu>10066&lt;/m:NumerOddzialu> 
                                                             else if (data($DC_NUMER_ODDZIALU[$p]) = '996')
                                                                then &lt;m:NumerOddzialu>10072&lt;/m:NumerOddzialu> 
                                                             else if (data($DC_NUMER_ODDZIALU[$p]) = '995')
                                                                then &lt;m:NumerOddzialu>10017&lt;/m:NumerOddzialu> 
                                                             else if (data($DC_NUMER_ODDZIALU[$p]) = '992')
                                                                then &lt;m:NumerOddzialu>10076&lt;/m:NumerOddzialu>          
                                                     else  &lt;m:NumerOddzialu>{ data($DC_NUMER_ODDZIALU[$p]) }&lt;/m:NumerOddzialu>
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie>{ data($DC_IMIE[$p]) }&lt;/m:Imie>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko>{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko>
						else ()
					}
					{
						if($CI_STATUS_GIODO[$p])
							then &lt;m:StatusGiodo>{ data($CI_STATUS_GIODO[$p]) }&lt;/m:StatusGiodo>
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then &lt;m:UdostepGrupa>{ data($CI_UDOSTEP_GRUPA[$p]) }&lt;/m:UdostepGrupa>
						else ()
					}
					{
						if($DC_NR_PESEL[$p])
							then &lt;m:NrPesel>{ data($DC_NR_PESEL[$p]) }&lt;/m:NrPesel>
						else ()
					}
					{
						if($DC_NR_DOWODU_REGON[$p])
							then &lt;m:NrDowoduRegon>{ data($DC_NR_DOWODU_REGON[$p]) }&lt;/m:NrDowoduRegon>
						else ()
					}
					{
						if($CI_WIECEJ_ADDR_KORESP[$p])
							then &lt;m:WiecejAddrKoresp>{ data($CI_WIECEJ_ADDR_KORESP[$p]) }&lt;/m:WiecejAddrKoresp>
						else ()
					}
					{
						if($DC_IMIE_I_NAZWISKO_ALT[$p])
							then &lt;m:ImieINazwiskoAlt>{ data($DC_IMIE_I_NAZWISKO_ALT[$p]) }&lt;/m:ImieINazwiskoAlt>
						else ()
					}
					{
						if($DC_ULICA_ADRES_ALT[$p])
							then &lt;m:UlicaAdresAlt>{ data($DC_ULICA_ADRES_ALT[$p]) }&lt;/m:UlicaAdresAlt>
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_ADRES_ALT[$p])
							then &lt;m:NrPosesLokaluAdresAlt>{ data($DC_NR_POSES_LOKALU_ADRES_ALT[$p]) }&lt;/m:NrPosesLokaluAdresAlt>
						else ()
					}
					{
						if($DC_MIASTO_ADRES_ALT[$p])
							then &lt;m:MiastoAdresAlt>{ data($DC_MIASTO_ADRES_ALT[$p]) }&lt;/m:MiastoAdresAlt>
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_ADRES_ALT[$p])
							then &lt;m:KodPocztowyAdresAlt>{ data($DC_KOD_POCZTOWY_ADRES_ALT[$p]) }&lt;/m:KodPocztowyAdresAlt>
						else ()
					}
					{
						if($DC_NR_TELEFONU[$p])
							then &lt;m:NrTelefonu>{ data($DC_NR_TELEFONU[$p]) }&lt;/m:NrTelefonu>
						else ()
					}
					{
						if($DC_NR_TELEF_KOMORKOWEGO[$p])
							then &lt;m:NrTelefKomorkowego>{ data($DC_NR_TELEF_KOMORKOWEGO[$p]) }&lt;/m:NrTelefKomorkowego>
						else ()
					}
					{
						if($CI_NR_TELEFONU_SMS[$p])
							then &lt;m:NrTelefonuSms>{ data($CI_NR_TELEFONU_SMS[$p]) }&lt;/m:NrTelefonuSms>
						else ()
					}
					{
						if($DC_ADRES_E_MAIL[$p])
							then &lt;m:AdresEMail>{ data($DC_ADRES_E_MAIL[$p]) }&lt;/m:AdresEMail>
						else ()
					}
					{
						if($CI_RELACJA[$p])
							then &lt;m:Relacja>{ data($CI_RELACJA[$p]) }&lt;/m:Relacja>
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI[$p])
							then &lt;m:KlasaObslugi>{ data($CI_KLASA_OBSLUGI[$p]) }&lt;/m:KlasaObslugi>
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then &lt;m:IdPortfela>{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela>
						else ()
					}
					{
						if($CI_CESJA_UPRAWNIEN[$p])
							then &lt;m:CesjaUprawnien>{ data($CI_CESJA_UPRAWNIEN[$p]) }&lt;/m:CesjaUprawnien>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_UPR[$p])
							then &lt;m:SkpPracownikaUpr>{ data($CI_SKP_PRACOWNIKA_UPR[$p]) }&lt;/m:SkpPracownikaUpr>
						else ()
					}
					{
						if($CI_DATA_CESJI_UPRAWNIEN[$p])
							then &lt;m:DataCesjiUprawnien>{ data($CI_DATA_CESJI_UPRAWNIEN[$p]) }&lt;/m:DataCesjiUprawnien>
						else ()
					}
					{
						if($CI_KLIENT_ZNANY[$p])
							then &lt;m:KlientZnany>{ data($CI_KLIENT_ZNANY[$p]) }&lt;/m:KlientZnany>
						else ()
					}
					{
						if($DC_NIP[$p])
							then &lt;m:Nip>{ data($DC_NIP[$p]) }&lt;/m:Nip>
						else ()
					}
					{
						if($DC_SEGMENT_MARKETINGOWY[$p])
							then &lt;m:SegmentMarketingowy>{ data($DC_SEGMENT_MARKETINGOWY[$p]) }&lt;/m:SegmentMarketingowy>
						else ()
					}
					{
						if($DC_PODSEGMENT_MARK[$p])
							then &lt;m:PodsegmentMark>{ data($DC_PODSEGMENT_MARK[$p]) }&lt;/m:PodsegmentMark>
						else ()
					}
					{
						if($CI_NIK[$p])
							then &lt;m:Nik>{ data($CI_NIK[$p]) }&lt;/m:Nik>
						else ()
					}
					&lt;/m:CRMGetCustomersCRMKlient>
			}

		&lt;/m:CRMGetCustomersResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetCustomersResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>