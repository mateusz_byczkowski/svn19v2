<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCusByPrdsRequest($req as element(m:CRMGetCusByPrdsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				for $v in $req/m:Decyl
				return
					&lt;fml:CI_DECYL>{ data($v) }&lt;/fml:CI_DECYL>
			}
			{
				for $v in $req/m:WarunekDecyl
				return
					&lt;fml:CI_WARUNEK_DECYL>{ data($v) }&lt;/fml:CI_WARUNEK_DECYL>
			}
			{
				for $v in $req/m:KodProduktu
				return
					&lt;fml:CI_KOD_PRODUKTU>{ data($v) }&lt;/fml:CI_KOD_PRODUKTU>
			}
			{
				for $v in $req/m:Posiada
				return
					&lt;fml:CI_POSIADA>{ data($v) }&lt;/fml:CI_POSIADA>
			}
			{
				for $v in $req/m:NazwaPola
				return
					&lt;fml:CI_NAZWA_POLA>{ data($v) }&lt;/fml:CI_NAZWA_POLA>
			}
			{
				for $v in $req/m:Operator
				return
					&lt;fml:CI_OPERATOR>{ data($v) }&lt;/fml:CI_OPERATOR>
			}
			{
				for $v in $req/m:Wartosc
				return
					&lt;fml:CI_WARTOSC>{ data($v) }&lt;/fml:CI_WARTOSC>
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER>{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI>{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI>
					else ()
			}
			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI>{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetCusByPrdsRequest($body/m:CRMGetCusByPrdsRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>