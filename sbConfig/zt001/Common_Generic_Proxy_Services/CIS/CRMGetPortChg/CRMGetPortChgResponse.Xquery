<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPortChgResponse($fml as element(fml:FML32))
	as element(m:CRMGetPortChgResponse) {
		&lt;m:CRMGetPortChgResponse>
			{
				let $CI_NUMER_PACZKI := $fml/fml:CI_NUMER_PACZKI return
					if($CI_NUMER_PACZKI)
						then &lt;m:NumerPaczki>{ data($CI_NUMER_PACZKI) }&lt;/m:NumerPaczki>
						else ()
			}
                        {
                                let $CI_NUMER_PACZKI_STR := $fml/fml:CI_NUMER_PACZKI_STR return
                                        if($CI_NUMER_PACZKI_STR)
                                                then &lt;m:NumerPaczkiStr>{ data($CI_NUMER_PACZKI_STR) }&lt;/m:NumerPaczkiStr>
                                                else ()
                        }
			{
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $CI_SKP_PRACOWNIKA_AKT := $fml/fml:CI_SKP_PRACOWNIKA_AKT
				let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
				let $CI_ID_PORTFELA_AKT := $fml/fml:CI_ID_PORTFELA_AKT
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $DC_NUMER_ODDZIALU := $fml/fml:DC_NUMER_ODDZIALU
				let $CI_NOWY_NUMER_ODDZIALU := $fml/fml:CI_NOWY_NUMER_ODDZIALU
				let $CI_DATA := $fml/fml:CI_DATA
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:PortChange>
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_AKT[$p])
							then &lt;m:SkpPracownikaAkt>{ data($CI_SKP_PRACOWNIKA_AKT[$p]) }&lt;/m:SkpPracownikaAkt>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA[$p])
							then &lt;m:SkpPracownika>{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:SkpPracownika>
						else ()
					}
					{
						if($CI_ID_PORTFELA_AKT[$p])
							then &lt;m:IdPortfelaAkt>{ data($CI_ID_PORTFELA_AKT[$p]) }&lt;/m:IdPortfelaAkt>
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then &lt;m:IdPortfela>{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela>
						else ()
					}
					{
						if($DC_NUMER_ODDZIALU[$p])
							then &lt;m:NumerOddzialu>{ data($DC_NUMER_ODDZIALU[$p]) }&lt;/m:NumerOddzialu>
						else ()
					}
					{
						if($CI_NOWY_NUMER_ODDZIALU[$p])
							then &lt;m:NowyNumerOddzialu>{ data($CI_NOWY_NUMER_ODDZIALU[$p]) }&lt;/m:NowyNumerOddzialu>
						else ()
					}
					{
						if($CI_DATA[$p])
							then &lt;m:Data>{ data($CI_DATA[$p]) }&lt;/m:Data>
						else ()
					}
					&lt;/m:PortChange>
			}
		&lt;/m:CRMGetPortChgResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetPortChgResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>