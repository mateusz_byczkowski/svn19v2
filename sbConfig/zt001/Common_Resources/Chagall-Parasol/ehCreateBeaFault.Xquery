<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Tworzenie komunikatu Fault na podstawie błędu zwróconego przez BEA ALSB (błąd wewnętrzny)</con:description>
  <con:xquery>declare namespace ctx = "http://www.bea.com/wli/sb/context";
declare namespace cl = "http://sygnity.pl/functions";

declare variable $fault as element(ctx:fault) external;
declare variable $exception as xs:string external;

declare function cl:messageConvert($errorNumber as xs:string) as xs:string {
    if(fn:starts-with($errorNumber, 'BEA-380')) then 'Wystąpił błąd w komunikacji pomiędzy systemami. Realizowany proces nie został zakończony poprawnie. Spróbuj ponownie w innym terminie'
    else if(fn:starts-with($errorNumber, 'BEA-382')) then 'Błąd wewnętrzny serwisu'
    else if(fn:starts-with($errorNumber, 'BEA-386')) then 'Błąd zabezpieczeń'
    else if(fn:starts-with($errorNumber, 'BEA-3945')) then 'Błąd w komunikacji z serwerem UDDI'
    else 'Nieznany błąd. Skontaktuj się z administratorem'
};

&lt;soap-env:Fault xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
	&lt;faultcode>K00319&lt;/faultcode>
	&lt;faultstring>{$exception}: { cl:messageConvert($fault/ctx:errorCode) }&lt;/faultstring>
	&lt;detail>
		&lt;e:ServiceFailException xmlns:e="urn:errors.hlbsentities.be.dcl">
			&lt;e:exceptionItem>
				&lt;e:errorCode1>1&lt;/e:errorCode1>
				&lt;e:errorCode2?>&lt;/e:errorCode2>
				&lt;e:errorDescription>{$exception}: { cl:messageConvert($fault/ctx:errorCode) }&lt;/e:errorDescription>
			&lt;/e:exceptionItem>
		&lt;/e:ServiceFailException>
	&lt;/detail>
&lt;/soap-env:Fault></con:xquery>
</con:xqueryEntry>