<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx = "http://www.bea.com/wli/sb/context";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace cl = "http://sygnity.pl/functions";
declare namespace crw = "http://bzwbk.com/crw/services/insurance/";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare function cl:crw2NfeCodeConvert($crwCode as xs:string*, $codes as element(cl:codes)) as xs:string? {
	let $allCodes :=
		for $code in $codes/cl:code
		where $code/@any-code = $crwCode
		return data($code/@nfe-code)
	return 
		if($allCodes[1] != '') then
			$allCodes[1]
		else
			'K00318'
};

declare function cl:createValidationList($exception as element(crw:ValidationException)) as element(err:ServiceFailException) {
	&lt;e:ServiceFailException xmlns:e="urn:errors.hlbsentities.be.dcl">
	{
		for $e in $exception/crw:errors
		return
			&lt;e:exceptionItem>
				&lt;e:errorCode1>1&lt;/e:errorCode1>
				&lt;e:errorCode2?>&lt;/e:errorCode2>
				&lt;e:errorDescription>{ data($e/crw:msg) }: { data($e/crw:path) }&lt;/e:errorDescription>
		    &lt;/e:exceptionItem>
	}
	&lt;/e:ServiceFailException>
};

declare variable $fault as element(soap-env:Fault) external;
declare variable $exception as xs:string external;
declare variable $codes as element(cl:codes) external;

&lt;soap-env:Fault xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
	&lt;faultcode>{ 
		let $code :=
			if($fault/detail/crw:IllegalApplicationAccessException) then 'ACCESS'
			else if($fault/detail/crw:InternalException) then 'INTERNAL'
			else if($fault/detail/crw:ArgumentException) then 'BAD_PARAM'
			else if($fault/detail/crw:ValidationException) then 'VALIDATION'
			else ''
		return cl:crw2NfeCodeConvert($code, $codes)
	}&lt;/faultcode>
	&lt;faultstring>{$exception}: { data($fault/faultstring) }&lt;/faultstring>
	&lt;detail>
	{
		if($fault/detail/crw:ValidationException) then
			cl:createValidationList($fault/detail/crw:ValidationException)
		else
			&lt;e:ServiceFailException xmlns:e="urn:errors.hlbsentities.be.dcl">
				&lt;e:exceptionItem>
					&lt;e:errorCode1>1&lt;/e:errorCode1>
					&lt;e:errorCode2?>&lt;/e:errorCode2>
					&lt;e:errorDescription>{$exception}: { data($fault/faultstring) }&lt;/e:errorDescription>
			    &lt;/e:exceptionItem>
			&lt;/e:ServiceFailException>
	}
	&lt;/detail>
&lt;/soap-env:Fault></con:xquery>
</con:xqueryEntry>