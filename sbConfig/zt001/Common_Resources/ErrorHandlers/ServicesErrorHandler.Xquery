<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/ewnioski/faults/";
declare variable $body external;

declare function local:fault($faultString as xs:string, $detail as element()) as element(soap-env:Fault) {
		&lt;soap-env:Fault>
			&lt;faultcode>soapenv:Server.userException&lt;/faultcode>
			&lt;faultstring>{ $faultString }&lt;/faultstring> 
			&lt;detail>{ $detail }&lt;/detail>
		&lt;/soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string) as element()* {
	&lt;ErrorCode1>{ $errorCode1 }&lt;/ErrorCode1>,
	&lt;ErrorCode2>{ $errorCode2 }&lt;/ErrorCode2>
};
&lt;soap-env:Body>
	&lt;soap-env:Body>
	{
		(: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
		(: let $reason := fn:substring-before(fn:substring-after(fn:substring-before($body/soap-env:Body/soap-env:Fault/detail/ctx:fault/ctx:reason, ":"), "("), ")")
		let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($body/soap-env:Body/soap-env:Fault/detail/ctx:fault/ctx:reason, ":"), ":"), ":") :)
                
                let $reason := fn:substring-before(fn:substring-after(fn:substring-before($body/ctx:fault/ctx:reason, ":"), "("), ")")
		let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($body/ctx:fault/ctx:reason, ":"), ":"), ":")
		return
			if($reason = "13") then
				local:fault("com.bzwbk.services.ewnioski.faults.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode) })
			else if($reason = "11") then
				if($urcode = "101") then
					local:fault("com.bzwbk.services.ewnioski.faults.FMLBufferException", element f:FMLBufferException { local:errors($reason, $urcode) })
				else if($urcode = "102") then
					local:fault("com.bzwbk.services.ewnioski.faults.WrongInputException", element f:WrongInputException { local:errors($reason, $urcode) })
				else if($urcode = "103") then
					local:fault("com.bzwbk.services.ewnioski.faults.NoDataException", element f:NoDataException { local:errors($reason, $urcode) })
				else if($urcode = "107") then
					local:fault("com.bzwbk.services.ewnioski.faults.RecordNotFoundException", element f:RecordNotFoundException { local:errors($reason, $urcode) })
				else if($urcode = "108") then
					local:fault("com.bzwbk.services.ewnioski.faults.AccessDeniedException", element f:AccessDeniedException { local:errors($reason, $urcode) })
				else if($urcode = "109") then
					local:fault("com.bzwbk.services.ewnioski.faults.RecordAlreadyExistsException", element f:RecordAlreadyExistsException { local:errors($reason, $urcode) })
				else if($urcode = "110") then
					local:fault("com.bzwbk.services.ewnioski.faults.ReverseRelationAlreadyExistsException", element f:ReverseRelationAlreadyExistsException { local:errors($reason, $urcode) })
				else if($urcode = "111") then
					local:fault("com.bzwbk.services.ewnioski.faults.InternalCustomerExistsException", element f:InternalCustomerExistsException { local:errors($reason, $urcode) })
				else if($urcode = "112") then
					local:fault("com.bzwbk.services.ewnioski.faults.ExternalCustomerExistsException", element f:ExternalCustomerExistsException { local:errors($reason, $urcode) })
				else if($urcode = "113") then
					local:fault("com.bzwbk.services.ewnioski.faults.IncoherentDataException", element f:IncoherentDataException { local:errors($reason, $urcode) })
				else if($urcode = "114") then
					local:fault("com.bzwbk.services.ewnioski.faults.OperationNotAllowedException", element f:OperationNotAllowedException { local:errors($reason, $urcode) })
				else if($urcode = "120") then
					local:fault("com.bzwbk.services.ewnioski.faults.RemoteServiceErrorException", element f:RemoteServiceErrorException { local:errors($reason, $urcode) })
				else if($urcode = "153") then
					local:fault("com.bzwbk.services.ewnioski.faults.PortfChangesExistException", element f:PortfChangesExistException { local:errors($reason, $urcode) })
				else if($urcode = "154") then
					local:fault("com.bzwbk.services.ewnioski.faults.PortfNotEmptyException", element f:PortfNotEmptyException { local:errors($reason, $urcode) })
				else if($urcode = "156") then
					local:fault("com.bzwbk.services.ewnioski.faults.PortfOwnedException", element f:PortfOwnedException { local:errors($reason, $urcode) })
				else
					local:fault("com.bzwbk.services.ewnioski.faults.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode) })
			else
				local:fault("com.bzwbk.services.ewnioski.faults.ServiceException", element f:ServiceException { local:errors($reason, $urcode) })
	}
	&lt;/soap-env:Body>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>