<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAttributesListCnt_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace n01 ="urn:baseauxentities.be.dcl";

declare function xf:getProductAttributesListCnt_resp($fml as element())
    as element() {
	&lt;srv:invokeResponse>
                    &lt;srv:attrCount>
                        &lt;n01:IntegerHolder>
                            	{if(xs:int(data($fml/PT_PACK_SIZE)) ne 0) then
                            	&lt;n01:value>{ data($fml/PT_PACK_SIZE) }&lt;/n01:value>
                            	else ()}
                         &lt;/n01:IntegerHolder>
                    &lt;/srv:attrCount>
    &lt;/srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductAttributesListCnt_resp($fml)</con:xquery>
</con:xqueryEntry>