<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAreaAttrListAvailValues2_resp/";

declare namespace dcl = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:baseauxentities.be.dcl"; 

declare function xf:getProductAreaAttrListAvailValues2_req($entity as element(dcl:ProductAreaAttributes),
$bcd as element(ns1:PageControl)

)
    as element() {
	&lt;FML32>
		&lt;PT_ID_AREA_ATTRIBUTES>{ data($entity/dcl:idProductAreaAttributes) }&lt;/PT_ID_AREA_ATTRIBUTES>
		&lt;PT_PACK_NO>{if  (contains(data($bcd/ns1:navigationKeyValue)," "))
                                                       then 0
                                                      else 
                                                        data($bcd/ns1:navigationKeyValue)
         }&lt;/PT_PACK_NO>
		&lt;PT_PACK_SIZE>{ data($bcd/ns1:pageSize)}&lt;/PT_PACK_SIZE>
		&lt;PT_VALUE>{ data($bcd/ns1:actionCode)}&lt;/PT_VALUE>

               &lt;/FML32>
};

declare variable $entity as element(dcl:ProductAreaAttributes) external;

declare variable $bcd as element(ns1:PageControl) external;


xf:getProductAreaAttrListAvailValues2_req($entity,$bcd )</con:xquery>
</con:xqueryEntry>