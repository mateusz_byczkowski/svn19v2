<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-06</con:description>
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAttributesList_req/";
declare namespace dcl = "urn:baseauxentities.be.dcl";

declare function xf:getProductAttributesList_req ($type as element(dcl:EditHelper), $id as element(dcl:IntegerHolder)) as element(FML32) {
&lt;FML32>
{
if (data($type/dcl:stringBased) eq "PRODUCT_GROUP") 
then &lt;PT_ID_GROUP>{ data( $id/dcl:value ) }&lt;/PT_ID_GROUP>
else if (data($type/dcl:stringBased) eq "PRODUCT_DEFINITION") 
then &lt;PT_ID_DEFINITION>{ data( $id/dcl:value ) }&lt;/PT_ID_DEFINITION>
else if (data($type/dcl:stringBased) eq "PRODUCT_PACKAGE") 
then &lt;PT_ID_PACKAGE>{ data( $id/dcl:value ) }&lt;/PT_ID_PACKAGE>
else if (data($type/dcl:stringBased) eq "PRODUCT_AREA_ATTRIBUTE") 
then &lt;PT_ID_AREA_ATTRIBUTES>{ data( $id/dcl:value ) }&lt;/PT_ID_AREA_ATTRIBUTES>
else if (data($type/dcl:stringBased) eq "PRODUCT_CATEGORY") 
then &lt;PT_ID_CATEGORY>{ data( $id/dcl:value ) }&lt;/PT_ID_CATEGORY>
else ()

}
&lt;/FML32>
};

declare variable $type as element(dcl:EditHelper) external;
declare variable $id as element(dcl:IntegerHolder) external;

&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
{
xf:getProductAttributesList_req($type, $id)
}
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>