<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/empty_response/";
declare namespace dcl = "urn:be.services.dcl";

declare function xf:empty_response()
    as element(dcl:invokeResponse) {
        &lt;dcl:invokeResponse/>
};

xf:empty_response()</con:xquery>
</con:xqueryEntry>