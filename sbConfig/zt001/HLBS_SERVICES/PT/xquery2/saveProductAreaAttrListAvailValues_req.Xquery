<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProductAreaAttrListAvailValues_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:saveProductAreaAttrListAvailValues_req ($entity as element(dcl:ProductAreaAttrListAvailVal)) as element(FML32) {
&lt;FML32>
     &lt;PT_VALUE>{ data( $entity/dcl:value ) }&lt;/PT_VALUE>
     &lt;PT_DESCRIPTION>{ data( $entity/dcl:valueDescription ) }&lt;/PT_DESCRIPTION>
     &lt;PT_CONNECTION_FIELD>{ data( $entity/dcl:connectionField ) }&lt;/PT_CONNECTION_FIELD>
     &lt;PT_ID_AREA_ATTRIBUTES>{ data( $entity/dcl:idProductAreaAttrtibutes ) }&lt;/PT_ID_AREA_ATTRIBUTES>
     
     &lt;PT_ID_AREA_ATTR_LIST_VAL>{ data( $entity/dcl:idProductAreaAttrListAvailVal ) }&lt;/PT_ID_AREA_ATTR_LIST_VAL> 
     &lt;PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP) }&lt;/PT_USER_CHANGE_SKP>
     &lt;PT_DATE_CHANGE>{ data( $entity/dcl:dateChange) }&lt;/PT_DATE_CHANGE>       
&lt;/FML32>
};
declare variable $entity as element(dcl:ProductAreaAttrListAvailVal) external;
xf:saveProductAreaAttrListAvailValues_req($entity)</con:xquery>
</con:xqueryEntry>