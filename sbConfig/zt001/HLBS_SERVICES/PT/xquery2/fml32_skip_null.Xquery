<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "urn:fml32.skip.null";

declare function xf:fml32_skip_null($in as element())
as element() {

&lt;FML32>{
if ( data($in) ) then
for $elt in $in/* return 
if ( data($elt) ) then $elt else ()
else ()
}&lt;/FML32>
};

declare variable $in as element() external;

xf:fml32_skip_null($in)</con:xquery>
</con:xqueryEntry>