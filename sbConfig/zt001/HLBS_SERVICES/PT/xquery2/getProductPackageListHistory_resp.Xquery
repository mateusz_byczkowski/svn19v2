<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductPackageList/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns1 = "urn:productstree.entities.be.dcl";

declare function xf:getProductPackageListHistory_resp($fml as element())
    as element(srv:invokeResponse) {
        &lt;srv:invokeResponse>
            &lt;srv:productPackageListHistory>
            {
            for $i in 1 to count($fml/PT_ID_PACKAGE)
            return
                &lt;ns1:ProductPackageListHistory>
                   &lt;ns1:obligatoryFlag>{ data($fml/PT_OBLIGATORY[$i]) }&lt;/ns1:obligatoryFlag>
                   &lt;ns1:idProductGroup>{ data($fml/PT_ID_GROUP[$i]) }&lt;/ns1:idProductGroup>
                   &lt;ns1:idProductDefinition>{ data($fml/PT_ID_DEFINITION[$i]) }&lt;/ns1:idProductDefinition>
                   &lt;ns1:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }&lt;/ns1:userChangeSKP>
                   &lt;ns1:idProductPackage>{ data($fml/PT_ID_PACKAGE[$i]) }&lt;/ns1:idProductPackage>
                   &lt;ns1:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }&lt;/ns1:dateChange>
                &lt;/ns1:ProductPackageListHistory>
            }
            &lt;/srv:productPackageListHistory>
        &lt;/srv:invokeResponse>
};

declare variable $fml as element(FML32) external;

xf:getProductPackageListHistory_resp($fml)</con:xquery>
</con:xqueryEntry>