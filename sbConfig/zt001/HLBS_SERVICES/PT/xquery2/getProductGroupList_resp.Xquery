<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductGroupList_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace dcl = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";

declare function xf:getProductGroupList_resp ($fml as element(FML32)) as element(srv:invokeResponse) {
&lt;srv:invokeResponse>
&lt;srv:productGroupList>
{
for $i in 1 to count($fml/PT_CODE_PRODUCT_GROUP) return
&lt;dcl:ProductGroup>
	&lt;dcl:polishGroupName>{ data( $fml/PT_POLISH_NAME [$i] ) }&lt;/dcl:polishGroupName>
	&lt;dcl:codeProductGroup>{ data( $fml/PT_CODE_PRODUCT_GROUP [$i] ) }&lt;/dcl:codeProductGroup>
	&lt;dcl:englishGroupName>{ data( $fml/PT_ENGLISH_NAME [$i] ) }&lt;/dcl:englishGroupName>
	&lt;dcl:sortOrder>{ data( $fml/PT_SORT_ORDER [$i] ) }&lt;/dcl:sortOrder>
     
        &lt;dcl:visibilitySzrek>{ xs:boolean(data( $fml/PT_VISIBILITY_SZREK [$i] )) }&lt;/dcl:visibilitySzrek>
	&lt;dcl:visibilityCRM>{ xs:boolean(data( $fml/PT_VISIBILITY_CRM [$i] )) }&lt;/dcl:visibilityCRM>
	&lt;dcl:receiver>{ data( $fml/PT_RECEIVER [$i] ) }&lt;/dcl:receiver>
	&lt;dcl:idProductCategory>{ data( $fml/PT_ID_CATEGORY [$i] ) }&lt;/dcl:idProductCategory>
	
       &lt;dcl:userChangeSKP>{ data( $fml/PT_USER_CHANGE_SKP [$i] ) }&lt;/dcl:userChangeSKP>
 
     {if (data( $fml/PT_DATE_CHANGE[$i]))
     then &lt;dcl:dateChange>{ data( $fml/PT_DATE_CHANGE [$i] ) }&lt;/dcl:dateChange>
     else () }

       &lt;dcl:visibilityDCL>{ xs:boolean(data( $fml/PT_VISIBILITY_DCL [$i] )) }&lt;/dcl:visibilityDCL>
       &lt;dcl:idProductGroup>{ data( $fml/PT_ID_GROUP [$i] ) }&lt;/dcl:idProductGroup>
       &lt;dcl:codeProductSystem>
       &lt;ns1:System>
     		&lt;ns1:system>{ data( $fml/PT_CODE_PRODUCT_SYSTEM [$i] ) }&lt;/ns1:system>
     	&lt;/ns1:System>
     &lt;/dcl:codeProductSystem>
&lt;/dcl:ProductGroup>
}
&lt;/srv:productGroupList>
&lt;/srv:invokeResponse>
};

declare variable $fml as element(FML32) external;
xf:getProductGroupList_resp($fml)</con:xquery>
</con:xqueryEntry>