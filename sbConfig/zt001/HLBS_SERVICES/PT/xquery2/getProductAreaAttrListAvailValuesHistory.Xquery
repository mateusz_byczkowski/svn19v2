<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAreaAttrListAvailValues_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";

declare function xf:getProductAreaAttrListAvailValuesHistory_resp($fml as element())
    as element() {
	&lt;srv:invokeResponse>
        &lt;srv:prodAreaAttrListAvailValuesHist>
            {
                for $i in 1 to count($fml/PT_ID_AREA_ATTR_LIST_VAL_HISTORY)
                return
                    &lt;ns0:ProdAreaAttrLstAvailValHist>
                                &lt;ns0:connectionField>{ data($fml/PT_CONNECTION_FIELD[$i]) }&lt;/ns0:connectionField>                                    
                            	&lt;ns0:idProductAreaAttributes>{ data($fml/PT_ID_AREA_ATTRIBUTES[$i]) }&lt;/ns0:idProductAreaAttributes>
                                &lt;ns0:valueDescription>{ data($fml/PT_DESCRIPTION[$i]) }&lt;/ns0:valueDescription>                                    
                                &lt;ns0:idProdAreaAttrListAvailValHist>{ data($fml/PT_ID_AREA_ATTR_LIST_VAL_HISTORY[$i]) }&lt;/ns0:idProdAreaAttrListAvailValHist>                                    
                                &lt;ns0:value>{ data($fml/PT_VALUE[$i]) }&lt;/ns0:value>                                    
                                &lt;ns0:idProductAreaAttrListAvailVal>{ data($fml/PT_ID_AREA_ATTR_LIST_VAL[$i]) }&lt;/ns0:idProductAreaAttrListAvailVal>                                    
                                &lt;ns0:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }&lt;/ns0:userChangeSKP>                                    
                                &lt;ns0:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }&lt;/ns0:dateChange>                                    
                    &lt;/ns0:ProdAreaAttrLstAvailValHist>
            }
        &lt;/srv:prodAreaAttrListAvailValuesHist>
    &lt;/srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductAreaAttrListAvailValuesHistory_resp($fml)</con:xquery>
</con:xqueryEntry>