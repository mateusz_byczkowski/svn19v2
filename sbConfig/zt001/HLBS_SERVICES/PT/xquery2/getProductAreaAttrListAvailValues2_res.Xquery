<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAreaAttrListAvailValues_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:baseauxentities.be.dcl"; 
declare namespace ns2 ="urn:entities.be.dcl";

declare function xf:getProductAreaAttrListAvailValues_resp($fml as element())
    as element() {
	&lt;srv:invokeResponse>
         &lt;srv:bcd>
            &lt;ns2:BusinessControlData>
               &lt;ns2:pageControl>
                  &lt;ns1:PageControl>
                     &lt;ns1:hasNext> 
                       { if (data($fml/PT_OPTION[1]) = 1 )  
                         then
                             "true"
                        else
                             "false"
                         }
                     &lt;/ns1:hasNext>
                     &lt;ns1:navigationKeyValue>{ data($fml/PT_PACK_NO[1]) }&lt;/ns1:navigationKeyValue>
                  &lt;/ns1:PageControl>
               &lt;/ns2:pageControl>
            &lt;/ns2:BusinessControlData>
         &lt;/srv:bcd>
        &lt;srv:prodAreaAttrListAvailValues>
            {
                for $i in 1 to count($fml/PT_ID_AREA_ATTR_LIST_VAL)
                return
                    &lt;ns0:ProductAreaAttrListAvailVal>
                                &lt;ns0:value>{ data($fml/PT_VALUE[$i]) }&lt;/ns0:value>                                    
                                &lt;ns0:valueDescription>{ data($fml/PT_DESCRIPTION[$i]) }&lt;/ns0:valueDescription>                                    
                                &lt;ns0:connectionField>{ data($fml/PT_CONNECTION_FIELD[$i]) }&lt;/ns0:connectionField>                                    
                                &lt;ns0:idProductAreaAttrListAvailVal>{ data($fml/PT_ID_AREA_ATTR_LIST_VAL[$i]) }&lt;/ns0:idProductAreaAttrListAvailVal>                                    
                            	&lt;ns0:idProductAreaAttrtibutes>{ data($fml/PT_ID_AREA_ATTRIBUTES[$i]) }&lt;/ns0:idProductAreaAttrtibutes>
                    &lt;/ns0:ProductAreaAttrListAvailVal>
            }
        &lt;/srv:prodAreaAttrListAvailValues>
    &lt;/srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductAreaAttrListAvailValues_resp($fml)</con:xquery>
</con:xqueryEntry>