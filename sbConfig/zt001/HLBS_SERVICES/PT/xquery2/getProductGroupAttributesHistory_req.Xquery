<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductGroupAttributesHistory_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:getProductGroupAttributesHistory_req ($entity as element()) as element(FML32) {
&lt;FML32>
     &lt;PT_ID_GROUP_ATTRIBUTES>{ data( $entity/dcl:idAttributesProductGroup) }&lt;/PT_ID_GROUP_ATTRIBUTES>
&lt;/FML32>
};

declare variable $entity as element() external;
xf:getProductGroupAttributesHistory_req($entity)</con:xquery>
</con:xqueryEntry>