<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPortfListResponse($fml as element(fml:FML32))
	as element(m:CRMGetPortfListResponse) {
		&lt;m:CRMGetPortfListResponse>
			{
				if($fml/fml:CI_NUMER_PACZKI_STR)
					then &lt;m:NumerPaczkiStr>{ data($fml/fml:CI_NUMER_PACZKI_STR) }&lt;/m:NumerPaczkiStr>
					else ()
			}
			{

				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
				let $CI_SKP_ZASTEPCY := $fml/fml:CI_SKP_ZASTEPCY
				let $CI_STATYSTYKI_PORTFELA := $fml/fml:CI_STATYSTYKI_PORTFELA
				for $it at $p in $fml/fml:CI_ID_PORTFELA
				return
					&lt;m:CRMGetPortfListPortfel>
					{
						if($CI_ID_PORTFELA[$p])
							then &lt;m:IdPortfela>{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA[$p])
							then &lt;m:SkpPracownika>{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:SkpPracownika>
						else ()
					}
					{
						if($CI_SKP_ZASTEPCY[$p])
							then &lt;m:SkpZastepcy>{ data($CI_SKP_ZASTEPCY[$p]) }&lt;/m:SkpZastepcy>
						else ()
					}
					{
						if($CI_STATYSTYKI_PORTFELA[$p])
							then &lt;m:StatystykiPortfela>{ data($CI_STATYSTYKI_PORTFELA[$p]) }&lt;/m:StatystykiPortfela>
						else ()
					}
					&lt;/m:CRMGetPortfListPortfel>
			}

		&lt;/m:CRMGetPortfListResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetPortfListResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>