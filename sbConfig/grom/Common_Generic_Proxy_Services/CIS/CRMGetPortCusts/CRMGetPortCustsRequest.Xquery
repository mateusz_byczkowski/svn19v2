<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPortCustsRequest($req as element(m:CRMGetPortCustsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI>{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI>
					else ()
			}
			{
				for $v in $req/m:StatusGiodo
				return
					&lt;fml:CI_STATUS_GIODO>{ data($v) }&lt;/fml:CI_STATUS_GIODO>
			}
			{
				for $v in $req/m:UdostepGrupa
				return
					&lt;fml:CI_UDOSTEP_GRUPA>{ data($v) }&lt;/fml:CI_UDOSTEP_GRUPA>
			}
			{
				if($req/m:InnePortfele)
					then &lt;fml:CI_INNE_PORTFELE>{ data($req/m:InnePortfele) }&lt;/fml:CI_INNE_PORTFELE>
					else ()
			}
			{
				if($req/m:Kontakty)
					then &lt;fml:CI_KONTAKTY>{ data($req/m:Kontakty) }&lt;/fml:CI_KONTAKTY>
					else ()
			}
			{
				if($req/m:SkpPracownikaUpr)
					then &lt;fml:CI_SKP_PRACOWNIKA_UPR>{ data($req/m:SkpPracownikaUpr) }&lt;/fml:CI_SKP_PRACOWNIKA_UPR>
					else ()
			}
			{
				if($req/m:TypKont)
					then &lt;fml:CI_TYP_KONT>{ data($req/m:TypKont) }&lt;/fml:CI_TYP_KONT>
					else ()
			}
			{
				if($req/m:CelKont)
					then &lt;fml:CI_CEL_KONT>{ data($req/m:CelKont) }&lt;/fml:CI_CEL_KONT>
					else ()
			}
			{
				if($req/m:DataOstKontaktu)
					then &lt;fml:CI_DATA_OST_KONTAKTU>{ data($req/m:DataOstKontaktu) }&lt;/fml:CI_DATA_OST_KONTAKTU>
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER>{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI>{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI>
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA>{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetPortCustsRequest($body/m:CRMGetPortCustsRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>