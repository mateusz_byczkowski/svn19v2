<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustModHResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustModHResponse) {
		&lt;m:CRMGetCustModHResponse>
			{
				if($fml/fml:CI_NUMER_PACZKI_STR)
					then &lt;m:NumerPaczkiStr>{ data($fml/fml:CI_NUMER_PACZKI_STR) }&lt;/m:NumerPaczkiStr>
					else ()
			}
			{

				let $CI_ID_OPERACJI := $fml/fml:CI_ID_OPERACJI
				let $CI_BUDOWA_PORTFELA := $fml/fml:CI_BUDOWA_PORTFELA
				let $CI_CZAS_ZMIANY := $fml/fml:CI_CZAS_ZMIANY
				let $CIE_TYP_ZMIANY := $fml/fml:CIE_TYP_ZMIANY
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $DC_NUMER_ODDZIALU := $fml/fml:DC_NUMER_ODDZIALU
				let $CI_ID_PORTFELA_AKT := $fml/fml:CI_ID_PORTFELA_AKT
				let $CI_SKP_PRACOWNIKA_AKT := $fml/fml:CI_SKP_PRACOWNIKA_AKT
				let $CI_KLASA_OBSLUGI_AKT := $fml/fml:CI_KLASA_OBSLUGI_AKT
				let $CI_NOWY_NUMER_ODDZIALU := $fml/fml:CI_NOWY_NUMER_ODDZIALU
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $CI_POWOD_ZMIANY := $fml/fml:CI_POWOD_ZMIANY
				let $CI_SKP_PRACOWNIKA_REJ := $fml/fml:CI_SKP_PRACOWNIKA_REJ
				for $it at $p in $fml/fml:CI_ID_OPERACJI
				return
					&lt;m:Zmiana>
					{
						if($CI_ID_OPERACJI[$p])
							then &lt;m:IdOperacji>{ data($CI_ID_OPERACJI[$p]) }&lt;/m:IdOperacji>
						else ()
					}
					{
						if($CI_BUDOWA_PORTFELA[$p])
							then &lt;m:BudowaPortfela>{ data($CI_BUDOWA_PORTFELA[$p]) }&lt;/m:BudowaPortfela>
						else ()
					}
					{
						if($CI_CZAS_ZMIANY[$p])
							then &lt;m:CzasZmiany>{ data($CI_CZAS_ZMIANY[$p]) }&lt;/m:CzasZmiany>
						else ()
					}
					{
						if($CIE_TYP_ZMIANY[$p])
							then &lt;m:TypZmiany>{ data($CIE_TYP_ZMIANY[$p]) }&lt;/m:TypZmiany>
						else ()
					}
					{
						if($DC_TYP_KLIENTA[$p])
							then &lt;m:TypKlienta>{ data($DC_TYP_KLIENTA[$p]) }&lt;/m:TypKlienta>
						else ()
					}
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta>
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie>{ data($DC_IMIE[$p]) }&lt;/m:Imie>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko>{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko>
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna>{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna>
						else ()
					}
					{
						if($DC_NUMER_ODDZIALU[$p])
							then &lt;m:NumerOddzialu>{ data($DC_NUMER_ODDZIALU[$p]) }&lt;/m:NumerOddzialu>
						else ()
					}
					{
						if($CI_ID_PORTFELA_AKT[$p])
							then &lt;m:IdPortfelaAkt>{ data($CI_ID_PORTFELA_AKT[$p]) }&lt;/m:IdPortfelaAkt>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_AKT[$p])
							then &lt;m:SkpPracownikaAkt>{ data($CI_SKP_PRACOWNIKA_AKT[$p]) }&lt;/m:SkpPracownikaAkt>
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI_AKT[$p])
							then &lt;m:KlasaObslugiAkt>{ data($CI_KLASA_OBSLUGI_AKT[$p]) }&lt;/m:KlasaObslugiAkt>
						else ()
					}
					{
						if($CI_NOWY_NUMER_ODDZIALU[$p])
							then &lt;m:NowyNumerOddzialu>{ data($CI_NOWY_NUMER_ODDZIALU[$p]) }&lt;/m:NowyNumerOddzialu>
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then &lt;m:IdPortfela>{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA[$p])
							then &lt;m:SkpPracownika>{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:SkpPracownika>
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI[$p])
							then &lt;m:KlasaObslugi>{ data($CI_KLASA_OBSLUGI[$p]) }&lt;/m:KlasaObslugi>
						else ()
					}
					{
						if($CI_POWOD_ZMIANY[$p])
							then &lt;m:PowodZmiany>{ data($CI_POWOD_ZMIANY[$p]) }&lt;/m:PowodZmiany>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_REJ[$p])
							then &lt;m:SkpPracownikaRej>{ data($CI_SKP_PRACOWNIKA_REJ[$p]) }&lt;/m:SkpPracownikaRej>
						else ()
					}
					&lt;/m:Zmiana>
			}

		&lt;/m:CRMGetCustModHResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetCustModHResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>