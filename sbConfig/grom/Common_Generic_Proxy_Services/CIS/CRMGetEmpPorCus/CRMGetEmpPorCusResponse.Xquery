<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetEmpPorCusResponse($fml as element(fml:FML32))
	as element(m:CRMGetEmpPorCusResponse) {
		&lt;m:CRMGetEmpPorCusResponse>
			{
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:CRMGetEmpPorCusKlient>
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA[$p])
							then &lt;m:SkpPracownika>{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:SkpPracownika>
						else ()
					}
					&lt;/m:CRMGetEmpPorCusKlient>
			}
		&lt;/m:CRMGetEmpPorCusResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetEmpPorCusResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>