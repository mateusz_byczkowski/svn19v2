<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns="http://bzwbk.com/services/getUserTills";


declare variable $body as element(soap-env:Body) external;

(:~
 : @param $fml32 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function local:getUserTillsResponse($fml32 as element(FML32))
    as element(ns:invokeResponse)
{
    &lt;ns:invokeResponse>
    {
    	let $sessionDate := $fml32/NF_USERTS_SESSIONDATE
		let $lastChangeDate := $fml32/NF_USERTS_LASTCHANGEDATE
		let $sessionStatus := $fml32/NF_USETSS_USERTXNSESSIONST
		let $userLastName :=$fml32/NF_USER_USERLASTNAME
		let $userID := $fml32/NF_USER_USERID
		let $userFirstName := $fml32/NF_USER_USERFIRSTNAME
		let $tillID := $fml32/NF_TILL_TILLID
		let $tillName := $fml32/NF_TILL_TILLNAME
		let $tillType := $fml32/NF_TILTY_TILLTYP
		let $tellerID := $fml32/NF_TELLER_TELLERID
		let $branchCode := $fml32/NF_BRANCC_BRANCHCODE
	    for $sessionNumber at $i in $fml32/NF_USERTS_SESSIONNUMBER
        return
            &lt;userTxnSession>
                &lt;sessionNumber>{ data($sessionNumber) }&lt;/sessionNumber>
				&lt;sessionDate?>{ data($sessionDate[$i]) }&lt;/sessionDate>
                (:
                 : status sesji:
                 : - 0 --> C (zamknięta)
                 : - 1 --> O (otwarta)
                 :)
                &lt;sessionStatus>
                {
                	if (data($sessionStatus[$i]) eq '0') then
                		'C'
                	else if(data($sessionStatus[$i]) eq '1') then
                		'O'
                	else
                   		()
                }
                &lt;/sessionStatus>
				&lt;lastChangeDate?>{ if(data($lastChangeDate[$i]) != '') then data($lastChangeDate[$i]) else () }&lt;/lastChangeDate>
                &lt;till>
                    &lt;tillID?>{ data($tillID[$i]) }&lt;/tillID>
                    &lt;tillName?>{ data($tillName[$i]) }&lt;/tillName>
                    &lt;tillType?>{ data($tillType[$i]) }&lt;/tillType>
                &lt;/till>
                &lt;tellerID?>{ data($tellerID[$i]) }&lt;/tellerID>
                &lt;user>
                    &lt;userID?>{ data($userID[$i]) }&lt;/userID>
                    &lt;userFirstName?>{ data($userFirstName[$i]) }&lt;/userFirstName>
                    &lt;userLastName?>{ data($userLastName[$i]) }&lt;/userLastName>
                &lt;/user>
                &lt;branchCode?>{ data($branchCode[$i]) }&lt;/branchCode>
            &lt;/userTxnSession>
	}
	
	    &lt;pageControl?>
			&lt;hasNext?>{ xs:boolean(data($fml32/NF_PAGEC_HASNEXT)) }&lt;/hasNext>
	        &lt;navigationKeyDefinition?>{ data($fml32/NF_PAGEC_NAVIGATIONKEYDEFI) }&lt;/navigationKeyDefinition>
	        &lt;navigationKeyValue?>{ data($fml32/NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/navigationKeyValue>
	        &lt;totalSize?>{ data($fml32/NF_PAGECC_OPERATIONS) }&lt;/totalSize>
	    &lt;/pageControl>
    &lt;/ns:invokeResponse>
};

&lt;soap-env:Body>{
	local:getUserTillsResponse($body/FML32)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>