<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.3
 : @since   2010-02-18
 :
 : wersja WSDLa: 18-02-2010 11:17:01
 :
 : $Proxy Services/Branches/getBranchCurrentStatus/getBranchCurrentStatusResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://sygnity.pl/ThirdStage/branch/getBranchCurrentStatusResponse/";
declare namespace ns0 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns2 = "urn:entities.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns4 = "";
declare namespace ns5 = "urn:baseauxentities.be.dcl";

declare variable $fML321 as element(ns4:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getBranchCurrentStatusResponse($fML321 as element(ns4:FML32))
    as element(ns3:invokeResponse)
{
    &lt;ns3:invokeResponse>
        &lt;ns3:branchCurrentStatusOut>
            &lt;ns1:BranchCurrentStatus?>
            	
            	{
            		let $workDate := $fML321/ns4:NF_BRACST_WORKDATE
            		return
            		 if (data($workDate)) then
						&lt;ns1:workDate>{
							data($workDate)
						}&lt;/ns1:workDate>
					else ()
            	}

                &lt;ns1:sessionNumber?>{
					data($fML321/ns4:NF_BRACST_SESSIONNUMBER)
				}&lt;/ns1:sessionNumber>
				
                {
                	let $lastChangeDate := $fML321/ns4:NF_BRACST_LASTCHANGEDATE
                	return
	                	if (data($lastChangeDate)) then
							&lt;ns1:lastChangeDate>{
								fn:concat(fn:substring(data($lastChangeDate), 1, 10),
										  'T',
										  fn:substring(data($lastChangeDate), 12, 2),
										  ':',
										  fn:substring(data($lastChangeDate), 15, 2),
										  ':',
										  fn:substring(data($lastChangeDate), 18, 2))
							}&lt;/ns1:lastChangeDate>
						else ()
                }
                
                {
                	let $branchStatus := data($fML321/ns4:NF_BRANCS_BRANCHSTATUS)
                	return
                	if (data($branchStatus)) then
	                    &lt;ns1:status>
    	                    &lt;ns0:BranchStatus>
	                            &lt;ns0:branchStatus>{ 
	                            	if (data($branchStatus) eq '1') then
	                            		'O'
	                            	else
	                            		'C'
								}&lt;/ns0:branchStatus>
	                        &lt;/ns0:BranchStatus>
	                    &lt;/ns1:status>
	                else ()
                }
                
				&lt;ns1:lastChangeUser?>
					&lt;ns2:User?>
						{
							let $lastChangeUserLastName := $fML321/ns4:NF_USER_USERLASTNAME[1]
							return
								if (data($lastChangeUserLastName)) then
									&lt;ns2:userLastName>{
										data($lastChangeUserLastName)
									}&lt;/ns2:userLastName>
								else ()
						}

						{
							let $lastChangeUserID := $fML321/ns4:NF_USER_USERID[1]
							return
								if (data($lastChangeUserID)) then
									&lt;ns2:userID>{
										data($lastChangeUserID)
									}&lt;/ns2:userID>
								else ()
						}

						{
							let $lastChangeUserFirstName := $fML321/ns4:NF_USER_USERFIRSTNAME[1]
							return
								if (data($lastChangeUserFirstName)) then
									&lt;ns2:userFirstName>{
										data($lastChangeUserFirstName)
									}&lt;/ns2:userFirstName>
								else ()				
						}
					&lt;/ns2:User>
				&lt;/ns1:lastChangeUser>

				&lt;ns1:lastChangeAcceptor?>
					&lt;ns2:User?>
						{
							let $lastChangeAcceptorLastName := $fML321/ns4:NF_USER_USERLASTNAME[2]
							return
								if (data($lastChangeAcceptorLastName)) then
									&lt;ns2:userLastName>{
										data($lastChangeAcceptorLastName)
									}&lt;/ns2:userLastName>
								else ()
						}

						{
							let $lastChangeAcceptorID := $fML321/ns4:NF_USER_USERID[2]
							return
								if (data($lastChangeAcceptorID)) then
									&lt;ns2:userID>{
										data($lastChangeAcceptorID)
									}&lt;/ns2:userID>
								else ()
						}

						{
							let $lastChangeAcceptorFirstName := $fML321/ns4:NF_USER_USERFIRSTNAME[2]
							return
								if (data($lastChangeAcceptorFirstName)) then
									&lt;ns2:userFirstName>{
										data($lastChangeAcceptorFirstName)
									}&lt;/ns2:userFirstName>
								else ()
						}
					&lt;/ns2:User>
				&lt;/ns1:lastChangeAcceptor>

            &lt;/ns1:BranchCurrentStatus>
        &lt;/ns3:branchCurrentStatusOut>

        &lt;ns3:converted>
			{
	        	let $boolVal := $fML321/ns4:NF_BOOLEH_VALUE
	        	return
	        		if (data($boolVal)) then
						&lt;ns5:BooleanHolder>
					        &lt;ns5:value>{
								xs:boolean(data($boolVal))
							}&lt;/ns5:value>
						&lt;/ns5:BooleanHolder>
	        		else ()
        	}
		&lt;/ns3:converted>
		
    &lt;/ns3:invokeResponse>

};

&lt;soap-env:Body>{
	xf:getBranchCurrentStatusResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>