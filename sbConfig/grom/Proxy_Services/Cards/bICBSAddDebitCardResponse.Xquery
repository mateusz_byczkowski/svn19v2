<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbICBSAddDebitCardResponse($fml as element(fml:FML32))
	as element(m:bICBSAddDebitCardResponse) {
		&lt;m:bICBSAddDebitCardResponse>
			{
				if($fml/fml:DC_NR_KARTY)
					then &lt;m:NrKarty>{ data($fml/fml:DC_NR_KARTY) }&lt;/m:NrKarty>
					else ()
			}
			{
				if($fml/fml:DC_DATA_WAZNOSCI)
					then &lt;m:DataWaznosci>{ data($fml/fml:DC_DATA_WAZNOSCI) }&lt;/m:DataWaznosci>
					else ()
			}
			{
				if($fml/fml:DC_OPIS_BLEDU)
					then &lt;m:OpisBledu>{ data($fml/fml:DC_OPIS_BLEDU) }&lt;/m:OpisBledu>
					else ()
			}
		&lt;/m:bICBSAddDebitCardResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbICBSAddDebitCardResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>