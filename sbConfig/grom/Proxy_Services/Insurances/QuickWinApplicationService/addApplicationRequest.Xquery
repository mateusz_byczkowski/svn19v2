<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace crw = "http://bzwbk.com/crw/services/quickwin/";
declare namespace urn = "urn:be.services.dcl";

declare namespace urn1 = "urn:accounts.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:productstree.entities.be.dcl";
declare namespace urn4 = "urn:insurance.entities.be.dcl";
declare namespace urn5 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace urn6 = "urn:cif.entities.be.dcl";
declare namespace urn7 = "urn:filtersandmessages.entities.be.dcl";
declare namespace urn8 = "urn:baseauxentities.be.dcl";

declare variable $invoke as element(urn:invoke) external;
declare variable $header as element(urn:header) external;

&lt;crw:createApplication>
	 &lt;application>
		&lt;actualDate?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:actualDate) }&lt;/actualDate>
		{
		for $info in $invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:additionalInfoList/urn7:AdditionalInfo
		return
			&lt;additionalInfos>
				&lt;definition?>{ data($info/urn7:additionalInfoDefinition/urn2:AdditionalInfoDefinition/urn2:additionalInfoDefinition) }&lt;/definition>
				&lt;value?>{ data($info/urn7:value) }&lt;/value>
			&lt;/additionalInfos>
		}
		&lt;applicationStatus?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:insPolicyApplication/urn4:InsPolicyApplication/urn4:applicationStatus/urn5:InsPolicyAppStatus/urn5:insPolicyAppStatus) }&lt;/applicationStatus>
		&lt;currency>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:currency/urn2:CurrencyCode/urn2:currencyCode) }&lt;/currency>
		{
		for $role in $invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:accountRelationshipList/urn6:AccountRelationship
		return
			&lt;customerRoles>
			   &lt;customer>
				  &lt;companyName?>{ data($role/urn6:customer/urn6:Customer/urn6:companyName)}&lt;/companyName>
				  &lt;customerNumber?>{ data($role/urn6:customer/urn6:Customer/urn6:customerNumber) }&lt;/customerNumber>
				  &lt;documentNumber?>{ data($role/urn6:customer/urn6:Customer/urn6:documentList/urn6:Document/urn6:documentNumber) }&lt;/documentNumber>
				  &lt;documentType?>{ data($role/urn6:customer/urn6:Customer/urn6:documentList/urn6:Document/urn6:documentType/urn2:CustomerDocumentType/urn2:customerDocumentType) }&lt;/documentType>
				  &lt;firstName?>{ data($role/urn6:customer/urn6:Customer/urn6:customerPersonal/urn6:CustomerPersonal/urn6:firstName) }&lt;/firstName>
				  &lt;lastName?>{ data($role/urn6:customer/urn6:Customer/urn6:customerPersonal/urn6:CustomerPersonal/urn6:lastName) }&lt;/lastName>
			   &lt;/customer>
			   &lt;relationship?>{ data($role/urn6:relationship/urn2:CustomerAccountRelationship/urn2:customerAccountRelationship) }&lt;/relationship>
			&lt;/customerRoles>
		}
		&lt;externalPolices?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:externalPolices) }&lt;/externalPolices>
		&lt;fixedPremium?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:fixedPremium) }&lt;/fixedPremium>
		&lt;icbsProductNumber?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:icbsProductNumber) }&lt;/icbsProductNumber>
		&lt;insuranceDescription?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:insuranceDescription) }&lt;/insuranceDescription>
		&lt;linkedAccount>
		   &lt;accountDescription?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:accountDescription) }&lt;/accountDescription>
		   &lt;accountNumber?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:accountNumber) }&lt;/accountNumber>
		   &lt;currency?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:currency/urn2:CurrencyCode/urn2:currencyCode) }&lt;/currency>
		   &lt;faceAmount?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:loanAccount/urn1:LoanAccount/urn1:faceAmount) }&lt;/faceAmount>
		   &lt;idProductDefinition?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:productDefinition/urn3:ProductDefinition/urn3:idProductDefinition) }&lt;/idProductDefinition>
		   &lt;totalLimitAmount?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:tranAccount/urn1:TranAccount/urn1:totalLimitAmount) }&lt;/totalLimitAmount>
		&lt;/linkedAccount>
		&lt;loanApplicationNum?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:insPolicyApplication/urn4:InsPolicyApplication/urn4:loanApplicationNum) }&lt;/loanApplicationNum>
		&lt;maintenanceAccount>
		   &lt;accountDescription?>{ data($invoke/urn:maintenanceAccount/urn1:Account/urn1:accountDescription) }&lt;/accountDescription>
		   &lt;accountNumber?>{ data($invoke/urn:maintenanceAccount/urn1:Account/urn1:accountNumber) }&lt;/accountNumber>
		   &lt;currency?>{ data($invoke/urn:maintenanceAccount/urn1:Account/urn1:currency/urn2:CurrencyCode/urn2:currencyCode) }&lt;/currency>
		   &lt;idProductDefinition?>{ data($invoke/urn:maintenanceAccount/urn1:Account/urn1:productDefinition/urn3:ProductDefinition/urn3:idProductDefinition) }&lt;/idProductDefinition>
		&lt;/maintenanceAccount>
		
		&lt;oddPremiumDay?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:oddPremiumDay/urn2:SpecialDay/urn2:specialDay) }&lt;/oddPremiumDay>
		&lt;owuSignature?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:insPolicyApplication/urn4:InsPolicyApplication/urn4:owuSignature) }&lt;/owuSignature>
		&lt;premiumDueDate?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:premiumDueDate) }&lt;/premiumDueDate>
		&lt;premiumFrequency?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:premiumFrequency) }&lt;/premiumFrequency>
		&lt;premiumPeriod?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:premiumPeriod/urn2:Period/urn2:period) }&lt;/premiumPeriod>
		&lt;saleEmployeeID?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:saleEmployeeID) }&lt;/saleEmployeeID>
		&lt;startDate?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:startDate) }&lt;/startDate>
		&lt;validityDate?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:validityDate) }&lt;/validityDate>
	 &lt;/application>
	&lt;authData>
		&lt;applicationId>{ data($header/urn:msgHeader/urn:appId) }&lt;/applicationId>
		&lt;applicationPassword>{ data($invoke/urn:password/urn8:StringHolder/urn8:value)}&lt;/applicationPassword>
		&lt;operator>{ data($header/urn:msgHeader/urn:userId) }&lt;/operator>
	&lt;/authData>
&lt;/crw:createApplication></con:xquery>
</con:xqueryEntry>