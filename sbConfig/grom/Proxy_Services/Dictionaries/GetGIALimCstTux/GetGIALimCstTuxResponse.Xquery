<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "urn:BProExtWebServices";


declare function local:fault($faultString as xs:string, $faultCode as xs:string) as element(fml:FML32)  {
        &lt;fml:FML32>
  		&lt;fml:U_FAULT_STRING>{ $faultCode }: {$faultString}&lt;/fml:U_FAULT_STRING>
                &lt;fml:U_ERROR_CODE>30&lt;/fml:U_ERROR_CODE>	
        &lt;/fml:FML32>
};

declare function xf:map_GetGIALimCstTuxResponse($req as element()) as element(fml:FML32) {
	&lt;fml:FML32>
        {
                 for $el in $req/param  return
                 (
                        if ($el/name = 'DataUmowy') then &lt;fml:DC_DATA_OTWARCIA>{data($el/value)}&lt;/fml:DC_DATA_OTWARCIA>
                        else
                        if ($el/name = 'CalkKosztKred') then &lt;fml:DC_KOSZT_CALKOWITY>{data($el/value)}&lt;/fml:DC_KOSZT_CALKOWITY>
                        else 
                        if ($el/name = 'RRSO') then &lt;fml:DC_STOPA_RZECZYWISTA>{data($el/value)}&lt;/fml:DC_STOPA_RZECZYWISTA>
                        else
                        if ($el/name = 'LiczbaRat') then &lt;fml:DC_LICZBA_RAT>{data($el/value)}&lt;/fml:DC_LICZBA_RAT>
                        else
                        if ($el/name = 'DataRaty' ) then
                        (
                            for $value in $el/value return &lt;fml:DC_DATA_NALICZENIA>{data($value)}&lt;/fml:DC_DATA_NALICZENIA>
                        )
                        else
                        if ($el/name = 'KwKapRaty' ) then
                        (
                            for $value in $el/value return &lt;fml:DC_KWOTA_KAPITALU>{data($value)}&lt;/fml:DC_KWOTA_KAPITALU>
                        )
                        else
                        if ($el/name = 'KwOdsRaty' ) then
                        (
                            for $value in $el/value return &lt;fml:DC_KWOTA_PLATN_ODS>{data($value)}&lt;/fml:DC_KWOTA_PLATN_ODS>
                        )
                        else
                        if ($el/name = 'KwRaty' ) then
                        (
                            for $value in $el/value return &lt;fml:DC_KWOTA_RATY_LUB_PROCENT>{data($value)}&lt;/fml:DC_KWOTA_RATY_LUB_PROCENT>
                        )
                        else
                        if ($el/name = 'KwSaldoKap' ) then
                        (
                            for $value in $el/value return &lt;fml:DC_SALDO_KAPITALU>{data($value)}&lt;/fml:DC_SALDO_KAPITALU>
                        )
                        else
                        if ($el/name = 'KwPozostale' ) then
                        (
                            for $value in $el/value return &lt;fml:DC_KOSZTY_POZOSTALE>{data($value)}&lt;/fml:DC_KOSZTY_POZOSTALE>
                        )
                        else ()
                 )
	}
	&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{
       let $reqAsString := fn:substring($body/urn:callServiceResponse,126) (: wytnij deklaracje dtd i naglowek xml  :)
       let $reqXml := fn-bea:inlinedXML($reqAsString)

       return if ($reqXml/status/code != 'OK') then
       (
           local:fault($reqXml/status/message,$reqXml/status/code)
       )
       else
       (
           xf:map_GetGIALimCstTuxResponse($reqXml)
       )
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>