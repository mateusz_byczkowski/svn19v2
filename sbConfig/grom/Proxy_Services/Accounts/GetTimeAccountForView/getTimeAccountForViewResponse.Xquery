<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-08-02</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace ns5="urn:filterandmessages.dictionaries.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForHoldList($parm as element(fml:FML32)) as element()
{

<ns6:holdList>
  {
    for $x at $occ in $parm/NF_HOLD_HOLDAMOUNT
    return
    <ns6:Hold>
      <ns6:holdAmount?>{data($parm/NF_HOLD_HOLDAMOUNT[$occ])}</ns6:holdAmount>
      <ns6:holdCurrencyCode>
        <ns4:CurrencyCode>
          <ns4:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns4:currencyCode>
        </ns4:CurrencyCode>
      </ns6:holdCurrencyCode>
    </ns6:Hold>
  }
</ns6:holdList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  <ns0:accountOut>
    <ns6:Account>
      <ns6:accountOpenDate?>{fn-bea:dateTime-from-string-with-format("dd-MM-yyyy",data($parm/NF_ACCOUN_ACCOUNTOPENDATE))}</ns6:accountOpenDate>
      <ns6:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE)}</ns6:currentBalance>
      <ns6:debitPercentage?>{data($parm/NF_ACCOUN_DEBITPERCENTAGE)}</ns6:debitPercentage>
      <ns6:statementForAccount?>{data($parm/NF_ACCOUN_STATEMENTFORACCO)}</ns6:statementForAccount>
      <ns6:productCode?>{data($parm/NF_ACCOUN_PRODUCTCODE)}</ns6:productCode>
      <ns6:timeAccount>
        <ns6:TimeAccount>
          <ns6:originalAmount?>{data($parm/NF_TIMEA_ORIGINALAMOUNT)}</ns6:originalAmount>
          <ns6:nextRenewalMaturityDate?>{fn-bea:dateTime-from-string-with-format("dd-MM-yyyy",data($parm/NF_TIMEA_NEXTRENEWALMATURI))}</ns6:nextRenewalMaturityDate>
          <ns6:dispositionCode?>{data($parm/NF_TIMEA_DISPOSITIONCODE)}</ns6:dispositionCode>
          <ns6:interestPaymentPeriod?>{data($parm/NF_TIMEA_INTERESTPAYMENTPE)}</ns6:interestPaymentPeriod>
          <ns6:interestPaymentFrequency?>{data($parm/NF_TIMEA_INTERESTPAYMENTFR)}</ns6:interestPaymentFrequency>
          <ns6:nextInrterestPaymentDate?>{fn-bea:dateTime-from-string-with-format("dd-MM-yyyy",data($parm/NF_TIMEA_NEXTINRTERESTPAYM))}</ns6:nextInrterestPaymentDate>
          <ns6:interestDisposition?>{data($parm/NF_TIMEA_INTERESTDISPOSITI)}</ns6:interestDisposition>
          <ns6:nextInterestPaymentAmount?>{data($parm/NF_TIMEA_NEXTINTERESTPAYME)}</ns6:nextInterestPaymentAmount>
          <ns6:interestDue?>{data($parm/NF_TIMEA_INTERESTDUE)}</ns6:interestDue>
          <ns6:interestForPay?>{data($parm/NF_TIMEA_INTERESTFORPAY)}</ns6:interestForPay>
          <ns6:dailyInterest?>{data($parm/NF_TIMEA_DAILYINTEREST)}</ns6:dailyInterest>
          <ns6:lastRenewalDate?>{fn-bea:dateTime-from-string-with-format("dd-MM-yyyy",data($parm/NF_TIMEA_LASTRENEWALDATE))}</ns6:lastRenewalDate>
          <ns6:dateMayWithdraw?>{data($parm/NF_TIMEA_DATEMAYWITHDRAW)}</ns6:dateMayWithdraw>
          <ns6:withholdingThresholdAmount?>{data($parm/NF_TIMEA_WITHHOLDINGTHRESH)}</ns6:withholdingThresholdAmount>
          <ns6:currentInterestRate?>{data($parm/NF_TIMEA_CURRENTINTERESTRA)}</ns6:currentInterestRate>
          <ns6:indexVarianceAmount?>{data($parm/NF_TIMEA_INDEXVARIANCEAMOU)}</ns6:indexVarianceAmount>
          <ns6:closeOverrideFlag?>{data($parm/NF_TIMEA_CLOSEOVERRIDEFLAG)}</ns6:closeOverrideFlag>
          <ns6:originalDateOfInstrument?>{fn-bea:dateTime-from-string-with-format("dd-MM-yyyy",data($parm/NF_TIMEA_ORIGINALDATEOFINS))}</ns6:originalDateOfInstrument>
          <ns6:balanceBucket?>{data($parm/NF_TIMEA_BALANCEBUCKET)}</ns6:balanceBucket>
          <ns6:dateOfMaint?>{data($parm/NF_TIMEA_DATEOFMAINT)}</ns6:dateOfMaint>
          {getElementsForHoldList($parm)}
          <ns6:tax>
            <ns6:Tax>
              <ns6:taxFlag?>{data($parm/NF_TAX_TAXFLAG)}</ns6:taxFlag>
              <ns6:taxPercentage?>{data($parm/NF_TAX_TAXPERCENTAGE)}</ns6:taxPercentage>
              <ns6:taxAmount?>{data($parm/NF_TAX_TAXAMOUNT)}</ns6:taxAmount>
            </ns6:Tax>
          </ns6:tax>
          <ns6:renewalID>
            <ns4:RenewalType>
              <ns4:renewalType?>{data($parm/NF_RENEWT_RENEWALTYPE)}</ns4:renewalType>
            </ns4:RenewalType>
          </ns6:renewalID>
          <ns6:renewalPeriod>
            <ns4:Period>
              <ns4:period?>{data($parm/NF_PERIOD_PERIOD2)}</ns4:period>
            </ns4:Period>
          </ns6:renewalPeriod>
          <ns6:renewalSpecificDay>
            <ns4:SpecialDay>
            </ns4:SpecialDay>
          </ns6:renewalSpecificDay>
          <ns6:interestPaymentSpecificDay>
            <ns4:SpecialDay>
              <ns4:specialDay?>{data($parm/NF_SPECID_SPECIALDAY)}</ns4:specialDay>
            </ns4:SpecialDay>
          </ns6:interestPaymentSpecificDay>
          <ns6:competencyCode>
            <ns5:CompetencyCode>
              <ns5:competencyCode?>{data($parm/NF_COMPEC_COMPETENCYCODE)}</ns5:competencyCode>
            </ns5:CompetencyCode>
          </ns6:competencyCode>
        </ns6:TimeAccount>
      </ns6:timeAccount>
      <ns6:statementParameters>
        <ns6:StatementParameters>
          <ns6:frequency?>{data($parm/NF_STATEP_FREQUENCY)}</ns6:frequency>
          <ns6:nextPrintoutDate?>{data($parm/NF_STATEP_NEXTPRINTOUTDATE)}</ns6:nextPrintoutDate>
          <ns6:cycle>
            <ns4:Period>
              <ns4:period?>{data($parm/NF_PERIOD_PERIOD2)}</ns4:period>
            </ns4:Period>
          </ns6:cycle>
          <ns6:specialDay>
            <ns4:SpecialDay>
              <ns4:specialDay?>{data($parm/NF_SPECID_SPECIALDAY)}</ns4:specialDay>
            </ns4:SpecialDay>
          </ns6:specialDay>
        </ns6:StatementParameters>
      </ns6:statementParameters>
      <ns6:currency>
        <ns4:CurrencyCode>
          <ns4:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE)}</ns4:currencyCode>
        </ns4:CurrencyCode>
      </ns6:currency>
      <ns6:accountBranchNumber>
        <ns4:BranchCode>
          <ns4:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE)}</ns4:branchCode>
        </ns4:BranchCode>
      </ns6:accountBranchNumber>
      <ns6:accountAddress>
        <ns6:AccountAddress>
          <ns6:versionID?>{data($parm/NF_ACCOUA_VERSIONID)}</ns6:versionID>
          <ns6:name1?>{data($parm/NF_ACCOUA_NAME1)}</ns6:name1>
          <ns6:name2?>{data($parm/NF_ACCOUA_NAME2)}</ns6:name2>
          <ns6:street?>{data($parm/NF_ACCOUA_STREET)}</ns6:street>
          <ns6:houseFlatNumber?>{data($parm/NF_ACCOUA_HOUSEFLATNUMBER)}</ns6:houseFlatNumber>
          <ns6:city?>{data($parm/NF_ACCOUA_CITY)}</ns6:city>
          <ns6:stateCountry?>{data($parm/NF_ACCOUA_STATECOUNTRY)}</ns6:stateCountry>
          <ns6:zipCode?>{data($parm/NF_ACCOUA_ZIPCODE)}</ns6:zipCode>
          <ns6:accountAddressType?>{data($parm/NF_ACCOUA_ACCOUNTADDRESSTY)}</ns6:accountAddressType>
          <ns6:validTo?>{fn-bea:dateTime-from-string-with-format("dd-MM-yyyy",data($parm/NF_ACCOUA_VALIDTO))}</ns6:validTo>
        </ns6:AccountAddress>
      </ns6:accountAddress>
    </ns6:Account>
  </ns0:accountOut>
  <ns0:renewalTransfer>
    <ns2:TransferOrderAnchor>
      <ns2:transferOrderNumber?>{data($parm/NF_TRANOA_TRANSFERORDERNUM)}</ns2:transferOrderNumber>
    </ns2:TransferOrderAnchor>
  </ns0:renewalTransfer>
  <ns0:customer>
    <ns3:Customer>
      <ns3:companyName?>{data($parm/NF_CUSTOM_COMPANYNAME)}</ns3:companyName>
      <ns3:taxID?>{data($parm/NF_CUSTOM_TAXID)}</ns3:taxID>
      <ns3:shortName?>{data($parm/NF_CUSTOM_SHORTNAME)}</ns3:shortName>
      <ns3:customerPersonal>
        <ns3:CustomerPersonal>
          <ns3:lastName?>{data($parm/NF_CUSTOP_LASTNAME)}</ns3:lastName>
          <ns3:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME)}</ns3:firstName>
        </ns3:CustomerPersonal>
      </ns3:customerPersonal>
      <ns3:customerType>
        <ns4:CustomerType>
          <ns4:customerType?>{data($parm/NF_CUSTOT_CUSTOMERTYPE)}</ns4:customerType>
        </ns4:CustomerType>
      </ns3:customerType>
    </ns3:Customer>
  </ns0:customer>
  <ns0:interestTransfer>
    <ns2:TransferOrderAnchor>
      <ns2:transferOrderNumber?>{data($parm/NF_TRANOA_TRANSFERORDERNUM)}</ns2:transferOrderNumber>
    </ns2:TransferOrderAnchor>
  </ns0:interestTransfer>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>