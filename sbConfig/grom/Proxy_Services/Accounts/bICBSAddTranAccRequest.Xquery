<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbICBSAddTranAccRequest($req as element(m:bICBSAddTranAccRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:TrnId)
					then &lt;fml:DC_TRN_ID>{ data($req/m:TrnId) }&lt;/fml:DC_TRN_ID>
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK>{ data($req/m:Uzytkownik) }&lt;/fml:DC_UZYTKOWNIK>
					else ()
			}
			{
				if($req/m:Oddzial)
					then &lt;fml:DC_ODDZIAL>{ data($req/m:Oddzial) }&lt;/fml:DC_ODDZIAL>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:Relacja)
					then &lt;fml:DC_RELACJA>{ data($req/m:Relacja) }&lt;/fml:DC_RELACJA>
					else ()
			}
			{
				if($req/m:ProcentZobPodatkowych)
					then &lt;fml:DC_PROCENT_ZOB_PODATKOWYCH>{ data($req/m:ProcentZobPodatkowych) }&lt;/fml:DC_PROCENT_ZOB_PODATKOWYCH>
					else ()
			}
			{
				if($req/m:FlagaZgody)
					then &lt;fml:DC_FLAGA_ZGODY>{ data($req/m:FlagaZgody) }&lt;/fml:DC_FLAGA_ZGODY>
					else ()
			}
			{
				if($req/m:DataZgody)
					then &lt;fml:DC_DATA_ZGODY>{ data($req/m:DataZgody) }&lt;/fml:DC_DATA_ZGODY>
					else ()
			}
			{
				if($req/m:OkresWaznosciZgody)
					then &lt;fml:DC_OKRES_WAZNOSCI_ZGODY>{ data($req/m:OkresWaznosciZgody) }&lt;/fml:DC_OKRES_WAZNOSCI_ZGODY>
					else ()
			}
			{
				if($req/m:NumerProduktu)
					then &lt;fml:DC_NUMER_PRODUKTU>{ data($req/m:NumerProduktu) }&lt;/fml:DC_NUMER_PRODUKTU>
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:KodPracownika)
					then &lt;fml:DC_KOD_PRACOWNIKA>{ data($req/m:KodPracownika) }&lt;/fml:DC_KOD_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:RodzajRachunku)
					then &lt;fml:DC_RODZAJ_RACHUNKU>{ data($req/m:RodzajRachunku) }&lt;/fml:DC_RODZAJ_RACHUNKU>
					else ()
			}
			{
				if($req/m:DataOtwarciaRachunku)
					then &lt;fml:DC_DATA_OTWARCIA_RACHUNKU>{ data($req/m:DataOtwarciaRachunku) }&lt;/fml:DC_DATA_OTWARCIA_RACHUNKU>
					else ()
			}
			{
				if($req/m:Rachunek)
					then &lt;fml:DC_RACHUNEK>{ data($req/m:Rachunek) }&lt;/fml:DC_RACHUNEK>
					else ()
			}
			{
				if($req/m:TypRelacji)
					then &lt;fml:DC_TYP_RELACJI>{ data($req/m:TypRelacji) }&lt;/fml:DC_TYP_RELACJI>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbICBSAddTranAccRequest($body/m:bICBSAddTranAccRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>