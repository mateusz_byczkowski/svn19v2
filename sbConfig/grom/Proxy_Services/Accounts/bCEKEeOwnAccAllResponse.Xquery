<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbCEKEeOwnAccAllResponse($fml as element(fml:FML32))
	as element(m:bCEKEeOwnAccAllResponse) {
		&lt;m:bCEKEeOwnAccAllResponse>
			{
				if($fml/fml:E_CUST_SEQNO)
					then &lt;m:CustSeqno>{ data($fml/fml:E_CUST_SEQNO) }&lt;/m:CustSeqno>
					else ()
			}
			{
				if($fml/fml:B_SYS_MASK)
					then &lt;m:SysMask>{ data($fml/fml:B_SYS_MASK) }&lt;/m:SysMask>
					else ()
			}
			{

				let $B_SYS := $fml/fml:B_SYS
				let $B_MIKROODDZIAL := $fml/fml:B_MIKROODDZIAL
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				let $B_BANK := $fml/fml:B_BANK
				let $E_SEQ_NO := $fml/fml:E_SEQ_NO
				let $E_CHANNEL_MASK := $fml/fml:E_CHANNEL_MASK
				let $B_KOD_RACH := $fml/fml:B_KOD_RACH
				let $B_ID_ODDZ := $fml/fml:B_ID_ODDZ
				let $E_TIME_STAMP := $fml/fml:E_TIME_STAMP
				let $E_ACCOUNT_TYPE_OPTIONS := $fml/fml:E_ACCOUNT_TYPE_OPTIONS
				let $E_DR_TRN_MASK := $fml/fml:E_DR_TRN_MASK
				let $E_CR_TRN_MASK := $fml/fml:E_CR_TRN_MASK
				let $E_LOGIN_ID := $fml/fml:E_LOGIN_ID
				let $E_OPTIONS := $fml/fml:E_OPTIONS
				let $B_FLAGA_REZ := $fml/fml:B_FLAGA_REZ
				let $B_NAZWA_KLIENTA := $fml/fml:B_NAZWA_KLIENTA
				let $B_UL_ZAM := $fml/fml:B_UL_ZAM
				let $B_M_ZAM := $fml/fml:B_M_ZAM
				let $B_KREWNY_BANKU := $fml/fml:B_KREWNY_BANKU
				let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
				let $B_SALDO := $fml/fml:B_SALDO
				let $B_DATA_OST_OPER := $fml/fml:B_DATA_OST_OPER
				let $B_OPIS_RACH := $fml/fml:B_OPIS_RACH
				let $B_WALUTA := $fml/fml:B_WALUTA
				let $B_DOST_SRODKI := $fml/fml:B_DOST_SRODKI
				let $B_SKROT_OPISU := $fml/fml:B_SKROT_OPISU
				let $B_NAZWA_ODDZ := $fml/fml:B_NAZWA_ODDZ
				let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
				let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
				for $it at $p in $fml/fml:E_SEQ_NO
				return
					&lt;m:bCEKEeOwnAccAllaccount>
					{
						if($B_SYS[$p])
							then &lt;m:Sys>{ data($B_SYS[$p]) }&lt;/m:Sys>
						else ()
					}
					{
						if($B_MIKROODDZIAL[$p])
							then &lt;m:Mikrooddzial>{ data($B_MIKROODDZIAL[$p]) }&lt;/m:Mikrooddzial>
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then &lt;m:TypRach>{ data($B_TYP_RACH[$p]) }&lt;/m:TypRach>
						else ()
					}
					{
						if($B_BANK[$p])
							then &lt;m:Bank>{ data($B_BANK[$p]) }&lt;/m:Bank>
						else ()
					}
					{
						if($E_SEQ_NO[$p])
							then &lt;m:SeqNo>{ data($E_SEQ_NO[$p]) }&lt;/m:SeqNo>
						else ()
					}
					{
						if($E_CHANNEL_MASK[$p])
							then &lt;m:ChannelMask>{ data($E_CHANNEL_MASK[$p]) }&lt;/m:ChannelMask>
						else ()
					}
					{
						if($B_KOD_RACH[$p])
							then &lt;m:KodRach>{ data($B_KOD_RACH[$p]) }&lt;/m:KodRach>
						else ()
					}
					{
						if($B_ID_ODDZ[$p])
							then &lt;m:IdOddz>{ data($B_ID_ODDZ[$p]) }&lt;/m:IdOddz>
						else ()
					}
					{
						if($E_TIME_STAMP[$p])
							then &lt;m:TimeStamp>{ data($E_TIME_STAMP[$p]) }&lt;/m:TimeStamp>
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_OPTIONS[$p])
							then &lt;m:AccountTypeOptions>{ data($E_ACCOUNT_TYPE_OPTIONS[$p]) }&lt;/m:AccountTypeOptions>
						else ()
					}
					{
						if($E_DR_TRN_MASK[$p])
							then &lt;m:DrTrnMask>{ data($E_DR_TRN_MASK[$p]) }&lt;/m:DrTrnMask>
						else ()
					}
					{
						if($E_CR_TRN_MASK[$p])
							then &lt;m:CrTrnMask>{ data($E_CR_TRN_MASK[$p]) }&lt;/m:CrTrnMask>
						else ()
					}
					{
						if($E_LOGIN_ID[$p])
							then &lt;m:LoginId>{ data($E_LOGIN_ID[$p]) }&lt;/m:LoginId>
						else ()
					}
					{
						if($E_OPTIONS[$p])
							then &lt;m:Options>{ data($E_OPTIONS[$p]) }&lt;/m:Options>
						else ()
					}
					{
						if($B_FLAGA_REZ[$p])
							then &lt;m:FlagaRez>{ data($B_FLAGA_REZ[$p]) }&lt;/m:FlagaRez>
						else ()
					}
					{
						if($B_NAZWA_KLIENTA[$p])
							then &lt;m:NazwaKlienta>{ data($B_NAZWA_KLIENTA[$p]) }&lt;/m:NazwaKlienta>
						else ()
					}
					{
						if($B_UL_ZAM[$p])
							then &lt;m:UlZam>{ data($B_UL_ZAM[$p]) }&lt;/m:UlZam>
						else ()
					}
					{
						if($B_M_ZAM[$p])
							then &lt;m:MZam>{ data($B_M_ZAM[$p]) }&lt;/m:MZam>
						else ()
					}
					{
						if($B_KREWNY_BANKU[$p])
							then &lt;m:KrewnyBanku>{ data($B_KREWNY_BANKU[$p]) }&lt;/m:KrewnyBanku>
						else ()
					}
					{
						if($B_DL_NR_RACH[$p])
							then &lt;m:DlNrRach>{ data($B_DL_NR_RACH[$p]) }&lt;/m:DlNrRach>
						else ()
					}
					{
						if($B_SALDO[$p])
							then &lt;m:Saldo>{ data($B_SALDO[$p]) }&lt;/m:Saldo>
						else ()
					}
					{
						if($B_DATA_OST_OPER[$p])
							then &lt;m:DataOstOper>{ data($B_DATA_OST_OPER[$p]) }&lt;/m:DataOstOper>
						else ()
					}
					{
						if($B_OPIS_RACH[$p])
							then &lt;m:OpisRach>{ data($B_OPIS_RACH[$p]) }&lt;/m:OpisRach>
						else ()
					}
					{
						if($B_WALUTA[$p])
							then &lt;m:Waluta>{ data($B_WALUTA[$p]) }&lt;/m:Waluta>
						else ()
					}
					{
						if($B_DOST_SRODKI[$p])
							then &lt;m:DostSrodki>{ data($B_DOST_SRODKI[$p]) }&lt;/m:DostSrodki>
						else ()
					}
					{
						if($B_SKROT_OPISU[$p])
							then &lt;m:SkrotOpisu>{ data($B_SKROT_OPISU[$p]) }&lt;/m:SkrotOpisu>
						else ()
					}
					{
						if($B_NAZWA_ODDZ[$p])
							then &lt;m:NazwaOddz>{ data($B_NAZWA_ODDZ[$p]) }&lt;/m:NazwaOddz>
						else ()
					}
					{
						if($B_KOD_WALUTY[$p])
							then &lt;m:KodWaluty>{ data($B_KOD_WALUTY[$p]) }&lt;/m:KodWaluty>
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_NAME[$p])
							then &lt;m:AccountTypeName>{ data($E_ACCOUNT_TYPE_NAME[$p]) }&lt;/m:AccountTypeName>
						else ()
					}
					&lt;/m:bCEKEeOwnAccAllaccount>
			}

		&lt;/m:bCEKEeOwnAccAllResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbCEKEeOwnAccAllResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>