<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbICBSICB_rachunekRequest($req as element(m:bICBSICB_rachunekRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:TypRach)
					then &lt;fml:B_TYP_RACH>{ data($req/m:TypRach) }&lt;/fml:B_TYP_RACH>
					else ()
			}
			{
				if($req/m:KodRachS)
					then &lt;fml:B_KOD_RACH_S>{ data($req/m:KodRachS) }&lt;/fml:B_KOD_RACH_S>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbICBSICB_rachunekRequest($body/m:bICBSICB_rachunekRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>