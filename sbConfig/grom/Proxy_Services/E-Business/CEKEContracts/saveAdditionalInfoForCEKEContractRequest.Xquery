<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-08-02</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "http://bzwbk.com/services/ceke/messages/";

declare function xf:map_saveAdditionalInfoForCEKEContractRequest($req as element(urn:SaveAdditionalInfoForCEKEContractRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
                  		&lt;fml:E_SYSTEM_ID?>{ data($req/systemId) }&lt;/fml:E_SYSTEM_ID>,
				&lt;fml:E_AGR_SIGNATURE?>{ data($req/signature) }&lt;/fml:E_AGR_SIGNATURE>,
				let $ATTRIBUTE := $req/contractAttributes
				for $it at $p in $req/contractAttributes
				return
				(
					&lt;fml:E_AGR_ATTR_NAME>{ data($ATTRIBUTE[$p]/name) }&lt;/fml:E_AGR_ATTR_NAME>,
					&lt;fml:E_AGR_ATTR_VALUE>{ data($ATTRIBUTE[$p]/value) }&lt;/fml:E_AGR_ATTR_VALUE>
				)
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_saveAdditionalInfoForCEKEContractRequest($body/urn:SaveAdditionalInfoForCEKEContractRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>