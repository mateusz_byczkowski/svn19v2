<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace loan = "http://bzwbk.com/crw/services/dc/loan";

declare function xf:cekeDateToDateType
  ( $dateStr as xs:string ) as xs:string {
    if ($dateStr = '') then ""
    else
     concat(fn:substring($dateStr,7,4),'-',fn:substring($dateStr,4,2),'-',fn:substring($dateStr,1,2))
 } ;


declare function xf:map_GetDictionaryInfoRequest($fml as element(fml:FML32))
	as element(loan:createApplication) {
      &lt;loan:createApplication>
         &lt;application>
            &lt;accountHolderName>{ data($fml/fml:DC_NAZWA) }&lt;/accountHolderName>
            &lt;additionalCosts>{ data($fml/fml:DC_KOSZTY_POZOSTALE) }&lt;/additionalCosts>
            {
             if (data($fml/fml:DC_TYP_RELACJI[1]) = '0') then
                &lt;beneficiary1>
                  &lt;birthDate>{ xf:cekeDateToDateType(data($fml/fml:DC_DATA_URODZENIA[2])) }&lt;/birthDate>
                  &lt;firstName>{ data($fml/fml:DC_IMIE[2]) }&lt;/firstName>
                  &lt;lastName>{ data($fml/fml:DC_NAZWISKO[2]) }&lt;/lastName>
                  &lt;pesel>{ data($fml/fml:DC_NR_PESEL[2]) }&lt;/pesel>
                  &lt;shareInBenefit>{ data($fml/fml:DC_PROCENT_ZOBOWIAZANIA[1]) }&lt;/shareInBenefit>
               &lt;/beneficiary1>
             else
               &lt;beneficiary1>
                   &lt;lastName>{ data($fml/fml:DC_NAZWISKO[2]) }&lt;/lastName>
                   &lt;shareInBenefit>{ data($fml/fml:DC_PROCENT_ZOBOWIAZANIA[1]) }&lt;/shareInBenefit>
                   &lt;regon>{ data($fml/fml:DC_NR_DOWODU_REGON[2]) }&lt;/regon>
                   &lt;address>
                       &lt;city>{ data($fml/fml:DC_MIASTO_DANE_PODST[2]) }&lt;/city>
                       &lt;street>{ data($fml/fml:DC_ULICA_DANE_PODST[2]) }&lt;/street>
                       &lt;houseFlatNumber>{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST[2]) }&lt;/houseFlatNumber>
                       &lt;zipCode>{ data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST[2]) }&lt;/zipCode>
                   &lt;/address>
               &lt;/beneficiary1>
             }
            {
             if (data($fml/fml:DC_TYP_RELACJI[2]) = '0') then
                &lt;beneficiary2>
                  &lt;birthDate>{ xf:cekeDateToDateType(data($fml/fml:DC_DATA_URODZENIA[3])) }&lt;/birthDate>
                  &lt;firstName>{ data($fml/fml:DC_IMIE[3]) }&lt;/firstName>
                  &lt;lastName>{ data($fml/fml:DC_NAZWISKO[3]) }&lt;/lastName>
                  &lt;pesel>{ data($fml/fml:DC_NR_PESEL[3]) }&lt;/pesel>
                  &lt;shareInBenefit>{ data($fml/fml:DC_PROCENT_ZOBOWIAZANIA[2]) }&lt;/shareInBenefit>
               &lt;/beneficiary2>
             else
               &lt;beneficiary2>
                   &lt;lastName>{ data($fml/fml:DC_NAZWISKO[3]) }&lt;/lastName>
                   &lt;shareInBenefit>{ data($fml/fml:DC_PROCENT_ZOBOWIAZANIA[2]) }&lt;/shareInBenefit>
                   &lt;regon>{ data($fml/fml:DC_NR_DOWODU_REGON[3]) }&lt;/regon>
                   &lt;address>
                       &lt;city>{ data($fml/fml:DC_MIASTO_DANE_PODST[3]) }&lt;/city>
                       &lt;street>{ data($fml/fml:DC_ULICA_DANE_PODST[3]) }&lt;/street>
                       &lt;houseFlatNumber>{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST[3]) }&lt;/houseFlatNumber>
                       &lt;zipCode>{ data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST[3]) }&lt;/zipCode>
                   &lt;/address>
               &lt;/beneficiary2>
             }
            &lt;commissionAmount>{ data($fml/fml:DC_KWOTA_PROWIZJI) }&lt;/commissionAmount>
            &lt;creditAmount>{ data($fml/fml:DC_KWOTA_KREDYTU) }&lt;/creditAmount>
            &lt;customerApplicationDate>{ xf:cekeDateToDateType(data($fml/fml:DC_DATA_WPROWADZENIA)) }&lt;/customerApplicationDate>
            &lt;customerRoles xsi:type="lo:SimpleCustomerRole" xmlns:lo="http://bzwbk.pl/crw/dc/loan/">
               &lt;customer>
                  &lt;birthDate>{ xf:cekeDateToDateType(data($fml/fml:DC_DATA_URODZENIA[1])) }&lt;/birthDate>
                  &lt;branch>{ data($fml/fml:DC_NUMER_ODDZIALU) }&lt;/branch>
                  &lt;chargesInfo>
                     &lt;alimonyAndOtherLegalObligationsValue>{ data($fml/fml:DC_WARTOSC_ZABEZPIECZENIA) }&lt;/alimonyAndOtherLegalObligationsValue>
                     {
                     if (data($fml/fml:DC_WARTOSC_ZABEZPIECZENIA) != '') then
                        &lt;alimonyAndOtherLegalObligationsPresent>true&lt;/alimonyAndOtherLegalObligationsPresent>
                     else 
                        &lt;alimonyAndOtherLegalObligationsPresent>false&lt;/alimonyAndOtherLegalObligationsPresent>
                     }
                    &lt;householdAndPhoneChargesValue>{ data($fml/fml:DC_MAX_KWOTA_PORECZENIA) }&lt;/householdAndPhoneChargesValue>
                     {
                     if (data($fml/fml:DC_MAX_KWOTA_PORECZENIA) != '') then
                        &lt;householdAndPhoneChargesPresent>true&lt;/householdAndPhoneChargesPresent>
                     else 
                        &lt;householdAndPhoneChargesPresent>false&lt;/householdAndPhoneChargesPresent>
                     }
                    &lt;otherChargesValue>{ data($fml/fml:DC_POZOSTALE) }&lt;/otherChargesValue>
                    {
                    if (data($fml/fml:DC_POZOSTALE) != '') then
                        &lt;otherChargesPresent>true&lt;/otherChargesPresent>
                     else 
                        &lt;otherChargesPresent>false&lt;/otherChargesPresent>
                     }
                  &lt;/chargesInfo>
                  &lt;cif>{ data($fml/fml:DC_NUMER_KLIENTA) }&lt;/cif>
                  &lt;dataProcessingAgreement>{ data($fml/fml:DC_ZGODA_NA_PRZETW) }&lt;/dataProcessingAgreement>
                  &lt;employmentInfo>
                        &lt;monthlyNetIncomePLN>{ data($fml/fml:DC_MIES_DOCHOD_NETTO_W_PLN) }&lt;/monthlyNetIncomePLN>
                  &lt;/employmentInfo>
                  &lt;email>{ data($fml/fml:DC_ADRES_E_MAIL) }&lt;/email>
                  &lt;familyInfo>
                     &lt;numberOfHouseholdMembers>{ data($fml/fml:DC_LICZBA_OSOB_NA_UTRZ) }&lt;/numberOfHouseholdMembers>
                  &lt;/familyInfo>
                  &lt;firstName>{ data($fml/fml:DC_IMIE[1]) }&lt;/firstName>
                  &lt;homePhoneAreaCode>{ data($fml/fml:DC_NR_KIER_TELEFONU) }&lt;/homePhoneAreaCode>
                  { 
		       if (data($fml/fml:DC_NR_TELEFONU) != '') then
		          &lt;homePhoneAvailability>PHONE_AVAILABLE&lt;/homePhoneAvailability>
		       else 
	    		  &lt;homePhoneAvailability>NO_PHONE&lt;/homePhoneAvailability>
	           }
                  &lt;homePhoneNumber>{ data($fml/fml:DC_NR_TELEFONU) }&lt;/homePhoneNumber>
                  {
                      if (data($fml/fml:DC_NR_DOWODU_REGON[1]) != '') then
                      (
                          &lt;idType>IDENTITY_CARD&lt;/idType>,
                          &lt;idSerNum>{ data($fml/fml:DC_NR_DOWODU_REGON[1]) }&lt;/idSerNum>
                      )
                      else
                      (
                          &lt;idType>OTHER&lt;/idType>,
                          &lt;idSerNum>{ data($fml/fml:DC_NUMER_PASZPORTU[1]) }&lt;/idSerNum>
                      )
                  }
                  &lt;lastName>{ data($fml/fml:DC_NAZWISKO[1]) } { data($fml/fml:DC_WSP_NAZWISKO) }&lt;/lastName>
                  &lt;middleName>{ data($fml/fml:DC_DRUGIE_IMIE[1]) }&lt;/middleName>
                  &lt;mobilePhoneNumber>{ data($fml/fml:DC_NR_TELEF_KOMORKOWEGO) }&lt;/mobilePhoneNumber>
                  {
		         for $it at $p in $fml/fml:DC_MAKS_KWOTA_ZABEZPIECZONA return
		        (
                           &lt;obligations>
                           &lt;amount>{ data($fml/fml:DC_MAKS_KWOTA_ZABEZPIECZONA[$p]) }&lt;/amount>
                             {			
         		      if (data($fml/fml:DC_NR_BANKU[$p]) != '') then
                                &lt;bankInBIK>true&lt;/bankInBIK>
                             else  
                                &lt;bankInBIK>false&lt;/bankInBIK>
                             }
                            {
                             if (data($fml/fml:DC_SPOSOB_ZABEZPIECZENIA[$p]) != 'N') then
                               &lt;obligationCollateral>true&lt;/obligationCollateral>
                             else
                               &lt;obligationCollateral>false&lt;/obligationCollateral>
                             }
                            {
                             if (data($fml/fml:DC_RODZAJ_ZACHOWANIA[$p]) != 'N') then
                            &lt;businessActivity>true&lt;/businessActivity>
                            else
                            &lt;businessActivity>false&lt;/businessActivity>
                            }
                            {
                             if (data($fml/fml:DC_NUMER_ZABEZPIECZENIA[$p]) != 'N') then
                               &lt;applicantObligation>true&lt;/applicantObligation>
                            else
                               &lt;applicantObligation>false&lt;/applicantObligation>
                            }
                           &lt;bankInBIKName>{ data($fml/fml:DC_NR_BANKU[$p]) }&lt;/bankInBIKName>
                           &lt;bankNotInBIKName>{ data($fml/fml:DC_BANK_NADZORUJACY[$p]) }&lt;/bankNotInBIKName>
                           &lt;consolidation>0&lt;/consolidation>
                           &lt;currency>{ data($fml/fml:DC_KOD_WALUTY[$p]) }&lt;/currency>
                           &lt;monthlyInstalment>{ data($fml/fml:DC_PODSTAWA_MIESIECZNA[$p]) }&lt;/monthlyInstalment>
                           &lt;subject>{ data($fml/fml:DC_TYP_PAPIERU_WARTOSCIOWEGO[$p]) }&lt;/subject>
                           &lt;type>2&lt;/type>
                        &lt;/obligations>
                       )
                  }
                  &lt;pesel>{ data($fml/fml:DC_NR_PESEL[1]) }&lt;/pesel>
                  &lt;placeOfBirth>{ data($fml/fml:DC_MIEJSCE_URODZENIA) }&lt;/placeOfBirth>
                  &lt;primaryAddress>
                     &lt;city>{ data($fml/fml:DC_MIASTO_DANE_PODST[1]) }&lt;/city>
                     &lt;houseFlatNumber>{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST[1]) }&lt;/houseFlatNumber>
                     &lt;street>{ data($fml/fml:DC_ULICA_DANE_PODST[1]) }&lt;/street>
                     &lt;zipCode>{ data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST[1]) }&lt;/zipCode>
                     &lt;country>{ data($fml/fml:DC_WOJ_KRAJ_DANE_PODST[1]) }&lt;/country>
                  &lt;/primaryAddress>
                  &lt;secondaryAddress>
                     &lt;city>{ data($fml/fml:DC_MIASTO_ADRES_ALT) }&lt;/city>
                     &lt;houseFlatNumber>{ data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT) }&lt;/houseFlatNumber>
                     &lt;street>{ data($fml/fml:DC_ULICA_ADRES_ALT) }&lt;/street>
                     &lt;zipCode>{ data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT) }&lt;/zipCode>
                     &lt;country>{ data($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT[1]) }&lt;/country>
                  &lt;/secondaryAddress>
                   {			
       		      if (data($fml/fml:DC_PLEC) = 'M') then
                         &lt;sex>MALE&lt;/sex>
                     else 
                     if (data($fml/fml:DC_PLEC) = 'F') then
                         &lt;sex>FEMALE&lt;/sex>
                     else ()
                    }
               &lt;/customer>
            &lt;personalDataProcessingPermission>false&lt;/personalDataProcessingPermission>
            &lt;spouseCoapplicant>false&lt;/spouseCoapplicant>
            &lt;/customerRoles>
            &lt;destinationAccountNumber>{ data($fml/fml:DC_NR_RACHUNKU) }&lt;/destinationAccountNumber>
            &lt;destinationBranch>{ data($fml/fml:DC_NUMER_ODDZIALU) }&lt;/destinationBranch>
            &lt;firstRepaymentDate>{ xf:cekeDateToDateType(data($fml/fml:DC_DATA_PIERWSZEJ_PLATNOSCI)) }&lt;/firstRepaymentDate>
            &lt;instalmentAmount>{ data($fml/fml:DC_KWOTA_RATY_LUB_PROCENT) }&lt;/instalmentAmount>
            &lt;instalmentType>{ data($fml/fml:DC_TYP_PLATNOSCI) }&lt;/instalmentType>
            &lt;interestRate>{ data($fml/fml:DC_STOPA_PROCENTOWA) }&lt;/interestRate>
            &lt;interestType>{ data($fml/fml:DC_KOD_STOPY) }&lt;/interestType>
            &lt;loanPeriod>{ data($fml/fml:DC_LICZBA_RAT) }&lt;/loanPeriod>
            &lt;pppInsuranceFee>{ data($fml/fml:DC_UBEZPIECZENIA_INNE) }&lt;/pppInsuranceFee>
            &lt;product>{ data($fml/fml:DC_PRODUKT) }&lt;/product>
            &lt;purpose>{ data($fml/fml:DC_CEL_KREDYTOWANIA) }&lt;/purpose>
            &lt;registrationBranch>{ data($fml/fml:DC_NUMER_ODDZIALU) }&lt;/registrationBranch>
            &lt;repaymentAccountNumber>{ data($fml/fml:DC_NR_RACHUNKU) }&lt;/repaymentAccountNumber>
            &lt;repaymentDay>{ data($fml/fml:DC_SPECYFICZNY_DZIEN_SPLATY) }&lt;/repaymentDay>
            &lt;scheduledDecisionDate>{ xf:cekeDateToDateType(data($fml/fml:DC_DATA_OTWARCIA)) }&lt;/scheduledDecisionDate>
            &lt;source>{ data($fml/fml:DC_ZRODLO) }&lt;/source>
         &lt;/application>
         &lt;authData>
            &lt;applicationId>{ data($fml/fml:U_APPLICATION_ID) }&lt;/applicationId>
            &lt;applicationPassword>{ data($fml/fml:U_APPLICATION_PWD) }&lt;/applicationPassword>
            &lt;operator>&lt;/operator>
         &lt;/authData>
      &lt;/loan:createApplication>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_GetDictionaryInfoRequest($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>