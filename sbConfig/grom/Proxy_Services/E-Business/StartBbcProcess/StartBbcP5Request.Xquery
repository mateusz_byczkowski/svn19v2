<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/BZWBK24flow/messages/";


declare function xf:map_getStartBbcP5Request($fml as element(fml:FML32))
	as element(urn:StartBBC5Request ) {
		&lt;urn:StartBBC5Request>
			&lt;urn:CekeBBCP005ApplicationBean>
				&lt;urn:documentConfig>
					{
						for $it at $i in $fml/fml:E_AGR_INDEX_NAME return
						(
                                                       if ($fml/fml:E_AGR_INDEX_NAME)
                                                       then
                                                       (
                                                                if ($fml/fml:E_AGR_INDEX_TYPE[$i] = 'D' and $fml/fml:E_AGR_INDEX_VALUE[$i]='')
                                                                then
                                                                ( )
                                                                else
                                                                (
                               					        &lt;urn:ElementArrayOf_tns1_DocumentConfigurationBean>
				         			              &lt;urn:name>{ data($fml/fml:E_AGR_INDEX_NAME[$i]) }&lt;/urn:name>
					        		              &lt;urn:type>{ data($fml/fml:E_AGR_INDEX_TYPE[$i]) }&lt;/urn:type>
						        	              &lt;urn:value>{ data($fml/fml:E_AGR_INDEX_VALUE[$i]) }&lt;/urn:value>
                      					 	        &lt;/urn:ElementArrayOf_tns1_DocumentConfigurationBean>
                                                                )
                                                       )
                                                       else ()
						)
					}
				&lt;/urn:documentConfig>
				{
					if ($fml/fml:E_AGR_SIGNATURE)
					then &lt;urn:contractId>{ data($fml/fml:E_AGR_SIGNATURE) }&lt;/urn:contractId>
					else ()
				}
				{				
					if ($fml/fml:E_AGR_CLASS)
					then &lt;urn:documentClassName>{ data($fml/fml:E_AGR_CLASS) }&lt;/urn:documentClassName>
					else ()
				}
				{
					if ($fml/fml:CI_SKP_PRACOWNIKA)
					then &lt;urn:skp>{ data($fml/fml:CI_SKP_PRACOWNIKA) }&lt;/urn:skp>
					else ()
				}
				{
					if ($fml/fml:E_AGR_NAME)
					then &lt;urn:documentName>{ data($fml/fml:E_AGR_NAME) }&lt;/urn:documentName>
					else()
				}
				{
					if ($fml/fml:E_AGR_DESC)
					then &lt;urn:documentDescription>{ data($fml/fml:E_AGR_DESC) }&lt;/urn:documentDescription>
					else ()
				}
			&lt;/urn:CekeBBCP005ApplicationBean>
		&lt;/urn:StartBBC5Request>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_getStartBbcP5Request($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>