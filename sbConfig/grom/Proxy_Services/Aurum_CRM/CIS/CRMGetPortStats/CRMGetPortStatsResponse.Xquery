<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function local:mapping($fml as element(fml:FML32), $globals as element(fml:FML32),
	$req as element(fml:FML32)) as element() {
		&lt;statistics>
		    &lt;level_id>{ data($req/fml:CI_ID_POZIOMU) }&lt;/level_id>
		    &lt;agg_level>{ data($req/fml:CI_POZIOM_AGREGACJI) }&lt;/agg_level>
		    &lt;service_class>{ data($req/fml:CI_KLASA_OBSLUGI) }&lt;/service_class>
		    &lt;report_range>{ data($req/fml:CI_ZAKRES_RAPORTU) }&lt;/report_range>
		    &lt;cust_type_in>{ data($req/fml:DC_TYP_KLIENTA) }&lt;/cust_type_in>
		    &lt;update_date>{ data($fml/fml:CI_DATA_AKTUALIZACJI[1]) }&lt;/update_date>
		    &lt;report_update_date>{ data($fml/fml:CI_DATA_ZMIANY_STAT) }&lt;/report_update_date>
			&lt;report>
				&lt;cust_type code="{ $req/fml:DC_TYP_KLIENTA }">
		            &lt;global_params>
{
	for $klasaObslugi at $p in $globals/fml:CI_KLASA_OBSLUGI
		let $klasa := fn:normalize-space($klasaObslugi)
		let $sum := data($globals/CI_ILOSC1[$p])
			return
				if($klasa = '') then
						&lt;param code="SUM_COUNT" type="C">{ $sum }&lt;/param>
				else if($klasa = '02') then
		                &lt;param code="PREMIUM_COUNT" type="C">{ $sum }&lt;/param>
		        else if($klasa = '01') then
		                &lt;param code="CLASSIC_COUNT" type="C">{ $sum }&lt;/param>
		        else
		        	()
}
		            &lt;/global_params>
					&lt;prd_groups>
{
	for $kodProduktu at $p in $fml/fml:CI_KOD_PRODUKTU
	let $kodGrupy1 := fn:normalize-space(data($fml/fml:CI_KOD_PRODUKTU1[$p]))
	let $kodGrupy2 := fn:normalize-space(data($fml/fml:CI_KOD_PRODUKTU2[$p]))
	let $kodGrupy3 := fn:normalize-space(data($fml/fml:CI_KOD_PRODUKTU3[$p])) return
						&lt;prd_group code="{ $kodProduktu }">
{
	if($kodGrupy1 = '2') then
							(&lt;param code="ACC_DEP_COUNT" type="C">{ data($fml/CI_ILOSC1[$p]) }&lt;/param>,
							(:
							21.09.2007 biznes nie chce tego pola
							&lt;param code="ACC_LIM_COUNT" type="C">{ data($fml/CI_ILOSC2[$p]) }&lt;/param>,
							:)
							&lt;param code="INT_IN_OPER_COUNT" type="C">{ data($fml/CI_ILOSC6[$p]) }&lt;/param>,
							&lt;param code="INT_OUT_OPER_COUNT" type="C">{ data($fml/CI_ILOSC5[$p]) }&lt;/param>,
							&lt;param code="CONST_ORD_COUNT" type="C">{ data($fml/CI_ILOSC3[$p]) }&lt;/param>,
							&lt;param code="DIR_DEBIT_COUNT" type="C">{ data($fml/CI_ILOSC4[$p]) }&lt;/param>,
							&lt;param code="BALANCE_TOTAL" type="V">{ data($fml/CI_WARTOSC1[$p]) }&lt;/param>,
							&lt;param code="LIMIT_TOTAL" type="V">{ data($fml/CI_WARTOSC2[$p]) }&lt;/param>,
							&lt;param code="INT_IN_OPER_TOTAL" type="V">{ data($fml/CI_WARTOSC4[$p]) }&lt;/param>,
							&lt;param code="INT_OUT_OPER_TOTAL" type="V">{ data($fml/CI_WARTOSC3[$p]) }&lt;/param>)
	else if($kodGrupy1 = '10') then
							(&lt;param code="ACC_COUNT" type="C">{ data($fml/CI_ILOSC1[$p]) }&lt;/param>,
							&lt;param code="BALANCE_TOTAL" type="V">{ data($fml/CI_WARTOSC1[$p]) }&lt;/param>,
							&lt;param code="AVG_BALANCE_1M" type="V">{ data($fml/CI_WARTOSC2[$p]) }&lt;/param>,
							&lt;param code="AVG_BALANCE_3M" type="V">{ data($fml/CI_WARTOSC3[$p]) }&lt;/param>)
	else if($kodGrupy1 = '3') then
							(&lt;param code="ACC_COUNT" type="C">{ data($fml/CI_ILOSC1[$p]) }&lt;/param>,
							&lt;param code="INIT_COMMIT_TOTAL" type="V">{ data($fml/CI_WARTOSC4[$p]) }&lt;/param>,
							&lt;param code="BALANCE_TOTAL" type="V">{ data($fml/CI_WARTOSC1[$p]) }&lt;/param>,
							&lt;param code="AVG_CONS_1M" type="V">{ data($fml/CI_WARTOSC2[$p]) }&lt;/param>)
	else if($kodGrupy1 = '8') then
							&lt;param code="CUSTOMER_COUNT" type="C">{ data($fml/CI_ILOSC1[$p]) }&lt;/param>
	else if($kodGrupy1 = '1' and $kodGrupy3 != '1055') then
							(&lt;param code="CARD_COUNT" type="C">{ data($fml/CI_ILOSC1[$p]) }&lt;/param>,
							&lt;param code="CARD_LIMIT_TOTAL" type="V">{ data($fml/CI_WARTOSC4[$p]) }&lt;/param>)
	else if($kodGrupy3 = '1055') then
							(&lt;param code="RATIO_CREDIT_COUNT" type="C">{ data($fml/CI_ILOSC1[$p]) }&lt;/param>,
							&lt;param code="ACC_CREDIT_COUNT" type="V">{ data($fml/CI_WARTOSC4[$p]) }&lt;/param>)	 
(:	else if($kodGrupy1 = '?????') then
							(&lt;param code="ACC_COUNT" type="C">{ data($fml/CI_ILOSC1[$p]) }&lt;/param>,
							&lt;param code="BALANCE_TOTAL" type="V">{ data($fml/CI_WARTOSC1[$p]) }&lt;/param>,
							&lt;param code="AVG_BALANCE_1M" type="V">{ data($fml/CI_WARTOSC2[$p]) }&lt;/param>,
							&lt;param code="AVG_BALANCE_3M" type="V">{ data($fml/CI_WARTOSC3[$p]) }&lt;/param>)
:)
	else if($kodGrupy1 = '4') then
							(&lt;param code="ACC_COUNT" type="C">{ data($fml/CI_ILOSC1[$p]) }&lt;/param>,
							&lt;param code="BALANCE_TOTAL" type="V">{ data($fml/CI_WARTOSC1[$p]) }&lt;/param>)
	else if($kodGrupy1 = '12') then
							(&lt;param code="ACC_COUNT" type="C">{ data($fml/CI_ILOSC1[$p]) }&lt;/param>,
							&lt;param code="BALANCE_TOTAL" type="V">{ data($fml/CI_WARTOSC1[$p]) }&lt;/param>)
	else if($kodGrupy1 = '15' or $kodGrupy1 = '16' or $kodGrupy1 = '17') then
							(&lt;param code="ACC_COUNT" type="C">{ data($fml/CI_ILOSC1[$p]) }&lt;/param>,
							&lt;param code="BALANCE_TOTAL" type="V">{ data($fml/CI_WARTOSC1[$p]) }&lt;/param>)
	else
		(&lt;!-- nieznana grupa -->)
}
						&lt;/prd_group>
}
					&lt;/prd_groups>
				&lt;/cust_type>
			&lt;/report>
		&lt;/statistics>
};

declare variable $fml as element(fml:FML32) external;
declare variable $req as element(fml:FML32) external;
declare variable $globals as element(fml:FML32) external;

&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	&lt;m:CRMGetPortStatsResponse>{ fn-bea:serialize(local:mapping($fml, $globals, $req)) }&lt;/m:CRMGetPortStatsResponse>
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>