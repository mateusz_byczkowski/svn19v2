<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/prelimit/faults/";
declare variable $body external;

declare function local:fault($faultString as xs:string, $detail as element()) as element(soap-env:Fault) {
    &lt;soap-env:Fault>
        &lt;faultcode>soapenv:Server.userException&lt;/faultcode> 
        &lt;faultstring>{ $faultString }&lt;/faultstring> 
        &lt;detail>{ $detail }&lt;/detail>
    &lt;/soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string) as element()* {
    &lt;ErrorCode1>{ $errorCode1 }&lt;/ErrorCode1>,
    &lt;ErrorCode2>{ $errorCode2 }&lt;/ErrorCode2>
};

&lt;soap-env:Body>
    &lt;soap-env:Body>
    {
        let $reason := fn:substring-before(fn:substring-after(fn:substring-before($body/ctx:fault/ctx:reason, ":"), "("), ")")
        let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($body/ctx:fault/ctx:reason, ":"), ":"), ":")
        return
            if($reason = "13") then
                local:fault("com.bzwbk.services.ceke.faults.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode) })
            else if($reason = "11") then
                if($urcode = "103") then
                    local:fault("com.bzwbk.services.ceke.faults.NoDataException", element f:NoDataException { local:errors($reason, $urcode) })
                else
                    local:fault("com.bzwbk.services.ceke.faults.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode) })
            else
                local:fault("com.bzwbk.services.ceke.faults.ServiceException", element f:ServiceException { local:errors($reason, $urcode) })
    }
    &lt;/soap-env:Body>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>