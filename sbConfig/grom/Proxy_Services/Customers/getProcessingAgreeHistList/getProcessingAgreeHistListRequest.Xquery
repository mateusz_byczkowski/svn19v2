<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:primeaccounts.entities.be.dcl";
declare namespace urn2 = "urn:accounts.entities.be.dcl";
declare namespace urn3 = "urn:cif.entities.be.dcl";
declare namespace pdk = "http://bzwbk.com/services/pdk/";

declare function local:invoke($invoke as element(urn:invoke)) as element(soap-env:Body) {
   &lt;soap-env:Body>
      &lt;pdk:GetArt105History>
         &lt;pdk:Cif>{ data($invoke/urn:customer[1]/urn3:Customer[1]/urn3:customerNumber[1]) }&lt;/pdk:Cif>
         {
            let $ccacc := data($invoke/urn:creditCardAccount[1]/urn1:CreditCardAccount[1]/urn1:accountNumber[1])
            return
                if($ccacc)
                then (
                    &lt;pdk:NrRachunku>{ $ccacc }&lt;/pdk:NrRachunku>,
                    &lt;pdk:System>P&lt;/pdk:System>
                )
                else (
                    &lt;pdk:NrRachunku>{ data($invoke/urn:account[1]/urn2:Account[1]/urn2:accountNumber[1]) }&lt;/pdk:NrRachunku>,
                    &lt;pdk:System>I&lt;/pdk:System>
                )
         }
      &lt;/pdk:GetArt105History>
   &lt;/soap-env:Body>
};

declare variable $body as element(soap-env:Body) external;

local:invoke($body/urn:invoke[1])</con:xquery>
</con:xqueryEntry>