<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
<soap:Body>
  <fml:FML32>
    <fml:CI_SERIA_NR_DOK>XX1234567</fml:CI_SERIA_NR_DOK>
    <fml:CI_DOK_TOZSAMOSCI>1</fml:CI_DOK_TOZSAMOSCI>
    <fml:CI_NAZWA_PELNA>NAZWA FIRMY</fml:CI_NAZWA_PELNA>
    <fml:DC_NUMER_KLIENTA>0000000022</fml:DC_NUMER_KLIENTA>
    <fml:DC_REZ_NIEREZ>1</fml:DC_REZ_NIEREZ>
    <fml:DC_NIP>111-11-11-111</fml:DC_NIP>
    <fml:DC_NAZWA_SKROCONA>NAZFIR</fml:DC_NAZWA_SKROCONA>
    <fml:CI_SKP_PRACOWNIKA>20001</fml:CI_SKP_PRACOWNIKA>
    <fml:CI_FOP>SPӣKA AKCYJNA</fml:CI_FOP>
    <fml:CI_PROCENT_PODATKU>19.0</fml:CI_PROCENT_PODATKU>
    <fml:DC_NAZWISKO>Kowalski</fml:DC_NAZWISKO>
    <fml:DC_IMIE>Jan</fml:DC_IMIE>
    <fml:DC_SEGMENT_MARKETINGOWY>99</fml:DC_SEGMENT_MARKETINGOWY>
    <fml:CI_UDOSTEP_GRUPA>5</fml:CI_UDOSTEP_GRUPA>
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>