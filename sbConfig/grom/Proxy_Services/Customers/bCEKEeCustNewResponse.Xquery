<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-08-12</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbCEKEeCustNewResponse($fml as element(fml:FML32))
	as element(m:bCEKEeCustNewResponse) {
		&lt;m:bCEKEeCustNewResponse>
			{
				if($fml/fml:B_SYS_MASK)
					then &lt;m:SysMask>{ data($fml/fml:B_SYS_MASK) }&lt;/m:SysMask>
					else ()
			}
			{
				if($fml/fml:E_LOGIN_ID)
					then &lt;m:LoginId>{ data($fml/fml:E_LOGIN_ID) }&lt;/m:LoginId>
					else ()
			}
			{
				if($fml/fml:B_NR_DOK)
					then &lt;m:NrDok>{ data($fml/fml:B_NR_DOK) }&lt;/m:NrDok>
					else ()
			}
			{
				if($fml/fml:B_RODZ_DOK)
					then &lt;m:RodzDok>{ data($fml/fml:B_RODZ_DOK) }&lt;/m:RodzDok>
					else ()
			}
			{
				if($fml/fml:B_ID_ODDZ)
					then &lt;m:IdOddz>{ data($fml/fml:B_ID_ODDZ) }&lt;/m:IdOddz>
					else ()
			}
			{
				if($fml/fml:E_LIMIT)
					then &lt;m:Limit>{ data($fml/fml:E_LIMIT) }&lt;/m:Limit>
					else ()
			}
			{
				if($fml/fml:E_AMOUNT_REMAINING)
					then &lt;m:AmountRemaining>{ data($fml/fml:E_AMOUNT_REMAINING) }&lt;/m:AmountRemaining>
					else ()
			}
			{
				if($fml/fml:E_CYCLE)
					then &lt;m:Cycle>{ data($fml/fml:E_CYCLE) }&lt;/m:Cycle>
					else ()
			}
			{
				if($fml/fml:E_CYCLE_BEGIN)
					then &lt;m:CycleBegin>{ data($fml/fml:E_CYCLE_BEGIN) }&lt;/m:CycleBegin>
					else ()
			}
			{
				if($fml/fml:E_EX_CYCLE)
					then &lt;m:ExCycle>{ data($fml/fml:E_EX_CYCLE) }&lt;/m:ExCycle>
					else ()
			}
			{
				if($fml/fml:E_EX_CYCLE_BEGIN)
					then &lt;m:ExCycleBegin>{ data($fml/fml:E_EX_CYCLE_BEGIN) }&lt;/m:ExCycleBegin>
					else ()
			}
			{
				if($fml/fml:E_EX_LIMIT)
					then &lt;m:ExLimit>{ data($fml/fml:E_EX_LIMIT) }&lt;/m:ExLimit>
					else ()
			}
			{
				if($fml/fml:E_TRN_LIMIT)
					then &lt;m:TrnLimit>{ data($fml/fml:E_TRN_LIMIT) }&lt;/m:TrnLimit>
					else ()
			}
			{
				if($fml/fml:E_EX_AMOUNT_REMAINING)
					then &lt;m:ExAmountRemaining>{ data($fml/fml:E_EX_AMOUNT_REMAINING) }&lt;/m:ExAmountRemaining>
					else ()
			}
			{
				if($fml/fml:E_HI_AMOUNT_REMAINING)
					then &lt;m:HiAmountRemaining>{ data($fml/fml:E_HI_AMOUNT_REMAINING) }&lt;/m:HiAmountRemaining>
					else ()
			}
			{
				if($fml/fml:E_HI_CYCLE)
					then &lt;m:HiCycle>{ data($fml/fml:E_HI_CYCLE) }&lt;/m:HiCycle>
					else ()
			}
			{
				if($fml/fml:E_HI_CYCLE_BEGIN)
					then &lt;m:HiCycleBegin>{ data($fml/fml:E_HI_CYCLE_BEGIN) }&lt;/m:HiCycleBegin>
					else ()
			}
			{
				if($fml/fml:E_HI_LIMIT)
					then &lt;m:HiLimit>{ data($fml/fml:E_HI_LIMIT) }&lt;/m:HiLimit>
					else ()
			}
			{
				if($fml/fml:E_REP_NAME)
					then &lt;m:RepName>{ data($fml/fml:E_REP_NAME) }&lt;/m:RepName>
					else ()
			}
			{
				if($fml/fml:E_CUST_SMSNO)
					then &lt;m:CustSmsno>{ data($fml/fml:E_CUST_SMSNO) }&lt;/m:CustSmsno>
					else ()
			}
			{
				if($fml/fml:E_CUST_NAME)
					then &lt;m:CustName>{ data($fml/fml:E_CUST_NAME) }&lt;/m:CustName>
					else ()
			}
			{
				if($fml/fml:E_CUST_CITY)
					then &lt;m:CustCity>{ data($fml/fml:E_CUST_CITY) }&lt;/m:CustCity>
					else ()
			}
			{
				if($fml/fml:E_CUST_ZIPCODE)
					then &lt;m:CustZipcode>{ data($fml/fml:E_CUST_ZIPCODE) }&lt;/m:CustZipcode>
					else ()
			}
			{
				if($fml/fml:E_CUST_STREET)
					then &lt;m:CustStreet>{ data($fml/fml:E_CUST_STREET) }&lt;/m:CustStreet>
					else ()
			}
			{
				if($fml/fml:E_CUST_TELNO)
					then &lt;m:CustTelno>{ data($fml/fml:E_CUST_TELNO) }&lt;/m:CustTelno>
					else ()
			}
			{
				if($fml/fml:E_CUST_FAXNO)
					then &lt;m:CustFaxno>{ data($fml/fml:E_CUST_FAXNO) }&lt;/m:CustFaxno>
					else ()
			}
			{
				if($fml/fml:E_CUST_GSMNO)
					then &lt;m:CustGsmno>{ data($fml/fml:E_CUST_GSMNO) }&lt;/m:CustGsmno>
					else ()
			}
			{
				if($fml/fml:E_CUST_EMAIL)
					then &lt;m:CustEmail>{ data($fml/fml:E_CUST_EMAIL) }&lt;/m:CustEmail>
					else ()
			}
			{
				if($fml/fml:E_CUST_STATUS)
					then &lt;m:CustStatus>{ data($fml/fml:E_CUST_STATUS) }&lt;/m:CustStatus>
					else ()
			}
			{
				if($fml/fml:B_D_URODZENIA)
					then &lt;m:DUrodzenia>{ data($fml/fml:B_D_URODZENIA) }&lt;/m:DUrodzenia>
					else ()
			}
			{
				if($fml/fml:B_M_URODZENIA)
					then &lt;m:MUrodzenia>{ data($fml/fml:B_M_URODZENIA) }&lt;/m:MUrodzenia>
					else ()
			}
			{
				if($fml/fml:E_OLD_CUSTOMER_ID)
					then &lt;m:OldCustomerId>{ data($fml/fml:E_OLD_CUSTOMER_ID) }&lt;/m:OldCustomerId>
					else ()
			}
			{
				if($fml/fml:E_OLD_ID_TYPE)
					then &lt;m:OldIdType>{ data($fml/fml:E_OLD_ID_TYPE) }&lt;/m:OldIdType>
					else ()
			}
			{
				if($fml/fml:E_CUST_IDENTIF)
					then &lt;m:CustIdentif>{ data($fml/fml:E_CUST_IDENTIF) }&lt;/m:CustIdentif>
					else ()
			}
			{
				if($fml/fml:E_ADMIN_USER_INFO)
					then &lt;m:AdminUserInfo>{ data($fml/fml:E_ADMIN_USER_INFO) }&lt;/m:AdminUserInfo>
					else ()
			}
			{
				if($fml/fml:E_MICRO_BRANCH)
					then &lt;m:MicroBranch>{ data($fml/fml:E_MICRO_BRANCH) }&lt;/m:MicroBranch>
					else ()
			}
			{
				if($fml/fml:E_CIF_NUMBER)
					then &lt;m:CifNumber>{ data($fml/fml:E_CIF_NUMBER) }&lt;/m:CifNumber>
					else ()
			}
			{
				if($fml/fml:E_CUST_OPTIONS)
					then &lt;m:CustOptions>{ data($fml/fml:E_CUST_OPTIONS) }&lt;/m:CustOptions>
					else ()
			}
			{
				if($fml/fml:E_LAST_ACTIVATED)
					then &lt;m:LastActivated>{ data($fml/fml:E_LAST_ACTIVATED) }&lt;/m:LastActivated>
					else ()
			}
			{
				if($fml/fml:E_LAST_DEACTIVATED)
					then &lt;m:LastDeactivated>{ data($fml/fml:E_LAST_DEACTIVATED) }&lt;/m:LastDeactivated>
					else ()
			}
			{
				if($fml/fml:E_TOKEN_SERIAL_NO)
					then &lt;m:TokenSerialNo>{ data($fml/fml:E_TOKEN_SERIAL_NO) }&lt;/m:TokenSerialNo>
					else ()
			}
			{
				if($fml/fml:E_TOKEN_CUSTOMER_ID)
					then &lt;m:TokenCustomerId>{ data($fml/fml:E_TOKEN_CUSTOMER_ID) }&lt;/m:TokenCustomerId>
					else ()
			}
			{
				if($fml/fml:E_TOKEN_ID_TYPE)
					then &lt;m:TokenIdType>{ data($fml/fml:E_TOKEN_ID_TYPE) }&lt;/m:TokenIdType>
					else ()
			}
			{
				if($fml/fml:E_TOKEN_LOGIN_ID)
					then &lt;m:TokenLoginId>{ data($fml/fml:E_TOKEN_LOGIN_ID) }&lt;/m:TokenLoginId>
					else ()
			}
			{
				if($fml/fml:E_ALLOWED_SECURITY_LEVEL)
					then &lt;m:AllowedSecurityLevel>{ data($fml/fml:E_ALLOWED_SECURITY_LEVEL) }&lt;/m:AllowedSecurityLevel>
					else ()
			}
			{
				if($fml/fml:E_PROFILE_ID)
					then &lt;m:ProfileId>{ data($fml/fml:E_PROFILE_ID) }&lt;/m:ProfileId>
					else ()
			}
			{
				if($fml/fml:E_PROFILE_TRIAL)
					then &lt;m:ProfileTrial>{ data($fml/fml:E_PROFILE_TRIAL) }&lt;/m:ProfileTrial>
					else ()
			}
			{
				if($fml/fml:E_PROFILE_EXPIRY)
					then &lt;m:ProfileExpiry>{ data($fml/fml:E_PROFILE_EXPIRY) }&lt;/m:ProfileExpiry>
					else ()
			}
			{
				if($fml/fml:E_CUSTOMER_TYPE)
					then &lt;m:CustomerType>{ data($fml/fml:E_CUSTOMER_TYPE) }&lt;/m:CustomerType>
					else ()
			}
			{
				if($fml/fml:E_CIF_OPTIONS)
					then &lt;m:CifOptions>{ data($fml/fml:E_CIF_OPTIONS) }&lt;/m:CifOptions>
					else ()
			}
			{
				if($fml/fml:E_CUST_REBATE)
					then &lt;m:CustRebate>{ data($fml/fml:E_CUST_REBATE) }&lt;/m:CustRebate>
					else ()
			}
			{
				if($fml/fml:E_CUST_REPORT_DATE)
					then &lt;m:CustReportDate>{ data($fml/fml:E_CUST_REPORT_DATE) }&lt;/m:CustReportDate>
					else ()
			}
			{
				if($fml/fml:E_CUST_REPORT_VERSION)
					then &lt;m:CustReportVersion>{ data($fml/fml:E_CUST_REPORT_VERSION) }&lt;/m:CustReportVersion>
					else ()
			}
			{
				if($fml/fml:E_TIME_STAMP)
					then &lt;m:TimeStamp>{ data($fml/fml:E_TIME_STAMP) }&lt;/m:TimeStamp>
					else ()
			}
		&lt;/m:bCEKEeCustNewResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbCEKEeCustNewResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>