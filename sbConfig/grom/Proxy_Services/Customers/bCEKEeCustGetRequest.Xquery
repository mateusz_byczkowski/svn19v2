<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbCEKEeCustGetRequest($req as element(m:bCEKEeCustGetRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:RodzDok)
					then &lt;fml:B_RODZ_DOK>{ data($req/m:RodzDok) }&lt;/fml:B_RODZ_DOK>
					else ()
			}
			{
				if($req/m:NrDok)
					then &lt;fml:B_NR_DOK>{ data($req/m:NrDok) }&lt;/fml:B_NR_DOK>
					else ()
			}
			{
				if($req/m:Bank)
					then &lt;fml:B_BANK>{ data($req/m:Bank) }&lt;/fml:B_BANK>
					else ()
			}
			{
				if($req/m:Sys)
					then &lt;fml:B_SYS>{ data($req/m:Sys) }&lt;/fml:B_SYS>
					else ()
			}
			{
				if($req/m:RecCount)
					then &lt;fml:E_REC_COUNT>{ data($req/m:RecCount) }&lt;/fml:E_REC_COUNT>
					else ()
			}
			{
				if($req/m:LoginId)
					then &lt;fml:E_LOGIN_ID>{ data($req/m:LoginId) }&lt;/fml:E_LOGIN_ID>
					else ()
			}
			{
				if($req/m:CustName)
					then &lt;fml:E_CUST_NAME>{ data($req/m:CustName) }&lt;/fml:E_CUST_NAME>
					else ()
			}
			{
				if($req/m:CustCity)
					then &lt;fml:E_CUST_CITY>{ data($req/m:CustCity) }&lt;/fml:E_CUST_CITY>
					else ()
			}
			{
				if($req/m:CustStreet)
					then &lt;fml:E_CUST_STREET>{ data($req/m:CustStreet) }&lt;/fml:E_CUST_STREET>
					else ()
			}
			{
				if($req/m:CifNumber)
					then &lt;fml:E_CIF_NUMBER>{ data($req/m:CifNumber) }&lt;/fml:E_CIF_NUMBER>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbCEKEeCustGetRequest($body/m:bCEKEeCustGetRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>