<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";
declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace soapenc="http://schemas.xmlsoap.org/soap/encoding/";
declare namespace s="http://bzwbk.com/crw/services/sme/";

declare variable $inputMessage external;

&lt;soapenv:Body>
    &lt;s:getBikReport xmlns:s="http://bzwbk.com/crw/services/sme/">
         &lt;number>{$inputMessage//number/text()}&lt;/number>
         &lt;customer>{$inputMessage//customer/text()}&lt;/customer>
         &lt;authData>
{$inputMessage//authData/*}
         &lt;/authData>
    &lt;/s:getBikReport>
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>