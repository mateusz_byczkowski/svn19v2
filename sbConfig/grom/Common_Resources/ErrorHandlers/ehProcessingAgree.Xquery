<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>error handler dla usług chgProcessingAgreeParameters i getProcessingAgreeParameters</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err = "urn:errors.hlbsentities.be.dcl";
declare namespace pdk = "http://bzwbk.com/services/pdk/";

declare function local:response($fault as element(soap-env:Fault)) as element() {
        let $se := $fault/detail/pdk:ServiceException
        let $faultstring := fn:concat(data($se/pdk:Description), ", ", data($se/pdk:ErrDetails))
        return
            &lt;soap-env:Fault>
                &lt;faultcode>Server&lt;/faultcode>
                &lt;faultstring>{ $faultstring }&lt;/faultstring>
                &lt;detail>
                    &lt;err:ServiceFailException>
                        &lt;err:exceptionItem>
                            &lt;err:errorCode1>{ data($se/pdk:ErrorCode) }&lt;/err:errorCode1>
                            &lt;err:errorCode2>0&lt;/err:errorCode2>
                            &lt;err:errorDescription>{ $faultstring }&lt;/err:errorDescription>
                        &lt;/err:exceptionItem>
                    &lt;/err:ServiceFailException>
                &lt;/detail>
        &lt;/soap-env:Fault>
};

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body>{ local:response($body/soap-env:Fault) }&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>