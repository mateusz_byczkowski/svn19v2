<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:schema><![CDATA[<?xml version="1.0" encoding="UTF-8"?>
<!--
        Typy dotyczace klientow i agentow
        Version: 1.004
-->
<xs:schema
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:cmf-tech="http://jv.channel.cu.com.pl/cmf-technical-types" 
xmlns:cmf-biz="http://jv.channel.cu.com.pl/cmf-biz-types" 
xmlns:cmf-party="http://jv.channel.cu.com.pl/cmf-party-types"
targetNamespace="http://jv.channel.cu.com.pl/cmf-party-types">
   <xs:import namespace="http://jv.channel.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd"/>
   <xs:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd"/>

	<xs:annotation>
		<xs:documentation>Typy dotyczace klientow i agentow

            CVS log: 
            $Revision: 1.47 $ 
            $Source: /it/krwnuk/cvs_do_migracji/SIS/ESB/CMF/base/cmf-party-types.xsd,v $ 
            $Author: kadamowi $ 
            $Date: 2008-09-10 12:11:08 $</xs:documentation>
	</xs:annotation>

	<xs:complexType name="AddressListType">
		<xs:annotation>
			<xs:documentation>Lista adresów</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="address" type="cmf-biz:AddressType" maxOccurs="unbounded" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="PhoneListType">
		<xs:annotation>
			<xs:documentation>Lista Telefonów</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="phone" type="cmf-biz:PhoneType" maxOccurs="unbounded" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="PersonalDataType">
		<xs:annotation>
			<xs:documentation>Dane osoby</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:NameType"/>
			<xs:element name="dob" type="xs:date" maxOccurs="1" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Data urodzenia</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="dod" type="xs:date" maxOccurs="1" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Data śmierci</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="age" type="xs:int" minOccurs="0"/>
			<xs:element name="calculated-age-date" type="xs:date" minOccurs="0"/>
			<xs:element name="pob" type="cmf-tech:CmfString" maxOccurs="1" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Miejsce urodzenia</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="cob" type="cmf-tech:CmfString" maxOccurs="1" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Kraj urodzenia</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="title" type="cmf-biz:PersonTitleType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="sex" type="cmf-biz:SexType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="maritial-status" type="cmf-biz:MaritalStatusType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="citizenship" type="cmf-tech:CmfString" maxOccurs="1" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Obywatelstwo</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="country" type="cmf-tech:CmfString" maxOccurs="1" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Kraj zamieszkania</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="identifiers" type="cmf-biz:IdentifiersType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="email" type="cmf-biz:EmailType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="address-list" type="cmf-party:AddressListType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="phone-list" type="cmf-party:PhoneListType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="no-of-children" type="xs:int" maxOccurs="1" minOccurs="0"/>
			<xs:element name="no-of-vehicles" type="xs:int" maxOccurs="1" minOccurs="0"/>
			<xs:element name="occupation-code" type="cmf-biz:OccupationCodeType" minOccurs="0"/>
			<xs:element name="segmentation" type="xs:int" minOccurs="0" maxOccurs="1"/>
			<xs:element name="community-property" type="cmf-biz:CommunityPropertyType" minOccurs="0" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Wspólnota majątkowa</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="resident" type="xs:boolean" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Rezydent czy osoba ma PESEL</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgreementSourceType">
		<xs:sequence>
			<xs:element name="system" type="cmf-biz:SourceSystemNameType"/>
			<xs:element name="type" type="cmf-biz:AgreementTypeType"/>
			<xs:element name="id" type="cmf-biz:AgreementIdType"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgreementType">
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:AgreementNameType"/>
			<xs:element name="version" type="cmf-biz:AgreementVersionType" minOccurs="0"/>
			<xs:element name="validFrom" type="xs:date" minOccurs="0"/>
			<xs:element name="validTo" type="xs:date" minOccurs="0"/>
			<xs:element name="status" type="cmf-biz:AgreementStatusType" minOccurs="0"/>
			<xs:element name="source" type="cmf-party:AgreementSourceType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgreementListType">
		<xs:sequence>
			<xs:element name="agreement" type="cmf-party:AgreementType" minOccurs="1" maxOccurs="unbounded">
			</xs:element>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="SourceIdentifierType">
		<xs:sequence>
			<xs:element name="person-id" type="cmf-tech:CmfString"/>
			<xs:element name="source-system" type="cmf-biz:SourceSystemNameType"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="SourceIdListType">
		<xs:sequence>
			<xs:element name="identifier" type="cmf-party:SourceIdentifierType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="ClientType">
		<xs:annotation>
			<xs:documentation>Obiekt reprezentujący dane klienta, który może być osobą lub firmą</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="agreements" type="cmf-party:AgreementListType" minOccurs="0"/>
			<xs:element name="contact" type="cmf-biz:ContactType" minOccurs="0"/>
			<xs:element name="factories" type="cmf-biz:FactoryNameListType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="source-ids" type="cmf-party:SourceIdListType" minOccurs="0"/>
			<xs:element name="company-ind" type="cmf-biz:CompanyIndType" minOccurs="0"/>
			<xs:element name="moto-data" type="cmf-party:MotoDataType" minOccurs="0" maxOccurs="1"/>
			<xs:element name="jv-data" type="cmf-party:JVDataType" minOccurs="0" maxOccurs="1"/>
			<xs:element name="person" type="cmf-party:PersonalDataType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="split" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Udział procentowy klienta (np. w roli beneficjenta)</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
		<xs:attribute name="client-code" type="cmf-biz:ClientCodeType" use="optional"/>
		<xs:attribute name="role" type="cmf-biz:ClientRoleType"/>
	</xs:complexType>

	<xs:complexType name="ClientShortType">
	    <xs:sequence>
	        <xs:element name="client-code" type="cmf-biz:ClientCodeType"/>
	        <xs:element name="last-name" type="cmf-biz:PersonSurnameType"/>
	        <xs:element name="first-name" type="cmf-biz:PersonNameType" minOccurs="0"/>
	    </xs:sequence>
	</xs:complexType>

	<xs:complexType name="ClientListType">
		<xs:annotation>
			<xs:documentation>Lista klientów</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="client" type="cmf-party:ClientType" maxOccurs="unbounded" minOccurs="1"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="ClientShortListType">
	    <xs:sequence>
	        <xs:element name="client" minOccurs="0" maxOccurs="unbounded" type="cmf-party:ClientShortType"/>
	    </xs:sequence>
	</xs:complexType>

	<xs:complexType name="ProspectType">
		<xs:sequence>
			<xs:element name="person" type="cmf-party:PersonalDataType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="agreements" type="cmf-party:AgreementListType" minOccurs="0"/>
			<xs:element name="factories" type="cmf-biz:FactoryNameListType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="comment" type="cmf-tech:CmfString" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="OrganizationLevelType">
		<xs:annotation>
			<xs:documentation>Nazwa organizacyjna</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="function" type="xs:int">
				<xs:annotation>
					<xs:documentation>Funkcja: 
                        0 - agent, 
                        1 - kierownik. 
                        2 - dyrektor,
                        3 - struktura</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="description" type="cmf-biz:DescriptionType"/>
		</xs:sequence>
		<xs:attribute name="name" type="cmf-biz:OrganizationLevelNameType"/>
	</xs:complexType>

	<xs:complexType name="OrganizationLevelListType">
		<xs:sequence>
			<xs:element name="organization-level" type="cmf-party:OrganizationLevelType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="BusinessDataType">
		<xs:sequence>
			<xs:element name="status" type="cmf-biz:AgentStatusType" minOccurs="0"/>
			<xs:element name="organization-level" type="cmf-biz:OrganizationLevelNameType" minOccurs="0"/>
			<xs:element name="basic-level" type="cmf-biz:SalesLevelType" minOccurs="0"/>
			<xs:element name="override-level" type="cmf-biz:SalesLevelType" minOccurs="0"/>
			<xs:element name="distrib-source" type="cmf-biz:DistributionSourceNameType" minOccurs="0"/>
			<xs:element name="manager" type="cmf-biz:AgentIdType" minOccurs="0"/>
			<xs:element name="structure" type="cmf-biz:AgentIdType" minOccurs="0"/>
			<xs:element name="structure-name" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="factory" type="cmf-biz:FactoryNameType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgentHistoryItemType">
		<xs:sequence>
			<xs:element name="effective-date" type="xs:date"/>
			<xs:element name="change-date" type="xs:date"/>
			<xs:element name="status" type="cmf-biz:AgentStatusType"/>
			<xs:element name="organization-level" type="cmf-biz:OrganizationLevelNameType"/>
			<xs:element name="basic-level" type="cmf-biz:SalesLevelType"/>
			<xs:element name="override-level" type="cmf-biz:SalesLevelType" minOccurs="0"/>
			<xs:element name="distrib-source" type="cmf-biz:DistributionSourceNameType"/>
			<xs:element name="manager" type="cmf-biz:AgentIdType" minOccurs="0"/>
			<xs:element name="structure" type="cmf-biz:AgentIdType" minOccurs="0" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgentHistoryType">
		<xs:sequence>
			<xs:element name="history" type="cmf-party:AgentHistoryItemType" maxOccurs="unbounded" minOccurs="1"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgentLicenceType">
		<xs:sequence>
			<xs:element name="factory" type="cmf-biz:FactoryNameType"/>
			<xs:element name="name" type="cmf-tech:CmfString"/>
			<xs:element name="number" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="active" type="xs:int"/>
			<xs:element name="created" type="xs:date"/>
			<xs:element name="expired" type="xs:date" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgentLicenceListType">
		<xs:sequence>
			<xs:element name="licence" type="cmf-party:AgentLicenceType" maxOccurs="unbounded" minOccurs="1"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgentType">
		<xs:annotation>
			<xs:documentation>Obiekt reprezentujący dane agenta</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="person-data" type="cmf-party:PersonalDataType"/>
			<xs:element name="business-data" type="cmf-party:BusinessDataType" minOccurs="0"/>
			<xs:element name="agent-history" type="cmf-party:AgentHistoryType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="licence-list" type="cmf-party:AgentLicenceListType" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="agent-id" type="cmf-biz:AgentIdType"/>
		<xs:attribute name="person-id" type="cmf-biz:AgentIdType"/>
	</xs:complexType>

	<xs:complexType name="AgentSplitType">
		<xs:sequence>
			<xs:element name="agent-id" type="cmf-biz:AgentIdType"/>
			<xs:element name="share" type="cmf-biz:SplitType"/>
			<xs:element name="main" type="xs:int"/>
			<xs:element name="comm-ratio" type="xs:double"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgentIdListType">
		<xs:sequence>
			<xs:element name="agent-id" type="cmf-biz:AgentIdType" maxOccurs="unbounded" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgentSplitListType">
		<xs:sequence>
			<xs:element name="split" type="cmf-party:AgentSplitType" maxOccurs="unbounded" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgentListType">
		<xs:sequence>
			<xs:element name="agent" minOccurs="0" maxOccurs="unbounded" type="cmf-party:AgentType"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="AgencyType">
		<xs:annotation>
			<xs:documentation>Obiekt reprezentujący strukturę agenta</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:CompanyNameType"/>
			<xs:element name="address-list" type="cmf-party:AddressListType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="factory" type="cmf-biz:FactoryNameType" minOccurs="0"/>
			<xs:element name="region" type="cmf-biz:AgentIdType" maxOccurs="1" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="agency-id" type="cmf-biz:AgentIdType"/>
	</xs:complexType>

	<xs:complexType name="BeneficiaryType" abstract="false">
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:NameType"/>
			<xs:element name="company-ind" type="cmf-biz:CompanyIndType" minOccurs="0"/>
			<xs:element name="sex" type="cmf-biz:SexType" minOccurs="0"/>
			<xs:element name="dob" type="xs:date" maxOccurs="1" minOccurs="0"/>
			<xs:element name="identifiers" type="cmf-biz:IdentifiersType" maxOccurs="1" minOccurs="0"/>
			<xs:element name="kinship" type="cmf-biz:KinshipType" minOccurs="0"/>
			<xs:element name="main" type="xs:int" minOccurs="0">
				<xs:annotation>
					<xs:documentation>1 - primery beneficiary
					  0 - secondary</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="split" type="xs:float"/>
			<xs:element name="address" type="cmf-biz:AddressType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="BeneficiaryListType">
		<xs:sequence>
			<xs:element name="beneficiary" type="cmf-party:BeneficiaryType" minOccurs="1" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="MotoDataType">
		<xs:annotation>
			<xs:documentation>Client data for Moto project
				no-damage-ac - period with no AC damage
				no-damage-oc - period with no OC damage
				damage-list  - list of previous damages</xs:documentation>
		</xs:annotation>
		<xs:sequence minOccurs="0">
			<xs:element name="no-damage-ac" type="xs:int"/>
			<xs:element name="no-damage-oc" type="xs:int"/>
			<xs:element name="damage-list" type="cmf-biz:DamageListType"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="JVDataType">
		<xs:annotation>
			<xs:documentation>
			  Dane specyficzne dla JV
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="giodo-status" type="cmf-biz:JVGiodoStatusType" minOccurs="0"/>
			<xs:element name="branch-id" type="cmf-biz:JVBranchIdType" minOccurs="0"/>
			<xs:element name="facsimile-signature" type="cmf-biz:FacsimileSignatureType" minOccurs="0"/>
			<xs:element name="language-code" type="cmf-biz:LanguageCodeType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>

	<xs:complexType name="UserType">
	    <xs:sequence>
	        <xs:element name="name" type="cmf-biz:NameType"/>
	        <xs:element name="principal" type="cmf-biz:UserIdType" minOccurs="0"/>
	        <xs:element name="position" type="cmf-tech:CmfString" minOccurs="0"/>
	        <xs:element name="cost-center" type="cmf-biz:CostCenterType" minOccurs="0"/>
	        <xs:element name="email" type="cmf-biz:EmailType" minOccurs="0"/>
	        <xs:element name="phone-list" type="cmf-party:PhoneListType" minOccurs="0"/>
	    </xs:sequence>
	    <xs:attribute name="user-id" type="cmf-biz:UserIdType"/>
	</xs:complexType>

</xs:schema>]]></con:schema>
  <con:dependencies>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd" ref="Business Services/ESB JV-CUP/bClientJV/base/cmf-technical-types"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd" ref="Business Services/ESB JV-CUP/bClientJV/base/cmf-biz-types"/>
  </con:dependencies>
  <con:targetNamespace>http://jv.channel.cu.com.pl/cmf-party-types</con:targetNamespace>
</con:schemaEntry>