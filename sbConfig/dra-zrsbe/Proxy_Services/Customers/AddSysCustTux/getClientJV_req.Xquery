<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace wsdl="http://jv.channel.cu.com.pl/cmf/wsdl-jv";

declare variable $invocationBody external;

&lt;getClientJVRequest xmlns="http://jv.channel.cu.com.pl/cmf/wsdl-jv"&gt;
	&lt;envelope&gt;
		&lt;request-time&gt; { data($invocationBody/wsdl:addClientJVRequest/wsdl:envelope/wsdl:request-time) }&lt;/request-time&gt;
		&lt;source-code&gt; { data($invocationBody/wsdl:addClientJVRequest/wsdl:envelope/wsdl:source-code) }&lt;/source-code&gt;
	&lt;/envelope&gt;
	&lt;client-code&gt; { data($invocationBody/wsdl:addClientJVRequest/wsdl:customer/@client-code) }&lt;/client-code&gt;
	&lt;factory&gt; { data($invocationBody/wsdl:addClientJVRequest/wsdl:customer/wsdl:factories/wsdl:factory) }&lt;/factory&gt;
&lt;/getClientJVRequest&gt;</con:xquery>
</con:xqueryEntry>