<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "urn:be.services.dcl/mappings";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/"; 

declare function xf:mapgetCustAllDataRequest( $reqhead as element(urn:header),$req  as element(urn1:Customer))
	as element(FML32) {
	&lt;FML32&gt;
	{
	   if ($reqhead/urn:msgHeader/urn:companyId)
	   then &lt;CI_ID_SPOLKI&gt;{data($reqhead/urn:msgHeader/urn:companyId)}&lt;/CI_ID_SPOLKI&gt;
	   else () 
	}
	{
	   if ($req/urn1:customerNumber)
	   then &lt;DC_NUMER_KLIENTA&gt;{data($req/urn1:customerNumber)}&lt;/DC_NUMER_KLIENTA&gt;
	   else () 
	}
	&lt;/FML32&gt;
};
 
declare variable $body external;
declare variable $header external;

&lt;soap-env:Body&gt;
{ xf:mapgetCustAllDataRequest($header/urn:header,$body/urn:invoke/urn:customer/urn1:Customer) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>