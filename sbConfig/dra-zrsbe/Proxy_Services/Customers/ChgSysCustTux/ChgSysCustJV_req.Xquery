<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace wsdl="http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace xf="http://jv.channel.cu.com.pl/cmf/functions";

declare variable $body external;
declare variable $fml as element(FML32):=$body/FML32;

declare function xf:parseElementGIODO($fmlElement as element()*, $parseName as xs:string)
{
	if (data($fmlElement)) then
		if (data($fmlElement) ne ' ') then
			element {$parseName} {data($fmlElement)}
		else ()
	else ()
};

declare function xf:parseGroupAgreement($groupAgreement as xs:string)
{
		if (($groupAgreement = '2')
		or  ($groupAgreement = '4')) then
			2
		else        (: groupAgreement in ('0', '1', '3', '99999') :)
			4
};

declare function xf:parseCorespelAgreement($corespelAgreement as xs:string)
{
	if ($corespelAgreement = '1') then
		2
	else if ($corespelAgreement = '0') then
		4
	else ()
};

declare function xf:parseBankSecret($bankSecret as xs:string)
{
	if ($bankSecret = '1') then
		2
	else if ($bankSecret = '0') then
		4
	else ()
};

declare function xf:parseDateTime($dateTimeString as xs:string)
{
	fn:string-join((fn:substring($dateTimeString, 7, 4),
					'-',
					fn:substring($dateTimeString, 4, 2),
					'-',
					fn:substring($dateTimeString, 1, 2),
					'T00:00:00'),
					'')
};

declare function xf:parseDate($dateString as xs:string)
{
	fn:string-join((fn:substring($dateString, 7, 4),
					'-',
					fn:substring($dateString, 4, 2),
					'-',
					fn:substring($dateString, 1, 2)),
					'')
};

declare function xf:parseElementAsDateTime($fmlElement as element()*, $parseName as xs:string)
{
	if (data($fmlElement)) then
		element {$parseName} {xf:parseDateTime($fmlElement)}
	else ()
};

declare function xf:parseElementAsDate($fmlElement as element()*, $parseName as xs:string)
{
	if (data($fmlElement)) then
		element {$parseName} {xf:parseDate($fmlElement)}
	else ()
};

declare function xf:parseElement($fmlElement as element()*, $parseName as xs:string)
{
	if (data($fmlElement)) then
		element {$parseName} {data($fmlElement)}
	else ()
};

&lt;soap-env:Body&gt;
	&lt;wsdl:changeClientJVRequest&gt;
	{
	if (data($fml/CI_DATA_WPROWADZENIA)
	or  data($fml/DC_TRN_ID)
	or  data($fml/DC_ODDZIAL)
	or  data($fml/CI_PRACOWNIK_WPROW)) then
		&lt;wsdl:envelope&gt;
			{ xf:parseElementAsDateTime($fml/CI_DATA_WPROWADZENIA, 'wsdl:request-time') }
			{ xf:parseElement($fml/DC_TRN_ID, 'wsdl:request-no') }
			&lt;wsdl:source-code&gt;placowka banku&lt;/wsdl:source-code&gt;
			{ xf:parseElement($fml/DC_ODDZIAL, 'wsdl:branch-id') }
			{ xf:parseElement($fml/CI_PRACOWNIK_WPROW, 'wsdl:user-id') }
		&lt;/wsdl:envelope&gt;
	else ()
	}
	{
	if (data($fml/DC_NUMER_KLIENTA)) then
		&lt;wsdl:customer client-code="{ data($fml/DC_NUMER_KLIENTA) }"&gt;
		{
		if (data($fml/CI_UDOSTEP_GRUPA)
		or  data($fml/CI_ZGODA_INF_HANDLOWA)
		or  data($fml/CI_ZGODA_TAJEM_BANK)) then
			&lt;wsdl:agreements&gt;
			{
			if (data($fml/CI_UDOSTEP_GRUPA)) then
				&lt;wsdl:agreement&gt;
					&lt;wsdl:name&gt;PMP&lt;/wsdl:name&gt;	(: zgoda na przetwarzanie danych :)
					&lt;wsdl:status&gt;{ xf:parseGroupAgreement(data($fml/CI_UDOSTEP_GRUPA)) }&lt;/wsdl:status&gt;
				&lt;/wsdl:agreement&gt;
			else ()
			}
			{
			if (data($fml/CI_ZGODA_INF_HANDLOWA)) then
				&lt;wsdl:agreement&gt;
					&lt;wsdl:name&gt;RCI&lt;/wsdl:name&gt;	(: zgoda na otrzymywanie informacji handlowych :)
					&lt;wsdl:status&gt;{ xf:parseCorespelAgreement(data($fml/CI_ZGODA_INF_HANDLOWA)) }&lt;/wsdl:status&gt;
				&lt;/wsdl:agreement&gt;
			else ()
			}
			{
			if (data($fml/CI_ZGODA_TAJEM_BANK)) then
				&lt;wsdl:agreement&gt;
					&lt;wsdl:name&gt;DBS&lt;/wsdl:name&gt;	(: zgoda na ujawnienie tajemnicy bankowej :)
					&lt;wsdl:status&gt;{ xf:parseBankSecret(data($fml/CI_ZGODA_TAJEM_BANK)) }&lt;/wsdl:status&gt;
				&lt;/wsdl:agreement&gt;
			else ()
			}
			&lt;/wsdl:agreements&gt;
		else ()
		}
		{
		if (data($fml/CI_ID_SPOLKI)) then
			&lt;wsdl:factories&gt;
			{
			if (data($fml/CI_ID_SPOLKI) = 7) then
				&lt;wsdl:factory&gt;JVL&lt;/wsdl:factory&gt;
			else if (data($fml/CI_ID_SPOLKI) = 8) then
				&lt;wsdl:factory&gt;JVG&lt;/wsdl:factory&gt;
			else ()					(: podano id spolki, ale niezgodne ze slownikiem :)
			}
			&lt;/wsdl:factories&gt;
		else ()
		}
		{
		if (data($fml/DC_TYP_KLIENTA)) then
			if ($fml/DC_TYP_KLIENTA = 'F') then
			&lt;wsdl:company-ind&gt;0&lt;/wsdl:company-ind&gt;
			else if ($fml/DC_TYP_KLIENTA = 'P') then
			&lt;wsdl:company-ind&gt;1&lt;/wsdl:company-ind&gt;
			else ()					(: podano typ klienta, ale niezgodny ze slownikiem :)
		else ()
		}
		{
		if (data($fml/CI_STATUS_GIODO)
		or  data($fml/CI_NR_ODDZIALU)
		or  data($fml/DC_KARTA_WZOROW_PODPISOW)
		or  data($fml/DC_ZRODLO_DANYCH)
		or  data($fml/DC_KOD_JEZYKA)) then
			&lt;wsdl:jv-data&gt;
				{ xf:parseElementGIODO($fml/CI_STATUS_GIODO, 'wsdl:giodo-status') }
				{ xf:parseElement($fml/CI_NR_ODDZIALU, 'wsdl:branch-id') }
				{ xf:parseElement($fml/DC_KARTA_WZOROW_PODPISOW, 'wsdl:facsimile-signature') }
				{ xf:parseElement($fml/DC_KOD_JEZYKA, 'wsdl:language-code') }
			&lt;/wsdl:jv-data&gt;
		else ()
		}
			&lt;wsdl:person&gt;
			{
			if (data($fml/DC_IMIE)
			or  data($fml/DC_DRUGIE_IMIE)
			or  data($fml/DC_NAZWISKO)) then
				&lt;wsdl:name&gt;
					{ xf:parseElement($fml/DC_IMIE, 'wsdl:first-name') }
					{ xf:parseElement($fml/DC_DRUGIE_IMIE, 'wsdl:second-name') }
					{ xf:parseElement($fml/DC_NAZWISKO, 'wsdl:last-name') }
				&lt;/wsdl:name&gt;
			else ()
			}
				{ xf:parseElementAsDate($fml/DC_DATA_URODZENIA, 'wsdl:dob') }
				{ xf:parseElement($fml/DC_MIEJSCE_URODZENIA ,' wsdl:pob') }
				(: &lt;wsdl:cob/&gt;  chwilowo niezamapowane - CI_KRAJ_POCHODZENIA? :)
				{ xf:parseElement($fml/DC_PLEC, 'wsdl:sex') }
				{ xf:parseElement($fml/CI_OBYWATELSTWO, 'wsdl:citizenship') }
				{ xf:parseElement($fml/CI_KOD_KRAJU, 'wsdl:country') }
			{
			if (data($fml/DC_NR_PESEL)
			or  data($fml/DC_NR_DOWODU_REGON)
			or  data($fml/DC_NUMER_PASZPORTU)
			or  data($fml/DC_NIP)
			or  (data($fml/CI_DOK_TOZSAMOSCI) and data($fml/CI_SERIA_NR_DOK))) then
				&lt;wsdl:identifiers&gt;
					{ xf:parseElement($fml/DC_NR_PESEL, 'wsdl:pesel') }
					{ xf:parseElement($fml/DC_NR_DOWODU_REGON, 'wsdl:pid') }
					{ xf:parseElement($fml/DC_NUMER_PASZPORTU, 'wsdl:passport') }
					{ xf:parseElement($fml/DC_NIP, 'wsdl:nip') }
					{
					if (data($fml/CI_DOK_TOZSAMOSCI) and data($fml/CI_SERIA_NR_DOK)) then
						xf:parseElement($fml/CI_DOK_TOZSAMOSCI, 'wsdl:other-kind')
						and
						xf:parseElement($fml/CI_SERIA_NR_DOK, 'wsdl:other-number')
					else ()
					}
				&lt;/wsdl:identifiers&gt;
			else ()
			}
				{ xf:parseElement($fml/DC_ADRES_E_MAIL, 'wsdl:email') }
			{
			if (data($fml/DC_ULICA_DANE_PODST)
			or  data($fml/DC_NR_POSES_LOKALU_DANE_PODST)
			or  data($fml/DC_KOD_POCZTOWY_DANE_PODST)
			or  data($fml/DC_MIASTO_DANE_PODST)
			or  data($fml/CI_KOD_KRAJU)
			or  data($fml/DC_WOJ_KRAJ_DANE_PODST)
			or  data($fml/DC_ULICA_ADRES_ALT)
			or  data($fml/DC_NR_POSES_LOKALU_ADRES_ALT)
			or  data($fml/DC_KOD_POCZTOWY_ADRES_ALT)
			or  data($fml/DC_MIASTO_ADRES_ALT)
			or  data($fml/CI_KOD_KRAJU_KORESP)
			or  data($fml/DC_WOJEWODZTWO_KRAJ_ADRES_ALT)) then
				&lt;wsdl:address-list&gt;
				{
				if (data($fml/DC_ULICA_DANE_PODST)			(: adres zameldowania :)
				or  data($fml/DC_NR_POSES_LOKALU_DANE_PODST)
				or  data($fml/DC_KOD_POCZTOWY_DANE_PODST)
				or  data($fml/DC_MIASTO_DANE_PODST)
				or  data($fml/CI_KOD_KRAJU)
				or  data($fml/DC_WOJ_KRAJ_DANE_PODST)) then
					&lt;wsdl:address kind="1"&gt;
						{ xf:parseElement($fml/DC_ULICA_DANE_PODST, 'wsdl:street') }
						{ xf:parseElement($fml/DC_NR_POSES_LOKALU_DANE_PODST, 'wsdl:home') }
						(: &lt;wsdl:flat/&gt; :)
						{ xf:parseElement($fml/DC_KOD_POCZTOWY_DANE_PODST, 'wsdl:postal-code') }
						{ xf:parseElement($fml/DC_MIASTO_DANE_PODST, 'wsdl:city') }
						{ xf:parseElement($fml/CI_KOD_KRAJU, 'wsdl:country') }
						{ xf:parseElement($fml/DC_WOJ_KRAJ_DANE_PODST, 'wsdl:county') }
					&lt;/wsdl:address&gt;
				else ()
				}
				{
				if (data($fml/DC_ULICA_ADRES_ALT)		(: adres korespondencyjny :)
				or  data($fml/DC_NR_POSES_LOKALU_ADRES_ALT)
				or  data($fml/DC_KOD_POCZTOWY_ADRES_ALT)
				or  data($fml/DC_MIASTO_ADRES_ALT)
				or  data($fml/CI_KOD_KRAJU_KORESP)
				or  data($fml/DC_WOJEWODZTWO_KRAJ_ADRES_ALT)) then
					&lt;wsdl:address kind="4"&gt;
						{ xf:parseElement($fml/DC_ULICA_ADRES_ALT, 'wsdl:street') }
						{ xf:parseElement($fml/DC_NR_POSES_LOKALU_ADRES_ALT, 'wsdl:home') }
						(: &lt;wsdl:flat/&gt; :)
						{ xf:parseElement($fml/DC_KOD_POCZTOWY_ADRES_ALT, 'wsdl:postal-code') }
						{ xf:parseElement($fml/DC_MIASTO_ADRES_ALT, 'wsdl:city') }
						{ xf:parseElement($fml/CI_KOD_KRAJU_KORESP, 'wsdl:country') }
						{ xf:parseElement($fml/DC_WOJEWODZTWO_KRAJ_ADRES_ALT, 'wsdl:county') }
					&lt;/wsdl:address&gt;
				else ()
				}
				&lt;/wsdl:address-list&gt;
			else ()
			}
			{
			if (data($fml/DC_NR_TELEFONU)
			or  data($fml/DC_NR_TELEF_KOMORKOWEGO)
			or  data($fml/DC_NUMER_TELEFONU_PRACA)) then
				&lt;wsdl:phone-list&gt;
				{
				if (data($fml/DC_NR_TELEFONU)) then		(: telefon stajonarny :)
					&lt;phone kind="1"&gt;
						&lt;phone-no&gt; { data($fml/DC_NR_TELEFONU) } &lt;/phone-no&gt;
					&lt;/phone&gt;
				else ()
				}
				{
				if (data($fml/DC_NR_TELEF_KOMORKOWEGO)) then	(: telefon komorkowy :)
					&lt;phone kind="6"&gt;
						&lt;phone-no&gt; { data($fml/DC_NR_TELEF_KOMORKOWEGO) } &lt;/phone-no&gt;
					&lt;/phone&gt;
				else ()
				}
				{
				if (data($fml/DC_NUMER_TELEFONU_PRACA)) then	(: telefon sluzbowy :)
					&lt;phone kind="2"&gt;
						&lt;phone-no&gt; { data($fml/DC_NUMER_TELEFONU_PRACA) } &lt;/phone-no&gt;
					&lt;/phone&gt;
				else ()
				}
				&lt;/wsdl:phone-list&gt;
			else ()
			}
			{
			if (data($fml/DC_REZ_NIEREZ)) then
				if (data($fml/DC_REZ_NIEREZ) = 0) then
				&lt;wsdl:resident&gt;false&lt;/wsdl:resident&gt;
				else if (data($fml/DC_REZ_NIEREZ) = 1) then
				&lt;wsdl:resident&gt;true&lt;/wsdl:resident&gt;
				else ()			(: podano pole rezydent, ale niezgodne ze slownikiem :)
			else ()
			}
			&lt;/wsdl:person&gt;
		&lt;/wsdl:customer&gt;
	else ()
	}
	&lt;/wsdl:changeClientJVRequest&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>