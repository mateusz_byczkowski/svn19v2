<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:operations.entities.be.dcl";
declare namespace urn2="urn:dictionaries.be.dcl";
declare namespace urn3="urn:operationsdictionary.dictionaries.be.dcl";
declare namespace urn4="urn:basedictionaries.be.dcl";

declare namespace xf="urn:be.services.dcl";


declare variable $body external;
declare variable $header external;


declare variable $trans as element (urn1:Transaction):=$body/urn:invoke/urn:transaction/urn1:Transaction;
declare variable $transWn as element (urn1:TransactionWn):=$trans/urn1:transactionWn/urn1:TransactionWn;
declare variable $transMa as element (urn1:TransactionMa):=$trans/urn1:transactionMa/urn1:TransactionMa;


declare function xf:convertTo2CharString($value as xs:integer) as xs:string
{
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:dateTime) as xs:string 
{
       fn:concat (xf:convertTo2CharString(fn:day-from-dateTime($dateIn)),"-",
                          xf:convertTo2CharString(fn:month-from-dateTime($dateIn)),"-",
                          fn:string(fn:year-from-dateTime($dateIn)))
};

declare function xf:mapTime($dateTimeIn as xs:dateTime) as xs:string 
{
     let $string := $dateTimeIn cast as xs:string
     return
       fn:concat (fn:substring($string,12,2),fn:substring($string,15,2),fn:substring($string,18,2))
};
 
 declare function xf:round ( $value as xs:double)  as xs:double {
    fn:round-half-to-even($value,  6)
};

&lt;soap-env:Body&gt;
    &lt;FML32&gt;
    &lt;!--  header --&gt;
        &lt;TR_ID_OPER&gt;{data($trans/urn1:transactionID)}&lt;/TR_ID_OPER&gt;
        &lt;TR_DATA_OPER&gt; {xf:mapDate( $trans/urn1:putDownDate)}&lt;/TR_DATA_OPER&gt;
        &lt;TR_CZAS_OPER&gt; {xf:mapTime( $trans/urn1:putDownDate)}&lt;/TR_CZAS_OPER&gt;
        &lt;TR_ODDZ_KASY&gt;{data($trans/urn1:branchCode/urn4:BaseBranchCode/urn4:branchCode)}&lt;/TR_ODDZ_KASY&gt;
        &lt;TR_KASA&gt;{data($trans/urn1:tillNumber)}&lt;/TR_KASA&gt;
        &lt;TR_KASJER&gt;{data($trans/urn1:tellerID)}&lt;/TR_KASJER&gt;
        &lt;TR_UZYTKOWNIK&gt;{data($header/urn:header/urn:msgHeader/urn:userId)}&lt;/TR_UZYTKOWNIK&gt;

    
    &lt;!--  main  --&gt;
    
        &lt;TR_TYTUL&gt;{data($trans/urn1:title)}&lt;/TR_TYTUL&gt;
        &lt;TR_TYP_KOM&gt;{data($trans/urn1:internalTransactionType/urn3:InternalTransactionType/urn3:internalType)}&lt;/TR_TYP_KOM&gt;
        &lt;TR_KANAL&gt;{data($trans/urn1:externalTransactionType/urn3:ExternalTransactionType/urn3:externalType)}&lt;/TR_KANAL&gt;
        &lt;TR_DATA_WALUTY&gt; {xf:mapDate( $trans/urn1:transactionDate)}&lt;/TR_DATA_WALUTY&gt;
        
         &lt;!--  WN --&gt;
         
        &lt;TR_RACH_NAD&gt;{data($transWn/urn1:accountNumber)}&lt;/TR_RACH_NAD&gt;
        { if  ($transWn/urn1:amountWn) then
        &lt;TR_KWOTA_NAD&gt;{xf:round($transWn/urn1:amountWn)}&lt;/TR_KWOTA_NAD&gt;
        else()
        }
        &lt;TR_WALUTA_NAD&gt;{data($transWn/urn1:currencyCode/urn2:CurrencyCode/urn2:currencyCode)}&lt;/TR_WALUTA_NAD&gt;
        { if  ($transWn/urn1:rate) then
        &lt;TR_KURS_NAD&gt;{xf:round($transWn/urn1:rate)}&lt;/TR_KURS_NAD&gt;
        else()
        }
        &lt;TR_NAZWA_NAD&gt;{data($transWn/urn1:name)}&lt;/TR_NAZWA_NAD&gt;
        &lt;TR_MIEJSCOWOSC_NAD&gt;{data($transWn/urn1:city)}&lt;/TR_MIEJSCOWOSC_NAD&gt;
        &lt;TR_ULICA_NAD&gt;{data($transWn/urn1:adsress)}&lt;/TR_ULICA_NAD&gt;
        &lt;TR_KOD_POCZT_NAD&gt;{data($transWn/urn1:zipCode)}&lt;/TR_KOD_POCZT_NAD&gt;

        &lt;!--  MA --&gt;
        
        &lt;TR_RACH_ADR&gt;{data($transMa/urn1:accountNumber)}&lt;/TR_RACH_ADR&gt;
        { if  ($transMa/urn1:amountMa) then
        &lt;TR_KWOTA_ADR&gt;{xf:round($transMa/urn1:amountMa)}&lt;/TR_KWOTA_ADR&gt;
        else()
        }
     
        &lt;TR_WALUTA_ADR&gt;{data($transMa/urn1:currencyCode/urn2:CurrencyCode/urn2:currencyCode)}&lt;/TR_WALUTA_ADR&gt;
        { if  ($transMa/urn1:rate) then
        &lt;TR_KURS_ADR&gt;{xf:round($transMa/urn1:rate)}&lt;/TR_KURS_ADR&gt;
        else()
        }
        &lt;TR_NAZWA_ADR&gt;{data($transMa/urn1:name)}&lt;/TR_NAZWA_ADR&gt;
        &lt;TR_MIEJSCOWOSC_ADR&gt;{data($transMa/urn1:city)}&lt;/TR_MIEJSCOWOSC_ADR&gt;
        &lt;TR_ULICA_ADR&gt;{data($transMa/urn1:address)}&lt;/TR_ULICA_ADR&gt;
        &lt;TR_KOD_POCZT_ADR&gt;{data($transMa/urn1:zipCode)}&lt;/TR_KOD_POCZT_ADR&gt;
        
        &lt;!--  ZUS --&gt;
        {
        if ($trans/urn1:transactionZUS) then
            let  $transZUS:=$trans/urn1:transactionZUS/urn1:TransactionZUS
            return
                (&lt;TR_ZUS_NIP&gt;{data($transZUS/urn1:nip)}&lt;/TR_ZUS_NIP&gt;,
                &lt;TR_ZUS_TYP_IDENTYF&gt;{data($transZUS/urn1:idType/urn3:IDType/urn3:idType)}&lt;/TR_ZUS_TYP_IDENTYF&gt;,
                &lt;TR_ZUS_IDENTYF_DOD&gt;{data($transZUS/urn1:idValue)}&lt;/TR_ZUS_IDENTYF_DOD&gt;,
                &lt;TR_ZUS_TYP_PLAT&gt;{data($transZUS/urn1:paymentType/urn3:ZUSPaymentType/urn3:paymentType)}&lt;/TR_ZUS_TYP_PLAT&gt;,
                &lt;TR_ZUS_NR_DEKLAR&gt;{data($transZUS/urn1:declarationNumber)}&lt;/TR_ZUS_NR_DEKLAR&gt;,
                &lt;TR_ZUS_NR_DEKLAR&gt;{data($transZUS/urn1:declarationNumber)}&lt;/TR_ZUS_NR_DEKLAR&gt;,
                &lt;TR_ZUS_DATA_DEKLAR&gt;{data($transZUS/urn1:declarationDate)}&lt;/TR_ZUS_DATA_DEKLAR&gt;,
                &lt;TR_ZUS_NIP&gt;{data($transZUS/urn1:nip)}&lt;/TR_ZUS_NIP&gt;,
                &lt;TR_ZUS_NAZWA_SKR_POD&gt;{data($transZUS/urn1:name)}&lt;/TR_ZUS_NAZWA_SKR_POD&gt;)
        else()
        }

        &lt;!--  US --&gt;
        {
        if ($trans/urn1:transactionUS) then
            let  $transUS:=$trans/urn1:transactionUS/urn1:TransctionUS
            return
            (&lt;TR_US_TYP_IDENTYF&gt;{data($transUS/urn1:idType/urn3:IDType/urn3:idType)}&lt;/TR_US_TYP_IDENTYF&gt;,
            &lt;TR_US_TYP_OKRESU&gt;{data($transUS/urn1:periodType/urn3:USPeriodType/urn3:periodType)}&lt;/TR_US_TYP_OKRESU&gt;,
            &lt;TR_US_SYMBOL_FORM&gt;{data($transUS/urn1:symbolForm/urn3:SymbolForm/urn3:symbolForm)}&lt;/TR_US_SYMBOL_FORM&gt;,
            &lt;TR_US_IDENTYF_ZOBOW&gt;{data($transUS/urn1:commitmentID)}&lt;/TR_US_IDENTYF_ZOBOW&gt;,
            &lt;TR_US_OPIS_ZOBOW&gt;{data($transUS/urn1:commitmentDesc)}&lt;/TR_US_OPIS_ZOBOW&gt;,
            &lt;TR_US_NAZWA_SKR_POD&gt;{data($transUS/urn1:name)}&lt;/TR_US_NAZWA_SKR_POD&gt;,
            &lt;TR_US_IDENTYF_DOD&gt;{data($transUS/urn1:idValue)}&lt;/TR_US_IDENTYF_DOD&gt;)
        else()
        }
        &lt;!-- Dysponenci --&gt;

     {  for $dysp  in $trans/urn1:disposerList/urn1:Disposer    return
                 (
                 &lt;TR_DYSP_CIF&gt;{data($dysp/urn1:cif)}&lt;/TR_DYSP_CIF&gt;,
                 &lt;TR_DYSP_NAZWA&gt;{data($dysp/urn1:firstName),data($dysp/urn1:lastName)}&lt;/TR_DYSP_NAZWA&gt;,
                  &lt;TR_DYSP_OBYWATELSTWO&gt;{data($dysp/urn1:citizenship/urn2:CountryCode/urn2:countryCode)}&lt;/TR_DYSP_OBYWATELSTWO&gt;,
                  &lt;TR_DYSP_KRAJ&gt;{data($dysp/urn1:countryCode/urn2:CountryCode/urn2:countryCode)}&lt;/TR_DYSP_KRAJ&gt;,
                  &lt;TR_DYSP_MIEJSCOWOSC&gt;{data($dysp/urn1:city)}&lt;/TR_DYSP_MIEJSCOWOSC&gt;,
                  &lt;TR_DYSP_KOD_POCZT&gt;{data($dysp/urn1:zipCode)}&lt;/TR_DYSP_KOD_POCZT&gt;,
                  &lt;TR_DYSP_ULICA&gt;{data($dysp/urn1:address)}&lt;/TR_DYSP_ULICA&gt;,
                  &lt;TR_DYSP_PESEL&gt;{data($dysp/urn1:pesel)}&lt;/TR_DYSP_PESEL&gt;,
                  &lt;TR_DYSP_NR_PASZPORTU&gt;{data($dysp/urn1:passportNumber)}&lt;/TR_DYSP_NR_PASZPORTU&gt;,
                  &lt;TR_DYSP_NR_DOWOD&gt;{data($dysp/urn1:documentNumber)}&lt;/TR_DYSP_NR_DOWOD&gt;
                 )
             }


    &lt;/FML32&gt;
    
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>