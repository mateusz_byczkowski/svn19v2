<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:filtersandmessages.entities.be.dcl";

declare function xf:mapchkTokenSerialNumberRequest($req as element(urn:invoke), $head as element(urn:header))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				&lt;fml:E_TOKEN_SERIAL_NO?&gt;{ data($req/urn:token/urn1:Token/urn1:serialNumber) }&lt;/fml:E_TOKEN_SERIAL_NO&gt;
			}
			{
				&lt;fml:E_MSHEAD_MSGID&gt;{data($head/urn:msgHeader/urn:msgId)}&lt;/fml:E_MSHEAD_MSGID&gt;
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body&gt;
{ xf:mapchkTokenSerialNumberRequest($body/urn:invoke,$header/urn:header) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>