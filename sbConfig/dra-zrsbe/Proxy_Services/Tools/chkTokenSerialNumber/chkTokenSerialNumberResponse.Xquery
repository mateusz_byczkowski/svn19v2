<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace con = "http://www.bea.com/wli/sb/test/config";
declare namespace tran = "http://www.bea.com/wli/sb/transports";
declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace f="http://bzwbk.com/services/ceke/faults/";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:filtersandmessages.entities.be.dcl";

declare function xf:mapchkTokenSerialNumberResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		&lt;urn:invokeResponse&gt;
			&lt;urn:response&gt;
				&lt;urn2:ResponseMessage&gt;
					{
						if ((data($fml/fml:E_TOKEN_STATUS) = "P") and (data($fml/fml:B_ID_ODDZ) &gt; 0)) then
						&lt;urn2:result&gt;true&lt;/urn2:result&gt;
						else (
						&lt;urn2:result&gt;false&lt;/urn2:result&gt;
						)
					}
				&lt;/urn2:ResponseMessage&gt;
			&lt;/urn:response&gt;
		&lt;/urn:invokeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
	{


		if ($body/con:metadata/tran:response-message) then
		(
			&lt;urn:invokeResponse&gt;
					&lt;urn:response&gt;
						&lt;urn:ResponseMessage&gt;



			if (fn:substring-before(fn:substring-after(fn:substring-after($body/con:metadata/tran:response-message, ":"), ":"), ":") = "73") then
			(
							&lt;urn2:result&gt;false&lt;/urn2:result&gt;
			) else (
							&lt;urn2:errorCode&gt;{fn:substring-before(fn:substring-after(fn:substring-before($body/con:metadata/tran:response-message, ":"), "("), ")")}&lt;/urn2:errorCode&gt;
							&lt;urn2:errorDescription&gt;{fn:substring-before(fn:substring-after(fn:substring-after($body/con:metadata/tran:response-message, ":"), ":"), ":")}&lt;/urn2:errorDescription&gt;
			)
						&lt;/urn:ResponseMessage&gt;
					&lt;/urn:response&gt;
				&lt;/urn:invokeResponse&gt;
		) else (
			xf:mapchkTokenSerialNumberResponse($body/fml:FML32)
		)

	}


&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>