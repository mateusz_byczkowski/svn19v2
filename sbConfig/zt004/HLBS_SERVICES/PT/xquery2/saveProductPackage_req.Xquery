<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProductPackage_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:saveProductPackage_req ($entity as element(dcl:ProductPackage)) as element(FML32) {
&lt;FML32>
     &lt;PT_CODE_PACKAGE>{ data( $entity/dcl:codePackage ) }&lt;/PT_CODE_PACKAGE>
     &lt;PT_POLISH_NAME>{ data( $entity/dcl:polishPackageName ) }&lt;/PT_POLISH_NAME>
     &lt;PT_ENGLISH_NAME>{ data( $entity/dcl:englishPackageName ) }&lt;/PT_ENGLISH_NAME>
     &lt;PT_SORT_ORDER>{ data( $entity/dcl:sortOrder ) }&lt;/PT_SORT_ORDER>
     &lt;PT_START_PACKAGE_DATE>{ data( $entity/dcl:startPackageDate ) }&lt;/PT_START_PACKAGE_DATE>     
     &lt;PT_END_PACKAGE_DATE>{ data( $entity/dcl:endPackageDate ) }&lt;/PT_END_PACKAGE_DATE>     
     &lt;PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP ) }&lt;/PT_USER_CHANGE_SKP>     
     &lt;PT_DATE_CHANGE>{ data( $entity/dcl:dateChange ) }&lt;/PT_DATE_CHANGE>     
     
     &lt;PT_ID_PACKAGE>{ data( $entity/dcl:idProductPackage ) }&lt;/PT_ID_PACKAGE>     
&lt;/FML32>
};
declare variable $entity as element(dcl:ProductPackage) external;
xf:saveProductPackage_req($entity)</con:xquery>
</con:xqueryEntry>