<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAreaHistory_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace dcl = "urn:productstree.entities.be.dcl";


declare function xf:getProductAreaHistory_resp($fml as element())
    as element(srv:invokeResponse) {
        &lt;srv:invokeResponse>
            &lt;srv:productAreaHistoryList>
                {
                    for $i in 1 to count($fml/PT_ID_AREA_HISTORY)
                    return
                        &lt;dcl:ProductAreaHistory>
                                    &lt;dcl:codeProductArea>{ data($fml/PT_CODE_PRODUCT_AREA[$i]) }&lt;/dcl:codeProductArea>
                                    &lt;dcl:polishAreaName>{ data($fml/PT_POLISH_NAME[$i]) }&lt;/dcl:polishAreaName>
                                    &lt;dcl:englishAreaName>{ data($fml/PT_ENGLISH_NAME[$i]) }&lt;/dcl:englishAreaName>
                                    &lt;dcl:sortOrder>{ data($fml/PT_SORT_ORDER[$i]) }&lt;/dcl:sortOrder>
                                    &lt;dcl:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }&lt;/dcl:userChangeSKP>
                                   { if( data($fml/PT_DATE_CHANGE[$i]) ) 
                                    then&lt;dcl:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }&lt;/dcl:dateChange>
                                    else() }
                                    &lt;dcl:cerberAtributeName>{ data($fml/PT_CERBER_ATTRIBUTE_NAME[$i]) }&lt;/dcl:cerberAtributeName>
                        &lt;/dcl:ProductAreaHistory>
                }
            &lt;/srv:productAreaHistoryList>
        &lt;/srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductAreaHistory_resp($fml)</con:xquery>
</con:xqueryEntry>