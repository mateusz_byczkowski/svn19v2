<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProductDefinition_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:saveProductDefinition_req ($entity as element(dcl:ProductDefinition)) as element(FML32) {
&lt;FML32>
     &lt;PT_CODE_PRODUCT_DEFINITION>{ data( $entity/dcl:codeProductDefinition ) }&lt;/PT_CODE_PRODUCT_DEFINITION>
     &lt;PT_POLISH_NAME>{ data( $entity/dcl:polishProductName ) }&lt;/PT_POLISH_NAME>
     &lt;PT_ENGLISH_NAME>{ data( $entity/dcl:englishProductName ) }&lt;/PT_ENGLISH_NAME>
     &lt;PT_SORT_ORDER>{ data( $entity/dcl:sortOrder ) }&lt;/PT_SORT_ORDER>
     &lt;PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP ) }&lt;/PT_USER_CHANGE_SKP>
     &lt;PT_DATE_CHANGE>{ data( $entity/dcl:dateChange ) }&lt;/PT_DATE_CHANGE>
     &lt;PT_PACKAGE_ONLY_FLAG>{ xs:short( xs:boolean(data( $entity/dcl:packageOnlyFlag ))) }&lt;/PT_PACKAGE_ONLY_FLAG>
     &lt;PT_START_DATE>{ data( $entity/dcl:startDate ) }&lt;/PT_START_DATE>
     &lt;PT_END_DATE>{ data( $entity/dcl:endDate ) }&lt;/PT_END_DATE>
     
     &lt;PT_SOURCE_PRODUCT_CODE>{ data( $entity/dcl:sorceProductCode ) }&lt;/PT_SOURCE_PRODUCT_CODE>
     &lt;PT_ID_GROUP>{ data( $entity/dcl:idProductGroup ) }&lt;/PT_ID_GROUP>
     
     &lt;PT_ID_DEFINITION>{ data( $entity/dcl:idProductDefinition ) }&lt;/PT_ID_DEFINITION>
     
&lt;/FML32>
};
declare variable $entity as element(dcl:ProductDefinition) external;
xf:saveProductDefinition_req($entity)</con:xquery>
</con:xqueryEntry>