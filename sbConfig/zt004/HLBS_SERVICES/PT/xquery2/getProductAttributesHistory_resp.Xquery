<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductArea_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";

declare function xf:getProductAttributesHistory_resp($fml as element())
    as element(srv:invokeResponse) {
        &lt;srv:invokeResponse>
            &lt;srv:productAttrHistoryEntityOut>
 {
                for $i in 1 to count($fml/PT_ID_ATTRIBUTES)
                return
                    &lt;ns0:ProductAttributesHistory>
                               &lt;ns0:idProductGroup>{ data($fml/PT_ID_GROUP[$i]) }&lt;/ns0:idProductGroup>
                                &lt;ns0:attributeValue>{ data($fml/PT_ATTRIBUTE_VALUE[$i]) }&lt;/ns0:attributeValue>
                                &lt;ns0:idProductDefinition>{ data($fml/PT_ID_DEFINITION[$i]) }&lt;/ns0:idProductDefinition>
                                &lt;ns0:attributeID>{ data($fml/PT_ID_ATTRIBUTE[$i]) }&lt;/ns0:attributeID>
                               &lt;ns0:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }&lt;/ns0:userChangeSKP>
                               &lt;ns0:attributeValue2>{ data($fml/PT_ATTRIBUTE_VALUE_2[$i]) }&lt;/ns0:attributeValue2>
                                &lt;ns0:idProductPackage>{ data($fml/PT_ID_PACKAGE[$i]) }&lt;/ns0:idProductPackage>

{
     if (data( $fml/PT_DATE_CHANGE[$i]))
     then &lt;ns0:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }&lt;/ns0:dateChange>
     else () 
}
                    &lt;/ns0:ProductAttributesHistory>
            }


           &lt;/srv:productAttrHistoryEntityOut>
        &lt;/srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductAttributesHistory_resp($fml)</con:xquery>
</con:xqueryEntry>