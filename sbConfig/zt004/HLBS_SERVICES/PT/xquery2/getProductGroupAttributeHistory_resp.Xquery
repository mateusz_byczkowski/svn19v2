<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductGroupAttributeList_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";

declare function xf:getProductGroupAttributeHistory_resp($fml as element())
    as element() {
	&lt;srv:invokeResponse>
        &lt;srv:productGroupAttributeHistory>
            {
                for $i in 1 to count($fml/PT_ID_GROUP_ATTRIBUTES_HISTORY)
                return
                    &lt;ns0:ProductGroupAttributesHistory>
                            &lt;ns0:idProductGroup>{ data($fml/PT_ID_GROUP[$i]) }&lt;/ns0:idProductGroup>
                            &lt;ns0:thirdProductFeature>{ data($fml/PT_THIRD_PRODUCT_FEATURE[$i]) }&lt;/ns0:thirdProductFeature>                                    
                            &lt;ns0:fifthProductFeature>{ data($fml/PT_FIFTH_PRODUCT_FEATURE[$i]) }&lt;/ns0:fifthProductFeature>
                            &lt;ns0:codeProduct>{ data($fml/PT_CODE_PRODUCT[$i]) }&lt;/ns0:codeProduct>   
                            &lt;ns0:firstProductFeature>{ data($fml/PT_FIRST_PRODUCT_FEATURE[$i]) }&lt;/ns0:firstProductFeature>                                    
                            &lt;ns0:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }&lt;/ns0:userChangeSKP>
                            &lt;ns0:fourthProductFeature>{ data($fml/PT_FOURTH_PRODUCT_FEATURE[$i]) }&lt;/ns0:fourthProductFeature>
                            &lt;ns0:secondProductFeature>{ data($fml/PT_SECOND_PRODUCT_FEATURE[$i]) }&lt;/ns0:secondProductFeature>                                    
                            &lt;ns0:idAttributesProductGroupHistory>{ data($fml/PT_ID_GROUP_ATTRIBUTES_HISTORY[$i]) }&lt;/ns0:idAttributesProductGroupHistory>
                            &lt;ns0:idAttributesProductGroup>{ data($fml/PT_ID_GROUP_ATTRIBUTES[$i]) }&lt;/ns0:idAttributesProductGroup>
                            &lt;ns0:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }&lt;/ns0:dateChange>
                    &lt;/ns0:ProductGroupAttributesHistory>
            }
        &lt;/srv:productGroupAttributeHistory>
    &lt;/srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductGroupAttributeHistory_resp($fml)</con:xquery>
</con:xqueryEntry>