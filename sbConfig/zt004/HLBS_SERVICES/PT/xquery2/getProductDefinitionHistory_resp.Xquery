<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductDefinitionHistory_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace dcl = "urn:productstree.entities.be.dcl";


declare function xf:getProductDefinitionHistory_resp ($fml as element()) {
&lt;srv:invokeResponse>
&lt;srv:productDefHistoryList>
{
for $i in 1 to count($fml/PT_ID_DEFINITION_HISTORY) return
&lt;dcl:ProductDefinitionHistory>
	&lt;dcl:codeProductDefinition>{ data( $fml/PT_CODE_PRODUCT_DEFINITION [$i] ) }&lt;/dcl:codeProductDefinition>
	&lt;dcl:polishProductName>{ data( $fml/PT_POLISH_NAME [$i] ) }&lt;/dcl:polishProductName>
	 &lt;dcl:englishProductName>{ data( $fml/PT_ENGLISH_NAME [$i] ) }&lt;/dcl:englishProductName>
     &lt;dcl:sortOrder>{ data( $fml/PT_SORT_ORDER [$i] ) }&lt;/dcl:sortOrder>
     &lt;dcl:userChangeSKP>{ data( $fml/PT_USER_CHANGE_SKP [$i] ) }&lt;/dcl:userChangeSKP>
{ if( data($fml/PT_DATE_CHANGE[$i]) )
then     &lt;dcl:dateChange>{ data( $fml/PT_DATE_CHANGE [$i] ) }&lt;/dcl:dateChange>
else () }
     &lt;dcl:overridingElementID>&lt;/dcl:overridingElementID>
     &lt;dcl:packageOnlyFlag>{ data( $fml/PT_PACKAGE_ONLY_FLAG [$i] ) }&lt;/dcl:packageOnlyFlag>
     &lt;dcl:sorceProductCode>{ data( $fml/PT_SOURCE_PRODUCT_CODE [$i] ) }&lt;/dcl:sorceProductCode>
     &lt;dcl:startDate>{ data( $fml/PT_START_DATE [$i] ) }&lt;/dcl:startDate>

     {if (data( $fml/PT_END_DATE [$i]))
     then &lt;dcl:endDate>{ data( $fml/PT_END_DATE [$i]) }&lt;/dcl:endDate>
     else () }

&lt;/dcl:ProductDefinitionHistory>
}
&lt;/srv:productDefHistoryList>
&lt;/srv:invokeResponse>
};

declare variable $fml as element() external;
xf:getProductDefinitionHistory_resp($fml)</con:xquery>
</con:xqueryEntry>