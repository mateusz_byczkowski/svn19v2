<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProdAreaAttribute_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";

declare function xf:saveProdAreaAttribute_req ($entity as element(dcl:ProductAreaAttributes)) as element(FML32) {
&lt;FML32>
     &lt;PT_ATTRIBUTE_NAME>{ data( $entity/dcl:attributeName ) }&lt;/PT_ATTRIBUTE_NAME>
     &lt;PT_ATTRIBUTE_TYPE>{ data( $entity/dcl:attributeType/ns1:ProductAreaAttributeType/ns1:attributeType ) }&lt;/PT_ATTRIBUTE_TYPE>
     &lt;PT_ALGORITHM_SEQ>{ data( $entity/dcl:algorithmSequence ) }&lt;/PT_ALGORITHM_SEQ>
     &lt;PT_ID_AREA>{ data( $entity/dcl:idProductArea ) }&lt;/PT_ID_AREA>
     
     &lt;PT_ID_AREA_ATTRIBUTES>{ data( $entity/dcl:idProductAreaAttributes ) }&lt;/PT_ID_AREA_ATTRIBUTES>     
     &lt;PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP) }&lt;/PT_USER_CHANGE_SKP>
     &lt;PT_DATE_CHANGE>{ data( $entity/dcl:dateChange) }&lt;/PT_DATE_CHANGE>     
&lt;/FML32>
};
declare variable $entity as element(dcl:ProductAreaAttributes) external;
xf:saveProdAreaAttribute_req($entity)</con:xquery>
</con:xqueryEntry>