<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductDefinitionList_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:getProductDefinitionList_req ($entity as element(dcl:ProductGroup)) as element(FML32) {
&lt;FML32>
     &lt;PT_ID_GROUP>{ data( $entity/dcl:idProductGroup ) }&lt;/PT_ID_GROUP>
     &lt;PT_SORT_ORDER>{ data( $entity/dcl:sortOrder ) }&lt;/PT_SORT_ORDER>
&lt;/FML32>
};

declare variable $entity as element(dcl:ProductGroup) external;
xf:getProductDefinitionList_req($entity)</con:xquery>
</con:xqueryEntry>