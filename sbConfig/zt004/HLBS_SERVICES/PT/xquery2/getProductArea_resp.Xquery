<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductArea_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";

declare function xf:getProductArea_resp($fml as element())
    as element(srv:invokeResponse) {
        &lt;srv:invokeResponse>
            &lt;srv:productAreaEntityOut>
                &lt;ns0:ProductArea>
                    &lt;ns0:codeProductArea>{ data($fml/PT_CODE_PRODUCT_AREA) }&lt;/ns0:codeProductArea>
                    &lt;ns0:polishAreaName>{ data($fml/PT_POLISH_NAME) }&lt;/ns0:polishAreaName>
                    &lt;ns0:englishAreaName>{ data($fml/PT_ENGLISH_NAME) }&lt;/ns0:englishAreaName>
                    &lt;ns0:sortOrder>{ data($fml/PT_SORT_ORDER) }&lt;/ns0:sortOrder>
                    &lt;ns0:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP) }&lt;/ns0:userChangeSKP>
 
     {if (data( $fml/PT_DATE_CHANGE))
     then &lt;ns0:dateChange>{ data($fml/PT_DATE_CHANGE) }&lt;/ns0:dateChange>
     else () }

                    &lt;ns0:cerberAtributeName>{ data($fml/PT_CERBER_ATTRIBUTE_NAME) }&lt;/ns0:cerberAtributeName>
                    &lt;ns0:idProductArea>{ data($fml/PT_ID_AREA) }&lt;/ns0:idProductArea>
                &lt;/ns0:ProductArea>
            &lt;/srv:productAreaEntityOut>
        &lt;/srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductArea_resp($fml)</con:xquery>
</con:xqueryEntry>