<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-15</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace hd="urn:be.services.dcl";
declare variable $header as element(soap:Header) external;


&lt;DC_MSHEAD_MSGID>{data($header/hd:header/hd:msgHeader/hd:msgId)}&lt;/DC_MSHEAD_MSGID></con:xquery>
</con:xqueryEntry>