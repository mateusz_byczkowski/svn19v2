<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISGetCustomersRequest($req as element(m:CISGetCustomersRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON>{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL>{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL>
					else ()
			}
			{
				if($req/m:Nip)
					then &lt;fml:DC_NIP>{ data($req/m:Nip) }&lt;/fml:DC_NIP>
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then &lt;fml:DC_NUMER_PASZPORTU>{ data($req/m:NumerPaszportu) }&lt;/fml:DC_NUMER_PASZPORTU>
					else ()
			}
			{
				if($req/m:Nik)
					then &lt;fml:CI_NIK>{ data($req/m:Nik) }&lt;/fml:CI_NIK>
					else ()
			}
			{
				if($req/m:Nazwa)
					then &lt;fml:DC_NAZWA>{ data($req/m:Nazwa) }&lt;/fml:DC_NAZWA>
					else ()
			}
			{
				if($req/m:GrupaKlientow)
					then &lt;fml:CI_GRUPA_KLIENTOW>{ data($req/m:GrupaKlientow) }&lt;/fml:CI_GRUPA_KLIENTOW>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE>{ data($req/m:Imie) }&lt;/fml:DC_IMIE>
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO>{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO>
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then &lt;fml:DC_MIASTO_DANE_PODST>{ data($req/m:MiastoDanePodst) }&lt;/fml:DC_MIASTO_DANE_PODST>
					else ()
			}
			{
				if($req/m:SegmentMarketingowy)
					then &lt;fml:DC_SEGMENT_MARKETINGOWY>{ data($req/m:SegmentMarketingowy) }&lt;/fml:DC_SEGMENT_MARKETINGOWY>
					else ()
			}
			{
				if($req/m:PodsegmentMark)
					then &lt;fml:CI_PODSEGMENT_MARK>{ data($req/m:PodsegmentMark) }&lt;/fml:CI_PODSEGMENT_MARK>
					else ()
			}
			{
				if($req/m:Urzednik1)
					then &lt;fml:DC_URZEDNIK_1>{ data($req/m:Urzednik1) }&lt;/fml:DC_URZEDNIK_1>
					else ()
			}
			{
				if($req/m:Urzednik2)
					then &lt;fml:DC_URZEDNIK_2>{ data($req/m:Urzednik2) }&lt;/fml:DC_URZEDNIK_2>
					else ()
			}
			{
				if($req/m:Urzednik3)
					then &lt;fml:DC_URZEDNIK_3>{ data($req/m:Urzednik3) }&lt;/fml:DC_URZEDNIK_3>
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER>{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI>{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI>
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA>{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCISGetCustomersRequest($body/m:CISGetCustomersRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>