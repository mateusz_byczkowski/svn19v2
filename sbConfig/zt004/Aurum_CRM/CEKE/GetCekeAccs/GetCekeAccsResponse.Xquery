<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCekeAccsResponse($fml as element(fml:FML32))
	as element(m:GetCekeAccsResponse) {
		&lt;m:GetCekeAccsResponse>
			{

				let $B_KOD_RACH := $fml/fml:B_KOD_RACH
				let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
				let $B_SALDO := $fml/fml:B_SALDO
				let $B_DOST_SRODKI := $fml/fml:B_DOST_SRODKI
				let $B_WALUTA := $fml/fml:B_WALUTA
				let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
				let $B_DATA_OST_OPER := $fml/fml:B_DATA_OST_OPER
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				let $B_RODZAJ_RACH := $fml/fml:B_RODZAJ_RACH
				let $B_OPIS_RACH := $fml/fml:B_OPIS_RACH
				let $B_SKROT_OPISU := $fml/fml:B_SKROT_OPISU
				let $B_ID_ODDZ := $fml/fml:B_ID_ODDZ
				let $B_NAZWA_ODDZ := $fml/fml:B_NAZWA_ODDZ
				let $B_MIKROODDZIAL := $fml/fml:B_MIKROODDZIAL
				let $B_PRAWA_KLIENTA := $fml/fml:B_PRAWA_KLIENTA
				let $B_NAZWA_KLIENTA := $fml/fml:B_NAZWA_KLIENTA
				let $B_UL_ZAM := $fml/fml:B_UL_ZAM
				let $B_M_ZAM := $fml/fml:B_M_ZAM
				let $B_OPROC1 := $fml/fml:B_OPROC1
				let $B_D_OTWARCIA := $fml/fml:B_D_OTWARCIA
				let $B_D_ZAMKNIECIA := $fml/fml:B_D_ZAMKNIECIA
				let $B_D_OSTATNIEJ_KAP := $fml/fml:B_D_OSTATNIEJ_KAP
				let $B_D_NASTEPNEJ_KAP := $fml/fml:B_D_NASTEPNEJ_KAP
				let $B_D_POCZ_WKL := $fml/fml:B_D_POCZ_WKL
				let $B_D_KON_WKL := $fml/fml:B_D_KON_WKL
				let $B_ODS_SKAP_AKT := $fml/fml:B_ODS_SKAP_AKT
				let $B_ODS_SKAP_POP := $fml/fml:B_ODS_SKAP_POP
				let $B_LIMIT2 := $fml/fml:B_LIMIT2
				let $B_D_KONCA_LIMITU := $fml/fml:B_D_KONCA_LIMITU
				let $B_BLOKADA := $fml/fml:B_BLOKADA
				let $B_RODZAJ_OPROC := $fml/fml:B_RODZAJ_OPROC
				let $B_KAPITALIZACJA := $fml/fml:B_KAPITALIZACJA
				let $B_PRZEKS_KAP := $fml/fml:B_PRZEKS_KAP
				let $B_OKR_WKLADU := $fml/fml:B_OKR_WKLADU
				let $B_JDN_OKR_WKLADU := $fml/fml:B_JDN_OKR_WKLADU
				let $B_D_NALICZ_ODS := $fml/fml:B_D_NALICZ_ODS
				let $B_D_NAST_OPER := $fml/fml:B_D_NAST_OPER
				let $B_OBROTY_DZIEN_UWN := $fml/fml:B_OBROTY_DZIEN_UWN
				let $B_OBROTY_DZIEN_UMA := $fml/fml:B_OBROTY_DZIEN_UMA
				let $B_OBROTY_DZIEN_NWN := $fml/fml:B_OBROTY_DZIEN_NWN
				let $B_OBROTY_DZIEN_NMA := $fml/fml:B_OBROTY_DZIEN_NMA
				let $B_OPROC2 := $fml/fml:B_OPROC2
				let $B_NR_WYCIAGU := $fml/fml:B_NR_WYCIAGU
				let $B_D_POP_WYCIAGU := $fml/fml:B_D_POP_WYCIAGU
				let $B_D_NAST_WYCIAGU := $fml/fml:B_D_NAST_WYCIAGU
				let $B_NAZWA_KR := $fml/fml:B_NAZWA_KR
				let $B_NAZWA_STOPY := $fml/fml:B_NAZWA_STOPY
				let $B_STOPA := $fml/fml:B_STOPA
				let $B_OKR_OPROC := $fml/fml:B_OKR_OPROC
				let $B_JDN_OKR_OPROC := $fml/fml:B_JDN_OKR_OPROC
				let $B_LIMIT_BIEZ_KR := $fml/fml:B_LIMIT_BIEZ_KR
				let $B_KREDYT_PRZYZN := $fml/fml:B_KREDYT_PRZYZN
				let $B_KOD_RACH_NIEPRZET := $fml/fml:B_KOD_RACH_NIEPRZET
				let $B_REWOLWING := $fml/fml:B_REWOLWING
				let $B_AUTO_KAPITAL := $fml/fml:B_AUTO_KAPITAL
				let $B_AUTO_ODSETKI := $fml/fml:B_AUTO_ODSETKI
				let $B_KOD_RACH_ODSETKI := $fml/fml:B_KOD_RACH_ODSETKI
				let $B_KOD_RACH_KAPITAL := $fml/fml:B_KOD_RACH_KAPITAL
				let $B_D_OST_SPLATY := $fml/fml:B_D_OST_SPLATY
				let $B_NAJBLIZSZE_NAL := $fml/fml:B_NAJBLIZSZE_NAL
				let $B_NAJBLIZSZE_ODS := $fml/fml:B_NAJBLIZSZE_ODS
				let $B_NAJBLIZSZE_RATY := $fml/fml:B_NAJBLIZSZE_RATY
				let $B_SUMA_ODS_ZAPL := $fml/fml:B_SUMA_ODS_ZAPL
				let $B_D_POP_WYMAG := $fml/fml:B_D_POP_WYMAG
				let $B_ODS_POP_WYMAG := $fml/fml:B_ODS_POP_WYMAG
				let $B_D_PRD_WYMAG := $fml/fml:B_D_PRD_WYMAG
				let $B_NR_ANEKSU := $fml/fml:B_NR_ANEKSU
				let $B_DOCHODY_DO := $fml/fml:B_DOCHODY_DO
				let $B_ODS_NIEROZL := $fml/fml:B_ODS_NIEROZL
				let $B_SYS := $fml/fml:B_SYS
				for $it at $p in $fml/fml:B_DL_NR_RACH
				return
					&lt;m:GetCekeAccsKonto>
					{
						if($B_KOD_RACH[$p])
							then &lt;m:KodRach>{ data($B_KOD_RACH[$p]) }&lt;/m:KodRach>
						else ()
					}
					{
						if($B_DL_NR_RACH[$p])
							then &lt;m:DlNrRach>{ data($B_DL_NR_RACH[$p]) }&lt;/m:DlNrRach>
						else ()
					}
					{
						if($B_SALDO[$p])
							then &lt;m:Saldo>{ data($B_SALDO[$p]) }&lt;/m:Saldo>
						else ()
					}
					{
						if($B_DOST_SRODKI[$p])
							then &lt;m:DostSrodki>{ data($B_DOST_SRODKI[$p]) }&lt;/m:DostSrodki>
						else ()
					}
					{
						if($B_WALUTA[$p])
							then &lt;m:Waluta>{ data($B_WALUTA[$p]) }&lt;/m:Waluta>
						else ()
					}
					{
						if($B_KOD_WALUTY[$p])
							then &lt;m:KodWaluty>{ data($B_KOD_WALUTY[$p]) }&lt;/m:KodWaluty>
						else ()
					}
					{
						if($B_DATA_OST_OPER[$p])
							then &lt;m:DataOstOper>{ data($B_DATA_OST_OPER[$p]) }&lt;/m:DataOstOper>
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then &lt;m:TypRach>{ data($B_TYP_RACH[$p]) }&lt;/m:TypRach>
						else ()
					}
					{
						if($B_RODZAJ_RACH[$p])
							then &lt;m:RodzajRach>{ data($B_RODZAJ_RACH[$p]) }&lt;/m:RodzajRach>
						else ()
					}
					{
						if($B_OPIS_RACH[$p])
							then &lt;m:OpisRach>{ data($B_OPIS_RACH[$p]) }&lt;/m:OpisRach>
						else ()
					}
					{
						if($B_SKROT_OPISU[$p])
							then &lt;m:SkrotOpisu>{ data($B_SKROT_OPISU[$p]) }&lt;/m:SkrotOpisu>
						else ()
					}
					{
						if($B_ID_ODDZ[$p])
							then &lt;m:IdOddz>{ data($B_ID_ODDZ[$p]) }&lt;/m:IdOddz>
						else ()
					}
					{
						if($B_NAZWA_ODDZ[$p])
							then &lt;m:NazwaOddz>{ data($B_NAZWA_ODDZ[$p]) }&lt;/m:NazwaOddz>
						else ()
					}
					{
						if($B_MIKROODDZIAL[$p])
							then &lt;m:Mikrooddzial>{ data($B_MIKROODDZIAL[$p]) }&lt;/m:Mikrooddzial>
						else ()
					}
					{
						if($B_PRAWA_KLIENTA[$p])
							then &lt;m:PrawaKlienta>{ data($B_PRAWA_KLIENTA[$p]) }&lt;/m:PrawaKlienta>
						else ()
					}
					{
						if($B_NAZWA_KLIENTA[$p])
							then &lt;m:NazwaKlienta>{ data($B_NAZWA_KLIENTA[$p]) }&lt;/m:NazwaKlienta>
						else ()
					}
					{
						if($B_UL_ZAM[$p])
							then &lt;m:UlZam>{ data($B_UL_ZAM[$p]) }&lt;/m:UlZam>
						else ()
					}
					{
						if($B_M_ZAM[$p])
							then &lt;m:MZam>{ data($B_M_ZAM[$p]) }&lt;/m:MZam>
						else ()
					}
					{
						if($B_OPROC1[$p])
							then &lt;m:Oproc1>{ data($B_OPROC1[$p]) }&lt;/m:Oproc1>
						else ()
					}
					{
						if($B_D_OTWARCIA[$p])
							then &lt;m:DOtwarcia>{ data($B_D_OTWARCIA[$p]) }&lt;/m:DOtwarcia>
						else ()
					}
					{
						if($B_D_ZAMKNIECIA[$p])
							then &lt;m:DZamkniecia>{ data($B_D_ZAMKNIECIA[$p]) }&lt;/m:DZamkniecia>
						else ()
					}
					{
						if($B_D_OSTATNIEJ_KAP[$p])
							then &lt;m:DOstatniejKap>{ data($B_D_OSTATNIEJ_KAP[$p]) }&lt;/m:DOstatniejKap>
						else ()
					}
					{
						if($B_D_NASTEPNEJ_KAP[$p])
							then &lt;m:DNastepnejKap>{ data($B_D_NASTEPNEJ_KAP[$p]) }&lt;/m:DNastepnejKap>
						else ()
					}
					{
						if($B_D_POCZ_WKL[$p])
							then &lt;m:DPoczWkl>{ data($B_D_POCZ_WKL[$p]) }&lt;/m:DPoczWkl>
						else ()
					}
					{
						if($B_D_KON_WKL[$p])
							then &lt;m:DKonWkl>{ data($B_D_KON_WKL[$p]) }&lt;/m:DKonWkl>
						else ()
					}
					{
						if($B_ODS_SKAP_AKT[$p])
							then &lt;m:OdsSkapAkt>{ data($B_ODS_SKAP_AKT[$p]) }&lt;/m:OdsSkapAkt>
						else ()
					}
					{
						if($B_ODS_SKAP_POP[$p])
							then &lt;m:OdsSkapPop>{ data($B_ODS_SKAP_POP[$p]) }&lt;/m:OdsSkapPop>
						else ()
					}
					{
						if($B_LIMIT2[$p])
							then &lt;m:Limit2>{ data($B_LIMIT2[$p]) }&lt;/m:Limit2>
						else ()
					}
					{
						if($B_D_KONCA_LIMITU[$p])
							then &lt;m:DKoncaLimitu>{ data($B_D_KONCA_LIMITU[$p]) }&lt;/m:DKoncaLimitu>
						else ()
					}
					{
						if($B_BLOKADA[$p])
							then &lt;m:Blokada>{ data($B_BLOKADA[$p]) }&lt;/m:Blokada>
						else ()
					}
					{
						if($B_RODZAJ_OPROC[$p])
							then &lt;m:RodzajOproc>{ data($B_RODZAJ_OPROC[$p]) }&lt;/m:RodzajOproc>
						else ()
					}
					{
						if($B_KAPITALIZACJA[$p])
							then &lt;m:Kapitalizacja>{ data($B_KAPITALIZACJA[$p]) }&lt;/m:Kapitalizacja>
						else ()
					}
					{
						if($B_PRZEKS_KAP[$p])
							then &lt;m:PrzeksKap>{ data($B_PRZEKS_KAP[$p]) }&lt;/m:PrzeksKap>
						else ()
					}
					{
						if($B_OKR_WKLADU[$p])
							then &lt;m:OkrWkladu>{ data($B_OKR_WKLADU[$p]) }&lt;/m:OkrWkladu>
						else ()
					}
					{
						if($B_JDN_OKR_WKLADU[$p])
							then &lt;m:JdnOkrWkladu>{ data($B_JDN_OKR_WKLADU[$p]) }&lt;/m:JdnOkrWkladu>
						else ()
					}
					{
						if($B_D_NALICZ_ODS[$p])
							then &lt;m:DNaliczOds>{ data($B_D_NALICZ_ODS[$p]) }&lt;/m:DNaliczOds>
						else ()
					}
					{
						if($B_D_NAST_OPER[$p])
							then &lt;m:DNastOper>{ data($B_D_NAST_OPER[$p]) }&lt;/m:DNastOper>
						else ()
					}
					{
						if($B_OBROTY_DZIEN_UWN[$p])
							then &lt;m:ObrotyDzienUwn>{ data($B_OBROTY_DZIEN_UWN[$p]) }&lt;/m:ObrotyDzienUwn>
						else ()
					}
					{
						if($B_OBROTY_DZIEN_UMA[$p])
							then &lt;m:ObrotyDzienUma>{ data($B_OBROTY_DZIEN_UMA[$p]) }&lt;/m:ObrotyDzienUma>
						else ()
					}
					{
						if($B_OBROTY_DZIEN_NWN[$p])
							then &lt;m:ObrotyDzienNwn>{ data($B_OBROTY_DZIEN_NWN[$p]) }&lt;/m:ObrotyDzienNwn>
						else ()
					}
					{
						if($B_OBROTY_DZIEN_NMA[$p])
							then &lt;m:ObrotyDzienNma>{ data($B_OBROTY_DZIEN_NMA[$p]) }&lt;/m:ObrotyDzienNma>
						else ()
					}
					{
						if($B_OPROC2[$p])
							then &lt;m:Oproc2>{ data($B_OPROC2[$p]) }&lt;/m:Oproc2>
						else ()
					}
					{
						if($B_NR_WYCIAGU[$p])
							then &lt;m:NrWyciagu>{ data($B_NR_WYCIAGU[$p]) }&lt;/m:NrWyciagu>
						else ()
					}
					{
						if($B_D_POP_WYCIAGU[$p])
							then &lt;m:DPopWyciagu>{ data($B_D_POP_WYCIAGU[$p]) }&lt;/m:DPopWyciagu>
						else ()
					}
					{
						if($B_D_NAST_WYCIAGU[$p])
							then &lt;m:DNastWyciagu>{ data($B_D_NAST_WYCIAGU[$p]) }&lt;/m:DNastWyciagu>
						else ()
					}
					{
						if($B_NAZWA_KR[$p])
							then &lt;m:NazwaKr>{ data($B_NAZWA_KR[$p]) }&lt;/m:NazwaKr>
						else ()
					}
					{
						if($B_NAZWA_STOPY[$p])
							then &lt;m:NazwaStopy>{ data($B_NAZWA_STOPY[$p]) }&lt;/m:NazwaStopy>
						else ()
					}
					{
						if($B_STOPA[$p])
							then &lt;m:Stopa>{ data($B_STOPA[$p]) }&lt;/m:Stopa>
						else ()
					}
					{
						if($B_OKR_OPROC[$p])
							then &lt;m:OkrOproc>{ data($B_OKR_OPROC[$p]) }&lt;/m:OkrOproc>
						else ()
					}
					{
						if($B_JDN_OKR_OPROC[$p])
							then &lt;m:JdnOkrOproc>{ data($B_JDN_OKR_OPROC[$p]) }&lt;/m:JdnOkrOproc>
						else ()
					}
					{
						if($B_LIMIT_BIEZ_KR[$p])
							then &lt;m:LimitBiezKr>{ data($B_LIMIT_BIEZ_KR[$p]) }&lt;/m:LimitBiezKr>
						else ()
					}
					{
						if($B_KREDYT_PRZYZN[$p])
							then &lt;m:KredytPrzyzn>{ data($B_KREDYT_PRZYZN[$p]) }&lt;/m:KredytPrzyzn>
						else ()
					}
					{
						if($B_KOD_RACH_NIEPRZET[$p])
							then &lt;m:KodRachNieprzet>{ data($B_KOD_RACH_NIEPRZET[$p]) }&lt;/m:KodRachNieprzet>
						else ()
					}
					{
						if($B_REWOLWING[$p])
							then &lt;m:Rewolwing>{ data($B_REWOLWING[$p]) }&lt;/m:Rewolwing>
						else ()
					}
					{
						if($B_AUTO_KAPITAL[$p])
							then &lt;m:AutoKapital>{ data($B_AUTO_KAPITAL[$p]) }&lt;/m:AutoKapital>
						else ()
					}
					{
						if($B_AUTO_ODSETKI[$p])
							then &lt;m:AutoOdsetki>{ data($B_AUTO_ODSETKI[$p]) }&lt;/m:AutoOdsetki>
						else ()
					}
					{
						if($B_KOD_RACH_ODSETKI[$p])
							then &lt;m:KodRachOdsetki>{ data($B_KOD_RACH_ODSETKI[$p]) }&lt;/m:KodRachOdsetki>
						else ()
					}
					{
						if($B_KOD_RACH_KAPITAL[$p])
							then &lt;m:KodRachKapital>{ data($B_KOD_RACH_KAPITAL[$p]) }&lt;/m:KodRachKapital>
						else ()
					}
					{
						if($B_D_OST_SPLATY[$p])
							then &lt;m:DOstSplaty>{ data($B_D_OST_SPLATY[$p]) }&lt;/m:DOstSplaty>
						else ()
					}
					{
						if($B_NAJBLIZSZE_NAL[$p])
							then &lt;m:NajblizszeNal>{ data($B_NAJBLIZSZE_NAL[$p]) }&lt;/m:NajblizszeNal>
						else ()
					}
					{
						if($B_NAJBLIZSZE_ODS[$p])
							then &lt;m:NajblizszeOds>{ data($B_NAJBLIZSZE_ODS[$p]) }&lt;/m:NajblizszeOds>
						else ()
					}
					{
						if($B_NAJBLIZSZE_RATY[$p])
							then &lt;m:NajblizszeRaty>{ data($B_NAJBLIZSZE_RATY[$p]) }&lt;/m:NajblizszeRaty>
						else ()
					}
					{
						if($B_SUMA_ODS_ZAPL[$p])
							then &lt;m:SumaOdsZapl>{ data($B_SUMA_ODS_ZAPL[$p]) }&lt;/m:SumaOdsZapl>
						else ()
					}
					{
						if($B_D_POP_WYMAG[$p])
							then &lt;m:DPopWymag>{ data($B_D_POP_WYMAG[$p]) }&lt;/m:DPopWymag>
						else ()
					}
					{
						if($B_ODS_POP_WYMAG[$p])
							then &lt;m:OdsPopWymag>{ data($B_ODS_POP_WYMAG[$p]) }&lt;/m:OdsPopWymag>
						else ()
					}
					{
						if($B_D_PRD_WYMAG[$p])
							then &lt;m:DPrdWymag>{ data($B_D_PRD_WYMAG[$p]) }&lt;/m:DPrdWymag>
						else ()
					}
					{
						if($B_NR_ANEKSU[$p])
							then &lt;m:NrAneksu>{ data($B_NR_ANEKSU[$p]) }&lt;/m:NrAneksu>
						else ()
					}
					{
						if($B_DOCHODY_DO[$p])
							then &lt;m:DochodyDo>{ data($B_DOCHODY_DO[$p]) }&lt;/m:DochodyDo>
						else ()
					}
					{
						if($B_ODS_NIEROZL[$p])
							then &lt;m:OdsNierozl>{ data($B_ODS_NIEROZL[$p]) }&lt;/m:OdsNierozl>
						else ()
					}
					{
						if($B_SYS[$p])
							then &lt;m:Sys>{ data($B_SYS[$p]) }&lt;/m:Sys>
						else ()
					}
					&lt;/m:GetCekeAccsKonto>
			}

		&lt;/m:GetCekeAccsResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetCekeAccsResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>