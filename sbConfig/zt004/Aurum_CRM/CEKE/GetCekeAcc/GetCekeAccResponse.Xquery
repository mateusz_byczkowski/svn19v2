<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCekeAccResponse($fml as element(fml:FML32))
	as element(m:GetCekeAccResponse) {
		&lt;m:GetCekeAccResponse>
			{
				if($fml/fml:B_KOD_RACH)
					then &lt;m:KodRach>{ data($fml/fml:B_KOD_RACH) }&lt;/m:KodRach>
					else ()
			}
			{
				if($fml/fml:B_DL_NR_RACH)
					then &lt;m:DlNrRach>{ data($fml/fml:B_DL_NR_RACH) }&lt;/m:DlNrRach>
					else ()
			}
			{
				if($fml/fml:B_SALDO)
					then &lt;m:Saldo>{ data($fml/fml:B_SALDO) }&lt;/m:Saldo>
					else ()
			}
			{
				if($fml/fml:B_DOST_SRODKI)
					then &lt;m:DostSrodki>{ data($fml/fml:B_DOST_SRODKI) }&lt;/m:DostSrodki>
					else ()
			}
			{
				if($fml/fml:B_WALUTA)
					then &lt;m:Waluta>{ data($fml/fml:B_WALUTA) }&lt;/m:Waluta>
					else ()
			}
			{
				if($fml/fml:B_KOD_WALUTY)
					then &lt;m:KodWaluty>{ data($fml/fml:B_KOD_WALUTY) }&lt;/m:KodWaluty>
					else ()
			}
			{
				if($fml/fml:B_DATA_OST_OPER)
					then &lt;m:DataOstOper>{ data($fml/fml:B_DATA_OST_OPER) }&lt;/m:DataOstOper>
					else ()
			}
			{
				if($fml/fml:B_TYP_RACH)
					then &lt;m:TypRach>{ data($fml/fml:B_TYP_RACH) }&lt;/m:TypRach>
					else ()
			}
			{
				if($fml/fml:B_OPIS_RACH)
					then &lt;m:OpisRach>{ data($fml/fml:B_OPIS_RACH) }&lt;/m:OpisRach>
					else ()
			}
			{
				if($fml/fml:B_SKROT_OPISU)
					then &lt;m:SkrotOpisu>{ data($fml/fml:B_SKROT_OPISU) }&lt;/m:SkrotOpisu>
					else ()
			}
			{
				if($fml/fml:B_ID_ODDZ)
					then &lt;m:IdOddz>{ data($fml/fml:B_ID_ODDZ) }&lt;/m:IdOddz>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_ODDZ)
					then &lt;m:NazwaOddz>{ data($fml/fml:B_NAZWA_ODDZ) }&lt;/m:NazwaOddz>
					else ()
			}
			{
				if($fml/fml:B_RODZAJ_RACH)
					then &lt;m:RodzajRach>{ data($fml/fml:B_RODZAJ_RACH) }&lt;/m:RodzajRach>
					else ()
			}
			{
				if($fml/fml:B_MIKROODDZIAL)
					then &lt;m:Mikrooddzial>{ data($fml/fml:B_MIKROODDZIAL) }&lt;/m:Mikrooddzial>
					else ()
			}
			{
				if($fml/fml:B_PRAWA_KLIENTA)
					then &lt;m:PrawaKlienta>{ data($fml/fml:B_PRAWA_KLIENTA) }&lt;/m:PrawaKlienta>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KLIENTA)
					then &lt;m:NazwaKlienta>{ data($fml/fml:B_NAZWA_KLIENTA) }&lt;/m:NazwaKlienta>
					else ()
			}
			{
				if($fml/fml:B_UL_ZAM)
					then &lt;m:UlZam>{ data($fml/fml:B_UL_ZAM) }&lt;/m:UlZam>
					else ()
			}
			{
				if($fml/fml:B_M_ZAM)
					then &lt;m:MZam>{ data($fml/fml:B_M_ZAM) }&lt;/m:MZam>
					else ()
			}
			{
				if($fml/fml:B_OPROC1)
					then &lt;m:Oproc1>{ data($fml/fml:B_OPROC1) }&lt;/m:Oproc1>
					else ()
			}
			{
				if($fml/fml:B_D_OTWARCIA)
					then &lt;m:DOtwarcia>{ data($fml/fml:B_D_OTWARCIA) }&lt;/m:DOtwarcia>
					else ()
			}
			{
				if($fml/fml:B_D_ZAMKNIECIA)
					then &lt;m:DZamkniecia>{ data($fml/fml:B_D_ZAMKNIECIA) }&lt;/m:DZamkniecia>
					else ()
			}
			{
				if($fml/fml:B_D_OSTATNIEJ_KAP)
					then &lt;m:DOstatniejKap>{ data($fml/fml:B_D_OSTATNIEJ_KAP) }&lt;/m:DOstatniejKap>
					else ()
			}
			{
				if($fml/fml:B_D_NASTEPNEJ_KAP)
					then &lt;m:DNastepnejKap>{ data($fml/fml:B_D_NASTEPNEJ_KAP) }&lt;/m:DNastepnejKap>
					else ()
			}
			{
				if($fml/fml:B_D_POCZ_WKL)
					then &lt;m:DPoczWkl>{ data($fml/fml:B_D_POCZ_WKL) }&lt;/m:DPoczWkl>
					else ()
			}
			{
				if($fml/fml:B_D_KON_WKL)
					then &lt;m:DKonWkl>{ data($fml/fml:B_D_KON_WKL) }&lt;/m:DKonWkl>
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_AKT)
					then &lt;m:OdsSkapAkt>{ data($fml/fml:B_ODS_SKAP_AKT) }&lt;/m:OdsSkapAkt>
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_POP)
					then &lt;m:OdsSkapPop>{ data($fml/fml:B_ODS_SKAP_POP) }&lt;/m:OdsSkapPop>
					else ()
			}
			{
				if($fml/fml:B_LIMIT2)
					then &lt;m:Limit2>{ data($fml/fml:B_LIMIT2) }&lt;/m:Limit2>
					else ()
			}
			{
				if($fml/fml:B_D_KONCA_LIMITU)
					then &lt;m:DKoncaLimitu>{ data($fml/fml:B_D_KONCA_LIMITU) }&lt;/m:DKoncaLimitu>
					else ()
			}
			{
				if($fml/fml:B_BLOKADA)
					then &lt;m:Blokada>{ data($fml/fml:B_BLOKADA) }&lt;/m:Blokada>
					else ()
			}
			{
				if($fml/fml:B_RODZAJ_OPROC)
					then &lt;m:RodzajOproc>{ data($fml/fml:B_RODZAJ_OPROC) }&lt;/m:RodzajOproc>
					else ()
			}
			{
				if($fml/fml:B_KAPITALIZACJA)
					then &lt;m:Kapitalizacja>{ data($fml/fml:B_KAPITALIZACJA) }&lt;/m:Kapitalizacja>
					else ()
			}
			{
				if($fml/fml:B_PRZEKS_KAP)
					then &lt;m:PrzeksKap>{ data($fml/fml:B_PRZEKS_KAP) }&lt;/m:PrzeksKap>
					else ()
			}
			{
				if($fml/fml:B_OKR_WKLADU)
					then &lt;m:OkrWkladu>{ data($fml/fml:B_OKR_WKLADU) }&lt;/m:OkrWkladu>
					else ()
			}
			{
				if($fml/fml:B_JDN_OKR_WKLADU)
					then &lt;m:JdnOkrWkladu>{ data($fml/fml:B_JDN_OKR_WKLADU) }&lt;/m:JdnOkrWkladu>
					else ()
			}
			{
				if($fml/fml:B_D_NALICZ_ODS)
					then &lt;m:DNaliczOds>{ data($fml/fml:B_D_NALICZ_ODS) }&lt;/m:DNaliczOds>
					else ()
			}
			{
				if($fml/fml:B_D_NAST_OPER)
					then &lt;m:DNastOper>{ data($fml/fml:B_D_NAST_OPER) }&lt;/m:DNastOper>
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_UWN)
					then &lt;m:ObrotyDzienUwn>{ data($fml/fml:B_OBROTY_DZIEN_UWN) }&lt;/m:ObrotyDzienUwn>
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_UMA)
					then &lt;m:ObrotyDzienUma>{ data($fml/fml:B_OBROTY_DZIEN_UMA) }&lt;/m:ObrotyDzienUma>
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_NWN)
					then &lt;m:ObrotyDzienNwn>{ data($fml/fml:B_OBROTY_DZIEN_NWN) }&lt;/m:ObrotyDzienNwn>
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_NMA)
					then &lt;m:ObrotyDzienNma>{ data($fml/fml:B_OBROTY_DZIEN_NMA) }&lt;/m:ObrotyDzienNma>
					else ()
			}
			{
				if($fml/fml:B_OPROC2)
					then &lt;m:Oproc2>{ data($fml/fml:B_OPROC2) }&lt;/m:Oproc2>
					else ()
			}
			{
				if($fml/fml:B_NR_WYCIAGU)
					then &lt;m:NrWyciagu>{ data($fml/fml:B_NR_WYCIAGU) }&lt;/m:NrWyciagu>
					else ()
			}
			{
				if($fml/fml:B_D_POP_WYCIAGU)
					then &lt;m:DPopWyciagu>{ data($fml/fml:B_D_POP_WYCIAGU) }&lt;/m:DPopWyciagu>
					else ()
			}
			{
				if($fml/fml:B_D_NAST_WYCIAGU)
					then &lt;m:DNastWyciagu>{ data($fml/fml:B_D_NAST_WYCIAGU) }&lt;/m:DNastWyciagu>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KR)
					then &lt;m:NazwaKr>{ data($fml/fml:B_NAZWA_KR) }&lt;/m:NazwaKr>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_STOPY)
					then &lt;m:NazwaStopy>{ data($fml/fml:B_NAZWA_STOPY) }&lt;/m:NazwaStopy>
					else ()
			}
			{
				if($fml/fml:B_STOPA)
					then &lt;m:Stopa>{ data($fml/fml:B_STOPA) }&lt;/m:Stopa>
					else ()
			}
			{
				if($fml/fml:B_OKR_OPROC)
					then &lt;m:OkrOproc>{ data($fml/fml:B_OKR_OPROC) }&lt;/m:OkrOproc>
					else ()
			}
			{
				if($fml/fml:B_JDN_OKR_OPROC)
					then &lt;m:JdnOkrOproc>{ data($fml/fml:B_JDN_OKR_OPROC) }&lt;/m:JdnOkrOproc>
					else ()
			}
			{
				if($fml/fml:B_LIMIT_BIEZ_KR)
					then &lt;m:LimitBiezKr>{ data($fml/fml:B_LIMIT_BIEZ_KR) }&lt;/m:LimitBiezKr>
					else ()
			}
			{
				if($fml/fml:B_KREDYT_PRZYZN)
					then &lt;m:KredytPrzyzn>{ data($fml/fml:B_KREDYT_PRZYZN) }&lt;/m:KredytPrzyzn>
					else ()
			}
			{
				if($fml/fml:B_KOD_RACH_NIEPRZET)
					then &lt;m:KodRachNieprzet>{ data($fml/fml:B_KOD_RACH_NIEPRZET) }&lt;/m:KodRachNieprzet>
					else ()
			}
			{
				if($fml/fml:B_REWOLWING)
					then &lt;m:Rewolwing>{ data($fml/fml:B_REWOLWING) }&lt;/m:Rewolwing>
					else ()
			}
			{
				if($fml/fml:B_AUTO_KAPITAL)
					then &lt;m:AutoKapital>{ data($fml/fml:B_AUTO_KAPITAL) }&lt;/m:AutoKapital>
					else ()
			}
			{
				if($fml/fml:B_AUTO_ODSETKI)
					then &lt;m:AutoOdsetki>{ data($fml/fml:B_AUTO_ODSETKI) }&lt;/m:AutoOdsetki>
					else ()
			}
			{
				if($fml/fml:B_KOD_RACH_ODSETKI)
					then &lt;m:KodRachOdsetki>{ data($fml/fml:B_KOD_RACH_ODSETKI) }&lt;/m:KodRachOdsetki>
					else ()
			}
			{
				if($fml/fml:B_KOD_RACH_KAPITAL)
					then &lt;m:KodRachKapital>{ data($fml/fml:B_KOD_RACH_KAPITAL) }&lt;/m:KodRachKapital>
					else ()
			}
			{
				if($fml/fml:B_D_OST_SPLATY)
					then &lt;m:DOstSplaty>{ data($fml/fml:B_D_OST_SPLATY) }&lt;/m:DOstSplaty>
					else ()
			}
			{
				if($fml/fml:B_NAJBLIZSZE_NAL)
					then &lt;m:NajblizszeNal>{ data($fml/fml:B_NAJBLIZSZE_NAL) }&lt;/m:NajblizszeNal>
					else ()
			}
			{
				if($fml/fml:B_NAJBLIZSZE_ODS)
					then &lt;m:NajblizszeOds>{ data($fml/fml:B_NAJBLIZSZE_ODS) }&lt;/m:NajblizszeOds>
					else ()
			}
			{
				if($fml/fml:B_NAJBLIZSZE_RATY)
					then &lt;m:NajblizszeRaty>{ data($fml/fml:B_NAJBLIZSZE_RATY) }&lt;/m:NajblizszeRaty>
					else ()
			}
			{
				if($fml/fml:B_SUMA_ODS_ZAPL)
					then &lt;m:SumaOdsZapl>{ data($fml/fml:B_SUMA_ODS_ZAPL) }&lt;/m:SumaOdsZapl>
					else ()
			}
			{
				if($fml/fml:B_D_POP_WYMAG)
					then &lt;m:DPopWymag>{ data($fml/fml:B_D_POP_WYMAG) }&lt;/m:DPopWymag>
					else ()
			}
			{
				if($fml/fml:B_ODS_POP_WYMAG)
					then &lt;m:OdsPopWymag>{ data($fml/fml:B_ODS_POP_WYMAG) }&lt;/m:OdsPopWymag>
					else ()
			}
			{
				if($fml/fml:B_D_PRD_WYMAG)
					then &lt;m:DPrdWymag>{ data($fml/fml:B_D_PRD_WYMAG) }&lt;/m:DPrdWymag>
					else ()
			}
			{
				if($fml/fml:B_NR_ANEKSU)
					then &lt;m:NrAneksu>{ data($fml/fml:B_NR_ANEKSU) }&lt;/m:NrAneksu>
					else ()
			}
			{
				if($fml/fml:B_DOCHODY_DO)
					then &lt;m:DochodyDo>{ data($fml/fml:B_DOCHODY_DO) }&lt;/m:DochodyDo>
					else ()
			}
			{
				if($fml/fml:B_ODS_NIEROZL)
					then &lt;m:OdsNierozl>{ data($fml/fml:B_ODS_NIEROZL) }&lt;/m:OdsNierozl>
					else ()
			}
			{
				if($fml/fml:B_UL_KORESP)
					then &lt;m:UlKoresp>{ data($fml/fml:B_UL_KORESP) }&lt;/m:UlKoresp>
					else ()
			}
			{
				if($fml/fml:B_M_KORESP)
					then &lt;m:MKoresp>{ data($fml/fml:B_M_KORESP) }&lt;/m:MKoresp>
					else ()
			}
			{
				if($fml/fml:B_KOD_KORESP)
					then &lt;m:KodKoresp>{ data($fml/fml:B_KOD_KORESP) }&lt;/m:KodKoresp>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KORESP)
					then &lt;m:NazwaKoresp>{ data($fml/fml:B_NAZWA_KORESP) }&lt;/m:NazwaKoresp>
					else ()
			}
			{
				let $B_LIMIT_OD := $fml/fml:B_LIMIT_OD
				let $B_LIMIT_KWOTA := $fml/fml:B_LIMIT_KWOTA
				for $it at $p in $fml/fml:B_LIMIT_OD
				return
					&lt;m:GetCekeAccTransza>
					{
						if($B_LIMIT_OD[$p])
							then &lt;m:LimitOd>{ data($B_LIMIT_OD[$p]) }&lt;/m:LimitOd>
						else ()
					}
					{
						if($B_LIMIT_KWOTA[$p])
							then &lt;m:LimitKwota>{ data($B_LIMIT_KWOTA[$p]) }&lt;/m:LimitKwota>
						else ()
					}
					&lt;/m:GetCekeAccTransza>
			}
			{
				let $B_D_ODS_NALICZ := $fml/fml:B_D_ODS_NALICZ
				let $B_D_WYMAGALNOSCI := $fml/fml:B_D_WYMAGALNOSCI
				for $it at $p in $fml/fml:B_D_ODS_NALICZ
				return
					&lt;m:GetCekeAccOdsetki>
					{
						if($B_D_ODS_NALICZ[$p])
							then &lt;m:DOdsNalicz>{ data($B_D_ODS_NALICZ[$p]) }&lt;/m:DOdsNalicz>
						else ()
					}
					{
						if($B_D_WYMAGALNOSCI[$p])
							then &lt;m:DWymagalnosci>{ data($B_D_WYMAGALNOSCI[$p]) }&lt;/m:DWymagalnosci>
						else ()
					}
					&lt;/m:GetCekeAccOdsetki>
			}
		&lt;/m:GetCekeAccResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetCekeAccResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>