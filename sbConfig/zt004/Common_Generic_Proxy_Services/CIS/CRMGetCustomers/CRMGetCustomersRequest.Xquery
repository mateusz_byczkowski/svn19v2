<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustomersRequest($req as element(m:CRMGetCustomersRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:ZakresWyszukiwania)
					then &lt;fml:CI_ZAKRES_WYSZUKIWANIA>{ data($req/m:ZakresWyszukiwania) }&lt;/fml:CI_ZAKRES_WYSZUKIWANIA>
					else ()
			}
                        {
                                for $v in $req/m:IdSpolki
                                return
				    &lt;fml:CI_ID_SPOLKI>{ data($v) }&lt;/fml:CI_ID_SPOLKI>
                        }
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON>{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL>{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL>
					else ()
			}
			{
				if($req/m:Nip)
					then &lt;fml:DC_NIP>{ data($req/m:Nip) }&lt;/fml:DC_NIP>
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then &lt;fml:DC_NUMER_PASZPORTU>{ data($req/m:NumerPaszportu) }&lt;/fml:DC_NUMER_PASZPORTU>
					else ()
			}
			{
				if($req/m:Nik)
					then &lt;fml:CI_NIK>{ data($req/m:Nik) }&lt;/fml:CI_NIK>
					else ()
			}
			{
				if($req/m:Nazwa)
					then &lt;fml:DC_NAZWA>{ data($req/m:Nazwa) }&lt;/fml:DC_NAZWA>
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO>{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO>
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then &lt;fml:DC_MIASTO_DANE_PODST>{ data($req/m:MiastoDanePodst) }&lt;/fml:DC_MIASTO_DANE_PODST>
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then &lt;fml:DC_KOD_POCZTOWY_DANE_PODST>{ data($req/m:KodPocztowyDanePodst) }&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST>
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER>{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI>{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI>
					else ()
			}
			{
				if($req/m:Sortowanie)
					then &lt;fml:CI_SORTOWANIE>{ data($req/m:Sortowanie) }&lt;/fml:CI_SORTOWANIE>
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA>{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA>
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then &lt;fml:DC_ULICA_DANE_PODST>{ data($req/m:UlicaDanePodst) }&lt;/fml:DC_ULICA_DANE_PODST>
					else ()
			}
			{
				if($req/m:NrTelefonu)
					then &lt;fml:DC_NR_TELEFONU>{ data($req/m:NrTelefonu) }&lt;/fml:DC_NR_TELEFONU>
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE>{ data($req/m:Imie) }&lt;/fml:DC_IMIE>
					else ()
			}
			{
				if($req/m:Ekd)
					then &lt;fml:DC_EKD>{ data($req/m:Ekd) }&lt;/fml:DC_EKD>
					else ()
			}
			{
				if($req/m:Spw)
					then &lt;fml:DC_SPW>{ data($req/m:Spw) }&lt;/fml:DC_SPW>
					else ()
			}
			{
				if($req/m:Decyl)
					then &lt;fml:CI_DECYL>{ data($req/m:Decyl) }&lt;/fml:CI_DECYL>
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:JednostkaKorporacyjna)
					then &lt;fml:DC_JEDNOSTKA_KORPORACYJNA>{ data($req/m:JednostkaKorporacyjna) }&lt;/fml:DC_JEDNOSTKA_KORPORACYJNA>
					else ()
			}
			{
				if($req/m:DataUrodzenia)
					then &lt;fml:DC_DATA_URODZENIA>{ data($req/m:DataUrodzenia) }&lt;/fml:DC_DATA_URODZENIA>
					else ()
			}
                        {
                               if($req/m:Sterowanie)
                                        then &lt;fml:CI_STEROWANIE>{ data($req/m:Sterowanie) }&lt;/fml:CI_STEROWANIE>
                                        else ()
                        }
                        {
                               if($req/m:NumerPaczkiStr)
                                        then &lt;fml:CI_NUMER_PACZKI_STR>{ data($req/m:NumerPaczkiStr) }&lt;/fml:CI_NUMER_PACZKI_STR>
                                        else ()
                        }
                        {
                               if($req/m:Cont)
                                        then &lt;fml:CI_CONT>{ data($req/m:Cont) }&lt;/fml:CI_CONT>
                                        else ()
                        }
			{
				if($req/m:DokTozsamosci)
					then &lt;fml:CI_DOK_TOZSAMOSCI>{ data($req/m:DokTozsamosci) }&lt;/fml:CI_DOK_TOZSAMOSCI>
					else ()
			}
			{
				if($req/m:SeriaNrDok)
					then &lt;fml:CI_SERIA_NR_DOK>{ data($req/m:SeriaNrDok) }&lt;/fml:CI_SERIA_NR_DOK>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetCustomersRequest($body/m:CRMGetCustomersRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>