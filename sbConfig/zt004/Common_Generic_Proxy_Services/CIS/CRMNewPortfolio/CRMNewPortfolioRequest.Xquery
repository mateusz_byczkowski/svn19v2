<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMNewPortfolioRequest($req as element(m:CRMNewPortfolioRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:SkpPracownikaRej)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:SkpPracownikaRej) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:ImiePrac)
					then &lt;fml:CI_IMIE_PRAC>{ data($req/m:ImiePrac) }&lt;/fml:CI_IMIE_PRAC>
					else ()
			}
			{
				if($req/m:NazwPrac)
					then &lt;fml:CI_NAZW_PRAC>{ data($req/m:NazwPrac) }&lt;/fml:CI_NAZW_PRAC>
					else ()
			}
			{
				if($req/m:SkpZastepcy)
					then &lt;fml:CI_SKP_ZASTEPCY>{ data($req/m:SkpZastepcy) }&lt;/fml:CI_SKP_ZASTEPCY>
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMNewPortfolioRequest($body/m:CRMNewPortfolioRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>