<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/esb/messages/";
declare namespace xf = "http://bzwbk.com/services/esb/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetProductAreasResponse($fml as element(fml:FML32))
	as element(m:getProductAreasResponse) {
		&lt;m:getProductAreasResponse>
			{

				let $CI_DICTIONARY_ID := $fml/fml:CI_DICTIONARY_ID
				let $CI_PRODUCT_AREA_CODE := $fml/fml:CI_PRODUCT_AREA_CODE
				let $CI_PRODUCT_AREA_NAME_PL := $fml/fml:CI_PRODUCT_AREA_NAME_PL
				let $CI_PRODUCT_AREA_NAME_EN := $fml/fml:CI_PRODUCT_AREA_NAME_EN
				let $CI_ORDER := $fml/fml:CI_ORDER
				for $it at $p in $fml/fml:CI_DICTIONARY_ID
				return
					&lt;m:getProductAreasslownik_l1>
					{
						if($CI_DICTIONARY_ID[$p])
							then &lt;m:DictionaryId>{ data($CI_DICTIONARY_ID[$p]) }&lt;/m:DictionaryId>
						else ()
					}
					{
						if($CI_PRODUCT_AREA_CODE[$p])
							then &lt;m:ProductAreaCode>{ data($CI_PRODUCT_AREA_CODE[$p]) }&lt;/m:ProductAreaCode>
						else ()
					}
					{
						if($CI_PRODUCT_AREA_NAME_PL[$p])
							then &lt;m:ProductAreaNamePl>{ data($CI_PRODUCT_AREA_NAME_PL[$p]) }&lt;/m:ProductAreaNamePl>
						else ()
					}
					{
						if($CI_PRODUCT_AREA_NAME_EN[$p])
							then &lt;m:ProductAreaNameEn>{ data($CI_PRODUCT_AREA_NAME_EN[$p]) }&lt;/m:ProductAreaNameEn>
						else ()
					}
					{
						if($CI_ORDER[$p])
							then &lt;m:Order>{ data($CI_ORDER[$p]) }&lt;/m:Order>
						else ()
					}
					&lt;/m:getProductAreasslownik_l1>
			}

		&lt;/m:getProductAreasResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetProductAreasResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>