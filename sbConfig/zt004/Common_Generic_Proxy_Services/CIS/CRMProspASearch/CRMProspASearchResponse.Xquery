<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMProspASearchResponse($fml as element(fml:FML32))
	as element(m:CRMProspASearchResponse) {
		&lt;m:CRMProspASearchResponse>
			{

				let $CI_ID_KLIENTA := $fml/fml:CI_ID_KLIENTA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $DC_NAZWA := $fml/fml:DC_NAZWA
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL
				let $DC_NR_DOWODU_REGON := $fml/fml:DC_NR_DOWODU_REGON
				let $DC_KRAJ := $fml/fml:DC_KRAJ
				let $DC_WOJEWODZTWO := $fml/fml:DC_WOJEWODZTWO
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST
				let $CI_ADRES_DOD := $fml/fml:CI_ADRES_DOD
				let $DC_NR_TELEFONU := $fml/fml:DC_NR_TELEFONU
				let $DC_NR_TELEF_KOMORKOWEGO := $fml/fml:DC_NR_TELEF_KOMORKOWEGO
				let $DC_JEDNOSTKA_KORPORACYJNA := $fml/fml:DC_JEDNOSTKA_KORPORACYJNA
				let $DC_DATA_URODZENIA := $fml/fml:DC_DATA_URODZENIA
                                let $CI_ID_SPOLKI := $fml/fml:CI_ID_SPOLKI

				for $it at $p in $fml/fml:CI_ID_KLIENTA
				return
					&lt;m:CRMProspASearchKlient>
					{
						if($CI_ID_KLIENTA[$p])
							then &lt;m:IdKlienta>{ data($CI_ID_KLIENTA[$p]) }&lt;/m:IdKlienta>
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie>{ data($DC_IMIE[$p]) }&lt;/m:Imie>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko>{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko>
						else ()
					}
					{
						if($DC_NAZWA[$p])
							then &lt;m:Nazwa>{ data($DC_NAZWA[$p]) }&lt;/m:Nazwa>
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then &lt;m:UdostepGrupa>{ data($CI_UDOSTEP_GRUPA[$p]) }&lt;/m:UdostepGrupa>
						else ()
					}
					{
						if($DC_NR_PESEL[$p])
							then &lt;m:NrPesel>{ data($DC_NR_PESEL[$p]) }&lt;/m:NrPesel>
						else ()
					}
					{
						if($DC_NR_DOWODU_REGON[$p])
							then &lt;m:NrDowoduRegon>{ data($DC_NR_DOWODU_REGON[$p]) }&lt;/m:NrDowoduRegon>
						else ()
					}
					{
						if($DC_KRAJ[$p])
							then &lt;m:Kraj>{ data($DC_KRAJ[$p]) }&lt;/m:Kraj>
						else ()
					}
					{
						if($DC_WOJEWODZTWO[$p])
							then &lt;m:Wojewodztwo>{ data($DC_WOJEWODZTWO[$p]) }&lt;/m:Wojewodztwo>
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_DANE_PODST[$p])
							then &lt;m:KodPocztowyDanePodst>{ data($DC_KOD_POCZTOWY_DANE_PODST[$p]) }&lt;/m:KodPocztowyDanePodst>
						else ()
					}
					{
						if($DC_MIASTO_DANE_PODST[$p])
							then &lt;m:MiastoDanePodst>{ data($DC_MIASTO_DANE_PODST[$p]) }&lt;/m:MiastoDanePodst>
						else ()
					}
					{
						if($DC_ULICA_DANE_PODST[$p])
							then &lt;m:UlicaDanePodst>{ data($DC_ULICA_DANE_PODST[$p]) }&lt;/m:UlicaDanePodst>
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_DANE_PODST[$p])
							then &lt;m:NrPosesLokaluDanePodst>{ data($DC_NR_POSES_LOKALU_DANE_PODST[$p]) }&lt;/m:NrPosesLokaluDanePodst>
						else ()
					}
					{
						if($CI_ADRES_DOD[$p])
							then &lt;m:AdresDod>{ data($CI_ADRES_DOD[$p]) }&lt;/m:AdresDod>
						else ()
					}
					{
						if($DC_NR_TELEFONU[$p])
							then &lt;m:NrTelefonu>{ data($DC_NR_TELEFONU[$p]) }&lt;/m:NrTelefonu>
						else ()
					}
					{
						if($DC_NR_TELEF_KOMORKOWEGO[$p])
							then &lt;m:NrTelefKomorkowego>{ data($DC_NR_TELEF_KOMORKOWEGO[$p]) }&lt;/m:NrTelefKomorkowego>
						else ()
					}
					{
						if($DC_JEDNOSTKA_KORPORACYJNA[$p])
							then &lt;m:JednostkaKorporacyjna>{ data($DC_JEDNOSTKA_KORPORACYJNA[$p]) }&lt;/m:JednostkaKorporacyjna>
						else ()
					}
					{
						if($DC_DATA_URODZENIA[$p])
							then &lt;m:DataUrodzenia>{ data($DC_DATA_URODZENIA[$p]) }&lt;/m:DataUrodzenia>
						else ()
					}
                                        {
                                                if($CI_ID_SPOLKI[$p])
                                                        then &lt;m:IdSpolki>{ data($CI_ID_SPOLKI[$p]) }&lt;/m:IdSpolki>
                                                else ()
                                        }
					&lt;/m:CRMProspASearchKlient>
			}

		&lt;/m:CRMProspASearchResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMProspASearchResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>