<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMProspASearchRequest($req as element(m:CRMProspASearchRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:ZakresWyszukiwania)
					then &lt;fml:CI_ZAKRES_WYSZUKIWANIA>{ data($req/m:ZakresWyszukiwania) }&lt;/fml:CI_ZAKRES_WYSZUKIWANIA>
					else ()
			}
			{
                                for $i in 1 to count($req/m:IdSpolki)
                                return
                                        &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki[$i]) }&lt;/fml:CI_ID_SPOLKI>

			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:Sortowanie)
					then &lt;fml:CI_SORTOWANIE>{ data($req/m:Sortowanie) }&lt;/fml:CI_SORTOWANIE>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL>{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL>
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON>{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($req/m:Nip)
					then &lt;fml:DC_NIP>{ data($req/m:Nip) }&lt;/fml:DC_NIP>
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then &lt;fml:DC_NUMER_PASZPORTU>{ data($req/m:NumerPaszportu) }&lt;/fml:DC_NUMER_PASZPORTU>
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE>{ data($req/m:Imie) }&lt;/fml:DC_IMIE>
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO>{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO>
					else ()
			}
			{
				if($req/m:NazwaPelna)
					then &lt;fml:CI_NAZWA_PELNA>{ data($req/m:NazwaPelna) }&lt;/fml:CI_NAZWA_PELNA>
					else ()
			}
			{
				if($req/m:NrTelefonu)
					then &lt;fml:DC_NR_TELEFONU>{ data($req/m:NrTelefonu) }&lt;/fml:DC_NR_TELEFONU>
					else ()
			}
			{
				if($req/m:JednostkaKorporacyjna)
					then &lt;fml:DC_JEDNOSTKA_KORPORACYJNA>{ data($req/m:JednostkaKorporacyjna) }&lt;/fml:DC_JEDNOSTKA_KORPORACYJNA>
					else ()
			}
			{
				if($req/m:DataUrodzenia)
					then &lt;fml:DC_DATA_URODZENIA>{ data($req/m:DataUrodzenia) }&lt;/fml:DC_DATA_URODZENIA>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMProspASearchRequest($body/m:CRMProspASearchRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>