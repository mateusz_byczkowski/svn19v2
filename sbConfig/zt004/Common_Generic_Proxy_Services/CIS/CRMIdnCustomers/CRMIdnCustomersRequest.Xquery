<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMIdnCustomersRequest($req as element(m:CRMIdnCustomersRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				for $v in $req/m:NrDowoduRegon
				return
					&lt;fml:DC_NR_DOWODU_REGON>{ data($v) }&lt;/fml:DC_NR_DOWODU_REGON>
			}
			{
				for $v in $req/m:NrPesel
				return
					&lt;fml:DC_NR_PESEL>{ data($v) }&lt;/fml:DC_NR_PESEL>
			}
			{
				for $v in $req/m:Nip
				return
					&lt;fml:DC_NIP>{ data($v) }&lt;/fml:DC_NIP>
			}
			{
				for $v in $req/m:NumerPaszportu
				return
					&lt;fml:DC_NUMER_PASZPORTU>{ data($v) }&lt;/fml:DC_NUMER_PASZPORTU>
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMIdnCustomersRequest($body/m:CRMIdnCustomersRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>