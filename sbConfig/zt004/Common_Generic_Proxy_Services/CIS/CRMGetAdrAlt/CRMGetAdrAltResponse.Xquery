<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetAdrAltResponse($fml as element(fml:FML32))
	as element(m:CRMGetAdrAltResponse) {
		&lt;m:CRMGetAdrAltResponse>
			{
				let $CI_NUMER_PACZKI := $fml/fml:CI_NUMER_PACZKI return
					if($CI_NUMER_PACZKI)
						then &lt;m:NumerPaczki>{ data($CI_NUMER_PACZKI) }&lt;/m:NumerPaczki>
						else ()
			}
			{
         			let $CI_TYP_ADRESU_KORESP := $fml/fml:CI_TYP_ADRESU_KORESP
				let $DC_IMIE_I_NAZWISKO_ALT := $fml/fml:DC_IMIE_I_NAZWISKO_ALT
				let $DC_ULICA_ADRES_ALT := $fml/fml:DC_ULICA_ADRES_ALT
				let $DC_NR_POSES_LOKALU_ADRES_ALT := $fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT
				let $DC_MIASTO_ADRES_ALT := $fml/fml:DC_MIASTO_ADRES_ALT
				let $DC_KOD_POCZTOWY_ADRES_ALT := $fml/fml:DC_KOD_POCZTOWY_ADRES_ALT
				let $DC_WOJEWODZTWO_KRAJ_ADRES_ALT := $fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT
				let $DC_DATA_WPROWADZENIA := $fml/fml:DC_DATA_WPROWADZENIA
				let $CI_DATA_OBOWIAZYWANIA := $fml/fml:CI_DATA_OBOWIAZYWANIA
				for $it at $p in $fml/fml:DC_IMIE_I_NAZWISKO_ALT
				return
					&lt;m:CRMGetAdrAltAdresAlt>
					{
						if($CI_TYP_ADRESU_KORESP[$p])
							then &lt;m:TypAdresuKoresp>{ data($CI_TYP_ADRESU_KORESP[$p]) }&lt;/m:TypAdresuKoresp>
						else ()
					}
					{
						if($DC_IMIE_I_NAZWISKO_ALT[$p])
							then &lt;m:ImieINazwiskoAlt>{ data($DC_IMIE_I_NAZWISKO_ALT[$p]) }&lt;/m:ImieINazwiskoAlt>
						else ()
					}
					{
						if($DC_ULICA_ADRES_ALT[$p])
							then &lt;m:UlicaAdresAlt>{ data($DC_ULICA_ADRES_ALT[$p]) }&lt;/m:UlicaAdresAlt>
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_ADRES_ALT[$p])
							then &lt;m:NrPosesLokaluAdresAlt>{ data($DC_NR_POSES_LOKALU_ADRES_ALT[$p]) }&lt;/m:NrPosesLokaluAdresAlt>
						else ()
					}
					{
						if($DC_MIASTO_ADRES_ALT[$p])
							then &lt;m:MiastoAdresAlt>{ data($DC_MIASTO_ADRES_ALT[$p]) }&lt;/m:MiastoAdresAlt>
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_ADRES_ALT[$p])
							then &lt;m:KodPocztowyAdresAlt>{ data($DC_KOD_POCZTOWY_ADRES_ALT[$p]) }&lt;/m:KodPocztowyAdresAlt>
						else ()
					}
					{
						if($DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$p])
							then &lt;m:WojewodztwoKrajAdresAlt>{ data($DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$p]) }&lt;/m:WojewodztwoKrajAdresAlt>
						else ()
					}
					{
						if($DC_DATA_WPROWADZENIA[$p])
							then &lt;m:DataWprowadzenia>{ data($DC_DATA_WPROWADZENIA[$p]) }&lt;/m:DataWprowadzenia>
						else ()
					}
					{
						if($CI_DATA_OBOWIAZYWANIA[$p])
							then &lt;m:DataObowiazywania>{ data($CI_DATA_OBOWIAZYWANIA[$p]) }&lt;/m:DataObowiazywania>
						else ()
					}
					&lt;/m:CRMGetAdrAltAdresAlt>
			}

		&lt;/m:CRMGetAdrAltResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetAdrAltResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>