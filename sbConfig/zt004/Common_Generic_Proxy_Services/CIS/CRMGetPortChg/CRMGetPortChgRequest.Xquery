<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPortChgRequest($req as element(m:CRMGetPortChgRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:DataOd)
					then &lt;fml:CI_DATA_OD>{ data($req/m:DataOd) }&lt;/fml:CI_DATA_OD>
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER>{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI>{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI>
					else ()
			}
                        {
                                if($req/m:NumerPaczkiStr)
                                        then &lt;fml:CI_NUMER_PACZKI_STR>{ data($req/m:NumerPaczkiStr) }&lt;/fml:CI_NUMER_PACZKI_STR>
                                        else ()
                        }
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetPortChgRequest($body/m:CRMGetPortChgRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>