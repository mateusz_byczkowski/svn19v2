<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare variable $user_id as xs:string* external;
declare variable $branch_id as xs:string* external;
declare variable $product_type as xs:string* external;

 <wsdl:getFundProductListRequest xmlns:wsdl="http://jv.channel.cu.com.pl/cmf/wsdl-jv">
        <envelope>
            <request-time>{current-dateTime()}</request-time>
            <request-no?>{current-dateTime()}</request-no>
            <source-code>placowka banku</source-code>
            <user-id?>{$user_id}</user-id>
            <branch-id?>{$branch_id}</branch-id>
            (:<bar-code>string</bar-code>:)
        </envelope>
           <product-type>{$product_type}</product-type>
   </wsdl:getFundProductListRequest>]]></con:xquery>
</con:xqueryEntry>