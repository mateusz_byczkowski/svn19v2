<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductsAreaResponse ($bdy as element(m:PRIMEGetCustProductsAreaResponse ))
	as element(fml:FML32) {
	 &lt;FML32>
  
     {	
       
           for $req  in $bdy/m:PRIMEGetCustProductsAreaResult/m:Product
               return
                &lt;PART>
                    &lt;fml:DC_NR_RACHUNKU>{substring(data($req/m:DC_NR_RACHUNKU),2)}&lt;/fml:DC_NR_RACHUNKU>	
                {			
                if ($req/m:DC_NR_ALTERNATYWNY and string-length(data($req/m:DC_NR_ALTERNATYWNY))>0)
		            then 
                                &lt;fml:DC_NR_RACHUNKU_W_SYSTEMIE>{data($req/m:DC_NR_ALTERNATYWNY)}&lt;/fml:DC_NR_RACHUNKU_W_SYSTEMIE>
			    else 
			       &lt;fml:DC_NR_RACHUNKU_W_SYSTEMIE>X&lt;/fml:DC_NR_RACHUNKU_W_SYSTEMIE>
		}    
                {        
                if ($req/m:B_POJEDYNCZY)
		            then
        		        &lt;fml:B_POJEDYNCZY>{data($req/m:B_POJEDYNCZY)}&lt;/fml:B_POJEDYNCZY>
			     else 
			        &lt;fml:B_POJEDYNCZY>&lt;/fml:B_POJEDYNCZY>
		}
                {			
                if ($req/m:CI_PRODUCT_ATT1)
		            then
                               if(string-length(data($req/m:CI_PRODUCT_ATT1))&lt;50)
		                  then &lt;fml:CI_PRODUCT_ATT1>{concat(data($req/m:CI_PRODUCT_ATT1),'X')}&lt;/fml:CI_PRODUCT_ATT1>
                                  else  &lt;fml:CI_PRODUCT_ATT1>{data($req/m:CI_PRODUCT_ATT1)}&lt;/fml:CI_PRODUCT_ATT1>
			    else 
			      &lt;fml:CI_PRODUCT_ATT1>X&lt;/fml:CI_PRODUCT_ATT1>
		}
                {			
                if ($req/m:CI_PRODUCT_ATT2)
		            then
                               if(string-length(data($req/m:CI_PRODUCT_ATT2))&lt;50)
		                  then &lt;fml:CI_PRODUCT_ATT2>{concat(data($req/m:CI_PRODUCT_ATT2),'X')}&lt;/fml:CI_PRODUCT_ATT2>
                                  else  &lt;fml:CI_PRODUCT_ATT2>{data($req/m:CI_PRODUCT_ATT2)}&lt;/fml:CI_PRODUCT_ATT2>
			    else 
			      &lt;fml:CI_PRODUCT_ATT2>X&lt;/fml:CI_PRODUCT_ATT2>
		}
                {			
                if ($req/m:CI_PRODUCT_ATT3)
		            then
                               if(string-length(data($req/m:CI_PRODUCT_ATT3))&lt;50)
		                  then &lt;fml:CI_PRODUCT_ATT3>{concat(data($req/m:CI_PRODUCT_ATT3),'X')}&lt;/fml:CI_PRODUCT_ATT3>
                                  else  &lt;fml:CI_PRODUCT_ATT3>{data($req/m:CI_PRODUCT_ATT3)}&lt;/fml:CI_PRODUCT_ATT3>
			    else 
			      &lt;fml:CI_PRODUCT_ATT3>X&lt;/fml:CI_PRODUCT_ATT3>
		}
                {			
                if ($req/m:CI_PRODUCT_ATT4)
		            then
                               if(string-length(data($req/m:CI_PRODUCT_ATT4))&lt;50)
		                  then &lt;fml:CI_PRODUCT_ATT4>{concat(data($req/m:CI_PRODUCT_ATT4),'X')}&lt;/fml:CI_PRODUCT_ATT4>
                                  else  &lt;fml:CI_PRODUCT_ATT4>{data($req/m:CI_PRODUCT_ATT4)}&lt;/fml:CI_PRODUCT_ATT4>
			    else 
			      &lt;fml:CI_PRODUCT_ATT4>X&lt;/fml:CI_PRODUCT_ATT4>
		}
                {			
                if ($req/m:CI_PRODUCT_ATT5)
		            then
                               if(string-length(data($req/m:CI_PRODUCT_ATT5))&lt;50)
		                  then &lt;fml:CI_PRODUCT_ATT5>{concat(data($req/m:CI_PRODUCT_ATT5),'X')}&lt;/fml:CI_PRODUCT_ATT5>
                                  else  &lt;fml:CI_PRODUCT_ATT5>{data($req/m:CI_PRODUCT_ATT5)}&lt;/fml:CI_PRODUCT_ATT5>
			    else 
			      &lt;fml:CI_PRODUCT_ATT5>X&lt;/fml:CI_PRODUCT_ATT5>
		}
                {			
                if ($req/m:CI_RACHUNEK_ADR_ALT)
		            then
			       &lt;fml:CI_RACHUNEK_ADR_ALT>{data($req/m:CI_RACHUNEK_ADR_ALT)}&lt;/fml:CI_RACHUNEK_ADR_ALT>
			    else 
                               &lt;fml:CI_RACHUNEK_ADR_ALT>N&lt;/fml:CI_RACHUNEK_ADR_ALT>
		}
                { 			
                if ($req/m:B_SALDO)
		            then
				        &lt;fml:B_SALDO>{data($req/m:B_SALDO)}&lt;/fml:B_SALDO>
					else 
					    &lt;fml:B_SALDO>&lt;/fml:B_SALDO>
		}
                {			
                if ($req/m:B_BLOKADA)
		            then
				        &lt;fml:B_BLOKADA>{data($req/m:B_BLOKADA)}&lt;/fml:B_BLOKADA>
					else 
					    &lt;fml:B_BLOKADA>&lt;/fml:B_BLOKADA>
		}
                {			
                if ($req/m:B_DOST_SRODKI)
		            then
				        &lt;fml:B_DOST_SRODKI>{data($req/m:B_DOST_SRODKI)}&lt;/fml:B_DOST_SRODKI>
					else 
					    &lt;fml:B_DOST_SRODKI>&lt;/fml:B_DOST_SRODKI>
		}
                {
                if ($req/m:B_KOD_WALUTY)
		            then
                               if(string-length(data($req/m:B_KOD_WALUTY))&lt;3)
		                  then &lt;fml:B_KOD_WALUTY>{concat(data($req/m:B_KOD_WALUTY),'X')}&lt;/fml:B_KOD_WALUTY>
                                  else  &lt;fml:B_KOD_WALUTY>{data($req/m:B_KOD_WALUTY)}&lt;/fml:B_KOD_WALUTY>
			    else 
			      &lt;fml:B_KOD_WALUTY>X&lt;/fml:B_KOD_WALUTY>
		}
                {			
                if ($req/m:B_D_OTWARCIA)
		            then
				        &lt;fml:B_D_OTWARCIA>{data($req/m:B_D_OTWARCIA)}&lt;/fml:B_D_OTWARCIA>
					else 
					    &lt;fml:B_D_OTWARCIA>&lt;/fml:B_D_OTWARCIA>
		}
                {			
                if ($req/m:B_DATA_OST_OPER)
		            then
				        &lt;fml:B_DATA_OST_OPER>{data($req/m:B_DATA_OST_OPER)}&lt;/fml:B_DATA_OST_OPER>
					else 
					    &lt;fml:B_DATA_OST_OPER>&lt;/fml:B_DATA_OST_OPER>
		}
                {			
                if ($req/m:B_LIMIT1)
		            then
				        &lt;fml:B_LIMIT1>{data($req/m:B_LIMIT1)}&lt;/fml:B_LIMIT1>
					else 
					    &lt;fml:B_LIMIT1>&lt;/fml:B_LIMIT1>
		}
                {			
                if ($req/m:CI_PRODUCT_NAME_ORYGINAL)
		            then
				        &lt;fml:CI_PRODUCT_NAME_ORYGINAL>{data($req/m:CI_PRODUCT_NAME_ORYGINAL)}&lt;/fml:CI_PRODUCT_NAME_ORYGINAL>
					else 
					    &lt;fml:CI_PRODUCT_NAME_ORYGINAL>&lt;/fml:CI_PRODUCT_NAME_ORYGINAL>            		       
	        }
     
                &lt;/PART>
	 }     
                       
         &lt;/FML32>
               
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>{  if ($body/m:PRIMEGetCustProductsAreaResponse)
    then  xf:mapPRIMEGetCustProductsAreaResponse($body/m:PRIMEGetCustProductsAreaResponse)
    else  
 if ($body/fml:FML32)
    then $body/fml:FML32
    else ()
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>