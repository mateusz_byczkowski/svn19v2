<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cekemessages/";
declare namespace xf = "http://bzwbk.com/services/cekemappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeEventExtInRequest($req as element(m:eEventExtInRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:Type)
					then &lt;fml:EV_TYPE>{ data($req/m:Type) }&lt;/fml:EV_TYPE>
					else ()
			}
			{
				if($req/m:Key)
					then &lt;fml:EV_KEY>{ data($req/m:Key) }&lt;/fml:EV_KEY>
					else ()
			}
			{
				if($req/m:ChannelType)
					then &lt;fml:EV_CHANNEL_TYPE>{ data($req/m:ChannelType) }&lt;/fml:EV_CHANNEL_TYPE>
					else ()
			}
			{
				if($req/m:SendAddress)
					then &lt;fml:EV_SEND_ADDRESS>{ data($req/m:SendAddress) }&lt;/fml:EV_SEND_ADDRESS>
					else ()
			}
			{
				if($req/m:Content)
					then &lt;fml:EV_CONTENT>{ data($req/m:Content) }&lt;/fml:EV_CONTENT>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapeEventExtInRequest($body/m:eEventExtInRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>