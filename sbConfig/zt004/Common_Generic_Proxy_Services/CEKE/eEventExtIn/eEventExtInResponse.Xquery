<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cekemessages/";
declare namespace xf = "http://bzwbk.com/services/cekemappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeEventExtInResponse($fml as element(fml:FML32))
	as element(m:eEventExtInResponse) {
		&lt;m:eEventExtInResponse>
		&lt;/m:eEventExtInResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapeEventExtInResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>