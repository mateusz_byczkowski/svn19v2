<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/CEKEGetVersionmessages/";
declare namespace xf = "http://bzwbk.com/services/CEKEGetVersionmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCEKEGetVersionResponse($fml as element(fml:FML32))
	as element(m:CEKEGetVersionResponse) {
		&lt;m:CEKEGetVersionResponse>
			{
				if($fml/fml:E_REQUESTER_ID)
					then &lt;m:RequesterId>{ data($fml/fml:E_REQUESTER_ID) }&lt;/m:RequesterId>
					else ()
			}
			{
				if($fml/fml:E_ALLOWED_REQ_ID)
					then &lt;m:AllowedReqId>{ data($fml/fml:E_ALLOWED_REQ_ID) }&lt;/m:AllowedReqId>
					else ()
			}
		&lt;/m:CEKEGetVersionResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCEKEGetVersionResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>