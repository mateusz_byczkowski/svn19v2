<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/accowner/messages/";
declare namespace xf = "http://bzwbk.com/services/accowner/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetAccOwnerResponse($fml as element(fml:FML32))
	as element(m:GetAccOwnerResponse) {
		&lt;m:GetAccOwnerResponse>
			{
				if($fml/fml:B_NR_RACH_NRB)
					then &lt;m:NrRachNrb>{ data($fml/fml:B_NR_RACH_NRB) }&lt;/m:NrRachNrb>
					else ()
			}
			{
				if($fml/fml:B_KOD_WALUTY)
					then &lt;m:KodWaluty>{ data($fml/fml:B_KOD_WALUTY) }&lt;/m:KodWaluty>
					else ()
			}
			{
				if($fml/fml:B_RODZAJ_RACH)
					then &lt;m:RodzajRach>{ data($fml/fml:B_RODZAJ_RACH) }&lt;/m:RodzajRach>
					else ()
			}
			{
				if($fml/fml:B_KOD_KLIENTA)
					then &lt;m:KodKlienta>{ data($fml/fml:B_KOD_KLIENTA) }&lt;/m:KodKlienta>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KLIENTA)
					then &lt;m:NazwaKlienta>{ data($fml/fml:B_NAZWA_KLIENTA) }&lt;/m:NazwaKlienta>
					else ()
			}
			{
				if($fml/fml:B_UL_ZAM)
					then &lt;m:UlZam>{ data($fml/fml:B_UL_ZAM) }&lt;/m:UlZam>
					else ()
			}
			{
				if($fml/fml:B_M_ZAM)
					then &lt;m:MZam>{ data($fml/fml:B_M_ZAM) }&lt;/m:MZam>
					else ()
			}
			{
				if($fml/fml:B_MIKROODDZIAL)
					then &lt;m:Mikrooddzial>{ data($fml/fml:B_MIKROODDZIAL) }&lt;/m:Mikrooddzial>
					else ()
			}
			{
				if($fml/fml:B_KOD_POCZT)
					then &lt;m:KodPoczt>{ data($fml/fml:B_KOD_POCZT) }&lt;/m:KodPoczt>
					else ()
			}
			{
				if($fml/fml:B_TELEFON)
					then &lt;m:Telefon>{ data($fml/fml:B_TELEFON) }&lt;/m:Telefon>
					else ()
			}
			{
				if($fml/fml:B_EMAIL)
					then &lt;m:Email>{ data($fml/fml:B_EMAIL) }&lt;/m:Email>
					else ()
			}
			{
				if($fml/fml:B_SEKCJA)
					then &lt;m:Sekcja>{ data($fml/fml:B_SEKCJA) }&lt;/m:Sekcja>
					else ()
			}
			{
				if($fml/fml:B_PODMIOT)
					then &lt;m:Podmiot>{ data($fml/fml:B_PODMIOT) }&lt;/m:Podmiot>
					else ()
			}
			{
				if($fml/fml:B_WLASNOSC)
					then &lt;m:Wlasnosc>{ data($fml/fml:B_WLASNOSC) }&lt;/m:Wlasnosc>
					else ()
			}
			{
				if($fml/fml:B_REZYDENT)
					then &lt;m:Rezydent>{ data($fml/fml:B_REZYDENT) }&lt;/m:Rezydent>
					else ()
			}
			{
				if($fml/fml:B_KRAJ)
					then &lt;m:Kraj>{ data($fml/fml:B_KRAJ) }&lt;/m:Kraj>
					else ()
			}
			{
				if($fml/fml:B_N_EKD)
					then &lt;m:NEkd>{ data($fml/fml:B_N_EKD) }&lt;/m:NEkd>
					else ()
			}
			{
				if($fml/fml:B_NR_DOK)
					then &lt;m:NrDok>{ data($fml/fml:B_NR_DOK) }&lt;/m:NrDok>
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_AKT)
					then &lt;m:OdsSkapAkt>{ data($fml/fml:B_ODS_SKAP_AKT) }&lt;/m:OdsSkapAkt>
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_POP)
					then &lt;m:OdsSkapPop>{ data($fml/fml:B_ODS_SKAP_POP) }&lt;/m:OdsSkapPop>
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_WN_AKT)
					then &lt;m:OdsSkapWnAkt>{ data($fml/fml:B_ODS_SKAP_WN_AKT) }&lt;/m:OdsSkapWnAkt>
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_WN_POP)
					then &lt;m:OdsSkapWnPop>{ data($fml/fml:B_ODS_SKAP_WN_POP) }&lt;/m:OdsSkapWnPop>
					else ()
			}
			{
				if($fml/fml:B_MIKROODDZIAL_RACH)
					then &lt;m:MikrooddzialRach>{ data($fml/fml:B_MIKROODDZIAL_RACH) }&lt;/m:MikrooddzialRach>
					else ()
			}
			{
				if($fml/fml:B_D_OTWARCIA)
					then &lt;m:DOtwarcia>{ data($fml/fml:B_D_OTWARCIA) }&lt;/m:DOtwarcia>
					else ()
			}
			{
				if($fml/fml:B_SALDO)
					then &lt;m:Saldo>{ data($fml/fml:B_SALDO) }&lt;/m:Saldo>
					else ()
			}
			{
				if($fml/fml:B_DOST_SRODKI)
					then &lt;m:DostSrodki>{ data($fml/fml:B_DOST_SRODKI) }&lt;/m:DostSrodki>
					else ()
			}
			{
				if($fml/fml:B_PRODUKT)
					then &lt;m:Produkt>{ data($fml/fml:B_PRODUKT) }&lt;/m:Produkt>
					else ()
			}

		&lt;/m:GetAccOwnerResponse>
};

declare variable $body as element(soap-env:Body) external;
 xf:mapGetAccOwnerResponse($body/fml:FML32)</con:xquery>
</con:xqueryEntry>