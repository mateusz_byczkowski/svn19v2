<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapupdateTransactionTypeRequest($req as element(m:updateTransactionType))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/userDescription/microbranch)
					then &lt;fml:E_MICRO_BRANCH>{ data($req/userDescription/microbranch) }&lt;/fml:E_MICRO_BRANCH>
					else ()
			}
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME>{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/transactionType/currency)
					then &lt;fml:E_TRN_TYPE_CURRENCY>{ data($req/transactionType/currency) }&lt;/fml:E_TRN_TYPE_CURRENCY>
					else ()
			}
			{
				if($req/transactionType/title)
					then &lt;fml:E_TRN_TITLE>{ data($req/transactionType/title) }&lt;/fml:E_TRN_TITLE>
					else ()
			}
			{
				if($req/transactionType/name)
					then &lt;fml:E_TRN_TYPE_NAME>{ data($req/transactionType/name) }&lt;/fml:E_TRN_TYPE_NAME>
					else ()
			}
			{
				if($req/transactionType/closeDate)
					then &lt;fml:E_TRN_TYPE_CLOSE_DATE>{ data($req/transactionType/closeDate) }&lt;/fml:E_TRN_TYPE_CLOSE_DATE>
					else ()
			}
			{
				if($req/transactionType/maxAmount)
					then &lt;fml:E_TRN_TYPE_MAX_AMOUNT>{ data($req/transactionType/maxAmount) }&lt;/fml:E_TRN_TYPE_MAX_AMOUNT>
					else ()
			}
			{
				if($req/transactionType/defaultChannelAllowance)
					then &lt;fml:E_DEF_CHANNEL_ALLOW>{ data($req/transactionType/defaultChannelAllowance) }&lt;/fml:E_DEF_CHANNEL_ALLOW>
					else ()
			}
			{
				if($req/transactionType/defaultTransferType)
					then &lt;fml:E_TRANSFER_TYPE>{ data($req/transactionType/defaultTransferType) }&lt;/fml:E_TRANSFER_TYPE>
					else ()
			}
			{
				if ($req/transactionType/productType) 
					then if (fn:string($req/transactionType/productType) != '')
						then &lt;fml:B_RODZAJ_RACH>{ data($req/transactionType/productType) }&lt;/fml:B_RODZAJ_RACH>
						else &lt;fml:B_RODZAJ_RACH nil="true"/>

					else ()
			}
			{
				if($req/transactionType/allowedSecurity)
					then &lt;fml:E_ALLOWED_SECURITY_LEVEL>{ data($req/transactionType/allowedSecurity) }&lt;/fml:E_ALLOWED_SECURITY_LEVEL>
					else ()
			}
			{
				if($req/transactionType/securityLevel)
					then &lt;fml:E_SECURITY_LEVEL>{ data($req/transactionType/securityLevel) }&lt;/fml:E_SECURITY_LEVEL>
					else ()
			}
			{
				if($req/transactionType/options)
					then &lt;fml:E_TRN_TYPE_OPTIONS>{ data($req/transactionType/options) }&lt;/fml:E_TRN_TYPE_OPTIONS>
					else ()
			}
			{
				if($req/transactionType/transactionTypeId)
					then &lt;fml:E_TRN_TYPE>{ data($req/transactionType/transactionTypeId) }&lt;/fml:E_TRN_TYPE>
					else ()
			}
			{
				if($req/transactionType/productMask)
					then &lt;fml:E_PRODUCT_ACTION_MASK>{ data($req/transactionType/productMask) }&lt;/fml:E_PRODUCT_ACTION_MASK>
					else ()
			}
			{
				if($req/transactionType/transactionMaxForwardDays)
					then &lt;fml:E_TRN_TYPE_MAX_FRWD_DAYS>{ data($req/transactionType/transactionMaxForwardDays) }&lt;/fml:E_TRN_TYPE_MAX_FRWD_DAYS>
					else ()
			}
			{
				if($req/transactionType/allowedTransferTypes)
					then &lt;fml:E_ALLOWED_TRANSFER_TYPES>{ data($req/transactionType/allowedTransferTypes) }&lt;/fml:E_ALLOWED_TRANSFER_TYPES>
				else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapupdateTransactionTypeRequest($body/m:updateTransactionType) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>