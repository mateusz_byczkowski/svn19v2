<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare namespace fml = "";

declare function xf:mapgetFirstTransactionTypesResponse($fml as element(fml:FML32))
	as element(m:getFirstTransactionTypesResponse) {
		&lt;m:getFirstTransactionTypesResponse xmlns:m="http://bzwbk.com/services/ppl/messages">
			&lt;getFirstTransactionTypesReturn xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="soapenc:Array" soapenc:arrayType="m:TransactionType[0]" xmlns="">
			{

				let $E_TRN_TYPE_CURRENCY := $fml/fml:E_TRN_TYPE_CURRENCY
				let $E_TRN_TITLE := $fml/fml:E_TRN_TITLE
				let $E_TRN_TYPE_NAME := $fml/fml:E_TRN_TYPE_NAME
				let $E_TRN_TYPE_CLOSE_DATE := $fml/fml:E_TRN_TYPE_CLOSE_DATE
				let $E_TRN_TYPE_MAX_AMOUNT := $fml/fml:E_TRN_TYPE_MAX_AMOUNT
				let $E_DEF_CHANNEL_ALLOW := $fml/fml:E_DEF_CHANNEL_ALLOW
				let $E_TRANSFER_TYPE := $fml/fml:E_TRANSFER_TYPE
				let $B_RODZAJ_RACH := $fml/fml:B_RODZAJ_RACH
				let $E_ALLOWED_SECURITY_LEVEL := $fml/fml:E_ALLOWED_SECURITY_LEVEL
				let $E_SECURITY_LEVEL := $fml/fml:E_SECURITY_LEVEL
				let $E_TRN_TYPE_OPTIONS := $fml/fml:E_TRN_TYPE_OPTIONS
				let $E_TRN_TYPE := $fml/fml:E_TRN_TYPE
				let $E_PRODUCT_ACTION_MASK := $fml/fml:E_PRODUCT_ACTION_MASK
				let $E_TRN_TYPE_MAX_FRWD_DAYS := $fml/fml:E_TRN_TYPE_MAX_FRWD_DAYS
				let $E_ALLOWED_TRANSFER_TYPES := $fml/fml:E_ALLOWED_TRANSFER_TYPES
				for $it at $p in $fml/fml:E_TRN_TYPE
				return
					&lt;item>
					{
						if($E_TRN_TYPE_CURRENCY[$p])
							then &lt;currency>{ data($E_TRN_TYPE_CURRENCY[$p]) }&lt;/currency>
						else ()
					}
					{
						if($E_TRN_TITLE[$p])
							then &lt;title>{ data($E_TRN_TITLE[$p]) }&lt;/title>
						else ()
					}
					{
						if($E_TRN_TYPE_NAME[$p])
							then &lt;name>{ data($E_TRN_TYPE_NAME[$p]) }&lt;/name>
						else ()
					}
					{
						if($E_TRN_TYPE_CLOSE_DATE[$p])
							then &lt;closeDate>{ data($E_TRN_TYPE_CLOSE_DATE[$p]) }&lt;/closeDate>
						else ()
					}
					{
						if($E_TRN_TYPE_MAX_AMOUNT[$p])
							then &lt;maxAmount>{ data($E_TRN_TYPE_MAX_AMOUNT[$p]) }&lt;/maxAmount>
						else ()
					}
					{
						if($E_DEF_CHANNEL_ALLOW[$p])
							then &lt;defaultChannelAllowance>{ data($E_DEF_CHANNEL_ALLOW[$p]) }&lt;/defaultChannelAllowance>
						else ()
					}
					{
						if($E_TRANSFER_TYPE[$p])
							then &lt;defaultTransferType>{ data($E_TRANSFER_TYPE[$p]) }&lt;/defaultTransferType>
						else ()
					}
					{
						if($B_RODZAJ_RACH[$p])
							then &lt;productType>{ data($B_RODZAJ_RACH[$p]) }&lt;/productType>
						else ()
					}
					{
						if($E_ALLOWED_SECURITY_LEVEL[$p])
							then &lt;allowedSecurity>{ data($E_ALLOWED_SECURITY_LEVEL[$p]) }&lt;/allowedSecurity>
						else ()
					}
					{
						if($E_SECURITY_LEVEL[$p])
							then &lt;securityLevel>{ data($E_SECURITY_LEVEL[$p]) }&lt;/securityLevel>
						else ()
					}
					{
						if($E_TRN_TYPE_OPTIONS[$p])
							then &lt;options>{ data($E_TRN_TYPE_OPTIONS[$p]) }&lt;/options>
						else ()
					}
					{
						if($E_TRN_TYPE[$p])
							then &lt;transactionTypeId>{ data($E_TRN_TYPE[$p]) }&lt;/transactionTypeId>
						else ()
					}
					{
						if($E_PRODUCT_ACTION_MASK[$p])
							then &lt;productMask>{ data($E_PRODUCT_ACTION_MASK[$p]) }&lt;/productMask>
						else ()
					}
					{
						if($E_TRN_TYPE_MAX_FRWD_DAYS[$p])
							then &lt;transactionMaxForwardDays>{ data($E_TRN_TYPE_MAX_FRWD_DAYS[$p]) }&lt;/transactionMaxForwardDays>
							else ()
					}

					{
						if($E_ALLOWED_TRANSFER_TYPES[$p])
							then &lt;allowedTransferTypes>{ data($E_ALLOWED_TRANSFER_TYPES[$p]) }&lt;/allowedTransferTypes>
							else ()
					}
					&lt;/item>
			}
			&lt;/getFirstTransactionTypesReturn>
		&lt;/m:getFirstTransactionTypesResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetFirstTransactionTypesResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>