<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetLatestContractTemplateRequest($req as element(m:getLatestContractTemplate))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/type)
					then &lt;fml:E_DOC_TEMPL_TYPE>{ data($req/type) }&lt;/fml:E_DOC_TEMPL_TYPE>
					else ()
			}
			{
				&lt;fml:E_GET_OPTIONS>1&lt;/fml:E_GET_OPTIONS>				
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetLatestContractTemplateRequest($body/m:getLatestContractTemplate) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>