<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetProductsRangeResponse($fml as element(fml:FML32))
	as element(m:getProductsRangeResponse) {
		&lt;m:getProductsRangeResponse xmlns:m="http://bzwbk.com/services/ppl/messages">
		&lt;getProductsRangeReturn xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="soapenc:Array" soapenc:arrayType="m:Product[0]">
			{

				let $B_KOD_USLUGI := $fml/fml:B_KOD_USLUGI
				let $B_PLAN_OPLAT := $fml/fml:B_PLAN_OPLAT
				let $B_IRODZAJ_RACH := $fml/fml:B_IRODZAJ_RACH
				let $B_ITYP := $fml/fml:B_ITYP
				let $B_NAZWA := $fml/fml:B_NAZWA
				let $B_OPIS_RACH := $fml/fml:B_OPIS_RACH
				let $B_SKROT_OPISU := $fml/fml:B_SKROT_OPISU
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				let $B_RODZAJ_RACH := $fml/fml:B_RODZAJ_RACH
				let $B_OSOBA := $fml/fml:B_OSOBA
				let $B_OPCJE_EL := $fml/fml:B_OPCJE_EL
				let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
				let $B_OPCJA := $fml/fml:B_OPCJA
				let $B_OKR_WKLADU := $fml/fml:B_OKR_WKLADU
				let $B_JDN_OKR_WKLADU := $fml/fml:B_JDN_OKR_WKLADU
				for $it at $p in $fml/fml:B_KOD_USLUGI
				return
					&lt;item>
					{
						if($B_KOD_USLUGI[$p])
							then &lt;serviceCode>{ data($B_KOD_USLUGI[$p]) }&lt;/serviceCode>
						else ()
					}
					{
						if($B_PLAN_OPLAT[$p])
							then &lt;paymentPlan>{ data($B_PLAN_OPLAT[$p]) }&lt;/paymentPlan>
						else ()
					}
					{
						if($B_IRODZAJ_RACH[$p])
							then &lt;productKindNo>{ data($B_IRODZAJ_RACH[$p]) }&lt;/productKindNo>
						else ()
					}
					{
						if($B_ITYP[$p])
							then &lt;subtype>{ data($B_ITYP[$p]) }&lt;/subtype>
						else ()
					}
					{
						if($B_NAZWA[$p])
							then &lt;name>{ data($B_NAZWA[$p]) }&lt;/name>
						else ()
					}
					{
						if($B_OPIS_RACH[$p])
							then &lt;description>{ data($B_OPIS_RACH[$p]) }&lt;/description>
						else ()
					}
					{
						if($B_SKROT_OPISU[$p])
							then &lt;shortDescription>{ data($B_SKROT_OPISU[$p]) }&lt;/shortDescription>
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then &lt;accountTypeId>{ data($B_TYP_RACH[$p]) }&lt;/accountTypeId>
						else ()
					}
					{
						if($B_RODZAJ_RACH[$p])
							then &lt;productKind>{ data($B_RODZAJ_RACH[$p]) }&lt;/productKind>
						else ()
					}
					{
						if($B_OSOBA[$p])
							then &lt;customerType>{ data($B_OSOBA[$p]) }&lt;/customerType>
						else ()
					}
					{
						if($B_OPCJE_EL[$p])
							then &lt;elOption>{ data($B_OPCJE_EL[$p]) }&lt;/elOption>
						else ()
					}
					{
						if($B_KOD_WALUTY[$p])
							then &lt;currency>{ data($B_KOD_WALUTY[$p]) }&lt;/currency>
						else ()
					}
					{
						if($B_OPCJA[$p])
							then &lt;option>{ data($B_OPCJA[$p]) }&lt;/option>
						else ()
					}
					{
						if($B_OKR_WKLADU[$p])
							then &lt;period>{ data($B_OKR_WKLADU[$p]) }&lt;/period>
						else ()
					}
					{
						if($B_JDN_OKR_WKLADU[$p])
							then &lt;unit>{ data($B_JDN_OKR_WKLADU[$p]) }&lt;/unit>
						else ()
					}
					&lt;/item>
			}
		&lt;/getProductsRangeReturn>
		&lt;/m:getProductsRangeResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetProductsRangeResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>