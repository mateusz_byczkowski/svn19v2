<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetPopularAccountsRangeResponse($fml as element(fml:FML32))
	as element(m:getPopularAccountsRangeResponse) {
		&lt;m:getPopularAccountsRangeResponse xmlns:m="http://bzwbk.com/services/ppl/messages">
		&lt;getPopularAccountsRangeReturn xsi:type="soapenc:Array" soapenc:arrayType="m:PopularAccount[0]" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">

			{

				let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
				let $E_POPACC_INFO := $fml/fml:E_POPACC_INFO
				let $E_VALID_TO := $fml/fml:E_VALID_TO
				let $E_VALID_FROM := $fml/fml:E_VALID_FROM
				let $E_TRN_TITLE := $fml/fml:E_TRN_TITLE
				let $E_BRANCH_CODE := $fml/fml:E_BRANCH_CODE
				let $B_WALUTA := $fml/fml:B_WALUTA
				let $B_KOD_POCZT := $fml/fml:B_KOD_POCZT
				let $B_M_ZAM := $fml/fml:B_M_ZAM
				let $B_UL_ZAM := $fml/fml:B_UL_ZAM
				let $B_NAZWA_KLIENTA := $fml/fml:B_NAZWA_KLIENTA
				let $E_TRANSFER_TYPE := $fml/fml:E_TRANSFER_TYPE
				let $E_POPACC_OPTIONS := $fml/fml:E_POPACC_OPTIONS
				let $E_TIME_STAMP := $fml/fml:E_TIME_STAMP
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				for $it at $p in $fml/fml:B_DL_NR_RACH
				return
					&lt;item>
					{
						if($B_DL_NR_RACH[$p])
							then &lt;accountNo>{ data($B_DL_NR_RACH[$p]) }&lt;/accountNo>
						else ()
					}
					{
						if($E_POPACC_INFO[$p])
							then &lt;info>{ data($E_POPACC_INFO[$p]) }&lt;/info>
						else ()
					}
					{
						if($E_VALID_TO[$p])
							then &lt;validTo>{ data($E_VALID_TO[$p]) }&lt;/validTo>
						else ()
					}
					{
						if($E_VALID_FROM[$p])
							then &lt;validFrom>{ data($E_VALID_FROM[$p]) }&lt;/validFrom>
						else ()
					}
					{
						if($E_TRN_TITLE[$p])
							then &lt;description>{ data($E_TRN_TITLE[$p]) }&lt;/description>
						else ()
					}
					{
						if($E_BRANCH_CODE[$p])
							then &lt;branchCode>{ data($E_BRANCH_CODE[$p]) }&lt;/branchCode>
						else ()
					}
					{
						if($B_WALUTA[$p])
							then &lt;currency>{ data($B_WALUTA[$p]) }&lt;/currency>
						else ()
					}
					{
						if($B_KOD_POCZT[$p])
							then &lt;zipCode>{ data($B_KOD_POCZT[$p]) }&lt;/zipCode>
						else ()
					}
					{
						if($B_M_ZAM[$p])
							then &lt;city>{ data($B_M_ZAM[$p]) }&lt;/city>
						else ()
					}
					{
						if($B_UL_ZAM[$p])
							then &lt;street>{ data($B_UL_ZAM[$p]) }&lt;/street>
						else ()
					}
					{
						if($B_NAZWA_KLIENTA[$p])
							then &lt;name>{ data($B_NAZWA_KLIENTA[$p]) }&lt;/name>
						else ()
					}
					{
						if($E_TRANSFER_TYPE[$p])
							then &lt;transferType>{ data($E_TRANSFER_TYPE[$p]) }&lt;/transferType>
						else ()
					}
					{
						if($E_POPACC_OPTIONS[$p])
							then &lt;options>{ data($E_POPACC_OPTIONS[$p]) }&lt;/options>
						else ()
					}
					{
						if($E_TIME_STAMP[$p])
							then &lt;timestamp>{ data($E_TIME_STAMP[$p]) }&lt;/timestamp>
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then &lt;accountTypeId>{ data($B_TYP_RACH[$p]) }&lt;/accountTypeId>
						else ()
					}
					&lt;/item>
			}
		&lt;/getPopularAccountsRangeReturn>
		&lt;/m:getPopularAccountsRangeResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetPopularAccountsRangeResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>