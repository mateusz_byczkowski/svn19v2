<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetShopResponse($fml as element(fml:FML32))
	as element(m:getShopResponse) {
		&lt;m:getShopResponse>
		&lt;getShopReturn>
			{
				if($fml/fml:E_SHOP_DESC)
					then &lt;shortName>{ data($fml/fml:E_SHOP_DESC) }&lt;/shortName>
					else ()
			}
			{
				if($fml/fml:E_CUST_EMAIL)
					then &lt;email>{ data($fml/fml:E_CUST_EMAIL) }&lt;/email>
					else ()
			}
			{
				if($fml/fml:E_CUST_TELNO)
					then &lt;telephoneNo>{ data($fml/fml:E_CUST_TELNO) }&lt;/telephoneNo>
					else ()
			}
			{
				if($fml/fml:E_CUST_STREET)
					then &lt;street>{ data($fml/fml:E_CUST_STREET) }&lt;/street>
					else ()
			}
			{
				if($fml/fml:E_CUST_ZIPCODE)
					then &lt;zipCode>{ data($fml/fml:E_CUST_ZIPCODE) }&lt;/zipCode>
					else ()
			}
			{
				if($fml/fml:E_CUST_CITY)
					then &lt;city>{ data($fml/fml:E_CUST_CITY) }&lt;/city>
					else ()
			}
			{
				if($fml/fml:E_CUST_NAME)
					then &lt;name>{ data($fml/fml:E_CUST_NAME) }&lt;/name>
					else ()
			}
			{
				if($fml/fml:E_REP_NAME)
					then &lt;repName>{ data($fml/fml:E_REP_NAME) }&lt;/repName>
					else ()
			}
			{
				if($fml/fml:E_CUST_GSMNO)
					then &lt;gsmNo>{ data($fml/fml:E_CUST_GSMNO) }&lt;/gsmNo>
					else ()
			}
			{
				if($fml/fml:E_CUST_FAXNO)
					then &lt;faxNo>{ data($fml/fml:E_CUST_FAXNO) }&lt;/faxNo>
					else ()
			}
			{
				if($fml/fml:B_BIN_KARTY)
					then &lt;cardBin>{ data($fml/fml:B_BIN_KARTY) }&lt;/cardBin>
					else ()
			}
			{
				if($fml/fml:B_DL_NR_RACH)
					then &lt;accountNo>{ data($fml/fml:B_DL_NR_RACH) }&lt;/accountNo>
					else ()
			}
			{
				if($fml/fml:B_NR_DOK)
					then &lt;regon>{ data($fml/fml:B_NR_DOK) }&lt;/regon>
					else ()
			}
			{
				if($fml/fml:E_STATUS)
					then &lt;status>{ data($fml/fml:E_STATUS) }&lt;/status>
					else ()
			}
			{
				if($fml/fml:E_SHOP_ID)
					then &lt;shopId>{ data($fml/fml:E_SHOP_ID) }&lt;/shopId>
					else ()
			}
			{
				if($fml/fml:E_CUST_OPTIONS)
					then &lt;options>{ data($fml/fml:E_CUST_OPTIONS) }&lt;/options>
					else ()
			}
			{
				if($fml/fml:E_TIME_STAMP)
					then &lt;timestamp>{ data($fml/fml:E_TIME_STAMP) }&lt;/timestamp>
					else ()
			}
			{
				if($fml/fml:E_TRN_TYPE)
					then &lt;transactionTypeId>{ data($fml/fml:E_TRN_TYPE) }&lt;/transactionTypeId>
					else ()
			}
		&lt;/getShopReturn>
		&lt;/m:getShopResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetShopResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>