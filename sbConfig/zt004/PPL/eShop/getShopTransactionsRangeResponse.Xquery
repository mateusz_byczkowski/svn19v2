<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetShopTransactionsRangeResponse($fml as element(fml:FML32))
	as element(m:getShopTransactionsRangeResponse) {
		&lt;m:getShopTransactionsRangeResponse xmlns:m="http://bzwbk.com/services/ppl/messages" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		&lt;getShopTransactionsRangeReturn xsi:type="soapenc:Array" soapenc:arrayType="m:ShopTransaction[0]">
			{

				let $E_SHOP_TRANS_ID := $fml/fml:E_SHOP_TRANS_ID
				let $E_TRN_DATE := $fml/fml:E_TRN_DATE
				let $E_TRN_ID := $fml/fml:E_TRN_ID
				let $E_SHOP_ID := $fml/fml:E_SHOP_ID
				let $E_TRN_STATE := $fml/fml:E_TRN_STATE
				let $E_SEND_TIME := $fml/fml:E_SEND_TIME
				for $it at $p in $fml/fml:E_SHOP_TRANS_ID
				return
					&lt;item>
					{
						if($E_SHOP_TRANS_ID[$p])
							then &lt;shopTransactionId>{ data($E_SHOP_TRANS_ID[$p]) }&lt;/shopTransactionId>
						else ()
					}
					{
						if($E_TRN_DATE[$p])
							then &lt;date>{ data($E_TRN_DATE[$p]) }&lt;/date>
						else ()
					}
					{
						if($E_TRN_ID[$p])
							then &lt;transactionId>{ data($E_TRN_ID[$p]) }&lt;/transactionId>
						else ()
					}
					{
						if($E_SHOP_ID[$p])
							then &lt;shopId>{ data($E_SHOP_ID[$p]) }&lt;/shopId>
						else ()
					}
					{
						if($E_TRN_STATE[$p])
							then &lt;state>{ data($E_TRN_STATE[$p]) }&lt;/state>
						else ()
					}
					{
						if($E_SEND_TIME[$p])
							then &lt;sendTime>{ data($E_SEND_TIME[$p]) }&lt;/sendTime>
						else ()
					}
					&lt;/item>
			}
		&lt;/getShopTransactionsRangeReturn>
		&lt;/m:getShopTransactionsRangeResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetShopTransactionsRangeResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>