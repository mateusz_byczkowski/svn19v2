<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetFirstAccountTypesResponse($fml as element(fml:FML32))
	as element(m:getFirstAccountTypesResponse) {
		&lt;m:getFirstAccountTypesResponse xmlns:m="http://bzwbk.com/services/ppl/messages">
			&lt;getFirstAccountTypesReturn xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="soapenc:Array" soapenc:arrayType="m:AccountType[0]">
			{

				let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
				let $E_DEF_CHANNEL_ALLOW := $fml/fml:E_DEF_CHANNEL_ALLOW
				let $B_RODZAJ_RACH := $fml/fml:B_RODZAJ_RACH
				let $E_CR_TRN_MASK := $fml/fml:E_CR_TRN_MASK
				let $E_DR_TRN_MASK := $fml/fml:E_DR_TRN_MASK
				let $E_ACCOUNT_TYPE_OPTIONS := $fml/fml:E_ACCOUNT_TYPE_OPTIONS
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				for $it at $p in $fml/fml:B_TYP_RACH
				return
					&lt;item>
					{
						if($E_ACCOUNT_TYPE_NAME[$p])
							then &lt;name>{ data($E_ACCOUNT_TYPE_NAME[$p]) }&lt;/name>
						else ()
					}
					{
						if($E_DEF_CHANNEL_ALLOW[$p])
							then &lt;defaultChannelAllowance>{ data($E_DEF_CHANNEL_ALLOW[$p]) }&lt;/defaultChannelAllowance>
						else ()
					}
					{
						if($B_RODZAJ_RACH[$p])
							then &lt;productType>{ data($B_RODZAJ_RACH[$p]) }&lt;/productType>
						else ()
					}
					{
						if($E_CR_TRN_MASK[$p])
							then &lt;creditTransactionMask>{ data($E_CR_TRN_MASK[$p]) }&lt;/creditTransactionMask>
						else ()
					}
					{
						if($E_DR_TRN_MASK[$p])
							then &lt;debitTransactionMask>{ data($E_DR_TRN_MASK[$p]) }&lt;/debitTransactionMask>
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_OPTIONS[$p])
							then &lt;options>{ data($E_ACCOUNT_TYPE_OPTIONS[$p]) }&lt;/options>
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then &lt;accountTypeId>{ data($B_TYP_RACH[$p]) }&lt;/accountTypeId>
						else ()
					}
					&lt;/item>
			}
		&lt;/getFirstAccountTypesReturn>
		&lt;/m:getFirstAccountTypesResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetFirstAccountTypesResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>