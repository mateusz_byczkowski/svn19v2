<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetAccountTypesRangeRequest($req as element(m:getAccountTypesRange))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/accountsNo)
					then &lt;fml:E_REC_COUNT>{ data($req/accountsNo) }&lt;/fml:E_REC_COUNT>
					else ()
			}
			{
				if($req/userDescription/microbranch)
					then &lt;fml:E_MICRO_BRANCH>{ data($req/userDescription/microbranch) }&lt;/fml:E_MICRO_BRANCH>
					else ()
			}
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME>{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/startAccountTypeId)
					then &lt;fml:B_TYP_RACH>{ data($req/startAccountTypeId) }&lt;/fml:B_TYP_RACH>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetAccountTypesRangeRequest($body/m:getAccountTypesRange) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>