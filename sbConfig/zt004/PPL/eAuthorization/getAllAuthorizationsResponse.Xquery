<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetAllAuthorizationsResponse($fml as element(fml:FML32))
	as element(m:getAllAuthorizationsResponse) {
		&lt;m:getAllAuthorizationsResponse xmlns:m="http://bzwbk.com/services/ppl/messages">
		&lt;getAllAuthorizationsReturn xsi:type="soapenc:Array" soapenc:arrayType="m:Authorization[0]" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
			{

				let $E_LOGIN_ID := $fml/fml:E_LOGIN_ID
				let $E_CUST_TELNO := $fml/fml:E_CUST_TELNO
				let $E_LAST_LOGIN := $fml/fml:E_LAST_LOGIN
				let $E_AGENT_NAME := $fml/fml:E_AGENT_NAME
				for $it at $p in $fml/fml:E_LOGIN_ID
				return
					&lt;item>
					{
						if($E_LOGIN_ID[$p])
							then &lt;nik>{ data($E_LOGIN_ID[$p]) }&lt;/nik>
						else ()
					}
					{
						if($E_CUST_TELNO[$p])
							then &lt;customerTelephone>{ data($E_CUST_TELNO[$p]) }&lt;/customerTelephone>
						else ()
					}
					{
						if($E_LAST_LOGIN[$p])
							then &lt;lastLogin>{ data($E_LAST_LOGIN[$p]) }&lt;/lastLogin>
						else ()
					}
					{
						if($E_AGENT_NAME[$p])
							then &lt;agentName>{ data($E_AGENT_NAME[$p]) }&lt;/agentName>
						else ()
					}
					&lt;/item>
			}
		&lt;/getAllAuthorizationsReturn>
		&lt;/m:getAllAuthorizationsResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetAllAuthorizationsResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>