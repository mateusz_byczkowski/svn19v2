<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace flt="http://bzwbk.com/services/ppl/faults";
declare variable $fault external;

declare function local:fault($faultString as xs:string, $message as xs:string, $detail as xs:anyType) as element(soap-env:Fault) {
		&lt;soap-env:Fault>
			&lt;faultcode>{ $faultString }&lt;/faultcode> 
			&lt;faultstring>{ $message }&lt;/faultstring> 
			&lt;detail>{ $detail }&lt;/detail>
		&lt;/soap-env:Fault>
};


declare function local:errors($message as xs:string) as element()* {
	&lt;message>{ $message }&lt;/message>
};

declare function local:codedErrors($message as xs:string, $errorCode1 as xs:string, $errorCode2 as xs:string) as element()* {
	&lt;primaryErrorCode>{ $errorCode1 }&lt;/primaryErrorCode>,
	&lt;secondaryErrorCode>{ $errorCode2 }&lt;/secondaryErrorCode>,
	&lt;message>{ $message }&lt;/message>
};

&lt;soap-env:Body xmlns:flt="http://bzwbk.com/services/ppl/faults">
{
	let $message := $fault/ctx:reason
	let $reason := fn:substring-before(fn:substring-after(fn:substring-before($fault/ctx:reason, ":"), "("), ")")
	let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($fault/ctx:reason, ":"), ":"), ":")
	return
		if($reason = "13") then
			local:fault("flt:CriticalSecurityOperationException", $message, element flt:CriticalSecurityOperationException{ local:errors($message) })
		else if($reason = "11") then
			if($urcode = "116") then
				local:fault("flt:PasswordExpiredException", $message, element flt:PasswordExpiredException { local:codedErrors($message, $reason, $urcode) })
			else
				local:fault("flt:GeneralDecodedSecurityOperationException", $message, element flt:GeneralDecodedSecurityOperationException{ local:codedErrors($message, $reason, $urcode) })
		else
			local:fault("flt:CriticalSecurityOperationException", $message, element flt:CriticalSecurityOperationException{ local:errors($message) })
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>