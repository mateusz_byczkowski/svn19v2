<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetProdRelResponse($fml as element(fml:FML32))
	as element(m:CRMGetProdRelResponse) {
		&lt;m:CRMGetProdRelResponse>
			{
				if($fml/fml:NF_PAGEC_PAGESIZE)
					then &lt;m:PagecPagesize>{ data($fml/fml:NF_PAGEC_PAGESIZE) }&lt;/m:PagecPagesize>
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
					then &lt;m:PagecNavigationkeyvalu>{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/m:PagecNavigationkeyvalu>
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_HASNEXT)
					then &lt;m:PagecHasnext>{ data($fml/fml:NF_PAGEC_HASNEXT) }&lt;/m:PagecHasnext>
					else ()
			}
			{

				let $NF_CUSTOM_CUSTOMERNUMBER := $fml/fml:NF_CUSTOM_CUSTOMERNUMBER
				let $NF_CUSTOT_CUSTOMERTYPE := $fml/fml:NF_CUSTOT_CUSTOMERTYPE
				let $NF_CUSTOP_FIRSTNAME := $fml/fml:NF_CUSTOP_FIRSTNAME
				let $NF_CUSTOP_LASTNAME := $fml/fml:NF_CUSTOP_LASTNAME
				let $NF_CUSTOM_COMPANYNAME := $fml/fml:NF_CUSTOM_COMPANYNAME
				let $NF_CUSTAR_CUSTOMERACCOUNTR := $fml/fml:NF_CUSTAR_CUSTOMERACCOUNTR
				let $NF_TAX_TAXPERCENTAGE := $fml/fml:NF_TAX_TAXPERCENTAGE
				for $it at $p in $fml/fml:NF_CUSTOM_CUSTOMERNUMBER
				return
					&lt;m:Customer>
					{
						if($NF_CUSTOM_CUSTOMERNUMBER[$p])
							then &lt;m:CustomCustomernumber>{ data($NF_CUSTOM_CUSTOMERNUMBER[$p]) }&lt;/m:CustomCustomernumber>
						else ()
					}
					{
						if($NF_CUSTOT_CUSTOMERTYPE[$p] = "P")
							then &lt;m:CustotCustomertype>F&lt;/m:CustotCustomertype>
						else if($NF_CUSTOT_CUSTOMERTYPE[$p] = "N")
							then &lt;m:CustotCustomertype>P&lt;/m:CustotCustomertype>
						else ()
					}
					{
						if($NF_CUSTOP_FIRSTNAME[$p])
							then &lt;m:CustopFirstname>{ data($NF_CUSTOP_FIRSTNAME[$p]) }&lt;/m:CustopFirstname>
						else ()
					}
					{
						if($NF_CUSTOP_LASTNAME[$p])
							then &lt;m:CustopLastname>{ data($NF_CUSTOP_LASTNAME[$p]) }&lt;/m:CustopLastname>
						else ()
					}
					{
						if($NF_CUSTOM_COMPANYNAME[$p])
							then &lt;m:CustomCompanyname>{ data($NF_CUSTOM_COMPANYNAME[$p]) }&lt;/m:CustomCompanyname>
						else ()
					}
					{
						if($NF_CUSTAR_CUSTOMERACCOUNTR[$p])
							then &lt;m:CustarCustomeraccountr>{ data($NF_CUSTAR_CUSTOMERACCOUNTR[$p]) }&lt;/m:CustarCustomeraccountr>
						else ()
					}
					{
						if($NF_TAX_TAXPERCENTAGE[$p])
							then &lt;m:TaxTaxpercentage>{ data($NF_TAX_TAXPERCENTAGE[$p]) }&lt;/m:TaxTaxpercentage>
						else ()
					}
					&lt;/m:Customer>
			}

		&lt;/m:CRMGetProdRelResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetProdRelResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>