<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-11-18</con:description>
  <con:xquery>(: Log Zmian: 
==================================
v.1.0  2010-11-12 PKLI NP2173_1 Utworzenie

:)

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:getCustomersIds($req as element(m:CRMDelCustProdsAddressesRequest), $idx as xs:integer, $res as xs:string)as xs:string{
   let $len:=string-length(data($req/m:numerKlienta[$idx]))
   let $value:=concat(substring("0000000000",1,10 - $len),data($req/m:numerKlienta[$idx]))
   return
     if ($len > 0 and $len &lt;=10)
         then xf:getCustomersIds($req,$idx+1,concat($res,$value))
         else $res
};

declare function xf:mappCRMDelCustProdsAddressesRequest($req as element(m:CRMDelCustProdsAddressesRequest)) as element(fml:FML32) {
  &lt;fml:FML32>
    {
       if($req/m:numerKlienta)
       then &lt;fml:DC_NUMER_KLIENTA>{xf:getCustomersIds($req,1,"") }&lt;/fml:DC_NUMER_KLIENTA>
       else ()
    }
    {
       if($req/m:trnId)
       then &lt;fml:DC_TRN_ID>{ data($req/m:trnId) }&lt;/fml:DC_TRN_ID>
       else ()
    }
    {
       if($req/m:uzytkownik)
       then &lt;fml:DC_UZYTKOWNIK>{  concat("SKP:",data($req/m:uzytkownik)) }&lt;/fml:DC_UZYTKOWNIK>
       else ()
    }
    {
       if($req/m:oddzial)
       then &lt;fml:DC_ODDZIAL>{ data($req/m:oddzial) }&lt;/fml:DC_ODDZIAL>
       else ()
    }
    &lt;fml:DC_TYP_ZMIANY>D&lt;/fml:DC_TYP_ZMIANY>
  &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappCRMDelCustProdsAddressesRequest($body/m:CRMDelCustProdsAddressesRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>