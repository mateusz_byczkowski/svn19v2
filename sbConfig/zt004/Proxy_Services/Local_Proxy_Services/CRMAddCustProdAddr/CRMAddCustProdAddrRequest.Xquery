<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
   let $length:= string-length($sourceString)
   let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
   let $accountNumber :=  substring($sourceString, $startPos)
   return
     $accountNumber
};

declare function xf:mapCRMAddCustProdAddrRequest($req as element(m:CRMAddCustProdAddrRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:TrnId)
					then &lt;fml:DC_TRN_ID>{ data($req/m:TrnId) }&lt;/fml:DC_TRN_ID>
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK>{ concat("SKP:",data($req/m:Uzytkownik)) }&lt;/fml:DC_UZYTKOWNIK>
					else ()
			}
			{
				if($req/m:Oddzial)
					then &lt;fml:DC_ODDZIAL>{ data($req/m:Oddzial) }&lt;/fml:DC_ODDZIAL>
					else ()
			}
			{
				if($req/m:System)
					then &lt;fml:CI_SYSTEM>{ data($req/m:System) }&lt;/fml:CI_SYSTEM>
					else ()
			}
			{
				if($req/m:NumerProduktu)
					then &lt;fml:DC_NUMER_PRODUKTU>{ data($req/m:NumerProduktu) }&lt;/fml:DC_NUMER_PRODUKTU>
					else ()
			}
			{
				if($req/m:NrRachunku)
					then &lt;fml:DC_NR_RACHUNKU>{ cutStr(data($req/m:NrRachunku),10) }&lt;/fml:DC_NR_RACHUNKU>
					else ()
			}
			{
				if($req/m:ImieINazwiskoAlt)
					then &lt;fml:DC_IMIE_I_NAZWISKO_ALT>{ data($req/m:ImieINazwiskoAlt) }&lt;/fml:DC_IMIE_I_NAZWISKO_ALT>
					else ()
			}
			{
				if($req/m:ImieINazwiskoAltCD)
					then &lt;fml:DC_IMIE_I_NAZWISKO_ALT_C_D>{ data($req/m:ImieINazwiskoAltCD) }&lt;/fml:DC_IMIE_I_NAZWISKO_ALT_C_D>
					else ()
			}
			{
				if($req/m:UlicaAdresAlt)
					then &lt;fml:DC_ULICA_ADRES_ALT>{ data($req/m:UlicaAdresAlt) }&lt;/fml:DC_ULICA_ADRES_ALT>
					else ()
			}
			{
				if($req/m:NrPosesLokaluAdresAlt)
					then &lt;fml:DC_NR_POSES_LOKALU_ADRES_ALT>{ data($req/m:NrPosesLokaluAdresAlt) }&lt;/fml:DC_NR_POSES_LOKALU_ADRES_ALT>
					else ()
			}
			{
				if($req/m:MiastoAdresAlt)
					then &lt;fml:DC_MIASTO_ADRES_ALT>{ data($req/m:MiastoAdresAlt) }&lt;/fml:DC_MIASTO_ADRES_ALT>
					else ()
			}
			{
				if($req/m:WojewodztwoKrajAdresAlt)
					then &lt;fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{ data($req/m:WojewodztwoKrajAdresAlt) }&lt;/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
					else ()
			}
			{
				if($req/m:KodPocztowyAdresAlt)
					then &lt;fml:DC_KOD_POCZTOWY_ADRES_ALT>{ data($req/m:KodPocztowyAdresAlt) }&lt;/fml:DC_KOD_POCZTOWY_ADRES_ALT>
					else ()
			}
			{
				if($req/m:KodKraju)
					then &lt;fml:DC_KOD_KRAJU>{ data($req/m:KodKraju) }&lt;/fml:DC_KOD_KRAJU>
					else ()
			}
			{
				if($req/m:DataWprowadzenia)
					then &lt;fml:DC_DATA_WPROWADZENIA>{ data($req/m:DataWprowadzenia) }&lt;/fml:DC_DATA_WPROWADZENIA>
					else ()
			}
			{
				if($req/m:DataKoncowa)
					then &lt;fml:DC_DATA_KONCOWA>{ data($req/m:DataKoncowa) }&lt;/fml:DC_DATA_KONCOWA>
					else ()
			}
			{
				if($req/m:AlternatywnyNrTelefonu)
					then &lt;fml:DC_ALTERNATYWNY_NR_TELEFONU>{ data($req/m:AlternatywnyNrTelefonu) }&lt;/fml:DC_ALTERNATYWNY_NR_TELEFONU>
					else ()
			}
			{
				if($req/m:KasowacPrzyWygasnieciu)
					then &lt;fml:DC_KASOWAC_PRZY_WYGASNIECIU>{ data($req/m:KasowacPrzyWygasnieciu) }&lt;/fml:DC_KASOWAC_PRZY_WYGASNIECIU>
					else ()
			}
			{
				if($req/m:TypAdresu)
					then &lt;fml:DC_TYP_ADRESU>{ data($req/m:TypAdresu) }&lt;/fml:DC_TYP_ADRESU>
					else ()
			}
                        &lt;DC_TYP_ZMIANY>A&lt;/DC_TYP_ZMIANY>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMAddCustProdAddrRequest($body/m:CRMAddCustProdAddrRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>