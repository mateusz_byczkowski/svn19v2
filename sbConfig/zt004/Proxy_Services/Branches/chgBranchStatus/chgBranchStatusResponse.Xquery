<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$fML321" element="ns2:FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-return element="ns0:invokeResponse" location="chgBranchStatus.WSDL" ::)

declare namespace ns2 = "";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns5 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Branches/chgBranchStatus/chgBranchStatusResponse/";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:chgBranchStatusResponse($fML321 as element(ns2:FML32))
    as element(ns0:invokeResponse) {
        &lt;ns0:invokeResponse>
            &lt;ns0:response>
                &lt;ns5:ResponseMessage?>
                    &lt;ns5:result?>
                               						true
                               					&lt;/ns5:result>
                &lt;/ns5:ResponseMessage>
            &lt;/ns0:response>
            &lt;ns0:branchCurrentStatusOut>
                &lt;ns1:BranchCurrentStatus?>
                
                					{
					let $dateTime := data($fML321/ns2:TR_CZAS_ODPOWIEDZI) 
					return
						if ($dateTime) then 
		                    &lt;ns1:lastChangeDate?>{  
								fn:concat(fn:substring($dateTime, 1, 10),
										  'T',
										  fn:substring($dateTime, 12, 15))
							}&lt;/ns1:lastChangeDate>
						else
							()
				}
                
                &lt;/ns1:BranchCurrentStatus>
            &lt;/ns0:branchCurrentStatusOut>
            &lt;ns0:transactionOut>
                &lt;ns7:Transaction?>
                    &lt;ns7:transactionStatus?>
                        &lt;ns3:TransactionStatus?>
                            &lt;ns3:transactionStatus?>{ data($fML321/ns2:TR_STATUS) }&lt;/ns3:transactionStatus>
                        &lt;/ns3:TransactionStatus>
                    &lt;/ns7:transactionStatus>
                &lt;/ns7:Transaction>
            &lt;/ns0:transactionOut>
            &lt;ns0:backendResponse>
                &lt;ns7:BackendResponse?>
    
    				{
					let $transactionDate := data($fML321/ns2:TR_DATA_KSIEG)
					return
						if ($transactionDate) then 
							&lt;ns7:icbsDate>{
								fn:concat(fn:substring($transactionDate, 7, 4),
										  '-',
										  fn:substring($transactionDate, 4, 2),
										  '-',
										  fn:substring($transactionDate, 1, 2))
							}&lt;/ns7:icbsDate>
						else
							()
				}
                    &lt;ns7:icbsSessionNumber?>{ xs:string( data($fML321/ns2:TR_TXN_SESJA) ) }&lt;/ns7:icbsSessionNumber>
                    &lt;ns7:transactionRefNumber?>{ data($fML321/ns2:TR_NR_REF) }&lt;/ns7:transactionRefNumber>
					{
					let $dateTime := data($fML321/ns2:TR_CZAS_ODPOWIEDZI) 
					return
						if ($dateTime) then 
		                    &lt;ns7:dateTime>{  
								fn:concat(fn:substring($dateTime, 1, 10),
										  'T',
										  fn:substring($dateTime, 12, 15))
							}&lt;/ns7:dateTime>
						else
							()
				}

                &lt;ns7:beUserId?>{ data($fML321/ns2:TR_UZYTKOWNIK) }&lt;/ns7:beUserId>

				&lt;ns4:beErrorCodeList?>{
					for $FML320 in ($fML321/ns1:TR_KOD_BLEDU_1
									 union $fML321/ns1:TR_KOD_BLEDU_2
									 union $fML321/ns1:TR_KOD_BLEDU_3
									 union $fML321/ns1:TR_KOD_BLEDU_4
									 union $fML321/ns1:TR_KOD_BLEDU_5)  
					return
						let $errorCode := data($FML320)
						return
							if ($errorCode ne ''
								and $errorCode ne '000') then
								&lt;ns4:BeErrorCode>
									&lt;ns4:errorCode>
										&lt;ns3:BackendErrorCode>
											&lt;ns3:errorCode>{
												data($FML320)
											}&lt;/ns3:errorCode>
										&lt;/ns3:BackendErrorCode>
									&lt;/ns4:errorCode>
								&lt;/ns4:BeErrorCode>
							else
								()
				}&lt;/ns4:beErrorCodeList>            
                &lt;/ns7:BackendResponse>
            &lt;/ns0:backendResponse>
        &lt;/ns0:invokeResponse>
};

declare variable $fML321 as element(ns2:FML32) external;

&lt;soap-env:Body>{
xf:chgBranchStatusResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>