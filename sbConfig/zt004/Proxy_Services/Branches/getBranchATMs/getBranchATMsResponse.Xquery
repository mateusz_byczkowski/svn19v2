<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2009-12-11
 :
 : wersja WSDLa: 24-03-2010 10:14:05
 :
 : $Proxy Services/Branches/getBranchATMs/getBranchATMsResponse.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getBranchATMs/getBranchATMsResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getBranchATMsResponse($fML321 as element(ns0:FML32))
    as element(ns5:invokeResponse)
{
	&lt;ns5:invokeResponse>
        &lt;ns5:atmList>{
           	for $i in 1 to count($fML321/ns0:NF_CURREC_CURRENCYCODE)
           	return
                &lt;ns1:CurrencyCash>
                    &lt;ns1:amount>{
						data($fML321/ns0:NF_CURRCA_AMOUNT[$i])
					}&lt;/ns1:amount>
                    &lt;ns1:atm>
                        &lt;ns4:ATM>
                            &lt;ns4:atmNumber>{
								data($fML321/ns0:NF_ATM_ATMNUMBER[$i])
							}&lt;/ns4:atmNumber>
                            &lt;ns4:atmType>
                                &lt;ns3:ATMType>
                                    &lt;ns3:atmType>{
										data($fML321/ns0:NF_ATMT_ATMTYP[$i])
									}&lt;/ns3:atmType>
                                &lt;/ns3:ATMType>
                            &lt;/ns4:atmType>
                            &lt;ns4:till>
                                &lt;ns4:Till>
                                    &lt;ns4:tillID>{
										data($fML321/ns0:NF_TILL_TILLID[$i])
									}&lt;/ns4:tillID>
									
                                    &lt;ns4:tillName>{
										data($fML321/ns0:NF_TILL_TILLNAME[$i])
									}&lt;/ns4:tillName>
                                &lt;/ns4:Till>
                            &lt;/ns4:till>
                            &lt;ns4:teller>
                                &lt;ns4:Teller>
                                    &lt;ns4:tellerID>{
										data($fML321/ns0:NF_TELLER_TELLERID[$i])
									}&lt;/ns4:tellerID>
                                &lt;/ns4:Teller>
                            &lt;/ns4:teller>
                        &lt;/ns4:ATM>
                    &lt;/ns1:atm>
                    &lt;ns1:currency>
                        &lt;ns2:CurrencyCode>
                            &lt;ns2:currencyCode>{
								data($fML321/ns0:NF_CURREC_CURRENCYCODE[$i])
							}&lt;/ns2:currencyCode>
                        &lt;/ns2:CurrencyCode>
                    &lt;/ns1:currency>
               	&lt;/ns1:CurrencyCash>	                    
        }&lt;/ns5:atmList>
	&lt;/ns5:invokeResponse>
};

&lt;soap-env:Body>{
	xf:getBranchATMsResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>