<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.4
 : @since   2009-12-21
 :
 : wersja WSDLa: 08-01-2010 18:35:54
 :
 : $Proxy Services/Branches/getBranchTills/getBranchTillsRequest.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getBranchTills/getBranchTillsRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";
declare namespace ns6 = "urn:baseauxentities.be.dcl";

declare variable $header1 as element(ns5:header) external;
declare variable $invoke1 as element(ns5:invoke) external;

(:~
 : @param $invoke1 operacja wejśiowa
 : @param $header1 nagłówek komunikatu
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:getBranchTillsRequest(	$header1 as element(ns5:header),
											$invoke1 as element(ns5:invoke))
    as element(ns0:FML32)
{
	&lt;ns0:FML32>
    
    	(:
    	 : dane z nagłówka
    	 :)
    	&lt;ns0:NF_MSHEAD_MSGID?>{
			data($header1/ns5:msgHeader/ns5:msgId)
		}&lt;/ns0:NF_MSHEAD_MSGID>
		
		(:
		 : dane do stronicowania
		 :)
        &lt;ns0:NF_PAGEC_ACTIONCODE?>{
			data($invoke1/ns5:bcd/ns4:BusinessControlData/ns4:pageControl/ns6:PageControl/ns6:actionCode)
		}&lt;/ns0:NF_PAGEC_ACTIONCODE>
		
        &lt;ns0:NF_PAGEC_PAGESIZE?>{
			data($invoke1/ns5:bcd/ns4:BusinessControlData/ns4:pageControl/ns6:PageControl/ns6:pageSize)
		}&lt;/ns0:NF_PAGEC_PAGESIZE>
		
        {
        	let $reverseOrder := data($invoke1/ns5:bcd/ns4:BusinessControlData/ns4:pageControl/ns6:PageControl/ns6:reverseOrder)
	        return 
            	if ($reverseOrder) then
		            &lt;ns0:NF_PAGEC_REVERSEORDER>{			            	
		            	if ($reverseOrder eq 'true') then
		            		'1'
		            	else
		            		'0'
		            }&lt;/ns0:NF_PAGEC_REVERSEORDER>
				else
					()
		}
		
        &lt;ns0:NF_PAGEC_NAVIGATIONKEYDEFI?>{
			data($invoke1/ns5:bcd/ns4:BusinessControlData/ns4:pageControl/ns6:PageControl/ns6:navigationKeyDefinition)
		}&lt;/ns0:NF_PAGEC_NAVIGATIONKEYDEFI>
		
        &lt;ns0:NF_PAGEC_NAVIGATIONKEYVALU?>{
			data($invoke1/ns5:bcd/ns4:BusinessControlData/ns4:pageControl/ns6:PageControl/ns6:navigationKeyValue)
		}&lt;/ns0:NF_PAGEC_NAVIGATIONKEYVALU>
		
		(:
		 : dane filtrujące
		 :)
        &lt;ns0:NF_BRANCC_BRANCHCODE?>{
			data($invoke1/ns5:branchCode/ns2:BranchCode/ns2:branchCode)
		}&lt;/ns0:NF_BRANCC_BRANCHCODE>
		
        &lt;ns0:NF_USERTS_SESSIONDATE?>{
			data($invoke1/ns5:userTxnSession/ns3:UserTxnSession/ns3:sessionDate)
		}&lt;/ns0:NF_USERTS_SESSIONDATE>
		
		(:
		 : status stanowiska
		 :
		 : O --> 1 (otwarte)
		 : C --> 0 (zamknięte)
		 :)
        {
			let $sessionStatus := data($invoke1/ns5:userTxnSession/ns3:UserTxnSession/ns3:sessionStatus/ns1:UserTxnSessionStatus/ns1:userTxnSessionStatus)
			return
	           	if ($sessionStatus) then
					&lt;ns0:NF_USETSS_USERTXNSESSIONST>{
						if ($sessionStatus eq 'O') then
							'1'
						else
							'0'
					}&lt;/ns0:NF_USETSS_USERTXNSESSIONST>
            	else
            		()
        }
        
        &lt;ns0:NF_USER_USERLASTNAME?>{
			data($invoke1/ns5:userTxnSession/ns3:UserTxnSession/ns3:user/ns4:User/ns4:userLastName)
		}&lt;/ns0:NF_USER_USERLASTNAME>
		
        &lt;ns0:NF_TILL_TILLID?>{
			data($invoke1/ns5:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID)
		}&lt;/ns0:NF_TILL_TILLID>
		
		(:
		 : typ stanowiska
		 :
		 : K - kasa
		 : S - skarbiec
		 :)
        &lt;ns0:NF_TILTY_TILLTYP?>{
			data($invoke1/ns5:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillType/ns1:TillType/ns1:tillType)
		}&lt;/ns0:NF_TILTY_TILLTYP>
		
    &lt;/ns0:FML32>
};

&lt;soap-env:Body>{
	xf:getBranchTillsRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>