<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.4
 : @since   2010-01-11
 :
 : wersja WSDLa: 08-01-2010 18:35:54
 :
 : $Proxy Services/Branches/getBranchTills/getBranchTillsResponse.xq$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getBranchTills/getBranchTillsResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns3 = "urn:entities.be.dcl";
declare namespace ns4 = "urn:be.services.dcl";
declare namespace ns5 = "urn:baseauxentities.be.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getBranchTillsResponse($fML321 as element(ns0:FML32))
    as element(ns4:invokeResponse)
{
    &lt;ns4:invokeResponse>
        &lt;ns4:userTxnSessionOut>{
            for $i in 1 to count($fML321/ns0:NF_USERTS_SESSIONNUMBER)
            return
                &lt;ns2:UserTxnSession>

                    &lt;ns2:sessionNumber>{
						data($fML321/ns0:NF_USERTS_SESSIONNUMBER[$i])
					}&lt;/ns2:sessionNumber>
					
					(:
					 : niepusta data, różna od 0001-01-01
					 :)
					{
						let $sessionDate := data($fML321/ns0:NF_USERTS_SESSIONDATE[$i])
						return
							if ($sessionDate
								and $sessionDate ne '0001-01-01') then
								&lt;ns2:sessionDate>{
									$sessionDate
								}&lt;/ns2:sessionDate>
							else
								()
					}

					(:
					 : niepusta data, różna od '0001-01-01T00:00:00'
					 :)
					{
						let $lastChangeDate := data($fML321/ns0:NF_USERTS_LASTCHANGEDATE[$i])
						return
                    		if ($lastChangeDate
                    			and $lastChangeDate ne '0001-01-01T00:00:00') then
								&lt;ns2:lastChangeDate>{
									$lastChangeDate
								}&lt;/ns2:lastChangeDate>
							else
								()
                    }

                    (:
                     : status sesji:
                     : - 0 --> C (zamknięta)
                     : - 1 --> O (otwarta)
                     :)
					{
						let $sessionStatus := data($fML321/ns0:NF_USETSS_USERTXNSESSIONST[$i])
						return
							if ($sessionStatus) then
			                    &lt;ns2:sessionStatus>
			                        &lt;ns1:UserTxnSessionStatus>
			                            &lt;ns1:userTxnSessionStatus>{
			                            	if ($sessionStatus eq '0') then
			                            		'C'
			                            	else
			                            		'O'
			                            }&lt;/ns1:userTxnSessionStatus>
			                        &lt;/ns1:UserTxnSessionStatus>
			                    &lt;/ns2:sessionStatus>
							else
								()
					}

					(:
					 : dane uzytkownika
					 :)
                    &lt;ns2:user>
                        &lt;ns3:User>
                            &lt;ns3:userLastName>{
								data($fML321/ns0:NF_USER_USERLASTNAME[$i])
							}&lt;/ns3:userLastName>
							
                            &lt;ns3:userID>{
								data($fML321/ns0:NF_USER_USERID[$i])
							}&lt;/ns3:userID>
							
                            &lt;ns3:userFirstName>{
								data($fML321/ns0:NF_USER_USERFIRSTNAME[$i])
							}&lt;/ns3:userFirstName>
                        &lt;/ns3:User>
                    &lt;/ns2:user>

					(:
					 : dane kasy
					 :)
                    &lt;ns2:till>
                        &lt;ns2:Till>
                            &lt;ns2:tillID>{
								data($fML321/ns0:NF_TILL_TILLID[$i])
							}&lt;/ns2:tillID>
							
                            &lt;ns2:tillName>{
                            	data($fML321/ns0:NF_TILL_TILLNAME[$i])
							}&lt;/ns2:tillName>
							
                            &lt;ns2:tillType>
                                &lt;ns1:TillType>
                                    &lt;ns1:tillType>{
										data($fML321/ns0:NF_TILTY_TILLTYP[$i])
									}&lt;/ns1:tillType>
                                &lt;/ns1:TillType>
                            &lt;/ns2:tillType>
                        &lt;/ns2:Till>
                    &lt;/ns2:till>

					(:
					 : kasjer
					 :)
                    &lt;ns2:teller>
                        &lt;ns2:Teller>
                            &lt;ns2:tellerID>{
								data($fML321/ns0:NF_TELLER_TELLERID[$i])
							}&lt;/ns2:tellerID>
                        &lt;/ns2:Teller>
                    &lt;/ns2:teller>
                &lt;/ns2:UserTxnSession>
		}&lt;/ns4:userTxnSessionOut>
		
        &lt;ns4:bcd>
            &lt;ns3:BusinessControlData>
                &lt;ns3:pageControl?>
                    &lt;ns5:PageControl?>
                    	{
                    		let $hasNext := data($fML321/ns0:NF_PAGEC_HASNEXT)
                    		return
                    			if ($hasNext) then
									&lt;ns5:hasNext>{ 
										xs:boolean($hasNext)
									}&lt;/ns5:hasNext>
								else
									()
                    	}
						
                        &lt;ns5:navigationKeyDefinition?>{
							data($fML321/ns0:NF_PAGEC_NAVIGATIONKEYDEFI)
						}&lt;/ns5:navigationKeyDefinition>
	
                        &lt;ns5:navigationKeyValue?>{
							data($fML321/ns0:NF_PAGEC_NAVIGATIONKEYVALU)
						}&lt;/ns5:navigationKeyValue>
						
					&lt;/ns5:PageControl>
                &lt;/ns3:pageControl>
            &lt;/ns3:BusinessControlData>
        &lt;/ns4:bcd>
    &lt;/ns4:invokeResponse>
};

&lt;soap-env:Body>{
	xf:getBranchTillsResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>