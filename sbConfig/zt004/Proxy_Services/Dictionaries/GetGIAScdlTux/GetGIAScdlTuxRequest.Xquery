<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "urn:BProExtWebServices";


declare function xf:map_GetGIAScdlTuxRequest($fml as element(fml:FML32))
	as element(urn:callService) {
          <urn:callService xmlns:xsd="http://www.w3.org/2001/XMLSchema">
              <arg_0_0 xsi:type="xsd:string"><![CDATA[Kalkulator6]]]]>><![CDATA[</arg_0_0>
              <arg_1_0 xsi:type="xsd:string">
                          <![CDATA[<?xml version="1.0"?><!DOCTYPE bpro-service-params PUBLIC "-//Talex S.A.//DTD BPro service parameters//PL" "parameters.dtd">                           
                            <bpro-service-params>
                               <param><name>KwKred</name><value>]]]]>><![CDATA[{ data($fml/fml:DC_KWOTA_KREDYTU) }<![CDATA[</value></param>
                               <param><name>LiczbaRat</name><value>]]]]>><![CDATA[{ data($fml/fml:DC_LICZBA_RAT) }<![CDATA[</value></param>
                               <param><name>KwProwizji</name><value>]]]]>><![CDATA[{ data($fml/fml:DC_KWOTA_PROWIZJI) }<![CDATA[</value></param>
                               <param><name>DataPlatn</name><value>]]]]>><![CDATA[{ data($fml/fml:DC_DATA_PIERWSZEJ_PLATNOSCI) }<![CDATA[</value></param>
                               <param><name>DzienPlatn</name><value>]]]]>><![CDATA[{ data($fml/fml:DC_SPECYFICZNY_DZIEN_SPLATY) }<![CDATA[</value></param>
                               <param><name>TypPlatn</name><value>]]]]>><![CDATA[{ data($fml/fml:DC_TYP_PLATNOSCI) }<![CDATA[</value></param>
                               <param><name>OprocKred</name><value>]]]]>><![CDATA[{ data($fml/fml:DC_STOPA_PROCENTOWA) }<![CDATA[</value></param>
                               <param><name>PozostKoszty</name><value>]]]]>><![CDATA[{ data($fml/fml:DC_KOSZTY_POZOSTALE) }<![CDATA[</value></param>
                               <param><name>Oddzial</name><value>]]]]>><![CDATA[{ data($fml/fml:DC_NUMER_ODDZIALU) }<![CDATA[</value></param>
                           </bpro-service-params>]]]]>><![CDATA[
               </arg_1_0>
      </urn:callService>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_GetGIAScdlTuxRequest($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>