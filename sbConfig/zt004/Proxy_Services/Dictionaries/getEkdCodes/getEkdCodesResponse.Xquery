<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace urn2="urn:entities.be.dcl";
declare namespace urn3="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function Num2Bool($dateIn as xs:string) as xs:boolean {
 if ($dateIn = "1") 
    then true()
    else false()
};

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
&lt;ns0:dicts>
  {
    let $opis := $parm/CI_OPIS_WARTOSCI_SLOWNIKA
    for $wartosc at $occ in $parm/CI_WARTOSC_SLOWNIKA
    return
    &lt;ns3:EkdCode>
      &lt;ns3:ekdCode?>{data($wartosc)}&lt;/ns3:ekdCode>
      &lt;ns3:description?>{data($opis[$occ])}&lt;/ns3:description>
    &lt;/ns3:EkdCode>
  }
&lt;/ns0:dicts>
};

declare function getElementsForBcd($parm as element(fml:FML32)) as element()
{
let $lastValue := data(fn:count($parm/CI_WARTOSC_SLOWNIKA))
return
&lt;ns0:bcd>
  &lt;urn2:BusinessControlData>
      &lt;urn2:pageControl>
          &lt;urn3:PageControl>
              &lt;urn3:hasNext?>{Num2Bool(data($parm/CI_OVERFLOW))}&lt;/urn3:hasNext>
              &lt;urn3:navigationKeyValue?>{data($parm/CI_WARTOSC_SLOWNIKA[$lastValue])}&lt;/urn3:navigationKeyValue>
          &lt;/urn3:PageControl>
      &lt;/urn2:pageControl>
  &lt;/urn2:BusinessControlData>
&lt;/ns0:bcd>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
&lt;ns0:invokeResponse>
  {getElementsForDicts($parm)}
  {getElementsForBcd($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>