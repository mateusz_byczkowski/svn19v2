<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-10-07</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:filtersandmessages.entities.be.dcl";

declare function xf:mapchkTokenSerialNumberRequest($req as element(urn:invoke), $head as element(urn:header))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				&lt;fml:E_TOKEN_SERIAL_NO?>{ data($req/urn:token/urn1:Token/urn1:serialNumber) }&lt;/fml:E_TOKEN_SERIAL_NO>
			}
			{
				&lt;fml:E_MSHEAD_MSGID>{data($head/urn:msgHeader/urn:msgId)}&lt;/fml:E_MSHEAD_MSGID>
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body>
{ xf:mapchkTokenSerialNumberRequest($body/urn:invoke,$header/urn:header) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>