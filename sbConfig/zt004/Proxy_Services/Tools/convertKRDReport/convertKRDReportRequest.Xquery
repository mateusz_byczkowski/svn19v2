<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns1 = "urn:hlbsentities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/XQueryTransformation/convertKRDReportRequest/";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:convertKRDReportRequest($invoke1 as element(ns0:invoke))
    as element(ns0:invoke) {
        &lt;invoke xmlns="urn:be.services.dcl">
            &lt;convertKRDReportHolder>
                &lt;ns2:ConvertKRDReportHolder xmlns:ns2="urn:hlbsentities.be.dcl">
                   &lt;ns2:reportContent>{ data($invoke1/ns0:convertKRDReportHolder/ns1:ConvertKRDReportHolder/ns1:reportContent) }&lt;/ns2:reportContent>
                &lt;/ns2:ConvertKRDReportHolder>
            &lt;/convertKRDReportHolder>
        &lt;/invoke>
};

declare variable $invoke1 as element(ns0:invoke) external;


&lt;soap:Body>
{ xf:convertKRDReportRequest($body/ns0:invoke) }
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>