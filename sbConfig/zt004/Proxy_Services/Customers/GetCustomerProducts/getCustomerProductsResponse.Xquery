<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1  2009-07-10  PKLI  TEET 39294</con:description>
  <con:xquery>(:Change log :)
(: v.1.1  2009-07-10  PKLI  TEET 39294: zmiana mapowania pola accountType :)

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:entities.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:be.services.dcl";
declare namespace ns9="urn:baseauxentities.be.dcl";
declare namespace ns10="urn:insurance.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function sourceValue2Boolean ($parm as xs:anyType,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function insertDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
      if ($value)
        then if(string-length($value)>5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)>5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };

declare function chgDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{

    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)

    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 


};

declare function getElementsForInsurancePackagesList($parm as element(fml:FML32)) as element()
{
&lt;ns8:insurancePackagesList>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
       if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "4") and (data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ]) = "91"))
     then
    &lt;ns10:InsurancePackageAcc>
      &lt;ns10:number?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns10:number>
	  {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns10:startDate")}
      &lt;ns10:insuranceDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns10:insuranceDescription>
      &lt;ns10:accountRelationshipList>
          &lt;ns3:AccountRelationship>
            &lt;ns3:relationship>
              &lt;ns5:CustomerAccountRelationship>
                &lt;ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship>
              &lt;/ns5:CustomerAccountRelationship>
            &lt;/ns3:relationship>
          &lt;/ns3:AccountRelationship>
      &lt;/ns10:accountRelationshipList>
      &lt;ns10:productDefinition>
        &lt;ns1:ProductDefinition>
          &lt;ns1:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns1:idProductDefinition>
          &lt;ns1:productGroup>
            &lt;ns1:ProductGroup>
              &lt;ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup>
            &lt;/ns1:ProductGroup>
          &lt;/ns1:productGroup>
        &lt;/ns1:ProductDefinition>
      &lt;/ns10:productDefinition>
    &lt;/ns10:InsurancePackageAcc>
   else()
  }
&lt;/ns8:insurancePackagesList>
};

declare function getElementsForInsurancePoliciesList($parm as element(fml:FML32)) as element()
{

&lt;ns8:insurancePoliciesList>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER 
    return
     if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "4") and (data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ]) = "3"))
     then
    &lt;ns10:InsurancePolicyAcc>
      &lt;ns10:policyRefNum?>{data($parm/NF_INSURA_ID[$occ])}&lt;/ns10:policyRefNum>
      {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns10:startDate")}
      &lt;ns10:insuranceDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns10:insuranceDescription>
	  &lt;ns10:accountRelationshipList>
          &lt;ns3:AccountRelationship>
            &lt;ns3:relationship>
              &lt;ns5:CustomerAccountRelationship>
                &lt;ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship>
              &lt;/ns5:CustomerAccountRelationship>
            &lt;/ns3:relationship>
          &lt;/ns3:AccountRelationship>
      &lt;/ns10:accountRelationshipList>
      &lt;ns10:productDefinition>
        &lt;ns1:ProductDefinition>
          &lt;ns1:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns1:idProductDefinition>
          &lt;ns1:productGroup>
            &lt;ns1:ProductGroup>
              &lt;ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup>
            &lt;/ns1:ProductGroup>
          &lt;/ns1:productGroup>
        &lt;/ns1:ProductDefinition>
      &lt;/ns10:productDefinition>
    &lt;/ns10:InsurancePolicyAcc>
   else()
  }
&lt;/ns8:insurancePoliciesList>
};

declare function getElementsForAccountListOut($parm as element(fml:FML32)) as element()
{

&lt;ns8:accountListOut>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
        if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="2") or 
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="3") or 
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="10") or
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="13") or
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="14")) then
    &lt;ns0:Account>
      &lt;ns0:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns0:accountNumber>
      &lt;ns0:customerNumber?>{data($parm/NF_CUSTOM_CUSTOMERNUMBER[$occ])}&lt;/ns0:customerNumber>
      {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns0:accountOpenDate")}
      &lt;ns0:accountName?>{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}&lt;/ns0:accountName>
      &lt;ns0:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns0:currentBalance>
      &lt;ns0:debitPercentage?>{data($parm/NF_ACCOUN_DEBITPERCENTAGE[$occ])}&lt;/ns0:debitPercentage>
      &lt;ns0:statementForAccount?>{sourceValue2Boolean(data($parm/NF_ACCOUN_STATEMENTFORACCO[$occ]),"1")}&lt;/ns0:statementForAccount>
      &lt;ns0:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns0:accountDescription>
      &lt;ns0:codeProductSystem?>{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}&lt;/ns0:codeProductSystem>
      &lt;ns0:productCode?>{data($parm/NF_ACCOUN_PRODUCTCODE[$occ])}&lt;/ns0:productCode>
      &lt;ns0:availableBalance?>{data($parm/NF_ACCOUN_AVAILABLEBALANCE[$occ])}&lt;/ns0:availableBalance>
      &lt;ns0:currentBalancePLN?>{data($parm/NF_ACCOUN_CURRENTBALANCEPL[$occ])}&lt;/ns0:currentBalancePLN>
      &lt;ns0:accountRelationshipList>
          &lt;ns3:AccountRelationship>
            &lt;ns3:relationship>
              &lt;ns5:CustomerAccountRelationship>
                &lt;ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship>
              &lt;/ns5:CustomerAccountRelationship>
            &lt;/ns3:relationship>
          &lt;/ns3:AccountRelationship>
      &lt;/ns0:accountRelationshipList>
       {
         if (data($parm/NF_PRODUA_CODEPRODUCTAREA) = "03") then
           &lt;ns0:loanAccount>
             &lt;ns0:LoanAccount>
               &lt;ns0:dateOfLastTrans?>{data($parm/NF_LOANA_DATEOFLASTTRANS[$occ])}&lt;/ns0:dateOfLastTrans>
             &lt;/ns0:LoanAccount>
           &lt;/ns0:loanAccount>
         else ()
      }
      {
          if (data($parm/NF_PRODUA_CODEPRODUCTAREA) = "10") then
     &lt;ns0:timeAccount>
        &lt;ns0:TimeAccount>
          &lt;ns0:renewalFrequency?>{data($parm/NF_TIMEA_RENEWALFREQUENCY[$occ])}&lt;/ns0:renewalFrequency>
		  {insertDate(data($parm/NF_TIMEA_NEXTRENEWALMATURI[$occ]),"yyyy-MM-dd","ns0:nextRenewalMaturityDate")}
          &lt;ns0:interestDisposition?>{sourceValue2Boolean(data($parm/NF_TIMEA_INTERESTDISPOSITI[$occ]),"1")}&lt;/ns0:interestDisposition>
          &lt;ns0:renewalID>
            &lt;ns5:RenewalType>
              &lt;ns5:renewalType?>{data($parm/NF_RENEWT_RENEWALTYPE[$occ])}&lt;/ns5:renewalType>
            &lt;/ns5:RenewalType>
          &lt;/ns0:renewalID>
          &lt;ns0:renewalPeriod>
            &lt;ns5:Period>
              &lt;ns5:period?>{data($parm/NF_PERIOD_PERIOD[$occ])}&lt;/ns5:period>
            &lt;/ns5:Period>
          &lt;/ns0:renewalPeriod>
        &lt;/ns0:TimeAccount>
      &lt;/ns0:timeAccount>
     else()
     }

      &lt;ns0:accountType>
        &lt;ns5:AccountType>
         (: T39249 &lt;ns5:accountType?>{data($parm/NF_ACCOUN_ACCOUNTTYPE[$occ])}&lt;/ns5:accountType> :)
             &lt;ns5:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns5:accountType>  (: T39249 :)
        &lt;/ns5:AccountType>
      &lt;/ns0:accountType>
      &lt;ns0:currency>
        &lt;ns5:CurrencyCode>
          &lt;ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode>
        &lt;/ns5:CurrencyCode>
      &lt;/ns0:currency>
      &lt;ns0:accountStatus>
        &lt;ns2:AccountStatus>
            &lt;ns2:accountStatus?>{data($parm/NF_PRODUCT_STATUSCODE[$occ])}&lt;/ns2:accountStatus>
        &lt;/ns2:AccountStatus>
      &lt;/ns0:accountStatus>
      &lt;ns0:accountBranchNumber>
        &lt;ns5:BranchCode>
          &lt;ns5:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE[$occ])}&lt;/ns5:branchCode>
        &lt;/ns5:BranchCode>
      &lt;/ns0:accountBranchNumber>
	  &lt;ns0:productDefinition>
        &lt;ns1:ProductDefinition>
          &lt;ns1:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns1:idProductDefinition>
          &lt;ns1:productGroup>
            &lt;ns1:ProductGroup>
              &lt;ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup>
            &lt;/ns1:ProductGroup>
          &lt;/ns1:productGroup>
        &lt;/ns1:ProductDefinition>
      &lt;/ns0:productDefinition>
    &lt;/ns0:Account>
   else ()
  }
&lt;/ns8:accountListOut>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns8:invokeResponse>

           {getElementsForInsurancePackagesList($parm)}
{getElementsForInsurancePoliciesList($parm)}
          {getElementsForAccountListOut($parm)}
          
      


  &lt;ns8:bcd>
    &lt;ns6:BusinessControlData>
      &lt;ns6:pageControl>
        &lt;ns9:PageControl>
          &lt;ns9:hasNext?>{data($parm/NF_PAGEC_HASNEXT)}&lt;/ns9:hasNext>
          &lt;ns9:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns9:navigationKeyDefinition>
          &lt;ns9:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns9:navigationKeyValue>
        &lt;/ns9:PageControl>
      &lt;/ns6:pageControl>
    &lt;/ns6:BusinessControlData>
  &lt;/ns8:bcd>
&lt;/ns8:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>