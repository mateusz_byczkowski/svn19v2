<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/GetCustomerULAllData/RequestTransform2/";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace ns-1 = "urn:baseentities.be.dcl";
declare namespace ns1 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns3 = "urn:baseauxentities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace ns5 = "urn:entities.be.dcl";

declare function xf:RequestTransform2($invoke1 as element(ns2:invoke), $header1 as element(ns2:header))
    as element(ns1:CISGetCustomerRequest) {
        &lt;ns1:CISGetCustomerRequest>
            {
                for $customerNumber in $invoke1/ns2:customer/ns0:Customer/ns0:customerNumber
                return
                    &lt;ns1:NumerKlienta>{ data($customerNumber) }&lt;/ns1:NumerKlienta>
            }
            {
                for $userID in $header1/ns2:msgHeader/ns2:userId
                return
                    &lt;ns1:IdWewPrac>{ xs:long( data($userID) ) }&lt;/ns1:IdWewPrac>
            }
            {
                for $companyType in $invoke1/ns2:customer/ns0:Customer/ns0:companyID/ns4:CompanyType/ns4:companyType
                return
                    &lt;ns1:IdSpolki>{ xs:short( data($companyType) ) }&lt;/ns1:IdSpolki>
            }
            {
                for $value in $invoke1/ns2:CIOpcja/ns3:StringHolder/ns3:value
                return
                    &lt;ns1:Opcja>{ xs:short( data($value) ) }&lt;/ns1:Opcja>
            }
        &lt;/ns1:CISGetCustomerRequest>
};

declare variable $header1 as element(ns2:header) external;
declare variable $invoke1 as element(ns2:invoke) external;

xf:RequestTransform2($invoke1, $header1)</con:xquery>
</con:xqueryEntry>