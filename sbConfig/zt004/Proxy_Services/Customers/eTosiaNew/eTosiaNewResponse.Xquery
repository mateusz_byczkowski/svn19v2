<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbCEKEeTosiaNewResponse($fml as element(fml:FML32))
	as element(m:bCEKEeTosiaNewResponse) {
		&lt;m:bCEKEeTosiaNewResponse>
			{
				if($fml/fml:E_SMS_TOKEN_NO)
					then &lt;m:SmsTokenNo>{ data($fml/fml:E_SMS_TOKEN_NO) }&lt;/m:SmsTokenNo>
					else ()
			}
			{
				if($fml/fml:E_SMS_TOKEN_STATUS)
					then &lt;m:SmsTokenStatus>{ data($fml/fml:E_SMS_TOKEN_STATUS) }&lt;/m:SmsTokenStatus>
					else ()
			}
			{
				if($fml/fml:E_SMS_TOKEN_COUNT)
					then &lt;m:SmsTokenCount>{ data($fml/fml:E_SMS_TOKEN_COUNT) }&lt;/m:SmsTokenCount>
					else ()
			}
			{
				if($fml/fml:E_SMS_TOKEN_FREE_COUNT)
					then &lt;m:SmsTokenFreeCount>{ data($fml/fml:E_SMS_TOKEN_FREE_COUNT) }&lt;/m:SmsTokenFreeCount>
					else ()
			}
			{
				if($fml/fml:E_TRIES)
					then &lt;m:Tries>{ data($fml/fml:E_TRIES) }&lt;/m:Tries>
					else ()
			}
			{
				if($fml/fml:E_TRIES_ALLOWED)
					then &lt;m:TriesAllowed>{ data($fml/fml:E_TRIES_ALLOWED) }&lt;/m:TriesAllowed>
					else ()
			}
			{
				if($fml/fml:E_LOGIN_COUNT)
					then &lt;m:LoginCount>{ data($fml/fml:E_LOGIN_COUNT) }&lt;/m:LoginCount>
					else ()
			}
			{
				if($fml/fml:E_BAD_LOGIN_COUNT)
					then &lt;m:BadLoginCount>{ data($fml/fml:E_BAD_LOGIN_COUNT) }&lt;/m:BadLoginCount>
					else ()
			}
			{
				if($fml/fml:E_LOGIN_ID)
					then &lt;m:LoginId>{ data($fml/fml:E_LOGIN_ID) }&lt;/m:LoginId>
					else ()
			}
			{
				if($fml/fml:E_TIME_STAMP)
					then &lt;m:TimeStamp>{ data($fml/fml:E_TIME_STAMP) }&lt;/m:TimeStamp>
					else ()
			}
		&lt;/m:bCEKEeTosiaNewResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbCEKEeTosiaNewResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>