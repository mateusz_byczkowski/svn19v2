<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeChnlSetRequest($req as element(m:eChnlSetRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:LoginId)
					then &lt;fml:E_LOGIN_ID>{ data($req/m:LoginId) }&lt;/fml:E_LOGIN_ID>
					else ()
			}
			{
				if($req/m:ChannelType)
					then &lt;fml:E_CHANNEL_TYPE>{ data($req/m:ChannelType) }&lt;/fml:E_CHANNEL_TYPE>
					else ()
			}
			{
				if($req/m:TimeStamp)
					then &lt;fml:E_TIME_STAMP>{ data($req/m:TimeStamp) }&lt;/fml:E_TIME_STAMP>
					else ()
			}
			{
				if($req/m:Pdata)
					then &lt;fml:E_PDATA>{ data($req/m:Pdata) }&lt;/fml:E_PDATA>
					else ()
			}
			{
				if($req/m:AdminMicroBranch)
					then &lt;fml:E_ADMIN_MICRO_BRANCH>{ data($req/m:AdminMicroBranch) }&lt;/fml:E_ADMIN_MICRO_BRANCH>
					else ()
			}
			{
				if($req/m:Tries)
					then &lt;fml:E_TRIES>{ data($req/m:Tries) }&lt;/fml:E_TRIES>
					else ()
			}
			{
				if($req/m:TriesAllowed)
					then &lt;fml:E_TRIES_ALLOWED>{ data($req/m:TriesAllowed) }&lt;/fml:E_TRIES_ALLOWED>
					else ()
			}
			{
				if($req/m:Status)
					then &lt;fml:E_STATUS>{ data($req/m:Status) }&lt;/fml:E_STATUS>
					else ()
			}
			{
				if($req/m:Expiry)
					then &lt;fml:E_EXPIRY>{ data($req/m:Expiry) }&lt;/fml:E_EXPIRY>
					else ()
			}
			{
				if($req/m:ChangePeriod)
					then &lt;fml:E_CHANGE_PERIOD>{ data($req/m:ChangePeriod) }&lt;/fml:E_CHANGE_PERIOD>
					else ()
			}
			{
				if($req/m:TrnMask)
					then &lt;fml:E_TRN_MASK>{ data($req/m:TrnMask) }&lt;/fml:E_TRN_MASK>
					else ()
			}
			{
				if($req/m:ChannelLimit)
					then &lt;fml:E_CHANNEL_LIMIT>{ data($req/m:ChannelLimit) }&lt;/fml:E_CHANNEL_LIMIT>
					else ()
			}
			{
				if($req/m:ChannelCycle)
					then &lt;fml:E_CHANNEL_CYCLE>{ data($req/m:ChannelCycle) }&lt;/fml:E_CHANNEL_CYCLE>
					else ()
			}
			{
				if($req/m:ChannelOptions)
					then &lt;fml:E_CHANNEL_OPTIONS>{ data($req/m:ChannelOptions) }&lt;/fml:E_CHANNEL_OPTIONS>
					else ()
			}
			{
				if($req/m:UserName)
					then &lt;fml:U_USER_NAME>{ data($req/m:UserName) }&lt;/fml:U_USER_NAME>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapeChnlSetRequest($body/m:eChnlSetRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>