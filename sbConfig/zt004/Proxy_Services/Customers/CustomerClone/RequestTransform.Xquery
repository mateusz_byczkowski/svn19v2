<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/CustomerClone/RequestTransform2/";
declare namespace ns0 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns-1 = "urn:cif.entities.be.dcl";
declare namespace ns2 = "urn:baseauxentities.be.dcl";

declare function xf:RequestTransform2($invoke1 as element(ns1:invoke))
    as element(ns0:cCustCloneRequest) {
        &lt;ns0:cCustCloneRequest>
            &lt;ns0:NumerKlienta>{ data($invoke1/ns1:customer/ns-1:Customer/ns-1:customerNumber) }&lt;/ns0:NumerKlienta>
            &lt;ns0:IdSys>{ xs:int( data($invoke1/ns1:idSys/ns2:StringHolder/ns2:value) ) }&lt;/ns0:IdSys>
        &lt;/ns0:cCustCloneRequest>
};

declare variable $invoke1 as element(ns1:invoke) external;

xf:RequestTransform2($invoke1)</con:xquery>
</con:xqueryEntry>