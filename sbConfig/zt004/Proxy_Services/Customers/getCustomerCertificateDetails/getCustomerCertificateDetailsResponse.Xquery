<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-12-15</con:description>
  <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:baseentities.be.dcl";
declare namespace ns0="urn:customercertificatedict.dictionaries.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:be.services.dcl";
declare namespace ns8="urn:customercertificates.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns7:invokeResponse>
  <ns7:customerCertificateOut>
    <ns8:CustomerCertificate>
      <ns8:registerDate?>{data($parm/NF_CUSTCE_REGISTERDATE)}</ns8:registerDate>
      <ns8:registerUserTelNo?>{data($parm/NF_CUSTCE_REGISTERUSERTELN)}</ns8:registerUserTelNo>
      <ns8:customCertificateDesc?>{data($parm/NF_CUSTCE_CUSTOMCERTIFICAT)}</ns8:customCertificateDesc>
      <ns8:preparationDate?>{data($parm/NF_CUSTCE_PREPARATIONDATE)}</ns8:preparationDate>
      <ns8:customerCertificateType>
        <ns0:CustomerCertificateType>
          <ns0:customerCertificateType?>{data($parm/NF_CUSTCT_CUSTOMERCERTIFIC)}</ns0:customerCertificateType>
          <ns0:description?>{data($parm/NF_CUSTCT_DESCRIPTION)}</ns0:description>
        </ns0:CustomerCertificateType>
      </ns8:customerCertificateType>
      <ns8:registerUser>
        <ns5:User>
          <ns5:userLastName?>{data($parm/NF_BASEU_USERLASTNAME)}</ns5:userLastName>
          <ns5:userID?>{data($parm/NF_BASEU_USERID)}</ns5:userID>
          <ns5:userFirstName?>{data($parm/NF_BASEU_USERFIRSTNAME)}</ns5:userFirstName>
        </ns5:User>
      </ns8:registerUser>
      <ns8:registerBranch>
        <ns3:BranchCode>
          <ns3:branchCode?>{data($parm/NF_BASEBC_BRANCHCODE)}</ns3:branchCode>
        </ns3:BranchCode>
      </ns8:registerBranch>
      <ns8:certificateKind>
        <ns0:CustomerCertificateKind>
          <ns0:customerCertificateKind?>{data($parm/NF_CUSTCK_CUSTOMERCERTIFIC)}</ns0:customerCertificateKind>
        </ns0:CustomerCertificateKind>
      </ns8:certificateKind>
      <ns8:certificateDelivery>
        <ns0:CustomerCertificateDelivery>
          <ns0:customerCertificateDelivery?>{data($parm/NF_CUSTCD_CUSTOMERCERTIFIC)}</ns0:customerCertificateDelivery>
        </ns0:CustomerCertificateDelivery>
      </ns8:certificateDelivery>
      <ns8:file>
        <ns1:FileHolder>
          <ns1:name?>{data($parm/NF_FILEH_NAME)}</ns1:name>
        </ns1:FileHolder>
      </ns8:file>
      <ns8:preparationUser>
        <ns5:User>
          <ns5:userLastName?>{data($parm/NF_USER_USERLASTNAME)}</ns5:userLastName>
          <ns5:userID?>{data($parm/NF_USER_USERID)}</ns5:userID>
          <ns5:userFirstName?>{data($parm/NF_USER_USERFIRSTNAME)}</ns5:userFirstName>
        </ns5:User>
      </ns8:preparationUser>
      <ns8:customerCertificateStatus>
        <ns0:CustomerCertificateStatus>
          <ns0:customerCertificateStatus?>{data($parm/NF_CUSTCS_CUSTOMERCERTIFIC)}</ns0:customerCertificateStatus>
        </ns0:CustomerCertificateStatus>
      </ns8:customerCertificateStatus>
      <ns8:applicationFile>
        <ns1:FileHolder>
          <ns1:name?>{data($parm/NF_WFCA_NAME)}</ns1:name>
        </ns1:FileHolder>
      </ns8:applicationFile>
      <ns8:customer>
        <ns2:Customer>
          <ns2:customerNumber?>{data($parm/NF_CUSTOM_CUSTOMERNUMBER)}</ns2:customerNumber>
        </ns2:Customer>
      </ns8:customer>
    </ns8:CustomerCertificate>
  </ns7:customerCertificateOut>
</ns7:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>