<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace mes = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";


declare function xf:map_CRMDelCProdAddrResponse($res as element(mes:CRMDelCustProdsAddressesResponse ))
    as element(fml:FML32) {
    &lt;fml:FML32>
    {
        let $SYS_RESPONSE := $res/mes:DelCustProdsAddressesResponse

        for $it at $p in $res/mes:DelCustProdsAddressesResponse
        return
        (          
            &lt;fml:B_SYS>{ data($SYS_RESPONSE[$p]/mes:idSystemu) }&lt;/fml:B_SYS>,
            &lt;fml:E_STATUS>{ data($SYS_RESPONSE[$p]/mes:statusPrzetwarzania) }&lt;/fml:E_STATUS>,
            &lt;fml:E_RESPONSE_CODE>{ data($SYS_RESPONSE[$p]/mes:kodBledu) }&lt;/fml:E_RESPONSE_CODE>,
            &lt;fml:DC_OPIS_BLEDU>{ data($SYS_RESPONSE[$p]/mes:opisBledu) }&lt;/fml:DC_OPIS_BLEDU>
        )

    }   
    &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_CRMDelCProdAddrResponse($body/mes:CRMDelCustProdsAddressesResponse) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>