<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace mes = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";


declare function xf:map_CRMDelCProdAddrRequest($fml as element(fml:FML32))
    as element(mes:CRMDelCustProdsAddressesRequest) {
        &lt;mes:CRMDelCustProdsAddressesRequest>
            {
                let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA


                for $it1 at $p1 in $fml/fml:DC_NUMER_KLIENTA
                return
                (
                    if($DC_NUMER_KLIENTA[$p1] and data($DC_NUMER_KLIENTA[$p1])) then
                    (
                        &lt;mes:numerKlienta>{ data($DC_NUMER_KLIENTA[$p1])  }&lt;/mes:numerKlienta>
                    )
                    else () 
                )                
            }
            &lt;mes:trnId>{ data($fml/fml:DC_TRN_ID) }&lt;/mes:trnId>
            &lt;mes:uzytkownik>{ data($fml/fml:DC_UZYTKOWNIK) }&lt;/mes:uzytkownik>
            &lt;mes:oddzial>{ data($fml/fml:DC_ODDZIAL) }&lt;/mes:oddzial>
        &lt;/mes:CRMDelCustProdsAddressesRequest>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_CRMDelCProdAddrRequest($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>