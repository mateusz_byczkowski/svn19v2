<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Typ klienta domyślny to F</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:cif.entities.be.dcl";
declare namespace e2 = "urn:dictionaries.be.dcl";

declare function local:mapUpdateCustomersEntryPasswordRequest($req as element(), $head as element())
    as element(fml:FML32) {
        &lt;fml:FML32>
        	&lt;fml:DC_NUMER_KLIENTA>{ data($req/m:customer/e:Customer/e:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA>
        	&lt;fml:DC_NAZWISKO_PANIENSKIE_MATKI>{ data($req/m:customer/e:Customer/e:customerPersonal/e:CustomerPersonal/e:motherMaidenName) }&lt;/fml:DC_NAZWISKO_PANIENSKIE_MATKI>
                 {
                 if (string-length(data($req/m:customer/e:Customer/e:customerType/e2:CustomerType/e2:customerType)) > 0) then
        	     &lt;fml:DC_TYP_KLIENTA>{ data($req/m:customer/e:Customer/e:customerType/e2:CustomerType/e2:customerType) }&lt;/fml:DC_TYP_KLIENTA>
                else
                      &lt;fml:DC_TYP_KLIENTA>F&lt;/fml:DC_TYP_KLIENTA>
                 }
        	&lt;fml:CI_PRACOWNIK_ZMIEN>{ data($head/m:msgHeader/m:userId) }&lt;/fml:CI_PRACOWNIK_ZMIEN>
                {
                 if (string-length(data($req/m:customer/e:Customer/e:companyID/e2:CompanyType/e2:companyType)) > 0) then
                    &lt;CI_ID_SPOLKI>{data($req/m:customer/e:Customer/e:companyID/e2:CompanyType/e2:companyType)}&lt;/CI_ID_SPOLKI>
                 else
                    &lt;CI_ID_SPOLKI>{ data($head/m:msgHeader/m:companyId) }&lt;/CI_ID_SPOLKI>
                }
         &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body>
{ local:mapUpdateCustomersEntryPasswordRequest($body/m:invoke, $header/m:header) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>