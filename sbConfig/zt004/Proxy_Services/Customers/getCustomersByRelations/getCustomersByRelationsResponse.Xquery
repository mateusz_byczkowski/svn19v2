<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-09</con:description>
  <con:xquery>xquery version "1.0";

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";

declare function xf:mapDate($dateIn as xs:string) as xs:string {
	fn:concat(fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2))
};

declare function xf:mapgetCustomersByRelationsResponse($fml as element(fml:FML32), $rel as element(urn:customerRelationships))
	as element(urn:invokeResponse) {
		&lt;urn:invokeResponse>
			&lt;urn:customerList>
			{
				let $relations := distinct-values(data($rel/urn1:CustomerRelationship/urn1:relationType/urn2:CustomerRelations/urn2:customerRelations))
				let $DC_NUMER_KLIENTA_REL := $fml/fml:DC_NUMER_KLIENTA_REL
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $DC_TYP_RELACJI := $fml/fml:DC_TYP_RELACJI
				let $CI_BUDOWA_PORTFELA := $fml/fml:CI_BUDOWA_PORTFELA
				let $CI_OBSLUGA_ZADAN := $fml/fml:CI_OBSLUGA_ZADAN
				let $CI_KORESP_SERYJNA := $fml/fml:CI_KORESP_SERYJNA
				let $CI_TYP_REL_ODWROTNEJ := $fml/fml:CI_TYP_REL_ODWROTNEJ
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL
				let $DC_DATA_URODZENIA := $fml/fml:DC_DATA_URODZENIA
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA_REL
				return
					if((exists(index-of($relations, data($DC_TYP_RELACJI[$p])))) or (exists(index-of($relations, data($CI_TYP_REL_ODWROTNEJ[$p]))))) then
					(
						&lt;urn1:Customer>
						{
							&lt;urn1:customerNumber?>{ data($DC_NUMER_KLIENTA_REL[$p]) }&lt;/urn1:customerNumber>
						}
						{
							&lt;urn1:customerRelationshipList>
							{
								if (exists(index-of($relations, data($DC_TYP_RELACJI[$p]))))
								then
								(
									&lt;urn1:CustomerRelationship>
										&lt;urn1:relationType>
											&lt;urn2:CustomerRelations>
												&lt;urn2:customerRelations>{ data($DC_TYP_RELACJI[$p]) }&lt;/urn2:customerRelations>
											&lt;/urn2:CustomerRelations>
										&lt;/urn1:relationType>
									&lt;/urn1:CustomerRelationship>
								)
								else
								(),
								if (exists(index-of($relations, data($CI_TYP_REL_ODWROTNEJ[$p]))))
								then
								(
									&lt;urn1:CustomerRelationship>
										&lt;urn1:relationType>
											&lt;urn2:CustomerRelations>
												&lt;urn2:customerRelations>{ data($CI_TYP_REL_ODWROTNEJ[$p]) }&lt;/urn2:customerRelations>
											&lt;/urn2:CustomerRelations>
										&lt;/urn1:relationType>
									&lt;/urn1:CustomerRelationship>
								)
								else
								()
							}
							&lt;/urn1:customerRelationshipList>
						}
						{
							&lt;urn1:customerPersonal>
								&lt;urn1:CustomerPersonal>
									{
										if (fn:normalize-space(data($DC_DATA_URODZENIA[$p])))
										then
										(
											&lt;urn1:dateOfBirth>{ xf:mapDate(data($DC_DATA_URODZENIA[$p])) }&lt;/urn1:dateOfBirth>
										)
										else
										()
									}
									&lt;urn1:lastName?>{ data($DC_NAZWISKO[$p]) }&lt;/urn1:lastName>
									&lt;urn1:pesel?>{ data($DC_NR_PESEL[$p]) }&lt;/urn1:pesel>
									&lt;urn1:firstName?>{ data($DC_IMIE[$p]) }&lt;/urn1:firstName>
								&lt;/urn1:CustomerPersonal>
							&lt;/urn1:customerPersonal>
						}
						&lt;/urn1:Customer>
					) else ()
			}
			&lt;/urn:customerList>
		&lt;/urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetCustomersByRelationsResponse($body/fml:FML32, $body/urn:customerRelationships) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>