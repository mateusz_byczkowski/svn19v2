<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:hlbsentities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body>
  {
    let $req  := $body/m:invoke/m:customer/m1:CheckCustomerInMIGInput
    return

    &lt;fml:FML32>
      {if($req/m1:documentNumber)
         then &lt;fml:RB_NR_DOWODU>{ data($req/m1:documentNumber) }&lt;/fml:RB_NR_DOWODU>
         else ()
      }
      {if($req/m1:regon)
           then &lt;fml:RB_REGON>{ data($req/m1:regon) }&lt;/fml:RB_REGON>
           else ()
      }
      {if($req/m1:pesel)
           then &lt;fml:RB_PESEL>{ data($req/m1:pesel) }&lt;/fml:RB_PESEL>
           else ()
      }
    &lt;/fml:FML32>
  }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>