<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:cif.entities.be.dcl";
declare namespace w = "urn:dictionaries.be.dcl";


declare function local:mapGetCustomerEmploymentAddressRequest($req as element(), $head as element())
    as element(fml:FML32) {
        &lt;fml:FML32>
        	&lt;fml:DC_NUMER_KLIENTA>{ data($req/m:customer/e:Customer/e:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA>
        	&lt;fml:DC_TYP_KLIENTA>F&lt;/fml:DC_TYP_KLIENTA>
        	&lt;fml:CI_PRACOWNIK_ZMIEN>{ data($head/m:msgHeader/m:userId) }&lt;/fml:CI_PRACOWNIK_ZMIEN>
                {
                 if(string-length(data($req/m:customer/e:Customer/e:companyID/w:CompanyType/w:companyType)) > 0) then
        	    &lt;fml:CI_ID_SPOLKI>{data($req/m:customer/e:Customer/e:companyID/w:CompanyType/w:companyType)}&lt;/fml:CI_ID_SPOLKI>
                 else
                    &lt;fml:CI_ID_SPOLKI>{ data($head/m:msgHeader/m:companyId) }&lt;/fml:CI_ID_SPOLKI>
                }
        	&lt;fml:CI_CZAS_AKTUALIZACJI>{ fn-bea:dateTime-to-string-with-format("yyyy-MM-dd-HH.mm.ss.SSSSSS", data($head/m:msgHeader/m:timestamp)) }&lt;/fml:CI_CZAS_AKTUALIZACJI>
                &lt;fml:CI_OPCJA>4&lt;/fml:CI_OPCJA>
   &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body>
{ local:mapGetCustomerEmploymentAddressRequest($body/m:invoke, $header/m:header) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>