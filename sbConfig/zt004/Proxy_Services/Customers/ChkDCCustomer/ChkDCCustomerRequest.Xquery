<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbICBSChkDCCustomerRequest($req as element(m:bICBSChkDCCustomerRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL>{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL>
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE>{ data($req/m:Imie) }&lt;/fml:DC_IMIE>
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON>{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO>{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO>
					else ()
			}
			{
				if($req/m:DrugieImie)
					then &lt;fml:DC_DRUGIE_IMIE>{ data($req/m:DrugieImie) }&lt;/fml:DC_DRUGIE_IMIE>
					else &lt;fml:DC_DRUGIE_IMIE/>
			}
			{
				if($req/m:NrPosesLokaluDanePodst)
					then &lt;fml:DC_NR_POSES_LOKALU_DANE_PODST>{ data($req/m:NrPosesLokaluDanePodst) }&lt;/fml:DC_NR_POSES_LOKALU_DANE_PODST>
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then &lt;fml:DC_MIASTO_DANE_PODST>{ data($req/m:MiastoDanePodst) }&lt;/fml:DC_MIASTO_DANE_PODST>
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then &lt;fml:DC_KOD_POCZTOWY_DANE_PODST>{ data($req/m:KodPocztowyDanePodst) }&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST>
					else ()
			}
			{
				if($req/m:AdresOd)
					then &lt;fml:DC_ADRES_OD>{ data($req/m:AdresOd) }&lt;/fml:DC_ADRES_OD>
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then &lt;fml:DC_ULICA_DANE_PODST>{ data($req/m:UlicaDanePodst) }&lt;/fml:DC_ULICA_DANE_PODST>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbICBSChkDCCustomerRequest($body/m:bICBSChkDCCustomerRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>