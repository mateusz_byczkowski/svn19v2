<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:cif.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function local:mapcheckCustomerRelationRequest($req as element())
	as element(fml:FML32) {
		let $customerNumber := $req/m:customer/e:Customer/e:customerNumber
		return
			&lt;fml:FML32>
				&lt;fml:B_CIF_KLIENTA>{ data($customerNumber) }&lt;/fml:B_CIF_KLIENTA>
			&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ local:mapcheckCustomerRelationRequest($body/m:invoke) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>