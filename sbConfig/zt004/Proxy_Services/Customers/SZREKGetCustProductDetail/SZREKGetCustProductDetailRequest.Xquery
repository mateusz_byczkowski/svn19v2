<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Usunięcie pół CI_, DC_. Zmiana NF_CTRL_OPTION.</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/esbmessages/";
declare namespace xf = "http://bzwbk.com/services/esbmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetCustProductDetailRequest($req as element(m:getCustProductDetailRequest))
    as element(fml:FML32) {
        &lt;fml:FML32>
            {
                if($req/m:NumerKlienta)
                    then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER>{ data($req/m:NumerKlienta) }&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER>
                    else ()
            }
            {
                if(string-length($req/m:NrAlternatywny) = 0 )
                    then &lt;fml:NF_ACCOUN_ACCOUNTNUMBER>{ data($req/m:NrRachunku) }&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER>
                    else &lt;fml:NF_ACCOUN_ACCOUNTNUMBER>{ data($req/m:NrAlternatywny) }&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER>
            }
            {
                if($req/m:ProductCategoryId)
                    then &lt;fml:NF_PRODUC_IDPRODUCTCATEGOR>{ data($req/m:ProductCategoryId) }&lt;/fml:NF_PRODUC_IDPRODUCTCATEGOR>
                    else ()
            }
        
            &lt;fml:NF_CTRL_OPTION>1&lt;/fml:NF_CTRL_OPTION>
            &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE>0&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE>
            &lt;fml:NF_MSHEAD_COMPANYID>1&lt;/fml:NF_MSHEAD_COMPANYID>
            &lt;fml:NF_CTRL_ACTIVENONACTIVE>0&lt;/fml:NF_CTRL_ACTIVENONACTIVE>
            &lt;fml:NF_PRODUG_VISIBILITYSZREK>1&lt;/fml:NF_PRODUG_VISIBILITYSZREK>
        &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetCustProductDetailRequest($body/m:getCustProductDetailRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>