<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace prim = "http://bzwbk.com/services/prime/";
declare namespace xf="http://bzwbk.com/services/prime/functions";
declare namespace fml = "";
declare namespace none = "";
declare namespace fault="http://schemas.datacontract.org/2004/07/PrimeCustomerLibrary.Structures";
declare namespace rsp="http://schemas.datacontract.org/2004/07/PrimeCustomerLibrary.Structures";


declare variable $timestamp external;

declare function xf:mapErrorCode($errorCode as xs:string) as xs:string
{
   if ($errorCode = '1003') then
      '106'
   else if ($errorCode = '1001') then
      '107'
   else if ($errorCode = '1') then
      '102'
   else
       $errorCode
};


declare function xf:mapTimeStamp($timestamp as xs:string) as xs:string
{
        fn:concat (fn:substring($timestamp,1,10),"-",
                          fn:substring($timestamp,12,2),".",
                          fn:substring($timestamp,15,2),".",
                          fn:substring($timestamp,18),".000000")
};


declare function xf:mapResponse($resp as element (prim:GetCustomerDetailsResponse))
	as element (fml:FML32) {

&lt;fml:FML32>
			{
				&lt;fml:CI_CZAS_AKTUALIZACJI?>{xf:mapTimeStamp(data ($resp/prim:GetCustomerDetailsResult/rsp:CiCzasAktualizacji))}&lt;/fml:CI_CZAS_AKTUALIZACJI>,
				&lt;fml:DC_NUMER_KLIENTA?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcNumerKlienta)}&lt;/fml:DC_NUMER_KLIENTA>,
				&lt;fml:DC_TYP_KLIENTA?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcTypKlienta)}&lt;/fml:DC_TYP_KLIENTA>,
				&lt;fml:CI_ID_PORTFELA?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiIdPortfela)}&lt;/fml:CI_ID_PORTFELA>,
				&lt;fml:CI_ID_PORTFELA_ZASTEPCY?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiIdPortfelaZastepcy)}&lt;/fml:CI_ID_PORTFELA_ZASTEPCY>,
				&lt;fml:CI_ID_PORTFELA_CBK?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiIdPortfelaCbk)}&lt;/fml:CI_ID_PORTFELA_CBK>,
				&lt;fml:DC_SEGMENT_MARKETINGOWY?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcSegmentMarketingowy)}&lt;/fml:DC_SEGMENT_MARKETINGOWY>,
				&lt;fml:CI_PODSEGMENT_MARK?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiPodsegmentMark)}&lt;/fml:CI_PODSEGMENT_MARK>,
				&lt;fml:CI_KLASA_OBSLUGI?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiKlasaObslugi)}&lt;/fml:CI_KLASA_OBSLUGI>,
				&lt;fml:CI_DATA_WPROWADZENIA?>{data ($resp/prim:GetCustomerDetailsResult/rsp:EntryData)}&lt;/fml:CI_DATA_WPROWADZENIA>,
				&lt;fml:DC_REZ_NIEREZ?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcRezNierez)}&lt;/fml:DC_REZ_NIEREZ>,
        &lt;fml:DC_NR_DOWODU_REGON?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiSeriaNrDowodu)}&lt;/fml:DC_NR_DOWODU_REGON>, 
				&lt;fml:DC_NR_PESEL?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcNrPesel)}&lt;/fml:DC_NR_PESEL>,
				&lt;fml:CI_UDOSTEP_GRUPA?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiUdostepGrupa)}&lt;/fml:CI_UDOSTEP_GRUPA>,
        &lt;fml:CI_NR_ODDZIALU?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiNrOddzialu)}&lt;/fml:CI_NR_ODDZIALU>,
				&lt;fml:DC_NUMER_PASZPORTU?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcNumerPaszportu)}&lt;/fml:DC_NUMER_PASZPORTU>,
				&lt;fml:DC_ODPOWIEDZI?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcOdpowiedzi)}&lt;/fml:DC_ODPOWIEDZI>,
				&lt;fml:DC_PLEC?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcPlec)}&lt;/fml:DC_PLEC>,
        &lt;fml:DC_IMIE?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcImie)}&lt;/fml:DC_IMIE>,
				&lt;fml:DC_DRUGIE_IMIE?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcDrugieImie)}&lt;/fml:DC_DRUGIE_IMIE>,
				&lt;fml:DC_NAZWISKO?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcNazwisko)}&lt;/fml:DC_NAZWISKO>,
				&lt;fml:DC_DATA_URODZENIA?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcDataUrodzenia)}&lt;/fml:DC_DATA_URODZENIA>,
        &lt;fml:CI_NAZWA_PRACODAWCY?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiNazwaPracodawcy)}&lt;/fml:CI_NAZWA_PRACODAWCY>,
				&lt;fml:CI_STATUS_GIODO?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiStatusGiodo)}&lt;/fml:CI_STATUS_GIODO>,
				&lt;fml:CI_OBYWATELSTWO?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiObywatelstwo)}&lt;/fml:CI_OBYWATELSTWO>,
				&lt;fml:DC_ADRES_E_MAIL?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcAdresEMail)}&lt;/fml:DC_ADRES_E_MAIL>,
        &lt;fml:CI_HASLO_W_CC?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiHasloWCc)}&lt;/fml:CI_HASLO_W_CC>,
				&lt;fml:DC_ULICA_DANE_PODST?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcUlicaDanePodst)}&lt;/fml:DC_ULICA_DANE_PODST>,
				&lt;fml:DC_NR_POSES_LOKALU_DANE_PODST?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcNrPosesDanePodst)}&lt;/fml:DC_NR_POSES_LOKALU_DANE_PODST>, (:DC_NR_POSES_LOKALU_DANE_PODST:)
				&lt;fml:DC_MIASTO_DANE_PODST?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcMiastoDanePodst)}&lt;/fml:DC_MIASTO_DANE_PODST>,
				&lt;fml:DC_KOD_POCZTOWY_DANE_PODST?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcKodPocztowyDanePodst)}&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST>,
				&lt;fml:CI_KOD_KRAJU?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiKodKrajuDanePodst)}&lt;/fml:CI_KOD_KRAJU>, (:CI_KOD_KRAJU:)
				&lt;fml:CI_ULICA_PRACODAWCY?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiUlicaPracodawcy)}&lt;/fml:CI_ULICA_PRACODAWCY>,
				&lt;fml:CI_NUMER_POSESJI_PRAC?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiNumerPosesjiPrac)}&lt;/fml:CI_NUMER_POSESJI_PRAC>,
				&lt;fml:CI_MIASTO_PRACODAWCY?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiMiastoPrac)}&lt;/fml:CI_MIASTO_PRACODAWCY>,
				&lt;fml:CI_KOD_POCZT_PRAC?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiKodPocztowyPrac)}&lt;/fml:CI_KOD_POCZT_PRAC>,
				&lt;fml:CI_KOD_KRAJU_PRAC?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiKodKrajuPrac)}&lt;/fml:CI_KOD_KRAJU_PRAC>,
                                &lt;fml:DC_ULICA_ADRES_ALT?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcUlicaAdresAlt)}&lt;/fml:DC_ULICA_ADRES_ALT>,
				&lt;fml:DC_NR_POSES_LOKALU_ADRES_ALT?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcNrPosesAdresAlt)}&lt;/fml:DC_NR_POSES_LOKALU_ADRES_ALT>, (:DC_NR_POSES_LOKALU_ADRES_ALT:)
				&lt;fml:DC_MIASTO_ADRES_ALT?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcMiastoAdresAlt)}&lt;/fml:DC_MIASTO_ADRES_ALT>,
				&lt;fml:DC_KOD_POCZTOWY_ADRES_ALT?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcKodPocztowyAdresAlt)}&lt;/fml:DC_KOD_POCZTOWY_ADRES_ALT>,
				&lt;fml:CI_KOD_KRAJU_KORESP?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiKodKrajuKoresp)}&lt;/fml:CI_KOD_KRAJU_KORESP>,
				&lt;fml:CI_DATA_OD?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiDataOd)}&lt;/fml:CI_DATA_OD>,
				&lt;fml:CI_DATA_DO?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiDataDo)}&lt;/fml:CI_DATA_DO>,
				&lt;fml:CI_TIMESTAMP_CIS?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiTimestampCis)}&lt;/fml:CI_TIMESTAMP_CIS>,
	                        &lt;fml:CI_PRACOWNIK_ZMIEN?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiPracownikZmien)}&lt;/fml:CI_PRACOWNIK_ZMIEN>,
			        &lt;fml:CI_NR_ZRODLOWY_KLIENTA?>{data ($resp/prim:GetCustomerDetailsResult/rsp:NrZrodlKlienta)}&lt;/fml:CI_NR_ZRODLOWY_KLIENTA>,
			        &lt;fml:DC_DATA_SMIERCI?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcDataSmierci)}&lt;/fml:DC_DATA_SMIERCI>,
                                &lt;fml:DC_NR_TELEF_KOMORKOWEGO?>{data ($resp/prim:GetCustomerDetailsResult/rsp:DcNrTelefKomorkowego)}&lt;/fml:DC_NR_TELEF_KOMORKOWEGO>,
                                &lt;fml:CI_ZGODA_INF_HANDLOWA?>{data ($resp/prim:GetCustomerDetailsResult/rsp:CiZgodaInfHandlowa)}&lt;/fml:CI_ZGODA_INF_HANDLOWA>,
                                &lt;fml:DC_OPIS_BLEDU />,
				&lt;fml:CI_KOD_BLEDU>0&lt;/fml:CI_KOD_BLEDU>
                              	}
		&lt;/fml:FML32>
};


declare function xf:mapFault($fau as element(soap-env:Fault))
	as element (fml:FML32) {

	&lt;fml:FML32>
		&lt;fml:CI_KOD_BLEDU>{ data($fau/detail/fault:WbkFault/fault:ErrorCode2) }&lt;/fml:CI_KOD_BLEDU>
		&lt;fml:DC_OPIS_BLEDU> { data($fau/detail/fault:WbkFault/fault:ErrorDescription) } &lt;/fml:DC_OPIS_BLEDU>
	&lt;/fml:FML32>
};



declare variable $body as element(soap-env:Body) external;


&lt;soap-env:Body>
{
	if (boolean($body/prim:GetCustomerDetailsResponse)) then (
 		xf:mapResponse($body/prim:GetCustomerDetailsResponse)
 	) else if (boolean($body/soap-env:Fault/detail/fault:WbkFault/fault:ErrorCode2)) then (
 		xf:mapFault($body/soap-env:Fault)
	) else ()
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>