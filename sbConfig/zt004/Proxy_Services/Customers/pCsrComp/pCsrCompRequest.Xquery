<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/csr/messages/";
declare namespace xf = "http://bzwbk.com/services/csr/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappCsrCompRequest($req as element(m:pCsrCompRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:CompPep)
					then &lt;fml:C_COMP_PEP>{ data($req/m:CompPep) }&lt;/fml:C_COMP_PEP>
					else ()
			}
			{
				if($req/m:DataPep)
					then &lt;fml:C_DATA_PEP>{ data($req/m:DataPep) }&lt;/fml:C_DATA_PEP>
					else ()
			}
			{
				if($req/m:Pep)
					then &lt;fml:C_PEP>{ data($req/m:Pep) }&lt;/fml:C_PEP>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappCsrCompRequest($body/m:pCsrCompRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>