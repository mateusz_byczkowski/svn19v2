<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustCorpsRequest($req as element(m:CRMGetCustCorpsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				for $v in $req/m:IdSpolki
				return
					&lt;fml:CI_ID_SPOLKI>{ data($v) }&lt;/fml:CI_ID_SPOLKI>
			}
                        {
                                if ($req/m:TypFolderu)
                                        then &lt;fml:CI_TYP_FOLDERU>{ data($req/m:TypFolderu) }&lt;/fml:CI_TYP_FOLDERU>
                                        else ()
                        }
                        {
                                if ($req/m:IdWewPrac)
                                        then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
                                        else ()
                        }
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetCustCorpsRequest($body/m:CRMGetCustCorpsRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>