<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustCorpsResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustCorpsResponse) {
		&lt;m:CRMGetCustCorpsResponse>
			{
				for $p in $fml/fml:CI_ID_SPOLKI
				return
					&lt;m:IdSpolki> { data($p) }&lt;/m:IdSpolki>
			}
		&lt;/m:CRMGetCustCorpsResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetCustCorpsResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>