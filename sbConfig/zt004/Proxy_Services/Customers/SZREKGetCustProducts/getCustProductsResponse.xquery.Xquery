<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.3 2009-11-25 PKL Teet 42955: brak numerów ubezpieczeń kart
v.1.2.0 2009-11-19 PKL Teet 42561: przełączenie pola CI_PRODUCT_NAME_PL na NF_ACCOUN_ACCOUNTDESCRIPTI</con:description>
  <con:xquery>(: Change Log :)
(:             TSG 2009-04-29 Wersja początkowa :)
(: T41047 MMa 2009-10-05 Teet 41047 :)
(: v.1.2 2009-11-19 PKL Teet 42561: przełączenie pola CI_PRODUCT_NAME_PL na NF_ACCOUN_ACCOUNTDESCRIPTI :)
(: v.1.3 2009-11-25 PKL Teet 42955: brak numerów ubezpieczeń kart :)

declare namespace m = "http://bzwbk.com/services/esbmessages/";
declare namespace xf = "http://bzwbk.com/services/esbmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetCustProductsResponse($fml as element(fml:FML32))
	as element(m:getCustProductsResponse) {
		&lt;m:getCustProductsResponse>
			{

				let $CI_PRODUCT_CATEGORY_ID := $fml/fml:PT_ID_CATEGORY
				let $CI_PRODUCT_ID := $fml/fml:PT_ID_DEFINITION
				let $CI_PRODUCT_CODE := $fml/fml:CI_PRODUCT_CODE
(: T42561 :)			(: let $CI_PRODUCT_NAME_PL := $fml/fml:PT_POLISH_NAME :)
(: T42561 :)			let $CI_PRODUCT_NAME_PL := $fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI
				let $CI_PRODUCT_NAME_EN := $fml/fml:PT_ENGLISH_NAME
				let $DC_NR_RACHUNKU := $fml/fml:NF_ACCOUN_ACCOUNTNUMBER
				let $DC_NR_RACHUNKU_W_SYSTEMIE := $fml/fml:NF_DEBITC_VIRTUALCARDNBR
				let $B_POJEDYNCZY := $fml/fml:NF_IS_COOWNER
                                let $CI_RACHUNEK_ADR_ALT := $fml/fml:NF_ACCOUN_ALTERNATIVEADDRE
				let $CI_PRODUCT_ATT1 := $fml/fml:NF_ATTRPG_FIRSTPRODUCTFEAT
				let $CI_PRODUCT_ATT2 := $fml/fml:NF_ATTRPG_SECONDPRODUCTFEA
				let $CI_PRODUCT_ATT3 := $fml/fml:NF_ATTRPG_THIRDPRODUCTFEAT
				let $CI_PRODUCT_ATT4 := $fml/fml:NF_ATTRPG_FOURTHPRODUCTFEA
				let $CI_PRODUCT_ATT5 := $fml/fml:NF_ATTRPG_FIFTHPRODUCTFEAT				
                                let $NF_DEBITC_CARDNBR:=$fml/fml:NF_DEBITC_CARDNBR 
                                let $NF_INSURA_ID:=$fml/fml:NF_INSURA_ID


				   for $x at $p in $fml/fml:NF_ACCOUN_ACCOUNTNUMBER
				      return 
					&lt;m:getCustProductsprodukty>
					{
						if($CI_PRODUCT_CATEGORY_ID[$p])
							then &lt;m:ProductCategoryId>{ data($CI_PRODUCT_CATEGORY_ID[$p]) }&lt;/m:ProductCategoryId>
						else ()
					}
					{
						if($CI_PRODUCT_ID[$p])
							then &lt;m:ProductId>{ data($CI_PRODUCT_ID[$p]) }&lt;/m:ProductId>
						else ()
					}
					{
						if($CI_PRODUCT_CODE[$p])
							then &lt;m:ProductCode>{ data($CI_PRODUCT_CODE[$p]) }&lt;/m:ProductCode>
						else ()
					}
					{
						if($CI_PRODUCT_NAME_PL[$p])
							then &lt;m:ProductNamePl>{ data($CI_PRODUCT_NAME_PL[$p]) }&lt;/m:ProductNamePl>
						else ()
					}
					{
						if($CI_PRODUCT_NAME_EN[$p])
							then &lt;m:ProductNameEn>{ data($CI_PRODUCT_NAME_EN[$p]) }&lt;/m:ProductNameEn>
						else ()
					}
					{
						if($NF_DEBITC_CARDNBR[$p] and string-length($NF_DEBITC_CARDNBR[$p])>0)
						  then &lt;m:NrRachunku>{ data($NF_DEBITC_CARDNBR[$p]) }&lt;/m:NrRachunku>
						  else 
(: T41047 :)                                 (: if($NF_INSURA_ID[$p] and string-length($NF_INSURA_ID[$p])>0) :)
(: T41047 :)(: T42561 :)                (: if($NF_INSURA_ID[$p] and string-length($NF_INSURA_ID[$p])>0 and $NF_INSURA_ID>0) :)
(: T42561 :)                                     if($NF_INSURA_ID[$p] and string-length($NF_INSURA_ID[$p])>0 and data($NF_INSURA_ID[$p])!="0")
							then &lt;m:NrRachunku>{ data($NF_INSURA_ID[$p]) }&lt;/m:NrRachunku>
                                                         else &lt;m:NrRachunku>{ data($DC_NR_RACHUNKU[$p]) }&lt;/m:NrRachunku>
					}
					{
						if($DC_NR_RACHUNKU_W_SYSTEMIE[$p])
							then &lt;m:NrAlternatywny>{ data($DC_NR_RACHUNKU_W_SYSTEMIE[$p]) }&lt;/m:NrAlternatywny>
						else ()
					}
					{
						if($B_POJEDYNCZY[$p])
							then &lt;m:Pojedynczy>{ data($B_POJEDYNCZY[$p]) }&lt;/m:Pojedynczy>
						else ()
					}
					{
						if($CI_RACHUNEK_ADR_ALT[$p])
						  then if (data($CI_RACHUNEK_ADR_ALT[$p]) = "1")
                                                                                                     then &lt;m:RachunekAdrAlt>T&lt;/m:RachunekAdrAlt>
                                                                                                     else  &lt;m:RachunekAdrAlt>N&lt;/m:RachunekAdrAlt>
						else 
                                                                                                   &lt;m:RachunekAdrAlt>N&lt;/m:RachunekAdrAlt>
					}
					{
						if($CI_PRODUCT_ATT1[$p])
							then &lt;m:ProductAtt1>{ data($CI_PRODUCT_ATT1[$p]) }&lt;/m:ProductAtt1>
						else ()
					}
					{
						if($CI_PRODUCT_ATT2[$p])
							then &lt;m:ProductAtt2>{ data($CI_PRODUCT_ATT2[$p]) }&lt;/m:ProductAtt2>
						else ()
					}
					{
						if($CI_PRODUCT_ATT3[$p])
							then &lt;m:ProductAtt3>{ data($CI_PRODUCT_ATT3[$p]) }&lt;/m:ProductAtt3>
						else ()
					}
					{
						if($CI_PRODUCT_ATT4[$p])
							then &lt;m:ProductAtt4>{ data($CI_PRODUCT_ATT4[$p]) }&lt;/m:ProductAtt4>
						else ()
					}
					{

						if($CI_PRODUCT_ATT5[$p])
							then &lt;m:ProductAtt5>{ data($CI_PRODUCT_ATT5[$p]) }&lt;/m:ProductAtt5>
						else ()
					}
					&lt;/m:getCustProductsprodukty>
			}

		&lt;/m:getCustProductsResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetCustProductsResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>