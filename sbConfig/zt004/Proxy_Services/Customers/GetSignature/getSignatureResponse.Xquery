<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:hlbsentities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForSignature($parm as element(fml:FML32)) as element()
{

&lt;ns0:signature>
  {
    for $x at $occ in $parm/XXXXX
    return
    &lt;ns0:Image>
      &lt;ns3:image>{data($parm/NF_IMAGE_IMAGE[$occ])}&lt;/ns3:image>
      &lt;ns3:mimeType>{data($parm/NF_IMAGE_MIMETYPE[$occ])}&lt;/ns3:mimeType>
    &lt;/ns0:Image>
  }
&lt;/ns0:signature>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  {getElementsForSignature($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/ns0:invokeResponse)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>