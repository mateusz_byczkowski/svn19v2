<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappcCustNewResponse($fml as element(fml:FML32))
	as element(m:pcCustNewResponse) {
		&lt;m:pcCustNewResponse>
			{
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
				 &lt;m:NumerKlienta>{ data($fml/fml:DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta>
			}
			{

				let $CI_NR_KOM := $fml/fml:CI_NR_KOM
				let $CI_NAZWA_POLA := $fml/fml:CI_NAZWA_POLA
				let $DC_OPIS_BLEDU := $fml/fml:DC_OPIS_BLEDU
				for $it at $p in $fml/fml:CI_NR_KOM
				return
					&lt;m:Error>
					{
						if($CI_NR_KOM[$p])
							then &lt;m:NrKom>{ data($CI_NR_KOM[$p]) }&lt;/m:NrKom>
						else ()
					}
					{
						if($CI_NAZWA_POLA[$p])
							then &lt;m:NazwaPola>{ data($CI_NAZWA_POLA[$p]) }&lt;/m:NazwaPola>
						else ()
					}
					{
						if($DC_OPIS_BLEDU[$p])
							then &lt;m:OpisBledu>{ data($DC_OPIS_BLEDU[$p]) }&lt;/m:OpisBledu>
						else ()
					}
					&lt;/m:Error>
			}

		&lt;/m:pcCustNewResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappcCustNewResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>