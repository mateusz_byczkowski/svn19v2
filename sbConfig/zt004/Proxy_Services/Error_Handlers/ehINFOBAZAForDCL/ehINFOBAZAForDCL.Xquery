<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-01</con:description>
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace urn3="urn:be.services.dcl";
declare variable $body external;
declare variable $fault external;
declare variable $headerCache external;


declare function local:fault($faultCode as xs:string, $faultString as xs:string, $detail as element()?) as element(soap-env:Fault) {
	&lt;soap-env:Fault>
		&lt;faultcode>{ $faultCode }&lt;/faultcode> 
		&lt;faultstring>{ $faultString }&lt;/faultstring> 
		&lt;detail>{ $detail }&lt;/detail>
	&lt;/soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string,
        $errorDescription as xs:string) as element(err:exceptionItem) {
   &lt;err:exceptionItem>
	&lt;err:errorCode1>{ $errorCode1 }&lt;/err:errorCode1>
	&lt;err:errorCode2>{ $errorCode2 }&lt;/err:errorCode2>
        &lt;err:errorDescription>{ $errorDescription }&lt;/err:errorDescription>
   &lt;/err:exceptionItem>
};

&lt;soap-env:Body>
 {
	let $reason := data($fault/ctx:reason)
	let $defaultDesc := "Krytyczny błąd usługi w systemie źródłowym"
	let $alsbErrorDesc := "Wystąpił błąd w usłudze proxy na ALSB"
	let $errorDesc:=data($body/soap-env:Fault/faultstring)
	let $messageID := data($headerCache/urn3:header/urn3:msgHeader/urn3:msgId)

        let $errorCode := data($fault/ctx:errorCode)

        return
        if (data($body) != "") then
          (: $body/soap-env:Fault :)
          local:fault($errorCode, "General exception", element err:ServiceException { local:errors("0","0",concat($reason, concat(' (',concat($messageID, ')'))))})
        else if ($reason = "Not Found") then
          local:fault($errorCode, $reason, element err:ServiceException { local:errors("0","0",concat("Błąd połączenia z Infobazą", concat(' (',concat($messageID, ')'))))})
        else
          local:fault($errorCode, $reason, element err:ServiceException { local:errors("0","0",concat($reason, concat(' (',concat($messageID, ')'))))})
    }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>