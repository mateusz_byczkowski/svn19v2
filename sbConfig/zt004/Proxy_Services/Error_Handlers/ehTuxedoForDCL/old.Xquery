<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare variable $body external;

declare function local:fault($faultString as xs:string, $detail as xs:anyType) as element(soap-env:Fault) {
		&lt;soap-env:Fault>
			&lt;faultcode>soapenv:Server.userException&lt;/faultcode> 
			&lt;faultstring>{ $faultString }&lt;/faultstring> 
			&lt;detail>{ $detail }&lt;/detail>
		&lt;/soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string, $errorDescription as xs:string) as element(err:exceptionItem) {
   &lt;err:exceptionItem>
	&lt;err:errorCode1>{ $errorCode1 }&lt;/err:errorCode1>
	&lt;err:errorCode2>{ $errorCode2 }&lt;/err:errorCode2>
	&lt;err:errorDescription>{ $errorDescription }&lt;/err:errorDescription>
   &lt;/err:exceptionItem>
};

&lt;soap-env:Body>
	&lt;soap-env:Body>
	{
		(: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
		let $reason := fn:substring-after(fn:substring-before($body/ctx:fault/ctx:reason, "):"), "(")
		let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($body/ctx:fault/ctx:reason, "):"), ":"), ":")
		let $timeoutDesc := "Wysołanie usługi przekroczyło dopuszczalny czas odpowiedzi"
		let $wrongInputDesc := "Błędny komunikat wejściowy"
		let $dcInterfaceDownDesc := "Interfejs DC ICBS jest wyłączony - przetwarzanie niemożliwe"
		let $fmlBufferDesc := "Błąd bufora FML" 
		let $defaultUrcodeDesc := "Błąd wywołania usługi"
		let $defaultDesc := "Krytyczny bład usługi w systemie źródłowym"
		let $alsbErrorDesc := "Wystąpił bład w usłudze proxy na ALSB"
		
		
		return
                  if ($body/ctx:fault/ctx:errorCode = "BEA-380000") then
                    if($reason = "13") then
		       local:fault("err:TimeoutException", element err:TimeoutException { local:errors($reason, $urcode, $timeoutDesc) })
		    else  if($reason = "11") then
		      if($urcode = "100") then
	                local:fault("err:SystemException", element err:SystemException { local:errors($reason, $urcode, $dcInterfaceDownDesc) })
                      else if($urcode = "101") then
                        local:fault("err:SystemException", element err:SystemException { local:errors($reason, $urcode, $fmlBufferDesc) })
                      else if($urcode = "102") then
                        local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $wrongInputDesc) })
                      else
                        local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $defaultUrcodeDesc) })
                    else
                      local:fault("err:ServiceException", element err:ServiceException { local:errors($reason, $urcode,$defaultDesc) })
                  else
                    local:fault("err:SystemException", element err:SystemException { local:errors("0","0",$alsbErrorDesc) })
             }
	&lt;/soap-env:Body>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>