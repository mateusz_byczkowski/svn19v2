<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.5
 : @since   2009-12-16
 :
 : wersja WSDLa: 04-02-2010 15:47:16
 :
 : $Proxy Services/Till/getTillCashLimits/getTillCashLimitsRequest.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getTillCashLimits/getTillCashLimitsRequest/";
declare namespace ns0 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:entities.be.dcl";
declare namespace ns2 = "";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns4 = "urn:baseauxentities.be.dcl";

declare variable $header1 as element(ns3:header) external;
declare variable $invoke1 as element(ns3:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 : 
 : @return FML32 bufor XML/FML
 :)
declare function xf:getTillCashLimitsRequest($header1 as element(ns3:header),
											   $invoke1 as element(ns3:invoke))
    as element(ns2:FML32)
{
    &lt;ns2:FML32>

		(:
		 : dane z nagłówka komunikatu
		 :)		 
    	&lt;ns2:NF_MSHEAD_MSGID?>{
			data($header1/ns3:msgHeader/ns3:msgId)
		}&lt;/ns2:NF_MSHEAD_MSGID>
		
		(:
		 : dane do stronicowania
		 :)
        &lt;ns2:NF_PAGEC_ACTIONCODE?>{
			data($invoke1/ns3:bcd/ns1:BusinessControlData/ns1:pageControl/ns4:PageControl/ns4:actionCode)
		}&lt;/ns2:NF_PAGEC_ACTIONCODE>
		
        &lt;ns2:NF_PAGEC_PAGESIZE?>{
			data($invoke1/ns3:bcd/ns1:BusinessControlData/ns1:pageControl/ns4:PageControl/ns4:pageSize)
		}&lt;/ns2:NF_PAGEC_PAGESIZE>
		
        {
        	let $reverseOrder := $invoke1/ns3:bcd/ns1:BusinessControlData/ns1:pageControl/ns4:PageControl/ns4:reverseOrder
        	return
           	if (data($reverseOrder)) then
          		&lt;ns2:NF_PAGEC_REVERSEORDER>{
           			if (data($reverseOrder) eq "true") then
           				1
           			else
           				0
           		}&lt;/ns2:NF_PAGEC_REVERSEORDER>
           	else ()
        }
        
        &lt;ns2:NF_PAGEC_NAVIGATIONKEYDEFI?>{
			data($invoke1/ns3:bcd/ns1:BusinessControlData/ns1:pageControl/ns4:PageControl/ns4:navigationKeyDefinition)
		}&lt;/ns2:NF_PAGEC_NAVIGATIONKEYDEFI>
        		
        &lt;ns2:NF_PAGEC_NAVIGATIONKEYVALU?>{
			data($invoke1/ns3:bcd/ns1:BusinessControlData/ns1:pageControl/ns4:PageControl/ns4:navigationKeyValue)
		}&lt;/ns2:NF_PAGEC_NAVIGATIONKEYVALU>

		(:
		 : numer oddziału
		 :)
        &lt;ns2:NF_BRANCC_BRANCHCODE?>{
			data($invoke1/ns3:branchCode/ns0:BranchCode/ns0:branchCode)
		}&lt;/ns2:NF_BRANCC_BRANCHCODE>
		
    &lt;/ns2:FML32>
};

&lt;soap-env:Body>{
	xf:getTillCashLimitsRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>