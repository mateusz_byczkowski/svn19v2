<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-23</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns1:header" location="postWithdrawal.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns1:invoke" location="postWithdrawal.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns11:invokeResponse" location="postWithdrawal.wsdl" ::)
(:: pragma bea:global-element-return element="ns7:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns9 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns12 = "urn:transactionfeedict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns6 = "urn:acceptance.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns10 = "urn:entities.be.dcl";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace ns11 = "urn:be.services.dcl";
declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:feedict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:fee.operations.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/postWithdrawal/postWithdrawalTLRequest/";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare function xf:postWithdrawalTLRequest($header1 as element(ns1:header),
    $invoke1 as element(ns1:invoke),
    $invokeResponse1 as element(ns11:invokeResponse)?,
    $faultResponse as element() ?)
    
    as element(ns7:transactionLogEntry) {
        &lt;ns7:transactionLogEntry>
            {
                for $businessTransactionType in $invoke1/ns1:transaction/ns8:Transaction/ns8:businessTransactionType/ns5:BusinessTransactionType/ns5:businessTransactionType
                return
                    &lt;businessTransactionType>{ data($businessTransactionType) }&lt;/businessTransactionType>
            }
            {
                for $cashTransactionBasketID in $invoke1/ns1:transaction/ns8:Transaction/ns8:cashTransactionBasketID
                return
                    &lt;cashTransactionBasketID>{ data($cashTransactionBasketID) }&lt;/cashTransactionBasketID>
            }
            {
                for $TransactionMa in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa
                return
                    &lt;credit>
                        {
                            for $accountNumber in $TransactionMa/ns8:accountNumber
                            return
                                &lt;accountNumber>{ data($accountNumber) }&lt;/accountNumber>
                        }
                        {
                            for $address in $TransactionMa/ns8:address
                            return
                                &lt;address>{ data($address) }&lt;/address>
                        }
                        {
                            for $amountMa in $TransactionMa/ns8:amountMa
                            return
                                &lt;amount>{ xs:decimal( data($amountMa) ) }&lt;/amount>
                        }
                        {
                            for $amountMaEquiv in $TransactionMa/ns8:amountMaEquiv
                            return
                                &lt;amountPLN>{ xs:decimal( data($amountMaEquiv) ) }&lt;/amountPLN>
                        }
                        {
                            for $city in $TransactionMa/ns8:city
                            return
                                &lt;city>{ data($city) }&lt;/city>
                        }
                        {
                            for $currencyCode in $TransactionMa/ns8:currencyCode/ns4:CurrencyCode/ns4:currencyCode
                            return
                                &lt;currencyCode>{ data($currencyCode) }&lt;/currencyCode>
                        }
                        {
                            for $rate in $TransactionMa/ns8:rate
                            return
                                &lt;exchangeRate>{ xs:decimal( data($rate) ) }&lt;/exchangeRate>
                        }
                        {
                            for $name in $TransactionMa/ns8:name
                            return
                                &lt;name>{ data($name) }&lt;/name>
                        }
                        {
                            for $nameSecond in $TransactionMa/ns8:nameSecond
                            return
                                &lt;nameSecond>{ data($nameSecond) }&lt;/nameSecond>
                        }
                        {
                            for $zipCode in $TransactionMa/ns8:zipCode
                            return
                                &lt;zipCode>{ data($zipCode) }&lt;/zipCode>
                        }
                    &lt;/credit>
            }
            {
                for $csrMessageType in $invoke1/ns1:transaction/ns8:Transaction/ns8:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType
                return
                    &lt;csrMessageType>{ data($csrMessageType) }&lt;/csrMessageType>
            }
            {
                for $TransactionWn in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn
                return
                    &lt;debit>
                        {
                            for $accountNumber in $TransactionWn/ns8:accountNumber
                            return
                                &lt;accountNumber>{ data($accountNumber) }&lt;/accountNumber>
                        }
                        {
                            for $address in $TransactionWn/ns8:address
                            return
                                &lt;address>{ data($address) }&lt;/address>
                        }
                        {
                            for $amountWn in $TransactionWn/ns8:amountWn
                            return
                                &lt;amount>{ xs:decimal( data($amountWn) ) }&lt;/amount>
                        }
                        {
                            for $amountWnEquiv in $TransactionWn/ns8:amountWnEquiv
                            return
                                &lt;amountPLN>{ xs:decimal( data($amountWnEquiv) ) }&lt;/amountPLN>
                        }
                        {
                            for $city in $TransactionWn/ns8:city
                            return
                                &lt;city>{ data($city) }&lt;/city>
                        }
                        {
                            for $currencyCode in $TransactionWn/ns8:currencyCode/ns4:CurrencyCode/ns4:currencyCode
                            return
                                &lt;currencyCode>{ data($currencyCode) }&lt;/currencyCode>
                        }
                        {
                            for $rate in $TransactionWn/ns8:rate
                            return
                                &lt;exchangeRate>{ xs:decimal( data($rate) ) }&lt;/exchangeRate>
                        }
                        {
                            for $name in $TransactionWn/ns8:name
                            return
                                &lt;name>{ data($name) }&lt;/name>
                        }
                        {
                            for $nameSecond in $TransactionWn/ns8:nameSecond
                            return
                                &lt;nameSecond>{ data($nameSecond) }&lt;/nameSecond>
                        }
                        {
                            for $zipCode in $TransactionWn/ns8:zipCode
                            return
                                &lt;zipCode>{ data($zipCode) }&lt;/zipCode>
                        }
                    &lt;/debit>
            }
            {
                let $result :=
                    for $Disposer in $invoke1/ns1:transaction/ns8:Transaction/ns8:disposerList/ns8:Disposer
                    return
                        &lt;disposer1>
                            {
                                for $address in $Disposer/ns8:address
                                return
                                    &lt;address>{ data($address) }&lt;/address>
                            }
                            {
                                for $cif in $Disposer/ns8:cif
                                return
                                    &lt;cif>{ data($cif) }&lt;/cif>
                            }
                            {
                                for $citizenshipCode in $Disposer/ns8:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode
                                return
                                    &lt;citizenship>{ data($citizenshipCode) }&lt;/citizenship>
                            }
                            {
                                for $city in $Disposer/ns8:city
                                return
                                    &lt;city>{ data($city) }&lt;/city>
                            }
                            {
                                for $countryCode in $Disposer/ns8:countryCode/ns4:CountryCode/ns4:countryCode
                                return
                                    &lt;countryCode>{ data($countryCode) }&lt;/countryCode>
                            }
                            {
                                for $dateOfBirth in $Disposer/ns8:dateOfBirth
                                return
                                    &lt;dateOfBirth>{ xs:date(data($dateOfBirth) ) }&lt;/dateOfBirth>
                            }
                            {
                                for $documentNumber in $Disposer/ns8:documentNumber
                                return
                                    &lt;documentNumber>{ data($documentNumber) }&lt;/documentNumber>
                            }
                            {
                                for $documentTypeForTxn in $Disposer/ns8:documentType/ns5:DocumentTypeForTxn/ns5:documentTypeForTxn
                                return
                                    &lt;documentType>{ data($documentTypeForTxn) }&lt;/documentType>
                            }
                            {
                                for $firstName in $Disposer/ns8:firstName
                                return
                                    &lt;firstName>{ data($firstName) }&lt;/firstName>
                            }
                            {
                                for $lastName in $Disposer/ns8:lastName
                                return
                                    &lt;lastName>{ data($lastName) }&lt;/lastName>
                            }
                            {
                                for $pesel in $Disposer/ns8:pesel
                                return
                                    &lt;pesel>{ data($pesel) }&lt;/pesel>
                            }
                            {
                                for $zipCode in $Disposer/ns8:zipCode
                                return
                                    &lt;zipCode>{ data($zipCode) }&lt;/zipCode>
                            }
                        &lt;/disposer1>
                return
                    $result[1]
            }
            {
                let $result :=
                    for $Disposer in $invoke1/ns1:transaction/ns8:Transaction/ns8:disposerList/ns8:Disposer
                    return
                        &lt;disposer2>
                            {
                                for $address in $Disposer/ns8:address
                                return
                                    &lt;address>{ data($address) }&lt;/address>
                            }
                            {
                                for $cif in $Disposer/ns8:cif
                                return
                                    &lt;cif>{ data($cif) }&lt;/cif>
                            }
                            {
                                for $citizenshipCode in $Disposer/ns8:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode
                                return
                                    &lt;citizenship>{ data($citizenshipCode) }&lt;/citizenship>
                            }
                            {
                                for $city in $Disposer/ns8:city
                                return
                                    &lt;city>{ data($city) }&lt;/city>
                            }
                            {
                                for $countryCode in $Disposer/ns8:countryCode/ns4:CountryCode/ns4:countryCode
                                return
                                    &lt;countryCode>{ data($countryCode) }&lt;/countryCode>
                            }
                            {
                                for $dateOfBirth in $Disposer/ns8:dateOfBirth
                                return
                                    &lt;dateOfBirth>{ data($dateOfBirth) }&lt;/dateOfBirth>
                            }
                            {
                                for $documentNumber in $Disposer/ns8:documentNumber
                                return
                                    &lt;documentNumber>{ data($documentNumber) }&lt;/documentNumber>
                            }
                            {
                                for $documentTypeForTxn in $Disposer/ns8:documentType/ns5:DocumentTypeForTxn/ns5:documentTypeForTxn
                                return
                                    &lt;documentType>{ data($documentTypeForTxn) }&lt;/documentType>
                            }
                            {
                                for $firstName in $Disposer/ns8:firstName
                                return
                                    &lt;firstName>{ data($firstName) }&lt;/firstName>
                            }
                            {
                                for $lastName in $Disposer/ns8:lastName
                                return
                                    &lt;lastName>{ data($lastName) }&lt;/lastName>
                            }
                            {
                                for $pesel in $Disposer/ns8:pesel
                                return
                                    &lt;pesel>{ data($pesel) }&lt;/pesel>
                            }
                            {
                                for $zipCode in $Disposer/ns8:zipCode
                                return
                                    &lt;zipCode>{ data($zipCode) }&lt;/zipCode>
                            }
                        &lt;/disposer2>
                return
                    $result[2]
            }
            {
                for $dtTransactionType in $invoke1/ns1:transaction/ns8:Transaction/ns8:dtTransactionType/ns5:DtTransactionType/ns5:dtTransactionType
                return
                    &lt;dtTransactionType>{ data($dtTransactionType) }&lt;/dtTransactionType>
            }
            &lt;executor>
                {
                    for $branchCode in $invoke1/ns1:branchCode/ns4:BranchCode/ns4:branchCode
                    return
                        &lt;branchNumber>{ xs:int( data($branchCode) ) }&lt;/branchNumber>
                }
                {
                    for $userID in $invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:user/ns10:User/ns10:userID
                    return
                        &lt;executorID>{ data($userID) }&lt;/executorID>
                }
                {
                    for $userFirstName in $invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:user/ns10:User/ns10:userFirstName
                    return
                        &lt;firstName>{ data($userFirstName) }&lt;/firstName>
                }
                {
                    for $userLastName in $invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:user/ns10:User/ns10:userLastName
                    return
                        &lt;lastName>{ data($userLastName) }&lt;/lastName>
                }
                {
                    for $tellerID in $invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:teller/ns2:Teller/ns2:tellerID
                    return
                        &lt;tellerID>{ data($tellerID) }&lt;/tellerID>
                }
                {
                    for $tillID in $invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:till/ns2:Till/ns2:tillID
                    return
                        &lt;tillNumber>{ data($tillID) }&lt;/tillNumber>
                }

                {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
                (
                    for $beUserId in $invokeResponse1/ns1:backendResponse/ns8:BackendResponse/ns8:beUserId
                    return
                        &lt;userID>{ data($beUserId) }&lt;/userID>
                )
                	else
                	()
                }

            &lt;/executor>
            {
                for $extendedCSRMessageType in $invoke1/ns1:transaction/ns8:Transaction/ns8:extendedCSRMessageType/ns5:ExtendedCSRMessageType/ns5:extendedCSRMessageType
                return
                    &lt;extendedCSRMessageType>{ data($extendedCSRMessageType) }&lt;/extendedCSRMessageType>
            }
            &lt;hlbsName>postWithdrawal&lt;/hlbsName>
            {
                for $orderedBy in $invoke1/ns1:transaction/ns8:Transaction/ns8:orderedBy
                return
                    &lt;orderedBy>{ data($orderedBy) }&lt;/orderedBy>
            }
            {
                for $ordererID in $invoke1/ns1:transaction/ns8:Transaction/ns8:ordererID
                return
                    &lt;ordererID>{ data($ordererID) }&lt;/ordererID>
            }
            {
                for $putDownDate in $invoke1/ns1:transaction/ns8:Transaction/ns8:putDownDate
                return
                    &lt;putDownDate>{ xs:date (data($putDownDate) ) }&lt;/putDownDate>
            }
            &lt;timestamp>{ data($header1/ns1:msgHeader/ns1:timestamp) }&lt;/timestamp>
            {
                for $AcceptTask in $invoke1/ns1:transaction/ns8:Transaction/ns8:acceptTask/ns6:AcceptTask
                return
                    &lt;tlAcceptance?>
                        {
                            for $acceptorFirstName in $AcceptTask/ns6:acceptorFirstName
                            return
                                &lt;acceptorFirstName>{ data($acceptorFirstName) }&lt;/acceptorFirstName>
                        }
                        {
                            for $acceptorLastName in $AcceptTask/ns6:acceptorLastName
                            return
                                &lt;acceptorLastName>{ data($acceptorLastName) }&lt;/acceptorLastName>
                        }
                        {
                            for $acceptor in $AcceptTask/ns6:acceptor
                            return
                                &lt;acceptorSkp>{ data($acceptor) }&lt;/acceptorSkp>
                        }
                        {
                            for $AcceptItem in $AcceptTask/ns6:acceptItemList/ns6:AcceptItem
                            return
                                &lt;tlAcceptanceTitleList?>
                                    {
                                        for $acceptItemTitle in $AcceptItem/ns6:acceptItemTitle/ns9:AcceptItemTitle/ns9:acceptItemTitle
                                        return
                                            &lt;acceptItemTitle>{ data($acceptItemTitle) }&lt;/acceptItemTitle>
                                    }
                                &lt;/tlAcceptanceTitleList>
                        }
                    &lt;/tlAcceptance>
            }
            {
                for $AcceptanceForBE in $invoke1/ns1:transaction/ns8:Transaction/ns8:acceeptanceForBe/ns8:AcceptanceForBE
                return
                    &lt;tlAcceptanceForBE?>
                        {
                            for $acceptorFirstName in $AcceptanceForBE/ns8:acceptorFirstName
                            return
                                &lt;acceptorFirstName>{ data($acceptorFirstName) }&lt;/acceptorFirstName>
                        }
                        {
                            for $acceptorLastName in $AcceptanceForBE/ns8:acceptorLastName
                            return
                                &lt;acceptorLastName>{ data($acceptorLastName) }&lt;/acceptorLastName>
                        }
                        {
                            for $acceptorSKP in $AcceptanceForBE/ns8:acceptorSKP
                            return
                                &lt;acceptorSkp>{ data($acceptorSKP) }&lt;/acceptorSkp>
                        }
                        {
                            for $flag in $AcceptanceForBE/ns8:flag
                            return
                                &lt;flag>{ data($flag) }&lt;/flag>
                        }
                        {
                            for $BeErrorCode in $AcceptanceForBE/ns8:beErrorCodeList/ns8:BeErrorCode
                            return
                                &lt;tlErrorCodeList?>
                                    &lt;errorCode?>
                                        {
                                            for $errorCode in $BeErrorCode/ns8:errorCode/ns5:BackendErrorCode/ns5:errorCode
                                            return
                                                &lt;errorCode>{ data($errorCode) }&lt;/errorCode>
                                        }
                                        &lt;systemId>1&lt;/systemId>
                                    &lt;/errorCode>
                                &lt;/tlErrorCodeList>
                        }
                    &lt;/tlAcceptanceForBE>
            }
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                let $backendResponse := $invokeResponse1/ns1:backendResponse
                return
                    &lt;tlBackendResponse?>
                        {
                            for $balanceCredit in $backendResponse/ns8:BackendResponse/ns8:balanceCredit
                            return
                                &lt;balanceCredit>{ xs:decimal( data($balanceCredit) ) }&lt;/balanceCredit>
                        }
                        {
                            for $balanceDebit in $backendResponse/ns8:BackendResponse/ns8:balanceDebit
                            return
                                &lt;balanceDebit>{ xs:decimal( data($balanceDebit) ) }&lt;/balanceDebit>
                        }
                        {
                            for $dateTime in $backendResponse/ns8:BackendResponse/ns8:dateTime
                            return
                                &lt;dateTime>{ data($dateTime) }&lt;/dateTime>
                        }
                        {
                            for $icbsDate in $backendResponse/ns8:BackendResponse/ns8:icbsDate
                            return
                                &lt;icbsDate>{ xs:date( data($icbsDate) ) }&lt;/icbsDate>
                        }
                        {
                            for $icbsSessionNumber in $backendResponse/ns8:BackendResponse/ns8:icbsSessionNumber
                            return
                                &lt;icbsSessionNumber>{ data($icbsSessionNumber) }&lt;/icbsSessionNumber>
                        }
                        {
                            for $psTransactionNumber in $backendResponse/ns8:BackendResponse/ns8:psTransactionNumber
                            return
                                &lt;psTransactionNumber>{ data($psTransactionNumber) }&lt;/psTransactionNumber>
                        }
                        {
                            for $BeErrorCode in $backendResponse/ns8:BackendResponse/ns8:beErrorCodeList/ns8:BeErrorCode
                            return
                                &lt;tlErrorCodeList?>
                                    &lt;errorCode?>
                                        {
                                            for $errorCode in $BeErrorCode/ns8:errorCode/ns5:BackendErrorCode/ns5:errorCode
                                            return
                                                &lt;errorCode>{ data($errorCode) }&lt;/errorCode>
                                        }
                                        &lt;systemId>1&lt;/systemId>
                                    &lt;/errorCode>
                                &lt;/tlErrorCodeList>
                        }
                        {
                            for $transactionRefNumber in $backendResponse/ns8:BackendResponse/ns8:transactionRefNumber
                            return
                                &lt;transactionRefNumber>{ data($transactionRefNumber) }&lt;/transactionRefNumber>
                        }
                    &lt;/tlBackendResponse>
                    )
                            
                	else
                	()
            }
            &lt;tlExceptionDataList?>
            {
            	let $errorCode1 := data($faultResponse//err:exceptionItem/err:errorCode1)
            	let $errorCode2 := data($faultResponse//err:exceptionItem/err:errorCode2)
                let $errorDescription := data($faultResponse//err:exceptionItem/err:errorDescription)
            	return
                   if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') != '') then
            		(
		                &lt;errorCode1?>{ xs:int($errorCode1) }&lt;/errorCode1>,
        		        &lt;errorCode2?>{ xs:int($errorCode2) }&lt;/errorCode2>,
                		&lt;errorDescription?>{ $errorDescription }&lt;/errorDescription>
                	)
                	else
                	()
			}
            &lt;/tlExceptionDataList>
            
            {
                for $TransactionGLEsp in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionGLEsp/ns8:TransactionGLEsp
                return
                    &lt;tlGlTransactionESP?>
                        {
                            for $accountNbr in $TransactionGLEsp/ns8:accountNbr
                            return
                                &lt;accountNbr>{ data($accountNbr) }&lt;/accountNbr>
                        }
                        {
                            for $productID in $TransactionGLEsp/ns8:productID
                            return
                                &lt;productID>{ data($productID) }&lt;/productID>
                        }
                    &lt;/tlGlTransactionESP>
            }
            {
                for $TransactionFee in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionFee/ns0:TransactionFee
                return
                    &lt;tlTransactionFeeList?>
                        {
                            for $currencyCode in $TransactionFee/ns0:feeCurrency/ns4:CurrencyCode/ns4:currencyCode
                            return
                                &lt;feeCurrency>{ data($currencyCode) }&lt;/feeCurrency>
                        }
                        {
                            for $feeField in $TransactionFee/ns0:transactionFeeFieldList/ns0:TransactionFeeField[1]/ns0:feeField
                            return
                                &lt;feeField>{ data($feeField) }&lt;/feeField>
                        }
                        {
                            for $TransactionFeeBusiness in $TransactionFee/ns0:transactionFeeFieldList/ns0:TransactionFeeField[1]/ns0:transactionFeeBussinessList/ns0:TransactionFeeBusiness
                            return
                                &lt;tlTransactionFeeBussinessList?>
                                    {
                                        for $feeBusinessAmount in $TransactionFeeBusiness/ns0:feeBusinessAmount
                                        return
                                            &lt;feeBusinessAmount>{ xs:decimal( data($feeBusinessAmount) ) }&lt;/feeBusinessAmount>
                                    }
                                    {
                                        for $feeCalculatedAmount in $TransactionFeeBusiness/ns0:feeCalculatedAmount
                                        return
                                            &lt;feeCalculatedAmount>{ xs:decimal( data($feeCalculatedAmount) ) }&lt;/feeCalculatedAmount>
                                    }
                                    {
                                        for $feeID in $TransactionFeeBusiness/ns0:feeID/ns12:FeeDefinition/ns12:feeID
                                        return
                                            &lt;feeID>{ data($feeID) }&lt;/feeID>
                                    }
                                &lt;/tlTransactionFeeBussinessList>
                        }
                        {
                            for $TransactionFeeField in $TransactionFee/ns0:transactionFeeFieldList/ns0:TransactionFeeField
                            return
                                &lt;tlTransactionFeeFieldList?>
                                    {
                                        for $feeField in $TransactionFeeField/ns0:feeField
                                        return
                                            &lt;feeField>{ data($feeField) }&lt;/feeField>
                                    }
                                    {
                                        for $fieldFeeAmount in $TransactionFeeField/ns0:fieldFeeAmount
                                        return
                                            &lt;fieldFeeAmount>{ xs:decimal( data($fieldFeeAmount) ) }&lt;/fieldFeeAmount>
                                    }
                                    {
                                        for $TransactionFeeBusiness in $TransactionFeeField/ns0:transactionFeeBussinessList/ns0:TransactionFeeBusiness
                                        return
                                            &lt;tlTransactionFeeBussinessList?>
                                                {
                                                    for $feeCalculatedAmount in $TransactionFeeBusiness/ns0:feeCalculatedAmount
                                                    return
                                                        &lt;feeBusinessAmount>{ xs:decimal( data($feeCalculatedAmount) ) }&lt;/feeBusinessAmount>
                                                }
                                                {
                                                    for $feeBusinessAmount in $TransactionFeeBusiness/ns0:feeBusinessAmount
                                                    return
                                                        &lt;feeCalculatedAmount>{ xs:decimal( data($feeBusinessAmount) ) }&lt;/feeCalculatedAmount>
                                                }
                                                {
                                                    for $feeID in $TransactionFeeBusiness/ns0:feeID/ns12:FeeDefinition/ns12:feeID
                                                    return
                                                        &lt;feeID>{ data($feeID) }&lt;/feeID>
                                                }
                                            &lt;/tlTransactionFeeBussinessList>
                                    }
                                &lt;/tlTransactionFeeFieldList>
                        }
                        {
                            for $totalFeeAmount in $TransactionFee/ns0:totalFeeAmount
                            return
                                &lt;totalFeeAmount>{ xs:decimal( data($totalFeeAmount) ) }&lt;/totalFeeAmount>
                        }
                    &lt;/tlTransactionFeeList>
            }
            {
                for $transactionDate in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionDate
                return
                    &lt;transactionDate>{ xs:date( data($transactionDate) ) }&lt;/transactionDate>
            }
            
            
            
            {
                for $transactionGroupID in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionGroupID
                return
                    &lt;transactionGroupID>{ data($transactionGroupID) }&lt;/transactionGroupID>
            }
            &lt;transactionID>{ data($header1/ns1:transHeader/ns1:transId) }&lt;/transactionID>
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                for $transactionStatus in $invokeResponse1/ns1:transactionOut/ns8:Transaction/ns8:transactionStatus/ns4:TransactionStatus/ns4:transactionStatus
                return
                    &lt;transactionStatus>{ data($transactionStatus) }&lt;/transactionStatus>
                	)
                	else
                	()
            }
            {
                for $title in $invoke1/ns1:transaction/ns8:Transaction/ns8:title
                return
                    &lt;transactionTitle>{ data($title) }&lt;/transactionTitle>
            }
        &lt;/ns7:transactionLogEntry>
};

declare variable $header1 as element(ns1:header) external;
declare variable $invoke1 as element(ns1:invoke) external;
declare variable $invokeResponse1 as element(ns11:invokeResponse) ? external;
declare variable $faultResponse  as element() ? external;

&lt;soap-env:Body>{
xf:postWithdrawalTLRequest($header1,
    $invoke1,
    $invokeResponse1,
    $faultResponse)
    
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>