<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.2
 : @since   2009-12-18
 :
 : wersja WSDLa: 12-11-2009 11:07:43
 :
 : $Proxy Services/Till/getInternalCashTransportDetails/getInternalCashTransportDetails.xq$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getInternalCashTransportDetails/getInternalCashTransportDetailsRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:internaltransports.operations.entities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $header1 as element(ns2:header) external;
declare variable $invoke1 as element(ns2:invoke) external;

(:~
 : @param $invoke1 operacja wejściowa
 : @param $header1 nagłówek komunikatu
 :
 : @return FML32 bufor XML/FML
 :) 
declare function xf:getInternalCashTransportDetailsRequest($header1 as element(ns2:header),
															 $invoke1 as element(ns2:invoke))
	as element(ns0:FML32)
{
	&lt;ns0:FML32>

		(:
		 : dane z nagłówka
		 :)
		&lt;ns0:NF_MSHEAD_MSGID?>{
			data($header1/ns2:msgHeader/ns2:msgId)
		}&lt;/ns0:NF_MSHEAD_MSGID>
		
		(:
		 : dane wejściowe
		 :)
		&lt;ns0:NF_INTECT_TRANSPORTID?>{
			data($invoke1/ns2:internalCashTransport/ns1:InternalCashTransport/ns1:transportID)
		}&lt;/ns0:NF_INTECT_TRANSPORTID>
		
	&lt;/ns0:FML32>
};

&lt;soap-env:Body>{
	xf:getInternalCashTransportDetailsRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>