<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2009-12-17
 :
 : wersja WSDLa: 12-11-2009 11:07:43
 :
 : $Proxy Service/Till/addInternalCashTransport/addInternalCashTransportRequest.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/addInternalCashTransport/addInternalCashTransportRequest/";
declare namespace ns0 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:internaltransports.operations.entities.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";
declare namespace ns6 = "";

declare variable $invoke1 as element(ns5:invoke) external;
declare variable $header1 as element(ns5:header) external;

(:~
 : @param $invoke1 operacja wejściowa
 : @param $header1 nagłówek komunikatu
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:addInternalCashTransportRequest($invoke1 as element(ns5:invoke),
													  $header1 as element(ns5:header))
    as element(ns6:FML32)
{
	&lt;ns6:FML32>
	
		(:
		 : dane z nagłówka komunikatu
		 :)
       	&lt;ns6:DC_TRN_ID?>{
			data($header1/ns5:transHeader/ns5:transId)
		}&lt;/ns6:DC_TRN_ID>

		&lt;ns6:DC_ODDZIAL?>{
			data($header1/ns5:msgHeader/ns5:companyId)
		}&lt;/ns6:DC_ODDZIAL>
		
		&lt;ns6:DC_UZYTKOWNIK?>{
			data($header1/ns5:msgHeader/ns5:userId)
		}&lt;/ns6:DC_UZYTKOWNIK>
		
		(:
		 : dane z operacji wejściowej
		 :)				
		&lt;ns6:NF_INTECT_SOURCENAME?>{
			data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:sourceName)
		}&lt;/ns6:NF_INTECT_SOURCENAME>
			
		&lt;ns6:NF_INTECT_TARGETNAME?>{
			data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:targetName)
		}&lt;/ns6:NF_INTECT_TARGETNAME>
			
		{
			for $i in 1 to count($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:currencyCashList/ns1:CurrencyCash)
            return
            	for $j in 1 to count($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:currencyCashList/ns1:CurrencyCash[$i]/ns1:denominationSpecificationList/ns1:DenominationSpecification/ns1:itemsNumber)
            	return
            	(
	            	&lt;ns6:NF_CURRCA_AMOUNT>{
						data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:currencyCashList/ns1:CurrencyCash[$i]/ns1:amount)
					}&lt;/ns6:NF_CURRCA_AMOUNT>
					,            	
            		&lt;ns6:NF_DENOMS_ITEMSNUMBER>{
	            		data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:currencyCashList/ns1:CurrencyCash[$i]/ns1:denominationSpecificationList/ns1:DenominationSpecification[$j]/ns1:itemsNumber)
            		}&lt;/ns6:NF_DENOMS_ITEMSNUMBER>
            		,
           			&lt;ns6:NF_DENOMD_DENOMINATIONID>{
						data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:currencyCashList/ns1:CurrencyCash[$i]/ns1:denominationSpecificationList/ns1:DenominationSpecification[$j]/ns1:denomination/ns0:DenominationDefinition/ns0:denominationID)
           			}&lt;/ns6:NF_DENOMD_DENOMINATIONID>
           			,
					&lt;ns6:NF_CURREC_CURRENCYCODE>{
						data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:currencyCashList/ns1:CurrencyCash[$i]/ns1:currency/ns2:CurrencyCode/ns2:currencyCode)
					}&lt;/ns6:NF_CURREC_CURRENCYCODE>           			
				)
		}
		
		&lt;ns6:NF_TILL_TILLID?>{
			data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:sourceTill/ns3:Till/ns3:tillID)
		}&lt;/ns6:NF_TILL_TILLID>
			
		&lt;ns6:NF_TILL_TILLID2?>{
			data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:targetTill/ns3:Till/ns3:tillID)
		}&lt;/ns6:NF_TILL_TILLID2>
			
		&lt;ns6:NF_TELLER_TELLERID?>{
			data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:sourceTeller/ns3:Teller/ns3:tellerID)
		}&lt;/ns6:NF_TELLER_TELLERID>
			
		&lt;ns6:NF_TELLER_TELLERID2?>{
			data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:targetTeller/ns3:Teller/ns3:tellerID)
		}&lt;/ns6:NF_TELLER_TELLERID2>
			
		&lt;ns6:NF_BRACST_WORKDATE?>{
			data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:branchCurrentStatus/ns3:BranchCurrentStatus/ns3:workDate)
		}&lt;/ns6:NF_BRACST_WORKDATE>
			
		&lt;ns6:NF_BRANCC_BRANCHCODE?>{
			data($invoke1/ns5:internalCashTransport/ns4:InternalCashTransport/ns4:branchCurrentStatus/ns3:BranchCurrentStatus/ns3:branchCode/ns2:BranchCode/ns2:branchCode)
		}&lt;/ns6:NF_BRANCC_BRANCHCODE>
			
	&lt;/ns6:FML32>
};

&lt;soap-env:Body>{
	xf:addInternalCashTransportRequest($invoke1, $header1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>