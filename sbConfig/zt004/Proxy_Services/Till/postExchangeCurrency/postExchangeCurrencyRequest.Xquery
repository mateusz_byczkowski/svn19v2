<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns1:header" location="postExchangeCurrency.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns1:invoke" location="postExchangeCurrency.wsdl" ::)
(:: pragma bea:global-element-return element="ns7:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns9 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns6 = "urn:acceptance.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns10 = "urn:entities.be.dcl";
declare namespace ns11="urn:giif.operations.entities.be.dcl";
declare namespace ns7 = "";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:feedict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:fee.operations.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/postExchangeCurrency/postExchangeCurrencyRequest/";

declare function xf:postExchangeCurrencyRequest($header1 as element(ns1:header),
    $invoke1 as element(ns1:invoke))
    as element(ns7:FML32) {
        &lt;ns7:FML32>
            &lt;ns7:TR_ID_OPER?>{ data($header1/ns1:transHeader/ns1:transId) }&lt;/ns7:TR_ID_OPER>
            &lt;ns7:TR_DATA_OPER?>
                {
                    let $transactionDate  := ($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			&lt;/ns7:TR_DATA_OPER>
            &lt;ns7:TR_ODDZ_KASY?>{ xs:short( data($invoke1/ns1:branchCode/ns4:BranchCode/ns4:branchCode) ) }&lt;/ns7:TR_ODDZ_KASY>
            &lt;ns7:TR_KASA?>{ xs:short( data($invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:till/ns2:Till/ns2:tillID) ) }&lt;/ns7:TR_KASA>
            &lt;ns7:TR_KASJER?>{ xs:short( data($invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:teller/ns2:Teller/ns2:tellerID) ) }&lt;/ns7:TR_KASJER>
            &lt;ns7:TR_UZYTKOWNIK?>{ fn:concat("SKP:",data($invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:user/ns10:User/ns10:userID)) }&lt;/ns7:TR_UZYTKOWNIK>
            &lt;ns7:TR_ID_GR_OPER?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:cashTransactionBasketID) }&lt;/ns7:TR_ID_GR_OPER>
            &lt;ns7:TR_MSG_ID?>{ data($header1/ns1:msgHeader/ns1:msgId) }&lt;/ns7:TR_MSG_ID>
            &lt;ns7:TR_FLAGA_AKCEPT?>
                {
                
                    if (fn:boolean(data($invoke1/ns1:transaction/ns8:Transaction/ns8:acceeptanceForBe/ns8:AcceptanceForBE/ns8:flag))) then
                        (data($invoke1/ns1:transaction/ns8:Transaction/ns8:acceeptanceForBe/ns8:AcceptanceForBE/ns8:flag))
                    else 
                        'T'
                 }
			&lt;/ns7:TR_FLAGA_AKCEPT>
            
            &lt;ns7:TR_AKCEPTANT?>{ fn:concat("SKP:",data($invoke1/ns1:transaction/ns8:Transaction/ns8:acceptTask/ns6:AcceptTask/ns6:acceptor)) }&lt;/ns7:TR_AKCEPTANT>
            &lt;ns7:TR_TYP_KOM?>{ xs:short( data($invoke1/ns1:transaction/ns8:Transaction/ns8:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType) ) }&lt;/ns7:TR_TYP_KOM>
            &lt;ns7:TR_KANAL?>0&lt;/ns7:TR_KANAL>
			&lt;ns7:TR_KWOTA_NAD?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:amountWn) }&lt;/ns7:TR_KWOTA_NAD>
            &lt;ns7:TR_WALUTA_NAD?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:currencyCode/ns4:CurrencyCode/ns4:currencyCode) }&lt;/ns7:TR_WALUTA_NAD>
            &lt;ns7:TR_KURS_NAD?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:rate)}&lt;/ns7:TR_KURS_NAD>
            &lt;ns7:TR_ROWN_PLN_NAD?>{  data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:amountWnEquiv)}&lt;/ns7:TR_ROWN_PLN_NAD>
            &lt;ns7:TR_KWOTA_ADR?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:amountMa) }&lt;/ns7:TR_KWOTA_ADR>
            &lt;ns7:TR_WALUTA_ADR?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:currencyCode/ns4:CurrencyCode/ns4:currencyCode)  }&lt;/ns7:TR_WALUTA_ADR>
            &lt;ns7:TR_KURS_ADR?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:rate)}&lt;/ns7:TR_KURS_ADR>
            &lt;ns7:TR_ROWN_PLN_ADR?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:amountMaEquiv) }&lt;/ns7:TR_ROWN_PLN_ADR>
            &lt;ns7:TR_TYTUL?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:title) }&lt;/ns7:TR_TYTUL>
            &lt;ns7:TR_DATA_WALUTY?>
                {
                    let $transactionDate  := ($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			  &lt;/ns7:TR_DATA_WALUTY>
	          &lt;ns7:TR_CZAS_OPER?>
                {
                    let $transactionTime  := ($header1/ns1:msgHeader/ns1:timestamp)
  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
				&lt;/ns7:TR_CZAS_OPER>
    
    	     {
                for  $TransactionFeeField in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionFee/ns0:TransactionFee/ns0:transactionFeeFieldList/ns0:TransactionFeeField
                return
				(	
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_1")
                 then(
                    &lt;ns7:TR_OPLATA_1?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_1>)
                   else()
                   ,
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_2")
                 then(
                    &lt;ns7:TR_OPLATA_2?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_2>)
                  else()
                   ,
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_3")
                 then(
                    &lt;ns7:TR_OPLATA_3?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_3>)
                  else()
                   ,
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_4")
                 then(
                    &lt;ns7:TR_OPLATA_4?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_4>)
                  else()
                   ,
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_5")
                 then(
                    &lt;ns7:TR_OPLATA_5?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_5>)
                  else(),
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_6")
                  then(
                    &lt;ns7:TR_OPLATA_6?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_6>)
                  else()
                )
             }
	
	{
    for  $Disposer in $invoke1/ns1:transaction/ns8:Transaction/ns8:disposerList/ns8:Disposer
        let $firstName := data($Disposer/ns8:firstName)
	    let $lastName  := data($Disposer/ns8:lastName)
		return
		(
		&lt;ns7:TR_DYSP_CIF?>{
			data($Disposer/ns8:cif)
		}&lt;/ns7:TR_DYSP_CIF>,
	
				if ($firstName
				or  $lastName) then
					&lt;ns7:TR_DYSP_NAZWA?>{
						fn-bea:trim-right(fn:concat($firstName,' ', $lastName))
					}&lt;/ns7:TR_DYSP_NAZWA>
				else
					(),
		
		&lt;ns7:TR_DYSP_OBYWATELSTWO?>{
			data($Disposer/ns8:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode)
		}&lt;/ns7:TR_DYSP_OBYWATELSTWO>
	,
		&lt;ns7:TR_DYSP_KRAJ?>{
			data($Disposer/ns8:countryCode/ns4:CountryCode/ns4:countryCode)
		}&lt;/ns7:TR_DYSP_KRAJ>
	,
	
		&lt;ns7:TR_DYSP_MIEJSCOWOSC?>{
			data($Disposer/ns8:city)
		}&lt;/ns7:TR_DYSP_MIEJSCOWOSC>,
	
		&lt;ns7:TR_DYSP_KOD_POCZT?>{
			data($Disposer/ns8:zipCode)
		}&lt;/ns7:TR_DYSP_KOD_POCZT>,
	
		&lt;ns7:TR_DYSP_ULICA?>{
			data($Disposer/ns8:address)
		}&lt;/ns7:TR_DYSP_ULICA>,
	
		&lt;ns7:TR_DYSP_PESEL?>{
			data($Disposer/ns8:pesel)
		}&lt;/ns7:TR_DYSP_PESEL>,
	
		&lt;ns7:TR_DYSP_NR_PASZPORTU?>{
			data($Disposer/ns8:documentNumber)
		}&lt;/ns7:TR_DYSP_NR_PASZPORTU>,

                for $documentType in $Disposer/ns8:documentType/ns5:DocumentTypeForTxn
                return
                (
                    &lt;TR_DYSP_NR_DOWOD?>{ data($documentType/ns5:documentCodeForGIIF) }&lt;/TR_DYSP_NR_DOWOD>,
                    &lt;TR_DYSP_TELEFON?>{ data($documentType/ns5:commentForGIIF) }&lt;/TR_DYSP_TELEFON>
                )  

)	
}
	
	
            &lt;ns7:TR_AKCEPTANT_SKP?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:acceptTask/ns6:AcceptTask/ns6:acceptor) }&lt;/ns7:TR_AKCEPTANT_SKP>
            &lt;ns7:TR_UZYTKOWNIK_SKP?>{ data($invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:user/ns10:User/ns10:userID) }&lt;/ns7:TR_UZYTKOWNIK_SKP>
			&lt;ns7:TR_ZRODLO_FINAN?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionGIIF/ns11:TransactionGIIF/ns11:moneySource) }&lt;/ns7:TR_ZRODLO_FINAN>
        &lt;/ns7:FML32>
};

declare variable $header1 as element(ns1:header) external;
declare variable $invoke1 as element(ns1:invoke) external;

&lt;soap-env:Body>{
xf:postExchangeCurrencyRequest($header1,
    $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>