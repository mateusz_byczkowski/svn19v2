<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.2
 : @since   2009-12-16
 :
 : wersja WSDLa: 28-05-2009 14:06:42
 :
 : $Proxy Services/Till/chgInternalCashTransportStatus/chgInternalCashTransportStatusRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/chgInternalCashTransportStatus/chgInternalCashTransportStatusRequest/";
declare namespace ns0 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns2 = "urn:internaltransports.operations.entities.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns4 = "urn:internaltransportsdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns5 = "";

declare variable $invoke1 as element(ns3:invoke) external;
declare variable $header1 as element(ns3:header) external;

(:~
 : @param $invoke1 operacja wejściowa
 : @param $header1 nagłówek komunikatu
 :
 : @return FML32 bufor XML/FML
 :
 : Na wejściu można podać identyfikator transportu, numer oddziału
 : lub numer oddziału z numerem kasy.
 :
 : W przypadku podania numeru transportu numery oddziału i kasy
 : będą ignorowane.
 :)
declare function xf:chgInternalCashTransportStatusRequest($invoke1 as element(ns3:invoke),
															$header1 as element(ns3:header))
    as element(ns5:FML32)
{
    &lt;ns5:FML32>
    	(:
    	 : dane z nagłówka komunikatu
		 :)
    	&lt;ns5:DC_TRN_ID?>{
			data($header1/ns3:transHeader/ns3:transId)
		}&lt;/ns5:DC_TRN_ID>

    	&lt;ns5:DC_ODDZIAL?>{
			data($header1/ns3:msgHeader/ns3:companyId)
		}&lt;/ns5:DC_ODDZIAL>

    	&lt;ns5:DC_UZYTKOWNIK?>{
			data($header1/ns3:msgHeader/ns3:userId)
		}&lt;/ns5:DC_UZYTKOWNIK>

    	(:
		 : dane z operacji wejściowej
		 :)
        &lt;ns5:NF_INTECT_TRANSPORTID?>{
			data($invoke1/ns3:internalCashTransport/ns2:InternalCashTransport/ns2:transportID)
		}&lt;/ns5:NF_INTECT_TRANSPORTID>

		&lt;ns5:NF_INTCTS_INTERNALCASHTRAN?>{
				data($invoke1/ns3:internalCashTransport/ns2:InternalCashTransport/ns2:transportExtStatus/ns4:InternalCashTranspExtStatus/ns4:internalCashTranspExtStatus)
		}&lt;/ns5:NF_INTCTS_INTERNALCASHTRAN>

		&lt;ns5:NF_INTCTS_INTERNALCASHTRAN>{
				fn:substring(data($invoke1/ns3:internalCashTransport/ns2:InternalCashTransport/ns2:transportExtStatus/ns4:InternalCashTranspExtStatus/ns4:internalCashTranspExtStatus),1,1)
		}&lt;/ns5:NF_INTCTS_INTERNALCASHTRAN>
		
		{
			if (fn:not(data($invoke1/ns3:internalCashTransport/ns2:InternalCashTransport/ns2:transportID))) then
				&lt;ns5:NF_BRANCC_BRANCHCODE?>{
					data($invoke1/ns3:branchCode/ns0:BranchCode/ns0:branchCode)
				}&lt;/ns5:NF_BRANCC_BRANCHCODE>
			else ()
		}

		{
			if (fn:not(data($invoke1/ns3:internalCashTransport/ns2:InternalCashTransport/ns2:transportID))) then
				&lt;ns5:NF_TILL_TILLID?>{
					data($invoke1/ns3:till/ns1:Till/ns1:tillID)
				}&lt;/ns5:NF_TILL_TILLID>
			else ()
		}	

    &lt;/ns5:FML32>
};

&lt;soap-env:Body>{
	xf:chgInternalCashTransportStatusRequest($invoke1, $header1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>