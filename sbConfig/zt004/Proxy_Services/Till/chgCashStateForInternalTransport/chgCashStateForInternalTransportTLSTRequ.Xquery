<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-23</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="chgCashStateForInternalTransport.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="chgCashStateForInternalTransport.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$sourceTransactionResponse" element="FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$sourceTransactionException" element="FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$sourceDenomChgException" element="FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-return element="ns7:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns9 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:internaltransports.operations.entities.be.dcl";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Till/chgCashStateForInternalTransport_CR124/chgCashStateForInternalTransportTLSTRequest/";
declare namespace ns7 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";


declare function xf:chgCashStateForInternalTransportTLSTRequest($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke),
    $sourceTransactionResponse as element(soap-env:Body),
    $sourceTransactionException as element(FML32),
    $sourceDenomChgException as element(FML32))
    as element(ns7:transactionLogEntry) {
        &lt;ns7:transactionLogEntry>
            {
                for $businessTransactionType in $invoke1/ns0:sourceTransaction/ns8:Transaction/ns8:businessTransactionType/ns6:BusinessTransactionType/ns6:businessTransactionType
                return
                    &lt;businessTransactionType>{ data($businessTransactionType) }&lt;/businessTransactionType>
            }
            &lt;credit>
                {
                    for $amount in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:amount
                    return
                        &lt;amount>{ xs:decimal( data($amount) ) }&lt;/amount>
                }
                {
                    for $currencyCode in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:currency/ns4:CurrencyCode/ns4:currencyCode
                    return
                        &lt;currencyCode>{ data($currencyCode) }&lt;/currencyCode>
                }
            &lt;/credit>
            {
                for $csrMessageType in $invoke1/ns0:sourceTransaction/ns8:Transaction/ns8:csrMessageType/ns6:CsrMessageType/ns6:csrMessageType
                return
                    &lt;csrMessageType>{ data($csrMessageType) }&lt;/csrMessageType>
            }
            {
                for $dtTransactionType in $invoke1/ns0:sourceTransaction/ns8:Transaction/ns8:dtTransactionType/ns6:DtTransactionType/ns6:dtTransactionType
                return
                    &lt;dtTransactionType>{ data($dtTransactionType) }&lt;/dtTransactionType>
            }
            &lt;executor>
                {
                    for $branchCode in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:branchCurrentStatus/ns2:BranchCurrentStatus/ns2:branchCode/ns4:BranchCode/ns4:branchCode
                    return
                        &lt;branchNumber>{ xs:int( data($branchCode) ) }&lt;/branchNumber>
                }
                {
                    for $userID in $invoke1/ns0:sourceUser/ns9:User/ns9:userID
                    return
                        &lt;executorID>{ data($userID) }&lt;/executorID>
                }
                {
                    for $userFirstName in $invoke1/ns0:sourceUser/ns9:User/ns9:userFirstName
                    return
                        &lt;firstName>{ data($userFirstName) }&lt;/firstName>
                }
                {
                    for $userLastName in $invoke1/ns0:sourceUser/ns9:User/ns9:userLastName
                    return
                        &lt;lastName>{ data($userLastName) }&lt;/lastName>
                }
                {
                    for $tellerID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:sourceTeller/ns2:Teller/ns2:tellerID
                    return
                        &lt;tellerID>{ data($tellerID) }&lt;/tellerID>
                }
                {
                    for $tillID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:sourceTill/ns2:Till/ns2:tillID
                    return
                        &lt;tillNumber>{ data($tillID) }&lt;/tillNumber>
                }
                &lt;userID?>{ data($sourceTransactionResponse/FML32/TR_UZYTKOWNIK) }&lt;/userID>
            &lt;/executor>
            {
                for $extendedCSRMessageType in $invoke1/ns0:sourceTransaction/ns8:Transaction/ns8:extendedCSRMessageType/ns6:ExtendedCSRMessageType/ns6:extendedCSRMessageType
                return
                    &lt;extendedCSRMessageType>{ data($extendedCSRMessageType) }&lt;/extendedCSRMessageType>
            }
            &lt;hlbsName>chgCashStateForInternalTransport&lt;/hlbsName>
            {
                for $putDownDate in $invoke1/ns0:sourceTransaction/ns8:Transaction/ns8:putDownDate
                return
                    &lt;putDownDate>{ data($putDownDate) }&lt;/putDownDate>
            }
            &lt;timestamp>{ data($header1/ns0:msgHeader/ns0:timestamp) }&lt;/timestamp>
            &lt;tlCurrencyCashList>
                {
                    for $amount in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:amount
                    return
                        &lt;amount>{ xs:decimal( data($amount) )*(-1) }&lt;/amount>
                }
                {
                    for $currencyCode in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:currency/ns4:CurrencyCode/ns4:currencyCode
                    return
                        &lt;currency>{ data($currencyCode) }&lt;/currency>
                }
                {
                    for $DenominationSpecification in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:denominationSpecificationList/ns1:DenominationSpecification
                    return
                        &lt;tlDenominationSpecList>
                            {
                                for $denominationID in $DenominationSpecification/ns1:denomination/ns3:DenominationDefinition/ns3:denominationID
                                return
                                    &lt;denomination>{ data($denominationID) }&lt;/denomination>
                            }
                            {
                                for $itemsNumber in $DenominationSpecification/ns1:itemsNumber
                                return
                                    &lt;itemsNumber>{ xs:int(data($itemsNumber)*(-1)) }&lt;/itemsNumber>
                            }
                        &lt;/tlDenominationSpecList>
                }
            &lt;/tlCurrencyCashList>
            {
				for $errorDesc at $occ in $sourceTransactionException/FML_ERROR_DESCRIPTION
				let $errorCode1 := $sourceTransactionException/FML_ERROR_CODE1
				let $errorCode2 := $sourceTransactionException/FML_ERROR_CODE2
				return
					&lt;tlExceptionDataList?>{
						if (data($errorDesc) != '') then
						(
							&lt;errorCode1?>{ xs:int(data($errorCode1[$occ])) }&lt;/errorCode1>,
							&lt;errorCode2?>{ xs:int(data($errorCode2[$occ])) }&lt;/errorCode2>,
							&lt;errorDescription?>{ data($errorDesc) }&lt;/errorDescription>
						)
						else
						()
					}&lt;/tlExceptionDataList>
			}
            {
				for $errorDesc at $occ in $sourceDenomChgException/FML_ERROR_DESCRIPTION
				let $errorCode1 := $sourceDenomChgException/FML_ERROR_CODE1
				let $errorCode2 := $sourceDenomChgException/FML_ERROR_CODE2
				return
					&lt;tlExceptionDataList?>{
						if (data($errorDesc) != '') then
						(
							&lt;errorCode1?>{ xs:int(data($errorCode1[$occ])) }&lt;/errorCode1>,
							&lt;errorCode2?>{ xs:int(data($errorCode2[$occ])) }&lt;/errorCode2>,
							&lt;errorDescription?>{ data($errorDesc) }&lt;/errorDescription>
						)
						else
						()
					}&lt;/tlExceptionDataList>
			}
            &lt;tlInternalCashTransport>
                {
                    for $sourceName in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:sourceName
                    return
                        &lt;sourceName>{ data($sourceName) }&lt;/sourceName>
                }
                {
                    for $tellerID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:sourceTeller/ns2:Teller/ns2:tellerID
                    return
                        &lt;sourceTeller>{ data($tellerID) }&lt;/sourceTeller>
                }
                {
                    for $tillID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:sourceTill/ns2:Till/ns2:tillID
                    return
                        &lt;sourceTill>{ data($tillID) }&lt;/sourceTill>
                }
                {
                    for $targetName in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:targetName
                    return
                        &lt;targetName>{ data($targetName) }&lt;/targetName>
                }
                {
                    for $tellerID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:targetTeller/ns2:Teller/ns2:tellerID
                    return
                        &lt;targetTeller>{ data($tellerID) }&lt;/targetTeller>
                }
                {
                    for $tillID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:targetTill/ns2:Till/ns2:tillID
                    return
                        &lt;targetTill>{ data($tillID) }&lt;/targetTill>
                }
                {
                    for $transportID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:transportID
                    return
                        &lt;transportID>{ data($transportID) }&lt;/transportID>
                }
            &lt;/tlInternalCashTransport>
            {
                for $transactionDate in $invoke1/ns0:sourceTransaction/ns8:Transaction/ns8:transactionDate
                return
                    &lt;transactionDate>{ data($transactionDate) }&lt;/transactionDate>
            }
            {
                for $transactionGroupID in $invoke1/ns0:sourceTransaction/ns8:Transaction/ns8:transactionGroupID
                return
                    &lt;transactionGroupID>{ data($transactionGroupID) }&lt;/transactionGroupID>
            }
            {
                for $transactionID in $invoke1/ns0:sourceTransaction/ns8:Transaction/ns8:transactionID
                return
                    &lt;transactionID>{ data($transactionID) }&lt;/transactionID>
            }
            {
				let $tr_status := $sourceTransactionResponse/FML32/TR_STATUS
				return
					if (xs:boolean(data($tr_status)!='')) then
					(
						&lt;transactionStatus?>{ data($tr_status) }&lt;/transactionStatus>
					)
					else
					()
			}
        &lt;/ns7:transactionLogEntry>
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;
declare variable $sourceTransactionResponse as element(soap-env:Body) external;
declare variable $sourceTransactionException as element(FML32) external;
declare variable $sourceDenomChgException as element(FML32) external;
&lt;soap-env:Body>{
xf:chgCashStateForInternalTransportTLSTRequest($header1,
    $invoke1,
    $sourceTransactionResponse,
    $sourceTransactionException,
    $sourceDenomChgException)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>