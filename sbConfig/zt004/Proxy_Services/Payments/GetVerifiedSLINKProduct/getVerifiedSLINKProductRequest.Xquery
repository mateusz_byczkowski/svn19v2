<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-12-30</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:operations.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:swift.operations.entities.be.dcl";
declare namespace urn4 =  "urn:transactionswiftdict.operationsdictionary.dictionaries.be.dcl";
declare namespace urn5 = "urn:cif.entities.be.dcl";
declare namespace urn6 = "urn:productstree.entities.be.dcl";



declare function xf:mapGetSLINKProdRequest($req as element(urn:invoke),$head as element(urn:header))
	as element(fml:FML32) {
	&lt;fml:FML32>   
        {
        &lt;fml:S_SYSTEM_ID?>{ data($head/urn:msgHeader/urn:appId) }&lt;/fml:S_SYSTEM_ID>
        }
        {
	&lt;fml:S_SRC_ACCOUNT_NO?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionWn/urn1:TransactionWn/urn1:accountNumber)}&lt;/fml:S_SRC_ACCOUNT_NO>
	}
	{
	&lt;fml:S_SRC_ACCOUNT_CUR?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionWn/urn1:TransactionWn/urn1:currencyCode/urn2:CurrencyCode/urn2:currencyCode)}&lt;/fml:S_SRC_ACCOUNT_CUR> 
	}
	{
	&lt;fml:S_DST_ACCOUNT_NO?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionMa/urn1:TransactionMa/urn1:accountNumber)}&lt;/fml:S_DST_ACCOUNT_NO>
	}
	{
	&lt;fml:S_DST_AMOUNT?>{ data($req/urn:transaction/urn1:Transaction/urn1:transactionMa/urn1:TransactionMa/urn1:amountMa)}&lt;/fml:S_DST_AMOUNT>
	}
        {
	&lt;fml:S_DST_ACCOUNT_CUR?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionMa/urn1:TransactionMa/urn1:currencyCode/urn2:CurrencyCode/urn2:currencyCode)}&lt;/fml:S_DST_ACCOUNT_CUR>
	}
	{
	&lt;fml:S_PRODUCT_ID?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionSwift/urn3:TransactionSwift/urn3:slinkProductNumber)}&lt;/fml:S_PRODUCT_ID>
	}
	{
	&lt;fml:S_SWIFT_COST?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionSwift/urn3:TransactionSwift/urn3:costOption/urn4:SwiftCostOption/urn4:swiftCostOption)}&lt;/fml:S_SWIFT_COST>
	}
	{
	&lt;fml:S_PAYMENT_TYPE?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionSwift/urn3:TransactionSwift/urn3:executionMode/urn4:SwiftExecutionMode/urn4:swiftExecutionMode)}&lt;/fml:S_PAYMENT_TYPE>
	}
	{
	&lt;fml:S_SWIFT_ID?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionSwift/urn3:TransactionSwift/urn3:swiftBenefBank/urn3:SwiftBenefBank/urn3:branchBankId)}&lt;/fml:S_SWIFT_ID>
	}
	{
	&lt;fml:S_FEE_ACCOUNT_NO?>{data($req/urn:chargedAccount/urn1:TxnAccountData/urn1:accountNumber)}&lt;/fml:S_FEE_ACCOUNT_NO>
	}
	{	
	&lt;fml:S_FEE_ACCOUNT_CUR?>{data($req/urn:chargedAccount/urn1:TxnAccountData/urn1:currency/urn2:CurrencyCode/urn2:currencyCode)}&lt;/fml:S_FEE_ACCOUNT_CUR>
	}
	{
	&lt;fml:S_CUSTOMER_CIF?> {data($req/urn:customer/urn5:Customer/urn5:customerNumber)}&lt;/fml:S_CUSTOMER_CIF>
	}
        {
         if (data($req/urn:customer/urn5:Customer/urn5:resident)="TRUE") then    
        	&lt;fml:S_CUSTOMER_STATUS?>1&lt;/fml:S_CUSTOMER_STATUS>
         else  if (data($req/urn:customer/urn5:Customer/urn5:resident)="true") then    
        	         &lt;fml:S_CUSTOMER_STATUS?>1&lt;/fml:S_CUSTOMER_STATUS>
               else   if (data($req/urn:customer/urn5:Customer/urn5:resident)="FALSE") then    
                        	&lt;fml:S_CUSTOMER_STATUS?>0&lt;/fml:S_CUSTOMER_STATUS>
                       else if (data($req/urn:customer/urn5:Customer/urn5:resident)="false") then    
                                	&lt;fml:S_CUSTOMER_STATUS?>0&lt;/fml:S_CUSTOMER_STATUS>
                             else (&lt;fml:S_CUSTOMER_STATUS?>{data($req/urn:customer/urn5:Customer/urn5:resident)}&lt;/fml:S_CUSTOMER_STATUS>)
	}
	{
	&lt;fml:S_CUSTOMER_TYPE?>{data($req/urn:customer/urn5:Customer/urn5:customerType/urn2:CustomerType/urn2:customerType)}&lt;/fml:S_CUSTOMER_TYPE>
	}
	{
	&lt;fml:S_SRC_ACCOUNT_PRODUCT_ID?>{data($req/urn:productId/urn6:ProductDefinition/urn6:idProductDefinition)}&lt;/fml:S_SRC_ACCOUNT_PRODUCT_ID>
	}     

&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body>
{ xf:mapGetSLINKProdRequest($body/urn:invoke,$header/urn:header) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>