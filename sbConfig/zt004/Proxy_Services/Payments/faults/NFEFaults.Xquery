<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-12-30</con:description>
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="urn:errors.hlbsentities.be.dcl";
declare namespace urn3="urn:be.services.dcl";

declare variable $fault external;
declare variable $headerCache external;

declare function local:fault($faultString as xs:string, $detail as element()*) as element(soap-env:Fault) {
		&lt;soap-env:Fault>
			&lt;faultcode>soapenv:Server.userException&lt;/faultcode> 
			&lt;faultstring>{ $faultString }&lt;/faultstring> 
			&lt;detail>{ $detail }&lt;/detail>
		&lt;/soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string, $errorDescription as xs:string) as element()* {
         &lt;f:exceptionItem>
         	&lt;f:errorCode1>{ $errorCode1 }&lt;/f:errorCode1>
         	&lt;f:errorCode2>{ $errorCode2 }&lt;/f:errorCode2>
        	&lt;f:errorDescription>{ $errorDescription }&lt;/f:errorDescription>
         &lt;/f:exceptionItem>

};

declare function local:getShortUrcode($urcode as xs:string) as xs:string
{
  if (fn:string-length($urcode) = 5) 
      then
      (
	  fn:substring($urcode,4,2)	 
      )
      else
     (
       $urcode
     )
};

declare function local:getLongUrcode($urcode as xs:string) as xs:string
{
  if (fn:string-length($urcode) = 5) 
      then
      (	
	  fn:substring($urcode,1,3)
       )
       else
       (
	    if (fn:string-length($urcode) = 3)
               then 
               (
                     fn:substring($urcode,1,3)
               ) 
               else
               (
                     $urcode
               )
       )
   
};

declare function local:getLongUrcodeDescription($longUrcode as xs:string) as xs:string
{

        if ($longUrcode = "100" ) then                    
               'ParameterRequired'
        else if ($longUrcode = "110" ) then                    
               'TransactionParameterNotAllowed'
        else if ($longUrcode = "120" ) then                    
               'PRZEKROCZONA GODZINA GRANICZNA dla wybranego produktu'
         else ""
};
&lt;soap-env:Body>
{
	(: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
	let $reason := fn:substring-before(fn:substring-after(fn:substring-before($fault/ctx:reason, ":"), "("), ")")
	let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($fault/ctx:reason, ":"), ":"), ":")
	let $messageID := data($headerCache/urn3:header/urn3:msgHeader/urn3:msgId)
        let $shortUrcode:=string(local:getShortUrcode($urcode))
        let $longUrcode:=string(local:getLongUrcode($urcode))


(: longDescripitons: :)
let $txtLongUrCode100 :="ParameterRequired"
let $txtLongUrCode110 :="TransactionParameterNotAllowed"
let $txtLongUrCode120 :="ProductNotAllowed"
  
  (: shortDescripitons: :)
  (: shortDescripitons: :)
let $txtShortUrCode01 :="Country"
let $txtShortUrCode02 :="CurrencyCode"
let $txtShortUrCode03 :="SlinkProductNumber"
let $txtShortUrCode04 :="BranchBankId"
let $txtShortUrCode11 :="AppId"
let $txtShortUrCode12 :=""
let $txtShortUrCode13 :="Resident"
let $txtShortUrCode14 :="CustomerType"
let $txtShortUrCode15 :="BranchBankId"
let $txtShortUrCode16 :="CostOption"
let $txtShortUrCode17 :="ExecutionMode"
let $txtShortUrCode18 :="TransactionMa.currencyCode"
let $txtShortUrCode19 :="TransactionMa.amountMa"
let $txtShortUrCode20 :="TransactionMa.AccountNumber"
let $txtShortUrCode21 :="TransactionWn.currencyCode"
let $txtShortUrCode22 :="TransactionWn.accountNumber"
let $txtShortUrCode23 :="Bic"
let $txtShortUrCode24 :="ValueDate"
let $txtShortUrCode25 :="TxnAccountData.currencyCode"
let $txtShortUrCode26 :="System.currencyCode"
let $txtShortUrCode27 :="SlinkFeeId"
let $txtShortUrCode28 :="OriginCurrFeeAmount"
let $txtShortUrCode29 :="FeeAmount"	

let $txtShortAndLong :=local:getLongUrcodeDescription($longUrcode)

	return
		if($reason = "13") then
			local:fault("f:TimeoutException", element f:TimeoutException { local:errors($reason, $urcode, $messageID) })
		else if($reason = "11") then
			if($longUrcode = "101") then
				local:fault("f:SystemException", element f:SystemException { local:errors($reason, $urcode, $messageID) })
			else if($longUrcode = "102") then
				local:fault("f:CbdErrGetInputException", element f:CbdErrGetInputException { local:errors($reason, $urcode, $messageID) })
			else if($longUrcode = "103") then
				local:fault("f:ErrNoDataException", element f:ErrNoDataException { local:errors($reason, $urcode, $messageID) })
                        else  if($shortUrcode = "01") then
                                local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode01, concat(' (',concat($messageID, ')') ))))) })
                        else if(  $shortUrcode = "02") then 
                                local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode02, concat(' (',concat($messageID, ')') ))))) })                       
                        else if(  $shortUrcode = "03")  then
                                local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode03, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "04")  then
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode04, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(   $shortUrcode = "11") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode11, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "12") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode12, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "13") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode13, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(   $shortUrcode = "14") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode14, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "15") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode15, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "16") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode16, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(   $shortUrcode = "17") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode17, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "18") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode18, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "19") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode19, concat(' (',concat($messageID, ')') ))))) })                       
                       else if (  $shortUrcode = "20") then                    
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode20, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(   $shortUrcode = "21") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode21, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "22") then 
                               local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode22, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "23") then 
                              local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode23, concat(' (',concat($messageID, ')') ))))) })                     
                       else if(   $shortUrcode = "24") then 
                              local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode24, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "25") then 
                             local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode25, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "26") then 
                             local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode26, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(   $shortUrcode = "27") then 
                             local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode27, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "28") then 
                             local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode28, concat(' (',concat($messageID, ')') ))))) })                       
                       else if(  $shortUrcode = "29") then 
                             local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ', concat($txtShortUrCode29, concat(' (',concat($messageID, ')') )))))  })                      
                       else 
                             local:fault("f:ServiceFailException", element f:ServiceException { local:errors($reason, $urcode, concat($txtShortAndLong, concat( ': ',  concat(' (',concat($messageID, ')') ))))  })                        
     		else
			local:fault("f:ServiceException", element f:ServiceException { local:errors($reason, $urcode, $messageID) })       
	    
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>