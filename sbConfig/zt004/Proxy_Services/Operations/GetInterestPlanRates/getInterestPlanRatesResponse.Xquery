<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-10-07</con:description>
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:filterandmessages.dictionaries.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInterestPlanRatesList($parm as element(fml:FML32)) as element()
{

&lt;ns0:interestPlanRatesList>
  {
    for $x at $occ in $parm/NF_INTEPR_INDEX
    return
    &lt;ns2:InterestPlanRates>
      &lt;ns2:index?>{data($parm/NF_INTEPR_INDEX[$occ])}&lt;/ns2:index>
      &lt;ns2:indexRate?>{data($parm/NF_INTEPR_INDEXRATE[$occ])}&lt;/ns2:indexRate>
      &lt;ns2:margin?>{data($parm/NF_INTEPR_MARGIN[$occ])}&lt;/ns2:margin>
      &lt;ns2:interestRate?>{data($parm/NF_INTEPR_INTERESTRATE[$occ])}&lt;/ns2:interestRate>
      &lt;ns2:thresholdBalance?>{data($parm/NF_INTEPR_THRESHOLDBAL[$occ])}&lt;/ns2:thresholdBalance>
    &lt;/ns2:InterestPlanRates>
  }
&lt;/ns0:interestPlanRatesList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  {getElementsForInterestPlanRatesList($parm)}
  &lt;ns0:interestDependentOn>
    &lt;ns2:InterestPlanRates>
      &lt;ns2:interestDependentOn>
        &lt;ns3:InterestDependentOn>
          &lt;ns3:interestDependentOn?>{data($parm/NF_INTEDO_INTERESTDEPENDEN[1])}&lt;/ns3:interestDependentOn>
        &lt;/ns3:InterestDependentOn>
      &lt;/ns2:interestDependentOn>
    &lt;/ns2:InterestPlanRates>
  &lt;/ns0:interestDependentOn>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>