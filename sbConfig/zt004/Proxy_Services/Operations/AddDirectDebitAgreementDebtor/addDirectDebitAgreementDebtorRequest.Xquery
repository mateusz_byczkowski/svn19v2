<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:directdebit.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function chkUnitId($unitId as xs:anyType) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function boolean2SourceValue ($parm as xs:anyType,$trueval as xs:string,$falseval as xs:string) as xs:string
{
   if(string-length($parm)>0) 
   then
         if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval  
    else()
};

declare function xf:getFields($parm as element(ns7:invoke), $msghead as element(ns7:msgHeader), $tranhead as element(ns7:transHeader)) as element(fml:FML32) 
{
  let $msgId:= $msghead/ns7:msgId
  let $companyId:= $msghead/ns7:companyId
  let $userId := $msghead/ns7:userId
  let $appId:= $msghead/ns7:appId
  let $unitId := $msghead/ns7:unitId
  let $timestamp:= $msghead/ns7:timestamp

  let $transId:=$tranhead/ns7:transId

  return
  &lt;fml:FML32>
    &lt;DC_TRN_ID?>{data($transId)}&lt;/DC_TRN_ID>
    &lt;DC_UZYTKOWNIK?>{concat("SKP:", data($userId))}&lt;/DC_UZYTKOWNIK>
    &lt;DC_ODDZIAL?>{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL>
    &lt;NF_DIRECD_ORIGINATORREFERE?>{data($parm/ns7:directDebit/ns2:DirectDebit/ns2:originatorReference)}&lt;/NF_DIRECD_ORIGINATORREFERE>
    &lt;NF_DIRECD_DEBTORTAXID?>{data($parm/ns7:directDebit/ns2:DirectDebit/ns2:debtorTaxID)}&lt;/NF_DIRECD_DEBTORTAXID>
    &lt;NF_DIRECD_ADDITIONALDESCRI?>{data($parm/ns7:directDebit/ns2:DirectDebit/ns2:additionalDescription)}&lt;/NF_DIRECD_ADDITIONALDESCRI>
    &lt;NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns7:directDebit/ns2:DirectDebit/ns2:tranAccount/ns1:TranAccount/ns1:account/ns1:Account/ns1:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER>
    &lt;NF_CUSTOM_COMPANYNAME?>{data($parm/ns7:directDebit/ns2:DirectDebit/ns2:customer/ns3:Customer/ns3:companyName)}&lt;/NF_CUSTOM_COMPANYNAME>
    &lt;NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns7:directDebit/ns2:DirectDebit/ns2:customer/ns3:Customer/ns3:customerNumber)}&lt;/NF_CUSTOM_CUSTOMERNUMBER>
    {
    if(data($parm/ns7:directDebit/ns2:DirectDebit/ns2:customer/ns3:Customer/ns3:resident)) then
      &lt;NF_CUSTOM_RESIDENT?>{boolean2SourceValue(data($parm/ns7:directDebit/ns2:DirectDebit/ns2:customer/ns3:Customer/ns3:resident), "1", "0")}&lt;/NF_CUSTOM_RESIDENT>
    else ()
    }
    &lt;NF_SPWC_SPWCODE?>{data($parm/ns7:directDebit/ns2:DirectDebit/ns2:customer/ns3:Customer/ns3:spw/ns5:SpwCode/ns5:spwCode)}&lt;/NF_SPWC_SPWCODE>
    &lt;NF_ACCOUA_STREET?>{data($parm/ns7:address/ns1:AccountAddress/ns1:street)}&lt;/NF_ACCOUA_STREET>
    &lt;NF_ACCOUA_HOUSEFLATNUMBER?>{data($parm/ns7:address/ns1:AccountAddress/ns1:houseFlatNumber)}&lt;/NF_ACCOUA_HOUSEFLATNUMBER>
    &lt;NF_ACCOUA_CITY?>{data($parm/ns7:address/ns1:AccountAddress/ns1:city)}&lt;/NF_ACCOUA_CITY>
    &lt;NF_ACCOUA_STATECOUNTRY?>{data($parm/ns7:address/ns1:AccountAddress/ns1:stateCountry)}&lt;/NF_ACCOUA_STATECOUNTRY>
    &lt;NF_ACCOUA_ZIPCODE?>{data($parm/ns7:address/ns1:AccountAddress/ns1:zipCode)}&lt;/NF_ACCOUA_ZIPCODE>
(:  &lt;NF_ACCOUA_ACCOUNTADDRESSTY?>{data($parm/ns7:address/ns1:AccountAddress/ns1:accountAddressType)}&lt;/NF_ACCOUA_ACCOUNTADDRESSTY> :)

   {
    if(string-length(data($parm/ns7:address/ns1:AccountAddress/ns1:accountAddressType)) > 0)
    then
     if(data($parm/ns7:address/ns1:AccountAddress/ns1:accountAddressType) = "Z") 
       then
         &lt;NF_ACCOUA_ACCOUNTADDRESSTY?>1&lt;/NF_ACCOUA_ACCOUNTADDRESSTY>
     else
          &lt;NF_ACCOUA_ACCOUNTADDRESSTY?>0&lt;/NF_ACCOUA_ACCOUNTADDRESSTY>
    else()
    }
  &lt;/fml:FML32>
};



&lt;soap:Body>
     { xf:getFields($body/ns7:invoke, $header/ns7:header/ns7:msgHeader, $header/ns7:header/ns7:transHeader)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>