<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:entities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:insurance.entities.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:short2bool($short as xs:int) as xs:string {
  if ($short >= 1)
    then "true"
    else "false"
};

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

declare function getElementsForInsPolicyPremiumHistList($parm as element(fml:FML32)) as element()
{

&lt;ns4:insPolicyPremiumHistList>
  {
    for $x at $occ in $parm/NF_INSPPH_CALCULATIONDATE
    return
    &lt;ns3:InsPolicyPremiumHist>
      {if (data($parm/NF_INSPPH_COLLECTIONDATE[$occ]) and xf:isData(data($parm/NF_INSPPH_COLLECTIONDATE[$occ])))
        then &lt;ns3:collectionDate?>{data($parm/NF_INSPPH_COLLECTIONDATE[$occ])}&lt;/ns3:collectionDate>
        else ()
      }
      {if (data($parm/NF_INSPPH_CURRENTAMOUNTDUE[$occ]) and xf:isData(data($parm/NF_INSPPH_CURRENTAMOUNTDUE[$occ])))
        then &lt;ns3:currentAmountDue?>{data($parm/NF_INSPPH_CURRENTAMOUNTDUE[$occ])}&lt;/ns3:currentAmountDue>
        else ()
      }
      {if (data($parm/NF_INSPPH_ORYGINALAMOUNTDU[$occ]) and xf:isData(data($parm/NF_INSPPH_ORYGINALAMOUNTDU[$occ])))
        then &lt;ns3:oryginalAmountDue?>{data($parm/NF_INSPPH_ORYGINALAMOUNTDU[$occ])}&lt;/ns3:oryginalAmountDue>
        else ()
      }
      {if (data($parm/NF_INSPPH_PAIDAMOUNT[$occ]) and xf:isData(data($parm/NF_INSPPH_PAIDAMOUNT[$occ])))
        then &lt;ns3:paidAmount?>{data($parm/NF_INSPPH_PAIDAMOUNT[$occ])}&lt;/ns3:paidAmount>
        else ()
      }
      {if (data($parm/NF_INSPPH_CALCULATIONDATE[$occ]) and xf:isData(data($parm/NF_INSPPH_CALCULATIONDATE[$occ])))
        then &lt;ns3:calculationDate?>{data($parm/NF_INSPPH_CALCULATIONDATE[$occ])}&lt;/ns3:calculationDate>
        else ()
      }
      {if (data($parm/NF_INSPPH_INSURERAMOUNT[$occ]) and xf:isData(data($parm/NF_INSPPH_INSURERAMOUNT[$occ])))
        then &lt;ns3:insurerAmount?>{data($parm/NF_INSPPH_INSURERAMOUNT[$occ])}&lt;/ns3:insurerAmount>
        else ()
      }
      {if (data($parm/NF_INSPPH_BANKAMOUNT[$occ]) and xf:isData(data($parm/NF_INSPPH_BANKAMOUNT[$occ])))
        then &lt;ns3:bankAmount?>{data($parm/NF_INSPPH_BANKAMOUNT[$occ])}&lt;/ns3:bankAmount>
        else ()
      }
    &lt;/ns3:InsPolicyPremiumHist>
  }
&lt;/ns4:insPolicyPremiumHistList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  {getElementsForInsPolicyPremiumHistList($parm)}
  &lt;ns4:bcd>
    &lt;ns1:BusinessControlData>
      &lt;ns1:pageControl>
        &lt;ns5:PageControl>
   
   { if (data($parm/NF_PAGEC_HASNEXT))
      then 	
       &lt;ns5:hasNext?>{xf:short2bool(data($parm/NF_PAGEC_HASNEXT))}&lt;/ns5:hasNext>
       else() }
        
          &lt;ns5:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns5:navigationKeyDefinition>
          &lt;ns5:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns5:navigationKeyValue>
        &lt;/ns5:PageControl>
      &lt;/ns1:pageControl>
    &lt;/ns1:BusinessControlData>
  &lt;/ns4:bcd>
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>