<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:insurance.entities.be.dcl";
declare namespace ns3="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function chkUnitId($unitId as xs:anyType) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function getFieldsFromHeader($parm as element(ns3:header)) as element()*
{

(: &lt;NF_MSHEAD_MSGID?>{data($parm/ns3:msgHeader/ns3:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns3:msgHeader/ns3:companyId)}&lt;/NF_MSHEAD_COMPANYID>
, :)
&lt;DC_ODDZIAL?>{chkUnitId(data($parm/ns3:msgHeader/ns3:unitId))}&lt;/DC_ODDZIAL>
,
&lt;DC_UZYTKOWNIK?>{concat("SKP:", data($parm/ns3:msgHeader/ns3:userId))}&lt;/DC_UZYTKOWNIK>
,
(: &lt;NF_MSHEAD_APPID?>{data($parm/ns3:msgHeader/ns3:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns3:msgHeader/ns3:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
, :)
&lt;DC_TRN_ID?>{data($parm/ns3:transHeader/ns3:transId)}&lt;/DC_TRN_ID>
};
declare function getFieldsFromInvoke($parm as element(ns3:invoke)) as element()*
{

&lt;DC_IS_NR_REFF_POLISY?>{data($parm/ns3:insurancePolicyAcc/ns2:InsurancePolicyAcc/ns2:policyRefNum)}&lt;/DC_IS_NR_REFF_POLISY>
,
&lt;DC_DATA_WAZNOSCI?>{data($parm/ns3:insurancePolicyAcc/ns2:InsurancePolicyAcc/ns2:validityDate)}&lt;/DC_DATA_WAZNOSCI>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns3:header)}
    {getFieldsFromInvoke($body/ns3:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>