<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$3.2011-02-08</con:description>
  <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:insurance.entities.be.dcl";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns5="urn:insurancedict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function insertZero($parm as xs:string) as xs:string
{
    if ($parm) then
     if (string-length(data($parm)) >1)
       then data($parm)
     else concat("0",data($parm))
    else()
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns1:invokeResponse>
  <ns1:insurancePolicyAccOut>
    <ns0:InsurancePolicyAcc>
      <ns0:insurerName?>{data($parm/NF_INSPAC_INSURERNAME)}</ns0:insurerName>
      <ns0:linkedAccount?>{data($parm/NF_INSUPA_LINKEDACCOUNT)}</ns0:linkedAccount>
      <ns0:employeeID?>{data($parm/NF_INSPAC_EMPLOYEEID)}</ns0:employeeID>
      <ns0:actualDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_INSPAC_ACTUALDATE))}</ns0:actualDate>
      <ns0:startDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_INSPAC_STARTDATE))}</ns0:startDate>
      <ns0:policyNum?>{fn-bea:trim(data($parm/NF_INSPAC_POLICYNUM))}</ns0:policyNum>
      <ns0:collateralNum?>{fn-bea:trim(data($parm/NF_INSPAC_COLLATERALNUM))}</ns0:collateralNum>
      <ns0:statusDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_INSPAC_STATUSDATE))}</ns0:statusDate>
      <ns0:agreement?>{fn-bea:trim(data($parm/NF_INSPAC_AGREEMENT))}</ns0:agreement>
      <ns0:validityDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_INSPAC_VALIDITYDATE))}</ns0:validityDate>
      <ns0:reminderDays?>{data($parm/NF_INSPAC_REMINDERDAYS)}</ns0:reminderDays>
      <ns0:protectionEndDate?>{data($parm/NF_INSPAC_PROTECTIONENDDAT)}</ns0:protectionEndDate>
      <ns0:creatingEmployeeProfile?>{data($parm/NF_INSPAC_CREATINGEMPLOYEE)}</ns0:creatingEmployeeProfile>
      <ns0:createDate?>{fn-bea:dateTime-from-string-with-format("yyyy-MM-dd hh:mm:ss",data($parm/NF_INSPAC_CREATEDATE))}</ns0:createDate>
      <ns0:modifyingEmployeeProfile?>{data($parm/NF_INSPAC_MODIFYINGEMPLOYE)}</ns0:modifyingEmployeeProfile>
      <ns0:modifyDate?>{fn-bea:dateTime-from-string-with-format("yyyy-MM-dd hh:mm:ss",data($parm/NF_INSPAC_MODIFYDATE))}</ns0:modifyDate>
      <ns0:fixedPremium?>{data($parm/NF_INSPAC_FIXEDPREMIUM)}</ns0:fixedPremium>
      <ns0:rate?>{data($parm/NF_INSPAC_RATE)}</ns0:rate>
      <ns0:netPremium?>{data($parm/NF_INSPAC_NETPREMIUM)}</ns0:netPremium>
      <ns0:insurerPremiumShare?>{data($parm/NF_INSPAC_INSURERPREMIUMSH)}</ns0:insurerPremiumShare>
      <ns0:dailyInsuranceCoefficient?>{data($parm/NF_INSPAC_DAILYINSURANCECO)}</ns0:dailyInsuranceCoefficient>
      <ns0:bankMargin?>{data($parm/NF_INSPAC_BANKMARGIN)}</ns0:bankMargin>
      <ns0:bankPremiumShare?>{data($parm/NF_INSPAC_BANKPREMIUMSHARE)}</ns0:bankPremiumShare>
      <ns0:yearlyBase?>{data($parm/NF_INSPAC_YEARLYBASE)}</ns0:yearlyBase>
      <ns0:increaseInsurance?>{data($parm/NF_INSPAC_INCREASEINSURANC)}</ns0:increaseInsurance>
      <ns0:calculationBase?>{data($parm/NF_INSPAC_CALCULATIONBASE)}</ns0:calculationBase>
      <ns0:paymentOrderNum?>{data($parm/NF_INSPAC_PAYMENTORDERNUM)}</ns0:paymentOrderNum>
      <ns0:premiumDueDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_INSPAC_PREMIUMDUEDATE))}</ns0:premiumDueDate>
      <ns0:premiumFrequency?>{data($parm/NF_INSPAC_PREMIUMFREQUENCY)}</ns0:premiumFrequency>
      <ns0:calculationDays?>{data($parm/NF_INSPAC_CALCULATIONDAYS)}</ns0:calculationDays>
      <ns0:premiumDue?>{data($parm/NF_INSPAC_PREMIUMDUE)}</ns0:premiumDue>
      <ns0:premiumDuePLN?>{data($parm/NF_INSPAC_PREMIUMDUEPLN)}</ns0:premiumDuePLN>
      <ns0:paidToDate?>{data($parm/NF_INSPAC_PAIDTODATE)}</ns0:paidToDate>
      <ns0:paidInMonth?>{data($parm/NF_INSPAC_PAIDINMONTH)}</ns0:paidInMonth>
      <ns0:paidInYear?>{data($parm/NF_INSPAC_PAIDINYEAR)}</ns0:paidInYear>
      <ns0:paidLastYear?>{data($parm/NF_INSPAC_PAIDLASTYEAR)}</ns0:paidLastYear>
      <ns0:calculationDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_INSPAC_CALCULATIONDATE))}</ns0:calculationDate>
      <ns0:calculatedToDate?>{data($parm/NF_INSPAC_CALCULATEDTODATE)}</ns0:calculatedToDate>
      <ns0:calulatedInMonth?>{data($parm/NF_INSPAC_CALULATEDINMONTH)}</ns0:calulatedInMonth>
      <ns0:calculatedInYear?>{data($parm/NF_INSPAC_CALCULATEDINYEAR)}</ns0:calculatedInYear>
      <ns0:calculatedLastYear?>{data($parm/NF_INSPAC_CALCULATEDLASTYE)}</ns0:calculatedLastYear>
      <ns0:feePaidToDate?>{data($parm/NF_INSPAC_FEEPAIDTODATE)}</ns0:feePaidToDate>
      <ns0:feePaidInMonth?>{data($parm/NF_INSPAC_FEEPAIDINMONTH)}</ns0:feePaidInMonth>
      <ns0:feePaidInYear?>{data($parm/NF_INSPAC_FEEPAIDINYEAR)}</ns0:feePaidInYear>
      {if (data($parm/NF_INSPAC_EXTERNALPOLICES) = 0) then 
            <ns0:externalPolices></ns0:externalPolices>
      else
            <ns0:externalPolices?>{data($parm/NF_INSPAC_EXTERNALPOLICES)}</ns0:externalPolices>}
      <ns0:saleEmployeeID?>{data($parm/NF_INSPAC_SALEEMPLOYEEID)}</ns0:saleEmployeeID>
      <ns0:mandatoryFlag?>{sourceValue2Boolean(data($parm/NF_INSPAC_MANDATORYFLAG),"1")}</ns0:mandatoryFlag>
      <ns0:insuranceDescription?>{data($parm/NF_INSPAC_INSURANCEDESCRIP)}</ns0:insuranceDescription>
      <ns0:icbsProductNumber?>{data($parm/NF_INSPAC_ICBSPRODUCTNUMBE)}</ns0:icbsProductNumber>
      <ns0:productDefinition>
        <ns6:ProductDefinition>
          <ns6:idProductDefinition?>{data($parm/NF_PRODUD_IDPRODUCTDEFINIT)}</ns6:idProductDefinition>
        </ns6:ProductDefinition>
      </ns0:productDefinition>
      <ns0:branch>
        <ns4:BranchCode>
          <ns4:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE)}</ns4:branchCode>
        </ns4:BranchCode>
      </ns0:branch>
      <ns0:currency>
        <ns4:CurrencyCode>
          <ns4:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE)}</ns4:currencyCode>
        </ns4:CurrencyCode>
      </ns0:currency>
      <ns0:status>
        <ns5:InsurancePolicyStatus>
          <ns5:insurancePolicyStatus?>{data($parm/NF_INSPST_INSURANCEPOLICYS)}</ns5:insurancePolicyStatus>
        </ns5:InsurancePolicyStatus>
      </ns0:status>
      <ns0:riskCode>
        <ns5:InsuranceRiskCode>
          <ns5:insuranceRiskCode?>{data($parm/NF_INSURC_INSURANCERISKCOD)}</ns5:insuranceRiskCode>
        </ns5:InsuranceRiskCode>
      </ns0:riskCode>
      <ns0:closingStatus>
        <ns5:InsurancePackageStatus>
          <ns5:insurancePackageStatus?>{data($parm/NF_INSUPS_INSURANCEPACKAGE)}</ns5:insurancePackageStatus>
        </ns5:InsurancePackageStatus>
      </ns0:closingStatus>
      <ns0:calculationMethod>
        <ns5:InsuranceCalculationMethod>
          <ns5:insuranceCalculationMethod?>{insertZero($parm/NF_INSUCM_INSURANCECALCULA)}</ns5:insuranceCalculationMethod>
        </ns5:InsuranceCalculationMethod>
      </ns0:calculationMethod>
      <ns0:calculationCode>
        <ns5:InsuranceCalculationCode>
          <ns5:insuranceCalculationCode?>{data($parm/NF_INSUCC_INSURANCECALCULA)}</ns5:insuranceCalculationCode>
        </ns5:InsuranceCalculationCode>
      </ns0:calculationCode>
      <ns0:premiumPeriod>
        <ns4:Period>
          <ns4:period?>{data($parm/NF_PERIOD_PERIOD)}</ns4:period>
        </ns4:Period>
      </ns0:premiumPeriod>
      <ns0:oddPremiumDay>
        <ns4:SpecialDay>
          <ns4:specialDay?>{data($parm/NF_SPECID_SPECIALDAY)}</ns4:specialDay>
        </ns4:SpecialDay>
      </ns0:oddPremiumDay>
      <ns0:insuranceLocumType>
        <ns5:InsuranceLocumType>
          <ns5:insuranceLocumType?>{data($parm/NF_INSULT_INSURANCELOCUMTY)}</ns5:insuranceLocumType>
        </ns5:InsuranceLocumType>
      </ns0:insuranceLocumType>
      <ns0:insuranceVariant>
        <ns5:InsuranceLocumVariant>
          <ns5:insuranceLocumVariant?>{data($parm/NF_INSULV_INSURANCELOCUMVA)}</ns5:insuranceLocumVariant>
        </ns5:InsuranceLocumVariant>
      </ns0:insuranceVariant>
      <ns0:closingReason>
        <ns5:InsuranceClosingReason>
          <ns5:insuranceClosingReason?>{data($parm/NF_INSUCR_INSURANCECLOSING)}</ns5:insuranceClosingReason>
        </ns5:InsuranceClosingReason>
      </ns0:closingReason>
        <ns0:policyResignCause>
        <ns5:PolicyResignCause>
          <ns5:policyResignCause?>{data($parm/NF_POLIRC_POLICYRESIGNCAUS)}</ns5:policyResignCause>
        </ns5:PolicyResignCause>
      </ns0:policyResignCause>
    </ns0:InsurancePolicyAcc>
  </ns1:insurancePolicyAccOut>
</ns1:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>