<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/BillProtection/Sygnity/CUP/GetOpCoverageVariant/ResponseTransform/";
declare namespace ns0 = "urn:baseentities.be.dcl";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf-product-types";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns2 = "urn:uldictionary.dictionaries.be.dcl";

declare function xf:ResponseTransform($getProductResponse1 as element(ns-1:getProductResponse))
    as element(ns3:invokeResponse) {
        &lt;ns3:invokeResponse>
            &lt;ns3:messageHelper>
                &lt;ns0:MessageHelper>
                    &lt;ns0:errorCode?>&lt;/ns0:errorCode>
                    &lt;ns0:errorType?>&lt;/ns0:errorType>
                &lt;/ns0:MessageHelper>
            &lt;/ns3:messageHelper>
            {
                let $level-list := $getProductResponse1/product/level-list
                return
                    &lt;ns3:opCoverageVariant>
                        {
                            for $level-definition in $level-list/level-definition
                            return
                                &lt;ns2:OpCoverageVariant>
                                    &lt;ns2:opCoverageVariant>{ xs:string( data($level-definition/level) ) }&lt;/ns2:opCoverageVariant>
                                    &lt;ns2:insurancePremium>{ xs:double( data($level-definition/amount) ) }&lt;/ns2:insurancePremium>
                                &lt;/ns2:OpCoverageVariant>
                        }
                    &lt;/ns3:opCoverageVariant>
            }
        &lt;/ns3:invokeResponse>
};

declare variable $getProductResponse1 as element(ns-1:getProductResponse) external;

xf:ResponseTransform($getProductResponse1)</con:xquery>
</con:xqueryEntry>