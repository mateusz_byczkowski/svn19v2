<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-08</con:description>
  <con:xquery>declare namespace crw = "http://bzwbk.com/crw/services/quickwin/";
declare namespace urn = "urn:be.services.dcl";

declare namespace urn4 = "urn:insurance.entities.be.dcl";
declare namespace urn5 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace urn8 = "urn:baseauxentities.be.dcl";

declare variable $invoke as element(urn:invoke) external;
declare variable $header as element(urn:header) external;

&lt;crw:updateApplicationNotNull>
	 &lt;application>
		&lt;applicationStatus>{ data($invoke/urn:insPolicyApplication/urn4:InsPolicyApplication/urn4:applicationStatus/urn5:InsPolicyAppStatus/urn5:insPolicyAppStatus) }&lt;/applicationStatus>
		&lt;number>{ data($invoke/urn:insPolicyApplication/urn4:InsPolicyApplication/urn4:applicationNumber)  }&lt;/number>
	 &lt;/application>
	&lt;authData>
		&lt;applicationId>{ data($header/urn:msgHeader/urn:appId) }&lt;/applicationId>
		&lt;applicationPassword>{ data($invoke/urn:password/urn8:StringHolder/urn8:value)}&lt;/applicationPassword>
		&lt;operator>{ data($header/urn:msgHeader/urn:userId) }&lt;/operator>
	&lt;/authData>
&lt;/crw:updateApplicationNotNull></con:xquery>
</con:xqueryEntry>