<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/StreamLine/UpdateWithdrawalAccount/RequestTransformNew/";
declare namespace ns4 = "urn:applicationul.entities.be.dcl";
declare namespace ns0 = "urn:baseentities.be.dcl";
declare namespace ns7 = "urn:baseauxentities.be.dcl";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns3 = "urn:basedictionaries.be.dcl";
declare namespace ns5 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns2 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns8 = "urn:entities.be.dcl";
declare namespace ns9 = "urn:dictionaries.be.dcl";

declare function xf:RequestTransformNew($invoke1 as element(ns6:invoke), $header1 as element(ns6:header))
    as element(ns-1:changeContractBanksRequest) {
        &lt;ns-1:changeContractBanksRequest>
           &lt;envelope>
                &lt;request-time>{ data($header1/ns6:msgHeader/ns6:timestamp) }&lt;/request-time>
                &lt;request-no>{ data($header1/ns6:msgHeader/ns6:msgId) }&lt;/request-no>
                &lt;source-code?>{ data($invoke1/ns6:sourceCode/ns7:StringHolder/ns7:value) }&lt;/source-code>
                &lt;user-id?>{ data($header1/ns6:msgHeader/ns6:userId) }&lt;/user-id>
            	&lt;branch-id?>{ data($header1/ns6:msgHeader/ns6:unitId) }&lt;/branch-id>
                &lt;bar-code?>{ data($invoke1/ns6:barCodeID/ns7:StringHolder/ns7:value) }&lt;/bar-code>
            &lt;/envelope>
            &lt;save>{ data($invoke1/ns6:mode/ns7:BooleanHolder/ns7:value) }&lt;/save>
            &lt;product-type>{ data($invoke1/ns6:policyContract/ns4:PolicyContract/ns4:productCode/ns5:UlParameters/ns5:productId) }&lt;/product-type>
            &lt;contract-number>{ data($invoke1/ns6:policyContract/ns4:PolicyContract/ns4:policyID) }&lt;/contract-number>
            {
               let  $PolicyContractAccount := $invoke1/ns6:policyContract/ns4:PolicyContract/ns4:policyContractAccountList/ns4:PolicyContractAccount[1]
               return
                   &lt;bank kind = "{ data($PolicyContractAccount/ns4:accountType/ns5:UlAccountType/ns5:ulAccountType) }">
			           	&lt;account>{data($PolicyContractAccount/ns4:accountNo)}&lt;/account>
			        	&lt;recipient-name?>
							&lt;first-name?>{ data($PolicyContractAccount/ns4:firstName) }&lt;/first-name>
			            	&lt;last-name?>{ data($PolicyContractAccount/ns4:name) }&lt;/last-name>
			        	&lt;/recipient-name>
                   		&lt;recipient-address? kind?="{ data($invoke1/ns6:addressType/ns7:StringHolder/ns7:value) }">
                    		&lt;street?>{ data($PolicyContractAccount/ns4:street) }&lt;/street>
                    		&lt;home?>{ data($PolicyContractAccount/ns4:house) }&lt;/home>
                    		&lt;flat?>{ data($PolicyContractAccount/ns4:flat) }&lt;/flat>
                        	&lt;postal-code?>{ data($PolicyContractAccount/ns4:zipCode) }&lt;/postal-code>
                        	&lt;city?>{ data($PolicyContractAccount/ns4:city) }&lt;/city>
                        	&lt;country?>{ data($PolicyContractAccount/ns4:country/ns9:CountryCode/ns9:countryCode) }&lt;/country>
    	                &lt;/recipient-address>
    	                &lt;standing-order-no?>{ data($invoke1/ns6:policyContract/ns4:PolicyContract/ns4:transferOrderNumber) }&lt;/standing-order-no>
        			&lt;/bank>
            }
        &lt;/ns-1:changeContractBanksRequest>
};

declare variable $invoke1 as element(ns6:invoke) external;
declare variable $header1 as element(ns6:header) external;

xf:RequestTransformNew($invoke1, $header1)</con:xquery>
</con:xqueryEntry>