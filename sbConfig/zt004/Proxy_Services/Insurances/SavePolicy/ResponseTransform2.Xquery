<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/SaveULPolicy/ResponseTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns1 = "urn:baseentities.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns2 = "urn:applicationul.entities.be.dcl";
declare namespace cl = "http://sygnity.pl/functions";

declare function xf:ResponseTransform($addContractResponse1 as element(ns0:addContractResponse), $codes as element(cl:codes))
    as element(ns3:invokeResponse) {
        &lt;ns3:invokeResponse>
            {
                let $validations := $addContractResponse1/validations
                return
                    &lt;ns3:messageHelper>
                        {
                            for $validation in $validations/validation
                            return
                                &lt;ns1:MessageHelper>
                                    &lt;ns1:errorCode>{ 
										let $allCodes :=
											for $code in $codes/cl:code
											where $code/@any-code = $validation/code
											return data($code/@nfe-code)
										return
											if($allCodes[1] != '') then
												$allCodes[1]
											else
												'K00385'
                                    }&lt;/ns1:errorCode>
                                    &lt;ns1:errorType>{ data($validation/severity) }&lt;/ns1:errorType>
                                &lt;/ns1:MessageHelper>
                        }
                    &lt;/ns3:messageHelper>
            }
            &lt;ns3:policyContract>
                &lt;ns2:PolicyContract>
                    &lt;ns2:policyID?>{ data($addContractResponse1/contract-number) }&lt;/ns2:policyID>
                    &lt;ns2:policyContractAccountList?>
                        &lt;ns2:PolicyContractAccount?>
                            &lt;ns2:accountNo?>{ xs:string( data($addContractResponse1/account) ) }&lt;/ns2:accountNo>
                        &lt;/ns2:PolicyContractAccount>
                    &lt;/ns2:policyContractAccountList>
                &lt;/ns2:PolicyContract>
            &lt;/ns3:policyContract>
        &lt;/ns3:invokeResponse>
};

declare variable $addContractResponse1 as element(ns0:addContractResponse) external;
declare variable $codes as element(cl:codes) external;

xf:ResponseTransform($addContractResponse1, $codes)</con:xquery>
</con:xqueryEntry>