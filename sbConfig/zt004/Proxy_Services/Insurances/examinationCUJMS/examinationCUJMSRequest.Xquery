<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;

<soap-env:Body>
{
    let $req := $body/RECORDSET/RECORD
    return

    <ref:ReferralRequest xmlns:ref="http://bzwbk.com/examinationCU/referral/">
      <AGREEMENTDATE?>{data($req/AGREEMENTDATE)}</AGREEMENTDATE>
      <REFERRALDATE?>{data($req/REFERRALDATE)}</REFERRALDATE>
      <REFERRALNO?>{data($req/REFERRALNO)}</REFERRALNO>
      <CLIENTDATA>
        <SURNAME?>{data($req/CUSTOMERSURNAME)}</SURNAME>
        <FIRSTNAME?>{data($req/CUSTOMERFIRSTNAME)}</FIRSTNAME>
        <DATEOFBIRTH?>{data($req/CUSTOMERDATEOFBIRTH)}</DATEOFBIRTH>
        <IDENTITYCARD?>{data($req/CUSTOMERIDENTITYCARD)}</IDENTITYCARD>
        <SOCIALSECURITYNO?>{data($req/CUSTOMERSOCIALSECURITYNO)}</SOCIALSECURITYNO>
        <CELLPHONENO?>{data($req/CUSTOMERCELLPHONENO)}</CELLPHONENO>
        <AMOUNTOFINSURANCE?>{data($req/CUSTOMERAMOUNTOFINSURANCE)}</AMOUNTOFINSURANCE>
        <TYPEOFINSURANCE?>{data($req/CUSTOMERTYPEOFINSURANCE)}</TYPEOFINSURANCE>
        <STREET?>{data($req/CUSTOMERSTREET)}</STREET>
        <HOMENO?>{data($req/CUSTOMERHOMENO)}</HOMENO>
        <CITY?>{data($req/CUSTOMERCITY)}</CITY>
        <ZIP?>{data($req/CUSTOMERZIP)}</ZIP>
        <POSTOFFICE?>{data($req/CUSTOMERPOSTOFFICE)}</POSTOFFICE>
      </CLIENTDATA>
      <CLERKDATA>
        <NAME?>{data($req/CLERKNAME)}</NAME>
        <BRANCH?>{data($req/CLERKBRANCH)}</BRANCH>
        <PHONENO?>{data($req/CLERKPHONENO)}</PHONENO>
      </CLERKDATA>
      <PHYSICIANDATA>
        <NAME?>{data($req/PHYSICIANNAME)}</NAME>
        <STREET?>{data($req/PHYSICIANSTREET)}</STREET>
        <HOMENO?>{data($req/PHYSICIANHOMENO)}</HOMENO>
        <CITY?>{data($req/PHYSICIANCITY)}</CITY>
        <ZIP?>{data($req/PHYSICIANZIP)}</ZIP>
        <PHONENO?>{data($req/PHYSICIANPHONENO)}</PHONENO>
      </PHYSICIANDATA>
    </ref:ReferralRequest>
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>