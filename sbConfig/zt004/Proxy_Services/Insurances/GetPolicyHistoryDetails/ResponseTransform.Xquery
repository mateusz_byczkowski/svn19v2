<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/StreamLine/GetPolicyHistoryDetails/ResponseTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns1 = "urn:baseentities.be.dcl";
declare namespace ns3 = "urn:applicationul.entities.be.dcl";
declare namespace ns2 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns5 = "urn:be.services.dcl";
declare namespace cl = "http://sygnity.pl/functions";

declare function xf:cupCodeConvert($input as xs:string, $type as xs:string, $codes as element(cl:historyCodes)) as xs:string {
	let $translated :=
		for $e in $codes/cl:historyCode
		where $e/@cup-code = $input
		return
			if($type = 'CODE') then
				data($e/@nfe-code)
			else
				data($e/@nfe-type)
	return
		if($translated[1] != '') then
			$translated[1]
		else if($type = 'CODE') then
			'0'
		else
			''
};

declare function xf:ResponseTransform($getContractFinancialOperationDetailsResponse1 as element(ns0:getContractFinancialOperationDetailsResponse),
						$codes as element(cl:historyCodes))
    as element(ns5:invokeResponse) {
        &lt;ns5:invokeResponse>
        	&lt;ns5:messageHelper>
        		&lt;ns1:MessageHelper>
        		&lt;/ns1:MessageHelper>
        	&lt;/ns5:messageHelper>
            &lt;ns5:policyContract>
                &lt;ns3:PolicyContract>
                    &lt;ns3:policyID>{ xs:string( data($getContractFinancialOperationDetailsResponse1/operation/@contract-number) ) }&lt;/ns3:policyID>
                    &lt;ns3:policyOrderHistoryListList>
                    	&lt;ns3:PolicyOrderHistoryList>
		                    &lt;ns3:policyOrderHistory>
		                        &lt;ns3:PolicyOrdersHistory>
		                            &lt;ns3:orderNumber>{ xs:string( data($getContractFinancialOperationDetailsResponse1/operation/order-no) ) }&lt;/ns3:orderNumber>
		                            &lt;ns3:operationDate>{ data($getContractFinancialOperationDetailsResponse1/operation/effective) }&lt;/ns3:operationDate>
		                            &lt;ns3:value>{ xs:double( data($getContractFinancialOperationDetailsResponse1/operation/value) ) }&lt;/ns3:value>
		                            &lt;ns3:accountNo>{ xs:string( data($getContractFinancialOperationDetailsResponse1/operation/bank/account) ) }&lt;/ns3:accountNo>
		                            &lt;ns3:operationName>{ data($getContractFinancialOperationDetailsResponse1/operation/description) }&lt;/ns3:operationName>
                                            {
                                            if (fn:boolean(data($getContractFinancialOperationDetailsResponse1/operation/execution))) then
		                                &lt;ns3:executionDate?>{ fn:substring-before(fn:concat(data($getContractFinancialOperationDetailsResponse1/operation/execution),'T'),'T') }&lt;/ns3:executionDate>
                                            else ()
                                            }
		                            &lt;ns3:operationId?>{ data($getContractFinancialOperationDetailsResponse1/operation/@operation-id) }&lt;/ns3:operationId>
		                            &lt;ns3:tax?>{ data($getContractFinancialOperationDetailsResponse1/operation/tax) }&lt;/ns3:tax>
		                            &lt;ns3:policyOrdersHistoryFundToList>
		                            {
		                                for $fund-change in $getContractFinancialOperationDetailsResponse1/operation/fund-to/fund-change
		                                return
		                                    &lt;ns3:PolicyOrdersHistoryFundTo>
		                                        &lt;ns3:quantity?>{ data($fund-change/unit-count) }&lt;/ns3:quantity>
		                                        &lt;ns3:price?>{ data($fund-change/price) }&lt;/ns3:price>
		                                        &lt;ns3:value?>{ data($fund-change/value) }&lt;/ns3:value>
		                                        &lt;ns3:percentage?>{ data($fund-change/percentage) }&lt;/ns3:percentage>
		                                        &lt;ns3:fundID>
		                                        	&lt;ns4:UlFund>
		                                        		&lt;ns4:fundID>{ data($fund-change/@fund-name) }&lt;/ns4:fundID>
		                                        	&lt;/ns4:UlFund>
		                                        &lt;/ns3:fundID>
		                                    &lt;/ns3:PolicyOrdersHistoryFundTo>
		                            }
		                            &lt;/ns3:policyOrdersHistoryFundToList>
		                            &lt;ns3:policyOrdersHistFundFromList>
		                            {
		                                for $fund-change in $getContractFinancialOperationDetailsResponse1/operation/fund-from/fund-change
		                                return
		                                    &lt;ns3:PolicyOrdersHistoryFundFrom>
		                                        &lt;ns3:percentage?>{ data($fund-change/percentage) }&lt;/ns3:percentage>
		                                        &lt;ns3:value1?>{ data($fund-change/value) }&lt;/ns3:value1>
		                                        &lt;ns3:price>{ data($fund-change/price) }&lt;/ns3:price>
		                                        &lt;ns3:quantity?>{ data($fund-change/unit-count) }&lt;/ns3:quantity>
		                                        &lt;ns3:tax?>{ data($fund-change/tax) }&lt;/ns3:tax>
		                                        &lt;ns3:fee?>{ data($fund-change/fee) }&lt;/ns3:fee>
		                                        &lt;ns3:value2?>&lt;/ns3:value2>
		                                        &lt;ns3:fundID>
		                                        	&lt;ns4:UlFund>
		                                        		&lt;ns4:fundID>{ data($fund-change/@fund-name) }&lt;/ns4:fundID>
		                                        	&lt;/ns4:UlFund>
		                                        &lt;/ns3:fundID>
		                                    &lt;/ns3:PolicyOrdersHistoryFundFrom>
		                            }
		                            &lt;/ns3:policyOrdersHistFundFromList>
		                            &lt;ns3:historyCode>
		                                &lt;ns4:UlHistoryCode>
		                                    &lt;ns4:ulHistoryCode>{ xs:string( data($getContractFinancialOperationDetailsResponse1/operation/operation-type) ) }&lt;/ns4:ulHistoryCode>
		                                &lt;/ns4:UlHistoryCode>
		                            &lt;/ns3:historyCode>
		                            &lt;ns3:historyStatus>
		                                &lt;ns4:UlHistoryStatus>
		                                    &lt;ns4:ulHistoryStatus>{ xs:string( data($getContractFinancialOperationDetailsResponse1/operation/status) ) }&lt;/ns4:ulHistoryStatus>
		                                &lt;/ns4:UlHistoryStatus>
		                            &lt;/ns3:historyStatus>
	                                &lt;ns3:eventCode>
	                                	&lt;ns4:UlHistoryEventCode>
	                                		&lt;ns4:ulHistoryEventCode>{ xf:cupCodeConvert(data($getContractFinancialOperationDetailsResponse1/operation/operation-type), 'CODE', $codes) }&lt;/ns4:ulHistoryEventCode>
	                                	&lt;/ns4:UlHistoryEventCode>
	                                &lt;/ns3:eventCode>
	                                &lt;ns3:eventType>
	                                	&lt;ns4:UlHistoryEventType>
	                                		&lt;ns4:ulHistoryEventType>{ xf:cupCodeConvert(data($getContractFinancialOperationDetailsResponse1/operation/operation-type), 'TYPE', $codes) }&lt;/ns4:ulHistoryEventType>
	                                	&lt;/ns4:UlHistoryEventType>
	                                &lt;/ns3:eventType>
		                        &lt;/ns3:PolicyOrdersHistory>
		                    &lt;/ns3:policyOrderHistory>
                    	&lt;/ns3:PolicyOrderHistoryList>
                    &lt;/ns3:policyOrderHistoryListList>
                    &lt;ns3:productCode>
                        &lt;ns4:UlParameters>
                            &lt;ns4:productId>{ xs:string( data($getContractFinancialOperationDetailsResponse1/operation/@product-type) ) }&lt;/ns4:productId>
                        &lt;/ns4:UlParameters>
                    &lt;/ns3:productCode>
                &lt;/ns3:PolicyContract>
            &lt;/ns5:policyContract>
        &lt;/ns5:invokeResponse>
};

declare variable $getContractFinancialOperationDetailsResponse1 as element(ns0:getContractFinancialOperationDetailsResponse) external;
declare variable $codes as element(cl:historyCodes) external;

xf:ResponseTransform($getContractFinancialOperationDetailsResponse1, $codes)</con:xquery>
</con:xqueryEntry>