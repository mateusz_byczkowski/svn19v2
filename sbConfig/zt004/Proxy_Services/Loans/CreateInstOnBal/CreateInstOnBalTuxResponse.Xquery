<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace prim = "http://bzwbk.com/services/prime/";
declare namespace xf="http://bzwbk.com/services/prime/functions";
declare namespace fml = "";
declare namespace none = "";
declare namespace fault="http://schemas.datacontract.org/2004/07/LoanServices";
declare namespace rsp="http://schemas.datacontract.org/2004/07/LoanServices";

declare variable $timestamp external;

declare function xf:mapErrorCode($errorCode as xs:string) as xs:string
{
       $errorCode
};


declare function xf:mapTimeStamp($timestamp as xs:string) as xs:string
{
        fn:concat (fn:substring($timestamp,1,10),"-",
                          fn:substring($timestamp,12,2),".",
                          fn:substring($timestamp,15,2),".",
                          fn:substring($timestamp,18),".000000")
};


declare function xf:mapResponse($resp as element (prim:CreateInstOnBalResponse))
	as element (fml:FML32) {

                               &lt;fml:FML32>
			{
				&lt;fml:B_STATUS?>{data ($resp/prim:CreateInstOnBalResult)}&lt;/fml:B_STATUS>,
                                                                &lt;fml:B_URCODE?>0&lt;/fml:B_URCODE>,
                                                                &lt;fml:B_TPERRNO?>0&lt;/fml:B_TPERRNO>,
                                                                &lt;fml:B_OPIS_BLEDU?>ok&lt;/fml:B_OPIS_BLEDU>
			}
		&lt;/fml:FML32>
};


declare function xf:mapFault($fau as element(soap-env:Fault))
	as element (fml:FML32) {

	&lt;fml:FML32>
		&lt;fml:B_TPERRNO>{ data($fau/detail/fault:WbkFault/fault:ErrorCode1) }&lt;/fml:B_TPERRNO>
		&lt;fml:B_URCODE>{ data($fau/detail/fault:WbkFault/fault:ErrorCode2) }&lt;/fml:B_URCODE>
		&lt;fml:B_OPIS_BLEDU>{ data($fau/detail/fault:WbkFault/fault:ErrorDescription) }&lt;/fml:B_OPIS_BLEDU>
	&lt;/fml:FML32>
};



declare variable $body as element(soap-env:Body) external;


&lt;soap-env:Body>
{
	if (boolean($body/prim:CreateInstOnBalResponse)) then (
 		xf:mapResponse($body/prim:CreateInstOnBalResponse)
 	) else if (boolean($body/soap-env:Fault/detail/fault:WbkFault/fault:ErrorCode2)) then (
 		xf:mapFault($body/soap-env:Fault)
	) else ()
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>