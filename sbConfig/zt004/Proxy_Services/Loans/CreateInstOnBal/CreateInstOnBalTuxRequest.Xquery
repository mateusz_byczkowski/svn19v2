<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/prime/";



declare function xf:map_CreateInstOnBalRequest($fml as element(fml:FML32))
	as element(urn:CreateInstOnBal) {
		&lt;urn:CreateInstOnBal>
			&lt;urn:accountNumber>{ data($fml/fml:B_NR_RACH)}&lt;/urn:accountNumber>
			&lt;urn:instalmentAmount>{ data($fml/fml:B_KWOTA_KRED)}&lt;/urn:instalmentAmount>
			&lt;urn:periodLength>{ data($fml/fml:B_L_RAT)}&lt;/urn:periodLength>
			&lt;urn:interestProfile>{ data($fml/fml:B_NR_IND)}&lt;/urn:interestProfile>
			&lt;urn:userName>{ data($fml/fml:B_USER_ID)}&lt;/urn:userName>
			&lt;urn:options>{ data($fml/fml:B_OPCJA)}&lt;/urn:options>
		&lt;/urn:CreateInstOnBal>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_CreateInstOnBalRequest($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>