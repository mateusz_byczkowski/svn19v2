<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:filtersandmessages.entities.be.dcl";
declare namespace ns3="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns5="urn:baseauxentities.be.dcl";
declare namespace ns6="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{
	&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>,
	&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>,
	&lt;NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID>,
	&lt;NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID>,
	&lt;NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID>,
	&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>,
	&lt;NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID>
};

declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
	&lt;NF_FILTEH_STARTDATE?>{data($parm/ns0:filterHistory/ns4:FilterHistory/ns4:startDate)}&lt;/NF_FILTEH_STARTDATE>,
	&lt;NF_FILTEH_ENDDATE?>{data($parm/ns0:filterHistory/ns4:FilterHistory/ns4:endDate)}&lt;/NF_FILTEH_ENDDATE>,
	&lt;NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns0:account/ns3:Account/ns3:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER>,
	&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns0:bcd/ns6:BusinessControlData/ns6:pageControl/ns5:PageControl/ns5:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>,
	&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns0:bcd/ns6:BusinessControlData/ns6:pageControl/ns5:PageControl/ns5:pageSize)}&lt;/NF_PAGEC_PAGESIZE>,
	if (data($parm/ns4:bcd/ns2:BusinessControlData/ns2:pageControl/ns5:PageControl/ns5:reverseOrder))
	then 	
		&lt;NF_PAGEC_REVERSEORDER?>{data($parm/ns0:bcd/ns6:BusinessControlData/ns6:pageControl/ns5:PageControl/ns5:reverseOrder)}&lt;/NF_PAGEC_REVERSEORDER>
	else(),
	&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns0:bcd/ns6:BusinessControlData/ns6:pageControl/ns5:PageControl/ns5:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>,
	&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns0:bcd/ns6:BusinessControlData/ns6:pageControl/ns5:PageControl/ns5:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>