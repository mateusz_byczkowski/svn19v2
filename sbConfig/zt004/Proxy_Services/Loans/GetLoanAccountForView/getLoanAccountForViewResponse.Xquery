<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:short2bool($short as xs:int) as xs:string {
	if ($short >= 1)
		then "true"
		else "false"
};

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

declare function getElementsForLoanAccountPastDueList($parm as element(fml:FML32)) as element()
{

&lt;ns0:loanAccountPastDueList>
  {
    for $x at $occ in $parm/NF_LOAAPD_PASTDUEDATE
    return
    &lt;ns0:LoanAccountPastDue>
      {if (data($parm/NF_LOAAPD_PASTDUENUMBER[$occ]) and xf:isData(data($parm/NF_LOAAPD_PASTDUENUMBER[$occ])))
      	then &lt;ns0:pastDueNumber?>{data($parm/NF_LOAAPD_PASTDUENUMBER[$occ])}&lt;/ns0:pastDueNumber>
      	else ()
      }
      {if (data($parm/NF_LOAAPD_PASTDUETYP[$occ]) and xf:isData(data($parm/NF_LOAAPD_PASTDUETYP[$occ])))
      	then &lt;ns0:pastDueTyp?>{data($parm/NF_LOAAPD_PASTDUETYP[$occ])}&lt;/ns0:pastDueTyp>
      	else ()
      }
      {if (data($parm/NF_LOAAPD_PASTDUEDATE[$occ]) and xf:isData(data($parm/NF_LOAAPD_PASTDUEDATE[$occ])))
      	then &lt;ns0:pastDueDate?>{data($parm/NF_LOAAPD_PASTDUEDATE[$occ])}&lt;/ns0:pastDueDate>
      	else ()
      }
      {if (data($parm/NF_LOAAPD_PASTDUEREQUIREDA[$occ]) and xf:isData(data($parm/NF_LOAAPD_PASTDUEREQUIREDA[$occ])))
      	then &lt;ns0:pastDueRequiredAmount?>{data($parm/NF_LOAAPD_PASTDUEREQUIREDA[$occ])}&lt;/ns0:pastDueRequiredAmount>
      	else ()
      }
      {if (data($parm/NF_LOAAPD_PASTDUEPRINCIPAL[$occ]) and xf:isData(data($parm/NF_LOAAPD_PASTDUEPRINCIPAL[$occ])))
      	then &lt;ns0:pastDuePrincipalAmt?>{data($parm/NF_LOAAPD_PASTDUEPRINCIPAL[$occ])}&lt;/ns0:pastDuePrincipalAmt>
      	else ()
      }
      {if (data($parm/NF_LOAAPD_PASTDUEINTERESTA[$occ]) and xf:isData(data($parm/NF_LOAAPD_PASTDUEINTERESTA[$occ])))
      	then &lt;ns0:pastDueInterestAmt?>{data($parm/NF_LOAAPD_PASTDUEINTERESTA[$occ])}&lt;/ns0:pastDueInterestAmt>
      	else ()
      }
      {if (data($parm/NF_LOAAPD_PASTDUELATEINT[$occ]) and xf:isData(data($parm/NF_LOAAPD_PASTDUELATEINT[$occ])))
      	then &lt;ns0:pastDueLateInt?>{data($parm/NF_LOAAPD_PASTDUELATEINT[$occ])}&lt;/ns0:pastDueLateInt>
      	else ()
      }
      {if (data($parm/NF_LOAAPD_PASTDUEFEE[$occ]) and xf:isData(data($parm/NF_LOAAPD_PASTDUEFEE[$occ])))
      	then &lt;ns0:pastDueFee?>{data($parm/NF_LOAAPD_PASTDUEFEE[$occ])}&lt;/ns0:pastDueFee>
      	else ()
      }
    &lt;/ns0:LoanAccountPastDue>
  }
&lt;/ns0:loanAccountPastDueList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse>
  &lt;ns5:accountOut>
    &lt;ns0:Account>
      {if (data($parm/NF_ACCOUN_ACCOUNTNUMBER))
      	then &lt;ns0:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER)}&lt;/ns0:accountNumber>
      	else ()
      }
      {if (data($parm/NF_ACCOUN_ACCOUNTOPENDATE) and xf:isData(data($parm/NF_ACCOUN_ACCOUNTOPENDATE)))
      	then &lt;ns0:accountOpenDate?>{data($parm/NF_ACCOUN_ACCOUNTOPENDATE)}&lt;/ns0:accountOpenDate>
      	else ()
      }
      {if (data($parm/NF_ACCOUN_CURRENTBALANCE) and xf:isData(data($parm/NF_ACCOUN_CURRENTBALANCE)))
      	then &lt;ns0:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE)}&lt;/ns0:currentBalance>
      	else ()
      }
      {if (data($parm/NF_ACCOUN_CURRENTBALANCEPL) and xf:isData(data($parm/NF_ACCOUN_CURRENTBALANCEPL)))
      	then &lt;ns0:currentBalancePLN?>{data($parm/NF_ACCOUN_CURRENTBALANCEPL)}&lt;/ns0:currentBalancePLN>
      	else ()
      }
      &lt;ns0:loanAccount>
        &lt;ns0:LoanAccount>
          {if (data($parm/NF_LOANA_INTERESTGUARANTEE) and xf:isData(data($parm/NF_LOANA_INTERESTGUARANTEE)))
          	then &lt;ns0:interestGuaranteeCode?>{data($parm/NF_LOANA_INTERESTGUARANTEE)}&lt;/ns0:interestGuaranteeCode>
          	else ()
          }
          {if (data($parm/NF_LOANA_INTERESTRATE) and xf:isData(data($parm/NF_LOANA_INTERESTRATE)))
          	then &lt;ns0:interestRate?>{data($parm/NF_LOANA_INTERESTRATE)}&lt;/ns0:interestRate>
          	else ()
          }
          {if (data($parm/NF_LOANA_INDEXRATENUMBER))
          	then &lt;ns0:indexRateNumber?>{data($parm/NF_LOANA_INDEXRATENUMBER)}&lt;/ns0:indexRateNumber>
          	else ()
          }
          {if (data($parm/NF_LOANA_INDEXRATEVALUE) and xf:isData(data($parm/NF_LOANA_INDEXRATEVALUE)))
          	then &lt;ns0:indexRateValue?>{data($parm/NF_LOANA_INDEXRATEVALUE)}&lt;/ns0:indexRateValue>
          	else ()
          }
          {if (data($parm/NF_LOANA_VARIANCEFROMINDEX) and xf:isData(data($parm/NF_LOANA_VARIANCEFROMINDEX)))
          	then &lt;ns0:varianceFromIndex?>{data($parm/NF_LOANA_VARIANCEFROMINDEX)}&lt;/ns0:varianceFromIndex>
          	else ()
          }
          {if (data($parm/NF_LOANA_RATEREVIEWPERIOD))
          	then &lt;ns0:rateReviewPeriod?>{data($parm/NF_LOANA_RATEREVIEWPERIOD)}&lt;/ns0:rateReviewPeriod>
          	else ()
          }
          {if (data($parm/NF_LOANA_RATEREVIEWFREQUEN) and xf:isData(data($parm/NF_LOANA_RATEREVIEWFREQUEN)))
          	then &lt;ns0:rateReviewFrequency?>{data($parm/NF_LOANA_RATEREVIEWFREQUEN)}&lt;/ns0:rateReviewFrequency>
          	else ()
          }
          {if (data($parm/NF_LOANA_NEXTRATEREVIEWCHA) and xf:isData(data($parm/NF_LOANA_NEXTRATEREVIEWCHA)))
          	then &lt;ns0:nextRateReviewChangeDate?>{data($parm/NF_LOANA_NEXTRATEREVIEWCHA)}&lt;/ns0:nextRateReviewChangeDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_FACEAMOUNT) and xf:isData(data($parm/NF_LOANA_FACEAMOUNT)))
          	then &lt;ns0:faceAmount?>{data($parm/NF_LOANA_FACEAMOUNT)}&lt;/ns0:faceAmount>
          	else ()
          }
          {if (data($parm/NF_LOANA_PRODUCTPPPTYPE))
          	then &lt;ns0:productpppType?>{data($parm/NF_LOANA_PRODUCTPPPTYPE)}&lt;/ns0:productpppType>
          	else ()
          }
          {if (data($parm/NF_LOANA_MATURITYDATE) and xf:isData(data($parm/NF_LOANA_MATURITYDATE)))
          	then &lt;ns0:maturityDate?>{data($parm/NF_LOANA_MATURITYDATE)}&lt;/ns0:maturityDate>
          	else ()
          }
          &lt;ns0:secured?>{xf:short2bool(data($parm/NF_LOANA_SECURED))}&lt;/ns0:secured>
          &lt;ns0:adjustAvail?>{xf:short2bool(data($parm/NF_LOANA_ADJUSTAVAIL))}&lt;/ns0:adjustAvail>
          {if (data($parm/NF_LOANA_RECOURSE))
          	then &lt;ns0:recourse?>{data($parm/NF_LOANA_RECOURSE)}&lt;/ns0:recourse>
          	else ()
          }
          {if (data($parm/NF_LOANA_INDEXRATEMULTIPLI))
          	then &lt;ns0:indexRateMultiplier?>{data($parm/NF_LOANA_INDEXRATEMULTIPLI)}&lt;/ns0:indexRateMultiplier>
          	else ()
          }
          {if (data($parm/NF_LOANA_RATEREVIEWSPDAY) and xf:isData(data($parm/NF_LOANA_RATEREVIEWSPDAY)))
          	then &lt;ns0:rateReviewSpDay?>{data($parm/NF_LOANA_RATEREVIEWSPDAY)}&lt;/ns0:rateReviewSpDay>
          	else ()
          }
          {if (data($parm/NF_LOANA_TIMESTOUSE) and xf:isData(data($parm/NF_LOANA_TIMESTOUSE)))
          	then &lt;ns0:timesToUse?>{data($parm/NF_LOANA_TIMESTOUSE)}&lt;/ns0:timesToUse>
          	else ()
          }
          {if (data($parm/NF_LOANA_INTERESTPERIOD))
          	then &lt;ns0:interestPeriod?>{data($parm/NF_LOANA_INTERESTPERIOD)}&lt;/ns0:interestPeriod>
          	else ()
          }
          {if (data($parm/NF_LOANA_INTERESTFREQUENCY) and xf:isData(data($parm/NF_LOANA_INTERESTFREQUENCY)))
          	then &lt;ns0:interestFrequency?>{data($parm/NF_LOANA_INTERESTFREQUENCY)}&lt;/ns0:interestFrequency>
          	else ()
          }
          {if (data($parm/NF_LOANA_FIRSTINTERESTBILL) and xf:isData(data($parm/NF_LOANA_FIRSTINTERESTBILL)))
          	then &lt;ns0:firstInterestBillingDate?>{data($parm/NF_LOANA_FIRSTINTERESTBILL)}&lt;/ns0:firstInterestBillingDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_SPECIFICDAYOFMONT) and xf:isData(data($parm/NF_LOANA_SPECIFICDAYOFMONT)))
          	then &lt;ns0:specificDayOfMonthToBill?>{data($parm/NF_LOANA_SPECIFICDAYOFMONT)}&lt;/ns0:specificDayOfMonthToBill>
          	else ()
          }
          {if (data($parm/NF_LOANA_RATECHANGESWINDEX) and xf:isData(data($parm/NF_LOANA_RATECHANGESWINDEX)))
          	then &lt;ns0:rateChangesWindexStartDate?>{data($parm/NF_LOANA_RATECHANGESWINDEX)}&lt;/ns0:rateChangesWindexStartDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_INTERESTDUE) and xf:isData(data($parm/NF_LOANA_INTERESTDUE)))
          	then &lt;ns0:interestDue?>{data($parm/NF_LOANA_INTERESTDUE)}&lt;/ns0:interestDue>
          	else ()
          }
          {if (data($parm/NF_LOANA_AVAILFORDISB) and xf:isData(data($parm/NF_LOANA_AVAILFORDISB)))
          	then &lt;ns0:availForDisb?>{data($parm/NF_LOANA_AVAILFORDISB)}&lt;/ns0:availForDisb>
          	else ()
          }
          {if (data($parm/NF_LOANA_PAYOFF) and xf:isData(data($parm/NF_LOANA_PAYOFF)))
          	then &lt;ns0:payoff?>{data($parm/NF_LOANA_PAYOFF)}&lt;/ns0:payoff>
          	else ()
          }
          {if (data($parm/NF_LOANA_PASTDUEPAYMENTS) and xf:isData(data($parm/NF_LOANA_PASTDUEPAYMENTS)))
          	then &lt;ns0:pastDuePayments?>{data($parm/NF_LOANA_PASTDUEPAYMENTS)}&lt;/ns0:pastDuePayments>
          	else ()
          }
          {if (data($parm/NF_LOANA_NEXTPAYMENTDATE) and xf:isData(data($parm/NF_LOANA_NEXTPAYMENTDATE)))
          	then &lt;ns0:nextPaymentDate?>{data($parm/NF_LOANA_NEXTPAYMENTDATE)}&lt;/ns0:nextPaymentDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_NEXTPAYMENTAMOUNT) and xf:isData(data($parm/NF_LOANA_NEXTPAYMENTAMOUNT)))
          	then &lt;ns0:nextPaymentAmount?>{data($parm/NF_LOANA_NEXTPAYMENTAMOUNT)}&lt;/ns0:nextPaymentAmount>
          	else ()
          }
          {if (data($parm/NF_LOANA_CUSTOMERRISKCODE))
          	then &lt;ns0:customerRiskCode?>{data($parm/NF_LOANA_CUSTOMERRISKCODE)}&lt;/ns0:customerRiskCode>
          	else ()
          }
          {if (data($parm/NF_LOANA_PDOSTATUS) and xf:isData(data($parm/NF_LOANA_PDOSTATUS)))
          	then &lt;ns0:pdoStatus?>{data($parm/NF_LOANA_PDOSTATUS)}&lt;/ns0:pdoStatus>
          	else ()
          }
          {if (data($parm/NF_LOANA_GOUGHTSOLD))
          	then &lt;ns0:goughtSold?>{data($parm/NF_LOANA_GOUGHTSOLD)}&lt;/ns0:goughtSold>
          	else ()
          }
          {if (data($parm/NF_LOANA_RATEEFFECTIVEDATE) and xf:isData(data($parm/NF_LOANA_RATEEFFECTIVEDATE)))
          	then &lt;ns0:rateEffectiveDate?>{data($parm/NF_LOANA_RATEEFFECTIVEDATE)}&lt;/ns0:rateEffectiveDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_PREVIOUSRATE) and xf:isData(data($parm/NF_LOANA_PREVIOUSRATE)))
          	then &lt;ns0:previousRate?>{data($parm/NF_LOANA_PREVIOUSRATE)}&lt;/ns0:previousRate>
          	else ()
          }
          {if (data($parm/NF_LOANA_ACCRUALBASE) and xf:isData(data($parm/NF_LOANA_ACCRUALBASE)))
          	then &lt;ns0:accrualBase?>{data($parm/NF_LOANA_ACCRUALBASE)}&lt;/ns0:accrualBase>
          	else ()
          }
          {if (data($parm/NF_LOANA_YEARBASE) and xf:isData(data($parm/NF_LOANA_YEARBASE)))
         	 then &lt;ns0:yearBase?>{data($parm/NF_LOANA_YEARBASE)}&lt;/ns0:yearBase>
         	 else ()
          }
          {if (data($parm/NF_LOANA_LATEFEEINTRATE) and xf:isData(data($parm/NF_LOANA_LATEFEEINTRATE)))
          	then &lt;ns0:lateFeeIntRate?>{data($parm/NF_LOANA_LATEFEEINTRATE)}&lt;/ns0:lateFeeIntRate>
          	else ()
          }
          {if (data($parm/NF_LOANA_LATECURRENTRATEDA) and xf:isData(data($parm/NF_LOANA_LATECURRENTRATEDA)))
          	then &lt;ns0:lateCurrentRateDate?>{data($parm/NF_LOANA_LATECURRENTRATEDA)}&lt;/ns0:lateCurrentRateDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_LATEPREVIOUSRATE) and xf:isData(data($parm/NF_LOANA_LATEPREVIOUSRATE)))
          	then &lt;ns0:latePreviousRate?>{data($parm/NF_LOANA_LATEPREVIOUSRATE)}&lt;/ns0:latePreviousRate>
          	else ()
          }
          {if (data($parm/NF_LOANA_LATEINDEX) and xf:isData(data($parm/NF_LOANA_LATEINDEX)))
          	then &lt;ns0:lateIndex?>{data($parm/NF_LOANA_LATEINDEX)}&lt;/ns0:lateIndex>
          	else ()
          }
          {if (data($parm/NF_LOANA_LATEVARIANCE) and xf:isData(data($parm/NF_LOANA_LATEVARIANCE)))
          	then &lt;ns0:lateVariance?>{data($parm/NF_LOANA_LATEVARIANCE)}&lt;/ns0:lateVariance>
          	else ()
          }
          {if (data($parm/NF_LOANA_LATEINTERESTRATEM) and xf:isData(data($parm/NF_LOANA_LATEINTERESTRATEM)))
          	then &lt;ns0:lateInterestRateMultiplier?>{data($parm/NF_LOANA_LATEINTERESTRATEM)}&lt;/ns0:lateInterestRateMultiplier>
          	else ()
          }
          {if (data($parm/NF_LOANA_LATEACCRUALBASE) and xf:isData(data($parm/NF_LOANA_LATEACCRUALBASE)))
          	then &lt;ns0:lateAccrualBase?>{data($parm/NF_LOANA_LATEACCRUALBASE)}&lt;/ns0:lateAccrualBase>
          	else ()
          }
          {if (data($parm/NF_LOANA_LATEYEARBASE) and xf:isData(data($parm/NF_LOANA_LATEYEARBASE)))
          	then &lt;ns0:lateYearBase?>{data($parm/NF_LOANA_LATEYEARBASE)}&lt;/ns0:lateYearBase>
          	else ()
          }
          {if (data($parm/NF_LOANA_CURRENTSCHEDULENB) and xf:isData(data($parm/NF_LOANA_CURRENTSCHEDULENB)))
          	then &lt;ns0:currentScheduleNbr?>{data($parm/NF_LOANA_CURRENTSCHEDULENB)}&lt;/ns0:currentScheduleNbr>
          	else ()
          }
          {if (data($parm/NF_LOANA_SCHEDULETIMESBILL) and xf:isData(data($parm/NF_LOANA_SCHEDULETIMESBILL)))
          	then &lt;ns0:scheduleTimesBilled?>{data($parm/NF_LOANA_SCHEDULETIMESBILL)}&lt;/ns0:scheduleTimesBilled>
          	else ()
          }
          {if (data($parm/NF_LOANA_BILLEDTYP) and xf:isData(data($parm/NF_LOANA_BILLEDTYP)))
          	then &lt;ns0:billedTyp?>{data($parm/NF_LOANA_BILLEDTYP)}&lt;/ns0:billedTyp>
          	else ()
          }
          {if (data($parm/NF_LOANA_BILLEDDUEDATE) and xf:isData(data($parm/NF_LOANA_BILLEDDUEDATE)))
          	then &lt;ns0:billedDueDate?>{data($parm/NF_LOANA_BILLEDDUEDATE)}&lt;/ns0:billedDueDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_BILLEDPRINCIPALAM) and xf:isData(data($parm/NF_LOANA_BILLEDPRINCIPALAM)))
          	then &lt;ns0:billedPrincipalAmt?>{data($parm/NF_LOANA_BILLEDPRINCIPALAM)}&lt;/ns0:billedPrincipalAmt>
          	else ()
          }
          {if (data($parm/NF_LOANA_BILLEDINTERESTAMT) and xf:isData(data($parm/NF_LOANA_BILLEDINTERESTAMT)))
          	then &lt;ns0:billedInterestAmt?>{data($parm/NF_LOANA_BILLEDINTERESTAMT)}&lt;/ns0:billedInterestAmt>
          	else ()
          }
          {if (data($parm/NF_LOANA_BILLEDFEE) and xf:isData(data($parm/NF_LOANA_BILLEDFEE)))
          	then &lt;ns0:billedFee?>{data($parm/NF_LOANA_BILLEDFEE)}&lt;/ns0:billedFee>
          	else ()
          }
          {if (data($parm/NF_LOANA_LATEFEEINTCOMPCOD) and xf:isData(data($parm/NF_LOANA_LATEFEEINTCOMPCOD)))
          	then &lt;ns0:lateFeeIntCompCode?>{data($parm/NF_LOANA_LATEFEEINTCOMPCOD)}&lt;/ns0:lateFeeIntCompCode>
          	else ()
          }
          {if (data($parm/NF_LOANA_PROCESSINGOPTION) and xf:isData(data($parm/NF_LOANA_PROCESSINGOPTION)))
          	then &lt;ns0:processingOption?>{data($parm/NF_LOANA_PROCESSINGOPTION)}&lt;/ns0:processingOption>
          	else ()
          }
          {if (data($parm/NF_LOANA_LATECHARGESDUE) and xf:isData(data($parm/NF_LOANA_LATECHARGESDUE)))
          	then &lt;ns0:lateChargesDue?>{data($parm/NF_LOANA_LATECHARGESDUE)}&lt;/ns0:lateChargesDue>
          	else ()
          }
          {if (data($parm/NF_LOANA_AASSESSEDTODATE) and xf:isData(data($parm/NF_LOANA_AASSESSEDTODATE)))
          	then &lt;ns0:aassessedToDate?>{data($parm/NF_LOANA_AASSESSEDTODATE)}&lt;/ns0:aassessedToDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_PAIDTODATE) and xf:isData(data($parm/NF_LOANA_PAIDTODATE)))
          	then &lt;ns0:paidToDate?>{data($parm/NF_LOANA_PAIDTODATE)}&lt;/ns0:paidToDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_WAIVEDTODATE) and xf:isData(data($parm/NF_LOANA_WAIVEDTODATE)))
          	then &lt;ns0:waivedToDate?>{data($parm/NF_LOANA_WAIVEDTODATE)}&lt;/ns0:waivedToDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_TIMESPASTDUECY1) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY1)))
          	then &lt;ns0:timesPastDueCY1?>{data($parm/NF_LOANA_TIMESPASTDUECY1)}&lt;/ns0:timesPastDueCY1>
          	else ()
          }
          {if (data($parm/NF_LOANA_TIMESPASTDUECY2) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY2)))
          	then &lt;ns0:timesPastDueCY2?>{data($parm/NF_LOANA_TIMESPASTDUECY2)}&lt;/ns0:timesPastDueCY2>
          	else ()
          }
          {if (data($parm/NF_LOANA_TIMESPASTDUECY3) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY3)))
          	then &lt;ns0:timesPastDueCY3?>{data($parm/NF_LOANA_TIMESPASTDUECY3)}&lt;/ns0:timesPastDueCY3>
          	else ()
          }
          {if (data($parm/NF_LOANA_TIMESPASTDUECY4) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY4)))
          	then &lt;ns0:timesPastDueCY4?>{data($parm/NF_LOANA_TIMESPASTDUECY4)}&lt;/ns0:timesPastDueCY4>
          	else ()
          }
          {if (data($parm/NF_LOANA_TIMESPASTDUECY5) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY5)))
          	then &lt;ns0:timesPastDueCY5?>{data($parm/NF_LOANA_TIMESPASTDUECY5)}&lt;/ns0:timesPastDueCY5>
          	else ()
          }
          {if (data($parm/NF_LOANA_TIMESPASTDUECY6) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY6)))
          	then &lt;ns0:timesPastDueCY6?>{data($parm/NF_LOANA_TIMESPASTDUECY6)}&lt;/ns0:timesPastDueCY6>
          	else ()
          }
          {if (data($parm/NF_LOANA_DUEAMOUNTFEE1) and xf:isData(data($parm/NF_LOANA_DUEAMOUNTFEE1)))
          	then &lt;ns0:dueAmountFee1?>{data($parm/NF_LOANA_DUEAMOUNTFEE1)}&lt;/ns0:dueAmountFee1>
          	else ()
          }
          {if (data($parm/NF_LOANA_DUEAMOUNTFEE2) and xf:isData(data($parm/NF_LOANA_DUEAMOUNTFEE2)))
          	then &lt;ns0:dueAmountFee2?>{data($parm/NF_LOANA_DUEAMOUNTFEE2)}&lt;/ns0:dueAmountFee2>
          	else ()
          }
          {if (data($parm/NF_LOANA_DUEAMOUNTFEE3) and xf:isData(data($parm/NF_LOANA_DUEAMOUNTFEE3)))
          	then &lt;ns0:dueAmountFee3?>{data($parm/NF_LOANA_DUEAMOUNTFEE3)}&lt;/ns0:dueAmountFee3>
          	else ()
          }
          {if (data($parm/NF_LOANA_ASSESSADJTODATEFE) and xf:isData(data($parm/NF_LOANA_ASSESSADJTODATEFE)))
          	then &lt;ns0:assessAdjToDateFee1?>{data($parm/NF_LOANA_ASSESSADJTODATEFE)}&lt;/ns0:assessAdjToDateFee1>
          	else ()
          }
          {if (data($parm/NF_LOANA_ASSESSADJTODATFEE) and xf:isData(data($parm/NF_LOANA_ASSESSADJTODATFEE)))
          	then &lt;ns0:assessAdjToDateFee2?>{data($parm/NF_LOANA_ASSESSADJTODATFEE)}&lt;/ns0:assessAdjToDateFee2>
          	else ()
          }
          {if (data($parm/NF_LOANA_ASSADJTODATFEE3) and xf:isData(data($parm/NF_LOANA_ASSADJTODATFEE3)))
          	then &lt;ns0:assessAdjToDateFee3?>{data($parm/NF_LOANA_ASSADJTODATFEE3)}&lt;/ns0:assessAdjToDateFee3>
          	else ()
          }
          {if (data($parm/NF_LOANA_PAIDTODATEFEE1) and xf:isData(data($parm/NF_LOANA_PAIDTODATEFEE1)))
          	then &lt;ns0:paidToDateFee1?>{data($parm/NF_LOANA_PAIDTODATEFEE1)}&lt;/ns0:paidToDateFee1>
          	else ()
          }
          {if (data($parm/NF_LOANA_PAIDTODATEFEE2) and xf:isData(data($parm/NF_LOANA_PAIDTODATEFEE2)))
          	then &lt;ns0:paidToDateFee2?>{data($parm/NF_LOANA_PAIDTODATEFEE2)}&lt;/ns0:paidToDateFee2>
          	else ()
          }
          {if (data($parm/NF_LOANA_PAIDTODATEFEE3) and xf:isData(data($parm/NF_LOANA_PAIDTODATEFEE3)))
          	then &lt;ns0:paidToDateFee3?>{data($parm/NF_LOANA_PAIDTODATEFEE3)}&lt;/ns0:paidToDateFee3>
          	else ()
          }
          &lt;ns0:subsidyOption?>{xf:short2bool(data($parm/NF_LOANA_SUBSIDYOPTION))}&lt;/ns0:subsidyOption>
          {if (data($parm/NF_LOANA_ORIGSUBSLOANAMT) and xf:isData(data($parm/NF_LOANA_ORIGSUBSLOANAMT)))
          	then &lt;ns0:origSubsLoanAmt?>{data($parm/NF_LOANA_ORIGSUBSLOANAMT)}&lt;/ns0:origSubsLoanAmt>
          	else ()
          }
          {if (data($parm/NF_LOANA_SUBSIDYLIMIT) and xf:isData(data($parm/NF_LOANA_SUBSIDYLIMIT)))
          	then &lt;ns0:subsidyLimit?>{data($parm/NF_LOANA_SUBSIDYLIMIT)}&lt;/ns0:subsidyLimit>
          	else ()
          }
          {if (data($parm/NF_LOANA_RATEEFFDATE) and xf:isData(data($parm/NF_LOANA_RATEEFFDATE)))
          	then &lt;ns0:rateEffDate?>{data($parm/NF_LOANA_RATEEFFDATE)}&lt;/ns0:rateEffDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_DAILYFACTOR) and xf:isData(data($parm/NF_LOANA_DAILYFACTOR)))
          	then &lt;ns0:dailyFactor?>{data($parm/NF_LOANA_DAILYFACTOR)}&lt;/ns0:dailyFactor>
          	else ()
          }
          {if (data($parm/NF_LOANA_SUBSIDYELIGIBLEDA) and xf:isData(data($parm/NF_LOANA_SUBSIDYELIGIBLEDA)))
          	then &lt;ns0:subsidyEligibleDate?>{data($parm/NF_LOANA_SUBSIDYELIGIBLEDA)}&lt;/ns0:subsidyEligibleDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_SUBSIDYLIMITEFFDA) and xf:isData(data($parm/NF_LOANA_SUBSIDYLIMITEFFDA)))
          	then &lt;ns0:subsidyLimitEffDate?>{data($parm/NF_LOANA_SUBSIDYLIMITEFFDA)}&lt;/ns0:subsidyLimitEffDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_CURRQUALBAL) and xf:isData(data($parm/NF_LOANA_CURRQUALBAL)))
          	then &lt;ns0:currQualBal?>{data($parm/NF_LOANA_CURRQUALBAL)}&lt;/ns0:currQualBal>
          	else ()
          }
          {if (data($parm/NF_LOANA_SUBSIDYBALANCE) and xf:isData(data($parm/NF_LOANA_SUBSIDYBALANCE)))
          	then &lt;ns0:subsidyBalance?>{data($parm/NF_LOANA_SUBSIDYBALANCE)}&lt;/ns0:subsidyBalance>
          	else ()
          }
          {if (data($parm/NF_LOANA_SUBSIDYRATE) and xf:isData(data($parm/NF_LOANA_SUBSIDYRATE)))
          	then &lt;ns0:subsidyRate?>{data($parm/NF_LOANA_SUBSIDYRATE)}&lt;/ns0:subsidyRate>
          	else ()
          }
          {if (data($parm/NF_LOANA_SUBSIDYACCRUED) and xf:isData(data($parm/NF_LOANA_SUBSIDYACCRUED)))
          	then &lt;ns0:subsidyAccrued?>{data($parm/NF_LOANA_SUBSIDYACCRUED)}&lt;/ns0:subsidyAccrued>
          	else ()
          }
          {if (data($parm/NF_LOANA_NEXTSUBSIDYCLAIMD) and xf:isData(data($parm/NF_LOANA_NEXTSUBSIDYCLAIMD)))
          	then &lt;ns0:nextSubsidyClaimDate?>{data($parm/NF_LOANA_NEXTSUBSIDYCLAIMD)}&lt;/ns0:nextSubsidyClaimDate>
          	else ()
          }
          {if (data($parm/NF_LOANA_LATEINDEXRATEVALU) and xf:isData(data($parm/NF_LOANA_LATEINDEXRATEVALU)))
          	then &lt;ns0:lateIndexRateValue?>{data($parm/NF_LOANA_LATEINDEXRATEVALU)}&lt;/ns0:lateIndexRateValue>
          	else ()
          }
          {if (data($parm/NF_LOANA_NEXTSCHEDULEDBILL) and xf:isData(data($parm/NF_LOANA_NEXTSCHEDULEDBILL)))
          	then &lt;ns0:nextScheduledBilling?>{data($parm/NF_LOANA_NEXTSCHEDULEDBILL)}&lt;/ns0:nextScheduledBilling>
          	else ()
          }	
          {if (data($parm/NF_LOANA_PAYMENTBILLEDSTAT))
          	then &lt;ns0:paymentBilledStatus?>{data($parm/NF_LOANA_PAYMENTBILLEDSTAT)}&lt;/ns0:paymentBilledStatus>
          	else ()
          }
          {if (data($parm/NF_LOANA_PASTDUEDAYS) and xf:isData(data($parm/NF_LOANA_PASTDUEDAYS)))
          	then &lt;ns0:pastDueDays?>{data($parm/NF_LOANA_PASTDUEDAYS)}&lt;/ns0:pastDueDays>
          	else ()
          }
          {if (data($parm/NF_LOANA_CYCLE1XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE1XLATEANDNOT)))
          	then &lt;ns0:cycle1XLateAndNotices?>{data($parm/NF_LOANA_CYCLE1XLATEANDNOT)}&lt;/ns0:cycle1XLateAndNotices>
          	else ()
          }
          {if (data($parm/NF_LOANA_CYCLE2XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE2XLATEANDNOT)))
          	then &lt;ns0:cycle2XLateAndNotices?>{data($parm/NF_LOANA_CYCLE2XLATEANDNOT)}&lt;/ns0:cycle2XLateAndNotices>
          	else ()
          }
          {if (data($parm/NF_LOANA_CYCLE3XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE3XLATEANDNOT)))
          	then &lt;ns0:cycle3XLateAndNotices?>{data($parm/NF_LOANA_CYCLE3XLATEANDNOT)}&lt;/ns0:cycle3XLateAndNotices>
          	else ()
          }
          {if (data($parm/NF_LOANA_CYCLE4XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE4XLATEANDNOT)))
          	then &lt;ns0:cycle4XLateAndNotices?>{data($parm/NF_LOANA_CYCLE4XLATEANDNOT)}&lt;/ns0:cycle4XLateAndNotices>
          	else ()
          }
          {if (data($parm/NF_LOANA_CYCLE5XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE5XLATEANDNOT)))
          	then &lt;ns0:cycle5XLateAndNotices?>{data($parm/NF_LOANA_CYCLE5XLATEANDNOT)}&lt;/ns0:cycle5XLateAndNotices>
          	else ()
          }
          {if (data($parm/NF_LOANA_CYCLE6XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE6XLATEANDNOT)))
          	then &lt;ns0:cycle6XLateAndNotices?>{data($parm/NF_LOANA_CYCLE6XLATEANDNOT)}&lt;/ns0:cycle6XLateAndNotices>
          	else ()
          }
          {getElementsForLoanAccountPastDueList($parm)}
          &lt;ns0:censusTract>
            &lt;ns2:CreditObject>
              {if (data($parm/NF_CREDIO_OBJECT))
              	then &lt;ns2:object?>{data($parm/NF_CREDIO_OBJECT)}&lt;/ns2:object>
              	else ()
              }
            &lt;/ns2:CreditObject>
          &lt;/ns0:censusTract>
        &lt;/ns0:LoanAccount>
      &lt;/ns0:loanAccount>
      &lt;ns0:currency>
        &lt;ns2:CurrencyCode>
          {if (data($parm/NF_CURREC_CURRENCYCODE))
          	then &lt;ns2:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE)}&lt;/ns2:currencyCode>
          	else ()
          }
        &lt;/ns2:CurrencyCode>
      &lt;/ns0:currency>
    &lt;/ns0:Account>
  &lt;/ns5:accountOut>
  &lt;ns5:bcd>
    &lt;ns3:BusinessControlData>
      &lt;ns3:pageControl>
        &lt;ns6:PageControl>
          &lt;ns6:hasNext?>{xf:short2bool(data($parm/NF_PAGEC_HASNEXT))}&lt;/ns6:hasNext>
          &lt;ns6:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition>
          &lt;ns6:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue>
        &lt;/ns6:PageControl>
      &lt;/ns3:pageControl>
    &lt;/ns3:BusinessControlData>
  &lt;/ns5:bcd>
&lt;/ns5:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>