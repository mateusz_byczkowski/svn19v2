<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:accountdict.dictionaries.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm = "1") then $trueval
    else $falseval
};


declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};

declare function xf:prepareShortAccountNumber($accountNumber as xs:string?) as xs:string?{
   let $accountNumberLength:= string-length($accountNumber)
   let $shortAccountNumber := substring($accountNumber,$accountNumberLength - 11)
   let $shortAccountNumberWithZeros := concat(substring("000000000000", string-length($shortAccountNumber) + 1) , $shortAccountNumber)
   return
       $shortAccountNumber
};

declare function xf:mapchgTranAccountParametersRequest($req as element(ns1:Account), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

let $msgId:= $msghead/dcl:transId
let $companyId:= $msghead/dcl:companyId
let $userId := $msghead/dcl:userId
let $appId:= $msghead/dcl:appId
let $unitId := $msghead/dcl:unitId
let $timestamp:= $msghead/dcl:timestamp

let $transId:=$tranhead/dcl:transId
let $skpOpener:=$req/ns1:skpOpener

let $accountNumber:= $req/ns1:accountNumber
let $statementForAccount:=$req/ns1:statementForAccount
let $productCode:= $req/ns1:productCode
let $serviceChargeCode:= $req/ns1:tranAccount/ns1:TranAccount/ns1:serviceChargeCode
let $interestPlanNumber:= $req/ns1:tranAccount/ns1:TranAccount/ns1:interestPlanNumber
let $accountTypeFlag:= $req/ns1:tranAccount/ns1:TranAccount/ns1:accountTypeFlag/ns2:AccountTypeFlag/ns2:accountTypeFlag
let $promotionCode:= $req/ns1:tranAccount/ns1:TranAccount/ns1:promotionCode/ns2:PromotionCode/ns2:promotionCode
let $frequency:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:frequency
let $nextPrintoutDate:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:nextPrintoutDate
let $aggregateTransaction:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:aggregateTransaction
let $period:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:cycle/ns2:Period/ns2:period
let $specialDay:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:specialDay
let $provideManner:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:provideManner/ns2:ProvideManner/ns2:provideManner
let $accessType:=$req/ns1:accessType/ns3:AccessType/ns3:accessType

return
&lt;fml:FML32>
  &lt;DC_TRN_ID?>{data($transId)}&lt;/DC_TRN_ID>
  &lt;DC_UZYTKOWNIK?>{concat("SKP:",data($userId))}&lt;/DC_UZYTKOWNIK>
  &lt;DC_ODDZIAL?>{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL>
  &lt;DC_NR_RACHUNKU?>{xf:prepareShortAccountNumber($accountNumber)}&lt;/DC_NR_RACHUNKU>
  &lt;DC_NUMER_PRODUKTU?>{data($productCode)}&lt;/DC_NUMER_PRODUKTU>
  &lt;DC_PLAN_OPLAT?>{data($serviceChargeCode)}&lt;/DC_PLAN_OPLAT>
  &lt;DC_PLAN_ODSETKOWY?>{data($interestPlanNumber)}&lt;/DC_PLAN_ODSETKOWY>
   {
    if((data($accountTypeFlag) = ' ') or (data($accountTypeFlag) = '') ) then
      &lt;DC_FLAGA_RACHUNKU?>00&lt;/DC_FLAGA_RACHUNKU>
    else
       &lt;DC_FLAGA_RACHUNKU?>{data($accountTypeFlag)}&lt;/DC_FLAGA_RACHUNKU>
   }
  &lt;DC_PROMOCJA?>{data($promotionCode)}&lt;/DC_PROMOCJA>
  &lt;DC_LICZBA_OKRESOW_CYKL_WYCIAG?>{data($frequency)}&lt;/DC_LICZBA_OKRESOW_CYKL_WYCIAG>
  &lt;DC_DATA_NASTEPNEGO_WYCIAGU?>{data($nextPrintoutDate)}&lt;/DC_DATA_NASTEPNEGO_WYCIAGU>

  &lt;DC_ZBIJANIE_TRANSAKCJI?>{boolean2SourceValue(data($aggregateTransaction),"1","0")}&lt;/DC_ZBIJANIE_TRANSAKCJI>
  (:&lt;DC_KONTROLA_WYCIAGOW?>{boolean2SourceValue(data($statementForAccount) ,"1","0")}&lt;/DC_KONTROLA_WYCIAGOW>:)
  &lt;DC_KONTROLA_WYCIAGOW?> &lt;/DC_KONTROLA_WYCIAGOW> 
  &lt;DC_KOD_CYKLU_WYCIAGOW?>{data($period)}&lt;/DC_KOD_CYKLU_WYCIAGOW>
  {
    if( string-length(data($specialDay)) > 0) then
    &lt;DC_OKRESLONY_DZIEN_WYCIAGU?>{data($specialDay)}&lt;/DC_OKRESLONY_DZIEN_WYCIAGU>
    else() 
   }
    &lt;DC_KOD_SPECJALNYCH_INSTRUKCJI?>{data($provideManner)}&lt;/DC_KOD_SPECJALNYCH_INSTRUKCJI>
  &lt;DC_KOD?>{data($accessType)}&lt;/DC_KOD>
&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


&lt;soap-env:Body>
{ xf:mapchgTranAccountParametersRequest($body/dcl:invoke/dcl:account/ns1:Account, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>