<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body>
  &lt;m:invokeResponse xmlns:urn="urn:be.services.dcl" />
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>