<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-10-28</con:description>
  <con:xquery>declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;

declare function getElementsForInvokeResponse($fml as element(fml:FML32)) as element()
{

&lt;ns4:invokeResponse>
&lt;ns4:mptTranAccoountList>
  {
    for $x at $occ in $fml/fml:B_LIMIT_KWOTA
    return
    &lt;ns0:TranAccountTurnover>
      &lt;ns0:monthLimit>{data($fml/fml:B_LIMIT_KWOTA[$occ])}&lt;/ns0:monthLimit>
      &lt;ns0:month>{data($fml/fml:B_MIESIAC[$occ])}&lt;/ns0:month>
      &lt;ns0:year>{data($fml/fml:B_ROK[$occ])}&lt;/ns0:year>
      &lt;ns0:monthRevenue>{data($fml/fml:B_OBROT_WN[$occ]) + data($fml/fml:B_OBROT_MA[$occ])}&lt;/ns0:monthRevenue>
      &lt;ns0:montLoad>{data($fml/fml:B_OBROT_WN[$occ])}&lt;/ns0:montLoad>
      &lt;ns0:monthCredit>{data($fml/fml:B_OBROT_MA[$occ])}&lt;/ns0:monthCredit>
      &lt;ns0:monthBalance>{data($fml/fml:B_AVG_SALDO_MA[$occ]) + data($fml/fml:B_AVG_SALDO_WN[$occ])}&lt;/ns0:monthBalance>
    &lt;/ns0:TranAccountTurnover>
  }
&lt;/ns4:mptTranAccoountList>
&lt;/ns4:invokeResponse>


};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>