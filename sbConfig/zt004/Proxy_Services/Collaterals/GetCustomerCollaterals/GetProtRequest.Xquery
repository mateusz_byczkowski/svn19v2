<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace u =   "urn:be.services.dcl";
declare namespace u1 =  "urn:cif.entities.be.dcl";
declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
declare variable $req := $body/u:invoke/u:customer/u1:Customer;
declare variable $reqh := $header/u:header;

&lt;soap-env:Body>
  &lt;fml:FML32>
    &lt;fml:DC_MSHEAD_MSGID?>{ data($reqh/u:msgHeader/u:msgId) }&lt;/fml:DC_MSHEAD_MSGID>
    {if($req/u1:customerNumber)
      then &lt;fml:DC_NUMER_KLIENTA>{ data($req/u1:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA>
      else ()
    }
    &lt;fml:DC_ENCODING>PL_MZV&lt;/fml:DC_ENCODING>
  &lt;/fml:FML32>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>