<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns5:invoke" location="getDepositRates.WSDL" ::)
(:: pragma bea:global-element-return element="ns0:FML32" location="fmlRequest.xsd" ::)

declare namespace ns4 = "urn:entities.be.dcl";
declare namespace xf = "http://tempuri.org/getDepositRates/getDepositRatesRequest/";
declare namespace ns0 = "http://www.example.org/fml_fields";
declare namespace ns1 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "urn:filterandmessages.dictionaries.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";
declare namespace ns6 = "urn:baseauxentities.be.dcl";

declare function xf:getDepositRatesRequest($invoke1 as element(ns5:invoke))
    as element(ns0:FML32) {
        &lt;ns0:FML32>
        	{
        		if (data($invoke1/ns5:filterDepositRate/ns1:FilterDepositRate/ns1:depositRateTables/ns2:DepositRateTables/ns2:depositRateTables)) then
        			 &lt;ns0:SP_ID_TABELI>{ data($invoke1/ns5:filterDepositRate/ns1:FilterDepositRate/ns1:depositRateTables/ns2:DepositRateTables/ns2:depositRateTables) }&lt;/ns0:SP_ID_TABELI>
        		else ()
        	}
        	{
        		if (data($invoke1/ns5:filterDepositRate/ns1:FilterDepositRate/ns1:originalAmount)) then
	        		&lt;ns0:SP_PROG>{ data($invoke1/ns5:filterDepositRate/ns1:FilterDepositRate/ns1:originalAmount) }&lt;/ns0:SP_PROG>
	        	else ()
        	}
        	{
        		if (data($invoke1/ns5:filterDepositRate/ns1:FilterDepositRate/ns1:currency/ns3:CurrencyCode/ns3:currencyCode)) then
		            &lt;ns0:SP_WALUTA>{ data($invoke1/ns5:filterDepositRate/ns1:FilterDepositRate/ns1:currency/ns3:CurrencyCode/ns3:currencyCode) }&lt;/ns0:SP_WALUTA>
		        else ()
        	}
            {
            	if (data($invoke1/ns5:filterDepositRate/ns1:FilterDepositRate/ns1:period/ns3:Period/ns3:period)) then
            		&lt;ns0:SP_OKRES>{ data($invoke1/ns5:filterDepositRate/ns1:FilterDepositRate/ns1:period/ns3:Period/ns3:period) }&lt;/ns0:SP_OKRES>
            	else ()
            }
            {
            	if (data($invoke1/ns5:filterDepositRate/ns1:FilterDepositRate/ns1:frequency)) then 
		            &lt;ns0:SP_CZEST>{ data($invoke1/ns5:filterDepositRate/ns1:FilterDepositRate/ns1:frequency)}&lt;/ns0:SP_CZEST>
		        else ()
			}
			{
				if (data($invoke1/ns5:bcd/ns4:BusinessControlData/ns4:pageControl/ns6:PageControl/ns6:actionCode) = 'N') then
		            &lt;ns0:CI_KIERUNEK>{ 1 }&lt;/ns0:CI_KIERUNEK>
		        else if (data($invoke1/ns5:bcd/ns4:BusinessControlData/ns4:pageControl/ns6:PageControl/ns6:actionCode) = 'P') then
		        	&lt;ns0:CI_KIERUNEK>{ -1 }&lt;/ns0:CI_KIERUNEK>
		        else ()
			}
			{
				if (data($invoke1/ns5:bcd/ns4:BusinessControlData/ns4:pageControl/ns6:PageControl/ns6:pageSize)) then
		            &lt;ns0:CI_LICZBA_OPER>{ data($invoke1/ns5:bcd/ns4:BusinessControlData/ns4:pageControl/ns6:PageControl/ns6:pageSize) }&lt;/ns0:CI_LICZBA_OPER>
				else ()
			}
        &lt;/ns0:FML32>
};

declare variable $invoke1 as element(ns5:invoke) external;

xf:getDepositRatesRequest($invoke1)</con:xquery>
</con:xqueryEntry>