<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "urn:be.services.dcl";
declare namespace b1 = "urn:entities.be.dcl";
declare namespace b2 = "urn:baseauxentities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function local:mapPaginationFields($fml as element())
	as element(m:bcd) {
		&lt;m:bcd>
			&lt;b1:BusinessControlData>
				&lt;b1:pageControl>
					&lt;b2:PageControl>
						&lt;b2:hasNext>
						{ 
						  if (data($fml/fml:NF_PAGEC_HASNEXT) = "1")
							then "true"
							else "false" 
						}
						&lt;/b2:hasNext>
                     	&lt;b2:navigationKeyDefinition>{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYDEFI) }&lt;/b2:navigationKeyDefinition>
                        &lt;b2:navigationKeyValue>{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/b2:navigationKeyValue>
					&lt;/b2:PageControl>
				&lt;/b1:pageControl>
			&lt;/b1:BusinessControlData>
		&lt;/m:bcd>		
};

declare variable $body as element(soap-env:Body) external;

local:mapPaginationFields($body/fml:FML32)</con:xquery>
</con:xqueryEntry>