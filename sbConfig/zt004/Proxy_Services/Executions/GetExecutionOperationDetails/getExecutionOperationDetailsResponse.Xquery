<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:executiondict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace ns4="urn:executions.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDateTime($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns3:invokeResponse>
  &lt;ns3:executionHistoryOut>
    &lt;ns4:ExecutionHistory>
      { insertDateTime(data($parm/NF_EXECUH_OPERATIONDATE),"yyyy-MM-dd-hh.mm.ss","ns4:operationDate")}
      &lt;ns4:operationTotalAmount?>{data($parm/NF_EXECUH_OPERATIONTOTALAM)}&lt;/ns4:operationTotalAmount>
      &lt;ns4:operationExecutionAmount?>{data($parm/NF_EXECUH_OPERATIONEXECUTI)}&lt;/ns4:operationExecutionAmount>
      &lt;ns4:operationExecutionExpense?>{data($parm/NF_EXECUH_OPERATIONEXEEXP)}&lt;/ns4:operationExecutionExpense>
      &lt;ns4:operationOtherExecutionExpense?>{data($parm/NF_EXECUH_OPERATIONOTHEREX)}&lt;/ns4:operationOtherExecutionExpense>
      &lt;ns4:operationExecutorInterest?>{data($parm/NF_EXECUH_OPERATIONEXEINT)}&lt;/ns4:operationExecutorInterest>
      &lt;ns4:operationBankInterest?>{data($parm/NF_EXECUH_OPERATIONBANKINT)}&lt;/ns4:operationBankInterest>
      &lt;ns4:operationTransferFee?>{data($parm/NF_EXECUH_OPERATIONTRANSFE)}&lt;/ns4:operationTransferFee>
      &lt;ns4:operationExecutorCharge?>{data($parm/NF_EXECUH_OPERATIONEXECUTO)}&lt;/ns4:operationExecutorCharge>
      &lt;ns4:operationType>
        &lt;ns0:ExecutionOperationType>
          &lt;ns0:executionOperationType?>{data($parm/NF_EXECOT_EXECUTIONOPERATI)}&lt;/ns0:executionOperationType>
        &lt;/ns0:ExecutionOperationType>
      &lt;/ns4:operationType>
    &lt;/ns4:ExecutionHistory>
  &lt;/ns3:executionHistoryOut>
&lt;/ns3:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>