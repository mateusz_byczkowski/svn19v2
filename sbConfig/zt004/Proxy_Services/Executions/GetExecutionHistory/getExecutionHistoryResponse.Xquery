<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:executiondict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:executions.entities.be.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:anyType,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDateTime($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
      if ($value)
        then if(string-length($value)>5 and not (substring($value,1,10) = "0001-01-01"))
            then prepareDateTime($value, $dateFormat, $fieldName)
        else() 
      else()
};

declare function prepareDateTime($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
     let $date := substring($value,1,10)
     let $time := substring($value, 12,8)
     let $datetime := concat(concat($date, "+"),$time)
   
     return
      element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$datetime)}
     
};


declare function getElementsForExecutionHistoryList($parm as element(fml:FML32)) as element()
{

&lt;ns4:executionHistoryList>
  {
    for $x at $occ in $parm/NF_EXECUH_OPERATIONID
    return
    &lt;ns5:ExecutionHistory>
      &lt;ns5:operationID?>{data($parm/NF_EXECUH_OPERATIONID[$occ])}&lt;/ns5:operationID>
      { insertDateTime(data($parm/NF_EXECUH_OPERATIONDATE[$occ]),"yyyy-MM-dd+hh.mm.ss","ns5:operationDate")}
      &lt;ns5:operationTotalAmount?>{data($parm/NF_EXECUH_OPERATIONTOTALAM[$occ])}&lt;/ns5:operationTotalAmount>
      &lt;ns5:operationType>
        &lt;ns0:ExecutionOperationType>
          &lt;ns0:executionOperationType?>{data($parm/NF_EXECOT_EXECUTIONOPERATI[$occ])}&lt;/ns0:executionOperationType>
        &lt;/ns0:ExecutionOperationType>
      &lt;/ns5:operationType>
    &lt;/ns5:ExecutionHistory>
  }
&lt;/ns4:executionHistoryList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  {getElementsForExecutionHistoryList($parm)}
  &lt;ns4:bcd>
    &lt;ns2:BusinessControlData>
      &lt;ns2:pageControl>
        &lt;ns6:PageControl>
          &lt;ns6:hasNext?>{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns6:hasNext>
          &lt;ns6:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition>
          &lt;ns6:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue>
        &lt;/ns6:PageControl>
      &lt;/ns2:pageControl>
    &lt;/ns2:BusinessControlData>
  &lt;/ns4:bcd>
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>