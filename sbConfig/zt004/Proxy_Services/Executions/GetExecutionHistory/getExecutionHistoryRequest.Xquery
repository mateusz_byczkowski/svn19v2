<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:executiondict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:executions.entities.be.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:anyType,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};


declare function getFieldsFromHeader($parm as element(ns4:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns4:msgHeader/ns4:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns4:msgHeader/ns4:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns4:msgHeader/ns4:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns4:msgHeader/ns4:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns4:msgHeader/ns4:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns4:msgHeader/ns4:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns4:transHeader/ns4:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns4:invoke)) as element()*
{

&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns4:bcd/ns2:BusinessControlData/ns2:pageControl/ns6:PageControl/ns6:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns4:bcd/ns2:BusinessControlData/ns2:pageControl/ns6:PageControl/ns6:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
&lt;NF_PAGEC_REVERSEORDER?>{boolean2SourceValue (data($parm/ns4:bcd/ns2:BusinessControlData/ns2:pageControl/ns6:PageControl/ns6:reverseOrder),"1","0")}&lt;/NF_PAGEC_REVERSEORDER>
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns4:bcd/ns2:BusinessControlData/ns2:pageControl/ns6:PageControl/ns6:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns4:bcd/ns2:BusinessControlData/ns2:pageControl/ns6:PageControl/ns6:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
,
&lt;NF_EXECUT_EXECUTIONNUMBERL?>{data($parm/ns4:execution/ns5:Execution/ns5:executionNumberLPP)}&lt;/NF_EXECUT_EXECUTIONNUMBERL>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns4:header)}
    {getFieldsFromInvoke($body/ns4:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>