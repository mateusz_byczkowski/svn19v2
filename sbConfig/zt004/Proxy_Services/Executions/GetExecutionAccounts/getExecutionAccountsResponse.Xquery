<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace ns6="urn:executions.entities.be.dcl";
declare namespace ns7="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:anyType,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForExecutionAccountList($parm as element(fml:FML32)) as element()
{

&lt;ns5:executionAccountList>
{ 
    for $x at $occ in $parm/NF_ACCOUN_CUSTOMERNUMBER
    return  
    &lt;ns6:ExecutionAccount>
      &lt;ns6:executionCharge?>{data($parm/NF_EXECUA_EXECUTIONCHARGE[$occ])}&lt;/ns6:executionCharge>
      &lt;ns6:coownerAvailableAmount?>{data($parm/NF_EXECUA_COOWNERAVAILABLE[$occ])}&lt;/ns6:coownerAvailableAmount>
      &lt;ns6:coownerUsedAmount?>{data($parm/NF_EXECUA_COOWNERUSEDAMOUN[$occ])}&lt;/ns6:coownerUsedAmount> 
       &lt;ns6:account>
        &lt;ns0:Account>
         &lt;ns0:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTIBAN[$occ])}&lt;/ns0:accountNumber> 
         &lt;ns0:customerNumber?>{data($parm/NF_ACCOUN_CUSTOMERNUMBER[$occ])}&lt;/ns0:customerNumber> 
         &lt;ns0:accountName?>{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}&lt;/ns0:accountName>
         &lt;ns0:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns0:accountDescription>
         &lt;ns0:currency>
            &lt;ns2:CurrencyCode>
             &lt;ns2:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns2:currencyCode>
            &lt;/ns2:CurrencyCode>
          &lt;/ns0:currency>
          &lt;ns0:accountBranchNumber>
            &lt;ns2:BranchCode>
              &lt;ns2:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE[$occ])}&lt;/ns2:branchCode>
            &lt;/ns2:BranchCode>
          &lt;/ns0:accountBranchNumber> 
        &lt;/ns0:Account>
      &lt;/ns6:account>
    &lt;/ns6:ExecutionAccount>
   } 
&lt;/ns5:executionAccountList>
};


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse>
   &lt;ns5:bcd>
    &lt;ns3:BusinessControlData>
      &lt;ns3:pageControl>
        &lt;ns7:PageControl>
          &lt;ns7:hasNext?>{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns7:hasNext>
          &lt;ns7:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns7:navigationKeyDefinition>
          &lt;ns7:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns7:navigationKeyValue>
        &lt;/ns7:PageControl>
      &lt;/ns3:pageControl>
    &lt;/ns3:BusinessControlData>
&lt;/ns5:bcd>
    {getElementsForExecutionAccountList($parm)} 
 &lt;/ns5:invokeResponse>
};


&lt;soap:Body>
 {getElementsForInvokeResponse($body/FML32)} 
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>