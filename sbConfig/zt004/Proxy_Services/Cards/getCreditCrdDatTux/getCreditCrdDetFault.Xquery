<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-01-07</con:description>
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace con="http://www.bea.com/wli/sb/context";
declare namespace con1="http://www.bea.com/wli/sb/stages/transform/config";
declare namespace WBKfault="http://bzwbk.com/services/faults/";

declare variable $fault external;




&lt;soap-env:Body>
    &lt;FML32>
    {
        let $urcode:=data($fault/con:details/con1:ReceivedFaultDetail/con1:detail/WBKfault:ServiceException/ErrorCode)
        return
            &lt;B_URCODE>{$urcode}&lt;/B_URCODE>
    }
    &lt;/FML32>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>