<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-01-07</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace prim= "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;


&lt;prim:PRIMEGetCustProductDetail xmlns:prim="http://bzwbk.com/services/prime/">
     &lt;prim:DC_NR_RACHUNKU>{fn:concat("V",$body/FML32/B_NR_KARTY)}&lt;/prim:DC_NR_RACHUNKU>
     &lt;prim:CI_OPCJA>0&lt;/prim:CI_OPCJA>
&lt;/prim:PRIMEGetCustProductDetail></con:xquery>
</con:xqueryEntry>