<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:card.entities.be.dcl";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:be.services.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:dictionaries.be.dcl";
declare namespace ns5="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7="urn:baseauxentities.be.dcl";
declare namespace ns8="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };

declare function insertDateTime($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };

declare function getElementsForCard($parm as element(fml:FML32)) as element()
{

&lt;ns2:card>
  {
    for $x at $occ in $parm/NF_CARD_BIN
    return
    &lt;ns0:Card>
      &lt;ns0:bin?>{data($parm/NF_CARD_BIN[$occ])}&lt;/ns0:bin>
      &lt;ns0:cardType?>{data($parm/NF_CARD_CARDTYPE[$occ])}&lt;/ns0:cardType>
      &lt;ns0:embossName1?>{data($parm/NF_CARD_EMBOSSNAME1[$occ])}&lt;/ns0:embossName1>
      &lt;ns0:embossName2?>{data($parm/NF_CARD_EMBOSSNAME2[$occ])}&lt;/ns0:embossName2>
      &lt;ns0:cardName?>{data($parm/NF_CARD_CARDNAME[$occ])}&lt;/ns0:cardName>
      &lt;ns0:debitCard>
        &lt;ns0:DebitCard>
          &lt;ns0:pinMailer?>{data($parm/NF_DEBITC_PINMAILER[$occ])}&lt;/ns0:pinMailer>
          &lt;ns0:pinDuplicate?>{data($parm/NF_DEBITC_PINDUPLICATE[$occ])}&lt;/ns0:pinDuplicate>
          &lt;ns0:nbrOfCardToGen?>{data($parm/NF_DEBITC_NBROFCARDTOGEN[$occ])}&lt;/ns0:nbrOfCardToGen>
          &lt;ns0:cardMailerNextCycle?>{data($parm/NF_DEBITC_CARDMAILERNEXTCY[$occ])}&lt;/ns0:cardMailerNextCycle>
          &lt;ns0:processingOptionFlag3?>{data($parm/NF_DEBITC_PROCESSINGOPTION[$occ])}&lt;/ns0:processingOptionFlag3>
          { insertDate(data($parm/NF_DEBITC_EXPIRATIONDATE[$occ]),"yyyy-MM-dd","ns0:expirationDate")}           
          &lt;ns0:contract?>{data($parm/NF_DEBITC_CONTRACT[$occ])}&lt;/ns0:contract>
          { insertDate(data($parm/NF_DEBITC_NEXTREISSUEDATE[$occ]),"yyyy-MM-dd","ns0:nextReissueDate")}
          { insertDate(data($parm/NF_DEBITC_NEXTCARDFEEDATE[$occ]),"yyyy-MM-dd","ns0:nextCardFeeDate")}
          &lt;ns0:cashLimit?>{data($parm/NF_DEBITC_CASHLIMIT[$occ])}&lt;/ns0:cashLimit>
          &lt;ns0:express?>{data($parm/NF_DEBITC_EXPRESS[$occ])}&lt;/ns0:express>
          &lt;ns0:policy?>{data($parm/NF_DEBITC_POLICY[$occ])}&lt;/ns0:policy>
          &lt;ns0:cardNbr?>{data($parm/NF_DEBITC_CARDNBR[$occ])}&lt;/ns0:cardNbr>
          &lt;ns0:cardDuplicate?>{data($parm/NF_DEBITC_CARDDUPLICATE[$occ])}&lt;/ns0:cardDuplicate>
          { insertDate(data($parm/NF_DEBITC_CARDRECORDOPENDA[$occ]),"yyyy-MM-dd","ns0:cardRecordOpenDate")}
          &lt;ns0:policyNbr?>{data($parm/NF_DEBITC_POLICYNBR[$occ])}&lt;/ns0:policyNbr>
          &lt;ns0:cardFee?>{data($parm/NF_DEBITC_CARDFEE[$occ])}&lt;/ns0:cardFee>
          &lt;ns0:individualTranList?>{data($parm/NF_DEBITC_INDIVIDUALTRANLI[$occ])}&lt;/ns0:individualTranList>
          &lt;ns0:collectiveTranList?>{data($parm/NF_DEBITC_COLLECTIVETRANLI[$occ])}&lt;/ns0:collectiveTranList>
          { insertDate(data($parm/NF_DEBITC_POLICYEXPIRDATE[$occ]),"yyyy-MM-dd","ns0:policyExpirDate")}
          &lt;ns0:changeParamCardHistory>
            &lt;ns0:ChangeParamCardHistory>
               { insertDateTime(data($parm/NF_CHAPCH_CHANGEDATE[$occ]),"yyyy-MM-dd-HH.mm.ss","ns0:changeDate")}
              &lt;ns0:userID?>{data($parm/NF_CHAPCH_USERID[$occ])}&lt;/ns0:userID>
              &lt;ns0:changeType>
                &lt;ns1:CrdChangeType>
                  &lt;ns1:crdChangeType?>{data($parm/NF_CRDCT_CRDCHANGETYPE[$occ])}&lt;/ns1:crdChangeType>
                &lt;/ns1:CrdChangeType>
              &lt;/ns0:changeType>
            &lt;/ns0:ChangeParamCardHistory>
          &lt;/ns0:changeParamCardHistory>
          &lt;ns0:resignCause>
            &lt;ns1:CrdResignCause>
              &lt;ns1:crdResignCause?>{data($parm/NF_CRDRC_CRDRESIGNCAUSE[$occ])}&lt;/ns1:crdResignCause>
            &lt;/ns1:CrdResignCause>
          &lt;/ns0:resignCause>
          &lt;ns0:addressFlag>
            &lt;ns1:CrdAddressFlag>
              &lt;ns1:crdAddressFlag?>{data($parm/NF_CRDAF_CRDADDRESSFLAG[$occ])}&lt;/ns1:crdAddressFlag>
            &lt;/ns1:CrdAddressFlag>
          &lt;/ns0:addressFlag>
          &lt;ns0:cycleLimit>
            &lt;ns1:CrdCycleLimit>
              &lt;ns1:crdCycleLimit?>{data($parm/NF_CRDCL_CRDCYCLELIMIT[$occ])}&lt;/ns1:crdCycleLimit>
            &lt;/ns1:CrdCycleLimit>
          &lt;/ns0:cycleLimit>
          &lt;ns0:weiverPIN>
            &lt;ns1:CrdWeiverPIN>
              &lt;ns1:crdWeiverPIN?>{data($parm/NF_CRWPIN_CRDWEIVERPIN[$occ])}&lt;/ns1:crdWeiverPIN>
            &lt;/ns1:CrdWeiverPIN>
          &lt;/ns0:weiverPIN>
          &lt;ns0:cardAddressBranchNumber>
            &lt;ns6:BranchCode>
              &lt;ns6:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE[$occ])}&lt;/ns6:branchCode>
            &lt;/ns6:BranchCode>
          &lt;/ns0:cardAddressBranchNumber>
          &lt;ns0:cardStatus>
            &lt;ns1:CrdStatus>
              &lt;ns1:crdStatus?>{data($parm/NF_CRDS_CRDSTATUS[$occ])}&lt;/ns1:crdStatus>
            &lt;/ns1:CrdStatus>
          &lt;/ns0:cardStatus>
        &lt;/ns0:DebitCard>
      &lt;/ns0:debitCard>
    &lt;/ns0:Card>
  }
&lt;/ns2:card>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns2:invokeResponse>
  &lt;ns2:response>
    &lt;ns5:ResponseMessage>
      &lt;ns5:result?>{data($parm/NF_RESPOM_RESULT)}&lt;/ns5:result>
      &lt;ns5:errorCode?>{data($parm/NF_RESPOM_ERRORCODE)}&lt;/ns5:errorCode>
      &lt;ns5:errorDescription?>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}&lt;/ns5:errorDescription>
    &lt;/ns5:ResponseMessage>
  &lt;/ns2:response>
  {getElementsForCard($parm)}
  &lt;ns2:bcd>
    &lt;ns8:BusinessControlData>
      &lt;ns8:pageControl>
        &lt;ns7:PageControl>
          &lt;ns7:hasNext?>{data($parm/NF_PAGEC_HASNEXT)}&lt;/ns7:hasNext>
          &lt;ns7:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns7:navigationKeyDefinition>
          &lt;ns7:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns7:navigationKeyValue>
        &lt;/ns7:PageControl>
      &lt;/ns8:pageControl>
    &lt;/ns8:BusinessControlData>
  &lt;/ns2:bcd>
&lt;/ns2:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>