<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  &lt;ns4:response>
    &lt;ns0:ResponseMessage>
     {
      if (data($parm/DC_OPIS_BLEDU)) then
      &lt;ns0:result?>false&lt;/ns0:result>
      else
       &lt;ns0:result?>true&lt;/ns0:result>
    }
    &lt;/ns0:ResponseMessage>
  &lt;/ns4:response>
  &lt;ns4:debitCardOut>
    &lt;ns5:DebitCard>
      { insertDate(data($parm/DC_DATA_WZNOWIENIA),"yyyy-MM-dd","ns5:nextReissueDate")}
      { insertDate(data($parm/DC_DATA_NALICZENIA),"yyyy-MM-dd","ns5:nextCardFeeDate")}
    &lt;/ns5:DebitCard>
  &lt;/ns4:debitCardOut>
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>