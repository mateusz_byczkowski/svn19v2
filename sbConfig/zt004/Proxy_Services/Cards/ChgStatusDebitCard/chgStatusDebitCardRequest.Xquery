<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:getFields($parm as element(ns4:invoke), $msghead as element(ns4:msgHeader), $tranhead as element(ns4:transHeader)) as element(fml:FML32)
{
  let $msgId:= $msghead/ns4:msgId
  let $companyId:= $msghead/ns4:companyId
  let $userId := $msghead/ns4:userId
  let $appId:= $msghead/ns4:appId
  let $unitId := $msghead/ns4:unitId
  let $timestamp:= $msghead/ns4:timestamp

  let $transId:=$tranhead/ns4:transId

  return
  &lt;fml:FML32>
     &lt;DC_TRN_ID?>{data($transId)}&lt;/DC_TRN_ID>
     &lt;DC_UZYTKOWNIK?>{data($userId)}&lt;/DC_UZYTKOWNIK>
     &lt;DC_ODDZIAL?>{data($unitId)}&lt;/DC_ODDZIAL>
     &lt;DC_TERMINAL_ID?>Terminal1&lt;/DC_TERMINAL_ID>
     &lt;DC_WIRT_NR_KARTY?>{data($parm/ns4:debitCard/ns5:DebitCard/ns5:virtualCardNbr)}&lt;/DC_WIRT_NR_KARTY>
     &lt;DC_STATUS_RACHUNKU?>{data($parm/ns4:debitCard/ns5:DebitCard/ns5:cardStatus/ns1:CrdStatus/ns1:crdStatus)}&lt;/DC_STATUS_RACHUNKU>
     &lt;DC_REZ_KARTA?>{data($parm/ns4:debitCard/ns5:DebitCard/ns5:resignCause/ns1:CrdResignCause/ns1:crdResignCause)}&lt;/DC_REZ_KARTA>
  &lt;/fml:FML32>

};

&lt;soap:Body>
     { xf:getFields($body/ns4:invoke, $header/ns4:header/ns4:msgHeader, $header/ns4:header/ns4:transHeader)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>