<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ARKA24/E-Business/messages/";
declare namespace xf = "http://bzwbk.com/services/ARKA24/E-Business/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapiFAZapisMDopiszRequest($req as element(m:iFAZapisMDopiszRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				&lt;fml:E_LOGIN_ID?>{ data($req/m:LoginId) }&lt;/fml:E_LOGIN_ID>
			}
			{
				&lt;fml:B_RODZ_DOK?>{ data($req/m:RodzDok) }&lt;/fml:B_RODZ_DOK>
			}
			{
				&lt;fml:B_NR_DOK?>{ data($req/m:NrDok) }&lt;/fml:B_NR_DOK>
			}
			{
				&lt;fml:E_CIF_NUMBER?>{ data($req/m:CifNumber) }&lt;/fml:E_CIF_NUMBER>
			}
			{
				&lt;fml:FA_KONWERSJA?>{ data($req/m:Konwersja) }&lt;/fml:FA_KONWERSJA>
			}
			{
				&lt;fml:FA_NR_Z?>{ data($req/m:NrZ) }&lt;/fml:FA_NR_Z>
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapiFAZapisMDopiszRequest($body/m:iFAZapisMDopiszRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>