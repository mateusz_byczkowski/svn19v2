<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "http://bzwbk.com/services/ceke/messages/";

declare function xf:map_getCEKEContractInPDFRequest($req as element(urn:GetCEKEContractInPDFRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
				&lt;fml:E_AGR_SIGNATURE?>{ data($req/signature) }&lt;/fml:E_AGR_SIGNATURE>
			        &lt;fml:E_SYSTEM_ID?>{ data($req/systemId) }&lt;/fml:E_SYSTEM_ID>			
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_getCEKEContractInPDFRequest($body/urn:GetCEKEContractInPDFRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>