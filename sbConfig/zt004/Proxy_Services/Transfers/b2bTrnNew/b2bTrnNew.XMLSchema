<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:schema><![CDATA[<?xml version="1.0" encoding="UTF-8"?>

<schema xmlns="http://www.w3.org/2001/XMLSchema" xmlns:tn="http://bzwbk.com/services/ceke/messages/"
	targetNamespace="http://bzwbk.com/services/ceke/messages/"
	elementFormDefault="qualified">
	<complexType name="MsgHeader">
		<sequence>
			<element name="OwnerId" minOccurs="1" maxOccurs="1" type="long" />
            <element name="KeyVariant" minOccurs="1" maxOccurs="1" type="short" />
            <element name="ExternalTimeStamp" minOccurs="1" maxOccurs="1" type="long" />
            <element name="ExternalSignature" minOccurs="1" maxOccurs="1">
                <simpleType>
                    <restriction base="string">
                        <minLength value="64" />
                        <maxLength value="64" />
                    </restriction>
                </simpleType>
            </element>
		</sequence>
	</complexType>
	<complexType name="SwiftDataType">
	   <sequence>
            <element name="BIC" minOccurs="1" maxOccurs="1">
                <simpleType>
                    <restriction base="string">
                        <minLength value="0" />
                        <maxLength value="11" />
                    </restriction>
                </simpleType>
            </element>
            <element name="SwiftBankLP" minOccurs="1" maxOccurs="1" type="long" />
            <element name="SwiftBankBranchCode" minOccurs="1" maxOccurs="1">
                <simpleType>
                    <restriction base="string">
                        <minLength value="0" />
                        <maxLength value="33" />
                    </restriction>
                </simpleType>
            </element>
            <element name="SwiftBankBranchName" minOccurs="1" maxOccurs="1">
                <simpleType>
                    <restriction base="string">
                        <minLength value="0" />
                        <maxLength value="80" />
                    </restriction>
                </simpleType>
            </element>
            <element name="SwiftBankName" minOccurs="1" maxOccurs="1">
                <simpleType>
                    <restriction base="string">
                        <minLength value="0" />
                        <maxLength value="35" />
                    </restriction>
                </simpleType>
            </element>
            <element name="SwiftBankAddress" minOccurs="1" maxOccurs="1">
                <simpleType>
                    <restriction base="string">
                        <minLength value="0" />
                        <maxLength value="80" />
                    </restriction>
                </simpleType>
            </element>
            <element name="SwiftBankTown" minOccurs="1" maxOccurs="1">
                <simpleType>
                    <restriction base="string">
                        <minLength value="0" />
                        <maxLength value="35" />
                    </restriction>
                </simpleType>
            </element>
            <element name="SwiftBankCountry" minOccurs="1" maxOccurs="1">
                <simpleType>
                    <restriction base="string">
                        <minLength value="0" />
                        <maxLength value="35" />
                    </restriction>
                </simpleType>
            </element>
            <element name="SwiftCost" minOccurs="1" maxOccurs="1"> <!-- koszty ponosi -->
                <simpleType>
                    <restriction base="short">
                        <enumeration value="1" /> <!-- beneficjent -->
                        <enumeration value="2" /> <!-- zleceniodawca -->
                        <enumeration value="3" /> <!-- wspoldzielony -->
                    </restriction>
                </simpleType>
            </element>
            <element name="SwiftType" minOccurs="1" maxOccurs="1"> <!-- typ kursu waluty -->
                <simpleType>
                    <restriction base="short">
                        <enumeration value="0" /> <!-- normalny -->
                        <enumeration value="1" /> <!-- negocjowany -->
                        <enumeration value="3" /> <!-- negocjowany z limitem -->
                    </restriction>
                </simpleType>
            </element>
            <element name="SwiftPriority" minOccurs="1" maxOccurs="1"> <!-- priorytet -->
                <simpleType>
                    <restriction base="short">
                        <enumeration value="0" /> <!-- niski -->
                        <enumeration value="1" /> <!-- wysoki -->
                    </restriction>
                </simpleType>
            </element>
            <element name="SwiftSpot" minOccurs="1" maxOccurs="1"> <!-- tryb realizacji -->
                <simpleType>
                    <restriction base="short">
                        <enumeration value="0" /> <!-- Normalny -->
                        <enumeration value="1" /> <!-- Pilny -->
                        <enumeration value="2" /> <!-- Ekspresowy -->
                    </restriction>
                </simpleType>
            </element>
        </sequence>
	</complexType>
	<complexType name="TrnData">
		<sequence>
			<element name="TrnType" minOccurs="1" maxOccurs="1">
				<simpleType>
					<restriction base="short">
						<enumeration value="0" /> <!-- Elixir -->
						<enumeration value="1" /> <!-- Swift -->
						<enumeration value="2" /> <!-- Walutowy wewnetrzny -->
						<enumeration value="3" /> <!-- Sorbnet -->
					</restriction>
				</simpleType>
			</element>
			<element name="SrcAccountNo" minOccurs="1" maxOccurs="1">
				<simpleType>
					<restriction base="string">
						<minLength value="10" />
						<maxLength value="44" />
					</restriction>
				</simpleType>
			</element>
			<element name="SrcAmount" minOccurs="1" maxOccurs="1" type="decimal" />
			<element name="DstAccountNo" minOccurs="1" maxOccurs="1">
				<simpleType>
					<restriction base="string">
						<minLength value="1" />
						<maxLength value="44" />
					</restriction>
				</simpleType>
			</element>
			<element name="DstAmount" minOccurs="0" maxOccurs="1" type="decimal" />
			<element name="DstCurrency" minOccurs="1" maxOccurs="1">
				<simpleType>
					<restriction base="string">
						<minLength value="3" />
						<maxLength value="3" />
					</restriction>
				</simpleType>
			</element>
			<element name="DstName" minOccurs="1" maxOccurs="1">
				<simpleType>
					<restriction base="string">
						<minLength value="1" />
						<maxLength value="32" />
					</restriction>
				</simpleType>
			</element>
			<element name="DstCity" minOccurs="1" maxOccurs="1">
				<simpleType>
					<restriction base="string">
						<minLength value="1" />
						<maxLength value="50" />
					</restriction>
				</simpleType>
			</element>
			<element name="DstZipCode" minOccurs="1" maxOccurs="1">
				<simpleType>
					<restriction base="string">
						<minLength value="0" />
						<maxLength value="8" />
					</restriction>
				</simpleType>
			</element>
			<element name="DstStreet" minOccurs="1" maxOccurs="1">
				<simpleType>
					<restriction base="string">
						<minLength value="1" />
						<maxLength value="50" />
					</restriction>
				</simpleType>
			</element>
			<element name="Title" minOccurs="1" maxOccurs="1">
				<simpleType>
					<restriction base="string">
						<minLength value="1" />
						<maxLength value="140" />
					</restriction>
				</simpleType>
			</element>
            <element name="SwiftData" type="tn:SwiftDataType"  minOccurs="0" maxOccurs="1"/>
		</sequence>
	</complexType>

    <element name="b2bHeader">
        <complexType>
            <sequence>
                <element name="msgHeader" type="tn:MsgHeader"
                    minOccurs="1" maxOccurs="1" nillable="false"/>
            </sequence>
        </complexType>
     </element>
     <element name="TrnNewRequest">
		<complexType>
			<sequence>
				<element name="UserId" minOccurs="2" maxOccurs="2" type="long" />
				<element name="TokenSignature" minOccurs="2" maxOccurs="2" type="long" />
				<element name="TokenDatetime" minOccurs="2" maxOccurs="2" type="long" />
				<element name="TrnData" type="tn:TrnData" minOccurs="1" maxOccurs="unbounded" ></element>
			</sequence>
		</complexType>
	</element>
	<element name="TrnNewResponse">
		<complexType>
			<sequence>
				<element name="TrnDate" type="date" minOccurs="0"  maxOccurs="unbounded" nillable="true" />
				<element name="TrnId" type="long" minOccurs="0"  maxOccurs="unbounded" nillable="true" />
				<element name="TrnState" minOccurs="0"  maxOccurs="unbounded" nillable="true" >
					<simpleType>
						<restriction base="string">
							<enumeration value="A" /> <!-- Niewykonana - Oczekujaca -->
							<enumeration value="W" /> <!-- Niewykonana - Realizowana -->
                            <enumeration value="M" /> <!-- Wykonywana -->
							<enumeration value="Z" /> <!-- Wykonana -->
                            <enumeration value="X" /> <!-- Anulowana -->
                            <enumeration value="a" /> <!-- Odrzucona (brak srodkow) -->
                            <enumeration value="b" /> <!-- Odrzucona (rachunek zablokowany) -->
                            <enumeration value="c" /> <!-- Odrzucona (brak rachunku) -->
                            <enumeration value="d" /> <!-- Odrzucona (odmowa wykonania) -->
                            <enumeration value="e" /> <!-- Odrzucona  -->
                            <enumeration value="f" /> <!-- Odrzucona (brak produktow) -->
                            <enumeration value="g" /> <!-- Odrzucona (niezgodna z regulaminem) -->
                            <enumeration value="h" /> <!-- Odrzucona (zbyt mala kwota) -->
                            <enumeration value="i" /> <!-- Odrzucona (zbyt duza kwota) -->
                            <enumeration value="x" /> <!-- Odrzucona (blad wykonania) -->
                            <enumeration value="y" /> <!-- Odrzucona (blad wykonania) -->
                            <enumeration value="z" /> <!-- Odrzucona (blad wykonania) -->
						</restriction>
					</simpleType>
				</element>
                <element name="TrnError" minOccurs="0"  maxOccurs="unbounded" nillable="true" >
                    <simpleType>
                        <restriction base="short">
                            <enumeration value="0" /> <!-- OK -->
                            <enumeration value="13" /> <!-- Transakcja niedozwolona -->
                            <enumeration value="16" /> <!-- Niedozwolony typ rachunku -->
                            <enumeration value="17" /> <!-- przekroczony limit transakcji -->
                            <enumeration value="21" /> <!-- Nie znaleziono klienta -->
                            <enumeration value="23" /> <!-- Nie znaleziono rachunku  -->
                            <enumeration value="24" /> <!-- Nie znaleziono rachunku danego typu -->
                            <enumeration value="27" /> <!-- Nie znaleziono transakcji danego typu -->
                            <enumeration value="42" /> <!-- Zbyt mala kwota transakcji -->
                            <enumeration value="43" /> <!-- Zbyt duza kwota transakcji  -->
                            <enumeration value="51" /> <!-- Nie znaleziono oddzialu banku -->
                            <enumeration value="52" /> <!-- Nie znaleziono banku -->
                            <enumeration value="1001" /> <!-- Inny blad -->
                        </restriction>
                    </simpleType>
                </element>
			</sequence>
		</complexType>
	</element>
</schema>]]></con:schema>
  <con:targetNamespace>http://bzwbk.com/services/ceke/messages/</con:targetNamespace>
</con:schemaEntry>