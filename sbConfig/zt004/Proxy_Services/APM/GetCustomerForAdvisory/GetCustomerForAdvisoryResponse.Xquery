<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCustomerForAdvisoryResponse($fml as element(fml:FML32))
	as element(m:GetCustomerForAdvisoryResponse) {
		&lt;m:GetCustomerForAdvisoryResponse>
			{
				let $CI_STATUS_AKT_DORADZTWA := $fml/fml:CI_STATUS_AKT_DORADZTWA return
					if($CI_STATUS_AKT_DORADZTWA)
						then &lt;m:StatusAktywnosciDoradztwa>{ data($CI_STATUS_AKT_DORADZTWA) }&lt;/m:StatusAktywnosciDoradztwa>
						else ()
			}
			{
				let $CI_PROFIL_INWESTYCYJNY := $fml/fml:CI_PROFIL_INWESTYCYJNY return
					if ($CI_PROFIL_INWESTYCYJNY)
						then &lt;m:ProfilInwestycyjny>{ data($CI_PROFIL_INWESTYCYJNY) }&lt;/m:ProfilInwestycyjny>
						else ()
			}
			{
				let $CI_DATA_AKT_DORADZTWA := $fml/fml:CI_DATA_AKT_DORADZTWA return
					if ($CI_DATA_AKT_DORADZTWA)
						then &lt;m:DataAktywacjiUslugi>{ data($CI_DATA_AKT_DORADZTWA) }&lt;/m:DataAktywacjiUslugi>
						else ()
			}
			{
				let $CI_DATA_ZAK_DORADZTWA := $fml/fml:CI_DATA_ZAK_DORADZTWA return
					if ($CI_DATA_ZAK_DORADZTWA)
						then &lt;m:DataOstatniejUslugi>{ data($CI_DATA_ZAK_DORADZTWA) }&lt;/m:DataOstatniejUslugi>
						else ()
			}
			{
				let $CI_ID_DORADZTWA := $fml/fml:CI_ID_DORADZTWA return
					if ($CI_ID_DORADZTWA)
						then &lt;m:IdentyfikatorDoradztwa>{ data($CI_ID_DORADZTWA) }&lt;/m:IdentyfikatorDoradztwa>
						else ()
			}
			{
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA return
					if ($CI_UDOSTEP_GRUPA)
						then &lt;m:UdostepGrupa>{ data($CI_UDOSTEP_GRUPA) }&lt;/m:UdostepGrupa>
						else ()
			}
			{
				let $DC_IMIE := $fml/fml:DC_IMIE return
					if ($DC_IMIE)
						then &lt;m:Imie>{ data($DC_IMIE) }&lt;/m:Imie>
						else ()
			}
			{
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO return
					if ($DC_NAZWISKO)
						then &lt;m:Nazwisko>{ data($DC_NAZWISKO) }&lt;/m:Nazwisko>
						else ()
			}
			{
				let $DC_DRUGIE_IMIE := $fml/fml:DC_DRUGIE_IMIE return
					if ($DC_DRUGIE_IMIE)
						then &lt;m:DrugieImie>{ data($DC_DRUGIE_IMIE) }&lt;/m:DrugieImie>
						else ()
			}
			{
				let $DC_TYP_ADRESU := $fml/fml:DC_TYP_ADRESU return
					if ($DC_TYP_ADRESU)
						then &lt;m:TypAdresu>{ data($DC_TYP_ADRESU) }&lt;/m:TypAdresu>
						else ()
			}
			{
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST return
					if ($DC_ULICA_DANE_PODST)
						then &lt;m:UlicaDanePodst>{ data($DC_ULICA_DANE_PODST) }&lt;/m:UlicaDanePodst>
						else ()
			}
			{
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST return
					if ($DC_NR_POSES_LOKALU_DANE_PODST)
						then &lt;m:NrPosesLokaluDanePodst>{ data($DC_NR_POSES_LOKALU_DANE_PODST) }&lt;/m:NrPosesLokaluDanePodst>
						else ()
			}
			{
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST return
					if ($DC_MIASTO_DANE_PODST)
						then &lt;m:MiastoDanePodst>{ data($DC_MIASTO_DANE_PODST) }&lt;/m:MiastoDanePodst>
						else ()
			}
			{
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST return
					if ($DC_KOD_POCZTOWY_DANE_PODST)
						then &lt;m:KodPocztowyDanePodst>{ data($DC_KOD_POCZTOWY_DANE_PODST) }&lt;/m:KodPocztowyDanePodst>
						else ()
			}
			{
				let $DC_WOJ_KRAJ_DANE_PODST := $fml/fml:DC_WOJ_KRAJ_DANE_PODST return
					if ($DC_WOJ_KRAJ_DANE_PODST)
						then &lt;m:Wojewodztwo>{ data($DC_WOJ_KRAJ_DANE_PODST) }&lt;/m:Wojewodztwo>
						else ()
			}
			{
				let $CI_KOD_KRAJU := $fml/fml:CI_KOD_KRAJU return
					if ($CI_KOD_KRAJU)
						then &lt;m:KodKraju>{ (data($CI_KOD_KRAJU) cast as xs:decimal) cast as xs:short }&lt;/m:KodKraju>
						else ()
			}
			{
				let $CI_KOD_KRAJU_KORESP := $fml/fml:CI_KOD_KRAJU_KORESP
				let $CI_DATA_OD := $fml/fml:CI_DATA_OD
				let $CI_DATA_DO := $fml/fml:CI_DATA_DO
				let $DC_IMIE_I_NAZWISKO_ALT := $fml/fml:DC_IMIE_I_NAZWISKO_ALT
				let $DC_ULICA_ADRES_ALT := $fml/fml:DC_ULICA_ADRES_ALT
				let $DC_NR_POSES_LOKALU_ADRES_ALT := $fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT
				let $DC_MIASTO_ADRES_ALT := $fml/fml:DC_MIASTO_ADRES_ALT
				let $DC_KOD_POCZTOWY_ADRES_ALT := $fml/fml:DC_KOD_POCZTOWY_ADRES_ALT
				let $DC_WOJEWODZTWO_KRAJ_ADRES_ALT := $fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT
				let $CI_TYP_ADRESU := $fml/fml:CI_TYP_ADRESU

				for $it at $p in $fml/fml:DC_IMIE_I_NAZWISKO_ALT
				return
					&lt;m:APMGetCustomerAdresAlt>
						{
							if ($CI_KOD_KRAJU_KORESP[$p])
								then &lt;m:KodKrajuKoresp>{ data($CI_KOD_KRAJU_KORESP[$p]) }&lt;/m:KodKrajuKoresp>
							else ()
						}
						{
							if ($CI_DATA_OD[$p])
								then &lt;m:DataOd>{ data($CI_DATA_OD[$p]) }&lt;/m:DataOd>
							else ()
						}
						{
							if ($CI_DATA_DO[$p])
								then &lt;m:DataDo>{ data($CI_DATA_DO[$p]) }&lt;/m:DataDo>
							else ()
						}
						{
							if ($DC_IMIE_I_NAZWISKO_ALT[$p])
								then &lt;m:ImieINazwiskoAlt>{ data($DC_IMIE_I_NAZWISKO_ALT[$p]) }&lt;/m:ImieINazwiskoAlt>
							else ()
						}
						{
							if ($DC_ULICA_ADRES_ALT[$p])
								then &lt;m:UlicaAdresAlt>{ data($DC_ULICA_ADRES_ALT[$p]) }&lt;/m:UlicaAdresAlt>
							else ()
						}
						{
							if ($DC_NR_POSES_LOKALU_ADRES_ALT[$p])
								then &lt;m:NrPosesLokaluAdresAlt>{ data($DC_NR_POSES_LOKALU_ADRES_ALT[$p]) }&lt;/m:NrPosesLokaluAdresAlt>
							else ()
						}
						{
							if ($DC_MIASTO_ADRES_ALT[$p])
								then &lt;m:MiastoAdresAlt>{ data($DC_MIASTO_ADRES_ALT[$p]) }&lt;/m:MiastoAdresAlt>
							else ()
						}
						{
							if ($DC_KOD_POCZTOWY_ADRES_ALT[$p])
								then &lt;m:KodPocztowyAdresAlt>{ data($DC_KOD_POCZTOWY_ADRES_ALT[$p]) }&lt;/m:KodPocztowyAdresAlt>
							else ()
						}
						{
							if ($DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$p])
								then &lt;m:WojewodztwoKrajAdresAlt>{ data($DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$p]) }&lt;/m:WojewodztwoKrajAdresAlt>
							else ()
						}
						{
							if ($CI_TYP_ADRESU[$p])
								then &lt;m:TypAdresuKoresp>{ data($CI_TYP_ADRESU[$p]) }&lt;/m:TypAdresuKoresp>
							else ()
						}
					&lt;/m:APMGetCustomerAdresAlt>
			}
			{
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL return
					if ($DC_NR_PESEL)
						then &lt;m:NrPesel>{ data($DC_NR_PESEL) }&lt;/m:NrPesel>
						else ()
			}
			{
				let $DC_REZ_NIEREZ := $fml/fml:DC_REZ_NIEREZ return
					if ($DC_REZ_NIEREZ)
						then &lt;m:RezNierez>{ data($DC_REZ_NIEREZ) }&lt;/m:RezNierez>
						else ()
			}
			{
				for $v in $fml/fml:CI_DOK_TOZSAMOSCI
				return
					&lt;m:DokTozsamosci>{ data($v) }&lt;/m:DokTozsamosci>
			}
			{
				for $v in $fml/fml:CI_SERIA_NR_DOK
				return
					&lt;m:SeriaNrDok>{ data($v) }&lt;/m:SeriaNrDok>
			}
			{
				let $DC_NR_DOWODU_REGON := $fml/fml:DC_NR_DOWODU_REGON return	
					if ($DC_NR_DOWODU_REGON)
						then &lt;m:NrDowoduRegon>{ data($DC_NR_DOWODU_REGON) }&lt;/m:NrDowoduRegon>
						else ()
			}
			{
				let $DC_NUMER_PASZPORTU := $fml/fml:DC_NUMER_PASZPORTU return
					if ($DC_NUMER_PASZPORTU)
						then &lt;m:NumerPaszportu>{ data($DC_NUMER_PASZPORTU) }&lt;/m:NumerPaszportu>
						else ()
			}
			{
				let $DC_NUMER_KLIENTA_REL := $fml/fml:DC_NUMER_KLIENTA_REL
				let $DC_TYP_RELACJI := $fml/fml:DC_TYP_RELACJI

				for $it at $p in $fml/fml:DC_NUMER_KLIENTA_REL
				return
					&lt;m:Relacje>
						{
							if ($DC_NUMER_KLIENTA_REL[$p])
								then &lt;m:customerID>{ data($DC_NUMER_KLIENTA_REL[$p]) }&lt;/m:customerID>
								else ()
						}
						{
							if ($DC_TYP_RELACJI[$p])
								then &lt;m:relationType>{ data($DC_TYP_RELACJI[$p]) }&lt;/m:relationType>
								else ()
						}
					&lt;/m:Relacje>
			}
			{
				let $DC_PLEC := $fml/fml:DC_PLEC return
					if ($DC_PLEC)
						then &lt;m:Plec>{ data($DC_PLEC) }&lt;/m:Plec>
						else ()
			}
			{
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA return
					if ($DC_TYP_KLIENTA)
						then &lt;m:TypKlienta>{ data($DC_TYP_KLIENTA) }&lt;/m:TypKlienta>
						else ()
			}
		&lt;/m:GetCustomerForAdvisoryResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetCustomerForAdvisoryResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>