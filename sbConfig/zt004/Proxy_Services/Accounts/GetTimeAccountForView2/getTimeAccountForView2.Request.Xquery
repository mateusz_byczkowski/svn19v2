<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns1="urn:filterandmessages.dictionaries.be.dcl";
declare namespace ns0="urn:transferorder.entities.be.dcl";
declare namespace ns2="urn:be.services.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:dictionaries.be.dcl";
declare namespace ns5="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns2:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns2:msgHeader/ns2:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns2:msgHeader/ns2:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns2:msgHeader/ns2:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns2:msgHeader/ns2:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns2:msgHeader/ns2:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns2:msgHeader/ns2:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns2:transHeader/ns2:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns2:invoke)) as element()*
{

&lt;NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns2:account/ns5:Account/ns5:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns2:header)}
    {getFieldsFromInvoke($body/ns2:invoke)}
    &lt;NF_INTEGH_VALUE>2&lt;/NF_INTEGH_VALUE>
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>