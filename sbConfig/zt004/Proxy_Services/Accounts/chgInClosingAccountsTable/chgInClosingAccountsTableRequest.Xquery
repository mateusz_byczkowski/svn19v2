<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2010-01-21
 :
 : wersja WSDLa: 08-01-2010 13:09:21
 :
 : $Proxy Services/Accounts/chgInClosingAccountsTable/chgInClosingAccountsRequest.xq$
 
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/chgInClosingAccountsTable/chgInClosingAccountsTableRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:accountdict.dictionaries.be.dcl";
declare namespace ns2 = "urn:accounts.entities.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";

declare variable $header1 as element(ns3:header) external;
declare variable $invoke1 as element(ns3:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)

declare function xf:chgInClosingAccountsTableRequest($header1 as element(ns3:header),
													   $invoke1 as element(ns3:invoke))
    as element(ns0:FML32)
{
	&lt;ns0:FML32>

		&lt;ns0:DC_TRN_ID?>{
			data($header1/ns3:transHeader/ns3:transId)
		}&lt;/ns0:DC_TRN_ID>

		&lt;ns0:DC_UZYTKOWNIK?>{
			data($header1/ns3:msgHeader/ns3:userId)
		}&lt;/ns0:DC_UZYTKOWNIK>

  		&lt;ns0:DC_ODDZIAL?>{
			data($header1/ns3:msgHeader/ns3:companyId)
		}&lt;/ns0:DC_ODDZIAL>

		&lt;ns0:NF_CTRL_OPTION>
			2
		&lt;/ns0:NF_CTRL_OPTION>
		
		&lt;ns0:NF_ACCOCT_ENDDATE?>{
			data($invoke1/ns3:accountClosingTask/ns2:AccountClosingTask/ns2:endDate)
		}&lt;/ns0:NF_ACCOCT_ENDDATE>

		&lt;ns0:NF_ACCOCT_REASON?>{
			data($invoke1/ns3:accountClosingTask/ns2:AccountClosingTask/ns2:reason)
		}&lt;/ns0:NF_ACCOCT_REASON>

		&lt;ns0:NF_ACCOCR_ACCOUNTCLOSINGRE?>{
			data($invoke1/ns3:accountClosingTask/ns2:AccountClosingTask/ns2:reasonType/ns1:AccountClosingReason/ns1:accountClosingReason)
		}&lt;/ns0:NF_ACCOCR_ACCOUNTCLOSINGRE>

		&lt;ns0:NF_ACCCRB_ACCOUNTCLOSINGRE?>{
			data($invoke1/ns3:accountClosingTask/ns2:AccountClosingTask/ns2:closingReasonBank/ns1:AccountClosingReasonBank/ns1:accountClosingReasonBank)
		}&lt;/ns0:NF_ACCCRB_ACCOUNTCLOSINGRE>

		&lt;ns0:NF_DSFTAC_DISPSTATFORTRANA?>{
			data($invoke1/ns3:accountClosingTask/ns2:AccountClosingTask/ns2:closingTaskStatus/ns1:DispStatForTranAccClosTask/ns1:dispStatForTranAccClosTask)
		}&lt;/ns0:NF_DSFTAC_DISPSTATFORTRANA>


		(: przycinami IBAN do 12 ostatnich:)
		{
		let $accountNumber := data($invoke1/ns3:account/ns2:Account/ns2:accountNumber)
			return
				if ($accountNumber ) then
		    		&lt;ns0:NF_ACCOUN_ACCOUNTNUMBER?>{
						fn:substring($accountNumber , ((fn:string-length($accountNumber))-11))
					}&lt;/ns0:NF_ACCOUN_ACCOUNTNUMBER>					
				else
					()
		}
(:~		
:&lt;ns0:NF_ACCOUN_ACCOUNTNUMBER>{
:			data($invoke1/ns3:account/ns2:Account/ns2:accountNumber)
:		}&lt;/ns0:NF_ACCOUN_ACCOUNTNUMBER>
:)
	&lt;/ns0:FML32>
};

&lt;soap-env:Body>{
	xf:chgInClosingAccountsTableRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>