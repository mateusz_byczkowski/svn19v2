<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbCEKEeOwnAccAllRequest($req as element(m:bCEKEeOwnAccAllRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:LoginId)
					then &lt;fml:E_LOGIN_ID>{ data($req/m:LoginId) }&lt;/fml:E_LOGIN_ID>
					else ()
			}
			{
				if($req/m:SysMask)
					then &lt;fml:B_SYS_MASK>{ data($req/m:SysMask) }&lt;/fml:B_SYS_MASK>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbCEKEeOwnAccAllRequest($body/m:bCEKEeOwnAccAllRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>