<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2010-02-18
 :
 : wersja WSDLa: 25-01-2010 15:50:42
 :
 : $Proxy Services/Accounts/chkTransactionToGIIF/chkTransactionToGIIFRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/chkTransactionToGIIF/chkTransactionToGIIFRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:operations.entities.be.dcl";
declare namespace ns4 = "urn:be.services.dcl";

declare variable $header1 as element(ns4:header) external;
declare variable $invoke1 as element(ns4:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)

declare function xf:chkTransactionToGIIFRequest($header1 as element(ns4:header),
												  $invoke1 as element(ns4:invoke))
    as element(ns0:FML32)
{
	&lt;ns0:FML32>

		(:
		 : dane z nagłówka
		 :)
		&lt;ns0:NF_MSHEAD_MSGID?>{
			data($header1/ns4:msgHeader/ns4:msgId)
		}&lt;/ns0:NF_MSHEAD_MSGID>
		
		(:
		 : dane wejściowe - lista identyfikatorów transakcji
		 :                  z danymi filtrującymi
		 : 
		 : W przypadku braku pola na wejściu
		 : do bufora XML/FML zostanie przekazany pusty tag,
		 : np.:
		 : &lt;NF_TRANSW_ACCOUNTNUMBER/>
		 :
		 :)
		{
			for $i in 1 to count($invoke1/ns4:transactionList/ns3:Transaction)
			return
			(
				&lt;ns0:NF_TRANSA_TRANSACTIONID>{
					data($invoke1/ns4:transactionList/ns3:Transaction[$i]/ns3:transactionID)
				}&lt;/ns0:NF_TRANSA_TRANSACTIONID>
				,
				&lt;ns0:NF_TRANSW_ACCOUNTNUMBER>{
					data($invoke1/ns4:transactionList/ns3:Transaction[$i]/ns3:transactionWn/ns3:TransactionWn/ns3:accountNumber)
				}&lt;/ns0:NF_TRANSW_ACCOUNTNUMBER>
				,
				&lt;ns0:NF_TRANSW_AMOUNTWN>{
					data($invoke1/ns4:transactionList/ns3:Transaction[$i]/ns3:transactionWn/ns3:TransactionWn/ns3:amountWn)
				}&lt;/ns0:NF_TRANSW_AMOUNTWN>
				,
				&lt;ns0:NF_TRANSW_RATE>{
					data($invoke1/ns4:transactionList/ns3:Transaction[$i]/ns3:transactionWn/ns3:TransactionWn/ns3:rate)
				}&lt;/ns0:NF_TRANSW_RATE>
				,
				&lt;ns0:NF_CURREC_CURRENCYCODE>{
					data($invoke1/ns4:transactionList/ns3:Transaction[$i]/ns3:transactionWn/ns3:TransactionWn/ns3:currencyCode/ns1:CurrencyCode/ns1:currencyCode)
				}&lt;/ns0:NF_CURREC_CURRENCYCODE>
				,
				&lt;ns0:NF_TRANSM_ACCOUNTNUMBER>{
					data($invoke1/ns4:transactionList/ns3:Transaction[$i]/ns3:transactionMa/ns3:TransactionMa/ns3:accountNumber)
				}&lt;/ns0:NF_TRANSM_ACCOUNTNUMBER>
				,
				&lt;ns0:NF_CSRMT_CSRMESSAGETYPE>{
					data($invoke1/ns4:transactionList/ns3:Transaction[$i]/ns3:csrMessageType/ns2:CsrMessageType/ns2:csrMessageType)
				}&lt;/ns0:NF_CSRMT_CSRMESSAGETYPE>
				,
				&lt;ns0:NF_ECSRMT_EXTENDEDCSRMESSA>{
					data($invoke1/ns4:transactionList/ns3:Transaction[$i]/ns3:extendedCSRMessageType/ns2:ExtendedCSRMessageType/ns2:extendedCSRMessageType)
				}&lt;/ns0:NF_ECSRMT_EXTENDEDCSRMESSA>
			)
		}
	&lt;/ns0:FML32>
};

&lt;soap-env:Body>{
	xf:chkTransactionToGIIFRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>