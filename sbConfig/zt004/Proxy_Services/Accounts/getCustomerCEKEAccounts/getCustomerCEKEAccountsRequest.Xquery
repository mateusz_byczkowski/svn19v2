<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-13</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:accounts.entities.be.dcl";
declare namespace urn4 = "urn:dictionaresrepo.dictionaries.be.dcl";

declare function xf:mapgetCustomerCEKEAccountsRequest($req as element(urn:invoke), $head as element(urn:header))
	as element(fml:FML32) {

		&lt;fml:FML32>
			&lt;fml:E_LOGIN_ID?>{ data($req/urn:customerCEKE/urn1:CustomerCEKE/urn1:nik) }&lt;/fml:E_LOGIN_ID>
			&lt;fml:E_MSHEAD_MSGID>{data($head/urn:msgHeader/urn:msgId)}&lt;/fml:E_MSHEAD_MSGID>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body>

{ xf:mapgetCustomerCEKEAccountsRequest($body/urn:invoke, $header/urn:header) }

&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>