<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:accounts.entities.be.dcl";
declare namespace e2 = "urn:accountdict.dictionaries.be.dcl";

declare function local:mapCheckLimitForAccountRequest($req as element())
    as element(fml:FML32) {
        &lt;fml:FML32>
			&lt;fml:NF_ACCOUN_ACCOUNTNUMBER>{ data($req/m:account/e:Account/e:accountNumber) }&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER>
			&lt;fml:NF_FTODAC_FLAGTODACA>{ data($req/m:flagTODACA/e2:FlagTODACA/e2:flagTODACA) }&lt;/fml:NF_FTODAC_FLAGTODACA>
        &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ local:mapCheckLimitForAccountRequest($body/m:invoke) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>