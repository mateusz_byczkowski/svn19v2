<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns3:invokeResponse>
  &lt;ns3:accountOut>
    &lt;ns1:Account>
      &lt;ns1:creditPrecentage>{data($parm/NF_ACCOUN_CREDITPRECENTAGE)}&lt;/ns1:creditPrecentage>
    &lt;/ns1:Account>
  &lt;/ns3:accountOut>
&lt;/ns3:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>