<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.2
 : @since   2010-01-08
 :
 : wersja WSDLa: 08-01-2010 13:09:21
 :
 : $Proxy Services/Accounts/getFromClosingAccountsTable/getFromClosingAccountsTableRequest.xq$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getFromClosingAccountsTable/getFromClosingAccountsTableRequest/";
declare namespace ns0 = "urn:accounts.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $header1 as element(ns2:header) external;
declare variable $invoke1 as element(ns2:invoke) external;

(:~
 : @param $invoke1 operacja wejściowa
 : @param $header1 nagłówek komunikatu
 :
 : @return FML32 bufor XML/FML
 :) 
declare function xf:getFromClosingAccountsTableRequest($header1 as element(ns2:header),
														 $invoke1 as element(ns2:invoke))
    as element(ns1:FML32)
{
	&lt;ns1:FML32>

		(:
		 : dane z nagłówka
		 :)
		&lt;ns1:NF_MSHEAD_MSGID?>{
			data($header1/ns2:msgHeader/ns2:msgId)
		}&lt;/ns1:NF_MSHEAD_MSGID>
		
		(:
		 : dane wejściowe - numer rachunku
		 :)
		&lt;ns1:NF_ACCOUN_ACCOUNTNUMBER?>{
			data($invoke1/ns2:account/ns0:Account/ns0:accountNumber)
		}&lt;/ns1:NF_ACCOUN_ACCOUNTNUMBER>
		
	&lt;/ns1:FML32>
};

&lt;soap-env:Body>{
	xf:getFromClosingAccountsTableRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>