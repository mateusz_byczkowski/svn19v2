<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:accounts.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function local:mapCheckExecutionsForAccountRequest($req as element())
	as element(fml:FML32) {
		&lt;fml:FML32>
			&lt;fml:NF_ACCOUN_ACCOUNTIBAN>{ data($req/m:account/e:Account/e:accountNumber) }&lt;/fml:NF_ACCOUN_ACCOUNTIBAN>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ local:mapCheckExecutionsForAccountRequest($body/m:invoke) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>