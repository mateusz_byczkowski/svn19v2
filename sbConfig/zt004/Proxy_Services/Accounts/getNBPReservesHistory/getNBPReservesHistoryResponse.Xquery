<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-10-21</con:description>
  <con:xquery>(: Log Zmian: 
v.1.0  2010-10-20  PKLI NP2238_1: Utworzenie
:)

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
   if ($value)
     then if(string-length($value)>5 and not ($value = "0001-01-01"))
         then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
     else() 
   else()
};

declare function xf:getElementsForProductsList($fml as element(fml:FML32)) as element()* {
  let $NF_MSRMR_DATEOFPROCESSING := $fml/fml:NF_MSRMR_DATEOFPROCESSING
  let $NF_MSRMR_OFFBALANCEPRINCIP := $fml/fml:NF_MSRMR_OFFBALANCEPRINCIP
  let $NF_MSRMR_OFFBALANCEPROVISI := $fml/fml:NF_MSRMR_OFFBALANCEPROVISI
  let $NF_MSRMR_PROVISIONFROMBALA := $fml/fml:NF_MSRMR_PROVISIONFROMBALA
  let $NF_MSRMR_PROVISIONFROMOFFB := $fml/fml:NF_MSRMR_PROVISIONFROMOFFB
  let $NF_APERI_RISKTYPE00 := $fml/fml:NF_APERI_RISKTYPE00
  let $NF_APPER_VALIDSINCE := $fml/fml:NF_APPER_VALIDSINCE
  let $NF_APERI_RISKTYPE01 := $fml/fml:NF_APERI_RISKTYPE01
  let $NF_APERI_RISKTYPE02 := $fml/fml:NF_APERI_RISKTYPE02
 
  for $it at $p in $fml/fml:NF_MSRMR_DATEOFPROCESSING return
&lt;m:NBPReserve>
     {
        if($fml/fml:NF_MSRMR_DATEOFPROCESSING[$p])
        then insertDate(data($fml/fml:NF_MSRMR_DATEOFPROCESSING[$p]),"yyyy-MM-dd","m:dateOfProcessing")
        else &lt;m:dateOfProcessing/>
      }
     {
        if($fml/fml:NF_MSRMR_OFFBALANCEPRINCIP[$p])
        then &lt;m:changeBalance>{ data($fml/fml:NF_MSRMR_OFFBALANCEPRINCIP[$p]) }&lt;/m:changeBalance>
        else  &lt;m:changeBalance/>
      }
     {
        if($fml/fml:NF_MSRMR_OFFBALANCEPROVISI[$p])
        then &lt;m:changeCommitment>{ data($fml/fml:NF_MSRMR_OFFBALANCEPROVISI[$p]) }&lt;/m:changeCommitment>
        else &lt;m:changeCommitment/>
      }
     {
        if($fml/fml:NF_MSRMR_PROVISIONFROMBALA[$p])
        then &lt;m:reserveAgainstBalance>{ data($fml/fml:NF_MSRMR_PROVISIONFROMBALA[$p]) }&lt;/m:reserveAgainstBalance>
        else &lt;m:reserveAgainstBalance/>
      }
     {
        if($fml/fml:NF_MSRMR_PROVISIONFROMOFFB[$p])
        then &lt;m:reserveAgainstCommitment>{ data($fml/fml:NF_MSRMR_PROVISIONFROMOFFB[$p]) }&lt;/m:reserveAgainstCommitment>
        else &lt;m:reserveAgainstCommitment/>
      }
     {
        if($fml/fml:NF_APERI_RISKTYPE00[$p])
        then &lt;m:statusOfPDO>{ data($fml/fml:NF_APERI_RISKTYPE00[$p]) }&lt;/m:statusOfPDO>
        else &lt;m:statusOfPDO/>
      }
     {
        if($fml/fml:NF_APPER_VALIDSINCE[$p])
        then insertDate(data($fml/fml:NF_APPER_VALIDSINCE[$p]),"yyyy-MM-dd","m:dateOfPDOStatus")
        else &lt;m:dateOfPDOStatus/>
      }
     {
        if($fml/fml:NF_APERI_RISKTYPE01[$p])
        then &lt;m:previousPDOStatus>{ data($fml/fml:NF_APERI_RISKTYPE01[$p]) }&lt;/m:previousPDOStatus>
        else &lt;m:previousPDOStatus/>
      }
     {
        if($fml/fml:NF_APERI_RISKTYPE02[$p])
        then &lt;m:riskCodeAIB>{ data($fml/fml:NF_APERI_RISKTYPE02[$p]) }&lt;/m:riskCodeAIB>
        else &lt;m:riskCodeAIB/>
      }
&lt;/m:NBPReserve>
};

declare function xf:getElementsForPageControl($fml as element(fml:FML32)) as element()* {
    &lt;m:pageControl>	
      {
        if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
        then &lt;m:navigationKeyValue>{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/m:navigationKeyValue>
        else &lt;m:navigationKeyValue/>
      }
      {
        if($fml/fml:NF_PAGEC_HASNEXT)
        then &lt;m:hasNext>{ data($fml/fml:NF_PAGEC_HASNEXT) }&lt;/m:hasNext>
        else &lt;m:hasNext/>
      }
   &lt;/m:pageControl>	
};     

declare function xf:mappgetNBPReservesHistoryResponse($fml as element(fml:FML32)) as element() {
  &lt;m:getNBPReservesHistoryResponse>
     {xf:getElementsForProductsList($fml)} 
     {xf:getElementsForPageControl($fml)} 
  &lt;/m:getNBPReservesHistoryResponse>        
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappgetNBPReservesHistoryResponse($body/fml:FML32) }

&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>