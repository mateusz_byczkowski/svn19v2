<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns3="urn:transferorder.entities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:anyType,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

     declare function insertDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };

declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32)) as element()
{

&lt;ns3:transferOrderTargetList>
  {
    for $x at $occ in $parm/NF_TRANOT_AMOUNTTARGET
    return
    &lt;ns3:TransferOrderTarget>
      &lt;ns3:amountTarget>{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}&lt;/ns3:amountTarget>
      &lt;ns3:transferOrderDescription>{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}&lt;/ns3:transferOrderDescription>
      &lt;ns3:beneficiary1>{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}&lt;/ns3:beneficiary1>
      &lt;ns3:beneficiary2>{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}&lt;/ns3:beneficiary2>
      &lt;ns3:beneficiary3>{data($parm/NF_TRANOT_BENEFICIARY3[$occ])}&lt;/ns3:beneficiary3>
      &lt;ns3:beneficiary4>{data($parm/NF_TRANOT_BENEFICIARY4[$occ])}&lt;/ns3:beneficiary4>
      &lt;ns3:account>
        &lt;ns0:Account>
          &lt;ns0:accountNumber>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns0:accountNumber>
        &lt;/ns0:Account>
      &lt;/ns3:account>
      &lt;ns3:accountOutside>
        &lt;ns3:AccountOutside>
          &lt;ns3:accountNumber>{data($parm/NF_ACCOUO_ACCOUNTNUMBER[$occ])}&lt;/ns3:accountNumber>
        &lt;/ns3:AccountOutside>
      &lt;/ns3:accountOutside>
      &lt;ns3:glaccount>
        &lt;ns0:GLAccount>
          &lt;ns0:accountGL>{data($parm/NF_GLA_ACCOUNTGL[$occ])}&lt;/ns0:accountGL>
          &lt;ns0:currencyCode>
            &lt;ns2:CurrencyCode>
              &lt;ns2:currencyCode>&lt;/ns2:currencyCode>
            &lt;/ns2:CurrencyCode>
          &lt;/ns0:currencyCode>
        &lt;/ns0:GLAccount>
      &lt;/ns3:glaccount>
    &lt;/ns3:TransferOrderTarget>
  }
&lt;/ns3:transferOrderTargetList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse>
  &lt;ns5:orderOut>
    &lt;ns3:TransferOrderAnchor>
      &lt;ns3:frequency>{data($parm/NF_TRANOA_FREQUENCY)}&lt;/ns3:frequency>
       {insertDate ($parm/NF_TRANOA_PAYMENTDATE,"yyyy-MM-dd","ns3:paymentDate")}
       {insertDate ($parm/NF_TRANOA_EXPIRATIONDATE,"yyyy-MM-dd","ns3:expirationDate")}
      &lt;ns3:feeCode>{data($parm/NF_TRANOA_FEECODE)}&lt;/ns3:feeCode>
      &lt;ns3:nonBusinessDayRule>{sourceValue2Boolean(data($parm/NF_TRANOA_NONBUSINESSDAYRU),"1")}&lt;/ns3:nonBusinessDayRule>
      &lt;ns3:charging>{sourceValue2Boolean(data($parm/NF_TRANOA_CHARGING),"1")}&lt;/ns3:charging>
      &lt;ns3:userID>{data($parm/NF_TRANOA_USERID)}&lt;/ns3:userID>
       {insertDate ($parm/NF_TRANOA_FIRSTPAYMENTDATE,"yyyy-MM-dd","ns3:firstPaymentDate")}
       &lt;ns3:feeAmount>{data($parm/NF_TRANOA_FEEAMOUNT)}&lt;/ns3:feeAmount>
      {getElementsForTransferOrderTargetList($parm)}
      &lt;ns3:period>
        &lt;ns2:Period>
          &lt;ns2:period>{data($parm/NF_PERIOD_PERIOD)}&lt;/ns2:period>
        &lt;/ns2:Period>
      &lt;/ns3:period>
      &lt;ns3:specificDay>
        &lt;ns2:SpecialDay>
          &lt;ns2:specialDay>{data($parm/NF_SPECID_SPECIALDAY)}&lt;/ns2:specialDay>
        &lt;/ns2:SpecialDay>
      &lt;/ns3:specificDay>
      &lt;ns3:currencyCode>
        &lt;ns2:CurrencyCode>
          &lt;ns2:currencyCode>{data($parm/NF_CURREC_CURRENCYCODE)}&lt;/ns2:currencyCode>
        &lt;/ns2:CurrencyCode>
      &lt;/ns3:currencyCode>
    &lt;/ns3:TransferOrderAnchor>
  &lt;/ns5:orderOut>
&lt;/ns5:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>