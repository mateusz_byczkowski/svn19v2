<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetFlagHistoryResponse($fml as element(fml:FML32))
	as element(m:CRMGetFlagHistoryResponse) {
		&lt;m:CRMGetFlagHistoryResponse>
					{
                                let $CI_SKP_PRACOWNIKA_REJ := $fml/fml:CI_SKP_PRACOWNIKA_REJ
                                let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
                                let $CI_DATA_AKTUALIZACJI := $fml/fml:CI_DATA_AKTUALIZACJI
                                let $CI_TYP := $fml/fml:CI_TYP
                                let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
                                let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
                                let $DC_IMIE := $fml/fml:DC_IMIE
                                let $DC_DRUGIE_IMIE := $fml/fml:DC_DRUGIE_IMIE
                                let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
                                let $CI_NUMER_ODDZIALU := $fml/fml:CI_NUMER_ODDZIALU

				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
                                      return
                                            &lt;m:FlagHistory>

			{
				if($CI_SKP_PRACOWNIKA_REJ[$p])
					then &lt;m:SetUserId>{ data($CI_SKP_PRACOWNIKA_REJ[$p]) }&lt;/m:SetUserId>
					else ()
			}
			{
				if($CI_SKP_PRACOWNIKA[$p])
					then &lt;m:AccUserId>{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:AccUserId>
					else ()
			}
			{
				if($CI_DATA_AKTUALIZACJI[$p])
					then &lt;m:AccDate>{ data($CI_DATA_AKTUALIZACJI[$p]) }&lt;/m:AccDate>
					else ()
			}
			{
				if($CI_TYP[$p])
					then &lt;m:FlagType>{ data($CI_TYP[$p]) }&lt;/m:FlagType>
					else ()
			}
			{
				if($DC_NUMER_KLIENTA[$p])
					then &lt;m:CustCif>{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:CustCif>
					else ()
			}
			{
				if($CI_NAZWA_PELNA[$p])
					then &lt;m:CustName>{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:CustName>
					else ()
			}
			{
				if($DC_IMIE[$p])
					then &lt;m:CustFirstName>{ data($DC_IMIE[$p]) }&lt;/m:CustFirstName>
					else ()
			}
			{
				if($DC_DRUGIE_IMIE[$p])
					then &lt;m:CustSecondName>{ data($DC_DRUGIE_IMIE[$p]) }&lt;/m:CustSecondName>
					else ()
			}
			{
				if($DC_NAZWISKO[$p])
					then &lt;m:CustSurname>{ data($DC_NAZWISKO[$p]) }&lt;/m:CustSurname>
					else ()
			}
			{
				if($CI_NUMER_ODDZIALU[$p])
					then &lt;m:IcbsNo>{ data($CI_NUMER_ODDZIALU[$p]) }&lt;/m:IcbsNo>
					else ()
			}
  &lt;/m:FlagHistory>
                        }
		&lt;/m:CRMGetFlagHistoryResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetFlagHistoryResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>