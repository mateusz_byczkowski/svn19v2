<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMRegCustProspRelRequest($req as element(m:CRMRegCustProspRelRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:EmpId)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:EmpId) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:CustId)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:CustId) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:ProspId)
					then &lt;fml:CI_NUMER_KLIENTA>{ data($req/m:ProspId) }&lt;/fml:CI_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:RelType)
					then &lt;fml:DC_TYP_RELACJI>{ data($req/m:RelType) }&lt;/fml:DC_TYP_RELACJI>
					else ()
			}
			{
				if($req/m:RelDetails)
					then &lt;fml:CI_PARAMETRY_RELACJI>{ data($req/m:RelDetails) }&lt;/fml:CI_PARAMETRY_RELACJI>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMRegCustProspRelRequest($body/m:CRMRegCustProspRelRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>