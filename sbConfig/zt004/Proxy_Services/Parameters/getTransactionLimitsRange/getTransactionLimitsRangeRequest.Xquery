<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:baseauxentities.be.dcl";
declare namespace urn2 = "urn:ceke.entities.be.dcl";
declare namespace urn3 = "urn:cekedict.dictionaries.be.dcl";

declare function xf:mapgetTransactionLimitsRangeRequest($req as element(urn:invoke), $head as element(urn:header))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				&lt;fml:E_MSHEAD_MSGID>{data($head/urn:msgHeader/urn:msgId)}&lt;/fml:E_MSHEAD_MSGID>
			}
			{
				&lt;fml:E_PROFILE_ID?>{ data($req/urn:customerCEKE/urn2:CustomerCEKE/urn2:profileCEKE/urn2:ProfileCEKE/urn2:profileCEKE) }&lt;/fml:E_PROFILE_ID>
			}
			{
				if ((data($req/urn:isToken/urn1:BooleanHolder/urn1:value)) = "true" ) then
				&lt;fml:E_TOKEN_AVAILABLE?>1&lt;/fml:E_TOKEN_AVAILABLE>
				else if ((data($req/urn:isToken/urn1:BooleanHolder/urn1:value)) = "false") then  (
				&lt;fml:E_TOKEN_AVAILABLE>0&lt;/fml:E_TOKEN_AVAILABLE>
				) else ()
			}
			{
				if ((data($req/urn:isSMSCode/urn1:BooleanHolder/urn1:value)) = "true") then
				&lt;fml:E_SMSCODE_AVAILABLE>1&lt;/fml:E_SMSCODE_AVAILABLE>
				else if ((data($req/urn:isSMSCode/urn1:BooleanHolder/urn1:value)) = "false") then (
				&lt;fml:E_SMSCODE_AVAILABLE>0&lt;/fml:E_SMSCODE_AVAILABLE>
				) else ()
			}
			{
				for $channelCEKEArray in $req/urn:customerCEKE/urn2:CustomerCEKE/urn2:customerChannelCEKEList return
				(
					&lt;fml:E_CHANNEL_TYPE>{ data($channelCEKEArray/urn2:CustomerChannelCEKE/urn2:channelID/urn3:ChannelCEKE/urn3:channelCEKE) }&lt;/fml:E_CHANNEL_TYPE>
				)
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

&lt;soap-env:Body>
{ xf:mapgetTransactionLimitsRangeRequest($body/urn:invoke, $header/urn:header) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>