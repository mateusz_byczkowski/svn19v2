<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace w = "urn:be.services.dcl";
declare namespace w1 = "urn:hlbsentities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare namespace urn = "urn:RatingWS";

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body>
{
    let $req := $body/urn:saveDecisionResponse/return
    return
    
    &lt;w:invokeResponse xmlns:urn="urn:be.services.dcl">
      &lt;w:output xmlns:urn1="urn:hlbsentities.be.dcl">
        &lt;w1:RatingDecisionSWWROutput>
          {if($req/status)
              then &lt;w1:status>{ data($req/status) }&lt;/w1:status>
              else ()
          }
          {if($req/errormessage)
              then &lt;w1:errorMessage>{ data($req/errormessage) }&lt;/w1:errorMessage>
              else ()
          }
        &lt;/w1:RatingDecisionSWWROutput>
      &lt;/w:output>
    &lt;/w:invokeResponse>
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>