<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace w="http://schemas.datacontract.org/2004/07/SzrekLibrary.Struktures";
declare namespace fml="";
declare namespace WBKFault="http://bzwbk.com/services/faults/";
declare variable $body external;


&lt;soap-env:Body>
	{
	  let $reason := $body/soap:Fault/detail/w:WbkFault
	  let $urcode := $body/soap:Fault/detail/w:WbkFault/w:ErrorCode2
          return
               
      	     if( $reason  and string-length(data($urcode))>0)
                 then
                      &lt;FML32>   
                       &lt;NF_ERROR_DESCRIPTION>11&lt;/NF_ERROR_DESCRIPTION>
                       &lt;NF_ERROR_CODE>{data($urcode)}&lt;/NF_ERROR_CODE>
                     &lt;/FML32>
                 else 
                     &lt;FML32>   
                       &lt;NF_ERROR_DESCRIPTION>12&lt;/NF_ERROR_DESCRIPTION>
                       &lt;NF_ERROR_CODE>0&lt;/NF_ERROR_CODE>
                     &lt;/FML32>
             
          
	}
  
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>