<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-10</con:description>
  <con:xquery>declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:entities.be.dcl";
declare namespace urn2="urn:baseauxentities.be.dcl";
declare namespace urn3="urn:accounts.entities.be.dcl";
declare namespace urn4="urn:dictionaries.be.dcl";


declare variable $body as element(soap:Body) external;


declare function getHoldFromInvoke($parm as element(fml:FML32),$idx as xs:integer) as element()*
{
            &lt;urn3:Hold>
               &lt;!--Optional:-->
               &lt;urn3:holdAmount>{data($parm/fml:NF_HOLD_HOLDAMOUNT[$idx ])}&lt;/urn3:holdAmount>
               &lt;!--Optional:-->
               &lt;urn3:holdEnterDate>{data($parm/fml:NF_HOLD_HOLDENTERDATE[$idx ])}&lt;/urn3:holdEnterDate>
               &lt;!--Optional:-->
               &lt;urn3:holdDescription>{data($parm/fml:NF_HOLD_HOLDDESCRIPTION[$idx ])}&lt;/urn3:holdDescription>
               &lt;!--Optional:-->
               &lt;urn3:holdExpirationDate>{data($parm/fml:NF_HOLD_HOLDEXPIRATIONDATE[$idx ])}&lt;/urn3:holdExpirationDate>
               &lt;!--Optional:-->
               &lt;urn3:holdNumber>{data($parm/fml:NF_HOLD_HOLDNUMBER[$idx ])}&lt;/urn3:holdNumber>
               &lt;!--Optional:-->
               &lt;urn3:holdDescription2>{data($parm/fml:NF_HOLD_HOLDDESCRIPTION2[$idx ])}&lt;/urn3:holdDescription2>
               &lt;!--Optional:-->
               &lt;urn3:holdType>
                  &lt;!--Optional:-->
                  &lt;urn4:HoldType>
                     &lt;!--Optional:-->
                     &lt;urn4:holdType>{data($parm/fml:NF_HOLDT_HOLDTYPE[$idx ])}&lt;/urn4:holdType>
                  &lt;/urn4:HoldType>
               &lt;/urn3:holdType>
            &lt;/urn3:Hold>
};

(:
declare function getFieldsFromInvoke($parm as element(fml:FML32),$idx as xs:integer) as element()*

{
&lt;fml:B_BLOKADA>{data($parm/fml:NF_HOLD_HOLDNUMBER[$idx ])}&lt;/fml:B_BLOKADA>
,
&lt;fml:B_KWOTA_BLOK>{data($parm/fml:NF_HOLD_HOLDAMOUNT[$idx ])}&lt;/fml:B_KWOTA_BLOK>
,
&lt;fml:B_DATA_OPER>{data($parm/fml:NF_HOLD_HOLDENTERDATE[$idx ])}&lt;/fml:B_DATA_OPER>
,
&lt;fml:B_DATA_STR>{data($parm/fml:NF_HOLD_HOLDEXPIRATIONDATE[$idx ])}&lt;/fml:B_DATA_STR>
,
&lt;fml:DC_TYP_BLOKADY>{data($parm/fml:NF_HOLDT_HOLDTYPE[$idx ])}&lt;/fml:DC_TYP_BLOKADY>
,
&lt;fml:B_POWOD1>{data($parm/fml:NF_HOLD_HOLDDESCRIPTION[$idx ])}&lt;/fml:B_POWOD1>
,
&lt;fml:B_POWOD2>{data($parm/fml:NF_HOLD_HOLDDESCRIPTION2[$idx ])}&lt;/fml:B_POWOD2>
};
:)
(:
&lt;soap:Body>
  &lt;fml:FML32>
   {for $x at $p in $body/fml:FML32/fml:NF_HOLD_HOLDNUMBER
      return
         getFieldsFromInvoke($body/fml:FML32,$p)
    }
     &lt;fml:B_LICZBA_REK>{ data($body/fml:FML32/fml:NF_PAGECC_OPERATIONS)}&lt;/fml:B_LICZBA_REK>
     &lt;fml:B_OPIS_BLEDU>{ data($body/fml:FML32/fml:NF_RESPOM_ERRORDESCRIPTION)}&lt;/fml:B_OPIS_BLEDU>
  &lt;/fml:FML32>
&lt;/soap:Body>
:)
   &lt;soapenv:Body>
      &lt;urn:invokeResponse>
         &lt;urn:bcd>
            &lt;urn1:BusinessControlData>
               &lt;!--Optional:-->
               &lt;urn1:pageControl>
                  &lt;!--Optional:-->
                  &lt;urn2:PageControl>
                     &lt;!--Optional:-->
                     &lt;urn2:pageSize>{data($body/fml:FML32/fml:NF_PAGECC_OPERATIONS)}&lt;/urn2:pageSize>
                     
                     &lt;urn2:navigationKeyValue>{data($body/fml:FML32/fml:NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/urn2:navigationKeyValue>
                     &lt;urn2:hasNext>{data($body/fml:FML32/fml:NF_PAGEC_HASNEXT)}&lt;/urn2:hasNext>
                      
                  &lt;/urn2:PageControl>
               &lt;/urn1:pageControl>
            &lt;/urn1:BusinessControlData>
         &lt;/urn:bcd>
         &lt;urn:holds>
            &lt;!--Zero or more repetitions:-->
   {for $x at $p in $body/fml:FML32/fml:NF_HOLD_HOLDNUMBER
      return
         getHoldFromInvoke($body/fml:FML32,$p)
    }
         &lt;/urn:holds>
      &lt;/urn:invokeResponse>
   &lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>