<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProductGroup_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";

declare function xf:saveProductGroup_req ($entity as element(dcl:ProductGroup)) as element(FML32) {
&lt;FML32>
     &lt;PT_CODE_PRODUCT_GROUP>{ data( $entity/dcl:codeProductGroup ) }&lt;/PT_CODE_PRODUCT_GROUP>
     &lt;PT_POLISH_NAME>{ data( $entity/dcl:polishGroupName ) }&lt;/PT_POLISH_NAME>
     &lt;PT_ENGLISH_NAME>{ data( $entity/dcl:englishGroupName ) }&lt;/PT_ENGLISH_NAME>
     &lt;PT_SORT_ORDER>{ data( $entity/dcl:sortOrder ) }&lt;/PT_SORT_ORDER>
     &lt;PT_CODE_PRODUCT_SYSTEM>{ data( $entity/dcl:codeProductSystem/ns1:System/ns1:system ) }&lt;/PT_CODE_PRODUCT_SYSTEM>
     &lt;PT_VISIBILITY_SZREK>{ xs:short( xs:boolean(data( $entity/dcl:visibilitySzrek ))) }&lt;/PT_VISIBILITY_SZREK>
     &lt;PT_VISIBILITY_CRM>{ xs:short( xs:boolean(data( $entity/dcl:visibilityCRM ))) }&lt;/PT_VISIBILITY_CRM>
     &lt;PT_VISIBILITY_DCL>{ xs:short( xs:boolean(data( $entity/dcl:visibilityDCL ))) }&lt;/PT_VISIBILITY_DCL>
     &lt;PT_RECEIVER>{ data( $entity/dcl:receiver ) }&lt;/PT_RECEIVER>
     &lt;PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP ) }&lt;/PT_USER_CHANGE_SKP>
     &lt;PT_DATE_CHANGE>{ data( $entity/dcl:dateChange ) }&lt;/PT_DATE_CHANGE>
	 &lt;PT_ID_CATEGORY>{ data( $entity/dcl:idProductCategory ) }&lt;/PT_ID_CATEGORY>

     &lt;PT_ID_GROUP>{ data( $entity/dcl:idProductGroup ) }&lt;/PT_ID_GROUP>
     
&lt;/FML32>
};
declare variable $entity as element(dcl:ProductGroup) external;
xf:saveProductGroup_req($entity)</con:xquery>
</con:xqueryEntry>