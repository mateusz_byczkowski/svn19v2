<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/deleteProdAreaAttr_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:deleteProdAreaAttr_req($entity as element(dcl:ProductAreaAttributes))
    as element() {
	&lt;FML32>
		&lt;PT_ID_AREA_ATTRIBUTES>{ data( $entity/dcl:idProductAreaAttributes ) }&lt;/PT_ID_AREA_ATTRIBUTES>
	&lt;/FML32>
};

declare variable $entity as element(dcl:ProductAreaAttributes) external;

xf:deleteProdAreaAttr_req($entity)</con:xquery>
</con:xqueryEntry>