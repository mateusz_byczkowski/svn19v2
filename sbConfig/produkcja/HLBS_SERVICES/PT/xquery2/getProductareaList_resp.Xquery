<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductareaList_resp/";
declare namespace dcl = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";

declare function xf:getProductareaList_resp($fml as element())
    as element(dcl:invokeResponse) {
        &lt;dcl:invokeResponse>
            &lt;dcl:productAreaList>
                {
                    for $i in 1 to count($fml/PT_ID_AREA)
                    return
                        &lt;ns0:ProductArea>
                                    &lt;ns0:codeProductArea>{ data($fml/PT_CODE_PRODUCT_AREA[$i]) }&lt;/ns0:codeProductArea>
                                    &lt;ns0:polishAreaName>{ data($fml/PT_POLISH_NAME[$i]) }&lt;/ns0:polishAreaName>
                                    &lt;ns0:englishAreaName>{ data($fml/PT_ENGLISH_NAME[$i]) }&lt;/ns0:englishAreaName>
                                    &lt;ns0:sortOrder>{ data($fml/PT_SORT_ORDER[$i]) }&lt;/ns0:sortOrder>
                                    &lt;ns0:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }&lt;/ns0:userChangeSKP>

     {if (data( $fml/PT_DATE_CHANGE[$i]))
     then &lt;ns0:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }&lt;/ns0:dateChange>
     else () }

                                    &lt;ns0:cerberAtributeName>{ data($fml/PT_CERBER_ATTRIBUTE_NAME[$i]) }&lt;/ns0:cerberAtributeName>
                                    &lt;ns0:idProductArea>{ data($fml/PT_ID_AREA[$i]) }&lt;/ns0:idProductArea>
                        &lt;/ns0:ProductArea>
                }
            &lt;/dcl:productAreaList>
        &lt;/dcl:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductareaList_resp($fml)</con:xquery>
</con:xqueryEntry>