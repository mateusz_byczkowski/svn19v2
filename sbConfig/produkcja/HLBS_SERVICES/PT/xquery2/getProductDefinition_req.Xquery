<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductDefinition_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:getProductDefinition_req ($entity as element(dcl:ProductDefinition)) as element(FML32) {
&lt;FML32>
     &lt;PT_ID_GROUP>{ data( $entity/dcl:idProductGroup ) }&lt;/PT_ID_GROUP>
     &lt;PT_ID_DEFINITION>{ data( $entity/dcl:idProductDefinition ) }&lt;/PT_ID_DEFINITION>
&lt;/FML32>
};

declare variable $entity as element(dcl:ProductDefinition) external;
xf:getProductDefinition_req($entity)</con:xquery>
</con:xqueryEntry>