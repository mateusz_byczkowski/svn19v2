<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProductAttributes_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:saveProductAttributes_req ($entity as element(dcl:ProductAttributes)) as element(FML32) {
&lt;FML32>
     &lt;PT_ATTRIBUTE_VALUE>{ data( $entity/dcl:attributeValue ) }&lt;/PT_ATTRIBUTE_VALUE>
     &lt;PT_ATTRIBUTE_VALUE_2>{ data( $entity/dcl:attributeValue2 ) }&lt;/PT_ATTRIBUTE_VALUE_2>
     &lt;PT_ID_PACKAGE>{ data( $entity/dcl:idProductPackage ) }&lt;/PT_ID_PACKAGE>
     &lt;PT_ID_GROUP>{ data( $entity/dcl:idProductGroup ) }&lt;/PT_ID_GROUP>
     &lt;PT_ID_DEFINITION>{ data( $entity/dcl:idProductDefinition ) }&lt;/PT_ID_DEFINITION>     
     &lt;PT_ID_PROD_AREA_ATT>{ data( $entity/dcl:idProductAreaAttributes ) }&lt;/PT_ID_PROD_AREA_ATT>     
     &lt;PT_ID_ATTRIBUTE>{ data( $entity/dcl:attributeID ) }&lt;/PT_ID_ATTRIBUTE>     
     
     &lt;PT_ID_ATTRIBUTES>{ data( $entity/dcl:idProductAttributes ) }&lt;/PT_ID_ATTRIBUTES>
     
&lt;PT_DATE_CHANGE>{ data( $entity/dcl:dateChange) }&lt;/PT_DATE_CHANGE>          
     &lt;PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP) }&lt;/PT_USER_CHANGE_SKP>
&lt;/FML32>
};
declare variable $entity as element(dcl:ProductAttributes) external;
xf:saveProductAttributes_req($entity)</con:xquery>
</con:xqueryEntry>