<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAreaAttrList_resp/";
declare namespace ns0 = "urn:productstree.entities.be.dcl";

declare function xf:getProductAreaAttrList_req($entity as element())
    as element() {
	&lt;FML32>
		&lt;PT_ID_AREA>{ data($entity/ns0:idProductArea) }&lt;/PT_ID_AREA>
    &lt;/FML32>
};

declare variable $entity as element() external;

xf:getProductAreaAttrList_req($entity)</con:xquery>
</con:xqueryEntry>