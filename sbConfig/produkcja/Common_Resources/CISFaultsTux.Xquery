<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1  2009-12-16  LK  PT58Version.$1.2010-09-09</con:description>
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="urn:errors.hlbsentities.be.dcl";
declare variable $fault external;
 
declare function local:fault($faultString as xs:string, $detail as element()) as element(soap-env:Fault) {
  &lt;soap-env:Fault>
   &lt;faultcode>soapenv:Server.userException&lt;/faultcode> 
   &lt;faultstring>{ $faultString }&lt;/faultstring> 
   &lt;detail>{ $detail }&lt;/detail>
  &lt;/soap-env:Fault>
};
 
declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string, $errorDescription as xs:string) as element()* {
 &lt;f:exceptionItem>
 &lt;f:errorCode1>{ $errorCode1 }&lt;/f:errorCode1>
 &lt;f:errorCode2>{ $errorCode2 }&lt;/f:errorCode2>
 &lt;f:errorDescription>{ $errorDescription }&lt;/f:errorDescription>
 &lt;/f:exceptionItem>
};
 
&lt;soap-env:Body>
{
  let $reason := fn:substring-after(fn:substring-before($fault/ctx:reason, "):"), "(")
  let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($fault/ctx:reason, ":"), ":"), ":")
  return
  if($reason = "6") then
    local:fault("errors.hlbsentities.be.dcl.ServiceException", element f:ServiceException { local:errors($reason, $urcode, "Brak usługi tuxedo") })
  else if($reason = "12") then
    local:fault("errors.hlbsentities.be.dcl.ServiceException", element f:ServiceException { local:errors($reason, $urcode, "Usługa tuxedo jest niedostępna") })
  else if($reason = "13") then
    local:fault("errors.hlbsentities.be.dcl.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode, "Timeout") })
  else if($reason = "11") then
    local:fault("errors.hlbsentities.be.dcl.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode, "Blad uslugi") })
  else
   local:fault("errors.hlbsentities.be.dcl.ServiceException", element f:ServiceException { local:errors($reason, $urcode, "Service Exception") })
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>