<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-24</con:description>
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns4="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForAttributesProductGroupList($parm as element(fml:FML32)) as element()
{

&lt;ns4:attributesProductGroupList>
  {
    for $x at $occ in $parm/NF_ATTRPG_CODEPRODUCT
    return
    &lt;ns4:AttributesProductGroup>
      &lt;ns4:codeProduct?>{data($parm/NF_ATTRPG_CODEPRODUCT[$occ])}&lt;/ns4:codeProduct>
      &lt;ns4:firstProductFeature?>{data($parm/NF_ATTRPG_FIRSTPRODUCTFEAT[$occ])}&lt;/ns4:firstProductFeature>
      &lt;ns4:secondProductFeature?>{data($parm/NF_ATTRPG_SECONDPRODUCTFEA[$occ])}&lt;/ns4:secondProductFeature>
      &lt;ns4:thirdProductFeature?>{data($parm/NF_ATTRPG_THIRDPRODUCTFEAT[$occ])}&lt;/ns4:thirdProductFeature>
      &lt;ns4:fourthProductFeature?>{data($parm/NF_ATTRPG_FOURTHPRODUCTFEA[$occ])}&lt;/ns4:fourthProductFeature>
      &lt;ns4:fifthProductFeature?>{data($parm/NF_ATTRPG_FIFTHPRODUCTFEAT[$occ])}&lt;/ns4:fifthProductFeature>
      &lt;ns4:idAttributesProductGroup?>{data($parm/NF_ATTRPG_IDATTRIBUTESPROD[$occ])}&lt;/ns4:idAttributesProductGroup>
    &lt;/ns4:AttributesProductGroup>
  }
&lt;/ns4:attributesProductGroupList>
};
declare function getElementsForProductAttributesList($parm as element(fml:FML32)) as element()
{

&lt;ns4:productAttributesList>
  {
    for $x at $occ in $parm/NF_PRODAT_ATTRIBUTEID
    return
    &lt;ns4:ProductAttributes>
      &lt;ns4:attributeValue?>{data($parm/NF_PRODAT_ATTRIBUTEVALUE[$occ])}&lt;/ns4:attributeValue>
      &lt;ns4:attributeValue2?>{data($parm/NF_PRODAT_ATTRIBUTEVALUE2[$occ])}&lt;/ns4:attributeValue2>
      &lt;ns4:attributeID?>{data($parm/NF_PRODAT_ATTRIBUTEID[$occ])}&lt;/ns4:attributeID>
      {
      if (data($parm/NF_PRODAT_IDPRODUCTCONFIGU[$occ]) != '-1') then
      &lt;ns4:idProductConfiguration?>{data($parm/NF_PRODAT_IDPRODUCTCONFIGU[$occ])}&lt;/ns4:idProductConfiguration>
      else ()
      }
      {
      if (data($parm/NF_PRODAT_IDPRODUCTCHANNEL[$occ]) != '-1') then
      &lt;ns4:idProductChannel?>{data($parm/NF_PRODAT_IDPRODUCTCHANNEL[$occ])}&lt;/ns4:idProductChannel>
      else ()
      }
      &lt;ns4:productAreaAtributes>
        &lt;ns4:ProductAreaAttributes>
          &lt;ns4:attributeID?>{data($parm/NF_PRODAA_ATTRIBUTEID[$occ])}&lt;/ns4:attributeID>
          &lt;ns4:attributeName?>{data($parm/NF_PRODAA_ATTRIBUTENAME[$occ])}&lt;/ns4:attributeName>
          &lt;ns4:attributeType>
            &lt;ns3:ProductAreaAttributeType>
              &lt;ns3:attributeType?>{data($parm/NF_PROAAT_ATTRIBUTETYPE[$occ])}&lt;/ns3:attributeType>
            &lt;/ns3:ProductAreaAttributeType>
          &lt;/ns4:attributeType>
        &lt;/ns4:ProductAreaAttributes>
      &lt;/ns4:productAreaAtributes>
{
if ($parm/NF_PRODCO_POLISHCONFIGURAT[$occ] != "") then
      &lt;ns4:productConfiguration>
        &lt;ns4:ProductConfiguration>
          &lt;ns4:polishConfigurationName?>{data($parm/NF_PRODCO_POLISHCONFIGURAT[$occ])}&lt;/ns4:polishConfigurationName>
        &lt;/ns4:ProductConfiguration>
      &lt;/ns4:productConfiguration>
else ()
}
{
if ($parm/NF_PRODCHA_POLISHCHANNELNA[$occ] != "") then
      &lt;ns4:productChannel>
        &lt;ns4:ProductChannel>
          &lt;ns4:polishChannelName?>{data($parm/NF_PRODCHA_POLISHCHANNELNA[$occ])}&lt;/ns4:polishChannelName>
        &lt;/ns4:ProductChannel>
      &lt;/ns4:productChannel>
else ()
}
    &lt;/ns4:ProductAttributes>
  }
&lt;/ns4:productAttributesList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:productGroupEntity>
    &lt;ns4:ProductGroup>
      &lt;ns4:polishGroupName?>{data($parm/NF_PRODUG_POLISHGROUPNAME)}&lt;/ns4:polishGroupName>
      &lt;ns4:codeProductGroup?>{data($parm/NF_PRODUG_CODEPRODUCTGROUP)}&lt;/ns4:codeProductGroup>
      &lt;ns4:englishGroupName?>{data($parm/NF_PRODUG_ENGLISHGROUPNAME)}&lt;/ns4:englishGroupName>
      &lt;ns4:sortOrder?>{data($parm/NF_PRODUG_SORTORDER)}&lt;/ns4:sortOrder>
      &lt;ns4:visibilitySzrek?>{sourceValue2Boolean(data($parm/NF_PRODUG_VISIBILITYSZREK),"1")}&lt;/ns4:visibilitySzrek>
      &lt;ns4:visibilityCRM?>{sourceValue2Boolean(data($parm/NF_PRODUG_VISIBILITYCRM),"1")}&lt;/ns4:visibilityCRM>
      &lt;ns4:receiver?>{data($parm/NF_PRODUG_RECEIVER)}&lt;/ns4:receiver>
      &lt;ns4:idProductCategory?>{data($parm/NF_PRODUG_IDPRODUCTCATEGOR)}&lt;/ns4:idProductCategory>
      &lt;ns4:userChangeSKP?>{data($parm/NF_PRODUG_USERCHANGESKP)}&lt;/ns4:userChangeSKP>
      &lt;ns4:dateChange?>{data($parm/NF_PRODUG_DATECHANGE)}&lt;/ns4:dateChange>
      &lt;ns4:visibilityDCL?>{sourceValue2Boolean(data($parm/NF_PRODUG_VISIBILITYDCL),"1")}&lt;/ns4:visibilityDCL>
      &lt;ns4:idProductGroup?>{data($parm/NF_PRODUG_IDPRODUCTGROUP)}&lt;/ns4:idProductGroup>
      {getElementsForAttributesProductGroupList($parm)}
      {getElementsForProductAttributesList($parm)}
      &lt;ns4:codeProductSystem>
        &lt;ns3:System>
          &lt;ns3:system?>{data($parm/NF_SYSTEM_SYSTEM)}&lt;/ns3:system>
          &lt;ns3:description?>{data($parm/NF_SYSTEM_DESCRIPTION)}&lt;/ns3:description>
        &lt;/ns3:System>
      &lt;/ns4:codeProductSystem>
    &lt;/ns4:ProductGroup>
  &lt;/ns0:productGroupEntity>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>