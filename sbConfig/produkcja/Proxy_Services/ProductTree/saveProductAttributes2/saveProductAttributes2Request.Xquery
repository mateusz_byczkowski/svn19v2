<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-24</con:description>
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns3="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

&lt;NF_PRODAT_ATTRIBUTEVALUE?>{data($parm/ns0:productAttributesEntity/ns3:ProductAttributes/ns3:attributeValue)}&lt;/NF_PRODAT_ATTRIBUTEVALUE>
,
&lt;NF_PRODAT_ATTRIBUTEVALUE2?>{data($parm/ns0:productAttributesEntity/ns3:ProductAttributes/ns3:attributeValue2)}&lt;/NF_PRODAT_ATTRIBUTEVALUE2>
,
&lt;NF_PRODAT_ATTRIBUTEID?>{data($parm/ns0:productAttributesEntity/ns3:ProductAttributes/ns3:attributeID)}&lt;/NF_PRODAT_ATTRIBUTEID>
,
&lt;NF_PRODAT_IDPRODUCTATTRIBU?>{data($parm/ns0:productAttributesEntity/ns3:ProductAttributes/ns3:idProductAttributes)}&lt;/NF_PRODAT_IDPRODUCTATTRIBU>
,
&lt;NF_PRODAT_IDPRODUCTAREAATT?>{data($parm/ns0:productAttributesEntity/ns3:ProductAttributes/ns3:idProductAreaAttributes)}&lt;/NF_PRODAT_IDPRODUCTAREAATT>
,
&lt;NF_PRODAT_IDPRODUCTGROUP?>{data($parm/ns0:productAttributesEntity/ns3:ProductAttributes/ns3:idProductGroup)}&lt;/NF_PRODAT_IDPRODUCTGROUP>
,
&lt;NF_PRODAT_IDPRODUCTDEFINIT?>{data($parm/ns0:productAttributesEntity/ns3:ProductAttributes/ns3:idProductDefinition)}&lt;/NF_PRODAT_IDPRODUCTDEFINIT>
,
&lt;NF_PRODAT_IDPRODUCTPACKAGE?>{data($parm/ns0:productAttributesEntity/ns3:ProductAttributes/ns3:idProductPackage)}&lt;/NF_PRODAT_IDPRODUCTPACKAGE>
,
&lt;NF_PRODAT_IDPRODUCTCONFIGU?>{data($parm/ns0:productAttributesEntity/ns3:ProductAttributes/ns3:idProductConfiguration)}&lt;/NF_PRODAT_IDPRODUCTCONFIGU>
,
&lt;NF_PRODAT_IDPRODUCTCHANNEL?>{data($parm/ns0:productAttributesEntity/ns3:ProductAttributes/ns3:idProductChannel)}&lt;/NF_PRODAT_IDPRODUCTCHANNEL>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>