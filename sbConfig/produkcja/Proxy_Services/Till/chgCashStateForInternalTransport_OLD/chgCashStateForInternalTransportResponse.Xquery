<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.4
 : @since   2009-12-17
 :
 : wesja WSDLa: 19-08-2009 10:27:11
 :
 : $Proxy Services/Till/chgCashStateForInternalTransport/chgCashStateForInternalTransportRequest.xq$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/chgCashStateForInternalTransport/chgCashStateForInternalTransportResponse_new/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:chgCashStateForInternalTransportResponse_new($fML321 as element(ns0:FML32))
    as element(ns2:invokeResponse)
{
	&lt;ns2:invokeResponse>
		&lt;ns2:response>
			&lt;ns1:ResponseMessage>
				&lt;ns1:result>
					true
				&lt;/ns1:result>
			&lt;/ns1:ResponseMessage>
		&lt;/ns2:response>
	&lt;/ns2:invokeResponse>
};

&lt;soap-env:Body>{
	xf:chgCashStateForInternalTransportResponse_new($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>