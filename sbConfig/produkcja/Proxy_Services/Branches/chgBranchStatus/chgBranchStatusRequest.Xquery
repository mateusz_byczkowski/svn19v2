<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="chgBranchStatus.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="chgBranchStatus.WSDL" ::)
(:: pragma bea:global-element-return element="ns6:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)


declare namespace ns2 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns9 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:acceptance.entities.be.dcl";
declare namespace ns6 = "";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Branches/chgBranchStatus/chgBranchStatusRequest/";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:chgBranchStatusRequest($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke))
    as element(ns6:FML32) {
        &lt;ns6:FML32>
            &lt;ns6:TR_ID_OPER>{ data($header1/ns0:transHeader/ns0:transId)  }&lt;/ns6:TR_ID_OPER>
            		{
			(:
			 : konwersja formatu daty z RRRR-MM-DD na DD-MM-RRRR
			 :)
			let $workDate := xs:string( data($invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:workDate) ) 
			return
				if ($workDate) then
					&lt;ns6:TR_DATA_OPER>{
						fn:concat(
							fn:substring($workDate, 9, 2),
							'-',
							fn:substring($workDate, 6, 2),
							'-',
							fn:substring($workDate, 1, 4)
						)
					}&lt;/ns6:TR_DATA_OPER>
				else
					()
		}
            
            &lt;ns6:TR_ODDZ_KASY?>{xs:short( data($invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:branchCode/ns3:BranchCode/ns3:branchCode) )  }&lt;/ns6:TR_ODDZ_KASY>
            &lt;ns6:TR_KASA?>{ xs:short( data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:till/ns1:Till/ns1:tillID) ) }&lt;/ns6:TR_KASA>
            &lt;ns6:TR_KASJER?>{ xs:short( data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:teller/ns1:Teller/ns1:tellerID) ) }&lt;/ns6:TR_KASJER>
            
            {
                for $userID in $invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:lastChangeUser/ns9:User/ns9:userID
                return
                    &lt;ns6:TR_UZYTKOWNIK>{ fn:concat("SKP:", data($userID)) }&lt;/ns6:TR_UZYTKOWNIK>
            }
            &lt;ns6:TR_MSG_ID>{ data($header1/ns0:msgHeader/ns0:msgId) }&lt;/ns6:TR_MSG_ID>
            
            &lt;ns6:TR_FLAGA_AKCEPT?>
                {
                if (data($invoke1/ns0:acceptanceForBE/ns7:AcceptanceForBE/ns7:flag) ) then
                (data($invoke1/ns0:acceptanceForBE/ns7:AcceptanceForBE/ns7:flag) )
                else
                'T'
                }
		    &lt;/ns6:TR_FLAGA_AKCEPT>
            
            
            {
                for $acceptor in $invoke1/ns0:acceptTask/ns5:AcceptTask/ns5:acceptor
                return
                    &lt;ns6:TR_AKCEPTANT?>{ fn:concat("SKP:", data($acceptor)) }&lt;/ns6:TR_AKCEPTANT>
            }
            &lt;ns6:TR_TYP_KOM?>{ xs:short( data($invoke1/ns0:transaction/ns7:Transaction/ns7:csrMessageType/ns4:CsrMessageType/ns4:csrMessageType)) }&lt;/ns6:TR_TYP_KOM>
            
            {
			(:
			 : konwersja formatu daty z RRRR-MM-DD na DD-MM-RRRR
			 :)
			let $workDate := xs:string( data($invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:workDate) ) 
			return
				if ($workDate) then
					&lt;ns6:TR_DATA_PRACY>{
						fn:concat(
							fn:substring($workDate, 9, 2),
							'-',
							fn:substring($workDate, 6, 2),
							'-',
							fn:substring($workDate, 1, 4)
						)
					}&lt;/ns6:TR_DATA_PRACY>
				else
					()
			}


            {
			(:
			 : konwersja formatu daty z RRRR-MM-DD na DD-MM-RRRR
			 :)
			let $workDate := xs:string( data($invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:workDate) ) 
			return
				if ($workDate) then
					&lt;ns6:TR_DATA_WALUTY>{
						fn:concat(
							fn:substring($workDate, 9, 2),
							'-',
							fn:substring($workDate, 6, 2),
							'-',
							fn:substring($workDate, 1, 4)
						)
					}&lt;/ns6:TR_DATA_WALUTY>
				else
					()
			}


            
            &lt;ns6:TR_AKCEPTANT_SKP?>{ data($invoke1/ns0:acceptTask/ns5:AcceptTask/ns5:acceptor) }&lt;/ns6:TR_AKCEPTANT_SKP>
            &lt;ns6:TR_UZYTKOWNIK_SKP?>{ data($invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:lastChangeUser/ns9:User/ns9:userID)  }&lt;/ns6:TR_UZYTKOWNIK_SKP>
        &lt;/ns6:FML32>
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;

&lt;soap-env:Body>{
	xf:chgBranchStatusRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>