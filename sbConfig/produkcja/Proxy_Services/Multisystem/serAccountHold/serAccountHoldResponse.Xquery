<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-10</con:description>
  <con:xquery>declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:accounts.entities.be.dcl";

declare variable $body as element(soap:Body) external;

&lt;soapenv:Body>
      &lt;urn:invokeResponse>
         &lt;urn:holdOut>
            &lt;!--Optional:-->
            &lt;urn1:Hold>
               &lt;!--Optional:-->
               &lt;urn1:holdNumber>{data($body/fml:FML32/fml:DC_NUMER_BLOKADY)}&lt;/urn1:holdNumber>
            &lt;/urn1:Hold>
         &lt;/urn:holdOut>
      &lt;/urn:invokeResponse>
   &lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>