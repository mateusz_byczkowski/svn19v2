<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-10-28</con:description>
  <con:xquery>declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:accountstat.accounts.entities.be.dcl";
declare namespace urn2 = "urn:filtersandmessages.entities.be.dcl";
declare namespace urn3 = "urn:accounts.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/"; 
declare namespace fml="";

declare variable $body external;
declare variable $header external;

&lt;soap-env:Body>
  {
    let $reqh := $header/urn:header
    let $req  := $body/urn:invoke
    let $monthNumber := $req/urn:filter/urn2:FilterRevenueTranAccount/urn2:monthNumber
    let $accNumberList := $req/urn:accountsList
    let $period := $req/urn:accountPeriodStat/urn1:AccountPeriodStat
    let $monthFrom := $period/urn1:monthFrom
    let $yearFrom := $period/urn1:yearFrom
    let $monthTo := $period/urn1:monthTo
    let $yearTo := $period/urn1:yearTo
    
    return
    &lt;fml:FML32>
      &lt;B_MSHEAD_MSGID?>{data($reqh/urn:msgHeader/urn:msgId)}&lt;/B_MSHEAD_MSGID>
      &lt;B_MIESIAC?>{data($monthFrom)}&lt;/B_MIESIAC>
      &lt;B_ROK?>{data($yearFrom)}&lt;/B_ROK>
      &lt;B_LICZBA_MIES?>{data($monthTo)}&lt;/B_LICZBA_MIES>
      &lt;B_AUTO_ROK_PR?>{data($yearTo)}&lt;/B_AUTO_ROK_PR>
      &lt;B_LICZBA_REK?>{data($monthNumber)}&lt;/B_LICZBA_REK>
      {for $it in $accNumberList/urn3:Account
        return
          if($it/urn3:accountNumber)
            then
              if (fn:string-length(data($it/urn3:accountNumber)) > 10)
                then
                  &lt;B_NR_IBAN>{data($it/urn3:accountNumber)}&lt;/B_NR_IBAN>
                else
                  &lt;B_KOD_RACH_S>{data($it/urn3:accountNumber)}&lt;/B_KOD_RACH_S>
            else ()
      }
   &lt;/fml:FML32>
  }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>