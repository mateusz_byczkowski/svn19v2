<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns0="urn:basedictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:entities.be.dcl";
declare namespace ns8="urn:accountdict.dictionaries.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function insertDateTime($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function sourceValue2Boolean ($parm as xs:string?,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};



declare function getElementsForPledgeCollateral($parm as element(fml:FML32)) as element()
{

&lt;ns1:pledgeCollateral>
  {
    for $x at $occ in $parm/NF_COLLAT_COLLATERALVALUE
    return
    &lt;ns4:PledgeCollateralToAccount>
      &lt;ns4:maximumPledgeAmount?>{data($parm/NF_PLECTA_MAXIMUMPLEDGEAMO[$occ])}&lt;/ns4:maximumPledgeAmount>
      &lt;ns4:pledgingPriority?>
         {
          if (number(data($parm/NF_PLECTA_PLEDGINGPRIORITY[$occ])) > 0)
              then data($parm/NF_PLECTA_PLEDGINGPRIORITY[$occ])
              else ()
          }
      &lt;/ns4:pledgingPriority>
      &lt;ns4:collateral>
        &lt;ns4:Collateral>
          &lt;ns4:locationCode?>{data($parm/NF_COLLAT_LOCATIONCODE[$occ])}&lt;/ns4:locationCode>
          &lt;ns4:collateralDescription?>{data($parm/NF_COLLAT_COLLATERALDESCRI[$occ])}&lt;/ns4:collateralDescription>
          &lt;ns4:collateralValue?>{data($parm/NF_COLLAT_COLLATERALVALUE[$occ])}&lt;/ns4:collateralValue>
          { insertDate(data($parm/NF_COLLAT_EXPIRATIONORMATU[$occ]),"yyyy-MM-dd","ns4:expirationOrMaturityDate")}
          &lt;ns4:insuranceRequired?>{data($parm/NF_COLLAT_INSURANCEREQUIRE[$occ])}&lt;/ns4:insuranceRequired>
          { insertDate(data($parm/NF_COLLAT_INSURANCEEXPIRYD[$occ]),"yyyy-MM-dd","ns4:insuranceExpiryDate")}
          { insertDate(data($parm/NF_COLLAT_LEGALCLAIMREGIST[$occ]),"yyyy-MM-dd","ns4:legalClaimRegisteredDate")}
          { insertDate(data($parm/NF_COLLAT_LEGALCLAIMEXPIRY[$occ]),"yyyy-MM-dd","ns4:legalClaimExpiryDate")}
          &lt;ns4:reviewFrequency?>{data($parm/NF_COLLAT_REVIEWFREQUENCY[$occ])}&lt;/ns4:reviewFrequency>
          &lt;ns4:reviewPeriod?>{data($parm/NF_COLLAT_REVIEWPERIOD[$occ])}&lt;/ns4:reviewPeriod>
          { insertDateTime(data($parm/NF_COLLAT_FIRSTREVIEWDATE[$occ]),"yyyy-MM-dd","ns4:firstReviewDate")}
          &lt;ns4:reviewComments?>{data($parm/NF_COLLAT_REVIEWCOMMENTS[$occ])}&lt;/ns4:reviewComments>
          {
           if (string-length(data($parm/NF_COLLAT_NUMBEROFUNITS[$occ]))>0) then
            &lt;ns4:numberOfUnits?>{data($parm/NF_COLLAT_NUMBEROFUNITS[$occ])}&lt;/ns4:numberOfUnits>
           else
            ()
          }
          &lt;ns4:unitPrice?>{data($parm/NF_COLLAT_UNITPRICE[$occ])}&lt;/ns4:unitPrice>
          &lt;ns4:marginPercentage?>{data($parm/NF_COLLAT_MARGINPERCENTAGE[$occ])}&lt;/ns4:marginPercentage>
          &lt;ns4:maximumCollateralValue?>{data($parm/NF_COLLAT_MAXIMUMCOLLATERA[$occ])}&lt;/ns4:maximumCollateralValue>
          { insertDate(data($parm/NF_COLLAT_DATELASTPRICED[$occ]),"yyyy-MM-dd","ns4:dateLastPriced")}
          &lt;ns4:collateralItemNumber?>{data($parm/NF_COLLAT_COLLATERALITEMNU[$occ])}&lt;/ns4:collateralItemNumber>
          &lt;ns4:commodityIdCusip?>{data($parm/NF_COLLAT_COMMODITYIDCUSIP[$occ])}&lt;/ns4:commodityIdCusip>
          &lt;ns4:itemReferenceNumber?>{data($parm/NF_COLLAT_ITEMREFERENCENUM[$occ])}&lt;/ns4:itemReferenceNumber>
          &lt;ns4:safekeepingReceiptNbr?>{data($parm/NF_COLLAT_SAFEKEEPINGRECEI[$occ])}&lt;/ns4:safekeepingReceiptNbr>
          &lt;ns4:shortDescription?>{data($parm/NF_COLLAT_SHORTDESCRIPTION[$occ])}&lt;/ns4:shortDescription>
          { insertDate(data($parm/NF_COLLAT_STARTDATEOFSECLI[$occ]),"yyyy-MM-dd","ns4:startDateOfSecLiquidat")}
          &lt;ns4:costOfSalesOfSecurity?>{data($parm/NF_COLLAT_COSTOFSALESOFSEC[$occ])}&lt;/ns4:costOfSalesOfSecurity>
          { insertDate(data($parm/NF_COLLAT_ENDDATEOFSECLIQU[$occ]),"yyyy-MM-dd","ns4:endDateOfSecLiquidat")}
          &lt;ns4:ccy?>{data($parm/NF_COLLAT_CCY[$occ])}&lt;/ns4:ccy>
          &lt;ns4:amt?>{data($parm/NF_COLLAT_AMT[$occ])}&lt;/ns4:amt>
          &lt;ns4:amountCurrentlyPledged?>{data($parm/NF_COLLAT_AMOUNTCURRENTLYP[$occ])}&lt;/ns4:amountCurrentlyPledged>
          &lt;ns4:availableToPledge?>{data($parm/NF_COLLAT_AVAILABLETOPLEDG[$occ])}&lt;/ns4:availableToPledge>
          &lt;ns4:originalCollateralValue?>{data($parm/NF_COLLAT_ORIGINALCOLLATER[$occ])}&lt;/ns4:originalCollateralValue>
          { insertDate(data($parm/NF_COLLAT_COLLATERALINACTI[$occ]),"yyyy-MM-dd","ns4:collateralInactiveDate")}
          &lt;ns4:userField6?>{data($parm/NF_COLLAT_USERFIELD6[$occ])}&lt;/ns4:userField6>
          { insertDate(data($parm/NF_COLLAT_LASTMAINTENANCE[$occ]),"yyyy-MM-dd","ns4:lastMaintenance")}
          &lt;ns4:maximumPledgeAmount?>{data($parm/NF_COLLAT_MAXIMUMPLEDGEAMO[$occ])}&lt;/ns4:maximumPledgeAmount>
          &lt;ns4:amountPledgedCCYOfCollateral?>{data($parm/NF_COLLAT_AMOUNTPLECCYOFCO[$occ])}&lt;/ns4:amountPledgedCCYOfCollateral>
          &lt;ns4:amountpledgedCCYOfAccount?>{data($parm/NF_COLLAT_AMOUNTPLEDGEDCCY[$occ])}&lt;/ns4:amountpledgedCCYOfAccount>
          &lt;ns4:pledgePurgeDays?>{data($parm/NF_COLLAT_PLEDGEPURGEDAYS[$occ])}&lt;/ns4:pledgePurgeDays>
          { insertDate(data($parm/NF_COLLAT_PLEDGEPURGEDATE[$occ]),"yyyy-MM-dd","ns4:pledgePurgeDate")}
          &lt;ns4:collateralMortgage>
            &lt;ns4:CollateralMortgage>
              &lt;ns4:shortLegalDescription?>{data($parm/NF_COLLAM_SHORTLEGALDESCRI[$occ])}&lt;/ns4:shortLegalDescription>
              &lt;ns4:legalRegistrationNumber?>{data($parm/NF_COLLAM_LEGALREGNUM[$occ])}&lt;/ns4:legalRegistrationNumber>
              &lt;ns4:yearBuilt?>{data($parm/NF_COLLAM_YEARBUILT[$occ])}&lt;/ns4:yearBuilt>
              &lt;ns4:yearPurchased?>{data($parm/NF_COLLAM_YEARPURCHASED[$occ])}&lt;/ns4:yearPurchased>
              &lt;ns4:purchasePrice?>{data($parm/NF_COLLAM_PURCHASEPRICE[$occ])}&lt;/ns4:purchasePrice>
              &lt;ns4:lotSize?>{data($parm/NF_COLLAM_LOTSIZE[$occ])}&lt;/ns4:lotSize>
              &lt;ns4:squareFeet?>{data($parm/NF_COLLAM_SQUAREFEET[$occ])}&lt;/ns4:squareFeet>
              &lt;ns4:numberOfResidents?>{data($parm/NF_COLLAM_NUMBEROFRESIDENT[$occ])}&lt;/ns4:numberOfResidents>
              { insertDate(data($parm/NF_COLLAM_LEASEEXPIRATIOND[$occ]),"yyyy-MM-dd","ns4:leaseExpirationDate")}
              { insertDate(data($parm/NF_COLLAM_DATEINSPECTED[$occ]),"yyyy-MM-dd","ns4:dateInspected")}
              &lt;ns4:subdivision?>{data($parm/NF_COLLAM_SUBDIVISION[$occ])}&lt;/ns4:subdivision>
              &lt;ns4:appraisedValue?>{data($parm/NF_COLLAM_APPRAISEDVALUE[$occ])}&lt;/ns4:appraisedValue>
              { insertDate(data($parm/NF_COLLAM_APPRAISALDATE[$occ]),"yyyy-MM-dd","ns4:appraisalDate")}
              &lt;ns4:appraiserName?>{data($parm/NF_COLLAM_APPRAISERNAME[$occ])}&lt;/ns4:appraiserName>
              &lt;ns4:mortgagesPayablePrior?>{data($parm/NF_COLLAM_MORTGAGESPAYABLE[$occ])}&lt;/ns4:mortgagesPayablePrior>
              &lt;ns4:userField2?>{data($parm/NF_COLLAM_USERFIELD2[$occ])}&lt;/ns4:userField2>
              &lt;ns4:userField3?>{data($parm/NF_COLLAM_USERFIELD3[$occ])}&lt;/ns4:userField3>
              &lt;ns4:userField4?>{data($parm/NF_COLLAM_USERFIELD4[$occ])}&lt;/ns4:userField4>
              { insertDate(data($parm/NF_COLLAM_USERFIELD5[$occ]),"yyyy-MM-dd","ns4:userField5")}
              &lt;ns4:newAdditionalUserField2?>{data($parm/NF_COLLAM_NEWADDUSEFIE2[$occ])}&lt;/ns4:newAdditionalUserField2>
              (:  &lt;ns4:newAdditionalUserField1>{data($parm/NF_COLLAM_NEWADDITIONALUSE[$occ])}&lt;/ns4:newAdditionalUserField1>   :)
              (:  &lt;ns4:newAdditionalUserField1?>0001-01-01&lt;/ns4:newAdditionalUserField1>  :)
              &lt;ns4:newAdditionalUserField3?>{data($parm/NF_COLLAM_NEWADDUSEFIE3[$occ])}&lt;/ns4:newAdditionalUserField3>
              { insertDate(data($parm/NF_COLLAM_LEGALREGISTRATIO[$occ]),"yyyy-MM-dd","ns4:legalRegistrationDate")}
              &lt;ns4:houseNumber?>{data($parm/NF_COLLAM_HOUSENUMBER[$occ])}&lt;/ns4:houseNumber>
              &lt;ns4:street?>{data($parm/NF_COLLAM_STREET[$occ])}&lt;/ns4:street>
              &lt;ns4:city?>{data($parm/NF_COLLAM_CITY[$occ])}&lt;/ns4:city>
              &lt;ns4:postalCode?>{data($parm/NF_COLLAM_POSTALCODE[$occ])}&lt;/ns4:postalCode>
              &lt;ns4:country?>{data($parm/NF_COLLAM_COUNTRY[$occ])}&lt;/ns4:country>
              &lt;ns4:district?>{data($parm/NF_COLLAM_DISTRICT[$occ])}&lt;/ns4:district>
              &lt;ns4:county?>{data($parm/NF_COLLAM_COUNTY[$occ])}&lt;/ns4:county>
              &lt;ns4:userField1?>{data($parm/NF_COLLAM_USERFIELD1[$occ])}&lt;/ns4:userField1>
              &lt;ns4:newUsedIndicatori>
                &lt;ns5:NewUsedType>
                  &lt;ns5:newUsedType?>{data($parm/NF_NEWUT_NEWUSEDTYPE[$occ])}&lt;/ns5:newUsedType>
                &lt;/ns5:NewUsedType>
              &lt;/ns4:newUsedIndicatori>
              &lt;ns4:propertyType>
                &lt;ns5:PropertyType>
                  &lt;ns5:propertyType?>{data($parm/NF_PROPET_PROPERTYTYPE[$occ])}&lt;/ns5:propertyType>
                &lt;/ns5:PropertyType>
              &lt;/ns4:propertyType>
              &lt;ns4:ownerOccupiedCode>
                &lt;ns5:OwnerOccupiedCode>
                  &lt;ns5:ownerOccupiedCode?>{data($parm/NF_OWNEOC_OWNEROCCUPIEDCOD[$occ])}&lt;/ns5:ownerOccupiedCode>
                &lt;/ns5:OwnerOccupiedCode>
              &lt;/ns4:ownerOccupiedCode>
              &lt;ns4:tenure>
                &lt;ns5:Tenure>
                  &lt;ns5:tenure?>{data($parm/NF_TENURE_TENURE[$occ])}&lt;/ns5:tenure>
                &lt;/ns5:Tenure>
              &lt;/ns4:tenure>
              
              &lt;ns4:newAdditionalUserField1>
                  &lt;ns8:CollateralMortgageAddType>
                      &lt;ns8:collateralMortgageAddType>
                          {data($parm/NF_COLMAT_COLLATERALMORTGA[$occ])}
                      &lt;/ns8:collateralMortgageAddType>
                  &lt;/ns8:CollateralMortgageAddType>
              &lt;/ns4:newAdditionalUserField1>

              &lt;ns4:realEstateUserField1>
                &lt;ns5:CollateralMortgagePosition>
                  &lt;ns5:collateralMortgagePosition?>{data($parm/NF_COLLMP_COLLATERALMORTGA[$occ])}&lt;/ns5:collateralMortgagePosition>
                &lt;/ns5:CollateralMortgagePosition>
              &lt;/ns4:realEstateUserField1>
            &lt;/ns4:CollateralMortgage>
          &lt;/ns4:collateralMortgage>
          &lt;ns4:collateralPledge>
            &lt;ns4:CollateralCar>
              &lt;ns4:modelYear?>{data($parm/NF_COLLAC_MODELYEAR[$occ])}&lt;/ns4:modelYear>
              &lt;ns4:rentalDemoFlag?>
                  {
                    if ('0' =data($parm/NF_COLLAC_RENTALDEMOFLAG[$occ]))
                    then '1' else '0'
                   }
              &lt;/ns4:rentalDemoFlag>
              { insertDate(data($parm/NF_COLLAC_VEHICLELICENSEEX[$occ]),"yyyy-MM-dd","ns4:vehicleLicenseExpiryDate")}
              { insertDate(data($parm/NF_COLLAC_FIRSTREGISTRATIO[$occ]),"yyyy-MM-dd","ns4:firstRegistrationDate")}
              &lt;ns4:serialNumber?>{data($parm/NF_COLLAC_SERIALNUMBER[$occ])}&lt;/ns4:serialNumber>
              &lt;ns4:motorNumber?>{data($parm/NF_COLLAC_MOTORNUMBER[$occ])}&lt;/ns4:motorNumber>
              &lt;ns4:plateNumber?>{data($parm/NF_COLLAC_PLATENUMBER[$occ])}&lt;/ns4:plateNumber>
              &lt;ns4:conditionFlag>
                &lt;ns5:ConditionFlag>
                  &lt;ns5:conditionFlag?>{data($parm/NF_CONDIF_CONDITIONFLAG[$occ])}&lt;/ns5:conditionFlag>
                &lt;/ns5:ConditionFlag>
              &lt;/ns4:conditionFlag>
              &lt;ns4:licenseTypeCode>
                &lt;ns5:LicenseTypeCode>
                {
                   if (string-length(data($parm/NF_LICETC_LICENSETYPECODE[$occ]))>0) then
                     &lt;ns5:licenseTypeCode?>{data($parm/NF_LICETC_LICENSETYPECODE[$occ])}&lt;/ns5:licenseTypeCode>
                  else 
                      ()   
                  }
                &lt;/ns5:LicenseTypeCode>
              &lt;/ns4:licenseTypeCode>
              &lt;ns4:financeTypeCode>
                &lt;ns5:FinanceTypeCode>
                 {
                   if (string-length(data($parm/NF_FINATC_FINANCETYPECODE[$occ]))>0) then
                     &lt;ns5:financeTypeCode?>{data($parm/NF_FINATC_FINANCETYPECODE[$occ])}&lt;/ns5:financeTypeCode>
                  else 
                      () 
                }
                &lt;/ns5:FinanceTypeCode>
              &lt;/ns4:financeTypeCode>
              &lt;ns4:registrationState>
                &lt;ns5:RegistrationState>
                  &lt;ns5:registrationState?>{data($parm/NF_BASES_STATE[$occ])}&lt;/ns5:registrationState>
                &lt;/ns5:RegistrationState>
              &lt;/ns4:registrationState>
              &lt;ns4:makeCode>
                &lt;ns5:MakeCode>
                 {
                   if (string-length(data($parm/NF_MAKEC_MAKECODE[$occ]))>0) then
                     &lt;ns5:makeCode?>{data($parm/NF_MAKEC_MAKECODE[$occ])}&lt;/ns5:makeCode>
                  else 
                      () 
                  }
                &lt;/ns5:MakeCode>
              &lt;/ns4:makeCode>
              &lt;ns4:bodyCode>
                &lt;ns5:BodyCode>
                  &lt;ns5:bodyCode?>{data($parm/NF_BODYC_BODYCODE[$occ])}&lt;/ns5:bodyCode>
                &lt;/ns5:BodyCode>
              &lt;/ns4:bodyCode>
            &lt;/ns4:CollateralCar>
          &lt;/ns4:collateralPledge>
          &lt;ns4:collateralCode>
            &lt;ns5:CollateralCode>
              &lt;ns5:collateralCode?>{data($parm/NF_COLLCO_COLLATERALCODE[$occ])}&lt;/ns5:collateralCode>
              &lt;ns5:pledgingRule?>{data($parm/NF_COLLCO_PLEDGINGRULE[$occ])}&lt;/ns5:pledgingRule>
            &lt;/ns5:CollateralCode>
          &lt;/ns4:collateralCode>
          &lt;ns4:currencyCode>
            &lt;ns5:CurrencyCode>
              &lt;ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode>
            &lt;/ns5:CurrencyCode>
          &lt;/ns4:currencyCode>
          &lt;ns4:methodOfSecurityValuation>
            &lt;ns5:MethodOfSecurityValuation>
              &lt;ns5:methodOfSecurityValuation?>{data($parm/NF_METOSV_METHODOFSECURITY[$occ])}&lt;/ns5:methodOfSecurityValuation>
            &lt;/ns5:MethodOfSecurityValuation>
          &lt;/ns4:methodOfSecurityValuation>
          &lt;ns4:personValuatingASecurity>
            &lt;ns5:PersonValuatingASecurity>
              &lt;ns5:personValuatingASecurity?>{data($parm/NF_PERVAS_PERSONVALUATINGA[$occ])}&lt;/ns5:personValuatingASecurity>
            &lt;/ns5:PersonValuatingASecurity>
          &lt;/ns4:personValuatingASecurity>
          &lt;ns4:itemApplicationNumber>
            &lt;ns5:ApplicationNumber>
              &lt;ns5:applicationNumber?>{data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ])}&lt;/ns5:applicationNumber>
            &lt;/ns5:ApplicationNumber>
          &lt;/ns4:itemApplicationNumber>
          &lt;ns4:entity>
            &lt;ns5:CollateralEntity>
             {
              if (string-length(data($parm/NF_COLLAE_COLLATERALENTITY[$occ]))>0) then
                     &lt;ns5:collateralEntity?>{data($parm/NF_COLLAE_COLLATERALENTITY[$occ])}&lt;/ns5:collateralEntity>
                  else 
                      ()   
               }         
            &lt;/ns5:CollateralEntity>
          &lt;/ns4:entity>
        &lt;/ns4:Collateral>
      &lt;/ns4:collateral>
      &lt;ns4:crossCurrencyRateCode>
        &lt;ns5:CurrencyRateCode>
         (:   &lt;ns5:currencyRateCode>{data($parm/NF_CURRRC_CURRENCYRATECODE[$occ])}&lt;/ns5:currencyRateCode>    :)
		  &lt;ns5:currencyRateCode?>{data($parm/NF_COLLCO_CROSSCURRENCYRAT[$occ])}&lt;/ns5:currencyRateCode>
		  &lt;/ns5:CurrencyRateCode>
      &lt;/ns4:crossCurrencyRateCode>
    &lt;/ns4:PledgeCollateralToAccount>
  }
&lt;/ns1:pledgeCollateral>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns1:invokeResponse>
  {getElementsForPledgeCollateral($parm)}
  &lt;ns1:bcd>
    &lt;ns7:BusinessControlData>
      &lt;ns7:pageControl>
        &lt;ns6:PageControl>
          &lt;ns6:hasNext?>{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns6:hasNext>
          &lt;ns6:navigationKeyDefinition>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition>
          &lt;ns6:navigationKeyValue>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue>
        &lt;/ns6:PageControl>
      &lt;/ns7:pageControl>
    &lt;/ns7:BusinessControlData>
  &lt;/ns1:bcd>
&lt;/ns1:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>