<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-11-18</con:description>
  <con:xquery>(: Log Zmian: 
==================================
v.1.0  2010-11-12 PKLI NP2173_1 Utworzenie

:)
declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:getElementsForResponseList($fml as element(fml:FML32)) as element()* {
 let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
  let $DC_NR_PODSYSTEMU := $fml/fml:DC_NR_PODSYSTEMU
  let $DC_KOD_WYKLUCZENIA := $fml/fml:DC_KOD_WYKLUCZENIA
  let $DC_TYLKO_STATUS := $fml/fml:DC_TYLKO_STATUS
  let $DC_OPIS_BLEDU := $fml/fml:DC_OPIS_BLEDU
 	                        
  for $it at $p in $fml/fml:DC_NUMER_KLIENTA return
    &lt;m:DelCustProdsAddressesResponse>
    {
      if($DC_NUMER_KLIENTA[$p])
      then &lt;m:numerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:numerKlienta>
      else ()
    } 
    {
      if($DC_NR_PODSYSTEMU[$p])
      then &lt;m:idSystemu>{ data($DC_NR_PODSYSTEMU[$p]) }&lt;/m:idSystemu>
      else ()
    } 
    {
      if($DC_KOD_WYKLUCZENIA[$p])
      then &lt;m:statusPrzetwarzania>{ data($DC_KOD_WYKLUCZENIA[$p]) }&lt;/m:statusPrzetwarzania>
      else ()
    } 
    {
      if($DC_TYLKO_STATUS[$p])
      then &lt;m:kodBledu>{ data($DC_TYLKO_STATUS[$p]) }&lt;/m:kodBledu>
      else ()
    } 
    {
      if($DC_OPIS_BLEDU[$p])
      then &lt;m:opisBledu>{ data($DC_OPIS_BLEDU[$p]) }&lt;/m:opisBledu>
      else ()
    }
    &lt;/m:DelCustProdsAddressesResponse>
};

declare function xf:mapCRMDelCustProdsAddressesResponse($fml as element(fml:FML32)) as element() {
  &lt;m:CRMDelCustProdsAddressesResponse>
    {xf:getElementsForResponseList($fml)}  
  &lt;/m:CRMDelCustProdsAddressesResponse>
 };

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
  { xf:mapCRMDelCustProdsAddressesResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>