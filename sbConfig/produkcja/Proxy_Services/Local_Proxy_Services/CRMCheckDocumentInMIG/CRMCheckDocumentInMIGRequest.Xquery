<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMCheckDocumentInMIGRequest($req as element(m:CRMCheckDocumentInMIGRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:DokTozsamosci)
					then &lt;fml:CI_DOK_TOZSAMOSCI>{ data($req/m:DokTozsamosci) }&lt;/fml:CI_DOK_TOZSAMOSCI>
					else ()
			}
			{
				if($req/m:SeriaNrDok)
					then &lt;fml:CI_SERIA_NR_DOK>{ data($req/m:SeriaNrDok) }&lt;/fml:CI_SERIA_NR_DOK>
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK>{ data($req/m:Uzytkownik) }&lt;/fml:DC_UZYTKOWNIK>
					else ()
			}
			{
				if($req/m:KodJednostki)
					then &lt;fml:DC_KOD_JEDNOSTKI>{ data($req/m:KodJednostki) }&lt;/fml:DC_KOD_JEDNOSTKI>
					else ()
			}
                        &lt;fml:DC_KOD_APLIKACJI>90&lt;/fml:DC_KOD_APLIKACJI>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMCheckDocumentInMIGRequest($body/m:CRMCheckDocumentInMIGRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>