<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMCheckDocumentInMIGResponse($fml as element(fml:FML32))
	as element(m:CRMCheckDocumentInMIGResponse) {
		&lt;m:CRMCheckDocumentInMIGResponse>
			{
				if($fml/fml:CI_STATUS and data($fml/fml:CI_STATUS) = "W")
					then &lt;m:Status>true&lt;/m:Status>
					else &lt;m:Status>false&lt;/m:Status>
			}
		&lt;/m:CRMCheckDocumentInMIGResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMCheckDocumentInMIGResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>