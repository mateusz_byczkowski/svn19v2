<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace op ="http://www.w3.org/2005/xpath-functions";

declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};

declare function chkUnitId($unitId as xs:string) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else()
};

declare function xf:mapSaveOvernightAccountRequest($req as element(ns1:Account), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

	let $unitId := $msghead/dcl:unitId
        let $transId:=$tranhead/dcl:transId
       
 let $timestamp:= $msghead/dcl:timestamp


	let $customerNumber:= $req/ns1:customerNumber
    let $skpOpener:=$req/ns1:skpOpener
    let $productCode:=$req/ns1:productCode
    let $officerCode:=$req/ns1:officerCode

   

        
    (: encja TranAccount :)
    let $serviceChargeCode:=$req/ns1:tranAccount/ns1:TranAccount/ns1:serviceChargeCode
    let $interestPlanNumber:=$req/ns1:tranAccount/ns1:TranAccount/ns1:interestPlanNumber
    let $interestTransferAccount:=$req/ns1:tranAccount/ns1:TranAccount/ns1:interestTransferAccount

	(: Encja accountBranchNumber :)
    let $accountBranchNumber:=$req/ns1:accountBranchNumber/ns2:BranchCode/ns2:branchCode

    (: długość pola  z numerem rachunku do transferu odsetek:)
     let $interestTransferAccountLength:=string-length(fn-bea:trim(data($interestTransferAccount)))
	return
          &lt;fml:FML32>
            &lt;DC_TRN_ID?>{data($transId)}&lt;/DC_TRN_ID>
            &lt;DC_UZYTKOWNIK?>{concat("SKP:",data($skpOpener))}&lt;/DC_UZYTKOWNIK>
            &lt;DC_ODDZIAL?>{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL>
            &lt;DC_NUMER_KLIENTA?>{data($customerNumber)}&lt;/DC_NUMER_KLIENTA>
            &lt;DC_RELACJA>SOW&lt;/DC_RELACJA>
            &lt;DC_TYP_RELACJI>1&lt;/DC_TYP_RELACJI>
            &lt;DC_PROCENT_ZOB_PODATKOWYCH>100.0000&lt;/DC_PROCENT_ZOB_PODATKOWYCH>
            &lt;DC_NUMER_PRODUKTU?>{data($productCode)}&lt;/DC_NUMER_PRODUKTU>
            &lt;DC_NUMER_ODDZIALU?>{data($accountBranchNumber)}&lt;/DC_NUMER_ODDZIALU>
            &lt;DC_RODZAJ_RACHUNKU>DM&lt;/DC_RODZAJ_RACHUNKU>            
            &lt;DC_PLAN_OPLAT?>{data($serviceChargeCode)}&lt;/DC_PLAN_OPLAT>
            &lt;DC_PLAN_ODSETKOWY?>{data($interestPlanNumber)}&lt;/DC_PLAN_ODSETKOWY>
            {
               if ( string-length($officerCode)>0 )
                 then &lt;DC_KOD_PRACOWNIKA>{data($officerCode)}&lt;/DC_KOD_PRACOWNIKA>
                 else   &lt;DC_KOD_PRACOWNIKA>DCL&lt;/DC_KOD_PRACOWNIKA>                 
             } 
	        { 
	          if ($interestTransferAccount and data($interestTransferAccount) != "0")
                 then 
                 	  (&lt;DC_KOD_DYSPOZYCJI_ODSETKI>T&lt;/DC_KOD_DYSPOZYCJI_ODSETKI>,
                                    
                                      if ($interestTransferAccountLength>12)
                                           then &lt;DC_NR_RACHUNKU_W_SYSTEMIE>{substring(fn-bea:trim(data($interestTransferAccount)),$interestTransferAccountLength - 11,12)}&lt;/DC_NR_RACHUNKU_W_SYSTEMIE>
                                           else &lt;DC_NR_RACHUNKU_W_SYSTEMIE>{data($interestTransferAccount)}&lt;/DC_NR_RACHUNKU_W_SYSTEMIE>
                                     ,
		   &lt;DC_RODZAJ_RACH_PRZEL>DM&lt;/DC_RODZAJ_RACH_PRZEL>)
                 else ()	             
	        }
&lt;DC_KOD_CYKLU_WYCIAGOW>M&lt;/DC_KOD_CYKLU_WYCIAGOW> 
&lt;DC_LICZBA_OKRESOW_CYKL_WYCIAG>1&lt;/DC_LICZBA_OKRESOW_CYKL_WYCIAG>
&lt;DC_OKRESLONY_DZIEN_WYCIAGU>{substring(string($timestamp),9,2)}&lt;/DC_OKRESLONY_DZIEN_WYCIAGU>
&lt;DC_DATA_NASTEPNEGO_WYCIAGU>{xf:DateTime2CYMD(string(($timestamp  cast as xs:dateTime) + xdt:yearMonthDuration("P0Y1M")))}&lt;/DC_DATA_NASTEPNEGO_WYCIAGU>&lt;DC_KOD_SPECJALNYCH_INSTRUKCJI>RBW&lt;/DC_KOD_SPECJALNYCH_INSTRUKCJI>
          &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


&lt;soap-env:Body>
{ xf:mapSaveOvernightAccountRequest($body/dcl:invoke/dcl:account/ns1:Account, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>