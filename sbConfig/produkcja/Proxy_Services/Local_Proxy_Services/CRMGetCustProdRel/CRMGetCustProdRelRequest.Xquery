<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustProdRelRequest($req as element(m:CRMGetCustProdRelRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
                                       (:         {
				if($req/m:IdSpolki)
					then &lt;fml:NF_MSHEAD_COMPANYID>{ data($req/m:IdSpolki) }&lt;/fml:NF_MSHEAD_COMPANYID>
					else ()			
                                                } :)

			{
				if($req/m:CustomCustomernumber)
					then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER>{ data($req/m:CustomCustomernumber) }&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER>
					else ()
			}
			{
				if($req/m:PagecNavigationkeyvalu)
					then &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU>{ data($req/m:PagecNavigationkeyvalu) }&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU>
					else ()
			}
			{
				if($req/m:PagecReverseorder)
					then &lt;fml:NF_PAGEC_REVERSEORDER>{ data($req/m:PagecReverseorder) }&lt;/fml:NF_PAGEC_REVERSEORDER>
					else ()
			}
			{
				if($req/m:PagecActioncode)
					then &lt;fml:NF_PAGEC_ACTIONCODE>{ data($req/m:PagecActioncode) }&lt;/fml:NF_PAGEC_ACTIONCODE>
					else ()
			}
			{
				if($req/m:PagecPagesize)
					then &lt;fml:NF_PAGEC_PAGESIZE>{ data($req/m:PagecPagesize) }&lt;/fml:NF_PAGEC_PAGESIZE>
					else ()
			}

                                               &lt;NF_CTRL_ACTIVENONACTIVE>1&lt;/NF_CTRL_ACTIVENONACTIVE>
                                               &lt;NF_ACCOUN_ALTERNATIVEADDRE>0&lt;/NF_ACCOUN_ALTERNATIVEADDRE>
                                               &lt;NF_PRODUA_CODEPRODUCTAREA>0&lt;/NF_PRODUA_CODEPRODUCTAREA>
                                               &lt;NF_CURREC_CURRENCYCODE>&lt;/NF_CURREC_CURRENCYCODE>
                                               &lt;NF_ACCOUR_RELATIONSHIP>&lt;/NF_ACCOUR_RELATIONSHIP>
                                               &lt;NF_PRODUC_IDPRODUCTCATEGOR>0&lt;/NF_PRODUC_IDPRODUCTCATEGOR>
                                               &lt;NF_CTRL_OPTION>0&lt;/NF_CTRL_OPTION>
                                               &lt;NF_MSHEAD_COMPANYID>1&lt;/NF_MSHEAD_COMPANYID> 
                                               &lt;NF_CTRL_AREAS>0203101314&lt;/NF_CTRL_AREAS>
                                               &lt;NF_PRODUG_VISIBILITYCRM>1&lt;/NF_PRODUG_VISIBILITYCRM>
 		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetCustProdRelRequest($body/m:CRMGetCustProdRelRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>