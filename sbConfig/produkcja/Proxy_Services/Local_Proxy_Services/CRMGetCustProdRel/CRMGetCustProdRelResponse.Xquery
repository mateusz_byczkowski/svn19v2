<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustProdRelResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustProdRelResponse) {
		&lt;m:CRMGetCustProdRelResponse>
			{
				if($fml/fml:NF_PAGECC_OPERATIONS)
					then &lt;m:PageccOperations>{ data($fml/fml:NF_PAGECC_OPERATIONS) }&lt;/m:PageccOperations>
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
					then &lt;m:PagecNavigationkeyvalu>{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/m:PagecNavigationkeyvalu>
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_HASNEXT)
					then &lt;m:PagecHasnext>{ data($fml/fml:NF_PAGEC_HASNEXT) }&lt;/m:PagecHasnext>
					else ()
			}
			{

				let $NF_APPLIN_APPLICATIONNUMBE := $fml/fml:NF_APPLIN_APPLICATIONNUMBE
				let $NF_ACCOUN_ACCOUNTNUMBER := $fml/fml:NF_ACCOUN_ACCOUNTNUMBER
				let $NF_ACCOUR_RELATIONSHIP := $fml/fml:NF_ACCOUR_RELATIONSHIP
				let $NF_TAX_TAXPERCENTAGE := $fml/fml:NF_TAX_TAXPERCENTAGE
                                                                let $NF_PRODAT_IDPRODUCTDEFINIT := $fml/fml:NF_PRODAT_IDPRODUCTDEFINIT
                                                                let $PT_ID_GROUP := $fml/fml:PT_ID_GROUP
				for $it at $p in $fml/fml:NF_ACCOUN_ACCOUNTNUMBER
				return
					&lt;m:Account>
					{
						if($NF_APPLIN_APPLICATIONNUMBE[$p])
							then &lt;m:ApplinApplicationnumbe>{ data($NF_APPLIN_APPLICATIONNUMBE[$p]) }&lt;/m:ApplinApplicationnumbe>
						else ()
					}
					{
						if($NF_ACCOUN_ACCOUNTNUMBER[$p])
							then &lt;m:AccounAccountnumber>{ data($NF_ACCOUN_ACCOUNTNUMBER[$p]) }&lt;/m:AccounAccountnumber>
						else ()
					}
					{
						if($NF_ACCOUR_RELATIONSHIP[$p])
							then &lt;m:CustarCustomeraccountr>{ data($NF_ACCOUR_RELATIONSHIP[$p]) }&lt;/m:CustarCustomeraccountr>
						else ()
					}
					{
						if($NF_TAX_TAXPERCENTAGE[$p])
							then &lt;m:TaxTaxpercentage>{ data($NF_TAX_TAXPERCENTAGE[$p]) }&lt;/m:TaxTaxpercentage>
						else ()
					}
                                                                                {
						if($NF_PRODAT_IDPRODUCTDEFINIT[$p])
							then &lt;m:ApplicationCode>{ data($NF_PRODAT_IDPRODUCTDEFINIT[$p]) }&lt;/m:ApplicationCode>
						else ()
					}
                                                                                {
						if($PT_ID_GROUP[$p])
							then &lt;m:ProductGroupId>{ data($PT_ID_GROUP[$p]) }&lt;/m:ProductGroupId>
						else ()
					}


					&lt;/m:Account>
			}

		&lt;/m:CRMGetCustProdRelResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetCustProdRelResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>