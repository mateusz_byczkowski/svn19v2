<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";

declare variable $body as element(soap-env:Body) external;

declare function xf:mapSetSaveAccountResponse($fml as element(fml:FML32))
	as element(dcl:invokeResponse) {
    let $rescode:=$fml/fml:DC_OPIS_BLEDU
    let $accountNumberIBAN:=$fml/fml:DC_NR_RACHUNKU[2] 
 
    return
	&lt;dcl:invokeResponse>
            &lt;dcl:response>
		&lt;ns3:ResponseMessage>
                   {
                      if (data($rescode)) then 
		         &lt;ns3:result>false&lt;/ns3:result>
                      else 
		         &lt;ns3:result>true&lt;/ns3:result>
                    }
                &lt;/ns3:ResponseMessage>
            &lt;/dcl:response>       
            {
             if (not(data($rescode))) then 
	       &lt;dcl:accountOut>
                  &lt;ns1:Account>
                    &lt;ns1:accountNumber>{data($accountNumberIBAN)}&lt;/ns1:accountNumber>
                  &lt;/ns1:Account>  
               &lt;/dcl:accountOut>  
             else ()
            }
	&lt;/dcl:invokeResponse>


};


&lt;soap-env:Body>
{ xf:mapSetSaveAccountResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>