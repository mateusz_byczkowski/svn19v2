<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function local:lpadz2($i as xs:integer) as xs:string {
	if ($i &lt; 10) then fn:concat("0", xs:string($i)) else xs:string($i)
};

declare function local:formatCISDate($d as xs:date) as xs:string {
	let $y := fn:year-from-date($d)
	let $m := fn:month-from-date($d)
	let $d := fn:day-from-date($d)
	return
		fn:concat(local:lpadz2($d), "-", local:lpadz2($m), "-", xs:string($y))
};

declare function xf:mapCRMGetPortStatsRequest($req as element(m:CRMGetPortStatsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdPoziomu)
					then &lt;fml:CI_ID_POZIOMU>{ data($req/m:IdPoziomu) }&lt;/fml:CI_ID_POZIOMU>
					else ()
			}
			{
				if($req/m:PoziomAgregacji)
					then &lt;fml:CI_POZIOM_AGREGACJI>{ data($req/m:PoziomAgregacji) }&lt;/fml:CI_POZIOM_AGREGACJI>
					else ()
			}
			{
				if($req/m:DataAktualizacji) then
					&lt;fml:CI_DATA_AKTUALIZACJI>{data($req/m:DataAktualizacji) }&lt;/fml:CI_DATA_AKTUALIZACJI>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				for $v in $req/m:KodProduktu
				return
					&lt;fml:CI_KOD_PRODUKTU>{ data($v) }&lt;/fml:CI_KOD_PRODUKTU>
			}
			

			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI>{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI>
					else ()
			}
			{
				if($req/m:ZakresRaportu)
					then &lt;fml:CI_ZAKRES_RAPORTU>{ data($req/m:ZakresRaportu) }&lt;/fml:CI_ZAKRES_RAPORTU>
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:ZnacznikOkresu)
					then &lt;fml:CI_ZNACZNIK_OKRESU>{ data($req/m:ZnacznikOkresu) }&lt;/fml:CI_ZNACZNIK_OKRESU>
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA>{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $req as element(m:CRMGetPortStatsRequest) external;
&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
{ xf:mapCRMGetPortStatsRequest($req) }
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>