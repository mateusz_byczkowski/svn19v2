<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>TEET 52630 - Dodanie pól:
BankRelative,  FolderCreateDate.Version.$1.2011-02-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCrtProspectRequest($req as element(m:CRMCreateProspFolderRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
{
if ($req/m:CompanyId) then
&lt;fml:CI_ID_SPOLKI>{data($req/m:CompanyId)}&lt;/fml:CI_ID_SPOLKI>
else ()
}
{
if ($req/m:CustType) then
&lt;fml:DC_TYP_KLIENTA>{data($req/m:CustType)}&lt;/fml:DC_TYP_KLIENTA>
else ()
}
{
if ($req/m:MarketingAgree) then
&lt;fml:CI_UDOSTEP_GRUPA>{data($req/m:MarketingAgree)}&lt;/fml:CI_UDOSTEP_GRUPA>
else ()
}
{
if ($req/m:InfoSaleAgree) then
&lt;fml:CI_ZGODA_INF_HANDLOWA>{data($req/m:InfoSaleAgree)}&lt;/fml:CI_ZGODA_INF_HANDLOWA>
else ()
}
{
if ($req/m:CustName) then
&lt;fml:CI_NAZWA_PELNA>{data($req/m:CustName)}&lt;/fml:CI_NAZWA_PELNA>
else ()
}
{
if ($req/m:CustAddressStreet) then
&lt;fml:DC_ULICA_DANE_PODST>{data($req/m:CustAddressStreet)}&lt;/fml:DC_ULICA_DANE_PODST>
else ()
}
{
if ($req/m:CustAddressHouseFlatNo) then
&lt;fml:DC_NR_POSES_LOKALU_DANE_PODST>{data($req/m:CustAddressHouseFlatNo)}&lt;/fml:DC_NR_POSES_LOKALU_DANE_PODST>
else ()
}
{
if ($req/m:CustAddressPostCode) then
&lt;fml:DC_KOD_POCZTOWY_DANE_PODST>{data($req/m:CustAddressPostCode)}&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST>
else ()
}
{
if ($req/m:CustAddressCity) then
&lt;fml:DC_MIASTO_DANE_PODST>{data($req/m:CustAddressCity)}&lt;/fml:DC_MIASTO_DANE_PODST>
else ()
}
{
if ($req/m:CustAddressCounty) then
&lt;fml:DC_WOJ_KRAJ_DANE_PODST>{data($req/m:CustAddressCounty)}&lt;/fml:DC_WOJ_KRAJ_DANE_PODST>
else ()
}
{
if ($req/m:CustAddressCountry) then
&lt;fml:CI_KOD_KRAJU>{data($req/m:CustAddressCountry)}&lt;/fml:CI_KOD_KRAJU>
else ()
}
{
if ($req/m:CustAddressForFlag) then
&lt;fml:DC_TYP_ADRESU>{data($req/m:CustAddressForFlag)}&lt;/fml:DC_TYP_ADRESU>
else ()
}
{
if ($req/m:ResidentFlag) then
&lt;fml:DC_REZ_NIEREZ>{data($req/m:ResidentFlag)}&lt;/fml:DC_REZ_NIEREZ>
else ()
}
{
if ($req/m:Email) then
&lt;fml:DC_ADRES_E_MAIL>{data($req/m:Email)}&lt;/fml:DC_ADRES_E_MAIL>
else ()
}
{
if ($req/m:PhoneNo) then
&lt;fml:DC_NR_TELEFONU>{data($req/m:PhoneNo)}&lt;/fml:DC_NR_TELEFONU>
else ()
}
{
if ($req/m:FaxNo) then
&lt;fml:DC_NUMER_FAKSU>{data($req/m:FaxNo)}&lt;/fml:DC_NUMER_FAKSU>
else ()
}
{
if ($req/m:MobileNo) then
&lt;fml:DC_NR_TELEF_KOMORKOWEGO>{data($req/m:MobileNo)}&lt;/fml:DC_NR_TELEF_KOMORKOWEGO>
else ()
}
{
if ($req/m:BusinessPhoneNo) then
&lt;fml:DC_NUMER_TELEFONU_PRACA>{data($req/m:BusinessPhoneNo)}&lt;/fml:DC_NUMER_TELEFONU_PRACA>
else ()
}
{
if ($req/m:CustIdCardNo) then
&lt;fml:DC_NR_DOWODU_REGON>{data($req/m:CustIdCardNo)}&lt;/fml:DC_NR_DOWODU_REGON>
else ()
}
{
if ($req/m:RegonNo) then
&lt;fml:DC_NR_DOWODU_REGON>{data($req/m:RegonNo)}&lt;/fml:DC_NR_DOWODU_REGON>
else ()
}
{
if ($req/m:CustPesel) then
&lt;fml:DC_NR_PESEL>{data($req/m:CustPesel)}&lt;/fml:DC_NR_PESEL>
else ()
}
{
if ($req/m:NipNo) then
&lt;fml:DC_NIP>{data($req/m:NipNo)}&lt;/fml:DC_NIP>
else ()
}
{
if ($req/m:NipGrantDate) then
&lt;fml:CI_DATA_NIP>{data($req/m:NipGrantDate)}&lt;/fml:CI_DATA_NIP>
else ()
}
{
if ($req/m:Spw) then
&lt;fml:DC_SPW>{data($req/m:Spw)}&lt;/fml:DC_SPW>
else ()
}
{
if ($req/m:SignCardIcbsNo) then
&lt;fml:DC_KARTA_WZOROW_PODPISOW>{data($req/m:SignCardIcbsNo)}&lt;/fml:DC_KARTA_WZOROW_PODPISOW>
else ()
}
{
if ($req/m:MainIcbsNo) then
&lt;fml:CI_NR_ODDZIALU>{data($req/m:MainIcbsNo)}&lt;/fml:CI_NR_ODDZIALU>
else ()
}
{
if ($req/m:DataSrc) then
&lt;fml:DC_ZRODLO_DANYCH>{data($req/m:DataSrc)}&lt;/fml:DC_ZRODLO_DANYCH>
else ()
}
{
if ($req/m:CorpoUnitId) then
&lt;fml:DC_JEDNOSTKA_KORPORACYJNA>{data($req/m:CorpoUnitId)}&lt;/fml:DC_JEDNOSTKA_KORPORACYJNA>
else ()
}
{
if ($req/m:TaxPercentage) then
&lt;fml:CI_PROCENT_PODATKU>{data($req/m:TaxPercentage)}&lt;/fml:CI_PROCENT_PODATKU>
else ()
}
{
if ($req/m:OtherBankPrds) then
&lt;fml:CI_PROD_INNE_BANKI>{data($req/m:OtherBankPrds)}&lt;/fml:CI_PROD_INNE_BANKI>
else ()
}
{
if ($req/m:NiNo) then
&lt;fml:CI_SERIA_NR_DOK>{data($req/m:NiNo)}&lt;/fml:CI_SERIA_NR_DOK>
else ()
}
{
if ($req/m:CustPassportNo) then
&lt;fml:DC_NUMER_PASZPORTU>{data($req/m:CustPassportNo)}&lt;/fml:DC_NUMER_PASZPORTU>
else ()
}
{
if ($req/m:CarOwnerFlag) then
&lt;fml:CI_POSIADA_SAMOCHOD>{data($req/m:CarOwnerFlag)}&lt;/fml:CI_POSIADA_SAMOCHOD>
else ()
}
{
if ($req/m:CustSex) then
&lt;fml:DC_PLEC>{data($req/m:CustSex)}&lt;/fml:DC_PLEC>
else ()
}
{
if ($req/m:CustFirstName) then
&lt;fml:DC_IMIE>{data($req/m:CustFirstName)}&lt;/fml:DC_IMIE>
else ()
}
{
if ($req/m:CustSecondName) then
&lt;fml:DC_DRUGIE_IMIE>{data($req/m:CustSecondName)}&lt;/fml:DC_DRUGIE_IMIE>
else ()
}
{
if ($req/m:CustSurname) then
&lt;fml:DC_NAZWISKO>{data($req/m:CustSurname)}&lt;/fml:DC_NAZWISKO>
else ()
}
{
if ($req/m:FatherName) then
&lt;fml:DC_IMIE_OJCA>{data($req/m:FatherName)}&lt;/fml:DC_IMIE_OJCA>
else ()
}
{
if ($req/m:MotherMaidenName) then
&lt;fml:DC_NAZWISKO_PANIENSKIE_MATKI>{data($req/m:MotherMaidenName)}&lt;/fml:DC_NAZWISKO_PANIENSKIE_MATKI>
else ()
}
{
if ($req/m:Nationality) then
&lt;fml:CI_OBYWATELSTWO>{data($req/m:Nationality)}&lt;/fml:CI_OBYWATELSTWO>
else ()
}
{
if ($req/m:CustBirthDate) then
&lt;fml:DC_DATA_URODZENIA>{data($req/m:CustBirthDate)}&lt;/fml:DC_DATA_URODZENIA>
else ()
}
{
if ($req/m:CustBirthPlace) then
&lt;fml:DC_MIEJSCE_URODZENIA>{data($req/m:CustBirthPlace)}&lt;/fml:DC_MIEJSCE_URODZENIA>
else ()
}
{
if ($req/m:CivilStatus) then
&lt;fml:DC_STAN_CYWILNY>{data($req/m:CivilStatus)}&lt;/fml:DC_STAN_CYWILNY>
else ()
}
{
if ($req/m:Language) then
&lt;fml:DC_KOD_JEZYKA>{data($req/m:Language)}&lt;/fml:DC_KOD_JEZYKA>
else ()
}
{
if ($req/m:HighSchoolName) then
&lt;fml:DC_UCZELNIA>{data($req/m:HighSchoolName)}&lt;/fml:DC_UCZELNIA>
else ()
}
{
if ($req/m:SchoolType) then
&lt;fml:DC_TYP_SZKOLY>{data($req/m:SchoolType)}&lt;/fml:DC_TYP_SZKOLY>
else ()
}
{
if ($req/m:PlanEndSchoolDate) then
&lt;fml:DC_PLANOW_DATA_UKON_SZK>{data($req/m:PlanEndSchoolDate)}&lt;/fml:DC_PLANOW_DATA_UKON_SZK>
else ()
}
{
if ($req/m:EduLevel) then
&lt;fml:DC_WYKSZTALCENIE>{data($req/m:EduLevel)}&lt;/fml:DC_WYKSZTALCENIE>
else ()
}
{
if ($req/m:EducationProfile) then
&lt;fml:DC_CHARAKTER_WYKSZTALC>{data($req/m:EducationProfile)}&lt;/fml:DC_CHARAKTER_WYKSZTALC>
else ()
}
{
if ($req/m:StudyType) then
&lt;fml:DC_SYSTEM_STUDIOW>{data($req/m:StudyType)}&lt;/fml:DC_SYSTEM_STUDIOW>
else ()
}
{
if ($req/m:SchoolSpecialization) then
&lt;fml:DC_SPECJALIZACJA>{data($req/m:SchoolSpecialization)}&lt;/fml:DC_SPECJALIZACJA>
else ()
}
{
if ($req/m:MilitaryService) then
&lt;fml:DC_SLUZBA_WOJSKOWA>{data($req/m:MilitaryService)}&lt;/fml:DC_SLUZBA_WOJSKOWA>
else ()
}
{
if ($req/m:JointPropertyFlag) then
&lt;fml:DC_WSPOLNOTA_MAJATK>{data($req/m:JointPropertyFlag)}&lt;/fml:DC_WSPOLNOTA_MAJATK>
else ()
}
{
if ($req/m:HousePeopleCount) then
&lt;fml:DC_LICZBA_OS_WE_WSP_GOSP_D>{data($req/m:HousePeopleCount)}&lt;/fml:DC_LICZBA_OS_WE_WSP_GOSP_D>
else ()
}
{
if ($req/m:KeptPeopleCount) then
&lt;fml:DC_LICZBA_OSOB_NA_UTRZ>{data($req/m:KeptPeopleCount)}&lt;/fml:DC_LICZBA_OSOB_NA_UTRZ>
else ()
}
{
if ($req/m:HouseOwnerFlag) then
&lt;fml:DC_POSIADA_DOM_MIESZK>{data($req/m:HouseOwnerFlag)}&lt;/fml:DC_POSIADA_DOM_MIESZK>
else ()
}
{
if ($req/m:HousePropertyType) then
&lt;fml:CI_TYP_WLASNOSCI>{data($req/m:HousePropertyType)}&lt;/fml:CI_TYP_WLASNOSCI>
else ()
}
{
if ($req/m:WorkPlace) then
&lt;fml:CI_SEKTOR_ZATRUDNIENIA>{data($req/m:WorkPlace)}&lt;/fml:CI_SEKTOR_ZATRUDNIENIA>
else ()
}
{
if ($req/m:MonthIncome) then
&lt;fml:DC_MIES_DOCHOD_NETTO_W_PLN>{data($req/m:MonthIncome)}&lt;/fml:DC_MIES_DOCHOD_NETTO_W_PLN>
else ()
}
{
if ($req/m:IncomeSrc) then
&lt;fml:CI_ZRODLO_DOCHODU>{data($req/m:IncomeSrc)}&lt;/fml:CI_ZRODLO_DOCHODU>
else ()
}
{
if ($req/m:ExtraMonthIncome) then
&lt;fml:CI_DOD_DOCHOD_MIES_NETTO>{data($req/m:ExtraMonthIncome)}&lt;/fml:CI_DOD_DOCHOD_MIES_NETTO>
else ()
}
{
if ($req/m:ExtraIncomeSrc) then
&lt;fml:CI_ZRODLO_DODAT_DOCH>{data($req/m:ExtraIncomeSrc)}&lt;/fml:CI_ZRODLO_DODAT_DOCH>
else ()
}
{
if ($req/m:ChargeType) then
&lt;fml:CI_RODZAJ_OBCIAZENIA>{data($req/m:ChargeType)}&lt;/fml:CI_RODZAJ_OBCIAZENIA>
else ()
}
{
if ($req/m:ChargeTypeOther) then
&lt;fml:CI_RODZAJ_OBC_INNE>{data($req/m:ChargeTypeOther)}&lt;/fml:CI_RODZAJ_OBC_INNE>
else ()
}
{
if ($req/m:SumMonthCharge) then
&lt;fml:CI_LACZNA_WYS_OBC>{data($req/m:SumMonthCharge)}&lt;/fml:CI_LACZNA_WYS_OBC>
else ()
}
{
if ($req/m:PolicesCount) then
&lt;fml:CI_LICZBA_POLIS>{data($req/m:PolicesCount)}&lt;/fml:CI_LICZBA_POLIS>
else ()
}
{
if ($req/m:PolicesSum) then
&lt;fml:CI_SUMA_POLIS_NA_ZYCIE>{data($req/m:PolicesSum)}&lt;/fml:CI_SUMA_POLIS_NA_ZYCIE>
else ()
}
{
if ($req/m:LifeInsurranceFlag) then
&lt;fml:DC_UBEZPIECZENIE_NA_ZYCIE>{data($req/m:LifeInsurranceFlag)}&lt;/fml:DC_UBEZPIECZENIE_NA_ZYCIE>
else ()
}
{
if ($req/m:NwInsurranceFlag) then
&lt;fml:DC_UBEZP_OD_NASTEPSTW_NW>{data($req/m:NwInsurranceFlag)}&lt;/fml:DC_UBEZP_OD_NASTEPSTW_NW>
else ()
}
{
if ($req/m:RetireInsurranceFlag) then
&lt;fml:DC_UBEZPIECZENIE_EMERYTALNE>{data($req/m:RetireInsurranceFlag)}&lt;/fml:DC_UBEZPIECZENIE_EMERYTALNE>
else ()
}
{
if ($req/m:WealthInsurranceFlag) then
&lt;fml:DC_UBEZP_MAJATKOWE_MIES>{data($req/m:WealthInsurranceFlag)}&lt;/fml:DC_UBEZP_MAJATKOWE_MIES>
else ()
}
{
if ($req/m:OtherInsurranceFlag) then
&lt;fml:DC_UBEZPIECZENIA_INNE>{data($req/m:OtherInsurranceFlag)}&lt;/fml:DC_UBEZPIECZENIA_INNE>
else ()
}
{
if ($req/m:RegisterNo) then
&lt;fml:CI_KRS>{data($req/m:RegisterNo)}&lt;/fml:CI_KRS>
else ()
}
{
if ($req/m:EkdCode) then
&lt;fml:DC_EKD>{data($req/m:EkdCode)}&lt;/fml:DC_EKD>
else ()
}
{
if ($req/m:AgencyCount) then
&lt;fml:CI_LICZBA_PLACOWEK>{data($req/m:AgencyCount)}&lt;/fml:CI_LICZBA_PLACOWEK>
else ()
}
{
if ($req/m:EmployedCount) then
&lt;fml:DC_LICZBA_ZATRUDNIONYCH>{data($req/m:EmployedCount)}&lt;/fml:DC_LICZBA_ZATRUDNIONYCH>
else ()
}
{
if ($req/m:BusinessEndDate) then
&lt;fml:DC_DATA_BANKRUCTWA>{data($req/m:BusinessEndDate)}&lt;/fml:DC_DATA_BANKRUCTWA>
else ()
}
{
if ($req/m:WebPage) then
&lt;fml:CI_STRONA_WWW>{data($req/m:WebPage)}&lt;/fml:CI_STRONA_WWW>
else ()
}
{
if ($req/m:BusinessInitDate) then
&lt;fml:CI_DATA_ROZP_DZIAL>{data($req/m:BusinessInitDate)}&lt;/fml:CI_DATA_ROZP_DZIAL>
else ()
}
{
if ($req/m:DebtValue) then
&lt;fml:CI_KWOTA_ZADLUZENIA>{data($req/m:DebtValue)}&lt;/fml:CI_KWOTA_ZADLUZENIA>
else ()
}
{
if ($req/m:SwiftNo) then
&lt;fml:CI_NUMER_SWIFT>{data($req/m:SwiftNo)}&lt;/fml:CI_NUMER_SWIFT>
else ()
}
{
if ($req/m:OrgLawForm) then
&lt;fml:CI_FOP>{data($req/m:OrgLawForm)}&lt;/fml:CI_FOP>
else ()
}
{
if ($req/m:RegOrgName) then
&lt;fml:CI_NAZWA_ORG_REJESTR>{data($req/m:RegOrgName)}&lt;/fml:CI_NAZWA_ORG_REJESTR>
else ()
}
{
if ($req/m:RegOrgStreet) then
&lt;fml:CI_ULICA_ORG_REJESTR>{data($req/m:RegOrgStreet)}&lt;/fml:CI_ULICA_ORG_REJESTR>
else ()
}
{
if ($req/m:RegOrgHouseNo) then
&lt;fml:CI_NR_POSES_LOKALU_ORG_REJ>{data($req/m:RegOrgHouseNo)}&lt;/fml:CI_NR_POSES_LOKALU_ORG_REJ>
else ()
}
{
if ($req/m:RegOrgPostCode) then
&lt;fml:CI_KOD_POCZT_ORG_REJESTR>{data($req/m:RegOrgPostCode)}&lt;/fml:CI_KOD_POCZT_ORG_REJESTR>
else ()
}
{
if ($req/m:RegOrgCity) then
&lt;fml:CI_MIASTO_ORG_REJESTR>{data($req/m:RegOrgCity)}&lt;/fml:CI_MIASTO_ORG_REJESTR>
else ()
}
{
if ($req/m:RegOrgCountry) then
&lt;fml:CI_KRAJ_ORG_REJESTR>{data($req/m:RegOrgCountry)}&lt;/fml:CI_KRAJ_ORG_REJESTR>
else ()
}
{
if ($req/m:RegOrgInitDate) then
&lt;fml:CI_DATA_WPISU_DO_REJESTR>{data($req/m:RegOrgInitDate)}&lt;/fml:CI_DATA_WPISU_DO_REJESTR>
else ()
}
{
if ($req/m:HeadquaterFlag) then
&lt;fml:CI_STATUS_SIEDZIBY>{data($req/m:HeadquaterFlag)}&lt;/fml:CI_STATUS_SIEDZIBY>
else ()
}
{
if ($req/m:Auditor) then
&lt;fml:CI_AUDYTOR>{data($req/m:Auditor)}&lt;/fml:CI_AUDYTOR>
else ()
}
{
if ($req/m:OriginalCapital) then
&lt;fml:CI_KAPITAL_ZALOZYCIELSKI>{data($req/m:OriginalCapital)}&lt;/fml:CI_KAPITAL_ZALOZYCIELSKI>
else ()
}
{
if ($req/m:SharesValue) then
&lt;fml:CI_WARTOSC_AKCJI>{data($req/m:SharesValue)}&lt;/fml:CI_WARTOSC_AKCJI>
else ()
}
{
if ($req/m:ImmovablesValue) then
&lt;fml:CI_WARTOSC_NIERUCHOMOSCI>{data($req/m:ImmovablesValue)}&lt;/fml:CI_WARTOSC_NIERUCHOMOSCI>
else ()
}
{
if ($req/m:SurfaceValue) then
&lt;fml:CI_WARTOSC_SRODKOW_MATER>{data($req/m:SurfaceValue)}&lt;/fml:CI_WARTOSC_SRODKOW_MATER>
else ()
}
{
if ($req/m:OtherAssets) then
&lt;fml:CI_INNE_AKTYWA>{data($req/m:OtherAssets)}&lt;/fml:CI_INNE_AKTYWA>
else ()
}
{
if ($req/m:TaxBorrowing) then
&lt;fml:CI_ZOBOWIAZANIA_PODATKOWE>{data($req/m:TaxBorrowing)}&lt;/fml:CI_ZOBOWIAZANIA_PODATKOWE>
else ()
}
{
if ($req/m:OtherBorrowing) then
&lt;fml:CI_INNE_ZOBOWIAZANIA>{data($req/m:OtherBorrowing)}&lt;/fml:CI_INNE_ZOBOWIAZANIA>
else ()
}
{
if ($req/m:FinancialUpdateDate) then
&lt;fml:CI_DATA_AKTUAL_DANYCH_FIN>{data($req/m:FinancialUpdateDate)}&lt;/fml:CI_DATA_AKTUAL_DANYCH_FIN>
else ()
}
{
if ($req/m:Income) then
&lt;fml:CI_PRZYCHODY_INST>{data($req/m:Income)}&lt;/fml:CI_PRZYCHODY_INST>
else ()
}
{
if ($req/m:CustCorrAddressStreet) then
&lt;fml:DC_ULICA_ADRES_ALT>{data($req/m:CustCorrAddressStreet)}&lt;/fml:DC_ULICA_ADRES_ALT>
else ()
}
{
if ($req/m:CustCorrAddressHouseFlatNo) then
&lt;fml:DC_NR_POSES_LOKALU_ADRES_ALT>{data($req/m:CustCorrAddressHouseFlatNo)}&lt;/fml:DC_NR_POSES_LOKALU_ADRES_ALT>
else ()
}
{
if ($req/m:CustCorrAddressPostCode) then
&lt;fml:DC_KOD_POCZTOWY_ADRES_ALT>{data($req/m:CustCorrAddressPostCode)}&lt;/fml:DC_KOD_POCZTOWY_ADRES_ALT>
else ()
}
{
if ($req/m:CustCorrAddressCity) then
&lt;fml:DC_MIASTO_ADRES_ALT>{data($req/m:CustCorrAddressCity)}&lt;/fml:DC_MIASTO_ADRES_ALT>
else ()
}
{
if ($req/m:CustCorrAddressCounty) then
&lt;fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{data($req/m:CustCorrAddressCounty)}&lt;/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
else ()
}
{
if ($req/m:CustCorrAddressCountry) then
&lt;fml:CI_KOD_KRAJU_KORESP>{data($req/m:CustCorrAddressCountry)}&lt;/fml:CI_KOD_KRAJU_KORESP>
else ()
}
{
if ($req/m:CustCorrValidDateFrom) then
&lt;fml:CI_DATA_OD>{data($req/m:CustCorrValidDateFrom)}&lt;/fml:CI_DATA_OD>
else ()
}
{
if ($req/m:CustCorrValidDateTo) then
&lt;fml:CI_DATA_DO>{data($req/m:CustCorrValidDateTo)}&lt;/fml:CI_DATA_DO>
else ()
}
{
if ($req/m:CustCorrAddressForFlag) then
&lt;fml:CI_TYP_ADRESU_KORESP>{data($req/m:CustCorrAddressForFlag)}&lt;/fml:CI_TYP_ADRESU_KORESP>
else ()
}
{
if ($req/m:EmpName) then
&lt;fml:CI_NAZWA_PRACODAWCY>{data($req/m:EmpName)}&lt;/fml:CI_NAZWA_PRACODAWCY>
else ()
}
{
if ($req/m:EmpAddressStreet) then
&lt;fml:CI_ULICA_PRACODAWCY>{data($req/m:EmpAddressStreet)}&lt;/fml:CI_ULICA_PRACODAWCY>
else ()
}
{
if ($req/m:EmpAddressHouseFlatNo) then
&lt;fml:CI_NUMER_POSESJI_PRAC>{data($req/m:EmpAddressHouseFlatNo)}&lt;/fml:CI_NUMER_POSESJI_PRAC>
else ()
}
{
if ($req/m:EmpAddressPostCode) then
&lt;fml:CI_KOD_POCZT_PRAC>{data($req/m:EmpAddressPostCode)}&lt;/fml:CI_KOD_POCZT_PRAC>
else ()
}
{
if ($req/m:EmpAddressCity) then
&lt;fml:CI_MIASTO_PRACODAWCY>{data($req/m:EmpAddressCity)}&lt;/fml:CI_MIASTO_PRACODAWCY>
else ()
}
{
if ($req/m:EmpAddressCounty) then
&lt;fml:CI_WOJEWODZTWO_PRAC>{data($req/m:EmpAddressCounty)}&lt;/fml:CI_WOJEWODZTWO_PRAC>
else ()
}
{
if ($req/m:EmpAddressCountry) then
&lt;fml:CI_KOD_KRAJU_PRAC>{data($req/m:EmpAddressCountry)}&lt;/fml:CI_KOD_KRAJU_PRAC>
else ()
}
{
if ($req/m:EmpPhone) then
&lt;fml:CI_NR_TELEFONU_PRAC>{data($req/m:EmpPhone)}&lt;/fml:CI_NR_TELEFONU_PRAC>
else ()
}
{
if ($req/m:JobCode) then
&lt;fml:DC_KOD_ZAWODU>{data($req/m:JobCode)}&lt;/fml:DC_KOD_ZAWODU>
else ()
}
{
if ($req/m:Profession) then
&lt;fml:CI_KOD_ZAWODU_WYUCZONEGO>{data($req/m:Profession)}&lt;/fml:CI_KOD_ZAWODU_WYUCZONEGO>
else ()
}
{
if ($req/m:EmployeeStatus) then
&lt;fml:DC_STATUS_PRACOWNIKA>{data($req/m:EmployeeStatus)}&lt;/fml:DC_STATUS_PRACOWNIKA>
else ()
}
{
if ($req/m:JobStartDate) then
&lt;fml:DC_DATA_PODJECIA_PRACY>{data($req/m:JobStartDate)}&lt;/fml:DC_DATA_PODJECIA_PRACY>
else ()
}
{
if ($req/m:TempJob) then
&lt;fml:DC_WYKONYWANA_PRACA>{data($req/m:TempJob)}&lt;/fml:DC_WYKONYWANA_PRACA>
else ()
}
{
if ($req/m:BusinessType) then
&lt;fml:CI_RODZ_DZIALALNOSCI>{data($req/m:BusinessType)}&lt;/fml:CI_RODZ_DZIALALNOSCI>
else ()
}

{
if ($req/m:BusinessCountry) then
&lt;fml:CI_KRAJ_PROWADZENIA_DZIAL>{data($req/m:BusinessCountry)}&lt;/fml:CI_KRAJ_PROWADZENIA_DZIAL>
else ()
}

{
if ($req/m:PodpisanaUmowaLimitWKo) then
&lt;fml:DC_PODPISANA_UMOWA_LIMIT_W_KO>{data($req/m:PodpisanaUmowaLimitWKo)}&lt;/fml:DC_PODPISANA_UMOWA_LIMIT_W_KO>
else ()
}
{
if ($req/m:EmpSkpNo) then
&lt;fml:CI_PRACOWNIK_WPROW>{data($req/m:EmpSkpNo)}&lt;/fml:CI_PRACOWNIK_WPROW>
else ()
}
{
if ($req/m:CustBirthCountry) then
&lt;fml:CI_KRAJ_POCHODZENIA>{data($req/m:CustBirthCountry)}&lt;/fml:CI_KRAJ_POCHODZENIA>
else ()
}
{
if ($req/m:StudyYear) then
&lt;fml:DC_ROK_STUDIOW>{data($req/m:StudyYear)}&lt;/fml:DC_ROK_STUDIOW>
else ()
}
{
if ($req/m:AssignUnitDate) then
&lt;fml:CI_DATA_ODDZIAL_OBS>{data($req/m:AssignUnitDate)}&lt;/fml:CI_DATA_ODDZIAL_OBS>
else ()
}
{
if ($req/m:OperationIcbsNo) then
&lt;fml:CI_ODDZIAL_OBSLUGUJACY>{data($req/m:OperationIcbsNo)}&lt;/fml:CI_ODDZIAL_OBSLUGUJACY>
else ()
}
{
if ($req/m:ResidenceCountry) then
&lt;fml:CI_KRAJ_ZAMIESZKANIA>{data($req/m:ResidenceCountry)}&lt;/fml:CI_KRAJ_ZAMIESZKANIA>
else ()
}
{
if ($req/m:ComplexStructFlag) then
&lt;fml:CI_STRUKTURA_ZLOZONA>{data($req/m:ComplexStructFlag)}&lt;/fml:CI_STRUKTURA_ZLOZONA>
else ()
}
{
if ($req/m:GainingsValue) then
&lt;fml:CI_ZYSKI_INST>{data($req/m:GainingsValue)}&lt;/fml:CI_ZYSKI_INST>
else()
}
{
if ($req/m:KodEkdPracodaw) then
&lt;fml:DC_KOD_EKD_PRACODAW>{data($req/m:KodEkdPracodaw)}&lt;/fml:DC_KOD_EKD_PRACODAW>
else ()
}
{
if ($req/m:LifeInsurerName) then
&lt;fml:CI_UBEZP_NA_ZYCIE>{data($req/m:LifeInsurerName)}&lt;/fml:CI_UBEZP_NA_ZYCIE>
else ()
}
{
if ($req/m:NwInsurerName) then
&lt;fml:CI_UBEZP_NW>{data($req/m:NwInsurerName)}&lt;/fml:CI_UBEZP_NW>
else ()
}
{
if ($req/m:RetireInsurerName) then
&lt;fml:CI_UBEZP_EMERYTALNE>{data($req/m:RetireInsurerName)}&lt;/fml:CI_UBEZP_EMERYTALNE>
else ()
}
{
if ($req/m:WealthInsurerName) then
&lt;fml:CI_UBEZP_MAJATKOWE>{data($req/m:WealthInsurerName)}&lt;/fml:CI_UBEZP_MAJATKOWE>
else ()
}
{
if ($req/m:OtherInsurerName) then
&lt;fml:CI_UBEZP_INNE>{data($req/m:OtherInsurerName)}&lt;/fml:CI_UBEZP_INNE>
else ()
}
{
if ($req/m:TypNiNo) then
&lt;fml:CI_DOK_TOZSAMOSCI>{data($req/m:TypNiNo)}&lt;/fml:CI_DOK_TOZSAMOSCI>
else ()
}
{
if ($req/m:EmpAddressForFlag) then
&lt;fml:CI_TYP_ADRESU_PRAC>{data($req/m:EmpAddressForFlag)}&lt;/fml:CI_TYP_ADRESU_PRAC>
else ()
}
{
if ($req/m:KierunekStudiow) then
&lt;fml:DC_KIERUNEK_STUDIOW>{data($req/m:KierunekStudiow)}&lt;/fml:DC_KIERUNEK_STUDIOW>
else ()
}
{
if ($req/m:CustProcessType) then
&lt;fml:CI_TYP_PRZET>{data($req/m:CustProcessType)}&lt;/fml:CI_TYP_PRZET>
else ()
}
{
if ($req/m:KapitalZalozOplacony) then
&lt;fml:CI_KAPITAL_ZALOZ_OPLACONY>{data($req/m:KapitalZalozOplacony)}&lt;/fml:CI_KAPITAL_ZALOZ_OPLACONY>
else()
}
{
if ($req/m:BankRelative) then
&lt;fml:DC_PRACOWNIK_BANKU>{data($req/m:BankRelative)}&lt;/fml:DC_PRACOWNIK_BANKU>
else ()
}
{
if ($req/m:FolderCreateDate) then
&lt;fml:CI_DATA_WPROWADZENIA>{data($req/m:FolderCreateDate)}&lt;/fml:CI_DATA_WPROWADZENIA>
else ()
}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCrtProspectRequest($body/m:CRMCreateProspFolderRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>