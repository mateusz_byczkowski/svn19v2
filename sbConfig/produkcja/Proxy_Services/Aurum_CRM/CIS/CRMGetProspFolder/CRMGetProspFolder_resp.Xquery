<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>TEET 52630 - Dodanie pola BankRelative.Version.$1.2011-02-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetProspFolderResponse($fml as element(fml:FML32))
as element(m:CRMGetProspFolderResponse) {
&lt;m:CRMGetProspFolderResponse>
{
if ($fml/fml:DC_TYP_KLIENTA) then
&lt;m:CustType>{data($fml/fml:DC_TYP_KLIENTA)}&lt;/m:CustType>
else ()
}
{
if ($fml/fml:CI_DATA_WPROWADZENIA) then
&lt;m:FolderCreateDate>{data($fml/fml:CI_DATA_WPROWADZENIA)}&lt;/m:FolderCreateDate>
else ()
}
{
if ($fml/fml:CI_UDOSTEP_GRUPA) then
&lt;m:MarketingAgree>{data($fml/fml:CI_UDOSTEP_GRUPA)}&lt;/m:MarketingAgree>
else ()
}
{
if ($fml/fml:CI_ZGODA_INF_HANDLOWA) then
&lt;m:InfoSaleAgree>{data($fml/fml:CI_ZGODA_INF_HANDLOWA)}&lt;/m:InfoSaleAgree>
else ()
}
{
if ($fml/fml:CI_NAZWA_PELNA) then
&lt;m:CustName>{data($fml/fml:CI_NAZWA_PELNA)}&lt;/m:CustName>
else ()
}
{
if ($fml/fml:DC_ULICA_DANE_PODST) then
&lt;m:CustAddressStreet>{data($fml/fml:DC_ULICA_DANE_PODST)}&lt;/m:CustAddressStreet>
else ()
}
{
if ($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST) then
&lt;m:CustAddressHouseFlatNo>{data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST)}&lt;/m:CustAddressHouseFlatNo>
else ()
}
{
if ($fml/fml:DC_KOD_POCZTOWY_DANE_PODST) then
&lt;m:CustAddressPostCode>{data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST)}&lt;/m:CustAddressPostCode>
else ()
}
{
if ($fml/fml:DC_MIASTO_DANE_PODST) then
&lt;m:CustAddressCity>{data($fml/fml:DC_MIASTO_DANE_PODST)}&lt;/m:CustAddressCity>
else ()
}
{
if ($fml/fml:DC_WOJ_KRAJ_DANE_PODST) then
&lt;m:CustAddressCounty>{data($fml/fml:DC_WOJ_KRAJ_DANE_PODST)}&lt;/m:CustAddressCounty>
else ()
}
{
if ($fml/fml:CI_KOD_KRAJU) then
&lt;m:CustAddressCountry>{data($fml/fml:CI_KOD_KRAJU)}&lt;/m:CustAddressCountry>
else ()
}
{
if ($fml/fml:DC_TYP_ADRESU) then
&lt;m:CustAddressForFlag>{data($fml/fml:DC_TYP_ADRESU)}&lt;/m:CustAddressForFlag>
else ()
}
{
if ($fml/fml:DC_REZ_NIEREZ) then
&lt;m:ResidentFlag>{data($fml/fml:DC_REZ_NIEREZ)}&lt;/m:ResidentFlag>
else ()
}
{
if ($fml/fml:DC_ADRES_E_MAIL) then
&lt;m:Email>{data($fml/fml:DC_ADRES_E_MAIL)}&lt;/m:Email>
else ()
}
{
if ($fml/fml:DC_NR_TELEFONU) then
&lt;m:PhoneNo>{data($fml/fml:DC_NR_TELEFONU)}&lt;/m:PhoneNo>
else ()
}
{
if ($fml/fml:DC_NUMER_FAKSU) then
&lt;m:FaxNo>{data($fml/fml:DC_NUMER_FAKSU)}&lt;/m:FaxNo>
else ()
}
{
if ($fml/fml:DC_NR_TELEF_KOMORKOWEGO) then
&lt;m:MobileNo>{data($fml/fml:DC_NR_TELEF_KOMORKOWEGO)}&lt;/m:MobileNo>
else ()
}
{
if ($fml/fml:DC_NUMER_TELEFONU_PRACA) then
&lt;m:BusinessPhoneNo>{data($fml/fml:DC_NUMER_TELEFONU_PRACA)}&lt;/m:BusinessPhoneNo>
else ()
}
{
if ($fml/fml:DC_NR_DOWODU_REGON) then
if ($fml/fml:DC_TYP_KLIENTA = "F") then
&lt;m:CustIdCardNo>{data($fml/fml:DC_NR_DOWODU_REGON)}&lt;/m:CustIdCardNo>
else
&lt;m:CustIdCardNo>&lt;/m:CustIdCardNo>
else ()
}
{
if ($fml/fml:DC_NR_DOWODU_REGON) then
if ($fml/fml:DC_TYP_KLIENTA = "F") then
&lt;m:RegonNo>&lt;/m:RegonNo>
else
&lt;m:RegonNo>{data($fml/fml:DC_NR_DOWODU_REGON)}&lt;/m:RegonNo>
else ()
}
{
if ($fml/fml:DC_NR_PESEL) then
&lt;m:CustPesel>{data($fml/fml:DC_NR_PESEL)}&lt;/m:CustPesel>
else ()
}
{
if ($fml/fml:DC_NIP) then
&lt;m:NipNo>{data($fml/fml:DC_NIP)}&lt;/m:NipNo>
else ()
}
{
if ($fml/fml:CI_DATA_NIP) then
&lt;m:NipGrantDate>{data($fml/fml:CI_DATA_NIP)}&lt;/m:NipGrantDate>
else ()
}
{
if ($fml/fml:DC_SPW) then
&lt;m:Spw>{data($fml/fml:DC_SPW)}&lt;/m:Spw>
else ()
}
{
if ($fml/fml:DC_KARTA_WZOROW_PODPISOW) then
&lt;m:SignCardIcbsNo>{data($fml/fml:DC_KARTA_WZOROW_PODPISOW)}&lt;/m:SignCardIcbsNo>
else ()
}
{
if ($fml/fml:CI_NR_ODDZIALU) then
&lt;m:MainIcbsNo>{data($fml/fml:CI_NR_ODDZIALU)}&lt;/m:MainIcbsNo>
else ()
}
{
if ($fml/fml:CI_PRACOWNIK_WPROW) then
&lt;m:CreateFolderEmpSkpNo>{data($fml/fml:CI_PRACOWNIK_WPROW)}&lt;/m:CreateFolderEmpSkpNo>
else ()
}
{
if ($fml/fml:CI_PRACOWNIK_ZMIEN) then
&lt;m:ModifyFolderEmpSkpNo>{data($fml/fml:CI_PRACOWNIK_ZMIEN)}&lt;/m:ModifyFolderEmpSkpNo>
else ()
}
{
if ($fml/fml:CI_DATA_MODYFIKACJI) then
&lt;m:FolderModificationDate>{data($fml/fml:CI_DATA_MODYFIKACJI)}&lt;/m:FolderModificationDate>
else ()
}
{
if ($fml/fml:DC_ZRODLO_DANYCH) then
&lt;m:DataSrc>{data($fml/fml:DC_ZRODLO_DANYCH)}&lt;/m:DataSrc>
else ()
}
{
if ($fml/fml:DC_JEDNOSTKA_KORPORACYJNA) then
&lt;m:CorpoUnitId>{data($fml/fml:DC_JEDNOSTKA_KORPORACYJNA)}&lt;/m:CorpoUnitId>
else ()
}
{
if ($fml/fml:CI_PROCENT_PODATKU) then
&lt;m:TaxPercentage>{fn:round-half-to-even(data($fml/fml:CI_PROCENT_PODATKU), 5)}&lt;/m:TaxPercentage>
else ()
}
{
if ($fml/fml:CI_PROD_INNE_BANKI) then
&lt;m:OtherBankPrds>{data($fml/fml:CI_PROD_INNE_BANKI)}&lt;/m:OtherBankPrds>
else ()
}
{
if ($fml/fml:CI_SERIA_NR_DOK) then
&lt;m:NiNo>{data($fml/fml:CI_SERIA_NR_DOK)}&lt;/m:NiNo>
else ()
}
{
if ($fml/fml:DC_NUMER_PASZPORTU) then
&lt;m:CustPassportNo>{data($fml/fml:DC_NUMER_PASZPORTU)}&lt;/m:CustPassportNo>
else ()
}
{
if ($fml/fml:CI_POSIADA_SAMOCHOD) then
&lt;m:CarOwnerFlag>{data($fml/fml:CI_POSIADA_SAMOCHOD)}&lt;/m:CarOwnerFlag>
else ()
}
{
if ($fml/fml:DC_PLEC) then
&lt;m:CustSex>{data($fml/fml:DC_PLEC)}&lt;/m:CustSex>
else ()
}
{
if ($fml/fml:DC_IMIE) then
&lt;m:CustFirstName>{data($fml/fml:DC_IMIE)}&lt;/m:CustFirstName>
else ()
}
{
if ($fml/fml:DC_DRUGIE_IMIE) then
&lt;m:CustSecondName>{data($fml/fml:DC_DRUGIE_IMIE)}&lt;/m:CustSecondName>
else ()
}
{
if ($fml/fml:DC_NAZWISKO) then
&lt;m:CustSurname>{data($fml/fml:DC_NAZWISKO)}&lt;/m:CustSurname>
else ()
}
{
if ($fml/fml:DC_IMIE_OJCA) then
&lt;m:FatherName>{data($fml/fml:DC_IMIE_OJCA)}&lt;/m:FatherName>
else ()
}
{
if ($fml/fml:DC_NAZWISKO_PANIENSKIE_MATKI) then
&lt;m:MotherMaidenName>{data($fml/fml:DC_NAZWISKO_PANIENSKIE_MATKI)}&lt;/m:MotherMaidenName>
else ()
}{
if ($fml/fml:CI_OBYWATELSTWO) then
&lt;m:Nationality>{data($fml/fml:CI_OBYWATELSTWO)}&lt;/m:Nationality>
else ()
}
{
if ($fml/fml:DC_DATA_URODZENIA) then
&lt;m:CustBirthDate>{data($fml/fml:DC_DATA_URODZENIA)}&lt;/m:CustBirthDate>
else ()
}
{
if ($fml/fml:DC_MIEJSCE_URODZENIA) then
&lt;m:CustBirthPlace>{data($fml/fml:DC_MIEJSCE_URODZENIA)}&lt;/m:CustBirthPlace>
else ()
}
{
if ($fml/fml:DC_STAN_CYWILNY) then
&lt;m:CivilStatus>{data($fml/fml:DC_STAN_CYWILNY)}&lt;/m:CivilStatus>
else ()
}
{
if ($fml/fml:DC_KOD_JEZYKA) then
&lt;m:Language>{data($fml/fml:DC_KOD_JEZYKA)}&lt;/m:Language>
else ()
}
{
if ($fml/fml:DC_UCZELNIA) then
&lt;m:HighSchoolName>{data($fml/fml:DC_UCZELNIA)}&lt;/m:HighSchoolName>
else ()
}
{
if ($fml/fml:DC_TYP_SZKOLY) then
&lt;m:SchoolType>{data($fml/fml:DC_TYP_SZKOLY)}&lt;/m:SchoolType>
else ()
}
{
if ($fml/fml:DC_PLANOW_DATA_UKON_SZK) then
&lt;m:PlanEndSchoolDate>{data($fml/fml:DC_PLANOW_DATA_UKON_SZK)}&lt;/m:PlanEndSchoolDate>
else ()
}
{
if ($fml/fml:DC_WYKSZTALCENIE) then
&lt;m:EduLevel>{data($fml/fml:DC_WYKSZTALCENIE)}&lt;/m:EduLevel>
else ()
}
{
if ($fml/fml:DC_CHARAKTER_WYKSZTALC) then
&lt;m:EducationProfile>{data($fml/fml:DC_CHARAKTER_WYKSZTALC)}&lt;/m:EducationProfile>
else ()
}
{
if ($fml/fml:DC_SYSTEM_STUDIOW) then
&lt;m:StudyType>{data($fml/fml:DC_SYSTEM_STUDIOW)}&lt;/m:StudyType>
else ()
}
{
if ($fml/fml:DC_SPECJALIZACJA) then
&lt;m:SchoolSpecialization>{data($fml/fml:DC_SPECJALIZACJA)}&lt;/m:SchoolSpecialization>
else ()
}
{
if ($fml/fml:DC_SLUZBA_WOJSKOWA) then
&lt;m:MilitaryService>{data($fml/fml:DC_SLUZBA_WOJSKOWA)}&lt;/m:MilitaryService>
else ()
}
{
if ($fml/fml:DC_WSPOLNOTA_MAJATK) then
&lt;m:JointPropertyFlag>{data($fml/fml:DC_WSPOLNOTA_MAJATK)}&lt;/m:JointPropertyFlag>
else ()
}
{
if ($fml/fml:DC_LICZBA_OS_WE_WSP_GOSP_D) then
&lt;m:HousePeopleCount>{data($fml/fml:DC_LICZBA_OS_WE_WSP_GOSP_D)}&lt;/m:HousePeopleCount>
else ()
}
{
if ($fml/fml:DC_LICZBA_OSOB_NA_UTRZ) then
&lt;m:KeptPeopleCount>{data($fml/fml:DC_LICZBA_OSOB_NA_UTRZ)}&lt;/m:KeptPeopleCount>
else ()
}
{
if ($fml/fml:DC_POSIADA_DOM_MIESZK) then
&lt;m:HouseOwnerFlag>{data($fml/fml:DC_POSIADA_DOM_MIESZK)}&lt;/m:HouseOwnerFlag>
else ()
}
{
if ($fml/fml:CI_TYP_WLASNOSCI) then
&lt;m:HousePropertyType>{data($fml/fml:CI_TYP_WLASNOSCI)}&lt;/m:HousePropertyType>
else ()
}
{
if ($fml/fml:DC_KOD_ZAWODU) then
&lt;m:JobCode>{data($fml/fml:DC_KOD_ZAWODU)}&lt;/m:JobCode>
else ()
}
{
if ($fml/fml:CI_KOD_ZAWODU_WYUCZONEGO) then
&lt;m:Profession>{data($fml/fml:CI_KOD_ZAWODU_WYUCZONEGO)}&lt;/m:Profession>
else ()
}
{
if ($fml/fml:CI_SEKTOR_ZATRUDNIENIA) then
&lt;m:WorkPlace>{data($fml/fml:CI_SEKTOR_ZATRUDNIENIA)}&lt;/m:WorkPlace>
else ()
}
{
if ($fml/fml:DC_MIES_DOCHOD_NETTO_W_PLN) then
&lt;m:MonthIncome>{data($fml/fml:DC_MIES_DOCHOD_NETTO_W_PLN)}&lt;/m:MonthIncome>
else ()
}
{
if ($fml/fml:CI_ZRODLO_DOCHODU) then
&lt;m:IncomeSrc>{data($fml/fml:CI_ZRODLO_DOCHODU)}&lt;/m:IncomeSrc>
else ()
}
{
if ($fml/fml:CI_DOD_DOCHOD_MIES_NETTO) then
&lt;m:ExtraMonthIncome>{data($fml/fml:CI_DOD_DOCHOD_MIES_NETTO)}&lt;/m:ExtraMonthIncome>
else ()
}{
if ($fml/fml:CI_ZRODLO_DODAT_DOCH) then
&lt;m:ExtraIncomeSrc>{data($fml/fml:CI_ZRODLO_DODAT_DOCH)}&lt;/m:ExtraIncomeSrc>
else ()
}
{
if ($fml/fml:CI_RODZAJ_OBCIAZENIA) then
&lt;m:ChargeType>{data($fml/fml:CI_RODZAJ_OBCIAZENIA)}&lt;/m:ChargeType>
else ()
}
{
if ($fml/fml:CI_RODZAJ_OBC_INNE) then
&lt;m:ChargeTypeOther>{data($fml/fml:CI_RODZAJ_OBC_INNE)}&lt;/m:ChargeTypeOther>
else ()
}
{
if ($fml/fml:CI_LACZNA_WYS_OBC) then
&lt;m:SumMonthCharge>{data($fml/fml:CI_LACZNA_WYS_OBC)}&lt;/m:SumMonthCharge>
else ()
}
{
if ($fml/fml:DC_DATA_SMIERCI) then
&lt;m:CustDeathDate>{data($fml/fml:DC_DATA_SMIERCI)}&lt;/m:CustDeathDate>
else ()
}
{
if ($fml/fml:CI_LICZBA_POLIS) then
&lt;m:PolicesCount>{data($fml/fml:CI_LICZBA_POLIS)}&lt;/m:PolicesCount>
else ()
}
{
if ($fml/fml:CI_SUMA_POLIS_NA_ZYCIE) then
&lt;m:PolicesSum>{data($fml/fml:CI_SUMA_POLIS_NA_ZYCIE)}&lt;/m:PolicesSum>
else ()
}
{
if ($fml/fml:DC_UBEZPIECZENIE_NA_ZYCIE) then
&lt;m:LifeInsurranceFlag>{data($fml/fml:DC_UBEZPIECZENIE_NA_ZYCIE)}&lt;/m:LifeInsurranceFlag>
else ()
}
{
if ($fml/fml:DC_UBEZP_OD_NASTEPSTW_NW) then
&lt;m:NwInsurranceFlag>{data($fml/fml:DC_UBEZP_OD_NASTEPSTW_NW)}&lt;/m:NwInsurranceFlag>
else ()
}
{
if ($fml/fml:DC_UBEZPIECZENIE_EMERYTALNE) then
&lt;m:RetireInsurranceFlag>{data($fml/fml:DC_UBEZPIECZENIE_EMERYTALNE)}&lt;/m:RetireInsurranceFlag>
else ()
}
{
if ($fml/fml:DC_UBEZP_MAJATKOWE_MIES) then
&lt;m:WealthInsurranceFlag>{data($fml/fml:DC_UBEZP_MAJATKOWE_MIES)}&lt;/m:WealthInsurranceFlag>
else ()
}
{
if ($fml/fml:DC_UBEZPIECZENIA_INNE) then
&lt;m:OtherInsurranceFlag>{data($fml/fml:DC_UBEZPIECZENIA_INNE)}&lt;/m:OtherInsurranceFlag>
else ()
}
{
if ($fml/fml:CI_KRS) then
&lt;m:RegisterNo>{data($fml/fml:CI_KRS)}&lt;/m:RegisterNo>
else ()
}
{
if ($fml/fml:DC_EKD) then
&lt;m:EkdCode>{data($fml/fml:DC_EKD)}&lt;/m:EkdCode>
else ()
}
{
if ($fml/fml:CI_LICZBA_PLACOWEK) then
&lt;m:AgencyCount>{data($fml/fml:CI_LICZBA_PLACOWEK)}&lt;/m:AgencyCount>
else ()
}
{
if ($fml/fml:DC_LICZBA_ZATRUDNIONYCH) then
&lt;m:EmployedCount>{data($fml/fml:DC_LICZBA_ZATRUDNIONYCH)}&lt;/m:EmployedCount>
else ()
}
{
if ($fml/fml:DC_DATA_BANKRUCTWA) then
&lt;m:BusinessEndDate>{data($fml/fml:DC_DATA_BANKRUCTWA)}&lt;/m:BusinessEndDate>
else ()
}
{
if ($fml/fml:CI_STRONA_WWW) then
&lt;m:WebPage>{data($fml/fml:CI_STRONA_WWW)}&lt;/m:WebPage>
else ()
}
{
if ($fml/fml:CI_DATA_ROZP_DZIAL) then
&lt;m:BusinessInitDate>{data($fml/fml:CI_DATA_ROZP_DZIAL)}&lt;/m:BusinessInitDate>
else ()
}
{
if ($fml/fml:CI_KWOTA_ZADLUZENIA) then
&lt;m:DebtValue>{data($fml/fml:CI_KWOTA_ZADLUZENIA)}&lt;/m:DebtValue>
else ()
}
{
if ($fml/fml:CI_NUMER_SWIFT) then
&lt;m:SwiftNo>{data($fml/fml:CI_NUMER_SWIFT)}&lt;/m:SwiftNo>
else ()
}
{
if ($fml/fml:CI_FOP) then
&lt;m:OrgLawForm>{data($fml/fml:CI_FOP)}&lt;/m:OrgLawForm>
else ()
}
{
if ($fml/fml:CI_NAZWA_ORG_REJESTR) then
&lt;m:RegOrgName>{data($fml/fml:CI_NAZWA_ORG_REJESTR)}&lt;/m:RegOrgName>
else ()
}
{
if ($fml/fml:CI_ULICA_ORG_REJESTR) then
&lt;m:RegOrgStreet>{data($fml/fml:CI_ULICA_ORG_REJESTR)}&lt;/m:RegOrgStreet>
else ()
}
{
if ($fml/fml:CI_NR_POSES_LOKALU_ORG_REJ) then
&lt;m:RegOrgHouseNo>{data($fml/fml:CI_NR_POSES_LOKALU_ORG_REJ)}&lt;/m:RegOrgHouseNo>
else ()
}
{
if ($fml/fml:CI_KOD_POCZT_ORG_REJESTR) then
&lt;m:RegOrgPostCode>{data($fml/fml:CI_KOD_POCZT_ORG_REJESTR)}&lt;/m:RegOrgPostCode>
else ()
}
{
if ($fml/fml:CI_MIASTO_ORG_REJESTR) then
&lt;m:RegOrgCity>{data($fml/fml:CI_MIASTO_ORG_REJESTR)}&lt;/m:RegOrgCity>
else ()
}
{
if ($fml/fml:CI_KRAJ_ORG_REJESTR) then
&lt;m:RegOrgCountry>{data($fml/fml:CI_KRAJ_ORG_REJESTR)}&lt;/m:RegOrgCountry>
else ()
}
{
if ($fml/fml:CI_DATA_WPISU_DO_REJESTR) then
&lt;m:RegOrgInitDate>{data($fml/fml:CI_DATA_WPISU_DO_REJESTR)}&lt;/m:RegOrgInitDate>
else ()
}
{
if ($fml/fml:CI_STATUS_SIEDZIBY) then
&lt;m:HeadquaterFlag>{data($fml/fml:CI_STATUS_SIEDZIBY)}&lt;/m:HeadquaterFlag>
else ()
}
{
if ($fml/fml:CI_AUDYTOR) then
&lt;m:Auditor>{data($fml/fml:CI_AUDYTOR)}&lt;/m:Auditor>
else ()
}
{
if ($fml/fml:CI_KAPITAL_ZALOZYCIELSKI) then
&lt;m:OriginalCapital>{data($fml/fml:CI_KAPITAL_ZALOZYCIELSKI)}&lt;/m:OriginalCapital>
else ()
}

{
if ($fml/fml:CI_WARTOSC_AKCJI) then
&lt;m:SharesValue>{data($fml/fml:CI_WARTOSC_AKCJI)}&lt;/m:SharesValue>
else ()
}
{
if ($fml/fml:CI_WARTOSC_NIERUCHOMOSCI) then
&lt;m:ImmovablesValue>{data($fml/fml:CI_WARTOSC_NIERUCHOMOSCI)}&lt;/m:ImmovablesValue>
else ()
}
{
if ($fml/fml:CI_WARTOSC_SRODKOW_MATER) then
&lt;m:SurfaceValue>{data($fml/fml:CI_WARTOSC_SRODKOW_MATER)}&lt;/m:SurfaceValue>
else ()
}
{
if ($fml/fml:CI_INNE_AKTYWA) then
&lt;m:OtherAssets>{data($fml/fml:CI_INNE_AKTYWA)}&lt;/m:OtherAssets>
else ()
}
{
if ($fml/fml:CI_ZOBOWIAZANIA_PODATKOWE) then
&lt;m:TaxBorrowing>{fn:round-half-to-even(data($fml/fml:CI_ZOBOWIAZANIA_PODATKOWE), 5)}&lt;/m:TaxBorrowing>
else ()
}
{
if ($fml/fml:CI_INNE_ZOBOWIAZANIA) then
&lt;m:OtherBorrowing>{data($fml/fml:CI_INNE_ZOBOWIAZANIA)}&lt;/m:OtherBorrowing>
else ()
}
{
if ($fml/fml:CI_DATA_AKTUAL_DANYCH_FIN) then
&lt;m:FinancialUpdateDate>{data($fml/fml:CI_DATA_AKTUAL_DANYCH_FIN)}&lt;/m:FinancialUpdateDate>
else ()
}
{
if ($fml/fml:CI_PRZYCHODY_INST) then
&lt;m:Income>{data($fml/fml:CI_PRZYCHODY_INST)}&lt;/m:Income>
else ()
}
{
if ($fml/fml:DC_ULICA_ADRES_ALT) then
&lt;m:CustCorrAddressStreet>{data($fml/fml:DC_ULICA_ADRES_ALT)}&lt;/m:CustCorrAddressStreet>
else ()
}
{
if ($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT) then
&lt;m:CustCorrAddressHouseFlatNo>{data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT)}&lt;/m:CustCorrAddressHouseFlatNo>
else ()
}
{
if ($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT) then
&lt;m:CustCorrAddressPostCode>{data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT)}&lt;/m:CustCorrAddressPostCode>
else ()
}
{
if ($fml/fml:DC_MIASTO_ADRES_ALT) then
&lt;m:CustCorrAddressCity>{data($fml/fml:DC_MIASTO_ADRES_ALT)}&lt;/m:CustCorrAddressCity>
else ()
}
{
if ($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT) then
&lt;m:CustCorrAddressCounty>{data($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT)}&lt;/m:CustCorrAddressCounty>
else ()
}
{
if ($fml/fml:CI_KOD_KRAJU_KORESP) then
&lt;m:CustCorrAddressCountry>{data($fml/fml:CI_KOD_KRAJU_KORESP)}&lt;/m:CustCorrAddressCountry>
else ()
}
{
if ($fml/fml:CI_DATA_OD) then
&lt;m:CustCorrValidDateFrom>{data($fml/fml:CI_DATA_OD)}&lt;/m:CustCorrValidDateFrom>
else ()
}
{
if ($fml/fml:CI_DATA_DO) then
&lt;m:CustCorrValidDateTo>{data($fml/fml:CI_DATA_DO)}&lt;/m:CustCorrValidDateTo>
else ()
}
{
if ($fml/fml:CI_TYP_ADRESU_KORESP) then
&lt;m:CustCorrAddressForFlag>{data($fml/fml:CI_TYP_ADRESU_KORESP)}&lt;/m:CustCorrAddressForFlag>
else ()
}
{
if ($fml/fml:CI_NAZWA_PRACODAWCY) then
&lt;m:EmpName>{data($fml/fml:CI_NAZWA_PRACODAWCY)}&lt;/m:EmpName>
else ()
}
{
if ($fml/fml:CI_ULICA_PRACODAWCY) then
&lt;m:EmpAddressStreet>{data($fml/fml:CI_ULICA_PRACODAWCY)}&lt;/m:EmpAddressStreet>
else ()
}
{
if ($fml/fml:CI_NUMER_POSESJI_PRAC) then
&lt;m:EmpAddressHouseFlatNo>{data($fml/fml:CI_NUMER_POSESJI_PRAC)}&lt;/m:EmpAddressHouseFlatNo>
else ()
}
{
if ($fml/fml:CI_KOD_POCZT_PRAC) then
&lt;m:EmpAddressPostCode>{data($fml/fml:CI_KOD_POCZT_PRAC)}&lt;/m:EmpAddressPostCode>
else ()
}
{
if ($fml/fml:CI_MIASTO_PRACODAWCY) then
&lt;m:EmpAddressCity>{data($fml/fml:CI_MIASTO_PRACODAWCY)}&lt;/m:EmpAddressCity>
else ()
}
{
if ($fml/fml:CI_WOJEWODZTWO_PRAC) then
&lt;m:EmpAddressCounty>{data($fml/fml:CI_WOJEWODZTWO_PRAC)}&lt;/m:EmpAddressCounty>
else ()
}
{
if ($fml/fml:CI_KOD_KRAJU_PRAC) then
&lt;m:EmpAddressCountry>{data($fml/fml:CI_KOD_KRAJU_PRAC)}&lt;/m:EmpAddressCountry>
else ()
}
{
if ($fml/fml:CI_TYP_ADRESU_PRAC) then
&lt;m:EmpAddressForFlag>{data($fml/fml:CI_TYP_ADRESU_PRAC)}&lt;/m:EmpAddressForFlag>
else ()
}
{
if ($fml/fml:CI_NR_TELEFONU_PRAC) then
&lt;m:EmpPhone>{data($fml/fml:CI_NR_TELEFONU_PRAC)}&lt;/m:EmpPhone>
else ()
}
{
if ($fml/fml:DC_STATUS_PRACOWNIKA) then
&lt;m:EmployeeStatus>{data($fml/fml:DC_STATUS_PRACOWNIKA)}&lt;/m:EmployeeStatus>
else ()
}
{
if ($fml/fml:DC_DATA_PODJECIA_PRACY) then
&lt;m:JobStartDate>{data($fml/fml:DC_DATA_PODJECIA_PRACY)}&lt;/m:JobStartDate>
else ()
}
{
if ($fml/fml:CI_KRAJ_ZAMIESZKANIA) then
&lt;m:ResidenceCountry>{data($fml/fml:CI_KRAJ_ZAMIESZKANIA)}&lt;/m:ResidenceCountry>
else ()
}
{
if ($fml/fml:CI_KRAJ_PROWADZENIA_DZIAL) then
&lt;m:BusinessCountry>{data($fml/fml:CI_KRAJ_PROWADZENIA_DZIAL)}&lt;/m:BusinessCountry>
else ()
}
{
if ($fml/fml:CI_STRUKTURA_ZLOZONA) then
&lt;m:ComplexStructFlag>{data($fml/fml:CI_STRUKTURA_ZLOZONA)}&lt;/m:ComplexStructFlag>
else ()
}
{
if ($fml/fml:CI_RODZ_DZIALALNOSCI) then
&lt;m:BusinessType>{data($fml/fml:CI_RODZ_DZIALALNOSCI)}&lt;/m:BusinessType>
else ()
}
{
if ($fml/fml:DC_PODPISANA_UMOWA_LIMIT_W_KO) then
&lt;m:PodpisanaUmowaLimitWKo>{data($fml/fml:DC_PODPISANA_UMOWA_LIMIT_W_KO)}&lt;/m:PodpisanaUmowaLimitWKo>
else ()
}
{
if ($fml/fml:CI_DATA_ODDZIAL_OBS) then
&lt;m:AssignUnitDate>{data($fml/fml:CI_DATA_ODDZIAL_OBS)}&lt;/m:AssignUnitDate>
else ()
}
{
if ($fml/fml:CI_KRAJ_POCHODZENIA) then
&lt;m:CustBirthCountry>{data($fml/fml:CI_KRAJ_POCHODZENIA)}&lt;/m:CustBirthCountry>
else ()
}
{
if ($fml/fml:DC_ROK_STUDIOW) then
&lt;m:StudyYear>{data($fml/fml:DC_ROK_STUDIOW)}&lt;/m:StudyYear>
else ()
}
{
if ($fml/fml:CI_ODDZIAL_OBSLUGUJACY) then
&lt;m:OperationIcbsNo>{data($fml/fml:CI_ODDZIAL_OBSLUGUJACY)}&lt;/m:OperationIcbsNo>
else ()
}
{
if ($fml/fml:CI_ZYSKI_INST) then
&lt;m:GainingsValue>{data($fml/fml:CI_ZYSKI_INST)}&lt;/m:GainingsValue>
else ()
}
{
if ($fml/fml:DC_WYKONYWANA_PRACA) then
&lt;m:TempJob>{data($fml/fml:DC_WYKONYWANA_PRACA)}&lt;/m:TempJob>
else ()
}
{
if ($fml/fml:DC_KOD_EKD_PRACODAW) then
&lt;m:KodEkdPracodaw>{data($fml/fml:DC_KOD_EKD_PRACODAW)}&lt;/m:KodEkdPracodaw>
else ()
}
{
if ($fml/fml:CI_UBEZP_NA_ZYCIE) then
&lt;m:LifeInsurerName>{data($fml/fml:CI_UBEZP_NA_ZYCIE)}&lt;/m:LifeInsurerName>
else ()
}
{
if ($fml/fml:CI_UBEZP_NW) then
&lt;m:NwInsurerName>{data($fml/fml:CI_UBEZP_NW)}&lt;/m:NwInsurerName>
else ()
}
{
if ($fml/fml:CI_UBEZP_EMERYTALNE) then
&lt;m:RetireInsurerName>{data($fml/fml:CI_UBEZP_EMERYTALNE)}&lt;/m:RetireInsurerName>
else ()
}
{
if ($fml/fml:CI_UBEZP_MAJATKOWE) then
&lt;m:WealthInsurerName>{data($fml/fml:CI_UBEZP_MAJATKOWE)}&lt;/m:WealthInsurerName>
else ()
}
{
if ($fml/fml:CI_UBEZP_INNE) then
&lt;m:OtherInsurerName>{data($fml/fml:CI_UBEZP_INNE)}&lt;/m:OtherInsurerName>
else ()
}
{
if ($fml/fml:CI_DOK_TOZSAMOSCI) then
&lt;m:TypNiNo>{data($fml/fml:CI_DOK_TOZSAMOSCI)}&lt;/m:TypNiNo>
else ()
}
{
if ($fml/fml:DC_KIERUNEK_STUDIOW) then
&lt;m:KierunekStudiow>{data($fml/fml:DC_KIERUNEK_STUDIOW)}&lt;/m:KierunekStudiow>
else ()
}
{
if ($fml/fml:CI_KAPITAL_ZALOZ_OPLACONY) then
&lt;m:KapitalZalozOplacony>{data($fml/fml:CI_KAPITAL_ZALOZ_OPLACONY)}&lt;/m:KapitalZalozOplacony>
else ()
}
{
if ($fml/fml:DC_PRACOWNIK_BANKU) then
&lt;m:BankRelative>{data($fml/fml:DC_PRACOWNIK_BANKU)}&lt;/m:BankRelative>
else ()
}
&lt;/m:CRMGetProspFolderResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetProspFolderResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>