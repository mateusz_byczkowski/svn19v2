<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMProspToCustRequest($req as element(m:CRMProspToCustRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:EmpId)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:EmpId) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:CompanyId)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:CompanyId) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:ProspId)
					then &lt;fml:CI_NUMER_KLIENTA>{ data($req/m:ProspId) }&lt;/fml:CI_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:CustId)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:CustId) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMProspToCustRequest($body/m:CRMProspToCustRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>