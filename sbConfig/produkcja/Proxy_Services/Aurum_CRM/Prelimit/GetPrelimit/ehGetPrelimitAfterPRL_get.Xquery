<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $fault external;

(: &lt;soap-env:Body> :)
&lt;soap-env:Body>
{
  let $reason := fn:substring-after(fn:substring-before(data($fault/ctx:reason), "):"), "(")
  let $urcode := fn:substring-before(fn:substring-after(fn:substring-after(data($fault/ctx:reason),":"),":"),":")

  return
       if($reason = "11") then
           &lt;no_PRL_get_data>{ if ($urcode = "103") then "1" else "0"}&lt;/no_PRL_get_data>
       else
           &lt;no_PRL_get_data>"0"&lt;/no_PRL_get_data>,

      &lt;tux_err_reason>{data($fault/ctx:reason)}&lt;/tux_err_reason>
}
&lt;/soap-env:Body>

(: &lt;/soap-env:Body> :)</con:xquery>
</con:xqueryEntry>