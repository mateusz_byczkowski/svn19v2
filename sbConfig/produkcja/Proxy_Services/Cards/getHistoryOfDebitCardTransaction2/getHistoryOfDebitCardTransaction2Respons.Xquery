<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDateTime($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function getElementsForHistory($parm as element(fml:FML32)) as element()
{

&lt;ns4:history>
  {
    for $x at $occ in $parm/NF_TRANHC_TRANCODE
    return
    &lt;ns5:TransactionHistoryCard>
      {insertDateTime(data($parm/NF_TRANHC_TRANDATE[$occ]),"yyyy-MM-dd-HH.mm.ss","ns5:tranDate")}
      &lt;ns5:tranCode?>{data($parm/NF_TRANHC_TRANCODE[$occ])}&lt;/ns5:tranCode>
      &lt;ns5:tranPlace?>{data($parm/NF_TRANHC_TRANPLACE[$occ])}&lt;/ns5:tranPlace>
      &lt;ns5:device?>{data($parm/NF_TRANHC_DEVICE[$occ])}&lt;/ns5:device>
      &lt;ns5:tranPlaceID?>{data($parm/NF_TRANHC_TRANPLACEID[$occ])}&lt;/ns5:tranPlaceID>
      &lt;ns5:tranID?>{data($parm/NF_TRANHC_TRANID[$occ])}&lt;/ns5:tranID>
      &lt;ns5:aquaierID?>{data($parm/NF_TRANHC_AQUAIERID[$occ])}&lt;/ns5:aquaierID>
      &lt;ns5:before?>{data($parm/NF_TRANHC_BEFORE[$occ])}&lt;/ns5:before>
      &lt;ns5:past?>{data($parm/NF_TRANHC_PAST[$occ])}&lt;/ns5:past>
      &lt;ns5:cardFeeTran?>{data($parm/NF_TRANHC_CARDFEETRAN[$occ])}&lt;/ns5:cardFeeTran>
      &lt;ns5:availableBalance?>{data($parm/NF_TRANHC_AVAILABLEBALANCE[$occ])}&lt;/ns5:availableBalance>
      &lt;ns5:tranTitle?>{data($parm/NF_TRANHC_TRANTITLE[$occ])}&lt;/ns5:tranTitle>
      &lt;ns5:tranAmount?>{data($parm/NF_TRANHC_TRANAMOUNT[$occ])}&lt;/ns5:tranAmount>
    &lt;/ns5:TransactionHistoryCard>
  }
&lt;/ns4:history>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  {getElementsForHistory($parm)}
  &lt;ns4:response>
    &lt;ns0:ResponseMessage>
      &lt;ns0:result?>{data($parm/NF_RESPOM_RESULT)}&lt;/ns0:result>
      &lt;ns0:errorCode?>{data($parm/NF_RESPOM_ERRORCODE)}&lt;/ns0:errorCode>
      &lt;ns0:errorDescription?>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}&lt;/ns0:errorDescription>
    &lt;/ns0:ResponseMessage>
  &lt;/ns4:response>
  &lt;ns4:bcd>
    &lt;ns2:BusinessControlData>
      &lt;ns2:pageControl>
        &lt;ns6:PageControl>
          &lt;ns6:hasNext?>{data($parm/NF_PAGEC_HASNEXT)}&lt;/ns6:hasNext>
          &lt;ns6:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition>
          &lt;ns6:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue>
        &lt;/ns6:PageControl>
      &lt;/ns2:pageControl>
    &lt;/ns2:BusinessControlData>
  &lt;/ns4:bcd>
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>