<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace ns4="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element(ns3:invokeResponse)
{
&lt;ns3:invokeResponse>
  &lt;ns3:response>
    &lt;ns0:ResponseMessage>
       {
        if (data($parm/DC_OPIS_BLEDU)) then
          &lt;ns0:result?>false&lt;/ns0:result>
        else
          &lt;ns0:result?>true&lt;/ns0:result>
        }
       &lt;ns0:errorDescription?>{data($parm/DC_OPIS_BLEDU)}&lt;/ns0:errorDescription>
    &lt;/ns0:ResponseMessage>
  &lt;/ns3:response>
&lt;/ns3:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>