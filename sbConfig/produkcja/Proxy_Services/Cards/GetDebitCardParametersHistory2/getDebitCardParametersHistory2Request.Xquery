<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:card.entities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns6="urn:filtersandmessages.entities.be.dcl";
declare namespace ns9="urn:errors.hlbsentities.be.dcl";
declare namespace ns8="urn:crddict.dictionaries.be.dcl";
declare namespace ns7="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

&lt;NF_DEBITC_VIRTUALCARDNBR?>{data($parm/ns0:debitCard/ns2:DebitCard/ns2:virtualCardNbr)}&lt;/NF_DEBITC_VIRTUALCARDNBR>
,
&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
&lt;NF_PAGEC_REVERSEORDER?>{boolean2SourceValue(data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:reverseOrder),"1","0")}&lt;/NF_PAGEC_REVERSEORDER>
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
,

if (data($parm/ns0:filter/ns6:FilterChgParamCardHistory/ns6:fromDateFilter))
	then &lt;NF_FICPCH_FROMDATEFILTER>{fn-bea:date-to-string-with-format("yyyy-MM-dd",data($parm/ns0:filter/ns6:FilterChgParamCardHistory/ns6:fromDateFilter))}&lt;/NF_FICPCH_FROMDATEFILTER>
	else &lt;NF_FICPCH_FROMDATEFILTER>0001-01-01&lt;/NF_FICPCH_FROMDATEFILTER>
,
if (data($parm/ns0:filter/ns6:FilterChgParamCardHistory/ns6:toDateFilter))
	then &lt;NF_FICPCH_TODATEFILTER>{fn-bea:date-to-string-with-format("yyyy-MM-dd",data($parm/ns0:filter/ns6:FilterChgParamCardHistory/ns6:toDateFilter))}&lt;/NF_FICPCH_TODATEFILTER>
	else &lt;NF_FICPCH_TODATEFILTER>2099-12-31&lt;/NF_FICPCH_TODATEFILTER>
,
if (data($parm/ns0:filter/ns6:FilterChgParamCardHistory/ns6:changeTypeFilter/ns8:CrdChangeType/ns8:crdChangeType))
	then &lt;NF_CRDCT_CRDCHANGETYPE>{data($parm/ns0:filter/ns6:FilterChgParamCardHistory/ns6:changeTypeFilter/ns8:CrdChangeType/ns8:crdChangeType)}&lt;/NF_CRDCT_CRDCHANGETYPE>
	else &lt;NF_CRDCT_CRDCHANGETYPE> &lt;/NF_CRDCT_CRDCHANGETYPE>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
    &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>