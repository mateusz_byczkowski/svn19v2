<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace prim = "http://bzwbk.com/services/prime/";
declare namespace xf="http://bzwbk.com/services/prime/functions";
declare namespace fml = "";
declare namespace none = "";
declare namespace fault="http://schemas.datacontract.org/2004/07/LpfAdjustmentLibrary.Structures";
declare namespace rsp="http://schemas.datacontract.org/2004/07/LpfAdjustmentLibrary.Structures";

declare variable $errCode2 external;
declare variable $errDesc external;

declare function xf:concatError($errCode2 as xs:string, $errDesc as xs:string) as xs:string
{
        fn:concat ($errCode2, " - ", $errDesc)
};

declare function xf:addLeadingZero($variable as xs:integer) as xs:string
{
if ($variable &lt; 10)
then fn:concat ("0", fn:string($variable))
else fn:string($variable)
};

declare function xf:currDateRRRRMMDD() as xs:string
{
let $currentDate := fn:current-date()
let $currYear := fn:year-from-date($currentDate)
let $currMonth := fn:month-from-date($currentDate)
let $currDay := fn:day-from-date($currentDate)

return fn:concat (xf:addLeadingZero($currYear), xf:addLeadingZero($currMonth), xf:addLeadingZero($currDay))
};

declare function xf:map_LpfAdjustmentResponse($resp as element (prim:LpfAdjustmentResponse))
	as element (fml:FML32) {

&lt;fml:FML32>
	{
		&lt;fml:B_DATA_AKCJI>{data ($resp/prim:LpfAdjustmentResult/rsp:ActionDate)}&lt;/fml:B_DATA_AKCJI>,
		&lt;fml:B_STATUS>{data ($resp/prim:LpfAdjustmentResult/rsp:ActionStatus)}&lt;/fml:B_STATUS>,
		&lt;fml:B_OPIS_BLEDU>{data ($resp/prim:LpfAdjustmentResult/rsp:ErrorCode)}&lt;/fml:B_OPIS_BLEDU>
	}
&lt;/fml:FML32>
};


declare function xf:mapFault($fau as element(soap-env:Fault))
	as element (fml:FML32) {

	&lt;fml:FML32>
		&lt;fml:B_DATA_AKCJI>{ xf:currDateRRRRMMDD() }&lt;/fml:B_DATA_AKCJI>
		&lt;fml:B_STATUS> { "1" } &lt;/fml:B_STATUS>
		&lt;fml:B_OPIS_BLEDU> { xf:concatError(data($fau/detail/fault:WbkFault/fault:ErrorCode2), data($fau/detail/fault:WbkFault/fault:ErrorDescription)) } &lt;/fml:B_OPIS_BLEDU>
	&lt;/fml:FML32>
};



declare variable $body as element(soap-env:Body) external;


&lt;soap-env:Body>
{
	if (boolean($body/prim:LpfAdjustmentResponse)) then (
 		xf:map_LpfAdjustmentResponse($body/prim:LpfAdjustmentResponse)
 	) else if (boolean($body/soap-env:Fault/detail/fault:WbkFault/fault:ErrorCode2)) then (
 		xf:mapFault($body/soap-env:Fault)
	) else ()
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>