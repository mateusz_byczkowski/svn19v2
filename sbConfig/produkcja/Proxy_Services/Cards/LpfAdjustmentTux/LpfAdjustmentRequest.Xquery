<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/prime/";
declare namespace lpf= "http://schemas.datacontract.org/2004/07/LpfAdjustmentLibrary.Structures";



declare function xf:map_GetCustomerDetailsRequest($fml as element(fml:FML32))
	as element(urn:input) {
               		&lt;urn:input>
			&lt;lpf:AccNumber>{ data($fml/fml:B_DL_NR_RACH)}&lt;/lpf:AccNumber>
                                                &lt;lpf:DueDate>{ data($fml/fml:B_D_OTWARCIA)}&lt;/lpf:DueDate>
                                                &lt;lpf:CreateDate>{ data($fml/fml:B_DATA_REJ)}&lt;/lpf:CreateDate>
		&lt;/urn:input>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
&lt;urn:LpfAdjustment>
{ xf:map_GetCustomerDetailsRequest($body/fml:FML32) }
&lt;/urn:LpfAdjustment>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>