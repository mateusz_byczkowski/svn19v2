<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/PregenPinmessages/";
declare namespace xf = "http://bzwbk.com/services/PregenPinmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPregenPinResponse($fml as element(fml:FML32))
	as element(m:PregenPinResponse) {
		&lt;m:PregenPinResponse>
		&lt;/m:PregenPinResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapPregenPinResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>