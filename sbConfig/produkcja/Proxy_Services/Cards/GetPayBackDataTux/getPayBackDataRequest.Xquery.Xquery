<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Zamiana FML na XML do wywołania WebserviceVersion.$1.2010-09-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/prime/";



declare function xf:map_getPayBackDataRequest($fml as element(fml:FML32))
	as element(urn:GetPayBackData) {
		&lt;urn:GetPayBackData>
			&lt;urn:strPBId2>{data($fml/fml:DC_NR_PB2)}&lt;/urn:strPBId2>
			&lt;urn:strAppSource>{ data($fml/fml:DC_ZRODLO)}&lt;/urn:strAppSource>
			&lt;urn:poolId>0&lt;/urn:poolId>
		&lt;/urn:GetPayBackData>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_getPayBackDataRequest($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>