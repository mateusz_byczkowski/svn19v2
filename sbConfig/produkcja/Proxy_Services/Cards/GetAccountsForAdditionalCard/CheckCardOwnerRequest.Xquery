<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace prim="http://bzwbk.com/services/prime/";

declare function local:lpad0($stringToPad as xs:string?, $len as xs:integer) as xs:string 
{
   fn:concat((for $i in 1 to $len - fn:string-length($stringToPad) return '0'), $stringToPad)
};

declare variable $cif as xs:string external;
declare variable $accNumber as xs:string external;

&lt;prim:CheckCardOwner>
   &lt;prim:docType>A&lt;/prim:docType>
   &lt;prim:docNumber>{ local:lpad0($cif, 10) }&lt;/prim:docNumber>
   &lt;prim:accNumber>{ fn:substring(fn:replace($accNumber, ' ', ''), 11) }&lt;/prim:accNumber>
&lt;/prim:CheckCardOwner></con:xquery>
</con:xqueryEntry>