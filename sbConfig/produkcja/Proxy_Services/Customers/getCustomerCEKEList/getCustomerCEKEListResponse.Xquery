<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn3 = "urn:ceke.entities.be.dcl";

declare function xf:mapgetCustomerCEKEListResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		&lt;urn:invokeResponse>
			&lt;urn:customerCEKEList>

				{

					let $E_LOGIN_ID := $fml/fml:E_LOGIN_ID
					let $B_NR_DOK := $fml/fml:B_NR_DOK
					let $B_RODZ_DOK := $fml/fml:B_RODZ_DOK
					let $E_CUST_STATUS := $fml/fml:E_CUST_STATUS
					let $E_PROFILE_ID := $fml/fml:E_PROFILE_ID
					let $E_PROFILE_NAME := $fml/fml:E_PROFILE_NAME
					let $E_PACKAGE_ID := $fml/fml:E_PACKAGE_ID
					let $E_PACKAGE_NAME := $fml/fml:E_PACKAGE_NAME
					let $E_VIEW_DET_ALLOWED := $fml/fml:E_VIEW_DET_ALLOWED
					let $E_CLOSE_CEKE_ALLOWED := $fml/fml:E_CLOSE_CEKE_ALLOWED
					let $E_DETACH_PRODUCT_ALLOWED := $fml/fml:E_DETACH_PRODUCT_ALLOWED
					let $E_PRINT_CONTRACT := $fml/fml:E_PRINT_CONTRACT

					for $it at $p in $fml/fml:E_TIME_STAMP
					return
						&lt;urn3:CustomerCEKE>

						{
							if($E_LOGIN_ID[$p])
								then &lt;urn3:nik>{ data($E_LOGIN_ID[$p]) }&lt;/urn3:nik>
							else ()
						}
						{
							if($B_NR_DOK[$p])
								then &lt;urn3:documentNumber>{ 
									if(data($B_RODZ_DOK[$p]) = "A") then								
										substring(concat('0000000000',data($B_NR_DOK[$p])),string-length(concat('0000000000',data($B_NR_DOK[$p])))-10+1,10)
									else
										 data($B_NR_DOK[$p]) 
								 }&lt;/urn3:documentNumber>
							else ()
						}
						{
							if($B_RODZ_DOK[$p])
								then (
								&lt;urn3:documentType>
									&lt;urn2:DocumentTypeCEKE>
										&lt;urn2:documentTypeCEKE>{ data($B_RODZ_DOK[$p]) }&lt;/urn2:documentTypeCEKE>
									&lt;/urn2:DocumentTypeCEKE>
								&lt;/urn3:documentType>
								)
							else ()
						}

						{
							if($E_CUST_STATUS[$p])
								then (
								&lt;urn3:customerCEKEStatus>
									&lt;urn2:CustomerStatusCEKE>
										{
											if(data($E_CUST_STATUS[$p]) = "0") then
											(
												&lt;urn2:customerStatusCEKE>1&lt;/urn2:customerStatusCEKE>
											) else if(data($E_CUST_STATUS[$p]) = "4") then
											(
												&lt;urn2:customerStatusCEKE>2&lt;/urn2:customerStatusCEKE>
											) else if(data($E_CUST_STATUS[$p]) = "5") then
											(
												&lt;urn2:customerStatusCEKE>3&lt;/urn2:customerStatusCEKE>
											) else if(data($E_CUST_STATUS[$p]) = "6") then
											(
												&lt;urn2:customerStatusCEKE>4&lt;/urn2:customerStatusCEKE>
											) else if(data($E_CUST_STATUS[$p]) = "9") then
											(
												&lt;urn2:customerStatusCEKE>5&lt;/urn2:customerStatusCEKE>
											) else
											(
												&lt;urn2:customerStatusCEKE>{data($E_CUST_STATUS[$p])}&lt;/urn2:customerStatusCEKE>
											)
										}
									&lt;/urn2:CustomerStatusCEKE>
								&lt;/urn3:customerCEKEStatus>
								)
							else ()
						}

						{
							&lt;urn3:packageCEKE>
								&lt;urn3:PackageCEKE>
									&lt;urn3:packageCEKE?>{ data($E_PACKAGE_ID[$p]) }&lt;/urn3:packageCEKE>
									&lt;urn3:description?>{ data($E_PACKAGE_NAME[$p]) }&lt;/urn3:description>
								&lt;/urn3:PackageCEKE>
							&lt;/urn3:packageCEKE>
						}

						{
							&lt;urn3:profileCEKE>
								&lt;urn3:ProfileCEKE>
									&lt;urn3:profileCEKE?>{ data($E_PROFILE_ID[$p]) }&lt;/urn3:profileCEKE>
									&lt;urn3:description?>{ data($E_PROFILE_NAME[$p]) }&lt;/urn3:description>
									{
										if ($E_VIEW_DET_ALLOWED[$p] = 0) then (
											&lt;urn3:nfeViewDetailsAllowed>false&lt;/urn3:nfeViewDetailsAllowed>
										) else if ($E_VIEW_DET_ALLOWED[$p] = 1) then (
											&lt;urn3:nfeViewDetailsAllowed>true&lt;/urn3:nfeViewDetailsAllowed>
										) else ()
									}
									{
										if ($E_CLOSE_CEKE_ALLOWED[$p] = 0) then (
											&lt;urn3:nfeCloseCEKEAllowed?>false&lt;/urn3:nfeCloseCEKEAllowed>
										) else if($E_CLOSE_CEKE_ALLOWED[$p] = 1) then (
											&lt;urn3:nfeCloseCEKEAllowed?>true&lt;/urn3:nfeCloseCEKEAllowed>
										) else ()
									}
									{
										if ($E_DETACH_PRODUCT_ALLOWED[$p] = 0) then (
											&lt;urn3:nfeDetachProductExamination?>false&lt;/urn3:nfeDetachProductExamination>
										) else if ($E_DETACH_PRODUCT_ALLOWED[$p] = 1) then (
											&lt;urn3:nfeDetachProductExamination?>true&lt;/urn3:nfeDetachProductExamination>
										) else ()
									}
									{
										if ($E_PRINT_CONTRACT[$p] = 0) then (
											&lt;urn3:nfePrintContract?>false&lt;/urn3:nfePrintContract>
										) else if ($E_PRINT_CONTRACT[$p] = 1) then (
											&lt;urn3:nfePrintContract?>true&lt;/urn3:nfePrintContract>
										) else ()
									}

								&lt;/urn3:ProfileCEKE>
							&lt;/urn3:profileCEKE>
						}

						&lt;/urn3:CustomerCEKE>
				}

			&lt;/urn:customerCEKEList>
		&lt;/urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetCustomerCEKEListResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>