<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:Change log 
v.1.1  2010-07-27  PK  T49926 Wyłączenie maskowania salda

:)

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetCustProdsForAdvisoryRequest($req as element(m:getCustProdsForAdvisoryRequest))
  as element(fml:FML32) {
    &lt;fml:FML32>
      {if($req/m:CustomCustomernumber)
        then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER>{ data($req/m:CustomCustomernumber) }&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER>
        else ()
      }
      {if($req/m:PagecPagesize)
        then &lt;fml:NF_PAGEC_PAGESIZE>{ data($req/m:PagecPagesize) }&lt;/fml:NF_PAGEC_PAGESIZE>
        else ()
      }
      {if($req/m:PagecActioncode)
        then &lt;fml:NF_PAGEC_ACTIONCODE>{ data($req/m:PagecActioncode) }&lt;/fml:NF_PAGEC_ACTIONCODE>
        else ()
      }
      {if($req/m:PagecReverseorder)
        then &lt;fml:NF_PAGEC_REVERSEORDER>{ data($req/m:PagecReverseorder) }&lt;/fml:NF_PAGEC_REVERSEORDER>
        else ()
      }
      {if($req/m:PagecNavigationkeyvalu)
        then &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU>{ data($req/m:PagecNavigationkeyvalu) }&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU>
        else ()
      }
      &lt;fml:NF_MSHEAD_COMPANYID>1&lt;/fml:NF_MSHEAD_COMPANYID>
      &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE>0&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE>
      &lt;fml:NF_PRODUG_RECEIVER>FU&lt;/fml:NF_PRODUG_RECEIVER>
      &lt;fml:NF_CTRL_AREAS>020310&lt;/fml:NF_CTRL_AREAS>
      &lt;fml:NF_ACCOUR_RELATIONSHIP>SOWJAFOWNJA1JA2JA3JA4JA5JAOJO1JO2JO3JO4JO5JOO&lt;/fml:NF_ACCOUR_RELATIONSHIP>
      &lt;fml:NF_CTRL_ACTIVENONACTIVE>1&lt;/fml:NF_CTRL_ACTIVENONACTIVE>
      &lt;fml:NF_CTRL_SHOWCURRBAL>2&lt;/fml:NF_CTRL_SHOWCURRBAL> (:1.1 dodano:) 
    &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetCustProdsForAdvisoryRequest($body/m:getCustProdsForAdvisoryRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>