<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2011-06-01</con:description>
  <con:xquery>declare namespace m = "urn:be.services.dcl";
declare namespace e1 = "urn:accounts.entities.be.dcl";
declare namespace e2 = "urn:cif.entities.be.dcl";
declare namespace e3 = "urn:baseauxentities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function local:mapsetCustomerRelationPOGRequest($req as element(), $head as element())
	as element(fml:FML32) {
		let $msgHeader:=$head/m:msgHeader
		let $transHeader := $head/m:transHeader		
		let $trnId := $transHeader/m:transId
		let $msgId := $msgHeader/m:msgId
		let $userId := $msgHeader/m:userId
		let $unitId := $msgHeader/m:unitId
		let $customerNumber := $req/m:customer/e2:Customer/e2:customerNumber
		let $accountNumber := $req/m:account/e1:Account/e1:accountNumber
		let $accountNumberLength := string-length($accountNumber)
		let $includeJointOwners := $req/m:includeJointOwners/e3:BooleanHolder/e3:value
		return				
			&lt;fml:FML32>
				&lt;fml:DC_MSHEAD_MSGID>{ data($msgId) }&lt;/fml:DC_MSHEAD_MSGID>
				&lt;fml:DC_TRN_ID>{ data($trnId) }&lt;/fml:DC_TRN_ID>
				&lt;fml:DC_UZYTKOWNIK>{concat("SKP:",data($userId))}&lt;/fml:DC_UZYTKOWNIK>
				&lt;fml:DC_ODDZIAL>{ chkUnitId(data($unitId)) }&lt;/fml:DC_ODDZIAL>
				&lt;fml:DC_NUMER_KLIENTA>{ data($customerNumber) }&lt;/fml:DC_NUMER_KLIENTA>
                                 {
                                 if ($accountNumberLength >  12) 
                                      then  &lt;fml:DC_NR_RACHUNKU>{substring(data($accountNumber),$accountNumberLength - 9,10) }&lt;/fml:DC_NR_RACHUNKU>
                                      else   &lt;fml:DC_NR_RACHUNKU>{data($accountNumber) }&lt;/fml:DC_NR_RACHUNKU>
                                 }
                                &lt;fml:DC_KOD_WLASCICIELA>{boolean2SourceValue(data($includeJointOwners),'1','0')}&lt;/fml:DC_KOD_WLASCICIELA>         
			&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

&lt;soap-env:Body>
{ local:mapsetCustomerRelationPOGRequest($body/m:invoke, $header/m:header) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>