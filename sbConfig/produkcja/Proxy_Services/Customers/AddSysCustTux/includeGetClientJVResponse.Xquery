<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace wsdl="http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace xf="http://jv.channel.cu.com.pl/cmf/functions";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $getClientJVResponse external;
declare variable $invocationBody external;
declare variable $timestamp external;

(: mapowanie zgody na przetwarzanie danych :)
declare function xf:mapGroupAgreement($groupAgreement)
{
	if (data($groupAgreement) = '2') then
		'2'
	else if (data($groupAgreement) = '4') then
		'0'
	else ()
};

(: mapowanie zgody na przekazywanie informacji handlowej :)
declare function xf:mapCorespelAgreement($corespelAgreement)
{
	if (data($corespelAgreement) = '2') then
		'1'
	else if (data($corespelAgreement) = '4') then
		'0'
	else ()
};

(: mapowanie zgody na przekazywanie tajemnicy bankowej :)
declare function xf:mapBankSecretAgreement($bankSecret)
{
	if (data($bankSecret) = '2') then
		'1'
	else if (data($bankSecret) = '4') then
		'0'
	else ()
};

(:mapowanie rodzaju zgody :)
declare function xf:mapAgreement($agreement as element()*) as element()*
{
	if (data($agreement/name) = 'PMP'
	and ((data($agreement/status) = '2') or (data($agreement/status) = '4'))) then
		&lt;CI_UDOSTEP_GRUPA>{ xf:mapGroupAgreement($agreement/status) }&lt;/CI_UDOSTEP_GRUPA>
	else if (data($agreement/name) = 'RCI'
	and ((data($agreement/status) = '2') or (data($agreement/status) = '4'))) then
		&lt;CI_ZGODA_INF_HANDLOWA>{ xf:mapCorespelAgreement($agreement/status) }&lt;/CI_ZGODA_INF_HANDLOWA>
	else if (data($agreement/name) = 'DBS'
	and ((data($agreement/status) = '2') or (data($agreement/status) = '4'))) then
		&lt;CI_ZGODA_TAJEM_BANK>{ xf:mapBankSecretAgreement($agreement/status) }&lt;/CI_ZGODA_TAJEM_BANK>
	else ()
};

(: mapowanie identyfikatora spolki :)
declare function xf:mapFactory($factory as xs:string) as element()*
{
	if (data($factory) = 'JVL') then
		&lt;CI_ID_SPOLKI>7&lt;/CI_ID_SPOLKI>
	else if (data($factory) = 'JVG') then
		&lt;CI_ID_SPOLKI>8&lt;/CI_ID_SPOLKI>
	else ()
};

(: mapowanie daty lub timestampa z formatu YYYY-MM-DD(T00:00:00) na DD-MM-YYYY :)
declare function xf:parseDate($dateString as xs:string)
{
	fn:string-join((fn:substring($dateString, 9, 2),
					'-',
					fn:substring($dateString, 6, 2),
					'-',
					fn:substring($dateString, 1, 4)),
					'')
};

(: obraz klienta przeksztalcony w bufor dla WTC, postac:
	&lt;FML32>
		&lt;pole_1>x_1&lt;/pole1>

		...

		&lt;pole_n>x_n&lt;/poleN>
	&lt;/FML32>
:)
&lt;soap-env:Body>
	&lt;fml:FML32>
		(: dane z faulta z addClientJV :)
		&lt;fml:CI_KOD_BLEDU>{ data($body/fml:FML32/fml:CI_KOD_BLEDU) }&lt;/fml:CI_KOD_BLEDU>
		&lt;fml:DC_OPIS_BLEDU> { data($body/fml:FML32/fml:DC_OPIS_BLEDU) } &lt;/fml:DC_OPIS_BLEDU>


		(: timestamp, nieobslugiwany po stronie Streamline'a :)
		&lt;fml:CI_CZAS_AKTUALIZACJI>{ data($timestamp) }&lt;/fml:CI_CZAS_AKTUALIZACJI>
		&lt;fml:CI_TIMESTAMP_CIS>{ data($timestamp) }&lt;/fml:CI_TIMESTAMP_CIS>

		(: dane z requesta dla addClientJV, ktorych nie zwraca usluga getClientJV :)
		&lt;fml:CI_DATA_WPROWADZENIA>{ xf:parseDate($invocationBody/wsdl:addClientJVRequest/wsdl:envelope/wsdl:request-time) }&lt;/fml:CI_DATA_WPROWADZENIA>
		{
			if ($invocationBody/wsdl:addClientJVRequest/wsdl:envelope/wsdl:request-no) then
				&lt;fml:DC_TRN_ID>{ data($invocationBody/wsdl:addClientJVRequest/wsdl:envelope/wsdl:request-no) }&lt;/fml:DC_TRN_ID>
			else ()
		}

		(: &lt;wsdl:source-code> pominiety - domyslnie w addClientJVRequest wpisywana jest wartosc 'placowka banku',
						  niewykorzystywany natomiast po stronie CISa:)

		{
			if ($invocationBody/wsdl:addClientJVRequest/wsdl:envelope/wsdl:branch-id) then
				&lt;fml:DC_ODDZIAL>{ data($invocationBody/wsdl:addClientJVRequest/wsdl:envelope/wsdl:branch-id) }&lt;/fml:DC_ODDZIAL>
			else ()
		}
		{
			if ($invocationBody/wsdl:addClientJVRequest/wsdl:envelope/wsdl:user-id) then
				&lt;fml:CI_PRACOWNIK_WPROW>{ data($invocationBody/wsdl:addClientJVRequest/wsdl:envelope/wsdl:user-id) }&lt;/fml:CI_PRACOWNIK_WPROW>
			else ()
		}


		(: obraz klienta z systemu zrodlowego na podstawie wywolania getClientJV :)
		&lt;fml:CI_NR_ZRODLOWY_KLIENTA>{ data($getClientJVResponse/customer/@client-code) }&lt;/fml:CI_NR_ZRODLOWY_KLIENTA>
		&lt;fml:DC_NUMER_KLIENTA>{ data($getClientJVResponse/customer/@client-code) }&lt;/fml:DC_NUMER_KLIENTA>
		{
			let $agreement := $getClientJVResponse/customer/agreements/agreement
			for $it at $p in $getClientJVResponse/customer/agreements/agreement
			return
				xf:mapAgreement($agreement[$p])
		}
		{
			let $factory := $getClientJVResponse/customer/factories/factory
			for $it at $p in $getClientJVResponse/customer/factories/factory
			return
				xf:mapFactory($factory[$p])
		}
		{
			if ((data($getClientJVResponse/customer/company-ind) = '0')
			or  (data($getClientJVResponse/customer/company-ind) = '1')) then
				&lt;fml:DC_TYP_KLIENTA>
				{ 
					if (data($getClientJVResponse/customer/company-ind) = '0') then
						'F'
					else if (data($getClientJVResponse/customer/company-ind) = '1') then
						'P'
					else ()
				}
				&lt;/fml:DC_TYP_KLIENTA>
			else ()
		}
		{
			if ($getClientJVResponse/customer/jv-data/giodo-status) then
				&lt;fml:CI_STATUS_GIODO>{ data($getClientJVResponse/customer/jv-data/giodo-status) }&lt;/fml:CI_STATUS_GIODO>
			else ()
		}
		{
			if ($getClientJVResponse/customer/jv-data/branch-id) then
				&lt;fml:CI_NR_ODDZIALU>{ data($getClientJVResponse/customer/jv-data/branch-id) }&lt;/fml:CI_NR_ODDZIALU>
			else ()
		}
		{
			if ($getClientJVResponse/jv-data/facsimile-signature) then
				&lt;fml:DC_KARTA_WZOROW_PODPISOW>{ data($getClientJVResponse/customer/jv-data/facsimile-signature) }&lt;/fml:DC_KARTA_WZOROW_PODPISOW>
			else ()
		}
		{
			if ($getClientJVResponse/jv-data/language-code) then
				&lt;fml:DC_KOD_JEZYKA>{ data($getClientJVResponse/customer/jv-data/language-code) }&lt;/fml:DC_KOD_JEZYKA>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/name/first-name) then
				&lt;fml:DC_IMIE>{ data($getClientJVResponse/customer/person/name/first-name) }&lt;/fml:DC_IMIE>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/name/second-name) then
				&lt;fml:DC_DRUGIE_IMIE>{ data($getClientJVResponse/customer/person/name/second-name) }&lt;/fml:DC_DRUGIE_IMIE>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/name/last-name) then
				&lt;fml:DC_NAZWISKO>{ data($getClientJVResponse/customer/person/name/last-name) }&lt;/fml:DC_NAZWISKO>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/dob) then
				&lt;fml:DC_DATA_URODZENIA>{ xf:parseDate($getClientJVResponse/customer/person/dob) }&lt;/fml:DC_DATA_URODZENIA>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/pob) then
				&lt;fml:DC_MIEJSCE_URODZENIA>{ data($getClientJVResponse/customer/person/pob) }&lt;/fml:DC_MIEJSCE_URODZENIA>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/sex) then
				&lt;fml:DC_PLEC>{ data($getClientJVResponse/customer/person/sex) }&lt;/fml:DC_PLEC>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/citizenship) then
				&lt;fml:CI_OBYWATELSTWO>{ data($getClientJVResponse/customer/person/citizenship) }&lt;/fml:CI_OBYWATELSTWO>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/country) then
				&lt;fml:CI_KOD_KRAJU>{ data($getClientJVResponse/customer/person/country) }&lt;/fml:CI_KOD_KRAJU>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/identifiers/pesel) then
				&lt;fml:DC_NR_PESEL>{ data($getClientJVResponse/customer/person/identifiers/pesel) }&lt;/fml:DC_NR_PESEL>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/identifiers/pid) then
				&lt;fml:DC_NR_DOWODU_REGON>{ data($getClientJVResponse/customer/person/identifiers/pid) }&lt;/fml:DC_NR_DOWODU_REGON>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/identifiers/passport) then
				&lt;fml:DC_NUMER_PASZPORTU>{ data($getClientJVResponse/customer/person/identifiers/passport) }&lt;/fml:DC_NUMER_PASZPORTU>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/identifiers/nip) then
				&lt;fml:DC_NIP>{ data($getClientJVResponse/customer/person/identifiers/nip) }&lt;/fml:DC_NIP>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/identifiers/other-kind) then
				&lt;fml:CI_DOK_TOZSAMOSCI>{ data($getClientJVResponse/customer/person/identifiers/other-kind) }&lt;/fml:CI_DOK_TOZSAMOSCI>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/identifiers/other-number) then
				&lt;fml:CI_SERIA_NR_DOK>{ data($getClientJVResponse/customer/person/identifiers/other-number) }&lt;/fml:CI_SERIA_NR_DOK>
			else ()
		}
		{
			if ($getClientJVResponse/customer/person/email) then
				&lt;fml:DC_ADRES_E_MAIL>{ data($getClientJVResponse/customer/person/email) }&lt;/fml:DC_ADRES_E_MAIL>
			else ()
		}
		{
			let $address := $getClientJVResponse/customer/person/address-list/address
			for $it at $p in $getClientJVResponse/customer/person/address-list/address
			return
				if (data($address[$p]/@kind) = '1') then
					&lt;fml:DC_ULICA_DANE_PODST>{ data($address[$p]/street) }&lt;/fml:DC_ULICA_DANE_PODST>
				else if (data($address[$p]/@kind) = '4') then
					&lt;fml:DC_ULICA_ADRES_ALT>{ data($address[$p]/street) }&lt;/fml:DC_ULICA_ADRES_ALT>
				else ()
		}
		{
			let $address := $getClientJVResponse/customer/person/address-list/address
			for $it at $p in $getClientJVResponse/customer/person/address-list/address
			return
				if (data($address[$p]/@kind) = '1') then
					&lt;fml:DC_NR_POSES_LOKALU_DANE_PODST>{ data($address[$p]/home) }&lt;/fml:DC_NR_POSES_LOKALU_DANE_PODST>
				else if (data($address[$p]/@kind) = '4') then
					&lt;fml:DC_NR_POSES_LOKALU_ADRES_ALT>{ data($address[$p]/home) }&lt;/fml:DC_NR_POSES_LOKALU_ADRES_ALT>
				else ()
		}
		{
			let $address := $getClientJVResponse/customer/person/address-list/address
			for $it at $p in $getClientJVResponse/customer/person/address-list/address
			return
				if (data($address[$p]/@kind) = '1') then
					&lt;fml:DC_KOD_POCZTOWY_DANE_PODST>{ data($address[$p]/postal-code) }&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST>
				else if (data($address[$p]/@kind) = '4') then
					&lt;fml:DC_KOD_POCZTOWY_ADRES_ALT>{ data($address[$p]/postal-code) }&lt;/fml:DC_KOD_POCZTOWY_ADRES_ALT>
				else ()
		}
		{
			let $address := $getClientJVResponse/customer/person/address-list/address
			for $it at $p in $getClientJVResponse/customer/person/address-list/address
			return
				if (data($address[$p]/@kind) = '1') then
					&lt;fml:DC_MIASTO_DANE_PODST>{ data($address[$p]/city) }&lt;/fml:DC_MIASTO_DANE_PODST>
				else if (data($address[$p]/@kind) = '4') then
					&lt;fml:DC_MIASTO_ADRES_ALT>{ data($address[$p]/city) }&lt;/fml:DC_MIASTO_ADRES_ALT>
				else ()
		}
		{
			let $address := $getClientJVResponse/customer/person/address-list/address
			for $it at $p in $getClientJVResponse/customer/person/address-list/address
			return
				if (data($address[$p]/@kind) = '1') then
					&lt;fml:CI_KOD_KRAJU>{ data($address[$p]/country) }&lt;/fml:CI_KOD_KRAJU>
				else if (data($address[$p]/@kind) = '4') then
					&lt;fml:CI_KOD_KRAJU_KORESP>{ data($address[$p]/country) }&lt;/fml:CI_KOD_KRAJU_KORESP>
				else ()
		}
		{
			let $address := $getClientJVResponse/customer/person/address-list/address
			for $it at $p in $getClientJVResponse/customer/person/address-list/address
			return
				if (data($address[$p]/@kind) = '1') then
					&lt;fml:DC_WOJ_KRAJ_DANE_PODST>{ data($address[$p]/county) }&lt;/fml:DC_WOJ_KRAJ_DANE_PODST>
				else if (data($address[$p]/@kind) = '4') then
					&lt;fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{ data($address[$p]/county) }&lt;/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
				else ()
		}
		{
			let $phone := $getClientJVResponse/customer/person/phone-list/phone
			for $it at $p in $getClientJVResponse/customer/person/phone-list/phone
			return
				if (data($phone[$p]/@kind) = '1') then		(: telefon stacjonarny :)
					&lt;fml:DC_NR_TELEFONU>{ data($phone[$p]/phone-no) }&lt;/fml:DC_NR_TELEFONU>
				else if (data($phone[$p]/@kind) = '6') then		(: telefon komorkowy :)
					&lt;fml:DC_NR_TELEF_KOMORKOWEGO>{ data($phone[$p]/phone-no) }&lt;/fml:DC_NR_TELEF_KOMORKOWEGO>
				else if (data($phone[$p]/@kind) = '2') then		(: telefon sluzbowy :)
					&lt;fml:DC_NUMER_TELEFONU_PRACA>{ data($phone[$p]/phone-no) }&lt;/fml:DC_NUMER_TELEFONU_PRACA>
				else ()
		}
		{
			if ((data($getClientJVResponse/customer/person/resident) = 'true')
			or  (data($getClientJVResponse/customer/preson/resident) = 'false'))then
				&lt;fml:DC_REZ_NIEREZ>
				{
					if (data($getClientJVResponse/customer/person/resident) = 'true') then
						'1'
					else
						'0'
				}
				&lt;/fml:DC_REZ_NIEREZ>
			else ()
		}
	&lt;/fml:FML32>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>