<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/CustCopy/RequestTransform2/";
declare namespace ns0 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns-1 = "urn:cif.entities.be.dcl";
declare namespace ns1 = "urn:basedictionaries.be.dcl";
declare namespace ns3 = "urn:entities.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns5 = "urn:baseauxentities.be.dcl";
declare namespace ns6 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace ns7 = "urn:cifdict.dictionaries.be.dcl";

declare function local:booleanToInt($tn as xs:boolean?) as xs:integer {
  if($tn) then 1
  else 0
};
declare function local:booleanToString($tn as xs:boolean?) as xs:string {
  if($tn) then '1'
  else '0'
};
declare function local:YMDToDMY($dateYMD as xs:string?) as xs:string {
   if($dateYMD) then
	   if ($dateYMD eq '') then ''
	   else fn:string-join((fn:substring($dateYMD, 9, 2), fn:substring($dateYMD, 6, 2),fn:substring($dateYMD, 1, 4)), '-')
   else ''
};

declare function xf:RequestTransform2($invoke1 as element(ns4:invoke), $header1 as element(ns4:header))
    as element(ns0:pcCustCopyRequest) {
        &lt;ns0:pcCustCopyRequest>
            {
                for $customerNumber in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerNumber
                return
                    &lt;ns0:NumerKlienta>{ data($customerNumber) }&lt;/ns0:NumerKlienta>
            }
            {
                for $userID in $header1/ns4:msgHeader/ns4:userId
                return
                    &lt;ns0:PracownikZmien>{ data($userID) }&lt;/ns0:PracownikZmien>
            }
            {
                for $companyType in $invoke1/ns4:customer/ns-1:Customer/ns-1:companyID/ns2:CompanyType/ns2:companyType
                return
                    &lt;ns0:IdSpolki>{ xs:short( data($companyType) ) }&lt;/ns0:IdSpolki>
            }
            {
                for $companyType in $invoke1/ns4:companyIdDest/ns-1:CustomerFolders/ns-1:companyID/ns2:CompanyType/ns2:companyType
                return
                    &lt;ns0:IdSpolki>{ xs:short( data($companyType) ) }&lt;/ns0:IdSpolki>
            }
            {
                for $value in $invoke1/ns4:CIopttion/ns5:StringHolder/ns5:value
                return
                    &lt;ns0:Opcja>{ xs:short( data($value) ) }&lt;/ns0:Opcja>
            }
            {
                for $agreementMarketing in $invoke1/ns4:customer/ns-1:Customer/ns-1:processingApproval/ns2:CustomerProcessingApproval/ns2:customerProcessingApproval
                return
                    &lt;ns0:UdostepGrupa>{ data($agreementMarketing) }&lt;/ns0:UdostepGrupa>
            }
            {
                for $dateAgreementMarketing in $invoke1/ns4:dateOfAgreement/ns5:DateHolder/ns5:value
                return
                    &lt;ns0:DataUdostepGrupa>{ local:YMDToDMY( data($dateAgreementMarketing) ) }&lt;/ns0:DataUdostepGrupa>
            }
            {
                for $agreementEmailMarketing in $invoke1/ns4:customer/ns-1:Customer/ns-1:agreementCode/ns7:CustomerAgreementCode/ns7:customerAgreementCode
                return
                    &lt;ns0:ZgodaInfHandlowa>{ data($agreementEmailMarketing) }&lt;/ns0:ZgodaInfHandlowa>
            }
            {
                for $dateAgreementEmailMarketing in $invoke1/ns4:dateOfAgreement/ns5:DateHolder/ns5:value
                return
                    &lt;ns0:DataZgodyInfHandlowej>{ local:YMDToDMY( data($dateAgreementEmailMarketing) ) }&lt;/ns0:DataZgodyInfHandlowej>
            }
            {
                for $resident in $invoke1/ns4:customer/ns-1:Customer/ns-1:resident
                return
                    &lt;ns0:RezNierez>{ local:booleanToInt( data($resident) ) }&lt;/ns0:RezNierez>
            }
            {
                for $isoCountryCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:countryOfOrigin/ns2:CountryCode/ns2:isoCountryCode
                return
                    &lt;ns0:KrajPochodzenia>{ data($isoCountryCode) }&lt;/ns0:KrajPochodzenia>
            }
            {
                for $email in $invoke1/ns4:customer/ns-1:Customer/ns-1:email
                return
                    &lt;ns0:AdresEMail>{ data($email) }&lt;/ns0:AdresEMail>
            }
            {
                for $phoneNo in $invoke1/ns4:customer/ns-1:Customer/ns-1:phoneNo
                return
                    &lt;ns0:NrTelefonu>{ data($phoneNo) }&lt;/ns0:NrTelefonu>
            }
            {
                for $mobileNo in $invoke1/ns4:customer/ns-1:Customer/ns-1:mobileNo
                return
                    &lt;ns0:NrTelefKomorkowego>{ data($mobileNo) }&lt;/ns0:NrTelefKomorkowego>
            }
            {
                for $businessNo in $invoke1/ns4:customer/ns-1:Customer/ns-1:businessNo
                return
                    &lt;ns0:NumerTelefonuPraca>{ data($businessNo) }&lt;/ns0:NumerTelefonuPraca>
            }
            {
                for $branchCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:signatureCardBranchId/ns2:BranchCode/ns2:branchCode
                return
                    &lt;ns0:KartaWzorowPodpisow>{ data($branchCode) }&lt;/ns0:KartaWzorowPodpisow>
            }
            {
                for $branchCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:branchOfOwnership/ns2:BranchCode/ns2:branchCode
                return
                    &lt;ns0:NrOddzialu>{ xs:long( data($branchCode) ) }&lt;/ns0:NrOddzialu>
            }
            {
                for $dataSource in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerAdditionalInfo/ns-1:CustomerAdditionalInfo/ns-1:dataSource/ns2:DataSource/ns2:dataSource
                return
                    &lt;ns0:ZrodloDanych>{ data($dataSource) }&lt;/ns0:ZrodloDanych>
            }
            {
                for $agreementBankConfInfo in $invoke1/ns4:customer/ns-1:Customer/ns-1:agreementBankInfo
                return
                    &lt;ns0:ZgodaTajemBank>{ local:booleanToString( data($agreementBankConfInfo) ) }&lt;/ns0:ZgodaTajemBank>
            }
            {
                for $dateAgreementBankConfInfo in $invoke1/ns4:dateOfAgreement/ns5:DateHolder/ns5:value
                return
                    &lt;ns0:DataZgodyTajemBank>{ local:YMDToDMY( data($dateAgreementBankConfInfo) ) }&lt;/ns0:DataZgodyTajemBank>
            }
            {
                for $customerDocumentType in $invoke1/ns4:customer/ns-1:Customer/ns-1:documentList/ns-1:Document[1]/ns-1:documentType/ns2:CustomerDocumentType/ns2:customerDocumentType
                return
                    &lt;ns0:DokTozsamosci>{ data($customerDocumentType) }&lt;/ns0:DokTozsamosci>
            }
            {
                for $documentNumber in $invoke1/ns4:customer/ns-1:Customer/ns-1:documentList/ns-1:Document[1]/ns-1:documentNumber
                return
                    &lt;ns0:SeriaNrDok>{ data($documentNumber) }&lt;/ns0:SeriaNrDok>
            }
            {
                for $customerSexCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:sexCode/ns2:CustomerSexCode/ns2:customerSexCode
                return
                    &lt;ns0:Plec>{ data($customerSexCode) }&lt;/ns0:Plec>
            }
            {
            	for $motherName in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:motherMaidenName
            	return
            		&lt;ns0:NazwiskoPanienskieMatki>{ data($motherName) }&lt;/ns0:NazwiskoPanienskieMatki>
            }
            {
                for $citizenshipCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:citizenship/ns2:CitizenshipCode/ns2:isoCitizenshipCode
                return
                    &lt;ns0:Obywatelstwo>{ data($citizenshipCode) }&lt;/ns0:Obywatelstwo>
            }
            {
                for $dateOfBirth in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:dateOfBirth
                return
                    &lt;ns0:DataUrodzenia>{ local:YMDToDMY( data($dateOfBirth) ) }&lt;/ns0:DataUrodzenia>
            }
            {
                for $birth in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:birthPlace
                return
                    &lt;ns0:MiejsceUrodzenia>{ data($birth) }&lt;/ns0:MiejsceUrodzenia>
            }
            {
                for $language in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerAdditionalInfo/ns-1:CustomerAdditionalInfo/ns-1:language/ns1:Language/ns1:languageID
                return
                    &lt;ns0:KodJezyka>{ data($language) }&lt;/ns0:KodJezyka>
            }
            {
                for $customerOccupationCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerEmploymentInfoList/ns-1:CustomerEmploymentInfo[1]/ns-1:occupationCode/ns2:CustomerOccupationCode/ns2:customerOccupationCode
                return
                    &lt;ns0:KodZawoduWyuczonego>{ xs:short( data($customerOccupationCode) ) }&lt;/ns0:KodZawoduWyuczonego>
            }
            {
                for $sourceOfIncomeCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerFinancialInfo/ns-1:CustomerFinancialInfo/ns-1:sourceOfIncomeCode/ns2:SourceOfIncomeCode/ns2:sourceOfIncomeCode
                return
                    &lt;ns0:ZrodloDochodu>{ xs:string( data($sourceOfIncomeCode) ) }&lt;/ns0:ZrodloDochodu>
            }
            {
                for $policyNo in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyNo
                return
                    &lt;ns0:LiczbaPolis>{ xs:string( data($policyNo) ) }&lt;/ns0:LiczbaPolis>
            }
            {
                for $policyTotalAmount in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyTotalAmount
                return
                    &lt;ns0:SumaPolisNaZycie>{ xs:string( data($policyTotalAmount) ) }&lt;/ns0:SumaPolisNaZycie>
            }
            {
                for $policyLife in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyLife
                return
                    &lt;ns0:UbezpieczenieNaZycie>{ local:booleanToString( data($policyLife) ) }&lt;/ns0:UbezpieczenieNaZycie>
            }
            {
                for $policyOther in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyOther
                return
                    &lt;ns0:UbezpieczeniaInne>{ local:booleanToString( data($policyOther) ) }&lt;/ns0:UbezpieczeniaInne>
            }
            {
                for $policyLifeName in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyLifeName/ns6:InsurerName/ns6:insurerName
                return
                    &lt;ns0:UbezpNaZycie>{ data($policyLifeName) }&lt;/ns0:UbezpNaZycie>
            }
            {
                for $policyOtherName in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyOtherName/ns6:InsurerName/ns6:insurerName
                return
                    &lt;ns0:UbezpInne>{ data($policyOtherName) }&lt;/ns0:UbezpInne>
            }
            {
                for $policyInvest in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyInvest
                return
                    &lt;ns0:UbezpFundInwest>{ local:booleanToString( data($policyInvest) ) }&lt;/ns0:UbezpFundInwest>
            }
            {
                for $policyInvestName in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyInvestName/ns6:InsurerName/ns6:insurerName
                return
                    &lt;ns0:NazwaUbezpFundInwest>{ data($policyInvestName) }&lt;/ns0:NazwaUbezpFundInwest>
            }
            {
                for $policyPremiumSum in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerInsuranceList/ns-1:CustomerInsurance[1]/ns-1:policyPremiumSum
                return
                    &lt;ns0:SumaSkladekPolis>{ xs:string( data($policyPremiumSum) ) }&lt;/ns0:SumaSkladekPolis>
            }
            {
            	let $adresy :=
	            	for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
	            	where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
	            	return
	            		&lt;ns0:DataOd>{ local:YMDToDMY(data($adres/ns-1:validFrom)) }&lt;/ns0:DataOd>
           		return $adresy[1]
            }
            {
            	let $adresy :=
	            	for $adres in $invoke1/ns4:customer/ns-1:Customer/ns-1:addressList/ns-1:Address
	            	where $adres/ns-1:addressType/ns2:AddressType/ns2:addressType = 2
	            	return
	            		&lt;ns0:DataDo>{ local:YMDToDMY(data($adres/ns-1:validTo)) }&lt;/ns0:DataDo>
	            return $adresy[1]
            }
            {
                for $isoCountryCode in $invoke1/ns4:customer/ns-1:Customer/ns-1:customerPersonal/ns-1:CustomerPersonal/ns-1:countryOfResidence/ns2:CountryCode/ns2:isoCountryCode
                return
                    &lt;ns0:KrajZamieszkania>{ data($isoCountryCode) }&lt;/ns0:KrajZamieszkania>
            }
        &lt;/ns0:pcCustCopyRequest>
};

declare variable $header1 as element(ns4:header) external;
declare variable $invoke1 as element(ns4:invoke) external;

xf:RequestTransform2($invoke1, $header1)</con:xquery>
</con:xqueryEntry>