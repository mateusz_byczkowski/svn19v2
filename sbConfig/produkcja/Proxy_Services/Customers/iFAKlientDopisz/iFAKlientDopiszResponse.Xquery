<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/arka24/messages/";
declare namespace xf = "http://bzwbk.com/services/arka24/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare function xf:mapbARKA24iFAKlientDopiszResponse($fml as element(fml:FML32))
	as element(m:bARKA24iFAKlientDopiszResponse) {
		&lt;m:bARKA24iFAKlientDopiszResponse>
			{
				let $FA_TYP_BLEDU := $fml/fml:FA_TYP_BLEDU
				for $it at $p in $fml/fml:FA_TYP_BLEDU
				return
					&lt;m:bARKA24iFAKlientDopisz>
					{
						if($FA_TYP_BLEDU[$p])
							then &lt;m:TypBledu>{ data($FA_TYP_BLEDU[$p]) }&lt;/m:TypBledu>
						else ()
					}
					&lt;/m:bARKA24iFAKlientDopisz>
			}
		&lt;/m:bARKA24iFAKlientDopiszResponse>
};
declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbARKA24iFAKlientDopiszResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>