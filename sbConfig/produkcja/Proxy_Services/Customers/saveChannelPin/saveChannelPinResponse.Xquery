<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-15</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapsaveChannelPinResponse($fml as element(fml:FML32))
	as element(m:SaveChannelPinResponse) {
		&lt;m:SaveChannelPinResponse>
			&lt;m:Nik?>{ data($fml/fml:E_LOGIN_ID) }&lt;/m:Nik>
			&lt;m:PinDataTable>
				{
					let $channelType := $fml/fml:E_CHANNEL_TYPE
					let $status := $fml/fml:E_STATUS
					let $timestamp := $fml/fml:E_TIME_STAMP
					for $it at $p in $fml/fml:E_CHANNEL_TYPE 
					return
						&lt;m:PinData>
							&lt;m:ChannelType?>{data($channelType[$p])}&lt;/m:ChannelType>
							&lt;m:Status?>{data($status[$p])}&lt;/m:Status>
							&lt;m:Timestamp?>{data($timestamp[$p])}&lt;/m:Timestamp>
						&lt;/m:PinData>
				}
			&lt;/m:PinDataTable>
	&lt;/m:SaveChannelPinResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapsaveChannelPinResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>