<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>PT58 - dodanie Body.Version.$1.2011-06-16</con:description>
  <con:xquery>declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/UpdateCustomer/ResponseTransform2/";
declare namespace ns0 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns-1 = "urn:baseentities.be.dcl";

declare function xf:ResponseTransform2($pcCustSetResponse1 as element(ns0:pcCustSetResponse))
    as element(ns1:invokeResponse) {
        &lt;ns1:invokeResponse>
            &lt;ns1:messageHelper>
                {
                    for $Error in $pcCustSetResponse1/ns0:Error
                    return
                        &lt;ns-1:MessageHelper>
                            {
                                for $NrKom in $Error/ns0:NrKom
                                return
                                    &lt;ns-1:errorCode>{ xs:string( data($NrKom) ) }&lt;/ns-1:errorCode>
                            }
                            {
                                for $OpisBledu in $Error/ns0:OpisBledu
                                return
                                    &lt;ns-1:message>{ data($OpisBledu) }&lt;/ns-1:message>
                            }
                        &lt;/ns-1:MessageHelper>
                }
            &lt;/ns1:messageHelper>
        &lt;/ns1:invokeResponse>
};

declare variable $pcCustSetResponse1 as element(ns0:pcCustSetResponse) external;

&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
{
xf:ResponseTransform2($pcCustSetResponse1)
}
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>