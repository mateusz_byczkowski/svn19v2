<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:schema>&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;!--
        Typy techniczne będące podstawą budowy webserwisów
        Version: 1.001
-->
&lt;xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:cmf-tech="http://jv.adapter.cu.com.pl/cmf-technical-types" xmlns:cmf-biz="http://jv.adapter.cu.com.pl/cmf-biz-types" xmlns:cmf-wsdl="http://jv.adapter.cu.com.pl/cmf-wsdl-types" targetNamespace="http://jv.adapter.cu.com.pl/cmf-wsdl-types">
	&lt;xs:import namespace="http://jv.adapter.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd"/>
	&lt;xs:import namespace="http://jv.adapter.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd"/>
	&lt;xs:annotation>
		&lt;xs:documentation>
            Typy techniczne będące podstawą budowy webserwisów

            CVS log: 
            $Revision: 1.21 $ 
            $Source: /it/krwnuk/cvs_do_migracji/SIS/ESB/CMF/base/cmf-wsdl-types.xsd,v $ 
            $Author: kadamowi $ 
            $Date: 2008-09-10 13:35:35 $
        &lt;/xs:documentation>
	&lt;/xs:annotation>
	&lt;xs:complexType name="CmfBodyType" abstract="true"/>
	&lt;xs:simpleType name="SourceCodeType">
		&lt;xs:restriction base="cmf-tech:CmfString">
	        &lt;xs:enumeration value="internet"/>
	        &lt;xs:enumeration value="placowka banku"/>
	        &lt;xs:enumeration value="placowka jv"/>
	    &lt;/xs:restriction>
	&lt;/xs:simpleType>
	&lt;xs:complexType name="EnvelopeType">
		&lt;xs:annotation>
			&lt;xs:documentation>
			  Dane generyczne do przekazywania informacji z requestem
			&lt;/xs:documentation>
		&lt;/xs:annotation>
		&lt;xs:sequence>
			&lt;xs:element name="request-time" type="xs:dateTime"/>
			&lt;xs:element name="request-no" type="cmf-tech:CmfString" minOccurs="0"/>
			&lt;xs:element name="source-code" type="cmf-wsdl:SourceCodeType" minOccurs="0"/>
			&lt;xs:element name="user-id" type="cmf-biz:UserIdType" minOccurs="0"/>
			&lt;xs:element name="branch-id" type="cmf-biz:JVBranchIdType" minOccurs="0"/>
			&lt;xs:element name="bar-code" type="cmf-biz:BarcodeType" minOccurs="0"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>
	&lt;xs:simpleType name="ErrorCodeType">
	    &lt;xs:restriction base="xs:int">
	        &lt;xs:enumeration value="0"/>
	        &lt;xs:enumeration value="1"/>
	    &lt;/xs:restriction>
	&lt;/xs:simpleType>
	&lt;xs:simpleType name="SeverityType">
		&lt;xs:annotation>
			&lt;xs:documentation>
                Rodzaj i waga błędu. 
                Możliwe wartości: 
                info - Informacja; 
                warning - Ostrzeżenie; 
                esbSecurity - Błąd zgłaszany przez ESB związany z autentykacją i
                    autoryzacją klientów Web serwisów, np 'Brak uprawnień do
                    wywołania serwisu X'; 
                appBusiness - Wyjątek biznesowy zgłoszony przez aplikację źródłową, nie świadczy o
                    błędnej pracy aplikacji, np 'Nie znaleziono klienta o podanym ID.'; 
                appSystem - Wyjątek systemowy zgłoszony przez aplikację źródłową,świadczy o 
                    wystąpieniu błędu w aplikacji, np 'Null Pointer Exception'; 
                esbSystem - Wyjątek systemowy ESB lub warstwy transportowej;
            &lt;/xs:documentation>
		&lt;/xs:annotation>
		&lt;xs:restriction base="xs:string">
			&lt;xs:enumeration value="info"/>
			&lt;xs:enumeration value="warning"/>
			&lt;xs:enumeration value="esbSecurity"/>
			&lt;xs:enumeration value="appBusiness"/>
			&lt;xs:enumeration value="appSystem"/>
			&lt;xs:enumeration value="esbSystem"/>
		&lt;/xs:restriction>
	&lt;/xs:simpleType>
	&lt;xs:complexType name="StatusType">
		&lt;xs:annotation>
			&lt;xs:documentation>
                Obiekt dołączany do odpowiedzi w razie wystąpienia błędu
                lub ostrzeżenia.
            &lt;/xs:documentation>
		&lt;/xs:annotation>
		&lt;xs:sequence>
			&lt;xs:element name="error-msg" type="cmf-tech:CmfString" minOccurs="0">
				&lt;xs:annotation>
					&lt;xs:documentation>
                        Treść komunikatu o błędzie
                    &lt;/xs:documentation>
				&lt;/xs:annotation>
			&lt;/xs:element>
			&lt;xs:element name="error-info" type="cmf-tech:CmfString" minOccurs="0"/>
			&lt;xs:element name="error-details" type="cmf-tech:CmfString" minOccurs="0"/>
			&lt;xs:element name="error-type" type="cmf-tech:CmfString" minOccurs="0"/>
		&lt;/xs:sequence>
		&lt;xs:attribute name="error-code" type="cmf-wsdl:ErrorCodeType" use="required"/>
		&lt;xs:attribute name="error-severity" type="cmf-wsdl:SeverityType" use="optional"/>
		&lt;xs:attribute name="application-id" type="cmf-tech:CmfString" use="optional">
			&lt;xs:annotation>
				&lt;xs:documentation>
                    Id aplikacji źródłowej zgłaszającej błąd
                &lt;/xs:documentation>
			&lt;/xs:annotation>
		&lt;/xs:attribute>
	&lt;/xs:complexType>
	&lt;xs:complexType name="CmfRequestType" abstract="true">
		&lt;xs:complexContent>
			&lt;xs:extension base="cmf-wsdl:CmfBodyType">
				&lt;xs:sequence>
					&lt;xs:element name="envelope" type="cmf-wsdl:EnvelopeType" minOccurs="0"/>
				&lt;/xs:sequence>
			&lt;/xs:extension>
		&lt;/xs:complexContent>
	&lt;/xs:complexType>
	&lt;xs:complexType name="CmfResponseType" abstract="true">
		&lt;xs:annotation>
			&lt;xs:documentation>
                Abstrakcyjny typ, którego implementacje definiują
                biznesową zawartość odpowiedzi komunikatów cmf
            &lt;/xs:documentation>
		&lt;/xs:annotation>
		&lt;xs:complexContent>
			&lt;xs:extension base="cmf-wsdl:CmfBodyType">
				&lt;xs:sequence>
					&lt;xs:element name="status" type="cmf-wsdl:StatusType"/>
				&lt;/xs:sequence>
			&lt;/xs:extension>
		&lt;/xs:complexContent>
	&lt;/xs:complexType>
&lt;/xs:schema></con:schema>
  <con:dependencies>
    <con:import namespace="http://jv.adapter.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd" ref="Proxy Services/Customers/ClientJV/base/cmf-technical-types"/>
    <con:import namespace="http://jv.adapter.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd" ref="Proxy Services/Customers/ClientJV/base/cmf-biz-types"/>
  </con:dependencies>
  <con:targetNamespace>http://jv.adapter.cu.com.pl/cmf-wsdl-types</con:targetNamespace>
</con:schemaEntry>