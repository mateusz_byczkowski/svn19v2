<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace tns="urn:services.dcl";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:cif.entities.be.dcl";
declare namespace urn2="urn:baseauxentities.be.dcl";

declare function xf:map_getCustomersCEKEBoxRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		&lt;fml:FML32>
		{
				for $it1 at $p1 in $req/urn:customersList/urn1:Customer
				return
				(
					&lt;fml:DC_NUMER_KLIENTA?>{ data($req/urn:customersList/urn1:Customer[$p1]/urn1:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA>
				)
		}
		{
				for $it2 at $p2 in $req/urn:customerCEKEFilter/urn2:IntegerHolder
				return
				(
					&lt;fml:E_MSG_FILTER_ID?>{ data($req/urn:customerCEKEFilter/urn2:IntegerHolder[$p2]/urn2:value) }&lt;/fml:E_MSG_FILTER_ID>					
				)	
		}
		
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_getCustomersCEKEBoxRequest($body/urn:invoke) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>