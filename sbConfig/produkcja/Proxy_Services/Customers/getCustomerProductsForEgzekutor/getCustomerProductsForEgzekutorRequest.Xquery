<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(: Log Zmian: 
v.1.0  2010-08-19 PKLI NP1836 Utworzenie 
:)

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappGetCustomerProductsRequest($req as element(m:GetCustomerProductsRequest))
	as element(fml:FML32) {
&lt;fml:FML32>
                {
	  if($req/m:customerNumber)
	  then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER>{ data($req/m:customerNumber) }&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER>
	  else ()
                }
  	{
	  if($req/m:accountNumber)
	  then &lt;fml:NF_ACCOUN_ACCOUNTNUMBER>{ data($req/m:accountNumber) }&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER>
	  else ()
	}
  	{
	  if($req/m:activeNonActive)
	  then &lt;fml:NF_CTRL_ACTIVENONACTIVE>{ data($req/m:activeNonActive) }&lt;/fml:NF_CTRL_ACTIVENONACTIVE>
	  else &lt;fml:NF_CTRL_ACTIVENONACTIVE>0&lt;/fml:NF_CTRL_ACTIVENONACTIVE>
	}
  	{
	  if($req/m:showCurrentBalance)
	  then &lt;fml:NF_CTRL_SHOWCURRBAL>{ data($req/m:showCurrentBalance) }&lt;/fml:NF_CTRL_SHOWCURRBAL>
	  else ()
	}
	{
  	  if($req/m:pageSize)
	  then &lt;fml:NF_PAGEC_PAGESIZE>{ data($req/m:pageSize) }&lt;/fml:NF_PAGEC_PAGESIZE>
	  else ()
	}
	{
	  if($req/m:actionCode)
	  then &lt;fml:NF_PAGEC_ACTIONCODE>{ data($req/m:actionCode) }&lt;/fml:NF_PAGEC_ACTIONCODE>
	  else  ()
	}
	{
	  if($req/m:navigationKeyValue)
	  then &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU>{data($req/m:navigationKeyValue)}&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU>
	  else  &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU>F&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU>
	}
                &lt;NF_MSHEAD_COMPANYID>1&lt;/NF_MSHEAD_COMPANYID>
                &lt;NF_CTRL_SYSTEMS>001&lt;/NF_CTRL_SYSTEMS>
                &lt;NF_CTRL_AREAS>02031013&lt;/NF_CTRL_AREAS>
                &lt;NF_ACCOUN_ALTERNATIVEADDRE>0&lt;/NF_ACCOUN_ALTERNATIVEADDRE>

&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappGetCustomerProductsRequest($body/m:GetCustomerProductsRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>