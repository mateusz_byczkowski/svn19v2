<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1.1 2009-06-03 PKL  TEET 37683
v.1.1.2 2009-06-10 PKL  TEET 37994
v.1.1.3 2009-06-17 KADA CR 61
v.1.1.4 2009-11-04 LKAB PT58Version.$1.2011-02-10</con:description>
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:card.entities.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns7="urn:dictionaries.be.dcl";
declare namespace ns8="urn:insurance.entities.be.dcl";
declare namespace ns9="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml="";
declare namespace ns10="urn:errors.hlbsentities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean($parm as xs:string*,$trueval as xs:string) as xs:string* {
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {
    if ($value)
      then if(string-length($value)>5)
          then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
      else() 
    else()
};

(:
declare function getElementsForInsurances($parm as element(fml:FML32)) as element()
{

&lt;ns0:insurances>
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="4") then 
    &lt;ns8:Insurance>
      &lt;ns8:insuranceNumber?>{data($parm/NF_INSURA_INSURANCENUMBER[$occ])}&lt;/ns8:insuranceNumber>
      &lt;ns8:insuranceDescription?>{data($parm/NF_INSURA_INSURANCEDESCRIP[$occ])}&lt;/ns8:insuranceDescription>
       { insertDate(data($parm/NF_INSURA_LASTINSURANCEPRE[$occ]),"yyyy-MM-dd","ns8:lastInsurancePremiumDate")}
       { insertDate(data($parm/NF_INSURA_INSURANCEOPENDAT[$occ]),"yyyy-MM-dd","ns8:insuranceOpenDate")}
        &lt;/ns8:Insurance>
    else ()
  }
&lt;/ns0:insurances>
};
:)


declare function getElementsForAccountRelationshipList($parm as element(fml:FML32)) as element()
{

&lt;ns9:accountRelationshipList>
  {
    for $x at $occ in $parm/XXXXX
    return
    &lt;ns4:AccountRelationship>
      &lt;ns4:relationship>
        &lt;ns7:CustomerAccountRelationship>
        &lt;/ns7:CustomerAccountRelationship>
      &lt;/ns4:relationship>
    &lt;/ns4:AccountRelationship>
  }
&lt;/ns9:accountRelationshipList>
};
declare function getElementsForAccounts($parm as element(fml:FML32)) as element()
{
&lt;ns0:accounts>
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="2" or data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="10"
          or data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="13") then
    &lt;ns9:Account>
      &lt;ns9:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns9:accountNumber>
     { insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns9:accountOpenDate")}
        &lt;ns9:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns9:currentBalance>
      &lt;ns9:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns9:accountDescription> 
      &lt;ns9:codeProductSystem?>{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}&lt;/ns9:codeProductSystem>
      &lt;ns9:accountRelationshipList>
        &lt;ns4:AccountRelationship>
          &lt;ns4:relationship>
             &lt;ns7:CustomerAccountRelationship>
                &lt;ns7:customerAccountRelationship>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns7:customerAccountRelationship>
             &lt;/ns7:CustomerAccountRelationship>
             &lt;/ns4:relationship>
        &lt;/ns4:AccountRelationship>
      &lt;/ns9:accountRelationshipList>
        {
           if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="10") then
             &lt;ns9:timeAccount>
               &lt;ns9:TimeAccount>
                   { insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]),"yyyy-MM-dd","ns9:dateOfMaint")}
               &lt;/ns9:TimeAccount>
             &lt;/ns9:timeAccount>
           else
             &lt;ns9:tranAccount>
                &lt;ns9:TranAccount>
                   { insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]),"yyyy-MM-dd","ns9:dateOfLastActivity")}
                   (: &lt;ns9:dateOfLastActivity?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]))}&lt;/ns9:dateOfLastActivity> :)
                 &lt;/ns9:TranAccount>
             &lt;/ns9:tranAccount>
          }
      &lt;ns9:accountType>
        &lt;ns7:AccountType>
          &lt;ns7:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns7:accountType>
        &lt;/ns7:AccountType>
      &lt;/ns9:accountType>
      &lt;ns9:currency>
        &lt;ns7:CurrencyCode>
          &lt;ns7:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns7:currencyCode>
        &lt;/ns7:CurrencyCode>
      &lt;/ns9:currency>
      {getElementsForProductDefinition($parm,$occ)}
    &lt;/ns9:Account>
    else()
  }
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="9" ) then
    &lt;ns9:Account>
      &lt;ns9:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns9:accountNumber>
     { insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns9:accountOpenDate")}
      &lt;ns9:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns9:currentBalance>
      &lt;ns9:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns9:accountDescription> 
      &lt;ns9:codeProductSystem?>{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}&lt;/ns9:codeProductSystem>
      &lt;ns9:tranAccount>
        &lt;ns9:TranAccount>
           { insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]),"yyyy-MM-dd","ns9:dateOfLastActivity")}
           &lt;/ns9:TranAccount>
      &lt;/ns9:tranAccount>
      &lt;ns9:accountType>
        &lt;ns7:AccountType>
          &lt;ns7:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns7:accountType>
        &lt;/ns7:AccountType>
      &lt;/ns9:accountType>
      &lt;ns9:currency>
        &lt;ns7:CurrencyCode>
          &lt;ns7:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns7:currencyCode>
        &lt;/ns7:CurrencyCode>
      &lt;/ns9:currency>
      {getElementsForProductDefinition($parm,$occ)}
    &lt;/ns9:Account>
    else()
  }
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="3"  ) then
    &lt;ns9:Account>
      &lt;ns9:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns9:accountNumber>
     { insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns9:accountOpenDate")}
      &lt;ns9:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns9:currentBalance>
      &lt;ns9:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns9:accountDescription> 
      &lt;ns9:codeProductSystem?>{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}&lt;/ns9:codeProductSystem>
      &lt;ns9:accountType>
        &lt;ns7:AccountType>
          &lt;ns7:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns7:accountType>
        &lt;/ns7:AccountType>
      &lt;/ns9:accountType>
      &lt;ns9:currency>
        &lt;ns7:CurrencyCode>
          &lt;ns7:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns7:currencyCode>
        &lt;/ns7:CurrencyCode>
      &lt;/ns9:currency>
      {getElementsForProductDefinition($parm,$occ)}
    &lt;/ns9:Account>
    else()
  }


&lt;/ns0:accounts>
};
declare function getElementsForCards($parm as element(fml:FML32)) as element()
{

&lt;ns0:cards>
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
     if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="1") then
    &lt;ns3:Card>
      &lt;ns3:cardName?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns3:cardName>
      &lt;ns3:debitCard>
        &lt;ns3:DebitCard>
          &lt;ns3:cardNbr?>{data($parm/NF_DEBITC_CARDNBR[$occ])}&lt;/ns3:cardNbr>
          &lt;ns3:virtualCardNbr?>{data($parm/NF_DEBITC_VIRTUALCARDNBR[$occ])}&lt;/ns3:virtualCardNbr>
          &lt;ns3:tranAccount>
            &lt;ns9:TranAccount>
              &lt;ns9:account>
                &lt;ns9:Account>
                  &lt;ns9:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns9:accountNumber>
                  &lt;ns9:currency>
                     &lt;ns7:CurrencyCode>
                      &lt;ns7:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns7:currencyCode>
                     &lt;/ns7:CurrencyCode>
                   &lt;/ns9:currency>
                 &lt;/ns9:Account>
               &lt;/ns9:account>
             &lt;/ns9:TranAccount>
           &lt;/ns3:tranAccount>
         &lt;/ns3:DebitCard>
       &lt;/ns3:debitCard>
    &lt;/ns3:Card>
         else ()
  }
&lt;/ns0:cards>
};
declare function getElementsForCollaterals($parm as element(fml:FML32)) as element()
{

&lt;ns0:collaterals>
  {
    for $x at $occ in $parm/NF_COLLAT_COLLATERALVALUE
    return
    if (string-length($parm/NF_COLLAT_COLLATERALVALUE[$occ])>0) then
    &lt;ns9:Collateral>      
      &lt;ns9:collateralDescription?>{data($parm/NF_COLLAT_COLLATERALDESCRI[$occ])}&lt;/ns9:collateralDescription>
      &lt;ns9:collateralValue?>{data($parm/NF_COLLAT_COLLATERALVALUE[$occ])}&lt;/ns9:collateralValue>
      &lt;ns9:collateralItemNumber?>{data($parm/NF_COLLAT_COLLATERALITEMNU[$occ])}&lt;/ns9:collateralItemNumber>
      &lt;ns9:lastMaintenance?>{data($parm/NF_COLLAT_LASTMAINTENANCE[$occ])}&lt;/ns9:lastMaintenance>
      &lt;ns9:currencyCode>
        &lt;ns7:CurrencyCode>
          &lt;ns7:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns7:currencyCode>
        &lt;/ns7:CurrencyCode>
      &lt;/ns9:currencyCode>
    &lt;/ns9:Collateral>
    else ()
  }
&lt;/ns0:collaterals>
};

(:
declare function getElementsForProductDefinition($parm as element(fml:FML32)) as element()
{

&lt;ns0:productDefinition>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
    &lt;ns1:ProductDefinition>
      &lt;ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup>
      &lt;ns1:idProductDefinition?>{data($parm/PT_ID_DEFINITION[$occ])}&lt;/ns1:idProductDefinition>
    &lt;/ns1:ProductDefinition>
  }
&lt;/ns0:productDefinition>
};
:)

declare function getElementsForProductDefinition($parm as element(fml:FML32), $occ as xs:integer) as element()
{
&lt;ns9:productDefinition>
    &lt;ns1:ProductDefinition>
      &lt;ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup>
      &lt;ns1:idProductDefinition?>{data($parm/PT_ID_DEFINITION[$occ])}&lt;/ns1:idProductDefinition>
    &lt;/ns1:ProductDefinition>
&lt;/ns9:productDefinition>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  (:{getElementsForInsurances($parm)}:)
  (:{getElementsForProductDefinition($parm)}:)
  {getElementsForAccounts($parm)}
  &lt;ns0:bcd>
    &lt;ns5:BusinessControlData>
      &lt;ns5:pageControl>
        &lt;ns6:PageControl>
          &lt;ns6:hasNext?>{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns6:hasNext>
          &lt;ns6:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition>
          &lt;ns6:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue>
        &lt;/ns6:PageControl>
      &lt;/ns5:pageControl>
    &lt;/ns5:BusinessControlData>
  &lt;/ns0:bcd>
  {getElementsForCards($parm)}
  {getElementsForCollaterals($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>