<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2010-02-03 PKL PT58 - błąd zaminy anyTypeVersion.$1.2011-06-01</con:description>
  <con:xquery>(: Rejestr zmian
v.1.1 2010-02-03 PKL PT58 - błąd zaminy anyType
:)

declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



(:1.1 declare function boolean2SourceValue ($parm as xs:string,$trueval as xs:string,$falseval as xs:string) as xs:string{ :)
declare function boolean2SourceValue ($parm as xs:string*,$trueval as xs:string,$falseval as xs:string) as xs:string{  (: 1.1 :)
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>

};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
&lt;NF_PAGEC_REVERSEORDER?>{boolean2SourceValue (data($parm/ns0:bcd/ns3:BusinessControlData/ns4:pageControl/ns4:PageControl/ns4:reverseOrder),"1","0")}&lt;/NF_PAGEC_REVERSEORDER>
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
,
&lt;NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns0:customer/ns2:Customer/ns2:customerNumber)}&lt;/NF_CUSTOM_CUSTOMERNUMBER>
,
&lt;NF_CTRL_ACTIVENONACTIVE>0&lt;/NF_CTRL_ACTIVENONACTIVE>
,
&lt;NF_ACCOUN_ALTERNATIVEADDRE>1&lt;/NF_ACCOUN_ALTERNATIVEADDRE>
,
(:&lt;NF_PRODUA_CODEPRODUCTAREA>0&lt;/NF_PRODUA_CODEPRODUCTAREA>
,:)
&lt;NF_CTRL_AREAS>02031013&lt;/NF_CTRL_AREAS>
,
&lt;NF_CTRL_OPTION>1&lt;/NF_CTRL_OPTION>
,
&lt;NF_PRODUG_VISIBILITYDCL>1&lt;/NF_PRODUG_VISIBILITYDCL>

};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>