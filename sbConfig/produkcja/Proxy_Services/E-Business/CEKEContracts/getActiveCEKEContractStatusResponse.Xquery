<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/ceke/messages/";



declare function xf:map_getActiveCEKEContractStatusResponse($fml as element(fml:FML32))
	as element(urn:GetActiveCEKEContractStatusResponse) {
                &lt;urn:GetActiveCEKEContractStatusResponse>
                    {
						if (data($fml/fml:E_REC_COUNT) != "0")
						then (
			            	&lt;state>{ data($fml/fml:E_AGR_STATUS) }&lt;/state>,
						 	&lt;stateDescription>{ data($fml/fml:E_AGR_STATUS_DESC) }&lt;/stateDescription>,
						(:	&lt;stateDate>{ data($fml/fml:E_AGR_STATUS_DATE) }&lt;/stateDate> :)
                                                        &lt;stateDate>{concat( substring(data($fml/fml:E_AGR_STATUS_DATE),1,10),'T', substring(data($fml/fml:E_AGR_STATUS_DATE),12,19) ) }&lt;/stateDate>
			             ) else ()
                    }

                &lt;/urn:GetActiveCEKEContractStatusResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_getActiveCEKEContractStatusResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>