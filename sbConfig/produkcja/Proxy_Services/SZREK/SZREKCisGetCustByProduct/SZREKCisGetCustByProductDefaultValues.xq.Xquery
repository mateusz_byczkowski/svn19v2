<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/esbmessages/messages/";
declare namespace xf = "http://bzwbk.com/services/esbmessages/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapSZREKCisGetCustByProductDefaultValues($req as element(m:CISGetCByProdRequest))
	as element(m:CISGetCByProdRequest) {
		&lt;m:CISGetCByProdRequest>
			{
				if($req/m:CI_ID_WEW_PRAC)
					then &lt;m:CI_ID_WEW_PRAC>{ data($req/m:CI_ID_WEW_PRAC) }&lt;/m:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:CI_ID_SPOLKI)
					then &lt;m:CI_ID_SPOLKI>{ data($req/m:CI_ID_SPOLKI) }&lt;/m:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:DC_NR_RACHUNKU)
					then &lt;m:DC_NR_RACHUNKU>{ data($req/m:DC_NR_RACHUNKU) }&lt;/m:DC_NR_RACHUNKU>
					else ()
			}
			{
				if($req/m:DC_TYP_KLIENTA)
					then &lt;m:DC_TYP_KLIENTA>{ data($req/m:DC_TYP_KLIENTA) }&lt;/m:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:CI_PRODUCT_TYPE)
					then &lt;m:CI_PRODUCT_TYPE>{ data($req/m:CI_PRODUCT_TYPE) }&lt;/m:CI_PRODUCT_TYPE>
					else ()
			}	
		        {
		               if($req/m:CI_PRODUCT_APLICATION )
					then &lt;m:CI_PRODUCT_APLICATION >{ data($req/m:CI_PRODUCT_APLICATION ) }&lt;/m:CI_PRODUCT_APLICATION >
					else ()
			}	
			&lt;m:CI_OPCJA>0&lt;/m:CI_OPCJA>	
		&lt;/m:CISGetCByProdRequest>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapSZREKCisGetCustByProductDefaultValues($body/m:CISGetCByProdRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>