<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:insurance.entities.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};

declare function getFields($parm as element(ns4:header), $parm2 as element(ns4:invoke)) as element()*
{

&lt;DC_TRN_ID?>{data($parm/ns4:transHeader/ns4:transId)}&lt;/DC_TRN_ID>
,
&lt;DC_UZYTKOWNIK?>{concat("SKP:", data($parm/ns4:msgHeader/ns4:userId))}&lt;/DC_UZYTKOWNIK>
,
&lt;DC_ODDZIAL?>{chkUnitId(data($parm/ns4:msgHeader/ns4:unitId))}&lt;/DC_ODDZIAL>
,
&lt;DC_RODZAJ_RACHUNKU>IS&lt;/DC_RODZAJ_RACHUNKU>
,
&lt;DC_NR_RACHUNKU?>{data($parm2/ns4:insurancePolicyAcc/ns3:InsurancePolicyAcc/ns3:policyRefNum)}&lt;/DC_NR_RACHUNKU>

};


&lt;soap:Body>
  &lt;fml:FML32>
    {getFields($header/ns4:header, $body/ns4:invoke)}
&lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>