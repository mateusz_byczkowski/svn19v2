<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:insurance.entities.be.dcl";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns2="urn:entities.be.dcl";
declare namespace ns3="urn:baseauxentities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:bool2short($bool as xs:boolean) as xs:string {
  if ($bool)
    then "1"
    else "0"
};

declare function getFieldsFromHeader($parm as element(ns1:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns1:msgHeader/ns1:msgId)}&lt;/NF_MSHEAD_MSGID>,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns1:msgHeader/ns1:companyId)}&lt;/NF_MSHEAD_COMPANYID>,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns1:msgHeader/ns1:unitId)}&lt;/NF_MSHEAD_UNITID>,
&lt;NF_MSHEAD_USERID?>{data($parm/ns1:msgHeader/ns1:userId)}&lt;/NF_MSHEAD_USERID>,
&lt;NF_MSHEAD_APPID?>{data($parm/ns1:msgHeader/ns1:appId)}&lt;/NF_MSHEAD_APPID>,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns1:msgHeader/ns1:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns1:transHeader/ns1:transId)}&lt;/NF_TRHEAD_TRANSID>
};


declare function getFieldsFromInvoke($parm as element(ns1:invoke)) as element()*
{
&lt;NF_INSUPA_NUMBER?>{data($parm/ns1:insurancePackageAcc/ns0:InsurancePackageAcc/ns0:number)}&lt;/NF_INSUPA_NUMBER>,
&lt;NF_INSPPH_HISTFLAG?>{data($parm/ns1:insPackPremiumHist/ns0:InsPackPremiumHist/ns0:histFlag)}&lt;/NF_INSPPH_HISTFLAG>,
&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns1:bcd/ns2:BusinessControlData/ns2:pageControl/ns3:PageControl/ns3:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns1:bcd/ns2:BusinessControlData/ns2:pageControl/ns3:PageControl/ns3:pageSize)}&lt;/NF_PAGEC_PAGESIZE>,
    if (data($parm/ns1:bcd/ns2:BusinessControlData/ns2:pageControl/ns3:PageControl/ns3:reverseOrder))
      then 	
         &lt;NF_PAGEC_REVERSEORDER?>{xf:bool2short(data($parm/ns1:bcd/ns2:BusinessControlData/ns2:pageControl/ns3:PageControl/ns3:reverseOrder))}&lt;/NF_PAGEC_REVERSEORDER>
      else()
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns1:bcd/ns2:BusinessControlData/ns2:pageControl/ns3:PageControl/ns3:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns1:header)}
    {getFieldsFromInvoke($body/ns1:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>