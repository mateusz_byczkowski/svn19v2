<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:applicationul.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CRW/AddApplicationUL/RequestTransform/";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace ns7 = "urn:baseauxentities.be.dcl";
declare namespace ns1 = "http://bzwbk.com/crw/services/insurance/";
declare namespace ns-1 = "urn:baseentities.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns5 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:basedictionaries.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns8 = "urn:entities.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns6:invoke), $header1 as element(ns6:header))
    as element(ns1:updateApplication) {
        &lt;ns1:updateApplication>
            &lt;application>
	            {
	            	let $accounts :=
	            		for $account in $invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:policyContractAccountList/ns4:PolicyContractAccount
	            		return
			                &lt;account>
				                &lt;accountNumber>{ data($account/ns4:accountNo) }&lt;/accountNumber>
	        	                &lt;accountOwner>{ fn:concat(data($account/ns4:firstName), ' ', data($account/ns4:name) ) }&lt;/accountOwner>
			                    &lt;address>
	                                &lt;city?>{ data($account/ns4:city) }&lt;/city>
	                                &lt;country?>{ data($account/ns3:CountryCode/ns3:countryCode) }&lt;/country>
	                                &lt;houseFlatNumber?>{ data($account/ns4:house) }&lt;/houseFlatNumber>
	                                &lt;street?>{ data($account/ns4:street) }&lt;/street>
	                                &lt;zipCode?>{ data($account/ns4:zipCode) }&lt;/zipCode>
			                    &lt;/address>
	        	            &lt;/account>
	        	    return $accounts[1]
	            }
	            &lt;accountNumber>{
	            	let $accounts :=
	            		for $account in $invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:policyContractAccountList/ns4:PolicyContractAccount
	            		return
	            			data($account/ns4:accountNo)
	            	return $accounts[1]
	            }&lt;/accountNumber>
	            &lt;advisorID?>{ data($header1/ns6:msgHeader/ns6:userId) }&lt;/advisorID>
                {
                    for $PolicyCoverageFundSplit in $invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:policyCoverageList/ns4:PolicyCoverage[1]/ns4:policyCoverageFundSplitList/ns4:PolicyCoverageFundSplit
                    return
                        &lt;allocations>
                            &lt;fundCode?>{ data($PolicyCoverageFundSplit/ns4:fundID/ns5:UlFund/ns5:fundID) }&lt;/fundCode>
                            &lt;share?>{ xs:decimal( data($PolicyCoverageFundSplit/ns4:splitPercentage) ) }&lt;/share>
                        &lt;/allocations>
                }
                &lt;barCodeId>{ data($invoke1/ns6:barCodeID/ns7:StringHolder/ns7:value) }&lt;/barCodeId>
                {
                    for $PolicyCoverageBenefited in $invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:policyCoverageList/ns4:PolicyCoverage[1]/ns4:policyCoverageBenefitedList/ns4:PolicyCoverageBenefited
                    return
                        &lt;beneficiaries>
	                        {
	                        let $birthDate := $PolicyCoverageBenefited/ns4:birthDate
	                        return
	                           if ($birthDate ne '') then
	                            &lt;birthDate>{ xs:dateTime( fn:concat(data($birthDate), 'T00:00:00') ) }&lt;/birthDate>
	                           else '' 
	                        }
                            &lt;firstName?>{ data($PolicyCoverageBenefited/ns4:firstName) }&lt;/firstName>
                            &lt;lastName?>{ data($PolicyCoverageBenefited/ns4:name) }&lt;/lastName>
                            &lt;pesel?>{ data($PolicyCoverageBenefited/ns4:pesel) }&lt;/pesel>
                            &lt;regon?>{ data($PolicyCoverageBenefited/ns4:regon) }&lt;/regon>
                            &lt;shareInBenefit?>{ xs:decimal( data($PolicyCoverageBenefited/ns4:percent) ) }&lt;/shareInBenefit>
                            &lt;type?>{ xs:short( data($PolicyCoverageBenefited/ns4:privatePerson/ns3:CustomerType/ns3:customerType) ) }&lt;/type>
                        &lt;/beneficiaries>
                }
                &lt;creationDate>{ xs:dateTime( fn:concat(data($invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:proposalDate), 'T00:00:00') ) }&lt;/creationDate>
                &lt;customerRoles>
	                {
	                	let $customer := $invoke1/ns6:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal
	                	return
	                		&lt;customer>
			                	{
		                        let $birthDate := $customer/ns0:dateOfBirth
		                        return
		                           if ($birthDate ne '') then
		                            &lt;birthDate>{ xs:dateTime( fn:concat(data($birthDate), 'T00:00:00') ) }&lt;/birthDate>
		                           else '' 
		                        }
	                            &lt;cif?>{ xs:int( data($invoke1/ns6:customer/ns0:Customer/ns0:customerNumber) ) }&lt;/cif>
	                            &lt;firstName?>{ data($customer/ns0:firstName) }&lt;/firstName>
                {
                            if (fn:boolean(data($customer/ns0:identityCardNumber))) then
	                           &lt;idSerNum?>{ data($customer/ns0:identityCardNumber) }&lt;/idSerNum>
	                        else
                            for $documentNumber in $invoke1/ns6:customer/ns0:Customer/ns0:documentList/ns0:Document[1]/ns0:documentNumber
                            return
                           if (fn:boolean(data($documentNumber))) then
                            &lt;idSerNum>{ data($documentNumber) }&lt;/idSerNum>
                           else() 
                        }
                {
                            if (fn:boolean(data($customer/ns0:identityCardNumber))) then
                               &lt;idType> { 1 }&lt;/idType>
	                        else
                            for $documentNumber in $invoke1/ns6:customer/ns0:Customer/ns0:documentList/ns0:Document[1]/ns0:documentNumber
                            return
                           if (fn:boolean(data($documentNumber))) then
                               &lt;idType?> { 0 }&lt;/idType> 
                           else() 
                        }

	                            &lt;lastName?>{ data($customer/ns0:lastName) }&lt;/lastName>
	                            &lt;passport?>{ data($customer/ns0:passportNumber) }&lt;/passport>
	                            &lt;pesel?>{ data($customer/ns0:pesel) }&lt;/pesel>
	                            {
	                            	let $addresses :=
	                            		for $address in $invoke1/ns6:customer/ns0:Customer/ns0:addressList/ns0:Address
	                            		return
					                        &lt;primaryAddress>
			                                    &lt;city?>{ data($address/ns0:city) }&lt;/city>
			                                    &lt;country?>{ data($address/ns0:countryId/ns3:CountryCode/ns3:countryCode) }&lt;/country>
			                                    &lt;houseFlatNumber?>{ data($address/ns0:houseFlatNumber) }&lt;/houseFlatNumber>
			                                    &lt;street?>{ data($address/ns0:street) }&lt;/street>
			                                    &lt;zipCode?>{ data($address/ns0:zipCode) }&lt;/zipCode>
					                        &lt;/primaryAddress>
	                            	return $addresses[1]
	                            }
	                		&lt;/customer>
	                }
                	&lt;type>APPLICANT&lt;/type>
                &lt;/customerRoles>
                {
                	let $addresses :=
                		for $address in $invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:policyContractAddressList/ns4:PolicyContractAddress
                		return
                			&lt;deliveryAddress>
                				&lt;city?>{ data($address/ns4:city) }&lt;/city>
	                            &lt;country?>{ data($address/ns4:country/ns3:CountryCode/ns3:countryCode) }&lt;/country>
	                            &lt;houseFlatNumber?>{ data($address/ns4:house) }&lt;/houseFlatNumber>
	                            &lt;lastName?>{ data($address/ns4:adressName) }&lt;/lastName>
	                            &lt;street?>{ data($address/ns4:street) }&lt;/street>
	                            &lt;zipCode?>{ data($address/ns4:zipCode) }&lt;/zipCode>
                			&lt;/deliveryAddress>
                	return $addresses[1]
                }
                &lt;email?>{ data($invoke1/ns6:customer/ns0:Customer/ns0:email) }&lt;/email>
                &lt;homePhoneNumber?>{ data($invoke1/ns6:customer/ns0:Customer/ns0:phoneNo) }&lt;/homePhoneNumber>
                &lt;insuranceFee?>{ 
                	let $coverages :=
                		for $coverage in $invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:policyCoverageList/ns4:PolicyCoverage
                		return
                			xs:decimal( data($coverage/ns4:premiumAmount) )
                	return $coverages[1] 
                }&lt;/insuranceFee>
                &lt;mobilePhoneNumber?>{ data($invoke1/ns6:customer/ns0:Customer/ns0:mobileNo) }&lt;/mobilePhoneNumber>
                &lt;number>{ data($invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:proposalID) }&lt;/number>
                &lt;registrationBranch?>{ xs:int( data($header1/ns6:msgHeader/ns6:unitId) ) }&lt;/registrationBranch>
                &lt;signatureOWU?>{ 
                	let $coverages :=
                		for $coverage in $invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:policyCoverageList/ns4:PolicyCoverage
                		return
                			data($coverage/ns4:owuSig)
                	return $coverages[1]
                }&lt;/signatureOWU>
                &lt;signaturePK?>{ 
                	let $coverages :=
                		for $coverage in $invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:policyCoverageList/ns4:PolicyCoverage
                		return
                			data($coverage/ns4:parametrsCardSig)
                	return $coverages[1]
                }&lt;/signaturePK>
                &lt;signatureUFK?>{ 
	               	let $coverages :=
                		for $coverage in $invoke1/ns6:customer/ns0:Customer/ns0:policyContract/ns4:PolicyContract/ns4:policyCoverageList/ns4:PolicyCoverage
                		return
		                	data($coverage/ns4:regulationSig)
		            return $coverages[1]
                }&lt;/signatureUFK>
                &lt;type?>{ data($invoke1/ns6:applicationType/ns7:StringHolder/ns7:value) }&lt;/type>
                &lt;workPhoneNumber?>{ data($invoke1/ns6:customer/ns0:Customer/ns0:businessNo) }&lt;/workPhoneNumber>
            &lt;/application>
            &lt;authData>
                &lt;applicationId>{ xs:int( data($invoke1/ns6:applicationID/ns7:IntegerHolder/ns7:value) ) }&lt;/applicationId>
                &lt;applicationPassword>{ data($invoke1/ns6:applicationPassword/ns7:StringHolder/ns7:value)}&lt;/applicationPassword>
                &lt;operator>{ data($header1/ns6:msgHeader/ns6:userId) }&lt;/operator>
            &lt;/authData>
        &lt;/ns1:updateApplication>
};

declare variable $invoke1 as element(ns6:invoke) external;
declare variable $header1 as element(ns6:header) external;

xf:RequestTransform($invoke1, $header1)</con:xquery>
</con:xqueryEntry>