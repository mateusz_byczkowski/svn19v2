<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:applicationul.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Pilot/GetContractList/RequestTransform/";
declare namespace ns0 = "urn:baseentities.be.dcl";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns7 = "urn:baseauxentities.be.dcl";
declare namespace ns3 = "urn:basedictionaries.be.dcl";
declare namespace ns2 = "urn:cif.entities.be.dcl";
declare namespace ns5 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns8 = "urn:entities.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns6:invoke), $header1 as element(ns6:header))
    as element(ns-1:getContractListRequest) {
        &lt;ns-1:getContractListRequest>
            &lt;envelope>
                &lt;request-time>{ data($header1/ns6:msgHeader/ns6:timestamp) }&lt;/request-time>
                &lt;request-no>{ data($header1/ns6:msgHeader/ns6:msgId) }&lt;/request-no>
            	&lt;source-code>{ data($invoke1/ns6:sourceCode/ns7:StringHolder/ns7:value) }&lt;/source-code>
            	&lt;user-id>{ data($header1/ns6:msgHeader/ns6:userId) }&lt;/user-id>
                &lt;branch-id>{ xs:long( data($header1/ns6:msgHeader/ns6:unitId) ) }&lt;/branch-id>
                &lt;bar-code?>{ data($invoke1/ns6:barCodeID/ns7:StringHolder/ns7:value) }&lt;/bar-code>
            &lt;/envelope>
            &lt;client-code>{ data($invoke1/ns6:customer/ns2:Customer/ns2:customerNumber) }&lt;/client-code>
            &lt;product-type>{ data($invoke1/ns6:customer/ns2:Customer/ns2:policyContract/ns4:PolicyContract/ns4:productCode/ns5:UlParameters/ns5:productId) }&lt;/product-type>
        &lt;/ns-1:getContractListRequest>
};

declare variable $invoke1 as element(ns6:invoke) external;
declare variable $header1 as element(ns6:header) external;

xf:RequestTransform($invoke1, $header1)</con:xquery>
</con:xqueryEntry>