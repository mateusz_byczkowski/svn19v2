<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf="urn:dcl:services.alsb.datamodel";

declare namespace  dcl="urn:be.services.dcl";
declare namespace  ns1="urn:operations.entities.be.dcl";
declare namespace  ns2="urn:operationsdictionary.dictionaries.be.dcl";
declare namespace  ns3="urn:dictionaries.be.dcl";
declare namespace  ns4="urn:basedictionaries.be.dcl";
declare namespace  ns5="urn:filtersandmessages.entities.be.dcl";



declare variable $body external;
declare variable $fml as element(FML32):=$body/FML32;

declare function xf:dateTimeFromString($dateIn as xs:string) as xs:string 
{
     fn:concat (fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2),
                        "T00:00:00")
};

&lt;soap-env:Body> 
     &lt;dcl:invokeResponse> 
        
        &lt;dcl:result>
            &lt;ns5:ResponseMessage>
                    &lt;ns5:result>
                        { if  ($fml/TR_STATUS = "P")
                           then     "true"
                           else     "false"}
                      &lt;/ns5:result>
            &lt;/ns5:ResponseMessage>
        &lt;/dcl:result>

          &lt;dcl:transaction>
              &lt;ns1:Transaction>
                  &lt;ns1:transactionID>{data($fml/TR_ID_OPER)}&lt;/ns1:transactionID>
                  &lt;ns1:transactionDate>{xf:dateTimeFromString($fml/TR_DATA_KSIEG)}&lt;/ns1:transactionDate>
                  &lt;ns1:transactionStatus>
                    &lt;ns3:TransactionStatus>
                       &lt;ns3:transactionStatus>{data($fml/TR_STATUS)}&lt;/ns3:transactionStatus>
                    &lt;/ns3:TransactionStatus>
                  &lt;/ns1:transactionStatus>
               &lt;/ns1:Transaction>
         &lt;/dcl:transaction>

        &lt;dcl:errCode>
            { if  ($fml/TR_KOD_BLEDU_1 != "000")  then
                &lt;ns5:ResponseMessage>
                         &lt;ns5:errorCode>{data($fml/TR_KOD_BLEDU_1)}&lt;/ns5:errorCode>
                         &lt;ns5:errorDescription>&lt;/ns5:errorDescription>
                &lt;/ns5:ResponseMessage>
                else()
            }
             { if  ($fml/TR_KOD_BLEDU_2 != "000")  then
                 &lt;ns5:ResponseMessage>
                     &lt;ns5:errorCode>{data($fml/TR_KOD_BLEDU_2)}&lt;/ns5:errorCode>
                     &lt;ns5:errorDescription>&lt;/ns5:errorDescription>
                 &lt;/ns5:ResponseMessage>
                   else()             
             }
             { if  ($fml/TR_KOD_BLEDU_3 != "000")  then
                 &lt;ns5:ResponseMessage>
                     &lt;ns5:errorCode>{data($fml/TR_KOD_BLEDU_3)}&lt;/ns5:errorCode>
                     &lt;ns5:errorDescription>&lt;/ns5:errorDescription>
                 &lt;/ns5:ResponseMessage>
                   else()             
             }
             { if  ($fml/TR_KOD_BLEDU_4 != "000")  then
                 &lt;ns5:ResponseMessage>
                     &lt;ns5:errorCode>{data($fml/TR_KOD_BLEDU_4)}&lt;/ns5:errorCode>
                     &lt;ns5:errorDescription>&lt;/ns5:errorDescription>
                 &lt;/ns5:ResponseMessage>
                   else()             
             }
             { if  ($fml/TR_KOD_BLEDU_5 != "000")  then
                 &lt;ns5:ResponseMessage>
                     &lt;ns5:errorCode>{data($fml/TR_KOD_BLEDU_5)}&lt;/ns5:errorCode>
                     &lt;ns5:errorDescription>&lt;/ns5:errorDescription>
                 &lt;/ns5:ResponseMessage>
                   else()             
             }
         &lt;/dcl:errCode>
         
        &lt;dcl:resultCode>
            &lt;ns5:ResponseMessage>
                    &lt;ns5:resultCode> {data($fml/TR_STATUS)} &lt;/ns5:resultCode>
            &lt;/ns5:ResponseMessage>
        &lt;/dcl:resultCode>

        &lt;dcl:errorDescription>
            &lt;ns5:ResponseMessage>
                    &lt;ns5:errorDescription> {data($fml/TR_OPIS_BLEDU)} &lt;/ns5:errorDescription>
            &lt;/ns5:ResponseMessage>
        &lt;/dcl:errorDescription>

   &lt;/dcl:invokeResponse> 

&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>