<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:cif.entities.be.dcl";
declare namespace m3 = "urn:dictionaries.be.dcl";
declare namespace m4 = "urn:accountdict.dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string
  return 
    if ($value &lt; 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDateTime($dateIn as xs:dateTime) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-dateTime($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-dateTime($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-dateTime($dateIn)))
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-date($dateIn)))
};

declare function xf:booleanTo01($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "1"
    else "0"
};

declare function xf:booleanToTN($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "T"
    else "N"
};

&lt;soap-env:Body>
  {
    let $req  := $body/m:invoke/m:collateral/m1:Collateral
    let $reqh := $header/m:header
    let $reqm := $req/m1:collateralMortgage/m1:CollateralMortgage
 
    return
    
    &lt;fml:FML32>
      {if($reqh/m:transHeader/m:transId)
          then &lt;fml:DC_TRN_ID>{ data($reqh/m:transHeader/m:transId) }&lt;/fml:DC_TRN_ID>
          else ()
      }
    {if($reqh/m:msgHeader/m:msgId)
          then &lt;fml:DC_MSHEAD_MSGID>{ data($reqh/m:msgHeader/m:msgId) }&lt;/fml:DC_MSHEAD_MSGID>
          else ()
      }
      {if($reqh/m:msgHeader/m:userId)
          then &lt;fml:DC_UZYTKOWNIK>{concat("SKP:", data($reqh/m:msgHeader/m:userId)) }&lt;/fml:DC_UZYTKOWNIK>
          else ()
      }
      {if($reqh/m:msgHeader/m:unitId)
          then 
              if($reqh/m:msgHeader/m:unitId &lt; 1000)
                then &lt;fml:DC_ODDZIAL>{ data($reqh/m:msgHeader/m:unitId) }&lt;/fml:DC_ODDZIAL>
                else &lt;fml:DC_ODDZIAL>0&lt;/fml:DC_ODDZIAL>
          else ()
      }

      {if($req/m1:locationCode and fn:string-length($req/m1:locationCode)>0)
          then &lt;fml:DC_KOD_NALICZENIA_REZERWY>{ data($req/m1:locationCode) }&lt;/fml:DC_KOD_NALICZENIA_REZERWY>
          else &lt;fml:DC_KOD_NALICZENIA_REZERWY>0&lt;/fml:DC_KOD_NALICZENIA_REZERWY>
      }
      {if($req/m1:collateralDescription and fn:string-length($req/m1:collateralDescription)>0)
          then (&lt;fml:DC_OPIS_ZABEZP_1_LINIA>{ substring(data($req/m1:collateralDescription),1,60) }&lt;/fml:DC_OPIS_ZABEZP_1_LINIA>,
                &lt;fml:DC_OPIS_ZABEZP_2_LINIA>{ substring(data($req/m1:collateralDescription),61,60) }&lt;/fml:DC_OPIS_ZABEZP_2_LINIA>,
                &lt;fml:DC_OPIS_ZABEZP_3_LINIA>{ substring(data($req/m1:collateralDescription),121,60) }&lt;/fml:DC_OPIS_ZABEZP_3_LINIA>)
          else ()
      }
      {if($req/m1:collateralValue and fn:string-length($req/m1:collateralValue)>0)
          then &lt;fml:DC_WARTOSC_ZABEZPIECZENIA>{ data($req/m1:collateralValue) }&lt;/fml:DC_WARTOSC_ZABEZPIECZENIA>
          else ()
      }
      {if($req/m1:expirationOrMaturityDate and fn:string-length($req/m1:expirationOrMaturityDate)>0)
          then &lt;fml:DC_DATA_WYGAS_LUB_ZAPADALNO>{ xf:mapDate($req/m1:expirationOrMaturityDate) }&lt;/fml:DC_DATA_WYGAS_LUB_ZAPADALNO>
          else ()
      }
      {if($req/m1:insuranceRequired and fn:string-length($req/m1:insuranceRequired)>0)
          then &lt;fml:DC_WYMAGANE_UBEZPIECZENIE_T_N>{ xf:booleanTo01($req/m1:insuranceRequired) }&lt;/fml:DC_WYMAGANE_UBEZPIECZENIE_T_N>
          else ()
      }
      {if($req/m1:insuranceExpiryDate and fn:string-length($req/m1:insuranceExpiryDate)>0)
          then &lt;fml:DC_DATA_WAZNOSCI_UBEZP>{ xf:mapDate($req/m1:insuranceExpiryDate) }&lt;/fml:DC_DATA_WAZNOSCI_UBEZP>
          else ()
      }
      {if($req/m1:legalClaimRegisteredDate and fn:string-length($req/m1:legalClaimRegisteredDate)>0)
          then &lt;fml:DC_DATA_REJESTRACJI_ZABEZP>{ xf:mapDate($req/m1:legalClaimRegisteredDate) }&lt;/fml:DC_DATA_REJESTRACJI_ZABEZP>
          else ()
      }
      {if($req/m1:legalClaimExpiryDate and fn:string-length($req/m1:legalClaimExpiryDate)>0)
          then &lt;fml:DC_DATA_WYGAS_REJESTRACJI>{ xf:mapDate($req/m1:legalClaimExpiryDate) }&lt;/fml:DC_DATA_WYGAS_REJESTRACJI>
          else ()
      }
      {if($req/m1:reviewFrequency and fn:string-length($req/m1:reviewFrequency)>0)
          then &lt;fml:DC_CZESTOTL_WYCENY>{ data($req/m1:reviewFrequency) }&lt;/fml:DC_CZESTOTL_WYCENY>
          else ()
      }
      {if($req/m1:reviewPeriod and fn:string-length($req/m1:reviewPeriod)>0)
          then &lt;fml:DC_OKRES_WYCENY_ZABEZP>{ data($req/m1:reviewPeriod) }&lt;/fml:DC_OKRES_WYCENY_ZABEZP>
          else ()
      }
      &lt;fml:DC_OKRESLONY_DZIEN_WYCENY>00&lt;/fml:DC_OKRESLONY_DZIEN_WYCENY>
      {if($req/m1:firstReviewDate and fn:string-length($req/m1:firstReviewDate)>0)
          then &lt;fml:DC_DATA_PIERWSZEJ_WYCENY>{ xf:mapDateTime($req/m1:firstReviewDate) }&lt;/fml:DC_DATA_PIERWSZEJ_WYCENY>
          else ()
      }
      {if($req/m1:reviewComments and fn:string-length($req/m1:reviewComments)>0)
          then &lt;fml:DC_KOMENTARZ_DLA_WYCENY>{ data($req/m1:reviewComments) }&lt;/fml:DC_KOMENTARZ_DLA_WYCENY>
          else ()
      }
      {if($req/m1:numberOfUnits and fn:string-length($req/m1:numberOfUnits)>0)
          then &lt;fml:DC_LICZBA_JEDN_ZABEZP>{ data($req/m1:numberOfUnits) }&lt;/fml:DC_LICZBA_JEDN_ZABEZP>
          else ()
      }
      {if($req/m1:unitPrice and fn:string-length($req/m1:unitPrice)>0)
          then &lt;fml:DC_WARTOSC_JEDNOSTKOWA>{ data($req/m1:unitPrice) }&lt;/fml:DC_WARTOSC_JEDNOSTKOWA>
          else ()
      }
      {if($req/m1:marginPercentage and fn:string-length($req/m1:marginPercentage)>0)
          then &lt;fml:DC_PROCENT_POMNIEJSZ_REZERWY>{ round-half-to-even(data($req/m1:marginPercentage)*100,4) }&lt;/fml:DC_PROCENT_POMNIEJSZ_REZERWY>
          else ()
      }
      {if($req/m1:maximumCollateralValue and fn:string-length($req/m1:maximumCollateralValue)>0)
          then &lt;fml:DC_MAKS_WARTOSC_ZABEZPIECZ>{ data($req/m1:maximumCollateralValue) }&lt;/fml:DC_MAKS_WARTOSC_ZABEZPIECZ>
          else ()
      }
      {if($req/m1:dateLastPriced and fn:string-length($req/m1:dateLastPriced)>0)
          then &lt;fml:DC_DATA_POPRZEDNIEJ_WYCENY>{ xf:mapDate($req/m1:dateLastPriced) }&lt;/fml:DC_DATA_POPRZEDNIEJ_WYCENY>
          else ()
      }
      {if($req/m1:itemReferenceNumber and fn:string-length($req/m1:itemReferenceNumber)>0)
          then &lt;fml:DC_NR_RACH_OBJETY_BLOKADA_SRO>{ data($req/m1:itemReferenceNumber) }&lt;/fml:DC_NR_RACH_OBJETY_BLOKADA_SRO>
          else ()
      }
      {if($req/m1:safekeepingReceiptNbr and fn:string-length($req/m1:safekeepingReceiptNbr)>0)
          then &lt;fml:DC_INFORM_DODATK_O_ZABEZP>{ data($req/m1:safekeepingReceiptNbr) }&lt;/fml:DC_INFORM_DODATK_O_ZABEZP>
          else ()
      }
      {if($req/m1:shortDescription and fn:string-length($req/m1:shortDescription)>0)
          then &lt;fml:DC_NAZWA_SKROCONA>{ data($req/m1:shortDescription) }&lt;/fml:DC_NAZWA_SKROCONA>
          else ()
      }
      {if($req/m1:userField6 and fn:string-length($req/m1:userField6)>0)
          then &lt;fml:DC_NR_UMOWY_PORECZ>{ data($req/m1:userField6) }&lt;/fml:DC_NR_UMOWY_PORECZ>
          else ()
      }

      {for $it at $i in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:customer/m2:Customer/m2:customerNumber)
            then 
              if($it/m1:relationship = "SOW" or $it/m1:relationship = "JAF")
                then &lt;fml:DC_NUMER_KLIENTA>{ data($it/m1:customer/m2:Customer/m2:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA>
                else ()
            else ()
      }

      {for $it in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:relationship and fn:string-length($it/m1:relationship)>0)
            then &lt;fml:DC_RELACJA>{ data($it/m1:relationship) }&lt;/fml:DC_RELACJA>
            else ()
      }
      {for $it in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:customer/m2:Customer/m2:customerNumber and fn:string-length($it/m1:customer/m2:Customer/m2:customerNumber)>0)
            then &lt;fml:DC_NUMER_KLIENTA_REL>{ data($it/m1:customer/m2:Customer/m2:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA_REL>
            else ()
      }

      {if($reqm/m1:shortLegalDescription and fn:string-length($reqm/m1:shortLegalDescription)>0)
          then &lt;fml:DC_OPIS_SKROCONY_ZABEZP>{ data($reqm/m1:shortLegalDescription) }&lt;/fml:DC_OPIS_SKROCONY_ZABEZP>
          else ()
      }
      {if($reqm/m1:legalRegistrationNumber and fn:string-length($reqm/m1:legalRegistrationNumber)>0)
          then &lt;fml:DC_NR_KSIEGI_WIECZYSTEJ_1>{ data($reqm/m1:legalRegistrationNumber) }&lt;/fml:DC_NR_KSIEGI_WIECZYSTEJ_1>
          else ()
      }
      {if($reqm/m1:yearBuilt and fn:string-length($reqm/m1:yearBuilt)>0)
          then &lt;fml:DC_ROK_BUDOWY_ROK_ZAKUPU_BUD>{ data($reqm/m1:yearBuilt) }&lt;/fml:DC_ROK_BUDOWY_ROK_ZAKUPU_BUD>
          else ()
      }
      {if($reqm/m1:yearPurchased and fn:string-length($reqm/m1:yearPurchased)>0)
          then &lt;fml:DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK>{ data($reqm/m1:yearPurchased) }&lt;/fml:DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK>
          else ()
      }
      {if($reqm/m1:purchasePrice and fn:string-length($reqm/m1:purchasePrice)>0)
          then &lt;fml:DC_CENA_ZAKUPU>{ data($reqm/m1:purchasePrice) }&lt;/fml:DC_CENA_ZAKUPU>
          else ()
      }
      {if($reqm/m1:lotSize and fn:string-length($reqm/m1:lotSize)>0)
          then &lt;fml:DC_POWIERZCHNIA_DZIALKI>{ data($reqm/m1:lotSize) }&lt;/fml:DC_POWIERZCHNIA_DZIALKI>
          else ()
      }
      {if($reqm/m1:squareFeet and fn:string-length($reqm/m1:squareFeet)>0)
          then &lt;fml:DC_POWIERZCHNIA_BUDYNKU>{ data($reqm/m1:squareFeet) }&lt;/fml:DC_POWIERZCHNIA_BUDYNKU>
          else ()
      }
      {if($reqm/m1:numberOfResidents and fn:string-length($reqm/m1:numberOfResidents)>0)
          then &lt;fml:DC_LICZBA_MIESZKANCOW>{ data($reqm/m1:numberOfResidents) }&lt;/fml:DC_LICZBA_MIESZKANCOW>
          else ()
      }
      {if($reqm/m1:leaseExpirationDate and fn:string-length($reqm/m1:leaseExpirationDate)>0)
          then &lt;fml:DC_DATA_ZL_WN_O_WPIS_HIPO>{ xf:mapDate($reqm/m1:leaseExpirationDate) }&lt;/fml:DC_DATA_ZL_WN_O_WPIS_HIPO>
          else ()
      }
      {if($reqm/m1:dateInspected and fn:string-length($reqm/m1:dateInspected)>0)
          then &lt;fml:DC_PRZEWIDYWANA_DATA_ZAK>{ xf:mapDate($reqm/m1:dateInspected) }&lt;/fml:DC_PRZEWIDYWANA_DATA_ZAK>
          else ()
      }
      {if($reqm/m1:subdivision and fn:string-length($reqm/m1:subdivision)>0)
          then &lt;fml:DC_SAD_REJONOWY>{ data($reqm/m1:subdivision) }&lt;/fml:DC_SAD_REJONOWY>
          else ()
      }
      {if($reqm/m1:appraisedValue and fn:string-length($reqm/m1:appraisedValue)>0)
          then &lt;fml:DC_SZACOWANA_WARTOSC>{ data($reqm/m1:appraisedValue) }&lt;/fml:DC_SZACOWANA_WARTOSC>
          else ()
      }
      {if($reqm/m1:appraisalDate and fn:string-length($reqm/m1:appraisalDate)>0)
          then &lt;fml:DC_DATA_SPORZ_WYCENY>{ xf:mapDate($reqm/m1:appraisalDate) }&lt;/fml:DC_DATA_SPORZ_WYCENY>
          else ()
      }
      {if($reqm/m1:appraiserName and fn:string-length($reqm/m1:appraiserName)>0)
          then &lt;fml:DC_NAZWISKO_RZECZOZNAWCY>{ data($reqm/m1:appraiserName) }&lt;/fml:DC_NAZWISKO_RZECZOZNAWCY>
          else ()
      }
      {if($reqm/m1:mortgagesPayablePrior and fn:string-length($reqm/m1:mortgagesPayablePrior)>0)
          then &lt;fml:DC_WARTOSC_WPISU_DO_HIPOTEKI>{ data($reqm/m1:mortgagesPayablePrior) }&lt;/fml:DC_WARTOSC_WPISU_DO_HIPOTEKI>
          else ()
      }
      {if($reqm/m1:userField2 and fn:string-length($reqm/m1:userField2)>0)
          then &lt;fml:DC_POTW_ZAK_BUDOWY>{ data($reqm/m1:userField2) }&lt;/fml:DC_POTW_ZAK_BUDOWY>
          else ()
      }
      {if($reqm/m1:userField3 and fn:string-length($reqm/m1:userField3)>0)
          then &lt;fml:DC_ID_POLISY>{ data($reqm/m1:userField3) }&lt;/fml:DC_ID_POLISY>
          else ()
      }
      {if($reqm/m1:userField4 and fn:string-length($reqm/m1:userField4)>0)
          then &lt;fml:DC_WNIOSEK_WYKRESL_HIPO>{ data($reqm/m1:userField4) }&lt;/fml:DC_WNIOSEK_WYKRESL_HIPO>
          else ()
      }
      {if($reqm/m1:userField5 and fn:string-length($reqm/m1:userField5)>0)
          then &lt;fml:DC_DATA_PODPISANIA_AKTU_WLASNO>{ xf:mapDate($reqm/m1:userField5) }&lt;/fml:DC_DATA_PODPISANIA_AKTU_WLASNO>
          else ()
      }
      {if($reqm/m1:newAdditionalUserField2 and fn:string-length($reqm/m1:newAdditionalUserField2)>0)
          then &lt;fml:DC_HIPOTEKA_USTANOW_PRZEZ_BANK>{ data($reqm/m1:newAdditionalUserField2) }&lt;/fml:DC_HIPOTEKA_USTANOW_PRZEZ_BANK>
          else ()
      }
     
      {if($reqm/m1:newAdditionalUserField3 and fn:string-length($reqm/m1:newAdditionalUserField3)>0)
          then &lt;fml:DC_WNIOSEK_O_UTWORZENIE_KW>{ xf:booleanToTN($reqm/m1:newAdditionalUserField3) }&lt;/fml:DC_WNIOSEK_O_UTWORZENIE_KW>
          else ()
      }
      {if($reqm/m1:legalRegistrationDate and fn:string-length($reqm/m1:legalRegistrationDate)>0)
          then &lt;fml:DC_NR_KSIEGI_WIECZYSTEJ_2>{ xf:mapDate($reqm/m1:legalRegistrationDate) }&lt;/fml:DC_NR_KSIEGI_WIECZYSTEJ_2>
          else ()
      }
      {if($reqm/m1:houseNumber and fn:string-length($reqm/m1:houseNumber)>0)
          then &lt;fml:DC_NUMER_DOMU>{ data($reqm/m1:houseNumber) }&lt;/fml:DC_NUMER_DOMU>
          else ()
      }
      {if($reqm/m1:street and fn:string-length($reqm/m1:street)>0)
          then &lt;fml:DC_ULICA>{ data($reqm/m1:street) }&lt;/fml:DC_ULICA>
          else ()
      }
      {if($reqm/m1:city and fn:string-length($reqm/m1:city)>0)
          then &lt;fml:DC_MIASTO_CITY>{ data($reqm/m1:city) }&lt;/fml:DC_MIASTO_CITY>
          else ()
      }
      {if($reqm/m1:postalCode and fn:string-length($reqm/m1:postalCode)>0)
          then &lt;fml:DC_KOD_POCZTOWY>{ data($reqm/m1:postalCode) }&lt;/fml:DC_KOD_POCZTOWY>
          else ()
      }
      {if($reqm/m1:country and fn:string-length($reqm/m1:country)>0)
          then &lt;fml:DC_KRAJ>{ data($reqm/m1:country) }&lt;/fml:DC_KRAJ>
          else ()
      }
      {if($reqm/m1:district and fn:string-length($reqm/m1:district)>0)
          then &lt;fml:DC_POWIAT>{ data($reqm/m1:district) }&lt;/fml:DC_POWIAT>
          else ()
      }
      {if($reqm/m1:county and fn:string-length($reqm/m1:county)>0)
          then &lt;fml:DC_WOJEWODZTWO>{ data($reqm/m1:county) }&lt;/fml:DC_WOJEWODZTWO>
          else ()
      }
      {if($reqm/m1:userField1 and fn:string-length($reqm/m1:userField1)>0)
          then &lt;fml:DC_DOSTAR_ODPIS_KW>{ data($reqm/m1:userField1) }&lt;/fml:DC_DOSTAR_ODPIS_KW>
          else ()
      }
      {if($reqm/m1:newUsedIndicatori/m3:NewUsedType/m3:newUsedType and fn:string-length($reqm/m1:newUsedIndicatori/m3:NewUsedType/m3:newUsedType)>0)
          then &lt;fml:DC_STAN_NIERUCHOMOSCI>{ data($reqm/m1:newUsedIndicatori/m3:NewUsedType/m3:newUsedType) }&lt;/fml:DC_STAN_NIERUCHOMOSCI>
          else ()
      }
      {if($reqm/m1:propertyType/m3:PropertyType/m3:propertyType and fn:string-length($reqm/m1:propertyType/m3:PropertyType/m3:propertyType)>0)
          then &lt;fml:DC_RODZAJ_NIERUCHOMOSCI>{ data($reqm/m1:propertyType/m3:PropertyType/m3:propertyType) }&lt;/fml:DC_RODZAJ_NIERUCHOMOSCI>
          else ()
      }
      {if($reqm/m1:ownerOccupiedCode/m3:OwnerOccupiedCode/m3:ownerOccupiedCode and fn:string-length($reqm/m1:ownerOccupiedCode/m3:OwnerOccupiedCode/m3:ownerOccupiedCode)>0)
          then &lt;fml:DC_KOD_WLASCICIELA>{ data($reqm/m1:ownerOccupiedCode/m3:OwnerOccupiedCode/m3:ownerOccupiedCode) }&lt;/fml:DC_KOD_WLASCICIELA>
          else ()
      }
      {if($reqm/m1:tenure/m3:Tenure/m3:tenure and fn:string-length($reqm/m1:tenure/m3:Tenure/m3:tenure)>0)
          then &lt;fml:DC_TYTUL_PRAWNY>{ data($reqm/m1:tenure/m3:Tenure/m3:tenure) }&lt;/fml:DC_TYTUL_PRAWNY>
          else ()
      }
      {if($reqm/m1:newAdditionalUserField1/m4:CollateralMortgageAddType/m4:collateralMortgageAddType and fn:string-length($reqm/m1:newAdditionalUserField1/m4:CollateralMortgageAddType/m4:collateralMortgageAddType)>0)
          then &lt;fml:DC_HIPOTEKA_W_RAMACH_GUK >{ data($reqm/m1:newAdditionalUserField1/m4:CollateralMortgageAddType/m4:collateralMortgageAddType) }&lt;/fml:DC_HIPOTEKA_W_RAMACH_GUK >
          else ()
      }
      {if($reqm/m1:realEstateUserField1/m3:CollateralMortgagePosition/m3:collateralMortgagePosition and fn:string-length($reqm/m1:realEstateUserField1/m3:CollateralMortgagePosition/m3:collateralMortgagePosition)>0)
          then &lt;fml:DC_HIPOTEKA_POZYCJA>{ data($reqm/m1:realEstateUserField1/m3:CollateralMortgagePosition/m3:collateralMortgagePosition) }&lt;/fml:DC_HIPOTEKA_POZYCJA>
          else ()
      }

      {if($req/m1:collateralCode/m3:CollateralCode/m3:collateralCode and fn:string-length($req/m1:collateralCode/m3:CollateralCode/m3:collateralCode)>0)
          then &lt;fml:DC_KOD_ZABEZPIECZENIA>{ data($req/m1:collateralCode/m3:CollateralCode/m3:collateralCode) }&lt;/fml:DC_KOD_ZABEZPIECZENIA>
          else ()
      }
      {if($req/m1:currencyCode/m3:CurrencyCode/m3:currencyCode and fn:string-length($req/m1:currencyCode/m3:CurrencyCode/m3:currencyCode)>0)
          then &lt;fml:DC_WALUTA>{ data($req/m1:currencyCode/m3:CurrencyCode/m3:currencyCode) }&lt;/fml:DC_WALUTA>
          else ()
      }
      {if($req/m1:methodOfSecurityValuation/m3:MethodOfSecurityValuation/m3:methodOfSecurityValuation and fn:string-length($req/m1:methodOfSecurityValuation/m3:MethodOfSecurityValuation/m3:methodOfSecurityValuation)>0)
          then &lt;fml:DC_METODA_WYCENY_ZABEZP>{ data($req/m1:methodOfSecurityValuation/m3:MethodOfSecurityValuation/m3:methodOfSecurityValuation) }&lt;/fml:DC_METODA_WYCENY_ZABEZP>
          else ()
      }
      {if($req/m1:personValuatingASecurity/m3:PersonValuatingASecurity/m3:personValuatingASecurity and fn:string-length($req/m1:personValuatingASecurity/m3:PersonValuatingASecurity/m3:personValuatingASecurity)>0)
          then &lt;fml:DC_OSOBA_WYCENIAJACA_ZABEZP>{ data($req/m1:personValuatingASecurity/m3:PersonValuatingASecurity/m3:personValuatingASecurity) }&lt;/fml:DC_OSOBA_WYCENIAJACA_ZABEZP>
          else ()
      }
      {if($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber and fn:string-length($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber)>0)
          then &lt;fml:DC_BLOKADA_SRODK_NA_R_KU>{ data($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber) }&lt;/fml:DC_BLOKADA_SRODK_NA_R_KU>
          else ()
      }
      &lt;fml:DC_TYP_RELACJI>1&lt;/fml:DC_TYP_RELACJI>
      &lt;fml:DC_ENCODING>PL_MZV&lt;/fml:DC_ENCODING>
    &lt;/fml:FML32>
  }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>