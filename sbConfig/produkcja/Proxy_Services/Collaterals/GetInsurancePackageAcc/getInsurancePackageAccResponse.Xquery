<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:dictionaries.be.dcl";
declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:insurance.entities.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:insurancedict.dictionaries.be.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



declare function xf:short2bool($short as xs:int) as xs:string
{
  if ($short >= 1)
    then "true"
    else "false"
};

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0 and not (fn:substring($dateIn,1,10)="0001-01-01"))
    then true()
    else false()
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  &lt;ns4:insurancePackageAccOut>
    &lt;ns3:InsurancePackageAcc>
      {if (data($parm/NF_INSUPA_TYPE))
        then &lt;ns3:type?>{data($parm/NF_INSUPA_TYPE)}&lt;/ns3:type>
        else ()
      }
      {if (data($parm/NF_INSUPA_LINKEDACCOUNT))
        then &lt;ns3:linkedAccount?>{data($parm/NF_INSUPA_LINKEDACCOUNT)}&lt;/ns3:linkedAccount>
        else ()
      }
      {if (data($parm/NF_INSUPA_ACTUALDATE) and xf:isData(data($parm/NF_INSUPA_ACTUALDATE)))
        then &lt;ns3:actualDate?>{data($parm/NF_INSUPA_ACTUALDATE)}&lt;/ns3:actualDate>
        else ()
      }
      {if (data($parm/NF_INSUPA_STARTDATE) and xf:isData(data($parm/NF_INSUPA_STARTDATE)))
        then &lt;ns3:startDate?>{data($parm/NF_INSUPA_STARTDATE)}&lt;/ns3:startDate>
        else ()
      }
      {if (data($parm/NF_INSUPA_CLOSEDATE) and xf:isData(data($parm/NF_INSUPA_CLOSEDATE)))
        then &lt;ns3:closeDate?>{data($parm/NF_INSUPA_CLOSEDATE)}&lt;/ns3:closeDate>
        else ()
      }
      {if (data($parm/NF_INSUPA_EMPLOYEESKP))
        then &lt;ns3:employeeSKP?>{data($parm/NF_INSUPA_EMPLOYEESKP)}&lt;/ns3:employeeSKP>
        else ()
      }
      {if (data($parm/NF_INSUPA_TRANREFNUM))
        then &lt;ns3:tranRefNum?>{data($parm/NF_INSUPA_TRANREFNUM)}&lt;/ns3:tranRefNum>
        else ()
      }
      &lt;ns3:mandatoryFlag?>{xf:short2bool(data($parm/NF_INSUPA_MANDATORYFLAG))}&lt;/ns3:mandatoryFlag>
      {if (data($parm/NF_INSUPA_CREATEDBY))
        then &lt;ns3:createdBy?>{data($parm/NF_INSUPA_CREATEDBY)}&lt;/ns3:createdBy>
        else ()
      }
      {if (data($parm/NF_INSUPA_CREATIONDATE) and xf:isData(data($parm/NF_INSUPA_CREATIONDATE)))
        then &lt;ns3:creationDate?>{data($parm/NF_INSUPA_CREATIONDATE)}&lt;/ns3:creationDate>
        else ()
      }
      {if (data($parm/NF_INSUPA_MODIFIEDBY))
        then &lt;ns3:modifiedBy?>{data($parm/NF_INSUPA_MODIFIEDBY)}&lt;/ns3:modifiedBy>
        else ()
      }
      {if (data($parm/NF_INSUPA_MODIFICATIONDATE) and xf:isData(data($parm/NF_INSUPA_MODIFICATIONDATE)))
        then &lt;ns3:modificationDate?>{data($parm/NF_INSUPA_MODIFICATIONDATE)}&lt;/ns3:modificationDate>
        else ()
      }
      {if (data($parm/NF_CURREC_CURRENCYCODE))
        then &lt;ns3:currency>
               &lt;ns1:CurrencyCode>
                 &lt;ns1:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE)}&lt;/ns1:currencyCode>
               &lt;/ns1:CurrencyCode>
             &lt;/ns3:currency>
        else ()
      }
      {if (data($parm/NF_BRANCC_BRANCHCODE))
        then &lt;ns3:branch>
               &lt;ns1:BranchCode>
                 &lt;ns1:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE)}&lt;/ns1:branchCode>
               &lt;/ns1:BranchCode>
             &lt;/ns3:branch>
        else ()
      }
      {if (data($parm/NF_INSUPA_STATUS))
        then &lt;ns3:status>
               &lt;ns5:InsurancePackageStatus>
                 &lt;ns5:insurancePackageStatus>{data($parm/NF_INSUPA_STATUS)}&lt;/ns5:insurancePackageStatus>
               &lt;/ns5:InsurancePackageStatus>
             &lt;/ns3:status>
        else ()
      }
    &lt;/ns3:InsurancePackageAcc>
  &lt;/ns4:insurancePackageAccOut>
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>