<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:dictionaries.be.dcl";
declare namespace m3 = "urn:basedictionaries.be.dcl";

declare namespace xm="urn:dcl:services.alsb.datamodel";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $fml as element(FML32):=$body/FML32;

declare function xm:dateTimeFromString($dateIn as xs:string) as xs:string {
  fn:concat(fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2),"T00:00:00")
};

declare function xm:dateFromString($dateIn as xs:string) as xs:string {
  fn:concat(fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2))
};

declare function xm:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

declare function xm:isRok($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))=4)
    then true()
    else false()
};

declare function xm:strToBoolean($dateIn as xs:string) as xs:boolean {
  if ($dateIn = "1") 
    then true()
    else false()
};

&lt;soap-env:Body>
  &lt;m:invokeResponse xmlns:urn="urn:be.services.dcl">
    &lt;m:collaterals xmlns:urn1="urn:accounts.entities.be.dcl" xmlns:urn2="urn:dictionaries.be.dcl" xmlns:urn3="urn:basedictionaries.be.dcl">
    {
      let $DC_KOD_NALICZENIA_REZERWY := $fml/fml:DC_KOD_NALICZENIA_REZERWY
      let $DC_ID_ZABEZPIECZENIA := $fml/fml:DC_ID_ZABEZPIECZENIA
      let $DC_KOD_ZABEZPIECZENIA := $fml/fml:DC_KOD_ZABEZPIECZENIA
      let $DC_INFORM_DODATK_O_ZABEZP := $fml/fml:DC_INFORM_DODATK_O_ZABEZP
      let $DC_NAZWA_SKROCONA := $fml/fml:DC_NAZWA_SKROCONA
      let $DC_OPIS_ZABEZP_1_LINIA := $fml/fml:DC_OPIS_ZABEZP_1_LINIA
      let $DC_OPIS_ZABEZP_2_LINIA := $fml/fml:DC_OPIS_ZABEZP_2_LINIA
      let $DC_OPIS_ZABEZP_3_LINIA := $fml/fml:DC_OPIS_ZABEZP_3_LINIA
      let $DC_DATA_WYGAS_LUB_ZAPADALNO := $fml/fml:DC_DATA_WYGAS_LUB_ZAPADALNO
      let $DC_WYMAGANE_UBEZPIECZENIE_T_N := $fml/fml:DC_WYMAGANE_UBEZPIECZENIE_T_N
      let $DC_DATA_WAZNOSCI_UBEZP := $fml/fml:DC_DATA_WAZNOSCI_UBEZP
      let $DC_DATA_REJESTRACJI_ZABEZP := $fml/fml:DC_DATA_REJESTRACJI_ZABEZP
      let $DC_DATA_WYGAS_REJESTRACJI := $fml/fml:DC_DATA_WYGAS_REJESTRACJI
      let $DC_CZESTOTL_WYCENY := $fml/fml:DC_CZESTOTL_WYCENY
      let $DC_OKRES_WYCENY_ZABEZP := $fml/fml:DC_OKRES_WYCENY_ZABEZP
      let $DC_DATA_PIERWSZEJ_WYCENY := $fml/fml:DC_DATA_PIERWSZEJ_WYCENY
      let $DC_KOMENTARZ_DLA_WYCENY := $fml/fml:DC_KOMENTARZ_DLA_WYCENY
      let $DC_LICZBA_JEDN_ZABEZP := $fml/fml:DC_LICZBA_JEDN_ZABEZP
      let $DC_WARTOSC_JEDNOSTKOWA := $fml/fml:DC_WARTOSC_JEDNOSTKOWA
      let $DC_WARTOSC_ZABEZPIECZENIA := $fml/fml:DC_WARTOSC_ZABEZPIECZENIA
      let $DC_SYMBOL_WALUTY := $fml/fml:DC_SYMBOL_WALUTY
      let $DC_PROCENT_POMNIEJSZ_REZERWY := $fml/fml:DC_PROCENT_POMNIEJSZ_REZERWY
      let $DC_MAKS_WARTOSC_ZABEZPIECZ := $fml/fml:DC_MAKS_WARTOSC_ZABEZPIECZ
      let $DC_DATA_POPRZEDNIEJ_WYCENY := $fml/fml:DC_DATA_POPRZEDNIEJ_WYCENY
      let $DC_METODA_WYCENY_ZABEZP := $fml/fml:DC_METODA_WYCENY_ZABEZP
      let $DC_OSOBA_WYCENIAJACA_ZABEZP := $fml/fml:DC_OSOBA_WYCENIAJACA_ZABEZP
      let $DC_OPIS_SKROCONY_ZABEZP := $fml/fml:DC_OPIS_SKROCONY_ZABEZP
      let $DC_NR_KSIEGI_WIECZYSTEJ_1 := $fml/fml:DC_NR_KSIEGI_WIECZYSTEJ_1
      let $DC_NR_KSIEGI_WIECZYSTEJ_2 := $fml/fml:DC_NR_KSIEGI_WIECZYSTEJ_2
      let $DC_HIPOTEKA_POZYCJA := $fml/fml:DC_HIPOTEKA_POZYCJA
      let $DC_ROK_BUDOWY_ROK_ZAKUPU_BUD := $fml/fml:DC_ROK_BUDOWY_ROK_ZAKUPU_BUD
      let $DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK := $fml/fml:DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK
      let $DC_CENA_ZAKUPU := $fml/fml:DC_CENA_ZAKUPU
      let $DC_STAN_NIERUCHOMOSCI := $fml/fml:DC_STAN_NIERUCHOMOSCI
      let $DC_POWIERZCHNIA_DZIALKI := $fml/fml:DC_POWIERZCHNIA_DZIALKI
      let $DC_POWIERZCHNIA_BUDYNKU := $fml/fml:DC_POWIERZCHNIA_BUDYNKU
      let $DC_RODZAJ_NIERUCHOMOSCI := $fml/fml:DC_RODZAJ_NIERUCHOMOSCI
      let $DC_KOD_WLASCICIELA := $fml/fml:DC_KOD_WLASCICIELA
      let $DC_LICZBA_MIESZKANCOW := $fml/fml:DC_LICZBA_MIESZKANCOW
      let $DC_TYTUL_PRAWNY := $fml/fml:DC_TYTUL_PRAWNY
      let $DC_DATA_ZL_WN_O_WPIS_HIPO := $fml/fml:DC_DATA_ZL_WN_O_WPIS_HIPO
      let $DC_PRZEWIDYWANA_DATA_ZAK := $fml/fml:DC_PRZEWIDYWANA_DATA_ZAK
      let $DC_SAD_REJONOWY := $fml/fml:DC_SAD_REJONOWY
      let $DC_SZACOWANA_WARTOSC := $fml/fml:DC_SZACOWANA_WARTOSC
      let $DC_DATA_SPORZ_WYCENY := $fml/fml:DC_DATA_SPORZ_WYCENY
      let $DC_NAZWISKO_RZECZOZNAWCY := $fml/fml:DC_NAZWISKO_RZECZOZNAWCY
      let $DC_WARTOSC_WPISU_DO_HIPOTEKI := $fml/fml:DC_WARTOSC_WPISU_DO_HIPOTEKI
      let $DC_POTW_ZAK_BUDOWY := $fml/fml:DC_POTW_ZAK_BUDOWY
      let $DC_ID_POLISY := $fml/fml:DC_ID_POLISY
      let $DC_WNIOSEK_WYKRESL_HIPO := $fml/fml:DC_WNIOSEK_WYKRESL_HIPO
      let $DC_HIPOTEKA_W_RAMACH_GUK := $fml/fml:DC_HIPOTEKA_W_RAMACH_GUK
      let $DC_HIPOTEKA_USTANOW_PRZEZ_BANK := $fml/fml:DC_HIPOTEKA_USTANOW_PRZEZ_BANK
      let $DC_DATA_PODPISANIA_AKTU_WLASNO := $fml/fml:DC_DATA_PODPISANIA_AKTU_WLASNO 
      let $DC_WNIOSEK_UTWORZENIE_KW := $fml/fml:DC_WNIOSEK_UTWORZENIE_KW 
      let $DC_ROK_PRODUKCJI_MODELU := $fml/fml:DC_ROK_PRODUKCJI_MODELU
      let $DC_RODZAJ_SRODKA_KOMUNIK := $fml/fml:DC_RODZAJ_SRODKA_KOMUNIK
      let $DC_PRZEWL_WARUNEK_ZAWIES := $fml/fml:DC_PRZEWL_WARUNEK_ZAWIES
      let $DC_DATA_UPLYWU_BADAN_TECH := $fml/fml:DC_DATA_UPLYWU_BADAN_TECH
      let $DC_RODZAJ_ZASTAWU_PRZEWL := $fml/fml:DC_RODZAJ_ZASTAWU_PRZEWL
      let $DC_DATA_ZLOZ_WN_O_WPIS_ZAST := $fml/fml:DC_DATA_ZLOZ_WN_O_WPIS_ZAST
      let $DC_NUMER_PODWOZIA_NADWOZIA := $fml/fml:DC_NUMER_PODWOZIA_NADWOZIA
      let $DC_NUMER_SILNIKA := $fml/fml:DC_NUMER_SILNIKA
      let $DC_NUMER_REJESTRACYJNY := $fml/fml:DC_NUMER_REJESTRACYJNY
      let $DC_SPOSOB_FINANSOWANIA := $fml/fml:DC_SPOSOB_FINANSOWANIA
      let $DC_MIEJSCE_REJESTRACJI := $fml/fml:DC_MIEJSCE_REJESTRACJI
      let $DC_MARKA := $fml/fml:DC_MARKA
      let $DC_TYP_NADWOZIA := $fml/fml:DC_TYP_NADWOZIA
      let $DC_BLOKADA_SRODK_NA_R_KU := $fml/fml:DC_BLOKADA_SRODK_NA_R_KU
      let $DC_NR_RACH_OBJETY_BLOKADA_SRO  := $fml/fml:DC_NR_RACH_OBJETY_BLOKADA_SRO
      let $DC_DATA_ROZPOCZECIA_REALIZACJI := $fml/fml:DC_DATA_ROZPOCZECIA_REALIZACJI
      let $DC_JEDNOSTKA_SPRZEDAJACA := $fml/fml:DC_JEDNOSTKA_SPRZEDAJACA
      let $DC_KOSZT_SPRZEDAZY_ZABEZP  := $fml/fml:DC_KOSZT_SPRZEDAZY_ZABEZP
      let $DC_DATA_ZAKONCZENIA_REALIZACJI := $fml/fml:DC_DATA_ZAKONCZENIA_REALIZACJI
      let $DC_WALUTA  := $fml/fml:DC_WALUTA
      let $DC_KWOTA := $fml/fml:DC_KWOTA
      let $DC_KWOTA_OBEC_ZABEZP   := $fml/fml:DC_KWOTA_OBEC_ZABEZP 
      let $DC_KWOTA_DOST_DO_ZABEZP := $fml/fml:DC_KWOTA_DOST_DO_ZABEZP 
      let $DC_PIERW_WART_ZABEZP := $fml/fml:DC_PIERW_WART_ZABEZP
      let $DC_DATA_WAGAS_ZABEZP := $fml/fml:DC_DATA_WAGAS_ZABEZP
      let $DC_NR_UMOWY_PORECZ := $fml/fml:DC_NR_UMOWY_PORECZ
      let $DC_NUMER_DOMU  := $fml/fml:DC_NUMER_DOMU
      let $DC_ULICA := $fml/fml:DC_ULICA
      let $DC_MIASTO_STATE  := $fml/fml:DC_MIASTO_STATE
      let $DC_KOD_POCZTOWY  := $fml/fml:DC_KOD_POCZTOWY
      let $DC_WOJEWODZTWO := $fml/fml:DC_WOJEWODZTWO
      let $DC_POWIAT  := $fml/fml:DC_POWIAT
      let $DC_KRAJ  := $fml/fml:DC_KRAJ
      let $DC_DOSTAR_ODPIS_KW := $fml/fml:DC_DOSTAR_ODPIS_KW

      for $it at $p in $fml/fml:DC_ID_ZABEZPIECZENIA
      return
        &lt;m1:Collateral>
          {if($DC_KOD_NALICZENIA_REZERWY[$p] and xm:isData($DC_KOD_NALICZENIA_REZERWY[$p]))
             then &lt;m1:locationCode>{data($DC_KOD_NALICZENIA_REZERWY[$p])}&lt;/m1:locationCode>
             else ()
          }
          {if($DC_OPIS_ZABEZP_1_LINIA[$p] and xm:isData($DC_OPIS_ZABEZP_1_LINIA[$p]))
             then &lt;m1:collateralDescription>{ concat(string(data($DC_OPIS_ZABEZP_1_LINIA[$p])),string(data($DC_OPIS_ZABEZP_2_LINIA[$p])),string(data($DC_OPIS_ZABEZP_3_LINIA[$p]))) }&lt;/m1:collateralDescription>
             else ()
          }
          {if($DC_WARTOSC_ZABEZPIECZENIA[$p] and xm:isData($DC_WARTOSC_ZABEZPIECZENIA[$p]))
             then &lt;m1:collateralValue>{ data($DC_WARTOSC_ZABEZPIECZENIA[$p]) }&lt;/m1:collateralValue>
             else ()
          }
          {if($DC_DATA_WYGAS_LUB_ZAPADALNO[$p] and xm:isData($DC_DATA_WYGAS_LUB_ZAPADALNO[$p]))
             then &lt;m1:expirationOrMaturityDate>{ xm:dateFromString($DC_DATA_WYGAS_LUB_ZAPADALNO[$p]) }&lt;/m1:expirationOrMaturityDate>
             else ()
          }
          {if($DC_WYMAGANE_UBEZPIECZENIE_T_N[$p] and xm:isData($DC_WYMAGANE_UBEZPIECZENIE_T_N[$p]))
             then &lt;m1:insuranceRequired>{ xm:strToBoolean($DC_WYMAGANE_UBEZPIECZENIE_T_N[$p]) }&lt;/m1:insuranceRequired>
             else ()
          }
          {if($DC_DATA_WAZNOSCI_UBEZP[$p] and xm:isData($DC_DATA_WAZNOSCI_UBEZP[$p]))
             then &lt;m1:insuranceExpiryDate>{ xm:dateFromString($DC_DATA_WAZNOSCI_UBEZP[$p]) }&lt;/m1:insuranceExpiryDate>
             else ()
          }
          {if($DC_DATA_REJESTRACJI_ZABEZP[$p] and xm:isData($DC_DATA_REJESTRACJI_ZABEZP[$p]))
             then &lt;m1:legalClaimRegisteredDate>{ xm:dateFromString($DC_DATA_REJESTRACJI_ZABEZP[$p]) }&lt;/m1:legalClaimRegisteredDate>
             else ()
          }
          {if($DC_DATA_WYGAS_REJESTRACJI[$p] and xm:isData($DC_DATA_WYGAS_REJESTRACJI[$p]))
             then &lt;m1:legalClaimExpiryDate>{ xm:dateFromString($DC_DATA_WYGAS_REJESTRACJI[$p]) }&lt;/m1:legalClaimExpiryDate>
             else ()
          }
          {if($DC_CZESTOTL_WYCENY[$p] and xm:isData($DC_CZESTOTL_WYCENY[$p]))
             then &lt;m1:reviewFrequency>{ data($DC_CZESTOTL_WYCENY[$p]) }&lt;/m1:reviewFrequency>
             else ()
          }
          {if($DC_OKRES_WYCENY_ZABEZP[$p] and xm:isData($DC_OKRES_WYCENY_ZABEZP[$p]))
             then &lt;m1:reviewPeriod>{ data($DC_OKRES_WYCENY_ZABEZP[$p]) }&lt;/m1:reviewPeriod>
             else ()
          }
          {if($DC_DATA_PIERWSZEJ_WYCENY[$p] and xm:isData($DC_DATA_PIERWSZEJ_WYCENY[$p]))
             then &lt;m1:firstReviewDate>{ xm:dateTimeFromString($DC_DATA_PIERWSZEJ_WYCENY[$p]) }&lt;/m1:firstReviewDate>
             else ()
          }
          {if($DC_KOMENTARZ_DLA_WYCENY[$p] and xm:isData($DC_KOMENTARZ_DLA_WYCENY[$p]))
             then &lt;m1:reviewComments>{ data($DC_KOMENTARZ_DLA_WYCENY[$p]) }&lt;/m1:reviewComments>
             else ()
          }
          {if($DC_LICZBA_JEDN_ZABEZP[$p] and xm:isData($DC_LICZBA_JEDN_ZABEZP[$p]))
             then &lt;m1:numberOfUnits>{ data($DC_LICZBA_JEDN_ZABEZP[$p]) }&lt;/m1:numberOfUnits>
             else ()
          }
          {if($DC_WARTOSC_JEDNOSTKOWA[$p] and xm:isData($DC_WARTOSC_JEDNOSTKOWA[$p]))
             then &lt;m1:unitPrice>{ data($DC_WARTOSC_JEDNOSTKOWA[$p]) }&lt;/m1:unitPrice>
             else ()
          }
          {if($DC_PROCENT_POMNIEJSZ_REZERWY[$p] and xm:isData($DC_PROCENT_POMNIEJSZ_REZERWY[$p]))
             then &lt;m1:marginPercentage>{ data($DC_PROCENT_POMNIEJSZ_REZERWY[$p]) }&lt;/m1:marginPercentage>
             else ()
          }
          {if($DC_MAKS_WARTOSC_ZABEZPIECZ[$p] and xm:isData($DC_MAKS_WARTOSC_ZABEZPIECZ[$p]))
             then &lt;m1:maximumCollateralValue>{ data($DC_MAKS_WARTOSC_ZABEZPIECZ[$p]) }&lt;/m1:maximumCollateralValue>
             else ()
          }
          {if($DC_DATA_POPRZEDNIEJ_WYCENY[$p] and xm:isData($DC_DATA_POPRZEDNIEJ_WYCENY[$p]))
             then &lt;m1:dateLastPriced>{ xm:dateFromString($DC_DATA_POPRZEDNIEJ_WYCENY[$p]) }&lt;/m1:dateLastPriced>
             else ()
          }
          {if($DC_ID_ZABEZPIECZENIA[$p] and xm:isData($DC_ID_ZABEZPIECZENIA[$p]))
             then &lt;m1:collateralItemNumber>{ data($DC_ID_ZABEZPIECZENIA[$p]) }&lt;/m1:collateralItemNumber>
             else ()
          }
          {if($DC_INFORM_DODATK_O_ZABEZP[$p] and xm:isData($DC_INFORM_DODATK_O_ZABEZP[$p]))
             then &lt;m1:commodityIdCusip>{ data($DC_INFORM_DODATK_O_ZABEZP[$p]) }&lt;/m1:commodityIdCusip>
             else ()
          }
          {if($DC_NR_RACH_OBJETY_BLOKADA_SRO[$p] and xm:isData($DC_NR_RACH_OBJETY_BLOKADA_SRO[$p]))
             then &lt;m1:itemReferenceNumber>{ data($DC_NR_RACH_OBJETY_BLOKADA_SRO[$p]) }&lt;/m1:itemReferenceNumber>
             else ()
          }
          {if($DC_INFORM_DODATK_O_ZABEZP[$p] and xm:isData($DC_INFORM_DODATK_O_ZABEZP[$p]))
             then &lt;m1:safekeepingReceiptNbr>{ data($DC_INFORM_DODATK_O_ZABEZP[$p]) }&lt;/m1:safekeepingReceiptNbr>
             else ()
          }
          {if($DC_NAZWA_SKROCONA[$p] and xm:isData($DC_NAZWA_SKROCONA[$p]))
             then &lt;m1:shortDescription>{ data($DC_NAZWA_SKROCONA[$p]) }&lt;/m1:shortDescription>
             else ()
          }
          {if($DC_DATA_ROZPOCZECIA_REALIZACJI[$p] and xm:isData($DC_DATA_ROZPOCZECIA_REALIZACJI[$p]))
             then &lt;m1:startDateOfSecLiquidat>{ xm:dateFromString($DC_DATA_ROZPOCZECIA_REALIZACJI[$p]) }&lt;/m1:startDateOfSecLiquidat>
             else ()
          }
          {if($DC_KOSZT_SPRZEDAZY_ZABEZP[$p] and xm:isData($DC_KOSZT_SPRZEDAZY_ZABEZP[$p]))
             then &lt;m1:costOfSalesOfSecurity>{ data($DC_KOSZT_SPRZEDAZY_ZABEZP[$p]) }&lt;/m1:costOfSalesOfSecurity>
             else ()
          }
          {if($DC_DATA_ZAKONCZENIA_REALIZACJI[$p] and xm:isData($DC_DATA_ZAKONCZENIA_REALIZACJI[$p]))
             then &lt;m1:endDateOfSecLiquidat>{ xm:dateFromString($DC_DATA_ZAKONCZENIA_REALIZACJI[$p]) }&lt;/m1:endDateOfSecLiquidat>
             else ()
          }
          {if($DC_WALUTA[$p] and xm:isData($DC_WALUTA[$p]))
             then &lt;m1:ccy>{ data($DC_WALUTA[$p]) }&lt;/m1:ccy>
             else ()
          }
          {if($DC_KWOTA[$p] and xm:isData($DC_KWOTA[$p]))
             then &lt;m1:amt>{ data($DC_KWOTA[$p]) }&lt;/m1:amt>
             else ()
          }
          {if($DC_KWOTA_OBEC_ZABEZP[$p] and xm:isData($DC_KWOTA_OBEC_ZABEZP[$p]))
             then &lt;m1:amountCurrentlyPledged>{ data($DC_KWOTA_OBEC_ZABEZP[$p]) }&lt;/m1:amountCurrentlyPledged>
             else ()
          } 
          {if($DC_KWOTA_DOST_DO_ZABEZP[$p] and xm:isData($DC_KWOTA_DOST_DO_ZABEZP[$p]))
             then &lt;m1:availableToPledge>{ data($DC_KWOTA_DOST_DO_ZABEZP[$p]) }&lt;/m1:availableToPledge>
             else ()
          } 
          {if($DC_PIERW_WART_ZABEZP[$p] and xm:isData($DC_PIERW_WART_ZABEZP[$p]))
             then &lt;m1:originalCollateralValue>{ data($DC_PIERW_WART_ZABEZP[$p]) }&lt;/m1:originalCollateralValue>
             else ()
          }
          {if($DC_DATA_WAGAS_ZABEZP[$p] and xm:isData($DC_DATA_WAGAS_ZABEZP[$p]))
             then &lt;m1:collateralInactiveDate>{ xm:dateFromString($DC_DATA_WAGAS_ZABEZP[$p]) }&lt;/m1:collateralInactiveDate>
             else ()
          }
          {if($DC_NR_UMOWY_PORECZ[$p] and xm:isData($DC_NR_UMOWY_PORECZ[$p]))
             then &lt;m1:userField6>{ data($DC_NR_UMOWY_PORECZ[$p]) }&lt;/m1:userField6>
             else ()
          }

          &lt;m1:collateralMortgage>
            &lt;m1:CollateralMortgage>
              {if($DC_OPIS_SKROCONY_ZABEZP[$p] and xm:isData($DC_OPIS_SKROCONY_ZABEZP[$p]))
                 then &lt;m1:shortLegalDescription>{ data($DC_OPIS_SKROCONY_ZABEZP[$p]) }&lt;/m1:shortLegalDescription>
                 else ()
              }
              {if($DC_NR_KSIEGI_WIECZYSTEJ_1[$p] and xm:isData($DC_NR_KSIEGI_WIECZYSTEJ_1[$p]))
                 then &lt;m1:legalRegistrationNumber>{data($DC_NR_KSIEGI_WIECZYSTEJ_1[$p]) }&lt;/m1:legalRegistrationNumber>
                 else ()
              }
              {if($DC_ROK_BUDOWY_ROK_ZAKUPU_BUD[$p] and xm:isRok($DC_ROK_BUDOWY_ROK_ZAKUPU_BUD[$p]))
                 then &lt;m1:yearBuilt>{ data($DC_ROK_BUDOWY_ROK_ZAKUPU_BUD[$p]) }&lt;/m1:yearBuilt>
                 else ()
              }
              {if($DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK[$p] and xm:isRok($DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK[$p]))
                 then &lt;m1:yearPurchased>{ data($DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK[$p]) }&lt;/m1:yearPurchased>
                 else ()
              }
              {if($DC_CENA_ZAKUPU[$p] and xm:isData($DC_CENA_ZAKUPU[$p]))
                 then &lt;m1:purchasePrice>{ data($DC_CENA_ZAKUPU[$p]) }&lt;/m1:purchasePrice>
                 else ()
              }
              {if($DC_POWIERZCHNIA_DZIALKI[$p] and xm:isData($DC_POWIERZCHNIA_DZIALKI[$p]))
                 then &lt;m1:lotSize>{ data($DC_POWIERZCHNIA_DZIALKI[$p]) }&lt;/m1:lotSize>
                 else ()
              }
              {if($DC_POWIERZCHNIA_BUDYNKU[$p] and xm:isData($DC_POWIERZCHNIA_BUDYNKU[$p]))
                 then &lt;m1:squareFeet>{ data($DC_POWIERZCHNIA_BUDYNKU[$p]) }&lt;/m1:squareFeet>
                 else ()
              }
              {if($DC_LICZBA_MIESZKANCOW[$p] and xm:isData($DC_LICZBA_MIESZKANCOW[$p]))
                 then &lt;m1:numberOfResidents>{ data($DC_LICZBA_MIESZKANCOW[$p]) }&lt;/m1:numberOfResidents>
                 else ()
              }
              {if($DC_DATA_ZL_WN_O_WPIS_HIPO[$p] and xm:isData($DC_DATA_ZL_WN_O_WPIS_HIPO[$p]))
                 then &lt;m1:leaseExpirationDate>{ xm:dateFromString($DC_DATA_ZL_WN_O_WPIS_HIPO[$p]) }&lt;/m1:leaseExpirationDate>
                 else ()
              }
              {if($DC_PRZEWIDYWANA_DATA_ZAK[$p] and xm:isData($DC_PRZEWIDYWANA_DATA_ZAK[$p]))
                 then &lt;m1:dateInspected>{ xm:dateFromString($DC_PRZEWIDYWANA_DATA_ZAK[$p]) }&lt;/m1:dateInspected>
                 else ()
              }
              {if($DC_SAD_REJONOWY[$p] and xm:isData($DC_SAD_REJONOWY[$p]))
                 then &lt;m1:subdivision>{ data($DC_SAD_REJONOWY[$p]) }&lt;/m1:subdivision>
                 else ()
              }
              {if($DC_SZACOWANA_WARTOSC[$p] and xm:isData($DC_SZACOWANA_WARTOSC[$p]))
                 then &lt;m1:appraisedValue>{ data($DC_SZACOWANA_WARTOSC[$p]) }&lt;/m1:appraisedValue>
                 else ()
              }
              {if($DC_DATA_SPORZ_WYCENY[$p] and xm:isData($DC_DATA_SPORZ_WYCENY[$p]))
                 then &lt;m1:appraisalDate>{ xm:dateFromString($DC_DATA_SPORZ_WYCENY[$p]) }&lt;/m1:appraisalDate>
                 else ()
              }
              {if($DC_NAZWISKO_RZECZOZNAWCY[$p] and xm:isData($DC_NAZWISKO_RZECZOZNAWCY[$p]))
                 then &lt;m1:appraiserName>{ data($DC_NAZWISKO_RZECZOZNAWCY[$p]) }&lt;/m1:appraiserName>
                 else ()
              }
              {if($DC_WARTOSC_WPISU_DO_HIPOTEKI[$p] and xm:isData($DC_WARTOSC_WPISU_DO_HIPOTEKI[$p]))
                 then &lt;m1:mortgagesPayablePrior>{ data($DC_WARTOSC_WPISU_DO_HIPOTEKI[$p]) }&lt;/m1:mortgagesPayablePrior>
                 else ()
              }
              {if($DC_POTW_ZAK_BUDOWY[$p] and xm:isData($DC_POTW_ZAK_BUDOWY[$p]))
                 then &lt;m1:userField2>{ data($DC_POTW_ZAK_BUDOWY[$p]) }&lt;/m1:userField2>
                 else ()
              }
              {if($DC_ID_POLISY[$p] and xm:isData($DC_ID_POLISY[$p]))
                 then &lt;m1:userField3>{ data($DC_ID_POLISY[$p]) }&lt;/m1:userField3>
                 else ()
              }
              {if($DC_WNIOSEK_WYKRESL_HIPO[$p] and xm:isData($DC_WNIOSEK_WYKRESL_HIPO[$p]))
                 then &lt;m1:userField4>{ data($DC_WNIOSEK_WYKRESL_HIPO[$p]) }&lt;/m1:userField4>
                 else ()
              }
              {if($DC_DATA_PODPISANIA_AKTU_WLASNO[$p] and xm:isData($DC_DATA_PODPISANIA_AKTU_WLASNO[$p]))
                 then &lt;m1:userField5>{ xm:dateFromString($DC_DATA_PODPISANIA_AKTU_WLASNO[$p]) }&lt;/m1:userField5>
                 else ()
              }
              {if($DC_HIPOTEKA_USTANOW_PRZEZ_BANK[$p] and xm:isData($DC_HIPOTEKA_USTANOW_PRZEZ_BANK[$p]))
                 then &lt;m1:newAdditionalUserField2>{ data($DC_HIPOTEKA_USTANOW_PRZEZ_BANK[$p]) }&lt;/m1:newAdditionalUserField2>
                 else ()
              }
              {if($DC_HIPOTEKA_W_RAMACH_GUK[$p] and xm:isData($DC_HIPOTEKA_W_RAMACH_GUK[$p]))
                 then &lt;m1:newAdditionalUserField1>{ xm:dateFromString($DC_HIPOTEKA_W_RAMACH_GUK[$p]) }&lt;/m1:newAdditionalUserField1>
                 else ()
              }
              {if($DC_WNIOSEK_UTWORZENIE_KW[$p] and xm:isData($DC_WNIOSEK_UTWORZENIE_KW[$p]))
                 then &lt;m1:newAdditionalUserField3>{ xm:strToBoolean($DC_WNIOSEK_UTWORZENIE_KW[$p]) }&lt;/m1:newAdditionalUserField3>
                 else ()
              }    
              {if($DC_NR_KSIEGI_WIECZYSTEJ_2[$p] and xm:isData($DC_NR_KSIEGI_WIECZYSTEJ_2[$p]))
                 then &lt;m1:legalRegistrationDate>{ xm:dateFromString($DC_NR_KSIEGI_WIECZYSTEJ_2[$p]) }&lt;/m1:legalRegistrationDate>
                 else ()
              }
              {if($DC_NUMER_DOMU[$p] and xm:isData($DC_NUMER_DOMU[$p]))
                 then &lt;m1:houseNumber>{ data($DC_NUMER_DOMU[$p]) }&lt;/m1:houseNumber>
                 else ()
              }
              {if($DC_ULICA[$p] and xm:isData($DC_ULICA[$p]))
                 then &lt;m1:street>{ data($DC_ULICA[$p]) }&lt;/m1:street>
                 else ()
              }
              {if($DC_MIASTO_STATE[$p] and xm:isData($DC_MIASTO_STATE[$p]))
                 then &lt;m1:city>{ data($DC_MIASTO_STATE[$p]) }&lt;/m1:city>
                 else ()
              }
              {if($DC_KOD_POCZTOWY[$p] and xm:isData($DC_KOD_POCZTOWY[$p]))
                 then &lt;m1:postalCode>{ data($DC_KOD_POCZTOWY[$p]) }&lt;/m1:postalCode>
                 else ()
              }
              {if($DC_KRAJ[$p] and xm:isData($DC_KRAJ[$p]))
                 then &lt;m1:country>{ data($DC_KRAJ[$p]) }&lt;/m1:country>
                 else ()
              }
              {if($DC_POWIAT[$p] and xm:isData($DC_POWIAT[$p]))
                 then &lt;m1:district>{ data($DC_POWIAT[$p]) }&lt;/m1:district>
                 else ()
              }
              {if($DC_WOJEWODZTWO[$p] and xm:isData($DC_WOJEWODZTWO[$p]))
                 then &lt;m1:county>{ data($DC_WOJEWODZTWO[$p]) }&lt;/m1:county>
                 else ()
              }
              {if($DC_DOSTAR_ODPIS_KW[$p] and xm:isData($DC_DOSTAR_ODPIS_KW[$p]))
                 then &lt;m1:userField1>{ data($DC_DOSTAR_ODPIS_KW[$p]) }&lt;/m1:userField1>
                 else ()
              }        
              {if($DC_STAN_NIERUCHOMOSCI[$p] and xm:isData($DC_STAN_NIERUCHOMOSCI[$p]))
                 then &lt;m1:newUsedIndicatori>&lt;m2:NewUsedType>&lt;m2:newUsedType>{ data($DC_STAN_NIERUCHOMOSCI[$p]) }&lt;/m2:newUsedType>&lt;/m2:NewUsedType>&lt;/m1:newUsedIndicatori>
                 else ()
              }
              {if($DC_RODZAJ_NIERUCHOMOSCI[$p] and xm:isData($DC_RODZAJ_NIERUCHOMOSCI[$p]))
                 then &lt;m1:propertyType>&lt;m2:PropertyType>&lt;m2:propertyType>{ data($DC_RODZAJ_NIERUCHOMOSCI[$p]) }&lt;/m2:propertyType>&lt;/m2:PropertyType>&lt;/m1:propertyType>
                 else ()
              }
              {if($DC_KOD_WLASCICIELA[$p] and xm:isData($DC_KOD_WLASCICIELA[$p]))
                 then &lt;m1:ownerOccupiedCode>&lt;m2:OwnerOccupiedCode>&lt;m2:ownerOccupiedCode>{ data($DC_KOD_WLASCICIELA[$p]) }&lt;/m2:ownerOccupiedCode>&lt;/m2:OwnerOccupiedCode>&lt;/m1:ownerOccupiedCode>
                 else ()
              }
              {if($DC_TYTUL_PRAWNY[$p] and xm:isData($DC_TYTUL_PRAWNY[$p]))
                 then &lt;m1:tenure>&lt;m2:Tenure>&lt;m2:tenure>{ data($DC_TYTUL_PRAWNY[$p]) }&lt;/m2:tenure>&lt;/m2:Tenure>&lt;/m1:tenure>
                 else ()
              }
              {if($DC_HIPOTEKA_POZYCJA[$p] and xm:isData($DC_HIPOTEKA_POZYCJA[$p]))
                 then &lt;m1:realEstateUserField1>&lt;m2:CollateralMortgagePosition>&lt;m2:collateralMortgagePosition>{ data($DC_HIPOTEKA_POZYCJA[$p]) }&lt;/m2:collateralMortgagePosition>&lt;/m2:CollateralMortgagePosition>&lt;/m1:realEstateUserField1>
                 else ()
              }
            &lt;/m1:CollateralMortgage>
          &lt;/m1:collateralMortgage>

          &lt;m1:collateralPledge>
            &lt;m1:CollateralCar>
              {if($DC_ROK_PRODUKCJI_MODELU[$p] and xm:isRok($DC_ROK_PRODUKCJI_MODELU[$p]))
                 then &lt;m1:modelYear>{ data($DC_ROK_PRODUKCJI_MODELU[$p]) }&lt;/m1:modelYear>
                 else ()
              }
              {if($DC_PRZEWL_WARUNEK_ZAWIES[$p] and xm:isData($DC_PRZEWL_WARUNEK_ZAWIES[$p]))
                 then &lt;m1:rentalDemoFlag>{ xm:strToBoolean($DC_PRZEWL_WARUNEK_ZAWIES[$p]) }&lt;/m1:rentalDemoFlag>
                 else ()
              }
              {if($DC_DATA_UPLYWU_BADAN_TECH[$p] and xm:isData($DC_DATA_UPLYWU_BADAN_TECH[$p]))
                 then &lt;m1:vehicleLicenseExpiryDate>{ xm:dateFromString($DC_DATA_UPLYWU_BADAN_TECH[$p]) }&lt;/m1:vehicleLicenseExpiryDate>
                 else ()
              }
              {if($DC_DATA_ZLOZ_WN_O_WPIS_ZAST[$p] and xm:isData($DC_DATA_ZLOZ_WN_O_WPIS_ZAST[$p]))
                 then &lt;m1:firstRegistrationDate>{ xm:dateFromString($DC_DATA_ZLOZ_WN_O_WPIS_ZAST[$p]) }&lt;/m1:firstRegistrationDate>
                 else ()
              }
              {if($DC_NUMER_PODWOZIA_NADWOZIA[$p] and xm:isData($DC_NUMER_PODWOZIA_NADWOZIA[$p]))
                 then &lt;m1:serialNumber>{ data($DC_NUMER_PODWOZIA_NADWOZIA[$p]) }&lt;/m1:serialNumber>
                 else ()
              }
              {if($DC_NUMER_SILNIKA[$p] and xm:isData($DC_NUMER_SILNIKA[$p]))
                 then &lt;m1:motorNumber>{ data($DC_NUMER_SILNIKA[$p]) }&lt;/m1:motorNumber>
                 else ()
              }
              {if($DC_NUMER_REJESTRACYJNY[$p] and xm:isData($DC_NUMER_REJESTRACYJNY[$p]))
                 then &lt;m1:plateNumber>{ data($DC_NUMER_REJESTRACYJNY[$p]) }&lt;/m1:plateNumber>
                 else ()
              }
              {if($DC_RODZAJ_SRODKA_KOMUNIK[$p] and xm:isData($DC_RODZAJ_SRODKA_KOMUNIK[$p]))
                 then &lt;m1:conditionFlag>&lt;m2:ConditionFlag>&lt;m2:conditionFlag>{ data($DC_RODZAJ_SRODKA_KOMUNIK[$p]) }&lt;/m2:conditionFlag>&lt;/m2:ConditionFlag>&lt;/m1:conditionFlag>
                 else ()
              }
              {if($DC_RODZAJ_ZASTAWU_PRZEWL[$p] and xm:isData($DC_RODZAJ_ZASTAWU_PRZEWL[$p]))
                 then &lt;m1:licenseTypeCode>&lt;m2:LicenseTypeCode>&lt;m2:licenseTypeCode>{ data($DC_RODZAJ_ZASTAWU_PRZEWL[$p]) }&lt;/m2:licenseTypeCode>&lt;/m2:LicenseTypeCode>&lt;/m1:licenseTypeCode>
                 else ()
              }
              {if($DC_SPOSOB_FINANSOWANIA[$p] and xm:isData($DC_SPOSOB_FINANSOWANIA[$p]))
                 then &lt;m1:financeTypeCode>&lt;m2:FinanceTypeCode>&lt;m2:financeTypeCode>{ data($DC_SPOSOB_FINANSOWANIA[$p]) }&lt;/m2:financeTypeCode>&lt;/m2:FinanceTypeCode>&lt;/m1:financeTypeCode>
                 else ()
              }
              {if($DC_MIEJSCE_REJESTRACJI[$p] and xm:isData($DC_MIEJSCE_REJESTRACJI[$p]))
                 then &lt;m1:registrationState>&lt;m3:BaseState>&lt;m3:state>{ data($DC_MIEJSCE_REJESTRACJI[$p]) }&lt;/m3:state>&lt;/m3:BaseState>&lt;/m1:registrationState>
                 else ()
              }
              {if($DC_MARKA[$p] and xm:isData($DC_MARKA[$p]))
                 then &lt;m1:makeCode>&lt;m2:MakeCode>&lt;m2:makeCode>{ data($DC_MARKA[$p]) }&lt;/m2:makeCode>&lt;/m2:MakeCode>&lt;/m1:makeCode>
                 else ()
              }
              {if($DC_TYP_NADWOZIA[$p] and xm:isData($DC_TYP_NADWOZIA[$p]))
                 then &lt;m1:bodyCode>&lt;m2:BodyCode>&lt;m2:bodyCode>{ data($DC_TYP_NADWOZIA[$p]) }&lt;/m2:bodyCode>&lt;/m2:BodyCode>&lt;/m1:bodyCode>
                 else ()
              }
            &lt;/m1:CollateralCar>
          &lt;/m1:collateralPledge>

          {if($DC_KOD_ZABEZPIECZENIA[$p] and xm:isData($DC_KOD_ZABEZPIECZENIA[$p]))
             then &lt;m1:collateralCode>&lt;m2:CollateralCode>&lt;m2:collateralCode>{ data($DC_KOD_ZABEZPIECZENIA[$p]) }&lt;/m2:collateralCode>&lt;/m2:CollateralCode>&lt;/m1:collateralCode>
             else ()
          }
          {if($DC_SYMBOL_WALUTY[$p] and xm:isData($DC_SYMBOL_WALUTY[$p]))
             then &lt;m1:currencyCode>&lt;m2:CurrencyCode>&lt;m2:currencyCode>{ data($DC_SYMBOL_WALUTY[$p]) }&lt;/m2:currencyCode>&lt;/m2:CurrencyCode>&lt;/m1:currencyCode>
             else ()
          }
          {if($DC_METODA_WYCENY_ZABEZP[$p] and xm:isData($DC_METODA_WYCENY_ZABEZP[$p]) and data($DC_METODA_WYCENY_ZABEZP[$p]) != 0)
             then &lt;m1:methodOfSecurityValuation>&lt;m2:MethodOfSecurityValuation>&lt;m2:methodOfSecurityValuation>{ data($DC_METODA_WYCENY_ZABEZP[$p]) }&lt;/m2:methodOfSecurityValuation>&lt;/m2:MethodOfSecurityValuation>&lt;/m1:methodOfSecurityValuation>
             else ()
          }
          {if($DC_OSOBA_WYCENIAJACA_ZABEZP[$p] and xm:isData($DC_OSOBA_WYCENIAJACA_ZABEZP[$p]))
             then &lt;m1:personValuatingASecurity>&lt;m2:PersonValuatingASecurity>&lt;m2:personValuatingASecurity>{ data($DC_OSOBA_WYCENIAJACA_ZABEZP[$p]) }&lt;/m2:personValuatingASecurity>&lt;/m2:PersonValuatingASecurity>&lt;/m1:personValuatingASecurity>
             else ()
          }
          {if($DC_BLOKADA_SRODK_NA_R_KU[$p] and xm:isData($DC_BLOKADA_SRODK_NA_R_KU[$p]))
             then &lt;m1:itemApplicationNumber>&lt;m2:ApplicationNumber>&lt;m2:applicationNumber>{ data($DC_BLOKADA_SRODK_NA_R_KU[$p]) }&lt;/m2:applicationNumber>&lt;/m2:ApplicationNumber>&lt;/m1:itemApplicationNumber>
             else ()
          }
          {if($DC_JEDNOSTKA_SPRZEDAJACA[$p] and xm:isData($DC_JEDNOSTKA_SPRZEDAJACA[$p]))
             then &lt;m1:entity>&lt;m2:CollateralEntity>&lt;m2:collateralEntity>{ data($DC_JEDNOSTKA_SPRZEDAJACA[$p]) }&lt;/m2:collateralEntity>&lt;/m2:CollateralEntity>&lt;/m1:entity>
             else ()
          }
        &lt;/m1:Collateral>
    }
    &lt;/m:collaterals>
  &lt;/m:invokeResponse>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>