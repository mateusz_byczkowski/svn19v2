<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCustomerForAdvisoryRequest($req as element(m:GetCustomerForAdvisoryRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if ($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
				else ()
			}
			{
				if ($req/m:IdSpolki)
					then if (data($req/m:IdSpolki))
						then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
						else &lt;fml:CI_ID_SPOLKI>{0}&lt;/fml:CI_ID_SPOLKI>
				else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body>
{ xf:mapGetCustomerForAdvisoryRequest($body/m:GetCustomerForAdvisoryRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>