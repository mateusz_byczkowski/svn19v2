<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapChgAdvDataRequest($req as element(m:ChgAdvDataRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if ($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
				else ()
			}
			{
				if ($req/m:IdSpolki)
					then if (data($req/m:IdSpolki))
						then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
						else &lt;fml:CI_ID_SPOLKI>{0}&lt;/fml:CI_ID_SPOLKI>
				else ()
			}
			{
				if ($req/m:StatusAktywnosciDoradztwa)
					then &lt;fml:CI_STATUS_AKT_DORADZTWA>{ data($req/m:StatusAktywnosciDoradztwa) }&lt;/fml:CI_STATUS_AKT_DORADZTWA>
				else ()
			}
			{
				if ($req/m:ProfilInwestycyjny)
					then if (data($req/m:ProfilInwestycyjny))
						then &lt;fml:CI_PROFIL_INWESTYCYJNY>{ data($req/m:ProfilInwestycyjny) }&lt;/fml:CI_PROFIL_INWESTYCYJNY>
						else &lt;fml:CI_PROFIL_INWESTYCYJNY>{0}&lt;/fml:CI_PROFIL_INWESTYCYJNY>
				else ()
			}
			{
				if ($req/m:DataAktywacjiUslugi)
					then &lt;fml:CI_DATA_AKT_DORADZTWA>{ data($req/m:DataAktywacjiUslugi) }&lt;/fml:CI_DATA_AKT_DORADZTWA>
				else ()
			}
			{
				if ($req/m:DataOstatniejUslugi)
					then &lt;fml:CI_DATA_ZAK_DORADZTWA>{ data($req/m:DataOstatniejUslugi) }&lt;/fml:CI_DATA_ZAK_DORADZTWA>
				else ()
			}
			{
				if ($req/m:IdentyfikatorDoradztwa)
					then &lt;fml:CI_ID_DORADZTWA>{ data($req/m:IdentyfikatorDoradztwa) }&lt;/fml:CI_ID_DORADZTWA>
				else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapChgAdvDataRequest($body/m:ChgAdvDataRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>