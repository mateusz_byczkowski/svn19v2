<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2010-03-15 PK NP1696 Dodanie pól wyjściowych</con:description>
  <con:xquery><![CDATA[(: Change log
v.1.1 2010-03-15 PK NP1696 Dodanie pól wyjściowych
:)

declare namespace fml="";
declare namespace ns1="urn:be.services.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

<soap:Body>
  <fml:FML32>
    <B_MSHEAD_MSGID?>{data($header/ns1:header/ns1:msgHeader/ns1:msgId)}</B_MSHEAD_MSGID>
    <B_IDENTYFIKATOR1>GLC001</B_IDENTYFIKATOR1>
    <B_EXTRA>3</B_EXTRA> (: 1.1 :)
    <B_ZNACZNIK>M</B_ZNACZNIK>
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>