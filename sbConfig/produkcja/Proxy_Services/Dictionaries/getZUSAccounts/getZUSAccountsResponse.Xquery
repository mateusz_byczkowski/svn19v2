<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
&lt;ns0:dicts>
  {
    let $miasto := $parm/B_MIASTO
    let $nazwa := $parm/B_NAZWA
    for $nrRach at $occ in $parm/B_NR_RACH
    return
    &lt;ns3:ZUSAccount>
      &lt;ns3:accountNumber?>{data($nrRach)}&lt;/ns3:accountNumber>
      &lt;ns3:description?>{data($nazwa[$occ])}&lt;/ns3:description>
      &lt;ns3:name?>{data($nazwa[$occ])}&lt;/ns3:name>
      &lt;ns3:street>&lt;/ns3:street>
      &lt;ns3:zipCode>&lt;/ns3:zipCode>
      &lt;ns3:city?>{data($miasto[$occ])}&lt;/ns3:city>
    &lt;/ns3:ZUSAccount>
  }
&lt;/ns0:dicts>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
&lt;ns0:invokeResponse>
  {getElementsForDicts($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>