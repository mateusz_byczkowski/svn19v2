<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-01</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:printouts.entities.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare namespace ns5="urn:bzwbk.com";
declare variable $body as element(soap:Body) external;

declare namespace ctx="http://www.bea.com/wli/sb/context";

declare function getFieldsFromInvoke($parm as element(ns5:CALLSERVICEReturn)) as element()*
{
 for $x in $parm/regulations
 return
  &lt;ns0:Regulations>
    &lt;ns0:regulationsName>{data($x/regulationsName)}&lt;/ns0:regulationsName>
    &lt;ns0:url>{data($x/url)}&lt;/ns0:url>
  &lt;/ns0:Regulations>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string, $errorDescription as xs:string) as element(ns1:exceptionItem) {
   &lt;ns1:exceptionItem>
	&lt;ns1:errorCode1>{ $errorCode1 }&lt;/ns1:errorCode1>
	&lt;ns1:errorCode2>{ $errorCode2 }&lt;/ns1:errorCode2>
        &lt;ns1:errorDescription>{ $errorDescription }&lt;/ns1:errorDescription>
   &lt;/ns1:exceptionItem>
};

declare function my_fault($param as element(soap:Fault), $detail as element()?) as element(soap:Fault){
   &lt;soap:Fault>
	&lt;faultcode>soap:Server:userException&lt;/faultcode> 
	&lt;faultstring>err:ServiceFailException&lt;/faultstring> 
	&lt;detail>
             { $detail }
	&lt;/detail>
   &lt;/soap:Fault>
};

declare function my_faultStart($serviceFailType as xs:string, $body as element(soap:Body)) as element(soap:Fault){
    if ($serviceFailType = "WRONGINPUTEXCEPTION") then
        my_fault($body/soap:Fault, element ns1:ServiceFailException {local:errors(data($body/soap:Fault/detail/ns5:WRONGINPUTEXCEPTION/ERRORCODE1),data($body/soap:Fault/detail/ns5:WRONGINPUTEXCEPTION/ERRORCODE2), concat(data($body/soap:Fault/faultstring), ""))})
    else
       my_fault($body/soap:Fault, element ns1:ServiceException {local:errors(data($body/soap:Fault/detail/ns5:SERVICEEXCEPTION/ERRORCODE1),data($body/soap:Fault/detail/ns5:SERVICEEXCEPTION/ERRORCODE2), concat(data($body/soap:Fault/faultstring), ""))})
};

&lt;soap:Body>
{
if (data($body/ns5:CALLSERVICEReturn) != "") then
  &lt;ns3:invokeResponse>
    &lt;ns3:regulationsListOut>
      {getFieldsFromInvoke($body/ns5:CALLSERVICEReturn)}
    &lt;/ns3:regulationsListOut>
  &lt;/ns3:invokeResponse>
else
  my_faultStart(local-name($body/soap:Fault/detail/*), $body)
   
}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>