<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-10</con:description>
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
(:  &lt;ns0:error>
    &lt;ns2:ExceptionData>
      &lt;ns2:errorDescription?>{data($parm/NF_EXCEPD_ERRORDESCRIPTION)}&lt;/ns2:errorDescription>
    &lt;/ns2:ExceptionData>
  &lt;/ns0:error> :)
  &lt;ns0:legalInterest>
    &lt;ns3:LegalInterest>
      &lt;ns3:obligatoryInterestDate?>{data($parm/CI_DATA_OD)}&lt;/ns3:obligatoryInterestDate>
      &lt;ns3:obligInterestPercentage?>{data($parm/B_ZAL_ODS)}&lt;/ns3:obligInterestPercentage>
      &lt;ns3:obligInterestForDelayDate?>{data($parm/CI_DATA)}&lt;/ns3:obligInterestForDelayDate>
      &lt;ns3:obligInterestForDelayPer?>{data($parm/B_ZAL_KAR)}&lt;/ns3:obligInterestForDelayPer>
    &lt;/ns3:LegalInterest>
  &lt;/ns0:legalInterest>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>