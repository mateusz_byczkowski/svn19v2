<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-10</con:description>
  <con:xquery>declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;

&lt;soap:Body>
  &lt;fml:FML32>
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>