<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace ebp = "http://www.softax.com.pl/ebppml";



declare function xf:map_getBankPaymentAcceptTuxRequest($fml as element(fml:FML32))
	as element(ebp:payment-accept-request) {
		&lt;ebp:payment-accept-request>
			 &lt;ebp:head>
			  &lt;/ebp:head>
			  &lt;ebp:request>
				&lt;ebp:payment-accept>
					(: &lt;ebp:payer-id>{ data($fml/fml:EBPP_PAYER_ID) }&lt;/ebp:payer-id> :)
					&lt;ebp:message-id>{ data($fml/fml:EBPP_PAYMENT_MSG_ID) }&lt;/ebp:message-id>
					(: Obejście na jedną cyfrę po kropce (schema KIR wymaga dwóch) :)
					{
						if(fn:matches(xs:string(data($fml/fml:EBPP_PAYMENT_AMOUNT)), "^[0-9]+(\.[0-9]{1})?$")) then 
						(
							&lt;ebp:amount curr="{ data($fml/fml:EBPP_PAYMENT_CURRENCY) }">{ fn:concat(xs:string(data($fml/fml:EBPP_PAYMENT_AMOUNT)),"0")}&lt;/ebp:amount>
						) else (
							&lt;ebp:amount curr="{ data($fml/fml:EBPP_PAYMENT_CURRENCY) }">{ xs:string(data($fml/fml:EBPP_PAYMENT_AMOUNT)) }&lt;/ebp:amount>
						)
					}
					&lt;ebp:date>{ data($fml/fml:EBPP_PAYMENT_DATE) }&lt;/ebp:date>
				&lt;/ebp:payment-accept>
		      &lt;/ebp:request>       		
		&lt;/ebp:payment-accept-request>
};

declare variable $body as element(soapenv:Body) external;
&lt;soapenv:Body>
{ xf:map_getBankPaymentAcceptTuxRequest($body/fml:FML32) }
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>