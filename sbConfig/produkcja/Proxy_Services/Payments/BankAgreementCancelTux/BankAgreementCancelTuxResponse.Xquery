<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsd="http://www.w3.org/2001/XMLSchema";
declare namespace f="http://bzwbk.com/services/BZWBK24flow/faults/";
declare default element namespace "http://www.softax.com.pl/ebppml";

declare function xf:map_getBankAgreementCancelTuxResponse ($rpl as element(soapenv:Body)) as element(fml:FML32) {
	
	&lt;fml:FML32>
	{
		if (data($rpl/agreement-cancel-reply/reply/status/result) = "OK") then (		
		)  else (
		&lt;fml:ERROR_MESSAGE>{ data($rpl/agreement-cancel-reply/reply/status/err-code)  }&lt;/fml:ERROR_MESSAGE>,
		&lt;fml:ERROR_DESCRIPTION>{ data($rpl/agreement-cancel-reply/reply/status/err-message) }&lt;/fml:ERROR_DESCRIPTION>
		)		
	}
	&lt;/fml:FML32>
	
	
};

declare variable $reply as element(soapenv:Body) external;
&lt;soapenv:Body>
{ xf:map_getBankAgreementCancelTuxResponse($reply) }
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>