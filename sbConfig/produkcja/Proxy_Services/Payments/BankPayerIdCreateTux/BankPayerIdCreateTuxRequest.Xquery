<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace ebp = "http://www.softax.com.pl/ebppml";



declare function xf:map_getBankPayerIdCreateTuxRequest($fml as element(fml:FML32))
	as element(ebp:payer-id-create-request) {
		&lt;ebp:payer-id-create-request>
			 &lt;ebp:head />
			  &lt;ebp:request>
				&lt;ebp:payer-id-create>
				&lt;/ebp:payer-id-create>
		      &lt;/ebp:request>       		
		&lt;/ebp:payer-id-create-request>
};

declare variable $body as element(soapenv:Body) external;
&lt;soapenv:Body>
{ xf:map_getBankPayerIdCreateTuxRequest($body/fml:FML32) }
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>