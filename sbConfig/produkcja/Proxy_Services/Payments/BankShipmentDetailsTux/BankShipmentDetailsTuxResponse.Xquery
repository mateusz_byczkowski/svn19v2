<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsd="http://www.w3.org/2001/XMLSchema";
declare namespace f="http://bzwbk.com/services/BZWBK24flow/faults/";
declare default element namespace "http://www.softax.com.pl/ebppml";

declare function xf:map_getBankShipmentDetailsTuxResponse ($rpl as element(soapenv:Body)) as element(fml:FML32) {
	
	&lt;fml:FML32>
	{
		if (data($rpl/shipment-details-reply/reply/status/result) = "OK") then (
			for $it at $p in $rpl/shipment-details-reply/reply/shipment-details
			return (
				&lt;fml:EBPP_SHIP_REF_ID>{ data($it/ref-id) }&lt;/fml:EBPP_SHIP_REF_ID>,
				&lt;fml:EBPP_AGREEMENT_ID>{ data($it/agreement-id) }&lt;/fml:EBPP_AGREEMENT_ID>,
				&lt;fml:EBPP_CREDITOR_ID>{ data($it/creditor-id) }&lt;/fml:EBPP_CREDITOR_ID>,
				&lt;fml:EBPP_SHIP_DATE>{ data($it/date) }&lt;/fml:EBPP_SHIP_DATE>,
				
				&lt;fml:EBPP_SHIP_PAYMENT_COUNT>{ fn:count($it/payment-list/payment) }&lt;/fml:EBPP_SHIP_PAYMENT_COUNT>,
				for $itpayment at $p in $it/payment-list/payment
				return (
					&lt;fml:EBPP_PAYMENT_STATUS>{ data($itpayment/status) }&lt;/fml:EBPP_PAYMENT_STATUS>,
					&lt;fml:EBPP_PAYMENT_REF_ID>{ data($itpayment/ref-id) }&lt;/fml:EBPP_PAYMENT_REF_ID>,
					&lt;fml:EBPP_PAYMENT_MSG_ID>{ data($itpayment/message-id ) }&lt;/fml:EBPP_PAYMENT_MSG_ID>,
					&lt;fml:EBPP_PAYMENT_ACCOUNTNO>{ data($itpayment/beneficiary-account) }&lt;/fml:EBPP_PAYMENT_ACCOUNTNO>,
					&lt;fml:EBPP_PAYMENT_NAME_L1>{ data($itpayment/beneficiary-name/line[1]) }&lt;/fml:EBPP_PAYMENT_NAME_L1>,
					&lt;fml:EBPP_PAYMENT_NAME_L2>{ data($itpayment/beneficiary-name/line[2]) }&lt;/fml:EBPP_PAYMENT_NAME_L2>,
					&lt;fml:EBPP_PAYMENT_ADDR_L1>{ data($itpayment/beneficiary-address/line[1]) }&lt;/fml:EBPP_PAYMENT_ADDR_L1>,
					&lt;fml:EBPP_PAYMENT_ADDR_L2>{ data($itpayment/beneficiary-address/line[2]) }&lt;/fml:EBPP_PAYMENT_ADDR_L2>,
					
					&lt;fml:EBPP_PAYMENT_TITLE_L1>{ data($itpayment/title/line[1]) }&lt;/fml:EBPP_PAYMENT_TITLE_L1>,
					&lt;fml:EBPP_PAYMENT_TITLE_L2>{ data($itpayment/title/line[2]) }&lt;/fml:EBPP_PAYMENT_TITLE_L2>,
					&lt;fml:EBPP_PAYMENT_TITLE_L3>{ data($itpayment/title/line[3]) }&lt;/fml:EBPP_PAYMENT_TITLE_L3>,
					&lt;fml:EBPP_PAYMENT_TITLE_L4>{ data($itpayment/title/line[4]) }&lt;/fml:EBPP_PAYMENT_TITLE_L4>,
					
					if (data($itpayment/amount)) then (
						&lt;fml:EBPP_PAYMENT_AMOUNT>{ data($itpayment/amount) }&lt;/fml:EBPP_PAYMENT_AMOUNT>
					) else ( &lt;fml:EBPP_PAYMENT_AMOUNT>0&lt;/fml:EBPP_PAYMENT_AMOUNT> )
					,
					&lt;fml:EBPP_PAYMENT_CURRENCY>{ data($itpayment/amount/@curr) }&lt;/fml:EBPP_PAYMENT_CURRENCY>,
					&lt;fml:EBPP_PAYMENT_DATE>{ data($itpayment/payment-date) }&lt;/fml:EBPP_PAYMENT_DATE> 		
				),
				
				&lt;fml:EBPP_SHIP_BILL_COUNT>{ fn:count($it/bill-list/bill) }&lt;/fml:EBPP_SHIP_BILL_COUNT>,
				for $itbill at $p in $it/bill-list/bill
				return (
					&lt;fml:EBPP_BILL_REF_ID>{ data($itbill/ref-id) }&lt;/fml:EBPP_BILL_REF_ID>,
					&lt;fml:EBPP_BILL_MSG_ID>{ data($itbill/message-id) }&lt;/fml:EBPP_BILL_MSG_ID>,
					&lt;fml:EBPP_BILL_NO>{ data($itbill/bill-number) }&lt;/fml:EBPP_BILL_NO>,
					if (data($itbill/amount)) then (
						&lt;fml:EBPP_BILL_AMOUNT>{ data($itbill/amount) }&lt;/fml:EBPP_BILL_AMOUNT>
					) else ( &lt;fml:EBPP_BILL_AMOUNT>0&lt;/fml:EBPP_BILL_AMOUNT> )
					,
					&lt;fml:EBPP_BILL_CURRENCY>{ data($itbill/amount/@curr) }&lt;/fml:EBPP_BILL_CURRENCY>,
					&lt;fml:EBPP_BILL_TITLE_L1>{ data($itbill/title/line[1]) }&lt;/fml:EBPP_BILL_TITLE_L1>,
					&lt;fml:EBPP_BILL_TITLE_L2>{ data($itbill/title/line[2]) }&lt;/fml:EBPP_BILL_TITLE_L2>,
					&lt;fml:EBPP_BILL_TITLE_L3>{ data($itbill/title/line[3]) }&lt;/fml:EBPP_BILL_TITLE_L3>,
					&lt;fml:EBPP_BILL_TITLE_L4>{ data($itbill/title/line[4]) }&lt;/fml:EBPP_BILL_TITLE_L4>,
					&lt;fml:EBPP_BILL_ISSUE_DATE>{ data($itbill/issue-date) }&lt;/fml:EBPP_BILL_ISSUE_DATE>,
					&lt;fml:EBPP_BILL_ISSUER_NAME_L1>{ data($itbill/issuer-name/line[1]) }&lt;/fml:EBPP_BILL_ISSUER_NAME_L1>,
					&lt;fml:EBPP_BILL_ISSUER_NAME_L2>{ data($itbill/issuer-name/line[2]) }&lt;/fml:EBPP_BILL_ISSUER_NAME_L2>,
					&lt;fml:EBPP_BILL_ISSUER_ADDR_L1>{ data($itbill/issuer-address/line[1]) }&lt;/fml:EBPP_BILL_ISSUER_ADDR_L1>,
					&lt;fml:EBPP_BILL_ISSUER_ADDR_L2>{ data($itbill/issuer-address/line[2]) }&lt;/fml:EBPP_BILL_ISSUER_ADDR_L2>,
					&lt;fml:EBPP_BILL_ISSUER_NIP>{ data($itbill/issuer-nip) }&lt;/fml:EBPP_BILL_ISSUER_NIP>
				),
				
				&lt;fml:EBPP_SHIP_ATTACHMENT_COUNT>{ fn:count($it/attachment-list/attachment) }&lt;/fml:EBPP_SHIP_ATTACHMENT_COUNT>,
				for $itattachment at $p in $it/attachment-list/attachment
				return (
					&lt;fml:EBPP_ATTACHMENT_REF_ID>{ data($itattachment/ref-id) }&lt;/fml:EBPP_ATTACHMENT_REF_ID>,
					&lt;fml:EBPP_ATTACHMENT_MSG_ID>{ data($itattachment/message-id ) }&lt;/fml:EBPP_ATTACHMENT_MSG_ID>,
					&lt;fml:EBPP_ATTACHMENT_DESC>{ data($itattachment/description) }&lt;/fml:EBPP_ATTACHMENT_DESC>,
					&lt;fml:EBPP_ATTACHMENT_TYPE>{ data($itattachment/type) }&lt;/fml:EBPP_ATTACHMENT_TYPE>,
					&lt;fml:EBPP_ATTACHMENT_CONTENT>{ data($itattachment/content) }&lt;/fml:EBPP_ATTACHMENT_CONTENT>
				)
			)
		) else (
		&lt;fml:ERROR_MESSAGE>{ data($rpl/shipment-details-reply/reply/status/err-code)  }&lt;/fml:ERROR_MESSAGE>,
		&lt;fml:ERROR_DESCRIPTION>{ data($rpl/shipment-details-reply/reply/status/err-message) }&lt;/fml:ERROR_DESCRIPTION>
		)		
	}
	&lt;/fml:FML32>
	
	
};

declare variable $reply as element(soapenv:Body) external;
&lt;soapenv:Body>
{ xf:map_getBankShipmentDetailsTuxResponse($reply) }
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>