<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace ebp = "http://www.softax.com.pl/ebppml";



declare function xf:map_getBankShipmentDetailsTuxRequest($fml as element(fml:FML32))
	as element(ebp:shipment-details-request) {
		&lt;ebp:shipment-details-request>
			 &lt;ebp:head>
			  &lt;/ebp:head>
			  &lt;ebp:request>
				&lt;ebp:shipment-details>
					&lt;ebp:shipment-id>{ data($fml/fml:EBPP_SHIPMENT_ID) }&lt;/ebp:shipment-id>
					&lt;ebp:payer-id>{ data($fml/fml:EBPP_PAYER_ID) }&lt;/ebp:payer-id>
				&lt;/ebp:shipment-details>
		      &lt;/ebp:request>       		
		&lt;/ebp:shipment-details-request>
};

declare variable $body as element(soapenv:Body) external;
&lt;soapenv:Body>
{ xf:map_getBankShipmentDetailsTuxRequest($body/fml:FML32) }
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>