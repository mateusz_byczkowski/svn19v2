<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-13</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/GetONBaseRate/messages/";
declare namespace xf = "http://bzwbk.com/services/GetONBaseRate/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetONBaseRateResponse($fml as element(fml:FML32))
	as element(m:GetONBaseRateResponse) {
		&lt;m:GetONBaseRateResponse>
			{

				let $S_DATA := $fml/fml:S_DATA
				let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
				let $S_BASE_INTEREST := $fml/fml:S_BASE_INTEREST
				for $it at $p in $fml/fml:B_KOD_WALUTY
				return
					&lt;m:GetONBaseRateList>
					{
						if($S_DATA[$p])
							then &lt;m:Data>{ data($S_DATA[$p]) }&lt;/m:Data>
						else ()
					}
					{
						if($B_KOD_WALUTY[$p])
							then &lt;m:KodWaluty>{ data($B_KOD_WALUTY[$p]) }&lt;/m:KodWaluty>
						else ()
					}
					{
						if($S_BASE_INTEREST[$p])
							then &lt;m:BaseInterest>{ data($S_BASE_INTEREST[$p]) }&lt;/m:BaseInterest>
						else ()
					}
					&lt;/m:GetONBaseRateList>
			}

		&lt;/m:GetONBaseRateResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetONBaseRateResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>