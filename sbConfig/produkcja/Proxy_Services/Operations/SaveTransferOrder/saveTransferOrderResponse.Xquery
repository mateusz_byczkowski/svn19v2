<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns5="urn:filtersandmessages.entities.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:response>
    &lt;ns5:ResponseMessage>
      &lt;ns5:result?>true&lt;/ns5:result>
      &lt;ns5:errorCode?>{data($parm/NF_RESPOM_ERRORCODE)}&lt;/ns5:errorCode>
      &lt;ns5:errorDescription?>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}&lt;/ns5:errorDescription>
    &lt;/ns5:ResponseMessage>
  &lt;/ns0:response>
  &lt;ns0:transferOrderOut>
    &lt;ns2:TransferOrderAnchor>
      &lt;ns2:transferOrderNumber?>{data($parm/NF_TRANOA_TRANSFERORDERNUM)}&lt;/ns2:transferOrderNumber>
    &lt;/ns2:TransferOrderAnchor>
  &lt;/ns0:transferOrderOut>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>