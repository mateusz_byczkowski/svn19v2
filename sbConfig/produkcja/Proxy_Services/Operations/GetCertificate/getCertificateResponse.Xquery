<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns0="urn:accountdict.dictionaries.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)>5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };

declare function chgDate($value as xs:string,$dateFormat as xs:string,$fieldName as xs:string) as xs:string{

    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)

    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 


};

declare function getElementsForLoanCertSavAccountList($parm as element(fml:FML32)) as element()
{

&lt;ns1:loanCertSavAccountList>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER

    return
    &lt;ns1:LoanCertSavAccount>
      &lt;ns1:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns1:accountNumber>
      &lt;ns1:currency>
        &lt;ns3:CurrencyCode>
          &lt;ns3:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE2[$occ])}&lt;/ns3:currencyCode>
        &lt;/ns3:CurrencyCode>
      &lt;/ns1:currency>
    &lt;/ns1:LoanCertSavAccount>
  }
&lt;/ns1:loanCertSavAccountList>
};
declare function getElementsForLoanCertificateList($parm as element(fml:FML32)) as element()
{

&lt;ns1:loanCertificateList>
  {
    for $x at $occ in $parm/NF_LOANC_CERTIFICATEPAYMEN
    return
    &lt;ns1:LoanCertificate>
      { insertDate(data($parm/NF_LOANC_CERTIFICATEPAYMEN[$occ]),"yyyy-MM-dd","ns1:certificatePaymentDate")} 
      &lt;ns1:certificateInterest?>{data($parm/NF_LOANC_CERTIFICATEINTERE[$occ])}&lt;/ns1:certificateInterest>
      &lt;ns1:certificateInterestPLN?>{data($parm/NF_LOANC_CERTIFICATEINTPLN[$occ])}&lt;/ns1:certificateInterestPLN>
      &lt;ns1:certificateTransactionDesc?>{data($parm/NF_LOANC_CERTIFICATETRANSA[$occ])}&lt;/ns1:certificateTransactionDesc>
      &lt;ns1:certificatePrincipal?>{data($parm/NF_LOANC_CERTIFICATEPRINCI[$occ])}&lt;/ns1:certificatePrincipal>
      &lt;ns1:certificatePrincipalPLN?>{data($parm/NF_LOANC_CERTIFICATEPRIPLN[$occ])}&lt;/ns1:certificatePrincipalPLN>
    &lt;/ns1:LoanCertificate>
  }
&lt;/ns1:loanCertificateList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse>
  &lt;ns5:certificateOut>
    &lt;ns1:LoanCertificate>
      &lt;ns1:certificateAgreementNbr?>{data($parm/NF_LOANC_CERTIFICATEAGREEM)}&lt;/ns1:certificateAgreementNbr>
      &lt;ns1:certificatePastDuePrincAmt?>{data($parm/NF_LOANC_CERPASDUEPRIAMT)}&lt;/ns1:certificatePastDuePrincAmt>
      &lt;ns1:certificateInterestDue?>{data($parm/NF_LOANC_CERTIFICATEINTDUE)}&lt;/ns1:certificateInterestDue>
      &lt;ns1:certificatePastDueIntAmt?>{data($parm/NF_LOANC_CERTIFICATEPASTDU)}&lt;/ns1:certificatePastDueIntAmt>
      &lt;ns1:certificatePastDueLateInt?>{data($parm/NF_LOANC_CERPASDUELATINT)}&lt;/ns1:certificatePastDueLateInt>
      &lt;ns1:certificateDueAmountFee?>{data($parm/NF_LOANC_CERTIFICATEDUEAMO)}&lt;/ns1:certificateDueAmountFee>
      &lt;ns1:certificatePastDuePrincAmtPLN?>{data($parm/NF_LOANC_CERPASDUEPRIAMTPL)}&lt;/ns1:certificatePastDuePrincAmtPLN>
      &lt;ns1:certificateInterestDuePLN?>{data($parm/NF_LOANC_CERINTDUEPLN)}&lt;/ns1:certificateInterestDuePLN>
      &lt;ns1:certificatePastDueLateIntPLN?>{data($parm/NF_LOANC_CERPASDUELATINTPL)}&lt;/ns1:certificatePastDueLateIntPLN>
      &lt;ns1:certificateDueAmountFeePLN?>{data($parm/NF_LOANC_CERDUEAMOFEEPLN)}&lt;/ns1:certificateDueAmountFeePLN>
      &lt;ns1:certificatePastDueIntAmtPLN?>{data($parm/NF_LOANC_CERTIFICATEPASDUE)}&lt;/ns1:certificatePastDueIntAmtPLN>
      {getElementsForLoanCertSavAccountList($parm)}
      &lt;ns1:loanAccount>
        &lt;ns1:LoanAccount>
          &lt;ns1:interestRate?>{data($parm/NF_LOANA_INTERESTRATE)}&lt;/ns1:interestRate>
          &lt;ns1:faceAmount?>{data($parm/NF_LOANA_FACEAMOUNT)}&lt;/ns1:faceAmount>
          { insertDate(data($parm/NF_LOANA_MATURITYDATE),"yyyy-MM-dd","ns1:maturityDate")} 
          &lt;ns1:timesPastDueCY1?>{data($parm/NF_LOANA_TIMESPASTDUECY1)}&lt;/ns1:timesPastDueCY1>
          &lt;ns1:timesPastDueCY2?>{data($parm/NF_LOANA_TIMESPASTDUECY2)}&lt;/ns1:timesPastDueCY2>
          &lt;ns1:timesPastDueCY3?>{data($parm/NF_LOANA_TIMESPASTDUECY3)}&lt;/ns1:timesPastDueCY3>
          &lt;ns1:timesPastDueCY4?>{data($parm/NF_LOANA_TIMESPASTDUECY4)}&lt;/ns1:timesPastDueCY4>
          &lt;ns1:timesPastDueCY5?>{data($parm/NF_LOANA_TIMESPASTDUECY5)}&lt;/ns1:timesPastDueCY5>
          &lt;ns1:timesPastDueCY6?>{data($parm/NF_LOANA_TIMESPASTDUECY6)}&lt;/ns1:timesPastDueCY6>
          {getElementsForLoanCertificateList($parm)}
          &lt;ns1:account>
            &lt;ns1:Account>
              { insertDate(data($parm/NF_TRAACA_CREATIONDATE),"yyyy-MM-dd","ns1:accountOpenDate")}
              &lt;ns1:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE)}&lt;/ns1:currentBalance>
              &lt;ns1:currentBalancePLN?>{data($parm/NF_ACCOUN_CURRENTBALANCEPL)}&lt;/ns1:currentBalancePLN>
              &lt;ns1:currency>
                &lt;ns3:CurrencyCode>
                  &lt;ns3:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE)}&lt;/ns3:currencyCode>
                &lt;/ns3:CurrencyCode>
              &lt;/ns1:currency>
            &lt;/ns1:Account>
          &lt;/ns1:account>
          &lt;ns1:censusTract>
            &lt;ns3:CreditObject>
              &lt;ns3:object?>{data($parm/NF_CREDIO_OBJECT)}&lt;/ns3:object>
            &lt;/ns3:CreditObject>
          &lt;/ns1:censusTract>
        &lt;/ns1:LoanAccount>
      &lt;/ns1:loanAccount>
    &lt;/ns1:LoanCertificate>
  &lt;/ns5:certificateOut>
&lt;/ns5:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>