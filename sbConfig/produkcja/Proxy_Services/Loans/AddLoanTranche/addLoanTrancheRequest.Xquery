<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string

  return 
    if ($value &lt; 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-date($dateIn)))
};

declare function xf:booleanTo01($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "1"
    else "0"
};

declare function xf:shortAccount($account as xs:string) as xs:string {
    if (fn:string-length($account) > 12)
      then fn:substring($account, (fn:string-length($account))-11)
      else $account
};

&lt;soap-env:Body>
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:account/m1:Account
    let $req1 := $req/m1:loanAccount/m1:LoanAccount/m1:loanAccountScheduleTrancheList
    let $req2 := $req/m1:loanAccount/m1:LoanAccount/m1:loanAccountScheduleTrancheList/m1:LoanAccountScheduleTranche

    let $msgId := $reqh/m:msgHeader/m:msgId
    let $transId := $reqh/m:transHeader/m:transId
    let $userId  := $reqh/m:msgHeader/m:userId
    let $unitId  := $reqh/m:msgHeader/m:unitId
    return
    
    &lt;fml:FML32>
      &lt;fml:DC_MSHEAD_MSGID?>{ data($msgId) }&lt;/fml:DC_MSHEAD_MSGID>
      &lt;fml:DC_TRN_ID?>{ data($transId) }&lt;/fml:DC_TRN_ID>
      &lt;fml:DC_UZYTKOWNIK?>{concat("SKP:", data($userId)) }&lt;/fml:DC_UZYTKOWNIK>

      {if($unitId)
          then 
              if($unitId &lt; 1000)
                then &lt;fml:DC_ODDZIAL>{ data($unitId) }&lt;/fml:DC_ODDZIAL>
                else &lt;fml:DC_ODDZIAL>0&lt;/fml:DC_ODDZIAL>
          else ()
      }

      {if($req/m1:accountNumber)
         then &lt;fml:DC_NR_RACHUNKU>{ xf:shortAccount($req/m1:accountNumber) }&lt;/fml:DC_NR_RACHUNKU>
         else ()
      }

      {for $it in $req1/m1:LoanAccountScheduleTranche
         return
         if($it/m1:disbursementDate and fn:string-length($it/m1:disbursementDate)>0)
           then &lt;fml:DC_DATA_URUCHOMIENIA>{ xf:mapDate($it/m1:disbursementDate) }&lt;/fml:DC_DATA_URUCHOMIENIA>
           else ()
      }
      {for $it in $req1/m1:LoanAccountScheduleTranche
         return
         if($it/m1:expiryDate and fn:string-length($it/m1:expiryDate)>0)
           then &lt;fml:DC_DATA_WYGASNIECIA>{ xf:mapDate($it/m1:expiryDate) }&lt;/fml:DC_DATA_WYGASNIECIA>
           else ()
      }
      {for $it in $req1/m1:LoanAccountScheduleTranche
         return
         if($it/m1:totalAmount)
           then &lt;fml:DC_KWOTA_TRANSZY>{ data($it/m1:totalAmount) }&lt;/fml:DC_KWOTA_TRANSZY>
           else ()
      }
      {for $it in $req1/m1:LoanAccountScheduleTranche
         return
         if($it/m1:conditionFlagRequired and fn:string-length($it/m1:conditionFlagRequired)>0)
           then &lt;fml:DC_WYMAGANY_ZNACZNIK_WARUNKU2>{ xf:booleanTo01($it/m1:conditionFlagRequired) }&lt;/fml:DC_WYMAGANY_ZNACZNIK_WARUNKU2>
           else ()
      }
      {for $it in $req1/m1:LoanAccountScheduleTranche
         return
         if($it/m1:conditionFlag and fn:string-length($it/m1:conditionFlag)>0)
           then &lt;fml:DC_ZNACZNIK_WARUNKU2>{ xf:booleanTo01($it/m1:conditionFlag) }&lt;/fml:DC_ZNACZNIK_WARUNKU2>
           else ()
      }
    &lt;/fml:FML32>
  }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>