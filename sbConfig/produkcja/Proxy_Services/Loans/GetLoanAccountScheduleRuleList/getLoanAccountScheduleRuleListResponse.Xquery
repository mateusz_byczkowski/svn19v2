<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:accountdict.dictionaries.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

declare function getElementsForScheduleRules($parm as element(fml:FML32)) as element()
{

&lt;ns4:scheduleRules>
  { for $x at $occ in $parm/NF_LOAASR_PMTINSCHED
    return
    &lt;ns0:LoanAccountScheduleRule>
      {&lt;ns0:paymentFrequency?>{data($parm/NF_LOAASR_PAYMENTFREQUENCY[$occ])}&lt;/ns0:paymentFrequency>}
      {&lt;ns0:pmtInSched?>{data($parm/NF_LOAASR_PMTINSCHED[$occ])}&lt;/ns0:pmtInSched>}
      {&lt;ns0:paymntAmountOrPctg?>{data($parm/NF_LOAASR_PAYMNTAMOUNTORPC[$occ])}&lt;/ns0:paymntAmountOrPctg>}      
      {&lt;ns0:firstPmtDate?>{data($parm/NF_LOAASR_FIRSTPMTDATE[$occ])}&lt;/ns0:firstPmtDate>}      
      {&lt;ns0:scheduleNumber?>{data($parm/NF_LOAASR_SCHEDULENUMBER[$occ])}&lt;/ns0:scheduleNumber>}            
      &lt;ns0:paymentPeriod>
        &lt;ns2:Period>
          {&lt;ns2:period?>{data($parm/NF_PERIOD_PERIOD[$occ])}&lt;/ns2:period>}
        &lt;/ns2:Period>
      &lt;/ns0:paymentPeriod>
      &lt;ns0:paymentType>
          &lt;ns3:CreditSchedulePaymentType>
              &lt;ns3:creditSchedulePaymentType?>{data($parm/NF_CRESPT_CREDITSCHEDULEPA[$occ])}&lt;/ns3:creditSchedulePaymentType>
          &lt;/ns3:CreditSchedulePaymentType>
      &lt;/ns0:paymentType>
    &lt;/ns0:LoanAccountScheduleRule>
  }
&lt;/ns4:scheduleRules>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  {getElementsForScheduleRules($parm)}
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>