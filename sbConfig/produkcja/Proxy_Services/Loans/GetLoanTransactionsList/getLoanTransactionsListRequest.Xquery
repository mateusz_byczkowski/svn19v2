<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:bool2short($bool as xs:boolean) as xs:string {
	if ($bool = true() )
		then "1"
		else "0"
};

declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns6:msgHeader/ns6:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns6:msgHeader/ns6:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns6:msgHeader/ns6:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns6:msgHeader/ns6:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns6:msgHeader/ns6:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns6:msgHeader/ns6:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns6:transHeader/ns6:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{

&lt;NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns6:account/ns1:Account/ns1:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER>
,
&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
    if (data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:reverseOrder))
      then 	
          &lt;NF_PAGEC_REVERSEORDER?>
	{ xf:bool2short($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:reverseOrder) }
	&lt;/NF_PAGEC_REVERSEORDER>
    else()
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns7:PageControl/ns7:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
,
&lt;NF_FILTEH_STARTDATE?>{data($parm/ns6:filter/ns0:FilterHistory/ns0:startDate)}&lt;/NF_FILTEH_STARTDATE>
,
&lt;NF_FILTEH_ENDDATE?>{data($parm/ns6:filter/ns0:FilterHistory/ns0:endDate)}&lt;/NF_FILTEH_ENDDATE>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvoke($body/ns6:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>