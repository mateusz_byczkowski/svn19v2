<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:accountdict.dictionaries.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns6="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};



declare function getElementsForMsrMonthReserves($parm as element(fml:FML32)) as element()
{

&lt;ns0:msrMonthReserves>
  {
    for $x at $occ in $parm/NF_MSRMR_TOTALPRINCIPAL
    return
    &lt;ns5:MSRMonthReserve>
      { insertDate(data($parm/NF_MSRMR_DATEOFPROCESSING[$occ]),"yyyy-MM-dd","ns5:dateOfProcessing")}
      &lt;ns5:balanceProvision?>{data($parm/NF_MSRMR_BALANCEPROVISION[$occ])}&lt;/ns5:balanceProvision>
      &lt;ns5:offBalanceProvision?>{data($parm/NF_MSRMR_OFFBALANCEPROVISI[$occ])}&lt;/ns5:offBalanceProvision>
      &lt;ns5:entirelyOffBalanceProduct?>{data($parm/NF_MSRMR_ENTIRELYOFFBALANC[$occ])}&lt;/ns5:entirelyOffBalanceProduct>
      &lt;ns5:totalPrincipal?>{data($parm/NF_MSRMR_TOTALPRINCIPAL[$occ])}&lt;/ns5:totalPrincipal>
      &lt;ns5:offBalancePrincipal?>{data($parm/NF_MSRMR_OFFBALANCEPRINCIP[$occ])}&lt;/ns5:offBalancePrincipal>
      &lt;ns5:provisionFromBalancePart?>{data($parm/NF_MSRMR_PROVISIONFROMBALA[$occ])}&lt;/ns5:provisionFromBalancePart>
      &lt;ns5:provisionFromOffBalancePart?>{data($parm/NF_MSRMR_PROVISIONFROMOFFB[$occ])}&lt;/ns5:provisionFromOffBalancePart>
      &lt;ns5:provisionCoverageIndicator>{data($parm/NF_MSRMR_PROVISIONCOVERAGE[$occ])}&lt;/ns5:provisionCoverageIndicator>
      &lt;ns5:unsettledCommission?>{data($parm/NF_MSRMR_UNSETTLEDCOMMISSI[$occ])}&lt;/ns5:unsettledCommission>
      &lt;ns5:settledCommission?>{data($parm/NF_MSRMR_SETTLEDCOMMISSION[$occ])}&lt;/ns5:settledCommission>
      &lt;ns5:correctedCommissionValue?>{data($parm/NF_MSRMR_CORRECTEDCOMMISSI[$occ])}&lt;/ns5:correctedCommissionValue>
      &lt;ns5:nominalInterestCorrection?>{data($parm/NF_MSRMR_NOMINALINTERESTCO[$occ])}&lt;/ns5:nominalInterestCorrection>
      &lt;ns5:wayOfCalculatingProvision>
        &lt;ns2:WayOfCalculatingProvision>
          &lt;ns2:wayOfCalculatingProvision?>{data($parm/NF_WAYOCP_WAYOFCALCULATING[$occ])}&lt;/ns2:wayOfCalculatingProvision>
        &lt;/ns2:WayOfCalculatingProvision>
      &lt;/ns5:wayOfCalculatingProvision>
    &lt;/ns5:MSRMonthReserve>
  }
&lt;/ns0:msrMonthReserves>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:bcd>
    &lt;ns3:BusinessControlData>
      &lt;ns3:pageControl>
        &lt;ns4:PageControl>
          &lt;ns4:hasNext>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns4:hasNext>
          &lt;ns4:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns4:navigationKeyDefinition>
          &lt;ns4:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns4:navigationKeyValue>
        &lt;/ns4:PageControl>
      &lt;/ns3:pageControl>
    &lt;/ns3:BusinessControlData>
  &lt;/ns0:bcd>
  {getElementsForMsrMonthReserves($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>