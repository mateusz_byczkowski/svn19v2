<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:entities.be.dcl";
declare namespace ns3="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;
	
declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
  if ($value)
    then if(string-length($value)>5 and not ($value = "0001-01-01"))
       then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
    else() 
  else()
};

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForFeeList($parm as element(fml:FML32)) as element()
{

&lt;ns0:feeList>
  {
    for $x at $occ in $parm/NF_FEE_COUNTER
    return
    &lt;ns4:Fee>
      &lt;ns4:accruedFee>{data($parm/NF_FEE_ACCRUEDFEE[$occ])}&lt;/ns4:accruedFee>
      &lt;ns4:counter>{data($parm/NF_FEE_COUNTER[$occ])}&lt;/ns4:counter>
      &lt;ns4:retiredAmount>{data($parm/NF_FEE_RETIREDAMOUNT[$occ])}&lt;/ns4:retiredAmount>
      { insertDate(data($parm/NF_FEE_FEEDATE[$occ]),"yyyy-MM-dd","ns4:feeDate")}
      &lt;ns4:chargeNumber>{data($parm/NF_FEE_CHARGENUMBER[$occ])}&lt;/ns4:chargeNumber>
    &lt;/ns4:Fee>
  }
&lt;/ns0:feeList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:bcd>
    &lt;ns2:BusinessControlData>
      &lt;ns2:pageControl>
        &lt;ns3:PageControl>
          &lt;ns3:hasNext>{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns3:hasNext>
          &lt;ns3:navigationKeyDefinition>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns3:navigationKeyDefinition>
          &lt;ns3:navigationKeyValue>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns3:navigationKeyValue>
        &lt;/ns3:PageControl>
      &lt;/ns2:pageControl>
    &lt;/ns2:BusinessControlData>
  &lt;/ns0:bcd>
  {getElementsForFeeList($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>