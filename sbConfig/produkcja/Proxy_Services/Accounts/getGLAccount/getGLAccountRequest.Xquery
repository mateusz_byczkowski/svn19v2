<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2009-12-11
 :
 : wersja WSDLa: 17-09-2009 12:10:29
 : 
 : $Proxy Services/Accounts/getGLAccount/getGLAccountRequest.xq$
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getGLAccount/getGLAccountRequest/";
declare namespace ns0 = "urn:accounts.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "";
declare namespace ns3 = "urn:be.services.dcl";

declare variable $invoke1 as element(ns3:invoke) external;
declare variable $header1 as element(ns3:header) external;

(:~
 : @param  invoke1 - operacja wejściowa
 : @param  header1 - nagłówek wiadomości
 :
 : @return FML32 - bufor XML/FML
 :)
declare function xf:getGLAccountRequest($invoke1 as element(ns3:invoke),
										  $header1 as element(ns3:header))
    as element(ns2:FML32)
{
	&lt;ns2:FML32>
		&lt;ns2:NF_MSHEAD_MSGID?>{ data($header1/ns3:msgHeader/ns3:msgId) }&lt;/ns2:NF_MSHEAD_MSGID>
		
	    &lt;ns2:NF_GLA_COSTCENTERGL?>{
			data($invoke1/ns3:glAccount/ns0:GLAccount/ns0:costCenterGL)
		}&lt;/ns2:NF_GLA_COSTCENTERGL>
		
        &lt;ns2:NF_GLA_ACCOUNTGL?>{
			data($invoke1/ns3:glAccount/ns0:GLAccount/ns0:accountGL)
		}&lt;/ns2:NF_GLA_ACCOUNTGL>
		
        &lt;ns2:NF_ACCOUN_ACCOUNTIBAN?>{
			data($invoke1/ns3:glAccount/ns0:GLAccount/ns0:accountNumberNRB)
		}&lt;/ns2:NF_ACCOUN_ACCOUNTIBAN>
		
        &lt;ns2:NF_CURREC_CURRENCYCODE?>{
			data($invoke1/ns3:glAccount/ns0:GLAccount/ns0:currencyCode/ns1:CurrencyCode/ns1:currencyCode)
		}&lt;/ns2:NF_CURREC_CURRENCYCODE>
    &lt;/ns2:FML32>
};

&lt;soap-env:Body>{
	xf:getGLAccountRequest($invoke1, $header1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>