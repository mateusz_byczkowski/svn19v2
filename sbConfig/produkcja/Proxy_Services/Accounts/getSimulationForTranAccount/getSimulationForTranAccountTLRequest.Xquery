<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-23</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns9:header" location="getSimulationForTranAccount.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns9:invoke" location="getSimulationForTranAccount.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns9 = "urn:be.services.dcl";
declare namespace ns5 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns6 = "urn:entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns10 = "urn:baseauxentities.be.dcl";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace ns2 = "urn:accounts.entities.be.dcl";
declare namespace ns1 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:productstree.entities.be.dcl";
declare namespace ns0 = "http://bzwbk.com/nfe/transactionLog";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/getSimulationForTranAccount/getSimulationForTranAccountTLRequest/";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare function xf:getSimulationForTranAccountTLRequest($header1 as element(ns9:header),
    $invoke1 as element(ns9:invoke),
    $SaveTransferResponse as element(FML32) ?,
    $faultResponse as element() ?)
    
    as element(ns0:transactionLogEntry) {
        &lt;ns0:transactionLogEntry>
            {
                for $businessTransactionType in $invoke1/ns9:transaction/ns8:Transaction/ns8:businessTransactionType/ns7:BusinessTransactionType/ns7:businessTransactionType
                return
                    &lt;businessTransactionType>{ data($businessTransactionType) }&lt;/businessTransactionType>
            }
            {
                for $csrMessageType in $invoke1/ns9:transaction/ns8:Transaction/ns8:csrMessageType/ns7:CsrMessageType/ns7:csrMessageType
                return
                    &lt;csrMessageType>{ data($csrMessageType) }&lt;/csrMessageType>
            }
            {
                for $dtTransactionType in $invoke1/ns9:transaction/ns8:Transaction/ns8:dtTransactionType/ns7:DtTransactionType/ns7:dtTransactionType
                return
                    &lt;dtTransactionType>{ data($dtTransactionType) }&lt;/dtTransactionType>
            }
            &lt;executor>
                {
                    for $branchCode in $invoke1/ns9:branchCode/ns4:BranchCode/ns4:branchCode
                    return
                        &lt;branchNumber>{ xs:int( data($branchCode) ) }&lt;/branchNumber>
                }
                &lt;executorID>{ data($header1/ns9:msgHeader/ns9:userId) }&lt;/executorID>
                {
                    for $userFirstName in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:user/ns6:User/ns6:userFirstName
                    return
                        &lt;firstName>{ data($userFirstName) }&lt;/firstName>
                }
                {
                    for $userLastName in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:user/ns6:User/ns6:userLastName
                    return
                        &lt;lastName>{ data($userLastName) }&lt;/lastName>
                }
                {
                    for $tellerID in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:teller/ns5:Teller/ns5:tellerID
                    return
                        &lt;tellerID>{ data($tellerID) }&lt;/tellerID>
                }
                {
                    for $tillID in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:till/ns5:Till/ns5:tillID
                    return
                        &lt;tillNumber>{ data($tillID) }&lt;/tillNumber>
                }
                        &lt;userID?>{ data($SaveTransferResponse/TR_UZYTKOWNIK) }&lt;/userID>

(:                { :)
(:                    for $userID in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:user/ns6:User/ns6:userID  :)
(:                    return  :)
(:                        &lt;userID>{ data($userID) }&lt;/userID>  :)
(:                }  :)
            &lt;/executor>
            {
                for $extendedCSRMessageType in $invoke1/ns9:transaction/ns8:Transaction/ns8:extendedCSRMessageType/ns7:ExtendedCSRMessageType/ns7:extendedCSRMessageType
                return
                    &lt;extendedCSRMessageType>{ data($extendedCSRMessageType) }&lt;/extendedCSRMessageType>
            }
            &lt;hlbsName>getSimulationForTranAccount&lt;/hlbsName>
            {
                for $orderedBy in $invoke1/ns9:transaction/ns8:Transaction/ns8:orderedBy
                return
                    &lt;orderedBy>{ data($orderedBy) }&lt;/orderedBy>
            }
            {
                for $putDownDate in $invoke1/ns9:transaction/ns8:Transaction/ns8:putDownDate
                return
                    &lt;putDownDate>{ xs:date( data($putDownDate)) }&lt;/putDownDate>
            }
            &lt;timestamp>{ data($header1/ns9:msgHeader/ns9:timestamp) }&lt;/timestamp>
            {
                let $Account := $invoke1/ns9:account/ns2:Account
                return
                    &lt;tlAccountList>
                        {
                            for $accountNumber in $Account/ns2:accountNumber
                            return
                                &lt;accountNumber>{ data($accountNumber) }&lt;/accountNumber>
                        }
                        {
                            for $idProductDefinition in $Account/ns2:productDefinition/ns3:ProductDefinition/ns3:idProductDefinition
                            return
                                &lt;productId>{ data($idProductDefinition) }&lt;/productId>
                        }
                    &lt;/tlAccountList>
            }
            {
                for $transactionDate in $invoke1/ns9:transaction/ns8:Transaction/ns8:transactionDate
                return
                    &lt;transactionDate>{ xs:date( data($transactionDate)) }&lt;/transactionDate>
            }
            
            &lt;tlExceptionDataList?>
            {
            	let $errorCode1 := data($faultResponse//err:exceptionItem/err:errorCode1)
            	let $errorCode2 := data($faultResponse//err:exceptionItem/err:errorCode2)
                let $errorDescription := data($faultResponse//err:exceptionItem/err:errorDescription)
            	return
                   if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') != '') then
            		(
		                &lt;errorCode1?>{ xs:int($errorCode1) }&lt;/errorCode1>,
        		        &lt;errorCode2?>{ xs:int($errorCode2) }&lt;/errorCode2>,
                		&lt;errorDescription?>{ $errorDescription }&lt;/errorDescription>
                	)
                	else
                	()
			}
            &lt;/tlExceptionDataList>

            
            {
                for $transactionGroupID in $invoke1/ns9:transaction/ns8:Transaction/ns8:transactionGroupID
                return
                    &lt;transactionGroupID>{ data($transactionGroupID) }&lt;/transactionGroupID>
            }
            &lt;transactionID>{ data($header1/ns9:transHeader/ns9:transId) }&lt;/transactionID>
  	   &lt;transactionStatus?>{ data($SaveTransferResponse/TR_STATUS) }&lt;/transactionStatus>

        &lt;/ns0:transactionLogEntry>
};

declare variable $header1 as element(ns9:header) external;
declare variable $invoke1 as element(ns9:invoke) external;
declare variable $SaveTransferResponse  as element(FML32) ? external;
declare variable $faultResponse  as element() ? external;

&lt;soap-env:Body>{
xf:getSimulationForTranAccountTLRequest($header1,
    $invoke1, $SaveTransferResponse,$faultResponse)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>