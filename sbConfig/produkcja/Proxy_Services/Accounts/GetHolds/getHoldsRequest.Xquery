<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:accountdict.dictionaries.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm  = "1") then $trueval
       else $falseval
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
if ($parm/ns0:hold/ns6:Hold/ns6:holdActivityFlag) then
&lt;NF_HOLD_HOLDSTAT?>{boolean2SourceValue(data($parm/ns0:hold/ns6:Hold/ns6:holdActivityFlag),"A","I")}&lt;/NF_HOLD_HOLDSTAT>
else()
,
&lt;NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns0:account/ns6:Account/ns6:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER>
,
&lt;NF_ACCOUN_ACCOUNTIBAN?>{data($parm/ns0:account/ns6:Account/ns6:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTIBAN>
,
&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_HOLDT_HOLDTYPE>00&lt;/NF_HOLDT_HOLDTYPE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
&lt;NF_PAGEC_REVERSEORDER?>{boolean2SourceValue(data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:reverseOrder),"1","0")}&lt;/NF_PAGEC_REVERSEORDER>
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>