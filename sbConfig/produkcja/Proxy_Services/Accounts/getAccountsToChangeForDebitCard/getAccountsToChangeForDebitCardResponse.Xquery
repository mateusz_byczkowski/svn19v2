<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2009-09-18 PKLI TEET 39529Version.$1.2011-05-10</con:description>
  <con:xquery>(:Change log :)
(: v.1.1 2009-09-18 PKLI TEET 39529: zmiana mapowania pola accountStatus :)

declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:entities.be.dcl";
declare namespace ns10="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns8="urn:be.services.dcl";
declare namespace fml="";
declare namespace ns9="urn:card.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForAccountRelationshipList($parm as element(fml:FML32), $occ) as element()
{

&lt;ns1:accountRelationshipList>
  {
    (:for $x at $occ in $parm/NF_ACCOUR_RELATIONSHIP
    return:)
    &lt;ns3:AccountRelationship>
      &lt;ns3:customer>
        &lt;ns3:Customer>
          &lt;ns3:companyName?>{data($parm/NF_CUSTOM_COMPANYNAME[$occ])}&lt;/ns3:companyName>
          &lt;ns3:customerPersonal>
            &lt;ns3:CustomerPersonal>
              &lt;ns3:lastName?>{data($parm/NF_CUSTOP_LASTNAME[$occ])}&lt;/ns3:lastName>
              &lt;ns3:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME[$occ])}&lt;/ns3:firstName>
            &lt;/ns3:CustomerPersonal>
          &lt;/ns3:customerPersonal>
          &lt;ns3:customerType>
            &lt;ns5:CustomerType>
              &lt;ns5:customerType?>{data($parm/NF_CUSTOT_CUSTOMERTYPE[$occ])}&lt;/ns5:customerType>
            &lt;/ns5:CustomerType>
          &lt;/ns3:customerType>
        &lt;/ns3:Customer>
      &lt;/ns3:customer>
      &lt;ns3:relationship>
        &lt;ns5:CustomerAccountRelationship>
          &lt;ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship>
        &lt;/ns5:CustomerAccountRelationship>
      &lt;/ns3:relationship>
    &lt;/ns3:AccountRelationship>
  }
&lt;/ns1:accountRelationshipList>
};
declare function getElementsForAccountList($parm as element(fml:FML32)) as element()
{

&lt;ns8:accountList>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
    &lt;ns1:Account>
      &lt;ns1:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns1:accountNumber>
      &lt;ns1:accountName?>{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}&lt;/ns1:accountName>
      &lt;ns1:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns1:currentBalance>
      &lt;ns1:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns1:accountDescription>
      {getElementsForAccountRelationshipList($parm,$occ)}
      &lt;ns1:accountType>
        &lt;ns5:AccountType>
          &lt;ns5:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns5:accountType>
        &lt;/ns5:AccountType>
      &lt;/ns1:accountType>
      &lt;ns1:currency>
        &lt;ns5:CurrencyCode>
          &lt;ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode>
        &lt;/ns5:CurrencyCode>
      &lt;/ns1:currency>
      &lt;ns1:accountStatus>
        &lt;ns2:AccountStatus>
(:T39529  &lt;ns2:accountStatus?>{data($parm/NF_ACCOUS_ACCOUNTSTATUS[$occ])}&lt;/ns2:accountStatus> :)
(:T39529:)&lt;ns2:accountStatus?>{data($parm/NF_PRODUCT_STATUSCODE[$occ])}&lt;/ns2:accountStatus>
        &lt;/ns2:AccountStatus>
      &lt;/ns1:accountStatus>
    &lt;/ns1:Account>
  }
&lt;/ns8:accountList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns8:invokeResponse>
  &lt;ns8:response>
    &lt;ns0:ResponseMessage>
      &lt;ns0:result?>{data($parm/NF_RESPOM_RESULT)}&lt;/ns0:result>
    &lt;/ns0:ResponseMessage>
  &lt;/ns8:response>
  {getElementsForAccountList($parm)}
  &lt;ns8:bcd>
    &lt;ns6:BusinessControlData>
      &lt;ns6:pageControl>
        &lt;ns10:PageControl>
          &lt;ns10:hasNext?>{data($parm/NF_PAGEC_HASNEXT)}&lt;/ns10:hasNext>
          &lt;ns10:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns10:navigationKeyDefinition>
          &lt;ns10:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns10:navigationKeyValue>
        &lt;/ns10:PageControl>
      &lt;/ns6:pageControl>
    &lt;/ns6:BusinessControlData>
  &lt;/ns8:bcd>
&lt;/ns8:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>