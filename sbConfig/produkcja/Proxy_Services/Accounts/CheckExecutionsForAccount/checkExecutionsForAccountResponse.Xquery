<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:filtersandmessages.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function local:mapCheckExecutionsForAccountResponse($fml as element())
	as element(m:invokeResponse) {
		&lt;m:invokeResponse>
			&lt;m:response>
				&lt;e:ResponseMessage>
					{	
						if($fml/fml:NF_RESPOM_RESULT)
							then 
								&lt;e:result>
								{
									if(data($fml/fml:NF_RESPOM_RESULT) = "1") 
										then "true"									
										else "false"
								}
								&lt;/e:result>
							else (),
						if($fml/fml:NF_RESPOM_ERRORCODE)
							then
								&lt;e:errorCode>{ data($fml/fml:NF_RESPOM_ERRORCODE) }&lt;/e:errorCode>
							else(),	
						if($fml/fml:NF_RESPOM_ERRORDESCRIPTION)
							then
								&lt;e:errorDescription>{ data($fml/fml:NF_RESPOM_ERRORDESCRIPTION) }&lt;/e:errorDescription>
							else()
					}
				&lt;/e:ResponseMessage>
			&lt;/m:response>
		&lt;/m:invokeResponse>		
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ local:mapCheckExecutionsForAccountResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>