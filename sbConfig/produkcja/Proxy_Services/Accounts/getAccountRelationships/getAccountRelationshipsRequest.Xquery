<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2010-02-25
 :
 : wersja WSDLa: 30-11-2009 10:44:21
 :
 : $Proxy Services/Accounts/getAccountRelationships/getAccountRelationshipsRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getAccountRelationships/getAccountRelationshipsRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:accounts.entities.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";

declare variable $header1 as element(ns3:header) external;
declare variable $invoke1 as element(ns3:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return $FML32 bufor XML/FML
 :)
declare function xf:getAccountRelationshipsRequest($header1 as element (ns3:header),
													 $invoke1 as element(ns3:invoke))
    as element(ns0:FML32)
{
	&lt;ns0:FML32>

		(:
		 : dane z nagłówka
		 :)
		&lt;ns0:NF_MSHEAD_MSGID?>{
			data($header1/ns3:msgHeader/ns3:msgId)
		}&lt;/ns0:NF_MSHEAD_MSGID>
		
		(:
		 : dane wejściowe
		 :)

		(:
		 : fizyczny - F --> P
		 : prawny   - P --> N
		 :)
		{
			let $customerType := $invoke1/ns3:customerType/ns2:CustomerType/ns2:customerType
			return
				if (data($customerType)) then
					&lt;ns0:NF_CUSTOT_CUSTOMERTYPE>{
						if (data($customerType) eq 'F') then
							'P'
						else
							'N'
					}&lt;/ns0:NF_CUSTOT_CUSTOMERTYPE>
				else ()
		}

		&lt;ns0:NF_ACCOUN_ACCOUNTNUMBER?>{
			data($invoke1/ns3:account/ns1:Account/ns1:accountNumber)
		}&lt;/ns0:NF_ACCOUN_ACCOUNTNUMBER>
		
		{
			for $i in 1 to count($invoke1/ns3:customerAccountRelationships/ns2:CustomerAccountRelationship)
			return
				&lt;ns0:NF_CUSTAR_CUSTOMERACCOUNTR?>{
					data($invoke1/ns3:customerAccountRelationships/ns2:CustomerAccountRelationship[$i]/ns2:customerAccountRelationship)
				}&lt;/ns0:NF_CUSTAR_CUSTOMERACCOUNTR>
		}
		
	&lt;/ns0:FML32>
};

&lt;soap-env:Body>{
	xf:getAccountRelationshipsRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>