<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:accountdict.dictionaries.be.dcl";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns6="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()? {
 if ($value)
   then if(string-length($value)>5 and not ($value = "0001-01-01"))
       then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
   else() 
 else()
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  <ns0:fee>
    <ns5:Fee>
      <ns5:feeFrequency?>{data($parm/NF_FEE_FEEFREQUENCY)}</ns5:feeFrequency>
      { insertDate(data($parm/NF_FEE_NEXTFEEDATE),"yyyy-MM-dd","ns5:nextFeeDate")}
      <ns5:accruedFee?>{data($parm/NF_FEE_ACCRUEDFEE)}</ns5:accruedFee>
      <ns5:currentPeriodDays?>{data($parm/NF_FEE_CURRENTPERIODDAYS)}</ns5:currentPeriodDays>
      <ns5:feePeriod>
        <ns4:Period>
          <ns4:period?>{data($parm/NF_PERIOD_PERIOD[1])}</ns4:period>
        </ns4:Period>
      </ns5:feePeriod>
      <ns5:feeType>
        <ns3:FeeType>
          <ns3:feeType?>{data($parm/NF_FEET_FEETYPE)}</ns3:feeType>
        </ns3:FeeType>
      </ns5:feeType>
      <ns5:feeSpecialDay>
        <ns4:SpecialDay>
          <ns4:specialDay?>{if(data($parm/NF_SPECID_SPECIALDAY[1]) !=0) then data($parm/NF_SPECID_SPECIALDAY[1]) else ''}</ns4:specialDay>
        </ns4:SpecialDay>
      </ns5:feeSpecialDay>
    </ns5:Fee>
  </ns0:fee>
  <ns0:tranAccountACA>
    <ns5:TranAccountACA>
      <ns5:flagTODACA>
        <ns3:FlagTODACA>
          <ns3:flagTODACA?>{data($parm/NF_FTODAC_FLAGTODACA)}</ns3:flagTODACA>
        </ns3:FlagTODACA>
      </ns5:flagTODACA>
    </ns5:TranAccountACA>
  </ns0:tranAccountACA>
  <ns0:accountOut>
    <ns5:Account>
      <ns5:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER)}</ns5:accountNumber>
     { insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE),"yyyy-MM-dd","ns5:accountOpenDate")}
      <ns5:accountName?>{data($parm/NF_ACCOUN_ACCOUNTNAME)}</ns5:accountName>
      <ns5:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE)}</ns5:currentBalance>
      <ns5:accountIBAN?>{data($parm/NF_ACCOUN_ACCOUNTIBAN)}</ns5:accountIBAN>
      <ns5:skpOpener?>{data($parm/NF_ACCOUN_SKPOPENER)}</ns5:skpOpener>
      <ns5:productCode?>{data($parm/NF_ACCOUN_PRODUCTCODE)}</ns5:productCode>
      <ns5:tranAccount>
        <ns5:TranAccount>
          <ns5:avaiableBalance?>{data($parm/NF_TRANA_AVAIABLEBALANCE)}</ns5:avaiableBalance>
          <ns5:memoBalance?>{data($parm/NF_TRANA_MEMOBALANCE)}</ns5:memoBalance>
          <ns5:interestRate?>{data($parm/NF_TRANA_INTERESTRATE)}</ns5:interestRate>
          { insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT),"yyyy-MM-dd","ns5:dateOfLastActivity")}
          <ns5:serviceChargeCode?>{data($parm/NF_TRANA_SERVICECHARGECODE)}</ns5:serviceChargeCode>
          <ns5:interestDueDebit?>{data($parm/NF_TRANA_INTERESTDUEDEBIT)}</ns5:interestDueDebit>
          <ns5:interestDueCredit?>{data($parm/NF_TRANA_INTERESTDUECREDIT)}</ns5:interestDueCredit>
          <ns5:interestTransferAccount?>{data($parm/NF_TRANA_INTERESTTRANSFERA)}</ns5:interestTransferAccount>
          <ns5:interestPlanNumber?>{data($parm/NF_TRANA_INTERESTPLANNUMBE)}</ns5:interestPlanNumber>
          <ns5:interestPlanIndex?>{data($parm/NF_TRANA_INTERESTPLANINDEX)}</ns5:interestPlanIndex>
          <ns5:interestAccd?>{data($parm/NF_TRANA_INTERESTACCD)}</ns5:interestAccd>
          { insertDate(data($parm/NF_TRANA_NEXTCREDITINTDATE),"yyyy-MM-dd","ns5:nextCreditIntDate")}
          <ns5:interestAccrued?>{data($parm/NF_TRANA_INTERESTACCRUED)}</ns5:interestAccrued>
          <ns5:interestAccruedPrevious?>{data($parm/NF_TRANA_INTERESTACCRUEDPR)}</ns5:interestAccruedPrevious>
          <ns5:interestAccruePrevYear?>{data($parm/NF_TRANA_INTERESTACCRUEPRE)}</ns5:interestAccruePrevYear>
          <ns5:witholdingIntTaxFlag?>{data($parm/NF_TRANA_WITHOLDINGINTTAXF)}</ns5:witholdingIntTaxFlag>
          <ns5:witholdingIntTaxPercent?>{data($parm/NF_TRANA_WITHOLDINGINTTAXP)}</ns5:witholdingIntTaxPercent>
          <ns5:accuredTax?>{data($parm/NF_TRANA_ACCUREDTAX)}</ns5:accuredTax>
          { insertDate(data($parm/NF_TRANA_DATEOFLASTPERFORM),"yyyy-MM-dd","ns5:dateOfLastPerform")}
          <ns5:creditAmount?>{data($parm/NF_TRANA_CREDITAMOUNT)}</ns5:creditAmount>
          <ns5:interestCodeDisposition?>{data($parm/NF_TRANA_INTERESTCODEDISPO)}</ns5:interestCodeDisposition>
          <ns5:interestFrequency?>{data($parm/NF_TRANA_INTERESTFREQUENCY)}</ns5:interestFrequency>
          <ns5:accountTypeFlag>
            <ns4:AccountTypeFlag>
              <ns4:accountTypeFlag?>{data($parm/NF_ACCOTF_ACCOUNTTYPEFLAG)}</ns4:accountTypeFlag>
            </ns4:AccountTypeFlag>
          </ns5:accountTypeFlag>
          <ns5:creditInterestPeriod>
            <ns4:Period>
              <ns4:period?>{data($parm/NF_PERIOD_PERIOD[2])}</ns4:period>
            </ns4:Period>
          </ns5:creditInterestPeriod>
          <ns5:specificDayDisposition>
            <ns4:SpecialDay>
              <ns4:specialDay?>{if (data($parm/NF_SPECID_SPECIALDAY[2]) !=0) then data($parm/NF_SPECID_SPECIALDAY[2])  else ''}</ns4:specialDay>
            </ns4:SpecialDay>
          </ns5:specificDayDisposition>
          <ns5:promotionCode>
            <ns4:PromotionCode>
              <ns4:promotionCode?>{data($parm/NF_PROMOC_PROMOTIONCODE)}</ns4:promotionCode>
            </ns4:PromotionCode>
          </ns5:promotionCode>
        </ns5:TranAccount>
      </ns5:tranAccount>
      <ns5:statementParameters>
        <ns5:StatementParameters>
          <ns5:frequency?>{data($parm/NF_STATEP_FREQUENCY)}</ns5:frequency>
          { insertDate(data($parm/NF_STATEP_NEXTPRINTOUTDATE),"yyyy-MM-dd","ns5:nextPrintoutDate")}
          <ns5:printFlag?>{data($parm/NF_STATEP_PRINTFLAG)}</ns5:printFlag>
          <ns5:aggregateTransaction?>{data($parm/NF_STATEP_AGGREGATETRANSAC)}</ns5:aggregateTransaction>
          <ns5:cycle>
            <ns4:Period>
              <ns4:period?>{data($parm/NF_PERIOD_PERIOD[3])}</ns4:period>
            </ns4:Period>
          </ns5:cycle>
          <ns5:specialDay>
            <ns4:SpecialDay>
              <ns4:specialDay?>{if(data($parm/NF_SPECID_SPECIALDAY[3]) != 0) then data($parm/NF_SPECID_SPECIALDAY[3])  else ''}</ns4:specialDay>
            </ns4:SpecialDay>
          </ns5:specialDay>
          <ns5:provideManner>
            <ns4:ProvideManner>
              <ns4:provideManner?>{data($parm/NF_PROVIM_PROVIDEMANNER)}</ns4:provideManner>
            </ns4:ProvideManner>
          </ns5:provideManner>
        </ns5:StatementParameters>
      </ns5:statementParameters>
      <ns5:currency>
        <ns4:CurrencyCode>
          <ns4:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE)}</ns4:currencyCode>
        </ns4:CurrencyCode>
      </ns5:currency>
      <ns5:accountStatus>
        <ns2:AccountStatus>
          <ns2:accountStatus?>{data($parm/NF_ACCOUS_ACCOUNTSTATUS)}</ns2:accountStatus>
        </ns2:AccountStatus>
      </ns5:accountStatus>
      <ns5:accountBranchNumber>
        <ns4:BranchCode>
          <ns4:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE)}</ns4:branchCode>
        </ns4:BranchCode>
      </ns5:accountBranchNumber>
      <ns5:accessType>
        <ns3:AccessType>
          <ns3:accessType>{data($parm/NF_ACCEST_ACCESSTYPE)}</ns3:accessType>
        </ns3:AccessType>
      </ns5:accessType>
      <ns5:accountAddress>
        <ns5:AccountAddress>
          <ns5:name1?>{data($parm/NF_ACCOUA_NAME1)}</ns5:name1>
          <ns5:name2?>{data($parm/NF_ACCOUA_NAME2)}</ns5:name2>
          <ns5:street?>{data($parm/NF_ACCOUA_STREET)}</ns5:street>
          <ns5:houseFlatNumber?>{data($parm/NF_ACCOUA_HOUSEFLATNUMBER)}</ns5:houseFlatNumber>
          <ns5:city?>{data($parm/NF_ACCOUA_CITY)}</ns5:city>
          <ns5:stateCountry?>{data($parm/NF_ACCOUA_STATECOUNTRY)}</ns5:stateCountry>
          <ns5:zipCode?>{data($parm/NF_ACCOUA_ZIPCODE)}</ns5:zipCode>
          <ns5:accountAddressType?>{data($parm/NF_ACCOUA_ACCOUNTADDRESSTY)}</ns5:accountAddressType>
          { insertDate(data($parm/NF_ACCOUA_VALIDFROM),"yyyy-MM-dd","ns5:validFrom")}
          { insertDate(data($parm/NF_ACCOUA_VALIDTO),"yyyy-MM-dd","ns5:validTo")}
        </ns5:AccountAddress>
      </ns5:accountAddress>
    </ns5:Account>
  </ns0:accountOut>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>