<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForFeeList($parm as element(fml:FML32)) as element()
{

&lt;ns0:feeList>
  {
    for $x at $occ in $parm/NF_FEE_COUNTER
    return
    &lt;ns2:Fee>
      &lt;ns2:counter>{data($parm/NF_FEE_COUNTER[$occ])}&lt;/ns2:counter>
      &lt;ns2:transactionAmount>{data($parm/NF_FEE_TRANSACTIONAMOUNT[$occ])}&lt;/ns2:transactionAmount>
      &lt;ns2:feeCode>{data($parm/NF_FEE_FEECODE[$occ])}&lt;/ns2:feeCode>
      &lt;ns2:feeDescription>{data($parm/NF_FEE_FEEDESCRIPTION[$occ])}&lt;/ns2:feeDescription>
    &lt;/ns2:Fee>
  }
&lt;/ns0:feeList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  {getElementsForFeeList($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>