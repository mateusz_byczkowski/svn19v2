<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustByProductResponse($bdy as element(m:PRIMEGetCustByProductResponse ))
	as element(fml:FML32) {
             let $NrRachunku := $bdy/m:PRIMEGetCustByProductResult/m:DC_NR_RACHUNKU
             let $NrAlternatywny := $bdy/m:PRIMEGetCustByProductResult/m:DC_NR_ALTERNATYWNY
             let $Pojedynczy:= $bdy/m:PRIMEGetCustByProductResult/m:B_POJEDYNCZY
             let $ProductAtt1 := $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_ATT1
             let $ProductAtt2 := $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_ATT2
             let $ProductAtt3 := $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_ATT3
             let $ProductAtt4 := $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_ATT4
             let $ProductAtt5 := $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_ATT5
             let $Saldo:= $bdy/m:PRIMEGetCustByProductResult/m:B_SALDO
             let $DostSrodki:= $bdy/m:PRIMEGetCustByProductResult/m:B_DOST_SRODKI
             let $Blokada:= $bdy/m:PRIMEGetCustByProductResult/m:B_BLOKADA
             let $KodWaluty:= $bdy/m:PRIMEGetCustByProductResult/m:B_KOD_WALUTY
             let $DOtwarcia:= $bdy/m:PRIMEGetCustByProductResult/m:B_D_OTWARCIA
             let $DataOstOper:= $bdy/m:PRIMEGetCustByProductResult/m:B_DATA_OST_OPER
             let $Limit1:= $bdy/m:PRIMEGetCustByProductResult/m:B_LIMIT1
             let $ProductNameOryginal:= $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_NAME_ORYGINAL
             let $NumerKlienta:= $bdy/m:PRIMEGetCustByProductResult/m:DC_NUMER_KLIENTA
          
             return
             &lt;FML32>    
	         {  
                    if  ($NrRachunku) 
                          then  &lt;fml:DC_NR_RACHUNKU>{substring(data($NrRachunku),2)}&lt;/fml:DC_NR_RACHUNKU>
                          else   &lt;fml:DC_NR_RACHUNKU nil="true" />
                 }
	         {  
                    if  ($NrAlternatywny ) 
                          then  &lt;fml:DC_NR_RACHUNKU_W_SYSTEMIE>{data($NrAlternatywny )}&lt;/fml:DC_NR_RACHUNKU_W_SYSTEMIE>
                          else   &lt;fml:DC_NR_RACHUNKU_W_SYSTEMIE nil="true" />
                 }
	         {  
                    if  ($Pojedynczy) 
                          then  &lt;fml:B_POJEDYNCZY>{data($Pojedynczy)}&lt;/fml:B_POJEDYNCZY>
                          else   &lt;fml:B_POJEDYNCZY nil="true" />
                 }
	         {  
                    if  ($ProductAtt1 and data($ProductAtt1)!="0") 
                          then  &lt;fml:CI_PRODUCT_ATT1>{data($ProductAtt1)}&lt;/fml:CI_PRODUCT_ATT1>
                          else   &lt;fml:CI_PRODUCT_ATT1 nil="true" />
                 }
	        (: {  
                    if  ($ProductAtt2 and data($ProductAtt2)!="0") 
                          then  &lt;fml:CI_PRODUCT_ATT2>{data($ProductAtt2)}&lt;/fml:CI_PRODUCT_ATT2>
                          else   &lt;fml:CI_PRODUCT_ATT2 nil="true" />
                 }
	         {  
                    if  ($ProductAtt3 and data($ProductAtt3)!="0") 
                          then  &lt;fml:CI_PRODUCT_ATT3>{data($ProductAtt3)}&lt;/fml:CI_PRODUCT_ATT3>
                          else   &lt;fml:CI_PRODUCT_ATT3 nil="true" />
                 }
	         {  
                    if  ($ProductAtt4 and data($ProductAtt4)!="0") 
                          then  &lt;fml:CI_PRODUCT_ATT4>{data($ProductAtt4)}&lt;/fml:CI_PRODUCT_ATT4>
                          else   &lt;fml:CI_PRODUCT_ATT4 nil="true" />
                 }
	         {  
                    if  ($ProductAtt5 and data($ProductAtt5)!="0") 
                          then  &lt;fml:CI_PRODUCT_ATT5>{data($ProductAtt5)}&lt;/fml:CI_PRODUCT_ATT5>
                          else   &lt;fml:CI_PRODUCT_ATT5 nil="true" />
                 }:)
                 &lt;fml:CI_PRODUCT_ATT2 nil="true" />
                 &lt;fml:CI_PRODUCT_ATT3 nil="true" />
                 &lt;fml:CI_PRODUCT_ATT4 nil="true" />
                 &lt;fml:CI_PRODUCT_ATT5 nil="true" />
                {  
                    if  ($Saldo) 
                          then  &lt;fml:B_SALDO>{data($Saldo)}&lt;/fml:B_SALDO>
                          else   &lt;fml:B_SALDO nil="true" />
                 }
	         {  
                    if  ($DostSrodki) 
                          then  &lt;fml:B_DOST_SRODKI>{data($DostSrodki)}&lt;/fml:B_DOST_SRODKI>
                          else   &lt;fml:B_DOST_SRODKI nil="true" />
                 }
	         {  
                    if  ($Blokada) 
                          then  &lt;fml:B_BLOKADA>{data($Blokada)}&lt;/fml:B_BLOKADA>
                          else   &lt;fml:B_BLOKADA nil="true" />
                 }
	         {  
                    if  ($KodWaluty) 
                          then  &lt;fml:B_KOD_WALUTY>{data($KodWaluty)}&lt;/fml:B_KOD_WALUTY>
                          else   &lt;fml:B_KOD_WALUTY nil="true" />
                 }
	         {  
                    if  ($DOtwarcia) 
                          then  &lt;fml:B_D_OTWARCIA>{data($DOtwarcia)}&lt;/fml:B_D_OTWARCIA>
                          else   &lt;fml:B_D_OTWARCIA nil="true" />
                 }
	         {  
                    if  ($DataOstOper) 
                          then  &lt;fml:B_DATA_OST_OPER>{data($DataOstOper)}&lt;/fml:B_DATA_OST_OPER>
                          else   &lt;fml:B_DATA_OST_OPER nil="true" />
                 }
	         {  
                    if  ($Limit1) 
                          then  &lt;fml:B_LIMIT1>{data($Limit1)}&lt;/fml:B_LIMIT1>
                          else   &lt;fml:B_LIMIT1 nil="true" />
                 }
	         {  
                    if  ($ProductNameOryginal) 
                          then  &lt;fml:CI_PRODUCT_NAME_ORYGINAL>{data($ProductNameOryginal)}&lt;/fml:CI_PRODUCT_NAME_ORYGINAL>
                          else   &lt;fml:CI_PRODUCT_NAME_ORYGINAL nil="true" />
                 }
	         {  
                    if  ($NumerKlienta) 
                          then  &lt;fml:DC_NUMER_KLIENTA>{data($NumerKlienta)}&lt;/fml:DC_NUMER_KLIENTA>
                          else   &lt;fml:DC_NUMER_KLIENTA nil="true" />
                 }
                {  
                    if  ($NumerKlienta) 
                          then  &lt;fml:CI_RACHUNEK_ADR_ALT>N&lt;/fml:CI_RACHUNEK_ADR_ALT>    
                          else   ()
                 } 						
	               
             &lt;/FML32>
               
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ 
  if($body/m:PRIMEGetCustByProductResponse)
     then  xf:mapPRIMEGetCustByProductResponse($body/m:PRIMEGetCustByProductResponse) 
     else
     if ($body/fml:FML32)
        then $body/fml:FML32
         else()
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>