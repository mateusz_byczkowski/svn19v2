<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddDictCatResponse($fml as element(fml:FML32))
	as element(m:CRMAddDictCatResponse) {
		&lt;m:CRMAddDictCatResponse>
			{
				if($fml/fml:CI_ID_SLOWNIKA)
					then &lt;m:IdSlownika>{ data($fml/fml:CI_ID_SLOWNIKA) }&lt;/m:IdSlownika>
					else ()
			}
			{
				if($fml/fml:CI_NAZWA_SLOWNIKA)
					then &lt;m:NazwaSlownika>{ data($fml/fml:CI_NAZWA_SLOWNIKA) }&lt;/m:NazwaSlownika>
					else ()
			}
		&lt;/m:CRMAddDictCatResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMAddDictCatResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>