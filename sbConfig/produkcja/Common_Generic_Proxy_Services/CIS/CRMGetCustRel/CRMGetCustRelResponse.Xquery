<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustRelResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustRelResponse) {
		&lt;m:CRMGetCustRelResponse>
			{

				let $DC_NUMER_KLIENTA_REL := $fml/fml:DC_NUMER_KLIENTA_REL
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $DC_TYP_RELACJI := $fml/fml:DC_TYP_RELACJI
				let $CI_TYP_REL_ODWROTNEJ := $fml/fml:CI_TYP_REL_ODWROTNEJ
				let $CI_BUDOWA_PORTFELA := $fml/fml:CI_BUDOWA_PORTFELA
				let $CI_OBSLUGA_ZADAN := $fml/fml:CI_OBSLUGA_ZADAN
				let $CI_KORESP_SERYJNA := $fml/fml:CI_KORESP_SERYJNA
                                let $CI_PARAMETRY_RELACJI := $fml/fml:CI_PARAMETRY_RELACJI
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA_REL
				return
					&lt;m:CRMGetCustRelRelacja>
					{
						if($DC_NUMER_KLIENTA_REL[$p])
							then &lt;m:NumerKlientaRel>{ data($DC_NUMER_KLIENTA_REL[$p]) }&lt;/m:NumerKlientaRel>
						else ()
					}
					{
						if($DC_TYP_KLIENTA[$p])
							then &lt;m:TypKlienta>{ data($DC_TYP_KLIENTA[$p]) }&lt;/m:TypKlienta>
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI[$p])
							then &lt;m:KlasaObslugi>{ data($CI_KLASA_OBSLUGI[$p]) }&lt;/m:KlasaObslugi>
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie>{ data($DC_IMIE[$p]) }&lt;/m:Imie>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko>{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko>
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna>{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna>
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then &lt;m:IdPortfela>{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela>
						else ()
					}
					{
						if($DC_TYP_RELACJI[$p])
							then &lt;m:TypRelacji>{ data($DC_TYP_RELACJI[$p]) }&lt;/m:TypRelacji>
						else ()
					}
					{
						if($CI_TYP_REL_ODWROTNEJ[$p])
							then &lt;m:TypRelOdwrotnej>{ data($CI_TYP_REL_ODWROTNEJ[$p]) }&lt;/m:TypRelOdwrotnej>
						else ()
					}
					{
						if($CI_BUDOWA_PORTFELA[$p])
							then &lt;m:BudowaPortfela>{ data($CI_BUDOWA_PORTFELA[$p]) }&lt;/m:BudowaPortfela>
						else ()
					}
					{
						if($CI_OBSLUGA_ZADAN[$p])
							then &lt;m:ObslugaZadan>{ data($CI_OBSLUGA_ZADAN[$p]) }&lt;/m:ObslugaZadan>
						else ()
					}
					{
						if($CI_KORESP_SERYJNA[$p])
							then &lt;m:KorespSeryjna>{ data($CI_KORESP_SERYJNA[$p]) }&lt;/m:KorespSeryjna>
						else ()
					}
                                        {
                                               if($CI_PARAMETRY_RELACJI[$p])
                                                       then &lt;m:SzczegolyRelacji>{ data($CI_PARAMETRY_RELACJI[$p]) }&lt;/m:SzczegolyRelacji>
                                               else ()
                                        }
					&lt;/m:CRMGetCustRelRelacja>
			}

		&lt;/m:CRMGetCustRelResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetCustRelResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>