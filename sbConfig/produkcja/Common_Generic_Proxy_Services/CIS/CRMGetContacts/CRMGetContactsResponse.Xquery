<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetContactsResponse($fml as element(fml:FML32))
	as element(m:CRMGetContactsResponse) {
		&lt;m:CRMGetContactsResponse>
			{
				let $CI_NUMER_PACZKI := $fml/fml:CI_NUMER_PACZKI return
					if($CI_NUMER_PACZKI)
						then &lt;m:NumerPaczki>{ data($CI_NUMER_PACZKI) }&lt;/m:NumerPaczki>
						else ()
			}
			{
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $CI_NUMER_KLIENTA := $fml/fml:CI_NUMER_KLIENTA
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_STATUS_GIODO := $fml/fml:CI_STATUS_GIODO
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $CI_RODZAJ_KONT := $fml/fml:CI_RODZAJ_KONT
				let $CI_CEL_KONT := $fml/fml:CI_CEL_KONT
				let $CI_SPOS_KONT := $fml/fml:CI_SPOS_KONT
				let $CI_ID_KAMP := $fml/fml:CI_ID_KAMP
				let $CI_PRODUKTY := $fml/fml:CI_PRODUKTY
				let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
				let $CI_ID_KONT := $fml/fml:CI_ID_KONT
				let $CI_DATA_KONT := $fml/fml:CI_DATA_KONT
				let $CI_WYNIK_KONTAKTU := $fml/fml:CI_WYNIK_KONTAKTU
				let $CI_ID_ZADANIA := $fml/fml:CI_ID_ZADANIA
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				for $it at $p in $fml/fml:CI_ID_KONT
				return
					&lt;m:CRMGetContactsKontakt>
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta>
						else ()
					}
					{
						if($CI_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlientaZewn>{ data($CI_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlientaZewn>
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna>{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna>
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie>{ data($DC_IMIE[$p]) }&lt;/m:Imie>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko>{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko>
						else ()
					}
					{
						if($CI_STATUS_GIODO[$p])
							then &lt;m:StatusGiodo>{ data($CI_STATUS_GIODO[$p]) }&lt;/m:StatusGiodo>
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then &lt;m:UdostepGrupa>{ data($CI_UDOSTEP_GRUPA[$p]) }&lt;/m:UdostepGrupa>
						else ()
					}
					{
						if($CI_RODZAJ_KONT[$p])
							then &lt;m:RodzajKont>{ data($CI_RODZAJ_KONT[$p]) }&lt;/m:RodzajKont>
						else ()
					}
					{
						if($CI_CEL_KONT[$p])
							then &lt;m:CelKont>{ data($CI_CEL_KONT[$p]) }&lt;/m:CelKont>
						else ()
					}
					{
						if($CI_SPOS_KONT[$p])
							then &lt;m:SposKont>{ data($CI_SPOS_KONT[$p]) }&lt;/m:SposKont>
						else ()
					}
					{
						if($CI_ID_KAMP[$p])
							then &lt;m:IdKamp>{ data($CI_ID_KAMP[$p]) }&lt;/m:IdKamp>
						else ()
					}
					{
						if($CI_PRODUKTY[$p])
							then &lt;m:Produkty>{ data($CI_PRODUKTY[$p]) }&lt;/m:Produkty>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA[$p])
							then &lt;m:SkpPracownika>{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:SkpPracownika>
						else ()
					}
					{
						if($CI_ID_KONT[$p])
							then &lt;m:IdKont>{ data($CI_ID_KONT[$p]) }&lt;/m:IdKont>
						else ()
					}
					{
						if($CI_DATA_KONT[$p])
							then &lt;m:DataKont>{ data($CI_DATA_KONT[$p]) }&lt;/m:DataKont>
						else ()
					}
					{
						if($CI_WYNIK_KONTAKTU[$p])
							then &lt;m:WynikKontaktu>{ data($CI_WYNIK_KONTAKTU[$p]) }&lt;/m:WynikKontaktu>
						else ()
					}
					{
						if($CI_ID_ZADANIA[$p])
							then &lt;m:IdZadania>{ data($CI_ID_ZADANIA[$p]) }&lt;/m:IdZadania>
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then &lt;m:IdPortfela>{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela>
						else ()
					}
					&lt;/m:CRMGetContactsKontakt>
			}
		&lt;/m:CRMGetContactsResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetContactsResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>