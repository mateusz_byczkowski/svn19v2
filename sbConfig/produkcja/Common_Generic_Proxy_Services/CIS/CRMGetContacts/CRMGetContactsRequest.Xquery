<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetContactsRequest($req as element(m:CRMGetContactsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI>{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:NumerKlientaZewn)
					then &lt;fml:CI_NUMER_KLIENTA>{ data($req/m:NumerKlientaZewn) }&lt;/fml:CI_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:DataOd)
					then &lt;fml:CI_DATA_OD>{ data($req/m:DataOd) }&lt;/fml:CI_DATA_OD>
					else ()
			}
			{
				if($req/m:DataDo)
					then &lt;fml:CI_DATA_DO>{ data($req/m:DataDo) }&lt;/fml:CI_DATA_DO>
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:KontRelacji)
					then &lt;fml:CI_KONT_RELACJI>{ data($req/m:KontRelacji) }&lt;/fml:CI_KONT_RELACJI>
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER>{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI>{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI>
					else ()
			}
			{
				if($req/m:Sortowanie)
					then &lt;fml:CI_SORTOWANIE>{ data($req/m:Sortowanie) }&lt;/fml:CI_SORTOWANIE>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:RodzajKont)
					then &lt;fml:CI_RODZAJ_KONT>{ data($req/m:RodzajKont) }&lt;/fml:CI_RODZAJ_KONT>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetContactsRequest($body/m:CRMGetContactsRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>