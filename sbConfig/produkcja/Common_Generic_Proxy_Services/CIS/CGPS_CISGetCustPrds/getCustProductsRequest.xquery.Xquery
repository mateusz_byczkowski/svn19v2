<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/esbmessages/";
declare namespace xf = "http://bzwbk.com/services/esbmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetCustProductsRequest($req as element(m:getCustProductsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			(:{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}:)
			{
				if($req/m:IdSpolki)
					then &lt;fml:NF_MSHEAD_COMPANYID>{ data($req/m:IdSpolki) }&lt;/fml:NF_MSHEAD_COMPANYID>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER>{ data($req/m:NumerKlienta) }&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER>
					else ()
			}
			{
				if($req/m:ProductAreaId)
					then &lt;fml:NF_PRODUA_CODEPRODUCTAREA>{ data($req/m:ProductAreaId) }&lt;/fml:NF_PRODUA_CODEPRODUCTAREA>
					else ()
			}
			{
				if(string-length(data($req/m:ProductCategoryId))>0 )
					then &lt;fml:NF_PRODUC_IDPRODUCTCATEGOR>{ data($req/m:ProductCategoryId) }&lt;/fml:NF_PRODUC_IDPRODUCTCATEGOR>
					else ()
			}
			(:{
				if($req/m:ProductAplication)
					then &lt;fml:CI_PRODUCT_APLICATION>{ data($req/m:ProductAplication) }&lt;/fml:CI_PRODUCT_APLICATION>
					else ()
			}:)
			(:{
				if($req/m:CiOpcja)
					then &lt;fml:NF_CTRL_OPTION>{ data($req/m:CiOpcja) }&lt;/fml:NF_CTRL_OPTION>
					else &lt;fml:NF_CTRL_OPTION>0&lt;/fml:NF_CTRL_OPTION>
			}:)
                        &lt;fml:NF_CTRL_OPTION>2&lt;/fml:NF_CTRL_OPTION>
                        &lt;fml:NF_PAGEC_PAGESIZE>200&lt;/fml:NF_PAGEC_PAGESIZE>
			&lt;fml:NF_PAGEC_ACTIONCODE>F&lt;/fml:NF_PAGEC_ACTIONCODE>
			&lt;fml:NF_PAGEC_REVERSEORDER>0&lt;/fml:NF_PAGEC_REVERSEORDER>
			&lt;fml:NF_PAGEC_NAVIGATIONKEYVALU/>
			&lt;fml:NF_CTRL_ACTIVENONACTIVE>0&lt;/fml:NF_CTRL_ACTIVENONACTIVE>
			&lt;fml:NF_ACCOUN_ALTERNATIVEADDRE>0&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE>
                        &lt;fml:NF_CTRL_SYSTEMS>001002005&lt;/fml:NF_CTRL_SYSTEMS>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetCustProductsRequest($body/m:getCustProductsRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>