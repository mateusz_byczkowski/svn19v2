<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetDictValResponse($fml as element(fml:FML32))
	as element(m:CRMGetDictValResponse) {
		&lt;m:CRMGetDictValResponse>
			{

				let $CI_ID_SLOWNIKA := $fml/fml:CI_ID_SLOWNIKA
				let $CI_WARTOSC_SLOWNIKA := $fml/fml:CI_WARTOSC_SLOWNIKA
				let $CI_OPIS_WARTOSCI_SLOWNIKA := $fml/fml:CI_OPIS_WARTOSCI_SLOWNIKA
				let $CI_WAGA_WARTOSCI_SLOWNIKA := $fml/fml:CI_WAGA_WARTOSCI_SLOWNIKA
				let $CI_DOMENA_RODZICA := $fml/fml:CI_DOMENA_RODZICA
				let $CI_ID_SLOWNIKA_RODZICA := $fml/fml:CI_ID_SLOWNIKA_RODZICA
				let $CI_DODATKOWA_WART_SLOWNIKA := $fml/fml:CI_DODATKOWA_WART_SLOWNIKA
				for $it at $p in $fml/fml:CI_ID_SLOWNIKA
				return
					&lt;m:CRMGetDictValWartoscSlownika>
					{
						if($CI_ID_SLOWNIKA[$p])
							then &lt;m:IdSlownika>{ data($CI_ID_SLOWNIKA[$p]) }&lt;/m:IdSlownika>
						else ()
					}
					{
						if($CI_WARTOSC_SLOWNIKA[$p])
							then &lt;m:WartoscSlownika>{ data($CI_WARTOSC_SLOWNIKA[$p]) }&lt;/m:WartoscSlownika>
						else ()
					}
					{
						if($CI_OPIS_WARTOSCI_SLOWNIKA[$p])
							then &lt;m:OpisWartosciSlownika>{ data($CI_OPIS_WARTOSCI_SLOWNIKA[$p]) }&lt;/m:OpisWartosciSlownika>
						else ()
					}
					{
						if($CI_WAGA_WARTOSCI_SLOWNIKA[$p])
							then &lt;m:WagaWartosciSlownika>{ data($CI_WAGA_WARTOSCI_SLOWNIKA[$p]) }&lt;/m:WagaWartosciSlownika>
						else ()
					}
					{
						if($CI_DOMENA_RODZICA[$p])
							then &lt;m:DomenaRodzica>{ data($CI_DOMENA_RODZICA[$p]) }&lt;/m:DomenaRodzica>
						else ()
					}
					{
						if($CI_ID_SLOWNIKA_RODZICA[$p])
							then &lt;m:IdSlownikaRodzica>{ data($CI_ID_SLOWNIKA_RODZICA[$p]) }&lt;/m:IdSlownikaRodzica>
						else ()
					}
					{
						if($CI_DODATKOWA_WART_SLOWNIKA[$p])
							then &lt;m:DodatkowaWartSlownika>{ data($CI_DODATKOWA_WART_SLOWNIKA[$p]) }&lt;/m:DodatkowaWartSlownika>
						else ()
					}
					&lt;/m:CRMGetDictValWartoscSlownika>
			}

		&lt;/m:CRMGetDictValResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetDictValResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>