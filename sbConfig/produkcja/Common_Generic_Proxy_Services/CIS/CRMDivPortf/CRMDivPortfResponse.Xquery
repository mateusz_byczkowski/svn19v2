<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMDivPortfResponse($fml as element(fml:FML32))
	as element(m:CRMDivPortfResponse) {
		&lt;m:CRMDivPortfResponse>
                        {
				let $DC_OPIS_BLEDU := $fml/fml:DC_OPIS_BLEDU return
					if($DC_OPIS_BLEDU)
						then &lt;m:OpisBledu>{ data($DC_OPIS_BLEDU) }&lt;/m:OpisBledu>
						else ()
			}
		&lt;/m:CRMDivPortfResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMDivPortfResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>