<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddProspectResponse($fml as element(fml:FML32))
	as element(m:CRMAddProspectResponse) {
		&lt;m:CRMAddProspectResponse>
			{

				let $CI_ID_KLIENTA := $fml/fml:CI_ID_KLIENTA
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $CI_STATUS_GIODO := $fml/fml:CI_STATUS_GIODO
				let $CI_NR_KOM := $fml/fml:CI_NR_KOM
				for $it at $p in $fml/fml:DC_NAZWISKO
				return
					&lt;m:CRMAddProspectKlient>
					{
						if($CI_ID_KLIENTA[$p])
							then &lt;m:IdKlienta>{ data($CI_ID_KLIENTA[$p]) }&lt;/m:IdKlienta>
						else ()
					}
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta>
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie>{ data($DC_IMIE[$p]) }&lt;/m:Imie>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko>{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko>
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna>{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna>
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then &lt;m:UdostepGrupa>{ data($CI_UDOSTEP_GRUPA[$p]) }&lt;/m:UdostepGrupa>
						else ()
					}
					{
						if($CI_STATUS_GIODO[$p])
							then &lt;m:StatusGiodo>{ data($CI_STATUS_GIODO[$p]) }&lt;/m:StatusGiodo>
						else ()
					}
					{
						if($CI_NR_KOM[$p])
							then &lt;m:NrKom>{ data($CI_NR_KOM[$p]) }&lt;/m:NrKom>
						else ()
					}
					&lt;/m:CRMAddProspectKlient>
			}

		&lt;/m:CRMAddProspectResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMAddProspectResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>