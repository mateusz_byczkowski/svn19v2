<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetProspectResponse($fml as element(fml:FML32))
	as element(m:CRMGetProspectResponse) {
		&lt;m:CRMGetProspectResponse>
			{
				if($fml/fml:CI_ID_KLIENTA)
					then &lt;m:IdKlienta>{ data($fml/fml:CI_ID_KLIENTA) }&lt;/m:IdKlienta>
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA_REJ)
					then &lt;m:SkpPracownikaRej>{ data($fml/fml:CI_SKP_PRACOWNIKA_REJ) }&lt;/m:SkpPracownikaRej>
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA)
					then &lt;m:SkpPracownika>{ data($fml/fml:CI_SKP_PRACOWNIKA) }&lt;/m:SkpPracownika>
					else ()
			}
			{
				if($fml/fml:CI_ID_SPOLKI)
					then &lt;m:IdSpolki>{ data($fml/fml:CI_ID_SPOLKI) }&lt;/m:IdSpolki>
					else ()
			}
			{
				if($fml/fml:DC_NUMER_ODDZIALU)
					then &lt;m:NumerOddzialu>{ data($fml/fml:DC_NUMER_ODDZIALU) }&lt;/m:NumerOddzialu>
					else ()
			}
			{
				if($fml/fml:DC_TYP_KLIENTA)
					then &lt;m:TypKlienta>{ data($fml/fml:DC_TYP_KLIENTA) }&lt;/m:TypKlienta>
					else ()
			}
			{
				if($fml/fml:DC_IMIE)
					then &lt;m:Imie>{ data($fml/fml:DC_IMIE) }&lt;/m:Imie>
					else ()
			}
			{
				if($fml/fml:DC_NAZWISKO)
					then &lt;m:Nazwisko>{ data($fml/fml:DC_NAZWISKO) }&lt;/m:Nazwisko>
					else ()
			}
			{
				if($fml/fml:DC_NAZWA)
					then &lt;m:Nazwa>{ data($fml/fml:DC_NAZWA) }&lt;/m:Nazwa>
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then &lt;m:NrPesel>{ data($fml/fml:DC_NR_PESEL) }&lt;/m:NrPesel>
					else ()
			}
			{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then &lt;m:NrDowoduRegon>{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:NrDowoduRegon>
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then &lt;m:Nip>{ data($fml/fml:DC_NIP) }&lt;/m:Nip>
					else ()
			}
			{
				if($fml/fml:DC_NUMER_PASZPORTU)
					then &lt;m:NumerPaszportu>{ data($fml/fml:DC_NUMER_PASZPORTU) }&lt;/m:NumerPaszportu>
					else ()
			}
			{
				if($fml/fml:CI_UDOSTEP_GRUPA)
					then &lt;m:UdostepGrupa>{ data($fml/fml:CI_UDOSTEP_GRUPA) }&lt;/m:UdostepGrupa>
					else ()
			}
			{
				if($fml/fml:DC_KRAJ)
					then &lt;m:Kraj>{ data($fml/fml:DC_KRAJ) }&lt;/m:Kraj>
					else ()
			}
			{
				if($fml/fml:DC_WOJEWODZTWO)
					then &lt;m:Wojewodztwo>{ data($fml/fml:DC_WOJEWODZTWO) }&lt;/m:Wojewodztwo>
					else ()
			}
			{
				if($fml/fml:DC_KOD_POCZTOWY_DANE_PODST)
					then &lt;m:KodPocztowyDanePodst>{ data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST) }&lt;/m:KodPocztowyDanePodst>
					else ()
			}
			{
				if($fml/fml:DC_MIASTO_DANE_PODST)
					then &lt;m:MiastoDanePodst>{ data($fml/fml:DC_MIASTO_DANE_PODST) }&lt;/m:MiastoDanePodst>
					else ()
			}
			{
				if($fml/fml:DC_ULICA_DANE_PODST)
					then &lt;m:UlicaDanePodst>{ data($fml/fml:DC_ULICA_DANE_PODST) }&lt;/m:UlicaDanePodst>
					else ()
			}
			{
				if($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST)
					then &lt;m:NrPosesLokaluDanePodst>{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST) }&lt;/m:NrPosesLokaluDanePodst>
					else ()
			}
			{
				if($fml/fml:CI_ADRES_DOD)
					then &lt;m:AdresDod>{ data($fml/fml:CI_ADRES_DOD) }&lt;/m:AdresDod>
					else ()
			}
			{
				if($fml/fml:DC_NR_TELEFONU)
					then &lt;m:NrTelefonu>{ data($fml/fml:DC_NR_TELEFONU) }&lt;/m:NrTelefonu>
					else ()
			}
			{
				if($fml/fml:DC_NR_TELEF_KOMORKOWEGO)
					then &lt;m:NrTelefKomorkowego>{ data($fml/fml:DC_NR_TELEF_KOMORKOWEGO) }&lt;/m:NrTelefKomorkowego>
					else ()
			}
			{
				if($fml/fml:DC_NUMER_FAKSU)
					then &lt;m:NumerFaksu>{ data($fml/fml:DC_NUMER_FAKSU) }&lt;/m:NumerFaksu>
					else ()
			}
			{
				if($fml/fml:DC_ADRES_E_MAIL)
					then &lt;m:AdresEMail>{ data($fml/fml:DC_ADRES_E_MAIL) }&lt;/m:AdresEMail>
					else ()
			}
			{
				if($fml/fml:CI_NOTATKI)
					then &lt;m:Notatki>{ data($fml/fml:CI_NOTATKI) }&lt;/m:Notatki>
					else ()
			}
			{
				if($fml/fml:CI_DATA_WPROWADZENIA)
					then &lt;m:DataWprowadzenia>{ data($fml/fml:CI_DATA_WPROWADZENIA) }&lt;/m:DataWprowadzenia>
					else ()
			}
		&lt;/m:CRMGetProspectResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetProspectResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>