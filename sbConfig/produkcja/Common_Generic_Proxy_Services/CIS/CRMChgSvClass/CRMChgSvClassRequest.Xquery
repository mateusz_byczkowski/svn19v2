<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMChgSvClassRequest($req as element(m:CRMChgSvClassRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:SkpPracownikaRej)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:SkpPracownikaRej) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:IdOperacji)
					then &lt;fml:CI_ID_OPERACJI>{ data($req/m:IdOperacji) }&lt;/fml:CI_ID_OPERACJI>
					else ()
			}
			{
				if($req/m:IdPortfelaAkt)
					then &lt;fml:CI_ID_PORTFELA_AKT>{ data($req/m:IdPortfelaAkt) }&lt;/fml:CI_ID_PORTFELA_AKT>
					else ()
			}
			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI>{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI>
					else ()
			}
			{
				if($req/m:PowodZmiany)
					then &lt;fml:CI_POWOD_ZMIANY>{ data($req/m:PowodZmiany) }&lt;/fml:CI_POWOD_ZMIANY>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA>
					else ()
			}
			{
				for $v in $req/m:NumerKlienta
				return
					&lt;fml:DC_NUMER_KLIENTA>{ data($v) }&lt;/fml:DC_NUMER_KLIENTA>
			}
			{
				for $v in $req/m:BudowaPortfela
				return
					&lt;fml:CI_BUDOWA_PORTFELA>{ data($v) }&lt;/fml:CI_BUDOWA_PORTFELA>
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMChgSvClassRequest($body/m:CRMChgSvClassRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>