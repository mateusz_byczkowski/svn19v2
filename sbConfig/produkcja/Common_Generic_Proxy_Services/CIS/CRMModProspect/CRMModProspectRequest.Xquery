<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMModProspectRequest($req as element(m:CRMModProspectRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:Identyfikacja)
					then &lt;fml:CI_IDENTYFIKACJA>{ data($req/m:Identyfikacja) }&lt;/fml:CI_IDENTYFIKACJA>
					else ()
			}
			{
				if($req/m:IdKlienta)
					then &lt;fml:CI_ID_KLIENTA>{ data($req/m:IdKlienta) }&lt;/fml:CI_ID_KLIENTA>
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE>{ data($req/m:Imie) }&lt;/fml:DC_IMIE>
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO>{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO>
					else ()
			}
			{
				if($req/m:Nazwa)
					then &lt;fml:DC_NAZWA>{ data($req/m:Nazwa) }&lt;/fml:DC_NAZWA>
					else ()
			}
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL>{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL>
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON>{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($req/m:Nip)
					then &lt;fml:DC_NIP>{ data($req/m:Nip) }&lt;/fml:DC_NIP>
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then &lt;fml:DC_NUMER_PASZPORTU>{ data($req/m:NumerPaszportu) }&lt;/fml:DC_NUMER_PASZPORTU>
					else ()
			}
			{
				if($req/m:UdostepGrupa)
					then &lt;fml:CI_UDOSTEP_GRUPA>{ data($req/m:UdostepGrupa) }&lt;/fml:CI_UDOSTEP_GRUPA>
					else ()
			}
			{
				if($req/m:Kraj)
					then &lt;fml:DC_KRAJ>{ data($req/m:Kraj) }&lt;/fml:DC_KRAJ>
					else ()
			}
			{
				if($req/m:Wojewodztwo)
					then &lt;fml:DC_WOJEWODZTWO>{ data($req/m:Wojewodztwo) }&lt;/fml:DC_WOJEWODZTWO>
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then &lt;fml:DC_KOD_POCZTOWY_DANE_PODST>{ data($req/m:KodPocztowyDanePodst) }&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST>
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then &lt;fml:DC_MIASTO_DANE_PODST>{ data($req/m:MiastoDanePodst) }&lt;/fml:DC_MIASTO_DANE_PODST>
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then &lt;fml:DC_ULICA_DANE_PODST>{ data($req/m:UlicaDanePodst) }&lt;/fml:DC_ULICA_DANE_PODST>
					else ()
			}
			{
				if($req/m:NrPosesLokaluDanePodst)
					then &lt;fml:DC_NR_POSES_LOKALU_DANE_PODST>{ data($req/m:NrPosesLokaluDanePodst) }&lt;/fml:DC_NR_POSES_LOKALU_DANE_PODST>
					else ()
			}
			{
				if($req/m:AdresDod)
					then &lt;fml:CI_ADRES_DOD>{ data($req/m:AdresDod) }&lt;/fml:CI_ADRES_DOD>
					else ()
			}
			{
				if($req/m:NrTelefonu)
					then &lt;fml:DC_NR_TELEFONU>{ data($req/m:NrTelefonu) }&lt;/fml:DC_NR_TELEFONU>
					else ()
			}
			{
				if($req/m:NrTelefKomorkowego)
					then &lt;fml:DC_NR_TELEF_KOMORKOWEGO>{ data($req/m:NrTelefKomorkowego) }&lt;/fml:DC_NR_TELEF_KOMORKOWEGO>
					else ()
			}
			{
				if($req/m:NumerFaksu)
					then &lt;fml:DC_NUMER_FAKSU>{ data($req/m:NumerFaksu) }&lt;/fml:DC_NUMER_FAKSU>
					else ()
			}
			{
				if($req/m:AdresEMail)
					then &lt;fml:DC_ADRES_E_MAIL>{ data($req/m:AdresEMail) }&lt;/fml:DC_ADRES_E_MAIL>
					else ()
			}
			{
				if($req/m:Notatki)
					then &lt;fml:CI_NOTATKI>{ data($req/m:Notatki) }&lt;/fml:CI_NOTATKI>
					else ()
			}
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMModProspectRequest($body/m:CRMModProspectRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>