<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/checkDocMIGmessages/";
declare namespace xf = "http://bzwbk.com/services/icbs/checkDocMIGmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapcheckDocMIGResponse($fml as element(fml:FML32))
	as element(m:checkDocMIGResponse) {
		&lt;m:checkDocMIGResponse>
			{
				if($fml/fml:DC_NR_BANKU)
					then &lt;m:NrBanku>{ data($fml/fml:DC_NR_BANKU) }&lt;/m:NrBanku>
					else ()
			}
			{
				if($fml/fml:DC_BANK_NADZORUJACY)
					then &lt;m:BankNadzorujacy>{ data($fml/fml:DC_BANK_NADZORUJACY) }&lt;/m:BankNadzorujacy>
					else ()
			}
			{
				if($fml/fml:CI_DOK_TOZSAMOSCI)
					then &lt;m:DokTozsamosci>{ data($fml/fml:CI_DOK_TOZSAMOSCI) }&lt;/m:DokTozsamosci>
					else ()
			}
			{
				if($fml/fml:CI_SERIA_NR_DOK)
					then &lt;m:SeriaNrDok>{ data($fml/fml:CI_SERIA_NR_DOK) }&lt;/m:SeriaNrDok>
					else ()
			}
			{
				if($fml/fml:CI_NAZWA_PELNA)
					then &lt;m:NazwaPelna>{ data($fml/fml:CI_NAZWA_PELNA) }&lt;/m:NazwaPelna>
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then &lt;m:NrPesel>{ data($fml/fml:DC_NR_PESEL) }&lt;/m:NrPesel>
					else ()
			}
			{
				if($fml/fml:CI_STATUS)
					then &lt;m:Status>{ data($fml/fml:CI_STATUS) }&lt;/m:Status>
					else ()
			}
			{
				if($fml/fml:DC_DATA_ZASTRZE)
					then &lt;m:DataZastrze>{ data($fml/fml:DC_DATA_ZASTRZE) }&lt;/m:DataZastrze>
					else ()
			}
			{
				if($fml/fml:DC_GODZINA_ZASTRZE)
					then &lt;m:GodzinaZastrze>{ data($fml/fml:DC_GODZINA_ZASTRZE) }&lt;/m:GodzinaZastrze>
					else ()
			}
		&lt;/m:checkDocMIGResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapcheckDocMIGResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>