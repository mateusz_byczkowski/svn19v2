<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetDictionaryResponse($fml as element(fml:FML32))
	as element(m:GetDictionaryResponse) {
		&lt;m:GetDictionaryResponse>
			{

				let $B_IDENTYFIKATOR2 := $fml/fml:B_IDENTYFIKATOR2
				let $B_KOD_PS := $fml/fml:B_KOD_PS
				let $B_NAZWA := $fml/fml:B_NAZWA
				let $B_WALUTA := $fml/fml:B_WALUTA
				let $B_RET_DATA := $fml/fml:B_RET_DATA
				let $B_STOPA := $fml/fml:B_STOPA
				let $B_STOPA_IND := $fml/fml:B_STOPA_IND
				for $it at $p in $fml/fml:B_KOD_PS
				return
					&lt;m:Entry>
					{
						if($B_IDENTYFIKATOR2[$p])
							then &lt;m:Identyfikator2>{ data($B_IDENTYFIKATOR2[$p]) }&lt;/m:Identyfikator2>
						else ()
					}
					{
						if($B_KOD_PS[$p])
							then &lt;m:KodPs>{ data($B_KOD_PS[$p]) }&lt;/m:KodPs>
						else ()
					}
					{
						if($B_NAZWA[$p])
							then &lt;m:Nazwa>{ data($B_NAZWA[$p]) }&lt;/m:Nazwa>
						else ()
					}
					{
						if($B_WALUTA[$p])
							then &lt;m:Waluta>{ data($B_WALUTA[$p]) }&lt;/m:Waluta>
						else ()
					}
					{
						if($B_RET_DATA[$p])
							then &lt;m:RetData>{ data($B_RET_DATA[$p]) }&lt;/m:RetData>
						else ()
					}
					{
						if($B_STOPA[$p])
							then &lt;m:Stopa>{ data($B_STOPA[$p]) }&lt;/m:Stopa>
						else ()
					}
					{
						if($B_STOPA_IND[$p])
							then &lt;m:StopaInd>{ data($B_STOPA_IND[$p]) }&lt;/m:StopaInd>
						else ()
					}
					&lt;/m:Entry>
			}

		&lt;/m:GetDictionaryResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetDictionaryResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>