<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Pomija puste pola w buforze FML32Version.$1.2011-04-06</con:description>
  <con:xquery>xquery version "1.0";

declare namespace xf = "urn:fml32.skip.null";

declare function xf:fml32_skip_null($in as element())
as element() {
&lt;FML32>
{
	if (data($in))
	then
	(
		for $elt in $in/FML32/*
		return
			if (data($elt))
			then $elt
			else ()
	)
	else ()
}
&lt;/FML32>
};

declare variable $in as element() external;

&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
{
	xf:fml32_skip_null($in)
}
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>