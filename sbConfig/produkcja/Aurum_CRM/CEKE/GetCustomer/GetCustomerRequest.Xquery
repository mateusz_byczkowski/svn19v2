<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCustomerRequest($req as element(m:GetCustomerRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:RodzDok)
					then &lt;fml:B_RODZ_DOK>{ data($req/m:RodzDok) }&lt;/fml:B_RODZ_DOK>
					else ()
			}
			{
				if($req/m:NrDok)
					then &lt;fml:B_NR_DOK>{ data($req/m:NrDok) }&lt;/fml:B_NR_DOK>
					else ()
			}
			{
				if($req/m:DlNrRach)
					then &lt;fml:B_DL_NR_RACH>{ data($req/m:DlNrRach) }&lt;/fml:B_DL_NR_RACH>
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:B_OPCJA>{ data($req/m:Opcja) }&lt;/fml:B_OPCJA>
					else ()
			}
			{
				if($req/m:IdOddz)
					then &lt;fml:B_ID_ODDZ>{ data($req/m:IdOddz) }&lt;/fml:B_ID_ODDZ>
					else ()
			}
			{
				if($req/m:CustomerType)
					then &lt;fml:E_CUSTOMER_TYPE>{ data($req/m:CustomerType) }&lt;/fml:E_CUSTOMER_TYPE>
					else ()
			}
			{
				if($req/m:CifNumber)
					then &lt;fml:E_CIF_NUMBER>{ data($req/m:CifNumber) }&lt;/fml:E_CIF_NUMBER>
					else ()
			}
			{
				if($req/m:CifOptions)
					then &lt;fml:E_CIF_OPTIONS>{ data($req/m:CifOptions) }&lt;/fml:E_CIF_OPTIONS>
					else ()
			}
			{
				if($req/m:Sys)
					then &lt;fml:B_SYS>{ data($req/m:Sys) }&lt;/fml:B_SYS>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetCustomerRequest($body/m:GetCustomerRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>