<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetProductsRangeRequest($req as element(m:getProductsRange))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/userDescription/microbranch)
					then &lt;fml:E_MICRO_BRANCH>{ data($req/userDescription/microbranch) }&lt;/fml:E_MICRO_BRANCH>
					else ()
			}
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME>{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/productKind)
					then &lt;fml:B_RODZAJ_RACH>{ data($req/productKind) }&lt;/fml:B_RODZAJ_RACH>
					else ()
			}
			{
				if($req/currency)
					then &lt;fml:B_KOD_WALUTY>{ data($req/currency) }&lt;/fml:B_KOD_WALUTY>
					else ()
			}
			{
				if($req/option)
					then &lt;fml:B_OPCJA>{ data($req/option) }&lt;/fml:B_OPCJA>
					else ()
			}
			{
				if($req/productsNo)
					then &lt;fml:B_LICZBA_REK>{ data($req/productsNo) }&lt;/fml:B_LICZBA_REK>
					else ()
			}
			{
				if($req/accountTypeId)
					then &lt;fml:B_TYP_RACH>{ data($req/accountTypeId) }&lt;/fml:B_TYP_RACH>
					else ()
			}
			{
				if($req/serviceCode)
					then &lt;fml:B_KOD_USLUGI>{ data($req/serviceCode) }&lt;/fml:B_KOD_USLUGI>
					else ()
			}
			{
				if($req/paymentPlan)
					then &lt;fml:B_PLAN_OPLAT>{ data($req/paymentPlan) }&lt;/fml:B_PLAN_OPLAT>
					else ()
			}
			{
				if($req/subtype)
					then &lt;fml:B_ITYP>{ data($req/subtype) }&lt;/fml:B_ITYP>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetProductsRangeRequest($body/m:getProductsRange) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>