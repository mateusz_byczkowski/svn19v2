<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapcreateAccountTypeRequest($req as element(m:createAccountType))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/userDescription/microbranch)
					then &lt;fml:E_MICRO_BRANCH>{ data($req/userDescription/microbranch) }&lt;/fml:E_MICRO_BRANCH>
					else ()
			}
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME>{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/accountType/name)
					then &lt;fml:E_ACCOUNT_TYPE_NAME>{ data($req/accountType/name) }&lt;/fml:E_ACCOUNT_TYPE_NAME>
					else ()
			}
			{
				if($req/accountType/defaultChannelAllowance)
					then &lt;fml:E_DEF_CHANNEL_ALLOW>{ data($req/accountType/defaultChannelAllowance) }&lt;/fml:E_DEF_CHANNEL_ALLOW>
					else ()
			}
			{
				if ($req/accountType/productType)
					then if (fn:string($req/accountType/productType) != '')
						then &lt;fml:B_RODZAJ_RACH>{ data($req/accountType/productType) }&lt;/fml:B_RODZAJ_RACH>
						else &lt;fml:B_RODZAJ_RACH nil="true"/>
					else ()
			}
			{
				if($req/accountType/creditTransactionMask)
					then &lt;fml:E_CR_TRN_MASK>{ data($req/accountType/creditTransactionMask) }&lt;/fml:E_CR_TRN_MASK>
					else ()
			}
			{
				if($req/accountType/debitTransactionMask)
					then &lt;fml:E_DR_TRN_MASK>{ data($req/accountType/debitTransactionMask) }&lt;/fml:E_DR_TRN_MASK>
					else ()
			}
			{
				if($req/accountType/options)
					then &lt;fml:E_ACCOUNT_TYPE_OPTIONS>{ data($req/accountType/options) }&lt;/fml:E_ACCOUNT_TYPE_OPTIONS>
					else ()
			}
			{
				if($req/accountType/accountTypeId)
					then &lt;fml:B_TYP_RACH>{ data($req/accountType/accountTypeId) }&lt;/fml:B_TYP_RACH>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapcreateAccountTypeRequest($body/m:createAccountType) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>