<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetShopTrnResponse($fml as element(fml:FML32))
	as element(m:getShopTrnResponse) {
		&lt;m:getShopTrnResponse>
			{
				if($fml/fml:E_REC_COUNT)
					then &lt;m:recCount>{ data($fml/fml:E_REC_COUNT) }&lt;/m:recCount>
					else ()
			}
		&lt;getShopTrnReturn xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="soapenc:Array" soapenc:arrayType="m:ShopTrn[0]" xmlns="">
			{

				let $E_SHOP_ID := $fml/fml:E_SHOP_ID
				let $E_SHOP_TRANS_ID := $fml/fml:E_SHOP_TRANS_ID
				let $E_TRN_STATE := $fml/fml:E_TRN_STATE
				let $E_TRN_ID := $fml/fml:E_TRN_ID
				let $E_TRN_DATE := $fml/fml:E_TRN_DATE
				let $E_SEND_TIME := $fml/fml:E_SEND_TIME
				for $it at $p in $fml/fml:E_SHOP_TRANS_ID
				return
					&lt;item>
					{
						if($E_SHOP_ID[$p])
							then &lt;shopId>{ data($E_SHOP_ID[$p]) }&lt;/shopId>
						else ()
					}
					{
						if($E_SHOP_TRANS_ID[$p])
							then &lt;shopTransId>{ data($E_SHOP_TRANS_ID[$p]) }&lt;/shopTransId>
						else ()
					}
					{
						if($E_TRN_STATE[$p])
							then &lt;trnState>{ data($E_TRN_STATE[$p]) }&lt;/trnState>
						else ()
					}
					{
						if($E_TRN_ID[$p])
							then &lt;trnId>{ data($E_TRN_ID[$p]) }&lt;/trnId>
						else ()
					}
					{
						if($E_TRN_DATE[$p])
							then &lt;trnDate>{ data($E_TRN_DATE[$p]) }&lt;/trnDate>
						else ()
					}
					{
						if($E_SEND_TIME[$p])
							then &lt;sendTime>{ data($E_SEND_TIME[$p]) }&lt;/sendTime>
						else ()
					}
					&lt;/item>
			}
                &lt;/getShopTrnReturn>
		&lt;/m:getShopTrnResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetShopTrnResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>