<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapcreateShopResponse($fml as element(fml:FML32))
	as element(m:createShopResponse) {
		&lt;m:createShopResponse>
			{
				if($fml/fml:E_SHOP_ID)
					then &lt;createShopReturn>{ data($fml/fml:E_SHOP_ID) }&lt;/createShopReturn>
					else ()
			}
		&lt;/m:createShopResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapcreateShopResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>