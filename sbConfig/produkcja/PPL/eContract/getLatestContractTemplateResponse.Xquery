<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetLatestContractTemplateResponse($fml as element(fml:FML32))
	as element(m:getLatestContractTemplateResponse) {
		&lt;m:getLatestContractTemplateResponse>
		&lt;getLatestContractTemplateReturn>
			{
				if($fml/fml:E_DOC_TEMPL_TYPE)
					then &lt;type>{ data($fml/fml:E_DOC_TEMPL_TYPE) }&lt;/type>
					else ()
			}
			{
				if($fml/fml:E_DOC_TEMPL_VERSION)
					then &lt;version>{ data($fml/fml:E_DOC_TEMPL_VERSION) }&lt;/version>
					else ()
			}
			{
				if($fml/fml:E_DOC_TEMPL_STATUS)
					then &lt;status>{ data($fml/fml:E_DOC_TEMPL_STATUS) }&lt;/status>
					else ()
			}
			{
				if($fml/fml:E_DOC_TEMPL_DATE)
					then &lt;date>{ data($fml/fml:E_DOC_TEMPL_DATE) }&lt;/date>
					else ()
			}
			{
				if($fml/fml:E_DOC_TEMPL_COMMENT)
					then &lt;comment>{ data($fml/fml:E_DOC_TEMPL_COMMENT) }&lt;/comment>
					else ()
			}
			{
				if($fml/fml:E_TIME_STAMP)
					then &lt;timestamp>{ data($fml/fml:E_TIME_STAMP) }&lt;/timestamp>
					else ()
			}
			{
				if($fml/fml:E_DOC_CONTENT)
					then &lt;content>{ data($fml/fml:E_DOC_CONTENT) }&lt;/content>
					else ()
			}
		&lt;/getLatestContractTemplateReturn>
		&lt;/m:getLatestContractTemplateResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetLatestContractTemplateResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>