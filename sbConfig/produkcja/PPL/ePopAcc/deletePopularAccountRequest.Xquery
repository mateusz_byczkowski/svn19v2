<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapdeletePopularAccountRequest($req as element(m:deletePopularAccount))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/userDescription/microbranch)
					then &lt;fml:E_MICRO_BRANCH>{ data($req/userDescription/microbranch) }&lt;/fml:E_MICRO_BRANCH>
					else ()
			}
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME>{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/accountNo)
					then &lt;fml:B_DL_NR_RACH>{ data($req/accountNo) }&lt;/fml:B_DL_NR_RACH>
					else ()
			}
			{
				if($req/timestamp)
					then &lt;fml:E_TIME_STAMP>{ data($req/timestamp) }&lt;/fml:E_TIME_STAMP>
					else ()
			}
			{
				if($req/accountType)
					then &lt;fml:B_TYP_RACH>{ data($req/accountType) }&lt;/fml:B_TYP_RACH>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapdeletePopularAccountRequest($body/m:deletePopularAccount) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>