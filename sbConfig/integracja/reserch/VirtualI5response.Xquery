<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace esb="http://bzwbk.com/services/esbmessages/";
declare namespace xs="http://www.w3.org/2001/XMLSchema";

declare variable $body as element(soap:Body) external;
declare variable $numberOfRecords as xs:string external;

declare function prepareICBSGetCustomersResponse($res as element(xpcml) , $limit as xs:string)
               as element(esb:GetCustomersResponse){

    let $array := $res/program/parameterList/structParm[@name="KLIENCI"]/arrayOfStructParm
    let $maxidx:=number($limit)

      return
     &lt;esb:GetCustomersResponse>
         { for $cust in $array/*[@index&lt;$maxidx]
            return 
                   &lt;customer>
                       &lt;NumerKlienta>{data($cust/stringParm[@name="CUNBR"])}&lt;/NumerKlienta>
                       &lt;Status>{data($cust/stringParm[@name="CUSTAT"])}&lt;/Status>
                       &lt;Nazwa>{data($cust/stringParm[@name="CUNA1"])}&lt;/Nazwa>
                       &lt;NazwaCD>{data($cust/stringParm[@name="CUNA2"])}&lt;/NazwaCD>
                       &lt;Ulica>{data($cust/stringParm[@name="CUNA3"])}&lt;/Ulica>
                       &lt;NrDomu>{data($cust/stringParm[@name="CUNA4"])}&lt;/NrDomu>
                       &lt;Miasto>{data($cust/stringParm[@name="CUNA5"])}&lt;/Miasto>
                       &lt;Wojewodztwo>{data($cust/stringParm[@name="CUNA6"])}&lt;/Wojewodztwo>
                       &lt;KodPocztowy>{data($cust/stringParm[@name="CUZIP"])}&lt;/KodPocztowy>
                       &lt;KodPocztowy2>{data($cust/stringParm[@name="CUZIP2"])}&lt;/KodPocztowy2>
                       &lt;KodPocztowy3>{data($cust/stringParm[@name="CUZIP3"])}&lt;/KodPocztowy3>
                       &lt;KodPocztowy4>{data($cust/stringParm[@name="CUZIP4"])}&lt;/KodPocztowy4>
                       &lt;NazwaKrotka>{data($cust/stringParm[@name="CUSHRT"])}&lt;/NazwaKrotka>
                       &lt;Pesel>{data($cust/stringParm[@name="CUSSNR"])}&lt;/Pesel>
                   &lt;/customer>
                 
         }
    &lt;/esb:GetCustomersResponse>
};


&lt;soap:Body>
{prepareICBSGetCustomersResponse($body/soap:Body/xpcml , $numberOfRecords)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>