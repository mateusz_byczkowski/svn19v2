<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace esb="http://bzwbk.com/services/esbmessages/";
declare namespace xs="http://www.w3.org/2001/XMLSchema";
declare namespace cis="http://bzwbk.com/services/icbs/CISGetCustEgmessages/";

declare variable $body as element(soap:Body) external;

declare function prepareICBSGetCustomersResponse($res as element(xpcml))
               as element(cis:CISGetCustEgResponse ){

    let $retstruct := $res/program/parameterList/structParm[@name="PARBUFF"]
    let $array := $res/program/parameterList/structParm[@name="PARBUFFOUT"]/arrayOfStructParm
    let $maxidx:=data($retstruct/packedDecimalParm[2])
    

      return
     &lt;cis:CISGetCustEgResponse>
         &lt;cis:OpisBledu>{data($retstruct/stringParm[12])}&lt;/cis:OpisBledu>
         &lt;cis:LiczbaRekordow>{$maxidx}&lt;/cis:LiczbaRekordow>
         { for $cust in $array/*[@index&lt;number($maxidx)]
            return 
                   &lt;customer>
                       &lt;cis:NumerKlienta>{data($cust/stringParm[1])}&lt;/cis:NumerKlienta>
                       &lt;cis:Typ>{data($cust/stringParm[2])}&lt;/cis:Typ>
                       &lt;cis:TypPodmiotu>{data($cust/packedDecimalParm[1])}&lt;/cis:TypPodmiotu>
	               &lt;cis:Oddzial>{data($cust/packedDecimalParm[2])}&lt;/cis:Oddzial>
                       &lt;cis:Nazwa>{data($cust/stringParm[3])}&lt;/cis:Nazwa>
	               &lt;cis:NazwaCD>{data($cust/stringParm[4])}&lt;/cis:NazwaCD>
	               &lt;cis:UlicaDanePodst>{data($cust/stringParm[5])}&lt;/cis:UlicaDanePodst>
	               &lt;cis:MiastoDanePodst>{data($cust/stringParm[6])}&lt;/cis:MiastoDanePodst>
	               &lt;cis:NrPosesLokaluDanePodst>{data($cust/stringParm[7])}&lt;/cis:NrPosesLokaluDanePodst>
	               &lt;cis:KodPocztowyDanePodst>{data($cust/stringParm[8])}&lt;/cis:KodPocztowyDanePodst>
	               &lt;cis:Nip>{data($cust/stringParm[9])}&lt;/cis:Nip>
                       &lt;cis:NrPesel>{data($cust/packedDecimalParm[3])}&lt;/cis:NrPesel>
                       &lt;cis:NrDowoduRegon>{data($cust/stringParm[10])}&lt;/cis:NrDowoduRegon>
                       &lt;cis:DataUrodzenia>{data($cust/zonedDecimalParm[1])}&lt;/cis:DataUrodzenia>
	               &lt;cis:ImieOjca>{data($cust/stringParm[11])}&lt;/cis:ImieOjca>
	               &lt;cis:KlasaObslugi>{data($cust/zonedDecimalParm[2])}&lt;/cis:KlasaObslugi>
	               &lt;cis:Imie>{data($cust/stringParm[12])}&lt;/cis:Imie>
	               &lt;cis:Nazwisko>{data($cust/stringParm[13])}&lt;/cis:Nazwisko>
	               &lt;cis:NazwaPracodawcy>{data($cust/stringParm[14])}&lt;/cis:NazwaPracodawcy>
                   &lt;/customer>
                 
         }
    &lt;/cis:CISGetCustEgResponse>
};


&lt;soap:Body>
{prepareICBSGetCustomersResponse($body/xpcml)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>