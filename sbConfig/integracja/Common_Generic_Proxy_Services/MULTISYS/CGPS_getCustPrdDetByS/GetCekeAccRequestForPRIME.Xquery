<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $nrRachunku as xs:anyType external;
declare variable $nrKlienta as xs:anyType external;
declare variable $system  as xs:anyType external;

   <fml:FML32> 
       <fml:B_KOD_RACH>{$nrRachunku}</fml:B_KOD_RACH>
       <fml:B_SYS>{$system}</fml:B_SYS>
       <fml:B_OPCJA>2</fml:B_OPCJA>
       <fml:E_CIF_OPTIONS>2</fml:E_CIF_OPTIONS>
       <fml:E_CIF_NUMBER>{$nrKlienta}</fml:E_CIF_NUMBER>
       <fml:B_RODZ_DOK>A</fml:B_RODZ_DOK>
       <fml:B_NR_DOK></fml:B_NR_DOK>
   </fml:FML32>]]></con:xquery>
</con:xqueryEntry>