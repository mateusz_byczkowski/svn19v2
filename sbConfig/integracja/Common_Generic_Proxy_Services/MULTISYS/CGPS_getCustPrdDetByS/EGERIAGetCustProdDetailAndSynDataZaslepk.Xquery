<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace fml="";

declare variable $body external;


<soap-env:Body>
    <FML32> 
       <CI_PRODUCT_ATT1>L09</CI_PRODUCT_ATT1>
       <CI_PRODUCT_ATT2/>
       <CI_PRODUCT_ATT3/>
       <CI_PRODUCT_ATT4/>
       <CI_PRODUCT_ATT5/>
       <CI_ID_SYS>3</CI_ID_SYS>
       <B_POJEDYNCZY>T</B_POJEDYNCZY>
       <DC_NR_RACHUNKU>LP6/00016/2005</DC_NR_RACHUNKU>
       <CI_RACHUNEK_ADR_ALT>N</CI_RACHUNEK_ADR_ALT>
       <CI_STATUS_AKTYWNOSCI>T</CI_STATUS_AKTYWNOSCI>
       <CI_STATUS>T</CI_STATUS>
       <B_SALDO>328043.36</B_SALDO>
       <B_KOD_WALUTY>PLN</B_KOD_WALUTY>
       <B_D_OTWARCIA>2005-10-19</B_D_OTWARCIA>
       <CI_PRODUCT_NAME_ORYGINAL>Pożyczka rolnicza</CI_PRODUCT_NAME_ORYGINAL>
       <B_OPROC1/> 

(: pozostałe pola typowe z EGERII zaślepione na poziomie proxy CRMGetCustomerProductDetail ze względnu na niedostępność pliku EGERIA.TBL :)
   

 

  </FML32> 
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>