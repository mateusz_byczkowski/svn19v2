<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace fml="";

declare variable $body external;


&lt;soap-env:Body>
  &lt;soap-env:Body>
	{
	  (: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
	  let $reason := fn:substring-before(fn:substring-after(fn:substring-before($body/ctx:fault/ctx:reason, ":"), "("), ")")
	  let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($body/ctx:fault/ctx:reason, ":"), ":"), ":")
	  return
           &lt;FML32>       
            { if( string-length(data($reason)) = 0)
                 then &lt;TPAERRNO>12&lt;/TPAERRNO>
                 else &lt;TPAERRNO>{$reason}&lt;/TPAERRNO>
            }           
            { if( string-length(data($urcode)) = 0)
                 then &lt;TPAURCODE>0&lt;/TPAURCODE>
                 else &lt;TPAURCODE>{data($urcode)}&lt;/TPAURCODE>
            }
            &lt;/FML32>
	}
   
  &lt;/soap-env:Body>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>