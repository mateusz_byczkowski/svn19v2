<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetViPorCustRequest($req as element(m:CRMGetViPorCustRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA>{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA>
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER>{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI>{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI>
					else ()
			}
			{
				if($req/m:ZakresDanych)
					then &lt;fml:CI_ZAKRES_DANYCH>{ data($req/m:ZakresDanych) }&lt;/fml:CI_ZAKRES_DANYCH>
					else ()
			}
			{
				for $v in $req/m:StatusGiodo
				return
					&lt;fml:CI_STATUS_GIODO>{ data($v) }&lt;/fml:CI_STATUS_GIODO>
			}
			{
				for $v in $req/m:UdostepGrupa
				return
					&lt;fml:CI_UDOSTEP_GRUPA>{ data($v) }&lt;/fml:CI_UDOSTEP_GRUPA>
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON>{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL>{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL>
					else ()
			}
			{
				if($req/m:Nip)
					then &lt;fml:DC_NIP>{ data($req/m:Nip) }&lt;/fml:DC_NIP>
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then &lt;fml:DC_NUMER_PASZPORTU>{ data($req/m:NumerPaszportu) }&lt;/fml:DC_NUMER_PASZPORTU>
					else ()
			}
			{
				if($req/m:Nik)
					then &lt;fml:CI_NIK>{ data($req/m:Nik) }&lt;/fml:CI_NIK>
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO>{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO>
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE>{ data($req/m:Imie) }&lt;/fml:DC_IMIE>
					else ()
			}
			{
				if($req/m:NazwaPelna)
					then &lt;fml:CI_NAZWA_PELNA>{ data($req/m:NazwaPelna) }&lt;/fml:CI_NAZWA_PELNA>
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then &lt;fml:DC_MIASTO_DANE_PODST>{ data($req/m:MiastoDanePodst) }&lt;/fml:DC_MIASTO_DANE_PODST>
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then &lt;fml:DC_KOD_POCZTOWY_DANE_PODST>{ data($req/m:KodPocztowyDanePodst) }&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST>
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then &lt;fml:DC_ULICA_DANE_PODST>{ data($req/m:UlicaDanePodst) }&lt;/fml:DC_ULICA_DANE_PODST>
					else ()
			}
			{
				if($req/m:KlasaObslugiAkt)
					then &lt;fml:CI_KLASA_OBSLUGI_AKT>{ data($req/m:KlasaObslugiAkt) }&lt;/fml:CI_KLASA_OBSLUGI_AKT>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetViPorCustRequest($body/m:CRMGetViPorCustRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>