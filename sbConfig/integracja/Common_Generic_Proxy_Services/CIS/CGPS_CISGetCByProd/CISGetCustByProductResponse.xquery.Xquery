<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/esbmessages/";
declare namespace xf = "http://bzwbk.com/services/esbmessages/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISGetCustByProdResponse($fml as element(fml:FML32))
	as element(m:CISGetCustByProdResponse) {
		&lt;m:CISGetCustByProdResponse>
			{
				if($fml/fml:CI_PRODUCT_AREA_ID)
					then &lt;m:CI_PRODUCT_AREA_ID>{ data($fml/fml:CI_PRODUCT_AREA_ID) }&lt;/m:CI_PRODUCT_AREA_ID>
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_CATEGORY_ID)
					then &lt;m:CI_PRODUCT_CATEGORY_ID>{ data($fml/fml:CI_PRODUCT_CATEGORY_ID) }&lt;/m:CI_PRODUCT_CATEGORY_ID>
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ID)
					then &lt;m:CI_PRODUCT_ID>{ data($fml/fml:CI_PRODUCT_ID) }&lt;/m:CI_PRODUCT_ID>
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_CODE)
					then &lt;m:CI_PRODUCT_CODE>{ data($fml/fml:CI_PRODUCT_CODE) }&lt;/m:CI_PRODUCT_CODE>
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_NAME_EN)
					then &lt;m:CI_PRODUCT_NAME_EN>{ data($fml/fml:CI_PRODUCT_NAME_EN) }&lt;/m:CI_PRODUCT_NAME_EN>
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_NAME_PL)
					then &lt;m:CI_PRODUCT_NAME_PL>{ data($fml/fml:CI_PRODUCT_NAME_PL) }&lt;/m:CI_PRODUCT_NAME_PL>
					else ()
			}
			{
				if($fml/fml:DC_NR_RACHUNKU)
					then &lt;m:DC_NR_RACHUNKU>{ data($fml/fml:DC_NR_RACHUNKU) }&lt;/m:DC_NR_RACHUNKU>
					else ()
			}
			{
				if($fml/fml:DC_NR_RACHUNKU_W_SYSTEMIE)
					then &lt;m:DC_NR_ALTERNATYWNY>{ data($fml/fml:DC_NR_RACHUNKU_W_SYSTEMIE) }&lt;/m:DC_NR_ALTERNATYWNY>
					else ()
			}
			{
				if($fml/fml:B_POJEDYNCZY)
					then  if(data($fml/fml:B_POJEDYNCZY)="T" or data($fml/fml:B_POJEDYNCZY)="N") 
                                                     then &lt;m:B_POJEDYNCZY>{ data($fml/fml:B_POJEDYNCZY) }&lt;/m:B_POJEDYNCZY>
					         else &lt;m:B_POJEDYNCZY>N&lt;/m:B_POJEDYNCZY>
                                        else &lt;m:B_POJEDYNCZY>N&lt;/m:B_POJEDYNCZY>
			}
			{
				if($fml/fml:CI_PRODUCT_ATT1)
					then &lt;m:CI_PRODUCT_ATT1>{ data($fml/fml:CI_PRODUCT_ATT1) }&lt;/m:CI_PRODUCT_ATT1>
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT2)
					then &lt;m:CI_PRODUCT_ATT2>{ data($fml/fml:CI_PRODUCT_ATT2) }&lt;/m:CI_PRODUCT_ATT2>
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT3)
					then &lt;m:CI_PRODUCT_ATT3>{ data($fml/fml:CI_PRODUCT_ATT3) }&lt;/m:CI_PRODUCT_ATT3>
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT4)
					then &lt;m:CI_PRODUCT_ATT4>{ data($fml/fml:CI_PRODUCT_ATT4) }&lt;/m:CI_PRODUCT_ATT4>
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT5)
					then &lt;m:CI_PRODUCT_ATT5>{ data($fml/fml:CI_PRODUCT_ATT5) }&lt;/m:CI_PRODUCT_ATT5>
					else ()
			}
			{
 			if($fml/fml:DC_NUMER_KLIENTA)
			   then &lt;m:DC_NUMER_KLIENTA>{ data($fml/fml:DC_NUMER_KLIENTA) }&lt;/m:DC_NUMER_KLIENTA>
			   else ()
		   	} 
			{
 			if($fml/fml:CI_NAZWA_PELNA)
			   then &lt;m:CI_NAZWA_PELNA>{ data($fml/fml:CI_NAZWA_PELNA) }&lt;/m:CI_NAZWA_PELNA>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_ULICA_DANE_PODST)
			   then &lt;m:DC_ULICA_DANE_PODST>{ data($fml/fml:DC_ULICA_DANE_PODST) }&lt;/m:DC_ULICA_DANE_PODST>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST)
			   then &lt;m:DC_NR_POSES_LOKALU_DANE_PODST>{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST) }&lt;/m:DC_NR_POSES_LOKALU_DANE_PODST>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_MIASTO_DANE_PODST)
			   then &lt;m:DC_MIASTO_DANE_PODST>{ data($fml/fml:DC_MIASTO_DANE_PODST) }&lt;/m:DC_MIASTO_DANE_PODST>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_WOJ_KRAJ_DANE_PODST)
			   then &lt;m:DC_WOJ_KRAJ_DANE_PODST>{ data($fml/fml:DC_WOJ_KRAJ_DANE_PODST) }&lt;/m:DC_WOJ_KRAJ_DANE_PODST>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NUMER_ODDZIALU)
			   then &lt;m:DC_NUMER_ODDZIALU>{ data($fml/fml:DC_NUMER_ODDZIALU) }&lt;/m:DC_NUMER_ODDZIALU>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_IMIE)
			   then &lt;m:DC_IMIE>{ data($fml/fml:DC_IMIE) }&lt;/m:DC_IMIE>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NAZWISKO)
			   then &lt;m:DC_NAZWISKO>{ data($fml/fml:DC_NAZWISKO) }&lt;/m:DC_NAZWISKO>
			   else ()
		   	} 
			{
 			if($fml/fml:CI_STATUS_GIODO)
			   then &lt;m:CI_STATUS_GIODO>{ data($fml/fml:CI_STATUS_GIODO) }&lt;/m:CI_STATUS_GIODO>
			   else ()
		   	} 
			{
 			if($fml/fml:CI_UDOSTEP_GRUPA)
			   then &lt;m:CI_UDOSTEP_GRUPA>{ data($fml/fml:CI_UDOSTEP_GRUPA) }&lt;/m:CI_UDOSTEP_GRUPA>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NR_DOWODU_REGON)
			   then &lt;m:DC_NR_DOWODU_REGON>{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:DC_NR_DOWODU_REGON>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NR_PESEL)
			   then &lt;m:DC_NR_PESEL>{ data($fml/fml:DC_NR_PESEL) }&lt;/m:DC_NR_PESEL>
			   else ()
		   	} 
			{
 			if($fml/fml:CI_WIECEJ_ADDR_KORESP)
			   then &lt;m:CI_WIECEJ_ADR_KORESP>{ data($fml/fml:CI_WIECEJ_ADDR_KORESP) }&lt;/m:CI_WIECEJ_ADR_KORESP>
			   else &lt;m:CI_WIECEJ_ADR_KORESP>N&lt;/m:CI_WIECEJ_ADR_KORESP>
		   	} 
			{
 			if($fml/fml:DC_IMIE_I_NAZWISKO_ALT)
			   then &lt;m:DC_IMIE_I_NAZWISKO_ALT>{ data($fml/fml:DC_IMIE_I_NAZWISKO_ALT) }&lt;/m:DC_IMIE_I_NAZWISKO_ALT>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_ULICA_ADRES_ALT)
			   then &lt;m:DC_ULICA_ADRES_ALT>{ data($fml/fml:DC_ULICA_ADRES_ALT) }&lt;/m:DC_ULICA_ADRES_ALT>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT)
			   then &lt;m:DC_NR_POSES_LOKALU_ADRES_ALT>{ data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT) }&lt;/m:DC_NR_POSES_LOKALU_ADRES_ALT>
			   else ()
		   	} 
			{
 			if($fml/fml:DC_MIASTO_ADRES_ALT)
			   then &lt;m:DC_MIASTO_ADRES_ALT>{ data($fml/fml:DC_MIASTO_ADRES_ALT) }&lt;/m:DC_MIASTO_ADRES_ALT>
			   else ()
		   	} 
		        {
 			if($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT)
			   then &lt;m:DC_KOD_POCZTOWY_ADRES_ALT>{ data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT) }&lt;/m:DC_KOD_POCZTOWY_ADRES_ALT>
			   else ()
		   	} 
		        {
 			if($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT)
			   then &lt;m:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{ data($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT) }&lt;/m:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
			   else ()
		   	} 
		        {
 			if($fml/fml:DC_NR_TELEFONU_KOMORKOWEGO)
			   then &lt;m:DC_NR_TELEFONU_KOMORKOWEGO>{ data($fml/fml:DC_NR_TELEFONU_KOMORKOWEGO) }&lt;/m:DC_NR_TELEFONU_KOMORKOWEGO>
			   else ()
		   	} 
		        {
 			if($fml/fml:DC_NR_TELEFONU)
			   then &lt;m:DC_NR_TELEFONU>{ data($fml/fml:DC_NR_TELEFONU) }&lt;/m:DC_NR_TELEFONU>
			   else ()
		   	} 
		        {
 			if($fml/fml:DC_ADRES_EMAIL)
			   then &lt;m:DC_ADRES_EMAIL>{ data($fml/fml:DC_ADRES_EMAIL) }&lt;/m:DC_ADRES_EMAIL>
			   else ()
		   	} 
		        {
 			if($fml/fml:CI_RELACJA)
			   then &lt;m:CI_RELACJA>{ data($fml/fml:CI_RELACJA) }&lt;/m:CI_RELACJA>
			   else ()
		   	} 
		        {
 			if($fml/fml:CI_KLASA_OBSLUGI)
			   then &lt;m:CI_KLASA_OBSLUGI>{ data($fml/fml:CI_KLASA_OBSLUGI) }&lt;/m:CI_KLASA_OBSLUGI>
			   else ()
		   	} 
		        
 
                        
                         
		&lt;/m:CISGetCustByProdResponse>
};
declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCISGetCustByProdResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>