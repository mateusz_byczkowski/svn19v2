<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAsgnPortfEmpRequest($req as element(m:CRMAsgnPortfEmpRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:SkpPracownikaRej)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:SkpPracownikaRej) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:ImiePrac)
					then &lt;fml:CI_IMIE_PRAC>{ data($req/m:ImiePrac) }&lt;/fml:CI_IMIE_PRAC>
					else ()
			}
			{
				if($req/m:NazwPrac)
					then &lt;fml:CI_NAZW_PRAC>{ data($req/m:NazwPrac) }&lt;/fml:CI_NAZW_PRAC>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA>{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMAsgnPortfEmpRequest($body/m:CRMAsgnPortfEmpRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>