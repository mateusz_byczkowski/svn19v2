<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-08-04</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns1:header" location="cisChkCustRelationExist.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns1:invoke" location="cisChkCustRelationExist.wsdl" ::)
(:: pragma bea:global-element-return element="ns2:FML32" location="../../Operations/savetransfer/CISIN.xsd" ::)

declare namespace ns2 = "";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns3 = "urn:baseauxentities.be.dcl";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Customers/cisChkCustRelationExist/cisChkCustRelationExistRequest/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:cisChkCustRelationExistRequest(
    $invoke1 as element(ns1:invoke))
    as element(ns2:FML32) {
        &lt;ns2:FML32>
            &lt;ns2:CI_ID_SPOLKI?>1&lt;/ns2:CI_ID_SPOLKI>
            &lt;ns2:DC_NUMER_KLIENTA?>{ data($invoke1/ns1:customer/ns0:Customer/ns0:customerNumber) }&lt;/ns2:DC_NUMER_KLIENTA>
            &lt;ns2:DC_TYP_RELACJI?>{ data($invoke1/ns1:relation/ns3:StringHolder/ns3:value) }&lt;/ns2:DC_TYP_RELACJI>
        &lt;/ns2:FML32>
};


declare variable $invoke1 as element(ns1:invoke) external;


&lt;soap-env:Body>{
xf:cisChkCustRelationExistRequest(
    $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>