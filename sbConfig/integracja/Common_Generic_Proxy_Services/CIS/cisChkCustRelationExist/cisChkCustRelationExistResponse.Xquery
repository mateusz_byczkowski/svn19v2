<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-08-04</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$fML321" element="ns1:FML32" location="../../Operations/savetransfer/CISIN.xsd" ::)
(:: pragma bea:global-element-return element="ns0:invokeResponse" location="cisChkCustRelationExist.wsdl" ::)

declare namespace ns2 = "urn:baseauxentities.be.dcl";
declare namespace ns1 = "";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Customers/cisChkCustRelationExist/cisChkCustRelationExistResponse/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:cisChkCustRelationExistResponse($fML321 as element(ns1:FML32))
    as element(ns0:invokeResponse) {
        &lt;ns0:invokeResponse>
            &lt;ns0:response>
                &lt;ns2:BooleanHolder>
                   &lt;ns2:value?>
                   {
                     let $w := data($fML321/ns1:CI_WYNIK)
                     return 
                    if ($w) then
                       if ($w = '0') then xs:boolean("true") else xs:boolean("false")
                       else () 
                    }
                    &lt;/ns2:value>
                &lt;/ns2:BooleanHolder>
            &lt;/ns0:response>
            &lt;ns0:responseMessage>
                &lt;ResponseMessage>
                    &lt;errorCode?>{ data($fML321/ns1:CI_KOD_BLEDU) }&lt;/errorCode>
                    &lt;errorDescription?>{ data($fML321/ns1:DC_OPIS_BLEDU) }&lt;/errorDescription>
                &lt;/ResponseMessage>
            &lt;/ns0:responseMessage>
        &lt;/ns0:invokeResponse>
};

declare variable $fML321 as element(ns1:FML32) external;

&lt;soap-env:Body>{
xf:cisChkCustRelationExistResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>