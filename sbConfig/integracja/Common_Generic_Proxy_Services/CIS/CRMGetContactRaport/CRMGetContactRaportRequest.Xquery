<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-30</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetContactRaportRequest($req as element(m:CRMGetContactRaportRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:DataOd)
					then &lt;fml:CI_DATA_OD>{ data($req/m:DataOd) }&lt;/fml:CI_DATA_OD>
					else ()
			}
			{
				if($req/m:DataDo)
					then &lt;fml:CI_DATA_DO>{ data($req/m:DataDo) }&lt;/fml:CI_DATA_DO>
					else ()
			}
			{
                                for $i in 1 to count($req/m:SKPPracownika)
                                return
                                        &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SKPPracownika[$i]) }&lt;/fml:CI_SKP_PRACOWNIKA>

			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetContactRaportRequest($body/m:CRMGetContactRaportRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>