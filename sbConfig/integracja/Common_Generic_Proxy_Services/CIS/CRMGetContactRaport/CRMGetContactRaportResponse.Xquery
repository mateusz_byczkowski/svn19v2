<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-30</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetContactRaportResponse($fml as element(fml:FML32))
	as element(m:CRMGetContactRaportResponse) {
		&lt;m:CRMGetContactRaportResponse>
			{

				let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
				let $CI_TYP_KONT := $fml/fml:CI_TYP_KONT
				let $CI_ILOSC1 := $fml/fml:CI_ILOSC1
				let $CI_ILOSC2 := $fml/fml:CI_ILOSC2

				for $it at $p in $fml/fml:CI_SKP_PRACOWNIKA
				return
					&lt;m:CRMGetContactRaportIloscKontaktow>
					{
						if($CI_SKP_PRACOWNIKA[$p])
							then &lt;m:SKPPracownika>{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:SKPPracownika>
						else ()
					}
					{
						if($CI_TYP_KONT[$p])
							then &lt;m:TypKontaktu>{ data($CI_TYP_KONT[$p]) }&lt;/m:TypKontaktu>
						else ()
					}
					{
						if($CI_ILOSC1[$p])
							then &lt;m:IloscKontaktow>{ data($CI_ILOSC1[$p]) }&lt;/m:IloscKontaktow>
						else ()
					}
					&lt;/m:CRMGetContactRaportIloscKontaktow>
			}

		&lt;/m:CRMGetContactRaportResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetContactRaportResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>