<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMCanResPortfRequest($req as element(m:CRMCanResPortfRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:IdOperacji)
					then &lt;fml:CI_ID_OPERACJI>{ data($req/m:IdOperacji) }&lt;/fml:CI_ID_OPERACJI>
					else ()
			}
			{
				for $v in $req/m:NumerKlienta
				return
					&lt;fml:DC_NUMER_KLIENTA>{ data($v) }&lt;/fml:DC_NUMER_KLIENTA>
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMCanResPortfRequest($body/m:CRMCanResPortfRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>