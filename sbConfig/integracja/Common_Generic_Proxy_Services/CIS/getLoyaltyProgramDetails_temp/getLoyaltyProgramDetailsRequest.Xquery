<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-07-08</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns2:header" location="getLoyaltyProgramDetails.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns2:invoke" location="getLoyaltyProgramDetails.wsdl" ::)
(:: pragma bea:global-element-return element="ns3:FML32" location="../../Operations/savetransfer/CISIN.xsd" ::)

declare namespace ns2 = "urn:be.services.dcl";
declare namespace ns1 = "urn:cif.entities.be.dcl";
declare namespace ns3 = "";
declare namespace ns0 = "urn:loyaltyprogram.entities.be.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Customers/getLoyaltyProgramDetails/getLoyaltyProgramDetailsRequest/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:getLoyaltyProgramDetailsRequest($header1 as element(ns2:header) ?,
    $invoke1 as element(ns2:invoke))
    as element(ns3:FML32) {
        &lt;FML32>
            {
                for $customerNumber in $invoke1/ns2:customer/ns1:Customer/ns1:customerNumber
                return
                    &lt;CI_NUMER_KLIENTA?>{ data($customerNumber) }&lt;/CI_NUMER_KLIENTA>
            }
            &lt;CI_LP_TYP?>{  data($invoke1/ns2:loyaltyProgram/ns0:LoyaltyProgram/ns0:loyaltyProgramType) }&lt;/CI_LP_TYP>
            &lt;CI_MSHEAD_MSGID?>{ data($header1/ns2:msgHeader/ns2:msgId) }&lt;/CI_MSHEAD_MSGID>
        &lt;/FML32>
};

declare variable $header1 as element(ns2:header) ? external;
declare variable $invoke1 as element(ns2:invoke) external;

&lt;soap-env:Body>{
	xf:getLoyaltyProgramDetailsRequest($header1,
    $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>