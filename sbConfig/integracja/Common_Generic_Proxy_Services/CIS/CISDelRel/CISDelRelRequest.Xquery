<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISDelRelRequest($req as element(m:CISDelRelRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:NumerKlientaRel)
					then &lt;fml:DC_NUMER_KLIENTA_REL>{ data($req/m:NumerKlientaRel) }&lt;/fml:DC_NUMER_KLIENTA_REL>
					else ()
			}
			{
				if($req/m:TypRelacji)
					then &lt;fml:DC_TYP_RELACJI>{ data($req/m:TypRelacji) }&lt;/fml:DC_TYP_RELACJI>
					else ()
			}
			{
				if($req/m:RelacjaOdwrotna)
					then &lt;fml:CI_RELACJA_ODWROTNA>{ data($req/m:RelacjaOdwrotna) }&lt;/fml:CI_RELACJA_ODWROTNA>
					else ()
			}
                        {
                                if($req/m:TypRelOdwrotnej)
                                        then &lt;fml:CI_TYP_REL_ODWROTNEJ>{ data($req/m:TypRelOdwrotnej) }&lt;/fml:CI_TYP_REL_ODWROTNEJ>
                                        else ()
                        }
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCISDelRelRequest($body/m:CISDelRelRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>