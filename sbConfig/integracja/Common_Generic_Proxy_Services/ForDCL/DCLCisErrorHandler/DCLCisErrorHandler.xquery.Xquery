<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://poz.bzwbk.pl/DCLFault/";
declare variable $body external;



(:     Major error codes (errCode1)
        0 = OK
        102 = Bad input
        103 = No data
        108 = Unauthorized
        1 = Tux service error -> errCode2 is set to $reason, errCode3 is set to $urcode
        2 = ALSB service error  
:)


declare function local:fault($faultString as xs:string, $detail as xs:anyType) as element(soap-env:Fault) {
		&lt;soap-env:Fault>
			&lt;faultcode>soapenv:Server.userException&lt;/faultcode> 
			&lt;faultstring>{ $faultString }&lt;/faultstring> 
			&lt;detail>{ $detail }&lt;/detail>
		&lt;/soap-env:Fault>
};
declare function local:errors($errorCode1 as xs:string,
                                       $errorCode2 as xs:string,
                                       $errorCode3 as xs:string) as element()* {
	&lt;errCode1>{ $errorCode1 }&lt;/errCode1>,
	&lt;errCode2>{ $errorCode2 }&lt;/errCode2>,
        &lt;errCode3>{ $errorCode3 }&lt;/errCode3>
};

&lt;soap-env:Body>
	&lt;soap-env:Body>
	{
		(: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
		let $reason := fn:substring-before(fn:substring-after(fn:substring-before($body/ctx:fault/ctx:reason, ":"), "("), ")")
		let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($body/ctx:fault/ctx:reason, ":"), ":"), ":")
		return                   
			if($reason = "13") then
				local:fault("com.bzwbk.services.dcl.faults.DCLException", 
                                               element f:DCLFault { local:errors('1',$reason, $urcode) })
			else if($reason = "11") then
				if($urcode = "102") then
					local:fault("com.bzwbk.services.dcl.faults.DCLException",
                                                       element f:DCLFault { local:errors('102','0','0') })
				else if($urcode = "103") then
					local:fault("com.bzwbk.services.dcl.faults.DCLException",
                                                       element f:DCLFault { local:errors('103','0','0') })
				else if($urcode = "108") then
					local:fault("com.bzwbk.services.dcl.faults.DCLException",
                                                       element f:DCLFault { local:errors('108','0','0') })
				else
					local:fault("com.bzwbk.services.dcl.faults.DCLException", 
                                        element f:DCLFault { local:errors('1',$reason, $urcode) })
                        (: other tuxedo errors :)
			else if (string-length($reason)>0) then
				local:fault("com.bzwbk.services.dcl.faults.DCLException", 
                                               element f:DCLFault { local:errors('1',$reason, $urcode) })
                        (: xquery error :)
			else 
			        local:fault("com.bzwbk.services.dcl.faults.DCLException", 
                                               element f:DCLFault { local:errors('2','0','0') })
	}
	&lt;/soap-env:Body>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>