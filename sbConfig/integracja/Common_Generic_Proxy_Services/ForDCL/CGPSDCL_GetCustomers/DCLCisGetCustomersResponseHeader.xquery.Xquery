<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace dcl = "http://poz.bzwbk.pl/DCLService/";
declare namespace xf = "http://bzwbk.com/services/dcl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISGetCustomersResponseHeader($fml as element(fml:FML32),
                                                                     $ctrldata as element(controlData))
	as element(dcl:controlData) {
		&lt;dcl:controlData>
                    &lt;controlData>
                  {
    		    if ($ctrldata/userID)
                        then $ctrldata/userID
                        else()
                  }

                  {
    		    if ($ctrldata/returnCode)
                        then $ctrldata/returnCode
                        else()
                  }

                  {
    		    if ($ctrldata/returnDescription)
                        then $ctrldata/returnDescription
                        else()
                  }
                  
                  &lt;charset>utf-8&lt;/charset>

                  {
    		    if ($ctrldata/goToPage)
                        then &lt;goToPage>{data($ctrldata/goToPage)}&lt;/goToPage>
                        else ()
                  }
                  
                  {
    		    if ($ctrldata/goToRow)
                        then $ctrldata/goToRow
                        else()
                  }
                 

                  {
    		    if ($ctrldata/pageSize)
                        then $ctrldata/pageSize
                        else()
                  }

                  {
    		    if ($ctrldata/replySize)
                        then $ctrldata/replySize
                        else()
                  }

                  {
    		    if ($ctrldata/totalSize)
                        then $ctrldata/totalSize
                        else()
                  }

                  {
    		    if ($ctrldata/actionCode)
                        then $ctrldata/actionCode
                        else()
                  }

                  {
    		    if ($ctrldata/reverseOrder)
                        then $ctrldata/reverseOrder
                        else()
                  }

                  {
    		    if ($ctrldata/navigationKeyDefinition)
                        then $ctrldata/navigationKeyDefinition
                        else()
                  }

                  {
    		    if ($ctrldata/navigationKeyValue)
                        then $ctrldata/navigationKeyValue
                        else()
                  }
                   &lt;/controlData>
		&lt;/dcl:controlData>
};

declare variable $fmlResponseBody as element(soap-env:Body) external;
declare variable $requestHeader as element(soap-env:Header) external;

&lt;soap-env:Header>
{ xf:mapCISGetCustomersResponseHeader($fmlResponseBody/fml:FML32,
                                                           $requestHeader/dcl:controlData/controlData)}
&lt;/soap-env:Header></con:xquery>
</con:xqueryEntry>