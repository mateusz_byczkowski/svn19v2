<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCustomerResponse($fml as element(fml:FML32))
	as element(m:GetCustomerResponse) {
		&lt;m:GetCustomerResponse>
			{
				if($fml/fml:B_NAZWA_KLIENTA)
					then &lt;m:NazwaKlienta>{ data($fml/fml:B_NAZWA_KLIENTA) }&lt;/m:NazwaKlienta>
					else ()
			}
			{
				if($fml/fml:B_UL_ZAM)
					then &lt;m:UlZam>{ data($fml/fml:B_UL_ZAM) }&lt;/m:UlZam>
					else ()
			}
			{
				if($fml/fml:B_M_ZAM)
					then &lt;m:MZam>{ data($fml/fml:B_M_ZAM) }&lt;/m:MZam>
					else ()
			}
			{
				if($fml/fml:B_KOD_POCZT)
					then &lt;m:KodPoczt>{ data($fml/fml:B_KOD_POCZT) }&lt;/m:KodPoczt>
					else ()
			}
			{
				if($fml/fml:B_TELEFON)
					then &lt;m:Telefon>{ data($fml/fml:B_TELEFON) }&lt;/m:Telefon>
					else ()
			}
			{
				if($fml/fml:B_ID_ODDZ)
					then &lt;m:IdOddz>{ data($fml/fml:B_ID_ODDZ) }&lt;/m:IdOddz>
					else ()
			}
			{
				if($fml/fml:B_MIKROODDZIAL)
					then &lt;m:Mikrooddzial>{ data($fml/fml:B_MIKROODDZIAL) }&lt;/m:Mikrooddzial>
					else ()
			}
			{
				if($fml/fml:B_KOD_KLIENTA)
					then &lt;m:KodKlienta>{ data($fml/fml:B_KOD_KLIENTA) }&lt;/m:KodKlienta>
					else ()
			}
			{
				if($fml/fml:E_CIF_NUMBER)
					then &lt;m:CifNumber>{ data($fml/fml:E_CIF_NUMBER) }&lt;/m:CifNumber>
					else ()
			}
			{
				if($fml/fml:E_CUSTOMER_TYPE)
					then &lt;m:CustomerType>{ data($fml/fml:E_CUSTOMER_TYPE) }&lt;/m:CustomerType>
					else ()
			}
			{
				if($fml/fml:B_M_URODZENIA)
					then &lt;m:MUrodzenia>{ data($fml/fml:B_M_URODZENIA) }&lt;/m:MUrodzenia>
					else ()
			}
			{
				if($fml/fml:B_D_URODZENIA)
					then &lt;m:DUrodzenia>{ data($fml/fml:B_D_URODZENIA) }&lt;/m:DUrodzenia>
					else ()
			}
			{
				if($fml/fml:B_SEKCJA)
					then &lt;m:Sekcja>{ data($fml/fml:B_SEKCJA) }&lt;/m:Sekcja>
					else ()
			}
			{
				if($fml/fml:B_PODMIOT)
					then &lt;m:Podmiot>{ data($fml/fml:B_PODMIOT) }&lt;/m:Podmiot>
					else ()
			}
			{
				if($fml/fml:B_WLASNOSC)
					then &lt;m:Wlasnosc>{ data($fml/fml:B_WLASNOSC) }&lt;/m:Wlasnosc>
					else ()
			}
			{
				if($fml/fml:B_KREWNY_BANKU)
					then &lt;m:KrewnyBanku>{ data($fml/fml:B_KREWNY_BANKU) }&lt;/m:KrewnyBanku>
					else ()
			}
			{
				if($fml/fml:B_REZYDENT)
					then &lt;m:Rezydent>{ data($fml/fml:B_REZYDENT) }&lt;/m:Rezydent>
					else ()
			}
			{
				if($fml/fml:B_SYT_FINANSOWA)
					then &lt;m:SytFinansowa>{ data($fml/fml:B_SYT_FINANSOWA) }&lt;/m:SytFinansowa>
					else ()
			}
			{
				if($fml/fml:B_FIRMA)
					then &lt;m:Firma>{ data($fml/fml:B_FIRMA) }&lt;/m:Firma>
					else ()
			}
			{
				if($fml/fml:B_FIRMA_CD)
					then &lt;m:FirmaCd>{ data($fml/fml:B_FIRMA_CD) }&lt;/m:FirmaCd>
					else ()
			}
			{
				if($fml/fml:B_ID_KL)
					then &lt;m:IdKl>{ data($fml/fml:B_ID_KL) }&lt;/m:IdKl>
					else ()
			}
			{
				if($fml/fml:B_N_FOP)
					then &lt;m:NFop>{ data($fml/fml:B_N_FOP) }&lt;/m:NFop>
					else ()
			}
			{
				if($fml/fml:B_N_EKD)
					then &lt;m:NEkd>{ data($fml/fml:B_N_EKD) }&lt;/m:NEkd>
					else ()
			}
			{
				if($fml/fml:B_N_KGN)
					then &lt;m:NKgn>{ data($fml/fml:B_N_KGN) }&lt;/m:NKgn>
					else ()
			}
			{
				if($fml/fml:B_KRAJ)
					then &lt;m:Kraj>{ data($fml/fml:B_KRAJ) }&lt;/m:Kraj>
					else ()
			}
			{
				if($fml/fml:B_WOJ_GM)
					then &lt;m:WojGm>{ data($fml/fml:B_WOJ_GM) }&lt;/m:WojGm>
					else ()
			}
			{
				if($fml/fml:B_KOD_USUNIECIA)
					then &lt;m:KodUsuniecia>{ data($fml/fml:B_KOD_USUNIECIA) }&lt;/m:KodUsuniecia>
					else ()
			}
			{
				if($fml/fml:B_D_OSTATNIEJ_AKT)
					then &lt;m:DOstatniejAkt>{ data($fml/fml:B_D_OSTATNIEJ_AKT) }&lt;/m:DOstatniejAkt>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KORESP)
					then &lt;m:NazwaKoresp>{ data($fml/fml:B_NAZWA_KORESP) }&lt;/m:NazwaKoresp>
					else ()
			}
			{
				if($fml/fml:B_UL_KORESP)
					then &lt;m:UlKoresp>{ data($fml/fml:B_UL_KORESP) }&lt;/m:UlKoresp>
					else ()
			}
			{
				if($fml/fml:B_KOD_KORESP)
					then &lt;m:KodKoresp>{ data($fml/fml:B_KOD_KORESP) }&lt;/m:KodKoresp>
					else ()
			}
			{
				if($fml/fml:B_M_KORESP)
					then &lt;m:MKoresp>{ data($fml/fml:B_M_KORESP) }&lt;/m:MKoresp>
					else ()
			}
		&lt;/m:GetCustomerResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetCustomerResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>