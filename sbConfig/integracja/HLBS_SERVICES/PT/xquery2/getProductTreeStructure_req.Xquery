<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductTreeStructure_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:getProductTreeStructure_req ($entity as element(dcl:ProductDefinition)) as element(FML32) {
&lt;FML32> 
     &lt;PT_PACK_NO>{ data( $entity/dcl:idProductDefinition ) }&lt;/PT_PACK_NO>
&lt;/FML32>
};

declare variable $entity as element(dcl:ProductDefinition) external;
xf:getProductTreeStructure_req($entity)</con:xquery>
</con:xqueryEntry>