<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/deleteProdAreaAttrAvailVal_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:deleteProdAreaAttrAvailVal_req($entity as element(dcl:ProductAreaAttrListAvailVal))
    as element() {
	&lt;FML32>
		&lt;PT_ID_AREA_ATTR_LIST_VAL>{ data( $entity/dcl:idProductAreaAttrListAvailVal ) }&lt;/PT_ID_AREA_ATTR_LIST_VAL>
	&lt;/FML32>
};

declare variable $entity as element(dcl:ProductAreaAttrListAvailVal) external;

xf:deleteProdAreaAttrAvailVal_req($entity)</con:xquery>
</con:xqueryEntry>