<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/moveProductCategory_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:moveProductCategory_req($entity as element(), $src as element(), $dest as element())
    as element(FML32) {
	&lt;FML32>
		&lt;PT_ID_CATEGORY>{ data($entity/dcl:idProductCategory) }&lt;/PT_ID_CATEGORY>
		&lt;PT_ID_AREA>{ data($src/dcl:idProductArea) }&lt;/PT_ID_AREA>
		&lt;PT_ID_AREA>{ data($dest/dcl:idProductArea) }&lt;/PT_ID_AREA>
    &lt;/FML32>
};

declare variable $entity as element(dcl:ProductCategory) external;
declare variable $src as element(dcl:ProductArea) external;
declare variable $dest as element(dcl:ProductArea) external;

xf:moveProductCategory_req($entity, $src, $dest)</con:xquery>
</con:xqueryEntry>