<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductPackageList_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:getProductPackageHistory_req(
	$pack as element(dcl:ProductPackage) )
    as element() {
	&lt;FML32>
		&lt;PT_ID_PACKAGE>{ data( $pack/dcl:idProductPackage) }&lt;/PT_ID_PACKAGE>
	&lt;/FML32>
};

declare variable $pack as element(dcl:ProductPackage) external;
xf:getProductPackageHistory_req($pack)</con:xquery>
</con:xqueryEntry>