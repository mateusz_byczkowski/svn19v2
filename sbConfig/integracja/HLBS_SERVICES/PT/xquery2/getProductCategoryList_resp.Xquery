<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductareaList_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";

declare function xf:getProductCategoryList_resp($fml as element())
    as element(srv:invokeResponse) {
        &lt;srv:invokeResponse>
            &lt;srv:productCategoryList>
                {
                    for $i in 1 to count($fml/PT_ID_AREA)
                    return
                        &lt;ns0:ProductCategory>
                                    &lt;ns0:codeProductCategory>{ data($fml/PT_CODE_PRODUCT_CATEGORY[$i]) }&lt;/ns0:codeProductCategory>
                                    &lt;ns0:polishCategoryName>{ data($fml/PT_POLISH_NAME[$i]) }&lt;/ns0:polishCategoryName>
                                    &lt;ns0:englishCategoryName>{ data($fml/PT_ENGLISH_NAME[$i]) }&lt;/ns0:englishCategoryName>
                                    &lt;ns0:sortOrder>{ data($fml/PT_SORT_ORDER[$i]) }&lt;/ns0:sortOrder>
                                    &lt;ns0:idProductArea>{ data($fml/PT_ID_AREA[$i]) }&lt;/ns0:idProductArea>
                                    &lt;ns0:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }&lt;/ns0:userChangeSKP>

     {if (data( $fml/PT_DATE_CHANGE[$i]))
     then &lt;ns0:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }&lt;/ns0:dateChange>
     else () }

                                    &lt;ns0:idProductCategory>{ data($fml/PT_ID_CATEGORY[$i]) }&lt;/ns0:idProductCategory>
                        &lt;/ns0:ProductCategory>
                }
            &lt;/srv:productCategoryList>
        &lt;/srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductCategoryList_resp($fml)</con:xquery>
</con:xqueryEntry>