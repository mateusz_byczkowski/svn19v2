<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-06</con:description>
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAttributesList_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";


declare function xf:getProductAttributesList_resp($fml as element())
    as element() {
	&lt;srv:invokeResponse>
        &lt;srv:productAttributesList>
            {
                for $i in 1 to count($fml/PT_ID_ATTRIBUTE)
                return
                    &lt;ns0:ProductAttributes>
                                &lt;ns0:attributeValue>{ data($fml/PT_ATTRIBUTE_VALUE[$i]) }&lt;/ns0:attributeValue>                                    
                                &lt;ns0:attributeValue2>{ data($fml/PT_ATTRIBUTE_VALUE_2[$i]) }&lt;/ns0:attributeValue2>                                    
                                &lt;ns0:attributeID>{ data($fml/PT_ID_ATTRIBUTE[$i]) }&lt;/ns0:attributeID>                                    
                                &lt;ns0:idProductAttributes>{ data($fml/PT_ID_ATTRIBUTES[$i]) }&lt;/ns0:idProductAttributes>                                    
                            	&lt;ns0:idProductAreaAttributes>{ data($fml/PT_ID_PROD_AREA_ATT[$i]) }&lt;/ns0:idProductAreaAttributes>
                            	{if(xs:int(data($fml/PT_ID_GROUP[$i])) ne 0) then
                            	&lt;ns0:idProductGroup>{ data($fml/PT_ID_GROUP[$i]) }&lt;/ns0:idProductGroup>
                            	else ()}
                            	{if(xs:int(data($fml/PT_ID_DEFINITION[$i])) ne 0) then
                            	&lt;ns0:idProductDefinition>{ data($fml/PT_ID_DEFINITION[$i]) }&lt;/ns0:idProductDefinition>
                            	else ()}
                            	{if(xs:int(data($fml/PT_ID_PACKAGE[$i])) ne 0) then
                            	&lt;ns0:idProductPackage>{ data($fml/PT_ID_PACKAGE[$i]) }&lt;/ns0:idProductPackage>
                            	else ()}
                    &lt;/ns0:ProductAttributes>
            }
        &lt;/srv:productAttributesList>
    &lt;/srv:invokeResponse>
};

declare variable $fml as element() external;

&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
{
xf:getProductAttributesList_resp($fml)
}
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>