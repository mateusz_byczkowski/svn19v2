<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductPackageHistory_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns1 = "urn:productstree.entities.be.dcl";

declare function xf:getProductPackageHistory_resp($fml as element())
    as element(srv:invokeResponse) {
        &lt;srv:invokeResponse>
            &lt;srv:productPackageHistory>
            {
            for $i in 1 to count($fml/PT_ID_PACKAGE)
            return
                &lt;ns1:ProductPackageHistory>
                    &lt;ns1:englishPackageName>{ data($fml/PT_ENGLISH_NAME[$i]) }&lt;/ns1:englishPackageName>
                    &lt;ns1:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }&lt;/ns1:userChangeSKP>
                    { if (data($fml/PT_END_PACKAGE_DATE[$i])) 
                    then  &lt;ns1:endPackageDate>{ xs:date($fml/PT_END_PACKAGE_DATE[$i])}&lt;/ns1:endPackageDate>
                    else () }
                    &lt;ns1:polishPackageName>{ data($fml/PT_POLISH_NAME[$i]) }&lt;/ns1:polishPackageName>
                    { if (data($fml/PT_START_PACKAGE_DATE[$i])) 
                    then  &lt;ns1:startPackageDate>{ xs:date($fml/PT_START_PACKAGE_DATE[$i])}&lt;/ns1:startPackageDate>
                    else () }
                    &lt;ns1:idProductPackage>{ data($fml/PT_ID_PACKAGE[$i]) }&lt;/ns1:idProductPackage>
                    &lt;ns1:sortOrder>{ data($fml/PT_SORT_ORDER[$i]) }&lt;/ns1:sortOrder>
                    &lt;ns1:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }&lt;/ns1:dateChange>
                    &lt;ns1:codePackage>{ data($fml/PT_CODE_PACKAGE[$i]) }&lt;/ns1:codePackage>
                &lt;/ns1:ProductPackageHistory>
            }
            &lt;/srv:productPackageHistory>
        &lt;/srv:invokeResponse>
};

declare variable $fml as element(FML32) external;

xf:getProductPackageHistory_resp($fml)</con:xquery>
</con:xqueryEntry>