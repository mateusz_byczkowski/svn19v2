<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProductArea_req/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:saveProductArea_req ($entity as element(dcl:entities.productstree.ProductArea)) as element(FML32) {
&lt;FML32>
     &lt;PT_CODE_PRODUCT_AREA>{ data( $entity/dcl:codeProductArea ) }&lt;/PT_CODE_PRODUCT_AREA>
     &lt;PT_POLISH_NAME>{ data( $entity/dcl:polishAreaName ) }&lt;/PT_POLISH_NAME>
     &lt;PT_ENGLISH_NAME>{ data( $entity/dcl:englishAreaName ) }&lt;/PT_ENGLISH_NAME>
     &lt;PT_SORT_ORDER>{ data( $entity/dcl:sortOrder ) }&lt;/PT_SORT_ORDER>
     &lt;PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP ) }&lt;/PT_USER_CHANGE_SKP>
     &lt;PT_DATE_CHANGE>{ data( $entity/dcl:dateChange ) }&lt;/PT_DATE_CHANGE>
     &lt;PT_CERBER_ATTRIBUTE_NAME>{ data( $entity/dcl:cerberAtributeName ) }&lt;/PT_CERBER_ATTRIBUTE_NAME>
     &lt;PT_ID_AREA>{ data( $entity/dcl:idProductArea ) }&lt;/PT_ID_AREA>
&lt;/FML32>
};
declare variable $entity as element(dcl:entities.productstree.ProductArea) external;
xf:saveProductArea_req($entity)</con:xquery>
</con:xqueryEntry>