<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductDefinitionHistory_req/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductDefinitionHistory_req ($entity as element(dcl:entities.productstree.ProductDefinition)) as element(FML32) {
&lt;FML32>
     &lt;PT_ID_DEFINITION>{ data( $entity/dcl:idProductDefinition ) }&lt;/PT_ID_DEFINITION>
&lt;/FML32>
};

declare variable $entity as element(dcl:entities.productstree.ProductDefinition) external;
xf:getProductDefinitionHistory_req($entity)</con:xquery>
</con:xqueryEntry>