<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductArea_req/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductArea_req($entity as element(dcl:entities.productstree.ProductArea))
    as element() {
	&lt;FML32>
		&lt;PT_ID_AREA>{ data( $entity/dcl:idProductArea ) }&lt;/PT_ID_AREA>
	&lt;/FML32>
};

declare variable $entity as element(dcl:entities.productstree.ProductArea) external;

xf:getProductArea_req($entity)</con:xquery>
</con:xqueryEntry>