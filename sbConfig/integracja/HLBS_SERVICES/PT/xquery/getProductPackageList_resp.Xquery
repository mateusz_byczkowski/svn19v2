<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductPackageList/";
declare namespace ns1 = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductPackageList($fml as element())
    as element(ns1:invokeResponse) {
        &lt;ns1:invokeResponse>
            &lt;ns1:productPackageList>
            {
            for $i in 1 to count($fml/PT_ID_PACKAGE)
            return
                &lt;ns1:entities.productstree.ProductPackage>
                    &lt;ns1:codePackage>{ data($fml/PT_CODE_PACKAGE[$i]) }&lt;/ns1:codePackage>
                    &lt;ns1:polishPackageName>{ data($fml/PT_POLISH_NAME[$i]) }&lt;/ns1:polishPackageName>
                    &lt;ns1:englishPackageName>{ data($fml/PT_ENGLISH_NAME[$i]) }&lt;/ns1:englishPackageName>
                    &lt;ns1:sortOrder>{ data($fml/PT_SORT_ORDER[$i]) }&lt;/ns1:sortOrder>
                    &lt;ns1:startPackageDate>{ data($fml/PT_START_PACKAGE_DATE[$i]) }T00:00:00&lt;/ns1:startPackageDate>
                    {
                    if (data($fml/PT_END_PACKAGE_DATE[$i])) 
                    then  &lt;ns1:endPackageDate>{ data($fml/PT_END_PACKAGE_DATE[$i]) }T00:00:00&lt;/ns1:endPackageDate>
                    else   &lt;ns1:endPackageDate>{ data($fml/PT_END_PACKAGE_DATE[$i]) }&lt;/ns1:endPackageDate>
                    }
                    &lt;ns1:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }&lt;/ns1:userChangeSKP>
                    &lt;ns1:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }&lt;/ns1:dateChange>
                    &lt;ns1:idProductPackage>{ data($fml/PT_ID_PACKAGE[$i]) }&lt;/ns1:idProductPackage>
                &lt;/ns1:entities.productstree.ProductPackage>
            }
            &lt;/ns1:productPackageList>
        &lt;/ns1:invokeResponse>
};

declare variable $fml as element(FML32) external;

xf:getProductPackageList($fml)</con:xquery>
</con:xqueryEntry>