<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAreaAttrList_resp/";
declare namespace ns0 = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductAreaAttrList_resp($fml as element())
    as element() {
	&lt;ns0:invokeResponse>
        &lt;ns0:productAreaAttrList>
            {
                for $i in 1 to count($fml/PT_ID_AREA_ATTRIBUTES)
                return
                    &lt;ns0:entities.productstree.ProductAreaAttributes>
                                &lt;ns0:attributeID>&lt;/ns0:attributeID>                                    
                                &lt;ns0:attributeName>{ data($fml/PT_ATTRIBUTE_NAME[$i]) }&lt;/ns0:attributeName>                                    
                                &lt;ns0:algorithmSequence>{ data($fml/PT_ALGORITHM_SEQ[$i]) }&lt;/ns0:algorithmSequence>                                    
                                &lt;ns0:idProductAreaAttributes>{ data($fml/PT_ID_AREA_ATTRIBUTES[$i]) }&lt;/ns0:idProductAreaAttributes>                                    
                            	&lt;ns0:idProductArea>{ data($fml/PT_ID_AREA[$i]) }&lt;/ns0:idProductArea>
                            	&lt;ns0:attributeType>
                            		&lt;ns0:dictionaries.ProductAreaAttributeType>
                            			&lt;ns0:attributeType>{ data($fml/PT_ATTRIBUTE_TYPE[$i]) }&lt;/ns0:attributeType>
                            		&lt;/ns0:dictionaries.ProductAreaAttributeType>
                            	&lt;/ns0:attributeType>
                    &lt;/ns0:entities.productstree.ProductAreaAttributes>
            }
        &lt;/ns0:productAreaAttrList>
    &lt;/ns0:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductAreaAttrList_resp($fml)</con:xquery>
</con:xqueryEntry>