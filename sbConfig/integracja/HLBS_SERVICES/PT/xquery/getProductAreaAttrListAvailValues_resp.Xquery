<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAreaAttrListAvailValues_resp/";
declare namespace ns0 = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductAreaAttrListAvailValues_resp($fml as element())
    as element() {
	&lt;ns0:invokeResponse>
        &lt;ns0:prodAreaAttrListAvailValues>
            {
                for $i in 1 to count($fml/PT_ID_AREA_ATTR_LIST_VAL)
                return
                    &lt;ns0:entities.productstree.ProductAreaAttrListAvailVal>
                                &lt;ns0:value>{ data($fml/PT_VALUE[$i]) }&lt;/ns0:value>                                    
                                &lt;ns0:valueDescription>{ data($fml/PT_DESCRIPTION[$i]) }&lt;/ns0:valueDescription>                                    
                                &lt;ns0:connectionField>{ data($fml/PT_CONNECTION_FIELD[$i]) }&lt;/ns0:connectionField>                                    
                                &lt;ns0:idProductAreaAttrListAvailVal>{ data($fml/PT_ID_AREA_ATTR_LIST_VAL[$i]) }&lt;/ns0:idProductAreaAttrListAvailVal>                                    
                            	&lt;ns0:idProductAreaAttrtibutes>{ data($fml/PT_ID_AREA_ATTRIBUTES[$i]) }&lt;/ns0:idProductAreaAttrtibutes>
                    &lt;/ns0:entities.productstree.ProductAreaAttrListAvailVal>
            }
        &lt;/ns0:prodAreaAttrListAvailValues>
    &lt;/ns0:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductAreaAttrListAvailValues_resp($fml)</con:xquery>
</con:xqueryEntry>