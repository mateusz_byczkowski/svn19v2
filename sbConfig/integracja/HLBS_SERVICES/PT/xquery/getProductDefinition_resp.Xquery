<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductDefinition_resp/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductDefinition_resp ($fml as element(FML32)) as element(dcl:invokeResponse) {
&lt;dcl:invokeResponse>
&lt;dcl:productDefinitionEntity>

&lt;dcl:entities.productstree.ProductDefinition>
	&lt;dcl:codeProductDefinition>{ data( $fml/PT_CODE_PRODUCT_DEFINITION ) }&lt;/dcl:codeProductDefinition>
	&lt;dcl:polishProductName>{ data( $fml/PT_POLISH_NAME ) }&lt;/dcl:polishProductName>
	 &lt;dcl:englishProductName>{ data( $fml/PT_ENGLISH_NAME ) }&lt;/dcl:englishProductName>
     &lt;dcl:sortOrder>{ data( $fml/PT_SORT_ORDER ) }&lt;/dcl:sortOrder>
     &lt;dcl:userChangeSKP>{ data( $fml/PT_USER_CHANGE_SKP ) }&lt;/dcl:userChangeSKP>
     &lt;dcl:dateChange>{ data( $fml/PT_DATE_CHANGE ) }&lt;/dcl:dateChange>
     &lt;dcl:idProductGroup>{ data( $fml/PT_ID_GROUP ) }&lt;/dcl:idProductGroup>
     &lt;dcl:packageOnlyFlag>{ xs:boolean( data( $fml/PT_PACKAGE_ONLY_FLAG )) }&lt;/dcl:packageOnlyFlag>
     &lt;dcl:sorceProductCode>{ data( $fml/PT_SOURCE_PRODUCT_CODE ) }&lt;/dcl:sorceProductCode>
     &lt;dcl:startDate>{ data( $fml/PT_START_DATE ) }T00:00:00&lt;/dcl:startDate>
     {if (data( $fml/PT_END_DATE ))
     then &lt;dcl:endDate>{ data( $fml/PT_END_DATE ) }T00:00:00&lt;/dcl:endDate>
     else &lt;dcl:endDate>{ data( $fml/PT_END_DATE ) }&lt;/dcl:endDate>}
     &lt;dcl:idProductDefinition>{ data( $fml/PT_ID_DEFINITION ) }&lt;/dcl:idProductDefinition>
&lt;/dcl:entities.productstree.ProductDefinition>

&lt;/dcl:productDefinitionEntity>
&lt;/dcl:invokeResponse>
};

declare variable $fml as element(FML32) external;
xf:getProductDefinition_resp($fml)</con:xquery>
</con:xqueryEntry>