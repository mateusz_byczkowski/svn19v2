<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/modifyPackageContents_req/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:modifyPackageContents_req (
	$operationType as element(dcl:baseauxentities.EditHelper),
	$obligatory as element(dcl:baseauxentities.BooleanHolder),
	$idProductEntity as element(dcl:baseauxentities.IntegerHolder),
	$productPackageEntity as element(dcl:entities.productstree.ProductPackage)
	) as element(FML32) {
&lt;FML32>{
if (data($operationType/dcl:stringBased) eq "ADD_PRODUCT") 
then (
	&lt;PT_OPTION>0&lt;/PT_OPTION>,
	&lt;PT_ID_DEFINITION>{data($idProductEntity/dcl:value)}&lt;/PT_ID_DEFINITION>
)
else if (data($operationType/dcl:stringBased) eq "ADD_GROUP") 
then (
	&lt;PT_OPTION>1&lt;/PT_OPTION>,
	&lt;PT_ID_GROUP>{data($idProductEntity/dcl:value)}&lt;/PT_ID_GROUP>
)
else if (data($operationType/dcl:stringBased) eq "DEL_PRODUCT") 
then (
	&lt;PT_OPTION>2&lt;/PT_OPTION>,
	&lt;PT_ID_DEFINITION>{data($idProductEntity/dcl:value)}&lt;/PT_ID_DEFINITION>
)
else if (data($operationType/dcl:stringBased) eq "DEL_GROUP") 
then (
	&lt;PT_OPTION>3&lt;/PT_OPTION>,
	&lt;PT_ID_GROUP>{data($idProductEntity/dcl:value)}&lt;/PT_ID_GROUP>
)
else if (data($operationType/dcl:stringBased) eq "SET_PRODUCT_OBLIGATORY") 
then (
	&lt;PT_OPTION>4&lt;/PT_OPTION>,
	&lt;PT_ID_DEFINITION>{data($idProductEntity/dcl:value)}&lt;/PT_ID_DEFINITION>
)
else if (data($operationType/dcl:stringBased) eq "SET_GROUP_OBLIGATORY") 
then (
	&lt;PT_OPTION>5&lt;/PT_OPTION>,
	&lt;PT_ID_GROUP>{data($idProductEntity/dcl:value)}&lt;/PT_ID_GROUP>
)
else ()
}
	&lt;PT_ID_PACKAGE>{data($productPackageEntity/dcl:idProductPackage)}&lt;/PT_ID_PACKAGE>
	&lt;PT_OBLIGATORY>{xs:short(xs:boolean(data($obligatory/dcl:value)))}&lt;/PT_OBLIGATORY>
	
&lt;/FML32>
};

declare variable $operationType as element(dcl:baseauxentities.EditHelper) external;
declare variable $obligatory as element(dcl:baseauxentities.BooleanHolder) external;
declare variable $idProductEntity as element(dcl:baseauxentities.IntegerHolder) external;
declare variable $productPackageEntity as element(dcl:entities.productstree.ProductPackage) external;

xf:modifyPackageContents_req($operationType, $obligatory, $idProductEntity, $productPackageEntity)</con:xquery>
</con:xqueryEntry>