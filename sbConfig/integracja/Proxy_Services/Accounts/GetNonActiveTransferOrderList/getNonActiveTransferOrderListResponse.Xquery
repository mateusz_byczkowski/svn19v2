<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:transferorder.entities.be.dcl";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32),$occ as xs:integer) as element()
{
&lt;ns0:transferOrderTargetList>
      &lt;ns0:TransferOrderTarget>
      &lt;ns0:amountTarget?>{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}&lt;/ns0:amountTarget>
      &lt;ns0:codeProfile?>{data($parm/NF_TRANOT_CODEPROFILE[$occ])}&lt;/ns0:codeProfile>
      &lt;ns0:transferOrderDescription?>{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}&lt;/ns0:transferOrderDescription>
      &lt;ns0:beneficiary1?>{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}&lt;/ns0:beneficiary1>
      &lt;ns0:beneficiary2?>{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}&lt;/ns0:beneficiary2>
      &lt;ns0:currencyCode>
        &lt;ns5:CurrencyCode>
          &lt;ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode>
        &lt;/ns5:CurrencyCode>
      &lt;/ns0:currencyCode>
    &lt;/ns0:TransferOrderTarget>
&lt;/ns0:transferOrderTargetList>
};
declare function getElementsForTransferOrder($parm as element(fml:FML32)) as element()
{

&lt;ns1:transferOrder>
  {
    for $x at $occ in $parm/NF_TRANOA_TRANSFERORDERNUM
    return
    &lt;ns0:TransferOrderAnchor>
      &lt;ns0:frequency?>{data($parm/NF_TRANOA_FREQUENCY[$occ])}&lt;/ns0:frequency>
      &lt;ns0:codeProfile?>{data($parm/NF_TRANOA_CODEPROFILE[$occ])}&lt;/ns0:codeProfile>
      &lt;ns0:transferOrderNumber?>{data($parm/NF_TRANOA_TRANSFERORDERNUM[$occ])}&lt;/ns0:transferOrderNumber>
      &lt;ns0:charging?>{data($parm/NF_TRANOA_CHARGING[$occ])}&lt;/ns0:charging>
      { insertDate(data($parm/NF_TRANOA_REVERSEDATE[$occ]),"yyyy-MM-dd","ns0:reverseDate")}
      {getElementsForTransferOrderTargetList($parm,$occ)}
      &lt;ns0:period>
        &lt;ns5:Period>
          &lt;ns5:period?>{data($parm/NF_PERIOD_PERIOD[$occ])}&lt;/ns5:period>
        &lt;/ns5:Period>
      &lt;/ns0:period>
    &lt;/ns0:TransferOrderAnchor>
  }
&lt;/ns1:transferOrder>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns1:invokeResponse>
  {getElementsForTransferOrder($parm)}
  &lt;ns1:bcd>
    &lt;ns7:BusinessControlData>
      &lt;ns7:pageControl>
        &lt;ns6:PageControl>
          &lt;ns6:hasNext?>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns6:hasNext>
          &lt;ns6:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition>
          &lt;ns6:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue>
        &lt;/ns6:PageControl>
      &lt;/ns7:pageControl>
    &lt;/ns7:BusinessControlData>
  &lt;/ns1:bcd>
&lt;/ns1:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>