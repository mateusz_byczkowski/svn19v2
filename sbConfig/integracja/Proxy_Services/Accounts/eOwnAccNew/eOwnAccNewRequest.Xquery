<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeOwnAccNewRequest($req as element(m:eOwnAccNewRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:Sys)
					then &lt;fml:B_SYS>{ data($req/m:Sys) }&lt;/fml:B_SYS>
					else ()
			}
			{
				if($req/m:Bank)
					then &lt;fml:B_BANK>{ data($req/m:Bank) }&lt;/fml:B_BANK>
					else ()
			}
			{
				if($req/m:AdminMicroBranch)
					then &lt;fml:E_ADMIN_MICRO_BRANCH>{ data($req/m:AdminMicroBranch) }&lt;/fml:E_ADMIN_MICRO_BRANCH>
					else ()
			}
			{
				if($req/m:UserName)
					then &lt;fml:U_USER_NAME>{ data($req/m:UserName) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/m:KodRach)
					then &lt;fml:B_KOD_RACH>{ data($req/m:KodRach) }&lt;/fml:B_KOD_RACH>
					else ()
			}
			{
				if($req/m:LoginId)
					then &lt;fml:E_LOGIN_ID>{ data($req/m:LoginId) }&lt;/fml:E_LOGIN_ID>
					else ()
			}
			{
				if($req/m:DlNrRach)
					then &lt;fml:B_DL_NR_RACH>{ data($req/m:DlNrRach) }&lt;/fml:B_DL_NR_RACH>
					else ()
			}
			{
				if($req/m:Waluta)
					then &lt;fml:B_WALUTA>{ data($req/m:Waluta) }&lt;/fml:B_WALUTA>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapeOwnAccNewRequest($body/m:eOwnAccNewRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>