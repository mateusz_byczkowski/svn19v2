<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl = "urn:be.services.dcl";
declare namespace ns1 = "urn:accounts.entities.be.dcl";
declare namespace ns2 = "urn:ceke.entities.be.dcl";
declare namespace ns3 = "urn:cif.entities.be.dcl";

declare function xf:map_delCustomerCEKEOwnAccountRequest($req as element(dcl:invoke),$head as element(dcl:header))
	as element(fml:FML32) {

		&lt;fml:FML32>
			{
				&lt;fml:E_MSHEAD_MSGID>{data($head/dcl:msgHeader/dcl:msgId)}&lt;/fml:E_MSHEAD_MSGID>
			}
			{
				&lt;fml:B_DL_NR_RACH?>{ data($req/dcl:account/ns1:Account/ns1:accountNumber) }&lt;/fml:B_DL_NR_RACH>
			}
			{
				for $custCIFList in $req/dcl:customerList/ns3:Customer return
				(
					&lt;fml:E_CIF_NUMBER?>{ data($custCIFList/ns3:customerNumber) }&lt;/fml:E_CIF_NUMBER>
				)
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body>
{ xf:map_delCustomerCEKEOwnAccountRequest($body/dcl:invoke,$header/dcl:header) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>