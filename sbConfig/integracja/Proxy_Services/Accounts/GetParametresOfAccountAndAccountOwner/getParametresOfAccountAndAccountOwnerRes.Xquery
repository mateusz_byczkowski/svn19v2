<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns6:invokeResponse>
  <ns6:response>
    <ns0:ResponseMessage>
      <ns0:result?>{data($parm/NF_RESPOM_RESULT)}</ns0:result>
    </ns0:ResponseMessage>
  </ns6:response>
  <ns6:accountOut>
    <ns1:Account>
      <ns1:accountName>{data($parm/NF_ACCOUN_ACCOUNTNAME)}</ns1:accountName>
      <ns1:accountIBAN>{data($parm/NF_ACCOUN_ACCOUNTIBAN)}</ns1:accountIBAN>
      <ns1:accountType>
        <ns4:AccountType>
          <ns4:accountType>{data($parm/NF_ACCOUT_ACCOUNTTYPE)}</ns4:accountType>
        </ns4:AccountType>
      </ns1:accountType>
      <ns1:currency>
        <ns4:CurrencyCode>
          <ns4:currencyCode>{data($parm/NF_CURREC_CURRENCYCODE)}</ns4:currencyCode>
        </ns4:CurrencyCode>
      </ns1:currency>
    </ns1:Account>
  </ns6:accountOut>
  <ns6:customer>
    <ns2:Customer>
      <ns2:companyName>{data($parm/NF_CUSTOM_COMPANYNAME)}</ns2:companyName>
      <ns2:customerNumber>{data($parm/NF_CUSTOM_CUSTOMERNUMBER)}</ns2:customerNumber>
      <ns2:customerPersonal>
        <ns2:CustomerPersonal>
          <ns2:lastName>{data($parm/NF_CUSTOP_LASTNAME)}</ns2:lastName>
          <ns2:firstName>{data($parm/NF_CUSTOP_FIRSTNAME)}</ns2:firstName>
        </ns2:CustomerPersonal>
      </ns2:customerPersonal>
      <ns2:customerType>
        <ns4:CustomerType>
          <ns4:customerType>{data($parm/NF_CUSTOT_CUSTOMERTYPE)}</ns4:customerType>
        </ns4:CustomerType>
      </ns2:customerType>
    </ns2:Customer>
  </ns6:customer>
</ns6:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>