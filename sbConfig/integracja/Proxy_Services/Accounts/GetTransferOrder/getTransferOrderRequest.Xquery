<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:transferorder.entities.be.dcl";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:accounts.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns1:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns1:msgHeader/ns1:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns1:msgHeader/ns1:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns1:msgHeader/ns1:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns1:msgHeader/ns1:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns1:msgHeader/ns1:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns1:msgHeader/ns1:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns1:transHeader/ns1:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns1:invoke)) as element()*
{

&lt;NF_TRANOA_TRANSFERORDERNUM?>{data($parm/ns1:orderIn/ns0:TransferOrderAnchor/ns0:transferOrderNumber)}&lt;/NF_TRANOA_TRANSFERORDERNUM>
,
&lt;NF_ACCOUN_ACCOUNTIBAN?>{data($parm/ns1:orderIn/ns0:TransferOrderAnchor/ns0:account/ns5:Account/ns5:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTIBAN>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns1:header)}
    {getFieldsFromInvoke($body/ns1:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>