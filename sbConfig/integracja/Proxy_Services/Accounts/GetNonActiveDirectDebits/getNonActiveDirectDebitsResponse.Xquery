<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:directdebit.entities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForDirectdebit($parm as element(fml:FML32)) as element()
{

&lt;ns0:directdebit>
  {
    for $x at $occ in $parm/NF_DIRECD_DIRECTDEBITID
    return
    &lt;ns1:DirectDebit>
      &lt;ns1:directDebitId>{data($parm/NF_DIRECD_DIRECTDEBITID[$occ])}&lt;/ns1:directDebitId>
      &lt;ns1:originatorReference>{data($parm/NF_DIRECD_ORIGINATORREFERE[$occ])}&lt;/ns1:originatorReference>
      &lt;ns1:debtorTaxID>{data($parm/NF_DIRECD_DEBTORTAXID[$occ])}&lt;/ns1:debtorTaxID>
      &lt;ns1:debtorName>{data($parm/NF_DIRECD_DEBTORNAME[$occ])}&lt;/ns1:debtorName>
      { insertDate(data($parm/NF_DIRECD_AUTHORISATIONDAT[$occ]),"yyyy-MM-dd","ns1:authorisationDate")}
      { insertDate(data($parm/NF_DIRECD_CANCELLATIONDATE[$occ]),"yyyy-MM-dd","ns1:cancellationDate")}
      &lt;ns1:authorisationSetUpUserID>{data($parm/NF_DIRECD_AUTHORISATIONSET[$occ])}&lt;/ns1:authorisationSetUpUserID>
      &lt;ns1:authorisationCancelUserID>{data($parm/NF_DIRECD_AUTHORISATIONCAN[$occ])}&lt;/ns1:authorisationCancelUserID>
      &lt;ns1:directDebitStatus>
        &lt;ns5:DirectDebitStatus>
          &lt;ns5:directDebitStatus>{data($parm/NF_DIREDS_DIRECTDEBITSTATU[$occ])}&lt;/ns5:directDebitStatus>
        &lt;/ns5:DirectDebitStatus>
      &lt;/ns1:directDebitStatus>
    &lt;/ns1:DirectDebit>
  }
&lt;/ns0:directdebit>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  {getElementsForDirectdebit($parm)}
  &lt;ns0:bcd>
    &lt;ns7:BusinessControlData>
      &lt;ns7:pageControl>
        &lt;ns6:PageControl>
          &lt;ns6:hasNext>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT), "1")}&lt;/ns6:hasNext>
          &lt;ns6:navigationKeyDefinition>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition>
          &lt;ns6:navigationKeyValue>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue>
        &lt;/ns6:PageControl>
      &lt;/ns7:pageControl>
    &lt;/ns7:BusinessControlData>
  &lt;/ns0:bcd>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>