<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:directdebit.entities.be.dcl";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:cif.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function sourceValue2Boolean ($parm as xs:string?,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{

      if ($value) then 
        if(string-length($value)>5 and not (string(fn-bea:date-from-string-with-format($dateFormat,$value)) = "0001-01-01"))
             then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
};

declare function getElementsForAccountRelationshipList($parm as element(fml:FML32)) as element()
{

&lt;ns0:accountRelationshipList>
  {
    for $x at $occ in 1
    return
    &lt;ns7:AccountRelationship>
      &lt;ns7:customer>
        &lt;ns7:Customer>
          &lt;ns7:companyName?>{data($parm/NF_CUSTOM_COMPANYNAME[$occ])}&lt;/ns7:companyName>
          &lt;ns7:customerPersonal>
            &lt;ns7:CustomerPersonal>
              &lt;ns7:lastName?>{data($parm/NF_CUSTOP_LASTNAME[$occ])}&lt;/ns7:lastName>
              &lt;ns7:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME[$occ])}&lt;/ns7:firstName>
            &lt;/ns7:CustomerPersonal>
          &lt;/ns7:customerPersonal>
          &lt;ns7:customerType>
            &lt;ns4:CustomerType>
              &lt;ns4:customerType?>{data($parm/NF_CUSTOT_CUSTOMERTYPE[$occ])}&lt;/ns4:customerType>
            &lt;/ns4:CustomerType>
          &lt;/ns7:customerType>
        &lt;/ns7:Customer>
      &lt;/ns7:customer>
    &lt;/ns7:AccountRelationship>
  }
&lt;/ns0:accountRelationshipList>
};

declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32)) as element()
{

&lt;ns2:transferOrderTargetList>
  {
    for $x at $occ in $parm/NF_TRANOT_AMOUNTTARGET
    return
    &lt;ns2:TransferOrderTarget>
      &lt;ns2:amountTarget?>{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}&lt;/ns2:amountTarget>
      &lt;ns2:codeProfile?>{data($parm/NF_TRANOT_CODEPROFILE[$occ])}&lt;/ns2:codeProfile>
      &lt;ns2:transferOrderDescription?>{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}&lt;/ns2:transferOrderDescription>
      &lt;ns2:beneficiary1?>{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}&lt;/ns2:beneficiary1>
      &lt;ns2:beneficiary2?>{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}&lt;/ns2:beneficiary2>
      &lt;ns2:beneficiary3?>{data($parm/NF_TRANOT_BENEFICIARY3[$occ])}&lt;/ns2:beneficiary3>
      &lt;ns2:beneficiary4?>{data($parm/NF_TRANOT_BENEFICIARY4[$occ])}&lt;/ns2:beneficiary4>
      &lt;ns2:account>
        &lt;ns0:Account>
          &lt;ns0:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns0:accountNumber>
        &lt;/ns0:Account>
      &lt;/ns2:account>
      &lt;ns2:accountOutside>
        &lt;ns2:AccountOutside>
          &lt;ns2:accountNumber?>{data($parm/NF_ACCOUO_ACCOUNTNUMBER[$occ])}&lt;/ns2:accountNumber>
        &lt;/ns2:AccountOutside>
      &lt;/ns2:accountOutside>
      &lt;ns2:glaccount>
        &lt;ns0:GLAccount>
          &lt;ns0:costCenterGL?>{data($parm/NF_GLA_COSTCENTERGL[$occ])}&lt;/ns0:costCenterGL>
          &lt;ns0:accountGL?>{data($parm/NF_GLA_ACCOUNTGL[$occ])}&lt;/ns0:accountGL>
          &lt;ns0:currencyCode>
            &lt;ns4:CurrencyCode>
             {
              if (string-length(data($parm/NF_GLA_ACCOUNTGL[$occ])) > 0) then
                   &lt;ns4:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns4:currencyCode>
              else
                    &lt;ns4:currencyCode?>&lt;/ns4:currencyCode>
             }
            &lt;/ns4:CurrencyCode>
          &lt;/ns0:currencyCode>
        &lt;/ns0:GLAccount>
      &lt;/ns2:glaccount>
      &lt;ns2:currencyCode>
        &lt;ns4:CurrencyCode>
          &lt;ns4:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns4:currencyCode>
        &lt;/ns4:CurrencyCode>
      &lt;/ns2:currencyCode>
    &lt;/ns2:TransferOrderTarget>
  }
&lt;/ns2:transferOrderTargetList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns6:invokeResponse>
  &lt;ns6:directDebitOut>
    &lt;ns1:DirectDebit>
      &lt;ns1:originatorReference?>{data($parm/NF_DIRECD_ORIGINATORREFERE)}&lt;/ns1:originatorReference>
      &lt;ns1:additionalDescription?>{data($parm/NF_DIRECD_ADDITIONALDESCRI)}&lt;/ns1:additionalDescription>
    &lt;/ns1:DirectDebit>
  &lt;/ns6:directDebitOut>
  &lt;ns6:orderOut>
    &lt;ns2:TransferOrderAnchor>
      &lt;ns2:frequency?>{data($parm/NF_TRANOA_FREQUENCY)}&lt;/ns2:frequency>
      {insertDate ($parm/NF_TRANOA_PAYMENTDATE,"yyyy-MM-dd","ns2:paymentDate")}
      {insertDate ($parm/NF_TRANOA_EXPIRATIONDATE,"yyyy-MM-dd","ns2:expirationDate")}
      (:&lt;ns2:paymentDate?>{data($parm/NF_TRANOA_PAYMENTDATE)}&lt;/ns2:paymentDate>:)
      (:&lt;ns2:expirationDate?>{data($parm/NF_TRANOA_EXPIRATIONDATE)}&lt;/ns2:expirationDate>:)
      &lt;ns2:codeProfile?>{data($parm/NF_TRANOA_CODEPROFILE)}&lt;/ns2:codeProfile>
      &lt;ns2:feeCode?>{data($parm/NF_TRANOA_FEECODE)}&lt;/ns2:feeCode>
      &lt;ns2:nonBusinessDayRule?>{sourceValue2Boolean(data($parm/NF_TRANOA_NONBUSINESSDAYRU),"1")}&lt;/ns2:nonBusinessDayRule>
      &lt;ns2:charging?>{sourceValue2Boolean(data($parm/NF_TRANOA_CHARGING),"1")}&lt;/ns2:charging>
      (:&lt;ns2:nonBusinessDayRule?>{data($parm/NF_TRANOA_NONBUSINESSDAYRU)}&lt;/ns2:nonBusinessDayRule>:)
      (:&lt;ns2:charging?>{data($parm/NF_TRANOA_CHARGING)}&lt;/ns2:charging>:)
      &lt;ns2:active?>{sourceValue2Boolean(data($parm/NF_TRANOA_ACTIVE),"0")}&lt;/ns2:active>
      &lt;ns2:userID?>{data($parm/NF_TRANOA_USERID)}&lt;/ns2:userID>
      {insertDate ($parm/NF_TRANOA_FIRSTPAYMENTDATE,"yyyy-MM-dd","ns2:firstPaymentDate")}
      (:&lt;ns2:firstPaymentDate?>{data($parm/NF_TRANOA_FIRSTPAYMENTDATE)}&lt;/ns2:firstPaymentDate>:)
      &lt;ns2:feeAmount?>{data($parm/NF_TRANOA_FEEAMOUNT)}&lt;/ns2:feeAmount>
      {getElementsForTransferOrderTargetList($parm)}
      &lt;ns2:account>
        &lt;ns0:Account>
          &lt;ns0:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTIBAN)}&lt;/ns0:accountNumber>
          {getElementsForAccountRelationshipList($parm)}
        &lt;/ns0:Account>
      &lt;/ns2:account>
      &lt;ns2:period>
        &lt;ns4:Period>
          &lt;ns4:period?>{data($parm/NF_PERIOD_PERIOD)}&lt;/ns4:period>
        &lt;/ns4:Period>
      &lt;/ns2:period>
      &lt;ns2:specificDay>
        &lt;ns4:SpecialDay>
          &lt;ns4:specialDay?>{data($parm/NF_SPECID_SPECIALDAY)}&lt;/ns4:specialDay>
        &lt;/ns4:SpecialDay>
      &lt;/ns2:specificDay>
    &lt;/ns2:TransferOrderAnchor>
  &lt;/ns6:orderOut>
&lt;/ns6:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>