<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-21</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="postCancelDirectDebit.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="postCancelDirectDebit.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns0:invokeResponse" location="postCancelDirectDebit.wsdl" ::)
(:: pragma bea:global-element-return element="ns5:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:acceptance.entities.be.dcl";
declare namespace ns3 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns5 = "http://bzwbk.com/nfe/transactionLog";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/postCancelDirectDebit/postCancelDirectDebitTLRequest/";
declare namespace ns6 = "urn:operations.entities.be.dcl";
declare namespace ns7 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns8 = "urn:entities.be.dcl";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare function xf:postCancelDirectDebitTLRequest($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke),
    $invokeResponse1 as element(ns0:invokeResponse)?,
    $faultResponse as element() ?)
    
    as element(ns5:transactionLogEntry) {
        &lt;ns5:transactionLogEntry>
            {
                for $businessTransactionType in $invoke1/ns0:transaction/ns6:Transaction/ns6:businessTransactionType/ns3:BusinessTransactionType/ns3:businessTransactionType
                return
                    &lt;businessTransactionType>{ data($businessTransactionType) }&lt;/businessTransactionType>
            }
            {
                for $TransactionMa in $invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa
                return
                    &lt;credit>
                        {
                            for $accountNumber in $TransactionMa/ns6:accountNumber
                            return
                                &lt;accountNumber>{ data($accountNumber) }&lt;/accountNumber>
                        }
                        {
                            for $address in $TransactionMa/ns6:address
                            return
                                &lt;address>{ data($address) }&lt;/address>
                        }
                        {
                            for $amountMa in $TransactionMa/ns6:amountMa
                            return
                                &lt;amount>{ xs:decimal( data($amountMa) ) }&lt;/amount>
                        }
                        {
                            for $amountMaEquiv in $TransactionMa/ns6:amountMaEquiv
                            return
                                &lt;amountPLN>{ xs:decimal( data($amountMaEquiv) ) }&lt;/amountPLN>
                        }
                        {
                            for $city in $TransactionMa/ns6:city
                            return
                                &lt;city>{ data($city) }&lt;/city>
                        }
                        {
                            for $currencyCode in $TransactionMa/ns6:currencyCode/ns2:CurrencyCode/ns2:currencyCode
                            return
                                &lt;currencyCode>{ data($currencyCode) }&lt;/currencyCode>
                        }
                        {
                            for $rate in $TransactionMa/ns6:rate
                            return
                                &lt;exchangeRate>{ xs:decimal( data($rate) ) }&lt;/exchangeRate>
                        }
                        {
                            for $name in $TransactionMa/ns6:name
                            return
                                &lt;name>{ data($name) }&lt;/name>
                        }
                        {
                            for $nameSecond in $TransactionMa/ns6:nameSecond
                            return
                                &lt;nameSecond>{ data($nameSecond) }&lt;/nameSecond>
                        }
                        {
                            for $zipCode in $TransactionMa/ns6:zipCode
                            return
                                &lt;zipCode>{ data($zipCode) }&lt;/zipCode>
                        }
                    &lt;/credit>
            }
            {
                for $csrMessageType in $invoke1/ns0:transaction/ns6:Transaction/ns6:csrMessageType/ns3:CsrMessageType/ns3:csrMessageType
                return
                    &lt;csrMessageType>{ data($csrMessageType) }&lt;/csrMessageType>
            }
            {
                for $TransactionWn in $invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn
                return
                    &lt;debit>
                        {
                            for $accountNumber in $TransactionWn/ns6:accountNumber
                            return
                                &lt;accountNumber>{ data($accountNumber) }&lt;/accountNumber>
                        }
                        {
                            for $address in $TransactionWn/ns6:address
                            return
                                &lt;address>{ data($address) }&lt;/address>
                        }
                        {
                            for $amountWn in $TransactionWn/ns6:amountWn
                            return
                                &lt;amount>{ xs:decimal( data($amountWn) ) }&lt;/amount>
                        }
                        {
                            for $amountWnEquiv in $TransactionWn/ns6:amountWnEquiv
                            return
                                &lt;amountPLN>{ xs:decimal( data($amountWnEquiv) ) }&lt;/amountPLN>
                        }
                        {
                            for $city in $TransactionWn/ns6:city
                            return
                                &lt;city>{ data($city) }&lt;/city>
                        }
                        {
                            for $currencyCode in $TransactionWn/ns6:currencyCode/ns2:CurrencyCode/ns2:currencyCode
                            return
                                &lt;currencyCode>{ data($currencyCode) }&lt;/currencyCode>
                        }
                        {
                            for $rate in $TransactionWn/ns6:rate
                            return
                                &lt;exchangeRate>{ xs:decimal( data($rate) ) }&lt;/exchangeRate>
                        }
                        {
                            for $name in $TransactionWn/ns6:name
                            return
                                &lt;name>{ data($name) }&lt;/name>
                        }
                        {
                            for $nameSecond in $TransactionWn/ns6:nameSecond
                            return
                                &lt;nameSecond>{ data($nameSecond) }&lt;/nameSecond>
                        }
                        {
                            for $zipCode in $TransactionWn/ns6:zipCode
                            return
                                &lt;zipCode>{ data($zipCode) }&lt;/zipCode>
                        }
                    &lt;/debit>
            }
            {
                for $dtTransactionType in $invoke1/ns0:transaction/ns6:Transaction/ns6:dtTransactionType/ns3:DtTransactionType/ns3:dtTransactionType
                return
                    &lt;dtTransactionType>{ data($dtTransactionType) }&lt;/dtTransactionType>
            }
            &lt;executor>
                {
                    for $branchCode in $invoke1/ns0:branchCode/ns2:BranchCode/ns2:branchCode
                    return
                        &lt;branchNumber>{ xs:int( data($branchCode) ) }&lt;/branchNumber>
                }
                {
                    for $userID in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns8:User/ns8:userID
                    return
                        &lt;executorID>{ data($userID) }&lt;/executorID>
                }
                {
                    for $userFirstName in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns8:User/ns8:userFirstName
                    return
                        &lt;firstName>{ data($userFirstName) }&lt;/firstName>
                }
                {
                    for $userLastName in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns8:User/ns8:userLastName
                    return
                        &lt;lastName>{ data($userLastName) }&lt;/lastName>
                }
                {
                    for $tellerID in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:teller/ns1:Teller/ns1:tellerID
                    return
                        &lt;tellerID>{ data($tellerID) }&lt;/tellerID>
                }
                {
                    for $tillID in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:till/ns1:Till/ns1:tillID
                    return
                        &lt;tillNumber>{ data($tillID) }&lt;/tillNumber>
                }
                {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
                (
                    for $beUserId in $invokeResponse1/ns0:backendResponse/ns6:BackendResponse/ns6:beUserId
                    return
                        &lt;userID>{ data($beUserId) }&lt;/userID>
                )
                	else
                	()

                }
            &lt;/executor>
            {
                for $extendedCSRMessageType in $invoke1/ns0:transaction/ns6:Transaction/ns6:extendedCSRMessageType/ns3:ExtendedCSRMessageType/ns3:extendedCSRMessageType
                return
                    &lt;extendedCSRMessageType>{ data($extendedCSRMessageType) }&lt;/extendedCSRMessageType>
            }
            &lt;hlbsName>postCancelDirectDebit&lt;/hlbsName>
            {
                for $putDownDate in $invoke1/ns0:transaction/ns6:Transaction/ns6:putDownDate
                return
                    &lt;putDownDate>{ xs:date( data($putDownDate) ) }&lt;/putDownDate>
            }
            &lt;timestamp>{ data($header1/ns0:msgHeader/ns0:timestamp) }&lt;/timestamp>
            {
                let $AcceptTask := $invoke1/ns0:acceptTask/ns4:AcceptTask
                return
                    &lt;tlAcceptance?>
                        {
                            for $acceptorFirstName in $AcceptTask/ns4:acceptorFirstName
                            return
                                &lt;acceptorFirstName>{ data($acceptorFirstName) }&lt;/acceptorFirstName>
                        }
                        {
                            for $acceptorLastName in $AcceptTask/ns4:acceptorLastName
                            return
                                &lt;acceptorLastName>{ data($acceptorLastName) }&lt;/acceptorLastName>
                        }
                        {
                            for $acceptor in $AcceptTask/ns4:acceptor
                            return
                                &lt;acceptorSkp>{ data($acceptor) }&lt;/acceptorSkp>
                        }
                        {
                            for $AcceptItem in $AcceptTask/ns4:acceptItemList/ns4:AcceptItem
                            return
                                &lt;tlAcceptanceTitleList?>
                                    {
                                        for $acceptItemTitle in $AcceptItem/ns4:acceptItemTitle/ns7:AcceptItemTitle/ns7:acceptItemTitle
                                        return
                                            &lt;acceptItemTitle>{ data($acceptItemTitle) }&lt;/acceptItemTitle>
                                    }
                                &lt;/tlAcceptanceTitleList>
                        }
                    &lt;/tlAcceptance>
            }
            {
                let $AcceptanceForBE := $invoke1/ns0:acceptanceForBE/ns6:AcceptanceForBE
                return
                    &lt;tlAcceptanceForBE?>
                        {
                            for $acceptorFirstName in $AcceptanceForBE/ns6:acceptorFirstName
                            return
                                &lt;acceptorFirstName>{ data($acceptorFirstName) }&lt;/acceptorFirstName>
                        }
                        {
                            for $acceptorLastName in $AcceptanceForBE/ns6:acceptorLastName
                            return
                                &lt;acceptorLastName>{ data($acceptorLastName) }&lt;/acceptorLastName>
                        }
                        {
                            for $acceptorSKP in $AcceptanceForBE/ns6:acceptorSKP
                            return
                                &lt;acceptorSkp>{ data($acceptorSKP) }&lt;/acceptorSkp>
                        }
                        {
                            for $flag in $AcceptanceForBE/ns6:flag
                            return
                                &lt;flag>{ data($flag) }&lt;/flag>
                        }
                        {
                            for $BeErrorCode in $AcceptanceForBE/ns6:beErrorCodeList/ns6:BeErrorCode
                            return
                                &lt;tlErrorCodeList?>
                                    &lt;errorCode?>
                                      &lt;errorCode>{ data($BeErrorCode/ns6:errorCode/ns3:BackendErrorCode/ns3:errorCode) }&lt;/errorCode>
                                        &lt;systemId>1&lt;/systemId>
                                    &lt;/errorCode>
                                &lt;/tlErrorCodeList>
                        }
                    &lt;/tlAcceptanceForBE>
            }
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                for $BackendResponse in $invokeResponse1/ns0:backendResponse/ns6:BackendResponse
                return
                    &lt;tlBackendResponse>
                        {
                            for $balanceCredit in $BackendResponse/ns6:balanceCredit
                            return
                                &lt;balanceCredit>{ xs:decimal( data($balanceCredit) ) }&lt;/balanceCredit>
                        }
                        {
                            for $balanceDebit in $BackendResponse/ns6:balanceDebit
                            return
                                &lt;balanceDebit>{ xs:decimal( data($balanceDebit) ) }&lt;/balanceDebit>
                        }
                        {
                            for $dateTime in $BackendResponse/ns6:dateTime
                            return
                                &lt;dateTime>{ data($dateTime) }&lt;/dateTime>
                        }
                        {
                            for $icbsDate in $BackendResponse/ns6:icbsDate
                            return
                                &lt;icbsDate>{ xs:date( data($icbsDate) ) }&lt;/icbsDate>
                        }
                        {
                            for $icbsSessionNumber in $BackendResponse/ns6:icbsSessionNumber
                            return
                                &lt;icbsSessionNumber>{ data($icbsSessionNumber) }&lt;/icbsSessionNumber>
                        }
                        {
                            for $psTransactionNumber in $BackendResponse/ns6:psTransactionNumber
                            return
                                &lt;psTransactionNumber>{ data($psTransactionNumber) }&lt;/psTransactionNumber>
                        }
                        {
                            for $BeErrorCode in $BackendResponse/ns6:beErrorCodeList/ns6:BeErrorCode
                            return
                                &lt;tlErrorCodeList?>
                                    &lt;errorCode?>
                                        {
                                            for $errorCode in $BeErrorCode/ns6:errorCode/ns3:BackendErrorCode/ns3:errorCode
                                            return
                                                &lt;errorCode>{ data($errorCode) }&lt;/errorCode>
                                        }
                                        &lt;systemId>1&lt;/systemId>
                                    &lt;/errorCode>
                                &lt;/tlErrorCodeList>
                        }
                        {
                            for $transactionRefNumber in $BackendResponse/ns6:transactionRefNumber
                            return
                                &lt;transactionRefNumber>{ data($transactionRefNumber) }&lt;/transactionRefNumber>
                        }
                    &lt;/tlBackendResponse>
                    )
                            
                	else
                	()
            }
            {
                for $DirectDebitCallOff in $invoke1/ns0:transaction/ns6:Transaction/ns6:directDebitCallOff/ns6:DirectDebitCallOff
                return
                    &lt;tlDirectDebitCallOff>
                        {
                            for $interestReturn in $DirectDebitCallOff/ns6:interestReturn
                            return
                                &lt;interestReturn>{ xs:decimal( data($interestReturn) ) }&lt;/interestReturn>
                        }
                        {
                            for $originDDTxnAmount in $DirectDebitCallOff/ns6:originDDTxnAmount
                            return
                                &lt;originDDTxnAmount>{ xs:decimal( data($originDDTxnAmount) ) }&lt;/originDDTxnAmount>
                        }
                        {
                            for $originDDTxnDate in $DirectDebitCallOff/ns6:originDDTxnDate
                            return
                                &lt;originDDTxnDate>{ xs:date( data($originDDTxnDate) ) }&lt;/originDDTxnDate>
                        }
                        {
                            for $originDDTxnReferenceNr in $DirectDebitCallOff/ns6:originDDTxnReferenceNr
                            return
                                &lt;originDDTxnReferenceNr>{ data($originDDTxnReferenceNr) }&lt;/originDDTxnReferenceNr>
                        }
                    &lt;/tlDirectDebitCallOff>
            }
            {
                for $transactionDate in $invoke1/ns0:transaction/ns6:Transaction/ns6:transactionDate
                return
                    &lt;transactionDate>{ xs:date( data($transactionDate) ) }&lt;/transactionDate>
            }

            &lt;tlExceptionDataList?>
            {
            	let $errorCode1 := data($faultResponse//err:exceptionItem/err:errorCode1)
            	let $errorCode2 := data($faultResponse//err:exceptionItem/err:errorCode2)
                let $errorDescription := data($faultResponse//err:exceptionItem/err:errorDescription)
            	return
                   if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') != '') then
            		(
		                &lt;errorCode1?>{ xs:int($errorCode1) }&lt;/errorCode1>,
        		        &lt;errorCode2?>{ xs:int($errorCode2) }&lt;/errorCode2>,
                		&lt;errorDescription?>{ $errorDescription }&lt;/errorDescription>
                	)
                	else
                	()
			}
            &lt;/tlExceptionDataList>


            {
                for $transactionGroupID in $invoke1/ns0:transaction/ns6:Transaction/ns6:transactionGroupID
                return
                    &lt;transactionGroupID>{ data($transactionGroupID) }&lt;/transactionGroupID>
            }
            &lt;transactionID>{ data($header1/ns0:transHeader/ns0:transId) }&lt;/transactionID>
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                for $transactionStatus in $invokeResponse1/ns0:transactionOut/ns6:Transaction/ns6:transactionStatus/ns2:TransactionStatus/ns2:transactionStatus
                return
                    &lt;transactionStatus>{ data($transactionStatus) }&lt;/transactionStatus>
                	)
                	else
                	()
            }
            {
                for $title in $invoke1/ns0:transaction/ns6:Transaction/ns6:title
                return
                    &lt;transactionTitle>{ data($title) }&lt;/transactionTitle>
            }
        &lt;/ns5:transactionLogEntry>
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;
declare variable $invokeResponse1 as element(ns0:invokeResponse) ? external;
declare variable $faultResponse  as element() ? external;

&lt;soap-env:Body>{
xf:postCancelDirectDebitTLRequest($header1,
    $invoke1,
    $invokeResponse1,
    $faultResponse)
    
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>