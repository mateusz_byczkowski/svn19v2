<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace ns6="urn:accountdict.dictionaries.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string
{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse>
  &lt;ns5:response>
    &lt;ns0:ResponseMessage>
      &lt;ns0:result?>{data($parm/NF_RESPOM_RESULT)}&lt;/ns0:result>
    &lt;/ns0:ResponseMessage>
  &lt;/ns5:response>
  &lt;ns5:statementParameters>
    &lt;ns1:StatementParameters>
    {if (string-length(data($parm/NF_STATEP_FREQUENCY))>0) then
      &lt;ns1:frequency?>{data($parm/NF_STATEP_FREQUENCY)}&lt;/ns1:frequency>
     else ()
     }
      (:&lt;ns1:nextPrintoutDate?>{data($parm/NF_STATEP_NEXTPRINTOUTDATE)}&lt;/ns1:nextPrintoutDate>:)
      {insertDate(data($parm/NF_STATEP_NEXTPRINTOUTDATE),"yyyy-MM-dd","ns1:nextPrintoutDate")}
      &lt;ns1:printFlag?>{data($parm/NF_STATEP_PRINTFLAG)}&lt;/ns1:printFlag>
     {
      if (string-length(data($parm/NF_STATEP_SPECIALDAY))>0) then
             &lt;ns1:specialDay?>{data($parm/NF_STATEP_SPECIALDAY)}&lt;/ns1:specialDay>
     else ()
     }

      &lt;ns1:aggregateTransaction?>{sourceValue2Boolean (data($parm/NF_STATEP_AGGREGATETRANSAC),"1")}&lt;/ns1:aggregateTransaction>
      &lt;ns1:cycle>
        &lt;ns2:Period>
        {
           if (string-length(data($parm/NF_PERIOD_PERIOD))>0) then
          &lt;ns2:period?>{data($parm/NF_PERIOD_PERIOD)}&lt;/ns2:period>
         else()
        }
        &lt;/ns2:Period>
      &lt;/ns1:cycle>
      &lt;ns1:provideManner>
        &lt;ns2:ProvideManner>
          &lt;ns2:provideManner?>{data($parm/NF_PROVIM_PROVIDEMANNER)}&lt;/ns2:provideManner>
        &lt;/ns2:ProvideManner>
      &lt;/ns1:provideManner>
      &lt;ns1:snapshotType>
        &lt;ns6:SnapshotType>
          {if (string-length(data($parm/NF_STATEP_SNAPSHOTTYPE))> 0) then
              &lt;ns6:snapshotType?>{data($parm/NF_STATEP_SNAPSHOTTYPE)}&lt;/ns6:snapshotType>
           else ()
         }
        &lt;/ns6:SnapshotType>
      &lt;/ns1:snapshotType>
    &lt;/ns1:StatementParameters>
  &lt;/ns5:statementParameters>
&lt;/ns5:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>