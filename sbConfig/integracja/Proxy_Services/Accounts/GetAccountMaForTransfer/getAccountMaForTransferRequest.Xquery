<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns6:msgHeader/ns6:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns6:msgHeader/ns6:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns6:msgHeader/ns6:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns6:msgHeader/ns6:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns6:msgHeader/ns6:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns6:msgHeader/ns6:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns6:transHeader/ns6:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{

&lt;NF_ACCOUN_ACCOUNTIBAN?>{data($parm/ns6:account/ns0:Account/ns0:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTIBAN>
,
&lt;NF_BRANCC_BRANCHCODE?>{data($parm/ns6:account/ns0:Account/ns0:accountBranchNumber/ns4:BranchCode/ns4:branchCode)}&lt;/NF_BRANCC_BRANCHCODE>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvoke($body/ns6:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>