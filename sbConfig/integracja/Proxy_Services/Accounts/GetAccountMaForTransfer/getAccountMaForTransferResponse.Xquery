<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:productstree.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns6:invokeResponse>
    &lt;ns6:customerOut>
    &lt;ns2:Customer>
      &lt;ns2:companyName?>{data($parm/NF_CUSTOM_COMPANYNAME)}&lt;/ns2:companyName>
      &lt;ns2:customerNumber?>{data($parm/NF_CUSTOM_CUSTOMERNUMBER)}&lt;/ns2:customerNumber>
      &lt;ns2:customerPersonal>
        &lt;ns2:CustomerPersonal>
          &lt;ns2:lastName?>{data($parm/NF_CUSTOP_LASTNAME)}&lt;/ns2:lastName>
          &lt;ns2:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME)}&lt;/ns2:firstName>
        &lt;/ns2:CustomerPersonal>
      &lt;/ns2:customerPersonal>
    &lt;/ns2:Customer>
  &lt;/ns6:customerOut>
  &lt;ns6:accountOut>
    &lt;ns0:Account>
      &lt;ns0:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTIBAN)}&lt;/ns0:accountNumber>
      {
      if (string-length(data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI)) >0) then
      &lt;ns0:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI)}&lt;/ns0:accountDescription>
      else() 
      }
      &lt;ns0:accountType>
        &lt;ns4:AccountType>
          {
          if (string-length(data($parm/NF_ACCOUT_ACCOUNTTYPE))>0) then 
          &lt;ns4:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE)}&lt;/ns4:accountType>
          else()
         }
        &lt;/ns4:AccountType>
      &lt;/ns0:accountType>
      &lt;ns0:currency>
        &lt;ns4:CurrencyCode>
          &lt;ns4:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE)}&lt;/ns4:currencyCode>
        &lt;/ns4:CurrencyCode>
      &lt;/ns0:currency>
      &lt;ns0:accountStatus>
        &lt;ns1:AccountStatus>
          &lt;ns1:accountStatus?>{data($parm/NF_ACCOUS_ACCOUNTSTATUS)}&lt;/ns1:accountStatus>
        &lt;/ns1:AccountStatus>
      &lt;/ns0:accountStatus>
      &lt;ns0:productDefinition>
        &lt;ns7:ProductDefinition>
         {
          if (string-length(data($parm/NF_PRODUD_IDPRODUCTGROUP))>0) then 
          &lt;ns7:idProductGroup?>{data($parm/NF_PRODUD_IDPRODUCTGROUP)}&lt;/ns7:idProductGroup>
          else()
         }
         {
          if (string-length(data($parm/NF_PRODUD_IDPRODUCTDEFINIT))>0) then 
          &lt;ns7:idProductDefinition?>{data($parm/NF_PRODUD_IDPRODUCTDEFINIT)}&lt;/ns7:idProductDefinition>
          else()
         }          
        &lt;/ns7:ProductDefinition>
      &lt;/ns0:productDefinition>
    &lt;/ns0:Account>
  &lt;/ns6:accountOut>
  &lt;ns6:addressOut>
    &lt;ns2:Address>
      &lt;ns2:houseFlatNumber?>{data($parm/NF_ADDRES_HOUSEFLATNUMBER)}&lt;/ns2:houseFlatNumber>
      &lt;ns2:city?>{data($parm/NF_ADDRES_CITY)}&lt;/ns2:city>
      &lt;ns2:street?>{data($parm/NF_ADDRES_STREET)}&lt;/ns2:street>
      &lt;ns2:zipCode?>{data($parm/NF_ADDRES_ZIPCODE)}&lt;/ns2:zipCode>
    &lt;/ns2:Address>
  &lt;/ns6:addressOut>
&lt;/ns6:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>