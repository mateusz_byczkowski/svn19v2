<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-21</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="postCloseAccount.wsdl"::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="postCloseAccount.wsdl"::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns0:invokeResponse" location="postCloseAccount.wsdl"::)
(:: pragma bea:global-element-return element="ns8:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns9 = "urn:operations.entities.be.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6 = "urn:acceptance.entities.be.dcl";
declare namespace ns10 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns7 = "urn:productstree.entities.be.dcl";
declare namespace ns11 = "urn:entities.be.dcl";
declare namespace ns8 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns2 = "urn:transactionbasketdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:accounts.entities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/postCloseAccount/postCloseAccountTLRequest/";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare function xf:postCloseAccountTLRequest($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke),
    $invokeResponse1 as element(ns0:invokeResponse) ?,
    $faultResponse as element() ?)
    
    as element(ns8:transactionLogEntry) {
        &lt;ns8:transactionLogEntry>
            {
                for $businessTransactionType in $invoke1/ns0:transaction/ns9:Transaction/ns9:businessTransactionType/ns5:BusinessTransactionType/ns5:businessTransactionType
                return
                    &lt;businessTransactionType>{ data($businessTransactionType) }&lt;/businessTransactionType>
            }
            {
                for $cashTransactionBasketID in $invoke1/ns0:transaction/ns9:Transaction/ns9:cashTransactionBasketID
                return
                    &lt;cashTransactionBasketID>{ data($cashTransactionBasketID) }&lt;/cashTransactionBasketID>
            }
            {
                for $TransactionMa in $invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa
                return
                    &lt;credit>
                        {
                            for $accountNumber in $TransactionMa/ns9:accountNumber
                            return
                                &lt;accountNumber>{ data($accountNumber) }&lt;/accountNumber>
                        }
                        {
                            for $address in $TransactionMa/ns9:address
                            return
                                &lt;address>{ data($address) }&lt;/address>
                        }
                        {
                            for $amountMa in $TransactionMa/ns9:amountMa
                            return
                                &lt;amount>{ xs:decimal( data($amountMa) ) }&lt;/amount>
                        }
                        {
                            for $amountMaEquiv in $TransactionMa/ns9:amountMaEquiv
                            return
                                &lt;amountPLN>{ xs:decimal( data($amountMaEquiv) ) }&lt;/amountPLN>
                        }
                        {
                            for $city in $TransactionMa/ns9:city
                            return
                                &lt;city>{ data($city) }&lt;/city>
                        }
                        {
                            for $currencyCode in $TransactionMa/ns9:currencyCode/ns4:CurrencyCode/ns4:currencyCode
                            return
                                &lt;currencyCode>{ data($currencyCode) }&lt;/currencyCode>
                        }
                        {
                            for $rate in $TransactionMa/ns9:rate
                            return
                                &lt;exchangeRate>{ xs:decimal( data($rate) ) }&lt;/exchangeRate>
                        }
                        {
                            for $name in $TransactionMa/ns9:name
                            return
                                &lt;name>{ data($name) }&lt;/name>
                        }
                        {
                            for $nameSecond in $TransactionMa/ns9:nameSecond
                            return
                                &lt;nameSecond>{ data($nameSecond) }&lt;/nameSecond>
                        }
                        {
                            for $zipCode in $TransactionMa/ns9:zipCode
                            return
                                &lt;zipCode>{ data($zipCode) }&lt;/zipCode>
                        }
                    &lt;/credit>
            }
            {
                for $csrMessageType in $invoke1/ns0:transaction/ns9:Transaction/ns9:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType
                return
                    &lt;csrMessageType>{ data($csrMessageType) }&lt;/csrMessageType>
            }
            {
                for $TransactionWn in $invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn
                return
                    &lt;debit>
                        {
                            for $accountNumber in $TransactionWn/ns9:accountNumber
                            return
                                &lt;accountNumber>{ data($accountNumber) }&lt;/accountNumber>
                        }
                        {
                            for $address in $TransactionWn/ns9:address
                            return
                                &lt;address>{ data($address) }&lt;/address>
                        }
                        {
                            for $amountWn in $TransactionWn/ns9:amountWn
                            return
                                &lt;amount>{ xs:decimal( data($amountWn) ) }&lt;/amount>
                        }
                        {
                            for $amountWnEquiv in $TransactionWn/ns9:amountWnEquiv
                            return
                                &lt;amountPLN>{ xs:decimal( data($amountWnEquiv) ) }&lt;/amountPLN>
                        }
                        {
                            for $city in $TransactionWn/ns9:city
                            return
                                &lt;city>{ data($city) }&lt;/city>
                        }
                        {
                            for $currencyCode in $TransactionWn/ns9:currencyCode/ns4:CurrencyCode/ns4:currencyCode
                            return
                                &lt;currencyCode>{ data($currencyCode) }&lt;/currencyCode>
                        }
                        {
                            for $rate in $TransactionWn/ns9:rate
                            return
                                &lt;exchangeRate>{ xs:decimal( data($rate) ) }&lt;/exchangeRate>
                        }
                        {
                            for $name in $TransactionWn/ns9:name
                            return
                                &lt;name>{ data($name) }&lt;/name>
                        }
                        {
                            for $nameSecond in $TransactionWn/ns9:nameSecond
                            return
                                &lt;nameSecond>{ data($nameSecond) }&lt;/nameSecond>
                        }
                        {
                            for $zipCode in $TransactionWn/ns9:zipCode
                            return
                                &lt;zipCode>{ data($zipCode) }&lt;/zipCode>
                        }
                    &lt;/debit>
            }
            {
                let $result :=
                    for $Disposer in $invoke1/ns0:transaction/ns9:Transaction/ns9:disposerList/ns9:Disposer
                    return
                        &lt;disposer1>
                            {
                                for $address in $Disposer/ns9:address
                                return
                                    &lt;address>{ data($address) }&lt;/address>
                            }
                            {
                                for $cif in $Disposer/ns9:cif
                                return
                                    &lt;cif>{ data($cif) }&lt;/cif>
                            }
                            {
                                for $citizenshipCode in $Disposer/ns9:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode
                                return
                                    &lt;citizenship>{ data($citizenshipCode) }&lt;/citizenship>
                            }
                            {
                                for $city in $Disposer/ns9:city
                                return
                                    &lt;city>{ data($city) }&lt;/city>
                            }
                            {
                                for $countryCode in $Disposer/ns9:countryCode/ns4:CountryCode/ns4:countryCode
                                return
                                    &lt;countryCode>{ data($countryCode) }&lt;/countryCode>
                            }
                            {
                                for $dateOfBirth in $Disposer/ns9:dateOfBirth
                                return
                                    &lt;dateOfBirth>{ xs:date( data($dateOfBirth) ) }&lt;/dateOfBirth>
                            }
                            {
                                for $documentNumber in $Disposer/ns9:documentNumber
                                return
                                    &lt;documentNumber>{ data($documentNumber) }&lt;/documentNumber>
                            }
                            {
                                for $firstName in $Disposer/ns9:firstName
                                return
                                    &lt;firstName>{ data($firstName) }&lt;/firstName>
                            }
                            {
                                for $lastName in $Disposer/ns9:lastName
                                return
                                    &lt;lastName>{ data($lastName) }&lt;/lastName>
                            }
                            {
                                for $pesel in $Disposer/ns9:pesel
                                return
                                    &lt;pesel>{ data($pesel) }&lt;/pesel>
                            }
                            {
                                for $zipCode in $Disposer/ns9:zipCode
                                return
                                    &lt;zipCode>{ data($zipCode) }&lt;/zipCode>
                            }
                        &lt;/disposer1>
                return
                    $result[1]
            }
            {
                let $result :=
                    for $Disposer in $invoke1/ns0:transaction/ns9:Transaction/ns9:disposerList/ns9:Disposer
                    return
                        &lt;disposer2>
                            {
                                for $address in $Disposer/ns9:address
                                return
                                    &lt;address>{ data($address) }&lt;/address>
                            }
                            {
                                for $cif in $Disposer/ns9:cif
                                return
                                    &lt;cif>{ data($cif) }&lt;/cif>
                            }
                            {
                                for $citizenshipCode in $Disposer/ns9:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode
                                return
                                    &lt;citizenship>{ data($citizenshipCode) }&lt;/citizenship>
                            }
                            {
                                for $city in $Disposer/ns9:city
                                return
                                    &lt;city>{ data($city) }&lt;/city>
                            }
                            {
                                for $countryCode in $Disposer/ns9:countryCode/ns4:CountryCode/ns4:countryCode
                                return
                                    &lt;countryCode>{ data($countryCode) }&lt;/countryCode>
                            }
                            {
                                for $dateOfBirth in $Disposer/ns9:dateOfBirth
                                return
                                    &lt;dateOfBirth>{ data($dateOfBirth) }&lt;/dateOfBirth>
                            }
                            {
                                for $documentNumber in $Disposer/ns9:documentNumber
                                return
                                    &lt;documentNumber>{ data($documentNumber) }&lt;/documentNumber>
                            }
                            {
                                for $documentTypeForTxn in $Disposer/ns9:documentType/ns5:DocumentTypeForTxn/ns5:documentTypeForTxn
                                return
                                    &lt;documentType>{ data($documentTypeForTxn) }&lt;/documentType>
                            }
                            {
                                for $firstName in $Disposer/ns9:firstName
                                return
                                    &lt;firstName>{ data($firstName) }&lt;/firstName>
                            }
                            {
                                for $lastName in $Disposer/ns9:lastName
                                return
                                    &lt;lastName>{ data($lastName) }&lt;/lastName>
                            }
                            {
                                for $pesel in $Disposer/ns9:pesel
                                return
                                    &lt;pesel>{ data($pesel) }&lt;/pesel>
                            }
                            {
                                for $zipCode in $Disposer/ns9:zipCode
                                return
                                    &lt;zipCode>{ data($zipCode) }&lt;/zipCode>
                            }
                        &lt;/disposer2>
                return
                    $result[2]
            }
            {
                for $dtTransactionType in $invoke1/ns0:transaction/ns9:Transaction/ns9:dtTransactionType/ns5:DtTransactionType/ns5:dtTransactionType
                return
                    &lt;dtTransactionType>{ data($dtTransactionType) }&lt;/dtTransactionType>
            }
            {
                let $UserTxnSession := $invoke1/ns0:userTxnSession/ns1:UserTxnSession
                return
                    &lt;executor>
                        {
                            for $branchCode in $invoke1/ns0:branchCode/ns4:BranchCode/ns4:branchCode
                            return
                                &lt;branchNumber>{ xs:int( data($branchCode) ) }&lt;/branchNumber>
                        }
                        {
                            for $userID in $UserTxnSession/ns1:user/ns11:User/ns11:userID
                            return
                                &lt;executorID>{ data($userID) }&lt;/executorID>
                        }
                        {
                            for $userFirstName in $UserTxnSession/ns1:user/ns11:User/ns11:userFirstName
                            return
                                &lt;firstName>{ data($userFirstName) }&lt;/firstName>
                        }
                        {
                            for $userLastName in $UserTxnSession/ns1:user/ns11:User/ns11:userLastName
                            return
                                &lt;lastName>{ data($userLastName) }&lt;/lastName>
                        }
                        {
                            for $tellerID in $UserTxnSession/ns1:teller/ns1:Teller/ns1:tellerID
                            return
                                &lt;tellerID>{ data($tellerID) }&lt;/tellerID>
                        }
                        {
                            for $tillID in $UserTxnSession/ns1:till/ns1:Till/ns1:tillID
                            return
                                &lt;tillNumber>{ data($tillID) }&lt;/tillNumber>
                        }
                        {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
                (
                            for $beUserId in $invokeResponse1/ns0:backendResponse/ns9:BackendResponse/ns9:beUserId
                            return
                                &lt;userID>{ data($beUserId) }&lt;/userID>
                )
                	else
                	()
                        }
                    &lt;/executor>
            }
            {
                for $extendedCSRMessageType in $invoke1/ns0:transaction/ns9:Transaction/ns9:extendedCSRMessageType/ns5:ExtendedCSRMessageType/ns5:extendedCSRMessageType
                return
                    &lt;extendedCSRMessageType>{ data($extendedCSRMessageType) }&lt;/extendedCSRMessageType>
            }
            &lt;hlbsName>postCloseAccount&lt;/hlbsName>
            {
                for $orderedBy in $invoke1/ns0:transaction/ns9:Transaction/ns9:orderedBy
                return
                    &lt;orderedBy>{ data($orderedBy) }&lt;/orderedBy>
            }
            {
                for $putDownDate in $invoke1/ns0:transaction/ns9:Transaction/ns9:putDownDate
                return
                    &lt;putDownDate>{ xs:date( data($putDownDate) ) }&lt;/putDownDate>
            }
            &lt;timestamp>{ data($header1/ns0:msgHeader/ns0:timestamp) }&lt;/timestamp>
            {
                let $acceptTask := $invoke1/ns0:acceptTask
                return
                    &lt;tlAcceptance?>
                        {
                            for $acceptorFirstName in $acceptTask/ns6:AcceptTask/ns6:acceptorFirstName
                            return
                                &lt;acceptorFirstName>{ data($acceptorFirstName) }&lt;/acceptorFirstName>
                        }
                        {
                            for $acceptorLastName in $acceptTask/ns6:AcceptTask/ns6:acceptorLastName
                            return
                                &lt;acceptorLastName>{ data($acceptorLastName) }&lt;/acceptorLastName>
                        }
                        {
                            for $acceptor in $acceptTask/ns6:AcceptTask/ns6:acceptor
                            return
                                &lt;acceptorSkp>{ data($acceptor) }&lt;/acceptorSkp>
                        }
                        {
                            for $AcceptItem in $acceptTask/ns6:AcceptTask/ns6:acceptItemList/ns6:AcceptItem
                            return
                                &lt;tlAcceptanceTitleList>
                                    {
                                        for $acceptItemTitle in $AcceptItem/ns6:acceptItemTitle/ns10:AcceptItemTitle/ns10:acceptItemTitle
                                        return
                                            &lt;acceptItemTitle>{ data($acceptItemTitle) }&lt;/acceptItemTitle>
                                    }
                                &lt;/tlAcceptanceTitleList>
                        }
                    &lt;/tlAcceptance>
            }
            {
                let $acceptanceForBE := $invoke1/ns0:acceptanceForBE
                return
                    &lt;tlAcceptanceForBE?>
                        {
                            for $acceptorFirstName in $acceptanceForBE/ns9:AcceptanceForBE/ns9:acceptorFirstName
                            return
                                &lt;acceptorFirstName>{ data($acceptorFirstName) }&lt;/acceptorFirstName>
                        }
                        {
                            for $acceptorLastName in $acceptanceForBE/ns9:AcceptanceForBE/ns9:acceptorLastName
                            return
                                &lt;acceptorLastName>{ data($acceptorLastName) }&lt;/acceptorLastName>
                        }
                        {
                            for $acceptorSKP in $acceptanceForBE/ns9:AcceptanceForBE/ns9:acceptorSKP
                            return
                                &lt;acceptorSkp>{ data($acceptorSKP) }&lt;/acceptorSkp>
                        }
                        {
                            for $flag in $acceptanceForBE/ns9:AcceptanceForBE/ns9:flag
                            return
                                &lt;flag>{ data($flag) }&lt;/flag>
                        }
                        {
                            for $BeErrorCode in $acceptanceForBE/ns9:AcceptanceForBE/ns9:beErrorCodeList/ns9:BeErrorCode
                            return
                                &lt;tlErrorCodeList?>
                                    &lt;errorCode?>
                                        {
                                            for $errorCode in $BeErrorCode/ns9:errorCode/ns5:BackendErrorCode/ns5:errorCode
                                            return
                                                &lt;errorCode>{ data($errorCode) }&lt;/errorCode>
                                        }
                                        &lt;systemId>1&lt;/systemId>
                                    &lt;/errorCode>
                                &lt;/tlErrorCodeList>
                        }
                    &lt;/tlAcceptanceForBE>
            }
            {
                let $Account := $invoke1/ns0:account/ns3:Account
                return
                    &lt;tlAccountList?>
                        {
                            for $accountNumber in $Account/ns3:accountNumber
                            return
                                &lt;accountNumber>{ data($accountNumber) }&lt;/accountNumber>
                        }
                        {
                            for $idProductDefinition in $Account/ns3:productDefinition/ns7:ProductDefinition/ns7:idProductDefinition
                            return
                                &lt;productId>{ data($idProductDefinition) }&lt;/productId>
                        }
                    &lt;/tlAccountList>
            }
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                let $backendResponse := $invokeResponse1/ns0:backendResponse
                return
                    &lt;tlBackendResponse?>
                        {
                            for $balanceCredit in $backendResponse/ns9:BackendResponse/ns9:balanceCredit
                            return
                                &lt;balanceCredit>{ xs:decimal( data($balanceCredit) ) }&lt;/balanceCredit>
                        }
                        {
                            for $balanceDebit in $backendResponse/ns9:BackendResponse/ns9:balanceDebit
                            return
                                &lt;balanceDebit>{ xs:decimal( data($balanceDebit) ) }&lt;/balanceDebit>
                        }
                        {
                            for $dateTime in $backendResponse/ns9:BackendResponse/ns9:dateTime
                            return
                                &lt;dateTime>{ data($dateTime) }&lt;/dateTime>
                        }
                        {
                            for $icbsDate in $backendResponse/ns9:BackendResponse/ns9:icbsDate
                            return
                                &lt;icbsDate>{ xs:date( data($icbsDate)) }&lt;/icbsDate>
                        }
                        {
                            for $icbsSessionNumber in $backendResponse/ns9:BackendResponse/ns9:icbsSessionNumber
                            return
                                &lt;icbsSessionNumber>{ data($icbsSessionNumber) }&lt;/icbsSessionNumber>
                        }
                        {
                            for $psTransactionNumber in $backendResponse/ns9:BackendResponse/ns9:psTransactionNumber
                            return
                                &lt;psTransactionNumber>{ data($psTransactionNumber) }&lt;/psTransactionNumber>
                        }
                        {
                            for $BeErrorCode in $backendResponse/ns9:BackendResponse/ns9:beErrorCodeList/ns9:BeErrorCode
                            return
                                &lt;tlErrorCodeList?>
                                    &lt;errorCode?>
                                        {
                                            for $errorCode in $BeErrorCode/ns9:errorCode/ns5:BackendErrorCode/ns5:errorCode
                                            return
                                                &lt;errorCode>{ data($errorCode) }&lt;/errorCode>
                                        }
                                        &lt;systemId>1&lt;/systemId>
                                    &lt;/errorCode>
                                &lt;/tlErrorCodeList>
                        }
                        {
                            for $transactionRefNumber in $backendResponse/ns9:BackendResponse/ns9:transactionRefNumber
                            return
                                &lt;transactionRefNumber>{ data($transactionRefNumber) }&lt;/transactionRefNumber>
                        }
                    &lt;/tlBackendResponse>
                    )
                            
                	else
                	()
            }
            {
                for $transactionDate in $invoke1/ns0:transaction/ns9:Transaction/ns9:transactionDate
                return
                    &lt;transactionDate>{ xs:date(data($transactionDate) ) }&lt;/transactionDate>
            }
            {
                for $transactionGroupID in $invoke1/ns0:transaction/ns9:Transaction/ns9:transactionGroupID
                return
                    &lt;transactionGroupID>{ data($transactionGroupID) }&lt;/transactionGroupID>
            }
            &lt;transactionID>{ data($header1/ns0:transHeader/ns0:transId) }&lt;/transactionID>
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                for $transactionStatus in $invokeResponse1/ns0:transactionOut/ns9:Transaction/ns9:transactionStatus/ns4:TransactionStatus/ns4:transactionStatus
                return
                    &lt;transactionStatus>{ data($transactionStatus) }&lt;/transactionStatus>
                	)
                	else
                	()
            }
            {
                for $title in $invoke1/ns0:transaction/ns9:Transaction/ns9:title
                return
                    &lt;transactionTitle>{ data($title) }&lt;/transactionTitle>
            }
        &lt;/ns8:transactionLogEntry>
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;
declare variable $invokeResponse1 as element(ns0:invokeResponse) external;
declare variable $faultResponse  as element() ? external;

&lt;soap-env:Body>{
xf:postCloseAccountTLRequest($header1,
    $invoke1,
    $invokeResponse1,
    $faultResponse)
    
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>