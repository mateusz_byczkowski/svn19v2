<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v1.11 - zamiana przecinka na kropkę w liczbach.</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @author  Tomasz Krajewski
 : @version 1.11
 : @since   2010-10-21
 :
 : wersja WSDLa: 04-02-2010 13:48:54
 :
 : $Proxy Services/Accounts/getAccountDetails/getAccountDetailsRequest.xq$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getAccountDetails/getAccountDetailsResponse/";
declare namespace ns0 = "urn:accountdict.dictionaries.be.dcl";
declare namespace ns1 = "urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns2 = "urn:productstree.entities.be.dcl";
declare namespace ns3 = "";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns5 = "urn:operations.entities.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns7="urn:accounts.entities.be.dcl";

declare variable $fML321 as element(ns3:FML32) external;
declare variable $PTGetPrBranchResponse  as element() external;
declare variable $CISGetCustomerResponse as element() external;

(: v1.9 - funkcja rozdzielająca pełną nazwę klienta na 2 pola :)
declare function local:SplitName($FullName as xs:string) as xs:string+
{
	let $tmpName := fn:normalize-space($FullName)
	let $Length as xs:integer := string-length($tmpName)
	let $Length2 := $Length idiv 2
	return 
	(
		if ($Length > 35) then (: Rozdzielamy na dwie części :)
		(
			if ($Length2 &lt; 35) then 
			(
				(: Szukamy spacji na prawo od środka w $Length2 :)
				for $i in $Length2 to min(($Length, 35))
				let $char := substring($tmpName, $i, 1)
				return
				(
					if ($char eq ' ') then
					(
						fn:normalize-space(substring($tmpName, 1, $i)),
						fn:normalize-space(substring($tmpName, $i + 1, 35)),
						"1"
					)
					else ()
				),
				(: Jeśli powyższa pętla zawiedzie, szukamy spacji na lewo od środka w $Length2 :)
				for $i in reverse(1 to $Length2)
				let $char := substring($tmpName, $i, 1)
				return
				(
					if ($char eq ' ') then
					(
						fn:normalize-space(substring($tmpName, 1, $i)),
						fn:normalize-space(substring($tmpName, $i + 1, 35)),
						"2"
					)
					else ()
				),
				(: Jeśli powyższa pętla zawiedzie, szukamy podziału na prawo od środka w $Length2 :)
				for $i in $Length2 to min(($Length, 35))
				let $char := substring($tmpName, $i, 1)
				return
				(
					if ($char eq '.' or $char eq ',' or $char eq '-') then
					(
						fn:normalize-space(substring($tmpName, 1, $i)),
						fn:normalize-space(substring($tmpName, $i + 1, 35)),
						"3"
					)
					else ()
				),
				(: Jeśli powyższa pętla zawiedzie, szukamy podziału na lewo od środka w $Length2 :)
				for $i in reverse(1 to $Length2)
				let $char := substring($tmpName, $i, 1)
				return
				(
					if ($char eq '.' or $char eq ',' or $char eq '-') then
					(
						fn:normalize-space(substring($tmpName, 1, $i)),
						fn:normalize-space(substring($tmpName, $i + 1, 35)),
						"4"
					)
					else ()
				),
				(: Jeśli powyższe pętle zawiodą, dzielimy napis na połowy :)	
				(
					fn:normalize-space(substring($tmpName, 1, $Length2)),
					fn:normalize-space(substring($tmpName, $Length2 + 1, 35)),
					"5"
				)
			)
			else (: ($Length2 >= 35) - szukamy podziału od pozycji 35 w lewo :) 
			(
				for $i in reverse(1 to 35)
				let $char := substring($tmpName, $i, 1)
				return
				(
					if ($char eq ' ') then
					(
						fn:normalize-space(substring($tmpName, 1, $i)),
						fn:normalize-space(substring($tmpName, $i + 1, 35)),
						"6"
					)
					else ()
				),
				for $i in reverse(1 to 35)
				let $char := substring($tmpName, $i, 1)
				return
				(
					if ($char eq '.' or $char eq ',' or $char eq '-') then
					(
						fn:normalize-space(substring($tmpName, 1, $i)),
						fn:normalize-space(substring($tmpName, $i + 1, 35)),
						"7"
					)
					else ()
				),				
				(: Jeśli powyższa pętla zawiedzie, dzielimy napis na połowy :)					
				(
					fn:normalize-space(substring($tmpName, 1, 35)),
					fn:normalize-space(substring($tmpName, 36, 35)),
					"8"
				)
			)
		)
		else (: ($Length &lt;= 35) - wszystko wrzucamy do ownerName :)
		(
			$tmpName,
			"",
			"9"
		)
	)
};

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getAccountDetailsResponse($fML321 as element(ns3:FML32),
												$PTGetPrBranchResponse as element(),
												$CISGetCustomerResponse as element())
    as element(ns6:invokeResponse)
{
	&lt;ns6:invokeResponse>
		&lt;ns6:tranAccountACA>
			&lt;ns7:TranAccountACA?>
				&lt;ns7:limitAmount?>{
					data($fML321/ns3:NF_TRAACA_LIMITAMOUNT)
				}&lt;/ns7:limitAmount>
			&lt;/ns7:TranAccountACA>
		&lt;/ns6:tranAccountACA>

        &lt;ns6:txnAccountData>
            &lt;ns5:TxnAccountData?>
                &lt;ns5:accountName?>{ if (data($PTGetPrBranchResponse/ns3:PT_POLISH_NAME))
                                       then 
										data($PTGetPrBranchResponse/ns3:PT_POLISH_NAME)
                                       else (
                                       		 if (data($PTGetPrBranchResponse/ns3:PT_POLISH_GROUP_NAME))
                                           	 then 
											  data($PTGetPrBranchResponse/ns3:PT_POLISH_GROUP_NAME)
                                             else (               
                                             	  if (data($PTGetPrBranchResponse/ns3:PT_POLISH_CATEGORY_NAME))
                                           		  then 
													data($PTGetPrBranchResponse/ns3:PT_POLISH_CATEGORY_NAME)
                                                  else (               
                                                  	data($fML321/ns3:NF_TXNAD_ACCOUNTNAME)
                                                      	)       
                                                  )
                                              )
		}&lt;/ns5:accountName>
				
                
            (:~    &lt;ns5:accountName?>{
			 :		data($PTGetPrBranchResponse/ns3:PT_POLISH_NAME)
			 :	}&lt;/ns5:accountName>
			 :)	
			 
			(: v1.7
			 : typ rachunku
			 : IS --> W
			 :)
                &lt;ns5:accountSpecificType?>{
                if (data($fML321/ns3:NF_TXNAD_ACCOUNTSPECIFICTY) eq 'IS')
                then
                	'W'
                else
					data($fML321/ns3:NF_TXNAD_ACCOUNTSPECIFICTY)
				}&lt;/ns5:accountSpecificType>
			(: v1.11 - zamiana przecinka na kropkę w liczbach :)
                &lt;ns5:currentBalance?>{
                	fn:replace(data($fML321/ns3:NF_TXNAD_CURRENTBALANCE), ',', '.')
                }&lt;/ns5:currentBalance>
                
                &lt;ns5:availableBalance?>{
					fn:replace(data($fML321/ns3:NF_TXNAD_AVAILABLEBALANCE), ',', '.')
				}&lt;/ns5:availableBalance>
				
				{
					let $ownerCIF := data($fML321/ns3:NF_TXNAD_OWNERCIF)
					return
						(:
						 : dociągnięte dane z CISGetCustomer w przypadku,
						 : gdy zwrócono CIFa
						 :)
						if ($ownerCIF) then
						(
							&lt;ns5:ownerCIF>{
								$ownerCIF
							}&lt;/ns5:ownerCIF>,

							(:
							 : imię i nazwisko dla klientów fizycznych
							 : nazwa pelna dla klientów prawnych
							 :)
							let $customerType := data($CISGetCustomerResponse/ns3:DC_TYP_KLIENTA)
							return
								if ($customerType eq 'F') then
								(
					                &lt;ns5:ownerName>{
										data($CISGetCustomerResponse/ns3:DC_IMIE)
									}&lt;/ns5:ownerName>,
									
					                &lt;ns5:ownerNameSecond>{
					                	data($CISGetCustomerResponse/ns3:DC_NAZWISKO)
					                }&lt;/ns5:ownerNameSecond>
								)
								else
								(
(: v1.10 - poprawna obsługa dla pustego pola CI_NAZWA_PELNA :)
									if (fn:string-length(data($CISGetCustomerResponse/ns3:CI_NAZWA_PELNA)) > 0)
									then
									(
										let $Names := local:SplitName(data($CISGetCustomerResponse/ns3:CI_NAZWA_PELNA))
										return
										(
					            		    &lt;ns5:ownerName>{
												data($Names[1])	
											}&lt;/ns5:ownerName>,
											&lt;ns5:ownerNameSecond>{
												data($Names[2])
					               		 	}&lt;/ns5:ownerNameSecond>
					               	 	)
					               	 )
					               	 else
					               	 (
					               	 	&lt;ns5:ownerName/>,
					               	 	&lt;ns5:ownerNameSecond/>
					               	 )
								),
							
			                &lt;ns5:ownerAddress>{
								fn:concat(data($CISGetCustomerResponse/ns3:DC_ULICA_DANE_PODST),
										  ' ',
										  data($CISGetCustomerResponse/ns3:DC_NR_POSES_LOKALU_DANE_PODST))
							}&lt;/ns5:ownerAddress>,
							
			                &lt;ns5:ownerCity>{
								data($CISGetCustomerResponse/ns3:DC_MIASTO_DANE_PODST)
							}&lt;/ns5:ownerCity>,
							
			                &lt;ns5:ownerZipCode>{
								data($CISGetCustomerResponse/ns3:DC_KOD_POCZTOWY_DANE_PODST)
							}&lt;/ns5:ownerZipCode>
						)
						else
						(
							&lt;ns5:ownerCIF/>,
							
			                &lt;ns5:ownerName>{
								data($fML321/ns3:NF_TXNAD_OWNERNAME)
							}&lt;/ns5:ownerName>,
							
			                &lt;ns5:ownerNameSecond>{
								data($fML321/ns3:NF_TXNAD_OWNERNAMESECOND)
							}&lt;/ns5:ownerNameSecond>,
							
			                &lt;ns5:ownerAddress>{
								data($fML321/ns3:NF_TXNAD_OWNERADDRES)
							}&lt;/ns5:ownerAddress>,
							
			                &lt;ns5:ownerCity>{
								data($fML321/ns3:NF_TXNAD_OWNERCITY)
							}&lt;/ns5:ownerCity>,
							
			                &lt;ns5:ownerZipCode>{
								data($fML321/ns3:NF_TXNAD_OWNERZIPCODE)
							}&lt;/ns5:ownerZipCode>
						)	
				}
				
				{
					let $ownerDataEditable := data($fML321/ns3:NF_TXNAD_OWNERDATAEDITABLE)
					return
						if ($ownerDataEditable) then
			                &lt;ns5:ownerDataEditable>{
								xs:boolean($ownerDataEditable)
							}&lt;/ns5:ownerDataEditable>
						else
							()
				}

				{
					let $showBalance := data($fML321/ns3:NF_TXNAD_SHOWBALANCE)
					return
						if ($showBalance) then
			                &lt;ns5:showBalance>{
								xs:boolean($showBalance)
							}&lt;/ns5:showBalance>
						else
							()
				}
				
				&lt;ns5:productDefinition?>
					&lt;ns2:ProductDefinition?>
						&lt;ns2:idProductDefinition?>{
							data($PTGetPrBranchResponse/ns3:PT_ID_DEFINITION)
						}&lt;/ns2:idProductDefinition>
						&lt;ns2:productGroup?>
							&lt;ns2:ProductGroup?>
						    	&lt;ns2:idProductGroup?>{
						    		data($PTGetPrBranchResponse/ns3:PT_ID_GROUP)
						    	}&lt;/ns2:idProductGroup>
								&lt;ns2:productCategory?>
							        &lt;ns2:ProductCategory?>
										&lt;ns2:idProductCategory?>{
											data($PTGetPrBranchResponse/ns3:PT_ID_CATEGORY)
										}&lt;/ns2:idProductCategory>
										&lt;ns2:productArea?>
									        &lt;ns2:ProductArea?>
												&lt;ns2:idProductArea?>{
													data($PTGetPrBranchResponse/ns3:PT_ID_AREA)
												}&lt;/ns2:idProductArea>
											&lt;/ns2:ProductArea>
										&lt;/ns2:productArea>
									&lt;/ns2:ProductCategory>
								&lt;/ns2:productCategory>
							&lt;/ns2:ProductGroup>
						&lt;/ns2:productGroup>
					&lt;/ns2:ProductDefinition>
				&lt;/ns5:productDefinition>
									
                &lt;ns5:currency?>
                    &lt;ns4:CurrencyCode?>
                        &lt;ns4:currencyCode?>{
							data($fML321/ns3:NF_CURREC_CURRENCYCODE)
						}&lt;/ns4:currencyCode>
                    &lt;/ns4:CurrencyCode>
                &lt;/ns5:currency>
                       
                &lt;ns5:accountAccessType?>
                    &lt;ns0:AccessType?>
                        &lt;ns0:accessType?>{
							data($fML321/ns3:NF_ACCEST_ACCESSTYPE)
						}&lt;/ns0:accessType>
                    &lt;/ns0:AccessType>
                &lt;/ns5:accountAccessType>
                
                &lt;ns5:accountStatus?>
                    &lt;ns1:AccountStatus?>
                        &lt;ns1:accountStatus?>{
							data($fML321/ns3:NF_ACCOUS_ACCOUNTSTATUS)
						}&lt;/ns1:accountStatus>
                    &lt;/ns1:AccountStatus>
                &lt;/ns5:accountStatus>

			(: v1.8 - start
			:)	
				&lt;ns5:accountBranchNumber?>
					&lt;ns4:BranchCode?>
						&lt;ns4:branchCode?>{ 
							data($fML321/ns3:NF_BRANCC_BRANCHCODE)
						}&lt;/ns4:branchCode>
					&lt;/ns4:BranchCode>
				&lt;/ns5:accountBranchNumber>				
			(: v1.8 - koniec
			:)	                
            &lt;/ns5:TxnAccountData>
        &lt;/ns6:txnAccountData>
        
		&lt;ns6:timeAccount>
			&lt;ns7:TimeAccount?>
				&lt;ns7:holdAmount?>{
					data($fML321/ns3:NF_TIMEA_HOLDAMOUNT)
				}&lt;/ns7:holdAmount>
			&lt;/ns7:TimeAccount>
		&lt;/ns6:timeAccount>

		&lt;ns6:tranAccount>
			&lt;ns7:TranAccount?>
				&lt;ns7:holdAmount?>{
					data($fML321/ns3:NF_HOLD_HOLDAMOUNT)
				}&lt;/ns7:holdAmount>
			&lt;/ns7:TranAccount>
		&lt;/ns6:tranAccount>
	&lt;/ns6:invokeResponse>
};

&lt;soap-env:Body>{
	xf:getAccountDetailsResponse($fML321, $PTGetPrBranchResponse, $CISGetCustomerResponse)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>