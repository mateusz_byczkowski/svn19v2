<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:accountdict.dictionaries.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function insertDateTime($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>10 and not (substring($value,1,10) = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForHolds($parm as element(fml:FML32)) as element()
{

&lt;ns0:holds>
  {
    for $x at $occ in $parm/NF_HOLD_HOLDAMOUNT
    return
    &lt;ns6:Hold>
      &lt;ns6:holdAmount?>{data($parm/NF_HOLD_HOLDAMOUNT[$occ])}&lt;/ns6:holdAmount>
      { insertDate(data($parm/NF_HOLD_HOLDENTERDATE[$occ]),"yyyy-MM-dd","ns6:holdEnterDate")}
      &lt;ns6:holdDescription?>{data($parm/NF_HOLD_HOLDDESCRIPTION[$occ])}&lt;/ns6:holdDescription>
      { insertDate(data($parm/NF_HOLD_HOLDEXPIRATIONDATE[$occ]),"yyyy-MM-dd","ns6:holdExpirationDate")}
    
(:    { insertDateTime(data($parm/NF_HOLD_HOLDCREATEDATE[$occ]),"yyyy-MM-dd-HH.mm.ss","ns6:holdCreateDate")}
      { insertDateTime(data($parm/NF_HOLD_HOLDACCEPTDATE[$occ]),"yyyy-MM-dd-HH.mm.ss","ns6:holdAcceptDate")} :)

(: zamienione w rpg, ale zeby tam nie zmieniac - zmiana na szynie :)
      { insertDateTime(data($parm/NF_HOLD_HOLDACCEPTDATE[$occ]),"yyyy-MM-dd-HH.mm.ss","ns6:holdCreateDate")}
      { insertDateTime(data($parm/NF_HOLD_HOLDCREATEDATE[$occ]),"yyyy-MM-dd-HH.mm.ss","ns6:holdAcceptDate")}

      &lt;ns6:firstChecqueNumber?>{data($parm/NF_HOLD_FIRSTCHECQUENUMBER[$occ])}&lt;/ns6:firstChecqueNumber>
      &lt;ns6:lastChecqueNumber?>{data($parm/NF_HOLD_LASTCHECQUENUMBER[$occ])}&lt;/ns6:lastChecqueNumber>
      &lt;ns6:holdBalanceFlag?>{sourceValue2Boolean(data($parm/NF_HOLD_HOLDBALANCEFLAG[$occ]),"1")}&lt;/ns6:holdBalanceFlag>
      &lt;ns6:holdExpirationDateFlag?>{sourceValue2Boolean(data($parm/NF_HOLD_HOLDEXPDATFLA[$occ]),"0")}&lt;/ns6:holdExpirationDateFlag>
      { insertDate(data($parm/NF_HOLD_CHECQUERESTRICTDAT[$occ]),"yyyy-MM-dd","ns6:checqueRestrictDate")}
      &lt;ns6:holdCreateUserId?>{data($parm/NF_HOLD_HOLDCREATEUSERID[$occ])}&lt;/ns6:holdCreateUserId>
      &lt;ns6:holdAcceptUserId?>{data($parm/NF_HOLD_HOLDACCEPTUSERID[$occ])}&lt;/ns6:holdAcceptUserId>
      &lt;ns6:holdActivityFlag?>{sourceValue2Boolean(data($parm/NF_HOLD_HOLDSTAT[$occ] ),"A")}&lt;/ns6:holdActivityFlag>
      &lt;ns6:holdNumber?>{data($parm/NF_HOLD_HOLDNUMBER[$occ])}&lt;/ns6:holdNumber>
      &lt;ns6:holdDescription2?>{data($parm/NF_HOLD_HOLDDESCRIPTION2[$occ])}&lt;/ns6:holdDescription2>   
      &lt;ns6:recCount?>{data($parm/NF_HOLD_RECCOUNT[$occ])}&lt;/ns6:recCount>
      &lt;ns6:recType?>{data($parm/NF_HOLD_RECTYPE[$occ])}&lt;/ns6:recType>
      &lt;ns6:checqueRestrictTime?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_HOLD_CHECQUERESTRICTTIM[$occ]))}&lt;/ns6:checqueRestrictTime>
      &lt;ns6:holdFeeAmount?>{data($parm/NF_HOLD_HOLDFEEAMOUNT[$occ])}&lt;/ns6:holdFeeAmount>
      &lt;ns6:holdType>
        &lt;ns5:HoldType>
          &lt;ns5:holdType?>{data($parm/NF_HOLDT_HOLDTYPE[$occ])}&lt;/ns5:holdType>
        &lt;/ns5:HoldType>
      &lt;/ns6:holdType>
      &lt;ns6:holdCurrencyCode>
        &lt;ns5:CurrencyCode>
          &lt;ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode>
        &lt;/ns5:CurrencyCode>
      &lt;/ns6:holdCurrencyCode>
      &lt;ns6:holdActionCode>
        &lt;ns2:HoldActionCode>
          &lt;ns2:holdActionCode?>{data($parm/NF_HOLDAC_HOLDACTIONCODE[$occ])}&lt;/ns2:holdActionCode>
        &lt;/ns2:HoldActionCode>
      &lt;/ns6:holdActionCode>
    &lt;/ns6:Hold>
  }
&lt;/ns0:holds>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:bcd>
    &lt;ns3:BusinessControlData>
      &lt;ns3:pageControl>
        &lt;ns4:PageControl>
          &lt;ns4:hasNext>{data($parm/NF_PAGEC_HASNEXT)}&lt;/ns4:hasNext>
          &lt;ns4:navigationKeyDefinition>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns4:navigationKeyDefinition>
          &lt;ns4:navigationKeyValue>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns4:navigationKeyValue>
        &lt;/ns4:PageControl>
      &lt;/ns3:pageControl>
    &lt;/ns3:BusinessControlData>
  &lt;/ns0:bcd>
  {getElementsForHolds($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>