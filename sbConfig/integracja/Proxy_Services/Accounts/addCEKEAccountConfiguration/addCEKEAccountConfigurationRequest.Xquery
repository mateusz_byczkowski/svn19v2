<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:accounts.entities.be.dcl";
declare namespace urn3 = "urn:filtersandmessages.entities.be.dcl";

declare function xf:mapeNOwnAccNewRequest($req as element(urn:invoke),$head as element(urn:header))
	as element(fml:FML32) {

		&lt;fml:FML32>
			{
				&lt;fml:E_MSHEAD_MSGID>{data($head/urn:msgHeader/urn:msgId)}&lt;/fml:E_MSHEAD_MSGID>			
			}
			{
				&lt;fml:E_LOGIN_ID?>{ data($req/urn:customerCEKE/urn1:CustomerCEKE/urn1:nik) }&lt;/fml:E_LOGIN_ID>
			}
			{
				&lt;fml:E_ADMIN_MICRO_BRANCH?>{ data($req/urn:unitId) }&lt;/fml:E_ADMIN_MICRO_BRANCH>
			}
			{
				&lt;fml:U_USER_NAME?>{ data($req/urn:userId) }&lt;/fml:U_USER_NAME>
			}
			{
				for $ownAccountCEKE in $req/urn:accountsList/urn1:OwnAccountCEKE return
				(
					&lt;fml:B_DL_NR_RACH?>{ data($ownAccountCEKE/urn1:account/urn2:Account/urn2:accountNumber) }&lt;/fml:B_DL_NR_RACH>,
					&lt;fml:E_SEQ_NO?>{ data($ownAccountCEKE/urn1:seqNum) }&lt;/fml:E_SEQ_NO>,
					&lt;fml:E_FEE_ALLOWED?>{ data($ownAccountCEKE/urn1:flagFeeAccount) }&lt;/fml:E_FEE_ALLOWED>,
					&lt;fml:B_TYP_RACH?>{ data($ownAccountCEKE/urn1:cekeAccountType) }&lt;/fml:B_TYP_RACH>
				)
			}


		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body>
{ xf:mapeNOwnAccNewRequest($body/urn:invoke,$header/urn:header) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>