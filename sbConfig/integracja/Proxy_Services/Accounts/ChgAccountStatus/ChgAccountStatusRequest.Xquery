<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapChgAccountStatusRequest($req as element(m:ChgAccountStatusRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:TrnId)
					then &lt;fml:DC_TRN_ID>{ data($req/m:TrnId) }&lt;/fml:DC_TRN_ID>
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK>{ data($req/m:Uzytkownik) }&lt;/fml:DC_UZYTKOWNIK>
					else ()
			}
			{
				if($req/m:Oddzial)
					then &lt;fml:DC_ODDZIAL>{ data($req/m:Oddzial) }&lt;/fml:DC_ODDZIAL>
					else ()
			}
			{
				if($req/m:RodzajRachunku)
					then &lt;fml:DC_RODZAJ_RACHUNKU>{ data($req/m:RodzajRachunku) }&lt;/fml:DC_RODZAJ_RACHUNKU>
					else ()
			}
			{
				if($req/m:NrRachunku)
					then &lt;fml:DC_NR_RACHUNKU>{ data($req/m:NrRachunku) }&lt;/fml:DC_NR_RACHUNKU>
					else ()
			}
			{
				if($req/m:StatusRachunku)
					then &lt;fml:DC_STATUS_RACHUNKU>{ data($req/m:StatusRachunku) }&lt;/fml:DC_STATUS_RACHUNKU>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapChgAccountStatusRequest($body/m:ChgAccountStatusRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>