<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{

if(string-length($parm) >0 )then
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
else
   $trueval
};


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};


declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
   let $length:= string-length($sourceString)
   let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
   let $accountNumber :=  substring($sourceString, $startPos)
   return
     $accountNumber
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{
(:
&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,:)
&lt;DC_ODDZIAL?>{chkUnitId(data($parm/ns0:msgHeader/ns0:unitId))}&lt;/DC_ODDZIAL>
,
&lt;DC_UZYTKOWNIK?>{concat("SKP:",data($parm/ns0:msgHeader/ns0:userId))}&lt;/DC_UZYTKOWNIK>
(:,
&lt;NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
:),
&lt;DC_TRN_ID?>{data($parm/ns0:transHeader/ns0:transId)}&lt;/DC_TRN_ID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
&lt;DC_NR_RACHUNKU?>{cutStr(data($parm/ns0:account/ns3:Account/ns3:accountNumber),10)}&lt;/DC_NR_RACHUNKU>
,
&lt;DC_IMIE_I_NAZWISKO_ALT?>{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:name1)}&lt;/DC_IMIE_I_NAZWISKO_ALT>
,
&lt;DC_IMIE_I_NAZWISKO_ALT_C_D?>{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:name2)}&lt;/DC_IMIE_I_NAZWISKO_ALT_C_D>
,
&lt;DC_ULICA_ADRES_ALT?>{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:street)}&lt;/DC_ULICA_ADRES_ALT>
,
&lt;DC_NR_POSES_LOKALU_ADRES_ALT?>{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:houseFlatNumber)}&lt;/DC_NR_POSES_LOKALU_ADRES_ALT>
,
&lt;DC_MIASTO_ADRES_ALT?>{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:city)}&lt;/DC_MIASTO_ADRES_ALT>
,
&lt;DC_WOJEWODZTWO_KRAJ_ADRES_ALT?>{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:stateCountry)}&lt;/DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
,
&lt;DC_KOD_POCZTOWY_ADRES_ALT>{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:zipCode)}&lt;/DC_KOD_POCZTOWY_ADRES_ALT>
,
(:&lt;DC_TYP_ADRESU?>{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:accountAddressType)}&lt;/DC_TYP_ADRESU>:)
if ($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:accountAddressType)
	then if (data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:accountAddressType) = "Z")
		then &lt;DC_TYP_ADRESU>1&lt;/DC_TYP_ADRESU>
		else &lt;DC_TYP_ADRESU>0&lt;/DC_TYP_ADRESU>
else()
,
&lt;DC_DATA_WPROWADZENIA?>{fn-bea:date-to-string-with-format("yyyy-MM-dd",data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:validFrom))}&lt;/DC_DATA_WPROWADZENIA>
,
&lt;DC_DATA_KONCOWA?>{fn-bea:date-to-string-with-format("yyyy-MM-dd",data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:validTo))}&lt;/DC_DATA_KONCOWA>
,
&lt;DC_KASOWAC_PRZY_WYGASNIECIU?>{boolean2SourceValue(data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:deleteWhenExpired),"1","0")}&lt;/DC_KASOWAC_PRZY_WYGASNIECIU>
,
&lt;DC_TYP_ZMIANY>A&lt;/DC_TYP_ZMIANY>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>