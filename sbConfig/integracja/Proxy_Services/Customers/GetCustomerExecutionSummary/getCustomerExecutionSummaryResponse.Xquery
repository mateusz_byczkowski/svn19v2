<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:cif.entities.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace ns4="urn:executions.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string
{
    if ($parm  =$trueval)
       then "true"
       else "false"
};





declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns3:invokeResponse>
  &lt;ns3:totalExecution>
    &lt;ns4:TotalExecution>
      &lt;ns4:totalExecutionAmount?>{data($parm/NF_TOTALE_TOTALEXECUTIONAM)}&lt;/ns4:totalExecutionAmount>
      &lt;ns4:freeAmountToUse?>{data($parm/NF_TOTALE_FREEAMOUNTTOUSE)}&lt;/ns4:freeAmountToUse>
      &lt;ns4:freeAmountAvailable?>{sourceValue2Boolean(data($parm/NF_TOTALE_FREEAMOUNTAVAILA),"1")}&lt;/ns4:freeAmountAvailable>
      &lt;ns4:freeAmount?>{data($parm/NF_TOTALE_FREEAMOUNT)}&lt;/ns4:freeAmount>
      &lt;ns4:incomingBalance?>{data($parm/NF_TOTALE_INCOMINGBALANCE)}&lt;/ns4:incomingBalance>
      &lt;ns4:outgoingBalance?>{data($parm/NF_TOTALE_OUTGOINGBALANCE)}&lt;/ns4:outgoingBalance>
      &lt;ns4:realizationAmount?>{data($parm/NF_TOTALE_REALIZATIONAMOUN)}&lt;/ns4:realizationAmount>
    &lt;/ns4:TotalExecution>
  &lt;/ns3:totalExecution>
&lt;/ns3:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>