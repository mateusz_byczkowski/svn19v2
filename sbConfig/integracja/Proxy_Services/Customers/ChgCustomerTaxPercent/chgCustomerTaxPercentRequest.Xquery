<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:cif.entities.be.dcl";
declare namespace e2 = "urn:dictionaries.be.dcl";


declare function local:mapChgCustomerTaxPercentRequest($req as element(), $head as element())
    as element(fml:FML32) {
        &lt;fml:FML32>
        	&lt;fml:DC_NUMER_KLIENTA>{ data($req/m:customer/e:Customer/e:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA>
        	&lt;fml:CI_PROCENT_PODATKU>{ data($req/m:customer/e:Customer/e:customerFinancialInfo/e:CustomerFinancialInfo/e:taxPercent)}&lt;/fml:CI_PROCENT_PODATKU>
(:       	&lt;fml:CI_PROCENT_PODATKU>{ data($req/m:customer/e:Customer/e:customerFinancialInfo/e:CustomerFinancialInfo/e:taxPercent)}&lt;/fml:CI_PROCENT_PODATKU>:)
        	&lt;fml:DC_TYP_KLIENTA>{ data($req/m:customer/e:Customer/e:customerType/e2:CustomerType/e2:customerType) }&lt;/fml:DC_TYP_KLIENTA>
        	&lt;fml:CI_PRACOWNIK_ZMIEN>{ data($head/m:msgHeader/m:userId) }&lt;/fml:CI_PRACOWNIK_ZMIEN>
        	&lt;fml:CI_ID_SPOLKI>{ data($head/m:msgHeader/m:companyId) }&lt;/fml:CI_ID_SPOLKI>
        &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body>
{ local:mapChgCustomerTaxPercentRequest($body/m:invoke, $header/m:header) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>