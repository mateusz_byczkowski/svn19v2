<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-12-15</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:customercertificatedict.dictionaries.be.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:customercertificates.entities.be.dcl";
declare namespace ns8="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns6:msgHeader/ns6:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns6:msgHeader/ns6:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns6:msgHeader/ns6:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns6:msgHeader/ns6:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns6:msgHeader/ns6:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns6:msgHeader/ns6:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns6:transHeader/ns6:transId)}&lt;/NF_TRHEAD_TRANSID>
};


declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{

&lt;NF_CUSTCS_CUSTOMERCERTIFIC?>{data($parm/ns6:CustomerCertificateStatus/ns0:CustomerCertificateStatus/ns0:customerCertificateStatus)}&lt;/NF_CUSTCS_CUSTOMERCERTIFIC>
,
&lt;NF_DATEH_VALUE?>{data($parm/ns6:dateTo/ns8:DateHolder/ns8:value)}&lt;/NF_DATEH_VALUE>
,
&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
&lt;NF_PAGEC_REVERSEORDER?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:reverseOrder)}&lt;/NF_PAGEC_REVERSEORDER>
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
,
&lt;NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns6:customer/ns1:Customer/ns1:customerNumber)}&lt;/NF_CUSTOM_CUSTOMERNUMBER>
,
&lt;NF_DATEH_VALUE?>{data($parm/ns6:dateFrom/ns8:DateHolder/ns8:value)}&lt;/NF_DATEH_VALUE>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvoke($body/ns6:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>