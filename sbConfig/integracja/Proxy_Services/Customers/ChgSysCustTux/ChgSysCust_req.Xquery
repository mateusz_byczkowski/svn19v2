<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf="http://bzwbk.com/services/prime/functions";

declare variable $body external;
declare variable $fml as element(FML32):=$body/FML32;



declare function xf:mapTimeStamp($timestamp as xs:string) as xs:string 
{
       fn:concat (fn:substring($timestamp,1,10),"T",
                          fn:substring($timestamp,12,2),":",
                          fn:substring($timestamp,15,2),":",
                          fn:substring($timestamp,18))
};


&lt;soap-env:Body xmlns:soap-env = "http://schemas.xmlsoap.org/soap/envelope/"> 
       &lt;prim:SetCustomerDetails xmlns:prim="http://bzwbk.com/services/prime/">
            &lt;prim:customer xmlns:prim1="http://schemas.datacontract.org/2004/07/PrimeCustomerLibrary.Structures">
                {if  ($fml/CI_CZAS_AKTUALIZACJI) then
                    &lt;prim1:CiCzasAktualizacji>{xf:mapTimeStamp(data($fml/CI_CZAS_AKTUALIZACJI))}&lt;/prim1:CiCzasAktualizacji>
                  else ()}
                &lt;prim1:DcNumerKlienta?>{data($fml/DC_NUMER_KLIENTA)}&lt;/prim1:DcNumerKlienta>
                &lt;prim1:CiIdPortfela?>{data($fml/CI_ID_PORTFELA)}&lt;/prim1:CiIdPortfela>
                &lt;prim1:CiIdPortfelaZastepcy?>{data($fml/CI_ID_PORTFELA_ZASTEPCY)}&lt;/prim1:CiIdPortfelaZastepcy>
                &lt;prim1:CiIdPortfelaCbk?>{data($fml/CI_ID_PORTFELA_CBK)}&lt;/prim1:CiIdPortfelaCbk>
                &lt;prim1:DcSegmentMarketingowy?>{data($fml/DC_SEGMENT_MARKETINGOWY)}&lt;/prim1:DcSegmentMarketingowy>
                &lt;prim1:CiPodsegmentMark?>{data($fml/CI_PODSEGMENT_MARK)}&lt;/prim1:CiPodsegmentMark>
                &lt;prim1:CiKlasaObslugi?>{data($fml/CI_KLASA_OBSLUGI)}&lt;/prim1:CiKlasaObslugi>
                &lt;prim1:EntryData?>{data($fml/CI_DATA_WPROWADZENIA)}&lt;/prim1:EntryData>
                &lt;prim1:DcRezNierez?>{data($fml/DC_REZ_NIEREZ)}&lt;/prim1:DcRezNierez>
                &lt;prim1:CiSeriaNrDowodu?>{data($fml/DC_NR_DOWODU_REGON)}&lt;/prim1:CiSeriaNrDowodu>
                &lt;prim1:DcNrPesel?>{data($fml/DC_NR_PESEL)}&lt;/prim1:DcNrPesel>
                &lt;prim1:CiUdostepGrupa?>{data($fml/CI_UDOSTEP_GRUPA)}&lt;/prim1:CiUdostepGrupa>
                &lt;prim1:CiNrOddzialu?>{data($fml/CI_NR_ODDZIALU)}&lt;/prim1:CiNrOddzialu>
                &lt;prim1:DcNumerPaszportu?>{data($fml/DC_NUMER_PASZPORTU)}&lt;/prim1:DcNumerPaszportu>
                &lt;prim1:DcOdpowiedzi?>{data($fml/DC_ODPOWIEDZI)}&lt;/prim1:DcOdpowiedzi>
                &lt;prim1:DcPlec?>{data($fml/DC_PLEC)}&lt;/prim1:DcPlec>
                &lt;prim1:DcImie?>{data($fml/DC_IMIE)}&lt;/prim1:DcImie>
                &lt;prim1:DcDrugieImie?>{data($fml/DC_DRUGIE_IMIE)}&lt;/prim1:DcDrugieImie>
                &lt;prim1:DcNazwisko?>{data($fml/DC_NAZWISKO)}&lt;/prim1:DcNazwisko>
                &lt;prim1:DcDataUrodzenia?>{data($fml/DC_DATA_URODZENIA)}&lt;/prim1:DcDataUrodzenia>
                &lt;prim1:CiNazwaPracodawcy?>{data($fml/CI_NAZWA_PRACODAWCY)}&lt;/prim1:CiNazwaPracodawcy>
                &lt;prim1:CiStatusGiodo?>{data($fml/CI_STATUS_GIODO)}&lt;/prim1:CiStatusGiodo>
                &lt;prim1:CiObywatelstwo?>{data($fml/CI_OBYWATELSTWO)}&lt;/prim1:CiObywatelstwo>
                &lt;prim1:DcAdresEMail?>{data($fml/DC_ADRES_E_MAIL)}&lt;/prim1:DcAdresEMail>
                &lt;prim1:CiHasloWCc?>{data($fml/CI_HASLO_W_CC)}&lt;/prim1:CiHasloWCc>
                &lt;prim1:DcUlicaDanePodst?>{data($fml/DC_ULICA_DANE_PODST)}&lt;/prim1:DcUlicaDanePodst>
                &lt;prim1:DcNrPosesDanePodst?>{data($fml/DC_NR_POSES_LOKALU_DANE_PODST)}&lt;/prim1:DcNrPosesDanePodst>
                &lt;prim1:DcMiastoDanePodst?>{data($fml/DC_MIASTO_DANE_PODST)}&lt;/prim1:DcMiastoDanePodst>
                &lt;prim1:DcKodPocztowyDanePodst?>{data($fml/DC_KOD_POCZTOWY_DANE_PODST)}&lt;/prim1:DcKodPocztowyDanePodst>
                &lt;prim1:CiKodKrajuDanePodst?>{data($fml/CI_KOD_KRAJU)}&lt;/prim1:CiKodKrajuDanePodst>
                &lt;prim1:CiUlicaPracodawcy?>{data($fml/CI_ULICA_PRACODAWCY)}&lt;/prim1:CiUlicaPracodawcy>
                &lt;prim1:CiNumerPosesjiPrac?>{data($fml/CI_NUMER_POSESJI_PRAC)}&lt;/prim1:CiNumerPosesjiPrac>
                &lt;prim1:CiMiastoPrac?>{data($fml/CI_MIASTO_PRACODAWCY)}&lt;/prim1:CiMiastoPrac>
                &lt;prim1:CiKodPocztowyPrac?>{data($fml/CI_KOD_POCZT_PRAC)}&lt;/prim1:CiKodPocztowyPrac>
                &lt;prim1:CiKodKrajuPrac?>{data($fml/CI_KOD_KRAJU_PRAC)}&lt;/prim1:CiKodKrajuPrac>
                &lt;prim1:DcUlicaAdresAlt?>{data($fml/DC_ULICA_ADRES_ALT)}&lt;/prim1:DcUlicaAdresAlt>
                &lt;prim1:DcNrPosesAdresAlt?>{data($fml/DC_NR_POSES_LOKALU_ADRES_ALT)}&lt;/prim1:DcNrPosesAdresAlt>
                &lt;prim1:DcMiastoAdresAlt?>{data($fml/DC_MIASTO_ADRES_ALT)}&lt;/prim1:DcMiastoAdresAlt>
                &lt;prim1:DcKodPocztowyAdresAlt?>{data($fml/DC_KOD_POCZTOWY_ADRES_ALT)}&lt;/prim1:DcKodPocztowyAdresAlt>
                &lt;prim1:CiKodKrajuKoresp?>{data($fml/CI_KOD_KRAJU_KORESP)}&lt;/prim1:CiKodKrajuKoresp>
                &lt;prim1:CiDataOd?>{data($fml/CI_DATA_OD)}&lt;/prim1:CiDataOd>
                &lt;prim1:CiDataDo?>{data($fml/CI_DATA_DO)}&lt;/prim1:CiDataDo>
                &lt;prim1:CiTimestampCis?>{data($fml/CI_TIMESTAMP_CIS)}&lt;/prim1:CiTimestampCis>
                &lt;prim1:DcTrnId?>{data($fml/DC_TRN_ID)}&lt;/prim1:DcTrnId>
                &lt;prim1:CiPracownikZmien?>{data($fml/CI_PRACOWNIK_ZMIEN)}&lt;/prim1:CiPracownikZmien>
                &lt;prim1:NrZrodlKlienta?>{data($fml/CI_NR_ZRODLOWY_KLIENTA)}&lt;/prim1:NrZrodlKlienta>
                &lt;prim1:DcDataSmierci?>{data($fml/DC_DATA_SMIERCI)}&lt;/prim1:DcDataSmierci>
                &lt;prim1:DcNrTelefKomorkowego?>{data($fml/DC_NR_TELEF_KOMORKOWEGO)}&lt;/prim1:DcNrTelefKomorkowego>
                &lt;prim1:CiZgodaInfHandlowa?>{data($fml/CI_ZGODA_INF_HANDLOWA)}&lt;/prim1:CiZgodaInfHandlowa>
           &lt;/prim:customer>
       &lt;/prim:SetCustomerDetails>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>