<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>T50787 kadaVersion.$1.2011-04-07</con:description>
  <con:xquery>declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:dictionaries.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace cer="http://bzwbk.com/services/cerber";
declare variable $body as element(soap:Body) external;
declare variable $cerberBody as element(cer:SearchUsersResponse) external;
declare variable $getCustCertBody as element(FML32) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)>5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
};


declare function chgDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
 
    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)
 
    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 
};

declare function sourceValue2Boolean ($parm as xs:string?,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForCustomerEmploymentInfoList($parm as element(fml:FML32)) as element()
{

&lt;ns2:customerEmploymentInfoList>
  {
    for $x at $occ in $parm/DC_WYKONYWANA_PRACA

    return
    &lt;ns2:CustomerEmploymentInfo>
      &lt;ns2:employment>{data($parm/DC_WYKONYWANA_PRACA[occ])}&lt;/ns2:employment>
    &lt;/ns2:CustomerEmploymentInfo>
  }
&lt;/ns2:customerEmploymentInfoList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32), $cerRes as element(cer:SearchUsersResponse), $getCustCert as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  &lt;ns4:documentsOut>
    &lt;ns2:Document>
      &lt;ns2:documentNumber>{data($parm/CI_SERIA_NR_DOK)}&lt;/ns2:documentNumber>
      &lt;ns2:primaryDocument>{sourceValue2Boolean (data($parm/CI_DOK_TOZSAMOSCI),"1")}&lt;/ns2:primaryDocument>
    &lt;/ns2:Document>
  &lt;/ns4:documentsOut>
  &lt;ns4:customerOut>
    &lt;ns2:Customer>
      &lt;ns2:companyName>{data($parm/CI_NAZWA_PELNA)}&lt;/ns2:companyName>
      &lt;ns2:customerNumber>{data($parm/DC_NUMER_KLIENTA)}&lt;/ns2:customerNumber>
      { insertDate(data($parm/CI_DATA_WPROWADZENIA),"yyyy-MM-dd","ns2:dateCustomerOpened")}
      &lt;ns2:resident>{sourceValue2Boolean (data($parm/DC_REZ_NIEREZ),"1")}&lt;/ns2:resident>
      &lt;ns2:taxID>{data($parm/DC_NIP)}&lt;/ns2:taxID>
      &lt;ns2:shortName>{data($parm/DC_NAZWA_SKROCONA)}&lt;/ns2:shortName>
      &lt;ns2:certificate>{sourceValue2Boolean (data($getCustCert/NF_CUSTOM_CERTIFICATE),"1")}&lt;/ns2:certificate>
      &lt;ns2:phoneNo>{data($parm/DC_NR_TELEFONU)}&lt;/ns2:phoneNo>
      &lt;ns2:mobileNo>{data($parm/DC_NR_TELEF_KOMORKOWEGO)}&lt;/ns2:mobileNo>
      &lt;ns2:email>{data($parm/DC_ADRES_E_MAIL)}&lt;/ns2:email>
      &lt;ns2:portfolio>{fn:concat(data($cerRes/cer:User/Name), ' ', data($cerRes/cer:User/Surname))}&lt;/ns2:portfolio>
      &lt;ns2:fax>{data($parm/DC_NUMER_FAKSU)}&lt;/ns2:fax>
      &lt;ns2:debtorTaxID>{data($getCustCert/NF_DIRECD_DEBTORTAXID)}&lt;/ns2:debtorTaxID>
      {getElementsForCustomerEmploymentInfoList($parm)}

      { 
      if (data($parm/DC_TYP_KLIENTA)= 'P') then
      &lt;ns2:customerFirm>
        &lt;ns2:CustomerFirm>
          &lt;ns2:numberOfEmployees?>{data($parm/DC_LICZBA_ZATRUDNIONYCH)}&lt;/ns2:numberOfEmployees>
          &lt;ns2:regon?>{data($parm/DC_NR_DOWODU_REGON)}&lt;/ns2:regon>
          &lt;ns2:krs?>{data($parm/CI_KRS)}&lt;/ns2:krs>
          &lt;ns2:legalForm>
            &lt;ns1:CustomerLegalForm>
              &lt;ns1:customerLegalForm>{data($parm/CI_FOP)}&lt;/ns1:customerLegalForm>
            &lt;/ns1:CustomerLegalForm>
          &lt;/ns2:legalForm>
        &lt;/ns2:CustomerFirm>
      &lt;/ns2:customerFirm>
      else ()
      }
      &lt;ns2:customerFinancialInfo>
        &lt;ns2:CustomerFinancialInfo>
          &lt;ns2:taxPercent>{data($parm/CI_PROCENT_PODATKU)}&lt;/ns2:taxPercent>
        &lt;/ns2:CustomerFinancialInfo>
      &lt;/ns2:customerFinancialInfo>

       { 
        if (data($parm/DC_TYP_KLIENTA)= 'F') then
      &lt;ns2:customerPersonal>
        &lt;ns2:CustomerPersonal>
         &lt;ns2:customerNumber?>{data($parm/DC_NUMER_KLIENTA)}&lt;/ns2:customerNumber>
         &lt;ns2:workInBankStatus?>{sourceValue2Boolean(data($parm/DC_PRACOWNIK_BANKU),"1") }&lt;/ns2:workInBankStatus>
         &lt;ns2:birthPlace?>{data($parm/DC_MIEJSCE_URODZENIA)}&lt;/ns2:birthPlace>
         &lt;ns2:motherMaidenName?>{data($parm/DC_NAZWISKO_PANIENSKIE_MATKI)}&lt;/ns2:motherMaidenName>
         &lt;ns2:fatherName?>{data($parm/DC_IMIE_OJCA)}&lt;/ns2:fatherName>
         &lt;ns2:numberOfPersonsHousehold?>{data($parm/DC_LICZBA_OS_WE_WSP_GOSP_D)}&lt;/ns2:numberOfPersonsHousehold>
         &lt;ns2:workplace?>{data($parm/CI_NAZWA_PRACODAWCY)}&lt;/ns2:workplace>
         { insertDate(data($parm/DC_DATA_URODZENIA),"yyyy-MM-dd","ns2:dateOfBirth")}
         &lt;ns2:lastName>{data($parm/DC_NAZWISKO)}&lt;/ns2:lastName>
         &lt;ns2:secondName?>{data($parm/DC_DRUGIE_IMIE)}&lt;/ns2:secondName>
         &lt;ns2:pesel?>{data($parm/DC_NR_PESEL)}&lt;/ns2:pesel>
         &lt;ns2:firstName>{data($parm/DC_IMIE)}&lt;/ns2:firstName>
         { insertDate(data($parm/DC_DATA_SMIERCI),"yyyy-MM-dd","ns2:dateOfDeath")}
         &lt;ns2:identityCardNumber?>{data($parm/DC_NR_DOWODU_REGON)}&lt;/ns2:identityCardNumber>
         &lt;ns2:passportNumber?>{data($parm/DC_NUMER_PASZPORTU)}&lt;/ns2:passportNumber>
         &lt;ns2:citizenship>
           &lt;ns1:CitizenshipCode>
             (:&lt;ns1:citizenshipCode?>{data($parm/CI_OBYWATELSTWO)}&lt;/ns1:citizenshipCode>:)(:T48111:)
             &lt;ns1:citizenshipCode?>{data($parm/CI_SYMBOL_KRAJU)}&lt;/ns1:citizenshipCode>(:CR140:)
             &lt;ns1:isoCitizenshipCode?>{data($parm/CI_OBYWATELSTWO)}&lt;/ns1:isoCitizenshipCode> (:T48111:)
           &lt;/ns1:CitizenshipCode>
         &lt;/ns2:citizenship>
        &lt;/ns2:CustomerPersonal>
      &lt;/ns2:customerPersonal>
      else ()
      }

      &lt;ns2:branchOfOwnership>
        &lt;ns1:BranchCode>
          &lt;ns1:branchCode?>{data($parm/DC_NUMER_ODDZIALU)}&lt;/ns1:branchCode>
        &lt;/ns1:BranchCode>
      &lt;/ns2:branchOfOwnership>

      &lt;ns2:customerType>
        &lt;ns1:CustomerType>
          &lt;ns1:customerType?>{data($parm/DC_TYP_KLIENTA)}&lt;/ns1:customerType>
        &lt;/ns1:CustomerType>
      &lt;/ns2:customerType>

      &lt;ns2:customerSegment>
        &lt;ns1:CustomerSegment>
          &lt;ns1:customerSegment>{data($parm/CI_KLASA_OBSLUGI)}&lt;/ns1:customerSegment>
        &lt;/ns1:CustomerSegment>
      &lt;/ns2:customerSegment>

      &lt;ns2:processingApproval>
        &lt;ns1:CustomerProcessingApproval>
          &lt;ns1:customerProcessingApproval>{data($parm/CI_UDOSTEP_GRUPA)}&lt;/ns1:customerProcessingApproval>
        &lt;/ns1:CustomerProcessingApproval>
      &lt;/ns2:processingApproval>

      &lt;ns2:spw>
        &lt;ns1:SpwCode>
          &lt;ns1:spwCode?>{data($parm/DC_SPW)}&lt;/ns1:spwCode>
        &lt;/ns1:SpwCode>
      &lt;/ns2:spw>

      &lt;ns2:customerLegalInfo>
        &lt;ns2:CustomerLegalInfo>
          &lt;ns2:registryNo?>{data($parm/CI_NUMER_W_REJESTRZE)}&lt;/ns2:registryNo>
          &lt;ns2:registryName?>{data($parm/CI_NAZWA_ORG_REJESTR)}&lt;/ns2:registryName>
          &lt;ns2:initialCapital?>{data($parm/CI_KAPITAL_ZALOZYCIELSKI)}&lt;/ns2:initialCapital>
          &lt;ns2:initialCapitalPaid?>{data($parm/CI_KAPITAL_ZALOZ_OPLACONY)}&lt;/ns2:initialCapitalPaid>
        &lt;/ns2:CustomerLegalInfo>
      &lt;/ns2:customerLegalInfo>

    &lt;/ns2:Customer>
  &lt;/ns4:customerOut>
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32, $cerberBody, $getCustCertBody)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>