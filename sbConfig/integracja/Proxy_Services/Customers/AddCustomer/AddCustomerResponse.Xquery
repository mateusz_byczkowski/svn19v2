<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbICBSAddCustomerResponse($fml as element(fml:FML32))
	as element(m:bICBSAddCustomerResponse) {
		&lt;m:bICBSAddCustomerResponse>
			{
				if($fml/fml:DC_NUMER_KLIENTA)
					then &lt;m:NumerKlienta>{ data($fml/fml:DC_NUMER_KLIENTA) }&lt;/m:NumerKlienta>
					else ()
			}
			{
				if($fml/fml:DC_OPIS_BLEDU)
					then &lt;m:OpisBledu>{ data($fml/fml:DC_OPIS_BLEDU) }&lt;/m:OpisBledu>
					else ()
			}
		&lt;/m:bICBSAddCustomerResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbICBSAddCustomerResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>