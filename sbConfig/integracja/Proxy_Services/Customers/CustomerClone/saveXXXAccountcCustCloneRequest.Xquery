<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace dcl="urn:be.services.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace fml = "";


declare function local:mapcCustCloneRequest($req as element(dcl:invoke))
	as element(fml:FML32) {
		&lt;fml:FML32>
			&lt;fml:DC_NUMER_KLIENTA?>{data($req/dcl:account/ns1:Account/ns1:customerNumber)}&lt;/fml:DC_NUMER_KLIENTA>
			&lt;fml:CI_ID_SYS>1&lt;/fml:CI_ID_SYS>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;

local:mapcCustCloneRequest($body/dcl:invoke)</con:xquery>
</con:xqueryEntry>