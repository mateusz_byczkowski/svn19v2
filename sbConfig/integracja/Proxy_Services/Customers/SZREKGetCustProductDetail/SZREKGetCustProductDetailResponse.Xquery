<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/esbmessages/";
declare namespace xf = "http://bzwbk.com/services/esbmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetCustProductDetailResponse($fml as element(fml:FML32))
	as element(m:getCustProductDetailResponse) {
		&lt;m:getCustProductDetailResponse>
			{
				if($fml/fml:PT_ID_CATEGORY)
					then &lt;m:ProductCategoryId>{ data($fml/fml:PT_ID_CATEGORY) }&lt;/m:ProductCategoryId>
					else ()
			}
			{
				if($fml/fml:NF_PRODAT_IDPRODUCTDEFINIT)
					then &lt;m:ProductId>{ data($fml/fml:NF_PRODAT_IDPRODUCTDEFINIT) }&lt;/m:ProductId>
					else ()
			}
	(:		{
				if($fml/fml:CI_PRODUCT_CODE)
					then &lt;m:ProductCode>{ data($fml/fml:CI_PRODUCT_CODE) }&lt;/m:ProductCode>
					else ()
			}:)
			{
				if($fml/fml:PT_POLISH_NAME)
					then &lt;m:ProductNamePl>{ data($fml/fml:PT_POLISH_NAME) }&lt;/m:ProductNamePl>
					else ()
			}
			{
				if($fml/fml:PT_ENGLISH_NAME)
					then &lt;m:ProductNameEn>{ data($fml/fml:PT_ENGLISH_NAME) }&lt;/m:ProductNameEn>
					else ()
			}
	(:		{
				if($fml/fml:DC_NR_RACHUNKU)
					then &lt;m:NrRachunku>{ data($fml/fml:DC_NR_RACHUNKU) }&lt;/m:NrRachunku>
					else ()
			}:)
			{
				if($fml/fml:NF_DEBITC_CARDNBR and string-length(data($fml/fml:NF_DEBITC_CARDNBR))>1 )
				then &lt;m:NrRachunku>{ data($fml/fml:NF_DEBITC_CARDNBR) }&lt;/m:NrRachunku>
				else 	if ($fml/fml:NF_INSURA_ID and string-length(data($fml/fml:NF_INSURA_ID))>1)
				   		then &lt;m:NrRachunku>{ data($fml/fml:NF_INSURA_ID) }&lt;/m:NrRachunku>
					   	else &lt;m:NrRachunku?>{ data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER) }&lt;/m:NrRachunku>
			}
			{
				if($fml/fml:NF_DEBITC_VIRTUALCARDNBR)
					then &lt;m:NrAlternatywny>{ data($fml/fml:NF_DEBITC_VIRTUALCARDNBR) }&lt;/m:NrAlternatywny>
					else ()
			}
			{
				if($fml/fml:NF_IS_COOWNER)
					then &lt;m:Pojedynczy>{ data($fml/fml:NF_IS_COOWNER) }&lt;/m:Pojedynczy>
					else ()
			}
			{
				if($fml/fml:NF_ATTRPG_FIRSTPRODUCTFEAT)
					then &lt;m:ProductAtt1>{ data($fml/fml:NF_ATTRPG_FIRSTPRODUCTFEAT) }&lt;/m:ProductAtt1>
					else ()
			}
			{
				if($fml/fml:NF_ATTRPG_SECONDPRODUCTFEA)
					then &lt;m:ProductAtt2>{ data($fml/fml:NF_ATTRPG_SECONDPRODUCTFEA) }&lt;/m:ProductAtt2>
					else ()
			}
			{
				if($fml/fml:NF_ATTRPG_THIRDPRODUCTFEAT)
					then &lt;m:ProductAtt3>{ data($fml/fml:NF_ATTRPG_THIRDPRODUCTFEAT) }&lt;/m:ProductAtt3>
					else ()
			}
			{
				if($fml/fml:NF_ATTRPG_FOURTHPRODUCTFEA)
					then &lt;m:ProductAtt4>{ data($fml/fml:NF_ATTRPG_FOURTHPRODUCTFEA) }&lt;/m:ProductAtt4>
					else ()
			}
			{
				if($fml/fml:NF_ATTRPG_FIFTHPRODUCTFEAT)
					then &lt;m:ProductAtt5>{ data($fml/fml:NF_ATTRPG_FIFTHPRODUCTFEAT) }&lt;/m:ProductAtt5>
					else ()
			}
			{
				if($fml/fml:NF_ACCOUN_ALTERNATIVEADDRE and data($fml/fml:NF_ACCOUN_ALTERNATIVEADDRE) = "1")
					then &lt;m:RachunekAdrAlt>T&lt;/m:RachunekAdrAlt>
					else &lt;m:RachunekAdrAlt>N&lt;/m:RachunekAdrAlt>
			}
			{
				if($fml/fml:NF_ACCOUA_NAME1 or $fml/fml:NF_ACCOUA_NAME2)
					then &lt;m:ImieINazwiskoAlt>{concat(data($fml/fml:NF_ACCOUA_NAME1),data($fml/fml:NF_ACCOUA_NAME2)) }&lt;/m:ImieINazwiskoAlt>
					else()
			}
			{
				if($fml/fml:NF_ACCOUA_STREET)
					then &lt;m:UlicaAdresAlt>{ data($fml/fml:NF_ACCOUA_STREET) }&lt;/m:UlicaAdresAlt>
					else ()
			}
			{
				if($fml/fml:NF_ACCOUA_HOUSEFLATNUMBER)
					then &lt;m:NrPosesLokaluAdresAlt>{ data($fml/fml:NF_ACCOUA_HOUSEFLATNUMBER) }&lt;/m:NrPosesLokaluAdresAlt>
					else ()
			}
			{
				if($fml/fml:NF_ACCOUA_CITY)
					then &lt;m:MiastoAdresAlt>{ data($fml/fml:NF_ACCOUA_CITY) }&lt;/m:MiastoAdresAlt>
					else ()
			}
			{
				if($fml/fml:NF_ACCOUA_ZIPCODE)
					then &lt;m:KodPocztowyAdresAlt>{ data($fml/fml:NF_ACCOUA_ZIPCODE) }&lt;/m:KodPocztowyAdresAlt>
					else ()
			}
			{
				if($fml/fml:NF_ACCOUA_CITY)
					then &lt;m:WojewodztwoKrajAdresAlt>T&lt;/m:WojewodztwoKrajAdresAlt>
					else ()
			}
		&lt;/m:getCustProductDetailResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapgetCustProductDetailResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>