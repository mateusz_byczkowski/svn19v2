<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-03-28</con:description>
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn = "urn:be.services.dcl";

&lt;soap-env:Body>
    &lt;urn:invokeResponse />
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>