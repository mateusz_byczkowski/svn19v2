<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";
declare namespace ns4="urn:executions.entities.be.dcl";
declare namespace ns6="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

     declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };

declare function getElementsForExecutions($parm as element(fml:FML32)) as element()
{

&lt;ns0:executions>
  {
    for $x at $occ in $parm/NF_EXECUT_SIGNATURE
    return
    &lt;ns4:Execution>
      &lt;ns4:signature>{data($parm/NF_EXECUT_SIGNATURE[$occ])}&lt;/ns4:signature>
      &lt;ns4:executionNumberLPP>{data($parm/NF_EXECUT_EXECUTIONNUMBERL[$occ])}&lt;/ns4:executionNumberLPP> 
      { insertDate(data($parm/NF_EXECUT_INCOMINGDATE[$occ]),"yyyy-MM-dd","ns4:incomingDate")}
      { insertDate(data($parm/NF_EXECUT_STARTEXECUTIONDA[$occ]),"yyyy-MM-dd","ns4:startExecutionDate")}  
  &lt;/ns4:Execution>
  }
&lt;/ns0:executions>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  {getElementsForExecutions($parm)}
  &lt;ns0:bcd>
    &lt;ns3:BusinessControlData>
      &lt;ns3:pageControl>
        &lt;ns5:PageControl>
          &lt;ns5:hasNext>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns5:hasNext>
          &lt;ns5:navigationKeyDefinition>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns5:navigationKeyDefinition>
          &lt;ns5:navigationKeyValue>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns5:navigationKeyValue>
        &lt;/ns5:PageControl>
      &lt;/ns3:pageControl>
    &lt;/ns3:BusinessControlData>
  &lt;/ns0:bcd>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>