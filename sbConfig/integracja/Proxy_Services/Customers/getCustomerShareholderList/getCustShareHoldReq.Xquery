<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:cif.entities.be.dcl";
declare namespace xf="urn:be.services.dcl/mappings";

declare variable $header external;
declare variable $body external;

&lt;soap-env:Body>
    &lt;FML32>
        &lt;CI_ID_SPOLKI>{data($header/urn:header/urn:msgHeader/urn:companyId)}&lt;/CI_ID_SPOLKI>
        &lt;DC_NUMER_KLIENTA>{data($body/urn:invoke/urn:customer/urn1:Customer/urn1:customerNumber)}&lt;/DC_NUMER_KLIENTA>
    &lt;/FML32>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>