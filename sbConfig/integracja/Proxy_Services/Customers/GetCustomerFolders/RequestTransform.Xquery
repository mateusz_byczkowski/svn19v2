<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-06</con:description>
  <con:xquery>declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/GetCustomerFolders/RequestTransform2/";
declare namespace ns0 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns1 = "urn:entities.be.dcl";
declare namespace ns-1 = "urn:cif.entities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace ns3 = "urn:baseauxentities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:RequestTransform2($invoke1 as element(ns2:invoke))
    as element(ns0:CRMGetCustCorpsRequest) {
        let $invoke := $invoke1
        return
            &lt;ns0:CRMGetCustCorpsRequest>
                {
                    for $customerNumber in $invoke/ns2:customer/ns-1:Customer/ns-1:customerNumber
                    return
                        &lt;ns0:NumerKlienta>{ data($customerNumber) }&lt;/ns0:NumerKlienta>
                }
                {
                    for $value in $invoke/ns2:userCompanyTypeList/ns-1:CustomerFolders
                    return
                        &lt;ns0:IdSpolki>{ xs:short( data($value/ns-1:companyID/ns4:CompanyType/ns4:companyType) ) }&lt;/ns0:IdSpolki>
                }
            &lt;/ns0:CRMGetCustCorpsRequest>
};

declare variable $invoke1 as element(ns2:invoke) external;

&lt;soapenv:Body>
{
xf:RequestTransform2($invoke1)
}
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>