<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns4="urn:filtersandmessages.entities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function chgDate($value as xs:string?) as xs:string?
{
if ($value)
   then if(string-length($value)>5 and not ($value = "0001-01-01"))
       then
      let $day := substring($value,1,2)
      let $month := substring($value,4,2)
      let $year := substring($value, 7,4)
             return concat($year,concat("-",concat($month,concat("-",$day))))
else() 
      else()
              
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:employmentInfo>
    &lt;ns2:CustomerEmploymentInfo>
      &lt;ns2:employment>{data($parm/DC_WYKONYWANA_PRACA)}&lt;/ns2:employment>
      {
       if ($parm/DC_DATA_PODJECIA_PRACY) then
       insertDate(chgDate(data($parm/DC_DATA_PODJECIA_PRACY)),"yyyy-MM-dd","ns2:employmentStartDate")
      else ()
      }
      &lt;ns2:employerEKDCode>
        &lt;ns3:StandardIndustryCode>
          &lt;ns3:standardIndustryCode>{data($parm/DC_KOD_EKD_PRACODAW)}&lt;/ns3:standardIndustryCode>
        &lt;/ns3:StandardIndustryCode>
      &lt;/ns2:employerEKDCode>
      &lt;ns2:occupationCode>
        &lt;ns3:CustomerOccupationCode>
          &lt;ns3:customerOccupationCode>{data($parm/DC_KOD_ZAWODU)}&lt;/ns3:customerOccupationCode>
        &lt;/ns3:CustomerOccupationCode>
      &lt;/ns2:occupationCode>
      &lt;ns2:customerWorkerStatus>
        &lt;ns3:CustomerWorkerStatus>
          &lt;ns3:customerWorkerStatus?>{data($parm/DC_STATUS_PRACOWNIKA)}&lt;/ns3:customerWorkerStatus>
        &lt;/ns3:CustomerWorkerStatus>
      &lt;/ns2:customerWorkerStatus> 
    &lt;/ns2:CustomerEmploymentInfo>
  &lt;/ns0:employmentInfo>
  &lt;ns0:response>
    &lt;ns4:ResponseMessage>
         {
          if ($parm/DC_OPIS_BLEDU) then
                &lt;ns4:result?>false&lt;/ns4:result>
          else
             &lt;ns4:result?>true&lt;/ns4:result>
          }
     (: &lt;ns4:errorCode?>{data($parm/NF_RESPOM_ERRORCODE)}&lt;/ns4:errorCode> :)
      &lt;ns4:errorDescription?>{data($parm/DC_OPIS_BLEDU)}&lt;/ns4:errorDescription>
           
      
    &lt;/ns4:ResponseMessage>
  &lt;/ns0:response>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>