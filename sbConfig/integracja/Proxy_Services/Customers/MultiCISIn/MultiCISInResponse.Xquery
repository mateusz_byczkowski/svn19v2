<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapMultiCISInResponse($fml as element(fml:FML32))
	as element(m:MultiCISInResponse) {
		&lt;m:MultiCISInResponse>
			{
				let $CI_CIF_BACK := $fml/fml:CI_CIF_BACK return
					if ($CI_CIF_BACK)
						then &lt;m:CifBack>{ data ($CI_CIF_BACK) }&lt;/m:CifBack>
						else ()
			}
			{
				let $CI_NR_ODDZIALU := $fml/fml:CI_NR_ODDZIALU return
					if ($CI_NR_ODDZIALU)
						then &lt;m:NrOddzialu>{ data($CI_NR_ODDZIALU) }&lt;/m:NrOddzialu>
						else ()
			}
			{
				let $CI_NR_KOM := $fml/fml:CI_NR_KOM return
					if ($CI_NR_KOM)
						then &lt;m:NrKom>{ data($CI_NR_KOM) }&lt;/m:NrKom>
						else ()
			}
			{
				let $CIE_ID := $fml/fml:CIE_ID return
					if ($CIE_ID)
						then &lt;m:Id>{ data($CIE_ID) }&lt;/m:Id>
						else ()
			}
			{
				let $CI_TIMESTAMP_CIS := $fml/fml:CI_TIMESTAMP_CIS return
					if ($CI_TIMESTAMP_CIS)
						then &lt;m:TimestampCis>{ data($CI_TIMESTAMP_CIS) }&lt;/m:TimestampCis>
						else ()
			}
			{
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA return
					if ($CI_ID_PORTFELA)
						then &lt;m:IdPortfela>{ data($CI_ID_PORTFELA) }&lt;/m:IdPortfela>
						else ()
			}
			{
				let $CI_ID_PORTFELA_ZASTEPCY := $fml/fml:CI_ID_PORTFELA_ZASTEPCY return
					if ($CI_ID_PORTFELA_ZASTEPCY)
						then &lt;m:IdPortfelaZastepcy>{ data($CI_ID_PORTFELA_ZASTEPCY) }&lt;/m:IdPortfelaZastepcy>
						else ()
			}
			{
				let $CI_ID_PORTFELA_CBK := $fml/fml:CI_ID_PORTFELA_CBK return
					if ($CI_ID_PORTFELA_CBK)
						then &lt;m:IdPortfelaCbk>{ data($CI_ID_PORTFELA_CBK) }&lt;/m:IdPortfelaCbk>
						else ()
			}
			{
				let $DC_SEGMENT_MARKETINGOWY := $fml/fml:DC_SEGMENT_MARKETINGOWY return
					if ($DC_SEGMENT_MARKETINGOWY)
						then &lt;m:SegmentMarketingowy>{ data($DC_SEGMENT_MARKETINGOWY) }&lt;/m:SegmentMarketingowy>
						else ()
			}
			{
				let $CI_PODSEGMENT_MARK := $fml/fml:CI_PODSEGMENT_MARK return
					if ($CI_PODSEGMENT_MARK)
						then &lt;m:PodsegmentMark>{ data($CI_PODSEGMENT_MARK) }&lt;/m:PodsegmentMark>
						else ()
			}
			{
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI return
					if ($CI_KLASA_OBSLUGI)
						then &lt;m:KlasaObslugi>{ data($CI_KLASA_OBSLUGI) }&lt;/m:KlasaObslugi>
						else ()
			}
			{
				let $CI_DECYZJA := $fml/fml:CI_DECYZJA return
					if ($CI_DECYZJA)
						then &lt;m:Decyzja>{ data($CI_DECYZJA) }&lt;/m:Decyzja>
						else ()
			}
			{
				let $DC_URZEDNIK_1 := $fml/fml:DC_URZEDNIK_1 return
					if ($DC_URZEDNIK_1)
						then &lt;m:Urzednik1>{ data($DC_URZEDNIK_1) }&lt;/m:Urzednik1>
						else ()
			}
			{
				let $DC_URZEDNIK_2 := $fml/fml:DC_URZEDNIK_2 return
					if ($DC_URZEDNIK_2)
						then &lt;m:Urzednik2>{ data($DC_URZEDNIK_2) }&lt;/m:Urzednik2>
						else ()
			}
			{
				let $DC_URZEDNIK_3 := $fml/fml:DC_URZEDNIK_3 return
					if ($DC_URZEDNIK_3)
						then &lt;m:Urzednik3>{ data($DC_URZEDNIK_3) }&lt;/m:Urzednik3>
						else ()
			}
			{
				let $DC_NUMER_ODDZIALU := $fml/fml:DC_NUMER_ODDZIALU return
					if ($DC_NUMER_ODDZIALU)
						then &lt;m:NumerOddzialuCIS1>{ data($DC_NUMER_ODDZIALU) }&lt;/m:NumerOddzialuCIS1>
						else ()
			}
		&lt;/m:MultiCISInResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapMultiCISInResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>