<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$6.2011-08-09</con:description>
  <con:xquery>(:Change log
v.1.1  2009-07-10  PKLI  TEET 39294: zmiana mapowania pola accountType
CR85  2010-01-06  MMa  NP1913_2 - CR85
CR85  2010-01-21  PKLI  NP1913_2 - CR85 dodanie pól InsurancePolicyAcc.currency i InsurancePackageAcc.currency
v.1.0.4  2010-02-12  PKL   PT 58
v.1.2  2010-05-06 PKLI T47597 Brak obsługi numeru wniosku ubezpieczeń Streamline 
v.1.3 2010-12-27 PKLI NP2224 CR3/165 Dodanie pól accessType i accountTypeFlag
v.1.4 2011-04-26 PKLI NP2233 T56473 Dane rachunku karty kredytowej.
v.1.5 2011-07-01 PKLI NP1696_2 T51433 Włączenie filtra na obszar/kategorię w trybie wywołania z podanym rachunkiem.
v.1.6 2011-08-08 PKLI  Poprawka do wersji 1.5 Zamiana pola NF_PRODUA_CODEPRODUCTAREA na PT_CODE_PRODUCT_AREA
:)

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
(: declare namespace ns4="urn:errors.hlbsentities.be.dcl"; :) (: CR85 :)
declare namespace ns4="urn:filtersandmessages.entities.be.dcl"; (: CR85 :)
declare namespace ns6="urn:entities.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:be.services.dcl";
declare namespace ns9="urn:baseauxentities.be.dcl";
declare namespace ns10="urn:insurance.entities.be.dcl";
declare namespace ns11="urn:applicationul.entities.be.dcl";
declare namespace ns12="urn:uldictionary.dictionaries.be.dcl";
declare namespace ns14="urn:card.entities.be.dcl";
declare namespace urn12="urn:accountdict.dictionaries.be.dcl";  (:1.3:)
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;
declare variable $inputNF_CTRL_AREAS as xs:string* external; (:1.5:)
declare variable $inputNF_ACCOUN_ACCOUNTNUMBER as xs:string* external; (:1.5:)


declare function sourceValue2Boolean ($parm as xs:string*,$trueval as xs:string) as xs:string* {
    if ($parm  =$trueval)
       then "true"
       else "false"
};

(:1.4 start :)
declare function CEKECurrencyToISOCode($CEKECurrencyCode as xs:string*) as xs:string* {
  if         ($CEKECurrencyCode = "999") then "PLN"
    else if ($CEKECurrencyCode = "978") then "EUR"
    else if ($CEKECurrencyCode = "797") then "CHF"
    else if ($CEKECurrencyCode = "789") then "GBP"
    else if ($CEKECurrencyCode = "784") then "JPY"
    else if ($CEKECurrencyCode = "781") then "AUD"
    else if ($CEKECurrencyCode = "788") then "CAD"
    else if ($CEKECurrencyCode = "213") then "CZK"
    else if ($CEKECurrencyCode = "792") then "DKK"
    else if ($CEKECurrencyCode = "207") then "HUF"
    else if ($CEKECurrencyCode = "796") then "NOK"
    else if ($CEKECurrencyCode = "798") then "SEK"
    else if ($CEKECurrencyCode = "235") then "LVL"
    else if ($CEKECurrencyCode = "214") then "SKK"
    else if ($CEKECurrencyCode = "786") then "ATS"
    else if ($CEKECurrencyCode = "791") then "BEF"
    else if ($CEKECurrencyCode = "795") then "DEM"
    else if ($CEKECurrencyCode = "785") then "ESP"
    else if ($CEKECurrencyCode = "780") then "FIM"
    else if ($CEKECurrencyCode = "793") then "FRF"
    else if ($CEKECurrencyCode = "724") then "GRD"
    else if ($CEKECurrencyCode = "782") then "IEP"
    else if ($CEKECurrencyCode = "799") then "ITL"
    else if ($CEKECurrencyCode = "790") then "LUF"
    else if ($CEKECurrencyCode = "794") then "NLG"
    else if ($CEKECurrencyCode = "779") then "PTE"
    else "---"
};
(:1.4 end:)

declare function insertDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {
      if ($value)
        then if(string-length($value)>5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)>5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };

declare function chgDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as  element()* {

    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)

    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 


};

declare function getElementsForInsurancePackagesList($parm as element(fml:FML32)) as element()
{
&lt;ns8:insurancePackagesList>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
       if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "4") and (data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ]) = "91"))
     then
    &lt;ns10:InsurancePackageAcc>
      &lt;ns10:number?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns10:number>
	  {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns10:startDate")}
      &lt;ns10:insuranceDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns10:insuranceDescription>
      &lt;ns10:accountRelationshipList>
          &lt;ns3:AccountRelationship>
            &lt;ns3:relationship>
              &lt;ns5:CustomerAccountRelationship>
                &lt;ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship>
              &lt;/ns5:CustomerAccountRelationship>
            &lt;/ns3:relationship>
          &lt;/ns3:AccountRelationship>
      &lt;/ns10:accountRelationshipList>
      &lt;ns10:productDefinition>
        &lt;ns1:ProductDefinition>
          &lt;ns1:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns1:idProductDefinition>
          &lt;ns1:productGroup>
            &lt;ns1:ProductGroup>
              &lt;ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup>
            &lt;/ns1:ProductGroup>
          &lt;/ns1:productGroup>
        &lt;/ns1:ProductDefinition>
      &lt;/ns10:productDefinition>
      &lt;ns10:currency>
        &lt;ns5:CurrencyCode>
          &lt;ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode>
        &lt;/ns5:CurrencyCode>
      &lt;/ns10:currency>
    &lt;/ns10:InsurancePackageAcc>
   else()
  }
&lt;/ns8:insurancePackagesList>
};

declare function getElementsForInsurancePoliciesList($parm as element(fml:FML32)) as element()
{

&lt;ns8:insurancePoliciesList>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER 
    return
     if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "4") and (data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ]) = "3"))
     then
    &lt;ns10:InsurancePolicyAcc>
      &lt;ns10:policyRefNum?>{data($parm/NF_INSURA_ID[$occ])}&lt;/ns10:policyRefNum>
      {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns10:startDate")}
      &lt;ns10:insuranceDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns10:insuranceDescription>
	  &lt;ns10:accountRelationshipList>
          &lt;ns3:AccountRelationship>
            &lt;ns3:relationship>
              &lt;ns5:CustomerAccountRelationship>
                &lt;ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship>
              &lt;/ns5:CustomerAccountRelationship>
            &lt;/ns3:relationship>
          &lt;/ns3:AccountRelationship>
      &lt;/ns10:accountRelationshipList>
      &lt;ns10:productDefinition>
        &lt;ns1:ProductDefinition>
          &lt;ns1:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns1:idProductDefinition>
          &lt;ns1:productGroup>
            &lt;ns1:ProductGroup>
              &lt;ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup>
            &lt;/ns1:ProductGroup>
          &lt;/ns1:productGroup>
        &lt;/ns1:ProductDefinition>
      &lt;/ns10:productDefinition>
      &lt;ns10:currency>
        &lt;ns5:CurrencyCode>
          &lt;ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode>
        &lt;/ns5:CurrencyCode>
      &lt;/ns10:currency>
    &lt;/ns10:InsurancePolicyAcc>
   else()
  }
&lt;/ns8:insurancePoliciesList>
};

declare function getElementsForAccountListOut($parm as element(fml:FML32)) as element()
{

&lt;ns8:accountListOut>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
        if ((data($parm/NF_CTRL_SYSTEMID[$occ]) = "1") and ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="2") or 
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="3") or 
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="10") or
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="13") or
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="14"))) then
    &lt;ns0:Account>
      &lt;ns0:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns0:accountNumber>
      &lt;ns0:customerNumber?>{data($parm/NF_CUSTOM_CUSTOMERNUMBER[$occ])}&lt;/ns0:customerNumber>
      {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns0:accountOpenDate")}
      &lt;ns0:accountName?>{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}&lt;/ns0:accountName>
      &lt;ns0:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns0:currentBalance>
      &lt;ns0:debitPercentage?>{data($parm/NF_ACCOUN_DEBITPERCENTAGE[$occ])}&lt;/ns0:debitPercentage>
      &lt;ns0:statementForAccount?>{sourceValue2Boolean(data($parm/NF_ACCOUN_STATEMENTFORACCO[$occ]),"1")}&lt;/ns0:statementForAccount>
      &lt;ns0:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns0:accountDescription>
      &lt;ns0:codeProductSystem?>{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}&lt;/ns0:codeProductSystem>
      &lt;ns0:productCode?>{data($parm/NF_ACCOUN_PRODUCTCODE[$occ])}&lt;/ns0:productCode>
      &lt;ns0:availableBalance?>{data($parm/NF_ACCOUN_AVAILABLEBALANCE[$occ])}&lt;/ns0:availableBalance>
      &lt;ns0:currentBalancePLN?>{data($parm/NF_ACCOUN_CURRENTBALANCEPL[$occ])}&lt;/ns0:currentBalancePLN>
      &lt;ns0:accountRelationshipList>
          &lt;ns3:AccountRelationship>
            &lt;ns3:relationship>
              &lt;ns5:CustomerAccountRelationship>
                &lt;ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship>
              &lt;/ns5:CustomerAccountRelationship>
            &lt;/ns3:relationship>
          &lt;/ns3:AccountRelationship>
      &lt;/ns0:accountRelationshipList>
       {
         if (data($parm/NF_PRODUA_CODEPRODUCTAREA) = "3") then
           &lt;ns0:loanAccount>
             &lt;ns0:LoanAccount>
               &lt;ns0:maturityDate?>{data($parm/NF_LOANA_MATURITYDATE[$occ])}&lt;/ns0:maturityDate>
               &lt;ns0:dateOfLastTrans?>{data($parm/NF_LOANA_DATEOFLASTTRANS[$occ])}&lt;/ns0:dateOfLastTrans>
              &lt;/ns0:LoanAccount>
           &lt;/ns0:loanAccount>
         else ()
      }
(: 1.3 start :)
         &lt;ns0:tranAccount>
              &lt;ns0:TranAccount>
                    &lt;ns0:accountTypeFlag>
                          &lt;ns5:AccountTypeFlag>
                                &lt;ns5:accountTypeFlag?>{data($parm/NF_ACCOTF_ACCOUNTTYPEFLAG[$occ])}&lt;/ns5:accountTypeFlag>
                    &lt;/ns5:AccountTypeFlag>
                &lt;/ns0:accountTypeFlag>
            &lt;/ns0:TranAccount>
            &lt;/ns0:tranAccount>
(: 1.3 koniec :)
      {
          if (data($parm/NF_PRODUA_CODEPRODUCTAREA) = "10") then
     &lt;ns0:timeAccount>
        &lt;ns0:TimeAccount>
          &lt;ns0:renewalFrequency?>{data($parm/NF_TIMEA_RENEWALFREQUENCY[$occ])}&lt;/ns0:renewalFrequency>
		  {insertDate(data($parm/NF_TIMEA_NEXTRENEWALMATURI[$occ]),"yyyy-MM-dd","ns0:nextRenewalMaturityDate")}
          &lt;ns0:interestDisposition?>{sourceValue2Boolean(data($parm/NF_TIMEA_INTERESTDISPOSITI[$occ]),"1")}&lt;/ns0:interestDisposition>
          &lt;ns0:renewalID>
            &lt;ns5:RenewalType>
              &lt;ns5:renewalType?>{data($parm/NF_RENEWT_RENEWALTYPE[$occ])}&lt;/ns5:renewalType>
            &lt;/ns5:RenewalType>
          &lt;/ns0:renewalID>
          &lt;ns0:renewalPeriod>
            &lt;ns5:Period>
              &lt;ns5:period?>{data($parm/NF_PERIOD_PERIOD[$occ])}&lt;/ns5:period>
            &lt;/ns5:Period>
          &lt;/ns0:renewalPeriod>
        &lt;/ns0:TimeAccount>
      &lt;/ns0:timeAccount>
     else()
     }

      &lt;ns0:accountType>
        &lt;ns5:AccountType>
         (: T39249 &lt;ns5:accountType?>{data($parm/NF_ACCOUN_ACCOUNTTYPE[$occ])}&lt;/ns5:accountType> :)
             &lt;ns5:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns5:accountType>  (: T39249 :)
        &lt;/ns5:AccountType>
      &lt;/ns0:accountType>
      &lt;ns0:currency>
        &lt;ns5:CurrencyCode>
          &lt;ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode>
        &lt;/ns5:CurrencyCode>
      &lt;/ns0:currency>
      &lt;ns0:accountStatus>
        &lt;ns2:AccountStatus>
            &lt;ns2:accountStatus?>{data($parm/NF_PRODUCT_STATUSCODE[$occ])}&lt;/ns2:accountStatus>
        &lt;/ns2:AccountStatus>
      &lt;/ns0:accountStatus>
      &lt;ns0:accountBranchNumber>
        &lt;ns5:BranchCode>
          &lt;ns5:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE[$occ])}&lt;/ns5:branchCode>
        &lt;/ns5:BranchCode>
      &lt;/ns0:accountBranchNumber>
(: 1.3 start :)
      &lt;ns0:accessType>
          &lt;urn12:AccessType>
                &lt;urn12:accessType?>{data($parm/NF_ACCEST_ACCESSTYPE[$occ])}&lt;/urn12:accessType>
           &lt;/urn12:AccessType>
       &lt;/ns0:accessType>
(: 1.3 koniec :)
      &lt;ns0:productDefinition>
        &lt;ns1:ProductDefinition>
          &lt;ns1:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns1:idProductDefinition>
          &lt;ns1:productGroup>
            &lt;ns1:ProductGroup>
              &lt;ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup>
            &lt;/ns1:ProductGroup>
          &lt;/ns1:productGroup>
        &lt;/ns1:ProductDefinition>
      &lt;/ns0:productDefinition>
    &lt;/ns0:Account>
   else ()
  }
&lt;/ns8:accountListOut>
};

declare function getElementsForServiceWarningsList($parm as element(fml:FML32)) as element()
{
&lt;ns8:serviceWarningsList>
  {
    for $x at $occ in $parm/NF_SERVIW_WARNINGCODE1
    return
       &lt;ns4:ServiceWarning>
          &lt;ns4:warningCode1?>{data($parm/NF_SERVIW_WARNINGCODE1[$occ])}&lt;/ns4:warningCode1>
          &lt;ns4:warningCode2?>{data($parm/NF_SERVIW_WARNINGCODE2[$occ])}&lt;/ns4:warningCode2>
          &lt;ns4:warningUserVisibility?>{data($parm/NF_SERVIW_WARNINGUSERVISIB[$occ])} &lt;/ns4:warningUserVisibility>
          &lt;ns4:warningDescription?>{data($parm/NF_SERVIW_WARNINGDESCRIPTI[$occ])}&lt;/ns4:warningDescription>
       &lt;/ns4:ServiceWarning>
  }	   
&lt;/ns8:serviceWarningsList>
};

declare function getElementsForCustomersCEKE($parm as element(fml:FML32)) as element()
{
&lt;ns8:customersCEKE>
&lt;/ns8:customersCEKE>
};

declare function getElementsForPolicyContractList($parm as element(fml:FML32)) as element()
{

&lt;ns8:policyContract>
  {
    for $x at $occ in $parm/NF_INSURA_ID 
    return
     if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "4") and ((data($parm/NF_CTRL_SYSTEMID[$occ]) = "10") or (data($parm/NF_CTRL_SYSTEMID[$occ]) = "11")))
     then
       &lt;ns11:PolicyContract>
(: 1.2 &lt;ns11:proposalID?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns11:proposalID> :)
(: 1.2 :) &lt;ns11:proposalID?>{data($parm/NF_POLICC_PROPOSALID[$occ])}&lt;/ns11:proposalID>
       &lt;ns11:policyID?>{data($parm/NF_INSURA_ID[$occ])}&lt;/ns11:policyID>
       {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns11:proposalDate")}
       &lt;ns11:policyDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns11:policyDescription>
      &lt;ns11:productDefinition>
        &lt;ns1:ProductDefinition>
          &lt;ns1:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns1:idProductDefinition>
          &lt;ns1:productGroup>
            &lt;ns1:ProductGroup>
              &lt;ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup>
            &lt;/ns1:ProductGroup>
          &lt;/ns1:productGroup>
        &lt;/ns1:ProductDefinition>
       &lt;/ns11:productDefinition>
       &lt;ns11:productCode>
          &lt;ns12:UlParameters>
             &lt;ns12:productId?>{data($parm/NF_ATTRPG_FIRSTPRODUCTFEAT[$occ])}&lt;/ns12:productId>
          &lt;/ns12:UlParameters>
       &lt;/ns11:productCode>
       &lt;ns11:policyStatus>
         &lt;ns12:UlContractStatus>
           &lt;ns12:ulContractStatus?>{data($parm/NF_PRODUCT_STAT[$occ])}&lt;/ns12:ulContractStatus>
         &lt;/ns12:UlContractStatus>
        &lt;/ns11:policyStatus>
       &lt;/ns11:PolicyContract>
   else()
  }
&lt;/ns8:policyContract>
};

(: CR85 - begin :)
declare function getElementsForDebitCards($parm as element(fml:FML32)) as element()
{
&lt;ns8:debitCards>
  {
    for $x at $occ in $parm/NF_CARD_BIN
    return
	if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "1") and (data($parm/NF_CTRL_SYSTEMID[$occ]) = "1"))
     then
	 &lt;ns4:Card xmlns:ns4="urn:card.entities.be.dcl">
	  &lt;ns4:bin?>{data($parm/NF_CARD_BIN[$occ])}&lt;/ns4:bin>
	  &lt;ns4:cardType?>{data($parm/NF_CARD_CARDTYPE[$occ])}&lt;/ns4:cardType>
	  &lt;ns4:cardName?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns4:cardName>
	  &lt;ns4:debitCard>
	   &lt;ns4:DebitCard>
                {insertDate(data($parm/NF_DEBITC_EXPIRATIONDATE[$occ]),"yyyy-MM-dd","ns14:expirationDate")}
		&lt;ns4:cardNbr?>{data($parm/NF_DEBITC_CARDNBR[$occ])}&lt;/ns4:cardNbr>
		&lt;ns4:virtualCardNbr?>{data($parm/NF_DEBITC_VIRTUALCARDNBR[$occ])}&lt;/ns4:virtualCardNbr>
		&lt;ns4:tranAccount>
		 &lt;ns1:TranAccount xmlns:ns1="urn:accounts.entities.be.dcl">
		  &lt;ns1:account>
		   &lt;ns1:Account>
			&lt;ns1:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns1:accountNumber>
			&lt;ns1:currency>
			 &lt;ns11:CurrencyCode xmlns:ns11="urn:dictionaries.be.dcl">
			  &lt;ns11:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns11:currencyCode>
			 &lt;/ns11:CurrencyCode>
			&lt;/ns1:currency>
		   &lt;/ns1:Account>
		  &lt;/ns1:account>
		 &lt;/ns1:TranAccount>
		&lt;/ns4:tranAccount>
		&lt;ns4:cardStatus>
		 &lt;ns8:CrdStatus xmlns:ns8="urn:crddict.dictionaries.be.dcl">
		  &lt;ns8:crdStatus?>{data($parm/NF_PRODUCT_STATUSCODE[$occ])}&lt;/ns8:crdStatus>
		 &lt;/ns8:CrdStatus>
		&lt;/ns4:cardStatus>
	   &lt;/ns4:DebitCard>
	  &lt;/ns4:debitCard>
	  &lt;ns4:customer>
	   &lt;ns7:Customer xmlns:ns7="urn:cif.entities.be.dcl">
		&lt;ns7:customerNumber?>{data($parm/NF_ACCOUN_CUSTOMERNUMBER[$occ])}&lt;/ns7:customerNumber>
		&lt;ns7:customerPersonal>  
		 &lt;ns7:CustomerPersonal>
		  &lt;ns7:lastName?>{data($parm/NF_CUSTOP_LASTNAME[$occ])}&lt;/ns7:lastName>
		  &lt;ns7:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME[$occ])}&lt;/ns7:firstName>
		 &lt;/ns7:CustomerPersonal>
		&lt;/ns7:customerPersonal>
	   &lt;/ns7:Customer>
	  &lt;/ns4:customer>
	  &lt;ns4:productDefinition>
	   &lt;ns16:ProductDefinition xmlns:ns16="urn:productstree.entities.be.dcl">
		&lt;ns16:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns16:idProductDefinition>
		&lt;ns16:productGroup>
		 &lt;ns16:ProductGroup>
		  &lt;ns16:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns16:idProductGroup>
		 &lt;/ns16:ProductGroup>
		&lt;/ns16:productGroup>
	   &lt;/ns16:ProductDefinition>
	  &lt;/ns4:productDefinition>
	 &lt;/ns4:Card>
	else ()
  }
&lt;/ns8:debitCards>
};

declare function getElementsForCreditCards($parm as element(fml:FML32)) as element()
{
&lt;ns8:creditCards>
  {
    for $x at $occ in $parm/NF_CARD_BIN
    return
	if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "1") and (data($parm/NF_CTRL_SYSTEMID[$occ]) = "2"))
     then
&lt;ns4:Card xmlns:ns4="urn:card.entities.be.dcl">
	  &lt;ns4:bin?>{data($parm/NF_CARD_BIN[$occ])}&lt;/ns4:bin>
	  &lt;ns4:cardName?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns4:cardName>
	  &lt;ns4:creditCard>
	   &lt;ns4:CreditCard>
		&lt;ns4:cardNbr?>{data($parm/NF_DEBITC_CARDNBR[$occ])}&lt;/ns4:cardNbr>
		&lt;ns4:virtualCardNbr?>{data($parm/NF_DEBITC_VIRTUALCARDNBR[$occ])}&lt;/ns4:virtualCardNbr>
                {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns14:cardRecordOpenDate")}
                {insertDate(data($parm/NF_DEBITC_EXPIRATIONDATE[$occ]),"yyyy-MM-dd","ns14:expirationDate")}
		&lt;ns4:creditCardType>
		 &lt;ns9:CreditCardType xmlns:ns9="urn:creditcarddict.dictionaries.be.dcl">
		  &lt;ns9:creditCardType?>{data($parm/NF_CARD_CARDTYPE[$occ])}&lt;/ns9:creditCardType>
		 &lt;/ns9:CreditCardType>
		&lt;/ns4:creditCardType>
		&lt;ns4:creditCardAccount> (:uzupełnienia w tej sekcji w ramach zmiany v.1.4 :)
		 &lt;ns15:CreditCardAccount xmlns:ns15="urn:primeaccounts.entities.be.dcl">
		  &lt;ns15:accountNumber?>{data($parm/NF_CREDCA_ACCOUNTNUMBER[$occ])}&lt;/ns15:accountNumber>
		  &lt;ns15:grantedLimitAmount?>{data($parm/NF_CREDCA_GRANTEDLIMITAMOU[$occ])}&lt;/ns15:grantedLimitAmount>
		  &lt;ns15:currentBalance?>{data($parm/NF_CREDCA_CURRENTBALANCE[$occ])}&lt;/ns15:currentBalance>
		  &lt;ns15:availableBalance?>{data($parm/NF_CREDCA_AVAILABLEBALANCE[$occ])}&lt;/ns15:availableBalance>
		  &lt;ns15:currency>
		   &lt;ns11:CurrencyCode xmlns:ns11="urn:dictionaries.be.dcl">
(:1.4			&lt;ns11:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE2[$occ])}&lt;/ns11:currencyCode> :)
(:1.4:)			&lt;ns11:currencyCode?>{CEKECurrencyToISOCode(data($parm/NF_CURREC_CURRENCYCODE2[$occ]))}&lt;/ns11:currencyCode> 

		   &lt;/ns11:CurrencyCode>
		  &lt;/ns15:currency>
		 &lt;/ns15:CreditCardAccount>
		&lt;/ns4:creditCardAccount>
	   &lt;/ns4:CreditCard>
	  &lt;/ns4:creditCard>
	  &lt;ns4:productDefinition>
	   &lt;ns16:ProductDefinition xmlns:ns16="urn:productstree.entities.be.dcl">
		&lt;ns16:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns16:idProductDefinition>
		&lt;ns16:productGroup>
		 &lt;ns16:ProductGroup>
		  &lt;ns16:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}&lt;/ns16:idProductGroup>
		 &lt;/ns16:ProductGroup>
		&lt;/ns16:productGroup>
	   &lt;/ns16:ProductDefinition>
	  &lt;/ns4:productDefinition>
	 &lt;/ns4:Card>

	else ()
  }
&lt;/ns8:creditCards>
};

(: CR85 - end :)

(:1.5 start:)
declare function areasMatch($productArea as xs:string, $inputAreaFilter as xs:string) as xs:string* {
  if (string-length($productArea) = 1)
  then (
    if (fn:contains($inputAreaFilter, concat("0", $productArea)))
       then "true"
       else "false")
  else (
    if (fn:contains($inputAreaFilter, $productArea))
       then "true"
       else "false"
)
};
(:1.5 end:)

(:1.5 uwaga! poniższa funkcja getElementsForInvokeResponse została przebudowana w ramach zmiany 1.5:)
declare function getElementsForInvokeResponse($parm as element(fml:FML32), $inputNF_ACCOUN_ACCOUNTNUMBER as xs:string*, $inputAreaFilter as xs:string*) as element()*
{
&lt;ns8:invokeResponse>
{
  if (string-length($inputNF_ACCOUN_ACCOUNTNUMBER) > 0 and 
      string-length($inputNF_CTRL_AREAS) > 0 and 
      $inputNF_CTRL_AREAS != '00' and
(: 1.6      $parm/NF_PRODUA_CODEPRODUCTAREA[1] and 
      areasMatch (data($parm/NF_PRODUA_CODEPRODUCTAREA[1]), $inputAreaFilter) = "false" ) 1.6 poprawione:)
      $parm/PT_CODE_PRODUCT_AREA[1] and 
      areasMatch (data($parm/PT_CODE_PRODUCT_AREA[1]), $inputAreaFilter) = "false" ) (:1.6 poprawione:)
then (
 &lt;ns8:serviceWarningsList/>,
 &lt;ns8:customersCEKE/>,
 &lt;ns8:insurancePackagesList/>,
 &lt;ns8:insurancePoliciesList/>,
 &lt;ns8:policyContract/>,
 &lt;ns8:accountListOut/>,
 &lt;ns8:debitCards/>,
 &lt;ns8:bcd>
    &lt;ns6:BusinessControlData>
      &lt;ns6:pageControl>
        &lt;ns9:PageControl>
          &lt;ns9:hasNext>0&lt;/ns9:hasNext>
          &lt;ns9:navigationKeyValue/>
        &lt;/ns9:PageControl>
      &lt;/ns6:pageControl>
    &lt;/ns6:BusinessControlData>
  &lt;/ns8:bcd>,
 &lt;ns8:creditCards/>
)
else (
  getElementsForServiceWarningsList($parm),
  getElementsForCustomersCEKE($parm),
  getElementsForInsurancePackagesList($parm),
  getElementsForInsurancePoliciesList($parm),
  getElementsForPolicyContractList($parm),
  getElementsForAccountListOut($parm),
  getElementsForDebitCards($parm),  (: CR85 :)
  &lt;ns8:bcd>
    &lt;ns6:BusinessControlData>
      &lt;ns6:pageControl>
        &lt;ns9:PageControl>
          &lt;ns9:hasNext?>{data($parm/NF_PAGEC_HASNEXT)}&lt;/ns9:hasNext>
          &lt;ns9:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns9:navigationKeyDefinition>
          &lt;ns9:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns9:navigationKeyValue>
        &lt;/ns9:PageControl>
      &lt;/ns6:pageControl>
    &lt;/ns6:BusinessControlData>
  &lt;/ns8:bcd>
,
  getElementsForCreditCards($parm)  (: CR85 :)
)
}
&lt;/ns8:invokeResponse>
};


&lt;soap:Body>
(:1.5 uwaga! poniższa funkcja getElementsForInvokeResponse została przebudowana w ramach zmiany 1.5:)
  {getElementsForInvokeResponse($body/FML32, $inputNF_ACCOUN_ACCOUNTNUMBER, $inputNF_CTRL_AREAS)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>