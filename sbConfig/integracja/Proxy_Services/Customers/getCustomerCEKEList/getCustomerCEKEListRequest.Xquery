<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";

declare function xf:mapgetCustomerCEKEListRequest($req as element(urn:invoke), $head as element(urn:header))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				&lt;fml:E_MSHEAD_MSGID>{data($head/urn:msgHeader/urn:msgId)}&lt;/fml:E_MSHEAD_MSGID>
			}			
			{
				&lt;fml:B_CIF_KLIENTA?>{ data($req/urn:customer/urn1:Customer/urn1:customerNumber) }&lt;/fml:B_CIF_KLIENTA>
			}
			{
				&lt;fml:B_REGON?>{ data($req/urn:customer/urn1:Customer/urn1:customerFirm/urn1:CustomerFirm/urn1:regon) }&lt;/fml:B_REGON>
			}
			{
				&lt;fml:B_NR_DOWODU?>{ data($req/urn:customer/urn1:Customer/urn1:customerPersonal/urn1:CustomerPersonal/urn1:identityCardNumber) }&lt;/fml:B_NR_DOWODU>
			}
			{
				&lt;fml:B_N_PESEL?>{ data($req/urn:customer/urn1:Customer/urn1:customerPersonal/urn1:CustomerPersonal/urn1:pesel) }&lt;/fml:B_N_PESEL>
			}
			{
				&lt;fml:B_NR_PASZPORTU?>{ data($req/urn:customer/urn1:Customer/urn1:customerPersonal/urn1:CustomerPersonal/urn1:passportNumber) }&lt;/fml:B_NR_PASZPORTU>
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body>
{ xf:mapgetCustomerCEKEListRequest($body/urn:invoke,$header/urn:header) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>