<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ark="http://bzwbk.com/services/arka24messages/";

declare variable $body as element(soapenv:Body) external;

<soapenv:Body>
      {
      let $NrDok := $body/ark:GetCustArkaRegsRequest/ark:NrDok
      let $RodzDok := $body/ark:GetCustArkaRegsRequest/ark:RodzDok
      
      return
      if($RodzDok = "S" and $NrDok = "25102152688")
          then
            <ark:GetCustArkaRegsResponse>
              <ark:GetCustArkaRegs>
                <ark:IdKlienta>103424</ark:IdKlienta>
                <ark:KodFunduszu>306000</ark:KodFunduszu>
                <ark:KodProduktu>000</ark:KodProduktu>
                <ark:Nazwa>ARKA BZ WBK OBLIGACJI EUROPEJSKICH FIO - PRODUKT STANDARDOWY</ark:Nazwa>
                <ark:NrRejestru>03-08087491-06-01</ark:NrRejestru>
                <ark:Waluta>EUR</ark:Waluta>
                <ark:TypJednostki>S</ark:TypJednostki>
                <ark:Ilosc>1257.9950</ark:Ilosc>
                <ark:DataWyceny>18-05-2009</ark:DataWyceny>
                <ark:Wartosc>17841.71</ark:Wartosc>
                <ark:WartoscPln>79882.68</ark:WartoscPln>
                <ark:WartoscWaluty>4.477</ark:WartoscWaluty>
                <ark:IdProduktu>1180</ark:IdProduktu>
              </ark:GetCustArkaRegs>
              <ark:GetCustArkaRegs>
                <ark:IdKlienta>103424</ark:IdKlienta>
                <ark:KodFunduszu>301000</ark:KodFunduszu>
                <ark:KodProduktu>000</ark:KodProduktu>
                <ark:Nazwa>ARKA BZ WBK AKCJI FIO - PRODUKT STANDARDOWY</ark:Nazwa>
                <ark:NrRejestru>03-08087491-01-01</ark:NrRejestru>
                <ark:Waluta>PLN</ark:Waluta>
                <ark:TypJednostki>S</ark:TypJednostki>
                <ark:Ilosc>1296.5840</ark:Ilosc>
                <ark:DataWyceny>18-05-2009</ark:DataWyceny>
                <ark:Wartosc>35785.72</ark:Wartosc>
                <ark:WartoscPln>35785.72</ark:WartoscPln>
                <ark:WartoscWaluty>1</ark:WartoscWaluty>
                <ark:IdProduktu>1172</ark:IdProduktu>
              </ark:GetCustArkaRegs>
              <ark:GetCustArkaRegs>
                <ark:IdKlienta>103424</ark:IdKlienta>
                <ark:KodFunduszu>305000</ark:KodFunduszu>
                <ark:KodProduktu>000</ark:KodProduktu>
                <ark:Nazwa>ARKA BZ WBK OBLIGACJI FIO - PRODUKT STANDARDOWY</ark:Nazwa>
                <ark:NrRejestru>03-08087491-05-01</ark:NrRejestru>
                <ark:Waluta>PLN</ark:Waluta>
                <ark:TypJednostki>S</ark:TypJednostki>
                <ark:Ilosc>2915.3760</ark:Ilosc>
                <ark:DataWyceny>18-05-2009</ark:DataWyceny>
                <ark:Wartosc>42885.18</ark:Wartosc>
                <ark:WartoscPln>42885.18</ark:WartoscPln>
                <ark:WartoscWaluty>1</ark:WartoscWaluty>
                <ark:IdProduktu>1179</ark:IdProduktu>
              </ark:GetCustArkaRegs>
              <ark:GetCustArkaRegs>
                <ark:IdKlienta>103424</ark:IdKlienta>
                <ark:KodFunduszu>302000</ark:KodFunduszu>
                <ark:KodProduktu>000</ark:KodProduktu>
                <ark:Nazwa>ARKA BZ WBK ZRÓWNOWAŻONY FIO - PRODUKT STANDARDOWY</ark:Nazwa>
                <ark:NrRejestru>03-08087491-02-02</ark:NrRejestru>
                <ark:Waluta>PLN</ark:Waluta>
                <ark:TypJednostki>S</ark:TypJednostki>
                <ark:Ilosc>848.7960</ark:Ilosc>
                <ark:DataWyceny>18-05-2009</ark:DataWyceny>
                <ark:Wartosc>18597.12</ark:Wartosc>
                <ark:WartoscPln>18597.12</ark:WartoscPln>
                <ark:WartoscWaluty>1</ark:WartoscWaluty>
                <ark:IdProduktu>1175</ark:IdProduktu>
              </ark:GetCustArkaRegs>
            </ark:GetCustArkaRegsResponse>
        else if ($RodzDok = "S" and $NrDok = "71040905798")
          then
            <ark:GetCustArkaRegsResponse>
              <ark:GetCustArkaRegs>
                <ark:IdKlienta>41135</ark:IdKlienta>
                <ark:KodFunduszu>301000</ark:KodFunduszu>
                <ark:KodProduktu>000</ark:KodProduktu>
                <ark:Nazwa>ARKA BZ WBK AKCJI FIO - PRODUKT STANDARDOWY</ark:Nazwa>
                <ark:NrRejestru>03-00381284-01-03</ark:NrRejestru>
                <ark:Waluta>PLN</ark:Waluta>
                <ark:TypJednostki>S</ark:TypJednostki>
                <ark:Ilosc>3692.7620</ark:Ilosc>
                <ark:DataWyceny>18-05-2009</ark:DataWyceny>
                <ark:Wartosc>101920.23</ark:Wartosc>
                <ark:WartoscPln>101920.23</ark:WartoscPln>
                <ark:WartoscWaluty>1</ark:WartoscWaluty>
                <ark:IdProduktu>1172</ark:IdProduktu>
              </ark:GetCustArkaRegs>
              <ark:GetCustArkaRegs>
                <ark:IdKlienta>41135</ark:IdKlienta>
                <ark:KodFunduszu>303000</ark:KodFunduszu>
                <ark:KodProduktu>000</ark:KodProduktu>
                <ark:Nazwa>ARKA BZ WBK OCHRONY KAPITAŁU FIO - PRODUKT STANDARDOWY</ark:Nazwa>
                <ark:NrRejestru>03-00381284-03-01</ark:NrRejestru>
                <ark:Waluta>PLN</ark:Waluta>
                <ark:TypJednostki>A</ark:TypJednostki>
                <ark:Ilosc>0.0000</ark:Ilosc>
                <ark:DataWyceny>18-05-2009</ark:DataWyceny>
                <ark:Wartosc>0.00</ark:Wartosc>
                <ark:WartoscPln>0.00</ark:WartoscPln>
                <ark:WartoscWaluty>1</ark:WartoscWaluty>
                <ark:IdProduktu>1178</ark:IdProduktu>
              </ark:GetCustArkaRegs>
              <ark:GetCustArkaRegs>
                <ark:IdKlienta>41135</ark:IdKlienta>
                <ark:KodFunduszu>303000</ark:KodFunduszu>
                <ark:KodProduktu>000</ark:KodProduktu>
                <ark:Nazwa>ARKA BZ WBK OCHRONY KAPITAŁU FIO - PRODUKT STANDARDOWY</ark:Nazwa>
                <ark:NrRejestru>03-00381284-03-02</ark:NrRejestru>
                <ark:Waluta>PLN</ark:Waluta>
                <ark:TypJednostki>A</ark:TypJednostki>
                <ark:Ilosc>0.0000</ark:Ilosc>
                <ark:DataWyceny>18-05-2009</ark:DataWyceny>
                <ark:Wartosc>0.00</ark:Wartosc>
                <ark:WartoscPln>0.00</ark:WartoscPln>
                <ark:WartoscWaluty>1</ark:WartoscWaluty>
                <ark:IdProduktu>1178</ark:IdProduktu>
              </ark:GetCustArkaRegs>
              <ark:GetCustArkaRegs>
                <ark:IdKlienta>41135</ark:IdKlienta>
                <ark:KodFunduszu>304000</ark:KodFunduszu>
                <ark:KodProduktu>000</ark:KodProduktu>
                <ark:Nazwa>ARKA BZ WBK STABILNEGO WZROSTU FIO - PRODUKT STANDARDOWY</ark:Nazwa>
                <ark:NrRejestru>03-00381284-04-01</ark:NrRejestru>
                <ark:Waluta>PLN</ark:Waluta>
                <ark:TypJednostki>S</ark:TypJednostki>
                <ark:Ilosc>4748.1810</ark:Ilosc>
                <ark:DataWyceny>18-05-2009</ark:DataWyceny>
                <ark:Wartosc>106501.70</ark:Wartosc>
                <ark:WartoscPln>106501.70</ark:WartoscPln>
                <ark:WartoscWaluty>1</ark:WartoscWaluty>
                <ark:IdProduktu>1176</ark:IdProduktu>
              </ark:GetCustArkaRegs>
            </ark:GetCustArkaRegsResponse>
          else 
            <ark:GetCustArkaRegsResponse>
            </ark:GetCustArkaRegsResponse>
      }
</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>