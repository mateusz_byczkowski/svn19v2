<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbCEKEeCustSetRequest($req as element(m:bCEKEeCustSetRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:AdminMicroBranch)
					then &lt;fml:E_ADMIN_MICRO_BRANCH>{ data($req/m:AdminMicroBranch) }&lt;/fml:E_ADMIN_MICRO_BRANCH>
					else ()
			}
			{
				if($req/m:LoginId)
					then &lt;fml:E_LOGIN_ID>{ data($req/m:LoginId) }&lt;/fml:E_LOGIN_ID>
					else ()
			}
			{
				if($req/m:Limit)
					then &lt;fml:E_LIMIT>{ data($req/m:Limit) }&lt;/fml:E_LIMIT>
					else ()
			}
			{
				if($req/m:HiLimit)
					then &lt;fml:E_HI_LIMIT>{ data($req/m:HiLimit) }&lt;/fml:E_HI_LIMIT>
					else ()
			}
			{
				if($req/m:TrnLimit)
					then &lt;fml:E_TRN_LIMIT>{ data($req/m:TrnLimit) }&lt;/fml:E_TRN_LIMIT>
					else ()
			}
			{
				if($req/m:NrDok)
					then &lt;fml:B_NR_DOK>{ data($req/m:NrDok) }&lt;/fml:B_NR_DOK>
					else ()
			}
			{
				if($req/m:RodzDok)
					then &lt;fml:B_RODZ_DOK>{ data($req/m:RodzDok) }&lt;/fml:B_RODZ_DOK>
					else ()
			}
			{
				if($req/m:HiCycle)
					then &lt;fml:E_HI_CYCLE>{ data($req/m:HiCycle) }&lt;/fml:E_HI_CYCLE>
					else ()
			}
			{
				if($req/m:CustSmsno)
					then &lt;fml:E_CUST_SMSNO>{ data($req/m:CustSmsno) }&lt;/fml:E_CUST_SMSNO>
					else ()
			}
			{
				if($req/m:ExCycle)
					then &lt;fml:E_EX_CYCLE>{ data($req/m:ExCycle) }&lt;/fml:E_EX_CYCLE>
					else ()
			}
			{
				if($req/m:Cycle)
					then &lt;fml:E_CYCLE>{ data($req/m:Cycle) }&lt;/fml:E_CYCLE>
					else ()
			}
			{
				if($req/m:CustName)
					then &lt;fml:E_CUST_NAME>{ data($req/m:CustName) }&lt;/fml:E_CUST_NAME>
					else ()
			}
			{
				if($req/m:CustCity)
					then &lt;fml:E_CUST_CITY>{ data($req/m:CustCity) }&lt;/fml:E_CUST_CITY>
					else ()
			}
			{
				if($req/m:CustZipcode)
					then &lt;fml:E_CUST_ZIPCODE>{ data($req/m:CustZipcode) }&lt;/fml:E_CUST_ZIPCODE>
					else ()
			}
			{
				if($req/m:CustTelno)
					then &lt;fml:E_CUST_TELNO>{ data($req/m:CustTelno) }&lt;/fml:E_CUST_TELNO>
					else ()
			}
			{
				if($req/m:CustFaxno)
					then &lt;fml:E_CUST_FAXNO>{ data($req/m:CustFaxno) }&lt;/fml:E_CUST_FAXNO>
					else ()
			}
			{
				if($req/m:CustGsmno)
					then &lt;fml:E_CUST_GSMNO>{ data($req/m:CustGsmno) }&lt;/fml:E_CUST_GSMNO>
					else ()
			}
			{
				if($req/m:CustEmail)
					then &lt;fml:E_CUST_EMAIL>{ data($req/m:CustEmail) }&lt;/fml:E_CUST_EMAIL>
					else ()
			}
			{
				if($req/m:CustStatus)
					then &lt;fml:E_CUST_STATUS>{ data($req/m:CustStatus) }&lt;/fml:E_CUST_STATUS>
					else ()
			}
			{
				if($req/m:CustIdentif)
					then &lt;fml:E_CUST_IDENTIF>{ data($req/m:CustIdentif) }&lt;/fml:E_CUST_IDENTIF>
					else ()
			}
			{
				if($req/m:CustOptions)
					then &lt;fml:E_CUST_OPTIONS>{ data($req/m:CustOptions) }&lt;/fml:E_CUST_OPTIONS>
					else ()
			}
			{
				if($req/m:RepName)
					then &lt;fml:E_REP_NAME>{ data($req/m:RepName) }&lt;/fml:E_REP_NAME>
					else ()
			}
			{
				if($req/m:ExLimit)
					then &lt;fml:E_EX_LIMIT>{ data($req/m:ExLimit) }&lt;/fml:E_EX_LIMIT>
					else ()
			}
			{
				if($req/m:TrnExLimit)
					then &lt;fml:E_TRN_EX_LIMIT>{ data($req/m:TrnExLimit) }&lt;/fml:E_TRN_EX_LIMIT>
					else ()
			}
			{
				if($req/m:Sys)
					then &lt;fml:B_SYS>{ data($req/m:Sys) }&lt;/fml:B_SYS>
					else ()
			}
			{
				if($req/m:Bank)
					then &lt;fml:B_BANK>{ data($req/m:Bank) }&lt;/fml:B_BANK>
					else ()
			}
			{
				if($req/m:TokenCustomerId)
					then &lt;fml:E_TOKEN_CUSTOMER_ID>{ data($req/m:TokenCustomerId) }&lt;/fml:E_TOKEN_CUSTOMER_ID>
					else ()
			}
			{
				if($req/m:TokenIdType)
					then &lt;fml:E_TOKEN_ID_TYPE>{ data($req/m:TokenIdType) }&lt;/fml:E_TOKEN_ID_TYPE>
					else ()
			}
			{
				if($req/m:TokenLoginId)
					then &lt;fml:E_TOKEN_LOGIN_ID>{ data($req/m:TokenLoginId) }&lt;/fml:E_TOKEN_LOGIN_ID>
					else ()
			}
			{
				if($req/m:CifOptions)
					then &lt;fml:E_CIF_OPTIONS>{ data($req/m:CifOptions) }&lt;/fml:E_CIF_OPTIONS>
					else ()
			}
			{
				if($req/m:AllowedSecurityLevel)
					then &lt;fml:E_ALLOWED_SECURITY_LEVEL>{ data($req/m:AllowedSecurityLevel) }&lt;/fml:E_ALLOWED_SECURITY_LEVEL>
					else ()
			}
			{
				if($req/m:DefaultSecurityLevel)
					then &lt;fml:E_DEFAULT_SECURITY_LEVEL>{ data($req/m:DefaultSecurityLevel) }&lt;/fml:E_DEFAULT_SECURITY_LEVEL>
					else ()
			}
			{
				if($req/m:ProfileId)
					then &lt;fml:E_PROFILE_ID>{ data($req/m:ProfileId) }&lt;/fml:E_PROFILE_ID>
					else ()
			}
			{
				if($req/m:CustomerType)
					then &lt;fml:E_CUSTOMER_TYPE>{ data($req/m:CustomerType) }&lt;/fml:E_CUSTOMER_TYPE>
					else ()
			}
			{
				if($req/m:ProfileTrial)
					then &lt;fml:E_PROFILE_TRIAL>{ data($req/m:ProfileTrial) }&lt;/fml:E_PROFILE_TRIAL>
					else ()
			}
			{
				if($req/m:ProfileExpiry)
					then &lt;fml:E_PROFILE_EXPIRY>{ data($req/m:ProfileExpiry) }&lt;/fml:E_PROFILE_EXPIRY>
					else ()
			}
			{
				if($req/m:CustReportVersion)
					then &lt;fml:E_CUST_REPORT_VERSION>{ data($req/m:CustReportVersion) }&lt;/fml:E_CUST_REPORT_VERSION>
					else ()
			}
			{
				if($req/m:CustReportDate)
					then &lt;fml:E_CUST_REPORT_DATE>{ data($req/m:CustReportDate) }&lt;/fml:E_CUST_REPORT_DATE>
					else ()
			}
			{
				if($req/m:CustRebate)
					then &lt;fml:E_CUST_REBATE>{ data($req/m:CustRebate) }&lt;/fml:E_CUST_REBATE>
					else ()
			}
			{
				if($req/m:SysMask)
					then &lt;fml:B_SYS_MASK>{ data($req/m:SysMask) }&lt;/fml:B_SYS_MASK>
					else ()
			}
			{
				if($req/m:ChannelType)
					then &lt;fml:E_CHANNEL_TYPE>{ data($req/m:ChannelType) }&lt;/fml:E_CHANNEL_TYPE>
					else ()
			}
			{
				if($req/m:Status)
					then &lt;fml:E_STATUS>{ data($req/m:Status) }&lt;/fml:E_STATUS>
					else ()
			}
			{
				if($req/m:Tries)
					then &lt;fml:E_TRIES>{ data($req/m:Tries) }&lt;/fml:E_TRIES>
					else ()
			}
			{
				if($req/m:TriesAllowed)
					then &lt;fml:E_TRIES_ALLOWED>{ data($req/m:TriesAllowed) }&lt;/fml:E_TRIES_ALLOWED>
					else ()
			}
			{
				if($req/m:Expiry)
					then &lt;fml:E_EXPIRY>{ data($req/m:Expiry) }&lt;/fml:E_EXPIRY>
					else ()
			}
			{
				if($req/m:ChangePeriod)
					then &lt;fml:E_CHANGE_PERIOD>{ data($req/m:ChangePeriod) }&lt;/fml:E_CHANGE_PERIOD>
					else ()
			}
			{
				if($req/m:TrnMask)
					then &lt;fml:E_TRN_MASK>{ data($req/m:TrnMask) }&lt;/fml:E_TRN_MASK>
					else ()
			}
			{
				if($req/m:Pdata)
					then &lt;fml:E_PDATA>{ data($req/m:Pdata) }&lt;/fml:E_PDATA>
					else ()
			}
			{
				if($req/m:ChannelCycle)
					then &lt;fml:E_CHANNEL_CYCLE>{ data($req/m:ChannelCycle) }&lt;/fml:E_CHANNEL_CYCLE>
					else ()
			}
			{
				if($req/m:ChannelLimit)
					then &lt;fml:E_CHANNEL_LIMIT>{ data($req/m:ChannelLimit) }&lt;/fml:E_CHANNEL_LIMIT>
					else ()
			}
			{
				if($req/m:ChannelCycleBegin)
					then &lt;fml:E_CHANNEL_CYCLE_BEGIN>{ data($req/m:ChannelCycleBegin) }&lt;/fml:E_CHANNEL_CYCLE_BEGIN>
					else ()
			}
			{
				if($req/m:SeqNo)
					then &lt;fml:E_SEQ_NO>{ data($req/m:SeqNo) }&lt;/fml:E_SEQ_NO>
					else ()
			}
			{
				if($req/m:KodRach)
					then &lt;fml:B_KOD_RACH>{ data($req/m:KodRach) }&lt;/fml:B_KOD_RACH>
					else ()
			}
			{
				if($req/m:Waluta)
					then &lt;fml:B_WALUTA>{ data($req/m:Waluta) }&lt;/fml:B_WALUTA>
					else ()
			}
			{
				if($req/m:DlNrRach)
					then &lt;fml:B_DL_NR_RACH>{ data($req/m:DlNrRach) }&lt;/fml:B_DL_NR_RACH>
					else ()
			}
			{
				if($req/m:TypRach)
					then &lt;fml:B_TYP_RACH>{ data($req/m:TypRach) }&lt;/fml:B_TYP_RACH>
					else ()
			}
			{
				if($req/m:Options)
					then &lt;fml:E_OPTIONS>{ data($req/m:Options) }&lt;/fml:E_OPTIONS>
					else ()
			}
			{
				if($req/m:CustStreet)
					then &lt;fml:E_CUST_STREET>{ data($req/m:CustStreet) }&lt;/fml:E_CUST_STREET>
					else ()
			}
			{
				if($req/m:UserName)
					then &lt;fml:U_USER_NAME>{ data($req/m:UserName) }&lt;/fml:U_USER_NAME>
					else ()
			}
			{
				if($req/m:TimeStamp)
					then &lt;fml:E_TIME_STAMP>{ data($req/m:TimeStamp) }&lt;/fml:E_TIME_STAMP>
					else ()
			}
			{
				if($req/m:IdOddz)
					then &lt;fml:B_ID_ODDZ>{ data($req/m:IdOddz) }&lt;/fml:B_ID_ODDZ>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapbCEKEeCustSetRequest($body/m:bCEKEeCustSetRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>