<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";

declare namespace ns1="urn:baseauxentities.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:cifdict.dictionaries.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";

declare function xf:mapgetCustomerICBSResponse($fml as element(fml:FML32))
  as element(urn:invokeResponse) {

    <urn:invokeResponse>
    {
      <ns2:address>
          <ns2:Address>
              <ns2:city?>{ data($fml/fml:B_M_ZAM) }</ns2:city>
              <ns2:county?>{ data($fml/fml:B_WOJ_GM) }</ns2:county>
              <ns2:street?>{ data($fml/fml:B_UL_ZAM) }</ns2:street>
              <ns2:zipCode?>{ data($fml/fml:B_KOD_POCZT) }</ns2:zipCode>
              <ns2:phoneNumber?>{ data($fml/fml:B_TELEFON) }</ns2:phoneNumber>
          </ns2:Address>
      </ns2:address>
    }
    {
      if (data($fml/fml:B_NAZWA_KORESP)) then
      (
      <ns2:forwardingAddress>
          <ns2:Address>
              <ns2:city?>{ data($fml/fml:B_M_KORESP) }</ns2:city>
              <ns2:street?>{ data($fml/fml:B_UL_KORESP) }</ns2:street>
              <ns2:zipCode?>{ data($fml/fml:B_KOD_KORESP) }</ns2:zipCode>
              <ns2:name?>{ data($fml/fml:B_NAZWA_KORESP) }</ns2:name>
          </ns2:Address>
      </ns2:forwardingAddress>
      ) else ()
    }
    { 
      <ns1:deleteCode>
          <ns1:StringHolder>
              <ns1:value?>{ data($fml/fml:B_KOD_USUNIECIA) }</ns1:value>
          </ns1:StringHolder>
      </ns1:deleteCode>
    }
    { 
      <ns2:customerOut>
          <ns2:Customer>
              <ns2:companyName?>{ concat(data($fml/fml:B_FIRMA),data($fml/fml:B_FIRMA_CD)) }</ns2:companyName>
              <ns2:customerNumber?>{ data($fml/fml:E_CIF_NUMBER) }</ns2:customerNumber>
              <ns2:fileMaintenanceLastDate?>{ concat(substring(data($fml/fml:B_D_OSTATNIEJ_AKT),7,4),'-', substring(data($fml/fml:B_D_OSTATNIEJ_AKT),4,2),'-',substring(data($fml/fml:B_D_OSTATNIEJ_AKT),1,2))}</ns2:fileMaintenanceLastDate>
              <ns2:resident?>{ data($fml/fml:B_REZYDENT) }</ns2:resident>
              <ns2:sectionType?>{ data($fml/fml:B_SEKCJA) }</ns2:sectionType>
              <ns2:memo?>{ data($fml/fml:B_MEMO) }</ns2:memo>
              <ns2:addressList?>{ data($fml/fml:B_M_KORESP) }</ns2:addressList>
              <ns2:customerAdditionalInfo>
                  <ns2:CustomerAdditionalInfo>
                      <ns2:riskCode?>{ data($fml/fml:B_SYT_FINANSOWA) }</ns2:riskCode>
                  </ns2:CustomerAdditionalInfo>
              </ns2:customerAdditionalInfo>
              <ns2:customerFirm>
                  <ns2:CustomerFirm>
                      <ns2:propertyType?>{ data($fml/fml:B_WLASNOSC) }</ns2:propertyType>
                      <ns4:countryOfResidency>
                          <ns4:CountryCode>
                              <ns4:countryCode?>{ data($fml/fml:B_KRAJ) } </ns4:countryCode>
                          </ns4:CountryCode>
                      </ns4:countryOfResidency>
                      <ns4:ekd>
                          <ns4:EkdCode>
                              <ns4:ekdCode?>{ data($fml/fml:B_N_EKD) }</ns4:ekdCode>
                          </ns4:EkdCode>
                      </ns4:ekd>
                  </ns2:CustomerFirm>
              </ns2:customerFirm>
              <ns2:customerPersonal>
                  <ns2:CustomerPersonal>
                      <ns2:workInBankStatus?>{ data($fml/fml:B_KREWNY_BANKU) }</ns2:workInBankStatus>
                      <ns2:birthPlace?>{ data($fml/fml:B_M_URODZENIA) }</ns2:birthPlace>
                      <ns2:dateOfBirth?>{ concat(substring(data($fml/fml:B_D_URODZENIA),7,4),'-', substring(data($fml/fml:B_D_URODZENIA),4,2),'-',substring(data($fml/fml:B_D_URODZENIA),1,2))}</ns2:dateOfBirth>
                  </ns2:CustomerPersonal>
              </ns2:customerPersonal>
              <ns4:branchOfOwnership>
                  <ns4:BranchCode>
                      <ns4:branchCode?>{ data($fml/fml:B_ID_ODDZ) }</ns4:branchCode>
                      <ns4:branchType>
                          <ns4:BranchType>
                              <ns4:branchType?>{ data($fml/fml:B_MIKROODDZIAL) }</ns4:branchType>
                          </ns4:BranchType>
                      </ns4:branchType>
                  </ns4:BranchCode>
              </ns4:branchOfOwnership>
              <ns4:customerType>
                  <ns4:CustomerType>
                      {
                        if (data($fml/fml:E_CUSTOMER_TYPE)= "C") then 
                        (
                          <ns4:customerType>F</ns4:customerType>
                        ) 
                        else 
                        (
                          <ns4:customerType>I</ns4:customerType>
                        )
                      }
                  </ns4:CustomerType>
              </ns4:customerType>
              <ns3:subjectType>
                  <ns3:SubjectType>
                      <ns3:subjectType?>{ data($fml/fml:B_PODMIOT) }</ns3:subjectType>
                  </ns3:SubjectType>
              </ns3:subjectType>
          </ns2:Customer>
      </ns2:customerOut>
    }
    </urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;

<soap-env:Body>
{ xf:mapgetCustomerICBSResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>