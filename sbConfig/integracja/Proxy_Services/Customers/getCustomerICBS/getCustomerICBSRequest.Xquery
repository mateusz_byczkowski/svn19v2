<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:dictionaries.be.dcl";
declare namespace urn2 = "urn:cif.entities.be.dcl";
declare namespace urn3 = "urn:baseauxentities.be.dcl";



declare function xf:mapgetCustomerICBSRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				&lt;fml:B_RODZ_DOK?>{ data($req/urn:documentType/urn1:AdditionalIDType/urn1:additionalIDType) }&lt;/fml:B_RODZ_DOK>
			}
			{
				&lt;fml:B_NR_DOK?>{ data($req/urn:customer/urn2:Customer/urn2:documentList/urn2:Document/urn2:documentNumber) }&lt;/fml:B_NR_DOK>
			}
			{
				&lt;fml:E_CIF_NUMBER?>{ data($req/urn:customer/urn2:Customer/urn2:customerNumber) }&lt;/fml:E_CIF_NUMBER>
			}
			{
				&lt;fml:B_ID_ODDZ?>{ data($req/urn:customer/urn2:Customer/urn2:branchOfOwnership/urn1:BranchCode/urn1:branchCode) }&lt;/fml:B_ID_ODDZ>
			}
			{
				&lt;fml:E_CUSTOMER_TYPE?>{ data($req/urn:customer/urn2:Customer/urn2:customerType/urn1:CustomerType/urn1:customerType) }&lt;/fml:E_CUSTOMER_TYPE>
			}
			{
				&lt;fml:E_CIF_OPTIONS?>{ data($req/urn:cifOption/urn3:IntegerHolder) }&lt;/fml:E_CIF_OPTIONS>
			}
			{
				&lt;fml:B_OPCJA?>{ data($req/urn:option/urn3:IntegerHolder) }&lt;/fml:B_OPCJA>
			}
		&lt;/fml:FML32>
};


declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body>
 { xf:mapgetCustomerICBSRequest($body/urn:invoke) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>