<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>PT58 - dodanie Body.Version.$1.2011-05-24</con:description>
  <con:xquery>declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/SetCustDataShare/ResponseTransform2/";
declare namespace ns0 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns-1 = "urn:baseentities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:ResponseTransform2($pcCustDataShareResponse1 as element(ns0:pcCustDataShareResponse))
    as element(ns1:invokeResponse) {
        &lt;ns1:invokeResponse>
                &lt;ns1:messageHelper>
	            &lt;/ns1:messageHelper>
            &lt;/ns1:invokeResponse>
        
};

declare variable $pcCustDataShareResponse1 as element(ns0:pcCustDataShareResponse) external;

&lt;soap-env:Body>{
xf:ResponseTransform2($pcCustDataShareResponse1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>