<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappcDtlsSysAddrNewRequest($req as element(m:pcDtlsSysAddrNewRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:PracownikWprow)
					then &lt;fml:CI_PRACOWNIK_WPROW>{ data($req/m:PracownikWprow) }&lt;/fml:CI_PRACOWNIK_WPROW>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:IdSys)
					then &lt;fml:CI_ID_SYS>{ data($req/m:IdSys) }&lt;/fml:CI_ID_SYS>
					else ()
			}
			{
				if($req/m:TypAdresu)
					then &lt;fml:CI_TYP_ADRESU>{ data($req/m:TypAdresu) }&lt;/fml:CI_TYP_ADRESU>
					else ()
			}
			{
				if($req/m:NumerRachunku)
					then &lt;fml:CI_NUMER_RACHUNKU>{ data($req/m:NumerRachunku) }&lt;/fml:CI_NUMER_RACHUNKU>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:DataWprowadzenia)
					then &lt;fml:DC_DATA_WPROWADZENIA>{ data($req/m:DataWprowadzenia) }&lt;/fml:DC_DATA_WPROWADZENIA>
					else ()
			}
			{
				if($req/m:AdresDomyslny)
					then &lt;fml:CI_ADRES_DOMYSLNY>{ data($req/m:AdresDomyslny) }&lt;/fml:CI_ADRES_DOMYSLNY>
					else ()
			}
			{
				if($req/m:ImieINazwiskoAlt)
					then &lt;fml:DC_IMIE_I_NAZWISKO_ALT>{ data($req/m:ImieINazwiskoAlt) }&lt;/fml:DC_IMIE_I_NAZWISKO_ALT>
					else ()
			}
			{
				if($req/m:UlicaAdresAlt)
					then &lt;fml:DC_ULICA_ADRES_ALT>{ data($req/m:UlicaAdresAlt) }&lt;/fml:DC_ULICA_ADRES_ALT>
					else ()
			}
			{
				if($req/m:NrPosesLokaluAdresAlt)
					then &lt;fml:DC_NR_POSES_LOKALU_ADRES_ALT>{ data($req/m:NrPosesLokaluAdresAlt) }&lt;/fml:DC_NR_POSES_LOKALU_ADRES_ALT>
					else ()
			}
			{
				if($req/m:KodPocztowyAdresAlt)
					then &lt;fml:DC_KOD_POCZTOWY_ADRES_ALT>{ data($req/m:KodPocztowyAdresAlt) }&lt;/fml:DC_KOD_POCZTOWY_ADRES_ALT>
					else ()
			}
			{
				if($req/m:MiastoAdresAlt)
					then &lt;fml:DC_MIASTO_ADRES_ALT>{ data($req/m:MiastoAdresAlt) }&lt;/fml:DC_MIASTO_ADRES_ALT>
					else ()
			}
			{
				if($req/m:WojewodztwoKrajAdresAlt)
					then &lt;fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{ data($req/m:WojewodztwoKrajAdresAlt) }&lt;/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
					else ()
			}
			{
				if($req/m:KodKraju)
					then &lt;fml:CI_KOD_KRAJU>{ data($req/m:KodKraju) }&lt;/fml:CI_KOD_KRAJU>
					else ()
			}
			{
				if($req/m:DataOd)
					then &lt;fml:CI_DATA_OD>{ data($req/m:DataOd) }&lt;/fml:CI_DATA_OD>
					else ()
			}
			{
				if($req/m:DataDo)
					then &lt;fml:CI_DATA_DO>{ data($req/m:DataDo) }&lt;/fml:CI_DATA_DO>
					else ()
			}
			{
				if($req/m:DataZamieszkania)
					then &lt;fml:CI_DATA_ZAMIESZKANIA>{ data($req/m:DataZamieszkania) }&lt;/fml:CI_DATA_ZAMIESZKANIA>
					else ()
			}
			{
				if($req/m:TypAdresuKoresp)
					then &lt;fml:CI_TYP_ADRESU_KORESP>{ data($req/m:TypAdresuKoresp) }&lt;/fml:CI_TYP_ADRESU_KORESP>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappcDtlsSysAddrNewRequest($body/m:pcDtlsSysAddrNewRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>