<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:cif.entities.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:executions.entities.be.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;
	

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };


declare function getElementsForExecutions($parm as element(fml:FML32)) as element()
{

&lt;ns4:executions>
  {
    for $x at $occ in $parm/NF_EXECUT_EXECUTIONNUMBERL
    return
    &lt;ns5:Execution>
      &lt;ns5:signature>{data($parm/NF_EXECUT_SIGNATURE[$occ])}&lt;/ns5:signature>
      &lt;ns5:executionNumberLPP>{data($parm/NF_EXECUT_EXECUTIONNUMBERL[$occ])}&lt;/ns5:executionNumberLPP>
      { insertDate(data($parm/NF_EXECUT_INCOMINGDATE[$occ]),"yyyy-MM-dd","ns5:incomingDate")}
      { insertDate(data($parm/NF_EXECUT_STARTEXECUTIONDA[$occ]),"yyyy-MM-dd","ns5:startExecutionDate")}
     &lt;/ns5:Execution>
  }
&lt;/ns4:executions>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse>
  {getElementsForExecutions($parm)}
  &lt;ns4:bcd>
    &lt;ns2:BusinessControlData>
      &lt;ns2:pageControl>
        &lt;ns6:PageControl>
          &lt;ns6:hasNext>{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns6:hasNext>
          &lt;ns6:navigationKeyDefinition>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition>
          &lt;ns6:navigationKeyValue>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue>
        &lt;/ns6:PageControl>
      &lt;/ns2:pageControl>
    &lt;/ns2:BusinessControlData>
  &lt;/ns4:bcd>
&lt;/ns4:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>