<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace wsdl="http://jv.adapter.cu.com.pl/cmf/wsdl-jv";
declare namespace cmf-jv="http://jv.adapter.cu.com.pl/cmf/jv";

declare variable $body as element(soap-env:Body) external;


   &lt;soap-env:Body>
      &lt;wsdl:updateClientResponse>
         &lt;client-code>{$body/FML32/CI_CIF_BACK}&lt;/client-code>
      &lt;/wsdl:updateClientResponse>
   &lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>