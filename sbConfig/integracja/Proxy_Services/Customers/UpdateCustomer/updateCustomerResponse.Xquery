<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";

declare function local:mapUpdateCustomerResponse($fml as element())
    as element(m:invokeResponse) {
        &lt;m:invokeResponse>
           { if($fml) then () else () }
        &lt;/m:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ local:mapUpdateCustomerResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>