<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";

declare function local:mapUpdateCustomerRequest($req as element())
    as element(fml:FML32) {
        &lt;fml:FML32>
           { if($req) then () else () }
        &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ local:mapUpdateCustomerRequest($body/m:invoke) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>