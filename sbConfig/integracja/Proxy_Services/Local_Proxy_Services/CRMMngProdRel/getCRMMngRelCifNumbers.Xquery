<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";


declare function xf:getCifs($req as element(m:CRMMngProdRelRequest))
	as element(Customers) {
		&lt;Customers>
                     {
                       for $el in $req/m:Paczka/m:Relacja/m:NumerKlienta
                         return
                             if  ( string-length(data($el)) > 0)
                                 then &lt;NumerKlienta>{data($el)}&lt;/NumerKlienta>
                                 else ()
                    }
		&lt;/Customers>
};

declare variable $body as element(soap-env:Body) external;

 xf:getCifs($body/m:CRMMngProdRelRequest)</con:xquery>
</con:xqueryEntry>