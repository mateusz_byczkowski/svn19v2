<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-17</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustomerProductDetailResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustomerProductDetailResponse) {
<m:CRMGetCustomerProductDetailResponse>
         <m:ProductCategoryId>101</m:ProductCategoryId>
         <m:ProductCategoryCode>101</m:ProductCategoryCode>
         <m:ProductNamePl>Karta kredytowa MasterCard PAYBACK</m:ProductNamePl>
         <m:ProductNameEn>brak</m:ProductNameEn>
         <m:ProductId>0</m:ProductId>
         <m:ProductCode>0</m:ProductCode>
         <m:Pojedynczy>N</m:Pojedynczy>
         <m:NrRachunku>545250******1927</m:NrRachunku>
         <m:KodRach>318297</m:KodRach>
         <m:NrAlternatywny>545250P001447252</m:NrAlternatywny>
         <m:ProductAtt1>MPBK</m:ProductAtt1>
         <m:ProductAtt2/>
         <m:ProductAtt3/>
         <m:ProductAtt4/>
         <m:ProductAtt5/>
         <m:RachunekAdrAlt>N</m:RachunekAdrAlt>
         <m:ProductStat>N</m:ProductStat>
         <m:ProductStatusCode>Aktywna</m:ProductStatusCode>
         <m:Saldo>-2058.38</m:Saldo>
         <m:DostSrodki>-58.38</m:DostSrodki>
         <m:Blokada>0.00</m:Blokada>
         <m:KodWaluty>PLN</m:KodWaluty>
         <m:DOtwarcia>27-07-2007</m:DOtwarcia>
         <m:DataOstOper>24-02-2011</m:DataOstOper>
         <m:Limit1>2000.00</m:Limit1>
         <m:ProductNameOryginal>MC PAYBACK</m:ProductNameOryginal>
         <m:Oproc1>21.00000</m:Oproc1>
         <m:Wlasciciel>1000737404</m:Wlasciciel>
         <m:OdsPopOkr>44.86</m:OdsPopOkr>
         <m:DOstZest>15-04-2011</m:DOstZest>
         <m:ZalMin>-194.51</m:ZalMin>
         <m:BiezMin>-142.67</m:BiezMin>
         <m:SplataMin>-337.18</m:SplataMin>
         <m:DataBPlat>02-05-2011</m:DataBPlat>
         <m:DNastZest>06-05-2011</m:DNastZest>
         <m:NrRach>4000000102061829</m:NrRach>
         <m:Karta>1</m:Karta>
         <m:KkExpDate>31-07-2011</m:KkExpDate>
         <m:SplataRazem>-2058.38</m:SplataRazem> 
         <m:CyklLimitu>Dzienny</m:CyklLimitu> 
         <m:LimitWyplatGotowki>2000.00</m:LimitWyplatGotowki> 
         <m:LimitWykorzystany>400.00</m:LimitWykorzystany> 
         <m:UbezpPlatne>Ubezpieczenie płatne 1</m:UbezpPlatne> 
         <m:StWznKarty>Nie wznawiać</m:StWznKarty> 
         <m:CyklNalOpl>Opłata miesięczna</m:CyklNalOpl> 
         <m:ObrotyKarty>1200.00</m:ObrotyKarty> 
         <m:DataZamkniecia>31-07-2011</m:DataZamkniecia> 
         <m:RodzajProduktu>0201</m:RodzajProduktu> 
</m:CRMGetCustomerProductDetailResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetCustomerProductDetailResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>