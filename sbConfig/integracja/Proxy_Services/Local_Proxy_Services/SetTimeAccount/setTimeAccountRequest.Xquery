<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-06</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="urn:filtersandmessages.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns3="urn:filterandmessages.dictionaries.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";


declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:mapSetTimeAccountRequest($req as element(ns1:Account), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

	let $msgId:= $msghead/dcl:msgId
	let $companyId:= $msghead/dcl:companyId
	let $userId := $msghead/dcl:userId
	let $appId:= $msghead/dcl:appId
	let $unitId := $msghead/dcl:unitId
	let $timestamp:= $msghead/dcl:timestamp
        let $transId:=$tranhead/dcl:transId

	let $customerNumber:= $req/ns1:customerNumber
        let $accountOpenDate:= $req/ns1:accountOpenDate
        let $creditPercentage:=$req/ns1:creditPrecentage
        let $skpOpener:=$req/ns1:skpOpener
        let $productCode:=$req/ns1:productCode
        let $officerCode:=$req/ns1:officerCode
        
        (: encja Time Account :)
        let $renewalFrequency:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:renewalFrequency
        let $originalAmount:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:originalAmount
        let $nextRenewalMaturityDate:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:nextRenewalMaturityDate
        let $dispositionCode:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:dispositionCode
        let $interestPaymentPeriod:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:interestPaymentPeriod/ns2:Period/ns2:period
        let $interestPaymentFrequency:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:interestPaymentFrequency
        let $nextInrterestPaymentDate:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:nextInrterestPaymentDate
        let $interestDisposition:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:interestDisposition
        let $penaultyPlan:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:penaultyPlan
        let $influenceWay:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:influenceWayas
        let $renewalId:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:renewalID/ns2:RenewalType/ns2:renewalType        
        let $renewalPeroid:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:renewalPeriod/ns2:Period/ns2:period
        let $competencyCode:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:competencyCode/ns3:CompetencyCode/ns3:competencyCode
        
       (: Encja statementParameters :)
        let $frequency:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:frequency
        let $nextPrintoutDate:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:nextPrintoutDate
        let $printFlag:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:printFlag
        let $cycle:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:cycle/ns2:Period/ns2:period
        let $provideManner:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:provideManner/ns2:ProvideManner/ns2:provideManner

	(: Encja addressAcount :)
        let $houseFlatNumber:=$req/ns1:addressAccount/ns4:Address/ns4:houseFlatNumber
        let $city:=$req/ns1:addressAccount/ns4:Address/ns4:city
        let $validFrom:=$req/ns1:addressAccount/ns4:Address/ns4:validFrom
        let $county:=$req/ns1:addressAccount/ns4:Address/ns4:county
        let $validTo:=$req/ns1:addressAccount/ns4:Address/ns4:validTo
        let $street:=$req/ns1:addressAccount/ns4:Address/ns4:street
        let $zipCode:=$req/ns1:addressAccount/ns4:Address/ns4:zipCode
        let $post:=$req/ns1:addressAccount/ns4:Address/ns4:post
        let $local:=$req/ns1:addressAccount/ns4:Address/ns4:local
            let $addressType:=$req/ns1:addressAccount/ns4:Address/ns4:addressType/ns2:AddressType/ns2:addressType
            let $state:=$req/ns1:addressAccount/ns4:Address/ns4:state/ns2:State/ns2:state

	(: Encja accountType :)
        let $accountType:=$req/ns1:accountType/ns2:AccountType/ns2:accountType
	(: Encja currency :)
        let $currency:=$req/ns1:currency/ns2:CurrencyCode/ns2:currencyCode
	(: Encja accountBranchNumber :)
        let $accountBranchNumber:=$req/ns1:accountBranchNumber/ns2:BranchCode/ns2:branchCode
	return
          &lt;fml:FML32>
			&lt;DC_MSHEAD_MSGID>{data($msgId)}&lt;/DC_MSHEAD_MSGID>
            &lt;DC_TRN_ID>{data($transId)}&lt;/DC_TRN_ID>
            &lt;DC_UZYTKOWNIK>{concat("SKP:",data($skpOpener))}&lt;/DC_UZYTKOWNIK>
            &lt;DC_ODDZIAL>{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL>
            &lt;DC_KOD_APLIKACJI>19&lt;/DC_KOD_APLIKACJI>
            &lt;DC_NUMER_KLIENTA>{data($customerNumber)}&lt;/DC_NUMER_KLIENTA>
            &lt;DC_RELACJA>SOW&lt;/DC_RELACJA>
            &lt;DC_TYP_RELACJI>1&lt;/DC_TYP_RELACJI>
            &lt;DC_PROCENT_ZOB_PODATKOWYCH>100.0000&lt;/DC_PROCENT_ZOB_PODATKOWYCH>

            &lt;DC_NUMER_PRODUKTU>{data($productCode)}&lt;/DC_NUMER_PRODUKTU>
            &lt;DC_NUMER_ODDZIALU>{data($accountBranchNumber)}&lt;/DC_NUMER_ODDZIALU>        
            &lt;DC_KWOTA>{data($originalAmount)}&lt;/DC_KWOTA>
            {
               if ( string-length($officerCode)>0 )
                 then &lt;DC_KOD_PRACOWNIKA>{data($officerCode)}&lt;/DC_KOD_PRACOWNIKA>
                 else   &lt;DC_KOD_PRACOWNIKA>DCL&lt;/DC_KOD_PRACOWNIKA>                 
             } 

             {
              if ($renewalId) 
                then &lt;DC_OPCJA_ODNAWIANIA>{data($renewalId)}&lt;/DC_OPCJA_ODNAWIANIA>
                else()
             }
             { 
              if ($interestDisposition)
                 then if (data($interestDisposition) = "true")
                     then &lt;DC_KOD_DYSPOZYCJI_ODSETKI>T&lt;/DC_KOD_DYSPOZYCJI_ODSETKI>
                     else &lt;DC_KOD_DYSPOZYCJI_ODSETKI>C&lt;/DC_KOD_DYSPOZYCJI_ODSETKI>
                 else()
             }
             { 
              if ($dispositionCode)
                 then if (data($dispositionCode) = "true")
                   then &lt;DC_KOD_DYSPOZYCJI_KAPITAL>T&lt;/DC_KOD_DYSPOZYCJI_KAPITAL>
                   else &lt;DC_KOD_DYSPOZYCJI_KAPITAL>N&lt;/DC_KOD_DYSPOZYCJI_KAPITAL>
                 else()
             }
             {
              if ($competencyCode) 
                then &lt;DC_KOD_KOMPETENCJI>{data($competencyCode)}&lt;/DC_KOD_KOMPETENCJI>
                else()
             }
             { 

              if ($printFlag)
                 then  if (data($printFlag) = "true")
                   then &lt;DC_ZNACZNIK_WYCIAGU_BRAKU_AKT>Y&lt;/DC_ZNACZNIK_WYCIAGU_BRAKU_AKT>
                   else &lt;DC_ZNACZNIK_WYCIAGU_BRAKU_AKT>N&lt;/DC_ZNACZNIK_WYCIAGU_BRAKU_AKT>
                 else()
             }
             { 

              if ($printFlag)
                 then  if (data($printFlag) = "true")
                   then &lt;DC_KOD_PRZYGOTOWANIA_WYCIAGU>Y&lt;/DC_KOD_PRZYGOTOWANIA_WYCIAGU>
                   else &lt;DC_KOD_PRZYGOTOWANIA_WYCIAGU>N&lt;/DC_KOD_PRZYGOTOWANIA_WYCIAGU>
                 else()
             }
             {
              if ($cycle) 
                then &lt;DC_KOD_CYKLU_WYCIAGOW>{data($cycle)}&lt;/DC_KOD_CYKLU_WYCIAGOW>
                else()
             }
             {
              if ($frequency) 
                then &lt;DC_LICZBA_OKRESOW_CYKL_WYCIAG>{data($frequency)}&lt;/DC_LICZBA_OKRESOW_CYKL_WYCIAG>
                else()
             }
             {
              if ($nextPrintoutDate) 
                then &lt;DC_DATA_NASTEPNEGO_WYCIAGU>{data($nextPrintoutDate)}&lt;/DC_DATA_NASTEPNEGO_WYCIAGU>
                else()
             }
             {
              if ($provideManner) 
                then &lt;DC_KOD_SPECJALNYCH_INSTRUKCJI>{data($provideManner)}&lt;/DC_KOD_SPECJALNYCH_INSTRUKCJI>
                else()
             }
             {
              if ($penaultyPlan) 
                then &lt;DC_NUMER_KARY>{data($penaultyPlan)}&lt;/DC_NUMER_KARY>
                else()
             }
             {
              if ($renewalPeroid) 
                then &lt;DC_CZEST_OKRES_SD_OKRES>{upper-case(substring(data($renewalPeroid),1,1))}&lt;/DC_CZEST_OKRES_SD_OKRES>
                else()
             }
             {
              if ($renewalFrequency) 
                then &lt;DC_CZEST_OKRES_SD_CZEST>{data($renewalFrequency)}&lt;/DC_CZEST_OKRES_SD_CZEST>
                else()
             }
           (: &lt;DC_CZEST_OKRES_SD_DZIEN>&lt;/DC_CZEST_OKRES_SD_DZIEN> :)
             {
              if ($nextRenewalMaturityDate) 
                then &lt;DC_DATA_NAST_ODNOWIENIA_WYMAG>{fn-bea:date-to-string-with-format("dd-MM-yyyy",data($nextRenewalMaturityDate))}&lt;/DC_DATA_NAST_ODNOWIENIA_WYMAG>
                else()
             }
             {
              if ($interestPaymentPeriod) 
                then &lt;DC_OKRES_NALICZENIA>{upper-case(substring(data($interestPaymentPeriod),1,1))}&lt;/DC_OKRES_NALICZENIA>
                else()
             }
             {
              if ($interestPaymentFrequency) 
                then &lt;DC_CZESTOT_NALICZENIA>{data($interestPaymentFrequency)}&lt;/DC_CZESTOT_NALICZENIA>
                else()
             }

             {
              if ($nextInrterestPaymentDate) 
                then &lt;DC_DATA_NAST_PLATNOSCI_ODSETE>{fn-bea:date-to-string-with-format("dd-MM-yyyy",data($nextInrterestPaymentDate))}&lt;/DC_DATA_NAST_PLATNOSCI_ODSETE>
                else()
             }
             {
              if ($creditPercentage) 
                then &lt;DC_STOPA_PROCENTOWA>{data($creditPercentage)}&lt;/DC_STOPA_PROCENTOWA>
                else()
             }
             {
              if ($accountOpenDate) 
                then &lt;DC_DATA_OTWARCIA_RACHUNKU>{fn-bea:date-to-string-with-format("dd-MM-yyyy",data($accountOpenDate))}&lt;/DC_DATA_OTWARCIA_RACHUNKU>
                else()
             }
            (:&lt;DC_IMIE_I_NAZWISKO_ALT>{data()}&lt;/DC_IMIE_I_NAZWISKO_ALT>:)
            (:&lt;DC_IMIE_I_NAZWISKO_ALT_C_D>{data()}&lt;/DC_IMIE_I_NAZWISKO_ALT_C_D>:)
             {
              if ($street) 
                then &lt;DC_ULICA_ADRES_ALT>{data($street)}&lt;/DC_ULICA_ADRES_ALT>
                else()
             }
             {
              if ($zipCode) 
                then &lt;DC_KOD_POCZTOWY_ADRES_ALT>{data($zipCode)}&lt;/DC_KOD_POCZTOWY_ADRES_ALT>
                else()
             }
             {
              if ($city) 
                then &lt;DC_MIASTO_ADRES_ALT>{data($city)}&lt;/DC_MIASTO_ADRES_ALT>
                else()
             }
             {
              if ($houseFlatNumber) 
                then &lt;DC_NR_POSESJI_LOKALU_ADRES_ALT>{data($houseFlatNumber)}&lt;/DC_NR_POSESJI_LOKALU_ADRES_ALT>
                else()
             }
             {
              if ($state or $county) 
                then &lt;DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{concat($state,'/',$county)}&lt;/DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
                else()
             }
             {
              if ($validFrom) 
                then &lt;DC_DATA_WPROWADZENIA>{data($validFrom)}&lt;/DC_DATA_WPROWADZENIA>
                else()
             }
             {
              if ($validTo) 
                then &lt;DC_DATA_KONCOWA>{data($validTo)}&lt;/DC_DATA_KONCOWA>
                else()
             }
             {
              if ($local) 
                then &lt;DC_TYP_ADRESU>{data($local)}&lt;/DC_TYP_ADRESU>
                else()
             }
       (:      {
               if ( string-length($validFrom)>0 )
                 then &lt;DC_RODZAJ_RACHUNKU>TM&lt;/DC_RODZAJ_RACHUNKU>
                 else()
             } :)               

          &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


&lt;soap-env:Body>
{ xf:mapSetTimeAccountRequest($body/dcl:invoke/dcl:accountIn/ns1:Account, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>