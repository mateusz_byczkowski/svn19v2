<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-24</con:description>
  <con:xquery><![CDATA[<S:Body  xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"> 
 <ns2:GetTransactionsByVpanResponse  xmlns:ns3="http://bzwbk.com/services/cortex/faults" xmlns:ns2="http://bzwbk.com/services/cortex"> 
 <return> 
 <pagecHasNext>1</pagecHasNext> 
 <pagecNavigationKeyValue>1</pagecNavigationKeyValue> 
 <pagecOperations>5</pagecOperations> 
 <pagecPagesCount>4</pagecPagesCount> 
 <tlogList> 
 <acnum1>37109018380000000106687464</acnum1> 
 <acnum2> </acnum2> 
 <acqcountry>276</acqcountry> 
 <actioncode>1</actioncode> 
 <actioncodeDescr/> 
 <afe>VISA</afe> 
 <aiid>490763</aiid> 
 <amtTraded>42.839999999999996</amtTraded> 
 <amtbill>42.839999999999996</amtbill> 
 <amtbillcb>0.0</amtbillcb> 
 <amtcbTraded>0.0</amtcbTraded> 
 <amtorgTraded>0.0</amtorgTraded> 
 <amtset>0.0</amtset> 
 <amttxn>42.839999999999996</amttxn> 
 <amttxnAuth>0.0</amttxnAuth> 
 <amttxncb>0.0</amttxncb> 
 <amttxncbAuth>0.0</amttxncbAuth> 
 <amttxnorg>0.0</amttxnorg> 
 <aprvlcode/> 
 <arn> </arn> 
 <crdacptbus>7995</crdacptbus> 
 <crdacptid>4556301175 </crdacptid> 
 <crdacptloc> 
 payments.bwin.com~~Gibraltar~ 292 
 </crdacptloc> 
 <crdacptlocCity> </crdacptlocCity> 
 <crdacptlocCountry/> 
 <crdacptlocName> </crdacptlocName> 
 <crdacptlocPostcode> </crdacptlocPostcode> 
 <crdacptlocRegion> </crdacptlocRegion> 
 <crdacptlocStreet> </crdacptlocStreet> 
 <crdproduct>ELES</crdproduct> 
 <crdseqno>0</crdseqno> 
 <ctxdate>2009-02-15T00:00:00+01:00</ctxdate> 
 <curTraded>985</curTraded> 
 <curbill>985</curbill> 
 <curbillAlphacode>PLN</curbillAlphacode> 
 <curset/> 
 <cursetAlphacode/> 
 <curtxn>985</curtxn> 
 <curtxnAlphacode>PLN</curtxnAlphacode> 
 <curtxnorg/> 
 <curtxnorgAlphacode/> 
 <dateexp>1103</dateexp> 
 <datelocal>2009-02-14T00:00:00+01:00</datelocal> 
 <dateset>2263-08-31T00:00:00+02:00</dateset> 
 <datexmit>2009-02-14T00:00:00+01:00</datexmit> 
 <datexmitorg>2263-08-31T00:00:00+02:00</datexmitorg> 
 <fncode>100</fncode> 
 <fncodeDescr>Autoryzacja transakcji </fncodeDescr> 
 <id>1505951158</id> 
 <ife>WBK</ife> 
 <msgcls>1</msgcls> 
 <msgclsorg/> 
 <msgfn>0</msgfn> 
 <msgfnorg/> 
 <outbtchid>0</outbtchid> 
 <outbtchtype>151</outbtchtype> 
 <panDisplay>477915_4557</panDisplay> 
 <poscap> </poscap> 
 <poscc89>59</poscc89> 
 <poscdic> </poscdic> 
 <poscdim>V</poscdim> 
 <poscha/> 
 <poschac>0</poschac> 
 <poschad/> 
 <poscham>9</poscham> 
 <poschic>0</poschic> 
 <poschp>5</poschp> 
 <poscp>0</poscp> 
 <poscrc>0</poscrc> 
 <posoe/> 
 <pospcc/> 
 <possd/> 
 <ratebill>1.0</ratebill> 
 <rateset>0.0</rateset> 
 <reasoncode>0</reasoncode> 
 <rejreason>655</rejreason> 
 <rrn>904514388685</rrn> 
 <rspcode>16</rspcode> 
 <rspsrc>I</rspsrc> 
 <stan>388685</stan> 
 <stanorg>0</stanorg> 
 <termcode>K3600002</termcode> 
 <termseq>0</termseq> 
 <termtype>0</termtype> 
 <timelocal>141155</timelocal> 
 <timexmit>141155</timexmit> 
 <timexmitorg>0</timexmitorg> 
 <title>Autoryzacja transakcji KASYNO </title> 
 <txncode>11</txncode> 
 <txncodeDescr>KASYNO</txncodeDescr> 
 <txnsrc>0</txnsrc> 
 <txnsrcorg/> 
 <txnstatus>5</txnstatus> 
 <txnstatusDescr> 
 Transakcja odrzucona - brak wystarczających środków 
 </txnstatusDescr> 
 <vpandet> 
 <vpan>477915I001525005</vpan> 
 </vpandet> 
 </tlogList> 
 <tlogList> 
 <acnum1>37109018380000000106687464</acnum1> 
 <acnum2> </acnum2> 
 <acqcountry>276</acqcountry> 
 <actioncode>0</actioncode> 
 <actioncodeDescr/> 
 <afe>VISA</afe> 
 <aiid>490763</aiid> 
 <amtTraded>42.839999999999996</amtTraded> 
 <amtbill>42.839999999999996</amtbill> 
 <amtbillcb>0.0</amtbillcb> 
 <amtcbTraded>0.0</amtcbTraded> 
 <amtorgTraded>0.0</amtorgTraded> 
 <amtset>0.0</amtset> 
 <amttxn>42.839999999999996</amttxn> 
 <amttxnAuth>0.0</amttxnAuth> 
 <amttxncb>0.0</amttxncb> 
 <amttxncbAuth>0.0</amttxncbAuth> 
 <amttxnorg>0.0</amttxnorg> 
 <aprvlcode>085203</aprvlcode> 
 <arn> </arn> 
 <crdacptbus>7995</crdacptbus> 
 <crdacptid>4556301175 </crdacptid> 
 <crdacptloc> 
 payments.bwin.com~~Gibraltar~ 292 
 </crdacptloc> 
 <crdacptlocCity> </crdacptlocCity> 
 <crdacptlocCountry/> 
 <crdacptlocName> </crdacptlocName> 
 <crdacptlocPostcode> </crdacptlocPostcode> 
 <crdacptlocRegion> </crdacptlocRegion> 
 <crdacptlocStreet> </crdacptlocStreet> 
 <crdproduct>ELES</crdproduct> 
 <crdseqno>0</crdseqno> 
 <ctxdate>2009-02-11T00:00:00+01:00</ctxdate> 
 <curTraded>985</curTraded> 
 <curbill>985</curbill> 
 <curbillAlphacode>PLN</curbillAlphacode> 
 <curset/> 
 <cursetAlphacode/> 
 <curtxn>985</curtxn> 
 <curtxnAlphacode>PLN</curtxnAlphacode> 
 <curtxnorg/> 
 <curtxnorgAlphacode/> 
 <dateexp>1103</dateexp> 
 <datelocal>2009-02-10T00:00:00+01:00</datelocal> 
 <dateset>2263-08-31T00:00:00+02:00</dateset> 
 <datexmit>2009-02-10T00:00:00+01:00</datexmit> 
 <datexmitorg>2263-08-31T00:00:00+02:00</datexmitorg> 
 <fncode>100</fncode> 
 <fncodeDescr>Autoryzacja transakcji </fncodeDescr> 
 <id>1501361125</id> 
 <ife>WBK</ife> 
 <msgcls>1</msgcls> 
 <msgclsorg/> 
 <msgfn>0</msgfn> 
 <msgfnorg/> 
 <outbtchid>1</outbtchid> 
 <outbtchtype>151</outbtchtype> 
 <panDisplay>477915_4557</panDisplay> 
 <poscap> </poscap> 
 <poscc89>59</poscc89> 
 <poscdic> </poscdic> 
 <poscdim>V</poscdim> 
 <poscha/> 
 <poschac>0</poschac> 
 <poschad/> 
 <poscham>9</poscham> 
 <poschic>0</poschic> 
 <poschp>5</poschp> 
 <poscp>0</poscp> 
 <poscrc>0</poscrc> 
 <posoe/> 
 <pospcc/> 
 <possd/> 
 <ratebill>1.0</ratebill> 
 <rateset>0.0</rateset> 
 <reasoncode>0</reasoncode> 
 <rejreason>0</rejreason> 
 <rrn>904120313213</rrn> 
 <rspcode>00</rspcode> 
 <rspsrc>H</rspsrc> 
 <stan>313213</stan> 
 <stanorg>0</stanorg> 
 <termcode>K3600002</termcode> 
 <termseq>0</termseq> 
 <termtype>0</termtype> 
 <timelocal>205205</timelocal> 
 <timexmit>205205</timexmit> 
 <timexmitorg>0</timexmitorg> 
 <title>Autoryzacja transakcji KASYNO </title> 
 <txncode>11</txncode> 
 <txncodeDescr>KASYNO</txncodeDescr> 
 <txnsrc>0</txnsrc> 
 <txnsrcorg/> 
 <txnstatus>7</txnstatus> 
 <txnstatusDescr>Transakcja zaakceptowana</txnstatusDescr> 
 <vpandet> 
 <vpan>477915I001525005</vpan> 
 </vpandet> 
 </tlogList> 
 <tlogList> 
 <acnum1>37109018380000000106687464</acnum1> 
 <acnum2> </acnum2> 
 <acqcountry>756</acqcountry> 
 <actioncode>1</actioncode> 
 <actioncodeDescr/> 
 <afe>VISA</afe> 
 <aiid>424500</aiid> 
 <amtTraded>10.25</amtTraded> 
 <amtbill>46.64</amtbill> 
 <amtbillcb>0.0</amtbillcb> 
 <amtcbTraded>0.0</amtcbTraded> 
 <amtorgTraded>0.0</amtorgTraded> 
 <amtset>0.0</amtset> 
 <amttxn>10.25</amttxn> 
 <amttxnAuth>0.0</amttxnAuth> 
 <amttxncb>0.0</amttxncb> 
 <amttxncbAuth>0.0</amttxncbAuth> 
 <amttxnorg>0.0</amttxnorg> 
 <aprvlcode>046470</aprvlcode> 
 <arn> </arn> 
 <crdacptbus>7995</crdacptbus> 
 <crdacptid>500010986 </crdacptid> 
 <crdacptloc>BET-AT-HOME.COM~~MALTA~ 470</crdacptloc> 
 <crdacptlocCity> </crdacptlocCity> 
 <crdacptlocCountry/> 
 <crdacptlocName> </crdacptlocName> 
 <crdacptlocPostcode> </crdacptlocPostcode> 
 <crdacptlocRegion> </crdacptlocRegion> 
 <crdacptlocStreet> </crdacptlocStreet> 
 <crdproduct>ELES</crdproduct> 
 <crdseqno>0</crdseqno> 
 <ctxdate>2009-02-11T00:00:00+01:00</ctxdate> 
 <curTraded>978</curTraded> 
 <curbill>985</curbill> 
 <curbillAlphacode>PLN</curbillAlphacode> 
 <curset/> 
 <cursetAlphacode/> 
 <curtxn>978</curtxn> 
 <curtxnAlphacode>EUR</curtxnAlphacode> 
 <curtxnorg/> 
 <curtxnorgAlphacode/> 
 <dateexp>1103</dateexp> 
 <datelocal>2009-02-10T00:00:00+01:00</datelocal> 
 <dateset>2263-08-31T00:00:00+02:00</dateset> 
 <datexmit>2009-02-10T00:00:00+01:00</datexmit> 
 <datexmitorg>2263-08-31T00:00:00+02:00</datexmitorg> 
 <fncode>100</fncode> 
 <fncodeDescr>Autoryzacja transakcji </fncodeDescr> 
 <id>1501362206</id> 
 <ife>WBK</ife> 
 <msgcls>1</msgcls> 
 <msgclsorg/> 
 <msgfn>0</msgfn> 
 <msgfnorg/> 
 <outbtchid>0</outbtchid> 
 <outbtchtype>151</outbtchtype> 
 <panDisplay>477915_4557</panDisplay> 
 <poscap> </poscap> 
 <poscc89>59</poscc89> 
 <poscdic> </poscdic> 
 <poscdim>T</poscdim> 
 <poscha/> 
 <poschac>0</poschac> 
 <poschad/> 
 <poscham>9</poscham> 
 <poschic>1</poschic> 
 <poschp>5</poschp> 
 <poscp>0</poscp> 
 <poscrc>0</poscrc> 
 <posoe/> 
 <pospcc/> 
 <possd/> 
 <ratebill>4.550244</ratebill> 
 <rateset>0.0</rateset> 
 <reasoncode>0</reasoncode> 
 <rejreason>0</rejreason> 
 <rrn>204121315051</rrn> 
 <rspcode>16</rspcode> 
 <rspsrc>H</rspsrc> 
 <stan>967786</stan> 
 <stanorg>0</stanorg> 
 <termcode>00000000</termcode> 
 <termseq>0</termseq> 
 <termtype>0</termtype> 
 <timelocal>205659</timelocal> 
 <timexmit>205659</timexmit> 
 <timexmitorg>0</timexmitorg> 
 <title>Autoryzacja transakcji KASYNO </title> 
 <txncode>11</txncode> 
 <txncodeDescr>KASYNO</txncodeDescr> 
 <txnsrc>0</txnsrc> 
 <txnsrcorg/> 
 <txnstatus>5</txnstatus> 
 <txnstatusDescr> 
 Transakcja odrzucona - brak wystarczających środków 
 </txnstatusDescr> 
 <vpandet> 
 <vpan>477915I001525005</vpan> 
 </vpandet> 
 </tlogList> 
 <tlogList> 
 <acnum1>37109018380000000106687464</acnum1> 
 <acnum2> </acnum2> 
 <acqcountry/> 
 <actioncode>0</actioncode> 
 <actioncodeDescr/> 
 <afe>VISA</afe> 
 <aiid>424500</aiid> 
 <amtTraded>0.0</amtTraded> 
 <amtbill>48.78</amtbill> 
 <amtbillcb>0.0</amtbillcb> 
 <amtcbTraded>0.0</amtcbTraded> 
 <amtorgTraded>0.0</amtorgTraded> 
 <amtset>10.52</amtset> 
 <amttxn>10.52</amttxn> 
 <amttxnAuth>0.0</amttxnAuth> 
 <amttxncb>0.0</amttxncb> 
 <amttxncbAuth>0.0</amttxncbAuth> 
 <amttxnorg>0.0</amttxnorg> 
 <aprvlcode>055852</aprvlcode> 
 <arn> </arn> 
 <crdacptbus>7995</crdacptbus> 
 <crdacptid>500010986 </crdacptid> 
 <crdacptloc>bet-at-home.com~~Malta~00000 470</crdacptloc> 
 <crdacptlocCity> </crdacptlocCity> 
 <crdacptlocCountry/> 
 <crdacptlocName> </crdacptlocName> 
 <crdacptlocPostcode> </crdacptlocPostcode> 
 <crdacptlocRegion> </crdacptlocRegion> 
 <crdacptlocStreet> </crdacptlocStreet> 
 <crdproduct> </crdproduct> 
 <crdseqno>0</crdseqno> 
 <ctxdate>2009-02-11T00:00:00+01:00</ctxdate> 
 <curTraded/> 
 <curbill>985</curbill> 
 <curbillAlphacode>PLN</curbillAlphacode> 
 <curset>978</curset> 
 <cursetAlphacode>EUR</cursetAlphacode> 
 <curtxn>978</curtxn> 
 <curtxnAlphacode>EUR</curtxnAlphacode> 
 <curtxnorg/> 
 <curtxnorgAlphacode/> 
 <dateexp/> 
 <datelocal>2009-02-09T00:00:00+01:00</datelocal> 
 <dateset>2009-02-11T00:00:00+01:00</dateset> 
 <datexmit>2009-02-09T00:00:00+01:00</datexmit> 
 <datexmitorg>2263-08-31T00:00:00+02:00</datexmitorg> 
 <fncode>200</fncode> 
 <fncodeDescr>Rozliczenie transakcji </fncodeDescr> 
 <id>1501716261</id> 
 <ife>WBK</ife> 
 <msgcls>2</msgcls> 
 <msgclsorg/> 
 <msgfn>2</msgfn> 
 <msgfnorg/> 
 <outbtchid>1</outbtchid> 
 <outbtchtype>151</outbtchtype> 
 <panDisplay>477915_7000</panDisplay> 
 <poscap> </poscap> 
 <poscc89>0</poscc89> 
 <poscdic> </poscdic> 
 <poscdim>T</poscdim> 
 <poscha>0</poscha> 
 <poschac>0</poschac> 
 <poschad>0</poschad> 
 <poscham>0</poscham> 
 <poschic>1</poschic> 
 <poschp>5</poschp> 
 <poscp>0</poscp> 
 <poscrc>0</poscrc> 
 <posoe>1</posoe> 
 <pospcc>0</pospcc> 
 <possd>0</possd> 
 <ratebill>4.6369</ratebill> 
 <rateset>0.0</rateset> 
 <reasoncode>0</reasoncode> 
 <rejreason>0</rejreason> 
 <rrn>902090099438</rrn> 
 <rspcode>00</rspcode> 
 <rspsrc/> 
 <stan>810578</stan> 
 <stanorg>0</stanorg> 
 <termcode>00000000</termcode> 
 <termseq>0</termseq> 
 <termtype>0</termtype> 
 <timelocal>0</timelocal> 
 <timexmit>0</timexmit> 
 <timexmitorg>0</timexmitorg> 
 <title>Rozliczenie transakcji ZAKUPY </title> 
 <txncode>0</txncode> 
 <txncodeDescr>ZAKUPY</txncodeDescr> 
 <txnsrc>0</txnsrc> 
 <txnsrcorg/> 
 <txnstatus>11</txnstatus> 
 <txnstatusDescr>Rozliczenie transakcji</txnstatusDescr> 
 <vpandet> 
 <vpan>477915I001525005</vpan> 
 </vpandet> 
 </tlogList> 
 <tlogList> 
 <acnum1>37109018380000000106687464</acnum1> 
 <acnum2> </acnum2> 
 <acqcountry>756</acqcountry> 
 <actioncode>1</actioncode> 
 <actioncodeDescr/> 
 <afe>VISA</afe> 
 <aiid>424500</aiid> 
 <amtTraded>11.190000000000001</amtTraded> 
 <amtbill>52.06</amtbill> 
 <amtbillcb>0.0</amtbillcb> 
 <amtcbTraded>0.0</amtcbTraded> 
 <amtorgTraded>0.0</amtorgTraded> 
 <amtset>0.0</amtset> 
 <amttxn>11.190000000000001</amttxn> 
 <amttxnAuth>0.0</amttxnAuth> 
 <amttxncb>0.0</amttxncb> 
 <amttxncbAuth>0.0</amttxncbAuth> 
 <amttxnorg>0.0</amttxnorg> 
 <aprvlcode>083970</aprvlcode> 
 <arn> </arn> 
 <crdacptbus>7995</crdacptbus> 
 <crdacptid>500010986 </crdacptid> 
 <crdacptloc>BET-AT-HOME.COM~~MALTA~ 470</crdacptloc> 
 <crdacptlocCity> </crdacptlocCity> 
 <crdacptlocCountry/> 
 <crdacptlocName> </crdacptlocName> 
 <crdacptlocPostcode> </crdacptlocPostcode> 
 <crdacptlocRegion> </crdacptlocRegion> 
 <crdacptlocStreet> </crdacptlocStreet> 
 <crdproduct>ELES</crdproduct> 
 <crdseqno>0</crdseqno> 
 <ctxdate>2009-02-09T00:00:00+01:00</ctxdate> 
 <curTraded>978</curTraded> 
 <curbill>985</curbill> 
 <curbillAlphacode>PLN</curbillAlphacode> 
 <curset/> 
 <cursetAlphacode/> 
 <curtxn>978</curtxn> 
 <curtxnAlphacode>EUR</curtxnAlphacode> 
 <curtxnorg/> 
 <curtxnorgAlphacode/> 
 <dateexp>1103</dateexp> 
 <datelocal>2009-02-09T00:00:00+01:00</datelocal> 
 <dateset>2263-08-31T00:00:00+02:00</dateset> 
 <datexmit>2009-02-09T00:00:00+01:00</datexmit> 
 <datexmitorg>2263-08-31T00:00:00+02:00</datexmitorg> 
 <fncode>100</fncode> 
 <fncodeDescr>Autoryzacja transakcji </fncodeDescr> 
 <id>1499562919</id> 
 <ife>WBK</ife> 
 <msgcls>1</msgcls> 
 <msgclsorg/> 
 <msgfn>0</msgfn> 
 <msgfnorg/> 
 <outbtchid>0</outbtchid> 
 <outbtchtype>151</outbtchtype> 
 <panDisplay>477915_4557</panDisplay> 
 <poscap> </poscap> 
 <poscc89>59</poscc89> 
 <poscdic> </poscdic> 
 <poscdim>T</poscdim> 
 <poscha/> 
 <poschac>0</poschac> 
 <poschad/> 
 <poscham>9</poscham> 
 <poschic>1</poschic> 
 <poschp>5</poschp> 
 <poscp>0</poscp> 
 <poscrc>0</poscrc> 
 <posoe/> 
 <pospcc/> 
 <possd/> 
 <ratebill>4.652368</ratebill> 
 <rateset>0.0</rateset> 
 <reasoncode>0</reasoncode> 
 <rejreason>0</rejreason> 
 <rrn>204013160817</rrn> 
 <rspcode>16</rspcode> 
 <rspsrc>H</rspsrc> 
 <stan>44761</stan> 
 <stanorg>0</stanorg> 
 <termcode>00000000</termcode> 
 <termseq>0</termseq> 
 <termtype>0</termtype> 
 <timelocal>125506</timelocal> 
 <timexmit>125506</timexmit> 
 <timexmitorg>0</timexmitorg> 
 <title>Autoryzacja transakcji KASYNO </title> 
 <txncode>11</txncode> 
 <txncodeDescr>KASYNO</txncodeDescr> 
 <txnsrc>0</txnsrc> 
 <txnsrcorg/> 
 <txnstatus>5</txnstatus> 
 <txnstatusDescr> 
 Transakcja odrzucona - brak wystarczających środków 
 </txnstatusDescr> 
 <vpandet> 
 <vpan>477915I001525005</vpan> 
 </vpandet> 
 </tlogList> 
 </return> 
 </ns2:GetTransactionsByVpanResponse> 
 </S:Body>]]></con:xquery>
</con:xqueryEntry>