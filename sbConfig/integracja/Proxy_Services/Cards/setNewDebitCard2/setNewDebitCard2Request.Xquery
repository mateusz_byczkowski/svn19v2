<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-07-05</con:description>
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:loyaltyprogram.entities.be.dcl";
declare namespace ns8="urn:be.services.dcl";
declare namespace ns9="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm = "1") then $trueval
    else $falseval
};

declare function iban2short ($num as xs:string) as xs:string{
(: dlugosc pola  z numerem rachunku do transferu odsetek:)
     let $numLength:=string-length(fn-bea:trim(data($num)))
     return   
        if ($numLength>12)
               then substring(fn-bea:trim(data($num )),$numLength - 11,12)
               else data($num )
};

declare function convertCycleLimit ($cycleLimit as xs:string?) as xs:string{
    if ($cycleLimit)
       then if (data($cycleLimit) = "M" or data($cycleLimit) = "m")
            then "83"
            else if (data($cycleLimit) = "D" or data($cycleLimit) = "d")
                 then "01"
                 else "00"
       else "00"
};


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function getFieldsFromHeader($parm as element(ns8:header)) as element()*
{
&lt;DC_MSHEAD_MSGID?>{data($parm/ns8:msgHeader/ns8:msgId)}&lt;/DC_MSHEAD_MSGID>,
&lt;DC_ODDZIAL?>{data($parm/ns8:msgHeader/ns8:unitId)}&lt;/DC_ODDZIAL>,
&lt;DC_UZYTKOWNIK?>{concat("SKP:",data($parm/ns8:msgHeader/ns8:userId))}&lt;/DC_UZYTKOWNIK>,
&lt;DC_TRN_ID?>{data($parm/ns8:transHeader/ns8:transId)}&lt;/DC_TRN_ID>
};

declare function getFieldsFromInvoke($parm as element(ns8:invoke)) as element()*
{
&lt;DC_NR_KARTY_BIN?>{data($parm/ns8:card/ns9:Card/ns9:bin)}&lt;/DC_NR_KARTY_BIN>,
&lt;DC_TYP_KARTY?>{data($parm/ns8:card/ns9:Card/ns9:cardType)}&lt;/DC_TYP_KARTY>,
&lt;DC_WYTLOCZONE_NA_KARCIE_I?>{data($parm/ns8:card/ns9:Card/ns9:embossName1)}&lt;/DC_WYTLOCZONE_NA_KARCIE_I>,
&lt;DC_WYTLOCZONE_NA_KARCIE_II?>{data($parm/ns8:card/ns9:Card/ns9:embossName2)}&lt;/DC_WYTLOCZONE_NA_KARCIE_II>,
&lt;DC_IDENTYFIKATOR_PIN?>{data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:pinMailer)}&lt;/DC_IDENTYFIKATOR_PIN>,
&lt;DC_UMORZYC_OPLATE?>{boolean2SourceValue(data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:processingOptionFlag3),"1","0")}&lt;/DC_UMORZYC_OPLATE>,
&lt;DC_STATUS_UMOWY?>{boolean2SourceValue(data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:contract),"1","0")}&lt;/DC_STATUS_UMOWY>,
&lt;DC_LIMIT_KARTY?>{data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:cashLimit)}&lt;/DC_LIMIT_KARTY>,
&lt;DC_EKSPRES?>{boolean2SourceValue(data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:express),"1","0")}&lt;/DC_EKSPRES>,
&lt;DC_ZALOZYC_POLISE?>{boolean2SourceValue(data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:policy),"1","0")}&lt;/DC_ZALOZYC_POLISE>,
&lt;DC_CYKL_ZESTAWIEN?>{boolean2SourceValue(data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:individualTranList),"30","0")}&lt;/DC_CYKL_ZESTAWIEN>,
&lt;DC_ZEST_ZBIORCZE?>{boolean2SourceValue(data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:collectiveTranList),"1","0")}&lt;/DC_ZEST_ZBIORCZE>,
&lt;DC_TECH_PIN?>{boolean2SourceValue(data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:pinMailerTech),"1","0")}&lt;/DC_TECH_PIN>,
&lt;DC_RACHUNEK?>{iban2short (data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:tranAccount/ns2:TranAccount/ns2:account/ns2:Account/ns2:accountNumber))}&lt;/DC_RACHUNEK>,
&lt;DC_TYP_ADRESU?>{data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:addressFlag/ns1:CrdAddressFlag/ns1:crdAddressFlag)}&lt;/DC_TYP_ADRESU>,
&lt;DC_CYKL_LIMITU?>{convertCycleLimit($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:cycleLimit/ns1:CrdCycleLimit/ns1:crdCycleLimit)}&lt;/DC_CYKL_LIMITU>,
&lt;DC_NUMER_ODDZIALU?>{data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:cardAddressBranchNumber/ns4:BranchCode/ns4:branchCode)}&lt;/DC_NUMER_ODDZIALU>,
&lt;DC_STATUS_KARTY?>{data($parm/ns8:card/ns9:Card/ns9:debitCard/ns9:DebitCard/ns9:cardStatus/ns1:CrdStatus/ns1:crdStatus)}&lt;/DC_STATUS_KARTY>,
&lt;DC_NUMER_KLIENTA?>{data($parm/ns8:card/ns9:Card/ns9:customer/ns3:Customer/ns3:customerNumber)}&lt;/DC_NUMER_KLIENTA>,
&lt;DC_NR_PB2?>{data($parm/ns8:card/ns9:Card/ns9:cardLpInfo/ns9:CardLpInfo/ns9:pb2)}&lt;/DC_NR_PB2>,
&lt;DC_CT_EXT_STATUS?>{data($parm/ns8:card/ns9:Card/ns9:cardLpInfo/ns9:CardLpInfo/ns9:cardLpStatus/ns1:CardLpStatus/ns1:cardLpStatus)}&lt;/DC_CT_EXT_STATUS>,
&lt;DC_TERMINAL_ID?>{data($parm/ns8:loginMsg/ns0:LoginMsg/ns0:terminalId)}&lt;/DC_TERMINAL_ID>,
&lt;DC_FLAGA_ZGODY?>{boolean2SourceValue(data($parm/ns8:loyaltyProgram/ns7:LoyaltyProgram/ns7:forwardingDataApproval),"1","0")}&lt;/DC_FLAGA_ZGODY>,
&lt;DC_FLAGA_MARKETINGU?>{boolean2SourceValue(data($parm/ns8:loyaltyProgram/ns7:LoyaltyProgram/ns7:processingApproval),"1","0")}&lt;/DC_FLAGA_MARKETINGU>,
&lt;DC_ZRODLO?>{data($parm/ns8:loyaltyProgram/ns7:LoyaltyProgram/ns7:applicationSource)}&lt;/DC_ZRODLO>,
&lt;DC_JEDNOSTKA_SPRZEDAJACA?>{data($parm/ns8:loyaltyProgram/ns7:LoyaltyProgram/ns7:unitApplicationSource)}&lt;/DC_JEDNOSTKA_SPRZEDAJACA>,
&lt;DC_PROMOCJA>2&lt;/DC_PROMOCJA>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns8:header)}
    {getFieldsFromInvoke($body/ns8:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>