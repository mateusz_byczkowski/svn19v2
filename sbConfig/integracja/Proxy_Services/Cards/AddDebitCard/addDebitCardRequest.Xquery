<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:card.entities.be.dcl";
declare namespace m3 = "urn:crddict.dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:booleanTo01($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "1"
    else "0"
};

declare function xf:shortAccount($account as xs:string) as xs:string {
    if (fn:string-length($account) > 12)
      then fn:substring($account, (fn:string-length($account))-11)
      else $account
};

&lt;soap-env:Body>
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:account/m1:Account
    let $req2 := $body/m:invoke/m:card/m2:Card
    let $req3 := $req2/m2:debitCard/m2:DebitCard
    let $req4 := $req3/m2:addressFlag/m3:CrdAddressFlag

    return
    
    &lt;fml:FML32>
      {if($reqh/m:transHeader/m:transId)
          then &lt;fml:DC_TRN_ID>{ data($reqh/m:transHeader/m:transId) }&lt;/fml:DC_TRN_ID>
          else ()
      }
      {if($reqh/m:msgHeader/m:userId)
          then &lt;fml:DC_UZYTKOWNIK>{concat("SKP:", data($reqh/m:msgHeader/m:userId)) }&lt;/fml:DC_UZYTKOWNIK>
          else ()
      }
      {if($reqh/m:msgHeader/m:unitId)
          then &lt;fml:DC_ODDZIAL>{ data($reqh/m:msgHeader/m:unitId) }&lt;/fml:DC_ODDZIAL>
          else ()
      }

      {if($req/m1:accountNumber)
          then &lt;fml:DC_RACHUNEK>{ xf:shortAccount($req/m1:accountNumber) }&lt;/fml:DC_RACHUNEK>
          else ()
      }
      {if($req/m1:customerNumber)
          then &lt;fml:DC_NUMER_KLIENTA>{ data($req/m1:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA>
          else ()
      }

      {if($req2/m2:bin)
          then &lt;fml:DC_NR_KARTY_BIN>{ data($req2/m2:bin) }&lt;/fml:DC_NR_KARTY_BIN>
          else ()
      }
      {if($req2/m2:cardType)
          then &lt;fml:DC_TYP_KARTY>{ data($req2/m2:cardType) }&lt;/fml:DC_TYP_KARTY>
          else ()
      }
      {if($req2/m2:embossName1)
          then &lt;fml:DC_WYTLOCZONE_NA_KARCIE_I>{ data($req2/m2:embossName1) }&lt;/fml:DC_WYTLOCZONE_NA_KARCIE_I>
          else ()
      }
      {if($req2/m2:embossName2)
          then &lt;fml:DC_WYTLOCZONE_NA_KARCIE_II>{ data($req2/m2:embossName2) }&lt;/fml:DC_WYTLOCZONE_NA_KARCIE_II>
          else ()
      }
      {if($req3/m2:pinMailer)
          then &lt;fml:DC_IDENTYFIKATOR_PIN>{ data($req3/m2:pinMailer) }&lt;/fml:DC_IDENTYFIKATOR_PIN>
          else ()
      }
      {if($req3/m2:processingOptionFlag3 and fn:string-length($req3/m2:processingOptionFlag3)>0)
          then &lt;fml:DC_UMORZYC_OPLATE>{ xf:booleanTo01($req3/m2:processingOptionFlag3) }&lt;/fml:DC_UMORZYC_OPLATE>
          else ()
      }
      {if($req3/m2:contract and fn:string-length($req3/m2:contract)>0)
          then &lt;fml:DC_STATUS_UMOWY>{ xf:booleanTo01($req3/m2:contract) }&lt;/fml:DC_STATUS_UMOWY>
          else ()
      }
      {if($req3/m2:cashLimit)
          then &lt;fml:DC_LIMIT_KARTY>{ data($req3/m2:cashLimit) }&lt;/fml:DC_LIMIT_KARTY>
          else ()
      }
      {if($req3/m2:tranList)
          then &lt;fml:DC_CYKL_ZESTAWIEN>{ data($req3/m2:tranList) }&lt;/fml:DC_CYKL_ZESTAWIEN>
          else ()
      }
      {if($req3/m2:express and fn:string-length($req3/m2:express)>0)
          then &lt;fml:DC_EKSPRES>{ xf:booleanTo01($req3/m2:express) }&lt;/fml:DC_EKSPRES>
          else ()
      }
      {if($req3/m2:policy and fn:string-length($req3/m2:policy)>0)
          then &lt;fml:DC_ZALOZYC_POLISE>{ xf:booleanTo01($req3/m2:policy) }&lt;/fml:DC_ZALOZYC_POLISE>
          else ()
      }
      {if($req3/m2:resignationFlag and fn:string-length($req3/m2:resignationFlag)>0)
          then &lt;fml:DC_ZEST_ZBIORCZE>{ xf:booleanTo01($req3/m2:resignationFlag) }&lt;/fml:DC_ZEST_ZBIORCZE>
          else ()
      }
      {if($req4/m3:crdAddressFlag)
          then &lt;fml:DC_TYP_ADRESU>{ data($req4/m3:crdAddressFlag) }&lt;/fml:DC_TYP_ADRESU>
          else ()
      }
      &lt;fml:DC_TERMINAL_ID>ALSB&lt;/fml:DC_TERMINAL_ID>
      &lt;fml:DC_PROMOCJA>0&lt;/fml:DC_PROMOCJA>
    &lt;/fml:FML32>
  }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>