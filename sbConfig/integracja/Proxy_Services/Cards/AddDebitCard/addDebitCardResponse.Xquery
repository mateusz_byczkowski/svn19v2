<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:card.entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $fml := $body/fml:FML32;

declare function xf:dateFromString($dateIn as xs:string) as xs:string {
  fn:concat(fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2))
};

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

&lt;soap-env:Body>
  &lt;m:invokeResponse xmlns:urn="urn:be.services.dcl">
    {
      let $DC_NR_KARTY := $fml/fml:DC_NR_KARTY
      let $DC_DATA_WAZNOSCI := $fml/fml:DC_DATA_WAZNOSCI
        
      return
      &lt;m:debitCard xmlns:urn1="urn:card.entities.be.dcl">
        &lt;m1:DebitCard>
            {if($DC_DATA_WAZNOSCI and xf:isData($DC_DATA_WAZNOSCI))
               then &lt;m1:expirationDate>{ xf:dateFromString($DC_DATA_WAZNOSCI) }&lt;/m1:expirationDate>
               else ()
            }
            {if($DC_NR_KARTY and xf:isData($DC_NR_KARTY))
               then &lt;m1:cardNbr>{ data($DC_NR_KARTY) }&lt;/m1:cardNbr>
               else ()
            }
        &lt;/m1:DebitCard>
      &lt;/m:debitCard>
    }
  &lt;/m:invokeResponse>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>