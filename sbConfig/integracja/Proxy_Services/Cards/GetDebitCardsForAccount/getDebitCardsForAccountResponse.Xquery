<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:crddict.dictionaries.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns8="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForCardsOut($parm as element(fml:FML32)) as element()
{

<ns6:cardsOut>
  {
    for $x at $occ in $parm/NF_DEBITC_VIRTUALCARDNBR
    return
    <ns7:Card>
      <ns7:bin?>{data($parm/NF_CARD_BIN[$occ])}</ns7:bin>
      <ns7:cardType?>{data($parm/NF_CARD_CARDTYPE[$occ])}</ns7:cardType>
      <ns7:embossName1?>{data($parm/NF_CARD_EMBOSSNAME1[$occ])}</ns7:embossName1>
      <ns7:embossName2?>{data($parm/NF_CARD_EMBOSSNAME2[$occ])}</ns7:embossName2>
      <ns7:cardName?>{data($parm/NF_CARD_CARDNAME[$occ])}</ns7:cardName>
      <ns7:debitCard>
        <ns7:DebitCard>
          <ns7:pinMailer?>{data($parm/NF_DEBITC_PINMAILER[$occ])}</ns7:pinMailer>
          <ns7:expirationDate?>{data($parm/NF_DEBITC_POLICYEXPIRDATE[$occ])}</ns7:expirationDate>
          <ns7:contract?>{data($parm/NF_DEBITC_CONTRACT[$occ])}</ns7:contract>
          <ns7:nextReissueDate?>{data($parm/NF_DEBITC_NEXTREISSUEDATE[$occ])}</ns7:nextReissueDate>
          <ns7:nextCardFeeDate?>{data($parm/NF_DEBITC_NEXTCARDFEEDATE[$occ])}</ns7:nextCardFeeDate>
          <ns7:cashLimit?>{data($parm/NF_DEBITC_CASHLIMIT[$occ])}</ns7:cashLimit>    
          <ns7:express?>{data($parm/NF_DEBITC_EXPRESS[$occ])}</ns7:express>   
          <ns7:policy?>{data($parm/NF_DEBITC_POLICY[$occ])}</ns7:policy>     
          <ns7:cardNbr?>{data($parm/NF_DEBITC_CARDNBR[$occ])}</ns7:cardNbr>
          <ns7:virtualCardNbr?>{data($parm/NF_DEBITC_VIRTUALCARDNBR[$occ])}</ns7:virtualCardNbr>
          <ns7:cycleLimit>
            <ns0:CrdCycleLimit>
              <ns0:crdCycleLimit?>{data($parm/NF_CRDCL_CRDCYCLELIMIT[$occ])}</ns0:crdCycleLimit>
            </ns0:CrdCycleLimit>
          </ns7:cycleLimit>
          <ns7:cardStatus>
            <ns0:CrdStatus>
              <ns0:crdStatus?>{data($parm/NF_CRDS_CRDSTATUS[$occ])}</ns0:crdStatus>
            </ns0:CrdStatus>
          </ns7:cardStatus>
        </ns7:DebitCard>
      </ns7:debitCard>
      <ns7:customer>
        <ns2:Customer>
          <ns2:customerNumber?>{data($parm/NF_CUSTOM_CUSTOMERNUMBER[$occ])}</ns2:customerNumber>
          <ns2:shortName?>{data($parm/NF_CUSTOM_SHORTNAME[$occ])}</ns2:shortName>
          <ns2:customerPersonal>
            <ns2:CustomerPersonal>
              <ns2:motherMaidenName?>{data($parm/NF_CUSTOP_MOTHERMAIDENNAME[$occ])}</ns2:motherMaidenName>
              <ns2:lastName?>{data($parm/NF_CUSTOP_LASTNAME[$occ])}</ns2:lastName>
              <ns2:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME[$occ])}</ns2:firstName>
            </ns2:CustomerPersonal>
          </ns2:customerPersonal>
        </ns2:Customer>
      </ns7:customer>
    </ns7:Card>
  }
</ns6:cardsOut>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns6:invokeResponse>
  <ns6:bcd>
    <ns4:BusinessControlData>
      <ns4:pageControl>
        <ns8:PageControl>
          <ns8:hasNext?>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}</ns8:hasNext>
          <ns8:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns8:navigationKeyDefinition>
          <ns8:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns8:navigationKeyValue>
        </ns8:PageControl>
      </ns4:pageControl>
    </ns4:BusinessControlData>
  </ns6:bcd>

  <ns6:account>
    <ns1:Account>
          <ns1:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER)}</ns1:accountNumber>
    </ns1:Account>
  </ns6:account>

  {getElementsForCardsOut($parm)}
</ns6:invokeResponse>
};
<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>