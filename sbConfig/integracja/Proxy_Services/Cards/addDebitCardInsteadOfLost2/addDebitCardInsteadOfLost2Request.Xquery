<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-07-05</con:description>
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:loyaltyprogram.entities.be.dcl";
declare namespace ns8="urn:be.services.dcl";
declare namespace ns9="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function chkField($value as xs:string?,$length as xs:integer,$fieldName as xs:string) as element()?
{
   if($value) then
      if(string-length($value)>$length) then
       element  {$fieldName}  {$value}   
      else()
   else ()
};


declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm = "1") then $trueval
       else $falseval
};

declare function cutStr($sourceString as xs:string?, $count as xs:decimal) as xs:string?
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};


declare function getFieldsFromHeader($parm as element(ns8:header)) as element()*
{
&lt;DC_ODDZIAL?>{chkUnitId(data($parm/ns8:msgHeader/ns8:unitId))}&lt;/DC_ODDZIAL>,
&lt;DC_UZYTKOWNIK?>{concat("SKP:",data($parm/ns8:msgHeader/ns8:userId))}&lt;/DC_UZYTKOWNIK>,
&lt;DC_TRN_ID?>{data($parm/ns8:transHeader/ns8:transId)}&lt;/DC_TRN_ID>
};

declare function getFieldsFromInvoke($parm as element(ns8:invoke)) as element()*
{
&lt;DC_PROMOCJA>0&lt;/DC_PROMOCJA>,
&lt;DC_TERMINAL_ID?>ALSB&lt;/DC_TERMINAL_ID>,
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:pinMailer),0,"DC_IDENTYFIKATOR_PIN"),
chkField(boolean2SourceValue(data($parm/ns8:debitCard/ns9:DebitCard/ns9:processingOptionFlag3),"1","0"),0,"DC_UMORZYC_OPLATE"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:expirationDate),0,"DC_DATA_WAZNOSCI"),
chkField(boolean2SourceValue(data($parm/ns8:debitCard/ns9:DebitCard/ns9:contract),"1","0"),0,"DC_STATUS_UMOWY"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:nextReissueDate),0,"DC_DATA_WZNOWIENIA"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:nextCardFeeDate),0,"DC_DATA_NALICZENIA"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:cashLimit),0,"DC_LIMIT_KARTY"),
chkField(boolean2SourceValue(data($parm/ns8:debitCard/ns9:DebitCard/ns9:express),"1","0"),0,"DC_EKSPRES"),
chkField(boolean2SourceValue(data($parm/ns8:debitCard/ns9:DebitCard/ns9:policy),"1","0"),0,"DC_ZALOZYC_POLISE"),
chkField(boolean2SourceValue(data($parm/ns8:debitCard/ns9:DebitCard/ns9:individualTranList),"30","0"),0,"DC_CYKL_ZESTAWIEN"),
chkField(boolean2SourceValue(data($parm/ns8:debitCard/ns9:DebitCard/ns9:collectiveTranList),"1","0"),0,"DC_ZEST_ZBIORCZE"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:card/ns9:Card/ns9:bin),0,"DC_NR_KARTY_BIN"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:card/ns9:Card/ns9:cardType),0,"DC_TYP_KARTY"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:card/ns9:Card/ns9:embossName1),0,"DC_WYTLOCZONE_NA_KARCIE_I"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:card/ns9:Card/ns9:embossName2),0,"DC_WYTLOCZONE_NA_KARCIE_II"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:card/ns9:Card/ns9:customer/ns3:Customer/ns3:customerNumber),0,"DC_NUMER_KLIENTA"),

if (string-length(data($parm/ns8:debitCard/ns9:DebitCard/ns9:tranAccount/ns2:TranAccount/ns2:account/ns2:Account/ns2:accountNumber)) > 12) then
	 &lt;DC_RACHUNEK?>{cutStr(data($parm/ns8:debitCard/ns9:DebitCard/ns9:tranAccount/ns2:TranAccount/ns2:account/ns2:Account/ns2:accountNumber),12)}&lt;/DC_RACHUNEK>
	 else
	  chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:tranAccount/ns2:TranAccount/ns2:account/ns2:Account/ns2:accountNumber),0,"DC_RACHUNEK")
,
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:addressFlag/ns1:CrdAddressFlag/ns1:crdAddressFlag),0,"DC_TYP_ADRESU"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:cycleLimit/ns1:CrdCycleLimit/ns1:crdCycleLimit),0,"DC_CYKL_LIMITU"),
chkField(data($parm/ns8:debitCard/ns9:DebitCard/ns9:cardAddressBranchNumber/ns4:BranchCode/ns4:branchCode),0,"DC_NUMER_ODDZIALU"),
chkField(data($parm/ns8:card/ns9:Card/ns9:cardLpInfo/ns9:CardLpInfo/ns9:pb2),0,"DC_NR_PB2"),
chkField(boolean2SourceValue(data($parm/ns8:loyaltyProgram/ns7:LoyaltyProgram/ns7:forwardingDataApproval),"1","0"),0,"DC_FLAGA_ZGODY"),
chkField(boolean2SourceValue(data($parm/ns8:loyaltyProgram/ns7:LoyaltyProgram/ns7:processingApproval),"1","0"),0,"DC_FLAGA_MARKETINGU"),
chkField(data($parm/ns8:loyaltyProgram/ns7:LoyaltyProgram/ns7:applicationSource),0,"DC_ZRODLO"),
chkField(data($parm/ns8:loyaltyProgram/ns7:LoyaltyProgram/ns7:unitApplicationSource),0,"DC_JEDNOSTKA_SPRZEDAJACA")
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns8:header)}
    {getFieldsFromInvoke($body/ns8:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>