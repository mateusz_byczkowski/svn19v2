<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-07-18</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace prim = "http://bzwbk.com/services/prime/";



declare function xf:map_ClonePayback($fml as element(fml:FML32))
	as element(prim:ClonePayback) {
		&lt;prim:ClonePayback>

 		   &lt;prim:in>
                       &lt;prim:Cif>{data($fml/fml:CI_NUMER_KLIENTA)}&lt;/prim:Cif>
                       &lt;prim:PbStatus>{data($fml/fml:CI_LP_STATUS)}&lt;/prim:PbStatus>
                       &lt;prim:Pbc>{data($fml/fml:CI_LP_NUMER_PBC)}&lt;/prim:Pbc>
                       &lt;prim:Pb2C>{data($fml/fml:CI_LP_NUMER_PB2C)}&lt;/prim:Pb2C>
                       &lt;prim:MarketingAgreementLp>{data($fml/fml:CI_LP_ZGODA_MARKET)}&lt;/prim:MarketingAgreementLp>
                       &lt;prim:SmsAgreementLp>{data($fml/fml:CI_LP_ZGODA_KOM_EL)}&lt;/prim:SmsAgreementLp>
		   &lt;/prim:in>
		&lt;/prim:ClonePayback>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_ClonePayback($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>