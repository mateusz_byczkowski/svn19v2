<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:be.services.dcl";
declare namespace ns0 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns1 = "urn:dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns3 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:transactionlog.entities.be.dcl";
declare namespace ns6 = "urn:baseauxentities.be.dcl";

declare function local:getTransactionLogResponse($searchResponse as element(ns0:searchResponse),
    $firstRow as xs:integer, $actionCode as xs:string)
    as element(ns4:invokeResponse) {
        &lt;ns4:invokeResponse>
            &lt;ns4:tlEntryList>
                {
                    for $rows in $searchResponse/rows
                    return
                        &lt;ns5:TLEntry>
                            &lt;ns5:transactionID?>{ data($rows/transactionId) }&lt;/ns5:transactionID>
                            &lt;ns5:putDownDate?>{ data($rows/putDownDate) }&lt;/ns5:putDownDate>
                            &lt;ns5:timestamp?>{ data($rows/timestamp) }&lt;/ns5:timestamp>
                            &lt;ns5:entryId?>{ data($rows/id) }&lt;/ns5:entryId>
                            &lt;ns5:businessTransactionType>
                                &lt;ns3:BusinessTransactionType>
                                    &lt;ns3:businessTransactionType>{ data($rows/businessTransactionType) }&lt;/ns3:businessTransactionType>
                                &lt;/ns3:BusinessTransactionType>
                            &lt;/ns5:businessTransactionType>
                            {
                            	if($rows/debitAccountNumber or $rows/debitAmount or $rows/debitCurrencyCode) then
		                            &lt;ns5:debit>
		                                &lt;ns5:TLTransferLeg>
		                                    &lt;ns5:accountNumber>{ data($rows/debitAccountNumber) }&lt;/ns5:accountNumber>
		                                    &lt;ns5:amount>{ data($rows/debitAmount) }&lt;/ns5:amount>
		                                    &lt;ns5:currencyCode>
		                                        &lt;ns1:CurrencyCode>
		                                            &lt;ns1:currencyCode>{ data($rows/debitCurrencyCode) }&lt;/ns1:currencyCode>
		                                        &lt;/ns1:CurrencyCode>
		                                    &lt;/ns5:currencyCode>
		                                &lt;/ns5:TLTransferLeg>
		                            &lt;/ns5:debit>
								else
									()
							}
                            {
                            	if($rows/creditAccountNumber or $rows/creditAmount or $rows/creditCurrencyCode) then
		                            &lt;ns5:credit>
		                                &lt;ns5:TLTransferLeg>
		                                    &lt;ns5:accountNumber>{ data($rows/creditAccountNumber) }&lt;/ns5:accountNumber>
		                                    &lt;ns5:amount>{ data($rows/creditAmount) }&lt;/ns5:amount>
		                                    &lt;ns5:currencyCode>
		                                        &lt;ns1:CurrencyCode>
		                                            &lt;ns1:currencyCode>{ data($rows/creditCurrencyCode) }&lt;/ns1:currencyCode>
		                                        &lt;/ns1:CurrencyCode>
		                                    &lt;/ns5:currencyCode>
		                                &lt;/ns5:TLTransferLeg>
		                            &lt;/ns5:credit>
		                        else
		                        	()
		                    }
                            &lt;ns5:executor>
                                &lt;ns5:TLExecutor>
                                    &lt;ns5:executorID>{ data($rows/executorID) }&lt;/ns5:executorID>
                                    &lt;ns5:branchNumber>{ data($rows/branchNumber) }&lt;/ns5:branchNumber>
                                    &lt;ns5:tellerID>{ data($rows/tellerID) }&lt;/ns5:tellerID>
                                    &lt;ns5:tillNumber>{ data($rows/tillNumber) }&lt;/ns5:tillNumber>
                                &lt;/ns5:TLExecutor>
                            &lt;/ns5:executor>
                            &lt;ns5:transactionStatus>
                                &lt;ns1:TransactionStatus>
                                    &lt;ns1:transactionStatus>{ data($rows/transactionStatus) }&lt;/ns1:transactionStatus>
                                &lt;/ns1:TransactionStatus>
                            &lt;/ns5:transactionStatus>
                        &lt;/ns5:TLEntry>
                }
            &lt;/ns4:tlEntryList>
            &lt;ns4:bcd>
                &lt;ns2:BusinessControlData>
                    &lt;ns2:pageControl>
                        &lt;ns6:PageControl>
                            &lt;ns6:hasNext>{
	if ($actionCode = 'F' or $actionCode = 'N') then
		(: stronicowanie do przodu :)
		number(string($searchResponse/totalCount)) gt ($firstRow + count($searchResponse/rows))
	else
		(: stronicowanie do tylu :)
		$firstRow gt 0
							}&lt;/ns6:hasNext>
                            &lt;ns6:navigationKeyValue>{ concat(string($firstRow), ":", data($searchResponse/totalCount)) }&lt;/ns6:navigationKeyValue>
                        &lt;/ns6:PageControl>
                    &lt;/ns2:pageControl>
                &lt;/ns2:BusinessControlData>
            &lt;/ns4:bcd>
        &lt;/ns4:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
declare variable $firstRow as xs:integer external;
declare variable $actionCode as xs:string external;

&lt;soap-env:Body>{ local:getTransactionLogResponse($body/ns0:searchResponse, $firstRow, $actionCode) }&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>