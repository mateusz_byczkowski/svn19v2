<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:directdebit.entities.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function checkNull($value as xs:string,$fieldName as xs:string) as xs:string
{
      if ($value)
        then if(string-length($value)>1 and not ($value = "0.00"))
            then element  {$fieldName}  {$value}
        else() 
      else()
};

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string
{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns6:invokeResponse>
  &lt;ns6:directDebitOut>
    &lt;ns1:DirectDebit>
      &lt;ns1:originatorReference?>{data($parm/NF_DIRECD_ORIGINATORREFERE)}&lt;/ns1:originatorReference>
      &lt;ns1:debtorTaxID?>{data($parm/NF_DIRECD_DEBTORTAXID)}&lt;/ns1:debtorTaxID>
        {
           if(string-length(data($parm/NF_DIRECD_DEBTORNAME)) > 0)
             then
             &lt;ns1:debtorName?>{data($parm/NF_DIRECD_DEBTORNAME)}&lt;/ns1:debtorName>
             else()
        }
      &lt;ns1:debtorShortName?>{data($parm/NF_DIRECD_DEBTORSHORTNAME)}&lt;/ns1:debtorShortName>
      &lt;ns1:additionalDescription?>{data($parm/NF_DIRECD_ADDITIONALDESCRI)}&lt;/ns1:additionalDescription>
      &lt;ns1:customer>
        &lt;ns2:Customer>
          {
             if(string-length(data($parm/NF_DIRECD_DEBTORNAME)) > 0)
               then
                 &lt;ns2:resident?>{sourceValue2Boolean (data($parm/NF_CUSTOM_RESIDENT),"1")}&lt;/ns2:resident>
             else()
          }
        &lt;/ns2:Customer>
      &lt;/ns1:customer>
      &lt;ns1:directDebitStatus>
        &lt;ns4:DirectDebitStatus>
          &lt;ns4:directDebitStatus?>{data($parm/NF_DIREDS_DIRECTDEBITSTATU)}&lt;/ns4:directDebitStatus>
        &lt;/ns4:DirectDebitStatus>
      &lt;/ns1:directDebitStatus>
    &lt;/ns1:DirectDebit>
  &lt;/ns6:directDebitOut>
  &lt;ns6:addressOut>
    &lt;ns2:Address>
      {
        if(string-length(data($parm/NF_ADDRES_HOUSEFLATNUMBER)) > 0)
          then
          &lt;ns2:houseFlatNumber?>{data($parm/NF_ADDRES_HOUSEFLATNUMBER)}&lt;/ns2:houseFlatNumber>
        else()
      }
      {
        if(string-length(data($parm/NF_ADDRES_CITY)) > 0)
          then
          &lt;ns2:city?>{data($parm/NF_ADDRES_CITY)}&lt;/ns2:city>
          else() 
      }
      {
        if(string-length(data($parm/NF_ADDRES_STREET)) > 0)
          then
          &lt;ns2:street?>{data($parm/NF_ADDRES_STREET)}&lt;/ns2:street>
          else()
      }
      {
        if(string-length(data($parm/NF_ADDRES_ZIPCODE)) > 0)
          then
          &lt;ns2:zipCode?>{data($parm/NF_ADDRES_ZIPCODE)}&lt;/ns2:zipCode>
          else()
      }
      {
        if(string-length(data($parm/NF_ADDRES_LOCAL)) > 0)
          then
          &lt;ns2:local?>{data($parm/NF_ADDRES_LOCAL)}&lt;/ns2:local>
          else()
      }
    &lt;/ns2:Address>
  &lt;/ns6:addressOut>
&lt;/ns6:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>