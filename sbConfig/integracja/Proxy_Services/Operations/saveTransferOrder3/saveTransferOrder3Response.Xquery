<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns7:invokeResponse>
  &lt;ns7:response>
    &lt;ns0:ResponseMessage>
       {
          if (string-length(data($parm/DC_OPIS_BLEDU))>0) then
                    &lt;ns0:result?>false&lt;/ns0:result>
          else
                    &lt;ns0:result?>true&lt;/ns0:result>
       }
      &lt;ns0:errorDescription?>{data($parm/DC_OPIS_BLEDU)}&lt;/ns0:errorDescription>
    &lt;/ns0:ResponseMessage>
  &lt;/ns7:response>
  &lt;ns7:transferOrderOut>
    &lt;ns2:TransferOrderAnchor>
      &lt;ns2:transferOrderNumber?>{data($parm/NF_TRANOA_TRANSFERORDERNUM)}&lt;/ns2:transferOrderNumber>
    &lt;/ns2:TransferOrderAnchor>
  &lt;/ns7:transferOrderOut>
&lt;/ns7:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>