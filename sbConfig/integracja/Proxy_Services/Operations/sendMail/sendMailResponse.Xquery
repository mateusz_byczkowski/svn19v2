<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:dcl:services.alsb.datamodel";

<soap:Body>
     <urn:invokeResponse>
        <urn:response>
            <urn:entities.filtersandmessages.ResponseMessage>
               <urn:result>true</urn:result>
            </urn:entities.filtersandmessages.ResponseMessage>
         </urn:response>       
     </urn:invokeResponse>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>