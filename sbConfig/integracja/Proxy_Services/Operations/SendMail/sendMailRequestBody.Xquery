<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace urn="urn:dcl:services.alsb.datamodel";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace email="http://www.bea.com/wli/sb/transports/email";

declare variable $body as element(soap:Body) external;

declare function prepareMailRequestBody($req as element(urn:entities.filtersandmessages.Mail))  {
   let $body := $req/urn:body
   return data($body)
};


&lt;soap:Body>
{prepareMailRequestBody($body/urn:invoke/urn:mail/urn:entities.filtersandmessages.Mail)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>