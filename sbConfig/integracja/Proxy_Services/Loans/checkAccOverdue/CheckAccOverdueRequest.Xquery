<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";

declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns2="urn:application.dictionaries.be.dcl";

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID>
};





(: Zwroc wartosc parametru na podstawie jego nazwy :)
declare function getParmVal($parm as element(ns0:invoke), $parmName as xs:string) as xs:string*
{
  for $x in $parm/ns0:appDenyRuleServiceParamList/ns2:AppDenyRuleServiceParam
  where $x/ns2:paramName=$parmName
  return if ( fn:string-length($x/ns2:paramValue) = 0) then "0" else ($x/ns2:paramValue)
};

declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

&lt;DC_NUMER_KLIENTA?>{data($parm/ns0:customer/ns1:Customer/ns1:customerNumber)}&lt;/DC_NUMER_KLIENTA>,

(: Minimalna kwota kwalifikowana jako przeterminowanie :)
&lt;DC_NR_PB1?>{getParmVal($parm, 'ovdAmount')}&lt;/DC_NR_PB1>,

(: Ilosc miesiecy wstecz sprawdzane - moze byc wartosc 0 - wtedy oznacza to ostatni miesiac :)
&lt;DC_NR_PB2?>{getParmVal($parm, 'ovdCheckPeriod')}&lt;/DC_NR_PB2>,

(: Minimalna ilosc dni opznienia splaty kwalifikowana jako przeterminowanie - musi byc > 0:)
&lt;DC_NR_PB3?>{getParmVal($parm, 'ovdDuration')}&lt;/DC_NR_PB3>

};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>