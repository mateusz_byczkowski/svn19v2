<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:short2bool($short as xs:int) as xs:string {
	if ($short >= 1)
		then "true"
		else "false"
};

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:accountOut>
    &lt;ns4:Account>
      {if (data($parm/NF_ACCOUN_ACCOUNTNUMBER) and xf:isData(data($parm/NF_ACCOUN_ACCOUNTNUMBER)))
	      then &lt;ns4:accountNumber>{data($parm/NF_ACCOUN_ACCOUNTNUMBER)}&lt;/ns4:accountNumber>
      	else ()
      }
      {if (data($parm/NF_ACCOUN_ACCOUNTOPENDATE) and xf:isData(data($parm/NF_ACCOUN_ACCOUNTOPENDATE)))
          then &lt;ns4:accountOpenDate>{data($parm/NF_ACCOUN_ACCOUNTOPENDATE)}&lt;/ns4:accountOpenDate>
      	else ()
      }
      {if (data($parm/NF_ACCOUN_CURRENTBALANCE) and xf:isData(data($parm/NF_ACCOUN_CURRENTBALANCE)))
          then &lt;ns4:currentBalance>{data($parm/NF_ACCOUN_CURRENTBALANCE)}&lt;/ns4:currentBalance>
      	else ()
      }
      {if (data($parm/NF_ACCOUN_CURRENTBALANCEPL) and xf:isData(data($parm/NF_ACCOUN_CURRENTBALANCEPL)))
          then &lt;ns4:currentBalancePLN>{data($parm/NF_ACCOUN_CURRENTBALANCEPL)}&lt;/ns4:currentBalancePLN>
      	else ()
      }
      &lt;ns4:loanAccount>
        &lt;ns4:LoanAccount>
      {if (data($parm/NF_LOANA_INTERESTGUARANTEE) and xf:isData(data($parm/NF_LOANA_INTERESTGUARANTEE)))
          then &lt;ns4:interestGuaranteeCode>{data($parm/NF_LOANA_INTERESTGUARANTEE)}&lt;/ns4:interestGuaranteeCode>
      	else ()
      }
      {if (data($parm/NF_LOANA_INTERESTRATE) and xf:isData(data($parm/NF_LOANA_INTERESTRATE)))
          then &lt;ns4:interestRate>{data($parm/NF_LOANA_INTERESTRATE)}&lt;/ns4:interestRate>
      	else ()
      }
      {if (data($parm/NF_LOANA_INDEXRATENUMBER) and xf:isData(data($parm/NF_LOANA_INDEXRATENUMBER)))
          then &lt;ns4:indexRateNumber>{data($parm/NF_LOANA_INDEXRATENUMBER)}&lt;/ns4:indexRateNumber>
      	else ()
      }
      {if (data($parm/NF_LOANA_INDEXRATEVALUE) and xf:isData(data($parm/NF_LOANA_INDEXRATEVALUE)))
          then &lt;ns4:indexRateValue>{data($parm/NF_LOANA_INDEXRATEVALUE)}&lt;/ns4:indexRateValue>
      	else ()
      }
      {if (data($parm/NF_LOANA_VARIANCEFROMINDEX) and xf:isData(data($parm/NF_LOANA_VARIANCEFROMINDEX)))
          then &lt;ns4:varianceFromIndex>{data($parm/NF_LOANA_VARIANCEFROMINDEX)}&lt;/ns4:varianceFromIndex>
      	else ()
      }
      {if (data($parm/NF_LOANA_RATEREVIEWPERIOD) and xf:isData(data($parm/NF_LOANA_RATEREVIEWPERIOD)))
          then &lt;ns4:rateReviewPeriod>{data($parm/NF_LOANA_RATEREVIEWPERIOD)}&lt;/ns4:rateReviewPeriod>
      	else ()
      }
      {if (data($parm/NF_LOANA_RATEREVIEWFREQUEN) and xf:isData(data($parm/NF_LOANA_RATEREVIEWFREQUEN)))
          then &lt;ns4:rateReviewFrequency>{data($parm/NF_LOANA_RATEREVIEWFREQUEN)}&lt;/ns4:rateReviewFrequency>
      	else ()
      }
      {if (data($parm/NF_LOANA_NEXTRATEREVIEWCHA) and xf:isData(data($parm/NF_LOANA_NEXTRATEREVIEWCHA)))
          then &lt;ns4:nextRateReviewChangeDate>{data($parm/NF_LOANA_NEXTRATEREVIEWCHA)}&lt;/ns4:nextRateReviewChangeDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_FACEAMOUNT) and xf:isData(data($parm/NF_LOANA_FACEAMOUNT)))
          then &lt;ns4:faceAmount>{data($parm/NF_LOANA_FACEAMOUNT)}&lt;/ns4:faceAmount>
      	else ()
      }
      {if (data($parm/NF_LOANA_PRODUCTPPPTYPE) and xf:isData(data($parm/NF_LOANA_PRODUCTPPPTYPE)))
          then &lt;ns4:productpppType>{data($parm/NF_LOANA_PRODUCTPPPTYPE)}&lt;/ns4:productpppType>
      	else ()
      }
      {if (data($parm/NF_LOANA_MATURITYDATE) and xf:isData(data($parm/NF_LOANA_MATURITYDATE)))
          then &lt;ns4:maturityDate>{data($parm/NF_LOANA_MATURITYDATE)}&lt;/ns4:maturityDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_SECURED) and xf:isData(data($parm/NF_LOANA_SECURED)))
          then &lt;ns4:secured>{xf:short2bool($parm/NF_LOANA_SECURED)}&lt;/ns4:secured>
      	else ()
      }
      {if (data($parm/NF_LOANA_ADJUSTAVAIL) and xf:isData(data($parm/NF_LOANA_ADJUSTAVAIL)))
          then &lt;ns4:adjustAvail>{xf:short2bool($parm/NF_LOANA_ADJUSTAVAIL)}&lt;/ns4:adjustAvail>
      	else ()
      }
      {if (data($parm/NF_LOANA_RECOURSE) and xf:isData(data($parm/NF_LOANA_RECOURSE)))
          then &lt;ns4:recourse>{data($parm/NF_LOANA_RECOURSE)}&lt;/ns4:recourse>
      	else ()
      }
      {if (data($parm/NF_LOANA_INDEXRATEMULTIPLI) and xf:isData(data($parm/NF_LOANA_INDEXRATEMULTIPLI)))
          then &lt;ns4:indexRateMultiplier>{data($parm/NF_LOANA_INDEXRATEMULTIPLI)}&lt;/ns4:indexRateMultiplier>
      	else ()
      }
      {if (data($parm/NF_LOANA_RATEREVIEWSPDAY) and xf:isData(data($parm/NF_LOANA_RATEREVIEWSPDAY)))
          then &lt;ns4:rateReviewSpDay>{data($parm/NF_LOANA_RATEREVIEWSPDAY)}&lt;/ns4:rateReviewSpDay>
      	else ()
      }
      {if (data($parm/NF_LOANA_TIMESTOUSE) and xf:isData(data($parm/NF_LOANA_TIMESTOUSE)))
          then &lt;ns4:timesToUse>{data($parm/NF_LOANA_TIMESTOUSE)}&lt;/ns4:timesToUse>
      	else ()
      }
      {if (data($parm/NF_LOANA_INTERESTPERIOD) and xf:isData(data($parm/NF_LOANA_INTERESTPERIOD)))
          then &lt;ns4:interestPeriod>{data($parm/NF_LOANA_INTERESTPERIOD)}&lt;/ns4:interestPeriod>
      	else ()
      }
      {if (data($parm/NF_LOANA_INTERESTFREQUENCY) and xf:isData(data($parm/NF_LOANA_INTERESTFREQUENCY)))
          then &lt;ns4:interestFrequency>{data($parm/NF_LOANA_INTERESTFREQUENCY)}&lt;/ns4:interestFrequency>
      	else ()
      }
      {if (data($parm/NF_LOANA_FIRSTINTERESTBILL) and xf:isData(data($parm/NF_LOANA_FIRSTINTERESTBILL)))
          then &lt;ns4:firstInterestBillingDate>{data($parm/NF_LOANA_FIRSTINTERESTBILL)}&lt;/ns4:firstInterestBillingDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_SPECIFICDAYOFMONT) and xf:isData(data($parm/NF_LOANA_SPECIFICDAYOFMONT)))
          then &lt;ns4:specificDayOfMonthToBill>{data($parm/NF_LOANA_SPECIFICDAYOFMONT)}&lt;/ns4:specificDayOfMonthToBill>
      	else ()
      }
      {if (data($parm/NF_LOANA_RATECHANGESWINDEX) and xf:isData(data($parm/NF_LOANA_RATECHANGESWINDEX)))
          then &lt;ns4:rateChangesWindexStartDate>{data($parm/NF_LOANA_RATECHANGESWINDEX)}&lt;/ns4:rateChangesWindexStartDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_INTERESTDUE) and xf:isData(data($parm/NF_LOANA_INTERESTDUE)))
          then &lt;ns4:interestDue>{data($parm/NF_LOANA_INTERESTDUE)}&lt;/ns4:interestDue>
      	else ()
      }
      {if (data($parm/NF_LOANA_AVAILFORDISB) and xf:isData(data($parm/NF_LOANA_AVAILFORDISB)))
          then &lt;ns4:availForDisb>{data($parm/NF_LOANA_AVAILFORDISB)}&lt;/ns4:availForDisb>
      	else ()
      }
      {if (data($parm/NF_LOANA_PAYOFF) and xf:isData(data($parm/NF_LOANA_PAYOFF)))
          then &lt;ns4:payoff>{data($parm/NF_LOANA_PAYOFF)}&lt;/ns4:payoff>
      	else ()
      }
      {if (data($parm/NF_LOANA_PASTDUEPAYMENTS) and xf:isData(data($parm/NF_LOANA_PASTDUEPAYMENTS)))
          then &lt;ns4:pastDuePayments>{data($parm/NF_LOANA_PASTDUEPAYMENTS)}&lt;/ns4:pastDuePayments>
      	else ()
      }
      {if (data($parm/NF_LOANA_NEXTPAYMENTDATE) and xf:isData(data($parm/NF_LOANA_NEXTPAYMENTDATE)))
          then &lt;ns4:nextPaymentDate>{data($parm/NF_LOANA_NEXTPAYMENTDATE)}&lt;/ns4:nextPaymentDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_NEXTPAYMENTAMOUNT) and xf:isData(data($parm/NF_LOANA_NEXTPAYMENTAMOUNT)))
          then &lt;ns4:nextPaymentAmount>{data($parm/NF_LOANA_NEXTPAYMENTAMOUNT)}&lt;/ns4:nextPaymentAmount>
      	else ()
      }
      {if (data($parm/NF_LOANA_CUSTOMERRISKCODE) and xf:isData(data($parm/NF_LOANA_CUSTOMERRISKCODE)))
          then &lt;ns4:customerRiskCode>{data($parm/NF_LOANA_CUSTOMERRISKCODE)}&lt;/ns4:customerRiskCode>
      	else ()
      }
      {if (data($parm/NF_LOANA_PDOSTATUS) and xf:isData(data($parm/NF_LOANA_PDOSTATUS)))
          then &lt;ns4:pdoStatus>{data($parm/NF_LOANA_PDOSTATUS)}&lt;/ns4:pdoStatus>
      	else ()
      }
      {if (data($parm/NF_LOANA_GOUGHTSOLD) and xf:isData(data($parm/NF_LOANA_GOUGHTSOLD)))
          then &lt;ns4:goughtSold>{data($parm/NF_LOANA_GOUGHTSOLD)}&lt;/ns4:goughtSold>
      	else ()
      }
      {if (data($parm/NF_LOANA_RATEEFFECTIVEDATE) and xf:isData(data($parm/NF_LOANA_RATEEFFECTIVEDATE)))
          then &lt;ns4:rateEffectiveDate>{data($parm/NF_LOANA_RATEEFFECTIVEDATE)}&lt;/ns4:rateEffectiveDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_PREVIOUSRATE) and xf:isData(data($parm/NF_LOANA_PREVIOUSRATE)))
          then &lt;ns4:previousRate>{data($parm/NF_LOANA_PREVIOUSRATE)}&lt;/ns4:previousRate>
      	else ()
      }
      {if (data($parm/NF_LOANA_ACCRUALBASE) and xf:isData(data($parm/NF_LOANA_ACCRUALBASE)))
          then &lt;ns4:accrualBase>{data($parm/NF_LOANA_ACCRUALBASE)}&lt;/ns4:accrualBase>
      	else ()
      }
      {if (data($parm/NF_LOANA_YEARBASE) and xf:isData(data($parm/NF_LOANA_YEARBASE)))
          then &lt;ns4:yearBase>{data($parm/NF_LOANA_YEARBASE)}&lt;/ns4:yearBase>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATEFEEINTRATE) and xf:isData(data($parm/NF_LOANA_LATEFEEINTRATE)))
          then &lt;ns4:lateFeeIntRate>{data($parm/NF_LOANA_LATEFEEINTRATE)}&lt;/ns4:lateFeeIntRate>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATECURRENTRATEDA) and xf:isData(data($parm/NF_LOANA_LATECURRENTRATEDA)))
          then &lt;ns4:lateCurrentRateDate>{data($parm/NF_LOANA_LATECURRENTRATEDA)}&lt;/ns4:lateCurrentRateDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATEPREVIOUSRATE) and xf:isData(data($parm/NF_LOANA_LATEPREVIOUSRATE)))
          then &lt;ns4:latePreviousRate>{data($parm/NF_LOANA_LATEPREVIOUSRATE)}&lt;/ns4:latePreviousRate>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATEINDEX) and xf:isData(data($parm/NF_LOANA_LATEINDEX)))
          then &lt;ns4:lateIndex>{data($parm/NF_LOANA_LATEINDEX)}&lt;/ns4:lateIndex>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATEVARIANCE) and xf:isData(data($parm/NF_LOANA_LATEVARIANCE)))
          then &lt;ns4:lateVariance>{data($parm/NF_LOANA_LATEVARIANCE)}&lt;/ns4:lateVariance>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATEINTERESTRATEM) and xf:isData(data($parm/NF_LOANA_LATEINTERESTRATEM)))
          then &lt;ns4:lateInterestRateMultiplier>{data($parm/NF_LOANA_LATEINTERESTRATEM)}&lt;/ns4:lateInterestRateMultiplier>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATEACCRUALBASE) and xf:isData(data($parm/NF_LOANA_LATEACCRUALBASE)))
          then &lt;ns4:lateAccrualBase>{data($parm/NF_LOANA_LATEACCRUALBASE)}&lt;/ns4:lateAccrualBase>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATEYEARBASE) and xf:isData(data($parm/NF_LOANA_LATEYEARBASE)))
          then &lt;ns4:lateYearBase>{data($parm/NF_LOANA_LATEYEARBASE)}&lt;/ns4:lateYearBase>
      	else ()
      }
      {if (data($parm/NF_LOANA_CURRENTSCHEDULENB) and xf:isData(data($parm/NF_LOANA_CURRENTSCHEDULENB)))
          then &lt;ns4:currentScheduleNbr>{data($parm/NF_LOANA_CURRENTSCHEDULENB)}&lt;/ns4:currentScheduleNbr>
      	else ()
      }
      {if (data($parm/NF_LOANA_SCHEDULETIMESBILL) and xf:isData(data($parm/NF_LOANA_SCHEDULETIMESBILL)))
          then &lt;ns4:scheduleTimesBilled>{data($parm/NF_LOANA_SCHEDULETIMESBILL)}&lt;/ns4:scheduleTimesBilled>
      	else ()
      }
      {if (data($parm/NF_LOANA_BILLEDTYP) and xf:isData(data($parm/NF_LOANA_BILLEDTYP)))
          then &lt;ns4:billedTyp>{data($parm/NF_LOANA_BILLEDTYP)}&lt;/ns4:billedTyp>
      	else ()
      }
      {if (data($parm/NF_LOANA_BILLEDDUEDATE) and xf:isData(data($parm/NF_LOANA_BILLEDDUEDATE)))
          then &lt;ns4:billedDueDate>{data($parm/NF_LOANA_BILLEDDUEDATE)}&lt;/ns4:billedDueDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_BILLEDPRINCIPALAM) and xf:isData(data($parm/NF_LOANA_BILLEDPRINCIPALAM)))
          then &lt;ns4:billedPrincipalAmt>{data($parm/NF_LOANA_BILLEDPRINCIPALAM)}&lt;/ns4:billedPrincipalAmt>
      	else ()
      }
      {if (data($parm/NF_LOANA_BILLEDINTERESTAMT) and xf:isData(data($parm/NF_LOANA_BILLEDINTERESTAMT)))
          then &lt;ns4:billedInterestAmt>{data($parm/NF_LOANA_BILLEDINTERESTAMT)}&lt;/ns4:billedInterestAmt>
      	else ()
      }
      {if (data($parm/NF_LOANA_BILLEDFEE) and xf:isData(data($parm/NF_LOANA_BILLEDFEE)))
          then &lt;ns4:billedFee>{data($parm/NF_LOANA_BILLEDFEE)}&lt;/ns4:billedFee>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATEFEEINTCOMPCOD) and xf:isData(data($parm/NF_LOANA_LATEFEEINTCOMPCOD)))
          then &lt;ns4:lateFeeIntCompCode>{data($parm/NF_LOANA_LATEFEEINTCOMPCOD)}&lt;/ns4:lateFeeIntCompCode>
      	else ()
      }
      {if (data($parm/NF_LOANA_PROCESSINGOPTION) and xf:isData(data($parm/NF_LOANA_PROCESSINGOPTION)))
          then &lt;ns4:processingOption>{data($parm/NF_LOANA_PROCESSINGOPTION)}&lt;/ns4:processingOption>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATECHARGESDUE) and xf:isData(data($parm/NF_LOANA_LATECHARGESDUE)))
          then &lt;ns4:lateChargesDue>{data($parm/NF_LOANA_LATECHARGESDUE)}&lt;/ns4:lateChargesDue>
      	else ()
      }
      {if (data($parm/NF_LOANA_AASSESSEDTODATE) and xf:isData(data($parm/NF_LOANA_AASSESSEDTODATE)))
          then &lt;ns4:aassessedToDate>{data($parm/NF_LOANA_AASSESSEDTODATE)}&lt;/ns4:aassessedToDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_PAIDTODATE) and xf:isData(data($parm/NF_LOANA_PAIDTODATE)))
          then &lt;ns4:paidToDate>{data($parm/NF_LOANA_PAIDTODATE)}&lt;/ns4:paidToDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_WAIVEDTODATE) and xf:isData(data($parm/NF_LOANA_WAIVEDTODATE)))
          then &lt;ns4:waivedToDate>{data($parm/NF_LOANA_WAIVEDTODATE)}&lt;/ns4:waivedToDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_TIMESPASTDUECY1) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY1)))
          then &lt;ns4:timesPastDueCY1>{data($parm/NF_LOANA_TIMESPASTDUECY1)}&lt;/ns4:timesPastDueCY1>
      	else ()
      }
      {if (data($parm/NF_LOANA_TIMESPASTDUECY2) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY2)))
          then &lt;ns4:timesPastDueCY2>{data($parm/NF_LOANA_TIMESPASTDUECY2)}&lt;/ns4:timesPastDueCY2>
      	else ()
      }
      {if (data($parm/NF_LOANA_TIMESPASTDUECY3) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY3)))
          then &lt;ns4:timesPastDueCY3>{data($parm/NF_LOANA_TIMESPASTDUECY3)}&lt;/ns4:timesPastDueCY3>
      	else ()
      }
      {if (data($parm/NF_LOANA_TIMESPASTDUECY4) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY4)))
          then &lt;ns4:timesPastDueCY4>{data($parm/NF_LOANA_TIMESPASTDUECY4)}&lt;/ns4:timesPastDueCY4>
      	else ()
      }
      {if (data($parm/NF_LOANA_TIMESPASTDUECY5) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY5)))
          then &lt;ns4:timesPastDueCY5>{data($parm/NF_LOANA_TIMESPASTDUECY5)}&lt;/ns4:timesPastDueCY5>
      	else ()
      }
      {if (data($parm/NF_LOANA_TIMESPASTDUECY6) and xf:isData(data($parm/NF_LOANA_TIMESPASTDUECY6)))
          then &lt;ns4:timesPastDueCY6>{data($parm/NF_LOANA_TIMESPASTDUECY6)}&lt;/ns4:timesPastDueCY6>
      	else ()
      }
      {if (data($parm/NF_LOANA_DUEAMOUNTFEE1) and xf:isData(data($parm/NF_LOANA_DUEAMOUNTFEE1)))
          then &lt;ns4:dueAmountFee1>{data($parm/NF_LOANA_DUEAMOUNTFEE1)}&lt;/ns4:dueAmountFee1>
      	else ()
      }
      {if (data($parm/NF_LOANA_DUEAMOUNTFEE2) and xf:isData(data($parm/NF_LOANA_DUEAMOUNTFEE2)))
          then &lt;ns4:dueAmountFee2>{data($parm/NF_LOANA_DUEAMOUNTFEE2)}&lt;/ns4:dueAmountFee2>
      	else ()
      }
      {if (data($parm/NF_LOANA_DUEAMOUNTFEE3) and xf:isData(data($parm/NF_LOANA_DUEAMOUNTFEE3)))
          then &lt;ns4:dueAmountFee3>{data($parm/NF_LOANA_DUEAMOUNTFEE3)}&lt;/ns4:dueAmountFee3>
      	else ()
      }
      {if (data($parm/NF_LOANA_ASSESSADJTODATEFE) and xf:isData(data($parm/NF_LOANA_ASSESSADJTODATEFE)))
          then &lt;ns4:assessAdjToDateFee1>{data($parm/NF_LOANA_ASSESSADJTODATEFE)}&lt;/ns4:assessAdjToDateFee1>
      	else ()
      }
      {if (data($parm/NF_LOANA_ASSESSADJTODATFEE) and xf:isData(data($parm/NF_LOANA_ASSESSADJTODATFEE)))
          then &lt;ns4:assessAdjToDateFee2>{data($parm/NF_LOANA_ASSESSADJTODATFEE)}&lt;/ns4:assessAdjToDateFee2>
      	else ()
      }
      {if (data($parm/NF_LOANA_ASSADJTODATFEE3) and xf:isData(data($parm/NF_LOANA_ASSADJTODATFEE3)))
          then &lt;ns4:assessAdjToDateFee3>{data($parm/NF_LOANA_ASSADJTODATFEE3)}&lt;/ns4:assessAdjToDateFee3>
      	else ()
      }
      {if (data($parm/NF_LOANA_PAIDTODATEFEE1) and xf:isData(data($parm/NF_LOANA_PAIDTODATEFEE1)))
          then &lt;ns4:paidToDateFee1>{data($parm/NF_LOANA_PAIDTODATEFEE1)}&lt;/ns4:paidToDateFee1>
      	else ()
      }
      {if (data($parm/NF_LOANA_PAIDTODATEFEE2) and xf:isData(data($parm/NF_LOANA_PAIDTODATEFEE2)))
          then &lt;ns4:paidToDateFee2>{data($parm/NF_LOANA_PAIDTODATEFEE2)}&lt;/ns4:paidToDateFee2>
      	else ()
      }
      {if (data($parm/NF_LOANA_PAIDTODATEFEE3) and xf:isData(data($parm/NF_LOANA_PAIDTODATEFEE3)))
          then &lt;ns4:paidToDateFee3>{data($parm/NF_LOANA_PAIDTODATEFEE3)}&lt;/ns4:paidToDateFee3>
      	else ()
      }
      {if (data($parm/NF_LOANA_SUBSIDYOPTION) and xf:isData(data($parm/NF_LOANA_SUBSIDYOPTION)))
          then &lt;ns4:subsidyOption>{xf:short2bool($parm/NF_LOANA_SUBSIDYOPTION)}&lt;/ns4:subsidyOption>
      	else ()
      }
      {if (data($parm/NF_LOANA_ORIGSUBSLOANAMT) and xf:isData(data($parm/NF_LOANA_ORIGSUBSLOANAMT)))
          then &lt;ns4:origSubsLoanAmt>{data($parm/NF_LOANA_ORIGSUBSLOANAMT)}&lt;/ns4:origSubsLoanAmt>
      	else ()
      }
      {if (data($parm/NF_LOANA_SUBSIDYLIMIT) and xf:isData(data($parm/NF_LOANA_SUBSIDYLIMIT)))
          then &lt;ns4:subsidyLimit>{data($parm/NF_LOANA_SUBSIDYLIMIT)}&lt;/ns4:subsidyLimit>
      	else ()
      }
      {if (data($parm/NF_LOANA_RATEEFFDATE) and xf:isData(data($parm/NF_LOANA_RATEEFFDATE)))
          then &lt;ns4:rateEffDate>{data($parm/NF_LOANA_RATEEFFDATE)}&lt;/ns4:rateEffDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_DAILYFACTOR) and xf:isData(data($parm/NF_LOANA_DAILYFACTOR)))
          then &lt;ns4:dailyFactor>{data($parm/NF_LOANA_DAILYFACTOR)}&lt;/ns4:dailyFactor>
      	else ()
      }
      {if (data($parm/NF_LOANA_SUBSIDYELIGIBLEDA) and xf:isData(data($parm/NF_LOANA_SUBSIDYELIGIBLEDA)))
          then &lt;ns4:subsidyEligibleDate>{data($parm/NF_LOANA_SUBSIDYELIGIBLEDA)}&lt;/ns4:subsidyEligibleDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_SUBSIDYLIMITEFFDA) and xf:isData(data($parm/NF_LOANA_SUBSIDYLIMITEFFDA)))
          then &lt;ns4:subsidyLimitEffDate>{data($parm/NF_LOANA_SUBSIDYLIMITEFFDA)}&lt;/ns4:subsidyLimitEffDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_CURRQUALBAL) and xf:isData(data($parm/NF_LOANA_CURRQUALBAL)))
          then &lt;ns4:currQualBal>{data($parm/NF_LOANA_CURRQUALBAL)}&lt;/ns4:currQualBal>
      	else ()
      }
      {if (data($parm/NF_LOANA_SUBSIDYBALANCE) and xf:isData(data($parm/NF_LOANA_SUBSIDYBALANCE)))
          then &lt;ns4:subsidyBalance>{data($parm/NF_LOANA_SUBSIDYBALANCE)}&lt;/ns4:subsidyBalance>
      	else ()
      }
      {if (data($parm/NF_LOANA_SUBSIDYRATE) and xf:isData(data($parm/NF_LOANA_SUBSIDYRATE)))
          then &lt;ns4:subsidyRate>{data($parm/NF_LOANA_SUBSIDYRATE)}&lt;/ns4:subsidyRate>
      	else ()
      }
      {if (data($parm/NF_LOANA_SUBSIDYACCRUED) and xf:isData(data($parm/NF_LOANA_SUBSIDYACCRUED)))
          then &lt;ns4:subsidyAccrued>{data($parm/NF_LOANA_SUBSIDYACCRUED)}&lt;/ns4:subsidyAccrued>
      	else ()
      }
      {if (data($parm/NF_LOANA_NEXTSUBSIDYCLAIMD) and xf:isData(data($parm/NF_LOANA_NEXTSUBSIDYCLAIMD)))
          then &lt;ns4:nextSubsidyClaimDate>{data($parm/NF_LOANA_NEXTSUBSIDYCLAIMD)}&lt;/ns4:nextSubsidyClaimDate>
      	else ()
      }
      {if (data($parm/NF_LOANA_LATEINDEXRATEVALU) and xf:isData(data($parm/NF_LOANA_LATEINDEXRATEVALU)))
          then &lt;ns4:lateIndexRateValue>{data($parm/NF_LOANA_LATEINDEXRATEVALU)}&lt;/ns4:lateIndexRateValue>
      	else ()
      }
      {if (data($parm/NF_LOANA_NEXTSCHEDULEDBILL) and xf:isData(data($parm/NF_LOANA_NEXTSCHEDULEDBILL)))
          then &lt;ns4:nextScheduledBilling>{data($parm/NF_LOANA_NEXTSCHEDULEDBILL)}&lt;/ns4:nextScheduledBilling>
      	else ()
      }
      {if (data($parm/NF_LOANA_PAYMENTBILLEDSTAT) and xf:isData(data($parm/NF_LOANA_PAYMENTBILLEDSTAT)))
          then &lt;ns4:paymentBilledStatus>{data($parm/NF_LOANA_PAYMENTBILLEDSTAT)}&lt;/ns4:paymentBilledStatus>
      	else ()
      }
      {if (data($parm/NF_LOANA_PASTDUEDAYS) and xf:isData(data($parm/NF_LOANA_PASTDUEDAYS)))
          then &lt;ns4:pastDueDays>{data($parm/NF_LOANA_PASTDUEDAYS)}&lt;/ns4:pastDueDays>
      	else ()
      }
      {if (data($parm/NF_LOANA_CYCLE1XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE1XLATEANDNOT)))
          then &lt;ns4:cycle1XLateAndNotices>{data($parm/NF_LOANA_CYCLE1XLATEANDNOT)}&lt;/ns4:cycle1XLateAndNotices>
      	else ()
      }
      {if (data($parm/NF_LOANA_CYCLE2XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE2XLATEANDNOT)))
          then &lt;ns4:cycle2XLateAndNotices>{data($parm/NF_LOANA_CYCLE2XLATEANDNOT)}&lt;/ns4:cycle2XLateAndNotices>
      	else ()
      }
      {if (data($parm/NF_LOANA_CYCLE3XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE3XLATEANDNOT)))
          then &lt;ns4:cycle3XLateAndNotices>{data($parm/NF_LOANA_CYCLE3XLATEANDNOT)}&lt;/ns4:cycle3XLateAndNotices>
      	else ()
      }
      {if (data($parm/NF_LOANA_CYCLE4XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE4XLATEANDNOT)))
          then &lt;ns4:cycle4XLateAndNotices>{data($parm/NF_LOANA_CYCLE4XLATEANDNOT)}&lt;/ns4:cycle4XLateAndNotices>
      	else ()
      }
      {if (data($parm/NF_LOANA_CYCLE5XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE5XLATEANDNOT)))
          then &lt;ns4:cycle5XLateAndNotices>{data($parm/NF_LOANA_CYCLE5XLATEANDNOT)}&lt;/ns4:cycle5XLateAndNotices>
      	else ()
      }
      {if (data($parm/NF_LOANA_CYCLE6XLATEANDNOT) and xf:isData(data($parm/NF_LOANA_CYCLE6XLATEANDNOT)))
          then &lt;ns4:cycle6XLateAndNotices>{data($parm/NF_LOANA_CYCLE6XLATEANDNOT)}&lt;/ns4:cycle6XLateAndNotices>
      	else ()
      }
          &lt;ns4:censusTract>
            &lt;ns3:CreditObject>
           {if (data($parm/NF_CREDIO_OBJECT) and xf:isData(data($parm/NF_CREDIO_OBJECT)))
              then &lt;ns3:object?>{data($parm/NF_CREDIO_OBJECT)}&lt;/ns3:object>
        	else ()
           }
            &lt;/ns3:CreditObject>
          &lt;/ns4:censusTract>
        &lt;/ns4:LoanAccount>
      &lt;/ns4:loanAccount>
      &lt;ns4:currency>
        &lt;ns3:CurrencyCode>
           {if (data($parm/NF_CURREC_CURRENCYCODE2) and xf:isData(data($parm/NF_CURREC_CURRENCYCODE2)))
              then &lt;ns3:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE2)}&lt;/ns3:currencyCode>
        	else ()
           }
        &lt;/ns3:CurrencyCode>
      &lt;/ns4:currency>
    &lt;/ns4:Account>
  &lt;/ns0:accountOut>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>