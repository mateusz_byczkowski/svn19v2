<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace cse = "http://bzwbk.com/crw/services/ewnioski/";
declare namespace cee = "http://bzwbk.com/crw/entity/ewnioski/";
declare namespace sce = "http://bzwbk.com/services/crw/eWnioski/";

declare function local:applicationExtToInt($application as element())
    as element(application) {
    <application>
      	<id?>{ data($application/id) }</id>
        <advisorID?>{ data($application/advisorID) }</advisorID>
        <destinationBranch?>{ data($application/destinationBranch) }</destinationBranch>
        <extSource?>{ data($application/extSource) }</extSource>
        <number?>{ data($application/number) }</number>
        <registrationBranch?>{ data($application/registrationBranch) }</registrationBranch>
        <source?>{ data($application/source) }</source>
        <status?>{ data($application/status) }</status>
        <type?>{ data($application/type) }</type>
        <accountNumber?>{ data($application/accountNumber) }</accountNumber>
        <additionalAppNumber?>{ data($application/additionalAppNumber) }</additionalAppNumber>
        <additionalPassword?>{ data($application/additionalPassword) }</additionalPassword>
        <deliveryAddress>
            <city?>{ data($application/deliveryAddress/city) }</city>
            <houseFlatNumber?>{ data($application/deliveryAddress/houseFlatNumber) }</houseFlatNumber>
            <street?>{ data($application/deliveryAddress/street) }</street>
            <zipCode?>{ data($application/deliveryAddress/zipCode) }</zipCode>
            <streetType?>{ data($application/deliveryAddress/streetType) }</streetType>
            <country?>{ data($application/deliveryAddress/country) }</country>
        </deliveryAddress>
        <agreement2?>{ data($application/agreement2) }</agreement2>
        <agreement3?>{ data($application/agreement3) }</agreement3>
        <agreement4?>{ data($application/agreement4) }</agreement4>
        <agreement5?>{ data($application/agreement5) }</agreement5>
        <agreement6?>{ data($application/agreement6) }</agreement6>
        <agreement7?>{ data($application/agreement7) }</agreement7>
        <agreementDate?>{ data($application/agreementDate) }</agreementDate>
        <agreementDelivery?>{ data($application/agreementDelivery) }</agreementDelivery>
        <authorization1?>{ data($application/authorization1) }</authorization1>
        <customerRoles>
        {
            for $customer in $application/customer
            return
                <customer>
                    <id?>{ data($customer/id) }</id>
                    <birthDate?>{ data($customer/birthDate) }</birthDate>
                    <firstName?>{ data($customer/firstName) }</firstName>
                    <lastName?>{ data($customer/lastName) }</lastName>
                    <pesel?>{ data($customer/pesel) }</pesel>
                    <cif?>{ data($customer/cif) }</cif>
                    <citizenship?>{ data($customer/citizenship) }</citizenship>
                    <email?>{ data($customer/email) }</email>
                    <fatherName?>{ data($customer/fatherName) }</fatherName>
                    <homePhoneNumber?>{ data($customer/homePhoneNumber) }</homePhoneNumber>
                    <homePhonePrefix?>{ data($customer/homePhonePrefix) }</homePhonePrefix>
                    <idSerNum?>{ data($customer/idSerNum) }</idSerNum>
                    <idType?>{ data($customer/idType) }</idType>
                    <lastName2?>{ data($customer/lastName2) }</lastName2>
                    <middleName?>{ data($customer/middleName) }</middleName>
                    <mobilePhoneNumber?>{ data($customer/mobilePhoneNumber) }</mobilePhoneNumber>
                    <mobilePhonePrefix?>{ data($customer/mobilePhonePrefix) }</mobilePhonePrefix>
                    <motherMaidenName?>{ data($customer/motherMaidenName) }</motherMaidenName>
                    <nik?>{ data($customer/nik) }</nik>
                    <passport?>{ data($customer/passport) }</passport>
                    <phoneAvailability?>{ data($customer/phoneAvailability) }</phoneAvailability>
                    <placeOfBirth?>{ data($customer/placeOfBirth) }</placeOfBirth>
                    {
                        for $primaryAddress in $customer/primaryAddress
                        return
                            <primaryAddress>
                                <city?>{ data($primaryAddress/city) }</city>
                                <houseFlatNumber?>{ data($primaryAddress/houseFlatNumber) }</houseFlatNumber>
                                <street?>{ data($primaryAddress/street) }</street>
                                <zipCode?>{ data($primaryAddress/zipCode) }</zipCode>
					            <streetType?>{ data($primaryAddress/streetType) }</streetType>
					            <country?>{ data($primaryAddress/country) }</country>
                            </primaryAddress>
                    }
                    {
                        for $secondaryAddress in $customer/secondaryAddress
                        return
                            <secondaryAddress>
                                <city>{ data($secondaryAddress/city) }</city>
                                <houseFlatNumber>{ data($secondaryAddress/houseFlatNumber) }</houseFlatNumber>
                                <street>{ data($secondaryAddress/street) }</street>
                                <zipCode>{ data($secondaryAddress/zipCode) }</zipCode>
					            <streetType?>{ data($secondaryAddress/streetType) }</streetType>
					            <country?>{ data($secondaryAddress/country) }</country>
                            </secondaryAddress>
                    }
                    <sex?>{ data($customer/sex) }</sex>
                    <spw?>{ data($customer/spw) }</spw>
                    (: 20.11.2008 :)
                    <hasBZWBKPersonalAccount?>{ data($customer/hasBZWBKPersonalAccount) }</hasBZWBKPersonalAccount>
                    <idSerNumStartDate?>{ data($customer/idSerNumStartDate) }</idSerNumStartDate>
                    <passportEndDate?>{ data($customer/passportEndDate) }</passportEndDate>
                    <status?>{ data($customer/status) }</status>
                    (: 12.02.2009 :)
                    <emailAgreement?>{ data($customer/emailAgreement) }</emailAgreement>
                </customer>
        }
        </customerRoles>
        <debitCardPresent?>{ data($application/debitCardPresent) }</debitCardPresent>
        <debitCardNumber?>{ data($application/debitCardNumber) }</debitCardNumber>
        <debitCardStatementDelivery?>{ data($application/debitCardStatementDelivery) }</debitCardStatementDelivery>
        <debitCardType?>{ data($application/debitCardType) }</debitCardType>
        <hasBZWBK24?>{ data($application/hasBZWBK24) }</hasBZWBK24>
        <promotionCode?>{ data($application/promotionCode) }</promotionCode>
        <smsCodePhoneNumber?>{ data($application/smsCodePhoneNumber) }</smsCodePhoneNumber>
        <smsCodePhonePrefix?>{ data($application/smsCodePhonePrefix) }</smsCodePhonePrefix>
        <statementDay?>{ data($application/statementDay) }</statementDay>
        <statementDelivery?>{ data($application/statementDelivery) }</statementDelivery>
        <statementPeriod?>{ data($application/statementPeriod) }</statementPeriod>
        <statementType?>{ data($application/statementType) }</statementType>
        <statementsAgreements?>{ data($application/statementsAgreements) }</statementsAgreements>
        <wantsBZWBK24?>{ data($application/wantsBZWBK24) }</wantsBZWBK24>
        <wantsTransactionalBZWBK24?>{ data($application/wantsTransactionalBZWBK24) }</wantsTransactionalBZWBK24>
        (: 20.11.2008 :)
		<advisorEmail?>{ data($application/advisorEmail) }</advisorEmail>
		<debitCardInsurancePresent?>{ data($application/debitCardInsurancePresent) }</debitCardInsurancePresent>
		<extLogin?>{ data($application/extLogin) }</extLogin>
		<registrator?>{ data($application/registrator) }</registrator>
		<textOnCard?>{ data($application/textOnCard) }</textOnCard>
		<textOnCard2?>{ data($application/textOnCard2) }</textOnCard2>
		<wantsLoanApplication?>{ data($application/wantsLoanApplication) }</wantsLoanApplication>
		(: 12.02.2009 :)
		<agreement8?>{ data($application/agreement8) }</agreement8>
		<agreement9?>{ data($application/agreement9) }</agreement9>
		<agreement10?>{ data($application/agreement10) }</agreement10>
		<wantsAlerts?>{ data($application/wantsAlerts) }</wantsAlerts>
		<authType?>{ data($application/authType) }</authType>
		<accountCurrency?>{ data($application/accountCurrency) }</accountCurrency>
		<savingsAccount?>{ data($application/savingsAccount) }</savingsAccount>
		<savingsAccountType?>{ data($application/savingsAccountType) }</savingsAccountType>
		<virtualDebitCardNumber?>{ data($application/virtualDebitCardNumber) }</virtualDebitCardNumber>
		<PB1?>{ data($application/PB1) }</PB1>
		<PB2?>{ data($application/PB2) }</PB2>
		<PB3?>{ data($application/PB3) }</PB3>
		<infolinia?>{ data($application/infolinia) }</infolinia>
    </application>
};

declare function local:applicationIntToExt($application as element())
    as element() {
    <result>
    	<id?>{ data($application/id) }</id>
        <number?>{ data($application/number) }</number>
        <type?>{ data($application/type) }</type>
        <registrationBranch?>{ data($application/registrationBranch) }</registrationBranch>
        <destinationBranch?>{ data($application/destinationBranch) }</destinationBranch>
        <extSource?>{ data($application/extSource) }</extSource>
        <source?>{ data($application/source) }</source>
        <advisorID?>{ data($application/advisorID) }</advisorID>
        <status?>{ data($application/status) }</status>
        <agreementDelivery?>{ data($application/agreementDelivery) }</agreementDelivery>
        <agreementDate?>{ data($application/agreementDate) }</agreementDate>
        <additionalAppNumber?>{ data($application/additionalAppNumber) }</additionalAppNumber>
        <debitCardNumber?>{ data($application/debitCardNumber) }</debitCardNumber>
        <accountNumber?>{ data($application/accountNumber) }</accountNumber>
        <statementsAgreements?>{ data($application/statementsAgreements) }</statementsAgreements>
        <statementType?>{ data($application/statementType) }</statementType>
        <statementPeriod?>{ data($application/statementPeriod) }</statementPeriod>
        <statementDay?>{ data($application/statementDay) }</statementDay>
        <statementDelivery?>{ data($application/statementDelivery) }</statementDelivery>
        <hasBZWBK24?>{ data($application/hasBZWBK24) }</hasBZWBK24>
        <wantsBZWBK24?>{ data($application/wantsBZWBK24) }</wantsBZWBK24>
        <wantsTransactionalBZWBK24?>{ data($application/wantsTransactionalBZWBK24) }</wantsTransactionalBZWBK24>
        <additionalPassword?>{ data($application/additionalPassword) }</additionalPassword>
        <smsCodePhonePrefix?>{ data($application/smsCodePhonePrefix) }</smsCodePhonePrefix>
        <smsCodePhoneNumber?>{ data($application/smsCodePhoneNumber) }</smsCodePhoneNumber>
        <debitCardPresent?>{ data($application/debitCardPresent) }</debitCardPresent>
        <debitCardType?>{ data($application/debitCardType) }</debitCardType>
        <debitCardStatementDelivery?>{ data($application/debitCardStatementDelivery) }</debitCardStatementDelivery>
        <promotionCode?>{ data($application/promotionCode) }</promotionCode>
        <authorization1?>{ data($application/authorization1) }</authorization1>
        <agreement2?>{ data($application/agreement2) }</agreement2>
        <agreement3?>{ data($application/agreement3) }</agreement3>
        <agreement4?>{ data($application/agreement4) }</agreement4>
        <agreement5?>{ data($application/agreement5) }</agreement5>
        <agreement6?>{ data($application/agreement6) }</agreement6>
        <agreement7?>{ data($application/agreement7) }</agreement7>
        {
            for $customer in $application/customerRoles/customer
            return
                <customer>
                    <id?>{ data($customer/id) }</id>
                    <firstName?>{ data($customer/firstName) }</firstName>
                    <lastName?>{ data($customer/lastName) }</lastName>
                    <birthDate?>{ data($customer/birthDate) }</birthDate>
                    <pesel?>{ data($customer/pesel) }</pesel>
                    <idSerNum?>{ data($customer/idSerNum) }</idSerNum>
                    <nik?>{ data($customer/nik) }</nik>
                    <cif?>{ data($customer/cif) }</cif>
                    <passport?>{ data($customer/passport) }</passport>
                    <sex?>{ data($customer/sex) }</sex>
                    <idType?>{ data($customer/idType) }</idType>
                    <placeOfBirth?>{ data($customer/placeOfBirth) }</placeOfBirth>
                    <citizenship?>{ data($customer/citizenship) }</citizenship>
                    <homePhonePrefix?>{ data($customer/homePhonePrefix) }</homePhonePrefix>
                    <homePhoneNumber?>{ data($customer/homePhoneNumber) }</homePhoneNumber>
                    <phoneAvailability?>{ data($customer/phoneAvailability) }</phoneAvailability>
                    <mobilePhonePrefix?>{ data($customer/mobilePhonePrefix) }</mobilePhonePrefix>
                    <mobilePhoneNumber?>{ data($customer/mobilePhoneNumber) }</mobilePhoneNumber>
                    <email?>{ data($customer/email) }</email>
                    <spw?>{ data($customer/spw) }</spw>
                    <motherMaidenName?>{ data($customer/motherMaidenName) }</motherMaidenName>
                    <fatherName?>{ data($customer/fatherName) }</fatherName>
                    <middleName?>{ data($customer/middleName) }</middleName>
                    <lastName2?>{ data($customer/lastName2) }</lastName2>
                    {
                        for $primaryAddress in $customer/primaryAddress
                        return
                            <primaryAddress>
                                <street?>{ data($primaryAddress/street) }</street>
                                <city?>{ data($primaryAddress/city) }</city>
                                <zipCode?>{ data($primaryAddress/zipCode) }</zipCode>
                                <houseFlatNumber?>{ data($primaryAddress/houseFlatNumber) }</houseFlatNumber>
					            <streetType?>{ data($primaryAddress/streetType) }</streetType>
					            <country?>{ data($primaryAddress/country) }</country>
                            </primaryAddress>
                    }
                    {
                        for $secondaryAddress in $customer/secondaryAddress
                        return
                            <secondaryAddress>
                                <street>{ data($secondaryAddress/street) }</street>
                                <city>{ data($secondaryAddress/city) }</city>
                                <zipCode>{ data($secondaryAddress/zipCode) }</zipCode>
                                <houseFlatNumber>{ data($secondaryAddress/houseFlatNumber) }</houseFlatNumber>
					            <streetType?>{ data($secondaryAddress/streetType) }</streetType>
					            <country?>{ data($secondaryAddress/country) }</country>
                            </secondaryAddress>
                    }
			        (: 20.11.2008 :)
                    <hasBZWBKPersonalAccount?>{ data($customer/hasBZWBKPersonalAccount) }</hasBZWBKPersonalAccount>
                    <idSerNumStartDate?>{ data($customer/idSerNumStartDate) }</idSerNumStartDate>
                    <passportEndDate?>{ data($customer/passportEndDate) }</passportEndDate>
                    <status?>{ data($customer/status) }</status>
                    (: 12.02.2009 :)
                    <emailAgreement?>{ data($customer/emailAgreement) }</emailAgreement>
                </customer>
        }
        <deliveryAddress>
            <street?>{ data($application/deliveryAddress/street) }</street>
            <city?>{ data($application/deliveryAddress/city) }</city>
            <zipCode?>{ data($application/deliveryAddress/zipCode) }</zipCode>
            <houseFlatNumber?>{ data($application/deliveryAddress/houseFlatNumber) }</houseFlatNumber>
            <streetType?>{ data($application/deliveryAddress/streetType) }</streetType>
            <country?>{ data($application/deliveryAddress/country) }</country>
        </deliveryAddress>
        (: 20.11.2008 :)
        <advisorEmail?>{ data($application/advisorEmail) }</advisorEmail>
		<debitCardInsurancePresent?>{ data($application/debitCardInsurancePresent) }</debitCardInsurancePresent>
		<extLogin?>{ data($application/extLogin) }</extLogin>
		<registrator?>{ data($application/registrator) }</registrator>
		<textOnCard?>{ data($application/textOnCard) }</textOnCard>
		<textOnCard2?>{ data($application/textOnCard2) }</textOnCard2>
		<wantsLoanApplication?>{ data($application/wantsLoanApplication) }</wantsLoanApplication>
		(: 12.02.2009 :)
		<agreement8?>{ data($application/agreement8) }</agreement8>
		<agreement9?>{ data($application/agreement9) }</agreement9>
		<agreement10?>{ data($application/agreement10) }</agreement10>
		<wantsAlerts?>{ data($application/wantsAlerts) }</wantsAlerts>
		<authType?>{ data($application/authType) }</authType>
		<accountCurrency?>{ data($application/accountCurrency) }</accountCurrency>
		<savingsAccount?>{ data($application/savingsAccount) }</savingsAccount>
		<savingsAccountType?>{ data($application/savingsAccountType) }</savingsAccountType>
		<virtualDebitCardNumber?>{ data($application/virtualDebitCardNumber) }</virtualDebitCardNumber>
		<PB1?>{ data($application/PB1) }</PB1>
		<PB2?>{ data($application/PB2) }</PB2>
		<PB3?>{ data($application/PB3) }</PB3>
		<infolinia?>{ data($application/infolinia) }</infolinia>
    </result>
};

declare function local:authDataExtToInt($authData as element())
    as element(authData) {
    <authData>
        <applicationId>{ data($authData/ApplicationId) }</applicationId>
        <applicationPassword>{ data($authData/ApplicationPassword) }</applicationPassword>
        <operator>{ data($authData/Operator) }</operator>
    </authData>
};

declare variable $body external;

<soap-env:Body>
{
	typeswitch($body/*[1])
		case $req as element(sce:readApplication) return
			<cse:readApplication>
				<number>{ data($req/id) }</number>
			    { local:authDataExtToInt($req/authData) }
			</cse:readApplication>
		case $resp as element(cse:readApplicationResponse) return
			<sce:readApplicationResponse>
				{ local:applicationIntToExt($resp/application) }
			</sce:readApplicationResponse>
		case $req as element(sce:createApplication) return
			<cse:createApplication>
				{ local:applicationExtToInt($req/application) }
			    { local:authDataExtToInt($req/authData) }
			</cse:createApplication>
		case $resp as element(cse:createApplicationResponse) return
			<sce:createApplicationResponse>
				<result>{ data($resp/number) }</result>
			</sce:createApplicationResponse>
		case $req as element(sce:updateApplication) return
			<cse:updateApplication>
				{ local:applicationExtToInt($req/application) }
			    { local:authDataExtToInt($req/authData) }
			</cse:updateApplication>
		case $resp as element(cse:updateApplicationResponse) return
			<sce:updateApplicationResponse />
		case $req as element(sce:changeApplicationStatus) return
			<cse:changeApplicationStatus>
				<number>{ data($req/id) }</number>
				<status>{ data($req/status) }</status>
			    { local:authDataExtToInt($req/authData) }				
			</cse:changeApplicationStatus>
		case $resp as element(cse:changeApplicationStatusResponse) return
			<sce:changeApplicationStatusResponse />
		default return $body
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>