<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/BZWBK24flow/messages/";



declare function xf:map_getStartBbcP1Request($fml as element(fml:FML32))
	as element(urn:StartBBC1Request) {
		<urn:StartBBC1Request>
			 <urn:CekeApplicationBean>
       			 <urn:customer>
       			 	<urn:name>{ data($fml/fml:DC_IMIE) }</urn:name>
           		    <urn:surname>{ data($fml/fml:DC_NAZWISKO) }</urn:surname>
          		    <urn:pesel>{ data($fml/fml:DC_NR_PESEL) }</urn:pesel>
            		<urn:primaryAddress>
                		<urn:country>{ data($fml/fml:DC_KOD_KRAJU) }</urn:country>
                		<urn:housenr>{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST) }</urn:housenr>
                		<urn:street>{ data($fml/fml:DC_ULICA_DANE_PODST) }</urn:street>
		                <urn:city>{ data($fml/fml:DC_MIASTO_DANE_PODST) }</urn:city>
		                <urn:postalcode>{ data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST) }</urn:postalcode>
		            </urn:primaryAddress>
		            <urn:cif>{ data($fml/fml:DC_NUMER_KLIENTA) }</urn:cif>
		            <urn:nik>{ data($fml/fml:E_LOGIN_ID) }</urn:nik>
		            <urn:card>{ data($fml/fml:DC_NR_DOWODU_REGON) }</urn:card>
                        <urn:icbsShortName>{ data($fml/fml:DC_NAZWA_SKROCONA) }</urn:icbsShortName>
                        <urn:homeBranch>{ data($fml/fml:B_MIKROODDZIAL) }</urn:homeBranch>
       			 </urn:customer>
       			 <urn:genContrDate>{ concat( substring(data($fml/fml:E_AGR_SIGN_DATE),1,10),'T', substring(data($fml/fml:E_AGR_SIGN_DATE),12,19) ) }</urn:genContrDate>
		         <urn:wnmobile>{ data($fml/fml:DC_NR_TELEF_KOMORKOWEGO) }</urn:wnmobile>
		         <urn:wnmobilep></urn:wnmobilep>
		         <urn:wnphone>{ data($fml/fml:DC_NR_TELEFONU) }</urn:wnphone>
		        <urn:wnareacode></urn:wnareacode>
		         <urn:diSubscrip>2</urn:diSubscrip>
		         <urn:contractId>{ data($fml/fml:E_AGR_SIGNATURE) }</urn:contractId>
		         <urn:portal>BZWBK24</urn:portal>
		        <urn:diProduct>{ data($fml/fml:E_PRODUCT_ID) }</urn:diProduct>
		         <urn:wnemail>{ data($fml/fml:DC_ADRES_E_MAIL) }</urn:wnemail>
		         <urn:contractStatus>{ data($fml/fml:E_AGR_STATUS) }</urn:contractStatus>
		         <urn:contractAddress>
		             <urn:country>{ data($fml/fml:E_AGR_COUNTRY) }</urn:country>
		             <urn:housenr>{ data($fml/fml:E_AGR_HOUSE_FLAT) }</urn:housenr>
		             <urn:street>{ data($fml/fml:E_AGR_STREET) }</urn:street>
		             <urn:city>{ data($fml/fml:E_AGR_CITY) }</urn:city>
		             <urn:postalcode>{ data($fml/fml:E_AGR_ZIP_CODE) }</urn:postalcode>
		         </urn:contractAddress>
                        <urn:skp>{ data($fml/fml:CI_SKP_PRACOWNIKA) }</urn:skp>
       		 </urn:CekeApplicationBean>
		</urn:StartBBC1Request>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_getStartBbcP1Request($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>