<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace op = "http://service.ws.flow.pbpolsoft.com.pl";
declare namespace tns1 = "http://bzwbk.com/services/BZWBK24flow/messages/";
declare namespace tns2 = "http://bzwbk.com/services/ewnioski/messages/";
declare namespace tns3 = "http://service.ws.flow.pbpolsoft.com.pl";
declare namespace tns4 = "urn:pbpolsoft.com.pl";
declare namespace xsd="http://www.w3.org/2001/XMLSchema";


declare function xf:transformBody ($req as element(soap-env:Body)) as element(soap-env:Body) {
	
	&lt;soap-env:Body>
	{
			
			if($req/tns1:StartBBC1Request)
			then 	&lt;tns3:startBBCP1>&lt;application href="#id0" />&lt;/tns3:startBBCP1>
			else()
			
	}

	{
			if($req/tns1:StartBBC2Request)
			then 	&lt;tns3:startBBCP2>&lt;application href="#id0" />&lt;/tns3:startBBCP2>
			else()
			
	}
	
	{
			if($req/tns1:StartBBC3Request)
			then 	&lt;tns3:startBBCP3>&lt;application href="#id0" />&lt;/tns3:startBBCP3>
			else()
			
	}

	{
			if($req/tns1:StartBBC4Request)
			then 	&lt;tns3:startBBCP4>&lt;application href="#id0" />&lt;/tns3:startBBCP4>
			else()
			
	}

	{	
	        xf:transformCekeApplicationBean($req/*/tns1:CekeApplicationBean)
	}
	
	{	
			if ($req/*/tns1:CekeApplicationBean/tns1:customer/*)
			then (xf:transformCustomer($req/*/tns1:CekeApplicationBean/tns1:customer))
			else ()
	}
	
	{	
			if ($req/*/tns1:CekeApplicationBean/tns1:contractAddress/*)
	        	then (xf:transformAddress($req/*/tns1:CekeApplicationBean/tns1:contractAddress,"id4"))
	        	else ()
	}
	
	
	{	
			if ($req/*/tns1:CekeApplicationBean/tns1:customer/tns1:primaryAddress/*)
	        	then (xf:transformAddress($req/*/tns1:CekeApplicationBean/tns1:customer/tns1:primaryAddress,"id5"))
	        	else ()
	}
	
	{	
			if ($req/*/tns1:CekeApplicationBean/tns1:customer/tns1:secondaryAddress/*)
	        	then (xf:transformAddress($req/*/tns1:CekeApplicationBean/tns1:customer/tns1:secondaryAddress,"id6"))
	        	else ()
	}
	
	
	{
	               if ($req/*/tns1:CekeApplicationBean/tns1:account/*)	
	        	then (xf:transformAccount($req/*/tns1:CekeApplicationBean/tns1:account))
	        	else ()
	}
	
	{
	                if ($req/*/tns1:CekeApplicationBean/tns1:account/tns1:statement/*)	
	        	then (xf:transformStatementsParameters($req/*/tns1:CekeApplicationBean/tns1:account/tns1:statement))
	        	else ()
	}
		
	{	
			if ($req/*/tns1:CekeApplicationBean/tns1:debitCard/*)
	        	then (xf:transformDebitCard($req/*/tns1:CekeApplicationBean/tns1:debitCard))
	        	else () 	
	}

	
	
	
	&lt;/soap-env:Body>
};

declare function xf:transformCekeApplicationBean ($req as element(tns1:CekeApplicationBean)) as element(multiRef) {
	
	&lt;multiRef id="id0" xsi:type="tns4:CekeApplicationBean" xmlns:tns4="urn:pbpolsoft.com.pl">
		
		{
		if ($req/tns1:customer/*)
			then &lt;customer href="#id1" />
			else &lt;customer xsi:type="tns4:CekeCustomerBean" xsi:nil="true"/>
		}
		
		{
		if ($req/tns1:debitCard/*)
			then &lt;debitCard href="#id2" />
			else &lt;debitCard xsi:type="tns4:CekeDebitCardBean" xsi:nil="true"/>
		}
		
				{
		if ($req/tns1:account/*)
			then &lt;account href="#id3" />
			else &lt;account xsi:type="tns4:CekeCustomerBean" xsi:nil="true"/>
		}
		
		{
		if ($req/tns1:contractAddress/*)
			then &lt;contractAddress href="#id4" />
			else &lt;contractAddress xsi:type="tns4:CekeAddressBean" xsi:nil="true"/>
		}

		
		{
		if($req/tns1:genContrDate)
			then &lt;genContrDate xsi:type="xsd:dateTime">{data($req/tns1:genContrDate)}&lt;/genContrDate>
			else ()
		}
		
		{
			if($req/tns1:wnmobile)
			then &lt;wnmobile xsi:type="soapenc:string">{data($req/tns1:wnmobile)}&lt;/wnmobile>
			else()
		}
		
		{
			if($req/tns1:portalCode)
			then &lt;portalCode xsi:type="soapenc:string">{data($req/tns1:portalCode)}&lt;/portalCode>
			else()
		}
		
		{
			if($req/tns1:wnmobilep)
			then &lt;wnmobilep xsi:type="soapenc:string">{data($req/tns1:wnmobilep)}&lt;/wnmobilep>
			else()
		}
		
		{
			if($req/tns1:wnphone)
			then &lt;wnphone xsi:type="soapenc:string">{data($req/tns1:wnphone)}&lt;/wnphone>
			else()
		}
		
		{
			if($req/tns1:chPaycard)
			then &lt;chPaycard xsi:type="soapenc:boolean">{data($req/tns1:chPaycard)}&lt;/chPaycard>
			else()
		}	

		{
			if($req/tns1:wnareacode)
			then &lt;wnareacode xsi:type="soapenc:string">{data($req/tns1:wnareacode)}&lt;/wnareacode>
			else()
		}
	
		{
			if($req/tns1:diSubscrip)
			then &lt;diSubscrip xsi:type="soapenc:string">{data($req/tns1:diSubscrip)}&lt;/diSubscrip>
			else()
		}
		
		{
			if($req/tns1:contractId)
			then &lt;contractId xsi:type="soapenc:string">{data($req/tns1:contractId)}&lt;/contractId>
			else()
		}

		{
			if($req/tns1:portal)
			then &lt;portal xsi:type="soapenc:string">{data($req/tns1:portal)}&lt;/portal>
			else()
		}

		{
			if($req/tns1:wnareacode)
			then &lt;wnareacode xsi:type="soapenc:string">{data($req/tns1:genContrDate)}&lt;/wnareacode>
			else()
		}

		{
			if($req/tns1:diProduct)
			then &lt;diProduct xsi:type="soapenc:string">{data($req/tns1:diProduct)}&lt;/diProduct>
			else()
		}

		{
			if($req/tns1:wnemail)
			then &lt;wnemail xsi:type="soapenc:string">{data($req/tns1:wnemail)}&lt;/wnemail>
			else()
		}
		
		{
			if($req/tns1:contractStatus)
			then &lt;contractStatus xsi:type="soapenc:string">{data($req/tns1:contractStatus)}&lt;/contractStatus>
			else()
		}

		{
			if($req/tns1:skp)
			then &lt;skp xsi:type="soapenc:string">{data($req/tns1:skp)}&lt;/skp>
			else()
		}
		
		
	&lt;/multiRef>
};

declare function xf:transformCustomer($req as element(tns1:customer)) as element(multiRef) {
	&lt;multiRef id="id1" xsi:type="tns4:CekeCustomerBean" xmlns:tns4="urn:pbpolsoft.com.pl">
	
	{
			if ($req/tns1:primaryAddress/*)
			then &lt;primaryAddress href="#id5" />
			else &lt;primaryAddress xsi:type="tns4:CekeAddressBean" xsi:nil="true"/>
	}

	{
			if($req/tns1:secondaryAddress/*)
			then &lt;secondaryAddress href="#id6" />
			else &lt;secondaryAddress xsi:type="tns4:CekeAddressBean" xsi:nil="true"/>
	}

	{
			if($req/tns1:name)
			then &lt;name xsi:type="soapenc:string">{data($req/tns1:name)}&lt;/name>
			else()
	}
	
	{
			if($req/tns1:surname)
			then &lt;surname xsi:type="soapenc:string">{data($req/tns1:surname)}&lt;/surname>
			else()
	}
	
	{
			if($req/tns1:pesel)
			then &lt;pesel xsi:type="soapenc:string">{data($req/tns1:pesel)}&lt;/pesel>
			else()
	}
	
	{
			if($req/tns1:mother)
			then &lt;mother xsi:type="soapenc:string">{data($req/tns1:mother)}&lt;/mother>
			else()
	}
		
	{
			if($req/tns1:cif)
			then &lt;cif xsi:type="soapenc:string">{data($req/tns1:cif)}&lt;/cif>
			else()
	}

	{
			if($req/tns1:nik)
			then &lt;nik xsi:type="soapenc:string">{data($req/tns1:nik)}&lt;/nik>
			else()
	}

	{
			if($req/tns1:card)
			then &lt;card xsi:type="soapenc:string">{data($req/tns1:card)}&lt;/card>
			else()
	}

       	{
			if($req/tns1:icbsShortName)
			then &lt;icbsShortName xsi:type="soapenc:string">{data($req/tns1:icbsShortName)}&lt;/icbsShortName>
			else()
	}

       	{
			if($req/tns1:homeBranch)
			then &lt;homeBranch xsi:type="soapenc:string">{data($req/tns1:homeBranch)}&lt;/homeBranch>
			else()
	}




	&lt;/multiRef>
};


declare function xf:transformAddress($req as item(), $ref as xsd:string) as element(multiRef){
	&lt;multiRef id="{$ref}" xsi:type="tns4:CekeAddressBean" xmlns:tns4="urn:pbpolsoft.com.pl">
		{
			if($req/tns1:country)
			then &lt;country xsi:type="soapenc:string">{data($req/tns1:country)}&lt;/country>
			else()
		}
		{
			if($req/tns1:housenr)
			then &lt;housenr xsi:type="soapenc:string">{data($req/tns1:housenr)}&lt;/housenr>
			else()
		}

		{
			if($req/tns1:street)
			then &lt;street xsi:type="soapenc:string">{data($req/tns1:street)}&lt;/street>
			else()
		}	

		{
			if($req/tns1:city)
			then &lt;city xsi:type="soapenc:string">{data($req/tns1:city)}&lt;/city>
			else()
		}

		{
			if($req/tns1:postalcode)
			then &lt;postalcode xsi:type="soapenc:string">{data($req/tns1:postalcode)}&lt;/postalcode>
			else()
		}

	&lt;/multiRef>
};

declare function xf:transformAccount($req as element(tns1:account)) as element(multiRef){
	&lt;multiRef id="id3" xsi:type="tns4:CekeAccountBean" xmlns:tns4="urn:pbpolsoft.com.pl">
	
		{
			if ($req/tns1:statement/*)
			then &lt;statement href="#id7" />
			else &lt;statement xsi:type="tns4:CekeStatementParametersBean" xsi:nil="true"/>
		}

	
		{
			if($req/tns1:accountCurrency)
			then &lt;accountCurrency xsi:type="soapenc:string">{data($req/tns1:accountCurrency)}&lt;/accountCurrency>
			else()
		}
		
		{
			if($req/tns1:chExtract)
				then &lt;chExtract xsi:type="soapenc:boolean">{data($req/tns1:chExtract)}&lt;/chExtract>
			else()
		}
		
		{
			if($req/tns1:accageId)
				then &lt;accageId xsi:type="soapenc:string">{data($req/tns1:accageId)}&lt;/accageId>
			else()
		}
		
		{
			if($req/tns1:accountCode)
			then &lt;accountCode xsi:type="soapenc:string">{data($req/tns1:accountCode)}&lt;/accountCode>
			else()
		}
		
		{
			if($req/tns1:accInterestPl)
			then &lt;accInterestPl xsi:type="soapenc:int">{data($req/tns1:accInterestPl)}&lt;/accInterestPl>
			else()
		}
		
		{
			if($req/tns1:productKind)
			then &lt;productKind xsi:type="soapenc:int">{data($req/tns1:productKind)}&lt;/productKind>
			else()
		}
		
		{
			if($req/tns1:accPromotion)
			then &lt;accPromotion xsi:type="soapenc:string">{data($req/tns1:accPromotion)}&lt;/accPromotion>
			else()
		}
		
		{
			if($req/tns1:accountNr)
			then &lt;accountNr xsi:type="soapenc:string">{data($req/tns1:accountNr)}&lt;/accountNr>
			else()
		}
		
		{
			if($req/tns1:accServicePl)
			then &lt;accServicePl xsi:type="soapenc:int">{data($req/tns1:accServicePl)}&lt;/accServicePl>
			else()
		}

		{
			if($req/tns1:accountName)
			then &lt;accountName xsi:type="soapenc:string">{data($req/tns1:accountName)}&lt;/accountName>
			else()
		}

		
		
	&lt;/multiRef>
};

declare function xf:transformDebitCard($req as element(tns1:debitCard)) as element(multiRef){
	&lt;multiRef id="id2" xsi:type="tns4:CekeDebitCardBean" xmlns:tns4="urn:pbpolsoft.com.pl">
	
		{
			if($req/tns1:cardVNumber)
			then &lt;cardVNumber xsi:type="soapenc:string">{data($req/tns1:cardVNumber)}&lt;/cardVNumber>
			else()
		}
		
		{
			if($req/tns1:chCardinsu)
			then &lt;chCardinsu xsi:type="soapenc:boolean">{data($req/tns1:chCardinsu)}&lt;/chCardinsu>
			else()
		}

		{
			if($req/tns1:cardName)
			then &lt;cardName xsi:type="soapenc:string">{data($req/tns1:cardName)}&lt;/cardName>
			else()
		}

		{
			if($req/tns1:cardLimit)
			then &lt;cardLimit xsi:type="soapenc:double">{data($req/tns1:cardLimit)}&lt;/cardLimit>
			else()
		}

		{
			if($req/tns1:cardType)
			then &lt;cardType xsi:type="soapenc:string">{data($req/tns1:cardType)}&lt;/cardType>
			else()
		}

		{
			if($req/tns1:cardNumber)
			then &lt;cardNumber xsi:type="soapenc:string">{data($req/tns1:cardNumber)}&lt;/cardNumber>
			else()
		}

		{
			if($req/tns1:cardBin)
			then &lt;cardBin xsi:type="soapenc:string">{data($req/tns1:cardBin)}&lt;/cardBin>
			else()
		}

		{
			if($req/tns1:diExtrcard)
			then &lt;diExtrcard xsi:type="soapenc:int">{data($req/tns1:diExtrcard)}&lt;/diExtrcard>
			else()
		}

		{
		if($req/tns1:cardLCycle)
			then &lt;cardLCycle xsi:type="soapenc:int">{data($req/tns1:cardLCycle)}&lt;/cardLCycle>
			else()
		}

		{
			if($req/tns1:diCard)
			then &lt;diCard xsi:type="soapenc:string">{data($req/tns1:diCard)}&lt;/diCard>
			else()
		}

		

	
	&lt;/multiRef>
};

declare function xf:transformStatementsParameters($req as element(tns1:statement)) as element(multiRef){
	&lt;multiRef id="id7" xsi:type="tns4:CekeStatementParametersBean" xmlns:tns4="urn:pbpolsoft.com.pl">
	
			{
		if($req/tns1:diExtractc)
			then &lt;diExtractc xsi:type="soapenc:int">{data($req/tns1:diExtractc)}&lt;/diExtractc>
			else()
		}
		
				{
		if($req/tns1:diExtractr)
			then &lt;diExtractr xsi:type="soapenc:int">{data($req/tns1:diExtractr)}&lt;/diExtractr>
			else()
		}
		
				{
		if($req/tns1:diExtractt)
			then &lt;diExtractt xsi:type="soapenc:int">{data($req/tns1:diExtractt)}&lt;/diExtractt>
			else()
		}
		
				{
		if($req/tns1:diExtractd)
			then &lt;diExtractd xsi:type="soapenc:int">{data($req/tns1:diExtractd)}&lt;/diExtractd>
			else()
		}
	
	&lt;/multiRef>
	
};
	
	declare variable $body as element(soap-env:Body) external;			
	xf:transformBody($body)</con:xquery>
</con:xqueryEntry>