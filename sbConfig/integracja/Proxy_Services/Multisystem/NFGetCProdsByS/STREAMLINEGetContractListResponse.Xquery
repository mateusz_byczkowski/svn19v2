<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-02</con:description>
  <con:xquery>(: Change Log
v.1.1 2010-01-15 PKLI NP1972_1 Zamiana mapowania dla pola NF_ACCOUN_ACCOUNTOPENDATE (z contract-start na proposal)
v.1.2 2010-03-05 PKLI NP2037_1 CR1 Zaprzestanie wyliczania salda ubezpieczeń JV
v.1.3 2010-05-06 PKLI NP1972_3 CR2 Filtrowanie produktów Streamline na podstawie statusu aktywności 
v.1.4 2010-05-17 PKLI T47597 Brak obsługi numeru wniosku ubezpieczeń Streamline
v.1.5 2011-02-02 PKL T52074 Błąd w obsłudze stronicowania dla PRIME, CEKE i STRL
:)

declare namespace wsdl-jv = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace cmf-holding = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";



declare function xf:calculatePageStart ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then 1
else if ($actioncode="N")
(: 1.5  then xs:integer(min(($records  , $oldstop + 1)))  :)  
           then xs:integer(min(($records + 1, $oldstop + 1))) (:1.5:)

else if ($actioncode="P")
   then max((1,$oldstart - $pagesize)) 
else if ($actioncode="L")
(: 1.5  then max((1,$records  - $pagesize)) :)
           then max((1,$records  - $pagesize + 1)) (:1.5:)

else 0
};

declare function xf:calculatePageStop ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then xs:integer(min(($pagesize,$records)))
else if ($actioncode="N")
   then xs:integer(min(($pagesize + $oldstop,$records))) 
else if ($actioncode="P")
(: 1.5  then max((1,$oldstart - 1)) :)
           then max((0,$oldstart - 1)) (:1.5:)
else if ($actioncode="L")
   then $records
else xs:integer(0)
};


declare function xf:calculateHasNext( $pagestart as xs:integer,$pagestop as xs:integer, 
                                                     $records as xs:integer, $actioncode as xs:string) as xs:integer{
(: 1.5   if ($actioncode != "P")  :)
    if ($actioncode != "P" and $actioncode != "L")  (:1.5:)
     then  if ($pagestop &lt; $records)
           then 1
           else 0
    else if ($pagestart > 1)
           then 1
           else 0 
};

declare function xf:mapProduct($prod as element(contract),$system_id as xs:string)  as element()* {
	&lt;fml:NF_ACCOUN_ACCOUNTNUMBER  nil="true" />
        ,
        &lt;NF_PRODUA_CODEPRODUCTAREA>4&lt;/NF_PRODUA_CODEPRODUCTAREA>
	, 
	(: if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
	  then &lt;fml:NF_ACCOUN_ACCOUNTNUMBER  nil="true" />
	  else &lt;fml:NF_ACCOUN_ACCOUNTNUMBER >{substring(data($prod/m:DC_NR_RACHUNKU),2)}&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER>
	, 
	if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
	  then &lt;fml:NF_DEBITC_VIRTUALCARDNBR>{data($prod/m:DC_NR_ALTERNATYWNY)}&lt;/fml:NF_DEBITC_VIRTUALCARDNBR>
	  else &lt;fml:NF_DEBITC_VIRTUALCARDNBR nil="true" />
	,
	if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
	  then &lt;fml:NF_DEBITC_CARDNBR>{substring(data($prod/m:DC_NR_RACHUNKU),2)}&lt;/fml:NF_DEBITC_CARDNBR>
	  else &lt;fml:NF_DEBITC_CARDNBR nil="true" />
	,
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
      then &lt;NF_PRODUA_CODEPRODUCTAREA>1&lt;/NF_PRODUA_CODEPRODUCTAREA>
      else &lt;NF_PRODUA_CODEPRODUCTAREA>2&lt;/NF_PRODUA_CODEPRODUCTAREA>
	, :)

      &lt;fml:NF_IS_COOWNER>N&lt;/fml:NF_IS_COOWNER>
	,
    (: if ($prod/m:CI_PRODUCT_ATT1 and string-length(data($prod/m:CI_PRODUCT_ATT1))>0)
      then &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT>{data($prod/m:CI_PRODUCT_ATT1)}&lt;/fml:NF_ATTRPG_FIRSTPRODUCTFEAT>    
      else &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT nil="true"/>
	,
   if ($prod/m:CI_PRODUCT_ATT2 and string-length(data($prod/m:CI_PRODUCT_ATT2))>0)
      then &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA>{data($prod/m:CI_PRODUCT_ATT2)}&lt;/fml:NF_ATTRPG_SECONDPRODUCTFEA>    
      else &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA nil="true"/>
	,			
    if ($prod/m:CI_PRODUCT_ATT3 and string-length(data($prod/m:CI_PRODUCT_ATT3))>0)
      then &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT>{data($prod/m:CI_PRODUCT_ATT3)}&lt;/fml:NF_ATTRPG_THIRDPRODUCTFEAT>    
      else &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/>
	,
    if ($prod/m:CI_PRODUCT_ATT4 and string-length(data($prod/m:CI_PRODUCT_ATT4))>0)
      then &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA>{data($prod/m:CI_PRODUCT_ATT4)}&lt;/fml:NF_ATTRPG_FOURTHPRODUCTFEA>    
      else &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/>
	,
    if ($prod/m:CI_PRODUCT_ATT5 and string-length(data($prod/m:CI_PRODUCT_ATT5))>0)
      then &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT>{data($prod/m:CI_PRODUCT_ATT5)}&lt;/fml:NF_ATTRPG_FIFTHPRODUCTFEAT>    
      else &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT nil="true"/>
    ,:)
     &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT>{data($prod/@product-type)}&lt;/fml:NF_ATTRPG_FIRSTPRODUCTFEAT>
     ,
    &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA nil="true"/>
     ,
    &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/>
     ,
    &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/>
     ,
    &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT>PLN&lt;/fml:NF_ATTRPG_FIFTHPRODUCTFEAT>
     ,  
    if ($prod/level)
     then &lt;fml:NF_PRODUD_SORCEPRODUCTCODE>{concat(data($prod/@product-type), data($prod/level))}&lt;/fml:NF_PRODUD_SORCEPRODUCTCODE>
     else &lt;fml:NF_PRODUD_SORCEPRODUCTCODE>{concat(data($prod/@product-type), "~")}&lt;/fml:NF_PRODUD_SORCEPRODUCTCODE>
     ,
    &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE>1&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE>
     , 			
    (:1.2   if ($prod/amount)
      then &lt;fml:NF_ACCOUN_CURRENTBALANCE>{data($prod/amount)}&lt;/fml:NF_ACCOUN_CURRENTBALANCE>
      else &lt;fml:NF_ACCOUN_CURRENTBALANCE nil="true" />
    , 1.2 :)	
     &lt;fml:NF_ACCOUN_CURRENTBALANCE nil="true" /> (:1.2 :)

    ,	
    (:if ($prod/m:B_BLOKADA)
      then &lt;fml:NF_HOLD_HOLDAMOUNT>{data($prod/m:B_BLOKADA)}&lt;/fml:NF_HOLD_HOLDAMOUNT>
      else &lt;fml:NF_HOLD_HOLDAMOUNT>0&lt;/fml:NF_HOLD_HOLDAMOUNT>
    ,			
    if ($prod/m:B_DOST_SRODKI)
      then &lt;fml:NF_ACCOUN_AVAILABLEBALANCE>{data($prod/m:B_DOST_SRODKI)}&lt;/fml:NF_ACCOUN_AVAILABLEBALANCE>
      else &lt;fml:NF_ACCOUN_AVAILABLEBALANCE nil="true" />
    ,
    if ($prod/m:B_KOD_WALUTY)
      then &lt;fml:NF_CURREC_CURRENCYCODE>{data($prod/m:B_KOD_WALUTY)}&lt;/fml:NF_CURREC_CURRENCYCODE>
      else &lt;fml:NF_CURREC_CURRENCYCODE nil="true"/>
    ,			
    if ($prod/m:B_D_OTWARCIA)
      then &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE>{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_D_OTWARCIA))}&lt;/fml:NF_ACCOUN_ACCOUNTOPENDATE>
      else &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE>0001-01-01&lt;/fml:NF_ACCOUN_ACCOUNTOPENDATE>
    ,			
    if ($prod/m:B_DATA_OST_OPER)
      then &lt;fml:NF_TRANA_DATEOFLASTACTIVIT>{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_DATA_OST_OPER))}&lt;/fml:NF_TRANA_DATEOFLASTACTIVIT>
      else &lt;fml:NF_TRANA_DATEOFLASTACTIVIT>0001-01-01&lt;/fml:NF_TRANA_DATEOFLASTACTIVIT>
    ,		
    if ($prod/m:B_LIMIT1)
      then &lt;fml:NF_TRAACA_LIMITAMOUNT>{data($prod/m:B_LIMIT1)}&lt;/fml:NF_TRAACA_LIMITAMOUNT>
      else &lt;fml:NF_TRAACA_LIMITAMOUNT nil="true" />
    ,			
    if ($prod/m:CI_PRODUCT_NAME_ORYGINAL)
      then &lt;fml:NF_ACCOUN_ACCOUNTNAME>{data($prod/m:CI_PRODUCT_NAME_ORYGINAL)}&lt;/fml:NF_ACCOUN_ACCOUNTNAME>
      else &lt;fml:NF_ACCOUN_ACCOUNTNAME nil="true" />
    ,			
    if ($prod/m:B_KOD_RACH)
      then &lt;fml:NF_PRIMAC_ACCOUNTCODE>{data($prod/m:B_KOD_RACH)}&lt;/fml:NF_PRIMAC_ACCOUNTCODE>
      else &lt;fml:NF_PRIMAC_ACCOUNTCODE nil="true" />
    ,:)
    &lt;NF_CTRL_SYSTEMID>{$system_id}&lt;/NF_CTRL_SYSTEMID>
    ,
    &lt;NF_ACCOUR_RELATIONSHIP nil="true"/>
    ,
    if ($prod[@contract-number])
    then &lt;NF_INSURA_ID>{data($prod/@contract-number)}&lt;/NF_INSURA_ID>
    else  &lt;NF_INSURA_ID nil="true"/>
    ,
    &lt;NF_DEBITC_VIRTUALCARDNBR>{concat(concat(data($prod/@product-type),":"),$prod/@contract-number)}
    &lt;/NF_DEBITC_VIRTUALCARDNBR>
    ,
(: v.1.3 start :)
    if (data($prod/status)='0' or data($prod/status)='1')
    then &lt;NF_PRODUCT_STAT>T&lt;/NF_PRODUCT_STAT>
    else  &lt;NF_PRODUCT_STAT>N&lt;/NF_PRODUCT_STAT>  (: statusy 2, 3, 5, 7, 8 i pozostałe :)
   ,
(: v.1.3 end :)
(: v.1.4 start :)
    if ($prod/proposal-number)
    then &lt;NF_POLICC_PROPOSALID>{data($prod/proposal-number)}&lt;/NF_POLICC_PROPOSALID>
    else ()
    ,
(: v.1.4 end :)
   &lt;NF_CURREC_CURRENCYCODE>PLN&lt;/NF_CURREC_CURRENCYCODE>
   ,
   if ($prod/contract-dates/proposal)
   then &lt;NF_ACCOUN_ACCOUNTOPENDATE>{data($prod/contract-dates/proposal)}&lt;/NF_ACCOUN_ACCOUNTOPENDATE>
   else &lt;NF_ACCOUN_ACCOUNTOPENDATE nil="true" />
};

declare function xf:mapSTREAMLINEGetContractListResponse ($res as element(wsdl-jv:getContractListResponse),$system_id as xs:string,
  $pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
								  $actioncode as xs:string )
as element(fml:FML32){
	let $records:=count( $res/contracts/contract)
	let $start:=xf:calculatePageStart($pagesize,$oldstart,$oldstop,$actioncode,$records)
	let $stop:=xf:calculatePageStop($pagesize,$oldstart,$oldstop,$actioncode,$records)
	let $newnavkey:=concat("012:",string($start),":",string($stop))
	let $hasnext:=xf:calculateHasNext($start,$stop,$records,$actioncode)
	return
	   &lt;fml:FML32>
                        {
                             (:1.5 start:)
                             if ($start > $records or $stop &lt;= 0) 
                             then (
		  &lt;NF_PAGEC_NAVIGATIONKEYVALU>012&lt;/NF_PAGEC_NAVIGATIONKEYVALU>,
		  &lt;NF_PAGEC_HASNEXT>0&lt;/NF_PAGEC_HASNEXT>,
		  &lt;NF_PAGECC_OPERATIONS>0&lt;/NF_PAGECC_OPERATIONS>
                                    )
                             else (     
		  &lt;NF_PAGEC_NAVIGATIONKEYVALU>{$newnavkey}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>,
		  &lt;NF_PAGEC_HASNEXT>{$hasnext}&lt;/NF_PAGEC_HASNEXT>,
		  &lt;NF_PAGECC_OPERATIONS>{max((0,$stop - $start+1))}&lt;/NF_PAGECC_OPERATIONS>,

			for $idx in ($start to $stop)
			   return
				  xf:mapProduct($res/contracts/contract[$idx], $system_id)
                                      ) (:1.5 koniec :)
		  }
	   &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $system_id as xs:string external;
declare variable $pagesize as xs:integer external;
declare variable $oldstart  as xs:integer external;
declare variable $oldstop  as xs:integer external;
declare variable $actioncode  as xs:string external;
&lt;soap-env:Body>
{xf:mapSTREAMLINEGetContractListResponse($body/wsdl-jv:getContractListResponse,$system_id,$pagesize,$oldstart,$oldstop,$actioncode)}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>