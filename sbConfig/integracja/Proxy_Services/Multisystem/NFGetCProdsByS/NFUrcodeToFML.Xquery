<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace fml="";

declare variable $body external;


  &lt;soap-env:Body>
	{
	  (: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
	  let $reason := fn:substring-before(fn:substring-after(fn:substring-before($body/ctx:reason, ":"), "("), ")")
	  let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($body/ctx:reason, ":"), ":"), ":")
         let $errorCode:= data($body/ctx:errorCode)
	  return
           &lt;FML32>       
            {
              if( string-length(data($reason)) = 0 or not($reason = "11"))
                 then &lt;NF_ERROR_CODE>{$reason}&lt;/NF_ERROR_CODE>
                 else  &lt;NF_ERROR_CODE>{$urcode}&lt;/NF_ERROR_CODE>
            }      
             {
            if ($errorCode != "BEA-380000")
              then  &lt;NF_ERROR_DESCRIPTION>Błąd usługi proxy na ALSB&lt;/NF_ERROR_DESCRIPTION>
              else
              if( string-length(data($reason)) = 0 or $reason = "12")
                 then &lt;NF_ERROR_DESCRIPTION>Krytyczny błąd usługi w systemie źródłowym&lt;/NF_ERROR_DESCRIPTION>
                 else if ($reason="13") then
                        &lt;NF_ERROR_DESCRIPTION>Przekroczenie maksymalnego czasu oczekiwania na odpowiedź&lt;/NF_ERROR_DESCRIPTION>
                 else if ($reason="6") then 
                        &lt;NF_ERROR_DESCRIPTION>Brak usługi w systemie źródłowym&lt;/NF_ERROR_DESCRIPTION>
                 else if ($reason="11") then
                        if ($urcode = "102") then
                           &lt;NF_ERROR_DESCRIPTION>Błędne dane wejściowe&lt;/NF_ERROR_DESCRIPTION>
                        else if ($urcode = "103") then
                           &lt;NF_ERROR_DESCRIPTION>Brak danych&lt;/NF_ERROR_DESCRIPTION>
                        else  
                            &lt;NF_ERROR_DESCRIPTION>Błąd usługi w systemie źródłowym&lt;/NF_ERROR_DESCRIPTION>
                  else  
                     &lt;NF_ERROR_DESCRIPTION>Nieznany krytyczny błąd usługi w systemie źródłowym&lt;/NF_ERROR_DESCRIPTION>
            }        
            &lt;/FML32>
	}
   
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>