<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapRequestForEGCustProdArea($fml as element(fml:FML32))
	as element(fml:FML32) {
		&lt;fml:FML32>
                     {if ($fml/NF_CUSTOM_CUSTOMERNUMBER)
                               then $fml//DC_NUMER_KLIENTA
                               else()
                      }
                   (: {if ($fml/DC_NR_DOWODU_REGON)
                               then $fml//DC_NR_DOWODU_REGON
                               else()
                      }
                     {if ($fml/DC_NR_PESEL)
                               then $fml//DC_NR_PESEL
                               else()
                      }:)
                     {if ($fml/CI_PRODUCT_APLICATION)
                               then $fml//CI_PRODUCT_APLICATION
                               else()
                      }
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapRequestForEGCustProdArea($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>