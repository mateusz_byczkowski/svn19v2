<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xf = "http://bzwbk.com/services/mappings/";

declare function xf:mapPartialFMLToFML($bdy as element(fml:FML32))
	as element(fml:FML32) {
	 &lt;FML32>
         {	
          for $req  at $p in $bdy/fml:PART/*
               return
                  $req
	 }               
         &lt;NF_PAGECC_OPERATIONS>{count($bdy/fml:PART)}&lt;/NF_PAGECC_OPERATIONS>
         &lt;NF_PAGEC_NAVIGATIONKEYVALU>000&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
         &lt;NF_PAGEC_HASNEXT>0&lt;/NF_PAGEC_HASNEXT>

         &lt;/FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>{ 
 if ($body/fml:FML32/PART)
    then  xf:mapPartialFMLToFML($body/fml:FML32)
    else if ($body/fml:FML32)
          then  $body/fml:FML32
          else() 
}
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>