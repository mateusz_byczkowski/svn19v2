<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;

declare function convertActionCodeValue ($parm as xs:string,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "Y")
       then $trueval
       else $falseval
};


declare function getFieldsFromInvoke($parm as element(fml:FML32)) as element()*
{
 let $blokada:=data($parm/fml:B_BLOKADA)
 let $blokada_len:=string-length($blokada)
 let $blokada_zero:=concat(substring("00000", 1+ $blokada_len),$blokada)
 let $typBlokady:=data($parm/fml:DC_TYP_BLOKADY[2])
 let $typBlokady_len:=string-length($typBlokady)
 let $typBlokady_zero:=concat(substring("00", 1+ $typBlokady_len),$typBlokady)
 let $key:=concat($blokada_zero,$typBlokady)
 let $spacje:='                                                                                             '
 return
&lt;fml:NF_PAGEC_NAVIGATIONKEYVALU>{concat($key,$spacje,$key)}&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU>
,
&lt;fml:NF_HOLD_HOLDSTAT>A&lt;/fml:NF_HOLD_HOLDSTAT>
,
&lt;fml:NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/fml:B_NR_RACH)}&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER>
,
&lt;fml:NF_ACCOUN_ACCOUNTIBAN?>{data($parm/fml:B_NR_RACH)}&lt;/fml:NF_ACCOUN_ACCOUNTIBAN>
,
&lt;fml:NF_PAGEC_ACTIONCODE>{convertActionCodeValue(data($parm/fml:B_POZYCJA),"F","N")}&lt;/fml:NF_PAGEC_ACTIONCODE>
,
&lt;fml:NF_HOLDT_HOLDTYPE?>{data($parm/fml:DC_TYP_BLOKADY[1])}&lt;/fml:NF_HOLDT_HOLDTYPE>
,
&lt;fml:NF_PAGEC_PAGESIZE?>{data($parm/fml:B_LICZBA_REK)}&lt;/fml:NF_PAGEC_PAGESIZE>
,
&lt;fml:NF_PAGEC_REVERSEORDER>0&lt;/fml:NF_PAGEC_REVERSEORDER>
};

&lt;soap:Body>
  &lt;fml:FML32>
   {getFieldsFromInvoke($body/fml:FML32)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>