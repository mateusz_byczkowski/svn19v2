<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;

declare function getFieldsFromInvoke($parm as element(fml:FML32)) as element()*

{
<fml:B_BLOKADA>{data($parm/fml:NF_HOLD_HOLDNUMBER[1])}</fml:B_BLOKADA>
,
<fml:B_KWOTA_BLOK>{data($parm/fml:NF_HOLD_HOLDAMOUNT[1])}</fml:B_KWOTA_BLOK>
,
<fml:B_DATA_OPER>{data($parm/fml:NF_HOLD_HOLDENTERDATE[1])}</fml:B_DATA_OPER>
,
<fml:B_DATA_STR>{data($parm/fml:NF_HOLD_HOLDEXPIRATIONDATE[1])}</fml:B_DATA_STR>
,
<fml:DC_TYP_BLOKADY>{data($parm/fml:NF_HOLDT_HOLDTYPE[1])}</fml:DC_TYP_BLOKADY>
,
<fml:B_POWOD1>{data($parm/fml:NF_HOLD_HOLDDESCRIPTION[1])}</fml:B_POWOD1>
,
<fml:B_POWOD2>{data($parm/fml:NF_HOLD_HOLDDESCRIPTION2[1])}</fml:B_POWOD2>
,
<fml:B_LICZBA_REK>{ data($parm/fml:NF_PAGECC_OPERATIONS)}</fml:B_LICZBA_REK>
,
<fml:B_OPIS_BLEDU>{ data($parm/fml:NF_RESPOM_ERRORDESCRIPTION)}</fml:B_OPIS_BLEDU>
};
<soap:Body>
  <fml:FML32>
   {getFieldsFromInvoke($body/fml:FML32)},
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>