<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-11-18</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace tem = "http://tempuri.org/";
declare namespace wcf="http://schemas.datacontract.org/2004/07/WcfServiceLibrary.Struktures";


declare function xf:map_manCustProdAddrResponse($res as element(tem:manageCardProductAdrResult ))
	as element(fml:FML32) {
	&lt;fml:FML32>
	{
	  	if($res/wcf:Status)
		then	&lt;fml:DC_OPIS_BLEDU>{ data($res/wcf:Status) }&lt;/fml:DC_OPIS_BLEDU>
		else()
	}	
	&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_manCustProdAddrResponse($body/tem:manageCardProductAdrResponse/tem:manageCardProductAdrResult ) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>