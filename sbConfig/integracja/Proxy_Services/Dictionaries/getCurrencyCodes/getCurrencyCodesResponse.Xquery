<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(: Change log
v.1.1 2010-03-15 PK NP1696 Dodanie pól wyjściowych
v.1.2 2010-05-11 PK T47137 Zmiana mapowania pola currencyCode
:)

declare namespace fml="";
declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function Num2Bool($dateIn as xs:string) as xs:boolean {
 if ($dateIn = "1") 
    then true()
    else false()
};

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
&lt;urn:dicts>
  {
    let $waluta := $parm/B_WALUTA
    let $retdata := $parm/B_RET_DATA 
    let $kraj := $parm/B_KRAJ (: 1.1 :)
    let $nazwa := $parm/B_NAZWA
    let $allowedCashPosting:= $parm/NF_CURREC_ALLOWEDCASHPOSTI 
    let $allowedPosting:= $parm/NF_CURREC_ALLOWEDPOSTING
    let $precisionOfCurrency:= $parm/NF_CURREC_PRECISIONOFCURRE
    let $precisionOfRate:= $parm/NF_CURREC_PRECISIONOFRATE
    let $presentationsOrder := $parm/NF_CURREC_PRESENTATIONSORD

    
    for $kodps at $occ in $parm/B_KOD_PS
    return
    &lt;urn1:CurrencyCode>
      &lt;urn1:allowedCashPosting?>{Num2Bool(data($allowedCashPosting[$occ]))}&lt;/urn1:allowedCashPosting>
      &lt;urn1:allowedPosting?>{Num2Bool(data($allowedPosting[$occ]))}&lt;/urn1:allowedPosting>
      (: 1.2 &lt;urn1:currencyCode?>{data($kodps)}&lt;/urn1:currencyCode> :)
      &lt;urn1:currencyCode?>{data($retdata[$occ])}&lt;/urn1:currencyCode> (: 1.2 :)
      &lt;urn1:description?>{data($nazwa[$occ])}&lt;/urn1:description>
      &lt;urn1:iso4217Code?>{data($waluta[$occ])}&lt;/urn1:iso4217Code>
      &lt;urn1:precisionOfCurrency?>{data($precisionOfCurrency[$occ])}&lt;/urn1:precisionOfCurrency>
      &lt;urn1:precisionOfRate?>{data($precisionOfRate[$occ])}&lt;/urn1:precisionOfRate>
      &lt;urn1:presentationsOrder?>{data($presentationsOrder[$occ])}&lt;/urn1:presentationsOrder>
      &lt;urn1:countryOfCurrency?>
        &lt;urn1:CountryCode?>
          (: &lt;urn1:countryCode?>{if(data($kodps) = "978") then "" else fn:substring(data($retdata[$occ]),1,2)}&lt;/urn1:countryCode> 1.1 :)
             &lt;urn1:countryCode?>{data($kraj[$occ])}&lt;/urn1:countryCode> (: 1.1 :)
        &lt;/urn1:CountryCode>
      &lt;/urn1:countryOfCurrency>
    &lt;/urn1:CurrencyCode>
  }
&lt;/urn:dicts>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
&lt;urn:invokeResponse>
  {getElementsForDicts($parm)}
&lt;/urn:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>