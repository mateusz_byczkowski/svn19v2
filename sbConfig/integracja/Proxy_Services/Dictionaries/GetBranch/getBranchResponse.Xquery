<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:dictionaries.be.dcl";
declare namespace urn2 = "urn:baseauxentities.be.dcl";

declare function xf:mapgetBranchResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {

		&lt;urn:invokeResponse>
			&lt;urn1:branchCodeOut>			
						{						  						
						  if (fn:count($fml/fml:B_MIKROODDZIAL) > 1) then
						  (
						
							for $it at $p in $fml/fml:B_MIKROODDZIAL
							return
							&lt;urn1:BranchCode>
									&lt;urn1:addressCity?>{ data($fml/fml:B_MIASTO[$p]) }&lt;/urn1:addressCity>
									&lt;urn1:addressStreet?>{ data($fml/fml:B_ADRES_ODDZ[$p]) }&lt;/urn1:addressStreet>
									&lt;urn1:addressZipCode?>{ data($fml/fml:B_KOD_POCZT[$p]) }&lt;/urn1:addressZipCode>
									&lt;urn1:branchCode?>{ data($fml/fml:B_ODDZIAL[$p]) }&lt;/urn1:branchCode>
									&lt;urn1:description?>{ data($fml/fml:B_NAZWA_ODDZ[$p]) }&lt;/urn1:description>
									&lt;urn1:phoneNumber?>{ data($fml/fml:B_TELEFON[$p]) }&lt;/urn1:phoneNumber>
									&lt;urn1:branchCostCenterList>
 									   &lt;urn1:BranchCostCenter>
 									     &lt;urn1:branchCostCenter>{ data($fml/fml:B_CENTR_KOSZT[$p]) }&lt;/urn1:branchCostCenter>
 									   &lt;/urn1:BranchCostCenter>
									&lt;/urn1:branchCostCenterList>
									&lt;urn1:branchType?>{ data($fml/fml:B_MIKROODDZIAL[$p]) }&lt;/urn1:branchType>
									
 							&lt;/urn1:BranchCode>
 							)
 							else
 							(
    							&lt;urn1:BranchCode>
									&lt;urn1:addressCity?>{ data($fml/fml:B_MIASTO) }&lt;/urn1:addressCity>
									&lt;urn1:addressStreet?>{ data($fml/fml:B_ADRES_ODDZ) }&lt;/urn1:addressStreet>
									&lt;urn1:addressZipCode?>{ data($fml/fml:B_KOD_POCZT) }&lt;/urn1:addressZipCode>
									&lt;urn1:branchCode?>{ data($fml/fml:B_ODDZIAL) }&lt;/urn1:branchCode>
									&lt;urn1:description?>{ data($fml/fml:B_NAZWA_ODDZ) }&lt;/urn1:description>
									&lt;urn1:phoneNumber?>{ data($fml/fml:B_TELEFON) }&lt;/urn1:phoneNumber>
									&lt;urn1:branchCostCenterList>
                  {
 							     for $it at $p in $fml/fml:B_CENTR_KOSZT
							     return
 									   &lt;urn1:BranchCostCenter>
 									     &lt;urn1:branchCostCenter>{ data($fml/fml:B_CENTR_KOSZT[$p]) }&lt;/urn1:branchCostCenter>
 									   &lt;/urn1:BranchCostCenter>
 									}
									&lt;/urn1:branchCostCenterList>
									&lt;urn1:branchType?>{ data($fml/fml:B_MIKROODDZIAL) }&lt;/urn1:branchType>
									
 							&lt;/urn1:BranchCode>
 							)
						}			
			&lt;/urn1:branchCodeOut>
		&lt;/urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body>
{ xf:mapgetBranchResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>