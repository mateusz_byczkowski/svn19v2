<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetHistoryRequest($req as element(m:GetHistoryRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:KodRach)
					then &lt;fml:B_KOD_RACH>{ data($req/m:KodRach) }&lt;/fml:B_KOD_RACH>
					else ()
			}
			{
				if($req/m:DlNrRach)
					then &lt;fml:B_DL_NR_RACH>{ data($req/m:DlNrRach) }&lt;/fml:B_DL_NR_RACH>
					else ()
			}
			{
				if($req/m:DataStr)
					then &lt;fml:B_DATA_STR>{ data($req/m:DataStr) }&lt;/fml:B_DATA_STR>
					else ()
			}
			{
				if($req/m:DataGran)
					then &lt;fml:B_DATA_GRAN>{ data($req/m:DataGran) }&lt;/fml:B_DATA_GRAN>
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:B_LICZBA_OPER>{ data($req/m:LiczbaOper) }&lt;/fml:B_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:LiczbaDni)
					then &lt;fml:B_LICZBA_DNI>{ data($req/m:LiczbaDni) }&lt;/fml:B_LICZBA_DNI>
					else ()
			}
			{
				if($req/m:Lp)
					then &lt;fml:B_LP>{ data($req/m:Lp) }&lt;/fml:B_LP>
					else ()
			}
			{
				if($req/m:Kierunek)
					then &lt;fml:B_KIERUNEK>{ data($req/m:Kierunek) }&lt;/fml:B_KIERUNEK>
					else ()
			}
			{
				if($req/m:Extra)
					then &lt;fml:B_EXTRA>{ data($req/m:Extra) }&lt;/fml:B_EXTRA>
					else ()
			}
			{
				if($req/m:Sys)
					then &lt;fml:B_SYS>{ data($req/m:Sys) }&lt;/fml:B_SYS>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetHistoryRequest($body/m:GetHistoryRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>