<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetHistoryResponse($fml as element(fml:FML32))
	as element(m:CRMGetHistoryResponse) {
		&lt;m:CRMGetHistoryResponse>
			{
				let $B_SALDO := $fml/fml:B_SALDO
				let $B_DATA_OPER := $fml/fml:B_DATA_OPER
				let $B_DATA_WALUTY := $fml/fml:B_DATA_WALUTY
				let $B_KWOTA_OPER := $fml/fml:B_KWOTA_OPER
				let $B_TYTUL_OPER := $fml/fml:B_TYTUL_OPER
				let $B_LP := $fml/fml:B_LP
				let $B_KOD_OPER := $fml/fml:B_KOD_OPER
				let $B_NR_WYCIAGU := $fml/fml:B_NR_WYCIAGU
				let $B_ODDZIAL := $fml/fml:B_ODDZIAL
				let $B_ADRES_WN := $fml/fml:B_ADRES_WN
				let $B_ADRES_MA := $fml/fml:B_ADRES_MA
				let $B_DL_TYTUL_OPER := $fml/fml:B_DL_TYTUL_OPER
				let $B_NR_RACH_WN := $fml/fml:B_NR_RACH_WN
				let $B_NR_RACH_MA := $fml/fml:B_NR_RACH_MA
				let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
				let $B_OBROT_WN := $fml/fml:B_OBROT_WN
				let $B_OBROT_MA := $fml/fml:B_OBROT_MA
				let $B_D_POCZ_HIST := $fml/fml:B_D_POCZ_HIST
				let $B_DATA_KSIEG := $fml/fml:B_DATA_KSIEG
				let $B_KWOTA_WALUTY := $fml/fml:B_KWOTA_WALUTY
				let $B_KARTA := $fml/fml:B_KARTA
				let $B_SYSTEM := $fml/fml:B_SYSTEM
				let $B_NR_PUNKTU := $fml/fml:B_NR_PUNKTU
				for $it at $p in $fml/fml:B_LP
				return
					&lt;m:Entry>
					{
						if($B_SALDO[$p])
							then &lt;m:Saldo>{ data($B_SALDO[$p]) }&lt;/m:Saldo>
						else ()
					}
					{
						if($B_DATA_OPER[$p])
							then &lt;m:DataOper>{ data($B_DATA_OPER[$p]) }&lt;/m:DataOper>
						else ()
					}
					{
						if($B_DATA_WALUTY[$p])
							then &lt;m:DataWaluty>{ data($B_DATA_WALUTY[$p]) }&lt;/m:DataWaluty>
						else ()
					}
					{
						if($B_KWOTA_OPER[$p])
							then &lt;m:KwotaOper>{ data($B_KWOTA_OPER[$p]) }&lt;/m:KwotaOper>
						else ()
					}
					{
						if($B_TYTUL_OPER[$p])
							then &lt;m:TytulOper>{ data($B_TYTUL_OPER[$p]) }&lt;/m:TytulOper>
						else ()
					}
					{
						if($B_LP[$p])
							then &lt;m:Lp>{ data($B_LP[$p]) }&lt;/m:Lp>
						else ()
					}
					{
						if($B_KOD_OPER[$p])
							then &lt;m:KodOper>{ data($B_KOD_OPER[$p]) }&lt;/m:KodOper>
						else ()
					}
					{
						if($B_NR_WYCIAGU[$p])
							then &lt;m:NrWyciagu>{ data($B_NR_WYCIAGU[$p]) }&lt;/m:NrWyciagu>
						else ()
					}
					{
						if($B_ODDZIAL[$p])
							then &lt;m:Oddzial>{ data($B_ODDZIAL[$p]) }&lt;/m:Oddzial>
						else ()
					}
					{
						if($B_ADRES_WN[$p])
							then &lt;m:AdresWn>{ data($B_ADRES_WN[$p]) }&lt;/m:AdresWn>
						else ()
					}
					{
						if($B_ADRES_MA[$p])
							then &lt;m:AdresMa>{ data($B_ADRES_MA[$p]) }&lt;/m:AdresMa>
						else ()
					}
					{
						if($B_DL_TYTUL_OPER[$p])
							then &lt;m:DlTytulOper>{ data($B_DL_TYTUL_OPER[$p]) }&lt;/m:DlTytulOper>
						else ()
					}
					{
						if($B_NR_RACH_WN[$p])
							then &lt;m:NrRachWn>{ data($B_NR_RACH_WN[$p]) }&lt;/m:NrRachWn>
						else ()
					}
					{
						if($B_NR_RACH_MA[$p])
							then &lt;m:NrRachMa>{ data($B_NR_RACH_MA[$p]) }&lt;/m:NrRachMa>
						else ()
					}
					{
						if($B_KOD_WALUTY[$p])
							then &lt;m:KodWaluty>{ data($B_KOD_WALUTY[$p]) }&lt;/m:KodWaluty>
						else ()
					}
					{
						if($B_OBROT_WN[$p])
							then &lt;m:ObrotWn>{ data($B_OBROT_WN[$p]) }&lt;/m:ObrotWn>
						else ()
					}
					{
						if($B_OBROT_MA[$p])
							then &lt;m:ObrotMa>{ data($B_OBROT_MA[$p]) }&lt;/m:ObrotMa>
						else ()
					}
					{
						if($B_D_POCZ_HIST[$p])
							then &lt;m:DPoczHist>{ data($B_D_POCZ_HIST[$p]) }&lt;/m:DPoczHist>
						else ()
					}
					{
						if($B_DATA_KSIEG[$p])
							then &lt;m:DataKsieg>{ data($B_DATA_KSIEG[$p]) }&lt;/m:DataKsieg>
						else ()
					}
					{
						if($B_KWOTA_WALUTY[$p])
							then &lt;m:KwotaWaluty>{ data($B_KWOTA_WALUTY[$p]) }&lt;/m:KwotaWaluty>
						else ()
					}
					{
						if($B_KARTA[$p])
							then &lt;m:Karta>{ data($B_KARTA[$p]) }&lt;/m:Karta>
						else ()
					}
					{
						if($B_SYSTEM[$p])
							then &lt;m:System>{ data($B_SYSTEM[$p]) }&lt;/m:System>
						else ()
					}
					{
						if($B_NR_PUNKTU[$p])
							then &lt;m:NrPunktu>{ data($B_NR_PUNKTU[$p]) }&lt;/m:NrPunktu>
						else ()
					}
					&lt;/m:Entry>
			}
		&lt;/m:CRMGetHistoryResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetHistoryResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>