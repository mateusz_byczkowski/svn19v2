<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/cis/mapping/";
declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace fml = "";

declare function xf:mapping($buf as element(fml:FML32), $req as element(fml:FML32))
    as element(customer) {
        &lt;customer>
            &lt;cif>{ data($buf/DC_NUMER_KLIENTA) }&lt;/cif>
		    &lt;period>{ data($req/CI_ZNACZNIK_OKRESU) }&lt;/period>
		    &lt;int_credit_class>{ data($buf/CI_WEW_KOD_RYZYKA) }&lt;/int_credit_class>
            &lt;profitability>
            {
	            for $kp at $pos in $buf/CI_KOD_PRODUKTU_PODST
	        	return
					&lt;prd_group code="{ $kp }" group="{ data($buf/CI_KOD_GRUPY_PRODUKTOW[$pos]) }">
						&lt;param code="INCOME_INT">{ data($buf/CI_DOCHODY_ODSETKOWE[$pos]) }&lt;/param>
						&lt;param code="INCOME_OTHER">{ data($buf/CI_DOCHODY_POZOSTALE[$pos]) }&lt;/param>
						&lt;param code="COSTS_EMPL">{ data($buf/CI_KOSZTY_BEZP_PRAC[$pos]) }&lt;/param>
						&lt;param code="COSTS_OTHER">{ data($buf/CI_KOSZTY_BEZP_POZ[$pos]) }&lt;/param>
						&lt;param code="COSTS_INDIR">{ data($buf/CI_KOSZTY_POSREDNIE[$pos]) }&lt;/param>
						&lt;param code="COSTS_RISK">{ data($buf/CI_KOSZTY_RYZYKA[$pos]) }&lt;/param>
						&lt;param code="SALDO_WN">{ data($buf/CI_SALDO_WN[$pos]) }&lt;/param>
						&lt;param code="SALDO_MA">{ data($buf/CI_SALDO_MA[$pos]) }&lt;/param>
						&lt;param code="MARGIN_WN">{ data($buf/CI_MARZA_KAPITALU_WN[$pos]) }&lt;/param>
						&lt;param code="MARGIN_MA">{ data($buf/CI_MARZA_KAPITALU_MA[$pos]) }&lt;/param>
						&lt;param code="RESULT">{ data($buf/CI_WYNIK[$pos]) }&lt;/param>
					&lt;/prd_group>
            }
            &lt;/profitability>
        &lt;/customer>
};

declare variable $buf as element(fml:FML32) external;
declare variable $req as element(fml:FML32) external;

&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	&lt;m:CRMGetProfCustResponse>{ fn-bea:serialize(xf:mapping($buf, $req)) }&lt;/m:CRMGetProfCustResponse>
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>