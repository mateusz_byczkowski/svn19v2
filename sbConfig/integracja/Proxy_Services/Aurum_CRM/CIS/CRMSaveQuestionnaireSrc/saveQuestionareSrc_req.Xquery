<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveQuestionareSrc_req/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:saveQuestionareSrc_req ($entity as element(dcl:entities.productstree.QuestSrc)) as element(FML32) {
&lt;FML32>
     &lt;CI_ID_ANKIETY>{ data( $entity/dcl:questId ) }&lt;/CI_ID_ANKIETY>
     &lt;CI_ID_SPOLKI>{ data( $entity/dcl:companyId) }&lt;/CI_ID_SPOLKI>
     &lt;CI_DATA_OD>{ data( $entity/dcl:validFrom ) }&lt;/CI_DATA_OD>
     &lt;CI_DATA_DO>{ data( $entity/dcl:validTo ) }&lt;/CI_DATA_DO>
     &lt;CI_SKP_AUTORA>{ data( $entity/dcl:usrSkpNo ) }&lt;/CI_SKP_AUTORA>
     &lt;CI_ANKIETA_XML>{ data( $entity/dcl:xmlQuestionare) }&lt;/CI_ANKIETA_XML>
&lt;/FML32>
};
declare variable $entity as element(dcl:entities.productstree.QuestSrc) external;
xf:saveQuestionareSrc_req ($entity)</con:xquery>
</con:xqueryEntry>