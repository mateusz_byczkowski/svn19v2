<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function xf:mapCRMGetProfPortfRequest($req as element(m:CRMGetProfPortfRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:ZnacznikOkresu)
					then &lt;fml:CI_ZNACZNIK_OKRESU>{ data($req/m:ZnacznikOkresu) }&lt;/fml:CI_ZNACZNIK_OKRESU>
					else ()
			}
			{
				for $Decyl in $req/m:Decyl
				return 
					&lt;fml:CI_DECYL>{ data($Decyl) }&lt;/fml:CI_DECYL>
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER>{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:NumerPaczkiStr)
					then &lt;fml:CI_NUMER_PACZKI_STR>{ data($req/m:NumerPaczkiStr) }&lt;/fml:CI_NUMER_PACZKI_STR>
					else ()
			}
			{
				if($req/m:Kierunek)
					then &lt;fml:CI_KIERUNEK>{ data($req/m:Kierunek) }&lt;/fml:CI_KIERUNEK>
					else ()
			}
			{
				if($req/m:Vip)
					then &lt;fml:CI_VIP>{ data($req/m:Vip) }&lt;/fml:CI_VIP>
					else ()
			}
			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI>{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $req as element(m:CRMGetProfPortfRequest) external;
&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
{ xf:mapCRMGetProfPortfRequest($req) }
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>