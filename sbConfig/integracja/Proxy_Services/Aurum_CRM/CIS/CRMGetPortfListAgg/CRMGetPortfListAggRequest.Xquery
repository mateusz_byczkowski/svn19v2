<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPortfListAggRequest($req as element(m:CRMGetPortfListAggRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if ($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if ($req/m:NumerOddzialu)
					then &lt;fml:CI_NR_ODDZIALU>{ data($req/m:NumerOddzialu) }&lt;/fml:CI_NR_ODDZIALU>
					else ()
			}
			{
				if ($req/m:Opcja)
					then &lt;fml:CI_OPCJA>{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetPortfListAggRequest($body/m:CRMGetPortfListAggRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>