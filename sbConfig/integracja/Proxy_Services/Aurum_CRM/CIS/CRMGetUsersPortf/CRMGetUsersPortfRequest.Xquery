<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetUsersPortfRequest($req as element(m:CRMGetUsersPortfRequest))
	as element(fml:FML32) {
		&lt;fml:FML32>
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER>{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI>{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI>
					else ()
			}
			{
				for $v in $req/m:IdsPrac
				return
					&lt;fml:CI_SKP_PRACOWNIKA>{ data($v) }&lt;/fml:CI_SKP_PRACOWNIKA>
			}
                        {
                                if ($req/m:IdWewPrac)
                                        then &lt;fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC>
                                        else ()
                        }
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCRMGetUsersPortfRequest($body/m:CRMGetUsersPortfRequest) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>