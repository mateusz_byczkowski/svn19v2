<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISGetCifResponse($fml as element(fml:FML32))
	as element(m:CISGetCifResponse) {
		&lt;m:CISGetCifResponse>
			{
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA return
					if($DC_NUMER_KLIENTA)
						then &lt;m:NumerKlienta>{ data($DC_NUMER_KLIENTA) }&lt;/m:NumerKlienta>
						else ()
			}
		&lt;/m:CISGetCifResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapCISGetCifResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>