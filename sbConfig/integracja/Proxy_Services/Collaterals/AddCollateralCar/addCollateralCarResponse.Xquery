<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

&lt;soap-env:Body>
  &lt;m:invokeResponse xmlns:urn="urn:be.services.dcl">
    &lt;m:collateral xmlns:urn1="urn:accounts.entities.be.dcl">
      {
        let $fml := $body/fml:FML32
        let $DC_ID_ZABEZPIECZENIA := $fml/fml:DC_ID_ZABEZPIECZENIA
        
        return
        &lt;urn1:Collateral>
          {if($DC_ID_ZABEZPIECZENIA and xf:isData($DC_ID_ZABEZPIECZENIA))
             then &lt;urn1:collateralItemNumber>{ data($DC_ID_ZABEZPIECZENIA) }&lt;/urn1:collateralItemNumber>
             else ()
          }
        &lt;/urn1:Collateral>
      }
    &lt;/m:collateral>
  &lt;/m:invokeResponse>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>