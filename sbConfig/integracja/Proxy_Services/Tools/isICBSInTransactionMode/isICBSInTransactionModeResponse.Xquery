<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/ceke/target/";



declare function xf:map_isICBSInTransactionModeResponse($fml as element(fml:FML32))
	as element(urn:isICBSInTransactionModeResponse) {
		&lt;urn:isICBSInTransactionModeResponse>
			{
				if(data($fml/fml:B_STAN_PS) = "0")
				then (
					&lt;urn:state>1&lt;/urn:state>
				) else if(data($fml/fml:B_STAN_PS) = "1")
				then (
					&lt;urn:state>0&lt;/urn:state>
				) else if(data($fml/fml:B_STAN_PS) = "2")
                                then (
                                        &lt;urn:state>0&lt;/urn:state>
				) else ()
			}
		&lt;/urn:isICBSInTransactionModeResponse>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:map_isICBSInTransactionModeResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>