<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:cif.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/StreamLine/UpdatePolicyAddress/RequestTransformNew/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-party-types";
declare namespace ns7 = "urn:applicationul.entities.be.dcl";
declare namespace ns5 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns6 = "urn:basedictionaries.be.dcl";
declare namespace ns9 = "urn:be.services.dcl";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns3 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns2 = "urn:baseentities.be.dcl";
declare namespace ns8 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns10 = "urn:baseauxentities.be.dcl";
declare namespace ns11 = "urn:entities.be.dcl";
declare namespace ns12 = "urn:dictionaries.be.dcl";

declare function xf:RequestTransformNew($invoke1 as element(ns9:invoke), $header1 as element(ns9:header))
    as element(ns1:changeContractAddressesRequest) {
        &lt;ns1:changeContractAddressesRequest>
            &lt;envelope>
                &lt;request-time>{ data($header1/ns9:msgHeader/ns9:timestamp) }&lt;/request-time>
                &lt;request-no?>{ data($header1/ns9:msgHeader/ns9:msgId) }&lt;/request-no>
                &lt;source-code>{ data($invoke1/ns9:sourceCode/ns10:StringHolder/ns10:value) }&lt;/source-code>
                &lt;user-id>{ data($header1/ns9:msgHeader/ns9:userId) }&lt;/user-id>
                &lt;branch-id>{ data($header1/ns9:msgHeader/ns9:unitId) }&lt;/branch-id>
                &lt;bar-code?>{ data($invoke1/ns9:barCodeID/ns10:StringHolder/ns10:value) }&lt;/bar-code>
            &lt;/envelope>
            &lt;save>{ data($invoke1/ns9:mode/ns10:BooleanHolder/ns10:value) }&lt;/save>
            &lt;product-type>{ data($invoke1/ns9:customer/ns4:Customer/ns4:policyContract/ns7:PolicyContract/ns7:productCode/ns8:UlParameters/ns8:productId) }&lt;/product-type>
            &lt;contract-number>{ data($invoke1/ns9:customer/ns4:Customer/ns4:policyContract/ns7:PolicyContract/ns7:policyID) }&lt;/contract-number>
        	{
        		let $addresses :=
        			for $address in $invoke1/ns9:customer/ns4:Customer/ns4:policyContract/ns7:PolicyContract/ns7:policyContractAddressList/ns7:PolicyContractAddress
        			where $address/ns7:addressType = '4'
        			return
	    				&lt;contract-address client-code = "{ data($invoke1/ns9:customer/ns4:Customer/ns4:customerNumber) }">
			                &lt;address kind = "{ data($address/ns7:addressType) }">
			                	&lt;street?>{ data($address/ns7:street) }&lt;/street>
			                	&lt;home?>{ data($address/ns7:house) }&lt;/home>
			                	&lt;flat?>{ data($address/ns7:flat) }&lt;/flat>
			                	&lt;postal-code?>{ data($address/ns7:zipCode) }&lt;/postal-code>
			                	&lt;city?>{ data($address/ns7:city) }&lt;/city>
			                	&lt;county?>{ data($address/ns7:state/ns12:State/ns12:state) }&lt;/county>
			                	&lt;country?>{ data($address/ns7:country/ns12:CountryCode/ns12:countryCode) }&lt;/country>
			            	&lt;/address>  
		            		&lt;person>
		            			&lt;name>
		            				&lt;first-name?>{ data($address/ns7:addressFirstName) }&lt;/first-name>
		            				&lt;last-name>{ data($address/ns7:addressName) }&lt;/last-name>
		            			&lt;/name>
		            		&lt;/person>			            	         			
						&lt;/contract-address>
        		return $addresses[1]
        	}
    &lt;/ns1:changeContractAddressesRequest>
};

declare variable $invoke1 as element(ns9:invoke) external;
declare variable $header1 as element(ns9:header) external;

xf:RequestTransformNew($invoke1, $header1)</con:xquery>
</con:xqueryEntry>