<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns9 = "urn:be.services.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Pilot/GetContract/ResponseTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns7 = "http://jv.channel.cu.com.pl/cmf-product-types";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-party-types";
declare namespace ns3 = "urn:cif.entities.be.dcl";
declare namespace ns5 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns2 = "urn:baseentities.be.dcl";
declare namespace ns6 = "urn:applicationul.entities.be.dcl";
declare namespace ns8 = "urn:uldictionary.dictionaries.be.dcl";

declare function xf:ResponseTransform($getContractResponse1 as element(ns1:getContractResponse))
    as element(ns9:invokeResponse) {
        &lt;ns9:invokeResponse>
            &lt;ns9:messageHelper>
                &lt;ns2:MessageHelper>
                    &lt;ns2:errorCode?>&lt;/ns2:errorCode>
                    &lt;ns2:errorType?>&lt;/ns2:errorType>
                    &lt;ns2:message?>&lt;/ns2:message>
                &lt;/ns2:MessageHelper>
            &lt;/ns9:messageHelper>
            &lt;ns9:customer>
                &lt;ns3:Customer>
                    &lt;ns3:customerNumber>{
                    	let $clients :=
	                        for $client in $getContractResponse1/contract/client-list/contract-person
	                        where $client/@role = '1'
	                        return
	                            data($client/@client-code)
	                    return $clients[1]
                    }&lt;/ns3:customerNumber>
                    &lt;ns3:policyContract>
                        {
                            let $contract := $getContractResponse1/contract
                            return
                                &lt;ns6:PolicyContract>
                                    &lt;ns6:proposalID>{ xs:string( data($contract/proposal-number) ) }&lt;/ns6:proposalID>
                                    &lt;ns6:policyID>{ xs:string( data($contract/@contract-number) ) }&lt;/ns6:policyID>
                                    &lt;ns6:proposalDate?>{ data($contract/contract-dates/proposal) }&lt;/ns6:proposalDate>
                                    &lt;ns6:policyDate?>{ data($contract/contract-dates/accepted) }&lt;/ns6:policyDate>
                                    &lt;ns6:cession>{ if (fn:exists(data($contract/cession))) then 1 else 0 }&lt;/ns6:cession>
                                    {
                                        for $coverages in $contract/coverages
                                        return
                                            &lt;ns6:policyCoverageList>
                                                {
                                                    for $coverage in $coverages/coverage
                                                    return
                                                        &lt;ns6:PolicyCoverage>
                                                            &lt;ns6:coverageID>{ xs:string( data($coverage/@coverage-number) ) }&lt;/ns6:coverageID>
                                                            &lt;ns6:owuSig>{ xs:string( data($coverage/owu/signature) ) }&lt;/ns6:owuSig>
                                                            &lt;ns6:regulationSig>{ xs:string( data($coverage/regulation-sig) ) }&lt;/ns6:regulationSig>
                                                            &lt;ns6:parametrsCardSig>{ xs:string( data($coverage/param-card-sig) ) }&lt;/ns6:parametrsCardSig>
                                                            &lt;ns6:coverageStartDate?>{ data($coverage/dates/contract-start) }&lt;/ns6:coverageStartDate>
                                                            &lt;ns6:premiumAmount>{ xs:double( data($coverage/amount) ) }&lt;/ns6:premiumAmount>
                                                            &lt;ns6:policyCoverageBenefitedList>
                                                            {
                                                                for $beneficiary in $coverage/beneficiaries/beneficiary
                                                                return
                                                                    &lt;ns6:PolicyCoverageBenefited>
                                                                        &lt;ns6:privatePerson>{ xs:string( data($beneficiary/company-ind) ) }&lt;/ns6:privatePerson>
                                                                        &lt;ns6:firstName?>{ xs:string( data($beneficiary/name/first-name) ) }&lt;/ns6:firstName>
                                                                        &lt;ns6:name?>{ xs:string( data($beneficiary/name/last-name) ) }&lt;/ns6:name>
                                                                        &lt;ns6:pesel?>{ data($beneficiary/identifiers/pesel) }&lt;/ns6:pesel>
                                                                        &lt;ns6:regon?>{ data($beneficiary/identifiers/regon) }&lt;/ns6:regon>
                                                                        &lt;ns6:birthDate?>{ if(fn:contains($beneficiary/dob, '+')) then fn:substring-before(data($beneficiary/dob), '+') else data($beneficiary/dob) }&lt;/ns6:birthDate>
                                                                        &lt;ns6:percent>{ xs:double( data($beneficiary/split) ) }&lt;/ns6:percent>
                                                                    &lt;/ns6:PolicyCoverageBenefited>
                                                            }
                                                            &lt;/ns6:policyCoverageBenefitedList>
                                                            &lt;ns6:policyCoverageFundSplitList>
                                                            {
                                                                for $fund in $coverage/funds/contract-fund[1]/funds/fund
                                                                return
                                                                    &lt;ns6:PolicyCoverageFundSplit>
                                                                        &lt;ns6:splitPercentage>{ data($fund/split) }&lt;/ns6:splitPercentage>
                                                                        &lt;ns6:fundID>
                                                                        	&lt;ns8:UlFund>
                                                                        		&lt;ns8:fundID>{ xs:string( data($fund/@fund-name) ) }&lt;/ns8:fundID>
                                                                        	&lt;/ns8:UlFund>
                                                                        &lt;/ns6:fundID>
                                                                    &lt;/ns6:PolicyCoverageFundSplit>
                                                            }
                                                            &lt;/ns6:policyCoverageFundSplitList>
                                                            &lt;ns6:policyCoverageFundList>
                                                            {
                                                                for $fund-value in $coverage/values/fund-value
                                                                return
                                                                    &lt;ns6:PolicyCoverageFund>
                                                                        &lt;ns6:fundUnit>{ xs:double( data($fund-value/quantity) ) }&lt;/ns6:fundUnit>
                                                                        &lt;ns6:fundID>
	                                                                        &lt;ns8:UlFund>
	                                                                        		&lt;ns8:fundID>{ xs:string( data($fund-value/@fund-name) ) }&lt;/ns8:fundID>
                                                                        	&lt;/ns8:UlFund>
	                                                                    &lt;/ns6:fundID>
                                                                    &lt;/ns6:PolicyCoverageFund>
                                                            }
                                                            &lt;/ns6:policyCoverageFundList>
                                                            &lt;ns6:coverageCategory>
                                                                &lt;ns8:UlCoverageCategory>
                                                                    &lt;ns8:ulCoverageCategory>{ xs:string( data($coverage/@coverage-category) ) }&lt;/ns8:ulCoverageCategory>
                                                                &lt;/ns8:UlCoverageCategory>
                                                            &lt;/ns6:coverageCategory>
                                                            &lt;ns6:coverageStatus>
                                                                &lt;ns8:UlContractStatus>
                                                                    &lt;ns8:ulContractStatus>{ xs:string( data($coverage/status) ) }&lt;/ns8:ulContractStatus>
                                                                &lt;/ns8:UlContractStatus>
                                                            &lt;/ns6:coverageStatus>
                                                        &lt;/ns6:PolicyCoverage>
                                                }
                                            &lt;/ns6:policyCoverageList>
                                    }
                                    &lt;ns6:policyContractAddressList>
                                    {
                                    	let $persons :=
                                    		for $p in $contract/client-list/contract-person
                                    		where $p/@role = '1'
                                    		return
                                    			let $addresses := 
                                    				for $address in $p/address
                                    				where $address/@kind = '4'
                                    				return
	                                        			&lt;ns6:PolicyContractAddress>
		                                                    &lt;ns6:addressType>{ data($address/@kind) }&lt;/ns6:addressType>
		                                                    &lt;ns6:addressName>{ data($address/company-name) }&lt;/ns6:addressName>
                                                            &lt;ns6:street>{ xs:string( data($address/street) ) }&lt;/ns6:street>
                                                            &lt;ns6:house>{ xs:string( data($address/home) ) }&lt;/ns6:house>
                                                            &lt;ns6:flat>{ xs:string( data($address/flat) ) }&lt;/ns6:flat>
                                                            &lt;ns6:city>{ xs:string( data($address/city) ) }&lt;/ns6:city>
                                                            &lt;ns6:zipCode>{ xs:string( data($address/postal-code) ) }&lt;/ns6:zipCode>
		                                                    &lt;ns6:state>
		                                                        &lt;ns4:State>
                                                                    &lt;ns4:state>{ xs:string( data($address/county) ) }&lt;/ns4:state>
		                                                        &lt;/ns4:State>
		                                                    &lt;/ns6:state>
		                                                    &lt;ns6:country>
		                                                        &lt;ns4:CountryCode>
                                                                    &lt;ns4:countryCode>{ xs:string( data($address/country) ) }&lt;/ns4:countryCode>
		                                                        &lt;/ns4:CountryCode>
		                                                    &lt;/ns6:country>
	                                        			&lt;/ns6:PolicyContractAddress>
	                                        	return $addresses[1]
                                    	return $persons[1]
                                    }
                                    &lt;/ns6:policyContractAddressList>
                                    &lt;ns6:policyContractAccountList>
                                    {
                                        for $bank in $contract/banks/bank
                                        return
                                            &lt;ns6:PolicyContractAccount>
                                                &lt;ns6:accountNo>{ xs:string( data($bank/account) ) }&lt;/ns6:accountNo>
                                                &lt;ns6:firstName>{ xs:string( data($bank/recipient-name/first-name) ) }&lt;/ns6:firstName>
                                                &lt;ns6:name>{ xs:string( data($bank/recipient-name/last-name) ) }&lt;/ns6:name>
                                                &lt;ns6:street>{ xs:string( data($bank/recipient-address/street) ) }&lt;/ns6:street>
                                                &lt;ns6:house>{ xs:string( data($bank/recipient-address/home) ) }&lt;/ns6:house>
                                                &lt;ns6:flat>{ xs:string( data($bank/recipient-address/flat) ) }&lt;/ns6:flat>
                                                &lt;ns6:zipCode>{ data($bank/recipient-address/postal-code) }&lt;/ns6:zipCode>
                                                &lt;ns6:city>{ xs:string( data($bank/recipient-address/city) ) }&lt;/ns6:city>
                                                &lt;ns6:accountType>
                                                	&lt;ns8:UlAccountType>
                                                		&lt;ns8:ulAccountType>{ data($bank/@kind) }&lt;/ns8:ulAccountType>
                                                	&lt;/ns8:UlAccountType>
                                                &lt;/ns6:accountType>
                                                &lt;ns6:country>
                                                    &lt;ns4:CountryCode>
                                                        &lt;ns4:countryCode>{ xs:string( data($bank/recipient-address/country) ) }&lt;/ns4:countryCode>
                                                    &lt;/ns4:CountryCode>
                                                &lt;/ns6:country>
                                            &lt;/ns6:PolicyContractAccount>
                                    }
                                    &lt;/ns6:policyContractAccountList>
                                    &lt;ns6:productCode>
                                        &lt;ns8:UlParameters>
                                            {
                                                for $product-type in $contract/@product-type
                                                return
                                                    &lt;ns8:productId>{ xs:string( data($product-type) ) }&lt;/ns8:productId>
                                            }
                                        &lt;/ns8:UlParameters>
                                    &lt;/ns6:productCode>
                                    &lt;ns6:policyStatus>
                                        &lt;ns8:UlContractStatus>
                                            &lt;ns8:ulContractStatus>{ xs:string( data($contract/status) ) }&lt;/ns8:ulContractStatus>
                                        &lt;/ns8:UlContractStatus>
                                    &lt;/ns6:policyStatus>
                                &lt;/ns6:PolicyContract>
                        }
                    &lt;/ns3:policyContract>
                &lt;/ns3:Customer>
            &lt;/ns9:customer>
        &lt;/ns9:invokeResponse>
};

declare variable $getContractResponse1 as element(ns1:getContractResponse) external;

xf:ResponseTransform($getContractResponse1)</con:xquery>
</con:xqueryEntry>