<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/UpdateAllocationSplit/ResponseTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns1 = "urn:baseentities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace cl = "http://sygnity.pl/functions";

declare function xf:ResponseTransform($changeContractCoverageFundsResponse1 as element(ns0:changeContractCoverageFundsResponse), $codes as element(cl:codes))
    as element(ns2:invokeResponse) {
        &lt;ns2:invokeResponse>
            &lt;ns2:messageHelper>
                {
                    for $validation in $changeContractCoverageFundsResponse1/validations/validation
                    return
                        &lt;ns1:MessageHelper>
                            &lt;ns1:errorCode>{ 
								let $allCodes :=
									for $code in $codes/cl:code
									where $code/@any-code = $validation/code
									return data($code/@nfe-code)
								return
									if($allCodes[1] != '') then
										$allCodes[1]
									else
										'K00385'
                            }&lt;/ns1:errorCode>
                            &lt;ns1:errorType>{ data($validation/severity) }&lt;/ns1:errorType>
                        &lt;/ns1:MessageHelper>
                }
            &lt;/ns2:messageHelper>
        &lt;/ns2:invokeResponse>
};

declare variable $changeContractCoverageFundsResponse1 as element(ns0:changeContractCoverageFundsResponse) external;
declare variable $codes as element(cl:codes) external;

xf:ResponseTransform($changeContractCoverageFundsResponse1, $codes)</con:xquery>
</con:xqueryEntry>