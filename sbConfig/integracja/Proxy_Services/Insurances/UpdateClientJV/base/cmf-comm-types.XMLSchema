<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:schema><![CDATA[<?xml version="1.0" encoding="UTF-8"?>
<!--
        Podstawowe typy do komunikacji.
        Version: 1.002
-->
<xs:schema targetNamespace="http://jv.adapter.cu.com.pl/cmf-comm-types" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:cmf-tech="http://jv.adapter.cu.com.pl/cmf-technical-types" xmlns:cmf-biz="http://jv.adapter.cu.com.pl/cmf-biz-types" xmlns:cmf-product="http://jv.adapter.cu.com.pl/cmf-product-types" xmlns:cmf-party="http://jv.adapter.cu.com.pl/cmf-party-types" xmlns:cmf-comm="http://jv.adapter.cu.com.pl/cmf-comm-types">
	<xs:import namespace="http://jv.adapter.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd"/>
	<xs:import namespace="http://jv.adapter.cu.com.pl/cmf-product-types" schemaLocation="cmf-product-types.xsd"/>
	<xs:import namespace="http://jv.adapter.cu.com.pl/cmf-party-types" schemaLocation="cmf-party-types.xsd"/>
	<xs:import namespace="http://jv.adapter.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd"/>
	<xs:annotation>
		<xs:documentation>Podstawowe typy do komunikacji.

            CVS log: 
            $Revision: 1.13 $ 
            $Source: /it/krwnuk/cvs_do_migracji/SIS/ESB/CMF/base/cmf-comm-types.xsd,v $ 
            $Author: kadamowi $
            $Date: 2008-08-08 15:17:50 $</xs:documentation>
	</xs:annotation>
	<xs:complexType name="FaxCoverPageType">
		<xs:sequence>
			<xs:element name="name" type="cmf-tech:CmfString"/>
			<xs:element name="note" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="subject" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="sender" type="cmf-party:PersonalDataType" minOccurs="0"/>
			<xs:element name="recipient" type="cmf-party:PersonalDataType" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="send" type="xs:boolean"/>
	</xs:complexType>
	<xs:complexType name="FaxType">
		<xs:sequence>
			<xs:element name="faxnumber" type="cmf-biz:PhoneNumberType"/>
			<xs:element name="coverpage" type="cmf-comm:FaxCoverPageType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="EmailAttachmentType">
		<xs:sequence>
			<xs:element name="filename" type="cmf-tech:CmfString"/>
			<xs:element name="uri" type="xs:anyURI" minOccurs="0"/>
			<xs:element name="attachment-data" type="xs:base64Binary" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="EmailAttachmentListType">
		<xs:sequence>
			<xs:element name="attachment" type="cmf-comm:EmailAttachmentType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="EmailListType">
		<xs:sequence>
			<xs:element name="email" type="cmf-biz:EmailType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="EmailMessageType">
		<xs:sequence>
			<xs:element name="priority" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="from" type="cmf-comm:EmailListType" minOccurs="0"/>
			<xs:element name="to" type="cmf-comm:EmailListType"/>
			<xs:element name="cc" type="cmf-comm:EmailListType" minOccurs="0"/>
			<xs:element name="bcc" type="cmf-comm:EmailListType" minOccurs="0"/>
			<xs:element name="subject" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="body" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="attachments" type="cmf-comm:EmailAttachmentListType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="PrinterType">
		<xs:sequence>
			<xs:element name="printername" type="cmf-tech:CmfString" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="DeliveryType">
		<xs:sequence>
			<xs:element name="fax" type="cmf-comm:FaxType" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="email" type="cmf-comm:EmailMessageType" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="printer" type="cmf-comm:PrinterType" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="content-uri" type="cmf-tech:CmfString" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="post" type="cmf-comm:PostType" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="courier" type="cmf-comm:CourierType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="SMSTargetType">
		<xs:sequence>
			<xs:element name="mobile-phone" type="cmf-biz:PhoneNumberType" minOccurs="0"/>
			<xs:element name="agent-id" type="cmf-biz:AgentIdType" minOccurs="0"/>
			<xs:element name="client-code" type="cmf-biz:ClientCodeType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="SMSType">
		<xs:sequence>
			<xs:element name="sms-target" type="cmf-comm:SMSTargetType"/>
			<xs:element name="sms-type" type="cmf-biz:SMSTypeNameType"/>
			<xs:element name="sms-text" type="cmf-biz:SMSTextType"/>
			<xs:element name="user-id" type="cmf-biz:UserIdType" minOccurs="0"/>
			<xs:element name="sms-id" type="cmf-biz:SMSIdType" minOccurs="0"/>
			<xs:element name="delivery-notification" type="xs:boolean" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="SMSStatusInfoType">
		<xs:sequence>
			<xs:element name="send-date" type="xs:date"/>
			<xs:element name="originator" type="cmf-tech:CmfString"/>
			<xs:element name="destination" type="cmf-biz:PhoneNumberType"/>
			<xs:element name="status" type="cmf-biz:SMSStatusType"/>
			<xs:element name="status-change" type="xs:date"/>
		</xs:sequence>
		<xs:attribute name="sms-id" type="cmf-biz:SMSIdType"/>
	</xs:complexType>
	<xs:simpleType name="ShipmentType">
		<xs:annotation>
			<xs:documentation>Type of letter shipment</xs:documentation>
		</xs:annotation>
		<xs:restriction base="cmf-tech:CmfString">
			<xs:enumeration value="STANDARD"/>
			<xs:enumeration value="PRIORITY"/>
			<xs:enumeration value="REGISTERED"/>
			<xs:enumeration value="PAYMENT"/>
			<xs:enumeration value="CONFIRMATION"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="PostType">
		<xs:annotation>
			<xs:documentation>Traditional letter type</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="shipment" type="cmf-comm:ShipmentType"/>
			<xs:element name="receiver" type="cmf-party:PersonalDataType" minOccurs="0"/>
			<xs:element name="return-envelope" type="xs:boolean" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="CourierType">
		<xs:annotation>
			<xs:documentation>
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="shipment" type="cmf-comm:ShipmentType"/>
			<xs:element name="receiver" type="cmf-party:PersonalDataType" minOccurs="0"/>
			<xs:element name="return-envelope" type="xs:boolean" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>]]></con:schema>
  <con:dependencies>
    <con:import namespace="http://jv.adapter.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd" ref="Proxy Services/Insurances/UpdateClientJV/base/cmf-technical-types"/>
    <con:import namespace="http://jv.adapter.cu.com.pl/cmf-product-types" schemaLocation="cmf-product-types.xsd" ref="Proxy Services/Insurances/UpdateClientJV/base/cmf-product-types"/>
    <con:import namespace="http://jv.adapter.cu.com.pl/cmf-party-types" schemaLocation="cmf-party-types.xsd" ref="Proxy Services/Insurances/UpdateClientJV/base/cmf-party-types"/>
    <con:import namespace="http://jv.adapter.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd" ref="Proxy Services/Insurances/UpdateClientJV/base/cmf-biz-types"/>
  </con:dependencies>
  <con:targetNamespace>http://jv.adapter.cu.com.pl/cmf-comm-types</con:targetNamespace>
</con:schemaEntry>