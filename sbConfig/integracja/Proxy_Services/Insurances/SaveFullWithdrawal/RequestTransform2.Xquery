<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns9 = "urn:dictionaries.be.dcl";
declare namespace ns4 = "urn:applicationul.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Us�ugi/StreamLine/SaveFullWithdrawal/RequestTransform/";
declare namespace ns0 = "urn:baseentities.be.dcl";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns7 = "urn:baseauxentities.be.dcl";
declare namespace ns3 = "urn:basedictionaries.be.dcl";
declare namespace ns2 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns5 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns8 = "urn:entities.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns6:invoke),
    $header1 as element(ns6:header))
    as element(ns-1:performContractSurrenderRequest) {
        &lt;ns-1:performContractSurrenderRequest>
            &lt;envelope>
                &lt;request-time>{ data($header1/ns6:msgHeader/ns6:timestamp) }&lt;/request-time>
                &lt;request-no>{ data($header1/ns6:msgHeader/ns6:msgId) }&lt;/request-no>
                &lt;source-code>{ data($invoke1/ns6:sourceCode/ns7:StringHolder/ns7:value) }&lt;/source-code>
                &lt;user-id>{ data($header1/ns6:msgHeader/ns6:userId) }&lt;/user-id>
                &lt;branch-id>{ data($header1/ns6:msgHeader/ns6:unitId) }&lt;/branch-id>
                &lt;bar-code>{ data($invoke1/ns6:barCodeID/ns7:StringHolder/ns7:value) }&lt;/bar-code>
            &lt;/envelope>
            &lt;save>{ data($invoke1/ns6:mode/ns7:BooleanHolder/ns7:value) }&lt;/save>
            &lt;product-type>{ data($invoke1/ns6:policyContract/ns4:PolicyContract/ns4:productCode/ns5:UlParameters/ns5:productId) }&lt;/product-type>
            &lt;contract-number>{ data($invoke1/ns6:policyContract/ns4:PolicyContract/ns4:policyID) }&lt;/contract-number>
            {
                for $value in $invoke1/ns6:operationDate/ns7:DateHolder/ns7:value
                return
                    &lt;effective>{ xs:date( fn:substring-before( fn:concat(data($value), 'T'), 'T' ) ) }&lt;/effective>
            }
            {
                for $opResignCause in $invoke1/ns6:policyContract/ns4:PolicyContract/ns4:resignCause/ns5:OpResignCause/ns5:strlKey
                return
                    &lt;status-reason>{ xs:int( data($opResignCause) ) }&lt;/status-reason>
            }
            {
                let $PolicyContract := $invoke1/ns6:policyContract/ns4:PolicyContract/ns4:policyOrder/ns4:PolicyOrder
                return
                    &lt;bank? kind?="{ data($PolicyContract/ns4:accountType/ns5:UlAccountType/ns5:ulAccountType) }">
                        &lt;account?>{ xs:string(data($PolicyContract/ns4:accountNoWithdrawal)) }&lt;/account>
                        &lt;recipient-name?>
                            &lt;first-name?>{ data($PolicyContract/ns4:withdrawalFirstName) }&lt;/first-name>
                            &lt;last-name?>{ data($PolicyContract/ns4:withdrawalName) }&lt;/last-name>
                        &lt;/recipient-name>
                        &lt;recipient-address? kind?="{ data($invoke1/ns6:addressType/ns7:StringHolder/ns7:value) }">
                            &lt;street?>{ data($PolicyContract/ns4:withdrawalStreet) }&lt;/street>
                            &lt;home?>{ data($PolicyContract/ns4:withdrawalHouse) }&lt;/home>
                            &lt;flat?>{ data($PolicyContract/ns4:withdrawalFlat) }&lt;/flat>
                            &lt;postal-code?>{ data($PolicyContract/ns4:withdrawalZipCode) }&lt;/postal-code>
                            &lt;city?>{ data($PolicyContract/ns4:withdrawalCity) }&lt;/city>
                            &lt;country?>{ data($PolicyContract/ns4:withdrawalCountry/ns9:CountryCode/ns9:countryCode) }&lt;/country>
                        &lt;/recipient-address>
                    &lt;/bank>
            }
        &lt;/ns-1:performContractSurrenderRequest>
};

declare variable $invoke1 as element(ns6:invoke) external;
declare variable $header1 as element(ns6:header) external;

xf:RequestTransform($invoke1,
    $header1)</con:xquery>
</con:xqueryEntry>