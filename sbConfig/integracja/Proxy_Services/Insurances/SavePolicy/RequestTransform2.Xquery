<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/SaveULPolicy/RequestTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf-party-types";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns7 = "http://jv.channel.cu.com.pl/cmf-product-types";
declare namespace ns12 = "urn:be.services.dcl";
declare namespace ns5 = "urn:basedictionaries.be.dcl";
declare namespace ns6 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns11 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns9 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "urn:baseentities.be.dcl";
declare namespace ns3 = "urn:cif.entities.be.dcl";
declare namespace ns2 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns10 = "urn:applicationul.entities.be.dcl";
declare namespace ns8 = "urn:baseauxentities.be.dcl";
declare namespace ns13 = "urn:entities.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns12:invoke), $header1 as element(ns12:header))
    as element(ns9:addContractRequest) {
        &lt;ns9:addContractRequest>
            &lt;envelope>
                &lt;request-time>{ data($header1/ns12:msgHeader/ns12:timestamp) }&lt;/request-time>
                &lt;request-no>{ data($header1/ns12:msgHeader/ns12:msgId) }&lt;/request-no>
                &lt;source-code>{ data($invoke1/ns12:sourceCode/ns8:StringHolder/ns8:value) }&lt;/source-code>
                &lt;user-id>{ data($header1/ns12:msgHeader/ns12:userId) }&lt;/user-id>
                &lt;branch-id>{ data($header1/ns12:msgHeader/ns12:unitId) }&lt;/branch-id>
                &lt;bar-code?>{ data($invoke1/ns12:barCodeID/ns8:StringHolder/ns8:value) }&lt;/bar-code>
            &lt;/envelope>
            &lt;save>{ data($invoke1/ns12:mode/ns8:BooleanHolder/ns8:value) }&lt;/save>
            {
                let $contract := $invoke1/ns12:customer/ns3:Customer/ns3:policyContract/ns10:PolicyContract
                return
		            &lt;contract product-type? = "{ data($invoke1/ns12:customer/ns3:Customer/ns3:policyContract/ns10:PolicyContract/ns10:productCode/ns11:UlParameters/ns11:productId) }">
		                &lt;proposal-number?>{ data($contract/ns10:proposalID) }&lt;/proposal-number>
		                &lt;contract-dates?>
		                	&lt;contract-start?>{ data($contract/ns10:policyStartDate) }&lt;/contract-start>
		                	&lt;contract-end?>{ data($contract/ns10:policyEndDate) }&lt;/contract-end>
		                    &lt;proposal?>{ data($contract/ns10:proposalDate) }&lt;/proposal>
		                    &lt;accepted?>{ data($contract/ns10:policyDate) }&lt;/accepted>
		                &lt;/contract-dates>
		                &lt;client-list?>
                        {
                            let $addresses := 
                            	for $address in $contract/ns10:policyContractAddressList/ns10:PolicyContractAddress
	                            return
				                    &lt;contract-person client-code = "{ data($invoke1/ns12:customer/ns3:Customer/ns3:customerNumber) }"
				                                     role = "{ data($invoke1/ns12:customerRole/ns8:StringHolder/ns8:value) }">
		                                &lt;address kind = "{ $address/ns10:addressType }">
		                                    &lt;street?>{ data($address/ns10:street) }&lt;/street>
		                                    &lt;home?>{ data($address/ns10:house) }&lt;/home>
		                                    &lt;flat?>{ data($address/ns10:flat) }&lt;/flat>
		                                    &lt;postal-code?>{ data($address/ns10:zipCode) }&lt;/postal-code>
		                                    &lt;city?>{ data($address/ns10:city) }&lt;/city>
		                                    &lt;county?>{ data($address/ns10:state/ns4:State/ns4:state) }&lt;/county>
		                                    &lt;country?>{ data($address/ns10:country/ns4:CountryCode/ns4:countryCode) }&lt;/country>
		                                &lt;/address>
		                                &lt;person>
		                                	&lt;name>
		                                		&lt;last-name>{ data($address/ns10:addressName) }&lt;/last-name>
		                                	&lt;/name>
		                                &lt;/person>
				                    &lt;/contract-person>
	                        return $addresses[1]
                        }
		                &lt;/client-list>
		                &lt;amount>{ data($contract/ns10:insurancePremium) }&lt;/amount>
		                &lt;status>0&lt;/status>
		                &lt;banks?>
	                    {
	                        for $bank in $contract/ns10:policyContractAccountList/ns10:PolicyContractAccount
	                        return
	                            &lt;bank kind = "{ data($invoke1/ns12:accountType/ns8:StringHolder/ns8:value) }">
	                                &lt;account>{ data($bank/ns10:accountNo) }&lt;/account>
	                                &lt;recipient-name?>
	                                    &lt;first-name?>{ data($bank/ns10:firstName) }&lt;/first-name>
	                                    &lt;last-name?>{ data($bank/ns10:name) }&lt;/last-name>
	                                &lt;/recipient-name>
	                                &lt;recipient-address? kind?="{ data($invoke1/ns12:addressType/ns8:StringHolder/ns8:value) }">
	                                	&lt;street?>{ data($bank/ns10:street) }&lt;/street>
	                                	&lt;home?>{ data($bank/ns10:house) }&lt;/home>
	                                	&lt;flat?>{ data($bank/ns10:flat) }&lt;/flat>
	                                	&lt;postal-code?>{ data($bank/ns10:zipCode) }&lt;/postal-code>
	                                	&lt;city?>{ data($bank/ns10:city) }&lt;/city>
	                                	&lt;country?>{ data($bank/ns10:country/ns4:CountryCode/ns4:countryCode) }&lt;/country>
	                                &lt;/recipient-address>
	                                &lt;standing-order-no?>{ data($contract/ns10:transferOrderNumber) }&lt;/standing-order-no>
	                            &lt;/bank>
	                    }
		                &lt;/banks>
		                &lt;coverages>
		                {
		                	for $coverage in $contract/ns10:policyCoverageList/ns10:PolicyCoverage
		                	return 
			                    &lt;coverage client-code = "{ data($invoke1/ns12:customer/ns3:Customer/ns3:customerNumber) }"
			                              coverage-category = "{ data($coverage/ns10:coverageCategory/ns11:UlCoverageCategory/ns11:ulCoverageCategory) }"
			                              product-type? = "{ data($invoke1/ns12:customer/ns3:Customer/ns3:policyContract/ns10:PolicyContract/ns10:productCode/ns11:UlParameters/ns11:productId) }">
			                        &lt;level?>{ data($coverage/ns10:variant/ns11:OpCoverageVariant/ns11:opCoverageVariant) }&lt;/level>
			                        &lt;owu?>
			                            &lt;signature?>{ data($coverage/ns10:owuSig) }&lt;/signature>
			                        &lt;/owu>
			                        &lt;param-card-sig?>{ data($coverage/ns10:parametrsCardSig) }&lt;/param-card-sig>
			                        &lt;regulation-sig?>{ data($coverage/ns10:regulationSig) }&lt;/regulation-sig>
			                        &lt;dates?>
			                        	&lt;proposal?>{ data($contract/ns10:proposalDate) }&lt;/proposal>
			                        &lt;/dates>
			                        &lt;status>0&lt;/status>
			                        &lt;funds?>
			                            &lt;contract-fund?>
			                            	&lt;funds?>
			                                {
			                                    for $fund in $coverage/ns10:policyCoverageFundSplitList/ns10:PolicyCoverageFundSplit
			                                    return
                                                    &lt;fund fund-name? = "{ data($fund/ns10:fundID/ns11:UlFund/ns11:fundID) }">
                                                        &lt;split>{ data($fund/ns10:splitPercentage) }&lt;/split>
                                                    &lt;/fund>
			                                }
		                                	&lt;/funds>
			                            &lt;/contract-fund>
			                        &lt;/funds>
	                                &lt;beneficiaries?>
                                    {
                                        for $benefied in $coverage/ns10:policyCoverageBenefitedList/ns10:PolicyCoverageBenefited
                                        return
                                            &lt;beneficiary>
                                                &lt;name>
                                                    &lt;first-name?>{ data($benefied/ns10:firstName) }&lt;/first-name>
                                                    &lt;last-name>{ data($benefied/ns10:name) }&lt;/last-name>
                                                &lt;/name>
                                                &lt;company-ind?>{ data($benefied/ns10:privatePerson) }&lt;/company-ind>
                                                &lt;dob?>{ data($benefied/ns10:birthDate) }&lt;/dob>
                                                &lt;identifiers>
                                                    &lt;pesel?>{ data($benefied/ns10:pesel) }&lt;/pesel>
                                                    &lt;regon?>{ data($benefied/ns10:regon) }&lt;/regon>
                                                &lt;/identifiers>
                                                &lt;split>{ xs:float( data($benefied/ns10:percent) ) }&lt;/split>
                                            &lt;/beneficiary>
                                    }
	                                &lt;/beneficiaries>
			                    &lt;/coverage>
			            }
		                &lt;/coverages>
		                &lt;sales-channel?>{ data($invoke1/ns12:sourceCode/ns8:StringHolder/ns8:value) }&lt;/sales-channel>
		            &lt;/contract>
            }
        &lt;/ns9:addContractRequest>
};

declare variable $invoke1 as element(ns12:invoke) external;
declare variable $header1 as element(ns12:header) external;

xf:RequestTransform($invoke1, $header1)</con:xquery>
</con:xqueryEntry>