<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace dec="http://bzwbk.com/examinationBZWBK/decision/";

declare variable $body as element(soap-env:Body) external;

<soap-env:Body>
{
    let $req := $body/dec:DecisionRequest
    return

    <RECORDSET>
      <RECORD>
        <REFERRALNO?>{data($req/REFERRALNO)}</REFERRALNO>
        <DECISIONDATE?>{data($req/DECISIONDATE)}</DECISIONDATE>
        <DECISIONSTATUS?>{data($req/DECISIONSTATUS)}</DECISIONSTATUS>
      </RECORD>
    </RECORDSET>
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>