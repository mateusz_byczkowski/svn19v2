<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="urn:insurance.entities.be.dcl";
declare namespace ns2="urn:insurancedict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:insurancePackageAccList>
    {for $x at $occ in $parm/NF_INSPAC_INSURANCEDESCRIP
      return
        &lt;ns1:InsurancePolicyAcc>
          &lt;ns1:policyRefNum?>{data($parm/NF_INSPAC_POLICYREFNUM[$occ])}&lt;/ns1:policyRefNum>
          &lt;ns1:insuranceDescription?>{data($parm/NF_INSPAC_INSURANCEDESCRIP[$occ])}&lt;/ns1:insuranceDescription>
          &lt;ns1:status?>
            &lt;ns2:InsurancePolicyStatus>
              &lt;ns2:insurancePolicyStatus>{data($parm/NF_INSPST_INSURANCEPOLICYS[$occ])}&lt;/ns2:insurancePolicyStatus>
            &lt;/ns2:InsurancePolicyStatus>
          &lt;/ns1:status>
        &lt;/ns1:InsurancePolicyAcc>
    }
  &lt;/ns0:insurancePackageAccList>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>