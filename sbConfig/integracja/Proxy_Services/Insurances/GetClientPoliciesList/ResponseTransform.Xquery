<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/XMLExamples/bzwbk/ResponseTransform/";
declare namespace ns0 = "http://cu.com.pl/cmf-wsdl-types";
declare namespace ns1 = "http://cu.com.pl/cmf-holding-types";
declare namespace ns-1 = "http://cu.com.pl/cmf-biz-types";
declare namespace ns-3 = "urn:applicationul.entities.be.dcl";
declare namespace ns3 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace ns-4 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns-2 = "urn:baseentities.be.dcl";

declare function xf:ResponseTransform($getContractListResponse1 as element(ns3:getContractListResponse))
    as element(ns2:invokeResponse) {
        &lt;ns2:invokeResponse>
            &lt;ns2:messageHelper>
                &lt;ns-2:MessageHelper>
                    &lt;ns-2:errorCode?>&lt;/ns-2:errorCode>
                    &lt;ns-2:errorType?>&lt;/ns-2:errorType>
                    &lt;ns-2:message?>&lt;/ns-2:message>
                &lt;/ns-2:MessageHelper>
            &lt;/ns2:messageHelper>
            {
                let $contracts := $getContractListResponse1/contracts
                return
                    &lt;ns2:policyList>
                        {
                            for $contract in $contracts/contract
                            return
                                &lt;ns-3:PolicyList>
                                    &lt;ns-3:proposalID>{ xs:long( data($contract/proposal-number) ) }&lt;/ns-3:proposalID>
                                    &lt;ns-3:policyID>{ xs:long( data($contract/@contract-number) ) }&lt;/ns-3:policyID>
                                    &lt;ns-3:productID>{ xs:string( data($contract/@product-type) ) }&lt;/ns-3:productID>
                                    &lt;ns-3:proposalDate>{ data($contract/contract-dates/proposal) }&lt;/ns-3:proposalDate>
                                    &lt;ns-3:accountNoPayment>{ 
                                    	let $accounts :=
                                    		for $bank in $contract/banks/bank
                                    		where $bank/@kind = '1'
                                    		return
                                    			data($bank/account)
                                    	return $accounts[1]
                                    }&lt;/ns-3:accountNoPayment>
                                    &lt;ns-3:policyValue>{ xs:double( data($contract/amount) ) }&lt;/ns-3:policyValue>
                                    &lt;ns-3:status>
                                        &lt;ns-4:UlContractStatus>
                                            &lt;ns-4:ulContractStatus>{ xs:string( data($contract/status) ) }&lt;/ns-4:ulContractStatus>
                                        &lt;/ns-4:UlContractStatus>
                                    &lt;/ns-3:status>
                                &lt;/ns-3:PolicyList>
                        }
                    &lt;/ns2:policyList>
            }
        &lt;/ns2:invokeResponse>
};

declare variable $getContractListResponse1 as element(ns3:getContractListResponse) external;

xf:ResponseTransform($getContractListResponse1)</con:xquery>
</con:xqueryEntry>