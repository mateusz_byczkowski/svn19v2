<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:insurance.entities.be.dcl";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};

declare function getFieldsFromHeader($parm as element(ns5:header)) as element()*
{

&lt;DC_TRN_ID?>{data($parm/ns5:transHeader/ns5:transId)}&lt;/DC_TRN_ID>
,
&lt;DC_UZYTKOWNIK?>{concat("SKP:", data($parm/ns5:msgHeader/ns5:userId))}&lt;/DC_UZYTKOWNIK>
,
&lt;DC_ODDZIAL?>{chkUnitId(data($parm/ns5:msgHeader/ns5:unitId))}&lt;/DC_ODDZIAL>

};

declare function getFieldsFromInvoke($parm as element(ns5:invoke)) as element()*
{

&lt;DC_NR_RACHUNKU?>{data($parm/ns5:insurancePolicyAcc/ns4:InsurancePolicyAcc/ns4:policyRefNum)}&lt;/DC_NR_RACHUNKU>
,
&lt;DC_NUMER_ZABEZPIECZENIA?>{data($parm/ns5:insurancePolicyAcc/ns4:InsurancePolicyAcc/ns4:policyNum)}&lt;/DC_NUMER_ZABEZPIECZENIA>
,
&lt;DC_ID_ZABEZPIECZENIA?>{data($parm/ns5:collateral/ns1:Collateral/ns1:collateralItemNumber)}&lt;/DC_ID_ZABEZPIECZENIA>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns5:header)}
    {getFieldsFromInvoke($body/ns5:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>