<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:entities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:insurance.entities.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:bool2short($bool as xs:boolean) as xs:string {
  if ($bool)
    then "1"
    else "0"
};

declare function getFieldsFromHeader($parm as element(ns4:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns4:msgHeader/ns4:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns4:msgHeader/ns4:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns4:msgHeader/ns4:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns4:msgHeader/ns4:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns4:msgHeader/ns4:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns4:msgHeader/ns4:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns4:transHeader/ns4:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns4:invoke)) as element()*
{

&lt;NF_INSPAC_POLICYREFNUM?>{data($parm/ns4:insurancePolicyAcc/ns3:InsurancePolicyAcc/ns3:policyRefNum)}&lt;/NF_INSPAC_POLICYREFNUM>
,
&lt;NF_INSPPH_HISTFLAG?>{data($parm/ns4:insPolicyPremiumHist/ns3:InsPolicyPremiumHist/ns3:histFlag)}&lt;/NF_INSPPH_HISTFLAG>
,
&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns4:bcd/ns1:BusinessControlData/ns1:pageControl/ns5:PageControl/ns5:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns4:bcd/ns1:BusinessControlData/ns1:pageControl/ns5:PageControl/ns5:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
    if (data($parm/ns4:bcd/ns1:BusinessControlData/ns1:pageControl/ns5:PageControl/ns5:reverseOrder))
      then 	
       &lt;NF_PAGEC_REVERSEORDER?>{xf:bool2short(data($parm/ns4:bcd/ns1:BusinessControlData/ns1:pageControl/ns5:PageControl/ns5:reverseOrder))}&lt;/NF_PAGEC_REVERSEORDER>
    else()
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns4:bcd/ns1:BusinessControlData/ns1:pageControl/ns5:PageControl/ns5:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns4:bcd/ns1:BusinessControlData/ns1:pageControl/ns5:PageControl/ns5:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns4:header)}
    {getFieldsFromInvoke($body/ns4:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>