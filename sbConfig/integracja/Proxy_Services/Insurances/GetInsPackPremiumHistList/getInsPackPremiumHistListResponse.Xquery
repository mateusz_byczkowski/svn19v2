<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:insurance.entities.be.dcl";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns2="urn:entities.be.dcl";
declare namespace ns3="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:short2bool($short as xs:int) as xs:string {
  if ($short >= 1)
    then "true"
    else "false"
};

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
&lt;ns1:invokeResponse>
  &lt;ns1:insPackPremiumHistList>
    {for $x at $occ in $parm/NF_INSPPH_CALCULATIONDATE
      return
        &lt;ns0:InsPackPremiumHist>
          {if (data($parm/NF_INSPPH_CALCULATIONDATE[$occ]) and xf:isData(data($parm/NF_INSPPH_CALCULATIONDATE[$occ])))
          then &lt;ns0:calculationDate?>{data($parm/NF_INSPPH_CALCULATIONDATE[$occ])}&lt;/ns0:calculationDate>
          else ()
          }
          {if (data($parm/NF_INSPPH_COLLECTIONDATE[$occ]) and xf:isData(data($parm/NF_INSPPH_COLLECTIONDATE[$occ])))
          then &lt;ns0:collectionDate?>{data($parm/NF_INSPPH_COLLECTIONDATE[$occ])}&lt;/ns0:collectionDate>
          else ()
          }
          &lt;ns0:premiumAmount?>{data($parm/NF_INSPPH_PREMIUMAMOUNT[$occ])}&lt;/ns0:premiumAmount>
          &lt;ns0:insurerAmount?>{data($parm/NF_INSPPH_INSURERAMOUNT[$occ])}&lt;/ns0:insurerAmount>
          &lt;ns0:bankAmount?>{data($parm/NF_INSPPH_BANKAMOUNT[$occ])}&lt;/ns0:bankAmount>
          &lt;ns0:currentAmountDue?>{data($parm/NF_INSPPH_CURRENTAMOUNTDUE[$occ])}&lt;/ns0:currentAmountDue>
          &lt;ns0:oryginalAmountDue?>{data($parm/NF_INPPHI_ORYGINALAMOUNTDU[$occ])}&lt;/ns0:oryginalAmountDue>
          &lt;ns0:paidAmount?>{data($parm/NF_INPPHI_PAIDAMOUNT[$occ])}&lt;/ns0:paidAmount>
        &lt;/ns0:InsPackPremiumHist>
    }
  &lt;/ns1:insPackPremiumHistList>
  &lt;ns1:bcd>
    &lt;ns2:BusinessControlData>
      &lt;ns2:pageControl>
        &lt;ns3:PageControl>
          &lt;ns3:hasNext?>{xf:short2bool(data($parm/NF_PAGEC_HASNEXT))}&lt;/ns3:hasNext>
          &lt;ns3:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns3:navigationKeyDefinition>
          &lt;ns3:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns3:navigationKeyValue>
        &lt;/ns3:PageControl>
      &lt;/ns2:pageControl>
    &lt;/ns2:BusinessControlData>
  &lt;/ns1:bcd>
&lt;/ns1:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>