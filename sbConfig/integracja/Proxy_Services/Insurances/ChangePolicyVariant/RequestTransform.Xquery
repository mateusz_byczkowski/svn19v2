<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:baseauxentities.be.dcl";
declare namespace xf = "http://tempuri.org/BillProtection/Sygnity/CUP/ChangePolicyVariant/RequestTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "urn:applicationul.entities.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns2 = "urn:uldictionary.dictionaries.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns3:invoke),
    $header1 as element(ns3:header))
    as element(ns-1:changeContractCoverageLevelRequest) {
        &lt;ns-1:changeContractCoverageLevelRequest>
            &lt;envelope>
                &lt;request-time>{ data($header1/ns3:msgHeader/ns3:timestamp) }&lt;/request-time>
                &lt;request-no>{ data($header1/ns3:msgHeader/ns3:msgId) }&lt;/request-no>
            	&lt;source-code>{ data($invoke1/ns3:sourceCode/ns4:StringHolder/ns4:value) }&lt;/source-code>
            	&lt;user-id>{ data($header1/ns3:msgHeader/ns3:userId) }&lt;/user-id>
                &lt;branch-id>{ xs:long( data($header1/ns3:msgHeader/ns3:unitId) ) }&lt;/branch-id>
                &lt;bar-code?>{ data($invoke1/ns3:barCodeID/ns4:StringHolder/ns4:value) }&lt;/bar-code>
            &lt;/envelope>
            &lt;save?>{ data($invoke1/ns3:mode/ns4:BooleanHolder/ns4:value) }&lt;/save>
            &lt;product-type?>{ data($invoke1/ns3:policyContract/ns1:PolicyContract/ns1:productCode/ns2:UlParameters/ns2:productId) }&lt;/product-type>
            &lt;contract-number?>{ data($invoke1/ns3:policyContract/ns1:PolicyContract/ns1:policyID) }&lt;/contract-number>
            &lt;effective?>{ fn:substring-before( data( concat($invoke1/ns3:executionDate/ns4:DateHolder/ns4:value, 'T') ), 'T' ) }&lt;/effective>
            &lt;level?>{ 
            	let $levels :=
            		for $coverage in $invoke1/ns3:policyContract/ns1:PolicyContract/ns1:policyCoverageList/ns1:PolicyCoverage
            		return data($coverage/ns1:variant/ns2:OpCoverageVariant/ns2:opCoverageVariant) 
            	return $levels[1] }&lt;/level>
        &lt;/ns-1:changeContractCoverageLevelRequest>
};

declare variable $invoke1 as element(ns3:invoke) external;
declare variable $header1 as element(ns3:header) external;

xf:RequestTransform($invoke1, $header1)</con:xquery>
</con:xqueryEntry>