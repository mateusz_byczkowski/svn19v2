<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:insurance.entities.be.dcl";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse>
  &lt;ns5:response>
    &lt;ns0:ResponseMessage>
      &lt;ns0:result?>{sourceValue2Boolean(data($parm/NF_RESPOM_RESULT),"1")}&lt;/ns0:result>
      &lt;ns0:errorCode?>{data($parm/NF_RESPOM_ERRORCODE)}&lt;/ns0:errorCode>
      &lt;ns0:errorDescription?>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}&lt;/ns0:errorDescription>
    &lt;/ns0:ResponseMessage>
  &lt;/ns5:response>
  &lt;ns5:account>
    &lt;ns1:Account>
      &lt;ns1:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER)}&lt;/ns1:accountNumber>
    &lt;/ns1:Account>
  &lt;/ns5:account>
&lt;/ns5:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>