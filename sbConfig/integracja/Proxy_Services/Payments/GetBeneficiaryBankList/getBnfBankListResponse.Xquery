<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:operations.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:baseauxentities.be.dcl";
declare namespace urn4 = "urn:swift.operations.entities.be.dcl";
declare namespace urn5 = "urn:entities.be.dcl";
declare namespace urn6 = "urn:transactionswiftdict.operationsdictionary.dictionaries.be.dcl";

declare function xf:mapGetBnkListResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		
&lt;urn:invokeResponse>

	&lt;urn:bcd>
		&lt;urn5:BusinessControlData>
			&lt;urn5:pageControl>
				&lt;urn3:PageControl>
			                &lt;urn3:hasNext>			
						{ data($fml/fml:NF_PAGEC_HASNEXT)}
					&lt;/urn3:hasNext>			
					&lt;urn3:navigationKeyDefinition>
						{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYDEFI)}				
					&lt;/urn3:navigationKeyDefinition>
					&lt;urn3:navigationKeyValue>
						{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)}			
					&lt;/urn3:navigationKeyValue>
				&lt;/urn3:PageControl>
			&lt;/urn5:pageControl>	
		&lt;/urn5:BusinessControlData>
	&lt;/urn:bcd>
	&lt;urn:swiftBenefBankList>
            {
                for $bank in $fml/fml:S_FML32 return
                (
			&lt;urn4:SwiftBenefBank>
				&lt;urn4:branchBankId>
					{ data($bank/fml:S_SWIFT_ID)}	
				&lt;/urn4:branchBankId>
				&lt;urn4:bic>
					{ data($bank/fml:S_SWIFT_BIC)}	
				&lt;/urn4:bic>
				&lt;urn4:nationalId>
					{ data($bank/fml:S_SWIFT_BRANCH_CODE)}	
				&lt;/urn4:nationalId>
				&lt;urn4:city>
					{ data($bank/fml:S_SWIFT_TOWN)}	
				&lt;/urn4:city>
				&lt;urn4:name>
					{ data($bank/fml:S_SWIFT_BANK_NAME)}	
				&lt;/urn4:name>
				&lt;urn4:shortName>
					{ data($bank/fml:S_BRANCH_SHORT)}	
				&lt;/urn4:shortName>
				&lt;urn4:address>
					{ data($bank/fml:S_SWIFT_ADDRESS)}	
				&lt;/urn4:address>
                                &lt;urn4:country>
				    &lt;urn2:CountryCode>
					&lt;urn2:countryCode>{ data($bank/fml:S_SWIFT_COUNTRY)}&lt;/urn2:countryCode>
				    &lt;/urn2:CountryCode>
                                &lt;/urn4:country>
			&lt;/urn4:SwiftBenefBank>
              )
              }
	&lt;/urn:swiftBenefBankList>	

  
		&lt;urn:swiftIbanVerificationStatus>
			&lt;urn6:SwiftIbanVerificationStatus>
				&lt;urn6:swiftIbanVerificationStatus>
					{ data($fml/fml:S_IBAN_STATUS)}						
				&lt;/urn6:swiftIbanVerificationStatus>
			&lt;/urn6:SwiftIbanVerificationStatus>
		&lt;/urn:swiftIbanVerificationStatus>
           

&lt;/urn:invokeResponse>						
		
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetBnkListResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>