<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns6="urn:dictionaresrepo.dictionaries.be.dcl";

declare function xf:CYMD2DateTime($indate as xsd:string ) as xsd:string{
     if (string-length($indate) =10) then 
       concat($indate,'T00:00:00')
     else ''
};


declare function xf:mapGetAccountsWnForTransferResponse($fml as element(fml:FML32))
	as element(ns2:Account)* {
    let $accountNumber:=$fml/fml:NF_ACCOUN_ACCOUNTNUMBER
    let $customerNumber:=$fml/fml:NF_ACCOUN_CUSTOMERNUMBER
    let $accountOpenDate:=$fml/fml:NF_ACCOUN_ACCOUNTOPENDAT
    let $accountName:=$fml/fml:NF_ACCOUN_ACCOUNTNAM
    let $skpOwner:=$fml/fml:NF_ACCOUN_SKPOPENER
    let $productCode:=$fml/fml:NF_ACCOUN_PRODUCTCODE
    let $serviceChargeCode:=$fml/fml:NF_TRANA_SERVICECHARGECODE
    let $interestPlanNumber:=$fml/fml:NF_TRANA_INTERESTPLANNUMBE
    let $accountStatus:=$fml/fml:NF_ACCOUS_ACCOUNTSTATUS
    let $branchCode:=$fml/fml:NF_BRANCC_BRANCHCODE
    let $accountDescription:= $fml/fml:NF_ACCOUN_ACCOUNTDESCRIPT
    let $accountIBAN:= $fml/fml:NF_ACCOUN_ACCOUNTIBAN
    let $currentBalance:= $fml/fml:NF_ACCOUN_CURRENTBALANC
    let $availableBalance:= $fml/fml:NF_ACCOUN_AVAILABLEBALANC
    let $currency:=$fml/fml:NF_CURREC_CURRENCYCODE 
    let $accountType:=$fml/fml:NF_ACCOUT_ACCOUNTTYPE

    for $it at $p in $fml/fml:B_NR_RACH
    return
            &lt;ns2:Account>
               &lt;ns2:accountNumber>{data($accountNumber[$p])}&lt;/ns2:accountNumber>
               &lt;ns2:customerNumber>{data($customerNumber[$p])}&lt;/ns2:customerNumber>
               &lt;ns2:accountOpenDate>{xf:CYMD2DateTime(data($accountOpenDate[$p]))}&lt;/ns2:accountOpenDate>
               &lt;ns2:accountName>{data($accountName[$p])}&lt;/ns2:accountName>
               &lt;ns2:currentBalance>{data($currentBalance[$p])}&lt;/ns2:currentBalance>
               &lt;ns2:accountDescription>{data($accountDescription[$p])}&lt;/ns2:accountDescription>
               &lt;ns2:accountIBAN>{data($accountIBAN[$p])}&lt;/ns2:accountIBAN>    
               &lt;ns2:skpOwner>{data($skpOwner[$p])}&lt;/ns2:skpOwner>
               &lt;ns2:productCode>{data($productCode[$p])}&lt;/ns2:productCode>
               &lt;ns2:tranAccount>
                   &lt;ns2:TranAccount>
                       &lt;ns2:serviceChargeCode>{data($serviceChargeCode[$p])}&lt;/ns2:serviceChargeCode>
                       &lt;ns2:interestPlanNumber>{data($interestPlanNumber[$p])}&lt;/ns2:interestPlanNumber>
                   &lt;/ns2:TranAccount>
               &lt;/ns2:tranAccount>
               &lt;ns2:availableBalance>{data($availableBalance[$p])}&lt;/ns2:availableBalance>
               &lt;ns2:accountType>
                 &lt;ns3:AccountType>
                     &lt;ns3:accountType>{data($accountType[$p])}&lt;/ns3:accountType>
                  &lt;/ns3:AccountType>
               &lt;/ns2:accountType>
               &lt;ns2:currency>
                  &lt;ns3:CurrencyCode>
                       &lt;ns3:currencyCode>{data($currency[$p])}&lt;/ns3:currencyCode>
                  &lt;/ns3:CurrencyCode>
               &lt;/ns2:currency>
               &lt;ns2:accountStatus>
                   &lt;ns6:AccountStatus>
                      &lt;ns6:accountStatus>{data($accountStatus[$p])}&lt;/ns6:accountStatus>
                   &lt;/ns6:AccountStatus>
               &lt;/ns2:accountStatus>
               &lt;ns2:accountBranchNumber>
                  &lt;ns3:BranchCode>
                       &lt;ns3:branchCode>{data($branchCode[$p])}&lt;/ns3:branchCode>
                  &lt;/ns3:BranchCode>
               &lt;/ns2:accountBranchNumber>
             &lt;/ns2:Account>  

};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
     &lt;dcl:invokeResponse>
        &lt;dcl:accountsList>
{ xf:mapGetAccountsWnForTransferResponse($body/fml:FML32) }
        &lt;/dcl:accountsList>       
     &lt;/dcl:invokeResponse>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>