<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";		
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";

declare function xf:mapGetAccountsWnForTransferRequest($cus as element(ns1:Customer), $acc as element(ns1:Account))
	as element(fml:FML32) {

      let $customerNumber:= $cus/ns1:customerNumber
      let $currency:=$acc/ns1:currency/ns3:CurrencyCode/ns3:currencyCode

return
		&lt;fml:FML32>
			{
				if($customerNumber)
					then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER >{ data($customerNumber)}&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER>
					else ()
			}
			{
				if($currency)
					then &lt;fml:NF_CURREC_CURRENCYCODE>{ data($currency)}&lt;/fml:NF_CURREC_CURRENCYCODE>
					else ()
			}
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetAccountsWnForTransferRequest($body/dcl:invoke/dcl:customer/ns1:Customer, $body/dcl:invoke/dcl:account/ns1:Account) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>