<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:baseauxentities.be.dcl";

declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:mapChgTranAccountTypeRequest($req as element(ns1:Account), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

              let $msgId:= $msghead/dcl:transId
	let $companyId:= $msghead/dcl:companyId
	let $userId := $msghead/dcl:userId
	let $appId:= $msghead/dcl:appId
	let $unitId := $msghead/dcl:unitId
	let $timestamp:= $msghead/dcl:timestamp
              let $transId:=$tranhead/dcl:transId
              let $skpOpener:=$req/ns1:skpOpener
	let $accountNumber:= $req/ns1:accountNumber
              let $productCode:= $req/ns1:productCode
              let $serviceChargeCode:= $req/ns1:serviceChargeCode
              let $interestPlanNumber:= $req/ns1:interestPlanNumber
              let $promotionCode:= $req/ns1:promotionCode
	return

          &lt;fml:FML32>
            &lt;DC_TRN_ID?>{data($transId)}&lt;/DC_TRN_ID>
            &lt;DC_UZYTKOWNIK?>{concat("SKP:",data($skpOpener))}&lt;/DC_UZYTKOWNIK>
            &lt;DC_ODDZIAL?>{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL>
            &lt;DC_NR_RACHUNKU?>{data($accountNumber)}&lt;/DC_NR_RACHUNKU>
            &lt;DC_NUMER_PRODUKTU?>{data($productCode)}&lt;/DC_NUMER_PRODUKTU>
           &lt;DC_PLAN_OPLAT?>{data($serviceChargeCode)}&lt;/DC_PLAN_OPLAT>
           &lt;DC_PLAN_ODSETKOWY?>{data($interestPlanNumber)}&lt;/DC_PLAN_ODSETKOWY>
           &lt;DC_PROMOCJA?>{data($promotionCode)}&lt;/DC_PROMOCJA>
          &lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


&lt;soap-env:Body>
{xf:mapChgTranAccountTypeRequest($body/dcl:invoke/dcl:account/ns1:Account, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>