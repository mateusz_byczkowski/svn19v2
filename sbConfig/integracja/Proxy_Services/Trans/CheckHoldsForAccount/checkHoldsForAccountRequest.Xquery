<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace urn="urn:be.services.dcl";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap:Body) external;

declare function prepareCheckHoldsForAccountRequest( $req as element(ns1:Account))
               as element(xpcml){
   let $acct:= $req/ns1:accountNumber

  return 

&lt;xpcml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="xpcml.xsd" version="4.0">
   &lt;!-- RPG program: ATA0300  -->
   &lt;!-- created: 2007-12-19-12.52.46 -->
   &lt;!-- source: POSPIT01/ATASORC(ATA0300) -->
   &lt;!-- 2000 -->
   
&lt;program name="program" path="/QSYS.LIB/AIBINTMRST.LIB/ATA0300.PGM"> 
&lt;parameterList>
      &lt;zonedDecimalParm name="PZACCT" passDirection="in" totalDigits="10" fractionDigits="0">{data($acct)}&lt;/zonedDecimalParm>
      &lt;zonedDecimalParm name="PZRETCODE" passDirection="inout" totalDigits="4" fractionDigits="0">0&lt;/zonedDecimalParm> 
      &lt;stringParm name="PNRETVAL" passDirection="inout" length="1">2&lt;/stringParm>
   &lt;/parameterList> 
&lt;/program>
&lt;/xpcml>


};


&lt;soap:Body>
{prepareCheckHoldsForAccountRequest($body/urn:invoke/urn:account/ns1:Account)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>