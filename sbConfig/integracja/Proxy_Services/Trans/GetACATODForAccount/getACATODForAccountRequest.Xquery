<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns3="urn:baseauxentities.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="urn:accountdict.dictionaries.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};



declare function getFieldsFromHeader($parm as element(ns7:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns7:msgHeader/ns7:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns7:msgHeader/ns7:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns7:msgHeader/ns7:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns7:msgHeader/ns7:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns7:msgHeader/ns7:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns7:msgHeader/ns7:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns7:transHeader/ns7:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns7:invoke)) as element()*
{

&lt;NF_FTODAC_FLAGTODACA?>{data($parm/ns7:tranAccountACA/ns0:TranAccountACA/ns0:flagTODACA/ns5:FlagTODACA/ns5:flagTODACA)}&lt;/NF_FTODAC_FLAGTODACA>
,
&lt;NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns7:account/ns0:Account/ns0:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER>
,
&lt;NF_ACCOUN_ACCOUNTIBAN?>{data($parm/ns7:account/ns0:Account/ns0:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTIBAN>
,
&lt;NF_PAGEC_ACTIONCODE?>{data($parm/ns7:bcd/ns4:BusinessControlData/ns4:pageControl/ns3:PageControl/ns3:actionCode)}&lt;/NF_PAGEC_ACTIONCODE>
,
&lt;NF_PAGEC_PAGESIZE?>{data($parm/ns7:bcd/ns4:BusinessControlData/ns4:pageControl/ns3:PageControl/ns3:pageSize)}&lt;/NF_PAGEC_PAGESIZE>
,
&lt;NF_PAGEC_REVERSEORDER?>{boolean2SourceValue (data($parm/ns7:bcd/ns4:BusinessControlData/ns4:pageControl/ns3:PageControl/ns3:reverseOrder),"1","0")}&lt;/NF_PAGEC_REVERSEORDER>
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns7:bcd/ns4:BusinessControlData/ns4:pageControl/ns3:PageControl/ns3:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns7:bcd/ns4:BusinessControlData/ns4:pageControl/ns3:PageControl/ns3:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns7:header)}
    {getFieldsFromInvoke($body/ns7:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>