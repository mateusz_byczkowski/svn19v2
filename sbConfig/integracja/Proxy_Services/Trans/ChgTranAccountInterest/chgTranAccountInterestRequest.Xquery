<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};

declare function xf:prepareShortAccountNumber($accountNumber as xs:string?) as xs:string?{
   let $accountNumberLength:= string-length($accountNumber)
   let $shortAccountNumber := substring($accountNumber,$accountNumberLength - 11)
   let $shortAccountNumberWithZeros := concat(substring("000000000000", string-length($shortAccountNumber) + 1) , $shortAccountNumber)
   return
       $shortAccountNumber
};

declare function xf:mapchgTranAccountInterestRequest($req as element(ns1:Account), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

let $msgId:= $msghead/dcl:transId
let $companyId:= $msghead/dcl:companyId
let $userId := $msghead/dcl:userId
let $appId:= $msghead/dcl:appId
let $unitId := $msghead/dcl:unitId
let $timestamp:= $msghead/dcl:timestamp

let $transId:=$tranhead/dcl:transId
let $skpOpener:=$req/ns1:skpOpener

let $accountNumber:= $req/ns1:accountNumber
let $interTransAccount:= $req/ns1:tranAccount/ns1:TranAccount/ns1:interestTransferAccount 
let $interCodeDisposition:= $req/ns1:tranAccount/ns1:TranAccount/ns1:interestCodeDisposition
let $interPlanNumber:= $req/ns1:tranAccount/ns1:TranAccount/ns1:interestPlanNumber
let $nextCreditIntDate:= $req/ns1:tranAccount/ns1:TranAccount/ns1:nextCreditIntDate
let $interFrequency:= $req/ns1:tranAccount/ns1:TranAccount/ns1:interestFrequency
let $period:=$req/ns1:tranAccount/ns1:TranAccount/ns1:creditInterestPeriod/ns2:Period/ns2:period
let $specialDay:=$req/ns1:tranAccount/ns1:TranAccount/ns1:specificDayDisposition

return
&lt;fml:FML32>
  &lt;DC_TRN_ID?>{data($transId)}&lt;/DC_TRN_ID>
  &lt;DC_UZYTKOWNIK?>{concat("SKP:",data($userId))}&lt;/DC_UZYTKOWNIK>
  &lt;DC_ODDZIAL?>{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL>
  &lt;DC_NR_RACHUNKU?>{xf:prepareShortAccountNumber($accountNumber)}&lt;/DC_NR_RACHUNKU>
 &lt;DC_RACHUNEK?>{xf:prepareShortAccountNumber($interTransAccount)}&lt;/DC_RACHUNEK>
 &lt;DC_PLAN_ODSETKOWY?>{data($interPlanNumber)}&lt;/DC_PLAN_ODSETKOWY>
 &lt;DC_DATA_PLATNOSCI?>{data($nextCreditIntDate)}&lt;/DC_DATA_PLATNOSCI>
 &lt;DC_CYKL_ZESTAWIEN?>{data($interFrequency)}&lt;/DC_CYKL_ZESTAWIEN>
 &lt;DC_OKRES_PLATNOSCI?>{data($period)}&lt;/DC_OKRES_PLATNOSCI>
{
  if(string-length(data($specialDay))> 0) then
    &lt;DC_OKRES_SPLATY?>{data($specialDay)}&lt;/DC_OKRES_SPLATY>
  else()
}
 &lt;DC_KOD_DYSP_ODS?>{data($interCodeDisposition)}&lt;/DC_KOD_DYSP_ODS>
&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


&lt;soap-env:Body>
{ xf:mapchgTranAccountInterestRequest($body/dcl:invoke/dcl:account/ns1:Account, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>