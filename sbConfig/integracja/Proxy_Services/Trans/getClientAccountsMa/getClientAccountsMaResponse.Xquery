<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";

declare function xf:CYMD2DateTime($indate as xsd:string ) as xsd:string{
     if (string-length($indate) =10) then 
       concat($indate,'T00:00:00')
     else ''
};


declare function xf:mapGetAccountsWnForTransferResponse($fml as element(fml:FML32))
	as element(ns2:Account)* {

    let $accountDescription:= $fml/fml:NF_ACCOUN_ACCOUNTDESCRIPT
    let $accountIBAN:= $fml/fml:NF_ACCOUN_ACCOUNTIBAN
    let $currency:=$fml/fml:NF_CURREC_CURRENCYCODE 
    let $accountType:=$fml/fml:NF_ACCOUT_ACCOUNTTYPE

    for $it at $p in $fml/fml:B_NR_RACH
    return
            &lt;ns2:Account>
               &lt;ns2:accountDescription>{data($accountDescription[$p])}&lt;/ns2:accountDescription>
               &lt;ns2:accountIBAN>{data($accountIBAN[$p])}&lt;/ns2:accountIBAN>    
               &lt;ns2:accountType>
                 &lt;ns3:AccountType>
                     &lt;ns3:accountType>{data($accountType[$p])}&lt;/ns3:accountType>
                  &lt;/ns3:AccountType>
               &lt;/ns2:accountType>
               &lt;ns2:currency>
                  &lt;ns3:CurrencyCode>
                       &lt;ns3:currencyCode>{data($currency[$p])}&lt;/ns3:currencyCode>
                  &lt;/ns3:CurrencyCode>
               &lt;/ns2:currency>
             &lt;/ns2:Account>  
 
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
     &lt;dcl:invokeResponse>
        &lt;dcl:accountsList>
{ xf:mapGetAccountsWnForTransferResponse($body/fml:FML32) }
        &lt;/dcl:accountsList>       
     &lt;/dcl:invokeResponse>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>