<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns4="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

&lt;NF_PRODCHA_CODEPRODUCTCHAN?>{data($parm/ns0:productChannel/ns4:ProductChannel/ns4:codeProductChannel)}&lt;/NF_PRODCHA_CODEPRODUCTCHAN>
,
&lt;NF_PRODCHA_POLISHCHANNELNA?>{data($parm/ns0:productChannel/ns4:ProductChannel/ns4:polishChannelName)}&lt;/NF_PRODCHA_POLISHCHANNELNA>
,
&lt;NF_PRODCHA_ENGLISHCHANNELN?>{data($parm/ns0:productChannel/ns4:ProductChannel/ns4:englishChannelName)}&lt;/NF_PRODCHA_ENGLISHCHANNELN>
,
&lt;NF_PRODCHA_IDPRODUCTCHANNE?>{data($parm/ns0:productChannel/ns4:ProductChannel/ns4:idProductChannel)}&lt;/NF_PRODCHA_IDPRODUCTCHANNE>
};

&lt;soap:Body>
  &lt;fml:FML32>
    {if (count($header/ns0:header/ns0:msgHeader)) then getFieldsFromHeader($header/ns0:header) else ()}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>