<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns4="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForProductAreaAttributesList($parm as element(fml:FML32)) as element()
{

&lt;ns4:productAreaAttributesList>
  {
    for $x at $occ in $parm/NF_PRODAA_ATTRIBUTENAME
    return
    &lt;ns4:ProductAreaAttributes>
      &lt;ns4:attributeID?>{data($parm/NF_PRODAA_ATTRIBUTEID[$occ])}&lt;/ns4:attributeID>
      &lt;ns4:attributeName?>{data($parm/NF_PRODAA_ATTRIBUTENAME[$occ])}&lt;/ns4:attributeName>
      &lt;ns4:idProductAreaAttributes?>{data($parm/NF_PRODAA_IDPRODUCTAREAATT[$occ])}&lt;/ns4:idProductAreaAttributes>
      &lt;ns4:descriptionProductAttributes?>{data($parm/NF_PRODAA_DESCRIPTIONPRODU[$occ])}&lt;/ns4:descriptionProductAttributes>
      &lt;ns4:businessNameProductAttributes?>{data($parm/NF_PRODAA_BUSINESSNAMEPROD[$occ])}&lt;/ns4:businessNameProductAttributes>
      &lt;ns4:attributeType>
        &lt;ns3:ProductAreaAttributeType>
          &lt;ns3:attributeType?>{data($parm/NF_PROAAT_ATTRIBUTETYPE[$occ])}&lt;/ns3:attributeType>
        &lt;/ns3:ProductAreaAttributeType>
      &lt;/ns4:attributeType>
    &lt;/ns4:ProductAreaAttributes>
  }
&lt;/ns4:productAreaAttributesList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  &lt;ns0:productAreaEntityOut>
    &lt;ns4:ProductArea>
      &lt;ns4:codeProductArea?>{data($parm/NF_PRODUA_CODEPRODUCTAREA)}&lt;/ns4:codeProductArea>
      &lt;ns4:polishAreaName?>{data($parm/NF_PRODUA_POLISHAREANAME)}&lt;/ns4:polishAreaName>
      &lt;ns4:englishAreaName?>{data($parm/NF_PRODUA_ENGLISHAREANAME)}&lt;/ns4:englishAreaName>
      &lt;ns4:sortOrder?>{data($parm/NF_PRODUA_SORTORDER)}&lt;/ns4:sortOrder>
      &lt;ns4:userChangeSKP?>{data($parm/NF_PRODUA_USERCHANGESKP)}&lt;/ns4:userChangeSKP>
      &lt;ns4:dateChange?>{data($parm/NF_PRODUA_DATECHANGE)}&lt;/ns4:dateChange>
      &lt;ns4:cerberAtributeName?>{data($parm/NF_PRODUA_CERBERATRIBUTENA)}&lt;/ns4:cerberAtributeName>
      &lt;ns4:idProductArea?>{data($parm/NF_PRODUA_IDPRODUCTAREA)}&lt;/ns4:idProductArea>
      {getElementsForProductAreaAttributesList($parm)}
    &lt;/ns4:ProductArea>
  &lt;/ns0:productAreaEntityOut>
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>