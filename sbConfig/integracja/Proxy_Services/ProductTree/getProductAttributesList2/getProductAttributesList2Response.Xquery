<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-06</con:description>
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns5="urn:productstree.entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForProductAttributesList($parm as element(fml:FML32)) as element()
{

&lt;ns0:productAttributesList>
  {
    for $x at $occ in $parm/NF_PRODAT_ATTRIBUTEID
    return
    &lt;ns5:ProductAttributes>
      &lt;ns5:attributeValue?>{data($parm/NF_PRODAT_ATTRIBUTEVALUE[$occ])}&lt;/ns5:attributeValue>
      &lt;ns5:attributeValue2?>{data($parm/NF_PRODAT_ATTRIBUTEVALUE2[$occ])}&lt;/ns5:attributeValue2>
      &lt;ns5:attributeID?>{data($parm/NF_PRODAT_ATTRIBUTEID[$occ])}&lt;/ns5:attributeID>
      &lt;ns5:idProductAttributes?>{data($parm/NF_PRODAT_IDPRODUCTATTRIBU[$occ])}&lt;/ns5:idProductAttributes>
      &lt;ns5:idProductAreaAttributes?>{data($parm/NF_PRODAT_IDPRODUCTAREAATT[$occ])}&lt;/ns5:idProductAreaAttributes>
      &lt;ns5:idProductGroup?>{data($parm/NF_PRODAT_IDPRODUCTGROUP[$occ])}&lt;/ns5:idProductGroup>
      &lt;ns5:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns5:idProductDefinition>
      &lt;ns5:idProductPackage?>{data($parm/NF_PRODAT_IDPRODUCTPACKAGE[$occ])}&lt;/ns5:idProductPackage>
      &lt;ns5:idProductConfiguration?>{data($parm/NF_PRODAT_IDPRODUCTCONFIGU[$occ])}&lt;/ns5:idProductConfiguration>
      &lt;ns5:idProductChannel?>{data($parm/NF_PRODAT_IDPRODUCTCHANNEL[$occ])}&lt;/ns5:idProductChannel>
      &lt;ns5:productAreaAtributes>
        &lt;ns5:ProductAreaAttributes>
          &lt;ns5:attributeName?>{data($parm/NF_PRODAA_ATTRIBUTENAME[$occ])}&lt;/ns5:attributeName>
          &lt;ns5:idProductArea?>{data($parm/NF_PRODAA_IDPRODUCTAREA[$occ])}&lt;/ns5:idProductArea>
          &lt;ns5:descriptionProductAttributes?>{data($parm/NF_PRODAA_DESCRIPTIONPRODU[$occ])}&lt;/ns5:descriptionProductAttributes>
          &lt;ns5:businessNameProductAttributes?>{data($parm/NF_PRODAA_BUSINESSNAMEPROD[$occ])}&lt;/ns5:businessNameProductAttributes>
          &lt;ns5:attributeType>
            &lt;ns3:ProductAreaAttributeType>
              &lt;ns3:attributeType?>{data($parm/NF_PROAAT_ATTRIBUTETYPE[$occ])}&lt;/ns3:attributeType>
            &lt;/ns3:ProductAreaAttributeType>
          &lt;/ns5:attributeType>
        &lt;/ns5:ProductAreaAttributes>
      &lt;/ns5:productAreaAtributes>
    &lt;/ns5:ProductAttributes>
  }
&lt;/ns0:productAttributesList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse>
  {getElementsForProductAttributesList($parm)}
&lt;/ns0:invokeResponse>
};

&lt;soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>