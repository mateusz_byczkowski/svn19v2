<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-06</con:description>
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns5="urn:productstree.entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID>
};


declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
	if (data($parm/ns0:productType/ns4:EditHelper/ns4:stringBased) eq "PRODUCT_GROUP") 
	then &lt;NF_PRODAT_IDPRODUCTGROUP>{ data( $parm/ns0:productEntityID/ns4:IntegerHolder/ns4:value ) }&lt;/NF_PRODAT_IDPRODUCTGROUP>
	else if (data($parm/ns0:productType/ns4:EditHelper/ns4:stringBased) eq "PRODUCT_DEFINITION") 
	then &lt;NF_PRODAT_IDPRODUCTDEFINIT>{ data( $parm/ns0:productEntityID/ns4:IntegerHolder/ns4:value ) }&lt;/NF_PRODAT_IDPRODUCTDEFINIT>
	else if (data($parm/ns0:productType/ns4:EditHelper/ns4:stringBased) eq "PRODUCT_PACKAGE") 
	then &lt;NF_PRODAT_IDPRODUCTPACKAGE>{ data( $parm/ns0:productEntityID/ns4:IntegerHolder/ns4:value ) }&lt;/NF_PRODAT_IDPRODUCTPACKAGE>
	else if (data($parm/ns0:productType/ns4:EditHelper/ns4:stringBased) eq "PRODUCT_AREA_ATTRIBUTE") 
	then &lt;NF_PRODAT_IDPRODUCTAREAATT>{ data( $parm/ns0:productEntityID/ns4:IntegerHolder/ns4:value ) }&lt;/NF_PRODAT_IDPRODUCTAREAATT>
	else if (data($parm/ns0:productType/ns4:EditHelper/ns4:stringBased) eq "PRODUCT_CATEGORY") 
	then &lt;NF_PRODUG_IDPRODUCTCATEGOR>{ data( $parm/ns0:productEntityID/ns4:IntegerHolder/ns4:value ) }&lt;/NF_PRODUG_IDPRODUCTCATEGOR>
	else ()
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>