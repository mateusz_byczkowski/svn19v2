<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$3.2011-04-11</con:description>
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:filtersandmessages.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns5="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
   if ($parm) then
 if ($parm  = "true")
       then $trueval
    else if($parm = "false")
       then $falseval
    else $parm
else()
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID>
,
&lt;NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID>
,
&lt;NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID>
,
&lt;NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID>
,
&lt;NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID>
,
&lt;NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP>
,
&lt;NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

&lt;NF_PRODUD_CODEPRODUCTDEFIN?>{data($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:codeProductDefinition)}&lt;/NF_PRODUD_CODEPRODUCTDEFIN>
,
&lt;NF_PRODUD_POLISHPRODUCTNAM?>{data($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:polishProductName)}&lt;/NF_PRODUD_POLISHPRODUCTNAM>
,
&lt;NF_PRODUD_ENGLISHPRODUCTNA?>{data($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:englishProductName)}&lt;/NF_PRODUD_ENGLISHPRODUCTNA>
,
&lt;NF_PRODUD_SORTORDER?>{data($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:sortOrder)}&lt;/NF_PRODUD_SORTORDER>
,
&lt;NF_PRODUD_IDPRODUCTGROUP?>{data($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:idProductGroup)}&lt;/NF_PRODUD_IDPRODUCTGROUP>
,
if ($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:packageOnlyFlag) then
&lt;NF_PRODUD_PACKAGEONLYFLAG?>{boolean2SourceValue(data($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:packageOnlyFlag),"1","0")}&lt;/NF_PRODUD_PACKAGEONLYFLAG>
else ()
,
&lt;NF_PRODUD_SORCEPRODUCTCODE?>{data($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:sorceProductCode)}&lt;/NF_PRODUD_SORCEPRODUCTCODE>
,
&lt;NF_PRODUD_STARTDATE?>{data($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:startDate)}&lt;/NF_PRODUD_STARTDATE>
,
&lt;NF_PRODUD_ENDDATE?>{data($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:endDate)}&lt;/NF_PRODUD_ENDDATE>
,
&lt;NF_PRODUD_IDPRODUCTDEFINIT?>{data($parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:idProductDefinition)}&lt;/NF_PRODUD_IDPRODUCTDEFINIT>
,

for $it in $parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:productAttributesList/ns5:ProductAttributes
return (
&lt;NF_PRODAT_ATTRIBUTEVALUE>{data($it/ns5:attributeValue)}&lt;/NF_PRODAT_ATTRIBUTEVALUE>,
&lt;NF_PRODAT_ATTRIBUTEVALUE2>{data($it/ns5:attributeValue2)}&lt;/NF_PRODAT_ATTRIBUTEVALUE2>,
&lt;NF_PRODAT_ATTRIBUTEID>{data($it/ns5:attributeID)}&lt;/NF_PRODAT_ATTRIBUTEID>,
if ($it/ns5:attributeID) then &lt;NF_PRODAT_IDPRODUCTDEFINIT>{data($it/ns5:idProductDefinition)}&lt;/NF_PRODAT_IDPRODUCTDEFINIT> else &lt;NF_PRODAT_IDPRODUCTDEFINIT>0&lt;/NF_PRODAT_IDPRODUCTDEFINIT>,
if ($it/ns5:idProductConfiguration) then &lt;NF_PRODAT_IDPRODUCTCONFIGU>{data($it/ns5:idProductConfiguration)}&lt;/NF_PRODAT_IDPRODUCTCONFIGU> else &lt;NF_PRODAT_IDPRODUCTCONFIGU>0&lt;/NF_PRODAT_IDPRODUCTCONFIGU>,
if ($it/ns5:idProductChannel) then &lt;NF_PRODAT_IDPRODUCTCHANNEL>{data($it/ns5:idProductChannel)}&lt;/NF_PRODAT_IDPRODUCTCHANNEL> else &lt;NF_PRODAT_IDPRODUCTCHANNEL>0&lt;/NF_PRODAT_IDPRODUCTCHANNEL>,
&lt;NF_PRODAA_ATTRIBUTENAME>{data($it/ns5:productAreaAtributes/ns5:ProductAreaAttributes/ns5:attributeName)}&lt;/NF_PRODAA_ATTRIBUTENAME>,
&lt;NF_PROAAT_ATTRIBUTETYPE>{data($it/ns5:productAreaAtributes/ns5:ProductAreaAttributes/ns5:attributeType/ns3:ProductAreaAttributeType/ns3:attributeType)}&lt;/NF_PROAAT_ATTRIBUTETYPE>
)
(:
for $it in $parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:productAttributesList/ns5:ProductAttributes/ns5:attributeValue
return
&lt;NF_PRODAT_ATTRIBUTEVALUE?>{data($it)}&lt;/NF_PRODAT_ATTRIBUTEVALUE>
,
for $it in $parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:productAttributesList/ns5:ProductAttributes/ns5:attributeValue2
return
&lt;NF_PRODAT_ATTRIBUTEVALUE2?>{data($it)}&lt;/NF_PRODAT_ATTRIBUTEVALUE2>
,
for $it in $parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:productAttributesList/ns5:ProductAttributes/ns5:attributeID
return
&lt;NF_PRODAT_ATTRIBUTEID?>{data($it)}&lt;/NF_PRODAT_ATTRIBUTEID>
,
for $it in $parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:productAttributesList/ns5:ProductAttributes/ns5:idProductDefinition
return
&lt;NF_PRODAT_IDPRODUCTDEFINIT?>{data($it)}&lt;/NF_PRODAT_IDPRODUCTDEFINIT>
,
for $it in $parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:productAttributesList/ns5:ProductAttributes/ns5:idProductConfiguration
return
&lt;NF_PRODAT_IDPRODUCTCONFIGU?>{data($it)}&lt;/NF_PRODAT_IDPRODUCTCONFIGU>
,
for $it in $parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:productAttributesList/ns5:ProductAttributes/ns5:idProductChannel
return
&lt;NF_PRODAT_IDPRODUCTCHANNEL?>{data($it)}&lt;/NF_PRODAT_IDPRODUCTCHANNEL>
,
for $it in $parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:productAttributesList/ns5:ProductAttributes/ns5:productAreaAtributes/ns5:ProductAreaAttributes/ns5:attributeName
return
&lt;NF_PRODAA_ATTRIBUTENAME?>{data($it)}&lt;/NF_PRODAA_ATTRIBUTENAME>
,
for $it in $parm/ns0:productDefinitionEntity/ns5:ProductDefinition/ns5:productAttributesList/ns5:ProductAttributes/ns5:productAreaAtributes/ns5:ProductAreaAttributes/ns5:attributeType/ns3:ProductAreaAttributeType/ns3:attributeType
return
&lt;NF_PROAAT_ATTRIBUTETYPE?>{data($it)}&lt;/NF_PROAAT_ATTRIBUTETYPE>
:)
};

&lt;soap:Body>
  &lt;fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32>
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>