<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.4
 : @since   2010-02-04
 :
 : wersja WSDLa: 13-01-2010 15:28:58
 :
 : $ProxyServices/Till/chgTillStatus/chgTillStatus_ChgTillStsRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/chgTillStatus/chgTillStatus_ChgTillSts/";
declare namespace ns0 = "urn:acceptance.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns9 = "urn:be.services.dcl";

declare variable $header1 as element(ns9:header) external;
declare variable $invoke1 as element(ns9:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)

	&lt;ns1:FML32>
   
      	(:
       	 : dane z nagłówka
       	 :)
		&lt;ns1:DC_TRN_ID?>{
			data($header1/ns9:transHeader/ns9:transId)
		}&lt;/ns1:DC_TRN_ID>
		
		&lt;ns1:NF_MSHEAD_MSGID?>{
			data($header1/ns9:msgHeader/ns9:msgId)
		}&lt;/ns1:NF_MSHEAD_MSGID>
		
		&lt;ns1:DC_ODDZIAL?>{
			data($header1/ns9:msgHeader/ns9:unitId)
		}&lt;/ns1:DC_ODDZIAL>
		
		&lt;ns1:DC_UZYTKOWNIK?>{
			data($header1/ns9:msgHeader/ns9:userId)
		}&lt;/ns1:DC_UZYTKOWNIK>
		(:
		&lt;ns1:NF_USERTS_SESSIONDATE?>{
			data($invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:sessionDate)
		}&lt;/ns1:NF_USERTS_SESSIONDATE>
		:)
		(:
		 : 456 --> 0 zamknięcie stanowiska
		 : 455 --> 1 otwarcie stanowiska
		 :)
		(:
		{
			let $csrMessageType := $invoke1/ns9:transaction/ns7:Transaction/ns7:csrMessageType/ns6:CsrMessageType/ns6:csrMessageType
			return
				if (data($csrMessageType)) then
					&lt;ns1:NF_USETFS_SESSIONSTATUS?>{
						if (data($csrMessageType) eq "456") then
							0
						else if (data($csrMessageType) eq "455") then
							1
						else ()
					}&lt;/ns1:NF_USETFS_SESSIONSTATUS>
				else ()
		}
		:)
		&lt;ns1:NF_TILL_TILLID?>{
			data($invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:till/ns4:Till/ns4:tillID)
		}&lt;/ns1:NF_TILL_TILLID>
		(:
		&lt;ns1:NF_TELLER_TELLERID?>{
			data($invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:teller/ns4:Teller/ns4:tellerID)
		}&lt;/ns1:NF_TELLER_TELLERID>
		
		&lt;ns1:NF_USER_USERLASTNAME?>{
			data($invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:user/ns5:User/ns5:userLastName)
		}&lt;/ns1:NF_USER_USERLASTNAME>
		
		&lt;ns1:NF_USER_USERID?>{
			data($invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:user/ns5:User/ns5:userID)
		}&lt;/ns1:NF_USER_USERID>
		
		&lt;ns1:NF_USER_USERFIRSTNAME?>{
			data($invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:user/ns5:User/ns5:userFirstName)
		}&lt;/ns1:NF_USER_USERFIRSTNAME>
		:)
		&lt;ns1:NF_BRANCC_BRANCHCODE?>{
			data($invoke1/ns9:branchCode/ns3:BranchCode/ns3:branchCode)
		}&lt;/ns1:NF_BRANCC_BRANCHCODE>
		
	&lt;/ns1:FML32></con:xquery>
</con:xqueryEntry>