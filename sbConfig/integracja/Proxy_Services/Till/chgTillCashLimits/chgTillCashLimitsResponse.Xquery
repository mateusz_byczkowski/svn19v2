<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2010-01-07
 :
 : wersja WSDLa: wersja WSDLa: 28-09-2009 14:43:41
 :
 : $Proxy Services/Till/chgTillCashLimits/chgTillCashLimitsResponse.xq$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/chgTillCashLimits/chgTillCashLimitsResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:chgTillCashLimitsResponse($fML321 as element(ns1:FML32))
    as element(ns2:invokeResponse)
{
    &lt;ns2:invokeResponse>
        &lt;ns2:response>
            &lt;ns0:ResponseMessage>
           		&lt;ns0:result>
           			true
           		&lt;/ns0:result>
            &lt;/ns0:ResponseMessage>
        &lt;/ns2:response>
    &lt;/ns2:invokeResponse>
};

&lt;soap-env:Body>{
	xf:chgTillCashLimitsResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>