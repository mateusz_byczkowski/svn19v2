<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="chgCashStateForInternalTransport.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="chgCashStateForInternalTransport.WSDL" ::)
(:: pragma bea:global-element-return element="ns7:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)
xquery version "1.0";

(:~ 
 :
 : @author  Kacper Pawlaczyk
 : @version 1.4
 : @since   2009-12-17
 :
 : wersja WSDLa: 19-08-2009 10:27:11
 :
 : $Proxy Services/Till/chgCashStateForInternalTransport/chgCashStateForInternalTransportRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/chgCashStateForInternalTransport/chgCashStateForInternalTransport/";
declare namespace ns0 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns2 = "urn:entities.be.dcl";
declare namespace ns3 = "urn:internaltransports.operations.entities.be.dcl";
declare namespace ns4 = "urn:be.services.dcl";
declare namespace ns5 = "";

declare variable $header1 as element(ns4:header) external;
declare variable $invoke1 as element(ns4:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:chgCashStateForInternalTransport($header1 as element(ns4:header),
													   $invoke1 as element(ns4:invoke))
    as element(ns5:FML32)
{
    &lt;ns5:FML32>

    	(:
    	 : dane z nagłówka
    	 :)
        &lt;ns5:DC_TRN_ID?>{
			data($header1/ns4:transHeader/ns4:transId)
		}&lt;/ns5:DC_TRN_ID>

        &lt;ns5:DC_ODDZIAL?>{
			data($header1/ns4:msgHeader/ns4:unitId)
		}&lt;/ns5:DC_ODDZIAL>

        &lt;ns5:DC_UZYTKOWNIK?>{
			data($header1/ns4:msgHeader/ns4:userId)
		}&lt;/ns5:DC_UZYTKOWNIK>

		(:
		 : dane z operacji wejściowej
		 :)
        &lt;ns5:NF_USER_USERLASTNAME?>{
			data($invoke1/ns4:sourceUser/ns2:User/ns2:userLastName)
		}&lt;/ns5:NF_USER_USERLASTNAME>

        &lt;ns5:NF_USER_USERID?>{
			data($invoke1/ns4:sourceUser/ns2:User/ns2:userID)
		}&lt;/ns5:NF_USER_USERID>

        &lt;ns5:NF_USER_USERFIRSTNAME?>{
			data($invoke1/ns4:sourceUser/ns2:User/ns2:userFirstName)
		}&lt;/ns5:NF_USER_USERFIRSTNAME>
		
        &lt;ns5:NF_USER_USERLASTNAME?>{
			data($invoke1/ns4:targetUser/ns2:User/ns2:userLastName)
		}&lt;/ns5:NF_USER_USERLASTNAME>
		
		&lt;ns5:NF_USER_USERID?>{
			data($invoke1/ns4:targetUser/ns2:User/ns2:userID)
		}&lt;/ns5:NF_USER_USERID>

		&lt;ns5:NF_USER_USERFIRSTNAME?>{
			data($invoke1/ns4:targetUser/ns2:User/ns2:userFirstName)
		}&lt;/ns5:NF_USER_USERFIRSTNAME>

        &lt;ns5:NF_INTECT_TRANSPORTID?>{
			data($invoke1/ns4:internalCashTransport/ns3:InternalCashTransport/ns3:transportID)
		}&lt;/ns5:NF_INTECT_TRANSPORTID>

		(:
		 : zmiana statusu transportu wewnętrznego:
		 : - podstawowy na zamknięty (C)
		 : - rozszerzony na zrealizowany (CD)
		 :)
		&lt;ns5:NF_INTCTS_INTERNALCASHTRAN>CD&lt;/ns5:NF_INTCTS_INTERNALCASHTRAN>
		&lt;ns5:NF_INTCTS_INTERNALCASHTRAN>C&lt;/ns5:NF_INTCTS_INTERNALCASHTRAN>

        &lt;ns5:NF_INTECT_SOURCENAME?>{
			data($invoke1/ns4:internalCashTransport/ns3:InternalCashTransport/ns3:sourceName)
		}&lt;/ns5:NF_INTECT_SOURCENAME>

        &lt;ns5:NF_INTECT_TARGETNAME?>{
			data($invoke1/ns4:internalCashTransport/ns3:InternalCashTransport/ns3:targetName)
		}&lt;/ns5:NF_INTECT_TARGETNAME>

        &lt;ns5:NF_TELLER_TELLERID?>{
			data($invoke1/ns4:internalCashTransport/ns3:InternalCashTransport/ns3:sourceTeller/ns1:Teller/ns1:tellerID)
		}&lt;/ns5:NF_TELLER_TELLERID>

        &lt;ns5:NF_TILL_TILLID2?>{
			data($invoke1/ns4:internalCashTransport/ns3:InternalCashTransport/ns3:targetTill/ns1:Till/ns1:tillID)
		}&lt;/ns5:NF_TILL_TILLID2>

        &lt;ns5:NF_TELLER_TELLERID2?>{
			data($invoke1/ns4:internalCashTransport/ns3:InternalCashTransport/ns3:targetTeller/ns1:Teller/ns1:tellerID)
		}&lt;/ns5:NF_TELLER_TELLERID2>

        &lt;ns5:NF_BRACST_WORKDATE?>{
			data($invoke1/ns4:internalCashTransport/ns3:InternalCashTransport/ns3:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:workDate)
		}&lt;/ns5:NF_BRACST_WORKDATE>

    &lt;/ns5:FML32>
};

&lt;soap-env:Body>{
	xf:chgCashStateForInternalTransport($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>