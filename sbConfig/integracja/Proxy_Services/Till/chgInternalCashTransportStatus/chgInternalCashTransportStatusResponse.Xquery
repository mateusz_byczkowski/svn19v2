<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.2
 : @since   2009-12-16
 :
 : wersja WSDLa: 28-05-2009 14:06:42
 :
 : $Proxy Services/Till/chgInternalCashTransportStatus/chgInternalCashTransportStatusResponse.xq$
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/chgInternalCashTransportStatus/chgInternalCashTransportStatusResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:chgInternalCashTransportStatusResponse($fML321 as element(ns0:FML32))
    as element(ns2:invokeResponse)
{
    &lt;ns2:invokeResponse>
        &lt;ns2:response>
            &lt;ns1:ResponseMessage>
                	&lt;ns1:result>
                		true
                	&lt;/ns1:result>
            &lt;/ns1:ResponseMessage>
        &lt;/ns2:response>
    &lt;/ns2:invokeResponse>
};

&lt;soap-env:Body>{
	xf:chgInternalCashTransportStatusResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>