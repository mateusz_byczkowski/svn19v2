<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Response z wywolania SaveTransfer</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$fML321" element="ns1:FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-return element="ns0:invokeResponse" location="addReconciliationItem.wsdl" ::)

declare namespace ns2 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/addReconciliationItem/addReconciliationItemStResponse/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";


declare function xf:addReconciliationItemStResponse($fML321 as element(ns1:FML32))
    as element(ns0:invokeResponse) {
        &lt;ns0:invokeResponse>
            &lt;ns0:result>
                &lt;ns2:ResponseMessage>
                    &lt;ns2:result>{ xs:boolean( (data($fML321/ns1:TR_STATUS)="P") ) }&lt;/ns2:result>
                &lt;/ns2:ResponseMessage>
            &lt;/ns0:result>
        &lt;/ns0:invokeResponse>
};

declare variable $fML321 as element(ns1:FML32) external;

&lt;soap-env:Body>{
xf:addReconciliationItemStResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>