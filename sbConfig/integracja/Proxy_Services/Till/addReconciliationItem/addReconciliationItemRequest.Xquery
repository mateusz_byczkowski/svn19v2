<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.3
 : @since   2009-12-16
 :
 : wersja WSDLa: 27-01-2010 09:10:30
 :
 : $Proxy Services/Till/addReconsiliationItem/addReconciliationItemRequest.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/addReconciliationItem/addReconciliationItemRequest/";
declare namespace ns0 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";

declare variable $header1 as element(ns6:header) external;
declare variable $invoke1 as element(ns6:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:addReconciliationItemRequest($header1 as element(ns6:header),
												   $invoke1 as element(ns6:invoke))
    as element(ns4:FML32)
{
        &lt;ns4:FML32>

        	(:
        	 : dane z nagłówka komunikatu
        	 :)
        	&lt;ns4:DC_TRN_ID?>{
				data($header1/ns6:transHeader/ns6:transId)
			}&lt;/ns4:DC_TRN_ID>
			
        	&lt;ns4:DC_ODDZIAL?>{
				data($header1/ns6:msgHeader/ns6:unitId)
			}&lt;/ns4:DC_ODDZIAL>
			
        	&lt;ns4:DC_UZYTKOWNIK?>{
				data($header1/ns6:msgHeader/ns6:userId)
			}&lt;/ns4:DC_UZYTKOWNIK>
        	
        	(:
        	 : dane z operacji wejściowej
        	 :)
            &lt;ns4:NF_BRANCC_BRANCHCODE?>{
            	data($invoke1/ns6:branchCode/ns2:BranchCode/ns2:branchCode)
			}&lt;/ns4:NF_BRANCC_BRANCHCODE>
			
            &lt;ns4:NF_USERTS_SESSIONDATE?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:sessionDate)
			}&lt;/ns4:NF_USERTS_SESSIONDATE>
            
            &lt;ns4:NF_TILL_TILLID?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID)
			}&lt;/ns4:NF_TILL_TILLID>
            
            &lt;ns4:NF_TELLER_TELLERID?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID)
			}&lt;/ns4:NF_TELLER_TELLERID>
			
            &lt;ns4:NF_USER_USERLASTNAME?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:user/ns5:User/ns5:userLastName)
			}&lt;/ns4:NF_USER_USERLASTNAME>
			
            &lt;ns4:NF_USER_USERID?>{	
            	data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:user/ns5:User/ns5:userID)
			}&lt;/ns4:NF_USER_USERID>
			
            &lt;ns4:NF_USER_USERFIRSTNAME?>{
            	data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:user/ns5:User/ns5:userFirstName)
			}&lt;/ns4:NF_USER_USERFIRSTNAME>
				
			&lt;ns4:NF_CURRCA_AMOUNT?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyCashList/ns1:CurrencyCash[1]/ns1:amount)
			}&lt;/ns4:NF_CURRCA_AMOUNT>
					
			{
				for $i in 1 to count($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyCashList/ns1:CurrencyCash[1]/ns1:denominationSpecificationList/ns1:DenominationSpecification)
				return
				(
					&lt;ns4:NF_DENOMS_ITEMSNUMBER>{
						data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyCashList/ns1:CurrencyCash[1]/ns1:denominationSpecificationList/ns1:DenominationSpecification[$i]/ns1:itemsNumber)
					}&lt;/ns4:NF_DENOMS_ITEMSNUMBER>,
					
					&lt;ns4:NF_DENOMD_DENOMINATIONID>{
						data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyCashList/ns1:CurrencyCash[1]/ns1:denominationSpecificationList/ns1:DenominationSpecification[$i]/ns1:denomination/ns0:DenominationDefinition/ns0:denominationID)
					}&lt;/ns4:NF_DENOMD_DENOMINATIONID>
				)
            }
            
            &lt;ns4:NF_CURREC_CURRENCYCODE?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyCashList/ns1:CurrencyCash[1]/ns1:currency/ns2:CurrencyCode/ns2:currencyCode)
			}&lt;/ns4:NF_CURREC_CURRENCYCODE>

			{
				let $reconciliationDate := $invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation[1]/ns1:reconciliationDate
				return
					if (data($reconciliationDate)) then
			            &lt;ns4:NF_CURRRE_RECONCILIATIONDA>{
			            	fn:concat(fn:substring(data($reconciliationDate), 1, 10),
			            			  '-',
			            			  fn:substring(data($reconciliationDate), 12, 2),
			            			  '.',
			            			  fn:substring(data($reconciliationDate), 15, 2),
			            			  '.',
			            			  fn:substring(data($reconciliationDate), 18, 2),
			            			  '.000000')
						}&lt;/ns4:NF_CURRRE_RECONCILIATIONDA>
					else
						()
			}
		            
            &lt;ns4:NF_CURRRE_DIFFERENCEAMOUNT?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation[1]/ns1:differenceAmount)
			}&lt;/ns4:NF_CURRRE_DIFFERENCEAMOUNT>
			
            &lt;ns4:NF_CURRRE_NEGATIVERECONCIL?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation[1]/ns1:negativeReconciliationCounter)
			}&lt;/ns4:NF_CURRRE_NEGATIVERECONCIL>

            &lt;ns4:NF_CURRES_CURRENCYRECONCEX?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation[1]/ns1:reconciliationExtStatus/ns0:CurrencyReconcExtStatus/ns0:currencyReconcExtStatus)
			}&lt;/ns4:NF_CURRES_CURRENCYRECONCEX>
								
            &lt;ns4:NF_CURRRS_CURRENCYRECONCST?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation[1]/ns1:reconciliationStatus/ns0:CurrencyReconcStatus/ns0:currencyReconcStatus)
			}&lt;/ns4:NF_CURRRS_CURRENCYRECONCST>
			
			&lt;ns4:NF_ACCEPT_ACCEPTOR?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation[1]/ns1:acceptor/ns5:User/ns5:userID)
			}&lt;/ns4:NF_ACCEPT_ACCEPTOR>
			
            &lt;ns4:NF_CURRRS_RECONPARAMETERWR?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation[1]/ns1:reconciliationStatus/ns0:CurrencyReconcStatus/ns0:reconParameterWrite)
			}&lt;/ns4:NF_CURRRS_RECONPARAMETERWR>
			
            &lt;ns4:NF_CURRRS_INTRECMKSSTAWRI?>{
				data($invoke1/ns6:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation[1]/ns1:reconciliationStatus/ns0:CurrencyReconcStatus/ns0:internalRecMKSStatusWrite)
			}&lt;/ns4:NF_CURRRS_INTRECMKSSTAWRI>
					
        &lt;/ns4:FML32>
};


	xf:addReconciliationItemRequest($header1, $invoke1)</con:xquery>
</con:xqueryEntry>