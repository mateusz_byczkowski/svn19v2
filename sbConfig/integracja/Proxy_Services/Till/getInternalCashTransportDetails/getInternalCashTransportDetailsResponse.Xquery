<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2010-06-30
 :
 : wersja WSDLa: 19-04-2010 09:48:18
 :
 : $Proxy Services/Till/getInternalCashTransportDetails/getInternalCashTransportDetailsResponse.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getInternalCashTransportDetails/getInternalCashTransportDetailsResponse/";
declare namespace ns0 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns2 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns6 = "urn:internaltransports.operations.entities.be.dcl";
declare namespace ns7 = "urn:be.services.dcl";
declare namespace ns8 = "";
declare namespace ns9 = "urn:internaltransportsdict.operationsdictionary.dictionaries.be.dcl";

declare variable $fML321 as element(ns8:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getInternalCashTransportDetailsResponse($fML321 as element(ns8:FML32))
    as element(ns7:invokeResponse)
{
	&lt;ns7:invokeResponse>
        &lt;ns7:internalCashTransportOut>
			&lt;ns6:InternalCashTransport>

		        &lt;ns6:sourceName?>{
					data($fML321/ns8:NF_INTECT_SOURCENAME)
				}&lt;/ns6:sourceName>

				&lt;ns6:targetName?>{
					data($fML321/ns8:NF_INTECT_TARGETNAME)
				}&lt;/ns6:targetName>

				(:
				 : data niepusta, różna od '0001-01-01'
				 :)
				{
					let $transportOpenDate := data($fML321/ns8:NF_INTECT_TRANSPORTOPENDAT)
					return
						if ($transportOpenDate
							and $transportOpenDate ne '0001-01-01') then
							&lt;ns6:transportOpenDate>{
								$transportOpenDate
							}&lt;/ns6:transportOpenDate>
						else
							()
				}

				(:
				 : data niepusta, różna od '0001-01-01'
				 :)
				{
					let $transportCloseDate := data($fML321/ns8:NF_INTECT_TRANSPORTCLOSEDA)
					return
						if ($transportCloseDate
							and $transportCloseDate ne '0001-01-01') then
							&lt;ns6:transportCloseDate>{
								$transportCloseDate
							}&lt;/ns6:transportCloseDate>
						else
							()
				}

				&lt;ns6:currencyCashList?>{

					(:
					 : lista walut - distinct
					 :)
					let $currencyList := fn:distinct-values($fML321/ns8:NF_CURREC_CURRENCYCODE)

					(:
					 : dla każdej różnej waluty
					 :)
					for $currencyCode in $currencyList
					return
						&lt;ns1:CurrencyCash>
							&lt;ns1:amount>{

								(:
								 : licznosc $fML321/ns8:NF_CURRCA_AMOUNT == licznosc $fML321/ns8:NF_CURREC_CURRENCYCODE
								 :
								 : Ponieważ kwoty dla dwóch różnych walut mogą się powtarzać,
								 : szukamy pierwszego indeksu na liście wszystkich dla każdej z walut.
								 : Znaleziony indeks jest również indeksem odpowiadającej walucie kwoty.
								 :
								 : Przykładowo:
								 : NF_CURRCA_CURRENCYCODE    PLN
								 : NF_CURRCA_CURRENCYCODE    PLN
								 : NF_CURRCA_CURRENCYCODE    USD
								 : NF_CURRCA_AMOUNT          1000.00 (dla PLN)
								 : NF_CURRCA_AMOUNT          1000.00 (dla PLN)
								 : NF_CURRCA_AMOUNT          1000.00 (dla USD)
								 :
								 : wowczas index-of dla $currencyCode == PLN zwroci (1,2) (wybieramy pierwsze wystapnienie - [1])
								 :         index-of dla $currencyCode == USD zwroci (3)   (analogicznie)
								 :
								 :)
								let $amountIndex := fn:index-of($fML321/ns8:NF_CURREC_CURRENCYCODE, $currencyCode)[1]
								return
									data($fML321/ns8:NF_CURRCA_AMOUNT[xs:int($amountIndex)])
							}&lt;/ns1:amount>

							&lt;ns1:denominationSpecificationList>{
								(:
								 : wszystkie wystąpienia NF_DENOMS_ITEMSNUMBER oraz NF_DENOMD_DENOMINATIONID
								 : na liscie wszystkich walut, ktore odpowiadaja
								 : obecnie rozważanej walucie (distinct)
								 :
								 : Przykladowo:
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE USD
								 : NF_CURRCA_CURRENCYCODE USD
								 :

								 : Pierwsze przejście:
								 :      NF_DENOMS_ITEMSNUMBER, NF_DENOMD_DENOMINATIONID z 1., 2. oraz 3. wystąpienia
								 : Drugie przejście:
								 :		NF_DENOMS_ITEMSNUMBER, NF_DENOMD_DENOMINATIONID z 4. oraz 5. wystąpienia
								 :)
								for $i in 1 to count($fML321/ns8:NF_CURREC_CURRENCYCODE)
								where (data($fML321/ns8:NF_CURREC_CURRENCYCODE[$i]) eq $currencyCode)
								return
									&lt;ns1:DenominationSpecification>
										&lt;ns1:itemsNumber>{
											data($fML321/ns8:NF_DENOMS_ITEMSNUMBER[$i])
										}&lt;/ns1:itemsNumber>
	
										&lt;ns1:denomination>
											&lt;ns0:DenominationDefinition>
												&lt;ns0:denominationID>{
													data($fML321/ns8:NF_DENOMD_DENOMINATIONID[$i])
												}&lt;/ns0:denominationID>
											&lt;/ns0:DenominationDefinition>
										&lt;/ns1:denomination>
									&lt;/ns1:DenominationSpecification>

							}&lt;/ns1:denominationSpecificationList>

							&lt;ns1:currency>
					            &lt;ns3:CurrencyCode>
			                        &lt;ns3:currencyCode>{
										data($currencyCode)
									}&lt;/ns3:currencyCode>
								&lt;/ns3:CurrencyCode>
							&lt;/ns1:currency>
						&lt;/ns1:CurrencyCash>

				}&lt;/ns6:currencyCashList>

                &lt;ns6:sourceTill?>
                    &lt;ns4:Till?>
                        &lt;ns4:tillID?>{
							data($fML321/ns8:NF_TILL_TILLID)
						}&lt;/ns4:tillID>
						&lt;ns4:tillType?>
							&lt;ns2:TillType?>
								&lt;ns2:tillType?>{
									data($fML321/ns8:NF_TILTY_TILLTYP[1])
								}&lt;/ns2:tillType>
							&lt;/ns2:TillType>
						&lt;/ns4:tillType>						
                    &lt;/ns4:Till>
                &lt;/ns6:sourceTill>

				&lt;ns6:sourceTeller?>
				    &lt;ns4:Teller?>
				        &lt;ns4:tellerID?>{
							data($fML321/ns8:NF_TELLER_TELLERID)
						}&lt;/ns4:tellerID>
				    &lt;/ns4:Teller>
				&lt;/ns6:sourceTeller>

                &lt;ns6:targetTill?>
                    &lt;ns4:Till?>
                        &lt;ns4:tillID?>{
							data($fML321/ns8:NF_TILL_TILLID2)
						}&lt;/ns4:tillID>
                        &lt;ns4:tillType?>
                            &lt;ns2:TillType?>
                                &lt;ns2:tillType?>{
									data($fML321/ns8:NF_TILTY_TILLTYP[2])
								}&lt;/ns2:tillType>
                            &lt;/ns2:TillType>
                        &lt;/ns4:tillType>
                    &lt;/ns4:Till>
                &lt;/ns6:targetTill>

                &lt;ns6:targetTeller?>
                    &lt;ns4:Teller?>
                        &lt;ns4:tellerID?>{
							data($fML321/ns8:NF_TELLER_TELLERID2)
						}&lt;/ns4:tellerID>
                    &lt;/ns4:Teller>
                &lt;/ns6:targetTeller>

                &lt;ns6:transportStatus?>
                    &lt;ns9:InternalCashTransportStatus?>
                        &lt;ns9:internalCashTransportStatus?>{
							data($fML321/ns8:NF_INTCTS_INTERNALCASHTRAN)
						}&lt;/ns9:internalCashTransportStatus>
                    &lt;/ns9:InternalCashTransportStatus>
                &lt;/ns6:transportStatus>

                &lt;ns6:transportExtStatus?>
                    &lt;ns9:InternalCashTranspExtStatus?>
                        &lt;ns9:internalCashTranspExtStatus?>{
							data($fML321/ns8:NF_INCTES_INTERNALCASHTRAN)
						}&lt;/ns9:internalCashTranspExtStatus>
                    &lt;/ns9:InternalCashTranspExtStatus>
                &lt;/ns6:transportExtStatus>

                &lt;ns6:initiatorUser?>
                    &lt;ns5:User?>
                        &lt;ns5:userLastName?>{
							data($fML321/ns8:NF_USER_USERLASTNAME[1])
						}&lt;/ns5:userLastName>

                        &lt;ns5:userID?>{
							data($fML321/ns8:NF_USER_USERID[1])
						}&lt;/ns5:userID>

                        &lt;ns5:userFirstName?>{
							data($fML321/ns8:NF_USER_USERFIRSTNAME[1])
						}&lt;/ns5:userFirstName>
                    &lt;/ns5:User>
                &lt;/ns6:initiatorUser>

                &lt;ns6:finisherUser?>
                    &lt;ns5:User?>
                        &lt;ns5:userLastName?>{
							data($fML321/ns8:NF_USER_USERLASTNAME[2])
						}&lt;/ns5:userLastName>

                        &lt;ns5:userID?>{
							data($fML321/ns8:NF_USER_USERID[2])
						}&lt;/ns5:userID>

                        &lt;ns5:userFirstName?>{
							data($fML321/ns8:NF_USER_USERFIRSTNAME[2])
						}&lt;/ns5:userFirstName>
                    &lt;/ns5:User>
                &lt;/ns6:finisherUser>

                &lt;ns6:branchCurrentStatus?>
                    &lt;ns4:BranchCurrentStatus?>
						(:
						 : data niepusta, różna od '0001-01-01'
						 :)
						{
							let $workDate := data($fML321/ns8:NF_BRACST_WORKDATE)
							return
								if ($workDate
									and $workDate ne '0001-01-01') then
			                        &lt;ns4:workDate>{
										$workDate
									}&lt;/ns4:workDate>
								else
									()
						}

                        &lt;ns4:branchCode?>
                            &lt;ns3:BranchCode?>
                                &lt;ns3:branchCode?>{
									data($fML321/ns8:NF_BRANCC_BRANCHCODE)
								}&lt;/ns3:branchCode>
                            &lt;/ns3:BranchCode>
                        &lt;/ns4:branchCode>
                    &lt;/ns4:BranchCurrentStatus>
                &lt;/ns6:branchCurrentStatus>
			&lt;/ns6:InternalCashTransport>
		&lt;/ns7:internalCashTransportOut>
	&lt;/ns7:invokeResponse>
};

&lt;soap-env:Body>{
	xf:getInternalCashTransportDetailsResponse($fML321)	
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>