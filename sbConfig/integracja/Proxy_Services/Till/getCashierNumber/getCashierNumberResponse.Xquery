<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2010-01-13
 :
 : $Proxy Services/Till/getCashierNumber/getCashierNumberRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getCashierNumber/getCashierNumberResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "http://bzwbk.com/services/icbs/messages/";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return getCashierNumberResponse operacja wyjściowa
 :)
declare function xf:getCashierNumberResponse($fML321 as element(ns0:FML32))
    as element(ns1:getCashierNumberResponse)
{
	&lt;ns1:getCashierNumberResponse>

		&lt;ns1:TellerID>{
			data($fML321/ns0:NF_TELLER_TELLERID)
		}&lt;/ns1:TellerID>

		&lt;ns1:DateFrom>{
			data($fML321/ns0:NF_DATE_FROM)
		}&lt;/ns1:DateFrom>

	&lt;/ns1:getCashierNumberResponse>
};

&lt;soap-env:Body>{
	xf:getCashierNumberResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>