<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace urn ="urn:be.services.dcl";
declare namespace urn1="urn:baseauxentities.be.dcl";
declare namespace urn2="urn:internaltransports.operations.entities.be.dcl";
declare namespace urn3="urn:branchmanagement.operations.entities.be.dcl";
declare namespace urn4="urn:internaltransportsdict.operationsdictionary.dictionaries.be.dcl";
declare namespace urn5="urn:entities.be.dcl";
declare namespace urn6="urn:dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $header as element(soap-env:Header) external;
declare variable $body as element(soap-env:Body) external;

declare function xf:mapDateTime($dateIn as xs:dateTime) as xs:string {
  fn:concat (xf:convertTo4CharString(fn:year-from-dateTime($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-dateTime($dateIn)),"-",
             xf:convertTo2CharString(fn:day-from-dateTime($dateIn)))
};

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string

  return 
    if ($value &lt; 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:booleanTo01($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "1"
    else "0"
};

&lt;soap-env:Body>
  {
    let $reqH := $header/urn:header
    let $m0   := $reqH/urn:msgHeader
    let $req  := $body/urn:invoke
    let $m1   := $req/urn:internalCashTransport/urn2:InternalCashTransport
    let $m2   := $req/urn:bcd/urn5:BusinessControlData/urn5:pageControl/urn1:PageControl

    return
    &lt;fml:FML32>
      {if($m0/urn:msgId)
        then &lt;fml:NF_MSHEAD_MSGID>{ data($m0/urn:msgId) }&lt;/fml:NF_MSHEAD_MSGID>
        else ()
      }
      {if($m0/urn:companyId)
        then &lt;fml:NF_MSHEAD_COMPANYID>{ data($m0/urn:companyId) }&lt;/fml:NF_MSHEAD_COMPANYID>
        else ()
      }
      {if($m0/urn:unitId)
        then &lt;fml:NF_MSHEAD_UNITID>{ data($m0/urn:unitId) }&lt;/fml:NF_MSHEAD_UNITID>
        else ()
      }
      {if($m0/urn:userId)
        then &lt;fml:NF_MSHEAD_USERID>{ data($m0/urn:userId) }&lt;/fml:NF_MSHEAD_USERID>
        else ()
      }
      {if($m0/urn:appId)
        then &lt;fml:NF_MSHEAD_APPID>{ data($m0/urn:appId) }&lt;/fml:NF_MSHEAD_APPID>
        else ()
      }
      {if($m0/urn:timestamp)
        then &lt;fml:NF_MSHEAD_TIMESTAMP>{ data($m0/urn:timestamp) }&lt;/fml:NF_MSHEAD_TIMESTAMP>
        else ()
      }
      {if($reqH/urn:transHeader/urn:transId)
        then &lt;fml:NF_TRHEAD_TRANSID>{ data($reqH/urn:transHeader/urn:transId) }&lt;/fml:NF_TRHEAD_TRANSID>
        else ()
      }

      {if($req/urn:workDateFrom/urn1:DateHolder/urn1:value)
        then &lt;fml:NF_FILTEH_STARTDATE>{ xf:mapDateTime($req/urn:workDateFrom/urn1:DateHolder/urn1:value) }&lt;/fml:NF_FILTEH_STARTDATE>
        else ()
      }
      {if($req/urn:workDateTo/urn1:DateHolder/urn1:value)
        then &lt;fml:NF_FILTEH_ENDDATE>{ xf:mapDateTime($req/urn:workDateTo/urn1:DateHolder/urn1:value) }&lt;/fml:NF_FILTEH_ENDDATE>
        else ()
      }
      {if($m1/urn2:sourceTill/urn3:Till/urn3:tillID)
        then &lt;fml:NF_TILL_TILLID>{ data($m1/urn2:sourceTill/urn3:Till/urn3:tillID) }&lt;/fml:NF_TILL_TILLID>
        else ()
      }
      {if($m1/urn2:targetTill/urn3:Till/urn3:tillID)
        then &lt;fml:NF_TILL_TILLID2>{ data($m1/urn2:targetTill/urn3:Till/urn3:tillID) }&lt;/fml:NF_TILL_TILLID2>
        else ()
      }
      {if($m1/urn2:transportStatus/urn4:InternalCashTransportStatus/urn4:internalCashTransportStatus)
        then &lt;fml:NF_INTCTS_INTERNALCASHTRAN>{ data($m1/urn2:transportStatus/urn4:InternalCashTransportStatus/urn4:internalCashTransportStatus) }&lt;/fml:NF_INTCTS_INTERNALCASHTRAN>
        else ()
      }
            
      {if($m2/urn1:actionCode)
        then &lt;fml:NF_PAGEC_ACTIONCODE>{ data($m2/urn1:actionCode) }&lt;/fml:NF_PAGEC_ACTIONCODE>
        else ()
      }
      {if($m2/urn1:pageSize)
        then &lt;fml:NF_PAGEC_PAGESIZE>{ data($m2/urn1:pageSize) }&lt;/fml:NF_PAGEC_PAGESIZE>
        else ()
      }
      {if($m2/urn1:reverseOrder)
        then &lt;fml:NF_PAGEC_REVERSEORDER>{ xf:booleanTo01($m2/urn1:reverseOrder) }&lt;/fml:NF_PAGEC_REVERSEORDER>
        else ()
      }
      {if($m2/urn1:navigationKeyDefinition)
        then &lt;fml:NF_PAGEC_NAVIGATIONKEYDEFI>{ data($m2/urn1:navigationKeyDefinition) }&lt;/fml:NF_PAGEC_NAVIGATIONKEYDEFI>
        else ()
      }
      {if($m2/urn1:navigationKeyValue)
        then &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU>{ data($m2/urn1:navigationKeyValue) }&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU>
        else ()
      }

      {if($req/urn:branchCode/urn6:BranchCode/urn6:branchCode)
        then &lt;fml:NF_BRANCC_BRANCHCODE>{ data($req/urn:branchCode/urn6:BranchCode/urn6:branchCode) }&lt;/fml:NF_BRANCC_BRANCHCODE>
        else ()
      }
    &lt;/fml:FML32>
  }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>