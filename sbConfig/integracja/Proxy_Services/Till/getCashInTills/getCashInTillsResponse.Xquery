<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @since   2010-02-17
 : @version 1.0
 :
 : wersja WSDLa: 06-07-2009 15:36:51
 :
 : $Proxy Services/Till/getCashInTills/getCashInTillsResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getCashInTills/getCashInTillsResponse/";
declare namespace ns0 = "urn:cash.operations.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns7 = "urn:baseauxentities.be.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getCashInTillsResponse($fML321 as element(ns1:FML32))
    as element(ns6:invokeResponse)
{
    &lt;ns6:invokeResponse>

		(:
		 : dane wyjściowe
		 :)
        &lt;ns6:currnecyCashList>{
           	for $i in 1 to count($fML321/ns1:NF_CURRCA_AMOUNT)
           	return
                &lt;ns0:CurrencyCash>
                    &lt;ns0:amount?>{
						data($fML321/ns1:NF_CURRCA_AMOUNT[$i])
					}&lt;/ns0:amount>

                    &lt;ns0:currency?>
                        &lt;ns3:CurrencyCode?>
                            &lt;ns3:currencyCode?>{
								data($fML321/ns1:NF_CURREC_CURRENCYCODE[$i])
							}&lt;/ns3:currencyCode>
                        &lt;/ns3:CurrencyCode>
                    &lt;/ns0:currency>

                    &lt;ns0:userTxnSession?>
                        &lt;ns4:UserTxnSession?>

							(:
							 : status stanowiska
							 :
							 : 0 --> C (zamknięte)
							 : 1 --> O (otwarte)
							 :)
							{
								let $sessionStatus := $fML321/ns1:NF_USETSS_USERTXNSESSIONST[$i]
								return
									if (data($sessionStatus)) then
										&lt;ns4:sessionStatus>
											&lt;ns2:UserTxnSessionStatus>
												&lt;ns2:userTxnSessionStatus>{
													if (data($sessionStatus) eq '0') then
														'C'
													else
														'O'
												}&lt;/ns2:userTxnSessionStatus>
											&lt;/ns2:UserTxnSessionStatus>
										&lt;/ns4:sessionStatus>
									else ()
							}

                            &lt;ns4:user?>
                                &lt;ns5:User?>
                                    &lt;ns5:userLastName?>{
										data($fML321/ns1:NF_USER_USERLASTNAME[$i])
									}&lt;/ns5:userLastName>

                                    &lt;ns5:userID?>{
										data($fML321/ns1:NF_USER_USERID[$i])
									}&lt;/ns5:userID>
									
                                    &lt;ns5:userFirstName?>{
										data($fML321/ns1:NF_USER_USERFIRSTNAME[$i])
									}&lt;/ns5:userFirstName>
                                &lt;/ns5:User>
                            &lt;/ns4:user>

                            &lt;ns4:till?>
                                &lt;ns4:Till?>
                                    &lt;ns4:tillID?>{
										data($fML321/ns1:NF_TILL_TILLID[$i])
									}&lt;/ns4:tillID>
                                &lt;/ns4:Till>
                            &lt;/ns4:till>

                            &lt;ns4:teller?>
                                &lt;ns4:Teller?>
                                    &lt;ns4:tellerID?>{
										data($fML321/ns1:NF_TELLER_TELLERID[$i])
									}&lt;/ns4:tellerID>
                                &lt;/ns4:Teller>
                            &lt;/ns4:teller>
                        &lt;/ns4:UserTxnSession>
                    &lt;/ns0:userTxnSession>
				&lt;/ns0:CurrencyCash>
		}&lt;/ns6:currnecyCashList>
        
        (:
         : dane do stronicowania
         :)
        &lt;ns6:bcd>
            &lt;ns5:BusinessControlData>
                &lt;ns5:pageControl?>
                    &lt;ns7:PageControl?>
						&lt;ns7:hasNext?>{
							xs:boolean(data($fML321/ns1:NF_PAGEC_HASNEXT))
						}&lt;/ns7:hasNext>

                        &lt;ns7:navigationKeyDefinition?>{
							data($fML321/ns1:NF_PAGEC_NAVIGATIONKEYDEFI)
						}&lt;/ns7:navigationKeyDefinition>
						
                        &lt;ns7:navigationKeyValue?>{
							data($fML321/ns1:NF_PAGEC_NAVIGATIONKEYVALU)
						}&lt;/ns7:navigationKeyValue>
                    &lt;/ns7:PageControl>
                &lt;/ns5:pageControl>
            &lt;/ns5:BusinessControlData>
        &lt;/ns6:bcd>
    &lt;/ns6:invokeResponse>
};

&lt;soap-env:Body>{
	xf:getCashInTillsResponse($fML321)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>