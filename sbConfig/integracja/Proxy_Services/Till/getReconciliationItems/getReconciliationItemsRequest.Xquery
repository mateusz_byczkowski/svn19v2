<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.5
 : @since   2010-02-01
 :
 : wersja WSDLa: 05-10-2009 13:19:46
 :
 : $Proxy Services/Till/getReconciliationItems/getReconciliationItemsRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getReconciliationItems/getReconciliationItemsRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:cash.operations.entities.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns7 = "urn:baseauxentities.be.dcl";

declare variable $header1 as element(ns6:header) external;
declare variable $invoke1 as element(ns6:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:getReconciliationItemsRequest($header1 as element(ns6:header),
													$invoke1 as element(ns6:invoke))
    as element(ns0:FML32)
{
        &lt;ns0:FML32>
        
        	(:
        	 : dane z nagłówka
        	 :)
        	&lt;ns0:NF_MSHEAD_MSGID?>{
				data($header1/ns6:msgHeader/ns6:msgId)
			}&lt;/ns0:NF_MSHEAD_MSGID>
			
			(:
			 : dane wejściowe
			 :)
            &lt;ns0:NF_INTEGH_VALUE?>{
				data($invoke1/ns6:scope/ns7:IntegerHolder/ns7:value)
			}&lt;/ns0:NF_INTEGH_VALUE>
			
			(:
			 : stronicowanie
			 :)
            &lt;ns0:NF_PAGEC_ACTIONCODE?>{
				data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:actionCode)
			}&lt;/ns0:NF_PAGEC_ACTIONCODE>
			
            &lt;ns0:NF_PAGEC_PAGESIZE?>{
				data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:pageSize)
			}&lt;/ns0:NF_PAGEC_PAGESIZE>
			
			(:
			 : kolejność stronicowania
			 : true  --> 1
			 : false --> 0
			 :)
            {
            	let $reverseOrder := $invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:reverseOrder
            	return
	            	if (data($reverseOrder)) then
		   	            &lt;ns0:NF_PAGEC_REVERSEORDER>{
		            		if (data($reverseOrder) eq "true") then
	    	        			1
							else
								0
	    	        	}&lt;/ns0:NF_PAGEC_REVERSEORDER>
	        		else ()
			}
			
            &lt;ns0:NF_PAGEC_NAVIGATIONKEYDEFI?>{
				data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:navigationKeyDefinition)
			}&lt;/ns0:NF_PAGEC_NAVIGATIONKEYDEFI>
			
            &lt;ns0:NF_PAGEC_NAVIGATIONKEYVALU?>{
				data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:navigationKeyValue)
			}&lt;/ns0:NF_PAGEC_NAVIGATIONKEYVALU>
            
            (:
             : dane wejściowe - cd.
             :)
            &lt;ns0:NF_BRANCC_BRANCHCODE?>{
				data($invoke1/ns6:branchCode/ns3:BranchCode/ns3:branchCode)
			}&lt;/ns0:NF_BRANCC_BRANCHCODE>
			
            &lt;ns0:NF_USERTS_SESSIONDATE?>{
				data($invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:sessionDate)
			}&lt;/ns0:NF_USERTS_SESSIONDATE>
			
            {
            	for $i in 1 to count($invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:currencyReconciliationList/ns2:CurrencyReconciliation)
            	return
            	(
					let $currencyCode := $invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:currencyReconciliationList/ns2:CurrencyReconciliation[$i]/ns2:currency/ns3:CurrencyCode/ns3:currencyCode
					return
						if (data($currencyCode)) then
							&lt;ns0:NF_CURREC_CURRENCYCODE>{
		            			data($currencyCode)
		            		}&lt;/ns0:NF_CURREC_CURRENCYCODE>
		            	else ()
		            ,
		            let $reconStatus := $invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:currencyReconciliationList/ns2:CurrencyReconciliation[$i]/ns2:reconciliationStatus/ns1:CurrencyReconcStatus/ns1:currencyReconcStatus
		            return
		            	if (data($reconStatus)) then
		            		&lt;ns0:NF_CURRRS_CURRENCYRECONCST>{
		            			data($reconStatus)
    		        		}&lt;/ns0:NF_CURRRS_CURRENCYRECONCST>
    		        	else ()
            	)
            }
            
            &lt;ns0:NF_TILL_TILLID?>{
				data($invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:till/ns4:Till/ns4:tillID)
			}&lt;/ns0:NF_TILL_TILLID>
			
        &lt;/ns0:FML32>
};

&lt;soap-env:Body>{
	xf:getReconciliationItemsRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>