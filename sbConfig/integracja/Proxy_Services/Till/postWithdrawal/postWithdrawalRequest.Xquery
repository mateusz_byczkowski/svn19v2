<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns1:header" location="postWithdrawal.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns1:invoke" location="postWithdrawal.wsdl" ::)
(:: pragma bea:global-element-return element="ns7:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

xquery version "1.0";

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/postWithdrawal/postWithdrawalRequest/";
declare namespace ns0 = "urn:fee.operations.entities.be.dcl";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns3 = "urn:feedict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns6 = "urn:acceptance.entities.be.dcl";
declare namespace ns7 = "";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace ns9 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns10 = "urn:entities.be.dcl";
declare namespace ns11="urn:transactionfeedict.operationsdictionary.dictionaries.be.dcl";

declare variable $header1 as element(ns1:header) external;
declare variable $invoke1 as element(ns1:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:postWithdrawalRequest($header1 as element(ns1:header),
											$invoke1 as element(ns1:invoke))
    as element(ns7:FML32)
{
	&lt;ns7:FML32>
		&lt;ns7:TR_ID_OPER?>{
			data($header1/ns1:transHeader/ns1:transId)
		}&lt;/ns7:TR_ID_OPER>

		{
			let $transactionDate := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionDate)
			return
				if ($transactionDate) then
					&lt;ns7:TR_DATA_OPER?>{
						fn:concat(fn:substring($transactionDate, 9, 2),
                        		  '-',
								  fn:substring($transactionDate, 6, 2),
								  '-',
								  fn:substring($transactionDate, 1, 4))
					}&lt;/ns7:TR_DATA_OPER>
				else
					()
		}

        &lt;ns7:TR_ODDZ_KASY?>{
			data($invoke1/ns1:branchCode/ns4:BranchCode/ns4:branchCode)
		}&lt;/ns7:TR_ODDZ_KASY>

		&lt;ns7:TR_KASA?>{
			data($invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:till/ns2:Till/ns2:tillID)
		}&lt;/ns7:TR_KASA>		

		&lt;ns7:TR_KASJER?>{
			data($invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:teller/ns2:Teller/ns2:tellerID)
		}&lt;/ns7:TR_KASJER>

		{
			let $userID := data($invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:user/ns10:User/ns10:userID)
			return
				if ($userID) then
					&lt;ns7:TR_UZYTKOWNIK?>{
						fn:concat('SKP:', $userID)
					}&lt;/ns7:TR_UZYTKOWNIK>
				else
					()
		}

		&lt;ns7:TR_ID_GR_OPER?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:cashTransactionBasketID)
		}&lt;/ns7:TR_ID_GR_OPER>

		&lt;ns7:TR_MSG_ID?>{
			data($header1/ns1:msgHeader/ns1:msgId)
		}&lt;/ns7:TR_MSG_ID>

     	&lt;ns7:TR_FLAGA_AKCEPT?>
                {
                
                    if (fn:boolean(data($invoke1/ns1:transaction/ns8:Transaction/ns8:acceeptanceForBe/ns8:AcceptanceForBE/ns8:flag)) = fn:boolean("true")) then
                        (data($invoke1/ns1:transaction/ns8:Transaction/ns8:acceeptanceForBe/ns8:AcceptanceForBE/ns8:flag))
                    else 
                        'T'
                }
		&lt;/ns7:TR_FLAGA_AKCEPT>
            
		{
			let $acceptor := data($invoke1/ns1:acceptTask/ns6:AcceptTask/ns6:acceptor)
			return
				if ($acceptor) then
					&lt;ns7:TR_AKCEPTANT?>{
						fn:concat('SKP:', $acceptor)
					}&lt;/ns7:TR_AKCEPTANT>
				else
					()
		}
            
		&lt;ns7:TR_TYP_KOM?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType)
		}&lt;/ns7:TR_TYP_KOM>

		&lt;ns7:TR_KANAL?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:extendedCSRMessageType/ns5:ExtendedCSRMessageType/ns5:extendedCSRMessageType)
		}&lt;/ns7:TR_KANAL>

          &lt;ns7:TR_CZAS_OPER?>
                {
                    let $transactionTime  := ($header1/ns1:msgHeader/ns1:timestamp)
  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
				&lt;/ns7:TR_CZAS_OPER>
            &lt;TR_RACH_POWIAZANY?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionGLEsp/ns8:TransactionGLEsp/ns8:accountNbr) }&lt;/TR_RACH_POWIAZANY>

		&lt;ns7:TR_RACH_NAD?>{ 
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:accountNumber)
		}&lt;/ns7:TR_RACH_NAD>

		&lt;ns7:TR_KWOTA_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:amountWn)
		}&lt;/ns7:TR_KWOTA_NAD>

		&lt;ns7:TR_WALUTA_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:currencyCode/ns4:CurrencyCode/ns4:currencyCode)
		}&lt;/ns7:TR_WALUTA_NAD>

		&lt;ns7:TR_KURS_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:rate)
		}&lt;/ns7:TR_KURS_NAD>


			{			
			let $name :=data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:name)
			let $nameSecond  := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:nameSecond)
			return
				if ($name
				or  $nameSecond) then
					&lt;ns7:TR_NAZWA_NAD?>{
						fn-bea:trim-right(fn:concat($name," ", $nameSecond))
					}&lt;/ns7:TR_NAZWA_NAD>			
				else
					()
				}

			

		&lt;ns7:TR_MIEJSCOWOSC_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:city)
		}&lt;/ns7:TR_MIEJSCOWOSC_NAD>

		&lt;ns7:TR_ULICA_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:address)
		}&lt;/ns7:TR_ULICA_NAD>

		&lt;ns7:TR_KOD_POCZT_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:zipCode)			
		}&lt;/ns7:TR_KOD_POCZT_NAD>

		&lt;ns7:TR_ROWN_PLN_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:amountWnEquiv)
		}&lt;/ns7:TR_ROWN_PLN_NAD>

		&lt;ns7:TR_RACH_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:accountNumber)
		}&lt;/ns7:TR_RACH_ADR>

		&lt;ns7:TR_KWOTA_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:amountMa)
		}&lt;/ns7:TR_KWOTA_ADR>

		&lt;ns7:TR_WALUTA_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:currencyCode/ns4:CurrencyCode/ns4:currencyCode)
		}&lt;/ns7:TR_WALUTA_ADR>

		&lt;ns7:TR_KURS_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:rate)
		}&lt;/ns7:TR_KURS_ADR>



			{			
			let $name :=data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:name)
			let $nameSecond  := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:nameSecond)
			return
				if ($name
				or  $nameSecond) then
					&lt;ns7:TR_NAZWA_ADR?>{
						fn-bea:trim-right(fn:concat($name," ", $nameSecond))
					}&lt;/ns7:TR_NAZWA_ADR>			
				else
					()
		 }


		&lt;ns7:TR_MIEJSCOWOSC_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:city)
		}&lt;/ns7:TR_MIEJSCOWOSC_ADR>

		&lt;ns7:TR_ULICA_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:address)
		}&lt;/ns7:TR_ULICA_ADR>

		&lt;ns7:TR_KOD_POCZT_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:zipCode)
		}&lt;/ns7:TR_KOD_POCZT_ADR>

		&lt;ns7:TR_ROWN_PLN_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:amountMaEquiv)
		}&lt;/ns7:TR_ROWN_PLN_ADR>

		&lt;ns7:TR_TYTUL?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:title)
		}&lt;/ns7:TR_TYTUL>
    
    	{
			let $transactionDate := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionDate)
			return
				if ($transactionDate) then
		            &lt;ns7:TR_DATA_WALUTY?>{
		            	fn:concat(fn:substring($transactionDate, 9, 2),
		            			  '-',
								  fn:substring($transactionDate, 6, 2),
								  '-',
								  fn:substring($transactionDate, 1, 4))
		            }&lt;/ns7:TR_DATA_WALUTY>
				else
					()
    	}
    
      {
                for  $TransactionFeeField in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionFee/ns0:TransactionFee/ns0:transactionFeeFieldList/ns0:TransactionFeeField
                return
				(	
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_1")
                 then(
                    &lt;ns7:TR_OPLATA_1?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_1>)
                   else()
                   ,
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_2")
                 then(
                    &lt;ns7:TR_OPLATA_2?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_2>)
                  else()
                   ,
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_3")
                 then(
                    &lt;ns7:TR_OPLATA_3?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_3>)
                  else()
                   ,
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_4")
                 then(
                    &lt;ns7:TR_OPLATA_4?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_4>)
                  else()
                   ,
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_5")
                 then(
                    &lt;ns7:TR_OPLATA_5?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_5>)
                  else(),
                 if (data($TransactionFeeField/ns0:feeField) eq "FEE_6")
                  then(
                    &lt;ns7:TR_OPLATA_6?>{data($TransactionFeeField/ns0:fieldFeeAmount)}&lt;/ns7:TR_OPLATA_6>)
                  else()
                )
             }	
    {
    for  $Disposer in ($invoke1/ns1:transaction/ns8:Transaction/ns8:disposerList/ns8:Disposer)
       let $firstName := data($Disposer/ns8:firstName)
	  let $lastName  := data($Disposer/ns8:lastName)
			
        return
        (
			   &lt;ns7:TR_DYSP_CIF?>{
						data($Disposer/ns8:cif)
					}&lt;/ns7:TR_DYSP_CIF>,

			
				if ($firstName
				or  $lastName) then
					&lt;ns7:TR_DYSP_NAZWA?>{
						fn-bea:trim-right(fn:concat($firstName," ", $lastName))
					}&lt;/ns7:TR_DYSP_NAZWA>			
				else
					()
		,				
		&lt;ns7:TR_DYSP_OBYWATELSTWO?>{
			data($Disposer/ns8:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode)
		}&lt;/ns7:TR_DYSP_OBYWATELSTWO>
	,
		&lt;ns7:TR_DYSP_KRAJ?>{
			data($Disposer/ns8:countryCode/ns4:CountryCode/ns4:countryCode)
		}&lt;/ns7:TR_DYSP_KRAJ>
	,
		&lt;ns7:TR_DYSP_MIEJSCOWOSC?>{
			data($Disposer/ns8:city)
		}&lt;/ns7:TR_DYSP_MIEJSCOWOSC>,
	
		&lt;ns7:TR_DYSP_KOD_POCZT?>{
			data($Disposer/ns8:zipCode)
		}&lt;/ns7:TR_DYSP_KOD_POCZT>,
	
		&lt;ns7:TR_DYSP_ULICA?>{
			data($Disposer/ns8:address)
		}&lt;/ns7:TR_DYSP_ULICA>,
	
		&lt;ns7:TR_DYSP_PESEL?>{
			data($Disposer/ns8:pesel)
		}&lt;/ns7:TR_DYSP_PESEL>,
	
		&lt;ns7:TR_DYSP_NR_PASZPORTU?>{
			data($Disposer/ns8:documentNumber)
		}&lt;/ns7:TR_DYSP_NR_PASZPORTU>,

                for $documentType in $Disposer/ns8:documentType/ns5:DocumentTypeForTxn
                return
                (
                    &lt;TR_DYSP_NR_DOWOD?>{ data($documentType/ns5:documentCodeForGIIF) }&lt;/TR_DYSP_NR_DOWOD>,
                    &lt;TR_DYSP_TELEFON?>{ data($documentType/ns5:commentForGIIF) }&lt;/TR_DYSP_TELEFON>
                )  
	 )
    }
    &lt;ns7:TR_AKCEPTANT_SKP?>{ data($invoke1/ns0:acceptTask/ns6:AcceptTask/ns6:acceptor) }&lt;/ns7:TR_AKCEPTANT_SKP>
    &lt;ns7:TR_UZYTKOWNIK_SKP?>{ data($invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:user/ns10:User/ns10:userID) }&lt;/ns7:TR_UZYTKOWNIK_SKP>
 

	    &lt;/ns7:FML32>
};

&lt;soap-env:Body>{
	xf:postWithdrawalRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>