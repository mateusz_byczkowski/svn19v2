<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/postWithdrawal/postWithdrawalResponse/";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns4 = "urn:operations.entities.be.dcl";

declare variable $fML32OUT1 as element(ns1:FML32) external;

(:~
 : @param FML32 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:postWithdrawalResponse($fML32OUT1 as element(ns1:FML32))
    as element(ns0:invokeResponse)
{
	&lt;ns0:invokeResponse>
		&lt;ns0:transactionOut>
			&lt;ns4:Transaction?>
				&lt;ns4:transactionStatus?>
					&lt;ns2:TransactionStatus?>
						&lt;ns2:transactionStatus?>{
							data($fML32OUT1/ns1:TR_STATUS)
						}&lt;/ns2:transactionStatus>
					&lt;/ns2:TransactionStatus>
				&lt;/ns4:transactionStatus>
			&lt;/ns4:Transaction>
		&lt;/ns0:transactionOut>

		&lt;ns0:backendResponse>
			&lt;ns4:BackendResponse>
				{
					let $transactionDate := data($fML32OUT1/ns1:TR_DATA_KSIEG)
					return
						if ($transactionDate) then 
							&lt;ns4:icbsDate>{
								fn:concat(fn:substring($transactionDate, 7, 4),
										  '-',
										  fn:substring($transactionDate, 4, 2),
										  '-',
										  fn:substring($transactionDate, 1, 2))
							}&lt;/ns4:icbsDate>
						else
							()
				}

				&lt;ns4:icbsSessionNumber?>{
					data($fML32OUT1/ns1:TR_TXN_SESJA)
				}&lt;/ns4:icbsSessionNumber>

				&lt;ns4:psTransactionNumber?>{
					data($fML32OUT1/ns1:TR_TXN_NR)
				}&lt;/ns4:psTransactionNumber>

				{
					let $transactionRefNumber := data($fML32OUT1/ns1:TR_NR_REF)
					return
						if ($transactionRefNumber) then 
							&lt;ns4:transactionRefNumber>{
								$transactionRefNumber
							}&lt;/ns4:transactionRefNumber> 
	                    else
							()
				}

				(:
				 : brak mapowania dla 'balanceDebit'
				 : brak mapowania dla 'balanceCredit'
				 :)

				{
					let $dateTime := data($fML32OUT1/ns1:TR_CZAS_ODPOWIEDZI)
					return
						if ($dateTime) then 
		                    &lt;ns4:dateTime>{  
								fn:concat(fn:substring($dateTime, 1, 10),
										  'T',
										  fn:substring($dateTime, 12, 15))
								}&lt;/ns4:dateTime>
						else
							()
				}

				(:
				 : brak mapowania dla 'beUserId'
				 :)
                &lt;ns4:beUserId?>{ data($fML32OUT1/ns1:TR_UZYTKOWNIK) }&lt;/ns4:beUserId>

				&lt;ns4:beErrorCodeList?>{
					for $FML320 in ($fML32OUT1/ns1:TR_KOD_BLEDU_1
									 union $fML32OUT1/ns1:TR_KOD_BLEDU_2
									 union $fML32OUT1/ns1:TR_KOD_BLEDU_3
									 union $fML32OUT1/ns1:TR_KOD_BLEDU_4
									 union $fML32OUT1/ns1:TR_KOD_BLEDU_5)  
					return
						let $errorCode := data($FML320)
						return
							if ($errorCode ne ''
								and $errorCode ne '000') then
								&lt;ns4:BeErrorCode>
									&lt;ns4:errorCode>
										&lt;ns3:BackendErrorCode>
											&lt;ns3:errorCode>{
												data($FML320)
											}&lt;/ns3:errorCode>
										&lt;/ns3:BackendErrorCode>
									&lt;/ns4:errorCode>
								&lt;/ns4:BeErrorCode>
							else
								()
				}&lt;/ns4:beErrorCodeList>
			&lt;/ns4:BackendResponse>
		&lt;/ns0:backendResponse>
	&lt;/ns0:invokeResponse>
};

&lt;soap-env:Body>{
	xf:postWithdrawalResponse($fML32OUT1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>