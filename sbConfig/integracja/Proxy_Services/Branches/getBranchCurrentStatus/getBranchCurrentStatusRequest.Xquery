<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.3
 : @since   2010-02-18
 :
 : wersja WSDLa: 18-02-2010 11:17:01
 :
 : $Proxy Services/Branches/getBranchCurrentStatus/getBranchCurrentStatusRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://sygnity.pl/ThirdStage/branch/getBranchCurrentStatusRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $header1 as element(ns2:header) external;
declare variable $invoke1 as element(ns2:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:getBranchCurrentStatusRequest($header1 as element(ns2:header),
													$invoke1 as element(ns2:invoke))
    as element(ns0:FML32)
{
	&lt;ns0:FML32>
	
		(:
		 : nagłówek komunikatu
		 :)
		&lt;ns0:NF_MSHEAD_MSGID?>{
			data($header1/ns2:msgHeader/ns2:msgId)
		}&lt;/ns0:NF_MSHEAD_MSGID>

		(:
		 : dane wejściowe
		 :)
		&lt;ns0:NF_BRANCC_BRANCHCODE?>{
			data($invoke1/ns2:branchCode/ns1:BranchCode/ns1:branchCode)
		}&lt;/ns0:NF_BRANCC_BRANCHCODE>
		
	&lt;/ns0:FML32>
};

&lt;soap-env:Body>{
	xf:getBranchCurrentStatusRequest($header1, $invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>