<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$fML321" element="ns2:FML32" location="getCashInBranchResponse.xsd" ::)
(:: pragma bea:global-element-return element="ns3:invokeResponse" location="getCashInBranch.WSDL" ::)

declare namespace xf = "http://tempuri.org/branch/getCashInBranch/getCashInBranchResponse/";
declare namespace ns0 = "urn:cash.operations.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns2 = "";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:getCashInBranchResponse($fML321 as element(ns2:FML32))
    as element(ns3:invokeResponse) {
        &lt;ns3:invokeResponse>
            &lt;ns3:currencyCash>
            {
            	for $i in 1 to count($fML321/ns2:NF_CURRCA_AMOUNT)
            	return
	                &lt;ns0:CurrencyCash>
	                    &lt;ns0:amount>{ xs:double( data($fML321/ns2:NF_CURRCA_AMOUNT[$i]) ) }&lt;/ns0:amount>
	                    {
	                    	if (data($fML321/ns2:NF_CURREC_CURRENCYCODE[$i])) then
			                    &lt;ns0:currency?>
			                        &lt;ns1:CurrencyCode?>
			                            &lt;ns1:currencyCode?>{ data($fML321/ns2:NF_CURREC_CURRENCYCODE[$i]) }&lt;/ns1:currencyCode>
			                        &lt;/ns1:CurrencyCode>
			                    &lt;/ns0:currency>
			                else ()	                    	
	                    }
	                &lt;/ns0:CurrencyCash>           	
            }
            &lt;/ns3:currencyCash>
        &lt;/ns3:invokeResponse>
};

declare variable $fML321 as element(ns2:FML32) external;

&lt;soap-env:Body>
{ xf:getCashInBranchResponse($fML321) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>