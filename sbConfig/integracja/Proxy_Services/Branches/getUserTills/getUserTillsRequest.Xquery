<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:~
 : @author  Michal Fijas
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns="http://bzwbk.com/services/getUserTills";

declare variable $body as element(soap-env:Body) external;

(:~
 : @param $invoke operacja wejśiowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function local:getUserTillsRequest($invoke as element(ns:invoke))
    as element(FML32)
{
	&lt;FML32>
    	&lt;NF_MSHEAD_MSGID?>{ data($invoke/msgId) }&lt;/NF_MSHEAD_MSGID>
		
		(: dane do stronicowania :)
        &lt;NF_PAGEC_ACTIONCODE?>{ data($invoke/pageControl/actionCode) }&lt;/NF_PAGEC_ACTIONCODE>
        &lt;NF_PAGEC_PAGESIZE?>{ data($invoke/pageControl/pageSize) }&lt;/NF_PAGEC_PAGESIZE>
        {
        	let $reverseOrder := data($invoke/pageControl/reverseOrder)
	        return 
            	if ($reverseOrder) then
		            &lt;NF_PAGEC_REVERSEORDER>{			            	
		            	if ($reverseOrder eq "true") then
		            		'1'
		            	else
		            		'0'
		            }&lt;/NF_PAGEC_REVERSEORDER>
				else ()
		}
        &lt;NF_PAGEC_NAVIGATIONKEYDEFI?>{ data($invoke/pageControl/navigationKeyDefinition) }&lt;/NF_PAGEC_NAVIGATIONKEYDEFI>
        &lt;NF_PAGEC_NAVIGATIONKEYVALU?>{ data($invoke/pageControl/navigationKeyValue) }&lt;/NF_PAGEC_NAVIGATIONKEYVALU>
		
		(: dane filtrujące :)
		&lt;NF_APPF_SKP?>{ data($invoke/skp) }&lt;/NF_APPF_SKP>
        &lt;NF_BRANCC_BRANCHCODE?>{ data($invoke/branchCode) }&lt;/NF_BRANCC_BRANCHCODE>
		(:
		 : status stanowiska
		 :
		 : O --> 1 (otwarte)
		 : C --> 0 (zamknięte)
		 :)
        {
			let $sessionStatus := data($invoke/userTxnSessionStatus)
			return
	           	if ($sessionStatus) then
					&lt;NF_USETSS_USERTXNSESSIONST>{
						if ($sessionStatus eq 'O') then
							1
						else
							0
					}&lt;/NF_USETSS_USERTXNSESSIONST>
            	else ()
        }
        &lt;NF_TILL_TILLID?>{ data($invoke/tillID) }&lt;/NF_TILL_TILLID>
    &lt;/FML32>
};

&lt;soap-env:Body>{ local:getUserTillsRequest($body/ns:invoke) }&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>