<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:dcl:services.alsb.datamodel";

declare variable $body as element(soap:Body) external;

declare function prepareSimulationForTimeAccountResponse($res as element(xpcml))
               as element(urn:invokeResponse){
    let $resstruct:=$res/program/parameterList/structParm[1]

    let $balance:=$resstruct/zonedDecimalParm[1]
    let $interestCalculate:=$resstruct/zonedDecimalParm[2]
    let $interestLow:=$resstruct/zonedDecimalParm[3]
    let $interestForPay:=$resstruct/zonedDecimalParm[4]
    let $interestPayed:=$resstruct/zonedDecimalParm[5]
    let $amountForPay:=$resstruct/zonedDecimalParm[6]
    let $tax:=$resstruct/zonedDecimalParm[7]
    let $commission:=$resstruct/zonedDecimalParm[8]
    let $interestCapitalized:=$resstruct/zonedDecimalParm[9]
    let $taxPayed:=$resstruct/zonedDecimalParm[10]

    return
     &lt;urn:invokeResponse>
        &lt;urn:simulation>
            &lt;urn:entities.filtersandmessages.SimulationForTimeAccount>
		&lt;urn:balance>{data($balance)}&lt;/urn:balance>
		&lt;urn:interestCalculate>{data($interestCalculate)}&lt;/urn:interestCalculate>
		&lt;urn:interestLow>{data($interestLow)}&lt;/urn:interestLow>
		&lt;urn:interestForPay>{data($interestForPay)}&lt;/urn:interestForPay>
		&lt;urn:interestPayed>{data($interestPayed)}&lt;/urn:interestPayed>
		&lt;urn:amountForPay>{data($amountForPay)}&lt;/urn:amountForPay>
		&lt;urn:tax>{data($tax)}&lt;/urn:tax>
		&lt;urn:commission>{data($commission)}&lt;/urn:commission>
		&lt;urn:interestCapitalized>{data($interestCapitalized)}&lt;/urn:interestCapitalized>
		&lt;urn:taxPayed>{data($taxPayed)}&lt;/urn:taxPayed>
            &lt;/urn:entities.filtersandmessages.SimulationForTimeAccount>
         &lt;/urn:simulation>       
     &lt;/urn:invokeResponse>
};


&lt;soap:Body>
{prepareSimulationForTimeAccountResponse($body/xpcml)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>