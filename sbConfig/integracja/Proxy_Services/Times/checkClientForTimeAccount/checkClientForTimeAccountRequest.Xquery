<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace urn="urn:dcl:services.alsb.datamodel";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap:Body) external;

declare function prepareCheckClientForTimeAccountRequest( $req as element(urn:entities.cif.Customer))
               as element(xpcml){
   let $cif := $req/urn:customerNumber

  return 
&lt;xpcml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="xpcml.xsd" version="4.0">

&lt;program name="program" path="/QSYS.LIB/AIBINTMRST.LIB/ATA0303.PGM"> 
&lt;parameterList>
      &lt;stringParm name="PSCIF" passDirection="in" length="10">{data($cif)}&lt;/stringParm>
      &lt;zonedDecimalParm name="PZRETCODE" passDirection="inout" totalDigits="4" fractionDigits="0">2&lt;/zonedDecimalParm>
      &lt;stringParm name="PNRETVAL" passDirection="inout" length="1">0&lt;/stringParm>
   &lt;/parameterList> 
&lt;/program>

&lt;/xpcml>
};

&lt;soap:Body>
{prepareCheckClientForTimeAccountRequest($body/urn:invoke/urn:customer/urn:entities.cif.Customer)}
&lt;/soap:Body></con:xquery>
</con:xqueryEntry>