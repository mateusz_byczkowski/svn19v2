<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn="urn:be.services.dcl";
declare namespace ns4="urn:filtersandmessages.entities.be.dcl";


declare variable $body as element(soap-env:Body) external;

declare function xf:mapChgTimeAccountResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
    let $rescode:=$fml/fml:DC_OPIS_BLEDU

    return
	
&lt;urn:invokeResponse>
   &lt;urn:response>
     &lt;ns4:ResponseMessage>
      {
	if (data($rescode)) then 
		    &lt;ns4:result>false&lt;/ns4:result>
	else 
		    &lt;ns4:result>true&lt;/ns4:result>
      }
     &lt;/ns4:ResponseMessage>
   &lt;/urn:response>       
&lt;/urn:invokeResponse>	
		

};


&lt;soap-env:Body>
{ xf:mapChgTimeAccountResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>