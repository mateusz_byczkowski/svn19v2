<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:schema>&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;schema targetNamespace="http://www.bea.com/wli/sb/context"
        xmlns:mc="http://www.bea.com/wli/sb/context"
        xmlns="http://www.w3.org/2001/XMLSchema"
        elementFormDefault="qualified"
        attributeFormDefault="unqualified">
    &lt;!--============================================================== -->

    &lt;!-- The context variable 'fault' is an instance of this element -->
    &lt;element name="fault" type="mc:FaultType"/>

    &lt;!-- The context variables 'inbound' and 'outbound' are instances of this element -->
    &lt;element name="endpoint" type="mc:EndpointType"/>

    &lt;!-- The three sub-elements within the 'inbound' and 'outbound' variables -->
    &lt;element name="service" type="mc:ServiceType"/>
    &lt;element name="transport" type="mc:TransportType"/>
    &lt;element name="security" type="mc:SecurityType"/>

    &lt;!-- The context variable 'attachments' is an instance of this element -->
    &lt;element name="attachments" type="mc:AttachmentsType"/>

    &lt;!-- Each attachment in the 'attachments' variable is represented by an instance of this element -->
    &lt;element name="attachment" type="mc:AttachmentType"/>

    &lt;!-- Element used to represent binary payloads and pass-by reference content -->
    &lt;element name="binary-content" type="mc:BinaryContentType"/>

    &lt;!-- =================================================================== -->

    &lt;!-- The schema type for  -->
    &lt;complexType name="AttachmentsType">
        &lt;sequence>
            &lt;!-- the 'attachments' variable is just a series of attachment elements -->
            &lt;element ref="mc:attachment" minOccurs="0" maxOccurs="unbounded"/>
        &lt;/sequence>
    &lt;/complexType>
    
    &lt;complexType name="AttachmentType">
        &lt;all>
            &lt;!-- Set of MIME headers associated with attachment -->
            &lt;element name="Content-ID" type="string" minOccurs="0"/>
            &lt;element name="Content-Type" type="string" minOccurs="0"/>
            &lt;element name="Content-Transfer-Encoding" type="string" minOccurs="0"/>
            &lt;element name="Content-Description" type="string" minOccurs="0"/>
            &lt;element name="Content-Location" type="string" minOccurs="0"/>
            &lt;element name="Content-Disposition" type="string" minOccurs="0"/>

            &lt;!-- Contains the attachment content itself, either in-lined or as &lt;binary-content/> -->
            &lt;element name="body" type="anyType"/>
        &lt;/all>
    &lt;/complexType>

    &lt;complexType name="BinaryContentType">
        &lt;!-- URI reference to the binary or pass-by-reference payload -->
        &lt;attribute name="ref" type="anyURI" use="required"/>
    &lt;/complexType>

    &lt;!-- =================================================================== -->

    &lt;complexType name="EndpointType">
        &lt;all>
            &lt;!-- Sub-elements holding service, transport, and security details for the endpoint -->
            &lt;element ref="mc:service" minOccurs="0" />
            &lt;element ref="mc:transport" minOccurs="0" />
            &lt;element ref="mc:security" minOccurs="0" />
        &lt;/all>

        &lt;!-- Fully-qualified name of the service represented by this endpoint -->
        &lt;attribute name="name" type="string" use="required"/>
    &lt;/complexType>

    &lt;!-- =================================================================== -->

    &lt;complexType name="ServiceType">
        &lt;all>
            &lt;!-- name of service provider -->
            &lt;element name="providerName" type="string" minOccurs="0"/>

            &lt;!-- the service operation being invoked -->
            &lt;element name="operation" type="string" minOccurs="0"/>
        &lt;/all>
    &lt;/complexType>

    &lt;!-- =================================================================== -->

    &lt;complexType name="TransportType">
        &lt;all>
            &lt;!-- URI of endpoint -->
            &lt;element name="uri" type="anyURI" minOccurs="0" />

            &lt;!-- Transport-specific metadata for request and response (includes transport headers) -->
            &lt;element name="request" type="anyType" minOccurs="0"/>
            &lt;element name="response" type="anyType" minOccurs="0" />

            &lt;!-- Indicates one-way (request only) or bi-directional (request/response) communication -->
            &lt;element name="mode" type="mc:ModeType" minOccurs="0" />

            &lt;!-- Specifies the quality of service -->
            &lt;element name="qualityOfService" type="mc:QoSType" minOccurs="0" />

            &lt;!-- Retry values (outbound only) -->
            &lt;element name="retryInterval" type="integer" minOccurs="0" />
            &lt;element name="retryCount" type="integer" minOccurs="0" />
        &lt;/all>
    &lt;/complexType>

    &lt;simpleType name="ModeType">
        &lt;restriction base="string">
            &lt;enumeration value="request"/>
            &lt;enumeration value="request-response"/>
        &lt;/restriction>
    &lt;/simpleType>

    &lt;simpleType name="QoSType">
        &lt;restriction base="string">
            &lt;enumeration value="best-effort"/>
            &lt;enumeration value="exactly-once"/>
        &lt;/restriction>
    &lt;/simpleType>

    &lt;!-- =================================================================== -->

    &lt;complexType name="SecurityType">
        &lt;all>
            &lt;!-- Transport-level client information (inbound only) -->
            &lt;element name="transportClient" type="mc:SubjectType" minOccurs="0"/>

            &lt;!-- Message-level client information (inbound only) -->
            &lt;element name="messageLevelClient" type="mc:SubjectType" minOccurs="0"/>
            
            &lt;!-- Boolean flag used to disable outbound WSS processing (outbound only) -->
            &lt;element name="doOutboundWss" type="boolean" minOccurs="0"/>
        &lt;/all>
    &lt;/complexType>

    &lt;complexType name="SubjectType">
        &lt;all>
            &lt;!-- User name associated with this tranport- or message-level subject -->
            &lt;element name="username" type="string"/>
        &lt;/all>
    &lt;/complexType>

    &lt;!-- =================================================================== -->

    &lt;complexType name="FaultType">
        &lt;all>
            &lt;!-- A short string identifying the error (e.g. BEA38229) -->
            &lt;element name="errorCode" type="string"/>

            &lt;!-- Descriptive text explaining the reason for the error -->
            &lt;element name="reason" type="string" minOccurs="0" />

            &lt;!-- Any additional details about the error  -->
            &lt;element name="details" type="anyType" minOccurs="0" />

            &lt;!-- Information about where the error occured in the proxy -->
            &lt;element name="location" type="mc:LocationType" minOccurs="0" />
        &lt;/all>
    &lt;/complexType>

    &lt;complexType name="LocationType">
        &lt;all>
            &lt;!-- Name of the Pipeline/Branch/Route node where error occured -->
            &lt;element name="node" type="string" minOccurs="0" />

            &lt;!-- Name of the Pipeline where error occured (if applicable) -->
            &lt;element name="pipeline" type="string" minOccurs="0" />

            &lt;!-- Name of the Stage where error occured (if applicable) -->
            &lt;element name="stage" type="string" minOccurs="0" />

            &lt;!-- Indicates if error occured from inside an error handler -->
            &lt;element name="error-handler" type="boolean" minOccurs="0" />
        &lt;/all>
    &lt;/complexType>
    &lt;!-- Encapsulates any stack-traces that may be added to a fault &lt;details> -->
    &lt;element name="stack-trace" type="string"/>
&lt;/schema></con:schema>
  <con:targetNamespace>http://www.bea.com/wli/sb/context</con:targetNamespace>
</con:schemaEntry>