<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-03-28</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err = "urn:errors.hlbsentities.be.dcl";
declare namespace pdk = "http://bzwbk.com/services/pdk/";

declare function local:response($fault as element(soap-env:Fault)) as element(soap-env:Body) {
    &lt;soap-env:Body>
        &lt;soap-env:Fault>
            &lt;faultcode>Server&lt;/faultcode>
            &lt;faultstring>{ data($fault/faultstring) }&lt;/faultstring>
            &lt;detail>
                &lt;err:ServiceFailException>
                    &lt;err:exceptionItem>
                        &lt;err:errorCode1>-1&lt;/err:errorCode1>
                        &lt;err:errorCode2>0&lt;/err:errorCode2>
                        &lt;err:errorDescription>{ data($fault/faultstring) }&lt;/err:errorDescription>
                    &lt;/err:exceptionItem>
                &lt;/err:ServiceFailException>
            &lt;/detail>
        &lt;/soap-env:Fault>
    &lt;/soap-env:Body>
};

declare variable $body as element(soap-env:Body) external;

local:response($body/soap-env:Fault)</con:xquery>
</con:xqueryEntry>