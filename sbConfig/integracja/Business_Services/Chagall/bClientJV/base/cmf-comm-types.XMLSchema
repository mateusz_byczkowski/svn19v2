<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:schema>&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;!--
        Podstawowe typy do komunikacji.
        Version: 1.004
-->
&lt;xs:schema targetNamespace="http://jv.channel.cu.com.pl/cmf-comm-types" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:cmf-tech="http://jv.channel.cu.com.pl/cmf-technical-types" xmlns:cmf-biz="http://jv.channel.cu.com.pl/cmf-biz-types" xmlns:cmf-product="http://jv.channel.cu.com.pl/cmf-product-types" xmlns:cmf-party="http://jv.channel.cu.com.pl/cmf-party-types" xmlns:cmf-comm="http://jv.channel.cu.com.pl/cmf-comm-types">
   &lt;xs:import namespace="http://jv.channel.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd"/>
   &lt;xs:import namespace="http://jv.channel.cu.com.pl/cmf-product-types" schemaLocation="cmf-product-types.xsd"/>
   &lt;xs:import namespace="http://jv.channel.cu.com.pl/cmf-party-types" schemaLocation="cmf-party-types.xsd"/>
   &lt;xs:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd"/>
	&lt;xs:annotation>
		&lt;xs:documentation>Podstawowe typy do komunikacji.

            CVS log: 
            $Revision: 1.13 $ 
            $Source: /it/krwnuk/cvs_do_migracji/SIS/ESB/CMF/base/cmf-comm-types.xsd,v $ 
            $Author: kadamowi $
            $Date: 2008-08-08 15:17:50 $&lt;/xs:documentation>
	&lt;/xs:annotation>

	&lt;xs:complexType name="FaxCoverPageType">
		&lt;xs:sequence>
			&lt;xs:element name="name" type="cmf-tech:CmfString" minOccurs="1" maxOccurs="1"/>
			&lt;xs:element name="note" type="cmf-tech:CmfString" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="subject" type="cmf-tech:CmfString" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="sender" type="cmf-party:PersonalDataType" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="recipient" type="cmf-party:PersonalDataType" minOccurs="0" maxOccurs="1"/>
		&lt;/xs:sequence>
		&lt;xs:attribute name="send" type="xs:boolean"/>
	&lt;/xs:complexType>

	&lt;xs:complexType name="FaxType">
		&lt;xs:sequence>
			&lt;xs:element name="faxnumber" type="cmf-biz:PhoneNumberType" minOccurs="1" maxOccurs="1"/>
			&lt;xs:element name="coverpage" type="cmf-comm:FaxCoverPageType" minOccurs="0" maxOccurs="1"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>

	&lt;xs:complexType name="EmailAttachmentType">
		&lt;xs:sequence>
			&lt;xs:element name="filename" type="cmf-tech:CmfString" minOccurs="1" maxOccurs="1"/>
			&lt;xs:element name="uri" type="xs:anyURI" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="attachment-data" type="xs:base64Binary" minOccurs="0"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>

	&lt;xs:complexType name="EmailAttachmentListType">
		&lt;xs:sequence>
			&lt;xs:element name="attachment" minOccurs="1" maxOccurs="unbounded" type="cmf-comm:EmailAttachmentType"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>

	&lt;xs:complexType name="EmailListType">
		&lt;xs:sequence>
			&lt;xs:element name="email" type="cmf-biz:EmailType" minOccurs="1" maxOccurs="unbounded"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>

	&lt;xs:complexType name="EmailMessageType">
		&lt;xs:sequence>
			&lt;xs:element name="priority" type="cmf-tech:CmfString" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="from" type="cmf-comm:EmailListType" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="to" type="cmf-comm:EmailListType" minOccurs="1" maxOccurs="1"/>
			&lt;xs:element name="cc" type="cmf-comm:EmailListType" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="bcc" type="cmf-comm:EmailListType" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="subject" type="cmf-tech:CmfString" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="body" type="cmf-tech:CmfString" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="attachments" type="cmf-comm:EmailAttachmentListType" minOccurs="0" maxOccurs="1"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>

	&lt;xs:complexType name="PrinterType">
		&lt;xs:sequence>
			&lt;xs:element name="printername" type="cmf-tech:CmfString" minOccurs="1" maxOccurs="unbounded"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>

	&lt;xs:complexType name="DeliveryType">
		&lt;xs:sequence>
			&lt;xs:element name="fax" type="cmf-comm:FaxType" minOccurs="0" maxOccurs="unbounded"/>
			&lt;xs:element name="email" type="cmf-comm:EmailMessageType" minOccurs="0" maxOccurs="unbounded"/>
			&lt;xs:element name="printer" type="cmf-comm:PrinterType" minOccurs="0" maxOccurs="unbounded"/>
			&lt;xs:element name="content-uri" type="cmf-tech:CmfString" minOccurs="0" maxOccurs="unbounded"/>
			&lt;xs:element name="post" type="cmf-comm:PostType" minOccurs="0" maxOccurs="unbounded"/>
			&lt;xs:element name="courier" type="cmf-comm:CourierType" minOccurs="0" maxOccurs="unbounded"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>

	&lt;xs:complexType name="SMSTargetType">
		&lt;xs:sequence>
			&lt;xs:element name="mobile-phone" type="cmf-biz:PhoneNumberType" minOccurs="0"/>
			&lt;xs:element name="agent-id" type="cmf-biz:AgentIdType" minOccurs="0"/>
			&lt;xs:element name="client-code" type="cmf-biz:ClientCodeType" minOccurs="0"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>

	&lt;xs:complexType name="SMSType">
		&lt;xs:sequence>
			&lt;xs:element name="sms-target" type="cmf-comm:SMSTargetType"/>
			&lt;xs:element name="sms-type" type="cmf-biz:SMSTypeNameType"/>
			&lt;xs:element name="sms-text" type="cmf-biz:SMSTextType"/>
			&lt;xs:element name="user-id" type="cmf-biz:UserIdType" minOccurs="0"/>
			&lt;xs:element name="sms-id" type="cmf-biz:SMSIdType" minOccurs="0" maxOccurs="1"/>
			&lt;xs:element name="delivery-notification" type="xs:boolean" minOccurs="0"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>

	&lt;xs:complexType name="SMSStatusInfoType">
		&lt;xs:sequence>
			&lt;xs:element name="send-date" type="xs:date"/>
			&lt;xs:element name="originator" type="cmf-tech:CmfString"/>
			&lt;xs:element name="destination" type="cmf-biz:PhoneNumberType"/>
			&lt;xs:element name="status" type="cmf-biz:SMSStatusType"/>
			&lt;xs:element name="status-change" type="xs:date"/>
		&lt;/xs:sequence>
		&lt;xs:attribute name="sms-id" type="cmf-biz:SMSIdType"/>
	&lt;/xs:complexType>

	&lt;xs:simpleType name="ShipmentType">
		&lt;xs:annotation>
			&lt;xs:documentation>Type of letter shipment&lt;/xs:documentation>
		&lt;/xs:annotation>
		&lt;xs:restriction base="cmf-tech:CmfString">
			&lt;xs:enumeration value="STANDARD"/>
			&lt;xs:enumeration value="PRIORITY"/>
			&lt;xs:enumeration value="REGISTERED"/>
			&lt;xs:enumeration value="PAYMENT"/>
			&lt;xs:enumeration value="CONFIRMATION"/>
		&lt;/xs:restriction>
	&lt;/xs:simpleType>

	&lt;xs:complexType name="PostType">
		&lt;xs:annotation>
			&lt;xs:documentation>Traditional letter type&lt;/xs:documentation>
		&lt;/xs:annotation>
		&lt;xs:sequence>
			&lt;xs:element name="shipment" type="cmf-comm:ShipmentType"/>
			&lt;xs:element name="receiver" type="cmf-party:PersonalDataType" minOccurs="0"/>
			&lt;xs:element name="return-envelope" type="xs:boolean" minOccurs="0"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>

	&lt;xs:complexType name="CourierType">
		&lt;xs:annotation>
			&lt;xs:documentation>
			&lt;/xs:documentation>
		&lt;/xs:annotation>
		&lt;xs:sequence>
			&lt;xs:element name="shipment" type="cmf-comm:ShipmentType"/>
			&lt;xs:element name="receiver" type="cmf-party:PersonalDataType" minOccurs="0"/>
			&lt;xs:element name="return-envelope" type="xs:boolean" minOccurs="0"/>
		&lt;/xs:sequence>
	&lt;/xs:complexType>
&lt;/xs:schema></con:schema>
  <con:dependencies>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd" ref="Business Services/Chagall/bClientJV/base/cmf-technical-types"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-product-types" schemaLocation="cmf-product-types.xsd" ref="Business Services/Chagall/bClientJV/base/cmf-product-types"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-party-types" schemaLocation="cmf-party-types.xsd" ref="Business Services/Chagall/bClientJV/base/cmf-party-types"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd" ref="Business Services/Chagall/bClientJV/base/cmf-biz-types"/>
  </con:dependencies>
  <con:targetNamespace>http://jv.channel.cu.com.pl/cmf-comm-types</con:targetNamespace>
</con:schemaEntry>