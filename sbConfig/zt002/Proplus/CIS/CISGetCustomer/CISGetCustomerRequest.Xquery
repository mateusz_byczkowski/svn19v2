<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISGetCustomerRequest($req as element(m:CISGetCustomerRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:NumerKlienta)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }</fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:IdWewPrac)
					then <fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }</fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then <fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }</fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:Opcja)
					then <fml:CI_OPCJA>{ data($req/m:Opcja) }</fml:CI_OPCJA>
					else ()
			}
			{
				if($req/m:IdSys)
					then <fml:CIE_ID_SYS>{ data($req/m:IdSys) }</fml:CIE_ID_SYS>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCISGetCustomerRequest($body/m:CISGetCustomerRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>