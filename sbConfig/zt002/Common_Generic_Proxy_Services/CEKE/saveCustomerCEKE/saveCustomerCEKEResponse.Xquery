<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapsaveCustomerCEKEResponse($fml as element(fml:FML32))
	as element(m:saveCustomerCEKEResponse) {
		<m:saveCustomerCEKEResponse>
			{
				if($fml/fml:E_LOGIN_ID)
					then <m:nik>{ data($fml/fml:E_LOGIN_ID) }</m:nik>
					else ()
			}
		</m:saveCustomerCEKEResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapsaveCustomerCEKEResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>