<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-07-08</con:description>
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetLoyaltyProgramListRequest($req as element(m:getLoyaltyProgramListRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:NumerPaczki)
					then <fml:CI_NUMER_PACZKI?>{ data($req/m:NumerPaczki) }</fml:CI_NUMER_PACZKI>
					else ()
			}
			{
				if($req/m:IloscRekWPaczce)
					then <fml:CI_LICZBA_OPER?>{ data($req/m:IloscRekWPaczce) }</fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:IdWywolania)
					then <fml:CI_MSHEAD_MSGID?>{ data($req/m:IdWywolania) }</fml:CI_MSHEAD_MSGID>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetLoyaltyProgramListRequest($body/m:getLoyaltyProgramListRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>