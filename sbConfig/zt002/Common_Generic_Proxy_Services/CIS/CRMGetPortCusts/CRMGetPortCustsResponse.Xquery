<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPortCustsResponse($fml as element(fml:FML32))
	as element(m:CRMGetPortCustsResponse) {
		<m:CRMGetPortCustsResponse>
			{
				let $CI_NUMER_PACZKI := $fml/fml:CI_NUMER_PACZKI return
					if($CI_NUMER_PACZKI)
						then <m:NumerPaczki>{ data($CI_NUMER_PACZKI) }</m:NumerPaczki>
						else ()
			}
			{
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST
				let $DC_NUMER_ODDZIALU := $fml/fml:DC_NUMER_ODDZIALU
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_STATUS_GIODO := $fml/fml:CI_STATUS_GIODO
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL
				let $DC_NR_DOWODU_REGON := $fml/fml:DC_NR_DOWODU_REGON
				let $CI_WIECEJ_ADDR_KORESP := $fml/fml:CI_WIECEJ_ADDR_KORESP
				let $DC_IMIE_I_NAZWISKO_ALT := $fml/fml:DC_IMIE_I_NAZWISKO_ALT
				let $DC_ULICA_ADRES_ALT := $fml/fml:DC_ULICA_ADRES_ALT
				let $DC_NR_POSES_LOKALU_ADRES_ALT := $fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT
				let $DC_MIASTO_ADRES_ALT := $fml/fml:DC_MIASTO_ADRES_ALT
				let $DC_KOD_POCZTOWY_ADRES_ALT := $fml/fml:DC_KOD_POCZTOWY_ADRES_ALT
				let $DC_NR_TELEFONU := $fml/fml:DC_NR_TELEFONU
				let $DC_NR_TELEF_KOMORKOWEGO := $fml/fml:DC_NR_TELEF_KOMORKOWEGO
				let $CI_NR_TELEFONU_SMS := $fml/fml:CI_NR_TELEFONU_SMS
				let $DC_ADRES_E_MAIL := $fml/fml:DC_ADRES_E_MAIL
				let $CI_RELACJA := $fml/fml:CI_RELACJA
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $CI_CESJA_UPRAWNIEN := $fml/fml:CI_CESJA_UPRAWNIEN
				let $CI_SKP_PRACOWNIKA_UPR := $fml/fml:CI_SKP_PRACOWNIKA_UPR
				let $CI_DATA_CESJI_UPRAWNIEN := $fml/fml:CI_DATA_CESJI_UPRAWNIEN
				let $CI_KLIENT_ZNANY := $fml/fml:CI_KLIENT_ZNANY
				let $CI_VIP := $fml/fml:CI_VIP
                                let $DC_SEGMENT_MARKETINGOWY := $fml/fml:DC_SEGMENT_MARKETINGOWY
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					<m:CRMGetPortCustsPortKlient>
					{
						if($DC_NUMER_KLIENTA[$p])
							then <m:NumerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }</m:NumerKlienta>
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then <m:NazwaPelna>{ data($CI_NAZWA_PELNA[$p]) }</m:NazwaPelna>
						else ()
					}
					{
						if($DC_ULICA_DANE_PODST[$p])
							then <m:UlicaDanePodst>{ data($DC_ULICA_DANE_PODST[$p]) }</m:UlicaDanePodst>
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_DANE_PODST[$p])
							then <m:NrPosesLokaluDanePodst>{ data($DC_NR_POSES_LOKALU_DANE_PODST[$p]) }</m:NrPosesLokaluDanePodst>
						else ()
					}
					{
						if($DC_MIASTO_DANE_PODST[$p])
							then <m:MiastoDanePodst>{ data($DC_MIASTO_DANE_PODST[$p]) }</m:MiastoDanePodst>
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_DANE_PODST[$p])
							then <m:KodPocztowyDanePodst>{ data($DC_KOD_POCZTOWY_DANE_PODST[$p]) }</m:KodPocztowyDanePodst>
						else ()
					}
					{
						if($DC_NUMER_ODDZIALU[$p])
							then <m:NumerOddzialu>{ data($DC_NUMER_ODDZIALU[$p]) }</m:NumerOddzialu>
						else ()
					}
					{
						if($DC_IMIE[$p])
							then <m:Imie>{ data($DC_IMIE[$p]) }</m:Imie>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then <m:Nazwisko>{ data($DC_NAZWISKO[$p]) }</m:Nazwisko>
						else ()
					}
					{
						if($CI_STATUS_GIODO[$p])
							then <m:StatusGiodo>{ data($CI_STATUS_GIODO[$p]) }</m:StatusGiodo>
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then <m:UdostepGrupa>{ data($CI_UDOSTEP_GRUPA[$p]) }</m:UdostepGrupa>
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI[$p])
							then <m:KlasaObslugi>{ data($CI_KLASA_OBSLUGI[$p]) }</m:KlasaObslugi>
						else ()
					}
					{
						if($DC_NR_PESEL[$p])
							then <m:NrPesel>{ data($DC_NR_PESEL[$p]) }</m:NrPesel>
						else ()
					}
					{
						if($DC_NR_DOWODU_REGON[$p])
							then <m:NrDowoduRegon>{ data($DC_NR_DOWODU_REGON[$p]) }</m:NrDowoduRegon>
						else ()
					}
					{
						if($CI_WIECEJ_ADDR_KORESP[$p])
							then <m:WiecejAddrKoresp>{ data($CI_WIECEJ_ADDR_KORESP[$p]) }</m:WiecejAddrKoresp>
						else ()
					}
					{
						if($DC_IMIE_I_NAZWISKO_ALT[$p])
							then <m:ImieINazwiskoAlt>{ data($DC_IMIE_I_NAZWISKO_ALT[$p]) }</m:ImieINazwiskoAlt>
						else ()
					}
					{
						if($DC_ULICA_ADRES_ALT[$p])
							then <m:UlicaAdresAlt>{ data($DC_ULICA_ADRES_ALT[$p]) }</m:UlicaAdresAlt>
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_ADRES_ALT[$p])
							then <m:NrPosesLokaluAdresAlt>{ data($DC_NR_POSES_LOKALU_ADRES_ALT[$p]) }</m:NrPosesLokaluAdresAlt>
						else ()
					}
					{
						if($DC_MIASTO_ADRES_ALT[$p])
							then <m:MiastoAdresAlt>{ data($DC_MIASTO_ADRES_ALT[$p]) }</m:MiastoAdresAlt>
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_ADRES_ALT[$p])
							then <m:KodPocztowyAdresAlt>{ data($DC_KOD_POCZTOWY_ADRES_ALT[$p]) }</m:KodPocztowyAdresAlt>
						else ()
					}
					{
						if($DC_NR_TELEFONU[$p])
							then <m:NrTelefonu>{ data($DC_NR_TELEFONU[$p]) }</m:NrTelefonu>
						else ()
					}
					{
						if($DC_NR_TELEF_KOMORKOWEGO[$p])
							then <m:NrTelefKomorkowego>{ data($DC_NR_TELEF_KOMORKOWEGO[$p]) }</m:NrTelefKomorkowego>
						else ()
					}
					{
						if($CI_NR_TELEFONU_SMS[$p])
							then <m:NrTelefonuSms>{ data($CI_NR_TELEFONU_SMS[$p]) }</m:NrTelefonuSms>
						else ()
					}
					{
						if($DC_ADRES_E_MAIL[$p])
							then <m:AdresEMail>{ data($DC_ADRES_E_MAIL[$p]) }</m:AdresEMail>
						else ()
					}
					{
						if($CI_RELACJA[$p])
							then <m:Relacja>{ data($CI_RELACJA[$p]) }</m:Relacja>
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then <m:IdPortfela>{ data($CI_ID_PORTFELA[$p]) }</m:IdPortfela>
						else ()
					}
					{
						if($CI_CESJA_UPRAWNIEN[$p])
							then <m:CesjaUprawnien>{ data($CI_CESJA_UPRAWNIEN[$p]) }</m:CesjaUprawnien>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_UPR[$p])
							then <m:SkpPracownikaUpr>{ data($CI_SKP_PRACOWNIKA_UPR[$p]) }</m:SkpPracownikaUpr>
						else ()
					}
					{
						if($CI_DATA_CESJI_UPRAWNIEN[$p])
							then <m:DataCesjiUprawnien>{ data($CI_DATA_CESJI_UPRAWNIEN[$p]) }</m:DataCesjiUprawnien>
						else ()
					}
					{
						if($CI_KLIENT_ZNANY[$p])
							then <m:KlientZnany>{ data($CI_KLIENT_ZNANY[$p]) }</m:KlientZnany>
						else ()
					}
					{
						if($CI_VIP[$p])
							then <m:Vip>{ data($CI_VIP[$p]) }</m:Vip>
						else ()
					}
                                        {
                                                if($DC_SEGMENT_MARKETINGOWY[$p])
                                                        then <m:Segment>{ data($DC_SEGMENT_MARKETINGOWY[$p]) }</m:Segment>
                                                else ()
                                        }
					</m:CRMGetPortCustsPortKlient>
			}
		</m:CRMGetPortCustsResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetPortCustsResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>