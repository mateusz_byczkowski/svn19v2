<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMChgDefSbsEmpRequest($req as element(m:CRMChgDefSbsEmpRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:SkpPracownikaRej)
					then <fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:SkpPracownikaRej) }</fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then <fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }</fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:SkpZastepcy)
					then <fml:CI_SKP_ZASTEPCY>{ data($req/m:SkpZastepcy) }</fml:CI_SKP_ZASTEPCY>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMChgDefSbsEmpRequest($body/m:CRMChgDefSbsEmpRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>