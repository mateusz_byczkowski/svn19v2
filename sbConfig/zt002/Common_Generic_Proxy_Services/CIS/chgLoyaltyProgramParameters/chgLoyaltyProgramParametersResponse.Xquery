<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-07-08</con:description>
    <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$fML321" element="ns1:FML32" location="../../Operations/savetransfer/CISIN.xsd" ::)
(:: pragma bea:global-element-return element="ns0:invokeResponse" location="chgLoyaltyProgramParameters.wsdl" ::)

declare namespace ns2 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Customers/chgLoyaltyProgramParameters/chgLoyaltyProgramParametersResponse/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:chgLoyaltyProgramParametersResponse($fML321 as element(ns1:FML32))
    as element(ns0:invokeResponse) {
        <ns0:invokeResponse>
            <ns0:responseMessage>
                <ns2:ResponseMessage>
                    <ns2:result>true</ns2:result>
                </ns2:ResponseMessage>
            </ns0:responseMessage>
        </ns0:invokeResponse>
};

declare variable $fML321 as element(ns1:FML32) external;


<soap-env:Body>{
	xf:chgLoyaltyProgramParametersResponse($fML321)}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>