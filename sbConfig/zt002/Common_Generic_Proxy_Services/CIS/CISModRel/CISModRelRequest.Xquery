<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISModRelRequest($req as element(m:CISModRelRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:IdWewPrac)
					then <fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }</fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }</fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:NumerKlientaRel)
					then <fml:DC_NUMER_KLIENTA_REL>{ data($req/m:NumerKlientaRel) }</fml:DC_NUMER_KLIENTA_REL>
					else ()
			}
			{
				if($req/m:TypRelacji)
					then <fml:DC_TYP_RELACJI>{ data($req/m:TypRelacji) }</fml:DC_TYP_RELACJI>
					else ()
			}
			{
				if($req/m:BudowaPortfela)
					then <fml:CI_BUDOWA_PORTFELA>{ data($req/m:BudowaPortfela) }</fml:CI_BUDOWA_PORTFELA>
					else ()
			}
			{
				if($req/m:ObslugaZadan)
					then <fml:CI_OBSLUGA_ZADAN>{ data($req/m:ObslugaZadan) }</fml:CI_OBSLUGA_ZADAN>
					else ()
			}
			{
				if($req/m:RelacjaOdwrotna)
					then <fml:CI_RELACJA_ODWROTNA>{ data($req/m:RelacjaOdwrotna) }</fml:CI_RELACJA_ODWROTNA>
					else ()
			}
			{
				if($req/m:KorespSeryjna)
					then <fml:CI_KORESP_SERYJNA>{ data($req/m:KorespSeryjna) }</fml:CI_KORESP_SERYJNA>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCISModRelRequest($body/m:CISModRelRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>