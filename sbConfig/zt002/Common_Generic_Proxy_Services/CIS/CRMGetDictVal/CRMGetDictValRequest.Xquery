<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetDictValRequest($req as element(m:CRMGetDictValRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:IdWewPrac)
					then <fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }</fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSlownika)
					then <fml:CI_ID_SLOWNIKA>{ data($req/m:IdSlownika) }</fml:CI_ID_SLOWNIKA>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetDictValRequest($body/m:CRMGetDictValRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>