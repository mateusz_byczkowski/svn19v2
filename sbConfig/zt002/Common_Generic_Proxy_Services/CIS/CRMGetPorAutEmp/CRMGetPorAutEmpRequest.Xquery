<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPorAutEmpRequest($req as element(m:CRMGetPorAutEmpRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:IdPortfela)
					then <fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }</fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then <fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }</fml:DC_TYP_KLIENTA>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetPorAutEmpRequest($body/m:CRMGetPorAutEmpRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>