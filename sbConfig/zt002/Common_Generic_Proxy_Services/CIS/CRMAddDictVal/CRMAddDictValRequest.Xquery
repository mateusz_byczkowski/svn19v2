<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddDictValRequest($req as element(m:CRMAddDictValRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:IdWewPrac)
					then <fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }</fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:IdSlownika)
					then <fml:CI_ID_SLOWNIKA>{ data($req/m:IdSlownika) }</fml:CI_ID_SLOWNIKA>
					else ()
			}
			{
				if($req/m:WartoscSlownika)
					then <fml:CI_WARTOSC_SLOWNIKA>{ data($req/m:WartoscSlownika) }</fml:CI_WARTOSC_SLOWNIKA>
					else ()
			}
			{
				if($req/m:OpisWartosciSlownika)
					then <fml:CI_OPIS_WARTOSCI_SLOWNIKA>{ data($req/m:OpisWartosciSlownika) }</fml:CI_OPIS_WARTOSCI_SLOWNIKA>
					else ()
			}
			{
				if($req/m:DomenaRodzica)
					then <fml:CI_DOMENA_RODZICA>{ data($req/m:DomenaRodzica) }</fml:CI_DOMENA_RODZICA>
					else ()
			}
			{
				if($req/m:IdSlownikaRodzica)
					then <fml:CI_ID_SLOWNIKA_RODZICA>{ data($req/m:IdSlownikaRodzica) }</fml:CI_ID_SLOWNIKA_RODZICA>
					else ()
			}
			{
				if($req/m:DodatkowaWartSlownika)
					then <fml:CI_DODATKOWA_WART_SLOWNIKA>{ data($req/m:DodatkowaWartSlownika) }</fml:CI_DODATKOWA_WART_SLOWNIKA>
					else ()
			}
			{
				if($req/m:WagaWartosciSlownika)
					then <fml:CI_WAGA_WARTOSCI_SLOWNIKA>{ data($req/m:WagaWartosciSlownika) }</fml:CI_WAGA_WARTOSCI_SLOWNIKA>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMAddDictValRequest($body/m:CRMAddDictValRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>