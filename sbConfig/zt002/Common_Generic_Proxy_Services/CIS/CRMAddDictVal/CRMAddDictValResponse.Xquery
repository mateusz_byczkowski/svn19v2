<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddDictValResponse($fml as element(fml:FML32))
	as element(m:CRMAddDictValResponse) {
		<m:CRMAddDictValResponse>
			{
				if($fml/fml:CI_ID_SLOWNIKA)
					then <m:IdSlownika>{ data($fml/fml:CI_ID_SLOWNIKA) }</m:IdSlownika>
					else ()
			}
			{
				if($fml/fml:CI_WARTOSC_SLOWNIKA)
					then <m:WartoscSlownika>{ data($fml/fml:CI_WARTOSC_SLOWNIKA) }</m:WartoscSlownika>
					else ()
			}
			{
				if($fml/fml:CI_OPIS_WARTOSCI_SLOWNIKA)
					then <m:OpisWartosciSlownika>{ data($fml/fml:CI_OPIS_WARTOSCI_SLOWNIKA) }</m:OpisWartosciSlownika>
					else ()
			}
			{
				if($fml/fml:CI_WAGA_WARTOSCI_SLOWNIKA)
					then <m:WagaWartosciSlownika>{ data($fml/fml:CI_WAGA_WARTOSCI_SLOWNIKA) }</m:WagaWartosciSlownika>
					else ()
			}
			{
				if($fml/fml:CI_DOMENA_RODZICA)
					then <m:DomenaRodzica>{ data($fml/fml:CI_DOMENA_RODZICA) }</m:DomenaRodzica>
					else ()
			}
			{
				if($fml/fml:CI_ID_SLOWNIKA_RODZICA)
					then <m:IdSlownikaRodzica>{ data($fml/fml:CI_ID_SLOWNIKA_RODZICA) }</m:IdSlownikaRodzica>
					else ()
			}
			{
				if($fml/fml:CI_DODATKOWA_WART_SLOWNIKA)
					then <m:DodatkowaWartSlownika>{ data($fml/fml:CI_DODATKOWA_WART_SLOWNIKA) }</m:DodatkowaWartSlownika>
					else ()
			}
		</m:CRMAddDictValResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMAddDictValResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>