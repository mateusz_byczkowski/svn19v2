<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";



declare function xf:mapCISGetCustomerResponse($fml as element(fml:FML32))
	as element(m:CISGetCustomerResponse) {
        
		<m:CISGetCustomerResponse>
			{
				if($fml/fml:DC_NUMER_KLIENTA)
					then <m:NumerKlienta>{ data($fml/fml:DC_NUMER_KLIENTA) }</m:NumerKlienta>
					else ()
			}
			{
				if($fml/fml:CI_NAZWA_PELNA)
					then <m:NazwaPelna>{ data($fml/fml:CI_NAZWA_PELNA) }</m:NazwaPelna>
					else ()
			}
			{
				if($fml/fml:CI_UDOSTEP_GRUPA)
					then <m:UdostepGrupa>{ data($fml/fml:CI_UDOSTEP_GRUPA) }</m:UdostepGrupa>
					else ()
			}
			{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then <m:NrDowoduRegon>{ data($fml/fml:DC_NR_DOWODU_REGON) }</m:NrDowoduRegon>
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then <m:NrPesel>{ data($fml/fml:DC_NR_PESEL) }</m:NrPesel>
					else ()
			}
			{
				if($fml/fml:DC_TYP_KLIENTA)
					then <m:TypKlienta>{ data($fml/fml:DC_TYP_KLIENTA) }</m:TypKlienta>
					else ()
			}
			{
				if($fml/fml:CI_STATUS_GIODO)
					then <m:StatusGiodo>{ data($fml/fml:CI_STATUS_GIODO) }</m:StatusGiodo>
					else ()
			}
			{
				if($fml/fml:DC_REZ_NIEREZ)
					then <m:RezNierez>{ data($fml/fml:DC_REZ_NIEREZ) }</m:RezNierez>
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then <m:Nip>{ data($fml/fml:DC_NIP) }</m:Nip>
					else ()
			}
			{
				if($fml/fml:CI_GRUPA_KLIENTOW)
					then <m:GrupaKlientow>{ data($fml/fml:CI_GRUPA_KLIENTOW) }</m:GrupaKlientow>
					else ()
			}
			{
				if($fml/fml:DC_PRACOWNIK_BANKU)
					then <m:PracownikBanku>{ data($fml/fml:DC_PRACOWNIK_BANKU) }</m:PracownikBanku>
					else ()
			}
			{
				if($fml/fml:DC_ULICA_DANE_PODST)
					then <m:UlicaDanePodst>{ data($fml/fml:DC_ULICA_DANE_PODST) }</m:UlicaDanePodst>
					else ()
			}
			{
				if($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST)
					then <m:NrPosesLokaluDanePodst>{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST) }</m:NrPosesLokaluDanePodst>
					else ()
			}
			{
				if($fml/fml:DC_MIASTO_DANE_PODST)
					then <m:MiastoDanePodst>{ data($fml/fml:DC_MIASTO_DANE_PODST) }</m:MiastoDanePodst>
					else ()
			}
			{
				if($fml/fml:DC_KOD_POCZTOWY_DANE_PODST)
					then <m:KodPocztowyDanePodst>{ data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST) }</m:KodPocztowyDanePodst>
					else ()
			}
			{
				if($fml/fml:DC_ADRES_E_MAIL)
					then <m:AdresEMail>{ data($fml/fml:DC_ADRES_E_MAIL) }</m:AdresEMail>
					else ()
			}
			{
				if($fml/fml:DC_NR_KIER_TELEFONU)
					then <m:NrKierTelefonu>{ data($fml/fml:DC_NR_KIER_TELEFONU) }</m:NrKierTelefonu>
					else ()
			}
			{
				if($fml/fml:DC_NR_TELEFONU)
					then <m:NrTelefonu>{ data($fml/fml:DC_NR_TELEFONU) }</m:NrTelefonu>
					else ()
			}
			{
				if($fml/fml:DC_NUMER_FAKSU)
					then <m:NumerFaksu>{ data($fml/fml:DC_NUMER_FAKSU) }</m:NumerFaksu>
					else ()
			}
			{
				if($fml/fml:DC_NR_TELEF_KOMORKOWEGO)
					then <m:NrTelefKomorkowego>{ data($fml/fml:DC_NR_TELEF_KOMORKOWEGO) }</m:NrTelefKomorkowego>
					else ()
			}
			{
				if($fml/fml:DC_WOJEWODZTWO)
					then <m:Wojewodztwo>{ data($fml/fml:DC_WOJEWODZTWO) }</m:Wojewodztwo>
					else ()
			}
			{
				if($fml/fml:DC_KOD_KRAJU)
					then <m:KodKraju>{ data($fml/fml:DC_KOD_KRAJU) }</m:KodKraju>
					else ()
			}
			{
				if($fml/fml:DC_NUMER_ODDZIALU)
					then if (data($fml/fml:DC_NUMER_ODDZIALU) = '997')
                                                   then <m:NumerOddzialu>10066</m:NumerOddzialu> 
                                         else if (data($fml/fml:DC_NUMER_ODDZIALU) = '996')
                                                   then <m:NumerOddzialu>10072</m:NumerOddzialu> 
                                         else if (data($fml/fml:DC_NUMER_ODDZIALU) = '995')
                                                   then <m:NumerOddzialu>10017</m:NumerOddzialu> 
                                         else if (data($fml/fml:DC_NUMER_ODDZIALU) = '992')
                                                   then <m:NumerOddzialu>10076</m:NumerOddzialu>          
                                         else  <m:NumerOddzialu>{ data($fml/fml:DC_NUMER_ODDZIALU) }</m:NumerOddzialu>
					else ()
			}
			{
				if($fml/fml:DC_SPW)
					then <m:Spw>{ data($fml/fml:DC_SPW) }</m:Spw>
					else ()
			}
			{
				if($fml/fml:DC_NAZWA_SKROCONA)
					then <m:NazwaSkrocona>{ data($fml/fml:DC_NAZWA_SKROCONA) }</m:NazwaSkrocona>
					else ()
			}
			{
				if($fml/fml:CI_DATA_WPROWADZENIA)
					then <m:DataWprowadzenia>{ data($fml/fml:CI_DATA_WPROWADZENIA) }</m:DataWprowadzenia>
					else ()
			}
			{
				if($fml/fml:CI_DATA_STATUSU_GIODU)
					then <m:DataStatusuGiodu>{ data($fml/fml:CI_DATA_STATUSU_GIODU) }</m:DataStatusuGiodu>
					else ()
			}
			{
				if($fml/fml:CI_DATA_UDOSTEP_GRUPA)
					then <m:DataUdostepGrupa>{ data($fml/fml:CI_DATA_UDOSTEP_GRUPA) }</m:DataUdostepGrupa>
					else ()
			}
			{
				if($fml/fml:CI_ZGODA_NA_KORESP)
					then <m:ZgodaNaKoresp>{ data($fml/fml:CI_ZGODA_NA_KORESP) }</m:ZgodaNaKoresp>
					else ()
			}
			{
				if($fml/fml:CI_UDOSTEP_GRUPA)
					(: then <m:ZgodaNaKontakt>{ data($fml/fml:CI_ZGODA_NA_KONTAKT) }</m:ZgodaNaKontakt>
                                         Zmiana pola na zawierające zgodę na przetwarzanie danych :)
                                            then if (data($fml/fml:CI_UDOSTEP_GRUPA ) = "99999") then 
                                                <m:ZgodaNaKontakt>9</m:ZgodaNaKontakt>
                                           else
                                                <m:ZgodaNaKontakt>{ data($fml/fml:CI_UDOSTEP_GRUPA) }</m:ZgodaNaKontakt>
					else ()
			}
			{
				if($fml/fml:CI_GIIF)
					then <m:Giif>{ data($fml/fml:CI_GIIF) }</m:Giif>
					else ()
			}
			{
				if($fml/fml:DC_KARTA_WZOROW_PODPISOW)
					then <m:KartaWzorowPodpisow>{ data($fml/fml:DC_KARTA_WZOROW_PODPISOW) }</m:KartaWzorowPodpisow>
					else ()
			}
			{
				if($fml/fml:DC_PREF_KONTAKT)
					then <m:PrefKontakt>{ data($fml/fml:DC_PREF_KONTAKT) }</m:PrefKontakt>
					else ()
			}
			{
				if($fml/fml:CI_RYZYKO_NBP)
					then <m:RyzykoNbp>{ data($fml/fml:CI_RYZYKO_NBP) }</m:RyzykoNbp>
					else ()
			}
			{
				if($fml/fml:CI_RYZYKO_BZWBK)
					then <m:RyzykoBzwbk>{ data($fml/fml:CI_RYZYKO_BZWBK) }</m:RyzykoBzwbk>
					else ()
			}
			{
				if($fml/fml:CI_MAX_RYZYKO)
					then <m:MaxRyzyko>{ data($fml/fml:CI_MAX_RYZYKO) }</m:MaxRyzyko>
					else ()
			}
			{
				if($fml/fml:CI_RYZYKO_INNE)
					then <m:RyzykoInne>{ data($fml/fml:CI_RYZYKO_INNE) }</m:RyzykoInne>
					else ()
			}
			{
				if($fml/fml:CI_DATA_RYZYKA)
					then <m:DataRyzyka>{ data($fml/fml:CI_DATA_RYZYKA) }</m:DataRyzyka>
					else ()
			}
			{
				if($fml/fml:CI_DATA_RYZYKA_INNEGO)
					then <m:DataRyzykaInnego>{ data($fml/fml:CI_DATA_RYZYKA_INNEGO) }</m:DataRyzykaInnego>
					else ()
			}
			{
				if($fml/fml:CI_DATA_OPOZNIENIA)
					then <m:DataOpoznienia>{ data($fml/fml:CI_DATA_OPOZNIENIA) }</m:DataOpoznienia>
					else ()
			}
			{
				if($fml/fml:CI_CZAS_OPOZNIENIA_DNI)
					then <m:CzasOpoznieniaDni>{ data($fml/fml:CI_CZAS_OPOZNIENIA_DNI) }</m:CzasOpoznieniaDni>
					else ()
			}
			{
				if($fml/fml:CI_LICZNIK_OPOZNIEN)
					then <m:LicznikOpoznien>{ data($fml/fml:CI_LICZNIK_OPOZNIEN) }</m:LicznikOpoznien>
					else ()
			}
			{
				if($fml/fml:CI_GRADING)
					then <m:Grading>{ data($fml/fml:CI_GRADING) }</m:Grading>

					else ()
			}
			{
				if($fml/fml:CI_SCORING)
					then <m:Scoring>{ data($fml/fml:CI_SCORING) }</m:Scoring>
					else ()
			}
			{
				if($fml/fml:CI_PRACOWNIK_WPROW)
					then <m:PracownikWprow>{ data($fml/fml:CI_PRACOWNIK_WPROW) }</m:PracownikWprow>
					else ()
			}
			{
				if($fml/fml:CI_PRACOWNIK_ZMIEN)
					then <m:PracownikZmien>{ data($fml/fml:CI_PRACOWNIK_ZMIEN) }</m:PracownikZmien>
					else ()
			}
			{
				if($fml/fml:CI_DATA_MODYFIKACJI)
					then <m:DataModyfikacji>{ data($fml/fml:CI_DATA_MODYFIKACJI) }</m:DataModyfikacji>
					else ()
			}
			{
				if($fml/fml:DC_URZEDNIK_1)
					then <m:Urzednik1>{ data($fml/fml:DC_URZEDNIK_1) }</m:Urzednik1>
					else ()
			}
			{
				if($fml/fml:DC_URZEDNIK_2)
					then <m:Urzednik2>{ data($fml/fml:DC_URZEDNIK_2) }</m:Urzednik2>
					else ()
			}
			{
				if($fml/fml:DC_URZEDNIK_3)
					then <m:Urzednik3>{ data($fml/fml:DC_URZEDNIK_3) }</m:Urzednik3>
					else ()
			}
			{
				if($fml/fml:DC_SEGMENT_MARKETINGOWY)
					then <m:SegmentMarketingowy>{ data($fml/fml:DC_SEGMENT_MARKETINGOWY) }</m:SegmentMarketingowy>
					else ()
			}
			{
				if($fml/fml:CI_PODSEGMENT_MARK)
					then <m:PodsegmentMark>{ data($fml/fml:CI_PODSEGMENT_MARK) }</m:PodsegmentMark>
					else ()
			}
			{
				if($fml/fml:CI_KLASA_OBSLUGI)
					then <m:KlasaObslugi>{ data($fml/fml:CI_KLASA_OBSLUGI) }</m:KlasaObslugi>
					else ()
			}
			{
				if($fml/fml:CI_DECYZJA)
					then <m:Decyzja>{ data($fml/fml:CI_DECYZJA) }</m:Decyzja>
					else ()
			}
			{
				if($fml/fml:DC_ZRODLO_DANYCH)
					then <m:ZrodloDanych>{ data($fml/fml:DC_ZRODLO_DANYCH) }</m:ZrodloDanych>
					else ()
			}
			{
				if($fml/fml:CI_KANALY_EL)
					then <m:KanalyEl>{ data($fml/fml:CI_KANALY_EL) }</m:KanalyEl>
					else ()
			}
			{
				if($fml/fml:DC_PAKIET)
					then <m:Pakiet>{ data($fml/fml:DC_PAKIET) }</m:Pakiet>
					else ()
			}
			{
				if($fml/fml:CI_DATA_OST_KONTAKTU)
					then <m:DataOstKontaktu>{ data($fml/fml:CI_DATA_OST_KONTAKTU) }</m:DataOstKontaktu>
					else ()
			}
			{
				if($fml/fml:CI_POSIADANIE_KONTA_OS)
					then <m:PosiadanieKontaOs>{ data($fml/fml:CI_POSIADANIE_KONTA_OS) }</m:PosiadanieKontaOs>
					else ()
			}
			{
				if($fml/fml:CI_KONTO_Z_LIMITEM)
					then <m:KontoZLimitem>{ data($fml/fml:CI_KONTO_Z_LIMITEM) }</m:KontoZLimitem>
					else ()
			}
			{
				if($fml/fml:CI_WYSOKOSC_LIMITU)
					then <m:WysokoscLimitu>{ data($fml/fml:CI_WYSOKOSC_LIMITU) }</m:WysokoscLimitu>
					else ()
			}
			{
				if($fml/fml:CI_LOKATY_BANKOWE)
					then <m:LokatyBankowe>{ data($fml/fml:CI_LOKATY_BANKOWE) }</m:LokatyBankowe>
					else ()
			}
			{
				if($fml/fml:CI_RACHUNEK_INWEST)
					then <m:RachunekInwest>{ data($fml/fml:CI_RACHUNEK_INWEST) }</m:RachunekInwest>
					else ()
			}
			{
				if($fml/fml:CI_WYSOKOSC_ZADLUZENIA)
					then <m:WysokoscZadluzenia>{ data($fml/fml:CI_WYSOKOSC_ZADLUZENIA) }</m:WysokoscZadluzenia>
					else ()
			}
			{
				if($fml/fml:CI_MIESIECZNA_RATA)
					then <m:MiesiecznaRata>{ data($fml/fml:CI_MIESIECZNA_RATA) }</m:MiesiecznaRata>
					else ()
			}
			{
				if($fml/fml:CI_KREDYT_OBSLUG_SAM)
					then <m:KredytObslugSam>{ data($fml/fml:CI_KREDYT_OBSLUG_SAM) }</m:KredytObslugSam>
					else ()
			}
			{
				if($fml/fml:CI_WYSOKOSC_PORECZENIA)
					then <m:WysokoscPoreczenia>{ data($fml/fml:CI_WYSOKOSC_PORECZENIA) }</m:WysokoscPoreczenia>
					else ()
			}
			{
				if($fml/fml:CI_DATA_WYGASNIECIA)
					then <m:DataWygasniecia>{ data($fml/fml:CI_DATA_WYGASNIECIA) }</m:DataWygasniecia>
					else ()
			}
			{
				if($fml/fml:CI_ZOBOW_PLAC_PORECZ)
					then <m:ZobowPlacPorecz>{ data($fml/fml:CI_ZOBOW_PLAC_PORECZ) }</m:ZobowPlacPorecz>
					else ()
			}
			{
				if($fml/fml:CI_WINDYKACJA)
					then <m:Windykacja>{ data($fml/fml:CI_WINDYKACJA) }</m:Windykacja>
					else ()
			}
			{
				if($fml/fml:CI_KWOTA_WINDYKACJI)
					then <m:KwotaWindykacji>{ data($fml/fml:CI_KWOTA_WINDYKACJI) }</m:KwotaWindykacji>
					else ()
			}
			{
				if($fml/fml:CI_NIK)
					then <m:Nik>{ data($fml/fml:CI_NIK) }</m:Nik>
					else ()
			}
			{
				if($fml/fml:CI_RELACJA)
					then <m:Relacja>{ data($fml/fml:CI_RELACJA) }</m:Relacja>
					else ()
			}
			{
				if($fml/fml:CI_DECYL)
					then <m:Decyl>{ data($fml/fml:CI_DECYL) }</m:Decyl>
					else ()
			}
			{
				if($fml/fml:CI_LISTA_KONTAKTOW)
					then <m:ListaKontaktow>{ data($fml/fml:CI_LISTA_KONTAKTOW) }</m:ListaKontaktow>
					else ()
			}
			{
				if($fml/fml:CI_STATUS_AKTYWNOSCI)
					then <m:StatusAktywnosci>{ data($fml/fml:CI_STATUS_AKTYWNOSCI) }</m:StatusAktywnosci>
					else ()
			}
			{
				if($fml/fml:CI_DANE_MARKETINGOWE)
					then <m:DaneMarketingowe>{ data($fml/fml:CI_DANE_MARKETINGOWE) }</m:DaneMarketingowe>
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA)
					then <m:SkpPracownika>{ data($fml/fml:CI_SKP_PRACOWNIKA) }</m:SkpPracownika>
					else ()
			}
			{
				if($fml/fml:CI_SKP_ZASTEPCY)
					then <m:SkpZastepcy>{ data($fml/fml:CI_SKP_ZASTEPCY) }</m:SkpZastepcy>
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA_UPR)
					then <m:SkpPracownikaUpr>{ data($fml/fml:CI_SKP_PRACOWNIKA_UPR) }</m:SkpPracownikaUpr>
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA_CBK)
					then <m:SkpPracownikaCbk>{ data($fml/fml:CI_SKP_PRACOWNIKA_CBK) }</m:SkpPracownikaCbk>
					else ()
			}
			{
				if($fml/fml:DC_IMIE)
					then <m:Imie>{ data($fml/fml:DC_IMIE) }</m:Imie>
					else ()
			}
			{
				if($fml/fml:DC_NAZWISKO)
					then <m:Nazwisko>{ data($fml/fml:DC_NAZWISKO) }</m:Nazwisko>
					else ()
			}
			{
				if($fml/fml:DC_NUMER_PASZPORTU)
					then <m:NumerPaszportu>{ data($fml/fml:DC_NUMER_PASZPORTU) }</m:NumerPaszportu>
					else ()
			}
			{
				if($fml/fml:CI_DOK_TOZSAMOSCI)
					then <m:DokTozsamosci>{ data($fml/fml:CI_DOK_TOZSAMOSCI) }</m:DokTozsamosci>
					else ()
			}
			{
				if($fml/fml:CI_SERIA_NR_DOK)
					then <m:SeriaNrDok>{ data($fml/fml:CI_SERIA_NR_DOK) }</m:SeriaNrDok>
					else ()
			}
			{
				if($fml/fml:CI_WYDANY_PRZEZ)
					then <m:WydanyPrzez>{ data($fml/fml:CI_WYDANY_PRZEZ) }</m:WydanyPrzez>
					else ()
			}
			{
				if($fml/fml:CI_SPRZECIW_PRZETW_DANYCH)
					then <m:SprzeciwPrzetwDanych>{ data($fml/fml:CI_SPRZECIW_PRZETW_DANYCH) }</m:SprzeciwPrzetwDanych>
					else ()
			}
			{
				if($fml/fml:CI_NR_LEGITYMACJI)
					then <m:NrLegitymacji>{ data($fml/fml:CI_NR_LEGITYMACJI) }</m:NrLegitymacji>
					else ()
			}
			{
				if($fml/fml:CI_NR_PRAWA_JAZDY)
					then <m:NrPrawaJazdy>{ data($fml/fml:CI_NR_PRAWA_JAZDY) }</m:NrPrawaJazdy>
					else ()
			}
			{
				if($fml/fml:DC_PLEC)
					then <m:Plec>{ data($fml/fml:DC_PLEC) }</m:Plec>
					else ()
			}
			{
				if($fml/fml:DC_TYTUL)
					then <m:Tytul>{ data($fml/fml:DC_TYTUL) }</m:Tytul>
					else ()
			}
			{
				if($fml/fml:DC_DRUGIE_IMIE)
					then <m:DrugieImie>{ data($fml/fml:DC_DRUGIE_IMIE) }</m:DrugieImie>
					else ()
			}
			{
				if($fml/fml:DC_IMIE_OJCA)
					then <m:ImieOjca>{ data($fml/fml:DC_IMIE_OJCA) }</m:ImieOjca>
					else ()
			}
			{
				if($fml/fml:DC_NAZWISKO_PANIENSKIE_MATKI)
					then <m:NazwiskoPanienskieMatki>{ data($fml/fml:DC_NAZWISKO_PANIENSKIE_MATKI) }</m:NazwiskoPanienskieMatki>
					else ()
			}
			{
				if($fml/fml:DC_DATA_URODZENIA)
					then <m:DataUrodzenia>{ data($fml/fml:DC_DATA_URODZENIA) }</m:DataUrodzenia>
					else ()
			}
			{
				if($fml/fml:DC_MIEJSCE_URODZENIA)
					then <m:MiejsceUrodzenia>{ data($fml/fml:DC_MIEJSCE_URODZENIA) }</m:MiejsceUrodzenia>
					else ()
			}
			{
				if($fml/fml:DC_STAN_CYWILNY)
					then <m:StanCywilny>{ data($fml/fml:DC_STAN_CYWILNY) }</m:StanCywilny>
					else ()
			}
			{
				if($fml/fml:DC_KOD_JEZYKA)
					then <m:KodJezyka>{ data($fml/fml:DC_KOD_JEZYKA) }</m:KodJezyka>
					else ()
			}
			{
				if($fml/fml:CI_HASLO_W_CC)
					then <m:HasloWCc>{ data($fml/fml:CI_HASLO_W_CC) }</m:HasloWCc>
					else ()
			}
			{
				if($fml/fml:CI_UCZEN)
					then <m:Uczen>{ data($fml/fml:CI_UCZEN) }</m:Uczen>
					else ()
			}
			{
				if($fml/fml:DC_UCZELNIA)
					then <m:Uczelnia>{ data($fml/fml:DC_UCZELNIA) }</m:Uczelnia>
					else ()
			}
			{
				if($fml/fml:DC_PLANOW_DATA_UKON_SZK)
					then <m:PlanowDataUkonSzk>{ data($fml/fml:DC_PLANOW_DATA_UKON_SZK) }</m:PlanowDataUkonSzk>
					else ()
			}
			{
				if($fml/fml:CI_STANOWISKO)
					then <m:Stanowisko>{ data($fml/fml:CI_STANOWISKO) }</m:Stanowisko>
					else ()
			}
			{
				if($fml/fml:DC_SLUZBA_WOJSKOWA)
					then <m:SluzbaWojskowa>{ data($fml/fml:DC_SLUZBA_WOJSKOWA) }</m:SluzbaWojskowa>
					else ()
			}
			{
				if($fml/fml:CI_ZATRUDNIENIE)
					then <m:Zatrudnienie>{ data($fml/fml:CI_ZATRUDNIENIE) }</m:Zatrudnienie>
					else ()
			}
			{
				if($fml/fml:DC_WSPOLNOTA_MAJATK)
					then <m:WspolnotaMajatk>{ data($fml/fml:DC_WSPOLNOTA_MAJATK) }</m:WspolnotaMajatk>
					else ()
			}
			{
				if($fml/fml:DC_LICZBA_OS_WE_WSP_GOSP_D)
					then <m:LiczbaOsWeWspGospD>{ data($fml/fml:DC_LICZBA_OS_WE_WSP_GOSP_D) }</m:LiczbaOsWeWspGospD>
					else ()
			}
			{
				if($fml/fml:DC_LICZBA_OSOB_NA_UTRZ)
					then <m:LiczbaOsobNaUtrz>{ data($fml/fml:DC_LICZBA_OSOB_NA_UTRZ) }</m:LiczbaOsobNaUtrz>
					else ()
			}
			{
				if($fml/fml:DC_POSIADA_DOM_MIESZK)
					then <m:PosiadaDomMieszk>{ data($fml/fml:DC_POSIADA_DOM_MIESZK) }</m:PosiadaDomMieszk>
					else ()
			}
			{
				if($fml/fml:DC_WARUNKI_MIESZKANIOWE)
					then <m:WarunkiMieszkaniowe>{ data($fml/fml:DC_WARUNKI_MIESZKANIOWE) }</m:WarunkiMieszkaniowe>
					else ()
			}
			{
				if($fml/fml:CI_ZAM_LOKAL)
					then <m:ZamLokal>{ data($fml/fml:CI_ZAM_LOKAL) }</m:ZamLokal>
					else ()
			}
			{
				if($fml/fml:CI_ZAM_LOKAL_INNE)
					then <m:ZamLokalInne>{ data($fml/fml:CI_ZAM_LOKAL_INNE) }</m:ZamLokalInne>
					else ()
			}
			{
				if($fml/fml:CI_LOKAL_OBC_HIPOTEKA)
					then <m:LokalObcHipoteka>{ data($fml/fml:CI_LOKAL_OBC_HIPOTEKA) }</m:LokalObcHipoteka>
					else ()
			}
			{
				if($fml/fml:CI_WARTOSC_HIPOTEKI)
					then <m:WartoscHipoteki>{ data($fml/fml:CI_WARTOSC_HIPOTEKI) }</m:WartoscHipoteki>
					else ()
			}
			{
				if($fml/fml:DC_WYKSZTALCENIE)
					then <m:Wyksztalcenie>{ data($fml/fml:DC_WYKSZTALCENIE) }</m:Wyksztalcenie>
					else ()
			}
			{
				if($fml/fml:DC_KOD_ZAWODU)
					then <m:KodZawodu>{ data($fml/fml:DC_KOD_ZAWODU) }</m:KodZawodu>
					else ()
			}
			{
				if($fml/fml:DC_MIEJSCE_ZATRUDNIENIA)
					then <m:MiejsceZatrudnienia>{ data($fml/fml:DC_MIEJSCE_ZATRUDNIENIA) }</m:MiejsceZatrudnienia>
					else ()
			}
			{
				if($fml/fml:DC_MIES_DOCHOD_NETTO_W_PLN)
					then <m:MiesDochodNettoWPln>{ data($fml/fml:DC_MIES_DOCHOD_NETTO_W_PLN) }</m:MiesDochodNettoWPln>
					else ()
			}
			{
				if($fml/fml:CI_SPOS_OTRZYM_DOCH)
					then <m:SposOtrzymDoch>{ data($fml/fml:CI_SPOS_OTRZYM_DOCH) }</m:SposOtrzymDoch>
					else ()
			}
			{
				if($fml/fml:CI_DOD_DOCHOD_MIES_NETTO)
					then <m:DodDochodMiesNetto>{ data($fml/fml:CI_DOD_DOCHOD_MIES_NETTO) }</m:DodDochodMiesNetto>
					else ()
			}
			{
				if($fml/fml:CI_ZRODLO_DODAT_DOCH)
					then <m:ZrodloDodatDoch>{ data($fml/fml:CI_ZRODLO_DODAT_DOCH) }</m:ZrodloDodatDoch>
					else ()
			}
			{
				if($fml/fml:CI_RODZAJ_OBCIAZENIA)
					then <m:RodzajObciazenia>{ data($fml/fml:CI_RODZAJ_OBCIAZENIA) }</m:RodzajObciazenia>
					else ()
			}
			{
				if($fml/fml:CI_RODZAJ_OBC_INNE)
					then <m:RodzajObcInne>{ data($fml/fml:CI_RODZAJ_OBC_INNE) }</m:RodzajObcInne>
					else ()
			}
			{
				if($fml/fml:CI_LACZNA_WYS_OBC)
					then <m:LacznaWysObc>{ data($fml/fml:CI_LACZNA_WYS_OBC) }</m:LacznaWysObc>
					else ()
			}
			{
				if($fml/fml:CI_POLISY_NA_ZYCIE)
					then <m:PolisyNaZycie>{ data($fml/fml:CI_POLISY_NA_ZYCIE) }</m:PolisyNaZycie>
					else ()
			}
			{
				if($fml/fml:CI_LICZBA_POLIS)
					then <m:LiczbaPolis>{ data($fml/fml:CI_LICZBA_POLIS) }</m:LiczbaPolis>
					else ()
			}
			{
				if($fml/fml:CI_NAZWA_TOW_UBEZP)
					then <m:NazwaTowUbezp>{ data($fml/fml:CI_NAZWA_TOW_UBEZP) }</m:NazwaTowUbezp>
					else ()
			}
			{
				if($fml/fml:CI_SUMA_POLIS_NA_ZYCIE)
					then <m:SumaPolisNaZycie>{ data($fml/fml:CI_SUMA_POLIS_NA_ZYCIE) }</m:SumaPolisNaZycie>
					else ()
			}
			{
				if($fml/fml:CI_AKTUALNE_ZADL_DZIALAL)
					then <m:AktualneZadlDzialal>{ data($fml/fml:CI_AKTUALNE_ZADL_DZIALAL) }</m:AktualneZadlDzialal>
					else ()
			}
			{
				if($fml/fml:DC_KOD_EKD_PRACODAW)
					then <m:KodEkdPracodaw>{ data($fml/fml:DC_KOD_EKD_PRACODAW) }</m:KodEkdPracodaw>
					else ()
			}
			{
				if($fml/fml:DC_DATA_PODJECIA_PRACY)
					then <m:DataPodjeciaPracy>{ data($fml/fml:DC_DATA_PODJECIA_PRACY) }</m:DataPodjeciaPracy>
					else ()
			}
			{
				if($fml/fml:DC_DATA_SMIERCI)
					then <m:DataSmierci>{ data($fml/fml:DC_DATA_SMIERCI) }</m:DataSmierci>
					else ()
			}
			{
				if($fml/fml:DC_UBEZP_OD_NASTEPSTW_NW)
					then <m:UbezpOdNastepstwNw>{ data($fml/fml:DC_UBEZP_OD_NASTEPSTW_NW) }</m:UbezpOdNastepstwNw>
					else ()
			}
			{
				if($fml/fml:DC_UBEZPIECZENIE_EMERYTALNE)
					then <m:UbezpieczenieEmerytalne>{ data($fml/fml:DC_UBEZPIECZENIE_EMERYTALNE) }</m:UbezpieczenieEmerytalne>
					else ()
			}
			{
				if($fml/fml:DC_UBEZP_MAJATKOWE_MIES)
					then <m:UbezpMajatkoweMies>{ data($fml/fml:DC_UBEZP_MAJATKOWE_MIES) }</m:UbezpMajatkoweMies>
					else ()
			}
			{
				if($fml/fml:DC_UBEZPIECZENIA_INNE)
					then <m:UbezpieczeniaInne>{ data($fml/fml:DC_UBEZPIECZENIA_INNE) }</m:UbezpieczeniaInne>
					else ()
			}
			{
				if($fml/fml:CI_KRS)
					then <m:Krs>{ data($fml/fml:CI_KRS) }</m:Krs>
					else ()
			}
			{
				if($fml/fml:DC_EKD)
					then <m:Ekd>{ data($fml/fml:DC_EKD) }</m:Ekd>
					else ()
			}
			{
				if($fml/fml:CI_LICZBA_PLACOWEK)
					then <m:LiczbaPlacowek>{ data($fml/fml:CI_LICZBA_PLACOWEK) }</m:LiczbaPlacowek>
					else ()
			}
			{
				if($fml/fml:DC_LICZBA_ZATRUDNIONYCH)
					then <m:LiczbaZatrudnionych>{ data($fml/fml:DC_LICZBA_ZATRUDNIONYCH) }</m:LiczbaZatrudnionych>
					else ()
			}
			{
				if($fml/fml:DC_NAZWISKO_OSOBY_KONTAKTOWEJ)
					then <m:NazwiskoOsobyKontaktowej>{ data($fml/fml:DC_NAZWISKO_OSOBY_KONTAKTOWEJ) }</m:NazwiskoOsobyKontaktowej>
					else ()
			}
			{
				if($fml/fml:DC_TYTUL_OSOBY_KONTAKTOWEJ)
					then <m:TytulOsobyKontaktowej>{ data($fml/fml:DC_TYTUL_OSOBY_KONTAKTOWEJ) }</m:TytulOsobyKontaktowej>
					else ()
			}
			{
				if($fml/fml:DC_DATA_BANKRUCTWA)
					then <m:DataBankructwa>{ data($fml/fml:DC_DATA_BANKRUCTWA) }</m:DataBankructwa>
					else ()
			}
			{
				if($fml/fml:CI_STRONA_WWW)
					then <m:StronaWww>{ data($fml/fml:CI_STRONA_WWW) }</m:StronaWww>
					else ()
			}
			{
				if($fml/fml:CI_DATA_ROZP_DZIAL)
					then <m:DataRozpDzial>{ data($fml/fml:CI_DATA_ROZP_DZIAL) }</m:DataRozpDzial>
					else ()
			}
			{
				if($fml/fml:CI_DANE_FINANSOWE)
					then <m:DaneFinansowe>{ data($fml/fml:CI_DANE_FINANSOWE) }</m:DaneFinansowe>
					else ()
			}
			{
				if($fml/fml:CI_DATA_SPRAWOZDANIA)
					then <m:DataSprawozdania>{ data($fml/fml:CI_DATA_SPRAWOZDANIA) }</m:DataSprawozdania>
					else ()
			}
			{
				if($fml/fml:CI_KWOTA_ZADLUZENIA)
					then <m:KwotaZadluzenia>{ data($fml/fml:CI_KWOTA_ZADLUZENIA) }</m:KwotaZadluzenia>
					else ()
			}
			{
				if($fml/fml:DC_JEDNOSTKA_KORPORACYJNA)
					then <m:JednostkaKorporacyjna>{ data($fml/fml:DC_JEDNOSTKA_KORPORACYJNA) }</m:JednostkaKorporacyjna>
					else ()
			}
			{
				if($fml/fml:CI_PRZYNAL_CENTR_KORP)
					then <m:PrzynalCentrKorp>{ data($fml/fml:CI_PRZYNAL_CENTR_KORP) }</m:PrzynalCentrKorp>
					else ()
			}
			{
				if($fml/fml:CI_TYP_PODMIOTU)
					then <m:TypPodmiotu>{ data($fml/fml:CI_TYP_PODMIOTU) }</m:TypPodmiotu>
					else ()
			}
			{
				if($fml/fml:CI_NUMER_SWIFT)
					then <m:NumerSwift>{ data($fml/fml:CI_NUMER_SWIFT) }</m:NumerSwift>
					else ()
			}
			{
				if($fml/fml:CI_NUMER_W_REJESTRZE)
					then <m:NumerWRejestrze>{ data($fml/fml:CI_NUMER_W_REJESTRZE) }</m:NumerWRejestrze>
					else ()
			}
			{
				if($fml/fml:CI_NAZWA_ORG_REJESTR)
					then <m:NazwaOrgRejestr>{ data($fml/fml:CI_NAZWA_ORG_REJESTR) }</m:NazwaOrgRejestr>
					else ()
			}
			{
				if($fml/fml:CI_KRAJ_ORG_REJESTR)
					then <m:KrajOrgRejestr>{ data($fml/fml:CI_KRAJ_ORG_REJESTR) }</m:KrajOrgRejestr>
					else ()
			}
			{
				if($fml/fml:CI_MIASTO_ORG_REJESTR)
					then <m:MiastoOrgRejestr>{ data($fml/fml:CI_MIASTO_ORG_REJESTR) }</m:MiastoOrgRejestr>
					else ()
			}
			{
				if($fml/fml:CI_KOD_POCZT_ORG_REJESTR)
					then <m:KodPocztOrgRejestr>{ data($fml/fml:CI_KOD_POCZT_ORG_REJESTR) }</m:KodPocztOrgRejestr>
					else ()
			}
			{
				if($fml/fml:CI_ULICA_ORG_REJESTR)
					then <m:UlicaOrgRejestr>{ data($fml/fml:CI_ULICA_ORG_REJESTR) }</m:UlicaOrgRejestr>
					else ()
			}
			{
				if($fml/fml:CI_NR_POSES_LOKALU_ORG_REJ)
					then <m:NrPosesLokaluOrgRej>{ data($fml/fml:CI_NR_POSES_LOKALU_ORG_REJ) }</m:NrPosesLokaluOrgRej>
					else ()
			}
			{
				if($fml/fml:CI_DATA_WPISU_DO_REJESTR)
					then <m:DataWpisuDoRejestr>{ data($fml/fml:CI_DATA_WPISU_DO_REJESTR) }</m:DataWpisuDoRejestr>
					else ()
			}
			{
				if($fml/fml:CI_STATUS_SIEDZIBY)
					then <m:StatusSiedziby>{ data($fml/fml:CI_STATUS_SIEDZIBY) }</m:StatusSiedziby>
					else ()
			}
			{
				if($fml/fml:CI_AUDYTOR)
					then <m:Audytor>{ data($fml/fml:CI_AUDYTOR) }</m:Audytor>
					else ()
			}
			{
				if($fml/fml:CI_KAPITAL_ZALOZYCIELSKI)
					then <m:KapitalZalozycielski>{ data($fml/fml:CI_KAPITAL_ZALOZYCIELSKI) }</m:KapitalZalozycielski>
					else ()
			}
			{
				if($fml/fml:CI_WARTOSC_AKCJI)
					then <m:WartoscAkcji>{ data($fml/fml:CI_WARTOSC_AKCJI) }</m:WartoscAkcji>
					else ()
			}
			{
				if($fml/fml:CI_WARTOSC_NIERUCHOMOSCI)
					then <m:WartoscNieruchomosci>{ data($fml/fml:CI_WARTOSC_NIERUCHOMOSCI) }</m:WartoscNieruchomosci>
					else ()
			}
			{
				if($fml/fml:CI_WARTOSC_SRODKOW_MATER)
					then <m:WartoscSrodkowMater>{ data($fml/fml:CI_WARTOSC_SRODKOW_MATER) }</m:WartoscSrodkowMater>
					else ()
			}
			{
				if($fml/fml:CI_INNE_AKTYWA)
					then <m:InneAktywa>{ data($fml/fml:CI_INNE_AKTYWA) }</m:InneAktywa>
					else ()
			}
			{
				if($fml/fml:CI_ZOBOWIAZANIA_PODATKOWE)
					then <m:ZobowiazaniaPodatkowe>{ data($fml/fml:CI_ZOBOWIAZANIA_PODATKOWE) }</m:ZobowiazaniaPodatkowe>
					else ()
			}
			{
				if($fml/fml:CI_INNE_ZOBOWIAZANIA)
					then <m:InneZobowiazania>{ data($fml/fml:CI_INNE_ZOBOWIAZANIA) }</m:InneZobowiazania>
					else ()
			}
			{
				if($fml/fml:CI_DATA_AKTUAL_DANYCH_FIN)
					then <m:DataAktualDanychFin>{ data($fml/fml:CI_DATA_AKTUAL_DANYCH_FIN) }</m:DataAktualDanychFin>
					else ()
			}
			{
                          for $x at $occ in $fml/CI_TYP_ADRESU where data($x) = "2"
                                  return
                                     <m:CISGetCustomerCustomerAdresAlt>
						<m:ImieINazwiskoAlt>{ data($fml/fml:DC_IMIE_I_NAZWISKO_ALT[$occ]) }</m:ImieINazwiskoAlt>
				                <m:UlicaAdresAlt>{ data($fml/fml:DC_ULICA_ADRES_ALT[$occ]) }</m:UlicaAdresAlt>
						<m:NrPosesLokaluAdresAlt>{ data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT[$occ]) }</m:NrPosesLokaluAdresAlt>
						<m:MiastoAdresAlt>{ data($fml/fml:DC_MIASTO_ADRES_ALT[$occ]) }</m:MiastoAdresAlt>
						<m:KodPocztowyAdresAlt>{ data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT[$occ]) }</m:KodPocztowyAdresAlt>
						<m:WojewodztwoKrajAdresAlt>{ data($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT[$occ]) }</m:WojewodztwoKrajAdresAlt>
				      </m:CISGetCustomerCustomerAdresAlt>
			}


		</m:CISGetCustomerResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCISGetCustomerResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>