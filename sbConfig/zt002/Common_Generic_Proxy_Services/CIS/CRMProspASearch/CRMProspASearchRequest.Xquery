<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-06-30</con:description>
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMProspASearchRequest($req as element(m:CRMProspASearchRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:ZakresWyszukiwania)
					then <fml:CI_ZAKRES_WYSZUKIWANIA>{ data($req/m:ZakresWyszukiwania) }</fml:CI_ZAKRES_WYSZUKIWANIA>
					else ()
			}
			{
                                for $i in 1 to count($req/m:IdSpolki)
                                return
                                        <fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki[$i]) }</fml:CI_ID_SPOLKI>

			}
			{
				if($req/m:NumerOddzialu)
					then <fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }</fml:DC_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:Sortowanie)
					then <fml:CI_SORTOWANIE>{ data($req/m:Sortowanie) }</fml:CI_SORTOWANIE>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then <fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }</fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:NrPesel)
					then <fml:DC_NR_PESEL>{ data($req/m:NrPesel) }</fml:DC_NR_PESEL>
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then <fml:DC_NR_DOWODU_REGON>{ data($req/m:NrDowoduRegon) }</fml:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($req/m:Nip)
					then <fml:DC_NIP>{ data($req/m:Nip) }</fml:DC_NIP>
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then <fml:DC_NUMER_PASZPORTU>{ data($req/m:NumerPaszportu) }</fml:DC_NUMER_PASZPORTU>
					else ()
			}
			{
				if($req/m:Imie)
					then <fml:DC_IMIE>{ data($req/m:Imie) }</fml:DC_IMIE>
					else ()
			}
			{
				if($req/m:Nazwisko)
					then <fml:DC_NAZWISKO>{ data($req/m:Nazwisko) }</fml:DC_NAZWISKO>
					else ()
			}
			{
				if($req/m:NazwaPelna)
					then <fml:CI_NAZWA_PELNA>{ data($req/m:NazwaPelna) }</fml:CI_NAZWA_PELNA>
					else ()
			}
			{
				if($req/m:NrTelefonu)
					then <fml:DC_NR_TELEFONU>{ data($req/m:NrTelefonu) }</fml:DC_NR_TELEFONU>
					else ()
			}
			{
				if($req/m:JednostkaKorporacyjna)
					then <fml:DC_JEDNOSTKA_KORPORACYJNA>{ data($req/m:JednostkaKorporacyjna) }</fml:DC_JEDNOSTKA_KORPORACYJNA>
					else ()
			}
			{
				if($req/m:DataUrodzenia)
					then <fml:DC_DATA_URODZENIA>{ data($req/m:DataUrodzenia) }</fml:DC_DATA_URODZENIA>
					else ()
			}
			{
				if($req/m:StatusGIODO)
					then <fml:CI_STATUS_GIODO>{ data($req/m:StatusGIODO) }</fml:CI_STATUS_GIODO>
					else ()
			}
			{
                                for $j in 1 to count($req/m:NrKlienta)
                                return
                                        <fml:CI_NUMER_KLIENTA>{ data($req/m:NrKlienta[$j]) }</fml:CI_NUMER_KLIENTA>

			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMProspASearchRequest($body/m:CRMProspASearchRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>