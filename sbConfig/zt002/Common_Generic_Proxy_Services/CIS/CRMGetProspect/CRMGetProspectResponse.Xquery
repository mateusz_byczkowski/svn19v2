<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetProspectResponse($fml as element(fml:FML32))
	as element(m:CRMGetProspectResponse) {
		<m:CRMGetProspectResponse>
			{
				if($fml/fml:CI_ID_KLIENTA)
					then <m:IdKlienta>{ data($fml/fml:CI_ID_KLIENTA) }</m:IdKlienta>
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA_REJ)
					then <m:SkpPracownikaRej>{ data($fml/fml:CI_SKP_PRACOWNIKA_REJ) }</m:SkpPracownikaRej>
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA)
					then <m:SkpPracownika>{ data($fml/fml:CI_SKP_PRACOWNIKA) }</m:SkpPracownika>
					else ()
			}
			{
				if($fml/fml:CI_ID_SPOLKI)
					then <m:IdSpolki>{ data($fml/fml:CI_ID_SPOLKI) }</m:IdSpolki>
					else ()
			}
			{
				if($fml/fml:DC_NUMER_ODDZIALU)
					then <m:NumerOddzialu>{ data($fml/fml:DC_NUMER_ODDZIALU) }</m:NumerOddzialu>
					else ()
			}
			{
				if($fml/fml:DC_TYP_KLIENTA)
					then <m:TypKlienta>{ data($fml/fml:DC_TYP_KLIENTA) }</m:TypKlienta>
					else ()
			}
			{
				if($fml/fml:DC_IMIE)
					then <m:Imie>{ data($fml/fml:DC_IMIE) }</m:Imie>
					else ()
			}
			{
				if($fml/fml:DC_NAZWISKO)
					then <m:Nazwisko>{ data($fml/fml:DC_NAZWISKO) }</m:Nazwisko>
					else ()
			}
			{
				if($fml/fml:DC_NAZWA)
					then <m:Nazwa>{ data($fml/fml:DC_NAZWA) }</m:Nazwa>
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then <m:NrPesel>{ data($fml/fml:DC_NR_PESEL) }</m:NrPesel>
					else ()
			}
			{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then <m:NrDowoduRegon>{ data($fml/fml:DC_NR_DOWODU_REGON) }</m:NrDowoduRegon>
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then <m:Nip>{ data($fml/fml:DC_NIP) }</m:Nip>
					else ()
			}
			{
				if($fml/fml:DC_NUMER_PASZPORTU)
					then <m:NumerPaszportu>{ data($fml/fml:DC_NUMER_PASZPORTU) }</m:NumerPaszportu>
					else ()
			}
			{
				if($fml/fml:CI_UDOSTEP_GRUPA)
					then <m:UdostepGrupa>{ data($fml/fml:CI_UDOSTEP_GRUPA) }</m:UdostepGrupa>
					else ()
			}
			{
				if($fml/fml:DC_KRAJ)
					then <m:Kraj>{ data($fml/fml:DC_KRAJ) }</m:Kraj>
					else ()
			}
			{
				if($fml/fml:DC_WOJEWODZTWO)
					then <m:Wojewodztwo>{ data($fml/fml:DC_WOJEWODZTWO) }</m:Wojewodztwo>
					else ()
			}
			{
				if($fml/fml:DC_KOD_POCZTOWY_DANE_PODST)
					then <m:KodPocztowyDanePodst>{ data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST) }</m:KodPocztowyDanePodst>
					else ()
			}
			{
				if($fml/fml:DC_MIASTO_DANE_PODST)
					then <m:MiastoDanePodst>{ data($fml/fml:DC_MIASTO_DANE_PODST) }</m:MiastoDanePodst>
					else ()
			}
			{
				if($fml/fml:DC_ULICA_DANE_PODST)
					then <m:UlicaDanePodst>{ data($fml/fml:DC_ULICA_DANE_PODST) }</m:UlicaDanePodst>
					else ()
			}
			{
				if($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST)
					then <m:NrPosesLokaluDanePodst>{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST) }</m:NrPosesLokaluDanePodst>
					else ()
			}
			{
				if($fml/fml:CI_ADRES_DOD)
					then <m:AdresDod>{ data($fml/fml:CI_ADRES_DOD) }</m:AdresDod>
					else ()
			}
			{
				if($fml/fml:DC_NR_TELEFONU)
					then <m:NrTelefonu>{ data($fml/fml:DC_NR_TELEFONU) }</m:NrTelefonu>
					else ()
			}
			{
				if($fml/fml:DC_NR_TELEF_KOMORKOWEGO)
					then <m:NrTelefKomorkowego>{ data($fml/fml:DC_NR_TELEF_KOMORKOWEGO) }</m:NrTelefKomorkowego>
					else ()
			}
			{
				if($fml/fml:DC_NUMER_FAKSU)
					then <m:NumerFaksu>{ data($fml/fml:DC_NUMER_FAKSU) }</m:NumerFaksu>
					else ()
			}
			{
				if($fml/fml:DC_ADRES_E_MAIL)
					then <m:AdresEMail>{ data($fml/fml:DC_ADRES_E_MAIL) }</m:AdresEMail>
					else ()
			}
			{
				if($fml/fml:CI_NOTATKI)
					then <m:Notatki>{ data($fml/fml:CI_NOTATKI) }</m:Notatki>
					else ()
			}
			{
				if($fml/fml:CI_DATA_WPROWADZENIA)
					then <m:DataWprowadzenia>{ data($fml/fml:CI_DATA_WPROWADZENIA) }</m:DataWprowadzenia>
					else ()
			}
		</m:CRMGetProspectResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetProspectResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>