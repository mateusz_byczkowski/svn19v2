<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCusLstCntResponse($fml as element(fml:FML32))
	as element(m:CRMGetCusLstCntResponse) {
		<m:CRMGetCusLstCntResponse>
			{
				let $CI_LISTA_KONTAKTOW := $fml/fml:CI_LISTA_KONTAKTOW return
					if($CI_LISTA_KONTAKTOW)
						then <m:ListaKontaktow>{ data($CI_LISTA_KONTAKTOW) }</m:ListaKontaktow>
						else ()
			}
		</m:CRMGetCusLstCntResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetCusLstCntResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>