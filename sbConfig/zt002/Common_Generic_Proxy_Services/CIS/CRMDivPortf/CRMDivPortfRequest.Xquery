<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMDivPortfRequest($req as element(m:CRMDivPortfRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:SkpPracownikaRej)
					then <fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:SkpPracownikaRej) }</fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then <fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }</fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:Relacje)
					then <fml:CI_RELACJE>{ data($req/m:Relacje) }</fml:CI_RELACJE>
					else ()
			}
			{
				for $v in $req/m:InnePortfele
				return
					<fml:CI_INNE_PORTFELE>{ data($v) }</fml:CI_INNE_PORTFELE>
			}
                        {
                                if ($req/m:KlasaObslugi)
                                        then <fml:CI_KLASA_OBSLUGI>{ data($req/m:KlasaObslugi) }</fml:CI_KLASA_OBSLUGI>
                                        else ()
                        }
                        {
                                if ($req/m:TypKlienta)
                                        then <fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }</fml:DC_TYP_KLIENTA>
                                        else ()
                        }
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMDivPortfRequest($body/m:CRMDivPortfRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>