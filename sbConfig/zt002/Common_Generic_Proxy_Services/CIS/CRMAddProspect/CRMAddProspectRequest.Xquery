<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddProspectRequest($req as element(m:CRMAddProspectRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:SkpPracownikaRej)
					then <fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:SkpPracownikaRej) }</fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then <fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }</fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then <fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }</fml:DC_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:Identyfikacja)
					then <fml:CI_IDENTYFIKACJA>{ data($req/m:Identyfikacja) }</fml:CI_IDENTYFIKACJA>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then <fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }</fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:Imie)
					then <fml:DC_IMIE>{ data($req/m:Imie) }</fml:DC_IMIE>
					else ()
			}
			{
				if($req/m:Nazwisko)
					then <fml:DC_NAZWISKO>{ data($req/m:Nazwisko) }</fml:DC_NAZWISKO>
					else ()
			}
			{
				if($req/m:NazwaPelna)
					then <fml:CI_NAZWA_PELNA>{ data($req/m:NazwaPelna) }</fml:CI_NAZWA_PELNA>
					else ()
			}
			{
				if($req/m:NrPesel)
					then <fml:DC_NR_PESEL>{ data($req/m:NrPesel) }</fml:DC_NR_PESEL>
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then <fml:DC_NR_DOWODU_REGON>{ data($req/m:NrDowoduRegon) }</fml:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($req/m:Nip)
					then <fml:DC_NIP>{ data($req/m:Nip) }</fml:DC_NIP>
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then <fml:DC_NUMER_PASZPORTU>{ data($req/m:NumerPaszportu) }</fml:DC_NUMER_PASZPORTU>
					else ()
			}
			{
				if($req/m:UdostepGrupa)
					then <fml:CI_UDOSTEP_GRUPA>{ data($req/m:UdostepGrupa) }</fml:CI_UDOSTEP_GRUPA>
					else ()
			}
			{
				if($req/m:Kraj)
					then <fml:DC_KRAJ>{ data($req/m:Kraj) }</fml:DC_KRAJ>
					else ()
			}
			{
				if($req/m:Wojewodztwo)
					then <fml:DC_WOJEWODZTWO>{ data($req/m:Wojewodztwo) }</fml:DC_WOJEWODZTWO>
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then <fml:DC_KOD_POCZTOWY_DANE_PODST>{ data($req/m:KodPocztowyDanePodst) }</fml:DC_KOD_POCZTOWY_DANE_PODST>
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then <fml:DC_MIASTO_DANE_PODST>{ data($req/m:MiastoDanePodst) }</fml:DC_MIASTO_DANE_PODST>
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then <fml:DC_ULICA_DANE_PODST>{ data($req/m:UlicaDanePodst) }</fml:DC_ULICA_DANE_PODST>
					else ()
			}
			{
				if($req/m:NrPosesLokaluDanePodst)
					then <fml:DC_NR_POSES_LOKALU_DANE_PODST>{ data($req/m:NrPosesLokaluDanePodst) }</fml:DC_NR_POSES_LOKALU_DANE_PODST>
					else ()
			}
			{
				if($req/m:AdresDod)
					then <fml:CI_ADRES_DOD>{ data($req/m:AdresDod) }</fml:CI_ADRES_DOD>
					else ()
			}
			{
				if($req/m:NrTelefonu)
					then <fml:DC_NR_TELEFONU>{ data($req/m:NrTelefonu) }</fml:DC_NR_TELEFONU>
					else ()
			}
			{
				if($req/m:NrTelefKomorkowego)
					then <fml:DC_NR_TELEF_KOMORKOWEGO>{ data($req/m:NrTelefKomorkowego) }</fml:DC_NR_TELEF_KOMORKOWEGO>
					else ()
			}
			{
				if($req/m:NumerFaksu)
					then <fml:DC_NUMER_FAKSU>{ data($req/m:NumerFaksu) }</fml:DC_NUMER_FAKSU>
					else ()
			}
			{
				if($req/m:AdresEMail)
					then <fml:DC_ADRES_E_MAIL>{ data($req/m:AdresEMail) }</fml:DC_ADRES_E_MAIL>
					else ()
			}
			{
				if($req/m:Notatki)
					then <fml:CI_NOTATKI>{ data($req/m:Notatki) }</fml:CI_NOTATKI>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMAddProspectRequest($body/m:CRMAddProspectRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>