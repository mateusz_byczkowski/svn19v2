<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustModHResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustModHResponse) {
		<m:CRMGetCustModHResponse>
			{
				if($fml/fml:CI_NUMER_PACZKI_STR)
					then <m:NumerPaczkiStr>{ data($fml/fml:CI_NUMER_PACZKI_STR) }</m:NumerPaczkiStr>
					else ()
			}
			{

				let $CI_ID_OPERACJI := $fml/fml:CI_ID_OPERACJI
				let $CI_BUDOWA_PORTFELA := $fml/fml:CI_BUDOWA_PORTFELA
				let $CI_CZAS_ZMIANY := $fml/fml:CI_CZAS_ZMIANY
				let $CIE_TYP_ZMIANY := $fml/fml:CIE_TYP_ZMIANY
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $DC_NUMER_ODDZIALU := $fml/fml:DC_NUMER_ODDZIALU
				let $CI_ID_PORTFELA_AKT := $fml/fml:CI_ID_PORTFELA_AKT
				let $CI_SKP_PRACOWNIKA_AKT := $fml/fml:CI_SKP_PRACOWNIKA_AKT
				let $CI_KLASA_OBSLUGI_AKT := $fml/fml:CI_KLASA_OBSLUGI_AKT
				let $CI_NOWY_NUMER_ODDZIALU := $fml/fml:CI_NOWY_NUMER_ODDZIALU
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $CI_POWOD_ZMIANY := $fml/fml:CI_POWOD_ZMIANY
				let $CI_SKP_PRACOWNIKA_REJ := $fml/fml:CI_SKP_PRACOWNIKA_REJ
				for $it at $p in $fml/fml:CI_ID_OPERACJI
				return
					<m:Zmiana>
					{
						if($CI_ID_OPERACJI[$p])
							then <m:IdOperacji>{ data($CI_ID_OPERACJI[$p]) }</m:IdOperacji>
						else ()
					}
					{
						if($CI_BUDOWA_PORTFELA[$p])
							then <m:BudowaPortfela>{ data($CI_BUDOWA_PORTFELA[$p]) }</m:BudowaPortfela>
						else ()
					}
					{
						if($CI_CZAS_ZMIANY[$p])
							then <m:CzasZmiany>{ data($CI_CZAS_ZMIANY[$p]) }</m:CzasZmiany>
						else ()
					}
					{
						if($CIE_TYP_ZMIANY[$p])
							then <m:TypZmiany>{ data($CIE_TYP_ZMIANY[$p]) }</m:TypZmiany>
						else ()
					}
					{
						if($DC_TYP_KLIENTA[$p])
							then <m:TypKlienta>{ data($DC_TYP_KLIENTA[$p]) }</m:TypKlienta>
						else ()
					}
					{
						if($DC_NUMER_KLIENTA[$p])
							then <m:NumerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }</m:NumerKlienta>
						else ()
					}
					{
						if($DC_IMIE[$p])
							then <m:Imie>{ data($DC_IMIE[$p]) }</m:Imie>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then <m:Nazwisko>{ data($DC_NAZWISKO[$p]) }</m:Nazwisko>
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then <m:NazwaPelna>{ data($CI_NAZWA_PELNA[$p]) }</m:NazwaPelna>
						else ()
					}
					{
						if($DC_NUMER_ODDZIALU[$p])
							then <m:NumerOddzialu>{ data($DC_NUMER_ODDZIALU[$p]) }</m:NumerOddzialu>
						else ()
					}
					{
						if($CI_ID_PORTFELA_AKT[$p])
							then <m:IdPortfelaAkt>{ data($CI_ID_PORTFELA_AKT[$p]) }</m:IdPortfelaAkt>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_AKT[$p])
							then <m:SkpPracownikaAkt>{ data($CI_SKP_PRACOWNIKA_AKT[$p]) }</m:SkpPracownikaAkt>
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI_AKT[$p])
							then <m:KlasaObslugiAkt>{ data($CI_KLASA_OBSLUGI_AKT[$p]) }</m:KlasaObslugiAkt>
						else ()
					}
					{
						if($CI_NOWY_NUMER_ODDZIALU[$p])
							then <m:NowyNumerOddzialu>{ data($CI_NOWY_NUMER_ODDZIALU[$p]) }</m:NowyNumerOddzialu>
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then <m:IdPortfela>{ data($CI_ID_PORTFELA[$p]) }</m:IdPortfela>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA[$p])
							then <m:SkpPracownika>{ data($CI_SKP_PRACOWNIKA[$p]) }</m:SkpPracownika>
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI[$p])
							then <m:KlasaObslugi>{ data($CI_KLASA_OBSLUGI[$p]) }</m:KlasaObslugi>
						else ()
					}
					{
						if($CI_POWOD_ZMIANY[$p])
							then <m:PowodZmiany>{ data($CI_POWOD_ZMIANY[$p]) }</m:PowodZmiany>
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_REJ[$p])
							then <m:SkpPracownikaRej>{ data($CI_SKP_PRACOWNIKA_REJ[$p]) }</m:SkpPracownikaRej>
						else ()
					}
					</m:Zmiana>
			}

		</m:CRMGetCustModHResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetCustModHResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>