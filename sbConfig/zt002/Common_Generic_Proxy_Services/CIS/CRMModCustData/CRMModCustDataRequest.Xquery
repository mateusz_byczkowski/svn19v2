<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMModCustDataRequest($req as element(m:CRMModCustDataRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:NumerKlienta)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }</fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				for $v in $req/m:IdPola
				return
					<fml:CI_ID_POLA>{ data($v) }</fml:CI_ID_POLA>
			}
			{
				for $v in $req/m:Wartosc
				return
					<fml:CI_WARTOSC>{ data($v) }</fml:CI_WARTOSC>
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMModCustDataRequest($body/m:CRMModCustDataRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>