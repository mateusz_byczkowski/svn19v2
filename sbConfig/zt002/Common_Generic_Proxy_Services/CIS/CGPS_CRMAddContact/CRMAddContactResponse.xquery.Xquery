<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddContactResponse($fml as element(fml:FML32))
	as element(m:CRMAddContactResponse) {
		<m:CRMAddContactResponse>
			{
				if($fml/fml:CI_ID_KONT)
					then <m:IdKont>{ data($fml/fml:CI_ID_KONT) }</m:IdKont>
					else ()
			}
		</m:CRMAddContactResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMAddContactResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>