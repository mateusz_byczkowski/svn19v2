<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddContactRequest($req as element(m:CRMAddContactRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:IdWewPrac)
					then <fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }</fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:Opcja)
					then <fml:CI_OPCJA>{ data($req/m:Opcja) }</fml:CI_OPCJA>
					else ()
			}
			{
				if($req/m:SkpPracownikaRej)
					then <fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:SkpPracownikaRej) }</fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:NumerOddzialu and string-length(data($req/m:NumerOddzialu)) >0)
					(:then <fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }</fml:DC_NUMER_ODDZIALU>:)
					then <fml:CI_NR_ODDZIALU>{ data($req/m:NumerOddzialu) }</fml:CI_NR_ODDZIALU>
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then <fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }</fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			(:{
				if($req/m:NumerKlienta)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }</fml:DC_NUMER_KLIENTA>
					else ()
			}:)
			{
				if($req/m:NumerKlientaWew)
				(:	then <fml:CI_NUMER_KLIENTA>{ data($req/m:NumerKlientaWew) }</fml:CI_NUMER_KLIENTA> 
                                       Niewlasciwe pole podawane przez szrek - zmiana mapowania :)
                                            then <fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlientaWew) }</fml:DC_NUMER_KLIENTA> 
					else ()
			}
			{
				if($req/m:DataKont)
					then <fml:CI_DATA_KONT>{ data($req/m:DataKont) }</fml:CI_DATA_KONT>
					else ()
			}
			{
				if($req/m:CelKont)
					then <fml:CI_CEL_KONT>{ data($req/m:CelKont) }</fml:CI_CEL_KONT>
					else ()
			}
			{
				if($req/m:TypKont)
					then <fml:CI_TYP_KONT>{ data($req/m:TypKont) }</fml:CI_TYP_KONT>
					else ()
			}
			{
				if($req/m:RodzajKont)
					then <fml:CI_RODZAJ_KONT>{ data($req/m:RodzajKont) }</fml:CI_RODZAJ_KONT>
					else ()
			}
			{
				if($req/m:IdZws  and string-length(data($req/m:IdZws)) >0)
					then <fml:CI_ID_ZWS>{ data($req/m:IdZws) }</fml:CI_ID_ZWS>
					else ()
			}
			{
				if($req/m:IdKamp and string-length(data($req/m:IdKamp)) >0)
					then <fml:CI_ID_KAMP>{ data($req/m:IdKamp) }</fml:CI_ID_KAMP>
					else ()
			}
			{
				if($req/m:SposKont)
					then <fml:CI_SPOS_KONT>{ data($req/m:SposKont) }</fml:CI_SPOS_KONT>
					else ()
			}
			{
				if($req/m:MiejsceSpotkania)
					then <fml:CI_MIEJSCE_SPOTKANIA>{ data($req/m:MiejsceSpotkania) }</fml:CI_MIEJSCE_SPOTKANIA>
					else ()
			}
			{
				if($req/m:WynikKontaktu)
					then <fml:CI_WYNIK_KONTAKTU>{ data($req/m:WynikKontaktu) }</fml:CI_WYNIK_KONTAKTU>
					else ()
			}
			{
				if($req/m:NotatKont)
					then <fml:CI_NOTAT_KONT>{ data($req/m:NotatKont) }</fml:CI_NOTAT_KONT>
					else ()
			}
			{
				if($req/m:Produkty)
					then <fml:CI_PRODUKTY>{ data($req/m:Produkty) }</fml:CI_PRODUKTY>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMAddContactRequest($body/m:CRMAddContactRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>