<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMDelAuthRequest($req as element(m:CRMDelAuthRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:IdWewPrac)
					then <fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }</fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				for $v in $req/m:NumerKlienta
				return
					<fml:DC_NUMER_KLIENTA>{ data($v) }</fml:DC_NUMER_KLIENTA>
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMDelAuthRequest($body/m:CRMDelAuthRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>