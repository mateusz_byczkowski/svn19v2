<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAsgnPortfEmpRequest($req as element(m:CRMAsgnPortfEmpRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:SkpPracownikaRej)
					then <fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:SkpPracownikaRej) }</fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then <fml:CI_SKP_PRACOWNIKA>{ data($req/m:SkpPracownika) }</fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:ImiePrac)
					then <fml:CI_IMIE_PRAC>{ data($req/m:ImiePrac) }</fml:CI_IMIE_PRAC>
					else ()
			}
			{
				if($req/m:NazwPrac)
					then <fml:CI_NAZW_PRAC>{ data($req/m:NazwPrac) }</fml:CI_NAZW_PRAC>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then <fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }</fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:Opcja)
					then <fml:CI_OPCJA>{ data($req/m:Opcja) }</fml:CI_OPCJA>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMAsgnPortfEmpRequest($body/m:CRMAsgnPortfEmpRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>