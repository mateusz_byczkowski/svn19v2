<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustByProductResponse($bdy as element(m:PRIMEGetCustByProductResponse ))
	as element(fml:FML32) {
             let $NrRachunku := $bdy/m:PRIMEGetCustByProductResult/m:DC_NR_RACHUNKU
             let $NrAlternatywny := $bdy/m:PRIMEGetCustByProductResult/m:DC_NR_ALTERNATYWNY
             let $Pojedynczy:= $bdy/m:PRIMEGetCustByProductResult/m:B_POJEDYNCZY
             let $ProductAtt1 := $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_ATT1
             let $ProductAtt2 := $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_ATT2
             let $ProductAtt3 := $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_ATT3
             let $ProductAtt4 := $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_ATT4
             let $ProductAtt5 := $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_ATT5
             let $Saldo:= $bdy/m:PRIMEGetCustByProductResult/m:B_SALDO
             let $DostSrodki:= $bdy/m:PRIMEGetCustByProductResult/m:B_DOST_SRODKI
             let $Blokada:= $bdy/m:PRIMEGetCustByProductResult/m:B_BLOKADA
             let $KodWaluty:= $bdy/m:PRIMEGetCustByProductResult/m:B_KOD_WALUTY
             let $DOtwarcia:= $bdy/m:PRIMEGetCustByProductResult/m:B_D_OTWARCIA
             let $DataOstOper:= $bdy/m:PRIMEGetCustByProductResult/m:B_DATA_OST_OPER
             let $Limit1:= $bdy/m:PRIMEGetCustByProductResult/m:B_LIMIT1
             let $ProductNameOryginal:= $bdy/m:PRIMEGetCustByProductResult/m:CI_PRODUCT_NAME_ORYGINAL
             let $NumerKlienta:= $bdy/m:PRIMEGetCustByProductResult/m:DC_NUMER_KLIENTA
          
             return
             <FML32>    
	         {  
                    if  ($NrRachunku) 
                          then  <fml:DC_NR_RACHUNKU>{substring(data($NrRachunku),2)}</fml:DC_NR_RACHUNKU>
                          else   <fml:DC_NR_RACHUNKU nil="true" />
                 }
	         {  
                    if  ($NrAlternatywny ) 
                          then  <fml:DC_NR_RACHUNKU_W_SYSTEMIE>{data($NrAlternatywny )}</fml:DC_NR_RACHUNKU_W_SYSTEMIE>
                          else   <fml:DC_NR_RACHUNKU_W_SYSTEMIE nil="true" />
                 }
	         {  
                    if  ($Pojedynczy) 
                          then  <fml:B_POJEDYNCZY>{data($Pojedynczy)}</fml:B_POJEDYNCZY>
                          else   <fml:B_POJEDYNCZY nil="true" />
                 }
	         {  
                    if  ($ProductAtt1 and data($ProductAtt1)!="0") 
                          then  <fml:CI_PRODUCT_ATT1>{data($ProductAtt1)}</fml:CI_PRODUCT_ATT1>
                          else   <fml:CI_PRODUCT_ATT1 nil="true" />
                 }
	        (: {  
                    if  ($ProductAtt2 and data($ProductAtt2)!="0") 
                          then  <fml:CI_PRODUCT_ATT2>{data($ProductAtt2)}</fml:CI_PRODUCT_ATT2>
                          else   <fml:CI_PRODUCT_ATT2 nil="true" />
                 }
	         {  
                    if  ($ProductAtt3 and data($ProductAtt3)!="0") 
                          then  <fml:CI_PRODUCT_ATT3>{data($ProductAtt3)}</fml:CI_PRODUCT_ATT3>
                          else   <fml:CI_PRODUCT_ATT3 nil="true" />
                 }
	         {  
                    if  ($ProductAtt4 and data($ProductAtt4)!="0") 
                          then  <fml:CI_PRODUCT_ATT4>{data($ProductAtt4)}</fml:CI_PRODUCT_ATT4>
                          else   <fml:CI_PRODUCT_ATT4 nil="true" />
                 }
	         {  
                    if  ($ProductAtt5 and data($ProductAtt5)!="0") 
                          then  <fml:CI_PRODUCT_ATT5>{data($ProductAtt5)}</fml:CI_PRODUCT_ATT5>
                          else   <fml:CI_PRODUCT_ATT5 nil="true" />
                 }:)
                 <fml:CI_PRODUCT_ATT2 nil="true" />
                 <fml:CI_PRODUCT_ATT3 nil="true" />
                 <fml:CI_PRODUCT_ATT4 nil="true" />
                 <fml:CI_PRODUCT_ATT5 nil="true" />
                {  
                    if  ($Saldo) 
                          then  <fml:B_SALDO>{data($Saldo)}</fml:B_SALDO>
                          else   <fml:B_SALDO nil="true" />
                 }
	         {  
                    if  ($DostSrodki) 
                          then  <fml:B_DOST_SRODKI>{data($DostSrodki)}</fml:B_DOST_SRODKI>
                          else   <fml:B_DOST_SRODKI nil="true" />
                 }
	         {  
                    if  ($Blokada) 
                          then  <fml:B_BLOKADA>{data($Blokada)}</fml:B_BLOKADA>
                          else   <fml:B_BLOKADA nil="true" />
                 }
	         {  
                    if  ($KodWaluty) 
                          then  <fml:B_KOD_WALUTY>{data($KodWaluty)}</fml:B_KOD_WALUTY>
                          else   <fml:B_KOD_WALUTY nil="true" />
                 }
	         {  
                    if  ($DOtwarcia) 
                          then  <fml:B_D_OTWARCIA>{data($DOtwarcia)}</fml:B_D_OTWARCIA>
                          else   <fml:B_D_OTWARCIA nil="true" />
                 }
	         {  
                    if  ($DataOstOper) 
                          then  <fml:B_DATA_OST_OPER>{data($DataOstOper)}</fml:B_DATA_OST_OPER>
                          else   <fml:B_DATA_OST_OPER nil="true" />
                 }
	         {  
                    if  ($Limit1) 
                          then  <fml:B_LIMIT1>{data($Limit1)}</fml:B_LIMIT1>
                          else   <fml:B_LIMIT1 nil="true" />
                 }
	         {  
                    if  ($ProductNameOryginal) 
                          then  <fml:CI_PRODUCT_NAME_ORYGINAL>{data($ProductNameOryginal)}</fml:CI_PRODUCT_NAME_ORYGINAL>
                          else   <fml:CI_PRODUCT_NAME_ORYGINAL nil="true" />
                 }
	         {  
                    if  ($NumerKlienta) 
                          then  <fml:DC_NUMER_KLIENTA>{data($NumerKlienta)}</fml:DC_NUMER_KLIENTA>
                          else   <fml:DC_NUMER_KLIENTA nil="true" />
                 }
                {  
                    if  ($NumerKlienta) 
                          then  <fml:CI_RACHUNEK_ADR_ALT>N</fml:CI_RACHUNEK_ADR_ALT>    
                          else   ()
                 } 						
	               
             </FML32>
               
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ 
  if($body/m:PRIMEGetCustByProductResponse)
     then  xf:mapPRIMEGetCustByProductResponse($body/m:PRIMEGetCustByProductResponse) 
     else
     if ($body/fml:FML32)
        then $body/fml:FML32
         else()
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>