<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapEGERIAGetCustProdDet($fml as element(fml:FML32))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($fml/fml:DC_NR_RACHUNKU)
					then <fml:EG_NR_UMOWY>{ data($fml/fml:DC_NR_RACHUNKU) }</fml:EG_NR_UMOWY>
					else  ()
                         }
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapEGERIAGetCustProdDet($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>