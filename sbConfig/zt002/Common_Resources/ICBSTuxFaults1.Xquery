<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>v.1.1  2009-12-16  LK  PT58</con:description>
    <con:xquery><![CDATA[declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="urn:errors.hlbsentities.be.dcl";
declare namespace fml = "";

declare variable $fault external;
declare variable $body external;

declare function local:fault($faultString as xs:string, $detail as element()) as element(soap-env:Fault) {
    <soap-env:Fault>
      <faultcode>soapenv:Server.userException</faultcode> 
      <faultstring>{ $faultString }</faultstring> 
      <detail>{ $detail }</detail>
    </soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string, $errorDescription as xs:string) as element()* {
    <f:exceptionItem>
      <f:errorCode1>{ $errorCode1 }</f:errorCode1>
      <f:errorCode2>{ $errorCode2 }</f:errorCode2>
      <f:errorDescription>{ $errorDescription }</f:errorDescription>
    </f:exceptionItem>
};

declare function local:errors2($errorCode1 as xs:string, $errorCode2 as xs:string, $errorDescription as xs:string, $body as element()) as element()* {
  <urn:ServiceFailException  xmlns:urn="urn:errors.hlbsentities.be.dcl">
    <f:exceptionItem>
      <f:errorCode1>{ $errorCode1 }</f:errorCode1>
      <f:errorCode2>{ $errorCode2 }</f:errorCode2>
      <f:errorDescription>{ $errorDescription }</f:errorDescription>
    </f:exceptionItem>
    {for $it at $p in $body/fml:FML32/fml:DC_OPIS_BLEDU
      return
        <f:exceptionItem>
          <f:errorCode1>{ $errorCode1 }</f:errorCode1>
          <f:errorCode2>{ $errorCode2 }</f:errorCode2>
          <f:errorDescription>{ data($body/fml:FML32/fml:DC_OPIS_BLEDU[$p]) }</f:errorDescription>
        </f:exceptionItem>
        }
  </urn:ServiceFailException> 
};

<soap-env:Body>
{
  (: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
  let $reason := fn:substring-after(fn:substring-before($fault/ctx:reason, "):"), "(")
  let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($fault/ctx:reason, ":"), ":"), ":")
  
  return
    if($reason = "6") then
      local:fault("errors.hlbsentities.be.dcl.ServiceException", element f:ServiceException { local:errors($reason, $urcode, "Brak usługi tuxedo") })
    else if($reason = "12") then
      local:fault("errors.hlbsentities.be.dcl.ServiceException", element f:ServiceException { local:errors($reason, $urcode, "Usługa tuxedo jest niedostępna") })
    else if($reason = "13") then
      local:fault("errors.hlbsentities.be.dcl.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode, "Timeout") })
    else if($reason = "11") then
      if($urcode = "100") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode, "Interfejs DC nieaktywny, przetwarzanie niemożliwe") })
      else if($urcode = "101") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode, "Błąd zapisu do bufora FML") })
      else if($urcode = "102") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors2($reason, $urcode, "Błędny bufor wejściowy", $body) )
      else if($urcode = "103") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors2($reason, $urcode, "Brak danych", $body) )
      else if($urcode = "10") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors2($reason, $urcode, "Błąd w czasie przetwarzania przez interfejs DC (operacja niezaksięgowana)", $body) )
      else if($urcode = "11") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors2($reason, $urcode, "Powtórzona transakcja o podanym ID", $body) )
      else if($urcode = "12") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors2($reason, $urcode, "Błąd w czasie przetwarzania przez CRS (operacja niezaksięgowana lub zaksięgowana częściowo)", $body) )
      else if($urcode = "1") then
        local:fault("errors.hlbsentities.be.dcl.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode, "Timeout") })
      else
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors2($reason, $urcode, "Błąd usługi", $body) )
    else
      local:fault("errors.hlbsentities.be.dcl.ServiceException", element f:ServiceException { local:errors($reason, $urcode, "Service Exception") })
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>