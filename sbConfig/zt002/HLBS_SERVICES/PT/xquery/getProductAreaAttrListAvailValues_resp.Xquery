<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAreaAttrListAvailValues_resp/";
declare namespace ns0 = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductAreaAttrListAvailValues_resp($fml as element())
    as element() {
	<ns0:invokeResponse>
        <ns0:prodAreaAttrListAvailValues>
            {
                for $i in 1 to count($fml/PT_ID_AREA_ATTR_LIST_VAL)
                return
                    <ns0:entities.productstree.ProductAreaAttrListAvailVal>
                                <ns0:value>{ data($fml/PT_VALUE[$i]) }</ns0:value>                                    
                                <ns0:valueDescription>{ data($fml/PT_DESCRIPTION[$i]) }</ns0:valueDescription>                                    
                                <ns0:connectionField>{ data($fml/PT_CONNECTION_FIELD[$i]) }</ns0:connectionField>                                    
                                <ns0:idProductAreaAttrListAvailVal>{ data($fml/PT_ID_AREA_ATTR_LIST_VAL[$i]) }</ns0:idProductAreaAttrListAvailVal>                                    
                            	<ns0:idProductAreaAttrtibutes>{ data($fml/PT_ID_AREA_ATTRIBUTES[$i]) }</ns0:idProductAreaAttrtibutes>
                    </ns0:entities.productstree.ProductAreaAttrListAvailVal>
            }
        </ns0:prodAreaAttrListAvailValues>
    </ns0:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductAreaAttrListAvailValues_resp($fml)]]></con:xquery>
</con:xqueryEntry>