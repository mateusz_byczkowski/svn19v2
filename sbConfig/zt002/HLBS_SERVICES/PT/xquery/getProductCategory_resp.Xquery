<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductCategory_resp/";
declare namespace ns0 = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductCategory_resp($fml as element())
    as element(ns0:invokeResponse) {
        <ns0:invokeResponse>
            <ns0:productCategoryEntityOut>
                <ns0:entities.productstree.ProductCategory>
                            <ns0:codeProductCategory>{ data($fml/PT_CODE_PRODUCT_CATEGORY) }</ns0:codeProductCategory>
                            <ns0:polishCategoryName>{ data($fml/PT_POLISH_NAME) }</ns0:polishCategoryName>
                            <ns0:englishCategoryName>{ data($fml/PT_ENGLISH_NAME) }</ns0:englishCategoryName>
                            <ns0:sortOrder>{ data($fml/PT_SORT_ORDER) }</ns0:sortOrder>
                            <ns0:idProductArea>{ data($fml/PT_ID_AREA) }</ns0:idProductArea>
                            <ns0:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP) }</ns0:userChangeSKP>
                            <ns0:dateChange>{ data($fml/PT_DATE_CHANGE) }</ns0:dateChange>
                            <ns0:idProductCategory>{ data($fml/PT_ID_CATEGORY) }</ns0:idProductCategory>
                </ns0:entities.productstree.ProductCategory>
            </ns0:productCategoryEntityOut>
        </ns0:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductCategory_resp($fml)]]></con:xquery>
</con:xqueryEntry>