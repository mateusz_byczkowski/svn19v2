<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/moveProductGroup_req/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:moveProductGroup_req($entity as element(), $src as element(), $dest as element())
    as element(FML32) {
	<FML32>
		<PT_ID_GROUP>{ data($entity/dcl:idProductGroup) }</PT_ID_GROUP>
		<PT_ID_CATEGORY>{ data($src/dcl:idProductCategory) }</PT_ID_CATEGORY>
		<PT_ID_CATEGORY>{ data($dest/dcl:idProductCategory) }</PT_ID_CATEGORY>
    </FML32>
};

declare variable $entity as element(dcl:entities.productstree.ProductGroup) external;
declare variable $src as element(dcl:entities.productstree.ProductCategory) external;
declare variable $dest as element(dcl:entities.productstree.ProductCategory) external;

xf:moveProductGroup_req($entity, $src, $dest)]]></con:xquery>
</con:xqueryEntry>