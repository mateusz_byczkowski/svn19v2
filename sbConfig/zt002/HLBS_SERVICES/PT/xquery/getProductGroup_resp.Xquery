<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductGroup_resp/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductGroup_resp ($fml as element(FML32)) as element(dcl:invokeResponse) {
<dcl:invokeResponse>
<dcl:productGroupEntity>
<dcl:entities.productstree.ProductGroup>
	<dcl:codeProductGroup>{ data( $fml/PT_CODE_PRODUCT_GROUP ) }</dcl:codeProductGroup>
	<dcl:polishGroupName>{ data( $fml/PT_POLISH_NAME ) }</dcl:polishGroupName>
	<dcl:englishGroupName>{ data( $fml/PT_ENGLISH_NAME ) }</dcl:englishGroupName>
	<dcl:sortOrder>{ data( $fml/PT_SORT_ORDER ) }</dcl:sortOrder>
     
        <dcl:visibilitySzrek>{ xs:boolean( data( $fml/PT_VISIBILITY_SZREK )) }</dcl:visibilitySzrek>
	<dcl:visibilityCRM>{xs:boolean(  data( $fml/PT_VISIBILITY_CRM )) }</dcl:visibilityCRM>
	<dcl:receiver>{ data( $fml/PT_RECEIVER ) }</dcl:receiver>
	<dcl:idProductCategory>{ data( $fml/PT_ID_CATEGORY ) }</dcl:idProductCategory>
	
        <dcl:userChangeSKP>{ data( $fml/PT_USER_CHANGE_SKP ) }</dcl:userChangeSKP>
        <dcl:dateChange>{ data( $fml/PT_DATE_CHANGE ) }</dcl:dateChange>
        <dcl:visibilityDCL>{xs:boolean(  data( $fml/PT_VISIBILITY_DCL )) }</dcl:visibilityDCL>
        <dcl:idProductGroup>{ data( $fml/PT_ID_GROUP ) }</dcl:idProductGroup>
        <dcl:codeProductSystem>
     	<dcl:dictionaries.System>
     		<dcl:system>{ data( $fml/PT_CODE_PRODUCT_SYSTEM ) }</dcl:system>
     	</dcl:dictionaries.System>
     </dcl:codeProductSystem>
</dcl:entities.productstree.ProductGroup>
</dcl:productGroupEntity>
</dcl:invokeResponse>
};

declare variable $fml as element(FML32) external;
xf:getProductGroup_resp($fml)]]></con:xquery>
</con:xqueryEntry>