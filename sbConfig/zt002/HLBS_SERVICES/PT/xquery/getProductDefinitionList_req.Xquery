<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductDefinitionList_req/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductDefinitionList_req ($entity as element(dcl:entities.productstree.ProductGroup)) as element(FML32) {
<FML32>
     <PT_ID_GROUP>{ data( $entity/dcl:idProductGroup ) }</PT_ID_GROUP>
     <PT_SORT_ORDER>{ data( $entity/dcl:sortOrder ) }</PT_SORT_ORDER>
</FML32>
};

declare variable $entity as element(dcl:entities.productstree.ProductGroup) external;
xf:getProductDefinitionList_req($entity)]]></con:xquery>
</con:xqueryEntry>