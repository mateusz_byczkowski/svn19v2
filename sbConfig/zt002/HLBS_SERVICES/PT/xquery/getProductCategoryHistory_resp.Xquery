<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductCategoryHistory_resp/";
declare namespace ns0 = "urn:dcl:services.alsb.datamodel";

declare function xf:getProductCategoryHistory_resp($fml as element())
    as element(ns0:invokeResponse) {
        <ns0:invokeResponse>
            <ns0:productCategoryHistoryList>
                {
                    for $i in 1 to count($fml/PT_ID_CATEGORY_HISTORY)
                    return
                        <ns0:entities.productstree.ProductCategoryHistory>
                                    <ns0:codeProductCategory>{ data($fml/PT_CODE_PRODUCT_CATEGORY[$i]) }</ns0:codeProductCategory>
                                    <ns0:polishCategoryName>{ data($fml/PT_POLISH_NAME[$i]) }</ns0:polishCategoryName>
                                    <ns0:englishCategoryName>{ data($fml/PT_ENGLISH_NAME[$i]) }</ns0:englishCategoryName>
                                    <ns0:sortOrder>{ data($fml/PT_SORT_ORDER[$i]) }</ns0:sortOrder>
                                    <ns0:overridingElementID/>
                                    <ns0:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }</ns0:userChangeSKP>
                                    <ns0:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }</ns0:dateChange>
                        </ns0:entities.productstree.ProductCategoryHistory>
                }
            </ns0:productCategoryHistoryList>
        </ns0:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductCategoryHistory_resp($fml)]]></con:xquery>
</con:xqueryEntry>