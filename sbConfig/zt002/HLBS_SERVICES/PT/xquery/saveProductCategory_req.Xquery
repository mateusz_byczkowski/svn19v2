<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProductCategory_req/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:saveProductCategory_req ($entity as element(dcl:entities.productstree.ProductCategory)) as element(FML32) {
<FML32>
     <PT_CODE_PRODUCT_CATEGORY>{ data( $entity/dcl:codeProductCategory ) }</PT_CODE_PRODUCT_CATEGORY>
     <PT_POLISH_NAME>{ data( $entity/dcl:polishCategoryName ) }</PT_POLISH_NAME>
     <PT_ENGLISH_NAME>{ data( $entity/dcl:englishCategoryName ) }</PT_ENGLISH_NAME>
     <PT_SORT_ORDER>{ data( $entity/dcl:sortOrder ) }</PT_SORT_ORDER>
     <PT_ID_AREA>{ data( $entity/dcl:idProductArea ) }</PT_ID_AREA>
     <PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP ) }</PT_USER_CHANGE_SKP>
     <PT_DATE_CHANGE>{ data( $entity/dcl:dateChange ) }</PT_DATE_CHANGE>
     <PT_ID_CATEGORY>{ data( $entity/dcl:idProductCategory ) }</PT_ID_CATEGORY>
</FML32>
};
declare variable $entity as element(dcl:entities.productstree.ProductCategory) external;
xf:saveProductCategory_req($entity)]]></con:xquery>
</con:xqueryEntry>