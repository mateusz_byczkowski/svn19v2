<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductGroup_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace dcl = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";

declare function xf:getProductGroup_resp ($fml as element(FML32)) as element(srv:invokeResponse) {
<srv:invokeResponse>
<srv:productGroupEntity>
<dcl:ProductGroup>
	<dcl:polishGroupName>{ data( $fml/PT_POLISH_NAME ) }</dcl:polishGroupName>
	<dcl:codeProductGroup>{ data( $fml/PT_CODE_PRODUCT_GROUP ) }</dcl:codeProductGroup>
	<dcl:englishGroupName>{ data( $fml/PT_ENGLISH_NAME ) }</dcl:englishGroupName>
	<dcl:sortOrder>{ data( $fml/PT_SORT_ORDER ) }</dcl:sortOrder>   
        <dcl:visibilitySzrek>{ xs:boolean( data( $fml/PT_VISIBILITY_SZREK )) }</dcl:visibilitySzrek>
	<dcl:visibilityCRM>{xs:boolean(  data( $fml/PT_VISIBILITY_CRM )) }</dcl:visibilityCRM>
	<dcl:receiver>{ data( $fml/PT_RECEIVER ) }</dcl:receiver>
	<dcl:idProductCategory>{ data( $fml/PT_ID_CATEGORY ) }</dcl:idProductCategory>
        <dcl:userChangeSKP>{ data( $fml/PT_USER_CHANGE_SKP ) }</dcl:userChangeSKP>

     {if (data( $fml/PT_DATE_CHANGE))
     then <dcl:dateChange>{ data( $fml/PT_DATE_CHANGE ) }</dcl:dateChange>
     else () }

        <dcl:visibilityDCL>{xs:boolean(  data( $fml/PT_VISIBILITY_DCL )) }</dcl:visibilityDCL>
        <dcl:idProductGroup>{ data( $fml/PT_ID_GROUP ) }</dcl:idProductGroup>
        <dcl:codeProductSystem>
     	<ns1:System>
     		<ns1:system>{ data( $fml/PT_CODE_PRODUCT_SYSTEM ) }</ns1:system>
     	</ns1:System>
     </dcl:codeProductSystem>
</dcl:ProductGroup>
</srv:productGroupEntity>
</srv:invokeResponse>
};

declare variable $fml as element(FML32) external;
xf:getProductGroup_resp($fml)]]></con:xquery>
</con:xqueryEntry>