<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductareaList_resp/";
declare namespace dcl = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";

declare function xf:getProductareaList_resp($fml as element())
    as element(dcl:invokeResponse) {
        <dcl:invokeResponse>
            <dcl:productAreaList>
                {
                    for $i in 1 to count($fml/PT_ID_AREA)
                    return
                        <ns0:ProductArea>
                                    <ns0:codeProductArea>{ data($fml/PT_CODE_PRODUCT_AREA[$i]) }</ns0:codeProductArea>
                                    <ns0:polishAreaName>{ data($fml/PT_POLISH_NAME[$i]) }</ns0:polishAreaName>
                                    <ns0:englishAreaName>{ data($fml/PT_ENGLISH_NAME[$i]) }</ns0:englishAreaName>
                                    <ns0:sortOrder>{ data($fml/PT_SORT_ORDER[$i]) }</ns0:sortOrder>
                                    <ns0:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }</ns0:userChangeSKP>

     {if (data( $fml/PT_DATE_CHANGE[$i]))
     then <ns0:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }</ns0:dateChange>
     else () }

                                    <ns0:cerberAtributeName>{ data($fml/PT_CERBER_ATTRIBUTE_NAME[$i]) }</ns0:cerberAtributeName>
                                    <ns0:idProductArea>{ data($fml/PT_ID_AREA[$i]) }</ns0:idProductArea>
                        </ns0:ProductArea>
                }
            </dcl:productAreaList>
        </dcl:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductareaList_resp($fml)]]></con:xquery>
</con:xqueryEntry>