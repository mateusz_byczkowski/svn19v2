<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAttributesListCnt_req/";
declare namespace dcl = "urn:baseauxentities.be.dcl";

declare function xf:getProductAttributesListCnt_req ($type as element(dcl:EditHelper), $id as element(dcl:IntegerHolder)) as element(FML32) {
<FML32>
{
if (data($type/dcl:stringBased) eq "PRODUCT_GROUP") 
then <PT_ID_GROUP>{ data( $id/dcl:value ) }</PT_ID_GROUP>
else if (data($type/dcl:stringBased) eq "PRODUCT_DEFINITION") 
then <PT_ID_DEFINITION>{ data( $id/dcl:value ) }</PT_ID_DEFINITION>
else if (data($type/dcl:stringBased) eq "PRODUCT_PACKAGE") 
then <PT_ID_PACKAGE>{ data( $id/dcl:value ) }</PT_ID_PACKAGE>
else if (data($type/dcl:stringBased) eq "PRODUCT_AREA_ATTRIBUTE") 
then <PT_ID_AREA_ATTRIBUTES>{ data( $id/dcl:value ) }</PT_ID_AREA_ATTRIBUTES>
else if (data($type/dcl:stringBased) eq "PRODUCT_CATEGORY") 
then <PT_ID_CATEGORY>{ data( $id/dcl:value ) }</PT_ID_CATEGORY>
else ()

}
</FML32>
};

declare variable $type as element(dcl:EditHelper) external;
declare variable $id as element(dcl:IntegerHolder) external;

xf:getProductAttributesListCnt_req($type, $id)]]></con:xquery>
</con:xqueryEntry>