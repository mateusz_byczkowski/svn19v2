<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAreaAttrListAvailValues_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:baseauxentities.be.dcl"; 
declare namespace ns2 ="urn:entities.be.dcl";

declare function xf:getProductAreaAttrListAvailValues_resp($fml as element())
    as element() {
	<srv:invokeResponse>
         <srv:bcd>
            <ns2:BusinessControlData>
               <ns2:pageControl>
                  <ns1:PageControl>
                     <ns1:hasNext> 
                       { if (data($fml/PT_OPTION[1]) = 1 )  
                         then
                             "true"
                        else
                             "false"
                         }
                     </ns1:hasNext>
                     <ns1:navigationKeyValue>{ data($fml/PT_PACK_NO[1]) }</ns1:navigationKeyValue>
                  </ns1:PageControl>
               </ns2:pageControl>
            </ns2:BusinessControlData>
         </srv:bcd>
        <srv:prodAreaAttrListAvailValues>
            {
                for $i in 1 to count($fml/PT_ID_AREA_ATTR_LIST_VAL)
                return
                    <ns0:ProductAreaAttrListAvailVal>
                                <ns0:value>{ data($fml/PT_VALUE[$i]) }</ns0:value>                                    
                                <ns0:valueDescription>{ data($fml/PT_DESCRIPTION[$i]) }</ns0:valueDescription>                                    
                                <ns0:connectionField>{ data($fml/PT_CONNECTION_FIELD[$i]) }</ns0:connectionField>                                    
                                <ns0:idProductAreaAttrListAvailVal>{ data($fml/PT_ID_AREA_ATTR_LIST_VAL[$i]) }</ns0:idProductAreaAttrListAvailVal>                                    
                            	<ns0:idProductAreaAttrtibutes>{ data($fml/PT_ID_AREA_ATTRIBUTES[$i]) }</ns0:idProductAreaAttrtibutes>
                    </ns0:ProductAreaAttrListAvailVal>
            }
        </srv:prodAreaAttrListAvailValues>
    </srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductAreaAttrListAvailValues_resp($fml)]]></con:xquery>
</con:xqueryEntry>