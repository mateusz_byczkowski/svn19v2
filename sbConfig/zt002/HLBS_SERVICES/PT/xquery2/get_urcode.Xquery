<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/get_urcode/";
declare namespace ns0 = "http://www.w3.org/2001/XMLSchema";

declare function xf:get_urcode($errorString as xs:string)
    as xs:string{
        if ( fn:starts-with($errorString,"TPESVCFAIL") ) then 
        	fn:tokenize($errorString,":")[3]
        else 'unknown'
};

declare variable $errorString as xs:string external;

xf:get_urcode($errorString)</con:xquery>
</con:xqueryEntry>