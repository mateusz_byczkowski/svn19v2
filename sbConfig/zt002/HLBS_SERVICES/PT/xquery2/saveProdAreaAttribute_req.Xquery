<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProdAreaAttribute_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";

declare function xf:saveProdAreaAttribute_req ($entity as element(dcl:ProductAreaAttributes)) as element(FML32) {
<FML32>
     <PT_ATTRIBUTE_NAME>{ data( $entity/dcl:attributeName ) }</PT_ATTRIBUTE_NAME>
     <PT_ATTRIBUTE_TYPE>{ data( $entity/dcl:attributeType/ns1:ProductAreaAttributeType/ns1:attributeType ) }</PT_ATTRIBUTE_TYPE>
     <PT_ALGORITHM_SEQ>{ data( $entity/dcl:algorithmSequence ) }</PT_ALGORITHM_SEQ>
     <PT_ID_AREA>{ data( $entity/dcl:idProductArea ) }</PT_ID_AREA>
     
     <PT_ID_AREA_ATTRIBUTES>{ data( $entity/dcl:idProductAreaAttributes ) }</PT_ID_AREA_ATTRIBUTES>     
     <PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP) }</PT_USER_CHANGE_SKP>
     <PT_DATE_CHANGE>{ data( $entity/dcl:dateChange) }</PT_DATE_CHANGE>     
</FML32>
};
declare variable $entity as element(dcl:ProductAreaAttributes) external;
xf:saveProdAreaAttribute_req($entity)]]></con:xquery>
</con:xqueryEntry>