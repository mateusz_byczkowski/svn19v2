<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/deleteProductPackage_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:deleteProductPackage_req($entity as element(dcl:ProductPackage))
    as element() {
	&lt;FML32>
		&lt;PT_ID_PACKAGE>{ data( $entity/dcl:idProductPackage ) }&lt;/PT_ID_PACKAGE>
	&lt;/FML32>
};

declare variable $entity as element(dcl:ProductPackage) external;

xf:deleteProductPackage_req($entity)</con:xquery>
</con:xqueryEntry>