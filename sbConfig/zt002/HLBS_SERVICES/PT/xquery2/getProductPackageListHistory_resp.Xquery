<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductPackageList/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns1 = "urn:productstree.entities.be.dcl";

declare function xf:getProductPackageListHistory_resp($fml as element())
    as element(srv:invokeResponse) {
        <srv:invokeResponse>
            <srv:productPackageListHistory>
            {
            for $i in 1 to count($fml/PT_ID_PACKAGE)
            return
                <ns1:ProductPackageListHistory>
                   <ns1:obligatoryFlag>{ data($fml/PT_OBLIGATORY[$i]) }</ns1:obligatoryFlag>
                   <ns1:idProductGroup>{ data($fml/PT_ID_GROUP[$i]) }</ns1:idProductGroup>
                   <ns1:idProductDefinition>{ data($fml/PT_ID_DEFINITION[$i]) }</ns1:idProductDefinition>
                   <ns1:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }</ns1:userChangeSKP>
                   <ns1:idProductPackage>{ data($fml/PT_ID_PACKAGE[$i]) }</ns1:idProductPackage>
                   <ns1:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }</ns1:dateChange>
                </ns1:ProductPackageListHistory>
            }
            </srv:productPackageListHistory>
        </srv:invokeResponse>
};

declare variable $fml as element(FML32) external;

xf:getProductPackageListHistory_resp($fml)]]></con:xquery>
</con:xqueryEntry>