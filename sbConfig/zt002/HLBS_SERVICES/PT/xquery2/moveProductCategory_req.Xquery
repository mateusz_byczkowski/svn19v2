<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/moveProductCategory_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:moveProductCategory_req($entity as element(), $src as element(), $dest as element())
    as element(FML32) {
	<FML32>
		<PT_ID_CATEGORY>{ data($entity/dcl:idProductCategory) }</PT_ID_CATEGORY>
		<PT_ID_AREA>{ data($src/dcl:idProductArea) }</PT_ID_AREA>
		<PT_ID_AREA>{ data($dest/dcl:idProductArea) }</PT_ID_AREA>
    </FML32>
};

declare variable $entity as element(dcl:ProductCategory) external;
declare variable $src as element(dcl:ProductArea) external;
declare variable $dest as element(dcl:ProductArea) external;

xf:moveProductCategory_req($entity, $src, $dest)]]></con:xquery>
</con:xqueryEntry>