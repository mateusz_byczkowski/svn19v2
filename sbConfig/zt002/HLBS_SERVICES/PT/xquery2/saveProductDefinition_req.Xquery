<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProductDefinition_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:saveProductDefinition_req ($entity as element(dcl:ProductDefinition)) as element(FML32) {
<FML32>
     <PT_CODE_PRODUCT_DEFINITION>{ data( $entity/dcl:codeProductDefinition ) }</PT_CODE_PRODUCT_DEFINITION>
     <PT_POLISH_NAME>{ data( $entity/dcl:polishProductName ) }</PT_POLISH_NAME>
     <PT_ENGLISH_NAME>{ data( $entity/dcl:englishProductName ) }</PT_ENGLISH_NAME>
     <PT_SORT_ORDER>{ data( $entity/dcl:sortOrder ) }</PT_SORT_ORDER>
     <PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP ) }</PT_USER_CHANGE_SKP>
     <PT_DATE_CHANGE>{ data( $entity/dcl:dateChange ) }</PT_DATE_CHANGE>
     <PT_PACKAGE_ONLY_FLAG>{ xs:short( xs:boolean(data( $entity/dcl:packageOnlyFlag ))) }</PT_PACKAGE_ONLY_FLAG>
     <PT_START_DATE>{ data( $entity/dcl:startDate ) }</PT_START_DATE>
     <PT_END_DATE>{ data( $entity/dcl:endDate ) }</PT_END_DATE>
     
     <PT_SOURCE_PRODUCT_CODE>{ data( $entity/dcl:sorceProductCode ) }</PT_SOURCE_PRODUCT_CODE>
     <PT_ID_GROUP>{ data( $entity/dcl:idProductGroup ) }</PT_ID_GROUP>
     
     <PT_ID_DEFINITION>{ data( $entity/dcl:idProductDefinition ) }</PT_ID_DEFINITION>
     
</FML32>
};
declare variable $entity as element(dcl:ProductDefinition) external;
xf:saveProductDefinition_req($entity)]]></con:xquery>
</con:xqueryEntry>