<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductTreeStructure_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:getProductTreeStructure_resp($fml as element(FML32))
    as element(srv:invokeResponse) {
<srv:invokeResponse>
<srv:productDefinitionList>
{
for $i in 1 to count($fml/PT_ID_DEFINITION)
return
<dcl:ProductDefinition>
    <dcl:polishProductName>{ data( $fml/PT_POLISH_NAME [$i] ) }</dcl:polishProductName>
    <dcl:englishProductName>{ data( $fml/PT_ENGLISH_NAME [$i] ) }</dcl:englishProductName>
    <dcl:sortOrder>{ data( $fml/PT_ID_DEFINITION [$i] ) }</dcl:sortOrder>
    <dcl:idProductGroup>{ data( $fml/PT_ID_GROUP [$i] ) }</dcl:idProductGroup>
    <dcl:idProductDefinition>{ data( $fml/PT_ID_DEFINITION [$i] ) }</dcl:idProductDefinition>
    <dcl:productGroup>
        <dcl:ProductGroup>
            <dcl:polishGroupName>{ data( $fml/PT_POLISH_GROUP_NAME [$i] ) }</dcl:polishGroupName>
            <dcl:englishGroupName>{ data( $fml/PT_ENGLISH_GROUP_NAME [$i] ) }</dcl:englishGroupName>
            <dcl:sortOrder>{ data( $fml/PT_ID_GROUP [$i] ) }</dcl:sortOrder>
            <dcl:idProductCategory>{ data( $fml/PT_ID_CATEGORY [$i] ) }</dcl:idProductCategory>
            <dcl:idProductGroup>{ data( $fml/PT_ID_GROUP [$i] ) }</dcl:idProductGroup>
            <dcl:productCategory>
                <dcl:ProductCategory>
                    <dcl:polishCategoryName>{ data( $fml/PT_POLISH_CATEGORY_NAME [$i] ) }</dcl:polishCategoryName>
                    <dcl:englishCategoryName>{ data( $fml/PT_ENGLISH_CATEGORY_NAME [$i] ) }</dcl:englishCategoryName>
                    <dcl:sortOrder>{ data( $fml/PT_ID_CATEGORY [$i] ) }</dcl:sortOrder>
                    <dcl:idProductArea>{ data( $fml/PT_ID_AREA [$i] ) }</dcl:idProductArea>
                    <dcl:idProductCategory>{ data( $fml/PT_ID_CATEGORY [$i] ) }</dcl:idProductCategory>
                    <dcl:productArea>
                        <dcl:ProductArea>
                            <dcl:polishAreaName>{ data( $fml/PT_POLISH_AREA_NAME [$i] ) }</dcl:polishAreaName>
                            <dcl:englishAreaName>{ data( $fml/PT_ENGLISH_AREA_NAME [$i] ) }</dcl:englishAreaName>
                            <dcl:sortOrder>{ data( $fml/PT_ID_AREA [$i] ) }</dcl:sortOrder>
                            <dcl:idProductArea>{ data( $fml/PT_ID_AREA [$i] ) }</dcl:idProductArea>
                        </dcl:ProductArea>
                    </dcl:productArea>
                </dcl:ProductCategory>
            </dcl:productCategory>
        </dcl:ProductGroup>
    </dcl:productGroup>
</dcl:ProductDefinition>
}
</srv:productDefinitionList>

<srv:productDefinitionEntity>

<dcl:ProductDefinition>
    <dcl:polishProductName></dcl:polishProductName>
    <dcl:englishProductName></dcl:englishProductName>
    <dcl:idProductGroup>{ data( $fml/PT_PACK_SIZE ) }</dcl:idProductGroup>
    <dcl:idProductDefinition>{ data( $fml/PT_PACK_NO ) }</dcl:idProductDefinition>
    <dcl:productGroup>
        <dcl:ProductGroup>
            <dcl:polishGroupName></dcl:polishGroupName>
            <dcl:englishGroupName></dcl:englishGroupName>
            <dcl:idProductCategory></dcl:idProductCategory>
            <dcl:idProductGroup></dcl:idProductGroup>
            <dcl:productCategory>
                <dcl:ProductCategory>
                    <dcl:polishCategoryName></dcl:polishCategoryName>
                    <dcl:englishCategoryName></dcl:englishCategoryName>
                    <dcl:idProductArea></dcl:idProductArea>
                    <dcl:idProductCategory></dcl:idProductCategory>
                    <dcl:productArea>
                        <dcl:ProductArea>
                            <dcl:polishAreaName></dcl:polishAreaName>
                            <dcl:englishAreaName></dcl:englishAreaName>
                            <dcl:cerberAtributeName></dcl:cerberAtributeName>
                            <dcl:idProductArea></dcl:idProductArea>
                        </dcl:ProductArea>
                    </dcl:productArea>
                </dcl:ProductCategory>
            </dcl:productCategory>
        </dcl:ProductGroup>
    </dcl:productGroup>
</dcl:ProductDefinition>



</srv:productDefinitionEntity>

</srv:invokeResponse>
};

declare variable $fml as element(FML32) external;

xf:getProductTreeStructure_resp($fml)]]></con:xquery>
</con:xqueryEntry>