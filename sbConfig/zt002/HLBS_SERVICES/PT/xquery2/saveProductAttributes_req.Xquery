<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProductAttributes_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:saveProductAttributes_req ($entity as element(dcl:ProductAttributes)) as element(FML32) {
<FML32>
     <PT_ATTRIBUTE_VALUE>{ data( $entity/dcl:attributeValue ) }</PT_ATTRIBUTE_VALUE>
     <PT_ATTRIBUTE_VALUE_2>{ data( $entity/dcl:attributeValue2 ) }</PT_ATTRIBUTE_VALUE_2>
     <PT_ID_PACKAGE>{ data( $entity/dcl:idProductPackage ) }</PT_ID_PACKAGE>
     <PT_ID_GROUP>{ data( $entity/dcl:idProductGroup ) }</PT_ID_GROUP>
     <PT_ID_DEFINITION>{ data( $entity/dcl:idProductDefinition ) }</PT_ID_DEFINITION>     
     <PT_ID_PROD_AREA_ATT>{ data( $entity/dcl:idProductAreaAttributes ) }</PT_ID_PROD_AREA_ATT>     
     <PT_ID_ATTRIBUTE>{ data( $entity/dcl:attributeID ) }</PT_ID_ATTRIBUTE>     
     
     <PT_ID_ATTRIBUTES>{ data( $entity/dcl:idProductAttributes ) }</PT_ID_ATTRIBUTES>
     
<PT_DATE_CHANGE>{ data( $entity/dcl:dateChange) }</PT_DATE_CHANGE>          
     <PT_USER_CHANGE_SKP>{ data( $entity/dcl:userChangeSKP) }</PT_USER_CHANGE_SKP>
</FML32>
};
declare variable $entity as element(dcl:ProductAttributes) external;
xf:saveProductAttributes_req($entity)]]></con:xquery>
</con:xqueryEntry>