<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAreaAttrHistory_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";

declare function xf:getProductAreaAttrHistory_resp($fml as element())
    as element() {
	<srv:invokeResponse>
        <srv:productAreaAttrHistory>
            {
                for $i in 1 to count($fml/PT_ID_AREA_ATTRIBUTES)
                return
                    <ns0:ProdAreaAttributesHist>
                                <ns0:idProductArea>{ data($fml/PT_ID_AREA[$i]) }</ns0:idProductArea>                                    
                                <ns0:idProductAreaAttributesHistory>{ data($fml/PT_ID_AREA_ATTRIBUTES_HISTORY[$i]) }</ns0:idProductAreaAttributesHistory>                                    
                                <ns0:attributeName>{ data($fml/PT_ATTRIBUTE_NAME[$i]) }</ns0:attributeName>                                    
                                <ns0:idProductAreaAttributes>{ data($fml/PT_ID_AREA_ATTRIBUTES[$i]) }</ns0:idProductAreaAttributes>                                    
                                <ns0:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }</ns0:userChangeSKP>                                    
                                <ns0:algorithmSequence>{ data($fml/PT_ALGORITHM_SEQ[$i]) }</ns0:algorithmSequence>                                    
                                <ns0:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }</ns0:dateChange>                                    
                            	<ns0:attributeType>
                            		<ns1:ProductAreaAttributeType>
                            			<ns1:attributeType>{ data($fml/PT_ATTRIBUTE_TYPE[$i]) }</ns1:attributeType>
                            		</ns1:ProductAreaAttributeType>
                            	</ns0:attributeType>
                    </ns0:ProdAreaAttributesHist>
            }
        </srv:productAreaAttrHistory>
    </srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductAreaAttrHistory_resp($fml)]]></con:xquery>
</con:xqueryEntry>