<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare variable $fml external;


<mes:TDCardCheckResponse xmlns:mes="http://bzwbk.com/services/ceke/messages/">
   <mes:ThreeDEnrolled>{data($fml/E_3D_ENROL)}</mes:ThreeDEnrolled>
   <mes:ExpiryDate>{data($fml/E_CARD_EXPIRY)}</mes:ExpiryDate>
   <mes:CardType>{data($fml/E_CARD_TYPE)}</mes:CardType>
    <mes:ThreeDCustStatus>{data($fml/E_3D_CUST_STATUS)}</mes:ThreeDCustStatus>
</mes:TDCardCheckResponse>]]></con:xquery>
</con:xqueryEntry>