<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/epacksubsnew/messages/";
declare namespace xf = "http://bzwbk.com/services/epacksubsnew/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";


declare variable $body as element(soap-env:Body) external;

declare variable $req as element (m:ePackSubsNewRequest):=$body/m:ePackSubsNewRequest;

<soap-env:Body>	
		<FML32>
			{
				if($req/m:LoginId)
					then <E_LOGIN_ID>{ data($req/m:LoginId) }</E_LOGIN_ID>
					else ()
			}
			{
				if($req/m:PackageType)
					then <EV_PACKAGE_TYPE>{ data($req/m:PackageType) }</EV_PACKAGE_TYPE>
					else ()
			}
			{
				if($req/m:MarketingAgree)
					then <EV_MARKETING_AGREE>{ data($req/m:MarketingAgree) }</EV_MARKETING_AGREE>
					else ()
			}
			{
				if($req/m:UserName)
					then <U_USER_NAME>{ data($req/m:UserName) }</U_USER_NAME>
					else ()
			}
			{
				if($req/m:DelivTimeFrom)
					then <EV_DELIV_TIME_FROM>{ data($req/m:DelivTimeFrom) }</EV_DELIV_TIME_FROM>
					else ()
			}
			{
				if($req/m:DelivTimeTo)
					then <EV_DELIV_TIME_TO>{ data($req/m:DelivTimeTo) }</EV_DELIV_TIME_TO>
					else ()
			}
			{
			            for $email in $req/m:emailList/m:CustEmail return
				    <E_CUST_EMAIL>{ data($email) }</E_CUST_EMAIL>
			}
			{
			            for $gsm in $req /m:gsmList/m:CustGsmno return
				    <E_CUST_GSMNO>{ data($gsm) }</E_CUST_GSMNO>
			}
			
		</FML32>
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>