<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns0="urn:filterandmessages.dictionaries.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };


declare function getElementsForDepositRatesList($parm as element(fml:FML32)) as element()
{

<ns4:depositRatesList>
  {
    for $x at $occ in $parm/XXXXX
    return
    <ns0:DepositRate>
      <ns0:thresholdMin>{data($parm/NF_DEPOSR_THRESHOLDMIN[$occ])}</ns0:thresholdMin>
      <ns0:thresholdMax>{data($parm/NF_DEPOSR_THRESHOLDMAX[$occ])}</ns0:thresholdMax>
      <ns0:frequency>{data($parm/NF_DEPOSR_FREQUENCY[$occ])}</ns0:frequency>
      <ns0:period>{data($parm/NF_DEPOSR_PERIOD[$occ])}</ns0:period>
      <ns0:rate>{data($parm/NF_DEPOSR_RATE[$occ])}</ns0:rate>
      <ns0:accountTerm>{data($parm/NF_DEPOSR_ACCOUNTTERM[$occ])}</ns0:accountTerm>
      { insertDate(data($parm/NF_DEPOSR_DATE[$occ]),"yyyy-MM-dd","ns0:date")}
      <ns0:depositRate>{data($parm/NF_DEPOSR_DEPOSITRATE[$occ])}</ns0:depositRate>
      <ns0:currencyCode>
        <ns2:CurrencyCode>
          <ns2:currencyCode>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns2:currencyCode>
        </ns2:CurrencyCode>
      </ns0:currencyCode>
    </ns0:DepositRate>
  }
</ns4:depositRatesList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns4:invokeResponse>
  {getElementsForDepositRatesList($parm)}
</ns4:invokeResponse>
};

declare function zaslepka() as element()
{
<ns4:invokeResponse>
<ns4:depositRatesList>
    <ns0:DepositRate>
      <ns0:thresholdMin>0.0</ns0:thresholdMin>
      <ns0:thresholdMax>0.0</ns0:thresholdMax>
      <ns0:frequency>0</ns0:frequency>
      <ns0:period>0</ns0:period>
      <ns0:rate>0.0</ns0:rate>
      <ns0:accountTerm>pusty</ns0:accountTerm>
      <ns0:date>2008-06-05</ns0:date>
      <ns0:depositRate>pusty</ns0:depositRate>
      <ns0:currencyCode>
        <ns2:CurrencyCode>
          <ns2:currencyCode>pusty</ns2:currencyCode>
        </ns2:CurrencyCode>
      </ns0:currencyCode>
    </ns0:DepositRate>
 </ns4:depositRatesList>
</ns4:invokeResponse>
};

<soap:Body>
  (:{getElementsForInvokeResponse($body/FML32)}:)
 {zaslepka()}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>