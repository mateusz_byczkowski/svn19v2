<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsd="http://www.w3.org/2001/XMLSchema";
declare namespace f="http://bzwbk.com/services/BZWBK24flow/faults/";
declare default element namespace "http://www.softax.com.pl/ebppml";

declare function xf:map_getBankApplicationListTuxResponse ($rpl as element(soapenv:Body)) as element(fml:FML32) {
	
	<fml:FML32>
	{
		if (data($rpl/application-list-reply/reply/status/result) = "OK") then (
			for $it at $p in $rpl/application-list-reply/reply/application-list/application
			return (

				<fml:EBPP_APPLICATION_ID>{ data($it/application-id) }</fml:EBPP_APPLICATION_ID>,
				<fml:EBPP_APP_STATUS>{ data($it/status) }</fml:EBPP_APP_STATUS>,
				
				
				<fml:EBPP_CREDITOR_ID>{ data($it/creditor-id) }</fml:EBPP_CREDITOR_ID>,
				<fml:EBPP_DEBTOR_ID>{ data($it/debtor-id) }</fml:EBPP_DEBTOR_ID>,
				<fml:EBPP_TITLE_ID>{ data($it/title-id) }</fml:EBPP_TITLE_ID>,
				<fml:EBPP_TITLE>{ data($it/title) }</fml:EBPP_TITLE>,
				
				<fml:EBPP_VALID_FROM>{ data($it/validity/@since) }</fml:EBPP_VALID_FROM>,
				<fml:EBPP_VALID_TO>{ data($it/validity/@to) }</fml:EBPP_VALID_TO>,
				
				if (data($it/amount)) then (
					<fml:EBPP_AMOUNT>{ data($it/amount) }</fml:EBPP_AMOUNT>
				) else ( <fml:EBPP_AMOUNT>0</fml:EBPP_AMOUNT> )
				,
				<fml:EBPP_CURRENCY>{ data($it/amount/@curr) }</fml:EBPP_CURRENCY>,
				<fml:EBPP_AGR_NAME>{ data($it/name) }</fml:EBPP_AGR_NAME>,
				
				<fml:EBPP_EFFICIENT_FROM>{ data($it/efficiency/@since) }</fml:EBPP_EFFICIENT_FROM>,
				<fml:EBPP_EFFICIENT_TO>{ data($it/efficiency/@to) }</fml:EBPP_EFFICIENT_TO>,
				
				<fml:EBPP_EXPIRATION_DATE>{ data($it/expire-date) }</fml:EBPP_EXPIRATION_DATE>,
				<fml:EBPP_SECRET>{ data($it/secret) }</fml:EBPP_SECRET>
			),
			if (xs:int(data($rpl/fml:E_REC_COUNT)) > 0) then (
				<fml:E_REC_COUNT>{ xs:int(data($rpl/fml:E_REC_COUNT)) - fn:count($rpl/application-list-reply/reply/application-list/application) }</fml:E_REC_COUNT>
			) else (
			<fml:E_REC_COUNT>{ xs:int(data($rpl/fml:E_REC_COUNT)) + fn:count($rpl/application-list-reply/reply/application-list/application) }</fml:E_REC_COUNT>
			)
		) else (
		<fml:ERROR_MESSAGE>{ data($rpl/application-list-reply/reply/status/err-code)  }</fml:ERROR_MESSAGE>,
		<fml:ERROR_DESCRIPTION>{ data($rpl/application-list-reply/reply/status/err-message) }</fml:ERROR_DESCRIPTION>
		)		
	}
	</fml:FML32>
	
	
};

declare variable $reply as element(soapenv:Body) external;
<soapenv:Body>
{ xf:map_getBankApplicationListTuxResponse($reply) }
</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>