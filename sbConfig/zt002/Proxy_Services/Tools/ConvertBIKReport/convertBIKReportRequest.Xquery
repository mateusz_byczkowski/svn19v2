<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-08-08</con:description>
    <con:xquery><![CDATA[declare namespace m = "urn:be.services.dcl";
declare namespace m1 = "urn:hlbsentities.be.dcl";
declare namespace w = "";
declare namespace u = "urn:ICBReportWebServices";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

<soap-env:Body>
{
    let $req := $body/m:invoke/m:input/m1:ConvertBIKReportInput
    return
    
    <gia:convertBIKReport xmlns:gia="http://gia.talex.com/">
      {if($req/m1:reportBIK)
          then <w:arg0>{data($req/m1:reportBIK)}</w:arg0>
          else ()
      }
      {if($req/m1:reportType)
          then <w:arg1>{data($req/m1:reportType) }</w:arg1>
          else ()
      }
    </gia:convertBIKReport>
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>