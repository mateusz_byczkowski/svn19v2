<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;

declare function convertActionCodeValue ($parm as xs:string,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "Y")
       then $trueval
       else $falseval
};


declare function getFieldsFromInvoke($parm as element(fml:FML32)) as element()*
{
 let $blokada:=data($parm/fml:B_BLOKADA)
 let $blokada_len:=string-length($blokada)
 let $blokada_zero:=concat(substring("00000", 1+ $blokada_len),$blokada)
 let $typBlokady:=data($parm/fml:DC_TYP_BLOKADY[2])
 let $typBlokady_len:=string-length($typBlokady)
 let $typBlokady_zero:=concat(substring("00", 1+ $typBlokady_len),$typBlokady)
 let $key:=concat($blokada_zero,$typBlokady)
 let $spacje:='                                                                                             '
 return
<fml:NF_PAGEC_NAVIGATIONKEYVALU>{concat($key,$spacje,$key)}</fml:NF_PAGEC_NAVIGATIONKEYVALU>
,
<fml:NF_HOLD_HOLDSTAT>A</fml:NF_HOLD_HOLDSTAT>
,
<fml:NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/fml:B_NR_RACH)}</fml:NF_ACCOUN_ACCOUNTNUMBER>
,
<fml:NF_ACCOUN_ACCOUNTIBAN?>{data($parm/fml:B_NR_RACH)}</fml:NF_ACCOUN_ACCOUNTIBAN>
,
<fml:NF_PAGEC_ACTIONCODE>{convertActionCodeValue(data($parm/fml:B_POZYCJA),"F","N")}</fml:NF_PAGEC_ACTIONCODE>
,
<fml:NF_HOLDT_HOLDTYPE?>{data($parm/fml:DC_TYP_BLOKADY[1])}</fml:NF_HOLDT_HOLDTYPE>
,
<fml:NF_PAGEC_PAGESIZE?>{data($parm/fml:B_LICZBA_REK)}</fml:NF_PAGEC_PAGESIZE>
,
<fml:NF_PAGEC_REVERSEORDER>0</fml:NF_PAGEC_REVERSEORDER>
};

<soap:Body>
  <fml:FML32>
   {getFieldsFromInvoke($body/fml:FML32)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>