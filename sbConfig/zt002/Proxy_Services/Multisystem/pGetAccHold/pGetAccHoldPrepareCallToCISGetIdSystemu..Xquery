<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery>declare namespace m = "http://bzwbk.com/services/esbmessages/";
declare namespace xf = "http://bzwbk.com/services/esbmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";


declare variable $body as element(soap-env:Body) external;

   &lt;FML32>
        &lt;B_BRANCH_EXT>{substring(data($body/FML32/B_NR_RACH),3,8)}&lt;/B_BRANCH_EXT>
   &lt;/FML32></con:xquery>
</con:xqueryEntry>