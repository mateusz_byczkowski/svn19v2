<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-09-10</con:description>
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:operations.entities.be.dcl";
declare namespace urn2="urn:accounts.entities.be.dcl";
declare namespace urn3="urn:dictionaries.be.dcl";
declare namespace urn4="urn:accountdict.dictionaries.be.dcl";
declare namespace urn5="urn:entities.be.dcl";
declare namespace urn6="urn:basedictionaries.be.dcl";

declare variable $body as element(soap:Body) external;

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm  = "1") then $trueval
       else $falseval
};

declare function defaultIfEmpty($parm as xs:string?,$defaultValue as xs:string) as xs:string{
    if ($parm)
       then insertDate($parm)
       else $defaultValue
};

declare function defaultIfFlagZero($parm as xs:string?,$defaultValue as xs:string, $flag as xs:string) as xs:string{
    if ($parm)
       then 
          if ($flag ="0") 
              then $defaultValue
          else
            insertDate($parm)
       else $defaultValue
};

declare function checkBalanceFlag($amount as xs:string,$flag as xs:string) as xs:string{
    if ($amount)
       then 
          if ($flag= "Y")
              then "99999999999.99"
          else
            $amount
       else ()
};

declare function defaultIfEmpty2($parm as xs:string?,$defaultValue as xs:string) as xs:string{
    if ($parm)
       then $parm
       else $defaultValue
};

declare function insertDate($value as xs:string?) as xs:string?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01") and not ($value = "01-01-0001"))
            then chgDate($value)
        else() 
      else()
      };

declare function chgDate($value as xs:string) as xs:string
{
    let $year := substring($value, 1, 4)
    let $month := substring($value, 6, 2)
    let $day := substring($value, 9, 2)
return
     concat($day,concat("-",concat($month, concat("-", $year))))
};

<soap:Body>
<fml:FML32>
   <fml:DC_TRN_ID>{data($body/urn:invoke/urn:transaction/urn1:Transaction/urn1:transactionID)}</fml:DC_TRN_ID>
   <fml:DC_UZYTKOWNIK>{data($body/urn:invoke/urn:user/urn5:User/urn5:userID)}</fml:DC_UZYTKOWNIK> 
   <fml:DC_ODDZIAL>{data($body/urn:invoke/urn:user/urn5:User/urn5:branchCode/urn6:BaseBranchCode/urn6:branchCode)}</fml:DC_ODDZIAL>
   <fml:DC_NR_RACHUNKU>{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:tranAccount/urn2:TranAccount/urn2:account/urn2:Account/urn2:accountNumber)}</fml:DC_NR_RACHUNKU>
   <fml:DC_KWOTA>{checkBalanceFlag(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdAmount),boolean2SourceValue(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdBalanceFlag),"Y","N"))}</fml:DC_KWOTA>
   <fml:DC_NUMER_BLOKADY>{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdNumber)}</fml:DC_NUMER_BLOKADY>
   <fml:DC_TYP_BLOKADY>{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdType/urn3:HoldType/urn3:holdType)}</fml:DC_TYP_BLOKADY> 
   <fml:DC_DATA_WAZNOSCI>{defaultIfFlagZero(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdExpirationDate),"31-12-2049",boolean2SourceValue(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdExpirationDateFlag),"0","1"))}</fml:DC_DATA_WAZNOSCI>
   <fml:DC_FLAGA_BLOKOWANIA_SALDA>{boolean2SourceValue(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdBalanceFlag),"Y","N")}</fml:DC_FLAGA_BLOKOWANIA_SALDA>
   <fml:DC_OPIS_1>{defaultIfEmpty2(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdDescription),"")}</fml:DC_OPIS_1>
   <fml:DC_OPIS_2>{defaultIfEmpty2(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdDescription2),"")}</fml:DC_OPIS_2>	
   <fml:DC_NUMER_SERYJNY_FAKTURY>{defaultIfEmpty2(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:firstChecqueNumber),"")}</fml:DC_NUMER_SERYJNY_FAKTURY>
   <fml:DC_DATA_CZEKU_FAKTURY>{defaultIfEmpty(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:checqueRestrictDate),"")}</fml:DC_DATA_CZEKU_FAKTURY> 
   <fml:DC_NUMER_CZEKU>{defaultIfEmpty2(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:lastChecqueNumber),"")}</fml:DC_NUMER_CZEKU>
   <fml:DC_WALUTA>{defaultIfEmpty2(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdCurrencyCode/urn3:CurrencyCode/urn3:currencyCode),"PLN")}</fml:DC_WALUTA> 
   <fml:DC_UZYTKOWNIK_MODYFIKUJACY>{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdCreateUserId)}</fml:DC_UZYTKOWNIK_MODYFIKUJACY>
   <fml:DC_UZYTKOWNIK_AUTORYZUJACY>{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdAcceptUserId)}</fml:DC_UZYTKOWNIK_AUTORYZUJACY> 
   <fml:DC_FLAGA_DATY_WYGASNIECIA>{boolean2SourceValue(data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdExpirationDateFlag),"0","1")}</fml:DC_FLAGA_DATY_WYGASNIECIA>
   <fml:DC_TYP_ZMIANY>{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:holdActionCode/urn4:HoldActionCode/urn4:holdActionCode)}</fml:DC_TYP_ZMIANY>
   <fml:DC_FLAGA_AUTORYZACJI>O</fml:DC_FLAGA_AUTORYZACJI>
   <fml:DC_NUMER_AKCJI_NA_BLOKADZIE>{data($body/urn:invoke/urn:hold/urn2:Hold/urn2:actionNumberOnHold)}</fml:DC_NUMER_AKCJI_NA_BLOKADZIE> 
</fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>