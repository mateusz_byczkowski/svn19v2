<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace fml="";

declare variable $body external;


  <soap-env:Body>
            <FML32> 
              <NF_ERROR_CODE>103</NF_ERROR_CODE>
              <NF_ERROR_DESCRIPTION>Brak danych</NF_ERROR_DESCRIPTION>
            </FML32> 
  </soap-env:Body>]]></con:xquery>
</con:xqueryEntry>