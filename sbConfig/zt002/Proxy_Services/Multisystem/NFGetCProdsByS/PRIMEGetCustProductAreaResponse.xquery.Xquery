<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$5.2011-07-27</con:description>
    <con:xquery><![CDATA[(: Change Log 
v.1.1 2009-08-26 PKL NP1995 Dodanie pól ProductStat i ProductStatusCode
v.1.2 2009-11-02 PKL T41479 Ukrywanie danych kart dodatkowych 
v.1.3 2010-01-12 PKL T43830 Pokazywanie wartości pola NF_IS_COOWNER kart dodatkowych
v.1.4 2011-02-02 PKL T52074 Błąd w obsłudze stronicowania dla PRIME, CEKE i STRL
v.1.5 2011-04-08 PKL NP2233 Zmiana wyznaczania rdscode dla kart kredytowych
v.1.6 2011-05-05 PKL NP2233 T56473 Dodanie daty wygaśnięcia karty kredytowej (NF_DEBITC_EXPIRATIONDATE)
v.1.7 2011-07-12 PKL NP2357 Obsługa filtra i danych Payback (karty debetowe i kredytowe) 
v.1.8 2011-07-19 PKL NP2307 Identyfikacja rachunków kart kredytowych
:)

declare namespace x = "http://bzwbk.com/services/prime/";
declare namespace m = "http://schemas.datacontract.org/2004/07/SzrekLibrary.Struktures";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";



declare function xf:calculatePageStart ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then 1
else if ($actioncode="N")
(: 1.4  then xs:integer(min(($records  , $oldstop + 1)))  :)  
           then xs:integer(min(($records + 1, $oldstop + 1))) (:1.4:)

else if ($actioncode="P")
   then max((1,$oldstart - $pagesize)) 
else if ($actioncode="L")
(: 1.4  then max((1,$records  - $pagesize)) :)
           then max((1,$records  - $pagesize + 1)) (:1.4:)

else 0
};

declare function xf:calculatePageStop ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then xs:integer(min(($pagesize,$records)))
else if ($actioncode="N")
   then xs:integer(min(($pagesize + $oldstop,$records))) 
else if ($actioncode="P")
(: 1.4  then max((1,$oldstart - 1)) :)
           then max((0,$oldstart - 1)) (:1.4:)
else if ($actioncode="L")
   then $records
else xs:integer(0)
};


declare function xf:calculateHasNext( $pagestart as xs:integer,$pagestop as xs:integer, 
                                                     $records as xs:integer, $actioncode as xs:string) as xs:integer{
(: 1.4   if ($actioncode != "P")  :)
    if ($actioncode != "P" and $actioncode != "L")  (:1.4:)
     then  if ($pagestop < $records)
           then 1
           else 0
    else if ($pagestart > 1)
           then 1
           else 0 
};

declare function xf:mapProduct($prod as element(m:Product))  as element()* {
(:-------------------------------------------------------------------------------------------------------------------------------------------------------:)
(: Pola wspólne kart, zwracane dla kart głównych i dodatkowych (bez rachunków kart): :)
(:1.8:) if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0) then (
(:1.5 out   <fml:NF_PRODUD_SORCEPRODUCTCODE> </fml:NF_PRODUD_SORCEPRODUCTCODE>
    , 1.5:)
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
      then <fml:NF_ACCOUN_ACCOUNTNUMBER  nil="true" />
(:1.8   else <fml:NF_ACCOUN_ACCOUNTNUMBER >{substring(data($prod/m:DC_NR_RACHUNKU),2)}</fml:NF_ACCOUN_ACCOUNTNUMBER> :)
(:1.8:) else <fml:NF_ACCOUN_ACCOUNTNUMBER >{substring(data($prod/m:B_NR_DL_RACHUNKU),3)}</fml:NF_ACCOUN_ACCOUNTNUMBER>
    ,
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
      then <fml:NF_DEBITC_VIRTUALCARDNBR>{concat("V",data($prod/m:DC_NR_ALTERNATYWNY))}</fml:NF_DEBITC_VIRTUALCARDNBR>
      else <fml:NF_DEBITC_VIRTUALCARDNBR nil="true" />
    ,
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
      then <fml:NF_DEBITC_CARDNBR>{substring(data($prod/m:DC_NR_RACHUNKU),2)}</fml:NF_DEBITC_CARDNBR>
      else <fml:NF_DEBITC_CARDNBR nil="true" />
    ,
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
      then <NF_PRODUA_CODEPRODUCTAREA>1</NF_PRODUA_CODEPRODUCTAREA>
(:1.8      else <NF_PRODUA_CODEPRODUCTAREA>2</NF_PRODUA_CODEPRODUCTAREA> :)
(:1.8:)    else <NF_PRODUA_CODEPRODUCTAREA>3</NF_PRODUA_CODEPRODUCTAREA> 
    ,
(:1.5 Start:)

    if ($prod/m:CI_PRODUCT_ATT4 and $prod/m:CI_PRODUCT_ATT1)
      then <fml:NF_PRODUD_SORCEPRODUCTCODE>{concat(data($prod/m:CI_PRODUCT_ATT4), data($prod/m:CI_PRODUCT_ATT1))}</fml:NF_PRODUD_SORCEPRODUCTCODE>
      else <fml:NF_PRODUD_SORCEPRODUCTCODE nil="true" />
    ,
(:1.5 Koniec:)
    if ($prod/m:CI_PRODUCT_ATT1 and string-length(data($prod/m:CI_PRODUCT_ATT1))>0)
      then <fml:NF_ATTRPG_FIRSTPRODUCTFEAT>{data($prod/m:CI_PRODUCT_ATT1)}</fml:NF_ATTRPG_FIRSTPRODUCTFEAT>    
      else <fml:NF_ATTRPG_FIRSTPRODUCTFEAT nil="true"/>
    ,
    <fml:NF_ATTRPG_SECONDPRODUCTFEA nil="true"/>
     ,
    <fml:NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/>
     ,
    <fml:NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/>
     ,
    <fml:NF_ATTRPG_FIFTHPRODUCTFEAT nil="true"/>
     ,
    if ($prod/m:CI_PRODUCT_NAME_ORYGINAL)
      then <fml:NF_ACCOUN_ACCOUNTNAME>{data($prod/m:CI_PRODUCT_NAME_ORYGINAL)}</fml:NF_ACCOUN_ACCOUNTNAME>
      else <fml:NF_ACCOUN_ACCOUNTNAME nil="true" />
    ,
(: NP1995 początek :)
    if ($prod/m:NF_PRODUCT_STAT)
      then <fml:NF_PRODUCT_STAT>{data($prod/m:NF_PRODUCT_STAT)}</fml:NF_PRODUCT_STAT>
      else <fml:NF_PRODUCT_STAT nil="true" />
    ,
    if ($prod/m:NF_PRODUCT_STATUSCODE)
      then <fml:NF_PRODUCT_STATUSCODE>{data($prod/m:B_KOD_RACH)}</fml:NF_PRODUCT_STATUSCODE>
      else <fml:NF_PRODUCT_STATUSCODE nil="true" />
    ,
(: NP1995 koniec :)
(: 1.7 początek :)
    if ($prod/m:PB1)
      then <fml:NF_CARDLI_PB1>{data($prod/m:PB1)}</fml:NF_CARDLI_PB1>
      else <fml:NF_CARDLI_PB1 nil="true" />
    ,
    if ($prod/m:PB2)
      then <fml:NF_CARDLI_PB2>{data($prod/m:PB2)}</fml:NF_CARDLI_PB2>
      else <fml:NF_CARDLI_PB2 nil="true" />
    ,
    if ($prod/m:PBStatus)
      then <fml:NF_CARDLS_CARDLPSTATUS>{data($prod/m:PBStatus)}</fml:NF_CARDLS_CARDLPSTATUS>
      else <fml:NF_CARDLS_CARDLPSTATUS nil="true" />
    ,
(: 1.7 koniec :)
    <NF_CTRL_SYSTEMID>2</NF_CTRL_SYSTEMID>
    ,			
    if ($prod/m:B_KOD_RACH)
      then <fml:NF_PRIMAC_ACCOUNTCODE>{data($prod/m:B_KOD_RACH)}</fml:NF_PRIMAC_ACCOUNTCODE>
      else <fml:NF_PRIMAC_ACCOUNTCODE nil="true" />
    ,		
    if ($prod/m:CI_PRODUCT_ATT4)
      then <fml:NF_CARD_BIN>{data($prod/m:CI_PRODUCT_ATT4)}</fml:NF_CARD_BIN>
      else <fml:NF_CARD_BIN nil="true" />
    ,	
    if ($prod/m:CI_PRODUCT_ATT2)
      then <fml:NF_CARD_CARDTYPE>{data($prod/m:CI_PRODUCT_ATT2)}</fml:NF_CARD_CARDTYPE>
      else <fml:NF_CARD_CARDTYPE nil="true" />
    ,	
     if ($prod/m:B_POJEDYNCZY)
        then <fml:NF_IS_COOWNER>{data($prod/m:B_POJEDYNCZY)}</fml:NF_IS_COOWNER>
        else <fml:NF_IS_COOWNER nil="true" />
      ,
(:1.6 początek:)
     if ($prod/m:B_CARD_EXPIRYDATE)
        then <fml:NF_DEBITC_EXPIRATIONDATE>{data($prod/m:B_CARD_EXPIRYDATE)}</fml:NF_DEBITC_EXPIRATIONDATE>
        else <fml:NF_DEBITC_EXPIRATIONDATE nil="true" />
      
(:1.6 koniec:)
,
(:1.8:) <fml:NF_ACCOUR_RELATIONSHIP nil="true" />,
(:-------------------------------------------------------------------------------------------------------------------------------------------------------:)
(: Pola zwracane tylko dla kart głównych: :)
    if ($prod/m:CI_PRODUCT_ATT2 and data($prod/m:CI_PRODUCT_ATT2)=1) then
   (
     if ($prod/m:CI_RACHUNEK_ADR_ALT)
        then <fml:NF_ACCOUN_ALTERNATIVEADDRE>{data($prod/m:CI_RACHUNEK_ADR_ALT)}</fml:NF_ACCOUN_ALTERNATIVEADDRE>
        else<fml:NF_ACCOUN_ALTERNATIVEADDRE>0</fml:NF_ACCOUN_ALTERNATIVEADDRE>
      , 			
      if ($prod/m:B_SALDO)
        then <fml:NF_ACCOUN_CURRENTBALANCE>{data($prod/m:B_SALDO)}</fml:NF_ACCOUN_CURRENTBALANCE>
        else <fml:NF_ACCOUN_CURRENTBALANCE nil="true" />
      ,			
      if ($prod/m:B_BLOKADA)
        then <fml:NF_HOLD_HOLDAMOUNT>{data($prod/m:B_BLOKADA)}</fml:NF_HOLD_HOLDAMOUNT>
        else <fml:NF_HOLD_HOLDAMOUNT>0</fml:NF_HOLD_HOLDAMOUNT>
      ,			
      if ($prod/m:B_DOST_SRODKI)
        then <fml:NF_ACCOUN_AVAILABLEBALANCE>{data($prod/m:B_DOST_SRODKI)}</fml:NF_ACCOUN_AVAILABLEBALANCE>
        else <fml:NF_ACCOUN_AVAILABLEBALANCE nil="true" />
      ,
      if ($prod/m:B_KOD_WALUTY)
        then <fml:NF_CURREC_CURRENCYCODE>{data($prod/m:B_KOD_WALUTY)}</fml:NF_CURREC_CURRENCYCODE>
        else <fml:NF_CURREC_CURRENCYCODE nil="true"/>
      ,			
      if ($prod/m:B_D_OTWARCIA)
        then <fml:NF_ACCOUN_ACCOUNTOPENDATE>{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_D_OTWARCIA))}</fml:NF_ACCOUN_ACCOUNTOPENDATE>
        else <fml:NF_ACCOUN_ACCOUNTOPENDATE>0001-01-01</fml:NF_ACCOUN_ACCOUNTOPENDATE>
      ,			
      if ($prod/m:B_DATA_OST_OPER)
        then <fml:NF_TRANA_DATEOFLASTACTIVIT>{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_DATA_OST_OPER))}</fml:NF_TRANA_DATEOFLASTACTIVIT>
        else <fml:NF_TRANA_DATEOFLASTACTIVIT>0001-01-01</fml:NF_TRANA_DATEOFLASTACTIVIT>
      ,		
      if ($prod/m:B_LIMIT1)
        then <fml:NF_TRAACA_LIMITAMOUNT>{data($prod/m:B_LIMIT1)}</fml:NF_TRAACA_LIMITAMOUNT>
        else <fml:NF_TRAACA_LIMITAMOUNT nil="true" />
   ) else (: koniec sekcji pól zwracanych tylko dla kart głównych :)
(:-------------------------------------------------------------------------------------------------------------------------------------------------------:)
  ( (: pola z wyczyszczoną wartością zwracane dla kart dodatkowych :)
     <fml:NF_ACCOUN_ALTERNATIVEADDRE nil="true" />,
     <fml:NF_ACCOUN_CURRENTBALANCE nil="true" />,
     <fml:NF_HOLD_HOLDAMOUNT nil="true" />,
     <fml:NF_ACCOUN_AVAILABLEBALANCE nil="true" /> ,
     <fml:NF_CURREC_CURRENCYCODE nil="true" /> ,
     <fml:NF_ACCOUN_ACCOUNTOPENDATE nil="true" />,
     <fml:NF_TRANA_DATEOFLASTACTIVIT nil="true" />,
     <fml:NF_TRAACA_LIMITAMOUNT nil="true" /> 
   ) 
) else()
(:1.8 koniec pól dla kart głównych i dodatkowych :)
(:-------------------------------------------------------------------------------------------------------------------------------------------------------:)
(: 1.8 start: Pola zwracane tylko dla rachunków kart kredytowych :)
   ,
    if ($prod/m:CI_PRODUCT_ATT1 and data($prod/m:CI_PRODUCT_ATT1)='F') then
   (
    <NF_CTRL_SYSTEMID>2</NF_CTRL_SYSTEMID>,	
    <NF_ACCOUR_RELATIONSHIP>SOW</NF_ACCOUR_RELATIONSHIP>,
    <NF_PRODUA_CODEPRODUCTAREA>3</NF_PRODUA_CODEPRODUCTAREA>,
    if ($prod/m:B_NR_DL_RACHUNKU and string-length(data($prod/m:B_NR_DL_RACHUNKU))>0)
      then <fml:NF_ACCOUN_ACCOUNTNUMBER >{substring(data($prod/m:B_NR_DL_RACHUNKU),3)}</fml:NF_ACCOUN_ACCOUNTNUMBER>
      else  <fml:NF_ACCOUN_ACCOUNTNUMBER  nil="true" />,
    if ($prod/m:DC_NR_RACHUNKU and string-length(data($prod/m:DC_NR_RACHUNKU))>0)
      then <fml:NF_DEBITC_CARDNBR>{substring(data($prod/m:DC_NR_RACHUNKU),2)}</fml:NF_DEBITC_CARDNBR>
      else <fml:NF_DEBITC_CARDNBR nil="true" />,
   if ($prod/m:CI_PRODUCT_ATT2 and $prod/m:CI_PRODUCT_ATT2)
      then <fml:NF_PRODUD_SORCEPRODUCTCODE>{concat('RACHKK_', data($prod/m:CI_PRODUCT_ATT2))}</fml:NF_PRODUD_SORCEPRODUCTCODE>
      else <fml:NF_PRODUD_SORCEPRODUCTCODE>'RACHKK_'</fml:NF_PRODUD_SORCEPRODUCTCODE>,
    <fml:NF_ATTRPG_FIRSTPRODUCTFEAT>'RACHKK_'</fml:NF_ATTRPG_FIRSTPRODUCTFEAT>,
    if ($prod/m:CI_PRODUCT_ATT2 and string-length(data($prod/m:CI_PRODUCT_ATT2))>0)
      then <fml:NF_ATTRPG_SECONDPRODUCTFEA>{data($prod/m:CI_PRODUCT_ATT2)}</fml:NF_ATTRPG_SECONDPRODUCTFEA>    
      else <fml:NF_ATTRPG_SECONDPRODUCTFEA nil="true"/>,
    <fml:NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/>,
    <fml:NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/>,
    <fml:NF_ATTRPG_FIFTHPRODUCTFEAT nil="true"/>,
    if ($prod/m:CI_PRODUCT_NAME_ORYGINAL)
      then <fml:NF_ACCOUN_ACCOUNTNAME>{data($prod/m:CI_PRODUCT_NAME_ORYGINAL)}</fml:NF_ACCOUN_ACCOUNTNAME>
      else <fml:NF_ACCOUN_ACCOUNTNAME nil="true" />,
    if ($prod/m:NF_PRODUCT_STAT)
      then <fml:NF_PRODUCT_STAT>{data($prod/m:NF_PRODUCT_STAT)}</fml:NF_PRODUCT_STAT>
      else <fml:NF_PRODUCT_STAT nil="true" />,
    if ($prod/m:NF_PRODUCT_STATUSCODE)
      then <fml:NF_PRODUCT_STATUSCODE>{data($prod/m:B_KOD_RACH)}</fml:NF_PRODUCT_STATUSCODE>
      else <fml:NF_PRODUCT_STATUSCODE nil="true" />,
    if ($prod/m:B_KOD_RACH)
      then <fml:NF_PRIMAC_ACCOUNTCODE>{data($prod/m:B_KOD_RACH)}</fml:NF_PRIMAC_ACCOUNTCODE>
      else <fml:NF_PRIMAC_ACCOUNTCODE nil="true" />,		
    if ($prod/m:B_POJEDYNCZY)
        then <fml:NF_IS_COOWNER>{data($prod/m:B_POJEDYNCZY)}</fml:NF_IS_COOWNER>
        else <fml:NF_IS_COOWNER nil="true" />,
    if ($prod/m:B_SALDO)
        then <fml:NF_ACCOUN_CURRENTBALANCE>{data($prod/m:B_SALDO)}</fml:NF_ACCOUN_CURRENTBALANCE>
        else <fml:NF_ACCOUN_CURRENTBALANCE nil="true" />,
    if ($prod/m:B_KOD_WALUTY)
        then <fml:NF_CURREC_CURRENCYCODE>{data($prod/m:B_KOD_WALUTY)}</fml:NF_CURREC_CURRENCYCODE>
        else <fml:NF_CURREC_CURRENCYCODE nil="true"/>,
    if ($prod/m:B_DATA_OST_OPER)
        then <fml:NF_TRANA_DATEOFLASTACTIVIT>{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_DATA_OST_OPER))}</fml:NF_TRANA_DATEOFLASTACTIVIT>
        else <fml:NF_TRANA_DATEOFLASTACTIVIT>0001-01-01</fml:NF_TRANA_DATEOFLASTACTIVIT>,
    if ($prod/m:B_D_OTWARCIA)
        then <fml:NF_ACCOUN_ACCOUNTOPENDATE>{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_D_OTWARCIA))}</fml:NF_ACCOUN_ACCOUNTOPENDATE>
        else <fml:NF_ACCOUN_ACCOUNTOPENDATE>0001-01-01</fml:NF_ACCOUN_ACCOUNTOPENDATE>,
    if ($prod/m:DC_NR_RACHUNKU)
      then <fml:NF_DEBITC_VIRTUALCARDNBR>{data($prod/m:DC_NR_RACHUNKU)}</fml:NF_DEBITC_VIRTUALCARDNBR>
      else <fml:NF_DEBITC_VIRTUALCARDNBR nil="true" />,

(:Pozostałe pola, nie ustawiane dla rachunkow kart kredytowych:)
     <fml:NF_ACCOUN_ALTERNATIVEADDRE nil="true" />,
     <fml:NF_HOLD_HOLDAMOUNT nil="true" />,
     <fml:NF_ACCOUN_AVAILABLEBALANCE nil="true" /> ,
     <fml:NF_TRAACA_LIMITAMOUNT nil="true" />, 
     <fml:NF_CARD_BIN nil="true" />, 
     <fml:NF_CARD_CARDTYPE nil="true" /> ,
     <fml:NF_DEBITC_EXPIRATIONDATE nil="true" /> ,
     <fml:NF_CARDLI_PB1 nil="true" /> ,
     <fml:NF_CARDLI_PB2 nil="true" /> ,
     <fml:NF_CARDLS_CARDLPSTATUS nil="true" /> 
   ) else () (:1.8 koniec:)
};

declare function xf:mapICBSGetCProAreaResponse ($res as element(x:PRIMEGetCustProductsAreaResult),$area as xs:string,
  $pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
								  $actioncode as xs:string )
as element(fml:FML32){
	let $records:=count( $res/m:Product)
	let $start:=xf:calculatePageStart($pagesize,$oldstart,$oldstop,$actioncode,$records)
	let $stop:=xf:calculatePageStop($pagesize,$oldstart,$oldstop,$actioncode,$records)
	let $newnavkey:=concat("012:",string($start),":",string($stop))
	let $hasnext:=xf:calculateHasNext($start,$stop,$records,$actioncode)
	return
	   <fml:FML32>
                           {
                             (:1.4 start:)
                             if ($start > $records or $stop <= 0) 
                             then (
		  <NF_PAGEC_NAVIGATIONKEYVALU>012</NF_PAGEC_NAVIGATIONKEYVALU>,
		  <NF_PAGEC_HASNEXT>0</NF_PAGEC_HASNEXT>,
		  <NF_PAGECC_OPERATIONS>0</NF_PAGECC_OPERATIONS>
                                    )
                             else (    
		  <NF_PAGEC_NAVIGATIONKEYVALU>{$newnavkey}</NF_PAGEC_NAVIGATIONKEYVALU>,
		  <NF_PAGEC_HASNEXT>{$hasnext}</NF_PAGEC_HASNEXT>,
		  <NF_PAGECC_OPERATIONS>{max((0,$stop - $start+1))}</NF_PAGECC_OPERATIONS>,
		 
			for $idx in ($start to $stop)
			   return
				  xf:mapProduct($res/m:Product[$idx]) 
		  
                                    )  (:1.4 koniec :)
                            }
	   </fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $area as xs:string external;
declare variable $pagesize as xs:integer external;
declare variable $oldstart  as xs:integer external;
declare variable $oldstop  as xs:integer external;
declare variable $actioncode  as xs:string external;
<soap-env:Body>
{xf:mapICBSGetCProAreaResponse($body/x:PRIMEGetCustProductsAreaResponse/x:PRIMEGetCustProductsAreaResult,$area,$pagesize,$oldstart,$oldstop,$actioncode)}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>