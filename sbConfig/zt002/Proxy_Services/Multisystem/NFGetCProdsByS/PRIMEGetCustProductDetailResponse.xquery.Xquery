<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductDetailResponse($bdy as element(m:PRIMEGetCustProductDetailResponse))
	as element(fml:FML32) {
		<fml:FML32>
	                {
				if($bdy/m:PRIMEGetCustProductDetailResult/m:DC_NR_RACHUNKU)
					then if( string-length(data($bdy/m:PRIMEGetCustProductDetailResult/m:DC_NR_ALTERNATYWNY)))
                                                    then <fml:NF_DEBITC_CARDNBR>{ substring(data($bdy/m:PRIMEGetCustProductDetailResult/m:DC_NR_RACHUNKU),2) }</fml:NF_DEBITC_CARDNBR>
					            else <fml:NF_ACCOUN_ACCOUNTNUMBER>{data($bdy/m:PRIMEGetCustProductDetailResult/m:DC_NR_RACHUNKU) }</fml:NF_ACCOUN_ACCOUNTNUMBER>
                                        else()
			}	              
                       {
				if(string-length(data($bdy/m:PRIMEGetCustProductDetailResult/m:DC_NR_ALTERNATYWNY))>0)
					then <fml:NF_DEBITC_VIRTUALCARDNBR>{ concat("V",data($bdy/m:PRIMEGetCustProductDetailResult/m:DC_NR_ALTERNATYWNY)) }</fml:NF_DEBITC_VIRTUALCARDNBR>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_POJEDYNCZY)
					then <fml:NF_IS_COOWNER>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_POJEDYNCZY) }</fml:NF_IS_COOWNER>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT1)
					then <fml:NF_ATTRPG_FIRSTPRODUCTFEAT>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT1) }</fml:NF_ATTRPG_FIRSTPRODUCTFEAT>
					else ()
			}
		(:	{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT2)
					then <fml:NF_ATTRPG_SECONDPRODUCTFEA>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT2) }</fml:NF_ATTRPG_SECONDPRODUCTFEA>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT3)
					then <fml:NF_ATTRPG_THIRDPRODUCTFEAT>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT3) }</fml:NF_ATTRPG_THIRDPRODUCTFEAT>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT4)
					then <fml:NF_ATTRPG_FOURTHPRODUCTFEA>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT4) }</fml:NF_ATTRPG_FOURTHPRODUCTFEA>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT5)
					then <fml:NF_ATTRPG_FIFTHPRODUCTFEAT>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT5) }</fml:NF_ATTRPG_FIFTHPRODUCTFEAT>
					else ()
			}:)
                       <fml:NF_ATTRPG_SECONDPRODUCTFEA/>
                       <fml:NF_ATTRPG_THIRDPRODUCTFEAT/>
                       <fml:NF_ATTRPG_FOURTHPRODUCTFEA/>
                       <fml:NF_ATTRPG_FIFTHPRODUCTFEAT/>
                       <fml:NF_PRODUD_SORCEPRODUCTCODE/>

			(:{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_RACHUNEK_ADR_ALT)
					then <fml:NF_ACCOUN_ALTERNATIVEADDRE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_RACHUNEK_ADR_ALT) }</fml:NF_ACCOUN_ALTERNATIVEADDRE>
					else ()
			}:)
                        <fml:NF_ACCOUN_ALTERNATIVEADDRE>0</fml:NF_ACCOUN_ALTERNATIVEADDRE>
			(:{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_TYP_ADRESU)
					then <fml:NF_ACCOUA_ACCOUNTADDRESSTY>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_TYP_ADRESU) }</fml:NF_ACCOUA_ACCOUNTADDRESSTY>
					else ()
			}:)
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_KOD_WALUTY)
					then <fml:NF_CURREC_CURRENCYCODE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_KOD_WALUTY) }</fml:NF_CURREC_CURRENCYCODE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_SALDO)
					then <fml:NF_ACCOUN_CURRENTBALANCE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_SALDO) }</fml:NF_ACCOUN_CURRENTBALANCE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_DOST_SRODKI)
					then <fml:NF_ACCOUN_AVAILABLEBALANCE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_DOST_SRODKI) }</fml:NF_ACCOUN_AVAILABLEBALANCE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_BLOKADA)
					then <fml:NF_HOLD_HOLDAMOUNT>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_BLOKADA) }</fml:NF_HOLD_HOLDAMOUNT>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_DATA_OST_OPER)
					then <fml:NF_TRANA_DATEOFLASTACTIVIT>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_DATA_OST_OPER) }</fml:NF_TRANA_DATEOFLASTACTIVIT>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_LIMIT1)
					then <fml:NF_TRAACA_LIMITAMOUNT>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_LIMIT1) }</fml:NF_TRAACA_LIMITAMOUNT>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_NAME_ORYGINAL)
					then <fml:NF_ACCOUN_ACCOUNTDESCRIPTI>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_NAME_ORYGINAL) }</fml:NF_ACCOUN_ACCOUNTDESCRIPTI>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_OPROC1)
					then <fml:NF_ACCOUN_CREDITPRECENTAGE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_OPROC1) }</fml:NF_ACCOUN_CREDITPRECENTAGE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:DC_ODDZIAL)
					then <fml:NF_BRANCC_BRANCHCODE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:DC_ODDZIAL) }</fml:NF_BRANCC_BRANCHCODE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_OSTATNIEJ_KAP)
					then <fml:NF_TIMEA_LASTINTERESTPAYMD>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_OSTATNIEJ_KAP) }</fml:NF_TIMEA_LASTINTERESTPAYMD>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_NASTEPNEJ_KAP)
					then <fml:NF_TIMEA_NEXTINRTERESTPAYM>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_NASTEPNEJ_KAP) }</fml:NF_TIMEA_NEXTINRTERESTPAYM>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_KONCA_LIMITU)
					then <fml:NF_TIMEA_NEXTRENEWALMATURI>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_KONCA_LIMITU) }</fml:NF_TIMEA_NEXTRENEWALMATURI>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_ODS_POP_OKR)
					then <fml:NF_CREDIC_LASTPERIODINTREST>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_ODS_POP_OKR) }</fml:NF_CREDIC_LASTPERIODINTREST>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_OST_ZEST)
					then <fml:NF_CREDIC_LASTSTATEMENTDATE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_OST_ZEST) }</fml:NF_CREDIC_LASTSTATEMENTDATE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_BIEZ_MIN)
					then <fml:NF_CREDIC_CURRENTMINIMUMPAYMENT>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_BIEZ_MIN) }</fml:NF_CREDIC_CURRENTMINIMUMPAYMENT>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_SPLATA_MIN)
					then <fml:NF_CREDIC_MINIMUMAMOUNT>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_SPLATA_MIN) }</fml:NF_CREDIC_MINIMUMAMOUNT>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_DATA_B_PLAT)
					then <fml:NF_CREDIC_CURRENTPAYMENTDATE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_DATA_B_PLAT) }</fml:NF_CREDIC_CURRENTPAYMENTDATE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_NAST_ZEST)
					then <fml:NF_CREDIC_NEXTSTATEMENTDATE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_NAST_ZEST) }</fml:NF_CREDIC_NEXTSTATEMENTDATE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_NR_RACH)
					then <fml:NF_ACCOUN_ACCOUNTNUMBER>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_NR_RACH) }</fml:NF_ACCOUN_ACCOUNTNUMBER>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_KARTA)
					then <fml:NF_CREDIC_CARDTYPE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_KARTA) }</fml:NF_CREDIC_CARDTYPE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_KK_EXP_DATE)
					then <fml:NF_CREDIC_EXPIRARTIONDATE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_KK_EXP_DATE) }</fml:NF_CREDIC_EXPIRARTIONDATE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_KREDYT_PRZYZN)
					then <fml:NF_LOANA_FACEAMOUNT>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_KREDYT_PRZYZN) }</fml:NF_LOANA_FACEAMOUNT>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_ODS_SKAP_AKT)
					then <fml:NF_LOANA_INTERESTDUE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_ODS_SKAP_AKT) }</fml:NF_LOANA_INTERESTDUE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_OST_SPLATY)
					then <fml:NF_LOANA_NEXTMATURITYDATE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_OST_SPLATY) }</fml:NF_LOANA_NEXTMATURITYDATE>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:DC_DATA_URUCHOMIENIA)
					then <fml:NF_LOANTD_POSTINGDATE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:DC_DATA_URUCHOMIENIA) }</fml:NF_LOANTD_POSTINGDATE>
					else ()
			}
		       (: {
				if($bdy/m:PRIMEGetCustProductDetailResult/m:DC_DATA_PLATNOSCI)
					then <fml:DC_DATA_PLATNOSCI>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:DC_DATA_PLATNOSCI) }</fml:DC_DATA_PLATNOSCI>
					else ()
			}:)
		        (:{
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_WLASCICIEL )
					then <fml:CI_WLASCICIEL >{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_WLASCICIEL ) }</fml:CI_WLASCICIEL >
					else ()
			}
		        {
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_WSPOLPOSIADACZ )
					then <fml:CI_WSPOLPOSIADACZ >{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_WSPOLPOSIADACZ ) }</fml:CI_WSPOLPOSIADACZ >
					else ()
			}
		        {
				if($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PELNOMOCNIK)
					then <fml:CI_PELNOMOCNIK>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:CI_PELNOMOCNIK) }</fml:CI_PELNOMOCNIK>
					else ()
			}:)	
		        {
				if($bdy/m:PRIMEGetCustProductDetailResult/m:B_KOD_RACH)
					then <fml:NF_PRIMAC_ACCOUNTCODE>{ data($bdy/m:PRIMEGetCustProductDetailResult/m:B_KOD_RACH) }</fml:NF_PRIMAC_ACCOUNTCODE>
					else ()
			}	  
                        {
				if(string-length($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_OTWARCIA) > 0)
					then <fml:NF_ACCOUN_ACCOUNTOPENDATE>{fn-bea:date-from-string-with-format("dd-MM-yyy",data($bdy/m:PRIMEGetCustProductDetailResult/m:B_D_OTWARCIA)) }</fml:NF_ACCOUN_ACCOUNTOPENDATE>
					else ()
			}	
                        <NF_CTRL_SYSTEMID>2</NF_CTRL_SYSTEMID>
                        <NF_PAGEC_HASNEXT>0</NF_PAGEC_HASNEXT>
                        <NF_PAGEC_NAVIGATIONKEYVALU>12:000000000000</NF_PAGEC_NAVIGATIONKEYVALU>
                        <NF_PAGEC_PAGESIZE>1</NF_PAGEC_PAGESIZE>
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ 
 if ($body/m:PRIMEGetCustProductDetailResponse)
    then xf:mapPRIMEGetCustProductDetailResponse($body/m:PRIMEGetCustProductDetailResponse) 
    else  
 if ($body/fml:FML32)
    then $body/fml:FML32
    else  ()
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>