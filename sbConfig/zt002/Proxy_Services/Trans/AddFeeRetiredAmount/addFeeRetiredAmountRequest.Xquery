<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:baseauxentities.be.dcl";

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};

declare function xf:prepareShortAccountNumber($accountNumber as xs:string) as xs:string{
   let $accountNumberLength:= string-length($accountNumber)
   let $shortAccountNumber := substring($accountNumber,$accountNumberLength - 11)
   let $shortAccountNumberWithZeros := concat(substring("000000000000", string-length($shortAccountNumber) + 1) , $shortAccountNumber)
   return
       $shortAccountNumber
};

declare function xf:mapaddFeeRetiredAmountRequest($req as element(ns1:Account), $val as element(ns2:DecimalHolder), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

                let $msgId:= $msghead/dcl:transId
	let $companyId:= $msghead/dcl:companyId
	let $userId := $msghead/dcl:userId
	let $appId:= $msghead/dcl:appId
	let $unitId := $msghead/dcl:unitId
	let $timestamp:= $msghead/dcl:timestamp

                let $transId:=$tranhead/dcl:transId

	let $accountNumber:= $req/ns1:accountNumber
	let $kwota:= $val/ns2:value
                    
	return
   <fml:FML32>
     <DC_TRN_ID?>{data($transId)}</DC_TRN_ID>
     <DC_UZYTKOWNIK?>{concat("SKP:",data($userId))}</DC_UZYTKOWNIK>
     <DC_ODDZIAL?>{chkUnitId(data($unitId))}</DC_ODDZIAL>
     <DC_NR_RACHUNKU?>{xf:prepareShortAccountNumber($accountNumber)}</DC_NR_RACHUNKU>
     <DC_KWOTA?>{data($kwota)}</DC_KWOTA>
    </fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


<soap-env:Body>
{ xf:mapaddFeeRetiredAmountRequest($body/dcl:invoke/dcl:account/ns1:Account, $body/dcl:invoke/dcl:decimalHolder/ns2:DecimalHolder, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>