<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-11-23</con:description>
    <con:xquery><![CDATA[(: Rejestr zmian
v.1.1  2010-06-22  PK  Teet 48896: zwracanie wyłącznie aktywnych rachunków
v.1.2  2010-11-23  PK  Teet 52824: zwracanie rachunków z obszarów 2, 10, 13 (było: 2) w relacjach SOWJAFJAOJA1JA2JA3JA4JA5POGPUS (było: dowolnych)
:)

declare namespace fml="";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};



declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}</NF_MSHEAD_MSGID>
,
(: <NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}</NF_MSHEAD_COMPANYID> :)
<NF_MSHEAD_COMPANYID?>1</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}</NF_TRHEAD_TRANSID>
};


declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
(: <NF_PRODUA_CODEPRODUCTAREA?>{data($parm/ns0:productArea/ns1:ProductArea/ns1:codeProductArea)}</NF_PRODUA_CODEPRODUCTAREA> :)

(:1.2 <NF_PRODUA_CODEPRODUCTAREA?>2</NF_PRODUA_CODEPRODUCTAREA> :)
<NF_CTRL_AREAS>021013</NF_CTRL_AREAS> (: 1.2 :)
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns0:bcd/ns7:BusinessControlData/ns7:pageControl/ns6:PageControl/ns6:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns0:bcd/ns7:BusinessControlData/ns7:pageControl/ns6:PageControl/ns6:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{boolean2SourceValue (data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:reverseOrder),"1","0")}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns0:bcd/ns7:BusinessControlData/ns7:pageControl/ns6:PageControl/ns6:navigationKeyDefinition)}</NF_PAGEC_NAVIGATIONKEYDEFI>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns0:bcd/ns7:BusinessControlData/ns7:pageControl/ns6:PageControl/ns6:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
,
<NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns0:customer/ns1:Customer/ns1:customerNumber)}</NF_CUSTOM_CUSTOMERNUMBER>
,
(: 1.1 <NF_CTRL_ACTIVENONACTIVE?>0</NF_CTRL_ACTIVENONACTIVE> :)
(:1.1:) <NF_CTRL_ACTIVENONACTIVE?>1</NF_CTRL_ACTIVENONACTIVE>
,
<NF_ACCOUN_ALTERNATIVEADDRE?>0</NF_ACCOUN_ALTERNATIVEADDRE>
,
<NF_CTRL_OPTION?>0</NF_CTRL_OPTION>
,
<NF_PRODUG_VISIBILITYDCL?>1</NF_PRODUG_VISIBILITYDCL>
,
<NF_ACCOUR_RELATIONSHIP>SOWJAFJAOJA1JA2JA3JA4JA5POGPUS</NF_ACCOUR_RELATIONSHIP> (: 1.2 :)
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>