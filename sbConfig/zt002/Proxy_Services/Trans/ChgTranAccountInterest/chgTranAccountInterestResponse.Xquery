<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";

declare variable $body as element(soap-env:Body) external;

declare function xf:mapchgTranAccountInterestResponse($fml as element(fml:FML32))
	as element(dcl:invokeResponse) {
    let $rescode:=$fml/fml:DC_OPIS_BLEDU
 
    return
       <dcl:invokeResponse>
            <dcl:response>
	<ns3:ResponseMessage>
                   {
                      if (data($rescode)) then 
	         <ns3:result>false</ns3:result>
                      else 
	         <ns3:result>true</ns3:result>
                    }
                </ns3:ResponseMessage>
            </dcl:response>       

       </dcl:invokeResponse>
};


<soap-env:Body>
{ xf:mapchgTranAccountInterestResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>