<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns0="urn:basedictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};




declare function getFieldsFromHeader($parm as element(ns1:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns1:msgHeader/ns1:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns1:msgHeader/ns1:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns1:msgHeader/ns1:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns1:msgHeader/ns1:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns1:msgHeader/ns1:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns1:msgHeader/ns1:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns1:transHeader/ns1:transId)}</NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns1:invoke)) as element()*
{

<NF_TRAACA_SUBACCOUNTNUMBER?>{data($parm/ns1:tranAccountACA/ns4:TranAccountACA/ns4:subaccountNumber)}</NF_TRAACA_SUBACCOUNTNUMBER>
,
<NF_ACCOUN_ACCOUNTIBAN?>{data($parm/ns1:account/ns4:Account/ns4:accountNumber)}</NF_ACCOUN_ACCOUNTIBAN>
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns1:bcd/ns7:BusinessControlData/ns7:pageControl/ns6:PageControl/ns6:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns1:bcd/ns7:BusinessControlData/ns7:pageControl/ns6:PageControl/ns6:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{boolean2SourceValue (data($parm/ns1:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:reverseOrder),"1","0")}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns1:bcd/ns7:BusinessControlData/ns7:pageControl/ns6:PageControl/ns6:navigationKeyDefinition)}</NF_PAGEC_NAVIGATIONKEYDEFI>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns1:bcd/ns7:BusinessControlData/ns7:pageControl/ns6:PageControl/ns6:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns1:header)}
    {getFieldsFromInvoke($body/ns1:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>