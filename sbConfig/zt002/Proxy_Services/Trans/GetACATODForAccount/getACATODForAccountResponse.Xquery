<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:accounts.entities.be.dcl";
declare namespace e2 = "urn:accountdict.dictionaries.be.dcl";
declare namespace e3 = "urn:dictionaries.be.dcl";
declare namespace ns3="urn:baseauxentities.be.dcl";
declare namespace ns4="urn:entities.be.dcl";

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
  if ($value)
    then if(string-length($value)>5 and not ($value = "0001-01-01"))
        then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
    else() 
  else()
};

declare function local:mapgetACATODForAccountResponse($fml as element())
    as element(m:invokeResponse) {
		<m:invokeResponse>
			<m:tranAccountACAList>
				{
				    for $flag at $i in $fml/fml:NF_FTODAC_FLAGTODACA
					return
						<e:TranAccountACA>
                                                                          { insertDate(data($fml/NF_TRAACA_CREATIONDATE[$i]),"yyyy-MM-dd","e:creationDate")}
				          <!-- date -->	
				          
                                                                          { insertDate(data($fml/NF_TRAACA_NEXTMATURITYDATE[$i]),"yyyy-MM-dd","e:nextMaturityDate")}
				          <!-- date -->
				          	
				          <e:limitAmount>{ data($fml/fml:NF_TRAACA_LIMITAMOUNT[$i]) }</e:limitAmount>
				          <!-- double -->
				          	
				          <e:indexNumber>{ data($fml/fml:NF_TRAACA_INDEXNUMBER[$i]) }</e:indexNumber>
				          <!-- string -->	
				          
				          <e:varianceForIndex>{ data($fml/fml:NF_TRAACA_VARIANCEFORINDEX[$i]) }</e:varianceForIndex>
				          <!-- double -->	
				          
				          <e:negotiatedInterestRate>{ data($fml/fml:NF_TRAACA_NEGOTIATEDINTERE[$i]) }</e:negotiatedInterestRate>
				          <!-- double -->	
				          
				          <e:authorisingOfficerCode>{ data($fml/fml:NF_TRAACA_AUTHORISINGOFFIC[$i]) }</e:authorisingOfficerCode>
				          <!-- string -->	
				          
				          <e:interestPdYTD>{ data($fml/fml:NF_TRAACA_INTERESTPDYTD[$i]) }</e:interestPdYTD>
				          <!-- double -->
				          	
				          <e:interestAccrUnpd>{ data($fml/fml:NF_TRAACA_INTERESTACCRUNPD[$i]) }</e:interestAccrUnpd>
				          <!-- double -->	
				          
				          <e:interestFrequency>{ data($fml/fml:NF_TRAACA_INTERESTFREQUENC[$i]) }</e:interestFrequency>
				          <!-- int -->	
				          
                                                                          { insertDate(data($fml/NF_TRAACA_NEXTINTERESTDATE[$i]),"yyyy-MM-dd","e:nextInterestDate")}
				          <!-- date -->	
				          
				          <e:accrualBase>{ data($fml/fml:NF_TRAACA_ACCRUALBASE[$i]) }</e:accrualBase>
				          <!-- string -->	
				          
				          <e:yearBase>{ data($fml/fml:NF_TRAACA_YEARBASE[$i]) }</e:yearBase>
				          <!-- string -->	

				          <e:interestAccuredLastDay>{ data($fml/fml:NF_TRAACA_INTERESTACCUREDL[$i]) }</e:interestAccuredLastDay>
				          <!-- string -->	
				          
				          <e:interestRate>{ data($fml/fml:NF_TRAACA_INTERESTRATE[$i]) }</e:interestRate>
				          <!-- double -->	
				          
				          <e:intAccrForPer>{ data($fml/fml:NF_TRAACA_INTACCRFORPER[$i]) }</e:intAccrForPer>
				          <!-- double -->	
				          
				          <e:accrualBalance>{ data($fml/fml:NF_TRAACA_ACCRUALBALANCE[$i]) }</e:accrualBalance>
				          <!-- double -->	
				          
                                                                          { insertDate(data($fml/NF_TRAACA_INTERESTPDLAST[$i]),"yyyy-MM-dd","e:interestPdLast")}
				          <!-- date -->	
				          
				          <e:intAsOfLstRun>{ data($fml/fml:NF_TRAACA_INTASOFLSTRUN[$i]) }</e:intAsOfLstRun>          
				          <!-- double -->	
				          
				          <e:usedLimitAmount>{ data($fml/fml:NF_TRAACA_ACCRUALBALANCE[$i]) }</e:usedLimitAmount>
				          <!-- double -->	
				          
                                                                          { insertDate(data($fml/NF_TRAACA_DATELASTRATECHAN[$i]),"yyyy-MM-dd","e:dateLastRateChange")}
				          <!-- date -->	
				          
                                                                          <e:subaccountNumber>{ data($fml/fml:NF_TRAACA_SUBACCOUNTNUMBER[$i]) }</e:subaccountNumber>

				          <e:feePercentage?>{ data($fml/fml:NF_TRAACA_FEEPERCENTAGE[$i]) }</e:feePercentage>
				          <!-- double -->	
				          
				          <e:feeAmount?>{ data($fml/fml:NF_TRAACA_FEEAMOUNT[$i]) }</e:feeAmount>
				          <!-- double -->	
				          
				          <e:minimumFeeAmount>{ data($fml/fml:NF_TRAACA_MINIMUMFEEAMOUNT[$i]) }</e:minimumFeeAmount>
				          <!-- double -->	
				          
				          <e:maximumFeeAmount>{ data($fml/fml:NF_TRAACA_MAXIMUMFEEAMOUNT[$i]) }</e:maximumFeeAmount>
				          <!-- double -->	
				          
				          <e:acaFeeFrequency>{ data($fml/fml:NF_FREQUE_FREQUENCY[$i]) }</e:acaFeeFrequency>
				          <!-- int -->	
				          
                                                                          { insertDate(data($fml/NF_TRAACA_NEXTACAFEEDATE[$i]),"yyyy-MM-dd","e:nextAcaFeeDate")}
				          <!-- date -->	
				          
                                                                          { insertDate(data($fml/NF_TRAACA_CBREPORTCDRPTDAT[$i]),"yyyy-MM-dd","e:cbReportCdrptDate")}
				          <!-- date -->	
				          
				          <e:intRate>{ data($fml/fml:NF_TRAACA_INTRATE[$i]) }</e:intRate>
				          <!-- double -->	
				          
				          <e:feesThisYear>{ data($fml/fml:NF_TRAACA_FEESTHISYEAR[$i]) }</e:feesThisYear>
				          <!-- double -->	
				          
				          <e:feesLastYear>{ data($fml/fml:NF_TRAACA_FEESLASTYEAR[$i]) }</e:feesLastYear>
				          <!-- double -->	
				          
				          <e:commitmentFee>{ data($fml/fml:NF_TRAACA_COMMITMENTFEE[$i]) }</e:commitmentFee>
				          <!-- double -->
				
						  <e:tranAccount>
							<e:TranAccount>
								<e:monthCredit?>{data($fml/fml:NF_TRANA_MONTHCREDIT[$i])}</e:monthCredit>
							</e:TranAccount>
						  </e:tranAccount>
				
				          <e:flagTODACA>
				          	<e2:FlagTODACA>
				          		<e2:flagTODACA>{ data($flag) }</e2:flagTODACA>
				          	</e2:FlagTODACA>
				          </e:flagTODACA>
				          <e:interestPeriod>
				          	<e3:Period>
					          	<e3:period>{ data($fml/fml:NF_PERIOD_PERIOD[$i]) }</e3:period>
				          	</e3:Period>
				          </e:interestPeriod>
				          <e:acaFeeCode>
				          	<e2:AcaFeeCode>
					          	<e2:acaFeeCode>{ data($fml/fml:NF_ACAFC_ACAFEECODE[$i]) }</e2:acaFeeCode>
				          	</e2:AcaFeeCode>
				          </e:acaFeeCode>
				          <e:acaFeePeriod>
				          	<e3:Period>
					          	<e3:period>{ data($fml/fml:NF_PERIOD_PERIOD2[$i]) }</e3:period>
				          	</e3:Period>
				          </e:acaFeePeriod>
				          <e:acaFeeSpecificDay>
				          	<e3:SpecialDay>
					          	<e3:specialDay>{ data($fml/fml:NF_SPECID_SPECIALDAY[$i]) }</e3:specialDay>
				          	</e3:SpecialDay>
				          </e:acaFeeSpecificDay>
                                       
				          <e:renewTheLimit>
				          	<e2:RenewTheLimit>
				          		<e2:renewTheLimit>{ data($fml/fml:NF_RENETL_RENEWTHELIMIT[$i]) }</e2:renewTheLimit>
				          	</e2:RenewTheLimit>
				          </e:renewTheLimit>
				          <e:signedAgreementForLimitInKO>
				          	<e2:SignedAgreementForLimitInKO>
				          		<e2:signedAgreementForLimitInKO>{ data($fml/fml:NF_SAFLIK_SIGNEDAGREEMENTF[$i]) }</e2:signedAgreementForLimitInKO>
				          	</e2:SignedAgreementForLimitInKO>
				          </e:signedAgreementForLimitInKO>
				          <e:interestSpecialDay>
				          	<e3:SpecialDay>
					          	<e3:specialDay>{ data($fml/fml:NF_SPECID_SPECIALDAY2[$i]) }</e3:specialDay>
				          	</e3:SpecialDay>
				          </e:interestSpecialDay>
						  					  				          						  
						</e:TranAccountACA>
				
					}
				
				</m:tranAccountACAList>
                                <m:bcd>
                                  <ns4:BusinessControlData>
                                    <ns4:pageControl>
                                      <ns3:PageControl>
                                        <ns3:hasNext>{data($fml/fml:NF_PAGEC_HASNEXT)}</ns3:hasNext>
                                        <ns3:navigationKeyDefinition>{data($fml/fml:NF_PAGEC_NAVIGATIONKEYDEFI)}</ns3:navigationKeyDefinition>
                                        <ns3:navigationKeyValue>{data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)}</ns3:navigationKeyValue>
                                      </ns3:PageControl>
                                    </ns4:pageControl>
                                  </ns4:BusinessControlData>
                                </m:bcd>
			</m:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ local:mapgetACATODForAccountResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>