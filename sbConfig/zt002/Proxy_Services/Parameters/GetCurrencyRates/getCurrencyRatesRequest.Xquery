<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:dictionaries.be.dcl";
declare namespace m2 = "urn:baseauxentities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
declare variable $req := $body/m:invoke;
declare variable $reqh := $header/m:header;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value < 10) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value < 10) 
         then fn:concat("000",$string)
      else if ($value < 100) 
         then fn:concat("00",$string)
      else if ($value < 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:dateTime) as xs:string {
    fn:concat (xf:convertTo2CharString(fn:day-from-dateTime($dateIn)),"-",
               xf:convertTo2CharString(fn:month-from-dateTime($dateIn)),"-",
               xf:convertTo4CharString(fn:year-from-dateTime($dateIn)))
};

declare function xf:mapTime($dateIn as xs:dateTime) as xs:string {
    fn:concat (xf:convertTo2CharString(fn:hours-from-dateTime($dateIn)),
               xf:convertTo2CharString(fn:minutes-from-dateTime($dateIn)),
               xf:convertTo2CharString(xs:integer(fn:seconds-from-dateTime($dateIn))))
};

<soap-env:Body>
  <fml:FML32>
    <fml:B_MSHEAD_MSGID?>{ data($reqh/m:msgHeader/m:msgId) }</fml:B_MSHEAD_MSGID>

    {for $it in $req/m:currencyCodes/m1:CurrencyCode
       return
       if($it/m1:currencyCode)
         then <fml:B_WALUTA>{ data($it/m1:currencyCode) }</fml:B_WALUTA>
         else ()
    }  
    {if($req/m:rateDate/m2:DateHolder/m2:value and fn:string-length($req/m:rateDate/m2:DateHolder/m2:value)>0)
       then <fml:B_DATA_WALUTY>{xf:mapDate(data($req/m:rateDate/m2:DateHolder/m2:value)) }</fml:B_DATA_WALUTY>
       else ()
    }
    {if($req/m:rateDate/m2:DateHolder/m2:value and fn:string-length($req/m:rateDate/m2:DateHolder/m2:value)>0)
       then <fml:B_GODZINA>{xf:mapTime(data($req/m:rateDate/m2:DateHolder/m2:value)) }</fml:B_GODZINA>
       else ()
    }
  </fml:FML32>
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>