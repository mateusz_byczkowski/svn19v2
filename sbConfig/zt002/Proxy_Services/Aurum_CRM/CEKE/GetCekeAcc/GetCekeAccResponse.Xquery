<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCekeAccResponse($fml as element(fml:FML32))
	as element(m:GetCekeAccResponse) {
		<m:GetCekeAccResponse>
			{
				if($fml/fml:B_KOD_RACH)
					then <m:KodRach>{ data($fml/fml:B_KOD_RACH) }</m:KodRach>
					else ()
			}
			{
				if($fml/fml:B_DL_NR_RACH)
					then <m:DlNrRach>{ data($fml/fml:B_DL_NR_RACH) }</m:DlNrRach>
					else ()
			}
			{
				if($fml/fml:B_SALDO)
					then <m:Saldo>{ data($fml/fml:B_SALDO) }</m:Saldo>
					else ()
			}
			{
				if($fml/fml:B_DOST_SRODKI)
					then <m:DostSrodki>{ data($fml/fml:B_DOST_SRODKI) }</m:DostSrodki>
					else ()
			}
			{
				if($fml/fml:B_WALUTA)
					then <m:Waluta>{ data($fml/fml:B_WALUTA) }</m:Waluta>
					else ()
			}
			{
				if($fml/fml:B_KOD_WALUTY)
					then <m:KodWaluty>{ data($fml/fml:B_KOD_WALUTY) }</m:KodWaluty>
					else ()
			}
			{
				if($fml/fml:B_DATA_OST_OPER)
					then <m:DataOstOper>{ data($fml/fml:B_DATA_OST_OPER) }</m:DataOstOper>
					else ()
			}
			{
				if($fml/fml:B_TYP_RACH)
					then <m:TypRach>{ data($fml/fml:B_TYP_RACH) }</m:TypRach>
					else ()
			}
			{
				if($fml/fml:B_OPIS_RACH)
					then <m:OpisRach>{ data($fml/fml:B_OPIS_RACH) }</m:OpisRach>
					else ()
			}
			{
				if($fml/fml:B_SKROT_OPISU)
					then <m:SkrotOpisu>{ data($fml/fml:B_SKROT_OPISU) }</m:SkrotOpisu>
					else ()
			}
			{
				if($fml/fml:B_ID_ODDZ)
					then <m:IdOddz>{ data($fml/fml:B_ID_ODDZ) }</m:IdOddz>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_ODDZ)
					then <m:NazwaOddz>{ data($fml/fml:B_NAZWA_ODDZ) }</m:NazwaOddz>
					else ()
			}
			{
				if($fml/fml:B_RODZAJ_RACH)
					then <m:RodzajRach>{ data($fml/fml:B_RODZAJ_RACH) }</m:RodzajRach>
					else ()
			}
			{
				if($fml/fml:B_MIKROODDZIAL)
					then <m:Mikrooddzial>{ data($fml/fml:B_MIKROODDZIAL) }</m:Mikrooddzial>
					else ()
			}
			{
				if($fml/fml:B_PRAWA_KLIENTA)
					then <m:PrawaKlienta>{ data($fml/fml:B_PRAWA_KLIENTA) }</m:PrawaKlienta>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KLIENTA)
					then <m:NazwaKlienta>{ data($fml/fml:B_NAZWA_KLIENTA) }</m:NazwaKlienta>
					else ()
			}
			{
				if($fml/fml:B_UL_ZAM)
					then <m:UlZam>{ data($fml/fml:B_UL_ZAM) }</m:UlZam>
					else ()
			}
			{
				if($fml/fml:B_M_ZAM)
					then <m:MZam>{ data($fml/fml:B_M_ZAM) }</m:MZam>
					else ()
			}
			{
				if($fml/fml:B_OPROC1)
					then <m:Oproc1>{ data($fml/fml:B_OPROC1) }</m:Oproc1>
					else ()
			}
			{
				if($fml/fml:B_D_OTWARCIA)
					then <m:DOtwarcia>{ data($fml/fml:B_D_OTWARCIA) }</m:DOtwarcia>
					else ()
			}
			{
				if($fml/fml:B_D_ZAMKNIECIA)
					then <m:DZamkniecia>{ data($fml/fml:B_D_ZAMKNIECIA) }</m:DZamkniecia>
					else ()
			}
			{
				if($fml/fml:B_D_OSTATNIEJ_KAP)
					then <m:DOstatniejKap>{ data($fml/fml:B_D_OSTATNIEJ_KAP) }</m:DOstatniejKap>
					else ()
			}
			{
				if($fml/fml:B_D_NASTEPNEJ_KAP)
					then <m:DNastepnejKap>{ data($fml/fml:B_D_NASTEPNEJ_KAP) }</m:DNastepnejKap>
					else ()
			}
			{
				if($fml/fml:B_D_POCZ_WKL)
					then <m:DPoczWkl>{ data($fml/fml:B_D_POCZ_WKL) }</m:DPoczWkl>
					else ()
			}
			{
				if($fml/fml:B_D_KON_WKL)
					then <m:DKonWkl>{ data($fml/fml:B_D_KON_WKL) }</m:DKonWkl>
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_AKT)
					then <m:OdsSkapAkt>{ data($fml/fml:B_ODS_SKAP_AKT) }</m:OdsSkapAkt>
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_POP)
					then <m:OdsSkapPop>{ data($fml/fml:B_ODS_SKAP_POP) }</m:OdsSkapPop>
					else ()
			}
			{
				if($fml/fml:B_LIMIT2)
					then <m:Limit2>{ data($fml/fml:B_LIMIT2) }</m:Limit2>
					else ()
			}
			{
				if($fml/fml:B_D_KONCA_LIMITU)
					then <m:DKoncaLimitu>{ data($fml/fml:B_D_KONCA_LIMITU) }</m:DKoncaLimitu>
					else ()
			}
			{
				if($fml/fml:B_BLOKADA)
					then <m:Blokada>{ data($fml/fml:B_BLOKADA) }</m:Blokada>
					else ()
			}
			{
				if($fml/fml:B_RODZAJ_OPROC)
					then <m:RodzajOproc>{ data($fml/fml:B_RODZAJ_OPROC) }</m:RodzajOproc>
					else ()
			}
			{
				if($fml/fml:B_KAPITALIZACJA)
					then <m:Kapitalizacja>{ data($fml/fml:B_KAPITALIZACJA) }</m:Kapitalizacja>
					else ()
			}
			{
				if($fml/fml:B_PRZEKS_KAP)
					then <m:PrzeksKap>{ data($fml/fml:B_PRZEKS_KAP) }</m:PrzeksKap>
					else ()
			}
			{
				if($fml/fml:B_OKR_WKLADU)
					then <m:OkrWkladu>{ data($fml/fml:B_OKR_WKLADU) }</m:OkrWkladu>
					else ()
			}
			{
				if($fml/fml:B_JDN_OKR_WKLADU)
					then <m:JdnOkrWkladu>{ data($fml/fml:B_JDN_OKR_WKLADU) }</m:JdnOkrWkladu>
					else ()
			}
			{
				if($fml/fml:B_D_NALICZ_ODS)
					then <m:DNaliczOds>{ data($fml/fml:B_D_NALICZ_ODS) }</m:DNaliczOds>
					else ()
			}
			{
				if($fml/fml:B_D_NAST_OPER)
					then <m:DNastOper>{ data($fml/fml:B_D_NAST_OPER) }</m:DNastOper>
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_UWN)
					then <m:ObrotyDzienUwn>{ data($fml/fml:B_OBROTY_DZIEN_UWN) }</m:ObrotyDzienUwn>
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_UMA)
					then <m:ObrotyDzienUma>{ data($fml/fml:B_OBROTY_DZIEN_UMA) }</m:ObrotyDzienUma>
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_NWN)
					then <m:ObrotyDzienNwn>{ data($fml/fml:B_OBROTY_DZIEN_NWN) }</m:ObrotyDzienNwn>
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_NMA)
					then <m:ObrotyDzienNma>{ data($fml/fml:B_OBROTY_DZIEN_NMA) }</m:ObrotyDzienNma>
					else ()
			}
			{
				if($fml/fml:B_OPROC2)
					then <m:Oproc2>{ data($fml/fml:B_OPROC2) }</m:Oproc2>
					else ()
			}
			{
				if($fml/fml:B_NR_WYCIAGU)
					then <m:NrWyciagu>{ data($fml/fml:B_NR_WYCIAGU) }</m:NrWyciagu>
					else ()
			}
			{
				if($fml/fml:B_D_POP_WYCIAGU)
					then <m:DPopWyciagu>{ data($fml/fml:B_D_POP_WYCIAGU) }</m:DPopWyciagu>
					else ()
			}
			{
				if($fml/fml:B_D_NAST_WYCIAGU)
					then <m:DNastWyciagu>{ data($fml/fml:B_D_NAST_WYCIAGU) }</m:DNastWyciagu>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KR)
					then <m:NazwaKr>{ data($fml/fml:B_NAZWA_KR) }</m:NazwaKr>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_STOPY)
					then <m:NazwaStopy>{ data($fml/fml:B_NAZWA_STOPY) }</m:NazwaStopy>
					else ()
			}
			{
				if($fml/fml:B_STOPA)
					then <m:Stopa>{ data($fml/fml:B_STOPA) }</m:Stopa>
					else ()
			}
			{
				if($fml/fml:B_OKR_OPROC)
					then <m:OkrOproc>{ data($fml/fml:B_OKR_OPROC) }</m:OkrOproc>
					else ()
			}
			{
				if($fml/fml:B_JDN_OKR_OPROC)
					then <m:JdnOkrOproc>{ data($fml/fml:B_JDN_OKR_OPROC) }</m:JdnOkrOproc>
					else ()
			}
			{
				if($fml/fml:B_LIMIT_BIEZ_KR)
					then <m:LimitBiezKr>{ data($fml/fml:B_LIMIT_BIEZ_KR) }</m:LimitBiezKr>
					else ()
			}
			{
				if($fml/fml:B_KREDYT_PRZYZN)
					then <m:KredytPrzyzn>{ data($fml/fml:B_KREDYT_PRZYZN) }</m:KredytPrzyzn>
					else ()
			}
			{
				if($fml/fml:B_KOD_RACH_NIEPRZET)
					then <m:KodRachNieprzet>{ data($fml/fml:B_KOD_RACH_NIEPRZET) }</m:KodRachNieprzet>
					else ()
			}
			{
				if($fml/fml:B_REWOLWING)
					then <m:Rewolwing>{ data($fml/fml:B_REWOLWING) }</m:Rewolwing>
					else ()
			}
			{
				if($fml/fml:B_AUTO_KAPITAL)
					then <m:AutoKapital>{ data($fml/fml:B_AUTO_KAPITAL) }</m:AutoKapital>
					else ()
			}
			{
				if($fml/fml:B_AUTO_ODSETKI)
					then <m:AutoOdsetki>{ data($fml/fml:B_AUTO_ODSETKI) }</m:AutoOdsetki>
					else ()
			}
			{
				if($fml/fml:B_KOD_RACH_ODSETKI)
					then <m:KodRachOdsetki>{ data($fml/fml:B_KOD_RACH_ODSETKI) }</m:KodRachOdsetki>
					else ()
			}
			{
				if($fml/fml:B_KOD_RACH_KAPITAL)
					then <m:KodRachKapital>{ data($fml/fml:B_KOD_RACH_KAPITAL) }</m:KodRachKapital>
					else ()
			}
			{
				if($fml/fml:B_D_OST_SPLATY)
					then <m:DOstSplaty>{ data($fml/fml:B_D_OST_SPLATY) }</m:DOstSplaty>
					else ()
			}
			{
				if($fml/fml:B_NAJBLIZSZE_NAL)
					then <m:NajblizszeNal>{ data($fml/fml:B_NAJBLIZSZE_NAL) }</m:NajblizszeNal>
					else ()
			}
			{
				if($fml/fml:B_NAJBLIZSZE_ODS)
					then <m:NajblizszeOds>{ data($fml/fml:B_NAJBLIZSZE_ODS) }</m:NajblizszeOds>
					else ()
			}
			{
				if($fml/fml:B_NAJBLIZSZE_RATY)
					then <m:NajblizszeRaty>{ data($fml/fml:B_NAJBLIZSZE_RATY) }</m:NajblizszeRaty>
					else ()
			}
			{
				if($fml/fml:B_SUMA_ODS_ZAPL)
					then <m:SumaOdsZapl>{ data($fml/fml:B_SUMA_ODS_ZAPL) }</m:SumaOdsZapl>
					else ()
			}
			{
				if($fml/fml:B_D_POP_WYMAG)
					then <m:DPopWymag>{ data($fml/fml:B_D_POP_WYMAG) }</m:DPopWymag>
					else ()
			}
			{
				if($fml/fml:B_ODS_POP_WYMAG)
					then <m:OdsPopWymag>{ data($fml/fml:B_ODS_POP_WYMAG) }</m:OdsPopWymag>
					else ()
			}
			{
				if($fml/fml:B_D_PRD_WYMAG)
					then <m:DPrdWymag>{ data($fml/fml:B_D_PRD_WYMAG) }</m:DPrdWymag>
					else ()
			}
			{
				if($fml/fml:B_NR_ANEKSU)
					then <m:NrAneksu>{ data($fml/fml:B_NR_ANEKSU) }</m:NrAneksu>
					else ()
			}
			{
				if($fml/fml:B_DOCHODY_DO)
					then <m:DochodyDo>{ data($fml/fml:B_DOCHODY_DO) }</m:DochodyDo>
					else ()
			}
			{
				if($fml/fml:B_ODS_NIEROZL)
					then <m:OdsNierozl>{ data($fml/fml:B_ODS_NIEROZL) }</m:OdsNierozl>
					else ()
			}
			{
				if($fml/fml:B_UL_KORESP)
					then <m:UlKoresp>{ data($fml/fml:B_UL_KORESP) }</m:UlKoresp>
					else ()
			}
			{
				if($fml/fml:B_M_KORESP)
					then <m:MKoresp>{ data($fml/fml:B_M_KORESP) }</m:MKoresp>
					else ()
			}
			{
				if($fml/fml:B_KOD_KORESP)
					then <m:KodKoresp>{ data($fml/fml:B_KOD_KORESP) }</m:KodKoresp>
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KORESP)
					then <m:NazwaKoresp>{ data($fml/fml:B_NAZWA_KORESP) }</m:NazwaKoresp>
					else ()
			}
			{
				if($fml/fml:B_ZAL_MIN)
					then <m:ZalMin>{ data($fml/fml:B_ZAL_MIN) }</m:ZalMin>
					else ()
			}
			{
				if($fml/fml:B_BIEZ_MIN)
					then <m:BiezMin>{ data($fml/fml:B_BIEZ_MIN) }</m:BiezMin>
					else ()
			}
			{
				if($fml/fml:B_DATA_B_PLAT)
					then <m:DataBPlat>{ data($fml/fml:B_DATA_B_PLAT) }</m:DataBPlat>
					else ()
			}
			{
				if($fml/fml:B_SPLATA_RAZEM)
					then <m:SplataRazem>{ data($fml/fml:B_SPLATA_RAZEM) }</m:SplataRazem>
					else ()
			}
			{
				if($fml/fml:B_ODS_POP_OKR)
					then <m:OdsPopOkr>{ data($fml/fml:B_ODS_POP_OKR) }</m:OdsPopOkr>
					else ()
			}
			{
				if($fml/fml:B_D_OST_ZEST)
					then <m:DOstZest>{ data($fml/fml:B_D_OST_ZEST) }</m:DOstZest>
					else ()
			}
			{
				if($fml/fml:B_D_NAST_ZEST)
					then <m:DNastZest>{ data($fml/fml:B_D_NAST_ZEST) }</m:DNastZest>
					else ()
			}
			{
				if($fml/fml:B_PRAVA)
					then <m:Prava>{ data($fml/fml:B_PRAVA) }</m:Prava>
					else ()
			}
			{
				if($fml/fml:B_D_OST_PRZEL)
					then <m:DOstPrzel>{ data($fml/fml:B_D_OST_PRZEL) }</m:DOstPrzel>
					else ()
			}
			{
				if($fml/fml:B_OST_PRZELEW)
					then <m:OstPrzelew>{ data($fml/fml:B_OST_PRZELEW) }</m:OstPrzelew>
					else ()
			}
			{
				if($fml/fml:B_NR_WNIOSKU)
					then <m:NrWniosku>{ data($fml/fml:B_NR_WNIOSKU) }</m:NrWniosku>
					else ()
			}
			{
				if($fml/fml:B_SPLATA_MIN)
					then <m:SplataMin>{ data($fml/fml:B_SPLATA_MIN) }</m:SplataMin>
					else ()
			}
			{
				let $B_LIMIT_OD := $fml/fml:B_LIMIT_OD
				let $B_LIMIT_KWOTA := $fml/fml:B_LIMIT_KWOTA
				for $it at $p in $fml/fml:B_LIMIT_OD
				return
					<m:GetCekeAccTransza>
					{
						if($B_LIMIT_OD[$p])
							then <m:LimitOd>{ data($B_LIMIT_OD[$p]) }</m:LimitOd>
						else ()
					}
					{
						if($B_LIMIT_KWOTA[$p])
							then <m:LimitKwota>{ data($B_LIMIT_KWOTA[$p]) }</m:LimitKwota>
						else ()
					}
					</m:GetCekeAccTransza>
			}
			{
				let $B_D_ODS_NALICZ := $fml/fml:B_D_ODS_NALICZ
				let $B_D_WYMAGALNOSCI := $fml/fml:B_D_WYMAGALNOSCI
				for $it at $p in $fml/fml:B_D_ODS_NALICZ
				return
					<m:GetCekeAccOdsetki>
					{
						if($B_D_ODS_NALICZ[$p])
							then <m:DOdsNalicz>{ data($B_D_ODS_NALICZ[$p]) }</m:DOdsNalicz>
						else ()
					}
					{
						if($B_D_WYMAGALNOSCI[$p])
							then <m:DWymagalnosci>{ data($B_D_WYMAGALNOSCI[$p]) }</m:DWymagalnosci>
						else ()
					}
					</m:GetCekeAccOdsetki>
			}
		</m:GetCekeAccResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapGetCekeAccResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>