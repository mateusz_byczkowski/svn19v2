<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function xf:mapCRMGetProfCustRequest($req as element(m:CRMGetProfCustRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:IdWewPrac)
					then <fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }</fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }</fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:TypRaportu)
					then <fml:CI_TYP_RAPORTU>{ data($req/m:TypRaportu) }</fml:CI_TYP_RAPORTU>
					else ()
			}
			{
				if($req/m:ZnacznikOkresu)
					then <fml:CI_ZNACZNIK_OKRESU>{ data($req/m:ZnacznikOkresu) }</fml:CI_ZNACZNIK_OKRESU>
					else ()
			}
		</fml:FML32>
};

declare variable $req as element(m:CRMGetProfCustRequest) external;
<soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
{ xf:mapCRMGetProfCustRequest($req) }
</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>