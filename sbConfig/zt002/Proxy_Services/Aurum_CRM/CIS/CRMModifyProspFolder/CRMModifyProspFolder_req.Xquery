<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMModProspFolderRequest($req as element(m:CRMModifyProspFolderRequest))
	as element(fml:FML32) {
		<fml:FML32>
	{
if ($req/m:CustId) then
<fml:CI_NUMER_KLIENTA>{data($req/m:CustId)}</fml:CI_NUMER_KLIENTA>
else ()
}
{
if ($req/m:CompanyId) then
<fml:CI_ID_SPOLKI>{data($req/m:CompanyId)}</fml:CI_ID_SPOLKI>
else ()
}
{
if ($req/m:MarketingAgree) then
<fml:CI_UDOSTEP_GRUPA>{data($req/m:MarketingAgree)}</fml:CI_UDOSTEP_GRUPA>
else ()
}
{
if ($req/m:InfoSaleAgree) then
<fml:CI_ZGODA_INF_HANDLOWA>{data($req/m:InfoSaleAgree)}</fml:CI_ZGODA_INF_HANDLOWA>
else ()
}
{
if ($req/m:CustName) then
<fml:CI_NAZWA_PELNA>{data($req/m:CustName)}</fml:CI_NAZWA_PELNA>
else ()
}
{
if ($req/m:CustAddressStreet) then
<fml:DC_ULICA_DANE_PODST>{data($req/m:CustAddressStreet)}</fml:DC_ULICA_DANE_PODST>
else ()
}
{
if ($req/m:CustAddressHouseFlatNo) then
<fml:DC_NR_POSES_LOKALU_DANE_PODST>{data($req/m:CustAddressHouseFlatNo)}</fml:DC_NR_POSES_LOKALU_DANE_PODST>
else ()
}
{
if ($req/m:CustAddressPostCode) then
<fml:DC_KOD_POCZTOWY_DANE_PODST>{data($req/m:CustAddressPostCode)}</fml:DC_KOD_POCZTOWY_DANE_PODST>
else ()
}
{
if ($req/m:CustAddressCity) then
<fml:DC_MIASTO_DANE_PODST>{data($req/m:CustAddressCity)}</fml:DC_MIASTO_DANE_PODST>
else ()
}
{
if ($req/m:CustAddressCounty) then
<fml:DC_WOJ_KRAJ_DANE_PODST>{data($req/m:CustAddressCounty)}</fml:DC_WOJ_KRAJ_DANE_PODST>
else ()
}
{
if ($req/m:CustAddressCountry) then
<fml:CI_KOD_KRAJU>{data($req/m:CustAddressCountry)}</fml:CI_KOD_KRAJU>
else ()
}
{
if ($req/m:CustAddressForFlag) then
<fml:DC_TYP_ADRESU>{data($req/m:CustAddressForFlag)}</fml:DC_TYP_ADRESU>
else ()
}
{
if ($req/m:ResidentFlag) then
<fml:DC_REZ_NIEREZ>{data($req/m:ResidentFlag)}</fml:DC_REZ_NIEREZ>
else ()
}
{
if ($req/m:Email) then
<fml:DC_ADRES_E_MAIL>{data($req/m:Email)}</fml:DC_ADRES_E_MAIL>
else ()
}
{
if ($req/m:PhoneNo) then
<fml:DC_NR_TELEFONU>{data($req/m:PhoneNo)}</fml:DC_NR_TELEFONU>
else ()
}
{
if ($req/m:FaxNo) then
<fml:DC_NUMER_FAKSU>{data($req/m:FaxNo)}</fml:DC_NUMER_FAKSU>
else ()
}
{
if ($req/m:MobileNo) then
<fml:DC_NR_TELEF_KOMORKOWEGO>{data($req/m:MobileNo)}</fml:DC_NR_TELEF_KOMORKOWEGO>
else ()
}
{
if ($req/m:BusinessPhoneNo) then
<fml:DC_NUMER_TELEFONU_PRACA>{data($req/m:BusinessPhoneNo)}</fml:DC_NUMER_TELEFONU_PRACA>
else ()
}
{
if ($req/m:CustIdCardNo) then
<fml:DC_NR_DOWODU_REGON>{data($req/m:CustIdCardNo)}</fml:DC_NR_DOWODU_REGON>
else ()
}
{
if ($req/m:RegonNo) then
<fml:DC_NR_DOWODU_REGON>{data($req/m:RegonNo)}</fml:DC_NR_DOWODU_REGON>
else ()
}
{
if ($req/m:CustPesel) then
<fml:DC_NR_PESEL>{data($req/m:CustPesel)}</fml:DC_NR_PESEL>
else ()
}
{
if ($req/m:NipNo) then
<fml:DC_NIP>{data($req/m:NipNo)}</fml:DC_NIP>
else ()
}
{
if ($req/m:NipGrantDate) then
<fml:CI_DATA_NIP>{data($req/m:NipGrantDate)}</fml:CI_DATA_NIP>
else ()
}
{
if ($req/m:Spw) then
<fml:DC_SPW>{data($req/m:Spw)}</fml:DC_SPW>
else ()
}
{
if ($req/m:SignCardIcbsNo) then
<fml:DC_KARTA_WZOROW_PODPISOW>{data($req/m:SignCardIcbsNo)}</fml:DC_KARTA_WZOROW_PODPISOW>
else ()
}
{
if ($req/m:DataSrc) then
<fml:DC_ZRODLO_DANYCH>{data($req/m:DataSrc)}</fml:DC_ZRODLO_DANYCH>
else ()
}
{
if ($req/m:CorpoUnitId) then
<fml:DC_JEDNOSTKA_KORPORACYJNA>{data($req/m:CorpoUnitId)}</fml:DC_JEDNOSTKA_KORPORACYJNA>
else ()
}
{
if ($req/m:TaxPercentage) then
<fml:CI_PROCENT_PODATKU>{data($req/m:TaxPercentage)}</fml:CI_PROCENT_PODATKU>
else ()
}
{
if ($req/m:OtherBankPrds) then
<fml:CI_PROD_INNE_BANKI>{data($req/m:OtherBankPrds)}</fml:CI_PROD_INNE_BANKI>
else ()
}
{
if ($req/m:NiNo) then
<fml:CI_SERIA_NR_DOK>{data($req/m:NiNo)}</fml:CI_SERIA_NR_DOK>
else ()
}
{
if ($req/m:CustPassportNo) then
<fml:DC_NUMER_PASZPORTU>{data($req/m:CustPassportNo)}</fml:DC_NUMER_PASZPORTU>
else ()
}
{
if ($req/m:CarOwnerFlag) then
<fml:CI_POSIADA_SAMOCHOD>{data($req/m:CarOwnerFlag)}</fml:CI_POSIADA_SAMOCHOD>
else ()
}
{
if ($req/m:BankRelative) then
<fml:DC_PRACOWNIK_BANKU>{data($req/m:BankRelative)}</fml:DC_PRACOWNIK_BANKU>
else ()
}
{
if ($req/m:CustSex) then
<fml:DC_PLEC>{data($req/m:CustSex)}</fml:DC_PLEC>
else ()
}
{
if ($req/m:CustFirstName) then
<fml:DC_IMIE>{data($req/m:CustFirstName)}</fml:DC_IMIE>
else ()
}
{
if ($req/m:CustSecondName) then
<fml:DC_DRUGIE_IMIE>{data($req/m:CustSecondName)}</fml:DC_DRUGIE_IMIE>
else ()
}
{
if ($req/m:CustSurname) then
<fml:DC_NAZWISKO>{data($req/m:CustSurname)}</fml:DC_NAZWISKO>
else ()
}
{
if ($req/m:FatherName) then
<fml:DC_IMIE_OJCA>{data($req/m:FatherName)}</fml:DC_IMIE_OJCA>
else ()
}
{
if ($req/m:MotherMaidenName) then
<fml:DC_NAZWISKO_PANIENSKIE_MATKI>{data($req/m:MotherMaidenName)}</fml:DC_NAZWISKO_PANIENSKIE_MATKI>
else ()
}
{
if ($req/m:Nationality) then
<fml:CI_OBYWATELSTWO>{data($req/m:Nationality)}</fml:CI_OBYWATELSTWO>
else ()
}
{
if ($req/m:CustBirthDate) then
<fml:DC_DATA_URODZENIA>{data($req/m:CustBirthDate)}</fml:DC_DATA_URODZENIA>
else ()
}
{
if ($req/m:CustBirthPlace) then
<fml:DC_MIEJSCE_URODZENIA>{data($req/m:CustBirthPlace)}</fml:DC_MIEJSCE_URODZENIA>
else ()
}
{
if ($req/m:CivilStatus) then
<fml:DC_STAN_CYWILNY>{data($req/m:CivilStatus)}</fml:DC_STAN_CYWILNY>
else ()
}
{
if ($req/m:Language) then
<fml:DC_KOD_JEZYKA>{data($req/m:Language)}</fml:DC_KOD_JEZYKA>
else ()
}
{
if ($req/m:HighSchoolName) then
<fml:DC_UCZELNIA>{data($req/m:HighSchoolName)}</fml:DC_UCZELNIA>
else ()
}
{
if ($req/m:SchoolType) then
<fml:DC_TYP_SZKOLY>{data($req/m:SchoolType)}</fml:DC_TYP_SZKOLY>
else ()
}
{
if ($req/m:PlanEndSchoolDate) then
<fml:DC_PLANOW_DATA_UKON_SZK>{data($req/m:PlanEndSchoolDate)}</fml:DC_PLANOW_DATA_UKON_SZK>
else ()
}
{
if ($req/m:EduLevel) then
<fml:DC_WYKSZTALCENIE>{data($req/m:EduLevel)}</fml:DC_WYKSZTALCENIE>
else ()
}
{
if ($req/m:EducationProfile) then
<fml:DC_CHARAKTER_WYKSZTALC>{data($req/m:EducationProfile)}</fml:DC_CHARAKTER_WYKSZTALC>
else ()
}
{
if ($req/m:StudyType) then
<fml:DC_SYSTEM_STUDIOW>{data($req/m:StudyType)}</fml:DC_SYSTEM_STUDIOW>
else ()
}
{
if ($req/m:SchoolSpecialization) then
<fml:DC_SPECJALIZACJA>{data($req/m:SchoolSpecialization)}</fml:DC_SPECJALIZACJA>
else ()
}
{
if ($req/m:MilitaryService) then
<fml:DC_SLUZBA_WOJSKOWA>{data($req/m:MilitaryService)}</fml:DC_SLUZBA_WOJSKOWA>
else ()
}
{
if ($req/m:JointPropertyFlag) then
<fml:DC_WSPOLNOTA_MAJATK>{data($req/m:JointPropertyFlag)}</fml:DC_WSPOLNOTA_MAJATK>
else ()
}
{
if ($req/m:HousePeopleCount) then
<fml:DC_LICZBA_OS_WE_WSP_GOSP_D>{data($req/m:HousePeopleCount)}</fml:DC_LICZBA_OS_WE_WSP_GOSP_D>
else ()
}
{
if ($req/m:KeptPeopleCount) then
<fml:DC_LICZBA_OSOB_NA_UTRZ>{data($req/m:KeptPeopleCount)}</fml:DC_LICZBA_OSOB_NA_UTRZ>
else ()
}
{
if ($req/m:HouseOwnerFlag) then
<fml:DC_POSIADA_DOM_MIESZK>{data($req/m:HouseOwnerFlag)}</fml:DC_POSIADA_DOM_MIESZK>
else ()
}
{
if ($req/m:HousePropertyType) then
<fml:CI_TYP_WLASNOSCI>{data($req/m:HousePropertyType)}</fml:CI_TYP_WLASNOSCI>
else ()
}
{
if ($req/m:JobCode) then
<fml:DC_KOD_ZAWODU>{data($req/m:JobCode)}</fml:DC_KOD_ZAWODU>
else ()
}
{
if ($req/m:Profession) then
<fml:DC_KOD_ZAWODU>{data($req/m:Profession)}</fml:DC_KOD_ZAWODU>
else ()
}
{
if ($req/m:WorkPlace) then
<fml:CI_SEKTOR_ZATRUDNIENIA>{data($req/m:WorkPlace)}</fml:CI_SEKTOR_ZATRUDNIENIA>
else ()
}
{
if ($req/m:MonthIncome) then
<fml:DC_MIES_DOCHOD_NETTO_W_PLN>{data($req/m:MonthIncome)}</fml:DC_MIES_DOCHOD_NETTO_W_PLN>
else ()
}
{
if ($req/m:IncomeSrc) then
<fml:CI_ZRODLO_DOCHODU>{data($req/m:IncomeSrc)}</fml:CI_ZRODLO_DOCHODU>
else ()
}
{
if ($req/m:ExtraMonthIncome) then
<fml:CI_DOD_DOCHOD_MIES_NETTO>{data($req/m:ExtraMonthIncome)}</fml:CI_DOD_DOCHOD_MIES_NETTO>
else ()
}
{
if ($req/m:ExtraIncomeSrc) then
<fml:CI_ZRODLO_DODAT_DOCH>{data($req/m:ExtraIncomeSrc)}</fml:CI_ZRODLO_DODAT_DOCH>
else ()
}
{
if ($req/m:ChargeType) then
<fml:CI_RODZAJ_OBCIAZENIA>{data($req/m:ChargeType)}</fml:CI_RODZAJ_OBCIAZENIA>
else ()
}
{
if ($req/m:ChargeTypeOther) then
<fml:CI_RODZAJ_OBC_INNE>{data($req/m:ChargeTypeOther)}</fml:CI_RODZAJ_OBC_INNE>
else ()
}
{
if ($req/m:SumMonthCharge) then
<fml:CI_LACZNA_WYS_OBC>{data($req/m:SumMonthCharge)}</fml:CI_LACZNA_WYS_OBC>
else ()
}
{
if ($req/m:CustDeathDate) then
<fml:DC_DATA_SMIERCI>{data($req/m:CustDeathDate)}</fml:DC_DATA_SMIERCI>
else ()
}
{
if ($req/m:PolicesCount) then
<fml:CI_LICZBA_POLIS>{data($req/m:PolicesCount)}</fml:CI_LICZBA_POLIS>
else ()
}
{
if ($req/m:PolicesSum) then
<fml:CI_SUMA_POLIS_NA_ZYCIE>{data($req/m:PolicesSum)}</fml:CI_SUMA_POLIS_NA_ZYCIE>
else ()
}
{
if ($req/m:LifeInsurranceFlag) then
<fml:DC_UBEZPIECZENIE_NA_ZYCIE>{data($req/m:LifeInsurranceFlag)}</fml:DC_UBEZPIECZENIE_NA_ZYCIE>
else ()
}
{
if ($req/m:NwInsurranceFlag) then
<fml:DC_UBEZP_OD_NASTEPSTW_NW>{data($req/m:NwInsurranceFlag)}</fml:DC_UBEZP_OD_NASTEPSTW_NW>
else ()
}
{
if ($req/m:RetireInsurranceFlag) then
<fml:DC_UBEZPIECZENIE_EMERYTALNE>{data($req/m:RetireInsurranceFlag)}</fml:DC_UBEZPIECZENIE_EMERYTALNE>
else ()
}
{
if ($req/m:WealthInsurranceFlag) then
<fml:DC_UBEZP_MAJATKOWE_MIES>{data($req/m:WealthInsurranceFlag)}</fml:DC_UBEZP_MAJATKOWE_MIES>
else ()
}
{
if ($req/m:OtherInsurranceFlag) then
<fml:DC_UBEZPIECZENIA_INNE>{data($req/m:OtherInsurranceFlag)}</fml:DC_UBEZPIECZENIA_INNE>
else ()
}
{
if ($req/m:RegisterNo) then
<fml:CI_KRS>{data($req/m:RegisterNo)}</fml:CI_KRS>
else ()
}
{
if ($req/m:EkdCode) then
<fml:DC_EKD>{data($req/m:EkdCode)}</fml:DC_EKD>
else ()
}
{
if ($req/m:AgencyCount) then
<fml:CI_LICZBA_PLACOWEK>{data($req/m:AgencyCount)}</fml:CI_LICZBA_PLACOWEK>
else ()
}
{
if ($req/m:EmployedCount) then
<fml:DC_LICZBA_ZATRUDNIONYCH>{data($req/m:EmployedCount)}</fml:DC_LICZBA_ZATRUDNIONYCH>
else ()
}
{
if ($req/m:BusinessEndDate) then
<fml:DC_DATA_BANKRUCTWA>{data($req/m:BusinessEndDate)}</fml:DC_DATA_BANKRUCTWA>
else ()
}
{
if ($req/m:WebPage) then
<fml:CI_STRONA_WWW>{data($req/m:WebPage)}</fml:CI_STRONA_WWW>
else ()
}
{
if ($req/m:BusinessInitDate) then
<fml:CI_DATA_ROZP_DZIAL>{data($req/m:BusinessInitDate)}</fml:CI_DATA_ROZP_DZIAL>
else ()
}
{
if ($req/m:DebtValue) then
<fml:CI_KWOTA_ZADLUZENIA>{data($req/m:DebtValue)}</fml:CI_KWOTA_ZADLUZENIA>
else ()
}
{
if ($req/m:SwiftNo) then
<fml:CI_NUMER_SWIFT>{data($req/m:SwiftNo)}</fml:CI_NUMER_SWIFT>
else ()
}
{
if ($req/m:OrgLawForm) then
<fml:CI_FOP>{data($req/m:OrgLawForm)}</fml:CI_FOP>
else ()
}
{
if ($req/m:RegOrgName) then
<fml:CI_NAZWA_ORG_REJESTR>{data($req/m:RegOrgName)}</fml:CI_NAZWA_ORG_REJESTR>
else ()
}
{
if ($req/m:RegOrgStreet) then
<fml:CI_ULICA_ORG_REJESTR>{data($req/m:RegOrgStreet)}</fml:CI_ULICA_ORG_REJESTR>
else ()
}
{
if ($req/m:RegOrgHouseNo) then
<fml:CI_NR_POSES_LOKALU_ORG_REJ>{data($req/m:RegOrgHouseNo)}</fml:CI_NR_POSES_LOKALU_ORG_REJ>
else ()
}
{
if ($req/m:RegOrgPostCode) then
<fml:CI_KOD_POCZT_ORG_REJESTR>{data($req/m:RegOrgPostCode)}</fml:CI_KOD_POCZT_ORG_REJESTR>
else ()
}
{
if ($req/m:RegOrgCity) then
<fml:CI_MIASTO_ORG_REJESTR>{data($req/m:RegOrgCity)}</fml:CI_MIASTO_ORG_REJESTR>
else ()
}
{
if ($req/m:RegOrgCountry) then
<fml:CI_KRAJ_ORG_REJESTR>{data($req/m:RegOrgCountry)}</fml:CI_KRAJ_ORG_REJESTR>
else ()
}
{
if ($req/m:RegOrgInitDate) then
<fml:CI_DATA_WPISU_DO_REJESTR>{data($req/m:RegOrgInitDate)}</fml:CI_DATA_WPISU_DO_REJESTR>
else ()
}
{
if ($req/m:HeadquaterFlag) then
<fml:CI_STATUS_SIEDZIBY>{data($req/m:HeadquaterFlag)}</fml:CI_STATUS_SIEDZIBY>
else ()
}
{
if ($req/m:Auditor) then
<fml:CI_AUDYTOR>{data($req/m:Auditor)}</fml:CI_AUDYTOR>
else ()
}
{
if ($req/m:OriginalCapital) then
<fml:CI_KAPITAL_ZALOZYCIELSKI>{data($req/m:OriginalCapital)}</fml:CI_KAPITAL_ZALOZYCIELSKI>
else ()
}
{
if ($req/m:SharesValue) then
<fml:CI_WARTOSC_AKCJI>{data($req/m:SharesValue)}</fml:CI_WARTOSC_AKCJI>
else ()
}
{
if ($req/m:ImmovablesValue) then
<fml:CI_WARTOSC_NIERUCHOMOSCI>{data($req/m:ImmovablesValue)}</fml:CI_WARTOSC_NIERUCHOMOSCI>
else ()
}
{
if ($req/m:SurfaceValue) then
<fml:CI_WARTOSC_SRODKOW_MATER>{data($req/m:SurfaceValue)}</fml:CI_WARTOSC_SRODKOW_MATER>
else ()
}
{
if ($req/m:OtherAssets) then
<fml:CI_INNE_AKTYWA>{data($req/m:OtherAssets)}</fml:CI_INNE_AKTYWA>
else ()
}
{
if ($req/m:TaxBorrowing) then
<fml:CI_ZOBOWIAZANIA_PODATKOWE>{data($req/m:TaxBorrowing)}</fml:CI_ZOBOWIAZANIA_PODATKOWE>
else ()
}
{
if ($req/m:OtherBorrowing) then
<fml:CI_INNE_ZOBOWIAZANIA>{data($req/m:OtherBorrowing)}</fml:CI_INNE_ZOBOWIAZANIA>
else ()
}
{
if ($req/m:FinancialUpdateDate) then
<fml:CI_DATA_AKTUAL_DANYCH_FIN>{data($req/m:FinancialUpdateDate)}</fml:CI_DATA_AKTUAL_DANYCH_FIN>
else ()
}
{
if ($req/m:Income) then
<fml:CI_PRZYCHODY_INST>{data($req/m:Income)}</fml:CI_PRZYCHODY_INST>
else ()
}
{
if ($req/m:CustCorrAddressStreet) then
<fml:DC_ULICA_ADRES_ALT>{data($req/m:CustCorrAddressStreet)}</fml:DC_ULICA_ADRES_ALT>
else ()
}
{
if ($req/m:CustCorrAddressHouseFlatNo) then
<fml:DC_NR_POSES_LOKALU_ADRES_ALT>{data($req/m:CustCorrAddressHouseFlatNo)}</fml:DC_NR_POSES_LOKALU_ADRES_ALT>
else ()
}
{
if ($req/m:CustCorrAddressPostCode) then
<fml:DC_KOD_POCZTOWY_ADRES_ALT>{data($req/m:CustCorrAddressPostCode)}</fml:DC_KOD_POCZTOWY_ADRES_ALT>
else ()
}
{
if ($req/m:CustCorrAddressCity) then
<fml:DC_MIASTO_ADRES_ALT>{data($req/m:CustCorrAddressCity)}</fml:DC_MIASTO_ADRES_ALT>
else ()
}
{
if ($req/m:CustCorrAddressCounty) then
<fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{data($req/m:CustCorrAddressCounty)}</fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
else ()
}
{
if ($req/m:CustCorrAddressCountry) then
<fml:CI_KOD_KRAJU_KORESP>{data($req/m:CustCorrAddressCountry)}</fml:CI_KOD_KRAJU_KORESP>
else ()
}
{
if ($req/m:CustCorrValidDateFrom) then
<fml:CI_DATA_OD>{data($req/m:CustCorrValidDateFrom)}</fml:CI_DATA_OD>
else ()
}
{
if ($req/m:CustCorrValidDateTo) then
<fml:CI_DATA_DO>{data($req/m:CustCorrValidDateTo)}</fml:CI_DATA_DO>
else ()
}
{
if ($req/m:CustCorrAddressForFlag) then
<fml:CI_TYP_ADRESU_KORESP>{data($req/m:CustCorrAddressForFlag)}</fml:CI_TYP_ADRESU_KORESP>
else ()
}
{
if ($req/m:EmpAddressStreet) then
<fml:CI_ULICA_PRACODAWCY>{data($req/m:EmpAddressStreet)}</fml:CI_ULICA_PRACODAWCY>
else ()
}
{
if ($req/m:EmpAddressHouseFlatNo) then
<fml:CI_NUMER_POSESJI_PRAC>{data($req/m:EmpAddressHouseFlatNo)}</fml:CI_NUMER_POSESJI_PRAC>
else ()
}
{
if ($req/m:EmpAddressPostCode) then
<fml:CI_KOD_POCZT_PRAC>{data($req/m:EmpAddressPostCode)}</fml:CI_KOD_POCZT_PRAC>
else ()
}
{
if ($req/m:EmpAddressCity) then
<fml:CI_MIASTO_PRACODAWCY>{data($req/m:EmpAddressCity)}</fml:CI_MIASTO_PRACODAWCY>
else ()
}
{
if ($req/m:EmpAddressCounty) then
<fml:CI_WOJEWODZTWO_PRAC>{data($req/m:EmpAddressCounty)}</fml:CI_WOJEWODZTWO_PRAC>
else ()
}
{
if ($req/m:EmpAddressCountry) then
<fml:CI_KOD_KRAJU_PRAC>{data($req/m:EmpAddressCountry)}</fml:CI_KOD_KRAJU_PRAC>
else ()
}
{
if ($req/m:EmpAddressForFlag) then
<fml:CI_TYP_ADRESU_PRAC>{data($req/m:EmpAddressForFlag)}</fml:CI_TYP_ADRESU_PRAC>
else ()
}
{
if ($req/m:EmpPhone) then
<fml:CI_NR_TELEFONU_PRAC>{data($req/m:EmpPhone)}</fml:CI_NR_TELEFONU_PRAC>
else ()
}
{
if ($req/m:EmployeeStatus) then
<fml:DC_STATUS_PRACOWNIKA>{data($req/m:EmployeeStatus)}</fml:DC_STATUS_PRACOWNIKA>
else ()
}
{
if ($req/m:JobStartDate) then
<fml:DC_DATA_PODJECIA_PRACY>{data($req/m:JobStartDate)}</fml:DC_DATA_PODJECIA_PRACY>
else ()
}
{
if ($req/m:TempJob) then
<fml:DC_WYKONYWANA_PRACA>{data($req/m:TempJob)}</fml:DC_WYKONYWANA_PRACA>
else ()
}
{
if ($req/m:ResidenceCountry) then
<fml:CI_KRAJ_ZAMIESZKANIA>{data($req/m:ResidenceCountry)}</fml:CI_KRAJ_ZAMIESZKANIA>
else ()
}
{
if ($req/m:BusinessCountry) then
<fml:CI_KRAJ_PROWADZENIA_DZIAL>{data($req/m:BusinessCountry)}</fml:CI_KRAJ_PROWADZENIA_DZIAL>
else ()
}
{
if ($req/m:ComplexStructFlag) then
<fml:CI_STRUKTURA_ZLOZONA>{data($req/m:ComplexStructFlag)}</fml:CI_STRUKTURA_ZLOZONA>
else ()
}



		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMModProspFolderRequest($body/m:CRMModifyProspFolderRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>