<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetQuestionnaireDataResponse($fml as element(fml:FML32))
	as element(m:CRMGetQuestionnaireDataResponse) {
            <m:CRMGetQuestionnaireDataResponse>
                        {
				let $CI_NUMER_PACZKI := $fml/fml:CI_NUMER_PACZKI return
					if($CI_NUMER_PACZKI)
						then <m:NumerPaczki>{ data($CI_NUMER_PACZKI) }</m:NumerPaczki>
						else ()
			}
			{
                                let $CI_ID_ANKIETY := $fml/fml:CI_ID_ANKIETY
                                let $CI_SKP_AUTORA := $fml/fml:CI_SKP_AUTORA
                                let $CI_DATA_ANKIETY := $fml/fml:CI_DATA_ANKIETY
                                let $CI_ANKIETA_XML := $fml/fml:CI_ANKIETA_XML
                                let $CI_LFR := $fml/fml:CI_LFR

				for $it at $p in $fml/fml:CI_ID_ANKIETY
                                      return
                                            <m:Questionnaire>
                                                          {
                                                             if ($CI_ID_ANKIETY[$p]) then
                                                                     <m:QuestId>{ data($CI_ID_ANKIETY[$p]) }</m:QuestId>
                                                             else ()
                                                          }
                                                          {
                                                             if ($CI_SKP_AUTORA[$p]) then
                                                                     <m:UsrSkpNo>{ data($CI_SKP_AUTORA[$p]) }</m:UsrSkpNo>
                                                             else ()
                                                          }
                                                          {
                                                             if ($CI_DATA_ANKIETY[$p]) then
                                                                     <m:QuestionnaireDate>{ data($CI_DATA_ANKIETY[$p]) }</m:QuestionnaireDate>
                                                             else ()
                                                          }
                                                          {
                                                             if ($CI_LFR[$p]) then
                                                                     <m:LfrFlag>{ data($CI_LFR[$p]) }</m:LfrFlag>
                                                             else ()
                                                          }
                                                          {
                                                             if ($CI_ANKIETA_XML[$p]) then
                                                                     <m:XmlQuestionnaire>{ data($fml/fml:CI_ANKIETA_XML[$p]) }</m:XmlQuestionnaire>
                                                             else ()
                                                          }
                                            </m:Questionnaire>   
                        }

		</m:CRMGetQuestionnaireDataResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetQuestionnaireDataResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>