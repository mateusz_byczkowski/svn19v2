<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-06-28</con:description>
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetProspFolderRequest($req as element(m:CRMGetProspFolderRequest))
	as element(fml:FML32) {
		<fml:FML32>

                   {
                    if($req/m:CustId)
			then <fml:CI_NUMER_KLIENTA>{data($req/m:CustId)}</fml:CI_NUMER_KLIENTA>
                        else ()
                    }
                   {
                    if($req/m:CompanyId)
			then <fml:CI_ID_SPOLKI>{data($req/m:CompanyId)}</fml:CI_ID_SPOLKI>
                        else ()
                    }
                   {
                    if($req/m:EmpSkpNo)
			then <fml:CI_SKP_PRACOWNIKA>{data($req/m:EmpSkpNo)}</fml:CI_SKP_PRACOWNIKA>
                        else ()
                    }
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapGetProspFolderRequest($body/m:CRMGetProspFolderRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>