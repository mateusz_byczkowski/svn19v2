<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetFlagHistoryRequest($req as element(m:CRMGetFlagHistoryRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:IcbsNo)
					then <fml:CI_NUMER_ODDZIALU>{ data($req/m:IcbsNo) }</fml:CI_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:CompanyId)
					then <fml:CI_ID_SPOLKI>{ data($req/m:CompanyId) }</fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:AccDateFrom)
					then <fml:CI_DATA_OD>{ data($req/m:AccDateFrom) }</fml:CI_DATA_OD>
					else ()
			}
			{
				if($req/m:AccDateTo)
					then <fml:CI_DATA_DO>{ data($req/m:AccDateTo) }</fml:CI_DATA_DO>
					else ()
			}
			{
				if($req/m:CustCif)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:CustCif) }</fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:SetUserId)
					then <fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:SetUserId) }</fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:AccUserId)
					then <fml:CI_SKP_PRACOWNIKA>{ data($req/m:AccUserId) }</fml:CI_SKP_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:FlagType)
					then <fml:CI_TYP>{ data($req/m:FlagType) }</fml:CI_TYP>
					else ()
			}
			{
				if($req/m:SortBy)
					then <fml:CI_SORTOWANIE>{ data($req/m:SortBy) }</fml:CI_SORTOWANIE>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetFlagHistoryRequest($body/m:CRMGetFlagHistoryRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>