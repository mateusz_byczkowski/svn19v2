<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISGetCifResponse($fml as element(fml:FML32))
	as element(m:CISGetCifResponse) {
		<m:CISGetCifResponse>
			{
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA return
					if($DC_NUMER_KLIENTA)
						then <m:NumerKlienta>{ data($DC_NUMER_KLIENTA) }</m:NumerKlienta>
						else ()
			}
		</m:CISGetCifResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCISGetCifResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>