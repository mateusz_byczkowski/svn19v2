<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveQuestionareSrc_req/";
declare namespace dcl = "urn:dcl:services.alsb.datamodel";

declare function xf:saveQuestionareSrc_req ($entity as element(dcl:entities.productstree.QuestSrc)) as element(FML32) {
<FML32>
     <CI_ID_ANKIETY>{ data( $entity/dcl:questId ) }</CI_ID_ANKIETY>
     <CI_ID_SPOLKI>{ data( $entity/dcl:companyId) }</CI_ID_SPOLKI>
     <CI_DATA_OD>{ data( $entity/dcl:validFrom ) }</CI_DATA_OD>
     <CI_DATA_DO>{ data( $entity/dcl:validTo ) }</CI_DATA_DO>
     <CI_SKP_AUTORA>{ data( $entity/dcl:usrSkpNo ) }</CI_SKP_AUTORA>
     <CI_ANKIETA_XML>{ data( $entity/dcl:xmlQuestionare) }</CI_ANKIETA_XML>
</FML32>
};
declare variable $entity as element(dcl:entities.productstree.QuestSrc) external;
xf:saveQuestionareSrc_req ($entity)]]></con:xquery>
</con:xqueryEntry>