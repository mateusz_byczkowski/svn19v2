<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMApproveRejectLFRFlagRequest($req as element(m:appRejectLFRFlagRequest))
	as element(fml:FML32) {
		<fml:FML32>
			
                        {
				if($req/m:SupervisorSKPNo)
					then <fml:CI_SKP_PRACOWNIKA>{ data($req/m:SupervisorSKPNo) }</fml:CI_SKP_PRACOWNIKA>
					else ()
			}
                        {
				if($req/m:CustCIF)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:CustCIF) }</fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:UsrSKPNo)
					then <fml:CI_SKP_PRACOWNIKA_REJ>{ data($req/m:UsrSKPNo) }</fml:CI_SKP_PRACOWNIKA_REJ>
					else ()
			}
			{
				if($req/m:CompanyId)
					then <fml:CI_ID_SPOLKI>{ data($req/m:CompanyId) }</fml:CI_ID_SPOLKI>
					else ()
			}			
			{
				if($req/m:DecisionFlag)
					then <fml:CI_AKCEPTACJA_LFR>{ data($req/m:DecisionFlag) }</fml:CI_AKCEPTACJA_LFR>
					else ()
			}

			{
				if($req/m:LfrRejectDesc)
                                        then <fml:CI_POWOD_ODRZUCENIA_LFR>{data($req/m:LfrRejectDesc)}</fml:CI_POWOD_ODRZUCENIA_LFR>
                                        else () 
			}		
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMApproveRejectLFRFlagRequest($body/m:appRejectLFRFlagRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>