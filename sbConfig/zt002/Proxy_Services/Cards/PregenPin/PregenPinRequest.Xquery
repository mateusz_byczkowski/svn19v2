<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/PregenPinmessages/";
declare namespace xf = "http://bzwbk.com/services/PregenPinmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPregenPinRequest($req as element(m:PregenPinRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:Ibanaccno)
					then <fml:CT_IBANACCNO>{ data($req/m:Ibanaccno) }</fml:CT_IBANACCNO>
					else ()
			}
			{
				if($req/m:PanDisp)
					then <fml:CT_PAN_DISP>{ data($req/m:PanDisp) }</fml:CT_PAN_DISP>
					else ()
			}
			{
				if($req/m:Vpan)
					then <fml:CT_VPAN>{ data($req/m:Vpan) }</fml:CT_VPAN>
					else ()
			}
			{
				if($req/m:PinmailerRef)
					then <fml:CT_PINMAILER_REF>{ data($req/m:PinmailerRef) }</fml:CT_PINMAILER_REF>
					else ()
			}
			{
				if($req/m:Custid)
					then <fml:CT_CUSTID>{ data($req/m:Custid) }</fml:CT_CUSTID>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapPregenPinRequest($body/m:PregenPinRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>