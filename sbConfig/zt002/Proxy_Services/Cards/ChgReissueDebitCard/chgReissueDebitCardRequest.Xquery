<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};


declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if($parm = "false")
       then $falseval
    else $parm
};


declare function xf:getFields($parm as element(ns4:invoke), $msghead as element(ns4:msgHeader), $tranhead as element(ns4:transHeader))
	as element(fml:FML32) {

let $msgId:= $msghead/ns4:msgId
let $companyId:= $msghead/ns4:companyId
let $userId := $msghead/ns4:userId
let $appId:= $msghead/ns4:appId
let $unitId := $msghead/ns4:unitId
let $timestamp:= $msghead/ns4:timestamp

let $transId:=$tranhead/ns4:transId

return
  <fml:FML32>
     <DC_TRN_ID?>{data($transId)}</DC_TRN_ID>
     <DC_UZYTKOWNIK?>{concat("SKP:", data($userId))}</DC_UZYTKOWNIK>
     <DC_ODDZIAL?>{chkUnitId(data($unitId))}</DC_ODDZIAL>
     <DC_TERMINAL_ID?>ALSB</DC_TERMINAL_ID>
     <DC_WIRT_NR_KARTY?>{data($parm/ns4:debitCard/ns5:DebitCard/ns5:virtualCardNbr)}</DC_WIRT_NR_KARTY>
(:     <DC_REZ_KARTA?>{data($parm/ns4:debitCard/ns5:DebitCard/ns5:resignCause/ns1:CrdResignCause/ns1:crdResignCause)}</DC_REZ_KARTA>:)
     {
     if (string-length(data($parm/ns4:debitCard/ns5:DebitCard/ns5:resignCause/ns1:CrdResignCause/ns1:crdResignCause))>0) then
     <DC_REZ_KARTA?>{data($parm/ns4:debitCard/ns5:DebitCard/ns5:resignCause/ns1:CrdResignCause/ns1:crdResignCause)}</DC_REZ_KARTA>
     else <DC_REZ_KARTA>0</DC_REZ_KARTA>
     }
     {
      if (string-length(data($parm/ns4:debitCard/ns5:DebitCard/ns5:nbrOfCardToGen))>0) then
     <DC_KARTA_PRODUKCJA?>{boolean2SourceValue(data($parm/ns4:debitCard/ns5:DebitCard/ns5:nbrOfCardToGen),"1","0")}</DC_KARTA_PRODUKCJA>
     else ()
     }
     {
     if (string-length(data($parm/ns4:debitCard/ns5:DebitCard/ns5:cardMailerNextCycle))>0) then
     <DC_AUTOMATYCZNE_WZNOWIENIE?>{boolean2SourceValue(data($parm/ns4:debitCard/ns5:DebitCard/ns5:cardMailerNextCycle),"1","0")}</DC_AUTOMATYCZNE_WZNOWIENIE>
     else ()
     }
     <DC_DATA_WZNOWIENIA?>{data($parm/ns4:debitCard/ns5:DebitCard/ns5:nextReissueDate)}</DC_DATA_WZNOWIENIA>
     <DC_KOD?>C</DC_KOD>
  </fml:FML32>
};

<soap:Body>
     { xf:getFields($body/ns4:invoke, $header/ns4:header/ns4:msgHeader, $header/ns4:header/ns4:transHeader) }
</soap:Body>]]></con:xquery>
</con:xqueryEntry>