<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace ns4="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:getFields($parm as element(ns3:invoke), $msghead as element(ns3:msgHeader), $tranhead as element(ns3:transHeader)) as element(fml:FML32)
{
  let $msgId:= $msghead/ns3:msgId
  let $companyId:= $msghead/ns3:companyId
  let $userId := $msghead/ns3:userId
  let $appId:= $msghead/ns3:appId
  let $unitId := $msghead/ns3:unitId
  let $timestamp:= $msghead/ns3:timestamp

  let $transId:=$tranhead/ns3:transId

  return
  <fml:FML32>
     <DC_TRN_ID?>{data($transId)}</DC_TRN_ID>
     <DC_UZYTKOWNIK?>{data($userId)}</DC_UZYTKOWNIK>
     <DC_ODDZIAL?>{chkUnitId(data($unitId))}</DC_ODDZIAL>
     <DC_TERMINAL_ID?>ALSB</DC_TERMINAL_ID>
     <DC_WIRT_NR_KARTY?>{data($parm/ns3:debitCard/ns4:DebitCard/ns4:virtualCardNbr)}</DC_WIRT_NR_KARTY>
   </fml:FML32>

};

<soap:Body>
     { xf:getFields($body/ns3:invoke, $header/ns3:header/ns3:msgHeader, $header/ns3:header/ns3:transHeader)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>