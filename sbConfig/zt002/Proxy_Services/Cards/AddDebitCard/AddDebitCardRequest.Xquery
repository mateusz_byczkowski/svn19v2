<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:mapbICBSAddDebitCardRequest($req as element(m:bICBSAddDebitCardRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:TrnId)
					then <fml:DC_TRN_ID>{ data($req/m:TrnId) }</fml:DC_TRN_ID>
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then <fml:DC_UZYTKOWNIK>{ data($req/m:Uzytkownik) }</fml:DC_UZYTKOWNIK>
					else ()
			}
			{
				if($req/m:Oddzial)
					then <fml:DC_ODDZIAL>{ chkUnitId(data($req/m:Oddzial)) }</fml:DC_ODDZIAL>
					else ()
			}
			{
				if($req/m:NrKartyBin)
					then <fml:DC_NR_KARTY_BIN>{ data($req/m:NrKartyBin) }</fml:DC_NR_KARTY_BIN>
					else ()
			}
			{
				if($req/m:TypKarty)
					then <fml:DC_TYP_KARTY>{ data($req/m:TypKarty) }</fml:DC_TYP_KARTY>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }</fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:Rachunek)
					then <fml:DC_RACHUNEK>{ data($req/m:Rachunek) }</fml:DC_RACHUNEK>
					else ()
			}
			{
				if($req/m:WytloczoneNaKarcieI)
					then <fml:DC_WYTLOCZONE_NA_KARCIE_I>{ data($req/m:WytloczoneNaKarcieI) }</fml:DC_WYTLOCZONE_NA_KARCIE_I>
					else ()
			}
			{
				if($req/m:WytloczoneNaKarcieIi)
					then <fml:DC_WYTLOCZONE_NA_KARCIE_II>{ data($req/m:WytloczoneNaKarcieIi) }</fml:DC_WYTLOCZONE_NA_KARCIE_II>
					else ()
			}
			{
				if($req/m:IdentyfikatorPin)
					then <fml:DC_IDENTYFIKATOR_PIN>{ data($req/m:IdentyfikatorPin) }</fml:DC_IDENTYFIKATOR_PIN>
					else ()
			}
			{
				if($req/m:ZalozycPolise)
					then <fml:DC_ZALOZYC_POLISE>{ data($req/m:ZalozycPolise) }</fml:DC_ZALOZYC_POLISE>
					else ()
			}
			{
				if($req/m:LimitKarty)
					then <fml:DC_LIMIT_KARTY>{ data($req/m:LimitKarty) }</fml:DC_LIMIT_KARTY>
					else ()
			}
			{
				if($req/m:StatusUmowy)
					then <fml:DC_STATUS_UMOWY>{ data($req/m:StatusUmowy) }</fml:DC_STATUS_UMOWY>
					else ()
			}
			{
				if($req/m:TerminalId)
					then <fml:DC_TERMINAL_ID>{ data($req/m:TerminalId) }</fml:DC_TERMINAL_ID>
					else ()
			}
			{
				if($req/m:UmorzycOplate)
					then <fml:DC_UMORZYC_OPLATE>{ data($req/m:UmorzycOplate) }</fml:DC_UMORZYC_OPLATE>
					else ()
			}
			{
				if($req/m:CyklZestawien)
					then <fml:DC_CYKL_ZESTAWIEN>{ data($req/m:CyklZestawien) }</fml:DC_CYKL_ZESTAWIEN>
					else ()
			}
			{
				if($req/m:Ekspres)
					then <fml:DC_EKSPRES>{ data($req/m:Ekspres) }</fml:DC_EKSPRES>
					else ()
			}
			{
				if($req/m:TypAdresu)
					then <fml:DC_TYP_ADRESU>{ data($req/m:TypAdresu) }</fml:DC_TYP_ADRESU>
					else ()
			}
			{
				if($req/m:Promocja)
					then <fml:DC_PROMOCJA>{ data($req/m:Promocja) }</fml:DC_PROMOCJA>
					else ()
			}
			{
				if($req/m:ZestZbiorcze)
					then <fml:DC_ZEST_ZBIORCZE>{ data($req/m:ZestZbiorcze) }</fml:DC_ZEST_ZBIORCZE>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapbICBSAddDebitCardRequest($body/m:bICBSAddDebitCardRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>