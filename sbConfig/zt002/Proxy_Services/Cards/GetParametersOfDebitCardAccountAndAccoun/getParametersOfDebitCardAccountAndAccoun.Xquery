<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:productstree.entities.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns6="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:be.services.dcl";
declare namespace ns9="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function sourceValue2Boolean ($parm as xs:string?,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns8:invokeResponse>
  <ns8:response>
    <ns0:ResponseMessage>
     {
       if (string-length(data($parm/NF_ERROR_DESCRIPTION))>0) then
          <ns0:result?>false</ns0:result>
       else
          <ns0:result?>true</ns0:result>
     }
    </ns0:ResponseMessage>
  </ns8:response>
  <ns8:cardOut>
    <ns9:Card>
      <ns9:bin?>{data($parm/NF_CARD_BIN)}</ns9:bin>
      <ns9:cardType?>{data($parm/NF_CARD_CARDTYPE)}</ns9:cardType>
      <ns9:embossName1?>{data($parm/NF_CARD_EMBOSSNAME1)}</ns9:embossName1>
      <ns9:embossName2?>{data($parm/NF_CARD_EMBOSSNAME2)}</ns9:embossName2>
      {
       if (data($parm/PT_ID_DEFINITION) = 0) then
             <ns9:cardName?>{data($parm/PT_POLISH_GROUP_NAME)}</ns9:cardName>
        else
             <ns9:cardName?>{data($parm/PT_POLISH_NAME)}</ns9:cardName>
      }
      <ns9:debitCard>
        <ns9:DebitCard>
          <ns9:pinMailer?>{data($parm/NF_DEBITC_PINMAILER)}</ns9:pinMailer>
          <ns9:nbrOfCardToGen?>{sourceValue2Boolean (data($parm/NF_DEBITC_NBROFCARDTOGEN),"1")}</ns9:nbrOfCardToGen>
          <ns9:cardMailerNextCycle?>{sourceValue2Boolean (data($parm/NF_DEBITC_CARDMAILERNEXTCY),"1")}</ns9:cardMailerNextCycle>          
          <ns9:processingOptionFlag3?>{sourceValue2Boolean (data($parm/NF_DEBITC_PROCESSINGOPTION),"1")}</ns9:processingOptionFlag3>
          { insertDate(data($parm/NF_DEBITC_EXPIRATIONDATE),"yyyy-MM-dd","ns9:expirationDate")}          
          <ns9:contract?>{sourceValue2Boolean (data($parm/NF_DEBITC_CONTRACT),"1")}</ns9:contract>
          { insertDate(data($parm/NF_DEBITC_NEXTREISSUEDATE),"yyyy-MM-dd","ns9:nextReissueDate")}
          { insertDate(data($parm/NF_DEBITC_NEXTCARDFEEDATE),"yyyy-MM-dd","ns9:nextCardFeeDate")}
          <ns9:cashLimit?>{data($parm/NF_DEBITC_CASHLIMIT)}</ns9:cashLimit>
          <ns9:express?>{sourceValue2Boolean (data($parm/NF_DEBITC_EXPRESS),"1")}</ns9:express>
          <ns9:policy?>{sourceValue2Boolean (data($parm/NF_DEBITC_POLICY),"1")}</ns9:policy>
          <ns9:cardNbr?>{data($parm/NF_DEBITC_CARDNBR)}</ns9:cardNbr>
          { insertDate(data($parm/NF_DEBITC_CARDRECORDOPENDA),"yyyy-MM-dd","ns9:cardRecordOpenDate")}
          {
           if (data($parm/NF_DEBITC_POLICYNBR) != "0") then
             <ns9:policyNbr?>{data($parm/NF_DEBITC_POLICYNBR)}</ns9:policyNbr>
           else()
          }
          <ns9:cardFee?>{data($parm/NF_DEBITC_CARDFEE)}</ns9:cardFee>
          <ns9:individualTranList?>{sourceValue2Boolean (data($parm/NF_DEBITC_INDIVIDUALTRANLI),"1")}</ns9:individualTranList>
          <ns9:collectiveTranList?>{sourceValue2Boolean (data($parm/NF_DEBITC_COLLECTIVETRANLI),"1")}</ns9:collectiveTranList>
          { insertDate(data($parm/NF_DEBITC_POLICYEXPIRDATE),"yyyy-MM-dd","ns9:policyExpirDate")}         
          {
           if (string-length(data($parm/NF_DEBCLI_LPNUMBER))>0) then
           <ns9:payback?>true</ns9:payback>
           else
           <ns9:payback?>false</ns9:payback>
           }                   
          <ns9:tranAccount>
            <ns2:TranAccount>
              <ns2:account>
                <ns2:Account>
                  <ns2:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER)}</ns2:accountNumber>
                 {
                 if (string-length(data($parm/NF_ACCOUS_DESCRIPTION))>0) then
                   <ns2:accountDescription?>{data($parm/NF_ACCOUS_DESCRIPTION)}</ns2:accountDescription>
                 else
                   <ns2:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI)}</ns2:accountDescription>
                 }
                  <ns2:accountType>
                    <ns5:AccountType>
                      <ns5:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE)}</ns5:accountType>
                    </ns5:AccountType>
                  </ns2:accountType>
                  <ns2:currency>
                    <ns5:CurrencyCode>
                      <ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE2)}</ns5:currencyCode>
                    </ns5:CurrencyCode>
                  </ns2:currency>
                </ns2:Account>
              </ns2:account>
            </ns2:TranAccount>
          </ns9:tranAccount>
          <ns9:resignCause>
            <ns1:CrdResignCause>
              <ns1:crdResignCause?>{data($parm/NF_CRDRC_CRDRESIGNCAUSE)}</ns1:crdResignCause>
            </ns1:CrdResignCause>
          </ns9:resignCause>
          <ns9:addressFlag>
            <ns1:CrdAddressFlag>
              <ns1:crdAddressFlag?>{data($parm/NF_CRDAF_CRDADDRESSFLAG)}</ns1:crdAddressFlag>
            </ns1:CrdAddressFlag>
          </ns9:addressFlag>
          <ns9:cycleLimit>
            <ns1:CrdCycleLimit>
              <ns1:crdCycleLimit?>{data($parm/NF_CRDCL_CRDCYCLELIMIT)}</ns1:crdCycleLimit>
            </ns1:CrdCycleLimit>
          </ns9:cycleLimit>
          <ns9:weiverPIN>
            <ns1:CrdWeiverPIN>
              <ns1:crdWeiverPIN?>{data($parm/NF_CRWPIN_CRDWEIVERPIN)}</ns1:crdWeiverPIN>
            </ns1:CrdWeiverPIN>
          </ns9:weiverPIN>
          <ns9:cardAddressBranchNumber>
            <ns5:BranchCode>
              <ns5:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE)}</ns5:branchCode>
            </ns5:BranchCode>
          </ns9:cardAddressBranchNumber>
          <ns9:cardStatus>
            <ns1:CrdStatus>
              <ns1:crdStatus?>{data($parm/NF_CRDS_CRDSTATUS)}</ns1:crdStatus>
            </ns1:CrdStatus>
          </ns9:cardStatus>
        </ns9:DebitCard>
      </ns9:debitCard>
      <ns9:customer>
        <ns4:Customer>
          <ns4:customerNumber?>{data($parm/NF_CUSTOM_CUSTOMERNUMBER)}</ns4:customerNumber>
          <ns4:customerPersonal>
            <ns4:CustomerPersonal>
              <ns4:motherMaidenName?>{data($parm/NF_CUSTOP_MOTHERMAIDENNAME)}</ns4:motherMaidenName>
              <ns4:lastName?>{data($parm/NF_CUSTOP_LASTNAME[2])}</ns4:lastName>
              <ns4:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME[2])}</ns4:firstName>
            </ns4:CustomerPersonal>
          </ns4:customerPersonal>
        </ns4:Customer>
      </ns9:customer>
      <ns9:productDefinition>
        <ns3:ProductDefinition>
          <ns3:idProductDefinition?>{data($parm/NF_PRODUD_IDPRODUCTDEFINIT)}</ns3:idProductDefinition>
          <ns3:productGroup>
            <ns3:ProductGroup>
              <ns3:idProductGroup?>{data($parm/NF_PRODUG_IDPRODUCTGROUP)}</ns3:idProductGroup>
            </ns3:ProductGroup>
          </ns3:productGroup>
        </ns3:ProductDefinition>
      </ns9:productDefinition>
      <ns9:cardLpInfo>
        <ns9:CardLpInfo>
          <ns9:pb1?>{data($parm/NF_DEBCLI_CUSTOMERLPNUMBER)}</ns9:pb1>
          <ns9:pb2?>{data($parm/NF_DEBCLI_OTHERCUSTOMERLPN)}</ns9:pb2>
          <ns9:cardLpStatus>
            <ns1:CardLpStatus>
              <ns1:cardLpStatus?>{sourceValue2Boolean(data($parm/NF_CRDCLS_CRDDEBITCARDLPST),"1")}</ns1:cardLpStatus>
            </ns1:CardLpStatus>
          </ns9:cardLpStatus>
        </ns9:CardLpInfo>
      </ns9:cardLpInfo>
    </ns9:Card>
  </ns8:cardOut>
  <ns8:accountOwner>
    <ns4:Customer>
      <ns4:companyName?>{data($parm/NF_CUSTOM_COMPANYNAME)}</ns4:companyName>
      <ns4:customerNumber?>{data($parm/NF_CUSTOM_CUSTOMERNUMBER[2])}</ns4:customerNumber>
      <ns4:customerPersonal>
        <ns4:CustomerPersonal>
          <ns4:lastName?>{data($parm/NF_CUSTOP_LASTNAME[1])}</ns4:lastName>
          <ns4:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME[1])}</ns4:firstName>
        </ns4:CustomerPersonal>
      </ns4:customerPersonal>
      <ns4:customerType>
        <ns5:CustomerType>
          <ns5:customerType?>{data($parm/NF_CUSTOT_CUSTOMERTYPE)}</ns5:customerType>
        </ns5:CustomerType>
      </ns4:customerType>
    </ns4:Customer>
  </ns8:accountOwner>
</ns8:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>