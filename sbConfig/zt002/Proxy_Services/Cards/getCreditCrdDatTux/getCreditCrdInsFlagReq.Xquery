<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-01-07</con:description>
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace prim= "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;


<soap-env:Body xmlns:prim="http://bzwbk.com/services/prime/">
      <prim:PRIMEGetCustProductDetail>
             <prim:DC_NR_RACHUNKU>{fn:concat("U",$body/FML32/B_NR_KARTY,'|BPK')}</prim:DC_NR_RACHUNKU>
             <prim:CI_OPCJA>0</prim:CI_OPCJA>
      </prim:PRIMEGetCustProductDetail>
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>