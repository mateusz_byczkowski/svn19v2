<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-01-07</con:description>
    <con:xquery><![CDATA[declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace WBKfault="http://bzwbk.com/services/faults/";

declare variable $body external;



<soap-env:Body>
    <FML32>
    {
        let $urcode:=data($body/soap-env:Fault/detail/WBKfault:ServiceException/ErrorCode)
        return
        if ($urcode = "103") then 
            <B_FLAGA_UBEZP>0</B_FLAGA_UBEZP>
        else
            <B_URCODE>{$urcode}</B_URCODE>
    }
    </FML32>
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>