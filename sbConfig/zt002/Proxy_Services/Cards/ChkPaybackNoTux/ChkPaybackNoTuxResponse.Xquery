<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn1 = "urn:filtersandmessages.entities.be.dcl";
declare namespace dcl = "urn:be.services.dcl";

declare function xf:map_ChkPaybackNoTuxResponse($req as element(dcl:invokeResponse))
	as element(fml:FML32) {
	<fml:FML32>
			{
				<fml:CI_STATUS?>{ string(data($req/dcl:responseMessage/urn1:ResponseMessage/urn1:result)) }</fml:CI_STATUS>
			}
	</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_ChkPaybackNoTuxResponse($body/dcl:invokeResponse) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>