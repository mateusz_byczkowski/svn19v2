<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-07-22</con:description>
    <con:xquery>declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace prim = "http://bzwbk.com/services/prime/";

declare variable $body as element(soapenv:Body) external;


declare function xf:map_GetPayBackDataRequest($parm as element(prim:GetPaybackData))
	as element()* 
 {

&lt;prim:GetPaybackNumber>
	{$parm/prim:in}
&lt;/prim:GetPaybackNumber>	

};
	

&lt;soapenv:Body>
 {xf:map_GetPayBackDataRequest($body/prim:GetPaybackData) }
&lt;/soapenv:Body></con:xquery>
</con:xqueryEntry>