<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:dictionaries.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

   

declare function chkUnitId($unitId as xs:string) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:getFields($parm as element(ns4:invoke), $msghead as element(ns4:msgHeader), $tranhead as element(ns4:transHeader))
	as element(fml:FML32) {

let $msgId:= $msghead/ns4:msgId
let $companyId:= $msghead/ns4:companyId
let $userId := $msghead/ns4:userId
let $appId:= $msghead/ns4:appId
let $unitId := $msghead/ns4:unitId
let $timestamp:= $msghead/ns4:timestamp

let $transId:=$tranhead/ns4:transId

return
  <fml:FML32>
     <DC_TRN_ID?>{data($transId)}</DC_TRN_ID>
     <DC_UZYTKOWNIK?>{concat("SKP:", data($userId))}</DC_UZYTKOWNIK>
     <DC_ODDZIAL?>{chkUnitId(data($unitId))}</DC_ODDZIAL>
     <DC_TERMINAL_ID?>ALSB</DC_TERMINAL_ID>
     <DC_WIRT_NR_KARTY?>{data($parm/ns4:card/ns5:Card/ns5:debitCard/ns5:DebitCard/ns5:virtualCardNbr)}</DC_WIRT_NR_KARTY>
     <DC_WYTLOCZONE_NA_KARCIE_I?>{data($parm/ns4:card/ns5:Card/ns5:embossName1)}</DC_WYTLOCZONE_NA_KARCIE_I>
     <DC_WYTLOCZONE_NA_KARCIE_II?>{data($parm/ns4:card/ns5:Card/ns5:embossName2)}</DC_WYTLOCZONE_NA_KARCIE_II>
     <DC_ODDZIAL?>{data($parm/ns4:card/ns5:Card/ns5:debitCard/ns5:DebitCard/ns5:cardDuplicateBranchNumber/ns1:BranchCode/ns1:branchCode)}</DC_ODDZIAL>
     <DC_KOD?>F</DC_KOD>
  </fml:FML32>
};

<soap:Body>
     { xf:getFields($body/ns4:invoke, $header/ns4:header/ns4:msgHeader, $header/ns4:header/ns4:transHeader) }
</soap:Body>]]></con:xquery>
</con:xqueryEntry>