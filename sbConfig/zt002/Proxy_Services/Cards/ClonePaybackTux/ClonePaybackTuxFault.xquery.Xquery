<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-07-22</con:description>
    <con:xquery><![CDATA[declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace fault = "http://bzwbk.com/services/faults/";

declare function xf:mapFault($req as element(soap:Fault))
	as element(fml:FML32) {

		<fml:FML32>
		<fml:CI_KOD_BLEDU>{ data($req/detail/fault:WbkFault/fault:ErrorCode2) }</fml:CI_KOD_BLEDU>
		<fml:DC_OPIS_BLEDU> { data($req/detail/fault:WbkFault/fault:ErrorDescription) } </fml:DC_OPIS_BLEDU>
	</fml:FML32>

};

declare variable $body as element(soap:Body) external;
<soap:Body>
{ xf:mapFault($body/soap:Fault) }
</soap:Body>]]></con:xquery>
</con:xqueryEntry>