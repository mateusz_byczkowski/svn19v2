<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-07-08</con:description>
    <con:xquery><![CDATA[declare namespace con="http://www.bea.com/wli/sb/context";
declare namespace con1="http://www.bea.com/wli/sb/stages/transform/config";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf="http://bzwbk.com/services/functions";

declare variable $fault external;

declare function xf:getErrorCode ($fau as element(soap-env:Fault)) as xs:integer
{
     if (boolean($fault/con:details/con1:ReceivedFaultDetail/con1:detail/com.kpp.services.faults.PPValidationFault)) then  102
     else  99
};

<soap-env:Body>
    <FML32>
            <B_URCODE>{xf:getErrorCode($fault)}</B_URCODE>
    </FML32>
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>