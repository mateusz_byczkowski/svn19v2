<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "http://bzwbk.com/services/cortex/";
                                     

declare function xf:map_getPanPoolRequest($req as element(urn:GetPanPool))
	as element(fml:FML32) {  
                 <fml:FML32>
			{
				<fml:CT_POOL_SIZE?>{data($req/urn:PoolSize)}</fml:CT_POOL_SIZE>,
				<fml:CT_PRODUCT_CODE?>{data($req/urn:ProductCode)}</fml:CT_PRODUCT_CODE>
				
				
			}
		</fml:FML32>

};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_getPanPoolRequest($body/urn:GetPanPool) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>