<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/GetCustFee/messages/";
declare namespace xf = "http://bzwbk.com/services/GetCustFee/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCustFeeRequest($req as element(m:GetCustFeeRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:CifNumber)
					then <fml:E_CIF_NUMBER>{ data($req/m:CifNumber) }</fml:E_CIF_NUMBER>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapGetCustFeeRequest($body/m:GetCustFeeRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>