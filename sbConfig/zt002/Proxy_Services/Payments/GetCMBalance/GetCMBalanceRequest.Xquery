<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-05-13</con:description>
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/GetCMBalance/messages/";
declare namespace xf = "http://bzwbk.com/services/GetCMBalance/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCMBalanceRequest($req as element(m:GetCMBalanceRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:AccGrpId)
					then <fml:S_ACC_GRP_ID>{ data($req/m:AccGrpId) }</fml:S_ACC_GRP_ID>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapGetCMBalanceRequest($body/m:GetCMBalanceRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>