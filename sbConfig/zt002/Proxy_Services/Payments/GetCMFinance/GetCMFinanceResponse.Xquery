<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-05-13</con:description>
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/GetCMFinance/messages/";
declare namespace xf = "http://bzwbk.com/services/GetCMFinance/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCMFinanceResponse($fml as element(fml:FML32))
	as element(m:GetCMFinanceResponse) {
		<m:GetCMFinanceResponse>
			{

				let $S_LIMIT_ID := $fml/fml:S_LIMIT_ID
				let $S_LIMIT_LEVEL := $fml/fml:S_LIMIT_LEVEL
				let $S_REK_ID := $fml/fml:S_REK_ID
				let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
				let $S_AMOUNT := $fml/fml:S_AMOUNT
				for $it at $p in $fml/fml:S_LIMIT_ID
				return
					<m:GetCMFinanceList>
					{
						if($S_LIMIT_ID[$p])
							then <m:LimitId>{ data($S_LIMIT_ID[$p]) }</m:LimitId>
						else ()
					}
					{
						if($S_LIMIT_LEVEL[$p])
							then <m:LimitLevel>{ data($S_LIMIT_LEVEL[$p]) }</m:LimitLevel>
						else ()
					}
					{
						if($S_REK_ID[$p])
							then <m:RekId>{ data($S_REK_ID[$p]) }</m:RekId>
						else ()
					}
					{
						if($B_KOD_WALUTY[$p])
							then <m:KodWaluty>{ data($B_KOD_WALUTY[$p]) }</m:KodWaluty>
						else ()
					}
					{
						if($S_AMOUNT[$p])
							then <m:Amount>{ data($S_AMOUNT[$p]) }</m:Amount>
						else ()
					}
					</m:GetCMFinanceList>
			}

		</m:GetCMFinanceResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapGetCMFinanceResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>