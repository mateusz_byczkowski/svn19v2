<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace prim = "http://bzwbk.com/services/prime/";
declare namespace xf="http://bzwbk.com/services/prime/functions";
declare namespace fml = "";
declare namespace none = "";
declare namespace fault="http://schemas.datacontract.org/2004/07/LoanServices";
declare namespace rsp="http://schemas.datacontract.org/2004/07/LoanServices";

declare variable $timestamp external;

declare function xf:mapErrorCode($errorCode as xs:string) as xs:string
{
       $errorCode
};


declare function xf:mapTimeStamp($timestamp as xs:string) as xs:string
{
        fn:concat (fn:substring($timestamp,1,10),"-",
                          fn:substring($timestamp,12,2),".",
                          fn:substring($timestamp,15,2),".",
                          fn:substring($timestamp,18),".000000")
};


declare function xf:mapResponse($resp as element (prim:CreateInstOnBalResponse))
	as element (fml:FML32) {

                               <fml:FML32>
			{
				<fml:B_STATUS?>{data ($resp/prim:CreateInstOnBalResult)}</fml:B_STATUS>,
                                                                <fml:B_URCODE?>0</fml:B_URCODE>,
                                                                <fml:B_TPERRNO?>0</fml:B_TPERRNO>,
                                                                <fml:B_OPIS_BLEDU?>ok</fml:B_OPIS_BLEDU>
			}
		</fml:FML32>
};


declare function xf:mapFault($fau as element(soap-env:Fault))
	as element (fml:FML32) {

	<fml:FML32>
		<fml:B_TPERRNO>{ data($fau/detail/fault:WbkFault/fault:ErrorCode1) }</fml:B_TPERRNO>
		<fml:B_URCODE>{ data($fau/detail/fault:WbkFault/fault:ErrorCode2) }</fml:B_URCODE>
		<fml:B_OPIS_BLEDU>{ data($fau/detail/fault:WbkFault/fault:ErrorDescription) }</fml:B_OPIS_BLEDU>
	</fml:FML32>
};



declare variable $body as element(soap-env:Body) external;


<soap-env:Body>
{
	if (boolean($body/prim:CreateInstOnBalResponse)) then (
 		xf:mapResponse($body/prim:CreateInstOnBalResponse)
 	) else if (boolean($body/soap-env:Fault/detail/fault:WbkFault/fault:ErrorCode2)) then (
 		xf:mapFault($body/soap-env:Fault)
	) else ()
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>