<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns4 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Pilot/GetProductFundsList/RequestTransform/";
declare namespace ns0 = "urn:baseentities.be.dcl";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns3 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:basedictionaries.be.dcl";
declare namespace ns5 = "urn:baseauxentities.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns4:invoke))
    as element(ns-1:getFundProductListRequest) {
        <ns-1:getFundProductListRequest>
            <envelope>
            	<request-time>{ fn:current-dateTime() }</request-time>
            	<source-code>{ data($invoke1/ns4:sourceCode/ns5:StringHolder/ns5:value) }</source-code>
            	<user-id>{ data($invoke1/ns4:baseUser/ns0:BaseUser/ns0:userID) }</user-id>
                <branch-id>{ data($invoke1/ns4:baseUser/ns0:BaseUser/ns0:branchCode/ns2:BaseBranchCode/ns2:branchCode) }</branch-id>
                <bar-code>{ data($invoke1/ns4:barCodeID/ns5:StringHolder/ns5:value) }</bar-code>
            </envelope>
            <product-type>{ data($invoke1/ns4:policyProductCode/ns3:PolicyProductCode/ns3:policyProductCode) }</product-type>
        </ns-1:getFundProductListRequest>
};

declare variable $invoke1 as element(ns4:invoke) external;

xf:RequestTransform($invoke1)]]></con:xquery>
</con:xqueryEntry>