<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.5
 : @since   2010-02-01
 :
 : wersja WSDLa: 05-10-2009 13:19:46
 :
 : $Proxy Services/Till/getReconciliationItems/getReconciliationItemsRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getReconciliationItems/getReconciliationItemsRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:cash.operations.entities.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns7 = "urn:baseauxentities.be.dcl";

declare variable $header1 as element(ns6:header) external;
declare variable $invoke1 as element(ns6:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:getReconciliationItemsRequest($header1 as element(ns6:header),
													$invoke1 as element(ns6:invoke))
    as element(ns0:FML32)
{
        <ns0:FML32>
        
        	(:
        	 : dane z nagłówka
        	 :)
        	<ns0:NF_MSHEAD_MSGID?>{
				data($header1/ns6:msgHeader/ns6:msgId)
			}</ns0:NF_MSHEAD_MSGID>
			
			(:
			 : dane wejściowe
			 :)
            <ns0:NF_INTEGH_VALUE?>{
				data($invoke1/ns6:scope/ns7:IntegerHolder/ns7:value)
			}</ns0:NF_INTEGH_VALUE>
			
			(:
			 : stronicowanie
			 :)
            <ns0:NF_PAGEC_ACTIONCODE?>{
				data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:actionCode)
			}</ns0:NF_PAGEC_ACTIONCODE>
			
            <ns0:NF_PAGEC_PAGESIZE?>{
				data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:pageSize)
			}</ns0:NF_PAGEC_PAGESIZE>
			
			(:
			 : kolejność stronicowania
			 : true  --> 1
			 : false --> 0
			 :)
            {
            	let $reverseOrder := $invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:reverseOrder
            	return
	            	if (data($reverseOrder)) then
		   	            <ns0:NF_PAGEC_REVERSEORDER>{
		            		if (data($reverseOrder) eq "true") then
	    	        			1
							else
								0
	    	        	}</ns0:NF_PAGEC_REVERSEORDER>
	        		else ()
			}
			
            <ns0:NF_PAGEC_NAVIGATIONKEYDEFI?>{
				data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:navigationKeyDefinition)
			}</ns0:NF_PAGEC_NAVIGATIONKEYDEFI>
			
            <ns0:NF_PAGEC_NAVIGATIONKEYVALU?>{
				data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:navigationKeyValue)
			}</ns0:NF_PAGEC_NAVIGATIONKEYVALU>
            
            (:
             : dane wejściowe - cd.
             :)
            <ns0:NF_BRANCC_BRANCHCODE?>{
				data($invoke1/ns6:branchCode/ns3:BranchCode/ns3:branchCode)
			}</ns0:NF_BRANCC_BRANCHCODE>
			
            <ns0:NF_USERTS_SESSIONDATE?>{
				data($invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:sessionDate)
			}</ns0:NF_USERTS_SESSIONDATE>
			
            {
            	for $i in 1 to count($invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:currencyReconciliationList/ns2:CurrencyReconciliation)
            	return
            	(
					let $currencyCode := $invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:currencyReconciliationList/ns2:CurrencyReconciliation[$i]/ns2:currency/ns3:CurrencyCode/ns3:currencyCode
					return
						if (data($currencyCode)) then
							<ns0:NF_CURREC_CURRENCYCODE>{
		            			data($currencyCode)
		            		}</ns0:NF_CURREC_CURRENCYCODE>
		            	else ()
		            ,
		            let $reconStatus := $invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:currencyReconciliationList/ns2:CurrencyReconciliation[$i]/ns2:reconciliationStatus/ns1:CurrencyReconcStatus/ns1:currencyReconcStatus
		            return
		            	if (data($reconStatus)) then
		            		<ns0:NF_CURRRS_CURRENCYRECONCST>{
		            			data($reconStatus)
    		        		}</ns0:NF_CURRRS_CURRENCYRECONCST>
    		        	else ()
            	)
            }
            
            <ns0:NF_TILL_TILLID?>{
				data($invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:till/ns4:Till/ns4:tillID)
			}</ns0:NF_TILL_TILLID>
			
        </ns0:FML32>
};

<soap-env:Body>{
	xf:getReconciliationItemsRequest($header1, $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>