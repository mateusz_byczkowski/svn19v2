<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="chgDenomSpecification.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="chgDenomSpecification.WSDL" ::)
(:: pragma bea:global-element-return element="ns6:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns2 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/chgDenomSpecification/chgDenomSpecification/";
declare namespace ns6 = "";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:entities.be.dcl";

declare function xf:chgDenomSpecification($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke))
    as element(ns6:FML32) {
        <ns6:FML32>
            <ns6:TR_ID_OPER?>{ data($header1/ns0:transHeader/ns0:transId) }</ns6:TR_ID_OPER>
            <ns6:TR_DATA_OPER?>
                {
                    let $transactionDate  := ($invoke1/ns0:transaction/ns7:Transaction/ns7:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			</ns6:TR_DATA_OPER>
            <ns6:TR_ODDZ_KASY?>{ xs:short( data($invoke1/ns0:branchCode/ns4:BranchCode/ns4:branchCode) ) }</ns6:TR_ODDZ_KASY>
            <ns6:TR_KASA?>{ xs:short( data($invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID) ) }</ns6:TR_KASA>
            <ns6:TR_KASJER?>{ xs:short( data($invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID) ) }</ns6:TR_KASJER>
            <ns6:TR_UZYTKOWNIK?>{ fn:concat("SKP:",data($invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns8:User/ns8:userID)) }</ns6:TR_UZYTKOWNIK>
            <ns6:TR_ID_GR_OPER?>{ data($invoke1/ns0:transaction/ns7:Transaction/ns7:cashTransactionBasketID)}</ns6:TR_ID_GR_OPER>
            <ns6:TR_MSG_ID?>{ data($header1/ns0:msgHeader/ns0:msgId) }</ns6:TR_MSG_ID>
            <ns6:TR_FLAGA_AKCEPT?>T</ns6:TR_FLAGA_AKCEPT>
            <ns6:TR_AKCEPTANT?>{ data($header1/ns0:msgHeader/ns0:userId) }</ns6:TR_AKCEPTANT>
            <ns6:TR_TYP_KOM?>{ xs:short( data($invoke1/ns0:transaction/ns7:Transaction/ns7:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType) ) }</ns6:TR_TYP_KOM>
            <ns6:TR_KANAL?>0</ns6:TR_KANAL>
            <ns6:TR_CZAS_OPER?>
                {
                    let $transactionTime  := ($header1/ns0:msgHeader/ns0:timestamp)
  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
			</ns6:TR_CZAS_OPER>
            <ns6:TR_AKCEPTANT_SKP?>{ data($header1/ns0:msgHeader/ns0:userId) }</ns6:TR_AKCEPTANT_SKP>
            <ns6:TR_UZYTKOWNIK_SKP?>{ data($invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns8:User/ns8:userID) }</ns6:TR_UZYTKOWNIK_SKP>


		   {
                for $CurrencyCash in ($invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:currencyCashList/ns1:CurrencyCash),
                    $DenominationSpecification in ($CurrencyCash/ns1:denominationSpecificationList/ns1:DenominationSpecification)
                   let $denominationID := ($DenominationSpecification/ns1:denomination/ns2:DenominationDefinition/ns2:denominationID)
                   let   $itemsNumber := ($DenominationSpecification/ns1:itemsNumber)
				where ($itemsNumber != 0)
                return
                (
                    <ns6:TR_NOMINAL>{ data($denominationID) }</ns6:TR_NOMINAL>,
                    <ns6:TR_NOMINAL_ILOSC>{ xs:double( data($itemsNumber) ) }</ns6:TR_NOMINAL_ILOSC>
                )
            }
            
        
        </ns6:FML32>
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;

<soap-env:Body>{
xf:chgDenomSpecification($header1,
    $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>