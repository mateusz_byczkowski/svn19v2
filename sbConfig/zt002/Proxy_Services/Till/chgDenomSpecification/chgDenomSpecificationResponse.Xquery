<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Dodanie fn:normalize-spaceVersion.$4.2010-10-22</con:description>
    <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$fML32OUT1" element="ns1:FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-return element="ns0:invokeResponse" location="chgDenomSpecification.wsdl" ::)


(:~
 :
 : @author  Grzegorz Strawa
 : @version 1.0
 : @since   2010-03-22
 :
 : wersja WSDLa: 04-02-2010 15:57:45
 :
 : $Proxy Services/Till/chgDenomSpecification/chgDenomSpecificationResponse.xq$
 :
 :) 
 
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "";
declare namespace ns3 = "urn:operations.entities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/chgDenomSpecification/chgDenomSpecificationResponse/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";

declare function xf:chgDenomSpecificationResponse($fML32OUT1 as element(ns1:FML32))
    as element(ns0:invokeResponse) {
        <ns0:invokeResponse>
            <ns0:transactionOut>
                <ns3:Transaction>
                    <ns3:transactionStatus>
                        <ns2:TransactionStatus>
                            <ns2:transactionStatus>{ data($fML32OUT1/ns1:TR_STATUS) }</ns2:transactionStatus>
                        </ns2:TransactionStatus>
                    </ns3:transactionStatus>
                </ns3:Transaction>
            </ns0:transactionOut>
                        <ns0:backendResponse>
                <ns3:BackendResponse>
                 {
                if (data($fML32OUT1/ns1:TR_DATA_KSIEG) ) then 
                   
                    <ns3:icbsDate>{  
                        let $transactionDate := $fML32OUT1/ns1:TR_DATA_KSIEG
					return
						fn:concat(
							fn:substring(data($transactionDate ), 7, 4),
							'-',
							fn:substring(data($transactionDate ), 4, 2),
							'-',
							fn:substring(data($transactionDate ), 1, 2)
							)
						}</ns3:icbsDate>
				else ()
				}
				
                {
                if (data($fML32OUT1/ns1:TR_CZAS_ODPOWIEDZI) ) then 
                   
                    <ns3:dateTime>{  
                        let $czasOdpowiedzi := $fML32OUT1/ns1:TR_CZAS_ODPOWIEDZI
					return
						fn:concat(
							fn:substring(data($czasOdpowiedzi ), 1, 10),
							'T',
							fn:substring(data($czasOdpowiedzi ), 12, 15)
							)
						}</ns3:dateTime>
				else ()
				}
                                   {               
                   if (data($fML32OUT1/ns1:TR_TXN_SESJA) ) then 
						<ns3:icbsSessionNumber>{ xs:string( data($fML32OUT1/ns1:TR_TXN_SESJA) ) }</ns3:icbsSessionNumber> 
                    else ()
                   }
                
                   {                
                    if (data($fML32OUT1/ns1:TR_TXN_NR)) then 
						<ns3:psTransactionNumber>{ xs:string( data($fML32OUT1/ns1:TR_TXN_NR) ) }</ns3:psTransactionNumber> 
                    else ()
                   }
                
                   {                
                    if (data($fML32OUT1/ns1:TR_ID_REF )) then 
						<ns3:transactionRefNumber>{ xs:string( data($fML32OUT1/ns1:TR_ID_REF) ) }</ns3:transactionRefNumber> 
                    else ()
                   }
                  
                  
                         <ns3:beUserId?>{ data($fML32OUT1/ns1:TR_UZYTKOWNIK) }</ns3:beUserId>
                         <ns3:beErrorCodeList>
                        {
                            for $FML320  in ($fML32OUT1/ns1:TR_KOD_BLEDU_1 union $fML32OUT1/ns1:TR_KOD_BLEDU_2 union $fML32OUT1/ns1:TR_KOD_BLEDU_3 union $fML32OUT1/ns1:TR_KOD_BLEDU_4 union $fML32OUT1/ns1:TR_KOD_BLEDU_5)  
                            return
                                if ((fn:boolean(fn:normalize-space($FML320)!="") and fn:boolean($FML320!="000"))) then
                                    (<ns3:BeErrorCode>
                                    <ns3:errorCode>
                                    <ns5:BackendErrorCode>                                    
                                    <ns5:errorCode>{ data($FML320) }</ns5:errorCode>                                   
                                    </ns5:BackendErrorCode>
                                    </ns3:errorCode>
                                    </ns3:BeErrorCode>)
                                else 
                                    ()
                        }
					</ns3:beErrorCodeList>
                </ns3:BackendResponse>
            </ns0:backendResponse>

        </ns0:invokeResponse>
};

declare variable $fML32OUT1 as element(ns1:FML32) external;

<soap-env:Body>{
  xf:chgDenomSpecificationResponse($fML32OUT1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>