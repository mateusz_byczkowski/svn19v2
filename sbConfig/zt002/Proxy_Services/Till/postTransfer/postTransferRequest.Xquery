<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$header1" element="ns1:header" location="postTransfer.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns1:invoke" location="postTransfer.WSDL" ::)
(:: pragma bea:global-element-return element="ns7:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

xquery version "1.0";


(:~
 :
 : @author  Grzegorz Strawa
 : @version 1.2
 : @since   2010-11-26
 :
 : wersja WSDLa: 25-10-2010 17:34:22
 :
 : $Proxy Services/Till/postTransfer/postTransferRequest.xq$
 :
 :) 

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/postTransfer/postTransferRequest/";
declare namespace ns0 = "urn:swift.operations.entities.be.dcl";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns2 = "urn:fee.operations.entities.be.dcl";
declare namespace ns3 = "urn:transactionfeedict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:dictionaries.be.dcl";
declare namespace ns6 = "urn:transactionswiftdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns7 = "";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace ns9 = "urn:acceptance.entities.be.dcl";
declare namespace ns10 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns11 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns12 = "urn:entities.be.dcl";

declare variable $header1 as element(ns1:header) external;
declare variable $invoke1 as element(ns1:invoke) external;

declare function pad-string-to-length 
  ( $stringToPad as xs:string? ,
    $padChar as xs:string ,
    $length as xs:integer )  as xs:string {
       
   substring(
     string-join (
       ($stringToPad, for $i in (1 to $length) return $padChar)
       ,'')
    ,1,$length)
 } ;
(:~
(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:postTransferRequest($header1 as element(ns1:header),
    									  $invoke1 as element(ns1:invoke))
    as element(ns7:FML32)
{
	<ns7:FML32>
	
        <ns7:TR_ID_OPER?>{
			data($header1/ns1:transHeader/ns1:transId)
		}</ns7:TR_ID_OPER>
		

		{
			let $transactionDate := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionDate)
			return
				if ($transactionDate) then
		            <ns7:TR_DATA_OPER?>{
						fn:concat(fn:substring($transactionDate, 9, 2),
		                          '-',
		                          fn:substring($transactionDate, 6, 2),
		                          '-',
		                          fn:substring($transactionDate, 1, 4))
					}</ns7:TR_DATA_OPER>					
				else
					()
		}

		<ns7:TR_ODDZ_KASY?>{
			data($invoke1/ns1:branchCode/ns5:BranchCode/ns5:branchCode)
		}</ns7:TR_ODDZ_KASY>

		<ns7:TR_KASA?>{
			data($invoke1/ns1:userTxnSession/ns4:UserTxnSession/ns4:till/ns4:Till/ns4:tillID)
		}</ns7:TR_KASA>

		<ns7:TR_KASJER?>{
			data($invoke1/ns1:userTxnSession/ns4:UserTxnSession/ns4:teller/ns4:Teller/ns4:tellerID)
		}</ns7:TR_KASJER>

		{
			let $userID := data($invoke1/ns1:userTxnSession/ns4:UserTxnSession/ns4:user/ns12:User/ns12:userID)
			return
				if ($userID) then
					<ns7:TR_UZYTKOWNIK?>{
						fn:concat('SKP:', $userID)
					}</ns7:TR_UZYTKOWNIK>		
				else
					()
		}

        <ns7:TR_ID_GR_OPER?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:cashTransactionBasketID)
		}</ns7:TR_ID_GR_OPER>

		<ns7:TR_MSG_ID?>{
				data($header1/ns1:msgHeader/ns1:msgId)
		}</ns7:TR_MSG_ID>

		<ns7:TR_FLAGA_AKCEPT?>
                {
                if (data($invoke1/ns1:transaction/ns8:Transaction/ns8:acceeptanceForBe/ns8:AcceptanceForBE/ns8:flag)) then
                (data($invoke1/ns1:transaction/ns8:Transaction/ns8:acceeptanceForBe/ns8:AcceptanceForBE/ns8:flag))
                else
                'T'
                }
		</ns7:TR_FLAGA_AKCEPT>

			 
		{
			let $acceptor := data($invoke1/ns1:transaction/ns8:Transaction/ns8:acceptTask/ns9:AcceptTask/ns9:acceptor)
			
			return
				if ($acceptor) then
					<ns7:TR_AKCEPTANT?>{
						fn:concat('SKP:', $acceptor)
					}</ns7:TR_AKCEPTANT>		
				else
					()
		}

		<ns7:TR_TYP_KOM?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:csrMessageType/ns10:CsrMessageType/ns10:csrMessageType)
		}</ns7:TR_TYP_KOM>

		<ns7:TR_KANAL?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:extendedCSRMessageType/ns10:ExtendedCSRMessageType/ns10:extendedCSRMessageType)     
		}</ns7:TR_KANAL>
            
		<ns7:TR_RACH_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:accountNumber)
		}</ns7:TR_RACH_NAD>
	
		<ns7:TR_KWOTA_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:amountWn)
		}</ns7:TR_KWOTA_NAD>
	
		<ns7:TR_WALUTA_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:currencyCode/ns5:CurrencyCode/ns5:currencyCode)
		}</ns7:TR_WALUTA_NAD>
	
		<ns7:TR_KURS_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:rate)
		}</ns7:TR_KURS_NAD>
	
	
			{
			let $name := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:name)
			return
				if ($name) then
					<ns7:TR_NAZWA_NAD?>{
						fn-bea:trim-right($name)
					}</ns7:TR_NAZWA_NAD>			
				else
					()
				}
	
			{
			let $nameSecond  := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:nameSecond)
			return
				if  ($nameSecond) then
					<ns7:TR_NAZWA_NAD_2?>{
						fn-bea:trim-right($nameSecond)
					}</ns7:TR_NAZWA_NAD_2>			
				else
					()
				}
		
			
		
	
		<ns7:TR_MIEJSCOWOSC_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:city)
		}</ns7:TR_MIEJSCOWOSC_NAD>
	
		<ns7:TR_ULICA_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:address)
		}</ns7:TR_ULICA_NAD>
	
		<ns7:TR_KOD_POCZT_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:zipCode)
		}</ns7:TR_KOD_POCZT_NAD>
	
		<ns7:TR_ROWN_PLN_NAD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:amountWnEquiv)
		}</ns7:TR_ROWN_PLN_NAD>
	
		<ns7:TR_RACH_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:accountNumber)
		}</ns7:TR_RACH_ADR>
	
		<ns7:TR_KWOTA_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:amountMa)
		}</ns7:TR_KWOTA_ADR>
	
		<ns7:TR_WALUTA_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:currencyCode/ns5:CurrencyCode/ns5:currencyCode)
		}</ns7:TR_WALUTA_ADR>
	
		<ns7:TR_KURS_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:rate)
		}</ns7:TR_KURS_ADR>



			{
			let $name := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:name)
			return
				if ($name) then
					<ns7:TR_NAZWA_ADR?>{
						fn-bea:trim-right($name)
					}</ns7:TR_NAZWA_ADR>			
				else
					()
				}

			{
			let $nameSecond  := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:nameSecond)
			return
				if  ($nameSecond) then
					<ns7:TR_NAZWA_ADR_2?>{
						fn-bea:trim-right($nameSecond)
					}</ns7:TR_NAZWA_ADR_2>			
				else
					()
				}

	
		<ns7:TR_MIEJSCOWOSC_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:city)
		}</ns7:TR_MIEJSCOWOSC_ADR>
	
		<ns7:TR_ULICA_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:address)
		}</ns7:TR_ULICA_ADR>
	
		<ns7:TR_KOD_POCZT_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:zipCode)
		}</ns7:TR_KOD_POCZT_ADR>
	
		<ns7:TR_ROWN_PLN_ADR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa/ns8:TransactionMa/ns8:amountMaEquiv)
		}</ns7:TR_ROWN_PLN_ADR>
	
		<ns7:TR_TYTUL?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:title)
		}</ns7:TR_TYTUL>


{
			let $transactionDate := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionDate)
			return
				if ($transactionDate) then
		            <ns7:TR_DATA_WALUTY?>{
						fn:concat(fn:substring($transactionDate, 9, 2),
		                          '-',
		                          fn:substring($transactionDate, 6, 2),
		                          '-',
		                          fn:substring($transactionDate, 1, 4))
					}</ns7:TR_DATA_WALUTY>					
				else
					()
		}


            
            
	    <ns7:TR_NUM_CZEKU?>{ 
	    data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionInkaso/ns8:TransactionInkaso/ns8:chequeNbr) 
	    }</ns7:TR_NUM_CZEKU>
    
		<ns7:TR_ZUS_NIP?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionZUS/ns8:TransactionZUS/ns8:nip)
		}</ns7:TR_ZUS_NIP>
	
		<ns7:TR_ZUS_TYP_IDENTYF?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionZUS/ns8:TransactionZUS/ns8:idType/ns10:IDType/ns10:idType)
		}</ns7:TR_ZUS_TYP_IDENTYF>
	
		<ns7:TR_ZUS_IDENTYF_DOD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionZUS/ns8:TransactionZUS/ns8:idValue)
		}</ns7:TR_ZUS_IDENTYF_DOD>
	
		<ns7:TR_ZUS_TYP_PLAT?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionZUS/ns8:TransactionZUS/ns8:paymentType/ns10:ZUSPaymentType/ns10:paymentType)
		}</ns7:TR_ZUS_TYP_PLAT>
	
		<ns7:TR_ZUS_NR_DEKLAR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionZUS/ns8:TransactionZUS/ns8:declarationNumber)
		}</ns7:TR_ZUS_NR_DEKLAR>
	
		<ns7:TR_ZUS_DATA_DEKLAR?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionZUS/ns8:TransactionZUS/ns8:declarationDate)
		}</ns7:TR_ZUS_DATA_DEKLAR>
	
		<ns7:TR_ZUS_NR_DECYZJI?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionZUS/ns8:TransactionZUS/ns8:decisionNumber)
		}</ns7:TR_ZUS_NR_DECYZJI>
	
			{
			let $TransactionWnName  := fn-bea:trim-right(concat(data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:name)," ",
										data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:nameSecond)))
			let $declarationNumber:=data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionZUS/ns8:TransactionZUS/ns8:declarationNumber)
			return
				if ($TransactionWnName and $declarationNumber) then
					<ns7:TR_ZUS_NAZWA_SKR_PLAT?>{
							$TransactionWnName
					}</ns7:TR_ZUS_NAZWA_SKR_PLAT>			
				else
					()
			}	
            
		<ns7:TR_US_TYP_IDENTYF?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionUS/ns8:TransactionUS/ns8:idType/ns10:IDType/ns10:idType)
		}</ns7:TR_US_TYP_IDENTYF>
	
		<ns7:TR_US_IDENTYF_DOD?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionUS/ns8:TransactionUS/ns8:idValue)
		}</ns7:TR_US_IDENTYF_DOD>
	
		<ns7:TR_US_OKRES?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionUS/ns8:TransactionUS/ns8:period)
		}</ns7:TR_US_OKRES>
	
		<ns7:TR_US_SYMBOL_FORM?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionUS/ns8:TransactionUS/ns8:symbolForm/ns10:SymbolForm/ns10:symbolForm)
		}</ns7:TR_US_SYMBOL_FORM>
	
		<ns7:TR_US_IDENTYF_ZOBOW?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionUS/ns8:TransactionUS/ns8:commitmentID)
		}</ns7:TR_US_IDENTYF_ZOBOW>
			{
			let $TransactionWnName := fn-bea:trim-right(concat(data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:name)," ",
										data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn/ns8:TransactionWn/ns8:nameSecond)))
			let $symbolForm  := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionUS/ns8:TransactionUS/ns8:symbolForm/ns10:SymbolForm/ns10:symbolForm)
			return
				if ($TransactionWnName and $symbolForm) then
					<ns7:TR_US_NAZWA_SKR_POD?>{
							$TransactionWnName
					}</ns7:TR_US_NAZWA_SKR_POD>			
				else
					()
            }
              <ns7:TR_CZAS_OPER?>
                {
                    let $transactionTime  := ($header1/ns1:msgHeader/ns1:timestamp)
  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
				</ns7:TR_CZAS_OPER>
    
            {
                for  $TransactionFeeField in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionFee/ns2:TransactionFee/ns2:transactionFeeFieldList/ns2:TransactionFeeField
                return
				(	
                 if (data($TransactionFeeField/ns2:feeField) eq "FEE_1")
                 then(
                    <ns7:TR_OPLATA_1?>{data($TransactionFeeField/ns2:fieldFeeAmount)}</ns7:TR_OPLATA_1>)
                   else()
                   ,
                 if (data($TransactionFeeField/ns2:feeField) eq "FEE_2")
                 then(
                    <ns7:TR_OPLATA_2?>{data($TransactionFeeField/ns2:fieldFeeAmount)}</ns7:TR_OPLATA_2>)
                  else()
                   ,
                 if (data($TransactionFeeField/ns2:feeField) eq "FEE_3")
                 then(
                    <ns7:TR_OPLATA_3?>{data($TransactionFeeField/ns2:fieldFeeAmount)}</ns7:TR_OPLATA_3>)
                  else()
                   ,
                 if (data($TransactionFeeField/ns2:feeField) eq "FEE_4")
                 then(
                    <ns7:TR_OPLATA_4?>{data($TransactionFeeField/ns2:fieldFeeAmount)}</ns7:TR_OPLATA_4>)
                  else()
                   ,
                 if (data($TransactionFeeField/ns2:feeField) eq "FEE_5")
                 then(
                    <ns7:TR_OPLATA_5?>{data($TransactionFeeField/ns2:fieldFeeAmount)}</ns7:TR_OPLATA_5>)
                  else(),
                 if (data($TransactionFeeField/ns2:feeField) eq "FEE_6")
                  then(
                    <ns7:TR_OPLATA_6?>{data($TransactionFeeField/ns2:fieldFeeAmount)}</ns7:TR_OPLATA_6>)
                  else()
                )
             }
             
         (: SWIFT :)   
         <ns7:TR_ID_END_TO_END?>{
          	data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:transactionSepa/ns0:TransactionSepa/ns0:endToEndIdentifier) 
          }</ns7:TR_ID_END_TO_END>
         
          <ns7:TR_ID_ZLEC?>{ 
          data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:transactionSepa/ns0:TransactionSepa/ns0:sender/ns0:SwiftSepaIdentifier/ns0:identifierValue) 
          }</ns7:TR_ID_ZLEC>
          
          <ns7:TR_ID_BENEF?>{ 
          data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:transactionSepa/ns0:TransactionSepa/ns0:receiver/ns0:SwiftSepaIdentifier/ns0:identifierValue) 
          }</ns7:TR_ID_BENEF>
          
          <ns7:TR_FLAGA_ID_ZLEC?>{ 
          data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:transactionSepa/ns0:TransactionSepa/ns0:sender/ns0:SwiftSepaIdentifier/ns0:identifierType/ns6:SwiftSepaIDType/ns6:swiftSepaIDType) 
          }</ns7:TR_FLAGA_ID_ZLEC>
          
            <ns7:TR_KOD_KRAJU_NAD?>{
             data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:transactionSepa/ns0:TransactionSepa/ns0:senderCountry/ns5:CountryCode/ns5:countryCode) 
             }</ns7:TR_KOD_KRAJU_NAD>
             
            <ns7:TR_KOD_KRAJU_ADR?>{ 
            data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:transactionSepa/ns0:TransactionSepa/ns0:receiverCountry/ns5:CountryCode/ns5:countryCode)
             }</ns7:TR_KOD_KRAJU_ADR>
          
          {
          for $AuxiliaryAccount in ($invoke1/ns1:transaction/ns8:Transaction/ns8:auxiliaryAccountList/ns8:AuxiliaryAccount)
             let $AccountNumber := ($AuxiliaryAccount/ns8:accountNumber)
             let $SequenceNumber := ($AuxiliaryAccount/ns8:sequenceNumber)
			where data($SequenceNumber > 0) 
                return
				(	
                 if (data($SequenceNumber) = 1)
                 then(
                    <ns7:TR_RACH_1?>{data($AccountNumber)}</ns7:TR_RACH_1>)
                   else()
                   ,
                 if (data($SequenceNumber) = 2)
                 then(
                    <ns7:TR_RACH_2?>{data($AccountNumber)}</ns7:TR_RACH_2>)
                   else()
                   ,
                 if (data($SequenceNumber) = 3)
                 then(
                    <ns7:TR_RACH_3?>{data($AccountNumber)}</ns7:TR_RACH_3>)
                   else()
                   ,
                 if (data($SequenceNumber) = 4)
                 then(
                    <ns7:TR_RACH_4?>{data($AccountNumber)}</ns7:TR_RACH_4>)
                   else()
                   )
                   
          }
          
          
          
          
          
          <ns7:TR_FLAGA_ID_BENEF?>{ 
          data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:transactionSepa/ns0:TransactionSepa/ns0:receiver/ns0:SwiftSepaIdentifier/ns0:identifierType/ns6:SwiftSepaIDType/ns6:swiftSepaIDType) 
          }</ns7:TR_FLAGA_ID_BENEF>
            <TR_RACH_POWIAZANY?>{ data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionGLEsp/ns8:TransactionGLEsp/ns8:accountNbr) }</TR_RACH_POWIAZANY>
            
{
    for  $Disposer in ($invoke1/ns1:transaction/ns8:Transaction/ns8:disposerList/ns8:Disposer)
       let $firstName := data($Disposer/ns8:firstName)
	    let $lastName  := data($Disposer/ns8:lastName)
			
        return
        (
	     
		<ns7:TR_DYSP_CIF>{
			data($Disposer/ns8:cif)
		}</ns7:TR_DYSP_CIF>,

		<ns7:TR_DYSP_NAZWA>{
			fn-bea:trim-right(fn:concat($firstName," ", $lastName))
		}</ns7:TR_DYSP_NAZWA>,
	
		<ns7:TR_DYSP_OBYWATELSTWO>{
			data($Disposer/ns8:citizenship/ns5:CitizenshipCode/ns5:citizenshipCode)
		}</ns7:TR_DYSP_OBYWATELSTWO>
	,
		<ns7:TR_DYSP_KRAJ>{
			data($Disposer/ns8:countryCode/ns5:CountryCode/ns5:countryCode)
		}</ns7:TR_DYSP_KRAJ>
	,
		<ns7:TR_DYSP_MIEJSCOWOSC>{
			data($Disposer/ns8:city)
		}</ns7:TR_DYSP_MIEJSCOWOSC>
	,
		<ns7:TR_DYSP_KOD_POCZT>{
			data($Disposer/ns8:zipCode)
		}</ns7:TR_DYSP_KOD_POCZT>
	,
	
		<ns7:TR_DYSP_ULICA>{
			data($Disposer/ns8:address)
		}</ns7:TR_DYSP_ULICA>
	,
		<ns7:TR_DYSP_PESEL>{
			data($Disposer/ns8:pesel)
		}</ns7:TR_DYSP_PESEL>
	,
		<ns7:TR_DYSP_NR_PASZPORTU>{
			data($Disposer/ns8:documentNumber)
		}</ns7:TR_DYSP_NR_PASZPORTU>,

                <TR_DYSP_NR_DOWOD>{ data($Disposer/ns8:documentType/ns10:DocumentTypeForTxn/ns10:documentCodeForGIIF) }</TR_DYSP_NR_DOWOD>,
                <TR_DYSP_TELEFON>{ data($Disposer/ns8:documentType/ns10:DocumentTypeForTxn/ns10:commentForGIIF) }</TR_DYSP_TELEFON>
                
	)
}	
		<ns7:TR_AKCEPTANT_SKP?>{
			data($invoke1/ns1:transaction/ns8:Transaction/ns8:acceptTask/ns9:AcceptTask/ns9:acceptor)
		}</ns7:TR_AKCEPTANT_SKP>
		
            
		<ns7:TR_UZYTKOWNIK_SKP?>{
			data($invoke1/ns1:userTxnSession/ns4:UserTxnSession/ns4:user/ns12:User/ns12:userID)
		}</ns7:TR_UZYTKOWNIK_SKP>

     (: SWIFT :)
     		<ns7:TR_SWIFT_IDEN_OPLATY?>{ 
     		data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:swiftFee/ns0:SwiftFee/ns0:slinkFeeId) 
     		}</ns7:TR_SWIFT_IDEN_OPLATY>
     		
            <ns7:TR_SWIFT_IDEN_PROD?>{ 
            data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:slinkProductNumber) 
            }</ns7:TR_SWIFT_IDEN_PROD>
            
            <ns7:TR_SWIFT_SEQ_NO?>{ 
            data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:swiftBenefBank/ns0:SwiftBenefBank/ns0:branchBankId) 
            }</ns7:TR_SWIFT_SEQ_NO>
            
            <ns7:TR_SWIFT_NAZWA_BANKU?>{ 
            data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:swiftBenefBank/ns0:SwiftBenefBank/ns0:name) 
            }</ns7:TR_SWIFT_NAZWA_BANKU>
            
            <ns7:TR_SWIFT_NR_ODDZUALU?>{ 
            data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:swiftBenefBank/ns0:SwiftBenefBank/ns0:nationalId) 
            }</ns7:TR_SWIFT_NR_ODDZUALU>
            
            <ns7:TR_SWIFT_BIC?>{ 
            data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:swiftBenefBank/ns0:SwiftBenefBank/ns0:bic) 
            }</ns7:TR_SWIFT_BIC>

         <ns7:TR_SWIFT_ADRES_BANKU?>{ 
        let $Address := fn:substring(data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:swiftBenefBank/ns0:SwiftBenefBank/ns0:address),1,70) 
        let $AddressToPass := pad-string-to-length($Address, ' ', 70)
        let $Description := pad-string-to-length(data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:swiftBenefBank/ns0:SwiftBenefBank/ns0:country/ns5:CountryCode/ns5:description), ' ',35)
           return
                (fn:concat($AddressToPass, $Description))
                
        }</ns7:TR_SWIFT_ADRES_BANKU>

           
            
            <ns7:TR_SWIFT_SZCZEG_PROW?>{ 
            data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:costOption/ns6:SwiftCostOption/ns6:swiftCostOption) 
            }</ns7:TR_SWIFT_SZCZEG_PROW>
            
            <ns7:TR_SWIFT_KOD_SLOWNIK?>{ 
            data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:paymentInstruction/ns0:SwiftPaymentInstruction/ns0:paymentInstructionType/ns6:SwiftPaymentInstructionType/ns6:codeWord) 
            }</ns7:TR_SWIFT_KOD_SLOWNIK>
            
            <ns7:TR_SWIFT_OPIS?>{ 
            data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:paymentInstruction/ns0:SwiftPaymentInstruction/ns0:paymentInstructionText) 
            }</ns7:TR_SWIFT_OPIS>
            
            
            {
            let $valueDate := data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:valueDate)
			return
				if ($valueDate) then
		            <ns7:TR_SWIFT_DATA_NEG?>{
						fn:concat(fn:substring($valueDate, 9, 2),
		                          '-',
		                          fn:substring($valueDate, 6, 2),
		                          '-',
		                          fn:substring($valueDate, 1, 4))
					}</ns7:TR_SWIFT_DATA_NEG>					
				else
					()
            }
            <ns7:TR_SWIFT_MIASTO?>{ 
            data($invoke1/ns1:transaction/ns8:Transaction/ns8:transactionSwift/ns0:TransactionSwift/ns0:swiftBenefBank/ns0:SwiftBenefBank/ns0:city) 
            }</ns7:TR_SWIFT_MIASTO>
	</ns7:FML32>
};


<soap-env:Body>{
	xf:postTransferRequest($header1, $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>