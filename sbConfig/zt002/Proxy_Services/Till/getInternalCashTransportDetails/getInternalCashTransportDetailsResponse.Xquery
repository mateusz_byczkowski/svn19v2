<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2010-06-30
 :
 : wersja WSDLa: 19-04-2010 09:48:18
 :
 : $Proxy Services/Till/getInternalCashTransportDetails/getInternalCashTransportDetailsResponse.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getInternalCashTransportDetails/getInternalCashTransportDetailsResponse/";
declare namespace ns0 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns2 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns6 = "urn:internaltransports.operations.entities.be.dcl";
declare namespace ns7 = "urn:be.services.dcl";
declare namespace ns8 = "";
declare namespace ns9 = "urn:internaltransportsdict.operationsdictionary.dictionaries.be.dcl";

declare variable $fML321 as element(ns8:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getInternalCashTransportDetailsResponse($fML321 as element(ns8:FML32))
    as element(ns7:invokeResponse)
{
	<ns7:invokeResponse>
        <ns7:internalCashTransportOut>
			<ns6:InternalCashTransport>

		        <ns6:sourceName?>{
					data($fML321/ns8:NF_INTECT_SOURCENAME)
				}</ns6:sourceName>

				<ns6:targetName?>{
					data($fML321/ns8:NF_INTECT_TARGETNAME)
				}</ns6:targetName>

				(:
				 : data niepusta, różna od '0001-01-01'
				 :)
				{
					let $transportOpenDate := data($fML321/ns8:NF_INTECT_TRANSPORTOPENDAT)
					return
						if ($transportOpenDate
							and $transportOpenDate ne '0001-01-01') then
							<ns6:transportOpenDate>{
								$transportOpenDate
							}</ns6:transportOpenDate>
						else
							()
				}

				(:
				 : data niepusta, różna od '0001-01-01'
				 :)
				{
					let $transportCloseDate := data($fML321/ns8:NF_INTECT_TRANSPORTCLOSEDA)
					return
						if ($transportCloseDate
							and $transportCloseDate ne '0001-01-01') then
							<ns6:transportCloseDate>{
								$transportCloseDate
							}</ns6:transportCloseDate>
						else
							()
				}

				<ns6:currencyCashList?>{

					(:
					 : lista walut - distinct
					 :)
					let $currencyList := fn:distinct-values($fML321/ns8:NF_CURREC_CURRENCYCODE)

					(:
					 : dla każdej różnej waluty
					 :)
					for $currencyCode in $currencyList
					return
						<ns1:CurrencyCash>
							<ns1:amount>{

								(:
								 : licznosc $fML321/ns8:NF_CURRCA_AMOUNT == licznosc $fML321/ns8:NF_CURREC_CURRENCYCODE
								 :
								 : Ponieważ kwoty dla dwóch różnych walut mogą się powtarzać,
								 : szukamy pierwszego indeksu na liście wszystkich dla każdej z walut.
								 : Znaleziony indeks jest również indeksem odpowiadającej walucie kwoty.
								 :
								 : Przykładowo:
								 : NF_CURRCA_CURRENCYCODE    PLN
								 : NF_CURRCA_CURRENCYCODE    PLN
								 : NF_CURRCA_CURRENCYCODE    USD
								 : NF_CURRCA_AMOUNT          1000.00 (dla PLN)
								 : NF_CURRCA_AMOUNT          1000.00 (dla PLN)
								 : NF_CURRCA_AMOUNT          1000.00 (dla USD)
								 :
								 : wowczas index-of dla $currencyCode == PLN zwroci (1,2) (wybieramy pierwsze wystapnienie - [1])
								 :         index-of dla $currencyCode == USD zwroci (3)   (analogicznie)
								 :
								 :)
								let $amountIndex := fn:index-of($fML321/ns8:NF_CURREC_CURRENCYCODE, $currencyCode)[1]
								return
									data($fML321/ns8:NF_CURRCA_AMOUNT[xs:int($amountIndex)])
							}</ns1:amount>

							<ns1:denominationSpecificationList>{
								(:
								 : wszystkie wystąpienia NF_DENOMS_ITEMSNUMBER oraz NF_DENOMD_DENOMINATIONID
								 : na liscie wszystkich walut, ktore odpowiadaja
								 : obecnie rozważanej walucie (distinct)
								 :
								 : Przykladowo:
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE USD
								 : NF_CURRCA_CURRENCYCODE USD
								 :

								 : Pierwsze przejście:
								 :      NF_DENOMS_ITEMSNUMBER, NF_DENOMD_DENOMINATIONID z 1., 2. oraz 3. wystąpienia
								 : Drugie przejście:
								 :		NF_DENOMS_ITEMSNUMBER, NF_DENOMD_DENOMINATIONID z 4. oraz 5. wystąpienia
								 :)
								for $i in 1 to count($fML321/ns8:NF_CURREC_CURRENCYCODE)
								where (data($fML321/ns8:NF_CURREC_CURRENCYCODE[$i]) eq $currencyCode)
								return
									<ns1:DenominationSpecification>
										<ns1:itemsNumber>{
											data($fML321/ns8:NF_DENOMS_ITEMSNUMBER[$i])
										}</ns1:itemsNumber>
	
										<ns1:denomination>
											<ns0:DenominationDefinition>
												<ns0:denominationID>{
													data($fML321/ns8:NF_DENOMD_DENOMINATIONID[$i])
												}</ns0:denominationID>
											</ns0:DenominationDefinition>
										</ns1:denomination>
									</ns1:DenominationSpecification>

							}</ns1:denominationSpecificationList>

							<ns1:currency>
					            <ns3:CurrencyCode>
			                        <ns3:currencyCode>{
										data($currencyCode)
									}</ns3:currencyCode>
								</ns3:CurrencyCode>
							</ns1:currency>
						</ns1:CurrencyCash>

				}</ns6:currencyCashList>

                <ns6:sourceTill?>
                    <ns4:Till?>
                        <ns4:tillID?>{
							data($fML321/ns8:NF_TILL_TILLID)
						}</ns4:tillID>
						<ns4:tillType?>
							<ns2:TillType?>
								<ns2:tillType?>{
									data($fML321/ns8:NF_TILTY_TILLTYP[1])
								}</ns2:tillType>
							</ns2:TillType>
						</ns4:tillType>						
                    </ns4:Till>
                </ns6:sourceTill>

				<ns6:sourceTeller?>
				    <ns4:Teller?>
				        <ns4:tellerID?>{
							data($fML321/ns8:NF_TELLER_TELLERID)
						}</ns4:tellerID>
				    </ns4:Teller>
				</ns6:sourceTeller>

                <ns6:targetTill?>
                    <ns4:Till?>
                        <ns4:tillID?>{
							data($fML321/ns8:NF_TILL_TILLID2)
						}</ns4:tillID>
                        <ns4:tillType?>
                            <ns2:TillType?>
                                <ns2:tillType?>{
									data($fML321/ns8:NF_TILTY_TILLTYP[2])
								}</ns2:tillType>
                            </ns2:TillType>
                        </ns4:tillType>
                    </ns4:Till>
                </ns6:targetTill>

                <ns6:targetTeller?>
                    <ns4:Teller?>
                        <ns4:tellerID?>{
							data($fML321/ns8:NF_TELLER_TELLERID2)
						}</ns4:tellerID>
                    </ns4:Teller>
                </ns6:targetTeller>

                <ns6:transportStatus?>
                    <ns9:InternalCashTransportStatus?>
                        <ns9:internalCashTransportStatus?>{
							data($fML321/ns8:NF_INTCTS_INTERNALCASHTRAN)
						}</ns9:internalCashTransportStatus>
                    </ns9:InternalCashTransportStatus>
                </ns6:transportStatus>

                <ns6:transportExtStatus?>
                    <ns9:InternalCashTranspExtStatus?>
                        <ns9:internalCashTranspExtStatus?>{
							data($fML321/ns8:NF_INCTES_INTERNALCASHTRAN)
						}</ns9:internalCashTranspExtStatus>
                    </ns9:InternalCashTranspExtStatus>
                </ns6:transportExtStatus>

                <ns6:initiatorUser?>
                    <ns5:User?>
                        <ns5:userLastName?>{
							data($fML321/ns8:NF_USER_USERLASTNAME[1])
						}</ns5:userLastName>

                        <ns5:userID?>{
							data($fML321/ns8:NF_USER_USERID[1])
						}</ns5:userID>

                        <ns5:userFirstName?>{
							data($fML321/ns8:NF_USER_USERFIRSTNAME[1])
						}</ns5:userFirstName>
                    </ns5:User>
                </ns6:initiatorUser>

                <ns6:finisherUser?>
                    <ns5:User?>
                        <ns5:userLastName?>{
							data($fML321/ns8:NF_USER_USERLASTNAME[2])
						}</ns5:userLastName>

                        <ns5:userID?>{
							data($fML321/ns8:NF_USER_USERID[2])
						}</ns5:userID>

                        <ns5:userFirstName?>{
							data($fML321/ns8:NF_USER_USERFIRSTNAME[2])
						}</ns5:userFirstName>
                    </ns5:User>
                </ns6:finisherUser>

                <ns6:branchCurrentStatus?>
                    <ns4:BranchCurrentStatus?>
						(:
						 : data niepusta, różna od '0001-01-01'
						 :)
						{
							let $workDate := data($fML321/ns8:NF_BRACST_WORKDATE)
							return
								if ($workDate
									and $workDate ne '0001-01-01') then
			                        <ns4:workDate>{
										$workDate
									}</ns4:workDate>
								else
									()
						}

                        <ns4:branchCode?>
                            <ns3:BranchCode?>
                                <ns3:branchCode?>{
									data($fML321/ns8:NF_BRANCC_BRANCHCODE)
								}</ns3:branchCode>
                            </ns3:BranchCode>
                        </ns4:branchCode>
                    </ns4:BranchCurrentStatus>
                </ns6:branchCurrentStatus>
			</ns6:InternalCashTransport>
		</ns7:internalCashTransportOut>
	</ns7:invokeResponse>
};

<soap-env:Body>{
	xf:getInternalCashTransportDetailsResponse($fML321)	
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>