<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Grzegorz Strawa
 : @version 1.1
 : @since   2010-03-22
 :
 : wersja WSDLa: 21-01-2010 09:35:56
 :
 : $Proxy Services/Till/postExchangeCurrency/postExchangeCurrencyResponse.xq$
 :
 :) 

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/postExchangeCurrency/postExchangeCurrencyResponse/";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns4 = "urn:operations.entities.be.dcl";

declare variable $fML32OUT1 as element(ns1:FML32) external;

(:~
 : @param FML32 bufor XML/FML
 :
 : @return inovkeResponse operacja wyjściowa
 :)
declare function xf:postExchangeCurrencyResponse($fML32OUT1 as element(ns1:FML32))
    as element(ns0:invokeResponse)
{
	<ns0:invokeResponse>
		<ns0:transactionOut>
			<ns4:Transaction?>
				<ns4:transactionStatus?>
					<ns2:TransactionStatus?>
						<ns2:transactionStatus?>{
							data($fML32OUT1/ns1:TR_STATUS)
						}</ns2:transactionStatus>
					</ns2:TransactionStatus>
				</ns4:transactionStatus>
			</ns4:Transaction>
		</ns0:transactionOut>

		<ns0:backendResponse>
			<ns4:BackendResponse>
				{
					let $transactionDate := data($fML32OUT1/ns1:TR_DATA_KSIEG)
					return
						if ($transactionDate) then 
							<ns4:icbsDate>{
								fn:concat(fn:substring($transactionDate, 7, 4),
										  '-',
										  fn:substring($transactionDate, 4, 2),
										  '-',
										  fn:substring($transactionDate, 1, 2))
							}</ns4:icbsDate>
						else
							()
				}

				<ns4:icbsSessionNumber?>{
					data($fML32OUT1/ns1:TR_TXN_SESJA)
				}</ns4:icbsSessionNumber>

				<ns4:psTransactionNumber?>{
					data($fML32OUT1/ns1:TR_TXN_NR)
				}</ns4:psTransactionNumber>

				{
					let $transactionRefNumber := data($fML32OUT1/ns1:TR_NR_REF)
					return
						if ($transactionRefNumber) then 
							<ns4:transactionRefNumber>{
								$transactionRefNumber
							}</ns4:transactionRefNumber> 
	                    else
							()
				}

				(:
				 : brak mapowania dla 'balanceDebit'
				 : brak mapowania dla 'balanceCredit'
				 :)

				{
					let $dateTime := data($fML32OUT1/ns1:TR_CZAS_ODPOWIEDZI)
					return
						if ($dateTime) then 
		                    <ns4:dateTime>{  
								fn:concat(fn:substring($dateTime, 1, 10),
										  'T',
										  fn:substring($dateTime, 12, 15))
								}</ns4:dateTime>
						else
							()
				}

				(:
				 : brak mapowania dla 'beUserId'
				 :)
                         <ns4:beUserId?>{ data($fML32OUT1/ns1:TR_UZYTKOWNIK) }</ns4:beUserId>

				<ns4:beErrorCodeList?>{
					for $FML320 in ($fML32OUT1/ns1:TR_KOD_BLEDU_1
									 union $fML32OUT1/ns1:TR_KOD_BLEDU_2
									 union $fML32OUT1/ns1:TR_KOD_BLEDU_3
									 union $fML32OUT1/ns1:TR_KOD_BLEDU_4
									 union $fML32OUT1/ns1:TR_KOD_BLEDU_5)  
					return
						let $errorCode := data($FML320)
						return
							if ($errorCode ne ''
								and $errorCode ne '000') then
								<ns4:BeErrorCode>
									<ns4:errorCode>
										<ns3:BackendErrorCode>
											<ns3:errorCode>{
												data($FML320)
											}</ns3:errorCode>
										</ns3:BackendErrorCode>
									</ns4:errorCode>
								</ns4:BeErrorCode>
							else
								()
				}</ns4:beErrorCodeList>
			</ns4:BackendResponse>
		</ns0:backendResponse>
	</ns0:invokeResponse>
};

<soap-env:Body>{
	xf:postExchangeCurrencyResponse($fML32OUT1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>