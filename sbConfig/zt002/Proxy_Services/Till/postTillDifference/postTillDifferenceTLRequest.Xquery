<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$header1" element="ns9:header" location="postTillDifference.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns9:invoke" location="postTillDifference.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns9:invokeResponse" location="postTillDifference.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns2 = "urn:cash.operations.entities.be.dcl";
declare namespace ns1 = "urn:acceptance.entities.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns0 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns9 = "urn:be.services.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/postTillDifference/postTillDifferenceTLRequest/";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace err = "urn:errors.hlbsentities.be.dcl";
declare namespace er2 = "http://www.bea.com/wli/sb/context";


declare function xf:postTillDifferenceTLRequest($header1 as element(ns9:header),
    $invoke1 as element(ns9:invoke),
    $invokeResponse1 as element(ns9:invokeResponse),
    $faultResponse as element() ?)
    as element(ns0:transactionLogEntry) {
        <ns0:transactionLogEntry>
            {
                for $businessTransactionType in $invoke1/ns9:transaction/ns7:Transaction/ns7:businessTransactionType/ns6:BusinessTransactionType/ns6:businessTransactionType
                return
                    <businessTransactionType>{ data($businessTransactionType) }</businessTransactionType>
            }    
            <credit>
                {
                    for $amountMa in $invoke1/ns9:transaction/ns7:Transaction/ns7:transactionMa/ns7:TransactionMa/ns7:amountMa
                    return
                        <amount>{ xs:decimal( data($amountMa) ) }</amount>
                }
                {
                    for $currencyCode in $invoke1/ns9:transaction/ns7:Transaction/ns7:transactionMa/ns7:TransactionMa/ns7:currencyCode/ns3:CurrencyCode/ns3:currencyCode
                    return
                        <currencyCode>{ data($currencyCode) }</currencyCode>
                }
            </credit>
            {
                for $csrMessageType in $invoke1/ns9:transaction/ns7:Transaction/ns7:csrMessageType/ns6:CsrMessageType/ns6:csrMessageType
                return
                    <csrMessageType>{ data($csrMessageType) }</csrMessageType>
            }
            <debit>
                {
                    for $amountWn in $invoke1/ns9:transaction/ns7:Transaction/ns7:transactionWn/ns7:TransactionWn/ns7:amountWn
                    return
                        <amount>{ xs:decimal( data($amountWn) ) }</amount>
                }
                {
                    for $currencyCode in $invoke1/ns9:transaction/ns7:Transaction/ns7:transactionWn/ns7:TransactionWn/ns7:currencyCode/ns3:CurrencyCode/ns3:currencyCode
                    return
                        <currencyCode>{ data($currencyCode) }</currencyCode>
                }
            </debit>
            {
                for $dtTransactionType in $invoke1/ns9:transaction/ns7:Transaction/ns7:dtTransactionType/ns6:DtTransactionType/ns6:dtTransactionType
                return
                    <dtTransactionType>{ data($dtTransactionType) }</dtTransactionType>
            }
            <executor>
                <branchNumber>{ xs:int( data($invoke1/ns9:branchCode/ns3:BranchCode/ns3:branchCode) ) }</branchNumber>
                {
                    for $userID in $invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:user/ns5:User/ns5:userID
                    return
                        <executorID>{ data($userID) }</executorID>
                }

                <firstName>{ data($invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:user/ns5:User/ns5:userFirstName) }</firstName>
                <lastName>{ data($invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:user/ns5:User/ns5:userLastName) }</lastName>
                <tellerID>{ data($invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:teller/ns4:Teller/ns4:tellerID) }</tellerID>
                <tillNumber>{ data($invoke1/ns9:userTxnSession/ns4:UserTxnSession/ns4:till/ns4:Till/ns4:tillID) }</tillNumber>
          (:      <userID>{ data($header1/ns9:msgHeader/ns9:userId) }</userID> :)
          {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
                (
                    for $beUserId in $invokeResponse1/ns9:backendResponse/ns7:BackendResponse/ns7:beUserId
                    return
                        <userID>{ data($beUserId) }</userID>
                )
                	else
                	()
             }
            </executor>
            <extendedCSRMessageType>0</extendedCSRMessageType>
            <hlbsName>postTillDifference</hlbsName>
            {
                for $putDownDate in $invoke1/ns9:transaction/ns7:Transaction/ns7:putDownDate
                return
                    <putDownDate>{ data($putDownDate) }</putDownDate>
            }
            <timestamp>{ data($header1/ns9:msgHeader/ns9:timestamp) }</timestamp>
            {
                let $AcceptTask := $invoke1/ns9:acceptTask/ns1:AcceptTask
                return
                    <tlAcceptance>
                        <acceptorFirstName>{ data($AcceptTask/ns1:acceptorFirstName) }</acceptorFirstName>
                        <acceptorLastName>{ data($AcceptTask/ns1:acceptorLastName) }</acceptorLastName>
                        <acceptorSkp>{ data($AcceptTask/ns1:acceptor) }</acceptorSkp>
                        {
                            for $AcceptItem in $AcceptTask/ns1:acceptItemList/ns1:AcceptItem
                            return
                                <tlAcceptanceTitleList>
                                    {
                                        for $acceptItemTitle in $AcceptItem/ns1:acceptItemTitle/ns8:AcceptItemTitle/ns8:acceptItemTitle
                                        return
                                            <acceptItemTitle>{ data($acceptItemTitle) }</acceptItemTitle>
                                    }
                                </tlAcceptanceTitleList>
                        }
                    </tlAcceptance>
            }
            {
                for $AuxiliaryAccount in $invoke1/ns9:auxiliaryAccountList/ns7:AuxiliaryAccount
                return
                    <tlAuxiliaryAccountList>
                        <accountNumber>{ data($AuxiliaryAccount/ns7:accountNumber) }</accountNumber>
                    </tlAuxiliaryAccountList>
            }
          {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                for $BackendResponse in $invokeResponse1/ns9:backendResponse/ns7:BackendResponse
                return
                    <tlBackendResponse>
                        {
                            for $dateTime in $BackendResponse/ns7:dateTime
                            return
                                <dateTime>{ data($dateTime) }</dateTime>
                        }
                        {
                            for $icbsDate in $BackendResponse/ns7:icbsDate
                            return
                                <icbsDate>{ xs:date( data($icbsDate)) }</icbsDate>
                        }
                        {
                            for $icbsSessionNumber in $BackendResponse/ns7:icbsSessionNumber
                            return
                                <icbsSessionNumber>{ data($icbsSessionNumber) }</icbsSessionNumber>
                        }
                        {
                            for $psTransactionNumber in $BackendResponse/ns7:psTransactionNumber
                            return
                                <psTransactionNumber>{ data($psTransactionNumber) }</psTransactionNumber>
                        }

                        {
                            for $BeErrorCode in $BackendResponse/ns7:beErrorCodeList/ns7:BeErrorCode
                            return
                                <tlErrorCodeList>
                                    <errorCode>
                                        <errorCode>{ data($BeErrorCode/ns7:errorCode) }</errorCode>
                                        <systemId>1</systemId>
                                    </errorCode>
                                </tlErrorCodeList>
                        }
                        {
                            for $transactionRefNumber in $BackendResponse/ns7:transactionRefNumber
                            return
                                <transactionRefNumber>{ data($transactionRefNumber) }</transactionRefNumber>
                        }
                    </tlBackendResponse>
                    )
                            
                	else
                	()
             }
            
            {
                let $CurrencyReconciliation := $invoke1/ns9:currencyReconciliation/ns2:CurrencyReconciliation
                return
                    <tlCurrencyReconciliationList>
                        {
                            for $currencyCode in $CurrencyReconciliation/ns2:currency/ns3:CurrencyCode/ns3:currencyCode
                            return
                                <currency>{ data($currencyCode) }</currency>
                        }
                        {
                            for $differenceAmount in $CurrencyReconciliation/ns2:differenceAmount
                            return
                                <differenceAmount>{ xs:decimal( data($differenceAmount) ) }</differenceAmount>
                        }
                        {
                            for $differenceAmountPLN in $CurrencyReconciliation/ns2:differenceAmountPLN
                            return
                                <differenceAmountPLN>{ xs:decimal( data($differenceAmountPLN) ) }</differenceAmountPLN>
                        }
                        {
                            for $differenceCurRate in $CurrencyReconciliation/ns2:differenceCurRate
                            return
                                <differenceCurRate>{ xs:decimal( data($differenceCurRate) ) }</differenceCurRate>
                        }
                    </tlCurrencyReconciliationList>
            }
            {
                for $transactionDate in $invoke1/ns9:transaction/ns7:Transaction/ns7:transactionDate
                return
                    <transactionDate>{ xs:date( data($transactionDate)) }</transactionDate>
            }
            
            <tlExceptionDataList?>
            {
            	let $errorCode1 := data($faultResponse//err:exceptionItem/err:errorCode1)
            	let $errorCode2 := data($faultResponse//err:exceptionItem/err:errorCode2)
                let $errorDescription := data($faultResponse//err:exceptionItem/err:errorDescription)
            	return
            		if (data($errorDescription) != '') then
            		(
		                <errorCode1?>{ xs:int($errorCode1) }</errorCode1>,
        		        <errorCode2?>{ xs:int($errorCode2) }</errorCode2>,
                		<errorDescription?>{ $errorDescription }</errorDescription>
                	)
                	else
                	()
			}
            </tlExceptionDataList>
            
            
            
            
            <transactionGroupID>{ data($invoke1/ns9:transaction/ns7:Transaction/ns7:cashTransactionBasketID) }</transactionGroupID>
            <transactionID>{ data($header1/ns9:transHeader/ns9:transId) }</transactionID>
          {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                for $transactionStatus in $invokeResponse1/ns9:transactionOut/ns7:Transaction/ns7:transactionStatus/ns3:TransactionStatus/ns3:transactionStatus
                return
                    <transactionStatus>{ data($transactionStatus) }</transactionStatus>
                	)
                	else
                	()
            }
            {
                for $title in $invoke1/ns9:transaction/ns7:Transaction/ns7:title
                return
                    <transactionTitle>{ data($title) }</transactionTitle>
            }
        </ns0:transactionLogEntry>
};

declare variable $header1 as element(ns9:header) external;
declare variable $invoke1 as element(ns9:invoke) external;
declare variable $invokeResponse1 as element(ns9:invokeResponse) ? external;
declare variable $faultResponse  as element() ? external;


<soap-env:Body>{
xf:postTillDifferenceTLRequest($header1,
    $invoke1,
    $invokeResponse1,
    $faultResponse)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>