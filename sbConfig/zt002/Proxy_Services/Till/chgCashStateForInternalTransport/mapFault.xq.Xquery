<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Mapowanie faulta zwracanego przez callSaveTransfer oraz z response</con:description>
    <con:xquery><![CDATA[xquery version "1.0";

(:
 : @author	Tomasz Krajewski
 : @version 1.0.1
 : @since	2010-09-03
 :
 : $Proxy Services/Till/chgCashStateForInternalTransport/mapFault.xq$
 :
 :)
  
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Till/chgCashStateForInternalTransport/mapFault/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urne = "urn:errors.hlbsentities.be.dcl";

declare variable $faultResponse as element(soap-env:Body) external;
declare variable $faultMode as xs:string external;
(:
 : $faultMode - parametr określający czy mamy do czynienia z gotówką, czy z nominałami.
 : 'money' - gotówka
 : 'denom' - nominały
:)

declare function xf:mapFault(
$faultResponse as element(soap-env:Body),
$faultMode as xs:string)
as element(FML32) {
	<FML32>
		<FML_ERROR_CODE1?>{
			data($faultResponse//urne:exceptionItem/urne:errorCode1)
		}</FML_ERROR_CODE1>
		<FML_ERROR_CODE2?>{
			data($faultResponse//urne:exceptionItem/urne:errorCode2)
		}</FML_ERROR_CODE2>
		<FML_ERROR_DESCRIPTION?>{
			data($faultResponse//urne:exceptionItem/urne:errorDescription)
		}</FML_ERROR_DESCRIPTION>
	{
		let $TR_OPIS_BLEDU_1 := fn:normalize-space(data($faultResponse/FML32/TR_OPIS_BLEDU_1))
		let $TR_OPIS_BLEDU_2 := fn:normalize-space(data($faultResponse/FML32/TR_OPIS_BLEDU_2))
		let $TR_OPIS_BLEDU_3 := fn:normalize-space(data($faultResponse/FML32/TR_OPIS_BLEDU_3))
		let $TR_OPIS_BLEDU_4 := fn:normalize-space(data($faultResponse/FML32/TR_OPIS_BLEDU_4))
		let $TR_OPIS_BLEDU_5 := fn:normalize-space(data($faultResponse/FML32/TR_OPIS_BLEDU_5))
		let $errorMoney := "Księgowanie gotówki  zwróciło następujące komunikaty o błędach: "
		let $errorDenom := "Księgowanie nominałów  zwróciło następujące komunikaty o błędach: "
		let $errorString := fn:concat("% ", $TR_OPIS_BLEDU_1,
									" % ", $TR_OPIS_BLEDU_2,
									" % ", $TR_OPIS_BLEDU_3,
									" % ", $TR_OPIS_BLEDU_4,
									" % ", $TR_OPIS_BLEDU_5, " %")
		return
				if (fn:string-length($errorString) > 16) then (: 16 to ilość znaków, gdy nie ma żadnego błędu :)
				(
					<FML_ERROR_DESCRIPTION?>{
						if ($faultMode = 'money') then
						(
							fn:concat($errorMoney, fn:replace(fn:replace(fn:replace(fn:replace($errorString, "%  %", "%"), "%  %", "%"), "%  %", "%"), "%  %", "%"))
						)
						else
						(
							fn:concat($errorDenom, fn:replace(fn:replace(fn:replace(fn:replace($errorString, "%  %", "%"), "%  %", "%"), "%  %", "%"), "%  %", "%"))
						)
					}</FML_ERROR_DESCRIPTION>,
					<FML_ERROR_CODE1?>{
						999	
					}</FML_ERROR_CODE1>,
					<FML_ERROR_CODE2?>{
						666
					}</FML_ERROR_CODE2>
				)
				else
				()				
	}
	</FML32>
};

xf:mapFault($faultResponse, $faultMode)]]></con:xquery>
</con:xqueryEntry>