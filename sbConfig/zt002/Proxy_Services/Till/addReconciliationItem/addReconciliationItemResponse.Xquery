<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.3
 : @since   2010-01-05
 :
 : wersja WSDLa: 27-01-2010 09:10:30
 :
 : $Proxy Services/Till/addReconciliationItem/addReconciliationItemRequest$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/addReconciliationItem/addReconciliationItemResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:addReconciliationItemResponse($fML321 as element(ns1:FML32))
    as element(ns2:invokeResponse)
{
	<ns2:invokeResponse>
		<ns2:result>
			<ns0:ResponseMessage>
				<ns0:result>{
					xs:boolean(data($fML321/ns1:NF_RESPOM_RESULT))
          		}</ns0:result>
			</ns0:ResponseMessage>
		</ns2:result>
	</ns2:invokeResponse>
};


	xf:addReconciliationItemResponse($fML321)]]></con:xquery>
</con:xqueryEntry>