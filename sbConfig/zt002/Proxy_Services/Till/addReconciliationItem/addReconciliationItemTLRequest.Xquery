<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="addReconciliationItem.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="addReconciliationItem.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns0:invokeResponse" location="addReconciliationItem.WSDL" ::)
(:: pragma bea:global-element-return element="ns6:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns2 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns4 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns5 = "urn:dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/addReconciliationItem/addReconciliationItemTLRequest/";
declare namespace ns6 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns7 = "urn:entities.be.dcl";

declare function xf:addReconciliationItemTLRequest($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke),
    $invokeResponse1 as element(ns0:invokeResponse))
    as element(ns6:transactionLogEntry) {
        <ns6:transactionLogEntry>
            <businessTransactionType>37</businessTransactionType>
            <executor>
                {
                    for $branchCode in $invoke1/ns0:branchCode/ns5:BranchCode/ns5:branchCode
                    return
                        <branchNumber>{ xs:int( data($branchCode) ) }</branchNumber>
                }
                {
                    for $userID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns7:User/ns7:userID
                    return
                        <executorID>{ data($userID) }</executorID>
                }
                {
                    for $userFirstName in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns7:User/ns7:userFirstName
                    return
                        <firstName>{ data($userFirstName) }</firstName>
                }
                {
                    for $userLastName in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns7:User/ns7:userLastName
                    return
                        <lastName>{ data($userLastName) }</lastName>
                }
                {
                    for $tellerID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID
                    return
                        <tellerID>{ data($tellerID) }</tellerID>
                }
                {
                    for $tillID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID
                    return
                        <tillNumber>{ data($tillID) }</tillNumber>
                }
                <userID>{ data($header1/ns0:msgHeader/ns0:userId) }</userID>
            </executor>
            <extendedCSRMessageType>0</extendedCSRMessageType>
            <hlbsName>addReconciliationItem</hlbsName>
            {
                for $CurrencyCash in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:currencyCashList/ns1:CurrencyCash
                return
                    <tlCurrencyCashList>
                        {
                            for $amount in $CurrencyCash/ns1:amount
                            return
                                <amount>{ xs:decimal( data($amount) ) }</amount>
                        }
                        {
                            for $currencyCode in $CurrencyCash/ns1:currency/ns5:CurrencyCode/ns5:currencyCode
                            return
                                <currency>{ data($currencyCode) }</currency>
                        }
                        {
                            for $DenominationSpecification in $CurrencyCash/ns1:denominationSpecificationList/ns1:DenominationSpecification
                            return
                                <tlDenominationSpecList>
                                    {
                                        for $denominationID in $DenominationSpecification/ns1:denomination/ns2:DenominationDefinition/ns2:denominationID
                                        return
                                            <denomination>{ data($denominationID) }</denomination>
                                    }
                                    {
                                        for $itemsNumber in $DenominationSpecification/ns1:itemsNumber
                                        return
                                            <itemsNumber>{ data($itemsNumber) }</itemsNumber>
                                    }
                                </tlDenominationSpecList>
                        }
                    </tlCurrencyCashList>
            }
            {
                for $CurrencyReconciliation in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation
                return
                    <tlCurrencyReconciliationList>
                        {
                            for $currencyCode in $CurrencyReconciliation/ns1:currency/ns5:CurrencyCode/ns5:currencyCode
                            return
                                <currency>{ data($currencyCode) }</currency>
                        }
                        {
                            for $differenceAmount in $CurrencyReconciliation/ns1:differenceAmount
                            return
                                <differenceAmount>{ xs:decimal( data($differenceAmount) ) }</differenceAmount>
                        }
                        {
                            for $currencyReconcExtStatus in $CurrencyReconciliation/ns1:reconciliationExtStatus/ns2:CurrencyReconcExtStatus/ns2:currencyReconcExtStatus
                            return
                                <reconciliationExtStatus>{ data($currencyReconcExtStatus) }</reconciliationExtStatus>
                        }
                    </tlCurrencyReconciliationList>
            }
            <transactionID>{ data($header1/ns0:transHeader/ns0:transId) }</transactionID>
        </ns6:transactionLogEntry>
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;
declare variable $invokeResponse1 as element(ns0:invokeResponse) external;

<soap-env:Body>{
xf:addReconciliationItemTLRequest($header1,
    $invoke1,
    $invokeResponse1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>