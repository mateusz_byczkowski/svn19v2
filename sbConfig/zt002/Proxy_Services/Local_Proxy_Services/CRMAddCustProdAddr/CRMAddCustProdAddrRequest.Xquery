<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
   let $length:= string-length($sourceString)
   let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
   let $accountNumber :=  substring($sourceString, $startPos)
   return
     $accountNumber
};


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:mapCRMAddCustProdAddrRequest($req as element(m:CRMAddCustProdAddrRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:TrnId)
					then <fml:DC_TRN_ID>{ data($req/m:TrnId) }</fml:DC_TRN_ID>
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then <fml:DC_UZYTKOWNIK>{ concat("SKP:",data($req/m:Uzytkownik)) }</fml:DC_UZYTKOWNIK>
					else ()
			}
			{
				if($req/m:Oddzial)
					then <fml:DC_ODDZIAL>{ data(chkUnitId($req/m:Oddzial)) }</fml:DC_ODDZIAL>
					else ()
			}
			{
				if($req/m:System)
					then <fml:CI_SYSTEM>{ data($req/m:System) }</fml:CI_SYSTEM>
					else ()
			}
			{
				if($req/m:NumerProduktu)
					then <fml:DC_NUMER_PRODUKTU>{ data($req/m:NumerProduktu) }</fml:DC_NUMER_PRODUKTU>
					else ()
			}
			{
				if($req/m:NrRachunku)
					then <fml:DC_NR_RACHUNKU>{ cutStr(data($req/m:NrRachunku),10) }</fml:DC_NR_RACHUNKU>
					else ()
			}
			{
				if($req/m:ImieINazwiskoAlt)
					then <fml:DC_IMIE_I_NAZWISKO_ALT>{ data($req/m:ImieINazwiskoAlt) }</fml:DC_IMIE_I_NAZWISKO_ALT>
					else ()
			}
			{
				if($req/m:ImieINazwiskoAltCD)
					then <fml:DC_IMIE_I_NAZWISKO_ALT_C_D>{ data($req/m:ImieINazwiskoAltCD) }</fml:DC_IMIE_I_NAZWISKO_ALT_C_D>
					else ()
			}
			{
				if($req/m:UlicaAdresAlt)
					then <fml:DC_ULICA_ADRES_ALT>{ data($req/m:UlicaAdresAlt) }</fml:DC_ULICA_ADRES_ALT>
					else ()
			}
			{
				if($req/m:NrPosesLokaluAdresAlt)
					then <fml:DC_NR_POSES_LOKALU_ADRES_ALT>{ data($req/m:NrPosesLokaluAdresAlt) }</fml:DC_NR_POSES_LOKALU_ADRES_ALT>
					else ()
			}
			{
				if($req/m:MiastoAdresAlt)
					then <fml:DC_MIASTO_ADRES_ALT>{ data($req/m:MiastoAdresAlt) }</fml:DC_MIASTO_ADRES_ALT>
					else ()
			}
			{
				if($req/m:WojewodztwoKrajAdresAlt)
					then <fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{ data($req/m:WojewodztwoKrajAdresAlt) }</fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
					else ()
			}
			{
				if($req/m:KodPocztowyAdresAlt)
					then <fml:DC_KOD_POCZTOWY_ADRES_ALT>{ data($req/m:KodPocztowyAdresAlt) }</fml:DC_KOD_POCZTOWY_ADRES_ALT>
					else ()
			}
			{
				if($req/m:KodKraju)
					then <fml:DC_KOD_KRAJU>{ data($req/m:KodKraju) }</fml:DC_KOD_KRAJU>
					else ()
			}
			{
				if($req/m:DataWprowadzenia)
					then <fml:DC_DATA_WPROWADZENIA>{ data($req/m:DataWprowadzenia) }</fml:DC_DATA_WPROWADZENIA>
					else ()
			}
			{
				if($req/m:DataKoncowa)
					then <fml:DC_DATA_KONCOWA>{ data($req/m:DataKoncowa) }</fml:DC_DATA_KONCOWA>
					else ()
			}
			{
				if($req/m:AlternatywnyNrTelefonu)
					then <fml:DC_ALTERNATYWNY_NR_TELEFONU>{ data($req/m:AlternatywnyNrTelefonu) }</fml:DC_ALTERNATYWNY_NR_TELEFONU>
					else ()
			}
			{
				if($req/m:KasowacPrzyWygasnieciu)
					then <fml:DC_KASOWAC_PRZY_WYGASNIECIU>{ data($req/m:KasowacPrzyWygasnieciu) }</fml:DC_KASOWAC_PRZY_WYGASNIECIU>
					else ()
			}
			{
				if($req/m:TypAdresu)
					then <fml:DC_TYP_ADRESU>{ data($req/m:TypAdresu) }</fml:DC_TYP_ADRESU>
					else ()
			}
                        <DC_TYP_ZMIANY>A</DC_TYP_ZMIANY>
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMAddCustProdAddrRequest($body/m:CRMAddCustProdAddrRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>