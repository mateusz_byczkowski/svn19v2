<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMCheckDocumentInMIGResponse($fml as element(fml:FML32))
	as element(m:CRMCheckDocumentInMIGResponse) {
		<m:CRMCheckDocumentInMIGResponse>
			{
				if($fml/fml:CI_STATUS and data($fml/fml:CI_STATUS) = "W")
					then <m:Status>true</m:Status>
					else <m:Status>false</m:Status>
			}
		</m:CRMCheckDocumentInMIGResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMCheckDocumentInMIGResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>