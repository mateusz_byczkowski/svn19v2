<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:cif.entities.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace ns4="urn:executions.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string
{
    if ($parm  =$trueval)
       then "true"
       else "false"
};





declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns3:invokeResponse>
  <ns3:totalExecution>
    <ns4:TotalExecution>
      <ns4:totalExecutionAmount?>{data($parm/NF_TOTALE_TOTALEXECUTIONAM)}</ns4:totalExecutionAmount>
      <ns4:freeAmountToUse?>{data($parm/NF_TOTALE_FREEAMOUNTTOUSE)}</ns4:freeAmountToUse>
      <ns4:freeAmountAvailable?>{sourceValue2Boolean(data($parm/NF_TOTALE_FREEAMOUNTAVAILA),"1")}</ns4:freeAmountAvailable>
      <ns4:freeAmount?>{data($parm/NF_TOTALE_FREEAMOUNT)}</ns4:freeAmount>
      <ns4:incomingBalance?>{data($parm/NF_TOTALE_INCOMINGBALANCE)}</ns4:incomingBalance>
      <ns4:outgoingBalance?>{data($parm/NF_TOTALE_OUTGOINGBALANCE)}</ns4:outgoingBalance>
      <ns4:realizationAmount?>{data($parm/NF_TOTALE_REALIZATIONAMOUN)}</ns4:realizationAmount>
    </ns4:TotalExecution>
  </ns3:totalExecution>
</ns3:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>