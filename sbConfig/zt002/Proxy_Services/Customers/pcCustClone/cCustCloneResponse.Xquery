<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-05-10</con:description>
    <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappcCustCloneResponse($fml as element(fml:FML32))
	as element(m:cCustCloneResponse) {
		&lt;m:cCustCloneResponse/>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mappcCustCloneResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>