<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/CustCopy/ResponseTransform2/";
declare namespace ns0 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns-1 = "urn:baseentities.be.dcl";
declare namespace ns1 = "urn:be.services.dcl";

declare function xf:ResponseTransform2($pcCustCopyResponse1 as element(ns0:pcCustCopyResponse))
    as element(ns1:invokeResponse) {
        <ns1:invokeResponse>
            <ns1:messageHelper>
                {
                    for $Error in $pcCustCopyResponse1/ns0:Error
                    return
                        <ns-1:MessageHelper>
                            {
                                for $NrKom in $Error/ns0:NrKom
                                return
                                    <ns-1:errorCode>{ xs:string( data($NrKom) ) }</ns-1:errorCode>
                            }
                            {
                                for $OpisBledu in $Error/ns0:OpisBledu
                                return
                                    <ns-1:message>{ data($OpisBledu) }</ns-1:message>
                            }
                        </ns-1:MessageHelper>
                }
            </ns1:messageHelper>

        </ns1:invokeResponse>
};

declare variable $pcCustCopyResponse1 as element(ns0:pcCustCopyResponse) external;

xf:ResponseTransform2($pcCustCopyResponse1)]]></con:xquery>
</con:xqueryEntry>