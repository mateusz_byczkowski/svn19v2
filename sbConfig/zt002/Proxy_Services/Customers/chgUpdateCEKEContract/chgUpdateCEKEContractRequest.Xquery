<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl = "urn:be.services.dcl";
declare namespace ns1 = "urn:ceke.entities.be.dcl";
declare namespace ns2 = "urn:cif.entities.be.dcl";

declare function xf:map_chgUpdateCEKEContractRequest($req as element(dcl:invoke),$head as element(dcl:header))
	as element(fml:FML32) {

		<fml:FML32>
			{
				<fml:E_MSHEAD_MSGID>{data($head/dcl:msgHeader/dcl:msgId)}</fml:E_MSHEAD_MSGID>
			}
			{
				<fml:E_LOGIN_ID?>{ data($req/dcl:customerCEKE/ns1:CustomerCEKE/ns1:nik) }</fml:E_LOGIN_ID>
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
<soap-env:Body>
{ xf:map_chgUpdateCEKEContractRequest($body/dcl:invoke,$header/dcl:header) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>