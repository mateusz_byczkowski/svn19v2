<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl = "urn:be.services.dcl";
declare namespace ns1 = "urn:ceke.entities.be.dcl";
declare namespace ns2 = "urn:cif.entities.be.dcl";

declare function xf:map_chgUpdateCEKEContractResponse($fml as element(fml:FML32))
	as element(dcl:invokeResponse) {
		<dcl:invokeResponse>
			<dcl:customerCEKEOut>
				<ns1:CustomerCEKE>
					<ns1:contractVersion?>{data($fml/fml:E_CUST_REPORT_VERSION)}</ns1:contractVersion>
				</ns1:CustomerCEKE>
			</dcl:customerCEKEOut>		
		</dcl:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_chgUpdateCEKEContractResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>