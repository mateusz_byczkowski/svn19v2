<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbICBSAddCustomerRequest($req as element(m:bICBSAddCustomerRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:TrnId)
					then <fml:DC_TRN_ID>{ data($req/m:TrnId) }</fml:DC_TRN_ID>
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then <fml:DC_UZYTKOWNIK>{ data($req/m:Uzytkownik) }</fml:DC_UZYTKOWNIK>
					else ()
			}
			{
				if($req/m:Oddzial)
					then <fml:DC_ODDZIAL>{ data($req/m:Oddzial) }</fml:DC_ODDZIAL>
					else ()
			}
			{
				if($req/m:ImieINazwiskoAlt)
					then <fml:DC_IMIE_I_NAZWISKO_ALT>{ data($req/m:ImieINazwiskoAlt) }</fml:DC_IMIE_I_NAZWISKO_ALT>
					else ()
			}
			{
				if($req/m:ImieINazwiskoAltCD)
					then <fml:DC_IMIE_I_NAZWISKO_ALT_C_D>{ data($req/m:ImieINazwiskoAltCD) }</fml:DC_IMIE_I_NAZWISKO_ALT_C_D>
					else ()
			}
			{
				if($req/m:UlicaAdresAlt)
					then <fml:DC_ULICA_ADRES_ALT>{ data($req/m:UlicaAdresAlt) }</fml:DC_ULICA_ADRES_ALT>
					else ()
			}
			{
				if($req/m:NrPosesLokaluAdresAlt)
					then <fml:DC_NR_POSES_LOKALU_ADRES_ALT>{ data($req/m:NrPosesLokaluAdresAlt) }</fml:DC_NR_POSES_LOKALU_ADRES_ALT>
					else ()
			}
			{
				if($req/m:MiastoAdresAlt)
					then <fml:DC_MIASTO_ADRES_ALT>{ data($req/m:MiastoAdresAlt) }</fml:DC_MIASTO_ADRES_ALT>
					else ()
			}
			{
				if($req/m:WojewodztwoKrajAdresAlt)
					then <fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{ data($req/m:WojewodztwoKrajAdresAlt) }</fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
					else ()
			}
			{
				if($req/m:KodPocztowyAdresAlt)
					then <fml:DC_KOD_POCZTOWY_ADRES_ALT>{ data($req/m:KodPocztowyAdresAlt) }</fml:DC_KOD_POCZTOWY_ADRES_ALT>
					else ()
			}
			{
				if($req/m:DataWprowadzenia)
					then <fml:DC_DATA_WPROWADZENIA>{ data($req/m:DataWprowadzenia) }</fml:DC_DATA_WPROWADZENIA>
					else ()
			}
			{
				if($req/m:DataKoncowa)
					then <fml:DC_DATA_KONCOWA>{ data($req/m:DataKoncowa) }</fml:DC_DATA_KONCOWA>
					else ()
			}
			{
				if($req/m:KasowacPrzyWygasnieciu)
					then <fml:DC_KASOWAC_PRZY_WYGASNIECIU>{ data($req/m:KasowacPrzyWygasnieciu) }</fml:DC_KASOWAC_PRZY_WYGASNIECIU>
					else ()
			}
			{
				if($req/m:AlternatywnyNrTelefonu)
					then <fml:DC_ALTERNATYWNY_NR_TELEFONU>{ data($req/m:AlternatywnyNrTelefonu) }</fml:DC_ALTERNATYWNY_NR_TELEFONU>
					else ()
			}
			{
				if($req/m:TypAdresu)
					then <fml:DC_TYP_ADRESU>{ data($req/m:TypAdresu) }</fml:DC_TYP_ADRESU>
					else ()
			}
			{
				if($req/m:DataBankructwa)
					then <fml:DC_DATA_BANKRUCTWA>{ data($req/m:DataBankructwa) }</fml:DC_DATA_BANKRUCTWA>
					else ()
			}
			{
				if($req/m:DataKlasyfikacjiKredytu)
					then <fml:DC_DATA_KLASYFIKACJI_KREDYTU>{ data($req/m:DataKlasyfikacjiKredytu) }</fml:DC_DATA_KLASYFIKACJI_KREDYTU>
					else ()
			}
			{
				if($req/m:DataWprowadzeniaDanych)
					then <fml:DC_DATA_WPROWADZENIA_DANYCH>{ data($req/m:DataWprowadzeniaDanych) }</fml:DC_DATA_WPROWADZENIA_DANYCH>
					else ()
			}
			{
				if($req/m:CzestPrzegladuMCe)
					then <fml:DC_CZEST_PRZEGLADU_M_CE>{ data($req/m:CzestPrzegladuMCe) }</fml:DC_CZEST_PRZEGLADU_M_CE>
					else ()
			}
			{
				if($req/m:DataNastepnejWeryfikacji)
					then <fml:DC_DATA_NASTEPNEJ_WERYFIKACJI>{ data($req/m:DataNastepnejWeryfikacji) }</fml:DC_DATA_NASTEPNEJ_WERYFIKACJI>
					else ()
			}
			{
				if($req/m:DomMieszkanieWlasnosc)
					then <fml:DC_DOM_MIESZKANIE_WLASNOSC>{ data($req/m:DomMieszkanieWlasnosc) }</fml:DC_DOM_MIESZKANIE_WLASNOSC>
					else ()
			}
			{
				if($req/m:InneNieruchomosci)
					then <fml:DC_INNE_NIERUCHOMOSCI>{ data($req/m:InneNieruchomosci) }</fml:DC_INNE_NIERUCHOMOSCI>
					else ()
			}
			{
				if($req/m:Inne)
					then <fml:DC_INNE>{ data($req/m:Inne) }</fml:DC_INNE>
					else ()
			}
			{
				if($req/m:SrodkiNaRachBiez)
					then <fml:DC_SRODKI_NA_RACH_BIEZ>{ data($req/m:SrodkiNaRachBiez) }</fml:DC_SRODKI_NA_RACH_BIEZ>
					else ()
			}
			{
				if($req/m:SrodkiNaRachTermin)
					then <fml:DC_SRODKI_NA_RACH_TERMIN>{ data($req/m:SrodkiNaRachTermin) }</fml:DC_SRODKI_NA_RACH_TERMIN>
					else ()
			}
			{
				if($req/m:PozostaleAktywa)
					then <fml:DC_POZOSTALE_AKTYWA>{ data($req/m:PozostaleAktywa) }</fml:DC_POZOSTALE_AKTYWA>
					else ()
			}
			{
				if($req/m:UdzialyWSpolkach)
					then <fml:DC_UDZIALY_W_SPOLKACH>{ data($req/m:UdzialyWSpolkach) }</fml:DC_UDZIALY_W_SPOLKACH>
					else ()
			}
			{
				if($req/m:PozostaleKredyty)
					then <fml:DC_POZOSTALE_KREDYTY>{ data($req/m:PozostaleKredyty) }</fml:DC_POZOSTALE_KREDYTY>
					else ()
			}
			{
				if($req/m:UdzielonePoreczenia)
					then <fml:DC_UDZIELONE_PORECZENIA>{ data($req/m:UdzielonePoreczenia) }</fml:DC_UDZIELONE_PORECZENIA>
					else ()
			}
			{
				if($req/m:PapieryWartosciowe)
					then <fml:DC_PAPIERY_WARTOSCIOWE>{ data($req/m:PapieryWartosciowe) }</fml:DC_PAPIERY_WARTOSCIOWE>
					else ()
			}
			{
				if($req/m:MiesDochodNettoWPln)
					then <fml:DC_MIES_DOCHOD_NETTO_W_PLN>{ data($req/m:MiesDochodNettoWPln) }</fml:DC_MIES_DOCHOD_NETTO_W_PLN>
					else ()
			}
			{
				if($req/m:WielkoscLiniiKredytWPl)
					then <fml:DC_WIELKOSC_LINII_KREDYT_W_PL>{ data($req/m:WielkoscLiniiKredytWPl) }</fml:DC_WIELKOSC_LINII_KREDYT_W_PL>
					else ()
			}
			{
				if($req/m:WykonywanaPraca)
					then <fml:DC_WYKONYWANA_PRACA>{ data($req/m:WykonywanaPraca) }</fml:DC_WYKONYWANA_PRACA>
					else ()
			}
			{
				if($req/m:NazwaPracodawcy)
					then <fml:DC_NAZWA_PRACODAWCY>{ data($req/m:NazwaPracodawcy) }</fml:DC_NAZWA_PRACODAWCY>
					else ()
			}
			{
				if($req/m:NazwaPracodawcyCd)
					then <fml:DC_NAZWA_PRACODAWCY_CD>{ data($req/m:NazwaPracodawcyCd) }</fml:DC_NAZWA_PRACODAWCY_CD>
					else ()
			}
			{
				if($req/m:UlicaAdresDaneOZatrud)
					then <fml:DC_ULICA_ADRES_DANE_O_ZATRUD>{ data($req/m:UlicaAdresDaneOZatrud) }</fml:DC_ULICA_ADRES_DANE_O_ZATRUD>
					else ()
			}
			{
				if($req/m:NrPosesLokaluDaneZatru)
					then <fml:DC_NR_POSES_LOKALU_DANE_ZATRU>{ data($req/m:NrPosesLokaluDaneZatru) }</fml:DC_NR_POSES_LOKALU_DANE_ZATRU>
					else ()
			}
			{
				if($req/m:MiastoDaneOZatrudnieniu)
					then <fml:DC_MIASTO_DANE_O_ZATRUDNIENIU>{ data($req/m:MiastoDaneOZatrudnieniu) }</fml:DC_MIASTO_DANE_O_ZATRUDNIENIU>
					else ()
			}
			{
				if($req/m:WojKrajDaneOZatru)
					then <fml:DC_WOJ_KRAJ_DANE_O_ZATRU>{ data($req/m:WojKrajDaneOZatru) }</fml:DC_WOJ_KRAJ_DANE_O_ZATRU>
					else ()
			}
			{
				if($req/m:KodPocztowyDaneOZatrud)
					then <fml:DC_KOD_POCZTOWY_DANE_O_ZATRUD>{ data($req/m:KodPocztowyDaneOZatrud) }</fml:DC_KOD_POCZTOWY_DANE_O_ZATRUD>
					else ()
			}
			{
				if($req/m:RodzajKoduDaneOZatrud)
					then <fml:DC_RODZAJ_KODU_DANE_O_ZATRUD>{ data($req/m:RodzajKoduDaneOZatrud) }</fml:DC_RODZAJ_KODU_DANE_O_ZATRUD>
					else ()
			}
			{
				if($req/m:KodEkdPracodaw)
					then <fml:DC_KOD_EKD_PRACODAW>{ data($req/m:KodEkdPracodaw) }</fml:DC_KOD_EKD_PRACODAW>
					else ()
			}
			{
				if($req/m:DataPodjeciaPracy)
					then <fml:DC_DATA_PODJECIA_PRACY>{ data($req/m:DataPodjeciaPracy) }</fml:DC_DATA_PODJECIA_PRACY>
					else ()
			}
			{
				if($req/m:AdresEMailPraca)
					then <fml:DC_ADRES_E_MAIL_PRACA>{ data($req/m:AdresEMailPraca) }</fml:DC_ADRES_E_MAIL_PRACA>
					else ()
			}
			{
				if($req/m:NumerTelefonuPraca)
					then <fml:DC_NUMER_TELEFONU_PRACA>{ data($req/m:NumerTelefonuPraca) }</fml:DC_NUMER_TELEFONU_PRACA>
					else ()
			}
			{
				if($req/m:NrWewTelefonuPraca)
					then <fml:DC_NR_WEW_TELEFONU_PRACA>{ data($req/m:NrWewTelefonuPraca) }</fml:DC_NR_WEW_TELEFONU_PRACA>
					else ()
			}
			{
				if($req/m:DostepnoscTelWPracy)
					then <fml:DC_DOSTEPNOSC_TEL_W_PRACY>{ data($req/m:DostepnoscTelWPracy) }</fml:DC_DOSTEPNOSC_TEL_W_PRACY>
					else ()
			}
			{
				if($req/m:NumerFaksuZaklPracy)
					then <fml:DC_NUMER_FAKSU_ZAKL_PRACY>{ data($req/m:NumerFaksuZaklPracy) }</fml:DC_NUMER_FAKSU_ZAKL_PRACY>
					else ()
			}
			{
				if($req/m:NrNaLisciePlac)
					then <fml:DC_NR_NA_LISCIE_PLAC>{ data($req/m:NrNaLisciePlac) }</fml:DC_NR_NA_LISCIE_PLAC>
					else ()
			}
			{
				if($req/m:NazwaSkrocona)
					then <fml:DC_NAZWA_SKROCONA>{ data($req/m:NazwaSkrocona) }</fml:DC_NAZWA_SKROCONA>
					else ()
			}
			{
				if($req/m:Tytul)
					then <fml:DC_TYTUL>{ data($req/m:Tytul) }</fml:DC_TYTUL>
					else ()
			}
			{
				if($req/m:Imie)
					then <fml:DC_IMIE>{ data($req/m:Imie) }</fml:DC_IMIE>
					else ()
			}
			{
				if($req/m:DrugieImie)
					then <fml:DC_DRUGIE_IMIE>{ data($req/m:DrugieImie) }</fml:DC_DRUGIE_IMIE>
					else ()
			}
			{
				if($req/m:Nazwisko)
					then <fml:DC_NAZWISKO>{ data($req/m:Nazwisko) }</fml:DC_NAZWISKO>
					else ()
			}
			{
				if($req/m:TytulCD)
					then <fml:DC_TYTUL_C_D>{ data($req/m:TytulCD) }</fml:DC_TYTUL_C_D>
					else ()
			}
			{
				if($req/m:NazwiskoCD)
					then <fml:DC_NAZWISKO_C_D>{ data($req/m:NazwiskoCD) }</fml:DC_NAZWISKO_C_D>
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then <fml:DC_ULICA_DANE_PODST>{ data($req/m:UlicaDanePodst) }</fml:DC_ULICA_DANE_PODST>
					else ()
			}
			{
				if($req/m:NrPosesLokaluDanePodst)
					then <fml:DC_NR_POSES_LOKALU_DANE_PODST>{ data($req/m:NrPosesLokaluDanePodst) }</fml:DC_NR_POSES_LOKALU_DANE_PODST>
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then <fml:DC_MIASTO_DANE_PODST>{ data($req/m:MiastoDanePodst) }</fml:DC_MIASTO_DANE_PODST>
					else ()
			}
			{
				if($req/m:WojKrajDanePodst)
					then <fml:DC_WOJ_KRAJ_DANE_PODST>{ data($req/m:WojKrajDanePodst) }</fml:DC_WOJ_KRAJ_DANE_PODST>
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then <fml:DC_KOD_POCZTOWY_DANE_PODST>{ data($req/m:KodPocztowyDanePodst) }</fml:DC_KOD_POCZTOWY_DANE_PODST>
					else ()
			}
			{
				if($req/m:AdresOd)
					then <fml:DC_ADRES_OD>{ data($req/m:AdresOd) }</fml:DC_ADRES_OD>
					else ()
			}
			{
				if($req/m:DataPotwierdz)
					then <fml:DC_DATA_POTWIERDZ>{ data($req/m:DataPotwierdz) }</fml:DC_DATA_POTWIERDZ>
					else ()
			}
			{
				if($req/m:ZrodloDanych)
					then <fml:DC_ZRODLO_DANYCH>{ data($req/m:ZrodloDanych) }</fml:DC_ZRODLO_DANYCH>
					else ()
			}
			{
				if($req/m:DochodBrTysRok)
					then <fml:DC_DOCHOD_BR_TYS_ROK>{ data($req/m:DochodBrTysRok) }</fml:DC_DOCHOD_BR_TYS_ROK>
					else ()
			}
			{
				if($req/m:KodZrodlaDochodu)
					then <fml:DC_KOD_ZRODLA_DOCHODU>{ data($req/m:KodZrodlaDochodu) }</fml:DC_KOD_ZRODLA_DOCHODU>
					else ()
			}
			{
				if($req/m:LiczbaLatPracyOgolem)
					then <fml:DC_LICZBA_LAT_PRACY_OGOLEM>{ data($req/m:LiczbaLatPracyOgolem) }</fml:DC_LICZBA_LAT_PRACY_OGOLEM>
					else ()
			}
			{
				if($req/m:KodZawodu)
					then <fml:DC_KOD_ZAWODU>{ data($req/m:KodZawodu) }</fml:DC_KOD_ZAWODU>
					else ()
			}
			{
				if($req/m:StatusPracownika)
					then <fml:DC_STATUS_PRACOWNIKA>{ data($req/m:StatusPracownika) }</fml:DC_STATUS_PRACOWNIKA>
					else ()
			}
			{
				if($req/m:NrPesel)
					then <fml:DC_NR_PESEL>{ data($req/m:NrPesel) }</fml:DC_NR_PESEL>
					else ()
			}
			{
				if($req/m:Nip)
					then <fml:DC_NIP>{ data($req/m:Nip) }</fml:DC_NIP>
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then <fml:DC_NR_DOWODU_REGON>{ data($req/m:NrDowoduRegon) }</fml:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($req/m:JednostkaKorporacyjna)
					then <fml:DC_JEDNOSTKA_KORPORACYJNA>{ data($req/m:JednostkaKorporacyjna) }</fml:DC_JEDNOSTKA_KORPORACYJNA>
					else ()
			}
			{
				if($req/m:SluzbaWojskowa)
					then <fml:DC_SLUZBA_WOJSKOWA>{ data($req/m:SluzbaWojskowa) }</fml:DC_SLUZBA_WOJSKOWA>
					else ()
			}
			{
				if($req/m:Zainteresowania)
					then <fml:DC_ZAINTERESOWANIA>{ data($req/m:Zainteresowania) }</fml:DC_ZAINTERESOWANIA>
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then <fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }</fml:DC_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:Urzednik1)
					then <fml:DC_URZEDNIK_1>{ data($req/m:Urzednik1) }</fml:DC_URZEDNIK_1>
					else ()
			}
			{
				if($req/m:Urzednik2)
					then <fml:DC_URZEDNIK_2>{ data($req/m:Urzednik2) }</fml:DC_URZEDNIK_2>
					else ()
			}
			{
				if($req/m:Urzednik3)
					then <fml:DC_URZEDNIK_3>{ data($req/m:Urzednik3) }</fml:DC_URZEDNIK_3>
					else ()
			}
			{
				if($req/m:NazwiskoOsobyKontaktowej)
					then <fml:DC_NAZWISKO_OSOBY_KONTAKTOWEJ>{ data($req/m:NazwiskoOsobyKontaktowej) }</fml:DC_NAZWISKO_OSOBY_KONTAKTOWEJ>
					else ()
			}
			{
				if($req/m:TytulOsobyKontaktowej)
					then <fml:DC_TYTUL_OSOBY_KONTAKTOWEJ>{ data($req/m:TytulOsobyKontaktowej) }</fml:DC_TYTUL_OSOBY_KONTAKTOWEJ>
					else ()
			}
			{
				if($req/m:NrKierTelefonu)
					then <fml:DC_NR_KIER_TELEFONU>{ data($req/m:NrKierTelefonu) }</fml:DC_NR_KIER_TELEFONU>
					else ()
			}
			{
				if($req/m:DostepnoscTel)
					then <fml:DC_DOSTEPNOSC_TEL>{ data($req/m:DostepnoscTel) }</fml:DC_DOSTEPNOSC_TEL>
					else ()
			}
			{
				if($req/m:NrTelefonu)
					then <fml:DC_NR_TELEFONU>{ data($req/m:NrTelefonu) }</fml:DC_NR_TELEFONU>
					else ()
			}
			{
				if($req/m:NumerWewnTelefonu)
					then <fml:DC_NUMER_WEWN_TELEFONU>{ data($req/m:NumerWewnTelefonu) }</fml:DC_NUMER_WEWN_TELEFONU>
					else ()
			}
			{
				if($req/m:DataOtwarcia)
					then <fml:DC_DATA_OTWARCIA>{ data($req/m:DataOtwarcia) }</fml:DC_DATA_OTWARCIA>
					else ()
			}
			{
				if($req/m:NumerFaksu)
					then <fml:DC_NUMER_FAKSU>{ data($req/m:NumerFaksu) }</fml:DC_NUMER_FAKSU>
					else ()
			}
			{
				if($req/m:KodJezyka)
					then <fml:DC_KOD_JEZYKA>{ data($req/m:KodJezyka) }</fml:DC_KOD_JEZYKA>
					else ()
			}
			{
				if($req/m:AdresEMail)
					then <fml:DC_ADRES_E_MAIL>{ data($req/m:AdresEMail) }</fml:DC_ADRES_E_MAIL>
					else ()
			}
			{
				if($req/m:Spw)
					then <fml:DC_SPW>{ data($req/m:Spw) }</fml:DC_SPW>
					else ()
			}
			{
				if($req/m:Korespondencja)
					then <fml:DC_KORESPONDENCJA>{ data($req/m:Korespondencja) }</fml:DC_KORESPONDENCJA>
					else ()
			}
			{
				if($req/m:KodKraju)
					then <fml:DC_KOD_KRAJU>{ data($req/m:KodKraju) }</fml:DC_KOD_KRAJU>
					else ()
			}
			{
				if($req/m:ZgodaNaPrzetw)
					then <fml:DC_ZGODA_NA_PRZETW>{ data($req/m:ZgodaNaPrzetw) }</fml:DC_ZGODA_NA_PRZETW>
					else ()
			}
			{
				if($req/m:PrefKontakt)
					then <fml:DC_PREF_KONTAKT>{ data($req/m:PrefKontakt) }</fml:DC_PREF_KONTAKT>
					else ()
			}
			{
				if($req/m:CharakterWyksztalc)
					then <fml:DC_CHARAKTER_WYKSZTALC>{ data($req/m:CharakterWyksztalc) }</fml:DC_CHARAKTER_WYKSZTALC>
					else ()
			}
			{
				if($req/m:RezNierez)
					then <fml:DC_REZ_NIEREZ>{ data($req/m:RezNierez) }</fml:DC_REZ_NIEREZ>
					else ()
			}
			{
				if($req/m:Pakiet)
					then <fml:DC_PAKIET>{ data($req/m:Pakiet) }</fml:DC_PAKIET>
					else ()
			}
			{
				if($req/m:RodzajZachowania)
					then <fml:DC_RODZAJ_ZACHOWANIA>{ data($req/m:RodzajZachowania) }</fml:DC_RODZAJ_ZACHOWANIA>
					else ()
			}
			{
				if($req/m:EtapZycia)
					then <fml:DC_ETAP_ZYCIA>{ data($req/m:EtapZycia) }</fml:DC_ETAP_ZYCIA>
					else ()
			}
			{
				if($req/m:Odpowiedzi)
					then <fml:DC_ODPOWIEDZI>{ data($req/m:Odpowiedzi) }</fml:DC_ODPOWIEDZI>
					else ()
			}
			{
				if($req/m:DataUrodzenia)
					then <fml:DC_DATA_URODZENIA>{ data($req/m:DataUrodzenia) }</fml:DC_DATA_URODZENIA>
					else ()
			}
			{
				if($req/m:Plec)
					then <fml:DC_PLEC>{ data($req/m:Plec) }</fml:DC_PLEC>
					else ()
			}
			{
				if($req/m:StanCywilny)
					then <fml:DC_STAN_CYWILNY>{ data($req/m:StanCywilny) }</fml:DC_STAN_CYWILNY>
					else ()
			}
			{
				if($req/m:PosiadaDomMieszk)
					then <fml:DC_POSIADA_DOM_MIESZK>{ data($req/m:PosiadaDomMieszk) }</fml:DC_POSIADA_DOM_MIESZK>
					else ()
			}
			{
				if($req/m:WarunkiMieszkaniowe)
					then <fml:DC_WARUNKI_MIESZKANIOWE>{ data($req/m:WarunkiMieszkaniowe) }</fml:DC_WARUNKI_MIESZKANIOWE>
					else ()
			}
			{
				if($req/m:LiczbaOsobNaUtrz)
					then <fml:DC_LICZBA_OSOB_NA_UTRZ>{ data($req/m:LiczbaOsobNaUtrz) }</fml:DC_LICZBA_OSOB_NA_UTRZ>
					else ()
			}
			{
				if($req/m:SegmentMarketingowy)
					then <fml:DC_SEGMENT_MARKETINGOWY>{ data($req/m:SegmentMarketingowy) }</fml:DC_SEGMENT_MARKETINGOWY>
					else ()
			}
			{
				if($req/m:PracownikBanku)
					then <fml:DC_PRACOWNIK_BANKU>{ data($req/m:PracownikBanku) }</fml:DC_PRACOWNIK_BANKU>
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then <fml:DC_NUMER_PASZPORTU>{ data($req/m:NumerPaszportu) }</fml:DC_NUMER_PASZPORTU>
					else ()
			}
			{
				if($req/m:NrTelefKomorkowego)
					then <fml:DC_NR_TELEF_KOMORKOWEGO>{ data($req/m:NrTelefKomorkowego) }</fml:DC_NR_TELEF_KOMORKOWEGO>
					else ()
			}
			{
				if($req/m:DataSmierci)
					then <fml:DC_DATA_SMIERCI>{ data($req/m:DataSmierci) }</fml:DC_DATA_SMIERCI>
					else ()
			}
			{
				if($req/m:LiczbaZatrudnionych)
					then <fml:DC_LICZBA_ZATRUDNIONYCH>{ data($req/m:LiczbaZatrudnionych) }</fml:DC_LICZBA_ZATRUDNIONYCH>
					else ()
			}
			{
				if($req/m:WspolnotaMajatk)
					then <fml:DC_WSPOLNOTA_MAJATK>{ data($req/m:WspolnotaMajatk) }</fml:DC_WSPOLNOTA_MAJATK>
					else ()
			}
			{
				if($req/m:MiejsceZatrudnienia)
					then <fml:DC_MIEJSCE_ZATRUDNIENIA>{ data($req/m:MiejsceZatrudnienia) }</fml:DC_MIEJSCE_ZATRUDNIENIA>
					else ()
			}
			{
				if($req/m:DataWpisaniaNipU)
					then <fml:DC_DATA_WPISANIA_NIP_U>{ data($req/m:DataWpisaniaNipU) }</fml:DC_DATA_WPISANIA_NIP_U>
					else ()
			}
			{
				if($req/m:NrTeleksu)
					then <fml:DC_NR_TELEKSU>{ data($req/m:NrTeleksu) }</fml:DC_NR_TELEKSU>
					else ()
			}
			{
				if($req/m:TypSzkoly)
					then <fml:DC_TYP_SZKOLY>{ data($req/m:TypSzkoly) }</fml:DC_TYP_SZKOLY>
					else ()
			}
			{
				if($req/m:Powitanie)
					then <fml:DC_POWITANIE>{ data($req/m:Powitanie) }</fml:DC_POWITANIE>
					else ()
			}
			{
				if($req/m:ZnKomplDokum)
					then <fml:DC_ZN_KOMPL_DOKUM>{ data($req/m:ZnKomplDokum) }</fml:DC_ZN_KOMPL_DOKUM>
					else ()
			}
			{
				if($req/m:PlanowDataUkonSzk)
					then <fml:DC_PLANOW_DATA_UKON_SZK>{ data($req/m:PlanowDataUkonSzk) }</fml:DC_PLANOW_DATA_UKON_SZK>
					else ()
			}
			{
				if($req/m:Ekd)
					then <fml:DC_EKD>{ data($req/m:Ekd) }</fml:DC_EKD>
					else ()
			}
			{
				if($req/m:Wyksztalcenie)
					then <fml:DC_WYKSZTALCENIE>{ data($req/m:Wyksztalcenie) }</fml:DC_WYKSZTALCENIE>
					else ()
			}
			{
				if($req/m:DinersClub)
					then <fml:DC_DINERS_CLUB>{ data($req/m:DinersClub) }</fml:DC_DINERS_CLUB>
					else ()
			}
			{
				if($req/m:PosiadaDinersClub)
					then <fml:DC_POSIADA_DINERS_CLUB>{ data($req/m:PosiadaDinersClub) }</fml:DC_POSIADA_DINERS_CLUB>
					else ()
			}
			{
				if($req/m:DataWaznosciDinersClub)
					then <fml:DC_DATA_WAZNOSCI_DINERS_CLUB>{ data($req/m:DataWaznosciDinersClub) }</fml:DC_DATA_WAZNOSCI_DINERS_CLUB>
					else ()
			}
			{
				if($req/m:Mastercard)
					then <fml:DC_MASTERCARD>{ data($req/m:Mastercard) }</fml:DC_MASTERCARD>
					else ()
			}
			{
				if($req/m:PosiadaMastercard)
					then <fml:DC_POSIADA_MASTERCARD>{ data($req/m:PosiadaMastercard) }</fml:DC_POSIADA_MASTERCARD>
					else ()
			}
			{
				if($req/m:DataWaznosciMastercard)
					then <fml:DC_DATA_WAZNOSCI_MASTERCARD>{ data($req/m:DataWaznosciMastercard) }</fml:DC_DATA_WAZNOSCI_MASTERCARD>
					else ()
			}
			{
				if($req/m:KodVisa)
					then <fml:DC_KOD_VISA>{ data($req/m:KodVisa) }</fml:DC_KOD_VISA>
					else ()
			}
			{
				if($req/m:PosiadaVisa)
					then <fml:DC_POSIADA_VISA>{ data($req/m:PosiadaVisa) }</fml:DC_POSIADA_VISA>
					else ()
			}
			{
				if($req/m:DataWaznosciVisa)
					then <fml:DC_DATA_WAZNOSCI_VISA>{ data($req/m:DataWaznosciVisa) }</fml:DC_DATA_WAZNOSCI_VISA>
					else ()
			}
			{
				if($req/m:KartaAtm)
					then <fml:DC_KARTA_ATM>{ data($req/m:KartaAtm) }</fml:DC_KARTA_ATM>
					else ()
			}
			{
				if($req/m:PosiadaAtm)
					then <fml:DC_POSIADA_ATM>{ data($req/m:PosiadaAtm) }</fml:DC_POSIADA_ATM>
					else ()
			}
			{
				if($req/m:DataWaznosciAtm)
					then <fml:DC_DATA_WAZNOSCI_ATM>{ data($req/m:DataWaznosciAtm) }</fml:DC_DATA_WAZNOSCI_ATM>
					else ()
			}
			{
				if($req/m:NazwiskoPanienskieMatki)
					then <fml:DC_NAZWISKO_PANIENSKIE_MATKI>{ data($req/m:NazwiskoPanienskieMatki) }</fml:DC_NAZWISKO_PANIENSKIE_MATKI>
					else ()
			}
			{
				if($req/m:UbezpieczenieNaZycie)
					then <fml:DC_UBEZPIECZENIE_NA_ZYCIE>{ data($req/m:UbezpieczenieNaZycie) }</fml:DC_UBEZPIECZENIE_NA_ZYCIE>
					else ()
			}
			{
				if($req/m:UbezpOdNastepstwNw)
					then <fml:DC_UBEZP_OD_NASTEPSTW_NW>{ data($req/m:UbezpOdNastepstwNw) }</fml:DC_UBEZP_OD_NASTEPSTW_NW>
					else ()
			}
			{
				if($req/m:UbezpieczenieEmerytalne)
					then <fml:DC_UBEZPIECZENIE_EMERYTALNE>{ data($req/m:UbezpieczenieEmerytalne) }</fml:DC_UBEZPIECZENIE_EMERYTALNE>
					else ()
			}
			{
				if($req/m:UbezpMajatkoweMies)
					then <fml:DC_UBEZP_MAJATKOWE_MIES>{ data($req/m:UbezpMajatkoweMies) }</fml:DC_UBEZP_MAJATKOWE_MIES>
					else ()
			}
			{
				if($req/m:UbezpieczeniaInne)
					then <fml:DC_UBEZPIECZENIA_INNE>{ data($req/m:UbezpieczeniaInne) }</fml:DC_UBEZPIECZENIA_INNE>
					else ()
			}
			{
				if($req/m:RokStudiow)
					then <fml:DC_ROK_STUDIOW>{ data($req/m:RokStudiow) }</fml:DC_ROK_STUDIOW>
					else ()
			}
			{
				if($req/m:KierunekStudiow)
					then <fml:DC_KIERUNEK_STUDIOW>{ data($req/m:KierunekStudiow) }</fml:DC_KIERUNEK_STUDIOW>
					else ()
			}
			{
				if($req/m:Specjalizacja)
					then <fml:DC_SPECJALIZACJA>{ data($req/m:Specjalizacja) }</fml:DC_SPECJALIZACJA>
					else ()
			}
			{
				if($req/m:PoreczenieBgk)
					then <fml:DC_PORECZENIE_BGK>{ data($req/m:PoreczenieBgk) }</fml:DC_PORECZENIE_BGK>
					else ()
			}
			{
				if($req/m:PolitykaDewizowa)
					then <fml:DC_POLITYKA_DEWIZOWA>{ data($req/m:PolitykaDewizowa) }</fml:DC_POLITYKA_DEWIZOWA>
					else ()
			}
			{
				if($req/m:Konwersja3)
					then <fml:DC_KONWERSJA_3>{ data($req/m:Konwersja3) }</fml:DC_KONWERSJA_3>
					else ()
			}
			{
				if($req/m:PodpisanaUmowaLimitWKo)
					then <fml:DC_PODPISANA_UMOWA_LIMIT_W_KO>{ data($req/m:PodpisanaUmowaLimitWKo) }</fml:DC_PODPISANA_UMOWA_LIMIT_W_KO>
					else ()
			}
			{
				if($req/m:Uczelnia)
					then <fml:DC_UCZELNIA>{ data($req/m:Uczelnia) }</fml:DC_UCZELNIA>
					else ()
			}
			{
				if($req/m:ImieOjca)
					then <fml:DC_IMIE_OJCA>{ data($req/m:ImieOjca) }</fml:DC_IMIE_OJCA>
					else ()
			}
			{
				if($req/m:SposobOdbioruDeklaracji)
					then <fml:DC_SPOSOB_ODBIORU_DEKLARACJI>{ data($req/m:SposobOdbioruDeklaracji) }</fml:DC_SPOSOB_ODBIORU_DEKLARACJI>
					else ()
			}
			{
				if($req/m:NumerPosrednikaSklepu)
					then <fml:DC_NUMER_POSREDNIKA_SKLEPU>{ data($req/m:NumerPosrednikaSklepu) }</fml:DC_NUMER_POSREDNIKA_SKLEPU>
					else ()
			}
			{
				if($req/m:ProwizjaPosrednika)
					then <fml:DC_PROWIZJA_POSREDNIKA>{ data($req/m:ProwizjaPosrednika) }</fml:DC_PROWIZJA_POSREDNIKA>
					else ()
			}
			{
				if($req/m:OdnawiacLimit)
					then <fml:DC_ODNAWIAC_LIMIT>{ data($req/m:OdnawiacLimit) }</fml:DC_ODNAWIAC_LIMIT>
					else ()
			}
			{
				if($req/m:KartaWzorowPodpisow)
					then <fml:DC_KARTA_WZOROW_PODPISOW>{ data($req/m:KartaWzorowPodpisow) }</fml:DC_KARTA_WZOROW_PODPISOW>
					else ()
			}
			{
				if($req/m:ZapisNaWypadekSmierci)
					then <fml:DC_ZAPIS_NA_WYPADEK_SMIERCI>{ data($req/m:ZapisNaWypadekSmierci) }</fml:DC_ZAPIS_NA_WYPADEK_SMIERCI>
					else ()
			}
			{
				if($req/m:KlasyfikacjaWewBzwbk)
					then <fml:DC_KLASYFIKACJA_WEW_BZWBK>{ data($req/m:KlasyfikacjaWewBzwbk) }</fml:DC_KLASYFIKACJA_WEW_BZWBK>
					else ()
			}
			{
				if($req/m:MiejsceUrodzenia)
					then <fml:DC_MIEJSCE_URODZENIA>{ data($req/m:MiejsceUrodzenia) }</fml:DC_MIEJSCE_URODZENIA>
					else ()
			}
			{
				if($req/m:WeryfZaswiadOZatrud)
					then <fml:DC_WERYF_ZASWIAD_O_ZATRUD>{ data($req/m:WeryfZaswiadOZatrud) }</fml:DC_WERYF_ZASWIAD_O_ZATRUD>
					else ()
			}
			{
				if($req/m:Konwersja1)
					then <fml:DC_KONWERSJA_1>{ data($req/m:Konwersja1) }</fml:DC_KONWERSJA_1>
					else ()
			}
			{
				if($req/m:FormaOrganizacyjnoPrawna)
					then <fml:DC_FORMA_ORGANIZACYJNO_PRAWNA>{ data($req/m:FormaOrganizacyjnoPrawna) }</fml:DC_FORMA_ORGANIZACYJNO_PRAWNA>
					else ()
			}
			{
				if($req/m:WartoscTowaruOddanego)
					then <fml:DC_WARTOSC_TOWARU_ODDANEGO>{ data($req/m:WartoscTowaruOddanego) }</fml:DC_WARTOSC_TOWARU_ODDANEGO>
					else ()
			}
			{
				if($req/m:LiczbaOsWeWspGospD)
					then <fml:DC_LICZBA_OS_WE_WSP_GOSP_D>{ data($req/m:LiczbaOsWeWspGospD) }</fml:DC_LICZBA_OS_WE_WSP_GOSP_D>
					else ()
			}
			{
				if($req/m:DodDochodMiesNetto)
					then <fml:DC_DOD_DOCHOD_MIES_NETTO>{ data($req/m:DodDochodMiesNetto) }</fml:DC_DOD_DOCHOD_MIES_NETTO>
					else ()
			}
			{
				if($req/m:NumerDrugiegoKlienta)
					then <fml:DC_NUMER_DRUGIEGO_KLIENTA>{ data($req/m:NumerDrugiegoKlienta) }</fml:DC_NUMER_DRUGIEGO_KLIENTA>
					else ()
			}
			{
				if($req/m:KodPowiazaniaDrugiego)
					then <fml:DC_KOD_POWIAZANIA_DRUGIEGO>{ data($req/m:KodPowiazaniaDrugiego) }</fml:DC_KOD_POWIAZANIA_DRUGIEGO>
					else ()
			}
			{
				if($req/m:KodPowiazaniaPierwszego)
					then <fml:DC_KOD_POWIAZANIA_PIERWSZEGO>{ data($req/m:KodPowiazaniaPierwszego) }</fml:DC_KOD_POWIAZANIA_PIERWSZEGO>
					else ()
			}
			{
				if($req/m:DataZalozeniaPowiazania)
					then <fml:DC_DATA_ZALOZENIA_POWIAZANIA>{ data($req/m:DataZalozeniaPowiazania) }</fml:DC_DATA_ZALOZENIA_POWIAZANIA>
					else ()
			}
			{
				if($req/m:DataKoncaPowiazania)
					then <fml:DC_DATA_KONCA_POWIAZANIA>{ data($req/m:DataKoncaPowiazania) }</fml:DC_DATA_KONCA_POWIAZANIA>
					else ()
			}
			{
				if($req/m:KodDostepu)
					then <fml:DC_KOD_DOSTEPU>{ data($req/m:KodDostepu) }</fml:DC_KOD_DOSTEPU>
					else ()
			}
			{
				if($req/m:DataOstPrzegLini)
					then <fml:DC_DATA_OST_PRZEG_LINI>{ data($req/m:DataOstPrzegLini) }</fml:DC_DATA_OST_PRZEG_LINI>
					else ()
			}
			{
				if($req/m:CzestPrzegladuLinii)
					then <fml:DC_CZEST_PRZEGLADU_LINII>{ data($req/m:CzestPrzegladuLinii) }</fml:DC_CZEST_PRZEGLADU_LINII>
					else ()
			}
			{
				if($req/m:DataNastPrzegLiniiKre)
					then <fml:DC_DATA_NAST_PRZEG_LINII_KRE>{ data($req/m:DataNastPrzegLiniiKre) }</fml:DC_DATA_NAST_PRZEG_LINII_KRE>
					else ()
			}
			{
				if($req/m:StRyzykaBzwbk)
					then <fml:DC_ST_RYZYKA_BZWBK>{ data($req/m:StRyzykaBzwbk) }</fml:DC_ST_RYZYKA_BZWBK>
					else ()
			}
			{
				if($req/m:DataZmianyKoduRyzyka)
					then <fml:DC_DATA_ZMIANY_KODU_RYZYKA>{ data($req/m:DataZmianyKoduRyzyka) }</fml:DC_DATA_ZMIANY_KODU_RYZYKA>
					else ()
			}
			{
				if($req/m:PierwszTejLiniiKredyt)
					then <fml:DC_PIERWSZ_TEJ_LINII_KREDYT>{ data($req/m:PierwszTejLiniiKredyt) }</fml:DC_PIERWSZ_TEJ_LINII_KREDYT>
					else ()
			}
			{
				if($req/m:ObecneZobowiazania)
					then <fml:DC_OBECNE_ZOBOWIAZANIA>{ data($req/m:ObecneZobowiazania) }</fml:DC_OBECNE_ZOBOWIAZANIA>
					else ()
			}
			{
				if($req/m:TypKlienta)
					then <fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }</fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:SytEkonomFinans)
					then <fml:DC_SYT_EKONOM_FINANS>{ data($req/m:SytEkonomFinans) }</fml:DC_SYT_EKONOM_FINANS>
					else ()
			}
			{
				if($req/m:RodzajKoduDanePodst)
					then <fml:DC_RODZAJ_KODU_DANE_PODST>{ data($req/m:RodzajKoduDanePodst) }</fml:DC_RODZAJ_KODU_DANE_PODST>
					else ()
			}
			{
				if($req/m:SystemStudiow)
					then <fml:DC_SYSTEM_STUDIOW>{ data($req/m:SystemStudiow) }</fml:DC_SYSTEM_STUDIOW>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapbICBSAddCustomerRequest($body/m:bICBSAddCustomerRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>