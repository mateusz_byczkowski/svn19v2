<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "urn:be.services.dcl";
declare namespace m1 = "urn:cif.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/"; 
declare namespace fml="";

declare variable $body external;
declare variable $header external;

<soap-env:Body>
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:customer/m1:Customer
    return
    <fml:FML32>
      <B_MSHEAD_MSGID?>{data($reqh/m:msgHeader/m:msgId)}</B_MSHEAD_MSGID>
      <B_NR_DOK?>{data($req/m1:customerNumber)}</B_NR_DOK>
    </fml:FML32>
  }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>