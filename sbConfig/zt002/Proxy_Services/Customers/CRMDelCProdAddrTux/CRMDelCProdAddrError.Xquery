<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace ctx = "http://www.bea.com/wli/sb/context";

declare function xf:mapFault($fau as element(soap-env:Fault))
    as element(fml:FML32) {
    <fml:FML32>
        {
            if ($fau/detail/ctx:fault/ctx:reason) then
            (
               let $reason := fn:substring-after(fn:substring-before(data($fau/detail/ctx:fault/ctx:reason), "):"), "(")
               let $urcode := fn:substring-before(fn:substring-after(fn:substring-after(data($fau/detail/ctx:fault/ctx:reason),":"),":"),":")
               return
               (
                  <fml:U_ERROR_CODE>{ data($reason) }</fml:U_ERROR_CODE>,
                  <fml:U_ERROR_CODE>{ data($urcode) }</fml:U_ERROR_CODE>,
                  <fml:U_FAULT_STRING>{ data($fau/faultstring) }</fml:U_FAULT_STRING>
               )
            ) 
            else 
            (
                 <fml:U_ERROR_CODE>30</fml:U_ERROR_CODE>,
                 <fml:U_FAULT_STRING>{ data($fau/faultstring) }</fml:U_FAULT_STRING>
            )      
        }
    </fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapFault($body/soap-env:Fault) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>