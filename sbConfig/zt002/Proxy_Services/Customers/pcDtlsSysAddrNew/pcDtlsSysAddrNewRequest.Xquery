<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappcDtlsSysAddrNewRequest($req as element(m:pcDtlsSysAddrNewRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:PracownikWprow)
					then <fml:CI_PRACOWNIK_WPROW>{ data($req/m:PracownikWprow) }</fml:CI_PRACOWNIK_WPROW>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }</fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:IdSys)
					then <fml:CI_ID_SYS>{ data($req/m:IdSys) }</fml:CI_ID_SYS>
					else ()
			}
			{
				if($req/m:TypAdresu)
					then <fml:CI_TYP_ADRESU>{ data($req/m:TypAdresu) }</fml:CI_TYP_ADRESU>
					else ()
			}
			{
				if($req/m:NumerRachunku)
					then <fml:CI_NUMER_RACHUNKU>{ data($req/m:NumerRachunku) }</fml:CI_NUMER_RACHUNKU>
					else ()
			}
			{
				if($req/m:IdSpolki)
					then <fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }</fml:CI_ID_SPOLKI>
					else ()
			}
			{
				if($req/m:DataWprowadzenia)
					then <fml:DC_DATA_WPROWADZENIA>{ data($req/m:DataWprowadzenia) }</fml:DC_DATA_WPROWADZENIA>
					else ()
			}
			{
				if($req/m:AdresDomyslny)
					then <fml:CI_ADRES_DOMYSLNY>{ data($req/m:AdresDomyslny) }</fml:CI_ADRES_DOMYSLNY>
					else ()
			}
			{
				if($req/m:ImieINazwiskoAlt)
					then <fml:DC_IMIE_I_NAZWISKO_ALT>{ data($req/m:ImieINazwiskoAlt) }</fml:DC_IMIE_I_NAZWISKO_ALT>
					else ()
			}
			{
				if($req/m:UlicaAdresAlt)
					then <fml:DC_ULICA_ADRES_ALT>{ data($req/m:UlicaAdresAlt) }</fml:DC_ULICA_ADRES_ALT>
					else ()
			}
			{
				if($req/m:NrPosesLokaluAdresAlt)
					then <fml:DC_NR_POSES_LOKALU_ADRES_ALT>{ data($req/m:NrPosesLokaluAdresAlt) }</fml:DC_NR_POSES_LOKALU_ADRES_ALT>
					else ()
			}
			{
				if($req/m:KodPocztowyAdresAlt)
					then <fml:DC_KOD_POCZTOWY_ADRES_ALT>{ data($req/m:KodPocztowyAdresAlt) }</fml:DC_KOD_POCZTOWY_ADRES_ALT>
					else ()
			}
			{
				if($req/m:MiastoAdresAlt)
					then <fml:DC_MIASTO_ADRES_ALT>{ data($req/m:MiastoAdresAlt) }</fml:DC_MIASTO_ADRES_ALT>
					else ()
			}
			{
				if($req/m:WojewodztwoKrajAdresAlt)
					then <fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{ data($req/m:WojewodztwoKrajAdresAlt) }</fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
					else ()
			}
			{
				if($req/m:KodKraju)
					then <fml:CI_KOD_KRAJU>{ data($req/m:KodKraju) }</fml:CI_KOD_KRAJU>
					else ()
			}
			{
				if($req/m:DataOd)
					then <fml:CI_DATA_OD>{ data($req/m:DataOd) }</fml:CI_DATA_OD>
					else ()
			}
			{
				if($req/m:DataDo)
					then <fml:CI_DATA_DO>{ data($req/m:DataDo) }</fml:CI_DATA_DO>
					else ()
			}
			{
				if($req/m:DataZamieszkania)
					then <fml:CI_DATA_ZAMIESZKANIA>{ data($req/m:DataZamieszkania) }</fml:CI_DATA_ZAMIESZKANIA>
					else ()
			}
			{
				if($req/m:TypAdresuKoresp)
					then <fml:CI_TYP_ADRESU_KORESP>{ data($req/m:TypAdresuKoresp) }</fml:CI_TYP_ADRESU_KORESP>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mappcDtlsSysAddrNewRequest($body/m:pcDtlsSysAddrNewRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>