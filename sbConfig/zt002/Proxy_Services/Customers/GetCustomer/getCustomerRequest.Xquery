<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$2.2011-04-07</con:description>
    <con:xquery><![CDATA[declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:dictionaries.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function xf:getFields($parm as element(ns4:invoke), $msghead as element(ns4:msgHeader), $tranhead as element(ns4:transHeader))
	as element(fml:FML32) {

let $msgId:= if ($msghead/ns4:msgId) then $msghead/ns4:msgId else $msghead/msgId 
let $companyId:= if ($msghead/ns4:companyId) then $msghead/ns4:companyId else $msghead/companyId
let $userId := if ($msghead/ns4:userId) then $msghead/ns4:userId else $msghead/userId
let $appId:= if ($msghead/ns4:appId) then $msghead/ns4:appId else $msghead/appId
let $unitId := if ($msghead/ns4:unitId) then $msghead/ns4:unitId else $msghead/unitId
let $timestamp:= if ($msghead/ns4:timestamp) then $msghead/ns4:timestamp else $msghead/timestamp

let $transId:= if ($tranhead/ns4:transId) then $tranhead/ns4:transId else $tranhead/transId

return
  <fml:FML32>
      <CI_MSHEAD_MSGID?>{data($msgId)}</CI_MSHEAD_MSGID> 
      <CI_ID_SPOLKI?>{data($companyId)}</CI_ID_SPOLKI>
      <CI_ID_WEW_PRAC?>{data($userId)}</CI_ID_WEW_PRAC>
      <DC_NUMER_KLIENTA?>{data($parm/ns4:customer/ns2:Customer/ns2:customerNumber)}</DC_NUMER_KLIENTA>
      <CI_OPCJA>7</CI_OPCJA>
  </fml:FML32>
};

<soap:Body>
  { xf:getFields($body/ns4:invoke, $header/ns4:header/ns4:msgHeader, $header/ns4:header/ns4:transHeader) }</soap:Body>]]></con:xquery>
</con:xqueryEntry>