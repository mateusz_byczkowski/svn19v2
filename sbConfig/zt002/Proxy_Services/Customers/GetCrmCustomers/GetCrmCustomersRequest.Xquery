<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetCrmCustomersRequest($req as element(m:GetCrmCustomersRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:IdWewPrac)
					then <fml:CI_ID_WEW_PRAC>{ data($req/m:IdWewPrac) }</fml:CI_ID_WEW_PRAC>
					else ()
			}
			{
				if($req/m:ZakresWyszukiwania)
					then <fml:CI_ZAKRES_WYSZUKIWANIA>{ data($req/m:ZakresWyszukiwania) }</fml:CI_ZAKRES_WYSZUKIWANIA>
					else ()
			}

(:  Przełączenie usługi na korzystanie z master file CIS dla wersji 1 aplikacji
			{
				if($req/m:IdSpolki)
					then <fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }</fml:CI_ID_SPOLKI>
					else ()
			}
:)
                  	<fml:CI_ID_SPOLKI>0</fml:CI_ID_SPOLKI>
			{
				if($req/m:TypKlienta)
					then <fml:DC_TYP_KLIENTA>{ data($req/m:TypKlienta) }</fml:DC_TYP_KLIENTA>
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }</fml:DC_NUMER_KLIENTA>
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then <fml:DC_NR_DOWODU_REGON>{ data($req/m:NrDowoduRegon) }</fml:DC_NR_DOWODU_REGON>
					else ()
			}
			{
				if($req/m:NrPesel)
					then <fml:DC_NR_PESEL>{ data($req/m:NrPesel) }</fml:DC_NR_PESEL>
					else ()
			}
			{
				if($req/m:Nip)
					then <fml:DC_NIP>{ data($req/m:Nip) }</fml:DC_NIP>
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then <fml:DC_NUMER_PASZPORTU>{ data($req/m:NumerPaszportu) }</fml:DC_NUMER_PASZPORTU>
					else ()
			}
			{
				if($req/m:Nik)
					then <fml:CI_NIK>{ data($req/m:Nik) }</fml:CI_NIK>
					else ()
			}
			{
				if($req/m:Nazwa)
					then <fml:DC_NAZWA>{ data($req/m:Nazwa) }</fml:DC_NAZWA>
					else ()
			}
			{
				if($req/m:Nazwisko)
					then <fml:DC_NAZWISKO>{ data($req/m:Nazwisko) }</fml:DC_NAZWISKO>
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then <fml:DC_MIASTO_DANE_PODST>{ data($req/m:MiastoDanePodst) }</fml:DC_MIASTO_DANE_PODST>
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then <fml:DC_KOD_POCZTOWY_DANE_PODST>{ data($req/m:KodPocztowyDanePodst) }</fml:DC_KOD_POCZTOWY_DANE_PODST>
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then <fml:DC_NUMER_ODDZIALU>{ data($req/m:NumerOddzialu) }</fml:DC_NUMER_ODDZIALU>
					else ()
			}
			{
				if($req/m:IdPortfela)
					then <fml:CI_ID_PORTFELA>{ data($req/m:IdPortfela) }</fml:CI_ID_PORTFELA>
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then <fml:CI_LICZBA_OPER>{ data($req/m:LiczbaOper) }</fml:CI_LICZBA_OPER>
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then <fml:CI_NUMER_PACZKI>{ data($req/m:NumerPaczki) }</fml:CI_NUMER_PACZKI>
					else ()
			}
			{
				if($req/m:Sortowanie)
					then <fml:CI_SORTOWANIE>{ data($req/m:Sortowanie) }</fml:CI_SORTOWANIE>
					else ()
			}
			{
				if($req/m:Opcja)
					then <fml:CI_OPCJA>{ data($req/m:Opcja) }</fml:CI_OPCJA>
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then <fml:DC_ULICA_DANE_PODST>{ data($req/m:UlicaDanePodst) }</fml:DC_ULICA_DANE_PODST>
					else ()
			}
			{
				if($req/m:NrTelefonu)
					then <fml:DC_NR_TELEFONU>{ data($req/m:NrTelefonu) }</fml:DC_NR_TELEFONU>
					else ()
			}
			{
				if($req/m:Imie)
					then <fml:DC_IMIE>{ data($req/m:Imie) }</fml:DC_IMIE>
					else ()
			}
			{
				if($req/m:Ekd)
					then <fml:DC_EKD>{ data($req/m:Ekd) }</fml:DC_EKD>
					else ()
			}
			{
				if($req/m:Spw)
					then <fml:DC_SPW>{ data($req/m:Spw) }</fml:DC_SPW>
					else ()
			}
			{
				if($req/m:Decyl)
					then <fml:CI_DECYL>{ data($req/m:Decyl) }</fml:CI_DECYL>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapGetCrmCustomersRequest($body/m:GetCrmCustomersRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>