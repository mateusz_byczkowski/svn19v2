<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:cif.entities.be.dcl";
declare namespace w = "urn:dictionaries.be.dcl";


declare function local:mapGetCustomerEmploymentAddressRequest($req as element(), $head as element())
    as element(fml:FML32) {
        <fml:FML32>
        	<fml:DC_NUMER_KLIENTA>{ data($req/m:customer/e:Customer/e:customerNumber) }</fml:DC_NUMER_KLIENTA>
        	<fml:DC_TYP_KLIENTA>F</fml:DC_TYP_KLIENTA>
        	<fml:CI_PRACOWNIK_ZMIEN>{ data($head/m:msgHeader/m:userId) }</fml:CI_PRACOWNIK_ZMIEN>
                {
                 if(string-length(data($req/m:customer/e:Customer/e:companyID/w:CompanyType/w:companyType)) > 0) then
        	    <fml:CI_ID_SPOLKI>{data($req/m:customer/e:Customer/e:companyID/w:CompanyType/w:companyType)}</fml:CI_ID_SPOLKI>
                 else
                    <fml:CI_ID_SPOLKI>{ data($head/m:msgHeader/m:companyId) }</fml:CI_ID_SPOLKI>
                }
        	<fml:CI_CZAS_AKTUALIZACJI>{ fn-bea:dateTime-to-string-with-format("yyyy-MM-dd-HH.mm.ss.SSSSSS", data($head/m:msgHeader/m:timestamp)) }</fml:CI_CZAS_AKTUALIZACJI>
                <fml:CI_OPCJA>4</fml:CI_OPCJA>
   </fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
<soap-env:Body>
{ local:mapGetCustomerEmploymentAddressRequest($body/m:invoke, $header/m:header) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>