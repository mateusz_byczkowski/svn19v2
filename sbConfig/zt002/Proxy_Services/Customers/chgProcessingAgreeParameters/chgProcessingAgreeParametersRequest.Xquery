<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-03-28</con:description>
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:primeaccounts.entities.be.dcl";
declare namespace urn2 = "urn:accounts.entities.be.dcl";
declare namespace urn3 = "urn:cif.entities.be.dcl";
declare namespace urn4 = "urn:dictionaries.be.dcl";
declare namespace pdk = "http://bzwbk.com/services/pdk/";

declare function local:xsdate2plDate($date as xs:string?) as xs:string? {
    if($date)
        then
            let $tokens := fn:tokenize($date, "-")
            return
                fn:concat($tokens[3], ".", $tokens[2], ".", $tokens[1])
        else ()
};


declare function local:invoke($invoke as element(urn:invoke)) as element(soap-env:Body) {
    <soap-env:Body>
        <pdk:SetArt105Data>
            <pdk:Cif?>{ data($invoke/urn:customer[1]/urn3:Customer[1]/urn3:customerNumber[1]) }</pdk:Cif>
            {
                let $processingAgree := $invoke/urn:processingAgree[1]/urn3:ProcessingAgree[1]
                return (
                    <pdk:FlagaZgody>{ data($processingAgree/urn3:agreementFlag[1]/urn4:CustomerAgreementStatus[1]/urn4:customerAgreementStatus[1]) }</pdk:FlagaZgody>,
                    <pdk:DataUdzielenia?>{ local:xsdate2plDate(data($processingAgree/urn3:agreementDate[1])) }</pdk:DataUdzielenia>,
                    <pdk:OkresWaznosci?>{ data($processingAgree/urn3:agreementValidityPeriod[1]) }</pdk:OkresWaznosci>,
                    (:
                        <pdk:DataPowiadomienia></pdk:DataPowiadomienia>
                        <pdk:WskaznikPrzetwarzania></pdk:WskaznikPrzetwarzania>
                     :)
                    <pdk:DataWycofania?>{ local:xsdate2plDate(data($processingAgree/urn3:agreementCancellingDate[1])) }</pdk:DataWycofania>,
                    <pdk:Uzytkownik>{ data($processingAgree/urn3:operationUserId[1]) }</pdk:Uzytkownik>
                )
            }
            {
                let $ccacc := data($invoke/urn:creditCardAccount[1]/urn1:CreditCardAccount[1]/urn1:accountNumber[1])
                return
                    if($ccacc)
                    then (
                        <pdk:NrRachunku>{ $ccacc }</pdk:NrRachunku>,
                        <pdk:System>P</pdk:System>
                    )
                    else (
                        <pdk:NrRachunku>{ data($invoke/urn:account[1]/urn2:Account[1]/urn2:accountNumber[1]) }</pdk:NrRachunku>,
                        <pdk:System>I</pdk:System>
                    )
            }
      </pdk:SetArt105Data>
   </soap-env:Body>
};

declare variable $body as element() external;

local:invoke($body/urn:invoke)]]></con:xquery>
</con:xqueryEntry>