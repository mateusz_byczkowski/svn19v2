<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(: Log Zmian: 
v.1.0  2010-08-19  PKLI NP1836 Utworzenie
:)

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:getElementsForProductsList($fml as element(fml:FML32)) as element()* {
  let $NF_ACCOUN_ACCOUNTNUMBER  := $fml/fml:NF_ACCOUN_ACCOUNTNUMBER 
  let $NF_ACCOUN_ACCOUNTNAME := $fml/fml:NF_ACCOUN_ACCOUNTNAME
  let $NF_ACCOUN_ACCOUNTDESCRIPTI := $fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI
  let $NF_CURREC_CURRENCYCODE := $fml/fml:NF_CURREC_CURRENCYCODE
  let $NF_ACCOUR_RELATIONSHIP := $fml/fml:NF_ACCOUR_RELATIONSHIP           
  let $NF_APPLIN_APPLICATIONNUMBE:= $fml/fml:NF_APPLIN_APPLICATIONNUMBE 
  let $NF_ACCOUN_PRODUCTCODE := $fml/fml:NF_ACCOUN_PRODUCTCODE
  let $NF_BRANCC_BRANCHCODE := $fml/fml:NF_BRANCC_BRANCHCODE
  let $NF_ACCOUT_ACCOUNTTYPE := $fml/fml:NF_ACCOUT_ACCOUNTTYPE
  let $NF_ACCOUN_CURRENTBALANCE := $fml/fml:NF_ACCOUN_CURRENTBALANCE
  let $NF_ACCOUN_CURRENTBALANCEPL:= $fml/fml:NF_ACCOUN_CURRENTBALANCEPL
  let $NF_ACCOUN_AVAILABLEBALANCE:=$fml/fml:NF_ACCOUN_AVAILABLEBALANCE
  let $NF_PRODUCT_STATUSCODE:=$fml/fml:NF_PRODUCT_STATUSCODE
  let $NF_ACCOTF_ACCOUNTTYPEFLAG:= $fml/fml:NF_ACCOTF_ACCOUNTTYPEFLAG
  let $NF_PRODAT_IDPRODUCTDEFINIT := $fml/fml:NF_PRODAT_IDPRODUCTDEFINIT
  let $PT_ID_GROUP := $fml/fml:PT_ID_GROUP
  let $NF_PRODUCT_STAT := $fml/fml:NF_PRODUCT_STAT
  let $NF_PRODUA_CODEPRODUCTAREA := $fml/fml:NF_PRODUA_CODEPRODUCTAREA	

  for $it at $p in $fml/fml:NF_ACCOUN_ACCOUNTNUMBER return
    <m:product>	
     {
        if($fml/fml:NF_ACCOUN_ACCOUNTNUMBER[$p])
        then <m:accountNumber>{ data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER[$p]) }</m:accountNumber>
        else <m:accountNumber/>
      }
     {
        if($fml/fml:NF_ACCOUN_ACCOUNTNAME[$p])
        then <m:accountName>{ data($fml/fml:NF_ACCOUN_ACCOUNTNAME[$p]) }</m:accountName>
        else  <m:accountName/>
      }
     {
        if($fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI[$p])
        then <m:accountDescription>{ data($fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI[$p]) }</m:accountDescription>
        else <m:accountDescription/>
      }
     {
        if($fml/fml:NF_CURREC_CURRENCYCODE[$p])
        then <m:currencyCode>{ data($fml/fml:NF_CURREC_CURRENCYCODE[$p]) }</m:currencyCode>
        else <m:currencyCode/>
      }
     {
        if($fml/fml:NF_ACCOUR_RELATIONSHIP[$p])
        then <m:relationship>{ data($fml/fml:NF_ACCOUR_RELATIONSHIP[$p]) }</m:relationship>
        else <m:relationship/>
      }
     {
        if($fml/fml:NF_APPLIN_APPLICATIONNUMBE[$p])
        then <m:codeProductSystem>{ data($fml/fml:NF_APPLIN_APPLICATIONNUMBE[$p]) }</m:codeProductSystem>
        else <m:codeProductSystem>0</m:codeProductSystem>
      }
     {
        if($fml/fml:NF_ACCOUN_PRODUCTCODE[$p])
        then <m:productCode>{ data($fml/fml:NF_ACCOUN_PRODUCTCODE[$p]) }</m:productCode>
        else <m:productCode/>
      }
     {
        if($fml/fml:NF_BRANCC_BRANCHCODE[$p])
        then <m:accountBranchNumber>{ data($fml/fml:NF_BRANCC_BRANCHCODE[$p]) }</m:accountBranchNumber>
        else <m:accountBranchNumber>0</m:accountBranchNumber>
      }
     {
        if($fml/fml:NF_ACCOUT_ACCOUNTTYPE[$p])
        then <m:accountType>{ data($fml/fml:NF_ACCOUT_ACCOUNTTYPE[$p]) }</m:accountType>
        else <m:accountType/>
      }
     {
        if($fml/fml:NF_ACCOUN_CURRENTBALANCE[$p])
        then <m:currentBalance>{ data($fml/fml:NF_ACCOUN_CURRENTBALANCE[$p]) }</m:currentBalance>
        else <m:currentBalance>0</m:currentBalance>
      }
     {
        if($fml/fml:NF_ACCOUN_CURRENTBALANCEPL[$p])
        then <m:currentBalancePLN>{ data($fml/fml:NF_ACCOUN_CURRENTBALANCEPL[$p]) }</m:currentBalancePLN>
        else <m:currentBalancePLN>0</m:currentBalancePLN>
      }
     {
        if($fml/fml:NF_ACCOUN_AVAILABLEBALANCE[$p])
        then <m:availableBalance>{ data($fml/fml:NF_ACCOUN_AVAILABLEBALANCE[$p]) }</m:availableBalance>
        else <m:availableBalance>0</m:availableBalance>
      }
     {
        if($fml/fml:NF_PRODUCT_STATUSCODE[$p])
        then <m:accountStatus>{ data($fml/fml:NF_PRODUCT_STATUSCODE[$p]) }</m:accountStatus>
        else <m:accountStatus/>
      }
     {
        if($fml/fml:NF_FEET_FEETYPE[$p])
        then <m:feeType>{ data($fml/fml:NF_FEET_FEETYPE[$p]) }</m:feeType>
        else <m:feeType>0</m:feeType>
      }
     {
        if($fml/fml:NF_ACCOTF_ACCOUNTTYPEFLAG[$p])
        then <m:accountTypeFlag>{ data($fml/fml:NF_ACCOTF_ACCOUNTTYPEFLAG[$p]) }</m:accountTypeFlag>
        else <m:accountTypeFlag/>
      }
     {
        if($fml/fml:NF_PRODAT_IDPRODUCTDEFINIT[$p])
        then <m:idProductDefinition>{ data($fml/fml:NF_PRODAT_IDPRODUCTDEFINIT[$p]) }</m:idProductDefinition>
        else <m:idProductDefinition>0</m:idProductDefinition>
      }
     {
        if($fml/fml:PT_ID_GROUP[$p])
        then <m:idProductGroup>{ data($fml/fml:PT_ID_GROUP[$p]) }</m:idProductGroup>
        else <m:idProductGroup>0</m:idProductGroup>
      }
     {
        if($fml/fml:NF_PRODUCT_STAT[$p])
        then <m:accountActiveStatus>{ data($fml/fml:NF_PRODUCT_STAT[$p]) }</m:accountActiveStatus>
        else <m:accountActiveStatus/>
      }
     {
        if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p])
        then <m:codeProductArea>{ data($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p]) }</m:codeProductArea>
        else <m:codeProductArea>0</m:codeProductArea>
      }

    </m:product>
			
};

declare function xf:getElementsForPageControl($fml as element(fml:FML32)) as element()* {
    <m:pageControl>	
      {
        if($fml/fml:NF_PAGECC_OPERATIONS)
        then <m:Operations>{ data($fml/fml:NF_PAGECC_OPERATIONS) }</m:Operations>
        else ()
      }
      {
        if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
        then <m:navigationKeyValue>{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }</m:navigationKeyValue>
        else ()
      }
      {
        if($fml/fml:NF_PAGEC_HASNEXT)
        then <m:hasNext>{ data($fml/fml:NF_PAGEC_HASNEXT) }</m:hasNext>
        else ()
      }

   </m:pageControl>	
};     

declare function xf:getElementsForServiceWarningsList($fml as element(fml:FML32)) as element()* {
  let $NF_SERVIW_WARNINGCODE1 := $fml/fml:NF_SERVIW_WARNINGCODE1
  let $NF_SERVIW_WARNINGCODE2 := $fml/fml:NF_SERVIW_WARNINGCODE2
  let $NF_SERVIW_WARNINGUSERVISIB := $fml/fml:NF_SERVIW_WARNINGUSERVISIB
  let $NF_SERVIW_WARNINGDESCRIPTI := $fml/fml:NF_SERVIW_WARNINGDESCRIPTI 
 
  for $it at $p in $fml/fml:NF_SERVIW_WARNINGCODE1 return
    <m:serviceWarning>	
      {
        if($NF_SERVIW_WARNINGCODE1[$p])
        then <m:WarningCode1>{ data($NF_SERVIW_WARNINGCODE1[$p]) }</m:WarningCode1>
        else ()
      }
      {
        if($NF_SERVIW_WARNINGCODE2[$p])
        then <m:WarningCode2>{ data($NF_SERVIW_WARNINGCODE2[$p]) }</m:WarningCode2>
        else ()
      }
      {
        if($NF_SERVIW_WARNINGUSERVISIB[$p])
        then <m:WarningUserVisibility>{ data($NF_SERVIW_WARNINGUSERVISIB[$p]) }</m:WarningUserVisibility>
        else ()
      }
      {
        if($NF_SERVIW_WARNINGDESCRIPTI[$p])
        then <m:WarningDescription>{ data($NF_SERVIW_WARNINGDESCRIPTI[$p]) }</m:WarningDescription>
        else ()
      } 
    </m:serviceWarning>	
};        
        
declare function xf:mappGetCustomerProductsResponse($fml as element(fml:FML32)) as element() {
  <m:GetCustomerProductsResponse>
     {xf:getElementsForProductsList($fml)} 
     {xf:getElementsForPageControl($fml)} 
     {xf:getElementsForServiceWarningsList($fml)} 
  </m:GetCustomerProductsResponse>        
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mappGetCustomerProductsResponse($body/fml:FML32) }

</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>