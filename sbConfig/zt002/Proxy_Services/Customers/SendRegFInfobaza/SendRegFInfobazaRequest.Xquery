<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap-env:Body) external;

<soapenv:Body  xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:bzwbk.com">
      <urn:regulationMail>
         <CONTENTMAILTYPE>{data($body/FML32/NF_DOCUME_DOCUMENTNUMBER)}</CONTENTMAILTYPE>
         <REGULATIONSLIST>
            <!--1 or more repetitions:-->
           {
            for $regId in $body/FML32/NF_DOCUME_DOCUMENTID
             return 
            <regulations>
               <regulationId>{data($regId)}</regulationId>
            </regulations>
           }
         </REGULATIONSLIST>
         <customersList>
            <customer>             
                <customerNumber>{data($body/FML32/NF_CUSTOM_CUSTOMERNUMBER)}</customerNumber>
                <email>{data($body/FML32/NF_CUSTOM_EMAIL)}</email>
            </customer>
         </customersList>
      </urn:regulationMail>
   </soapenv:Body>]]></con:xquery>
</con:xqueryEntry>