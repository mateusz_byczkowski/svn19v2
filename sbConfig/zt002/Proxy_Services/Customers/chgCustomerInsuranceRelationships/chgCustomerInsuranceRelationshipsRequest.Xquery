<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$2.2011-01-18</con:description>
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:cif.entities.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:dictionaries.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:insurance.entities.be.dcl";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function shortAccountNbr($account as xs:string) as xs:string
{        
    if (string-length($account) >  10) 
           then  substring($account, string-length($account)- 9,10)
    else   $account
};

declare function xf:mapRow($numerKlienta as xs:string,
						   $typRelacji as xs:string) as element()*{	   
           
	  <fml:DC_NUMER_KLIENTA>{$numerKlienta}</fml:DC_NUMER_KLIENTA>,
      <fml:DC_RELACJA>{$typRelacji }</fml:DC_RELACJA>,
      <fml:DC_TYP_RELACJI>1</fml:DC_TYP_RELACJI>,
	  <fml:DC_PROCENT_ZOB_PODATKOWYCH?>0</fml:DC_PROCENT_ZOB_PODATKOWYCH> 
};


declare function xf:mapNullRow() as element()*{	   
      <fml:DC_NUMER_KLIENTA nil="true"/>,
      <fml:DC_RELACJA nil="true"/>,
      <fml:DC_TYP_RELACJI nil="true"/>, 
      <fml:DC_PROCENT_ZOB_PODATKOWYCH nil="true"/>
};

declare function mapPackageNew($input as element(ns0:AccountRelationship), $number as xs:integer) as element()*{

       if ($number <= 7) then
        if (data($input)) then
            xf:mapRow(data($input/ns0:customer/ns0:Customer/ns0:customerNumber),
            data($input/ns0:relationship/ns1:CustomerAccountRelationship/ns1:customerAccountRelationship))
        else
            xf:mapNullRow()
        else()
};



declare function getFieldsFromHeader($parm as element(ns5:header)) as element()*
{

<DC_ODDZIAL?>{chkUnitId(data($parm/ns5:msgHeader/ns5:unitId))}</DC_ODDZIAL>
,
<DC_UZYTKOWNIK?>{concat("SKP:",data($parm/ns5:msgHeader/ns5:userId))}</DC_UZYTKOWNIK>
,
<DC_TRN_ID?>{data($parm/ns5:transHeader/ns5:transId)}</DC_TRN_ID>
};

declare function getFieldsFromInvoke($parm as element(ns5:invoke)) as element()*
{

if(string-length(data($parm/ns5:insurancePolicyAcc/ns4:InsurancePolicyAcc/ns4:policyRefNum))>0) then
<DC_NR_RACHUNKU?>{shortAccountNbr(data($parm/ns5:insurancePolicyAcc/ns4:InsurancePolicyAcc/ns4:policyRefNum))}</DC_NR_RACHUNKU>
else
<DC_NR_RACHUNKU?>{shortAccountNbr(data($parm/ns5:insurancePackageAcc/ns4:InsurancePackageAcc/ns4:number))}</DC_NR_RACHUNKU>
,
if(string-length(data($parm/ns5:insurancePolicyAcc/ns4:InsurancePolicyAcc/ns4:policyRefNum))>0) then
<DC_KOD_APLIKACJI>3</DC_KOD_APLIKACJI>
else
<DC_KOD_APLIKACJI>91</DC_KOD_APLIKACJI>
,
 for $i in (1 to count($parm/ns5:accountRelatioshipList/ns0:AccountRelationship))
     return 
        mapPackageNew($parm/ns5:accountRelatioshipList/ns0:AccountRelationship[$i], $i)
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns5:header)}
    {getFieldsFromInvoke($body/ns5:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>