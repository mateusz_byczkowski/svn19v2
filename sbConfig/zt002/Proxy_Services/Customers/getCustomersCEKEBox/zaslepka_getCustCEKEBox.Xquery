<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-10-22</con:description>
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:ceke.entities.be.dcl";
declare namespace ns0="urn:cif.entities.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForCustomerCEKEList($parm as element(soap:Body)) as element()
{

<ns0:customerCEKEList>
(:  {
    for $x at $occ in $parm/XXXXX
    return
    <ns1:CustomerCEKE>
      <ns1:nik?>{data($parm/NF_CUCEKE_NIK[$occ])}</ns1:nik>
    </ns1:CustomerCEKE>
  } :)
</ns0:customerCEKEList>
};
declare function getElementsForCustomersListOut($parm as element(soap:Body)) as element()
{

<ns4:customersListOut>
    <ns0:Customer>
      <ns0:customerNumber?>{data($parm/ns4:invoke/ns4:customersList/ns0:Customer/ns0:customerNumber)}</ns0:customerNumber>
      {getElementsForCustomerCEKEList($parm)}
    </ns0:Customer>
</ns4:customersListOut>
};
declare function getElementsForInvokeResponse($parm as element(soap:Body)) as element()*
{

<ns4:invokeResponse>
  {getElementsForCustomersListOut($parm)}
</ns4:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>