<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeChnlNewResponse($fml as element(fml:FML32))
	as element(m:eChnlNewResponse) {
		<m:eChnlNewResponse>
			{
				if($fml/fml:E_ADMIN_MICRO_BRANCH)
					then <m:AdminMicroBranch>{ data($fml/fml:E_ADMIN_MICRO_BRANCH) }</m:AdminMicroBranch>
					else ()
			}
			{
				if($fml/fml:E_LOGIN_ID)
					then <m:LoginId>{ data($fml/fml:E_LOGIN_ID) }</m:LoginId>
					else ()
			}
			{
				if($fml/fml:U_USER_NAME)
					then <m:UserName>{ data($fml/fml:U_USER_NAME) }</m:UserName>
					else ()
			}
			{
				if($fml/fml:E_CHANNEL_TYPE)
					then <m:ChannelType>{ data($fml/fml:E_CHANNEL_TYPE) }</m:ChannelType>
					else ()
			}
			{
				if($fml/fml:E_STATUS)
					then <m:Status>{ data($fml/fml:E_STATUS) }</m:Status>
					else ()
			}
			{
				if($fml/fml:E_TRIES)
					then <m:Tries>{ data($fml/fml:E_TRIES) }</m:Tries>
					else ()
			}
			{
				if($fml/fml:E_TRIES_ALLOWED)
					then <m:TriesAllowed>{ data($fml/fml:E_TRIES_ALLOWED) }</m:TriesAllowed>
					else ()
			}
			{
				if($fml/fml:E_EXPIRY)
					then <m:Expiry>{ data($fml/fml:E_EXPIRY) }</m:Expiry>
					else ()
			}
			{
				if($fml/fml:E_CHANGE_PERIOD)
					then <m:ChangePeriod>{ data($fml/fml:E_CHANGE_PERIOD) }</m:ChangePeriod>
					else ()
			}
			{
				if($fml/fml:E_TRN_MASK)
					then <m:TrnMask>{ data($fml/fml:E_TRN_MASK) }</m:TrnMask>
					else ()
			}
			{
				if($fml/fml:E_CHANNEL_CYCLE)
					then <m:ChannelCycle>{ data($fml/fml:E_CHANNEL_CYCLE) }</m:ChannelCycle>
					else ()
			}
			{
				if($fml/fml:E_CHANNEL_CYCLE_BEGIN)
					then <m:ChannelCycleBegin>{ data($fml/fml:E_CHANNEL_CYCLE_BEGIN) }</m:ChannelCycleBegin>
					else ()
			}
			{
				if($fml/fml:E_CHANNEL_LIMIT)
					then <m:ChannelLimit>{ data($fml/fml:E_CHANNEL_LIMIT) }</m:ChannelLimit>
					else ()
			}
			{
				if($fml/fml:E_CHANNEL_AMOUNT_REMAINING)
					then <m:ChannelAmountRemaining>{ data($fml/fml:E_CHANNEL_AMOUNT_REMAINING) }</m:ChannelAmountRemaining>
					else ()
			}
			{
				if($fml/fml:E_CHANNEL_OPTIONS)
					then <m:ChannelOptions>{ data($fml/fml:E_CHANNEL_OPTIONS) }</m:ChannelOptions>
					else ()
			}
			{
				if($fml/fml:E_LOGIN_COUNT)
					then <m:LoginCount>{ data($fml/fml:E_LOGIN_COUNT) }</m:LoginCount>
					else ()
			}
			{
				if($fml/fml:E_BAD_LOGIN_COUNT)
					then <m:BadLoginCount>{ data($fml/fml:E_BAD_LOGIN_COUNT) }</m:BadLoginCount>
					else ()
			}
			{
				if($fml/fml:E_ALLOWED_SECURITY_LEVEL)
					then <m:AllowedSecurityLevel>{ data($fml/fml:E_ALLOWED_SECURITY_LEVEL) }</m:AllowedSecurityLevel>
					else ()
			}
			{
				if($fml/fml:E_LAST_LOGIN)
					then <m:LastLogin>{ data($fml/fml:E_LAST_LOGIN) }</m:LastLogin>
					else ()
			}
			{
				if($fml/fml:E_LAST_BAD_LOGIN)
					then <m:LastBadLogin>{ data($fml/fml:E_LAST_BAD_LOGIN) }</m:LastBadLogin>
					else ()
			}
			{
				if($fml/fml:E_LAST_CHANGED)
					then <m:LastChanged>{ data($fml/fml:E_LAST_CHANGED) }</m:LastChanged>
					else ()
			}
			{
				if($fml/fml:E_PDATA)
					then <m:Pdata>{ data($fml/fml:E_PDATA) }</m:Pdata>
					else ()
			}
			{
				if($fml/fml:E_TIME_STAMP)
					then <m:TimeStamp>{ data($fml/fml:E_TIME_STAMP) }</m:TimeStamp>
					else ()
			}
			{

				let $E_SEQ_NO := $fml/fml:E_SEQ_NO
				let $B_KOD_RACH := $fml/fml:B_KOD_RACH
				let $B_WALUTA := $fml/fml:B_WALUTA
				let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				let $E_OPTIONS := $fml/fml:E_OPTIONS
				let $B_BANK := $fml/fml:B_BANK
				let $B_SYS := $fml/fml:B_SYS
				let $E_CHANNEL_MASK := $fml/fml:E_CHANNEL_MASK
				let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
				let $E_ACCOUNT_TYPE_OPTIONS := $fml/fml:E_ACCOUNT_TYPE_OPTIONS
				let $E_DR_TRN_MASK := $fml/fml:E_DR_TRN_MASK
				let $E_CR_TRN_MASK := $fml/fml:E_CR_TRN_MASK
				let $E_BRANCH_CODE := $fml/fml:E_BRANCH_CODE
				let $B_NAZWA := $fml/fml:B_NAZWA
				let $B_NAZWA_KLIENTA := $fml/fml:B_NAZWA_KLIENTA
				let $B_M_ZAM := $fml/fml:B_M_ZAM
				let $B_UL_ZAM := $fml/fml:B_UL_ZAM
				let $B_KOD_POCZT := $fml/fml:B_KOD_POCZT
				let $E_TRN_TITLE := $fml/fml:E_TRN_TITLE
				for $it at $p in $fml/fml:E_SEQ_NO
				return
					<m:eChnlNewAccount>
					{
						if($E_SEQ_NO[$p])
							then <m:SeqNo>{ data($E_SEQ_NO[$p]) }</m:SeqNo>
						else ()
					}
					{
						if($B_KOD_RACH[$p])
							then <m:KodRach>{ data($B_KOD_RACH[$p]) }</m:KodRach>
						else ()
					}
					{
						if($B_WALUTA[$p])
							then <m:Waluta>{ data($B_WALUTA[$p]) }</m:Waluta>
						else ()
					}
					{
						if($B_DL_NR_RACH[$p])
							then <m:DlNrRach>{ data($B_DL_NR_RACH[$p]) }</m:DlNrRach>
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then <m:TypRach>{ data($B_TYP_RACH[$p]) }</m:TypRach>
						else ()
					}
					{
						if($E_OPTIONS[$p])
							then <m:Options>{ data($E_OPTIONS[$p]) }</m:Options>
						else ()
					}
					{
						if($B_BANK[$p])
							then <m:Bank>{ data($B_BANK[$p]) }</m:Bank>
						else ()
					}
					{
						if($B_SYS[$p])
							then <m:Sys>{ data($B_SYS[$p]) }</m:Sys>
						else ()
					}
					{
						if($E_CHANNEL_MASK[$p])
							then <m:ChannelMask>{ data($E_CHANNEL_MASK[$p]) }</m:ChannelMask>
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_NAME[$p])
							then <m:AccountTypeName>{ data($E_ACCOUNT_TYPE_NAME[$p]) }</m:AccountTypeName>
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_OPTIONS[$p])
							then <m:AccountTypeOptions>{ data($E_ACCOUNT_TYPE_OPTIONS[$p]) }</m:AccountTypeOptions>
						else ()
					}
					{
						if($E_DR_TRN_MASK[$p])
							then <m:DrTrnMask>{ data($E_DR_TRN_MASK[$p]) }</m:DrTrnMask>
						else ()
					}
					{
						if($E_CR_TRN_MASK[$p])
							then <m:CrTrnMask>{ data($E_CR_TRN_MASK[$p]) }</m:CrTrnMask>
						else ()
					}
					{
						if($E_BRANCH_CODE[$p])
							then <m:BranchCode>{ data($E_BRANCH_CODE[$p]) }</m:BranchCode>
						else ()
					}
					{
						if($B_NAZWA[$p])
							then <m:Nazwa>{ data($B_NAZWA[$p]) }</m:Nazwa>
						else ()
					}
					{
						if($B_NAZWA_KLIENTA[$p])
							then <m:NazwaKlienta>{ data($B_NAZWA_KLIENTA[$p]) }</m:NazwaKlienta>
						else ()
					}
					{
						if($B_M_ZAM[$p])
							then <m:MZam>{ data($B_M_ZAM[$p]) }</m:MZam>
						else ()
					}
					{
						if($B_UL_ZAM[$p])
							then <m:UlZam>{ data($B_UL_ZAM[$p]) }</m:UlZam>
						else ()
					}
					{
						if($B_KOD_POCZT[$p])
							then <m:KodPoczt>{ data($B_KOD_POCZT[$p]) }</m:KodPoczt>
						else ()
					}
					{
						if($E_TRN_TITLE[$p])
							then <m:TrnTitle>{ data($E_TRN_TITLE[$p]) }</m:TrnTitle>
						else ()
					}
					</m:eChnlNewAccount>
			}

		</m:eChnlNewResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapeChnlNewResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>