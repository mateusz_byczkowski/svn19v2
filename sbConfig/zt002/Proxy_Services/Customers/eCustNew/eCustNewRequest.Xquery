<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeCustNewRequest($req as element(m:eCustNewRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:AdminMicroBranch)
					then <fml:E_ADMIN_MICRO_BRANCH>{ data($req/m:AdminMicroBranch) }</fml:E_ADMIN_MICRO_BRANCH>
					else ()
			}
			{
				if($req/m:LoginId)
					then <fml:E_LOGIN_ID>{ data($req/m:LoginId) }</fml:E_LOGIN_ID>
					else ()
			}
			{
				if($req/m:Limit)
					then <fml:E_LIMIT>{ data($req/m:Limit) }</fml:E_LIMIT>
					else ()
			}
			{
				if($req/m:HiLimit)
					then <fml:E_HI_LIMIT>{ data($req/m:HiLimit) }</fml:E_HI_LIMIT>
					else ()
			}
			{
				if($req/m:TrnLimit)
					then <fml:E_TRN_LIMIT>{ data($req/m:TrnLimit) }</fml:E_TRN_LIMIT>
					else ()
			}
			{
				if($req/m:NrDok)
					then <fml:B_NR_DOK>{ data($req/m:NrDok) }</fml:B_NR_DOK>
					else ()
			}
			{
				if($req/m:RodzDok)
					then <fml:B_RODZ_DOK>{ data($req/m:RodzDok) }</fml:B_RODZ_DOK>
					else ()
			}
			{
				if($req/m:HiCycle)
					then <fml:E_HI_CYCLE>{ data($req/m:HiCycle) }</fml:E_HI_CYCLE>
					else ()
			}
			{
				if($req/m:CustSmsno)
					then <fml:E_CUST_SMSNO>{ data($req/m:CustSmsno) }</fml:E_CUST_SMSNO>
					else ()
			}
			{
				if($req/m:ExCycle)
					then <fml:E_EX_CYCLE>{ data($req/m:ExCycle) }</fml:E_EX_CYCLE>
					else ()
			}
			{
				if($req/m:Cycle)
					then <fml:E_CYCLE>{ data($req/m:Cycle) }</fml:E_CYCLE>
					else ()
			}
			{
				if($req/m:CustName)
					then <fml:E_CUST_NAME>{ data($req/m:CustName) }</fml:E_CUST_NAME>
					else ()
			}
			{
				if($req/m:CustCity)
					then <fml:E_CUST_CITY>{ data($req/m:CustCity) }</fml:E_CUST_CITY>
					else ()
			}
			{
				if($req/m:CustZipcode)
					then <fml:E_CUST_ZIPCODE>{ data($req/m:CustZipcode) }</fml:E_CUST_ZIPCODE>
					else ()
			}
			{
				if($req/m:CustTelno)
					then <fml:E_CUST_TELNO>{ data($req/m:CustTelno) }</fml:E_CUST_TELNO>
					else ()
			}
			{
				if($req/m:CustFaxno)
					then <fml:E_CUST_FAXNO>{ data($req/m:CustFaxno) }</fml:E_CUST_FAXNO>
					else ()
			}
			{
				if($req/m:CustGsmno)
					then <fml:E_CUST_GSMNO>{ data($req/m:CustGsmno) }</fml:E_CUST_GSMNO>
					else ()
			}
			{
				if($req/m:CustEmail)
					then <fml:E_CUST_EMAIL>{ data($req/m:CustEmail) }</fml:E_CUST_EMAIL>
					else ()
			}
			{
				if($req/m:CustStatus)
					then <fml:E_CUST_STATUS>{ data($req/m:CustStatus) }</fml:E_CUST_STATUS>
					else ()
			}
			{
				if($req/m:CustIdentif)
					then <fml:E_CUST_IDENTIF>{ data($req/m:CustIdentif) }</fml:E_CUST_IDENTIF>
					else ()
			}
			{
				if($req/m:CustOptions)
					then <fml:E_CUST_OPTIONS>{ data($req/m:CustOptions) }</fml:E_CUST_OPTIONS>
					else ()
			}
			{
				if($req/m:RepName)
					then <fml:E_REP_NAME>{ data($req/m:RepName) }</fml:E_REP_NAME>
					else ()
			}
			{
				if($req/m:ExLimit)
					then <fml:E_EX_LIMIT>{ data($req/m:ExLimit) }</fml:E_EX_LIMIT>
					else ()
			}
			{
				if($req/m:TrnExLimit)
					then <fml:E_TRN_EX_LIMIT>{ data($req/m:TrnExLimit) }</fml:E_TRN_EX_LIMIT>
					else ()
			}
			{
				if($req/m:Sys)
					then <fml:B_SYS>{ data($req/m:Sys) }</fml:B_SYS>
					else ()
			}
			{
				if($req/m:Bank)
					then <fml:B_BANK>{ data($req/m:Bank) }</fml:B_BANK>
					else ()
			}
			{
				if($req/m:TokenCustomerId)
					then <fml:E_TOKEN_CUSTOMER_ID>{ data($req/m:TokenCustomerId) }</fml:E_TOKEN_CUSTOMER_ID>
					else ()
			}
			{
				if($req/m:TokenIdType)
					then <fml:E_TOKEN_ID_TYPE>{ data($req/m:TokenIdType) }</fml:E_TOKEN_ID_TYPE>
					else ()
			}
			{
				if($req/m:TokenLoginId)
					then <fml:E_TOKEN_LOGIN_ID>{ data($req/m:TokenLoginId) }</fml:E_TOKEN_LOGIN_ID>
					else ()
			}
			{
				if($req/m:CifOptions)
					then <fml:E_CIF_OPTIONS>{ data($req/m:CifOptions) }</fml:E_CIF_OPTIONS>
					else ()
			}
			{
				if($req/m:AllowedSecurityLevel)
					then <fml:E_ALLOWED_SECURITY_LEVEL>{ data($req/m:AllowedSecurityLevel) }</fml:E_ALLOWED_SECURITY_LEVEL>
					else ()
			}
			{
				if($req/m:DefaultSecurityLevel)
					then <fml:E_DEFAULT_SECURITY_LEVEL>{ data($req/m:DefaultSecurityLevel) }</fml:E_DEFAULT_SECURITY_LEVEL>
					else ()
			}
			{
				if($req/m:ProfileId)
					then <fml:E_PROFILE_ID>{ data($req/m:ProfileId) }</fml:E_PROFILE_ID>
					else ()
			}
			{
				if($req/m:CustomerType)
					then <fml:E_CUSTOMER_TYPE>{ data($req/m:CustomerType) }</fml:E_CUSTOMER_TYPE>
					else ()
			}
			{
				if($req/m:ProfileTrial)
					then <fml:E_PROFILE_TRIAL>{ data($req/m:ProfileTrial) }</fml:E_PROFILE_TRIAL>
					else ()
			}
			{
				if($req/m:ProfileExpiry)
					then <fml:E_PROFILE_EXPIRY>{ data($req/m:ProfileExpiry) }</fml:E_PROFILE_EXPIRY>
					else ()
			}
			{
				if($req/m:CustReportVersion)
					then <fml:E_CUST_REPORT_VERSION>{ data($req/m:CustReportVersion) }</fml:E_CUST_REPORT_VERSION>
					else ()
			}
			{
				if($req/m:CustReportDate)
					then <fml:E_CUST_REPORT_DATE>{ data($req/m:CustReportDate) }</fml:E_CUST_REPORT_DATE>
					else ()
			}
			{
				if($req/m:CustRebate)
					then <fml:E_CUST_REBATE>{ data($req/m:CustRebate) }</fml:E_CUST_REBATE>
					else ()
			}
			{
				if($req/m:SysMask)
					then <fml:B_SYS_MASK>{ data($req/m:SysMask) }</fml:B_SYS_MASK>
					else ()
			}
			{
				for $x in $req/m:ChannelType
				return <fml:E_CHANNEL_TYPE>{ data($x) }</fml:E_CHANNEL_TYPE>
			}
			{
			        for $x in $req/m:Status
				return <fml:E_STATUS>{ data($x) }</fml:E_STATUS>
			}
			{
				for $x in $req/m:Tries
				return <fml:E_TRIES>{ data($x) }</fml:E_TRIES>
			}
			{
				for $x in $req/m:TriesAllowed
				return <fml:E_TRIES_ALLOWED>{ data($x) }</fml:E_TRIES_ALLOWED>
			}
			{
				for $x in $req/m:Expiry
				return <fml:E_EXPIRY>{ data($x) }</fml:E_EXPIRY>
			}
			{
				for $x in $req/m:ChangePeriod
				return <fml:E_CHANGE_PERIOD>{ data($x) }</fml:E_CHANGE_PERIOD>
			}
			{
				for $x in $req/m:TrnMask
				return <fml:E_TRN_MASK>{ data($x) }</fml:E_TRN_MASK>
			}
			{
				for $x in $req/m:Pdata
				return <fml:E_PDATA>{ data($x) }</fml:E_PDATA>
			}
			{
				for $x in $req/m:ChannelCycle
				return <fml:E_CHANNEL_CYCLE>{ data($x) }</fml:E_CHANNEL_CYCLE>
			}
			{
				for $x in $req/m:ChannelLimit
				return <fml:E_CHANNEL_LIMIT>{ data($x) }</fml:E_CHANNEL_LIMIT>
			}
			{
				for $x in $req/m:ChannelCycleBegin
				return <fml:E_CHANNEL_CYCLE_BEGIN>{ data($x) }</fml:E_CHANNEL_CYCLE_BEGIN>
			}
			{
				if($req/m:SeqNo)
					then <fml:E_SEQ_NO>{ data($req/m:SeqNo) }</fml:E_SEQ_NO>
					else ()
			}
			{
				if($req/m:KodRach)
					then <fml:B_KOD_RACH>{ data($req/m:KodRach) }</fml:B_KOD_RACH>
					else ()
			}
			{
				if($req/m:Waluta)
					then <fml:B_WALUTA>{ data($req/m:Waluta) }</fml:B_WALUTA>
					else ()
			}
			{
				if($req/m:DlNrRach)
					then <fml:B_DL_NR_RACH>{ data($req/m:DlNrRach) }</fml:B_DL_NR_RACH>
					else ()
			}
			{
				if($req/m:TypRach)
					then <fml:B_TYP_RACH>{ data($req/m:TypRach) }</fml:B_TYP_RACH>
					else ()
			}
			{
				if($req/m:Options)
					then <fml:E_OPTIONS>{ data($req/m:Options) }</fml:E_OPTIONS>
					else ()
			}
			{
				if($req/m:IdOddz)
					then <fml:B_ID_ODDZ>{ data($req/m:IdOddz) }</fml:B_ID_ODDZ>
					else ()
			}
			{
				if($req/m:CustStreet)
					then <fml:E_CUST_STREET>{ data($req/m:CustStreet) }</fml:E_CUST_STREET>
					else ()
			}
			{
				if($req/m:CifNumber)
					then <fml:E_CIF_NUMBER>{ data($req/m:CifNumber) }</fml:E_CIF_NUMBER>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapeCustNewRequest($body/m:eCustNewRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>