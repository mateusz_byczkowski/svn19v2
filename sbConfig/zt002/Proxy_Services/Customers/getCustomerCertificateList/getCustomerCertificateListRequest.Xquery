<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-12-15</con:description>
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:customercertificatedict.dictionaries.be.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:customercertificates.entities.be.dcl";
declare namespace ns8="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns6:msgHeader/ns6:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns6:msgHeader/ns6:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns6:msgHeader/ns6:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns6:msgHeader/ns6:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns6:msgHeader/ns6:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns6:msgHeader/ns6:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns6:transHeader/ns6:transId)}</NF_TRHEAD_TRANSID>
};


declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{

<NF_CUSTCS_CUSTOMERCERTIFIC?>{data($parm/ns6:CustomerCertificateStatus/ns0:CustomerCertificateStatus/ns0:customerCertificateStatus)}</NF_CUSTCS_CUSTOMERCERTIFIC>
,
<NF_DATEH_VALUE?>{data($parm/ns6:dateTo/ns8:DateHolder/ns8:value)}</NF_DATEH_VALUE>
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:reverseOrder)}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:navigationKeyDefinition)}</NF_PAGEC_NAVIGATIONKEYDEFI>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
,
<NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns6:customer/ns1:Customer/ns1:customerNumber)}</NF_CUSTOM_CUSTOMERNUMBER>
,
<NF_DATEH_VALUE?>{data($parm/ns6:dateFrom/ns8:DateHolder/ns8:value)}</NF_DATEH_VALUE>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvoke($body/ns6:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>