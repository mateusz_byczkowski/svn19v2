<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[xquery version "1.0";

declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;

declare function xf:mappcCustSetResponse($fml as element(fml:FML32))
	as element(m:pcCustSetResponse)
{
	<m:pcCustSetResponse>{ 
		for $i in 1 to count($fml/fml:CI_NR_KOM)
		return
			<m:Error>
				<m:NrKom>{
					data($fml/fml:CI_NR_KOM[$i])
				}</m:NrKom>

				<m:OpisBledu>{
					data($fml/fml:DC_OPIS_BLEDU[$i])
				}</m:OpisBledu>
			</m:Error>
	}</m:pcCustSetResponse>
};

<soap-env:Body>{
	xf:mappcCustSetResponse($body/fml:FML32)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>