<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-09-02</con:description>
    <con:xquery><![CDATA[(:Change log 
v.1.0  2010-06-28  PKL  NP2042 Utworzenie 
:)

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:accounts.entities.be.dcl";
declare namespace urn2="urn:cif.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(urn:header)) as element()*
{
  <CI_ID_SPOLKI?>{data($parm/urn:msgHeader/urn:companyId)}</CI_ID_SPOLKI>
  ,
  <CI_ID_WEW_PRAC?>{data($parm/urn:msgHeader/urn:userId)}</CI_ID_WEW_PRAC>
  , 
  <BLIB_CALL_ID?>{data($parm/urn:msgHeader/urn:msgId)}</BLIB_CALL_ID>
};


declare function getFieldsFromInvoke($parm as element(urn:invoke)) as element()*
{
  <DC_NUMER_KLIENTA?>{data($parm/urn:customer/urn2:Customer/urn2:customerNumber)}</DC_NUMER_KLIENTA>
  ,
  <DC_NR_RACHUNKU?>{data($parm/urn:account/urn1:Account/urn1:accountNumber)}</DC_NR_RACHUNKU>
  ,
  (: Parametry obecnie ustawiane na sztywno: :)
  <CI_OPCJA>1</CI_OPCJA>
  ,
  <CI_ID_SYS>1</CI_ID_SYS>
  ,
  <CI_PRODUCT_AREA_ID>3</CI_PRODUCT_AREA_ID>
  ,
  <CI_PRODUCT_CATEGORY_ID>0</CI_PRODUCT_CATEGORY_ID>
  ,
  <DC_NR_RACHUNKU_W_SYSTEMIE/> 
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/urn:header)}
    {getFieldsFromInvoke($body/urn:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>