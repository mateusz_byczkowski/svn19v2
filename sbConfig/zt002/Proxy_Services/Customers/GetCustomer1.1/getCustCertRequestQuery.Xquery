<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-11-30</con:description>
    <con:xquery>declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;

declare function getFieldsFromInvoke($parm as element(ns4:invoke)) as element()*
{
&lt;fml:NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns4:customer/ns2:Customer/ns2:customerNumber)}&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER>
};

  &lt;fml:FML32>
    {getFieldsFromInvoke($body/ns4:invoke)}
  &lt;/fml:FML32></con:xquery>
</con:xqueryEntry>