<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace prim = "http://bzwbk.com/services/prime/";
declare namespace xf="http://bzwbk.com/services/prime/functions";
declare namespace jxf="http://jv.channel.cu.com.pl/cmf/functions";
declare namespace fml = "";
declare namespace none = "";
declare namespace fault="http://schemas.datacontract.org/2004/07/PrimeCustomerLibrary.Structures";
declare namespace rsp="http://schemas.datacontract.org/2004/07/PrimeCustomerLibrary.Structures";
declare namespace wsdl="http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ser-root="http://jv.channel.cu.com.pl/webservice/";
declare namespace wsdl-jv="http://jv.channel.cu.com.pl/cmf/wsdl-jv";

declare variable $timestamp external;

declare function xf:mapErrorCode($errorCode as xs:string) as xs:string
{
   if ($errorCode = '1003') then
      '106'
   else if ($errorCode = '1001') then
      '107'
   else if ($errorCode = '1') then
      '102'
   else
       $errorCode
};

declare function xf:mapTimeStamp($timestamp as xs:string) as xs:string
{
        fn:concat (fn:substring($timestamp,1,10),"-",
                          fn:substring($timestamp,12,2),".",
                          fn:substring($timestamp,15,2),".",
                          fn:substring($timestamp,18),"000")
};


declare function xf:mapResponse($resp as element (prim:SetCustomerDetailsResponse))
	as element (fml:FML32) {

		<fml:FML32>
			{
				<fml:CI_NR_ZRODLOWY_KLIENTA>{data ($resp/prim:SetCustomerDetailsResult/rsp:PeopleSerno)}</fml:CI_NR_ZRODLOWY_KLIENTA>,
				<fml:DC_OPIS_BLEDU />,
				<fml:CI_KOD_BLEDU>0</fml:CI_KOD_BLEDU>,
				<fml:CI_CZAS_AKTUALIZACJI>{xf:mapTimeStamp(data ($resp/prim:SetCustomerDetailsResult/rsp:CiCzasAktualizacji))}</fml:CI_CZAS_AKTUALIZACJI>
			}
		</fml:FML32>
};

declare function xf:mapFault($fau as element(soap-env:Fault))
	as element (fml:FML32) {

	<fml:FML32>
		<fml:CI_KOD_BLEDU>{ data($fau/detail/fault:WbkFault/fault:ErrorCode2) }</fml:CI_KOD_BLEDU>
		<fml:DC_OPIS_BLEDU> { data($fau/detail/fault:WbkFault/fault:ErrorDescription) } </fml:DC_OPIS_BLEDU>
	</fml:FML32>
};

declare function jxf:mapResponse($resp as element(wsdl:changeClientJVResponse))
		as element (fml:FML32)
{
	<fml:FML32>
		<fml:CI_NR_ZRODLOWY_KLIENTA> { data($resp/client-code) } </fml:CI_NR_ZRODLOWY_KLIENTA>
                <fml:CI_CZAS_AKTUALIZACJI>{ data($timestamp) }</fml:CI_CZAS_AKTUALIZACJI>
	</fml:FML32>
};

declare function jxf:mapFault($fault as element(soap-env:Fault))
		as element (fml:FML32)
{
	<fml:FML32>
		<fml:CI_KOD_BLEDU> { xf:mapErrorCode($fault/detail/ser-root:faultResponse/wsdl-jv:faultResponse/status/@error-code) } </fml:CI_KOD_BLEDU>
		<fml:DC_OPIS_BLEDU> { data($fault/detail/ser-root:faultResponse/wsdl-jv:faultResponse/status/error-msg) } </fml:DC_OPIS_BLEDU>
	</fml:FML32>
};


declare function xf:systemFault()
		as element (fml:FML32)
{
	<fml:FML32>
		<fml:CI_KOD_BLEDU>100</fml:CI_KOD_BLEDU>
		<fml:DC_OPIS_BLEDU>Błąd mapowania odpowiedzi na ESB</fml:DC_OPIS_BLEDU>
	</fml:FML32>
};


declare variable $body as element(soap-env:Body) external;


<soap-env:Body>
{
	if (boolean($body/prim:SetCustomerDetailsResponse)) then (
 		xf:mapResponse($body/prim:SetCustomerDetailsResponse)
	) else if (boolean($body/wsdl:changeClientJVResponse)) then (
		jxf:mapResponse($body/wsdl:changeClientJVResponse)
 	) else if (boolean($body/soap-env:Fault/detail/fault:WbkFault/fault:ErrorCode2)) then (
 		xf:mapFault($body/soap-env:Fault)
	) else if (boolean($body/soap-env:Fault/detail/ser-root:faultResponse/wsdl-jv:faultResponse/status)) then (
		jxf:mapFault($body/soap-env:Fault)
	) else xf:systemFault()

}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>