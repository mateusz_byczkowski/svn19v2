<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$4.2011-08-09</con:description>
    <con:xquery><![CDATA[(:Change log
v.1.1  2009-07-10  PKLI  TEET 39294: zmiana mapowania pola accountType
v.1.2  2009-11-04  LKAB  PT 58
v.1.3  2010-03-03  PKLI  TEET 45401 zmiana mapowania pola Account.codeProductSystem
v.1.4  2011-07-01  PKLI  NP1696_2 T51433 Włączenie filtra na obszar/kategorię w trybie wywołania z podanym rachunkiem.
v.1.5  2011-08-08  PKLI  Poprawka do wersji 1.4 Zamiana pola NF_PRODUA_CODEPRODUCTAREA na PT_CODE_PRODUCT_AREA
:)

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:entities.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:be.services.dcl";
declare namespace ns9="urn:baseauxentities.be.dcl";
declare namespace ns10="urn:insurance.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;
declare variable $inputNF_CTRL_AREAS as xs:string* external; (:1.4:)
declare variable $inputNF_ACCOUN_ACCOUNTNUMBER as xs:string* external; (:1.4:)

declare function sourceValue2Boolean($parm as xs:string*,$trueval as xs:string) as xs:string* {
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function insertDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {
      if ($value)
        then if(string-length($value)>5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)>5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };

declare function chgDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {

    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)

    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 


};

declare function getElementsForInsurancePackagesList($parm as element(fml:FML32)) as element()
{
<ns8:insurancePackagesList>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
       if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "4") and (data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ]) = "91"))
     then
    <ns10:InsurancePackageAcc>
      <ns10:number?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}</ns10:number>
	  {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns10:startDate")}
      <ns10:insuranceDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}</ns10:insuranceDescription>
      <ns10:accountRelationshipList>
          <ns3:AccountRelationship>
            <ns3:relationship>
              <ns5:CustomerAccountRelationship>
                <ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}</ns5:customerAccountRelationship>
              </ns5:CustomerAccountRelationship>
            </ns3:relationship>
          </ns3:AccountRelationship>
      </ns10:accountRelationshipList>
      <ns10:productDefinition>
        <ns1:ProductDefinition>
          <ns1:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}</ns1:idProductDefinition>
          <ns1:productGroup>
            <ns1:ProductGroup>
              <ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}</ns1:idProductGroup>
            </ns1:ProductGroup>
          </ns1:productGroup>
        </ns1:ProductDefinition>
      </ns10:productDefinition>
    </ns10:InsurancePackageAcc>
   else()
  }
</ns8:insurancePackagesList>
};

declare function getElementsForInsurancePoliciesList($parm as element(fml:FML32)) as element()
{

<ns8:insurancePoliciesList>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER 
    return
     if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "4") and (data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ]) = "3"))
     then
    <ns10:InsurancePolicyAcc>
      <ns10:policyRefNum?>{data($parm/NF_INSURA_ID[$occ])}</ns10:policyRefNum>
      {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns10:startDate")}
      <ns10:insuranceDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}</ns10:insuranceDescription>
	  <ns10:accountRelationshipList>
          <ns3:AccountRelationship>
            <ns3:relationship>
              <ns5:CustomerAccountRelationship>
                <ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}</ns5:customerAccountRelationship>
              </ns5:CustomerAccountRelationship>
            </ns3:relationship>
          </ns3:AccountRelationship>
      </ns10:accountRelationshipList>
      <ns10:productDefinition>
        <ns1:ProductDefinition>
          <ns1:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}</ns1:idProductDefinition>
          <ns1:productGroup>
            <ns1:ProductGroup>
              <ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}</ns1:idProductGroup>
            </ns1:ProductGroup>
          </ns1:productGroup>
        </ns1:ProductDefinition>
      </ns10:productDefinition>
    </ns10:InsurancePolicyAcc>
   else()
  }
</ns8:insurancePoliciesList>
};

declare function getElementsForAccountListOut($parm as element(fml:FML32)) as element()
{

<ns8:accountListOut>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
        if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="2") or 
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="3") or 
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="10") or
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="13") or
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="14")) then
    <ns0:Account>
      <ns0:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}</ns0:accountNumber>
      <ns0:customerNumber?>{data($parm/NF_CUSTOM_CUSTOMERNUMBER[$occ])}</ns0:customerNumber>
      {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns0:accountOpenDate")}
      <ns0:accountName?>{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}</ns0:accountName>
      <ns0:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}</ns0:currentBalance>
      <ns0:debitPercentage?>{data($parm/NF_ACCOUN_DEBITPERCENTAGE[$occ])}</ns0:debitPercentage>
      <ns0:statementForAccount?>{sourceValue2Boolean(data($parm/NF_ACCOUN_STATEMENTFORACCO[$occ]),"1")}</ns0:statementForAccount>
      <ns0:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}</ns0:accountDescription>
 (:1.3  <ns0:codeProductSystem?>{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}</ns0:codeProductSystem> :)
(:1.3:) <ns0:codeProductSystem?>{data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ])}</ns0:codeProductSystem> 
      <ns0:productCode?>{data($parm/NF_ACCOUN_PRODUCTCODE[$occ])}</ns0:productCode>
      <ns0:availableBalance?>{data($parm/NF_ACCOUN_AVAILABLEBALANCE[$occ])}</ns0:availableBalance>
      <ns0:currentBalancePLN?>{data($parm/NF_ACCOUN_CURRENTBALANCEPL[$occ])}</ns0:currentBalancePLN>
      <ns0:accountRelationshipList>
          <ns3:AccountRelationship>
            <ns3:relationship>
              <ns5:CustomerAccountRelationship>
                <ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}</ns5:customerAccountRelationship>
              </ns5:CustomerAccountRelationship>
            </ns3:relationship>
          </ns3:AccountRelationship>
      </ns0:accountRelationshipList>
       {
         if (data($parm/NF_PRODUA_CODEPRODUCTAREA) = "03") then
           <ns0:loanAccount>
             <ns0:LoanAccount>
               <ns0:dateOfLastTrans?>{data($parm/NF_LOANA_DATEOFLASTTRANS[$occ])}</ns0:dateOfLastTrans>
             </ns0:LoanAccount>
           </ns0:loanAccount>
         else ()
      }
      {
          if (data($parm/NF_PRODUA_CODEPRODUCTAREA) = "10") then
     <ns0:timeAccount>
        <ns0:TimeAccount>
          <ns0:renewalFrequency?>{data($parm/NF_TIMEA_RENEWALFREQUENCY[$occ])}</ns0:renewalFrequency>
		  {insertDate(data($parm/NF_TIMEA_NEXTRENEWALMATURI[$occ]),"yyyy-MM-dd","ns0:nextRenewalMaturityDate")}
          <ns0:interestDisposition?>{sourceValue2Boolean(data($parm/NF_TIMEA_INTERESTDISPOSITI[$occ]),"1")}</ns0:interestDisposition>
          <ns0:renewalID>
            <ns5:RenewalType>
              <ns5:renewalType?>{data($parm/NF_RENEWT_RENEWALTYPE[$occ])}</ns5:renewalType>
            </ns5:RenewalType>
          </ns0:renewalID>
          <ns0:renewalPeriod>
            <ns5:Period>
              <ns5:period?>{data($parm/NF_PERIOD_PERIOD[$occ])}</ns5:period>
            </ns5:Period>
          </ns0:renewalPeriod>
        </ns0:TimeAccount>
      </ns0:timeAccount>
     else()
     }

      <ns0:accountType>
        <ns5:AccountType>
         (: T39249 <ns5:accountType?>{data($parm/NF_ACCOUN_ACCOUNTTYPE[$occ])}</ns5:accountType> :)
             <ns5:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}</ns5:accountType>  (: T39249 :)
        </ns5:AccountType>
      </ns0:accountType>
      <ns0:currency>
        <ns5:CurrencyCode>
          <ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns5:currencyCode>
        </ns5:CurrencyCode>
      </ns0:currency>
      <ns0:accountStatus>
        <ns2:AccountStatus>
            <ns2:accountStatus?>{data($parm/NF_PRODUCT_STATUSCODE[$occ])}</ns2:accountStatus>
        </ns2:AccountStatus>
      </ns0:accountStatus>
      <ns0:accountBranchNumber>
        <ns5:BranchCode>
          <ns5:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE[$occ])}</ns5:branchCode>
        </ns5:BranchCode>
      </ns0:accountBranchNumber>
	  <ns0:productDefinition>
        <ns1:ProductDefinition>
          <ns1:idProductDefinition?>{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}</ns1:idProductDefinition>
          <ns1:productGroup>
            <ns1:ProductGroup>
              <ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}</ns1:idProductGroup>
            </ns1:ProductGroup>
          </ns1:productGroup>
        </ns1:ProductDefinition>
      </ns0:productDefinition>
    </ns0:Account>
   else ()
  }
</ns8:accountListOut>
};

(:1.4 start:)
declare function areasMatch($productArea as xs:string, $inputAreaFilter as xs:string) as xs:string* {
  if (string-length($productArea) = 1)
  then (
    if (fn:contains($inputAreaFilter, concat("0", $productArea)))
       then "true"
       else "false")
  else (
    if (fn:contains($inputAreaFilter, $productArea))
       then "true"
       else "false"
)
};
(:1.4 end:)

(:1.4 uwaga! poniższa funkcja getElementsForInvokeResponse została przebudowana w ramach zmiany 1.4:)
declare function getElementsForInvokeResponse($parm as element(fml:FML32), $inputNF_ACCOUN_ACCOUNTNUMBER as xs:string*, $inputAreaFilter as xs:string*) as element()*
{
<ns8:invokeResponse>
{
  if (string-length($inputNF_ACCOUN_ACCOUNTNUMBER) > 0 and 
      string-length($inputNF_CTRL_AREAS) > 0 and 
      $inputNF_CTRL_AREAS != '00' and
      (:$parm/NF_PRODUA_CODEPRODUCTAREA[1] and 
      areasMatch (data($parm/NF_PRODUA_CODEPRODUCTAREA[1]), $inputAreaFilter) = "false" ) 1.5 poprawiono :) 
      $parm/PT_CODE_PRODUCT_AREA[1] and (: 1.5 poprawiono :) 
      areasMatch (data($parm/PT_CODE_PRODUCT_AREA[1]), $inputAreaFilter) = "false" ) (: 1.5 poprawiono :) 
then (
 <ns8:insurancePackagesList/>,
 <ns8:insurancePoliciesList/>,
 <ns8:accountListOut/>,
 <ns8:bcd>
    <ns6:BusinessControlData>
      <ns6:pageControl>
        <ns9:PageControl>
          <ns9:hasNext>false</ns9:hasNext>
          <ns9:navigationKeyValue/>
        </ns9:PageControl>
      </ns6:pageControl>
    </ns6:BusinessControlData>
  </ns8:bcd>
)
else (
        getElementsForInsurancePackagesList($parm),
        getElementsForInsurancePoliciesList($parm),
        getElementsForAccountListOut($parm),
  <ns8:bcd>
    <ns6:BusinessControlData>
      <ns6:pageControl>
        <ns9:PageControl>
          <ns9:hasNext?>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}</ns9:hasNext>
          <ns9:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns9:navigationKeyDefinition>
          <ns9:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns9:navigationKeyValue>
        </ns9:PageControl>
      </ns6:pageControl>
    </ns6:BusinessControlData>
  </ns8:bcd>
)
}
</ns8:invokeResponse>
};

<soap:Body>
(:1.4 uwaga! poniższa funkcja getElementsForInvokeResponse została przebudowana w ramach zmiany 1.4:)
  {getElementsForInvokeResponse($body/FML32, $inputNF_ACCOUN_ACCOUNTNUMBER, $inputNF_CTRL_AREAS)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>