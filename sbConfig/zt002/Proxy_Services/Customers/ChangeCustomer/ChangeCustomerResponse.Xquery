<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapChangeCustomerResponse($fml as element(fml:FML32))
	as element(m:ChangeCustomerResponse) {
		<m:ChangeCustomerResponse>
			{

				let $DC_OPIS_BLEDU := $fml/fml:DC_OPIS_BLEDU
				for $it at $p in $fml/fml:DC_OPIS_BLEDU
				return
					<m:ChangeCustomer>
					{
						if($DC_OPIS_BLEDU[$p])
							then <m:OpisBledu>{ data($DC_OPIS_BLEDU[$p]) }</m:OpisBledu>
						else ()
					}
					</m:ChangeCustomer>
			}

		</m:ChangeCustomerResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapChangeCustomerResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>