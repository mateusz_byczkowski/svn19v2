<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns4 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/GetCustomerAllData/ResponseTransform2/";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace ns-1 = "urn:baseentities.be.dcl";
declare namespace ns1 = "urn:basedictionaries.be.dcl";
declare namespace ns3 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns6 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace ns7 = "urn:cifdict.dictionaries.be.dcl";


declare function local:intToBoolean($tn as xs:string?)
   as xs:string {
if($tn) then
	if ($tn eq '') then ''
	else xs:string($tn ne '0')
else ''
};

declare function local:DMYToYMD($dateDMY as xs:string?)
   as xs:string {
   if($dateDMY) then
	   if ($dateDMY eq '') then ''
	   else fn:string-join((fn:substring($dateDMY, 7, 4), fn:substring($dateDMY, 4, 2),fn:substring($dateDMY, 1, 2)), '-')
   else ''
};


declare function xf:ResponseTransform2($cISGetCustomerResponse1 as element(ns3:CISGetCustomerResponse),
    									 $invoke1 as element(ns4:invoke) )
    as element(ns4:invokeResponse) {
        <ns4:invokeResponse>
        
            <ns4:messageHelper>
                <ns-1:MessageHelper>
                    <ns-1:errorCode?></ns-1:errorCode>
                    <ns-1:message?></ns-1:message>
                </ns-1:MessageHelper>
            </ns4:messageHelper>

            <ns4:customer>
                <ns0:Customer>

                	{
                	for $RezNierez in $cISGetCustomerResponse1/ns3:RezNierez
                	where $RezNierez != ''
                	return
                    	<ns0:resident?>{ local:intToBoolean( data($RezNierez) ) }</ns0:resident>
					}
                    <ns0:taxID?>{ data($cISGetCustomerResponse1/ns3:Nip) }</ns0:taxID>
                    <ns0:phoneNo?>{ data($cISGetCustomerResponse1/ns3:NrTelefonu) }</ns0:phoneNo>
                    <ns0:mobileNo?>{ data($cISGetCustomerResponse1/ns3:NrTelefKomorkowego) }</ns0:mobileNo>
                    <ns0:email?>{ data($cISGetCustomerResponse1/ns3:AdresEMail) }</ns0:email>
                    {
                   for $companyType in $invoke1/ns4:customer/ns0:Customer/ns0:companyID/ns2:CompanyType/ns2:companyType 
                    where $companyType = 1
                     return
                     <ns0:businessNo?>{data($cISGetCustomerResponse1/ns3:NrTelefonuPrac)}</ns0:businessNo>
                    }
                   {
                   for $companyType in $invoke1/ns4:customer/ns0:Customer/ns0:companyID/ns2:CompanyType/ns2:companyType 
                    where $companyType = 7
                     return
                     <ns0:businessNo?>{data($cISGetCustomerResponse1/ns3:NumerTelefonuPraca)}</ns0:businessNo>                                                            
                    }
                    <ns0:agreementBankInfo>{ local:intToBoolean( data($cISGetCustomerResponse1/ns3:ZgodaTajemBank) ) }</ns0:agreementBankInfo>
                    <ns0:addressList>
                        <ns0:Address>
                            <ns0:houseFlatNumber?>{ data($cISGetCustomerResponse1/ns3:NrPosesLokaluDanePodst) }</ns0:houseFlatNumber>
                            <ns0:city?>{ data($cISGetCustomerResponse1/ns3:MiastoDanePodst) }</ns0:city>
                            {
                            for $AdresOd in $cISGetCustomerResponse1/ns3:AdresOd
                            where $AdresOd!= ''
                            return
                            	<ns0:validFrom>{local:DMYToYMD( data($AdresOd))} </ns0:validFrom>
                            }
                            {
                            for $AdresDo in $cISGetCustomerResponse1/ns3:AdresDo
                            where $AdresDo!= ''
                            return
                            	<ns0:validTo?>{ local:DMYToYMD( data($AdresDo) ) }</ns0:validTo>
                            }
                            <ns0:street?>{ data($cISGetCustomerResponse1/ns3:UlicaDanePodst) }</ns0:street>
                            <ns0:zipCode?>{ data($cISGetCustomerResponse1/ns3:KodPocztowyDanePodst) }</ns0:zipCode>
                        	
                        	<ns0:addressType>
                        		<ns2:AddressType>
                        			<ns2:addressType>1</ns2:addressType>
                    			</ns2:AddressType>
                        	</ns0:addressType>
                            {
                            	for $wojew in $cISGetCustomerResponse1/ns3:Wojewodztwo
                            	return
		                            <ns0:state>
		                                <ns2:State>
			                                <ns2:state>{ data($wojew) }</ns2:state>
		                                </ns2:State>
		                            </ns0:state>
                            }
                            {
                            	for $kraj in $cISGetCustomerResponse1/ns3:KodKraju
                            	return
		                            <ns0:countryId>
		                                <ns2:CountryCode>
			                                <ns2:isoCountryCode>{ xs:string( data($kraj) ) }</ns2:isoCountryCode>
		                                </ns2:CountryCode>
		                            </ns0:countryId>
                            }
                            {
                            	for $typAdresu in $cISGetCustomerResponse1/ns3:TypAdresu
                            	return
		                            <ns0:typeOfLocation>
		                            	<ns2:AddressTypeOfLocation>
		                            		<ns2:addressTypeOfLocation>{data($typAdresu)}</ns2:addressTypeOfLocation>
		                            	</ns2:AddressTypeOfLocation>
		                            </ns0:typeOfLocation>
                            }
                        </ns0:Address>
                        {
                        	for $adresKoresp in $cISGetCustomerResponse1/ns3:CISGetCustomerAdresAlt
                        	return
		                        <ns0:Address>
		                            <ns0:houseFlatNumber?>{ data($adresKoresp/ns3:NrPosesLokaluAdresAlt) }</ns0:houseFlatNumber>
		                            <ns0:city?>{ data($adresKoresp/ns3:MiastoAdresAlt) }</ns0:city>
		                            {
		                            for $validFrom in $adresKoresp/ns3:DataOd
		                            where $validFrom!=''
		                            return
		                            	<ns0:validFrom>{ local:DMYToYMD( data($validFrom) ) }</ns0:validFrom>
		                            }
             						{
		                            for $validTo in $adresKoresp/ns3:DataDo
		                            where $validTo!=''
		                            return
		                            	<ns0:validTo>{ local:DMYToYMD( data($validTo) ) }</ns0:validTo>
		                            }
		                            <ns0:street?>{ data($adresKoresp/ns3:UlicaAdresAlt) }</ns0:street>
		                            <ns0:zipCode?>{ data($adresKoresp/ns3:KodPocztowyAdresAlt) }</ns0:zipCode>
		                        	<ns0:addressType>
		                        		<ns2:AddressType>
		                        			<ns2:addressType>2</ns2:addressType>
		                    			</ns2:AddressType>
		                        	</ns0:addressType>
		                            {
		                            	for $wojew in $adresKoresp/ns3:WojewodztwoKrajAdresAlt
		                            	return
				                            <ns0:state>
				                                <ns2:State>
					                                <ns2:state>{ data($wojew) }</ns2:state>
				                                </ns2:State>
				                            </ns0:state>
		                            }
		                            {
		                            	for $kraj in $adresKoresp/ns3:KodKrajuKoresp
		                            	return
				                            <ns0:countryId>
				                                <ns2:CountryCode>
					                                <ns2:isoCountryCode>{ xs:string( data($kraj) ) }</ns2:isoCountryCode>
				                                </ns2:CountryCode>
				                            </ns0:countryId>
		                            }
		                            {
		                            	for $typAdresu in $adresKoresp/ns3:TypAdresuKoresp
		                            	return
				                            <ns0:typeOfLocation>
				                            	<ns2:AddressTypeOfLocation>
				                            		<ns2:addressTypeOfLocation>{data($typAdresu)}</ns2:addressTypeOfLocation>
				                            	</ns2:AddressTypeOfLocation>
				                            </ns0:typeOfLocation>
		                            }
		                        </ns0:Address>
                        }
                    </ns0:addressList>
                    {
	                    for $SeriaNrDok in $cISGetCustomerResponse1/ns3:SeriaNrDok
	                    return
	                    	<ns0:documentList>
	            				<ns0:Document>
	                        		<ns0:documentNumber>{ data($SeriaNrDok) }</ns0:documentNumber>
		                            {
		                            	for $DokTozsamosci in $cISGetCustomerResponse1/ns3:DokTozsamosci
		                            	return
			                                <ns0:documentType>
	                                			<ns2:CustomerDocumentType>
			                                    	<ns2:customerDocumentType?>{ data($DokTozsamosci) }</ns2:customerDocumentType>
	                                			</ns2:CustomerDocumentType>
	                            			</ns0:documentType>
                            		}
                        		</ns0:Document>
                    		</ns0:documentList>
                    }


                    {
                        for $KodZawodu in $cISGetCustomerResponse1/ns3:KodZawoduWyuczonego
                        return
                        	<ns0:customerEmploymentInfoList>
                        		<ns0:CustomerEmploymentInfo>
                            		<ns0:occupationCode>
                                		<ns2:CustomerOccupationCode>
                            				<ns2:customerOccupationCode>{ xs:string( data($KodZawodu) ) }</ns2:customerOccupationCode>
                                		</ns2:CustomerOccupationCode>
                            		</ns0:occupationCode>
                        		</ns0:CustomerEmploymentInfo>
                    		</ns0:customerEmploymentInfoList>
                    }
                    <ns0:customerInsuranceList>
                        <ns0:CustomerInsurance>
                        	{
	                        	let $policyInvest := $cISGetCustomerResponse1/ns3:UbezpFundInwest
	                        	return
		                        	if ($policyInvest != '') then
			                        	<ns0:policyInvest?>{ local:intToBoolean( data($policyInvest) ) }</ns0:policyInvest>
			                        else ''
	                        }
                        	{
	                        	let $policyLife := $cISGetCustomerResponse1/ns3:UbezpieczenieNaZycie
	                        	return
		                        	if ($policyLife != '') then
			                        	<ns0:policyLife?>{ local:intToBoolean( data($policyLife) ) }</ns0:policyLife>
			                        else ''
	                        }
                       		{
	                        	let $policyOther := $cISGetCustomerResponse1/ns3:UbezpieczeniaInne
	                        	return
		                        	if ($policyOther != '') then
			                        	<ns0:policyOther?>{ local:intToBoolean( data($policyOther) ) }</ns0:policyOther>
			                        else ''
	                        }
	                        {
	                        let $policyTotalAmount := $cISGetCustomerResponse1/ns3:SumaPolisNaZycie
	                        return 
	                        	if ($policyTotalAmount != '') then
	                        		<ns0:policyTotalAmount?>{ xs:double( data($policyTotalAmount) ) }</ns0:policyTotalAmount>
	                        	else ''
	                        }
	                        {
	                        let $policyPremiumSum := $cISGetCustomerResponse1/ns3:SumaSkladekPolis
	                        return 
	                        	if ($policyPremiumSum != '') then
	                        		<ns0:policyPremiumSum?>{ xs:double( data($policyPremiumSum) ) }</ns0:policyPremiumSum>
	                        	else ''
	                        }
	                        {
	                        let $policyNo := $cISGetCustomerResponse1/ns3:LiczbaPolis
	                        return 
	                        	if ($policyNo != '') then
	                        		<ns0:policyNo?>{ xs:int( data($policyNo) ) }</ns0:policyNo>
	                        	else ''
	                        }
	                        <ns0:policyInvestName?>
	                        	<ns6:InsurerName?>
	                        		<ns6:insurerName?>{ data($cISGetCustomerResponse1/ns3:NazwaUbezpFundInwest) }</ns6:insurerName>
	                        	</ns6:InsurerName>
	                        </ns0:policyInvestName>
	                        <ns0:policyLifeName?>
	                        	<ns6:InsurerName?>
	                        		<ns6:insurerName?>{ data($cISGetCustomerResponse1/ns3:UbezpNaZycie) }</ns6:insurerName>
	                        	</ns6:InsurerName>
	                        </ns0:policyLifeName>
	                        <ns0:policyOtherName?>
	                        	<ns6:InsurerName?>
	                        		<ns6:insurerName?>{ data($cISGetCustomerResponse1/ns3:UbezpInne) }</ns6:insurerName>
	                        	</ns6:InsurerName>
	                        </ns0:policyOtherName>	                        
                        </ns0:CustomerInsurance>
                    </ns0:customerInsuranceList>
                    <ns0:customerAdditionalInfo>
                    	 <ns0:CustomerAdditionalInfo>
                            {
                            for $KodJezyka in $cISGetCustomerResponse1/ns3:KodJezyka
                            return
	                            <ns0:language>
	                                <ns1:Language>
                                        <ns1:languageID>{ data($KodJezyka) }</ns1:languageID>
	                                </ns1:Language>
	                            </ns0:language>                                    
                            }
                            {
                            for $ZrodloDanych in $cISGetCustomerResponse1/ns3:ZrodloDanych 
                            return 
	                            <ns0:dataSource>
		                         	<ns2:DataSource>
		                         		<ns2:dataSource>{ data($ZrodloDanych) }</ns2:dataSource>
		                         	</ns2:DataSource>
		                         </ns0:dataSource>
	                       }
                    	 </ns0:CustomerAdditionalInfo>
                    </ns0:customerAdditionalInfo>
                    {
                    for $ZrodloDochodu in $cISGetCustomerResponse1/ns3:ZrodloDochodu
                    where $ZrodloDochodu != ''
                    return                    
                    	<ns0:customerFinancialInfo>
                	 		<ns0:CustomerFinancialInfo>
	                            <ns0:sourceOfIncomeCode>
	                                <ns2:SourceOfIncomeCode>
	                                    <ns2:sourceOfIncomeCode>{ data($ZrodloDochodu) }</ns2:sourceOfIncomeCode>
	                                </ns2:SourceOfIncomeCode>
	                            </ns0:sourceOfIncomeCode>
                           	 </ns0:CustomerFinancialInfo>
            			</ns0:customerFinancialInfo>
               		}
                    <ns0:customerPersonal>
                        <ns0:CustomerPersonal>
                            <ns0:birthPlace?>{ data($cISGetCustomerResponse1/ns3:MiejsceUrodzenia) }</ns0:birthPlace>
                            <ns0:motherMaidenName?>{ data($cISGetCustomerResponse1/ns3:NazwiskoPanienskieMatki) }</ns0:motherMaidenName>
                            {
                            for $dateOfBirth in $cISGetCustomerResponse1/ns3:DataUrodzenia
                            where $dateOfBirth!=''
                            return
                            	<ns0:dateOfBirth>{local:DMYToYMD( data($dateOfBirth) ) }</ns0:dateOfBirth>
                            }
                            <ns0:lastName?>{ data($cISGetCustomerResponse1/ns3:Nazwisko) }</ns0:lastName>
                            <ns0:secondName?>{ data($cISGetCustomerResponse1/ns3:DrugieImie) }</ns0:secondName>
                            <ns0:pesel?>{ data($cISGetCustomerResponse1/ns3:NrPesel) }</ns0:pesel>
                            <ns0:firstName?>{ data($cISGetCustomerResponse1/ns3:Imie) }</ns0:firstName>
                            <ns0:identityCardNumber?>{ data($cISGetCustomerResponse1/ns3:NrDowoduRegon) }</ns0:identityCardNumber>
                            <ns0:passportNumber?>{ data($cISGetCustomerResponse1/ns3:NumerPaszportu) }</ns0:passportNumber>
                            

                            {
                                for $Plec in $cISGetCustomerResponse1/ns3:Plec
                                return
		                            <ns0:sexCode>
		                                <ns2:CustomerSexCode>
		                                    <ns2:customerSexCode>{ data($Plec) }</ns2:customerSexCode>
		                                </ns2:CustomerSexCode>
		                            </ns0:sexCode>
                           }
                           {
		                        for $KrajPochodzenia in $cISGetCustomerResponse1/ns3:KrajPochodzenia
		                        return
		                            <ns0:countryOfOrigin>
		                                <ns2:CountryCode>
                                            <ns2:isoCountryCode>{ data($KrajPochodzenia) }</ns2:isoCountryCode>
                                		</ns2:CountryCode>
                            		</ns0:countryOfOrigin>
                            }
                            {
	                            for $Obywatelstwo in $cISGetCustomerResponse1/ns3:Obywatelstwo
	                            return
		                            <ns0:citizenship>
		                            	<ns2:CitizenshipCode>
		                            		<ns2:isoCitizenshipCode>{ data($Obywatelstwo) }</ns2:isoCitizenshipCode>
		                        		</ns2:CitizenshipCode>
		                            </ns0:citizenship>
                            }
                            {
	                            for $KrajZamieszkania in $cISGetCustomerResponse1/ns3:KrajZamieszkania
	                            return
                            		<ns0:countryOfResidence>
                                		<ns2:CountryCode>
                                            <ns2:isoCountryCode>{ data($KrajZamieszkania) }</ns2:isoCountryCode>
                                		</ns2:CountryCode>
                            		</ns0:countryOfResidence>
                            }


                        </ns0:CustomerPersonal>
                    </ns0:customerPersonal>
                    {
	                    for $NumerOddzialu in $cISGetCustomerResponse1/ns3:NumerOddzialu
	                    return
		                    <ns0:branchOfOwnership>
		                        <ns2:BranchCode>
                                    <ns2:branchCode>{ xs:string( data($NumerOddzialu) ) }</ns2:branchCode>
		                        </ns2:BranchCode>
		                    </ns0:branchOfOwnership>
                    }
                    {
	                    for $TypKlienta in $cISGetCustomerResponse1/ns3:TypKlienta
	                    return
		                    <ns0:customerType>
		                        <ns2:CustomerType>
                                    <ns2:customerType>{ xs:string( data($TypKlienta) ) }</ns2:customerType>
		                        </ns2:CustomerType>
		                    </ns0:customerType>
                    }
                    {
                    	for $approval in $cISGetCustomerResponse1/ns3:UdostepGrupa
                    	where $approval != ''
                    	return
		                    <ns0:processingApproval?>
		                    	<ns2:CustomerProcessingApproval?>
		                    		<ns2:customerProcessingApproval?>{ data($approval) }</ns2:customerProcessingApproval>
		                    	</ns2:CustomerProcessingApproval>
		                    </ns0:processingApproval>
                    }
                    {
	                    for $KartaWzorowPodpisow in $cISGetCustomerResponse1/ns3:KartaWzorowPodpisow
	                    return
		                    <ns0:signatureCardBranchId>
		                        <ns2:BranchCode>
                                    <ns2:branchCode>{ data($KartaWzorowPodpisow) }</ns2:branchCode>
		                        </ns2:BranchCode>
		                    </ns0:signatureCardBranchId>
                    }
                    {
                    	for $approval in $cISGetCustomerResponse1/ns3:ZgodaInfHandlowa
                    	where $approval != ''
                    	return
		                    <ns0:agreementCode?>
		                    	<ns7:CustomerAgreementCode?>
		                    		<ns7:customerAgreementCode?>{ data($approval) }</ns7:customerAgreementCode>
		                    	</ns7:CustomerAgreementCode>
		                    </ns0:agreementCode>
                    }
                </ns0:Customer>
            </ns4:customer>
        </ns4:invokeResponse>
};
declare variable $invoke1 as element(ns4:invoke)  external;
declare variable $cISGetCustomerResponse1 as element(ns3:CISGetCustomerResponse) external;

xf:ResponseTransform2($cISGetCustomerResponse1, $invoke1)]]></con:xquery>
</con:xqueryEntry>