<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>TEET 51746. Poprawka w funkcji xf:toBoolean oraz w polu employment.</con:description>
    <con:xquery><![CDATA[declare namespace xf = "urn:be.services.dcl/mappings";
declare namespace m = "urn:be.services.dcl";
declare namespace m1 = "urn:cif.entities.be.dcl";
declare namespace m2 = "urn:dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/"; 
 
declare variable $body external;
 
declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value < 10) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value < 10) 
         then fn:concat("000",$string)
      else if ($value < 100) 
         then fn:concat("00",$string)
      else if ($value < 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
    fn:concat (xf:convertTo4CharString(fn:year-from-date($dateIn)),"-",
               xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
               xf:convertTo2CharString(fn:day-from-date($dateIn)))
};

declare function xf:toBoolean($dateIn as xs:string) as xs:boolean {
  if ($dateIn = "T" or $dateIn = "1") 
    then true()
    else false()
};

<soap-env:Body>
  {
  let $fml := $body/FML32
  return

  <m:invokeResponse xmlns:urn="urn:be.services.dcl">
    <m:customer xmlns:urn1="urn:cif.entities.be.dcl" xmlns:urn2="urn:dictionaries.be.dcl">
      <m1:Customer>
        {if($fml/CI_NAZWA_PELNA and xf:isData($fml/CI_NAZWA_PELNA))
            then <m1:companyName>{ data($fml/CI_NAZWA_PELNA) }</m1:companyName>
            else ()
        }
        {if($fml/DC_NUMER_KLIENTA and xf:isData($fml/DC_NUMER_KLIENTA))
            then <m1:customerNumber>{ data($fml/DC_NUMER_KLIENTA) }</m1:customerNumber>
            else ()
        }
        {if($fml/DC_DATA_WPROWADZENIA and xf:isData($fml/DC_DATA_WPROWADZENIA))
            then <m1:dateCustomerOpened>{ xf:mapDate($fml/DC_DATA_WPROWADZENIA) }</m1:dateCustomerOpened>
            else ()
        }
        {if($fml/CI_DATA_AKTUALIZACJI and xf:isData($fml/CI_DATA_AKTUALIZACJI))
            then <m1:fileMaintenanceLastDate>{ xf:mapDate($fml/CI_DATA_AKTUALIZACJI) }</m1:fileMaintenanceLastDate>
            else ()
        }
        {if($fml/DC_REZ_NIEREZ and xf:isData($fml/DC_REZ_NIEREZ))
            then <m1:resident>{boolean(xs:integer(data($fml/DC_REZ_NIEREZ)))}</m1:resident>
            else ()
        }
        {if($fml/DC_NIP and xf:isData($fml/DC_NIP))
            then <m1:taxID>{ data($fml/DC_NIP) }</m1:taxID>
            else ()
        }
        {if($fml/DC_NAZWA_SKROCONA and xf:isData($fml/DC_NAZWA_SKROCONA))
            then <m1:shortName>{ data($fml/DC_NAZWA_SKROCONA) }</m1:shortName>
            else (if($fml/CI_NAZWA_PELNA and xf:isData($fml/CI_NAZWA_PELNA))
                    then <m1:shortName>{ data($fml/CI_NAZWA_PELNA) }</m1:shortName>
                    else ()
                  )
        }
        {if($fml/CI_DECYZJA and xf:isData($fml/CI_DECYZJA))
            then <m1:crmDecision>{ data($fml/CI_DECYZJA) }</m1:crmDecision>
            else ()
        }
        {if($fml/CI_DATA_NIP and xf:isData($fml/CI_DATA_NIP))
            then <m1:taxIdDate>{ xf:mapDate($fml/CI_DATA_NIP) }</m1:taxIdDate>
            else ()
        }

        <m1:addressList>
          {for $it at $a in $fml/CI_MIASTO
            return
              <m1:Address>
                {if($fml/CI_NR_POSES_LOKALU[$a] and xf:isData($fml/CI_NR_POSES_LOKALU[$a]))
                    then <m1:houseFlatNumber>{ data($fml/CI_NR_POSES_LOKALU[$a]) }</m1:houseFlatNumber>
                    else ()
                }
                {if($fml/CI_MIASTO[$a] and xf:isData($fml/CI_MIASTO[$a]))
                    then <m1:city>{ data($fml/CI_MIASTO[$a]) }</m1:city>
                    else ()
                }
                {if($fml/CI_WOJEWODZTWO[$a] and xf:isData($fml/CI_WOJEWODZTWO[$a]))
                    then <m1:county>{ data($fml/CI_WOJEWODZTWO[$a]) }</m1:county>
                    else ()
                }
                {if($fml/CI_ULICA[$a] and xf:isData($fml/CI_ULICA[$a]))
                    then <m1:street>{ data($fml/CI_ULICA[$a]) }</m1:street>
                    else ()
                }
                {if($fml/CI_KOD_POCZTOWY[$a] and xf:isData($fml/CI_KOD_POCZTOWY[$a]))
                    then <m1:zipCode>{ data($fml/CI_KOD_POCZTOWY[$a]) }</m1:zipCode>
                    else ()
                }
                {if($fml/CI_NAZWA_KORESP[$a] and xf:isData($fml/CI_NAZWA_KORESP[$a]))
                    then <m1:name>{ data($fml/CI_NAZWA_KORESP[$a]) }</m1:name>
                    else ()
                }
                {if($fml/CI_TYP_ADRESU[$a] and xf:isData($fml/CI_TYP_ADRESU[$a]))
                    then 
                      <m1:addressType>
                      <m2:AddressType>
                      <m2:addressType>{data($fml/CI_TYP_ADRESU[$a])}</m2:addressType>
                      </m2:AddressType>
                      </m1:addressType>
                    else ()
                }
                {if($fml/CI_KOD_KRAJU[$a] and xf:isData($fml/CI_KOD_KRAJU[$a]))
                    then
                      <m1:countryId>
                      <m2:CountryCode>
                      <m2:isoCountryCode>{ data($fml/CI_KOD_KRAJU[$a]) }</m2:isoCountryCode>
                      </m2:CountryCode>
                      </m1:countryId>
                    else ()
                }
              </m1:Address> 
          }
        </m1:addressList>

        <m1:documentList>
          {if($fml/DC_NR_DOWODU_REGON and $fml/DC_TYP_KLIENTA and xf:isData($fml/DC_TYP_KLIENTA) and $fml/DC_TYP_KLIENTA!='P')
            then
              for $it at $a in $fml/DC_NR_DOWODU_REGON
                return
                  <m1:Document>
                    {if($fml/DC_NR_DOWODU_REGON[$a] and xf:isData($fml/DC_NR_DOWODU_REGON[$a]))
                        then (<m1:documentNumber>{ data($fml/DC_NR_DOWODU_REGON[$a]) }</m1:documentNumber>,
                          <m1:documentType>
                          <m2:CustomerDocumentType>
                          <m2:customerDocumentType>D</m2:customerDocumentType>
                          </m2:CustomerDocumentType>
                          </m1:documentType>)
                        else ()
                    }
                  </m1:Document> 
            else ()
          }         
        </m1:documentList>

        <m1:customerEmploymentInfoList>
          {for $it at $a in $fml/DC_DATA_PODJECIA_PRACY
            return
              <m1:CustomerEmploymentInfo>
                {if($fml/CI_NAZWA_PRACODAWCY[$a] and xf:isData($fml/CI_NAZWA_PRACODAWCY[$a]))
                    then <m1:employment>{ data($fml/CI_NAZWA_PRACODAWCY[$a]) }</m1:employment>
                    else ()
                }
                {if($fml/DC_DATA_PODJECIA_PRACY[$a] and xf:isData($fml/DC_DATA_PODJECIA_PRACY[$a]))
                    then <m1:employmentStartDate>{ xf:mapDate($fml/DC_DATA_PODJECIA_PRACY[$a]) }</m1:employmentStartDate>
                    else ()
                }
                {if($fml/DC_KOD_EKD_PRACODAW[$a] and xf:isData($fml/DC_KOD_EKD_PRACODAW[$a]))
                    then 
                      <m1:employerEKDCode>
                      <m2:StandardIndustryCode>
                      <m2:standardIndustryCode>{ data($fml/DC_KOD_EKD_PRACODAW[$a]) }</m2:standardIndustryCode>
                      </m2:StandardIndustryCode>
                      </m1:employerEKDCode>
                    else ()
                }
                {if($fml/CI_STATUS_PRACOWNIKA[$a] and xf:isData($fml/CI_STATUS_PRACOWNIKA[$a]))
                    then
                      <m1:customerWorkerStatus>
                      <m2:CustomerWorkerStatus>
                      <m2:customerWorkerStatus>{ data($fml/CI_STATUS_PRACOWNIKA[$a]) }</m2:customerWorkerStatus>
                      </m2:CustomerWorkerStatus>
                      </m1:customerWorkerStatus>
                    else ()
                }
              </m1:CustomerEmploymentInfo> 
          }
        </m1:customerEmploymentInfoList>


        <m1:customerRelationshipList>
          {for $it at $a in $fml/DC_TYP_RELACJI
            return
              <m1:CustomerRelationship>
                {if($fml/DC_NUMER_KLIENTA_REL[$a] and xf:isData($fml/DC_NUMER_KLIENTA_REL[$a]))
                    then <m1:customerID>{ data($fml/DC_NUMER_KLIENTA_REL[$a]) }</m1:customerID>
                    else ()
                }
                {if($fml/DC_TYP_RELACJI[$a] and xf:isData($fml/DC_TYP_RELACJI[$a]))
                    then 
                      <m1:relationType>
                      <m2:CustomerRelations>
                      <m2:customerRelations>{ data($fml/DC_TYP_RELACJI[$a]) }</m2:customerRelations>
                      </m2:CustomerRelations>
                      </m1:relationType>
                    else ()
                }
              </m1:CustomerRelationship> 
          }
        </m1:customerRelationshipList>


        <m1:customerAdditionalInfo>
          <m1:CustomerAdditionalInfo>
            {if($fml/DC_NUMER_KLIENTA and xf:isData($fml/DC_NUMER_KLIENTA))
                then <m1:externalCustomerNumber>{ data($fml/DC_NUMER_KLIENTA) }</m1:externalCustomerNumber>
                else ()
            }
 
(:            {if($fml/DC_KARTA_WZOROW_PODPISOW and xf:isData($fml/DC_KARTA_WZOROW_PODPISOW))
                then 
                  <m1:specimenSignatureCard>
                  <m2:SpecimenSignatureCard>
                  <m2:specimenSignatureCard>{ data($fml/DC_KARTA_WZOROW_PODPISOW) }</m2:specimenSignatureCard>
                  </m2:SpecimenSignatureCard>
                  </m1:specimenSignatureCard>
                else ()
            }
:)
            <m1:corporateUnit>
              <m2:CustomerCorporateUnit>
                {if($fml/DC_JEDNOSTKA_KORPORACYJNA and xf:isData($fml/DC_JEDNOSTKA_KORPORACYJNA))
                    then <m2:customerCorporateUnit>{ data($fml/DC_JEDNOSTKA_KORPORACYJNA) }</m2:customerCorporateUnit>
                    else ()
                } 
                {if($fml/DC_KOD_JEZYKA and xf:isData($fml/DC_KOD_JEZYKA))
                    then <m2:language>{ data($fml/DC_KOD_JEZYKA) }</m2:language>
                    else ()
                }
              </m2:CustomerCorporateUnit>
            </m1:corporateUnit>

            {if($fml/DC_ZRODLO_DANYCH and xf:isData($fml/DC_ZRODLO_DANYCH))
                then 
                  <m1:dataSource>
                  <m2:DataSource>
                  <m2:dataSource>{ data($fml/DC_ZRODLO_DANYCH) }</m2:dataSource>
                  </m2:DataSource>
                  </m1:dataSource>
                else ()
            }
            {if($fml/CI_ODPOWIEDZ and xf:isData($fml/CI_ODPOWIEDZ))
                then 
                  <m1:answers>
                  <m2:Answers>
                  <m2:answers>{ data($fml/CI_ODPOWIEDZ) }</m2:answers>
                  </m2:Answers>
                  </m1:answers>
                else ()
            }
            {if($fml/DC_PREF_KONTAKT and xf:isData($fml/DC_PREF_KONTAKT))
                then 
                  <m1:preferredContact>
                  <m2:CustomerPreferenceContact>
                  <m2:customerPreferenceContact>{ data($fml/DC_PREF_KONTAKT) }</m2:customerPreferenceContact>
                  </m2:CustomerPreferenceContact>
                  </m1:preferredContact>
                else ()
            }
          </m1:CustomerAdditionalInfo>
        </m1:customerAdditionalInfo>

        <m1:customerFirm>
          <m1:CustomerFirm>
            {if($fml/DC_LICZBA_ZATRUDNIONYCH and xf:isData($fml/DC_LICZBA_ZATRUDNIONYCH))
                then <m1:numberOfEmployees>{ data($fml/DC_LICZBA_ZATRUDNIONYCH) }</m1:numberOfEmployees>
                else ()
            }
            {if($fml/CI_DATA_ROZP_DZIAL and xf:isData($fml/CI_DATA_ROZP_DZIAL))
                then <m1:foundationDate>{ xf:mapDate($fml/CI_DATA_ROZP_DZIAL) }</m1:foundationDate>
                else ()
            }
            {if($fml/DC_DATA_BANKRUCTWA and xf:isData($fml/DC_DATA_BANKRUCTWA))
                then <m1:bankruptcyDate>{ xf:mapDate($fml/DC_DATA_BANKRUCTWA) }</m1:bankruptcyDate>
                else ()
            }
            {if($fml/DC_NR_DOWODU_REGON and xf:isData($fml/DC_NR_DOWODU_REGON) and $fml/DC_TYP_KLIENTA and xf:isData($fml/DC_TYP_KLIENTA) and $fml/DC_TYP_KLIENTA='P')
                then <m1:regon>{ data($fml/DC_NR_DOWODU_REGON) }</m1:regon>
                else ()
            }
            {if($fml/CI_KRS and xf:isData($fml/CI_KRS))
                then <m1:krs>{ data($fml/CI_KRS) }</m1:krs>
                else ()
            }
            {if($fml/CI_LICZBA_PLACOWEK and xf:isData($fml/CI_LICZBA_PLACOWEK))
                then <m1:agencies>{ data($fml/CI_LICZBA_PLACOWEK) }</m1:agencies>
                else ()
            }
            {if($fml/CI_FOP and xf:isData($fml/CI_FOP))
                then 
                  <m1:legalForm>
                  <m2:CustomerLegalForm>
                  <m2:customerLegalForm>{ data($fml/CI_FOP) }</m2:customerLegalForm>
                  </m2:CustomerLegalForm>
                  </m1:legalForm>
                else ()
            }                                                          
            {if($fml/DC_EKD and xf:isData($fml/DC_EKD))
                then 
                  <m1:ekd>
                  <m2:EkdCode>
                  <m2:ekdCode>{ data($fml/DC_EKD) }</m2:ekdCode>
                  </m2:EkdCode>
                  </m1:ekd>
                else ()
            }                             
          </m1:CustomerFirm>
        </m1:customerFirm>

        <m1:customerStudiesInfo>
          <m1:CustomerStudiesInfo>
            {if($fml/DC_PLANOW_DATA_UKON_SZK and xf:isData($fml/DC_PLANOW_DATA_UKON_SZK))
                then <m1:dateOfEndSchool>{ xf:mapDate($fml/DC_PLANOW_DATA_UKON_SZK) }</m1:dateOfEndSchool>
                else ()
            }
            {if($fml/CI_SYSTEM_NAUCZANIA and xf:isData($fml/CI_SYSTEM_NAUCZANIA))
                then 
                  <m1:studySystem>
                  <m2:StudySystem>
                  <m2:studySystem>{ data($fml/CI_SYSTEM_NAUCZANIA) }</m2:studySystem>
                  </m2:StudySystem>
                  </m1:studySystem>
                else ()
            }
            {if($fml/CI_SPECJALNOSC and xf:isData($fml/CI_SPECJALNOSC))
                then 
                  <m1:specialization>
                  <m2:CustomerStudiesSpecial>
                  <m2:customerStudiesSpecial>{ data($fml/CI_SPECJALNOSC) }</m2:customerStudiesSpecial>
                  </m2:CustomerStudiesSpecial>
                  </m1:specialization>
                else ()
            } 
            {if($fml/CI_KIERUNEK_STUDIOW and xf:isData($fml/CI_KIERUNEK_STUDIOW))
                then 
                  <m1:fieldOfStudies>
                  <m2:CustomerFieldOfStudies>
                  <m2:customerFieldOfStudies>{ data($fml/CI_KIERUNEK_STUDIOW) }</m2:customerFieldOfStudies>
                  </m2:CustomerFieldOfStudies>
                  </m1:fieldOfStudies>
                else ()
            }
          </m1:CustomerStudiesInfo>
        </m1:customerStudiesInfo>

        <m1:customerPersonal>
          <m1:CustomerPersonal>
            {if($fml/DC_NUMER_KLIENTA and xf:isData($fml/DC_NUMER_KLIENTA))
                then <m1:customerNumber>{ data($fml/DC_NUMER_KLIENTA) }</m1:customerNumber>
                else ()
            }
            {if($fml/DC_PRACOWNIK_BANKU and xf:isData($fml/DC_PRACOWNIK_BANKU))
                then <m1:workInBankStatus>{ boolean(xs:integer(data($fml/DC_PRACOWNIK_BANKU))) }</m1:workInBankStatus>
                else ()
            }
            {if($fml/DC_MIEJSCE_URODZENIA and xf:isData($fml/DC_MIEJSCE_URODZENIA))
                then <m1:birthPlace>{ data($fml/DC_MIEJSCE_URODZENIA) }</m1:birthPlace>
                else ()
            }
            {if($fml/DC_IMIE_OJCA and xf:isData($fml/DC_IMIE_OJCA))
                then <m1:fatherName>{ data($fml/DC_IMIE_OJCA) }</m1:fatherName>
                else ()
            }
            {if($fml/DC_LICZBA_OS_WE_WSP_GOSP_D and xf:isData($fml/DC_LICZBA_OS_WE_WSP_GOSP_D))
                then <m1:numberOfPersonsHousehold>{ data($fml/DC_LICZBA_OS_WE_WSP_GOSP_D) }</m1:numberOfPersonsHousehold>
                else ()
            }
            {if($fml/DC_POSIADA_DOM_MIESZK and xf:isData($fml/DC_POSIADA_DOM_MIESZK))
                then <m1:ownHouseApartment>{ xf:toBoolean($fml/DC_POSIADA_DOM_MIESZK) }</m1:ownHouseApartment>
                else ()
            }
            {if($fml/DC_MIEJSCE_ZATRUDNIENIA and xf:isData($fml/DC_MIEJSCE_ZATRUDNIENIA))
                then <m1:workplace>{ data($fml/DC_MIEJSCE_ZATRUDNIENIA) }</m1:workplace>
                else ()
            }
            {if($fml/DC_LICZBA_OSOB_NA_UTRZ and xf:isData($fml/DC_LICZBA_OSOB_NA_UTRZ))
                then <m1:numberOfDependents>{ data($fml/DC_LICZBA_OSOB_NA_UTRZ) }</m1:numberOfDependents>
                else ()
            }
            {if($fml/DC_DATA_URODZENIA and xf:isData($fml/DC_DATA_URODZENIA))
                then <m1:dateOfBirth>{ xf:mapDate($fml/DC_DATA_URODZENIA) }</m1:dateOfBirth>
                else ()
            }
            {if($fml/DC_NAZWISKO and xf:isData($fml/DC_NAZWISKO))
                then <m1:lastName>{ data($fml/DC_NAZWISKO) }</m1:lastName>
                else ()
            }
            {if($fml/DC_DRUGIE_IMIE and xf:isData($fml/DC_DRUGIE_IMIE))
                then <m1:secondName>{ data($fml/DC_DRUGIE_IMIE) }</m1:secondName>
                else ()
            }
            {if($fml/DC_NR_PESEL and xf:isData($fml/DC_NR_PESEL))
                then <m1:pesel>{ data($fml/DC_NR_PESEL) }</m1:pesel>
                else ()
            }
            {if($fml/DC_IMIE and xf:isData($fml/DC_IMIE))
                then <m1:firstName>{ data($fml/DC_IMIE) }</m1:firstName>
                else ()
            }
            {if($fml/DC_DATA_SMIERCI and xf:isData($fml/DC_DATA_SMIERCI))
                then <m1:dateOfDeath>{ xf:mapDate($fml/DC_DATA_SMIERCI) }</m1:dateOfDeath>
                else ()
            }
            {if($fml/CI_CHARAKTER_WYKSZTALCENIA and xf:isData($fml/CI_CHARAKTER_WYKSZTALCENIA))
                then 
                  <m1:educationNature>
                  <m2:CustomerEducationNature>
                  <m2:customerEducationNature>{ data($fml/CI_CHARAKTER_WYKSZTALCENIA) }</m2:customerEducationNature>
                  </m2:CustomerEducationNature>
                  </m1:educationNature>
                else ()
            }
            {if($fml/DC_SLUZBA_WOJSKOWA and xf:isData($fml/DC_SLUZBA_WOJSKOWA))
                then 
                  <m1:militaryServiceStatus>
                  <m2:CustomerMilitaryServiceStat>
                  <m2:customerMilitaryServiceStat>{ data($fml/DC_SLUZBA_WOJSKOWA) }</m2:customerMilitaryServiceStat>
                  </m2:CustomerMilitaryServiceStat>
                  </m1:militaryServiceStatus>
                else ()
            } 
            {if($fml/DC_STAN_CYWILNY and xf:isData($fml/DC_STAN_CYWILNY))
                then 
                  <m1:maritalStatus>
                  <m2:CustomerMaritalStatus>
                  <m2:customerMaritalStatus>{ data($fml/DC_STAN_CYWILNY) }</m2:customerMaritalStatus>
                  </m2:CustomerMaritalStatus>
                  </m1:maritalStatus>
                else ()
            }
            {if($fml/CI_ZRODLO_DOCHODU and xf:isData($fml/CI_ZRODLO_DOCHODU))
                then 
                  <m1:sourceOfIncome>
                  <m2:SourceOfIncome>
                  <m2:sourceOfIncome>{ data($fml/CI_ZRODLO_DOCHODU) }</m2:sourceOfIncome>
                  </m2:SourceOfIncome>
                  </m1:sourceOfIncome>
                else ()
            }
            {if($fml/DC_WYKSZTALCENIE and xf:isData($fml/DC_WYKSZTALCENIE))
                then 
                  <m1:educationCode>
                  <m2:CustomerEducationCode>
                  <m2:customerEducationCode>{ data($fml/DC_WYKSZTALCENIE) }</m2:customerEducationCode>
                  </m2:CustomerEducationCode>
                  </m1:educationCode>
                else ()
            }
            {if($fml/DC_WARUNKI_MIESZKANIOWE and xf:isData($fml/DC_WARUNKI_MIESZKANIOWE))
                then 
                  <m1:flatConditions>
                  <m2:FlatConditions>
                  <m2:flatConditions>{ data($fml/DC_WARUNKI_MIESZKANIOWE) }</m2:flatConditions>
                  </m2:FlatConditions>
                  </m1:flatConditions>
                else ()
            }
            {if($fml/DC_PLEC and xf:isData($fml/DC_PLEC))
                then 
                  <m1:sexCode>
                  <m2:CustomerSexCode>
                  <m2:customerSexCode>{ data($fml/DC_PLEC) }</m2:customerSexCode>
                  </m2:CustomerSexCode>
                  </m1:sexCode>
                else ()
            }                             
          </m1:CustomerPersonal>
        </m1:customerPersonal>

        {if($fml/DC_NUMER_ODDZIALU and xf:isData($fml/DC_NUMER_ODDZIALU))
            then 
              <m1:branchOfOwnership>
              <m2:BranchCode>
              <m2:branchCode>{ data($fml/DC_NUMER_ODDZIALU) }</m2:branchCode>
              </m2:BranchCode>
              </m1:branchOfOwnership>
            else ()
        }                             
        {if($fml/DC_TYP_KLIENTA and xf:isData($fml/DC_TYP_KLIENTA))
            then 
              <m1:customerType>
              <m2:CustomerType>
              <m2:customerType>{ data($fml/DC_TYP_KLIENTA) }</m2:customerType>
              </m2:CustomerType>
              </m1:customerType>
            else ()
        }                             
        {if($fml/DC_SEGMENT_MARKETINGOWY and xf:isData($fml/DC_SEGMENT_MARKETINGOWY))
            then 
              <m1:customerSegment>
              <m2:CustomerSegment>
              <m2:customerSegment>{ data($fml/DC_SEGMENT_MARKETINGOWY) }</m2:customerSegment>
              </m2:CustomerSegment>
              </m1:customerSegment>
            else ()
        }                             
        {if($fml/CI_UDOSTEP_GRUPA and xf:isData($fml/CI_UDOSTEP_GRUPA))
            then 
              <m1:processingApproval>
              <m2:CustomerProcessingApproval>
              <m2:customerProcessingApproval>{ data($fml/CI_UDOSTEP_GRUPA) }</m2:customerProcessingApproval>
              </m2:CustomerProcessingApproval>
              </m1:processingApproval>
            else ()
        }                             
        {if($fml/DC_SPW and xf:isData($fml/DC_SPW))
            then 
              <m1:spw>
              <m2:SpwCode>
              <m2:spwCode>{ data($fml/DC_SPW) }</m2:spwCode>
              </m2:SpwCode>
              </m1:spw>
            else ()
        }                             

        <m1:customerLegalInfo>
          <m1:CustomerLegalInfo>
            {if($fml/DC_JEDNOSTKA_KORPORACYJNA and xf:isData($fml/DC_JEDNOSTKA_KORPORACYJNA))
                then <m1:corporateUnit>{ data($fml/DC_JEDNOSTKA_KORPORACYJNA) }</m1:corporateUnit>
                else ()
            }
            {if($fml/CI_PRZYNAL_CENTR_KORP and xf:isData($fml/CI_PRZYNAL_CENTR_KORP))
                then <m1:corporateCenterInd>{ data($fml/CI_PRZYNAL_CENTR_KORP) }</m1:corporateCenterInd>
                else ()
            }
            {if($fml/CI_NUMER_W_REJESTRZE and xf:isData($fml/CI_NUMER_W_REJESTRZE))
                then <m1:registryNo>{ data($fml/CI_NUMER_W_REJESTRZE) }</m1:registryNo>
                else ()
            }
            {if($fml/CI_NAZWA_ORG_REJESTR and xf:isData($fml/CI_NAZWA_ORG_REJESTR))
                then <m1:registryName>{ data($fml/CI_NAZWA_ORG_REJESTR) }</m1:registryName>
                else ()
            }
            {if($fml/CI_MIASTO_ORG_REJESTR and xf:isData($fml/CI_MIASTO_ORG_REJESTR))
                then <m1:registryCity>{ data($fml/CI_MIASTO_ORG_REJESTR) }</m1:registryCity>
                else ()
            }
            {if($fml/CI_KOD_POCZT_ORG_REJESTR and xf:isData($fml/CI_KOD_POCZT_ORG_REJESTR))
                then <m1:registryZip>{ data($fml/CI_KOD_POCZT_ORG_REJESTR) }</m1:registryZip>
                else ()
            }
            {if($fml/CI_ULICA_ORG_REJESTR and xf:isData($fml/CI_ULICA_ORG_REJESTR))
                then <m1:registryStreet>{ data($fml/CI_ULICA_ORG_REJESTR) }</m1:registryStreet>
                else ()
            }
            {if($fml/CI_NR_POSES_LOKALU_ORG_REJ and xf:isData($fml/CI_NR_POSES_LOKALU_ORG_REJ))
                then <m1:registryPropertyNo>{ data($fml/CI_NR_POSES_LOKALU_ORG_REJ) }</m1:registryPropertyNo>
                else ()
            }
            {if($fml/CI_DATA_WPISU_DO_REJESTR and xf:isData($fml/CI_DATA_WPISU_DO_REJESTR))
                then <m1:registryDate>{ xf:mapDate($fml/CI_DATA_WPISU_DO_REJESTR) }</m1:registryDate>
                else ()
            }
            {if($fml/CI_STATUS_SIEDZIBY and xf:isData($fml/CI_STATUS_SIEDZIBY))
                then <m1:seatOwnershipStatus>{ data($fml/CI_STATUS_SIEDZIBY) }</m1:seatOwnershipStatus>
                else ()
            }
            {if($fml/CI_KAPITAL_ZALOZYCIELSKI and xf:isData($fml/CI_KAPITAL_ZALOZYCIELSKI))
                then <m1:initialCapital>{ data($fml/CI_KAPITAL_ZALOZYCIELSKI) }</m1:initialCapital>
                else ()
            }
            {if($fml/CI_KAPITAL_ZALOZ_OPLACONY and xf:isData($fml/CI_KAPITAL_ZALOZ_OPLACONY))
                then <m1:initialCapitalPaid>{ data($fml/CI_KAPITAL_ZALOZ_OPLACONY) }</m1:initialCapitalPaid>
                else ()
            }
            {if($fml/CI_WARTOSC_AKCJI and xf:isData($fml/CI_WARTOSC_AKCJI))
                then <m1:stocksValue>{ data($fml/CI_WARTOSC_AKCJI) }</m1:stocksValue>
                else ()
            }
            {if($fml/CI_KRAJ_ORG_REJESTR and xf:isData($fml/CI_KRAJ_ORG_REJESTR))
                then
                  <m1:registryCountry>
                  <m2:CountryCode>
                  <m2:countryCode>{ data($fml/CI_KRAJ_ORG_REJESTR) }</m2:countryCode>
                  </m2:CountryCode>
                  </m1:registryCountry>
                else ()
            }
          </m1:CustomerLegalInfo>
        </m1:customerLegalInfo>
      </m1:Customer>
    </m:customer>
  </m:invokeResponse>
  }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>