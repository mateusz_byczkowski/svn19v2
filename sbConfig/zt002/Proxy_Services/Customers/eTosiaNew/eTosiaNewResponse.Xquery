<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbCEKEeTosiaNewResponse($fml as element(fml:FML32))
	as element(m:bCEKEeTosiaNewResponse) {
		<m:bCEKEeTosiaNewResponse>
			{
				if($fml/fml:E_SMS_TOKEN_NO)
					then <m:SmsTokenNo>{ data($fml/fml:E_SMS_TOKEN_NO) }</m:SmsTokenNo>
					else ()
			}
			{
				if($fml/fml:E_SMS_TOKEN_STATUS)
					then <m:SmsTokenStatus>{ data($fml/fml:E_SMS_TOKEN_STATUS) }</m:SmsTokenStatus>
					else ()
			}
			{
				if($fml/fml:E_SMS_TOKEN_COUNT)
					then <m:SmsTokenCount>{ data($fml/fml:E_SMS_TOKEN_COUNT) }</m:SmsTokenCount>
					else ()
			}
			{
				if($fml/fml:E_SMS_TOKEN_FREE_COUNT)
					then <m:SmsTokenFreeCount>{ data($fml/fml:E_SMS_TOKEN_FREE_COUNT) }</m:SmsTokenFreeCount>
					else ()
			}
			{
				if($fml/fml:E_TRIES)
					then <m:Tries>{ data($fml/fml:E_TRIES) }</m:Tries>
					else ()
			}
			{
				if($fml/fml:E_TRIES_ALLOWED)
					then <m:TriesAllowed>{ data($fml/fml:E_TRIES_ALLOWED) }</m:TriesAllowed>
					else ()
			}
			{
				if($fml/fml:E_LOGIN_COUNT)
					then <m:LoginCount>{ data($fml/fml:E_LOGIN_COUNT) }</m:LoginCount>
					else ()
			}
			{
				if($fml/fml:E_BAD_LOGIN_COUNT)
					then <m:BadLoginCount>{ data($fml/fml:E_BAD_LOGIN_COUNT) }</m:BadLoginCount>
					else ()
			}
			{
				if($fml/fml:E_LOGIN_ID)
					then <m:LoginId>{ data($fml/fml:E_LOGIN_ID) }</m:LoginId>
					else ()
			}
			{
				if($fml/fml:E_TIME_STAMP)
					then <m:TimeStamp>{ data($fml/fml:E_TIME_STAMP) }</m:TimeStamp>
					else ()
			}
		</m:bCEKEeTosiaNewResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapbCEKEeTosiaNewResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>