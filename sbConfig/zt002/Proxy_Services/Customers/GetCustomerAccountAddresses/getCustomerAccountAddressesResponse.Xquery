<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>v.1.1 2010-02-03 PKL PT58 - błąd zaminy anyType</con:description>
    <con:xquery><![CDATA[(: Rejestr zmian 
v.1.1 2010-02-03 PKL PT58 - błąd zaminy anyType
:)

declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()*
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};


declare function sourceValue2Boolean ($parm as xs:string*,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForAccount($parm as element(fml:FML32)) as element()
{

<ns0:account>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
    <ns6:Account>
      <ns6:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}</ns6:accountNumber>
      <ns6:accountName?>{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}</ns6:accountName>
      <ns6:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}</ns6:currentBalance>
      <ns6:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}</ns6:accountDescription>
      <ns6:accountIBAN?>{data($parm/NF_ACCOUN_ACCOUNTIBAN[$occ])}</ns6:accountIBAN>
      <ns6:currency>
        <ns5:CurrencyCode>
          <ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns5:currencyCode>
        </ns5:CurrencyCode>
      </ns6:currency>
      <ns6:accountAddress>
        <ns6:AccountAddress>
          <ns6:name1?>{data($parm/NF_ACCOUA_NAME1[$occ])}</ns6:name1>
          <ns6:name2?>{data($parm/NF_ACCOUA_NAME2[$occ])}</ns6:name2>
          <ns6:street?>{data($parm/NF_ACCOUA_STREET[$occ])}</ns6:street>
          <ns6:houseFlatNumber?>{data($parm/NF_ACCOUA_HOUSEFLATNUMBER[$occ])}</ns6:houseFlatNumber>
          <ns6:city?>{data($parm/NF_ACCOUA_CITY[$occ])}</ns6:city>
          <ns6:stateCountry?>{data($parm/NF_ACCOUA_STATECOUNTRY[$occ])}</ns6:stateCountry>
          <ns6:zipCode?>{data($parm/NF_ACCOUA_ZIPCODE[$occ])}</ns6:zipCode>
          <ns6:accountAddressType?>{data($parm/NF_ACCOUA_ACCOUNTADDRESSTY[$occ])}</ns6:accountAddressType>
          {insertDate(data($parm/NF_ACCOUA_VALIDFROM[$occ]),"yyyy-MM-dd","ns6:validFrom")}
          {insertDate(data($parm/NF_ACCOUA_VALIDTO[$occ]),"yyyy-MM-dd","ns6:validTo")}
          <ns6:deleteWhenExpired?>{sourceValue2Boolean (data($parm/NF_ACCOUA_DELETEWHENEXPIRE[$occ]),"1")}</ns6:deleteWhenExpired>
        </ns6:AccountAddress>
      </ns6:accountAddress>
    </ns6:Account>
  }
</ns0:account>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForAccount($parm)}
  <ns0:bcd>
    <ns3:BusinessControlData>
      <ns3:pageControl>
        <ns4:PageControl>
          <ns4:hasNext?>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}</ns4:hasNext>
        
          <ns4:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns4:navigationKeyValue>
        </ns4:PageControl>
      </ns3:pageControl>
    </ns3:BusinessControlData>
  </ns0:bcd>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>