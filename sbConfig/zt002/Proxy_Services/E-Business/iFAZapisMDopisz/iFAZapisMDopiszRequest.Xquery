<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ARKA24/E-Business/messages/";
declare namespace xf = "http://bzwbk.com/services/ARKA24/E-Business/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapiFAZapisMDopiszRequest($req as element(m:iFAZapisMDopiszRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				<fml:E_LOGIN_ID?>{ data($req/m:LoginId) }</fml:E_LOGIN_ID>
			}
			{
				<fml:B_RODZ_DOK?>{ data($req/m:RodzDok) }</fml:B_RODZ_DOK>
			}
			{
				<fml:B_NR_DOK?>{ data($req/m:NrDok) }</fml:B_NR_DOK>
			}
			{
				<fml:E_CIF_NUMBER?>{ data($req/m:CifNumber) }</fml:E_CIF_NUMBER>
			}
			{
				<fml:FA_KONWERSJA?>{ data($req/m:Konwersja) }</fml:FA_KONWERSJA>
			}
			{
				<fml:FA_NR_Z?>{ data($req/m:NrZ) }</fml:FA_NR_Z>
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapiFAZapisMDopiszRequest($body/m:iFAZapisMDopiszRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>