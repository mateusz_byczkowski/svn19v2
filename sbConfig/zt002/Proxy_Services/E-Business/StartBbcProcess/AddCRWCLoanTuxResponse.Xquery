<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/crw/services/dc/loan";

declare function xf:map_AddCRWCLoanTuxResponse($req as element(urn:createApplicationResponse))
	as element(fml:FML32) {
	<fml:FML32>
			{
				<fml:E_AGR_SIGNATURE?>{ data($req/number) }</fml:E_AGR_SIGNATURE>
			}
	</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_AddCRWCLoanTuxResponse($body/urn:createApplicationResponse) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>