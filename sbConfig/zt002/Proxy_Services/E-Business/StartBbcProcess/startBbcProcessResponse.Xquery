<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(: This is the default content for new XQuery Project files :)

declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace op = "http://service.ws.flow.pbpolsoft.com.pl";
declare namespace tns1 = "http://bzwbk.com/services/BZWBK24flow/messages/";
declare namespace tns2 = "http://bzwbk.com/services/ewnioski/messages/";
declare namespace tns3 = "http://service.ws.flow.pbpolsoft.com.pl";
declare namespace tns4 = "urn:pbpolsoft.com.pl";
declare namespace xsd="http://www.w3.org/2001/XMLSchema";
declare namespace f="http://bzwbk.com/services/BZWBK24flow/faults/";

declare function xf:transformBody ($req as element(soap-env:Body)) as element(soap-env:Body) {
	
	<soap-env:Body>
	{
			if($req/tns3:startBBCP1Response)
			then 	<tns1:StartCEKEProcessResponse><tns1:caseRef>{data($req/tns3:startBBCP1Response/startBBCP1Return)}</tns1:caseRef></tns1:StartCEKEProcessResponse>
			else()
			
	}
	{
			if($req/tns3:startBBCP2Response)
			then 	<tns1:StartCEKEProcessResponse><tns1:caseRef>{data($req/tns3:startBBCP2Response/startBBCP2Return)}</tns1:caseRef></tns1:StartCEKEProcessResponse>
			else()
			
	}
	{
			if($req/tns3:startBBCP3Response)
			then 	<tns1:StartCEKEProcessResponse><tns1:caseRef>{data($req/tns3:startBBCP3Response/startBBCP3Return)}</tns1:caseRef></tns1:StartCEKEProcessResponse>
			else()
			
	}
	
	{
			if($req/tns3:startBBCP4Response)
			then 	<tns1:StartCEKEProcessResponse><tns1:caseRef>{data($req/tns3:startBBCP4Response/startBBCP4Return)}</tns1:caseRef></tns1:StartCEKEProcessResponse>
			else()
			
	}
	
	{
			if($req/tns3:startBBCP5Response)
			then 	<tns1:StartCEKEProcessResponse><tns1:caseRef>{data($req/tns3:startBBCP5Response/startBBCP5Return)}</tns1:caseRef></tns1:StartCEKEProcessResponse>
			else()
			
	}


	</soap-env:Body>
};

	declare variable $body as element(soapenv:Body) external;			
	xf:transformBody($body)]]></con:xquery>
</con:xqueryEntry>