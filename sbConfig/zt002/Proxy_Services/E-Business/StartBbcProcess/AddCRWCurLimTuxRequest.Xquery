<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace limit = "http://bzwbk.com/crw/services/dc/limit";

declare function xf:cekeDateToDateType
  ( $dateStr as xs:string ) as xs:string {
    if ($dateStr = '') then ""
    else
     concat(fn:substring($dateStr,7,4),'-',fn:substring($dateStr,4,2),'-',fn:substring($dateStr,1,2))
 } ;


declare function xf:map_AddCRWCurLimTuxRequest($fml as element(fml:FML32))
	as element(limit:createApplication) {
      <limit:createApplication>
         <application>
            <accountNumber>{ data($fml/fml:DC_NR_RACHUNKU) }</accountNumber>
            <additionalCosts>{ data($fml/fml:DC_KOSZTY_POZOSTALE) }</additionalCosts>
            <agreement2>{ data($fml/fml:DC_ODNAWIAC_LIMIT) }</agreement2>
            <currentAccountStartDate>{ xf:cekeDateToDateType(data($fml/fml:DC_DATA_OTWARCIA_RACHUNKU)) }</currentAccountStartDate>
            <prelimitAmount>{ data($fml/fml:DC_KWOTA_PRELIMITU) }</prelimitAmount>
            <minIncome>{ data($fml/fml:DC_LK_WPLYWY_MIESIECZNE) }</minIncome>  
            <commissionAmount>{ data($fml/fml:DC_KWOTA_PROWIZJI) }</commissionAmount>
            <creditAmount>{ data($fml/fml:DC_KWOTA_KREDYTU) }</creditAmount>            
            <product>{ data($fml/fml:DC_PRODUKT) }</product>
            {
                    if (data($fml/fml:DC_PRODUKT) != 'Z') then
                    (
                        <currentLimitAmount>{ data($fml/fml:DC_LIMIT_BIEZ) }</currentLimitAmount>,
                        <increaseCreditAmount>{ data($fml/fml:DC_PODPISANA_UMOWA_LIMIT_W_KO) }</increaseCreditAmount>,
                        <endDate>{ data($fml/fml:DC_DATA_KONCA_POWIAZANIA) }</endDate>
                    )
                    else ()
             }
            <customerApplicationDate>{ xf:cekeDateToDateType(data($fml/fml:DC_DATA_WPROWADZENIA)) }</customerApplicationDate>
            <customerRoles xsi:type="lo:SimpleCustomerRole" xmlns:lo="http://bzwbk.pl/crw/dc/limit/">
               <customer>
                  <birthDate>{ xf:cekeDateToDateType(data($fml/fml:DC_DATA_URODZENIA[1])) }</birthDate>
                  <branch>{ data($fml/fml:DC_NUMER_ODDZIALU) }</branch>
                  <chargesInfo>
                     <alimonyAndOtherLegalObligationsValue>{ data($fml/fml:DC_WARTOSC_ZABEZPIECZENIA) }</alimonyAndOtherLegalObligationsValue>
                     {
                     if (data($fml/fml:DC_WARTOSC_ZABEZPIECZENIA) != '') then
                        <alimonyAndOtherLegalObligationsPresent>true</alimonyAndOtherLegalObligationsPresent>
                     else 
                        <alimonyAndOtherLegalObligationsPresent>false</alimonyAndOtherLegalObligationsPresent>
                     }
                    <householdAndPhoneChargesValue>{ data($fml/fml:DC_MAX_KWOTA_PORECZENIA) }</householdAndPhoneChargesValue>
                     {
                     if (data($fml/fml:DC_MAX_KWOTA_PORECZENIA) != '') then
                        <householdAndPhoneChargesPresent>true</householdAndPhoneChargesPresent>
                     else 
                        <householdAndPhoneChargesPresent>false</householdAndPhoneChargesPresent>
                     }
                    <otherChargesValue>{ data($fml/fml:DC_POZOSTALE) }</otherChargesValue>
                    {
                    if (data($fml/fml:DC_POZOSTALE) != '') then
                        <otherChargesPresent>true</otherChargesPresent>
                     else 
                        <otherChargesPresent>false</otherChargesPresent>
                     }
                  </chargesInfo>
                  <cif>{ data($fml/fml:DC_NUMER_KLIENTA) }</cif>
                  <dataProcessingAgreement>{ data($fml/fml:DC_ZGODA_NA_PRZETW) }</dataProcessingAgreement>
                  <employmentInfo>
                        <monthlyNetIncomePLN>{ data($fml/fml:DC_MIES_DOCHOD_NETTO_W_PLN) }</monthlyNetIncomePLN>
                  </employmentInfo>
                  <email>{ data($fml/fml:DC_ADRES_E_MAIL) }</email>
                  <familyInfo>
                     <numberOfHouseholdMembers>{ data($fml/fml:DC_LICZBA_OSOB_NA_UTRZ) }</numberOfHouseholdMembers>
                  </familyInfo>
                  <firstName>{ data($fml/fml:DC_IMIE[1]) }</firstName>
                  <homePhoneAreaCode>{ data($fml/fml:DC_NR_KIER_TELEFONU) }</homePhoneAreaCode>
                  { 
		       if (data($fml/fml:DC_NR_TELEFONU) != '') then
		          <homePhoneAvailability>PHONE_AVAILABLE</homePhoneAvailability>
		       else 
	    		  <homePhoneAvailability>NO_PHONE</homePhoneAvailability>
	           }
                  <homePhoneNumber>{ data($fml/fml:DC_NR_TELEFONU) }</homePhoneNumber>
                  {
                      if (data($fml/fml:DC_NR_DOWODU_REGON[1]) != '') then
                      (
                          <idType>IDENTITY_CARD</idType>,
                          <idSerNum>{ data($fml/fml:DC_NR_DOWODU_REGON[1]) }</idSerNum>
                      )
                      else
                      (
                          <idType>OTHER</idType>,
                          <idSerNum>{ data($fml/fml:DC_NUMER_PASZPORTU[1]) }</idSerNum>
                      )
                  }
                  <lastName>{ data($fml/fml:DC_NAZWISKO[1]) } { data($fml/fml:DC_WSP_NAZWISKO) }</lastName>
                  <middleName>{ data($fml/fml:DC_DRUGIE_IMIE[1]) }</middleName>
                  <mobilePhoneNumber>{ data($fml/fml:DC_NR_TELEF_KOMORKOWEGO) }</mobilePhoneNumber>
                  {
		         for $it at $p in $fml/fml:DC_MAKS_KWOTA_ZABEZPIECZONA return
		        (
                           <obligations>
                           <amount>{ data($fml/fml:DC_MAKS_KWOTA_ZABEZPIECZONA[$p]) }</amount>
                             {			
         		      if (data($fml/fml:DC_NR_BANKU[$p]) != '') then
                                <bankInBIK>true</bankInBIK>
                             else  
                                <bankInBIK>false</bankInBIK>
                             }
                            {
                             if (data($fml/fml:DC_SPOSOB_ZABEZPIECZENIA[$p]) != 'N') then
                               <obligationCollateral>true</obligationCollateral>
                             else
                               <obligationCollateral>false</obligationCollateral>
                             }
                            {
                             if (data($fml/fml:DC_RODZAJ_ZACHOWANIA[$p]) != 'N') then
                            <businessActivity>true</businessActivity>
                            else
                            <businessActivity>false</businessActivity>
                            }
                            {
                             if (data($fml/fml:DC_NUMER_ZABEZPIECZENIA[$p]) != 'N') then
                               <applicantObligation>true</applicantObligation>
                            else
                               <applicantObligation>false</applicantObligation>
                            }
                           <bankInBIKName>{ data($fml/fml:DC_NR_BANKU[$p]) }</bankInBIKName>
                           <bankNotInBIKName>{ data($fml/fml:DC_BANK_NADZORUJACY[$p]) }</bankNotInBIKName>
                           <consolidation>0</consolidation>
                           <currency>{ data($fml/fml:DC_KOD_WALUTY[$p]) }</currency>
                           <monthlyInstalment>{ data($fml/fml:DC_PODSTAWA_MIESIECZNA[$p]) }</monthlyInstalment>
                           <subject>{ data($fml/fml:DC_TYP_PAPIERU_WARTOSCIOWEGO[$p]) }</subject>
                           <type>2</type>
                        </obligations>
                       )
                  }
                  <pesel>{ data($fml/fml:DC_NR_PESEL[1]) }</pesel>
                  <placeOfBirth>{ data($fml/fml:DC_MIEJSCE_URODZENIA) }</placeOfBirth>
                  <primaryAddress>
                     <city>{ data($fml/fml:DC_MIASTO_DANE_PODST[1]) }</city>
                     <houseFlatNumber>{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST[1]) }</houseFlatNumber>
                     <street>{ data($fml/fml:DC_ULICA_DANE_PODST[1]) }</street>
                     <zipCode>{ data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST[1]) }</zipCode>
                     <country>{ data($fml/fml:DC_WOJ_KRAJ_DANE_PODST[1]) }</country>
                  </primaryAddress>
                  <secondaryAddress>
                     <city>{ data($fml/fml:DC_MIASTO_ADRES_ALT) }</city>
                     <houseFlatNumber>{ data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT) }</houseFlatNumber>
                     <street>{ data($fml/fml:DC_ULICA_ADRES_ALT) }</street>
                     <zipCode>{ data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT) }</zipCode>
                     <country>{ data($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT[1]) }</country>
                  </secondaryAddress>
                   {			
       		      if (data($fml/fml:DC_PLEC) = 'M') then
                         <sex>MALE</sex>
                     else 
                     if (data($fml/fml:DC_PLEC) = 'F') then
                         <sex>FEMALE</sex>
                     else ()
                    }
               </customer>
            <personalDataProcessingPermission>false</personalDataProcessingPermission>
             <spouseCoapplicant>false</spouseCoapplicant>
            </customerRoles>
            <destinationBranch>{ data($fml/fml:DC_NUMER_ODDZIALU) }</destinationBranch>
            <interestRate>{ data($fml/fml:DC_STOPA_PROCENTOWA) }</interestRate>
            <registrationBranch>{ data($fml/fml:DC_NUMER_ODDZIALU) }</registrationBranch>
            <scheduledDecisionDate>{ xf:cekeDateToDateType(data($fml/fml:DC_DATA_OTWARCIA)) }</scheduledDecisionDate>
            <source>{ data($fml/fml:DC_ZRODLO) }</source>
         </application>
         <authData>
            <applicationId>{ data($fml/fml:U_APPLICATION_ID) }</applicationId>
            <applicationPassword>{ data($fml/fml:U_APPLICATION_PWD) }</applicationPassword>
            <operator></operator>
         </authData>
      </limit:createApplication>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_AddCRWCurLimTuxRequest($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>