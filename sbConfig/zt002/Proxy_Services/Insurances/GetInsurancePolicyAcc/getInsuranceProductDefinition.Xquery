<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-01-21</con:description>
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:card.entities.be.dcl";
declare namespace ns2="urn:be.services.dcl";
declare namespace ns1="urn:card.entities.be.dcl";
declare namespace ns3="urn:accounts.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $callBody as element(soap:Body) external;

declare function getFieldsFromBody($parm as element(fml:FML32)) as element(fml:FML32)
{
<fml:FML32>
<fml:PT_SOURCE_PRODUCT_CODE>{data($parm/fml:NF_PRODUD_SORCEPRODUCTCODE)}</fml:PT_SOURCE_PRODUCT_CODE>
<fml:PT_CODE_PRODUCT_SYSTEM>1</fml:PT_CODE_PRODUCT_SYSTEM>
<fml:PT_FIRST_PRODUCT_FEATURE>{data($parm/fml:NF_ATTRPG_FIRSTPRODUCTFEAT)}</fml:PT_FIRST_PRODUCT_FEATURE>
<fml:PT_SECOND_PRODUCT_FEATURE>{data($parm/fml:NF_ATTRPG_SECONDPRODUCTFEA)}</fml:PT_SECOND_PRODUCT_FEATURE>
<fml:PT_THIRD_PRODUCT_FEATURE>{data($parm/fml:NF_ATTRPG_THIRDPRODUCTFEAT)}</fml:PT_THIRD_PRODUCT_FEATURE>
<fml:PT_FOURTH_PRODUCT_FEATURE>{data($parm/fml:NF_ATTRPG_FOURTHPRODUCTFEA)}</fml:PT_FOURTH_PRODUCT_FEATURE>
<fml:PT_FIFTH_PRODUCT_FEATURE>{data($parm/fml:NF_ATTRPG_FIFTHPRODUCTFEAT)}</fml:PT_FIFTH_PRODUCT_FEATURE>
</fml:FML32>
};

getFieldsFromBody($callBody/fml:FML32)]]></con:xquery>
</con:xqueryEntry>