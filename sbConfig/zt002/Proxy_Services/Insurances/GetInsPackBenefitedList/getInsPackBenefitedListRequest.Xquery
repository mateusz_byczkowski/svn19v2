<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:insurance.entities.be.dcl";
declare namespace ns1="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:bool2short($bool as xs:boolean) as xs:string {
	if ($bool)
		then "1"
		else "0"
};

declare function getFieldsFromHeader($parm as element(ns1:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns1:msgHeader/ns1:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns1:msgHeader/ns1:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns1:msgHeader/ns1:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns1:msgHeader/ns1:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns1:msgHeader/ns1:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns1:msgHeader/ns1:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns1:transHeader/ns1:transId)}</NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns1:invoke)) as element()*
{

<NF_PAGEC_ACTIONCODE?>{data($parm/ns1:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns1:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:pageSize)}</NF_PAGEC_PAGESIZE>
,

    if (data($parm/ns1:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:reverseOrder))
      then 	
       <NF_PAGEC_REVERSEORDER?>{xf:bool2short(data($parm/ns1:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:reverseOrder))}</NF_PAGEC_REVERSEORDER>
    else()

,
<NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns1:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:navigationKeyDefinition)}</NF_PAGEC_NAVIGATIONKEYDEFI>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns1:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
,
<NF_INSUPA_NUMBER?>{data($parm/ns1:insurancePackageAcc/ns0:InsurancePackageAcc/ns0:number)}</NF_INSUPA_NUMBER>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns1:header)}
    {getFieldsFromInvoke($body/ns1:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>