<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CRW/UpdateApplicationULStatus/ResponseTransform/";
declare namespace ns0 = "http://bzwbk.com/crw/services/insurance/";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns-1 = "urn:baseentities.be.dcl";

declare function xf:ResponseTransform($changeApplicationStatusResponse1 as element(ns0:changeApplicationStatusResponse))
    as element(ns1:invokeResponse) {
        <ns1:invokeResponse>
        	<ns1:messageHelper>
        		<ns-1:MessageHelper>
        		</ns-1:MessageHelper>
        	</ns1:messageHelper>
        </ns1:invokeResponse>
};

declare variable $changeApplicationStatusResponse1 as element(ns0:changeApplicationStatusResponse) external;

xf:ResponseTransform($changeApplicationStatusResponse1)]]></con:xquery>
</con:xqueryEntry>