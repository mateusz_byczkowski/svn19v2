<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/BillProtection/Sygnity/CUP/GetOpCoverageVariant/ResponseTransform/";
declare namespace ns0 = "urn:baseentities.be.dcl";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf-product-types";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns2 = "urn:uldictionary.dictionaries.be.dcl";

declare function xf:ResponseTransform($getProductResponse1 as element(ns-1:getProductResponse))
    as element(ns3:invokeResponse) {
        <ns3:invokeResponse>
            <ns3:messageHelper>
                <ns0:MessageHelper>
                    <ns0:errorCode?></ns0:errorCode>
                    <ns0:errorType?></ns0:errorType>
                </ns0:MessageHelper>
            </ns3:messageHelper>
            {
                let $level-list := $getProductResponse1/product/level-list
                return
                    <ns3:opCoverageVariant>
                        {
                            for $level-definition in $level-list/level-definition
                            return
                                <ns2:OpCoverageVariant>
                                    <ns2:opCoverageVariant>{ xs:string( data($level-definition/level) ) }</ns2:opCoverageVariant>
                                    <ns2:insurancePremium>{ xs:double( data($level-definition/amount) ) }</ns2:insurancePremium>
                                </ns2:OpCoverageVariant>
                        }
                    </ns3:opCoverageVariant>
            }
        </ns3:invokeResponse>
};

declare variable $getProductResponse1 as element(ns-1:getProductResponse) external;

xf:ResponseTransform($getProductResponse1)]]></con:xquery>
</con:xqueryEntry>