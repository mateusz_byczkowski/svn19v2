<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/Testy_XQuery/Pilot/GetContract/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "urn:applicationul.entities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace ns3 = "urn:baseauxentities.be.dcl";
declare namespace ns4 = "urn:baseentities.be.dcl";
declare namespace ns5 = "urn:basedictionaries.be.dcl";
declare namespace ns6 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns7 = "urn:cif.entities.be.dcl";
declare namespace ns8 = "urn:entities.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns2:invoke), $header1 as element(ns2:header))
    as element(ns-1:getContractRequest) {
        <ns-1:getContractRequest>
            <envelope>
                <request-time>{ data($header1/ns2:msgHeader/ns2:timestamp) }</request-time>
                <request-no>{ data($header1/ns2:msgHeader/ns2:msgId) }</request-no>
                <source-code>{ data($invoke1/ns2:sourceCode/ns3:StringHolder/ns3:value) }</source-code>
				<user-id>{ data($header1/ns2:msgHeader/ns2:userId) }</user-id>
                <branch-id>{ xs:long( data($header1/ns2:msgHeader/ns2:unitId) ) }</branch-id>
            </envelope>
            <category?>{ data($invoke1/ns2:policyContract/ns1:PolicyContract/ns1:productCode/ns6:UlParameters/ns6:productCategory) }</category>
            <product-type?>{ data($invoke1/ns2:policyContract/ns1:PolicyContract/ns1:productCode/ns6:UlParameters/ns6:productId) }</product-type>
            <contract-number>{ data($invoke1/ns2:policyContract/ns1:PolicyContract/ns1:policyID) }</contract-number>
            <proposal-number?>{ data($invoke1/ns2:policyContract/ns1:PolicyContract/ns1:proposalID) }</proposal-number>
        </ns-1:getContractRequest>
};

declare variable $invoke1 as element(ns2:invoke) external;
declare variable $header1 as element(ns2:header) external;

xf:RequestTransform($invoke1, $header1)]]></con:xquery>
</con:xqueryEntry>