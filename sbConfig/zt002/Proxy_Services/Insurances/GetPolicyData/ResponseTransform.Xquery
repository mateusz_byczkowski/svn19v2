<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns9 = "urn:be.services.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Pilot/GetContract/ResponseTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns7 = "http://jv.channel.cu.com.pl/cmf-product-types";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-party-types";
declare namespace ns3 = "urn:cif.entities.be.dcl";
declare namespace ns5 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns2 = "urn:baseentities.be.dcl";
declare namespace ns6 = "urn:applicationul.entities.be.dcl";
declare namespace ns8 = "urn:uldictionary.dictionaries.be.dcl";

declare function xf:ResponseTransform($getContractResponse1 as element(ns1:getContractResponse))
    as element(ns9:invokeResponse) {
        <ns9:invokeResponse>
            <ns9:messageHelper>
                <ns2:MessageHelper>
                    <ns2:errorCode?></ns2:errorCode>
                    <ns2:errorType?></ns2:errorType>
                    <ns2:message?></ns2:message>
                </ns2:MessageHelper>
            </ns9:messageHelper>
            <ns9:customer>
                <ns3:Customer>
                    <ns3:customerNumber>{
                    	let $clients :=
	                        for $client in $getContractResponse1/contract/client-list/contract-person
	                        where $client/@role = '1'
	                        return
	                            data($client/@client-code)
	                    return $clients[1]
                    }</ns3:customerNumber>
                    <ns3:policyContract>
                        {
                            let $contract := $getContractResponse1/contract
                            return
                                <ns6:PolicyContract>
                                    <ns6:proposalID>{ xs:string( data($contract/proposal-number) ) }</ns6:proposalID>
                                    <ns6:policyID>{ xs:string( data($contract/@contract-number) ) }</ns6:policyID>
                                    <ns6:proposalDate?>{ data($contract/contract-dates/proposal) }</ns6:proposalDate>
                                    <ns6:policyDate?>{ data($contract/contract-dates/accepted) }</ns6:policyDate>
                                    <ns6:cession>{ if (fn:exists(data($contract/cession))) then 1 else 0 }</ns6:cession>
                                    {
                                        for $coverages in $contract/coverages
                                        return
                                            <ns6:policyCoverageList>
                                                {
                                                    for $coverage in $coverages/coverage
                                                    return
                                                        <ns6:PolicyCoverage>
                                                            <ns6:coverageID>{ xs:string( data($coverage/@coverage-number) ) }</ns6:coverageID>
                                                            <ns6:owuSig>{ xs:string( data($coverage/owu/signature) ) }</ns6:owuSig>
                                                            <ns6:regulationSig>{ xs:string( data($coverage/regulation-sig) ) }</ns6:regulationSig>
                                                            <ns6:parametrsCardSig>{ xs:string( data($coverage/param-card-sig) ) }</ns6:parametrsCardSig>
                                                            <ns6:coverageStartDate?>{ data($coverage/dates/contract-start) }</ns6:coverageStartDate>
                                                            <ns6:premiumAmount>{ xs:double( data($coverage/amount) ) }</ns6:premiumAmount>
                                                            <ns6:policyCoverageBenefitedList>
                                                            {
                                                                for $beneficiary in $coverage/beneficiaries/beneficiary
                                                                return
                                                                    <ns6:PolicyCoverageBenefited>
                                                                        <ns6:privatePerson>{ xs:string( data($beneficiary/company-ind) ) }</ns6:privatePerson>
                                                                        <ns6:firstName?>{ xs:string( data($beneficiary/name/first-name) ) }</ns6:firstName>
                                                                        <ns6:name?>{ xs:string( data($beneficiary/name/last-name) ) }</ns6:name>
                                                                        <ns6:pesel?>{ data($beneficiary/identifiers/pesel) }</ns6:pesel>
                                                                        <ns6:regon?>{ data($beneficiary/identifiers/regon) }</ns6:regon>
                                                                        <ns6:birthDate?>{ if(fn:contains($beneficiary/dob, '+')) then fn:substring-before(data($beneficiary/dob), '+') else data($beneficiary/dob) }</ns6:birthDate>
                                                                        <ns6:percent>{ xs:double( data($beneficiary/split) ) }</ns6:percent>
                                                                    </ns6:PolicyCoverageBenefited>
                                                            }
                                                            </ns6:policyCoverageBenefitedList>
                                                            <ns6:policyCoverageFundSplitList>
                                                            {
                                                                for $fund in $coverage/funds/contract-fund[1]/funds/fund
                                                                return
                                                                    <ns6:PolicyCoverageFundSplit>
                                                                        <ns6:splitPercentage>{ data($fund/split) }</ns6:splitPercentage>
                                                                        <ns6:fundID>
                                                                        	<ns8:UlFund>
                                                                        		<ns8:fundID>{ xs:string( data($fund/@fund-name) ) }</ns8:fundID>
                                                                        	</ns8:UlFund>
                                                                        </ns6:fundID>
                                                                    </ns6:PolicyCoverageFundSplit>
                                                            }
                                                            </ns6:policyCoverageFundSplitList>
                                                            <ns6:policyCoverageFundList>
                                                            {
                                                                for $fund-value in $coverage/values/fund-value
                                                                return
                                                                    <ns6:PolicyCoverageFund>
                                                                        <ns6:fundUnit>{ xs:double( data($fund-value/quantity) ) }</ns6:fundUnit>
                                                                        <ns6:fundID>
	                                                                        <ns8:UlFund>
	                                                                        		<ns8:fundID>{ xs:string( data($fund-value/@fund-name) ) }</ns8:fundID>
                                                                        	</ns8:UlFund>
	                                                                    </ns6:fundID>
                                                                    </ns6:PolicyCoverageFund>
                                                            }
                                                            </ns6:policyCoverageFundList>
                                                            <ns6:coverageCategory>
                                                                <ns8:UlCoverageCategory>
                                                                    <ns8:ulCoverageCategory>{ xs:string( data($coverage/@coverage-category) ) }</ns8:ulCoverageCategory>
                                                                </ns8:UlCoverageCategory>
                                                            </ns6:coverageCategory>
                                                            <ns6:coverageStatus>
                                                                <ns8:UlContractStatus>
                                                                    <ns8:ulContractStatus>{ xs:string( data($coverage/status) ) }</ns8:ulContractStatus>
                                                                </ns8:UlContractStatus>
                                                            </ns6:coverageStatus>
                                                        </ns6:PolicyCoverage>
                                                }
                                            </ns6:policyCoverageList>
                                    }
                                    <ns6:policyContractAddressList>
                                    {
                                    	let $persons :=
                                    		for $p in $contract/client-list/contract-person
                                    		where $p/@role = '1'
                                    		return
                                    			let $addresses := 
                                    				for $address in $p/address
                                    				where $address/@kind = '4'
                                    				return
	                                        			<ns6:PolicyContractAddress>
		                                                    <ns6:addressType>{ data($address/@kind) }</ns6:addressType>
		                                                    <ns6:addressName>{ data($address/company-name) }</ns6:addressName>
                                                            <ns6:street>{ xs:string( data($address/street) ) }</ns6:street>
                                                            <ns6:house>{ xs:string( data($address/home) ) }</ns6:house>
                                                            <ns6:flat>{ xs:string( data($address/flat) ) }</ns6:flat>
                                                            <ns6:city>{ xs:string( data($address/city) ) }</ns6:city>
                                                            <ns6:zipCode>{ xs:string( data($address/postal-code) ) }</ns6:zipCode>
		                                                    <ns6:state>
		                                                        <ns4:State>
                                                                    <ns4:state>{ xs:string( data($address/county) ) }</ns4:state>
		                                                        </ns4:State>
		                                                    </ns6:state>
		                                                    <ns6:country>
		                                                        <ns4:CountryCode>
                                                                    <ns4:countryCode>{ xs:string( data($address/country) ) }</ns4:countryCode>
		                                                        </ns4:CountryCode>
		                                                    </ns6:country>
	                                        			</ns6:PolicyContractAddress>
	                                        	return $addresses[1]
                                    	return $persons[1]
                                    }
                                    </ns6:policyContractAddressList>
                                    <ns6:policyContractAccountList>
                                    {
                                        for $bank in $contract/banks/bank
                                        return
                                            <ns6:PolicyContractAccount>
                                                <ns6:accountNo>{ xs:string( data($bank/account) ) }</ns6:accountNo>
                                                <ns6:firstName>{ xs:string( data($bank/recipient-name/first-name) ) }</ns6:firstName>
                                                <ns6:name>{ xs:string( data($bank/recipient-name/last-name) ) }</ns6:name>
                                                <ns6:street>{ xs:string( data($bank/recipient-address/street) ) }</ns6:street>
                                                <ns6:house>{ xs:string( data($bank/recipient-address/home) ) }</ns6:house>
                                                <ns6:flat>{ xs:string( data($bank/recipient-address/flat) ) }</ns6:flat>
                                                <ns6:zipCode>{ data($bank/recipient-address/postal-code) }</ns6:zipCode>
                                                <ns6:city>{ xs:string( data($bank/recipient-address/city) ) }</ns6:city>
                                                <ns6:accountType>
                                                	<ns8:UlAccountType>
                                                		<ns8:ulAccountType>{ data($bank/@kind) }</ns8:ulAccountType>
                                                	</ns8:UlAccountType>
                                                </ns6:accountType>
                                                <ns6:country>
                                                    <ns4:CountryCode>
                                                        <ns4:countryCode>{ xs:string( data($bank/recipient-address/country) ) }</ns4:countryCode>
                                                    </ns4:CountryCode>
                                                </ns6:country>
                                            </ns6:PolicyContractAccount>
                                    }
                                    </ns6:policyContractAccountList>
                                    <ns6:productCode>
                                        <ns8:UlParameters>
                                            {
                                                for $product-type in $contract/@product-type
                                                return
                                                    <ns8:productId>{ xs:string( data($product-type) ) }</ns8:productId>
                                            }
                                        </ns8:UlParameters>
                                    </ns6:productCode>
                                    <ns6:policyStatus>
                                        <ns8:UlContractStatus>
                                            <ns8:ulContractStatus>{ xs:string( data($contract/status) ) }</ns8:ulContractStatus>
                                        </ns8:UlContractStatus>
                                    </ns6:policyStatus>
                                </ns6:PolicyContract>
                        }
                    </ns3:policyContract>
                </ns3:Customer>
            </ns9:customer>
        </ns9:invokeResponse>
};

declare variable $getContractResponse1 as element(ns1:getContractResponse) external;

xf:ResponseTransform($getContractResponse1)]]></con:xquery>
</con:xqueryEntry>