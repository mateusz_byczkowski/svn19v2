<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-12-21</con:description>
    <con:xquery><![CDATA[declare namespace crw = "http://bzwbk.com/crw/services/quickwin/";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn = "urn:be.services.dcl";

declare namespace urn1 = "urn:accounts.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:productstree.entities.be.dcl";
declare namespace urn4 = "urn:insurance.entities.be.dcl";
declare namespace urn5 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace urn6 = "urn:cif.entities.be.dcl";
declare namespace urn7 = "urn:filtersandmessages.entities.be.dcl";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare variable $body as element(soap:Body) external;


declare function local:datetime2date($x as xs:dateTime) as xs:string {
	let $m := 
		if (fn:month-from-dateTime($x) < 10) then
			fn:concat('0', xs:string(fn:month-from-dateTime($x)))
		else
			xs:string(fn:month-from-dateTime($x))

	let $d := 
		if (fn:day-from-dateTime($x) < 10) then
			fn:concat('0', xs:string(fn:day-from-dateTime($x)))
		else
			xs:string(fn:day-from-dateTime($x))

	return fn:concat(xs:string(fn:year-from-dateTime($x)), '-', $m, '-', $d)
};



declare function local:readApplicationResponse($resp as element(crw:readApplicationResponse))
    as element(urn:invokeResponse) {
        <urn:invokeResponse>
         <urn:linkedAccount>
            <urn1:Account>
               <urn1:accountNumber?>{ data($resp/application/linkedAccount/accountNumber) }</urn1:accountNumber>
               <urn1:accountDescription?>{ data($resp/application/linkedAccount/accountDescription) }</urn1:accountDescription>
               <urn1:loanAccount?>
                  <urn1:LoanAccount?>
                     <urn1:faceAmount?>{ data($resp/application/linkedAccount/faceAmount) }</urn1:faceAmount>
                  </urn1:LoanAccount>
               </urn1:loanAccount>
               <urn1:tranAccount?>
                  <urn1:TranAccount?>
                     <urn1:totalLimitAmount?>{ data($resp/application/linkedAccount/totalLimitAmount) }</urn1:totalLimitAmount>
                  </urn1:TranAccount>
               </urn1:tranAccount>
               <urn1:currency?>
                  <urn2:CurrencyCode?>
                     <urn2:currencyCode?>{ data($resp/application/linkedAccount/currency) }</urn2:currencyCode>
                  </urn2:CurrencyCode>
               </urn1:currency>
               <urn1:productDefinition?>
                  <urn3:ProductDefinition?>
                     <urn3:idProductDefinition?>{ data($resp/application/linkedAccount/idProductDefinition) }</urn3:idProductDefinition>
                  </urn3:ProductDefinition>
               </urn1:productDefinition>
            </urn1:Account>
         </urn:linkedAccount>


         <urn:insurancePolicyAcc>
            <urn4:InsurancePolicyAcc>
				{ if (data($resp/application/actualDate) ne '') then
               <urn4:actualDate?>{ local:datetime2date(data($resp/application/actualDate)) }</urn4:actualDate>
			   else ''
				}
				{ if (data($resp/application/startDate) ne '') then
               <urn4:startDate?>{ local:datetime2date(data($resp/application/startDate)) }</urn4:startDate>
			   else ''
				}
				{ if (data($resp/application/validityDate) ne '') then
               <urn4:validityDate?>{ local:datetime2date(data($resp/application/validityDate)) }</urn4:validityDate>
			   else ''
				}				
               <urn4:fixedPremium?>{ data($resp/application/fixedPremium) }</urn4:fixedPremium>
				{ if (data($resp/application/premiumDueDate) ne '') then
               <urn4:premiumDueDate?>{ local:datetime2date(data($resp/application/premiumDueDate)) }</urn4:premiumDueDate>
			   else ''
				}
               <urn4:premiumFrequency?>{ data($resp/application/premiumFrequency) }</urn4:premiumFrequency>
               <urn4:externalPolices?>{ data($resp/application/externalPolices) }</urn4:externalPolices>
               <urn4:saleEmployeeID?>{ data($resp/application/saleEmployeeID) }</urn4:saleEmployeeID>
               <urn4:insuranceDescription?>{ data($resp/application/insuranceDescription) }</urn4:insuranceDescription>
               <urn4:icbsProductNumber?>{ data($resp/application/icbsProductNumber) }</urn4:icbsProductNumber>
				<urn4:accountRelationshipList>
			   {for $role in $resp/application/customerRoles
				return
					<urn6:AccountRelationship>
					   <urn6:customer>
						  <urn6:Customer>
							 <urn6:companyName?>{ data($role/customer/companyName) }</urn6:companyName>
							 <urn6:customerNumber?>{ data($role/customer/customerNumber) }</urn6:customerNumber>
							 <urn6:documentList>
								<urn6:Document>
								   <urn6:documentNumber?>{ data($role/customer/documentNumber) }</urn6:documentNumber>
								   <urn6:documentType>
									  <urn2:CustomerDocumentType>
										 <urn2:customerDocumentType?>{ data($role/customer/documentType) }</urn2:customerDocumentType>
									  </urn2:CustomerDocumentType>
								   </urn6:documentType>
								</urn6:Document>
							 </urn6:documentList>
							 <urn6:customerPersonal>
								<urn6:CustomerPersonal>
								   <urn6:lastName?>{ data($role/customer/lastName) }</urn6:lastName>
								   <urn6:firstName?>{ data($role/customer/firstName) }</urn6:firstName>
								</urn6:CustomerPersonal>
							 </urn6:customerPersonal>
						  </urn6:Customer>
					   </urn6:customer>
					   <urn6:relationship>
						  <urn2:CustomerAccountRelationship>
							 <urn2:customerAccountRelationship?>{ data($role/relationship) }</urn2:customerAccountRelationship>
						  </urn2:CustomerAccountRelationship>
					   </urn6:relationship>
					</urn6:AccountRelationship>
				}
				 </urn4:accountRelationshipList>
				 <urn4:additionalInfoList?>
				 {for $info in $resp/application/additionalInfos
				return
						<urn7:AdditionalInfo?>
						   <urn7:value?>{ data($info/value) }</urn7:value>
						   <urn7:additionalInfoDefinition?>
							  <urn2:AdditionalInfoDefinition?>
								 <urn2:additionalInfoDefinition?>{ data($info/definition) }</urn2:additionalInfoDefinition>
							  </urn2:AdditionalInfoDefinition>
						   </urn7:additionalInfoDefinition>
						</urn7:AdditionalInfo>
				}
				</urn4:additionalInfoList>

		
               <urn4:currency>
                  <urn2:CurrencyCode>
                     <urn2:currencyCode?>{ data($resp/application/currency) }</urn2:currencyCode>
                  </urn2:CurrencyCode>
               </urn4:currency>
               <urn4:premiumPeriod>
                  <urn2:Period>
                     <urn2:period?>{ data($resp/application/premiumPeriod) }</urn2:period>
                  </urn2:Period>
               </urn4:premiumPeriod>
               <urn4:oddPremiumDay>
                  <urn2:SpecialDay>
                     <urn2:specialDay?>{ data($resp/application/oddPremiumDay) }</urn2:specialDay>
                  </urn2:SpecialDay>
               </urn4:oddPremiumDay>
				<urn4:insPolicyApplication>
					<urn4:InsPolicyApplication>
						   <urn4:applicationNumber?>{ data($resp/application/number) }</urn4:applicationNumber>
						   <urn4:owuSignature?>{ data($resp/application/owuSignature) }</urn4:owuSignature>
						   <urn4:loanApplicationNum?>{ data($resp/application/loanApplicationNum) }</urn4:loanApplicationNum>
						   <urn4:applicationStatus?>
							  <urn5:InsPolicyAppStatus?>
								 <urn5:insPolicyAppStatus?>{ data($resp/application/applicationStatus) }</urn5:insPolicyAppStatus>
							  </urn5:InsPolicyAppStatus>
						   </urn4:applicationStatus>
					</urn4:InsPolicyApplication>
				</urn4:insPolicyApplication>
            </urn4:InsurancePolicyAcc>
         </urn:insurancePolicyAcc>

         <urn:maintenanceAccount>
            <urn1:Account>
               <urn1:accountNumber?>{ data($resp/application/maintenanceAccount/accountNumber) }</urn1:accountNumber>
               <urn1:accountDescription?>{ data($resp/application/maintenanceAccount/accountDescription) }</urn1:accountDescription>
               <urn1:currency?>
                  <urn2:CurrencyCode?>
                     <urn2:currencyCode?>{ data($resp/application/maintenanceAccount/currency) }</urn2:currencyCode>
                  </urn2:CurrencyCode>
               </urn1:currency>
               <urn1:productDefinition?>
                  <urn3:ProductDefinition?>
                     <urn3:idProductDefinition?>{ data($resp/application/maintenanceAccount/idProductDefinition) }</urn3:idProductDefinition>
                  </urn3:ProductDefinition>
               </urn1:productDefinition>
            </urn1:Account>
         </urn:maintenanceAccount>

        </urn:invokeResponse>
};



declare function local:createApplicationResponse($resp as element(crw:createApplicationResponse))
    as element(urn:invokeResponse) {
        <urn:invokeResponse>
			<urn:insPolicyApplication>
				<urn4:InsPolicyApplication>
					<urn4:applicationNumber>{ data($resp/number) }</urn4:applicationNumber>
				</urn4:InsPolicyApplication>
			</urn:insPolicyApplication>
		</urn:invokeResponse>
};


declare function local:emptyResponse() as element(urn:invokeResponse) {
	<urn:invokeResponse>
	</urn:invokeResponse>
};

declare function local:faultMapping($fault as element(soap:Fault)) as element(soap:Fault) {
      <soap:Fault>
         <faultcode>{ data($fault/faultcode) }</faultcode>
         <faultstring>{ data($fault/faultstring) }</faultstring>
         <detail>
            <err:ServiceFailException xmlns:err="urn:errors.hlbsentities.be.dcl">
               <err:exceptionItem>
                  <err:errorCode1>0</err:errorCode1>
                  <err:errorCode2>0</err:errorCode2>
                  <err:errorDescription>{ data($fault/faultstring) }</err:errorDescription>
               </err:exceptionItem>
            </err:ServiceFailException>
         </detail>
      </soap:Fault>
};



declare function local:unknownResponse() as element(soap:Fault) {
      <soap:Fault>
         <faultcode>UNKNOWN</faultcode>
         <faultstring>Nieznany błąd systemowy na ALSB</faultstring>
         <detail>
            <err:ServiceFailException xmlns:err="urn:errors.hlbsentities.be.dcl">
               <err:exceptionItem>
                  <err:errorCode1>0</err:errorCode1>
                  <err:errorCode2>0</err:errorCode2>
                  <err:errorDescription>Nieznany błąd systemowy na ALSB</err:errorDescription>
               </err:exceptionItem>
            </err:ServiceFailException>
         </detail>
      </soap:Fault>
};



typeswitch($body/*)
	case $resp as element(crw:readApplicationResponse) return local:readApplicationResponse($resp)
	case $resp as element(crw:createApplicationResponse) return local:createApplicationResponse($resp)
	case $resp as element(crw:updateApplicationResponse ) return local:emptyResponse()
	case $resp as element(crw:updateApplicationNotNullResponse ) return local:emptyResponse()
	case $resp as element(soap:Fault) return local:faultMapping($resp)
	default $resp return local:unknownResponse()]]></con:xquery>
</con:xqueryEntry>