<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns4 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/StreamLine/UpdateCancelPolicy/RequestTransformNew/";
declare namespace ns0 = "urn:baseentities.be.dcl";
declare namespace ns7 = "urn:entities.be.dcl";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns3 = "urn:applicationul.entities.be.dcl";
declare namespace ns2 = "urn:basedictionaries.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";
declare namespace ns6 = "urn:baseauxentities.be.dcl";

declare function xf:RequestTransformNew($invoke1 as element(ns5:invoke),
    $header1 as element(ns5:header))
    as element(ns-1:performContractCancelRequest) {
        <ns-1:performContractCancelRequest>
            <envelope>
                <request-time>{ data($header1/ns5:msgHeader/ns5:timestamp) }</request-time>
                <request-no>{ data($header1/ns5:msgHeader/ns5:msgId) }</request-no>
                <source-code>{ data($invoke1/ns5:sourceCode/ns6:StringHolder/ns6:value) }</source-code>
                <user-id>{ data($header1/ns5:msgHeader/ns5:userId) }</user-id>
                <branch-id>{ data($header1/ns5:msgHeader/ns5:unitId) }</branch-id>
                <bar-code>{ data($invoke1/ns5:barCodeID/ns6:StringHolder/ns6:value) }</bar-code>
            </envelope>
            <save>{ data($invoke1/ns5:mode/ns6:BooleanHolder/ns6:value) }</save>
            <product-type>{ data($invoke1/ns5:policyContract/ns3:PolicyContract/ns3:productCode/ns4:UlParameters/ns4:productId) }</product-type>
            <contract-number>{ data($invoke1/ns5:policyContract/ns3:PolicyContract/ns3:policyID) }</contract-number>
            {
                for $value in $invoke1/ns5:operationDate/ns6:DateHolder/ns6:value
                return
                    <effective>{ xs:date( fn:substring-before(fn:concat(data($value), 'T'), 'T') ) }</effective>
            }
            {
                for $opResignCause in $invoke1/ns5:policyContract/ns3:PolicyContract/ns3:resignCause/ns4:OpResignCause/ns4:strlKey
                return
                    <status-reason>{ xs:int( data($opResignCause) ) }</status-reason>
            }
        </ns-1:performContractCancelRequest>
};

declare variable $invoke1 as element(ns5:invoke) external;
declare variable $header1 as element(ns5:header) external;

xf:RequestTransformNew($invoke1,
    $header1)]]></con:xquery>
</con:xqueryEntry>