<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$2.2011-02-04</con:description>
    <con:xquery><![CDATA[declare namespace ns0="urn:insurancedict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:insurance.entities.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:filtersandmessages.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};


declare function getFields($parm as element(ns4:header), $parm2 as element(ns4:invoke)) as element()*
{

<DC_TRN_ID?>{data($parm/ns4:transHeader/ns4:transId)}</DC_TRN_ID>
,
<DC_UZYTKOWNIK?>{concat("SKP:", data($parm/ns4:msgHeader/ns4:userId))}</DC_UZYTKOWNIK>
,
<DC_ODDZIAL?>{chkUnitId(data($parm/ns4:msgHeader/ns4:unitId))}</DC_ODDZIAL>
,
<DC_RODZAJ_RACHUNKU>IS</DC_RODZAJ_RACHUNKU>
,
<DC_NR_RACHUNKU?>{data($parm2/ns4:insurancePolicyAcc/ns3:InsurancePolicyAcc/ns3:policyRefNum)}</DC_NR_RACHUNKU>
,
<DC_DATA_WYGASNIECIA?>{data($parm2/ns4:insurancePolicyAcc/ns3:InsurancePolicyAcc/ns3:validityDate)}</DC_DATA_WYGASNIECIA>
,
<DC_POWOD_REZ_POLISY?>{data($parm2/ns4:insurancePolicyAcc/ns3:InsurancePolicyAcc/ns3:policyResignCause/ns0:PolicyResignCause/ns0:policyResignCause)}</DC_POWOD_REZ_POLISY>

};


<soap:Body>
  <fml:FML32>
    {getFields($header/ns4:header, $body/ns4:invoke)}
</fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>