<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:insurance.entities.be.dcl";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};

declare function xf:getFields($parm as element(ns6:invoke), $msghead as element(ns6:msgHeader), $tranhead as element(ns6:transHeader))
	as element(fml:FML32) {

let $msgId:= $msghead/ns6:msgId
let $companyId:= $msghead/ns6:companyId
let $userId := $msghead/ns6:userId
let $appId:= $msghead/ns6:appId
let $unitId := $msghead/ns6:unitId
let $timestamp:= $msghead/ns6:timestamp

let $transId:=$tranhead/ns6:transId

return
  <fml:FML32>
     <DC_TRN_ID?>{data($transId)}</DC_TRN_ID>
     <DC_UZYTKOWNIK?>{concat("SKP:", data($userId))}</DC_UZYTKOWNIK>
     <DC_ODDZIAL?>{chkUnitId(data($unitId))}</DC_ODDZIAL>
     <DC_IS_NR_REFF_POLISY?>{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:policyRefNum)}</DC_IS_NR_REFF_POLISY>
     <DC_DATA_WAZNOSCI?>{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:validityDate)}</DC_DATA_WAZNOSCI>
     <DC_IS_DNI_POWIAD_PRZED_WYGAS?>{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:reminderDays)}</DC_IS_DNI_POWIAD_PRZED_WYGAS>
     <DC_IS_STALA_SKLADKA_UBEZP?>{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:fixedPremium)}</DC_IS_STALA_SKLADKA_UBEZP>
     <DC_IS_DATA_NALEZNOSCI_SKLADKI?>{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:premiumDueDate)}</DC_IS_DATA_NALEZNOSCI_SKLADKI>
     <DC_IS_NIETYPOWY_DZIEN_SKLADKI?>{data($parm/ns6:insurancePolicy/ns5:InsurancePolicyAcc/ns5:oddPremiumDay/ns3:SpecialDay/ns3:specialDay)}</DC_IS_NIETYPOWY_DZIEN_SKLADKI>
	 {
     if (data($parm/ns6:account/ns1:Account/ns1:accountNumber)) 
      then <DC_IS_NR_RACHUNKU_W_SYSTEMIE?>{cutStr(data($parm/ns6:account/ns1:Account/ns1:accountNumber),12)}</DC_IS_NR_RACHUNKU_W_SYSTEMIE>
     else()
	 }
  </fml:FML32>
};

<soap:Body>
     { xf:getFields($body/ns6:invoke, $header/ns6:header/ns6:msgHeader, $header/ns6:header/ns6:transHeader) }
</soap:Body>]]></con:xquery>
</con:xqueryEntry>