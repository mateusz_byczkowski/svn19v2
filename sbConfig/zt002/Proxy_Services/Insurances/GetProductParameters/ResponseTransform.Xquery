<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/GetProductParameters/ResponseTransform/";
declare namespace ns0 = "urn:baseentities.be.dcl";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf-product-types";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns2 = "urn:uldictionary.dictionaries.be.dcl";

declare function xf:ResponseTransform($getProductResponse1 as element(ns-1:getProductResponse))
    as element(ns3:invokeResponse) {
        <ns3:invokeResponse>
            <ns3:messageHelper>
                <ns0:MessageHelper>
                    <ns0:errorCode?></ns0:errorCode>
                    <ns0:errorType?></ns0:errorType>
                    <ns0:message?></ns0:message>
                </ns0:MessageHelper>
            </ns3:messageHelper>
            <ns3:ulParameters>
                {
                    let $product := $getProductResponse1/product
                    return
                        <ns2:UlParameters>
                            <ns2:productId>{ xs:string( data($product/@name) ) }</ns2:productId>
                            <ns2:productName>{ xs:string( data($product/description) ) }</ns2:productName>
                            <ns2:productCategory?>{ xs:string( data($product/category) ) }</ns2:productCategory>
                            <ns2:parametersCardSig>{ xs:string( data($product/param-card-sig) ) }</ns2:parametersCardSig>
                            <ns2:owuSig>{ xs:string( data($product/owu/signature) ) }</ns2:owuSig>
                            <ns2:regulationSig>{ xs:string( data($product/regulation-sig) ) }</ns2:regulationSig>
                            <ns2:policyCurrency>{ data($product/currency) }</ns2:policyCurrency>
                            <ns2:minCustomerAge>{ data($product/min-customer-age) }</ns2:minCustomerAge>
                            <ns2:minFirstPayment>{ xs:double( data($product/min-first-payment) ) }</ns2:minFirstPayment>
                            <ns2:minWithdrawal>{ xs:double( data($product/min-withdrawal) ) }</ns2:minWithdrawal>
                            <ns2:minPolicyValue>{ xs:double( data($product/min-product-value) ) }</ns2:minPolicyValue>
                            <ns2:fundMinSplitPercentage>{ xs:double( data($product/fund-min-split-percentage) ) }</ns2:fundMinSplitPercentage>
                            <ns2:minNextPayment>{ xs:double( data($product/min-additional-premium) ) }</ns2:minNextPayment>
                            <ns2:minBenefit>{ xs:double( data($product/min-percentage-benefit) ) }</ns2:minBenefit>
                            <ns2:maxDateFirstPayment>{ data($product/max-day-first-payment) }</ns2:maxDateFirstPayment>
                            <ns2:maxBenefit>{ xs:double( data($product/max-sum-percentage-benefit) ) }</ns2:maxBenefit>
                        </ns2:UlParameters>
                }
            </ns3:ulParameters>
        </ns3:invokeResponse>
};

declare variable $getProductResponse1 as element(ns-1:getProductResponse) external;

xf:ResponseTransform($getProductResponse1)]]></con:xquery>
</con:xqueryEntry>