<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;

<soap-env:Body>
   {
    <Body>
      {if(string-length(data($body/soap-env:Fault/faultstring)) > 0)
          then 
            if(data($body/soap-env:Fault/faultstring) = "com.bzwbk.AlreadyExistException")
               then <Error>F</Error>
               else <Error>T</Error>

          else <Error>F</Error>
      }
    </Body>
   }

</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>