<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns4 = "urn:basedictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/SaveWithdrawal/RequestTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns7 = "urn:be.services.dcl";
declare namespace ns1 = "urn:baseentities.be.dcl";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns3 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns5 = "urn:applicationul.entities.be.dcl";
declare namespace ns2 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns6 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns8 = "urn:baseauxentities.be.dcl";
declare namespace ns9 = "urn:entities.be.dcl";
declare namespace ns10 = "urn:dictionaries.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns7:invoke), $header1 as element(ns7:header))
    as element(ns0:performContractWithdrawalRequest) {
        <ns0:performContractWithdrawalRequest>
            <envelope>
                <request-time>{ data($header1/ns7:msgHeader/ns7:timestamp) }</request-time>
                <request-no?>{ data($header1/ns7:msgHeader/ns7:msgId) }</request-no>
                <source-code?>{data($invoke1/ns7:sourceCode/ns8:StringHolder/ns8:value)}</source-code>
                <user-id?>{data($header1/ns7:msgHeader/ns7:userId)}</user-id>
                <branch-id?>{data($header1/ns7:msgHeader/ns7:unitId)}</branch-id>
                <bar-code?>{data($invoke1/ns7:barCodeID/ns8:StringHolder/ns8:value)}</bar-code>
            </envelope>
            <save>{ data($invoke1/ns7:mode/ns8:BooleanHolder/ns8:value) }</save>
            <product-type>{ data($invoke1/ns7:policyContract/ns5:PolicyContract/ns5:productCode/ns6:UlParameters/ns6:productId) }</product-type>
            <contract-number>{ data($invoke1/ns7:policyContract/ns5:PolicyContract/ns5:policyID) }</contract-number>
            <coverage-number>{ data($invoke1/ns7:policyContract/ns5:PolicyContract/ns5:policyCoverageList/ns5:PolicyCoverage[1]/ns5:coverageID) }</coverage-number>
            <fund-from>
            {
                for $PolicyOrderFundFrom in $invoke1/ns7:policyContract/ns5:PolicyContract/ns5:policyOrder/ns5:PolicyOrder/ns5:policyOrderFundFromList/ns5:PolicyOrderFundFrom
                return
                    <fund-change fund-name= "{ data($PolicyOrderFundFrom/ns5:fundID/ns6:UlFund/ns6:fundID) }" >
                         <value?>{ data($PolicyOrderFundFrom/ns5:value) }</value>
                         <percentage?>{data($PolicyOrderFundFrom/ns5:percentage)}</percentage>
                         <unit-count?></unit-count>
                         <price?></price>
                         <fee?></fee>
                         <tax?></tax>
                    </fund-change>
            }
            </fund-from>
            {
               let  $PolicyOrder := $invoke1/ns7:policyContract/ns5:PolicyContract/ns5:policyOrder/ns5:PolicyOrder
               return
               <bank kind = "{ data($PolicyOrder/ns5:accountType/ns6:UlAccountType/ns6:ulAccountType) }">
		           	<account>{xs:string(data($PolicyOrder/ns5:accountNoWithdrawal))}</account>
		        	<recipient-name>
						<first-name?>{ data($PolicyOrder/ns5:withdrawalFirstName) }</first-name>
		            	<last-name>{ data($PolicyOrder/ns5:withdrawalName) }</last-name>
		        	</recipient-name>

               		<recipient-address kind = "{ data($invoke1/ns7:addressType/ns8:StringHolder/ns8:value) }">
                		<street?>{ data($PolicyOrder/ns5:withdrawalStreet) }</street>
                		<home?>{ data($PolicyOrder/ns5:withdrawalHouse) }</home>
                		<flat?>{ data($PolicyOrder/ns5:withdrawalFlat) }</flat>
                    	<postal-code?>{ data($PolicyOrder/ns5:withdrawalZipCode) }</postal-code>
                    	<city?>{ data($PolicyOrder/ns5:withdrawalCity) }</city>
                    	<country?>{ data($PolicyOrder/ns5:withdrawalCountry/ns10:CountryCode/ns10:countryCode) }</country>
	                </recipient-address>


    			</bank>
            }
      </ns0:performContractWithdrawalRequest>
};

declare variable $invoke1 as element(ns7:invoke) external;
declare variable $header1 as element(ns7:header) external;

xf:RequestTransform($invoke1, $header1)]]></con:xquery>
</con:xqueryEntry>