<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/Testy_XQuery/Us�ugi/StreamLine/SaveFullWithdrawal/ResponseTransform/";
declare namespace ns0 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "urn:baseentities.be.dcl";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace cl = "http://sygnity.pl/functions";

declare function xf:ResponseTransform($performContractSurrenderResponse1 as element(ns0:performContractSurrenderResponse), $codes as element(cl:codes))
    as element(ns2:invokeResponse) {
        <ns2:invokeResponse>
            <ns2:messageHelper>
                {
                    for $validation in $performContractSurrenderResponse1/validations/validation
                    return
                        <ns1:MessageHelper>
                            <ns1:errorCode>{ 
								let $allCodes :=
									for $code in $codes/cl:code
									where $code/@any-code = $validation/code
									return data($code/@nfe-code)
								return
									if($allCodes[1] != '') then
										$allCodes[1]
									else
										'K00385'
                             }</ns1:errorCode>
                            <ns1:errorType>{ data($validation/severity) }</ns1:errorType>
                            <ns1:message?></ns1:message>
                        </ns1:MessageHelper>
                }
            </ns2:messageHelper>
        </ns2:invokeResponse>
};

declare variable $performContractSurrenderResponse1 as element(ns0:performContractSurrenderResponse) external;
declare variable $codes as element(cl:codes) external;

xf:ResponseTransform($performContractSurrenderResponse1, $codes)]]></con:xquery>
</con:xqueryEntry>