<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf="urn:dcl:services.alsb.datamodel";

declare namespace  dcl="urn:be.services.dcl";
declare namespace  ns1="urn:operations.entities.be.dcl";
declare namespace  ns2="urn:operationsdictionary.dictionaries.be.dcl";
declare namespace  ns3="urn:dictionaries.be.dcl";
declare namespace  ns4="urn:basedictionaries.be.dcl";
declare namespace  ns5="urn:filtersandmessages.entities.be.dcl";



declare variable $body external;
declare variable $fml as element(FML32):=$body/FML32;

declare function xf:dateTimeFromString($dateIn as xs:string) as xs:string 
{
     fn:concat (fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2),
                        "T00:00:00")
};

<soap-env:Body> 
     <dcl:invokeResponse> 
        
        <dcl:result>
            <ns5:ResponseMessage>
                    <ns5:result>
                        { if  ($fml/TR_STATUS = "P")
                           then     "true"
                           else     "false"}
                      </ns5:result>
            </ns5:ResponseMessage>
        </dcl:result>

          <dcl:transaction>
              <ns1:Transaction>
                  <ns1:transactionID>{data($fml/TR_ID_OPER)}</ns1:transactionID>
                  <ns1:transactionDate>{xf:dateTimeFromString($fml/TR_DATA_KSIEG)}</ns1:transactionDate>
                  <ns1:transactionStatus>
                    <ns3:TransactionStatus>
                       <ns3:transactionStatus>{data($fml/TR_STATUS)}</ns3:transactionStatus>
                    </ns3:TransactionStatus>
                  </ns1:transactionStatus>
               </ns1:Transaction>
         </dcl:transaction>

        <dcl:errCode>
            { if  ($fml/TR_KOD_BLEDU_1 != "000")  then
                <ns5:ResponseMessage>
                         <ns5:errorCode>{data($fml/TR_KOD_BLEDU_1)}</ns5:errorCode>
                         <ns5:errorDescription></ns5:errorDescription>
                </ns5:ResponseMessage>
                else()
            }
             { if  ($fml/TR_KOD_BLEDU_2 != "000")  then
                 <ns5:ResponseMessage>
                     <ns5:errorCode>{data($fml/TR_KOD_BLEDU_2)}</ns5:errorCode>
                     <ns5:errorDescription></ns5:errorDescription>
                 </ns5:ResponseMessage>
                   else()             
             }
             { if  ($fml/TR_KOD_BLEDU_3 != "000")  then
                 <ns5:ResponseMessage>
                     <ns5:errorCode>{data($fml/TR_KOD_BLEDU_3)}</ns5:errorCode>
                     <ns5:errorDescription></ns5:errorDescription>
                 </ns5:ResponseMessage>
                   else()             
             }
             { if  ($fml/TR_KOD_BLEDU_4 != "000")  then
                 <ns5:ResponseMessage>
                     <ns5:errorCode>{data($fml/TR_KOD_BLEDU_4)}</ns5:errorCode>
                     <ns5:errorDescription></ns5:errorDescription>
                 </ns5:ResponseMessage>
                   else()             
             }
             { if  ($fml/TR_KOD_BLEDU_5 != "000")  then
                 <ns5:ResponseMessage>
                     <ns5:errorCode>{data($fml/TR_KOD_BLEDU_5)}</ns5:errorCode>
                     <ns5:errorDescription></ns5:errorDescription>
                 </ns5:ResponseMessage>
                   else()             
             }
         </dcl:errCode>
         
        <dcl:resultCode>
            <ns5:ResponseMessage>
                    <ns5:resultCode> {data($fml/TR_STATUS)} </ns5:resultCode>
            </ns5:ResponseMessage>
        </dcl:resultCode>

        <dcl:errorDescription>
            <ns5:ResponseMessage>
                    <ns5:errorDescription> {data($fml/TR_OPIS_BLEDU)} </ns5:errorDescription>
            </ns5:ResponseMessage>
        </dcl:errorDescription>

   </dcl:invokeResponse> 

</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>