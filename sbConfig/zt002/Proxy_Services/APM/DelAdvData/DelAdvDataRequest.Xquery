<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapDelAdvDataRequest($req as element(m:DelAdvDataRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if ($req/m:NumerKlienta)
					then <fml:DC_NUMER_KLIENTA>{ data($req/m:NumerKlienta) }</fml:DC_NUMER_KLIENTA>
				else ()
			}
			{
				if ($req/m:IdSpolki)
					then if (data($req/m:IdSpolki))
						then <fml:CI_ID_SPOLKI>{ data($req/m:IdSpolki) }</fml:CI_ID_SPOLKI>
						else <fml:CI_ID_SPOLKI>{0}</fml:CI_ID_SPOLKI>
				else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapDelAdvDataRequest($body/m:DelAdvDataRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>