<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.4
 : @since   2010-01-11
 :
 : wersja WSDLa: 08-01-2010 18:35:54
 :
 : $Proxy Services/Branches/getBranchTills/getBranchTillsResponse.xq$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getBranchTills/getBranchTillsResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns3 = "urn:entities.be.dcl";
declare namespace ns4 = "urn:be.services.dcl";
declare namespace ns5 = "urn:baseauxentities.be.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getBranchTillsResponse($fML321 as element(ns0:FML32))
    as element(ns4:invokeResponse)
{
    <ns4:invokeResponse>
        <ns4:userTxnSessionOut>{
            for $i in 1 to count($fML321/ns0:NF_USERTS_SESSIONNUMBER)
            return
                <ns2:UserTxnSession>

                    <ns2:sessionNumber>{
						data($fML321/ns0:NF_USERTS_SESSIONNUMBER[$i])
					}</ns2:sessionNumber>
					
					(:
					 : niepusta data, różna od 0001-01-01
					 :)
					{
						let $sessionDate := data($fML321/ns0:NF_USERTS_SESSIONDATE[$i])
						return
							if ($sessionDate
								and $sessionDate ne '0001-01-01') then
								<ns2:sessionDate>{
									$sessionDate
								}</ns2:sessionDate>
							else
								()
					}

					(:
					 : niepusta data, różna od '0001-01-01T00:00:00'
					 :)
					{
						let $lastChangeDate := data($fML321/ns0:NF_USERTS_LASTCHANGEDATE[$i])
						return
                    		if ($lastChangeDate
                    			and $lastChangeDate ne '0001-01-01T00:00:00') then
								<ns2:lastChangeDate>{
									$lastChangeDate
								}</ns2:lastChangeDate>
							else
								()
                    }

                    (:
                     : status sesji:
                     : - 0 --> C (zamknięta)
                     : - 1 --> O (otwarta)
                     :)
					{
						let $sessionStatus := data($fML321/ns0:NF_USETSS_USERTXNSESSIONST[$i])
						return
							if ($sessionStatus) then
			                    <ns2:sessionStatus>
			                        <ns1:UserTxnSessionStatus>
			                            <ns1:userTxnSessionStatus>{
			                            	if ($sessionStatus eq '0') then
			                            		'C'
			                            	else
			                            		'O'
			                            }</ns1:userTxnSessionStatus>
			                        </ns1:UserTxnSessionStatus>
			                    </ns2:sessionStatus>
							else
								()
					}

					(:
					 : dane uzytkownika
					 :)
                    <ns2:user>
                        <ns3:User>
                            <ns3:userLastName>{
								data($fML321/ns0:NF_USER_USERLASTNAME[$i])
							}</ns3:userLastName>
							
                            <ns3:userID>{
								data($fML321/ns0:NF_USER_USERID[$i])
							}</ns3:userID>
							
                            <ns3:userFirstName>{
								data($fML321/ns0:NF_USER_USERFIRSTNAME[$i])
							}</ns3:userFirstName>
                        </ns3:User>
                    </ns2:user>

					(:
					 : dane kasy
					 :)
                    <ns2:till>
                        <ns2:Till>
                            <ns2:tillID>{
								data($fML321/ns0:NF_TILL_TILLID[$i])
							}</ns2:tillID>
							
                            <ns2:tillName>{
                            	data($fML321/ns0:NF_TILL_TILLNAME[$i])
							}</ns2:tillName>
							
                            <ns2:tillType>
                                <ns1:TillType>
                                    <ns1:tillType>{
										data($fML321/ns0:NF_TILTY_TILLTYP[$i])
									}</ns1:tillType>
                                </ns1:TillType>
                            </ns2:tillType>
                        </ns2:Till>
                    </ns2:till>

					(:
					 : kasjer
					 :)
                    <ns2:teller>
                        <ns2:Teller>
                            <ns2:tellerID>{
								data($fML321/ns0:NF_TELLER_TELLERID[$i])
							}</ns2:tellerID>
                        </ns2:Teller>
                    </ns2:teller>
                </ns2:UserTxnSession>
		}</ns4:userTxnSessionOut>
		
        <ns4:bcd>
            <ns3:BusinessControlData>
                <ns3:pageControl?>
                    <ns5:PageControl?>
                    	{
                    		let $hasNext := data($fML321/ns0:NF_PAGEC_HASNEXT)
                    		return
                    			if ($hasNext) then
									<ns5:hasNext>{ 
										xs:boolean($hasNext)
									}</ns5:hasNext>
								else
									()
                    	}
						
                        <ns5:navigationKeyDefinition?>{
							data($fML321/ns0:NF_PAGEC_NAVIGATIONKEYDEFI)
						}</ns5:navigationKeyDefinition>
	
                        <ns5:navigationKeyValue?>{
							data($fML321/ns0:NF_PAGEC_NAVIGATIONKEYVALU)
						}</ns5:navigationKeyValue>
						
					</ns5:PageControl>
                </ns3:pageControl>
            </ns3:BusinessControlData>
        </ns4:bcd>
    </ns4:invokeResponse>
};

<soap-env:Body>{
	xf:getBranchTillsResponse($fML321)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>