<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:dictionaries.be.dcl";
declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:insurance.entities.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns4:header)) as element()*
{
	<NF_MSHEAD_MSGID?>{data($parm/ns4:msgHeader/ns4:msgId)}</NF_MSHEAD_MSGID>,
	<NF_MSHEAD_COMPANYID?>{data($parm/ns4:msgHeader/ns4:companyId)}</NF_MSHEAD_COMPANYID>,
	<NF_MSHEAD_UNITID?>{data($parm/ns4:msgHeader/ns4:unitId)}</NF_MSHEAD_UNITID>,
	<NF_MSHEAD_USERID?>{data($parm/ns4:msgHeader/ns4:userId)}</NF_MSHEAD_USERID>,
	<NF_MSHEAD_APPID?>{data($parm/ns4:msgHeader/ns4:appId)}</NF_MSHEAD_APPID>,
	<NF_MSHEAD_TIMESTAMP?>{data($parm/ns4:msgHeader/ns4:timestamp)}</NF_MSHEAD_TIMESTAMP>,
	<NF_TRHEAD_TRANSID?>{data($parm/ns4:transHeader/ns4:transId)}</NF_TRHEAD_TRANSID>
};

declare function getFieldsFromInvoke($parm as element(ns4:invoke)) as element()*
{
	<NF_INSUPA_NUMBER?>{data($parm/ns4:insurancePackageAcc/ns3:InsurancePackageAcc/ns3:number)}</NF_INSUPA_NUMBER>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns4:header)}
    {getFieldsFromInvoke($body/ns4:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>