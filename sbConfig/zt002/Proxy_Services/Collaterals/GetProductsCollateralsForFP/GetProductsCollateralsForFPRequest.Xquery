<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:application.dictionaries.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:applicationsme.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}</NF_TRHEAD_TRANSID>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}

    <NF_CUSTOM_CUSTOMERNUMBER?>{data($body/ns0:invoke/ns0:customer/ns1:Customer/ns1:customerNumber)}</NF_CUSTOM_CUSTOMERNUMBER>
    {
      let $relationships := $body/ns0:invoke/ns0:customer/ns1:Customer/ns1:accountRelationshipList/ns1:AccountRelationship/ns1:relationship/ns4:CustomerAccountRelationship/ns4:customerAccountRelationship
      for $tran at $i in $relationships
      return
    (
    <NF_CUSTAR_CUSTOMERACCOUNTR?>{data($relationships[$i])}</NF_CUSTAR_CUSTOMERACCOUNTR>
    )
    }
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>