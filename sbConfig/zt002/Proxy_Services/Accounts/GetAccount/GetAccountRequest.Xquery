<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbICBSGetAccountRequest($req as element(m:bICBSGetAccountRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:KodRach)
					then <fml:B_KOD_RACH>{ data($req/m:KodRach) }</fml:B_KOD_RACH>
					else ()
			}
			{
				if($req/m:Opcja)
					then <fml:B_OPCJA>{ data($req/m:Opcja) }</fml:B_OPCJA>
					else ()
			}
			{
				if($req/m:RodzDok)
					then <fml:B_RODZ_DOK>{ data($req/m:RodzDok) }</fml:B_RODZ_DOK>
					else ()
			}
			{
				if($req/m:NrDok)
					then <fml:B_NR_DOK>{ data($req/m:NrDok) }</fml:B_NR_DOK>
					else ()
			}
			{
				if($req/m:CifNumber)
					then <fml:E_CIF_NUMBER>{ data($req/m:CifNumber) }</fml:E_CIF_NUMBER>
					else ()
			}
			{
				if($req/m:CifOptions)
					then <fml:E_CIF_OPTIONS>{ data($req/m:CifOptions) }</fml:E_CIF_OPTIONS>
					else ()
			}
			{
				if($req/m:DlNrRach)
					then <fml:B_DL_NR_RACH>{ data($req/m:DlNrRach) }</fml:B_DL_NR_RACH>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapbICBSGetAccountRequest($body/m:bICBSGetAccountRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>