<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:directdebit.entities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

     declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };


declare function getElementsForDirectdebit($parm as element(fml:FML32)) as element()
{

<ns0:directdebit>
  {
    for $x at $occ in $parm/NF_DIRECD_DIRECTDEBITID
    return
    <ns1:DirectDebit>
      <ns1:directDebitId>{data($parm/NF_DIRECD_DIRECTDEBITID[$occ])}</ns1:directDebitId>
      <ns1:originatorReference>{data($parm/NF_DIRECD_ORIGINATORREFERE[$occ])}</ns1:originatorReference>
      <ns1:debtorTaxID>{data($parm/NF_DIRECD_DEBTORTAXID[$occ])}</ns1:debtorTaxID>
      <ns1:debtorName>{data($parm/NF_DIRECD_DEBTORNAME[$occ])}</ns1:debtorName>
    { insertDate(data($parm/NF_DIRECD_AUTHORISATIONDAT[$occ]),"yyyy-MM-dd","ns1:authorisationDate")}

      <ns1:authorisationSetUpUserID>{data($parm/NF_DIRECD_AUTHORISATIONSET[$occ])}</ns1:authorisationSetUpUserID>
      <ns1:authorisationCancelUserID></ns1:authorisationCancelUserID>
  (:    <ns1:authorisationCancelUserID>{data($parm/NF_DIRECD_AUTHORISATIONCAN[$occ])}</ns1:authorisationCancelUserID> :)
      <ns1:directDebitStatus>
        <ns5:DirectDebitStatus>
          <ns5:directDebitStatus>{data($parm/NF_DIREDS_DIRECTDEBITSTATU[$occ])}</ns5:directDebitStatus>
        </ns5:DirectDebitStatus>
      </ns1:directDebitStatus>
    </ns1:DirectDebit>
  }
</ns0:directdebit>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForDirectdebit($parm)}
  <ns0:bcd>
    <ns7:BusinessControlData>
      <ns7:pageControl>
        <ns6:PageControl>
          <ns6:hasNext>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT), "1")}</ns6:hasNext>
          <ns6:navigationKeyDefinition>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns6:navigationKeyDefinition>
          <ns6:navigationKeyValue>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns6:navigationKeyValue>
        </ns6:PageControl>
      </ns7:pageControl>
    </ns7:BusinessControlData>
  </ns0:bcd>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>