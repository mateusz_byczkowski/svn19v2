<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-05-18</con:description>
    <con:xquery><![CDATA[declare namespace urn  = "urn:be.services.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;

<soap-env:Body>
  {
    let $req := $body/urn:invoke

    let $dokument := $req/urn:listaDokumentow/urn:dokument
    let $rodzajDokumentu := $dokument/urn:rodzajDokumentu
    let $numerDokumentu := $dokument/urn:numerDokumentu
    let $numerRachunku := $req/urn:listaRachunkow/urn:numerRachunku
    let $rodzajRachunkow := $req/urn:rodzajRachunkow
    let $opcja := $req/urn:opcja
    let $pelenNumerRachunku := $req/urn:pelenNumerRachunku
    
    return

    <fml:FML32>
      {for $it at $i in $dokument
        return
        (
          <fml:B_RODZ_DOK?>{ data($rodzajDokumentu[$i]) }</fml:B_RODZ_DOK>,
          <fml:B_NR_DOK?>{ data($numerDokumentu[$i]) }</fml:B_NR_DOK>
        )
      }
      {for $it at $i in $numerRachunku
        return
        (
          <fml:B_KOD_RACH?>{ data($numerRachunku[$i]) }</fml:B_KOD_RACH>
        )
      }
      <fml:B_RODZAJ_RACH?>{ data($rodzajRachunkow) }</fml:B_RODZAJ_RACH>
      <fml:B_OPCJA?>{ data($opcja) }</fml:B_OPCJA>
      <fml:B_DL_NR_RACH?>{ data($pelenNumerRachunku) }</fml:B_DL_NR_RACH>
    </fml:FML32>
  }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>