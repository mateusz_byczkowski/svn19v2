<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace ns6="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForCard($parm as element(fml:FML32)) as element()
{

<ns5:card>
  {
    for $x at $occ in $parm/NF_DEBITC_CARDNBR
    return
    <ns6:Card>
      <ns6:cardName?>{data($parm/NF_CARD_CARDNAME[$occ])}</ns6:cardName>
      <ns6:debitCard>
        <ns6:DebitCard>
          <ns6:cardNbr?>{data($parm/NF_DEBITC_CARDNBR[$occ])}</ns6:cardNbr>
          <ns6:virtualCardNbr?>{data($parm/NF_DEBITC_VIRTUALCARDNBR[$occ])}</ns6:virtualCardNbr>
        </ns6:DebitCard>
      </ns6:debitCard>
      <ns6:customer>
        <ns2:Customer>
          <ns2:customerNumber?>{data($parm/NF_CUSTOM_CUSTOMERNUMBER[$occ])}</ns2:customerNumber>
          <ns2:customerPersonal>
            <ns2:CustomerPersonal>
              <ns2:lastName?>{data($parm/NF_CUSTOP_LASTNAME[$occ])}</ns2:lastName>
              <ns2:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME[$occ])}</ns2:firstName>
            </ns2:CustomerPersonal>
          </ns2:customerPersonal>
        </ns2:Customer>
      </ns6:customer>
    </ns6:Card>
  }
</ns5:card>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns5:invokeResponse>
  <ns5:response>
    <ns0:ResponseMessage>
      <ns0:result?>true</ns0:result>
    </ns0:ResponseMessage>
  </ns5:response>
  {getElementsForCard($parm)}
</ns5:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>