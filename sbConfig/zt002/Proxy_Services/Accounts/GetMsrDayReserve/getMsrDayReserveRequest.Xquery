<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:accounts.entities.be.dcl";

declare function local:mapGetMsrDayReserveRequest($req as element())
    as element(fml:FML32) {
        <fml:FML32>
        	<fml:NF_MSRMR_DATEOFPROCESSING>{fn-bea:dateTime-to-string-with-format("yyyyMMdd",data($req/m:msrMonthReserve/e:MSRMonthReserve/e:dateOfProcessing)) }</fml:NF_MSRMR_DATEOFPROCESSING>
        	<fml:NF_ACCOUN_ACCOUNTNUMBER>{ data($req/m:account/e:Account/e:accountNumber) }</fml:NF_ACCOUN_ACCOUNTNUMBER>
                <fml:NF_ACCOUN_ACCOUNTIBAN>{ data($req/m:account/e:Account/e:accountNumber) }</fml:NF_ACCOUN_ACCOUNTIBAN>    
        </fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ local:mapGetMsrDayReserveRequest($body/m:invoke) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>