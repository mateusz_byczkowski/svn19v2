<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-04-06</con:description>
    <con:xquery><![CDATA[declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForRelationships($parm as element(fml:FML32)) as element()
{

<ns6:relationships>
  {
    for $x at $occ in $parm/NF_ACCOUR_RELATIONSHIP
    return
    <ns4:AccountRelationship>
      <ns4:relationship>
        <ns3:CustomerAccountRelationship>
          <ns3:customerAccountRelationship>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}</ns3:customerAccountRelationship>
        </ns3:CustomerAccountRelationship>
      </ns4:relationship>
      <ns4:applicationNumber>
        <ns3:ApplicationNumber>
          <ns3:applicationNumber>{data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ])}</ns3:applicationNumber>
        </ns3:ApplicationNumber>
      </ns4:applicationNumber>
    </ns4:AccountRelationship>
  }
</ns6:relationships>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns6:invokeResponse>
  <ns6:response>
    <ns2:ResponseMessage>
      <ns2:result>true</ns2:result>
    </ns2:ResponseMessage>
  </ns6:response>
  {getElementsForRelationships($parm)}
</ns6:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>