<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:entities.be.dcl";
declare namespace ns10="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns8="urn:be.services.dcl";
declare namespace fml="";
declare namespace ns9="urn:card.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function boolean2SourceValue($parm as xs:string*,$trueval as xs:string,$falseval as xs:string) as xs:string* {
    if ($parm  = "true") then $trueval
      else if ($parm  = "1") then $trueval
      else $falseval
};


declare function getFieldsFromHeader($parm as element(ns8:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns8:msgHeader/ns8:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns8:msgHeader/ns8:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns8:msgHeader/ns8:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns8:msgHeader/ns8:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns8:msgHeader/ns8:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns8:msgHeader/ns8:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns8:transHeader/ns8:transId)}</NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns8:invoke)) as element()*
{

<NF_DEBITC_VIRTUALCARDNBR?>{data($parm/ns8:debitCard/ns9:DebitCard/ns9:virtualCardNbr)}</NF_DEBITC_VIRTUALCARDNBR>
,
<NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns8:customerForList/ns3:Customer/ns3:customerNumber)}</NF_CUSTOM_CUSTOMERNUMBER>
,
<NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns8:customerUserCard/ns3:Customer/ns3:customerNumber)}</NF_CUSTOM_CUSTOMERNUMBER>
,
<NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns8:account/ns1:Account/ns1:accountNumber)}</NF_ACCOUN_ACCOUNTNUMBER>
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns10:PageControl/ns10:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns10:PageControl/ns10:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{boolean2SourceValue(data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns10:PageControl/ns10:reverseOrder),"1","0")}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns10:PageControl/ns10:navigationKeyDefinition)}</NF_PAGEC_NAVIGATIONKEYDEFI>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns10:PageControl/ns10:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns8:header)}
    {getFieldsFromInvoke($body/ns8:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>