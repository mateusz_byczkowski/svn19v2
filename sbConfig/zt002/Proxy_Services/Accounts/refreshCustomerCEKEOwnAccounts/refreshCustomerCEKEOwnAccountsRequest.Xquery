<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "http://bzwbk.com/services/ceke/messages/";

declare function xf:map_refreshCustomerCEKEOwnAccountsRequest($req as element(urn:refreshCustomerCEKEOwnAccountsRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				let $E_CIF_NUMBER := $req/urn:CustomerNumber

				for $it at $p in $req/urn:CustomerNumber
				return
				(
					<fml:E_CIF_NUMBER?>{ data($E_CIF_NUMBER[$p]) }</fml:E_CIF_NUMBER>
				)
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;

<soap-env:Body>
{ xf:map_refreshCustomerCEKEOwnAccountsRequest($body/urn:refreshCustomerCEKEOwnAccountsRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>