<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns4="urn:filtersandmessages.entities.be.dcl";
declare namespace ns6="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  <ns0:response>
    <ns4:ResponseMessage>
      <ns4:result>true</ns4:result>
      <ns4:errorCode>{data($parm/NF_RESPOM_ERRORCODE)}</ns4:errorCode>
      <ns4:errorDescription>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}</ns4:errorDescription>
    </ns4:ResponseMessage>
  </ns0:response>
  <ns0:relationship>
    <ns2:AccountRelationship>
          {
            if($parm/NF_ACCOUR_RELATIONSHIP[1]) then
      <ns2:relationship>{data($parm/NF_ACCOUR_RELATIONSHIP[1])}</ns2:relationship>
            else ()
          }
      <ns2:applicationNumber>
        <ns3:ApplicationNumber>
          {
            if($parm/NF_APPLIN_APPLICATIONNUMBE[1]) then
 <ns3:applicationNumber>{data($parm/NF_APPLIN_APPLICATIONNUMBE[1])}</ns3:applicationNumber>
            else ()
          }
        </ns3:ApplicationNumber>
      </ns2:applicationNumber>
    </ns2:AccountRelationship>
  </ns0:relationship>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>