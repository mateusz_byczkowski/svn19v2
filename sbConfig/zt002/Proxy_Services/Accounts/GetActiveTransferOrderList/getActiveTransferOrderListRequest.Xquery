<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-04-06</con:description>
    <con:xquery><![CDATA[declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns3="urn:baseauxentities.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="urn:transferorder.entities.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:transferorderdict.dictionaries.be.dcl";
declare namespace ns8="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm = "1") then $trueval
       else $falseval
};



declare function getFieldsFromHeader($parm as element(ns8:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns8:msgHeader/ns8:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns8:msgHeader/ns8:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns8:msgHeader/ns8:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns8:msgHeader/ns8:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns8:msgHeader/ns8:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns8:msgHeader/ns8:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns8:transHeader/ns8:transId)}</NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns8:invoke)) as element()*
{

<NF_ACCOUN_ACCOUNTIBAN?>{data($parm/ns8:account/ns0:Account/ns0:accountNumber)}</NF_ACCOUN_ACCOUNTIBAN>
,
<NF_TRANOA_ACTIVE>0</NF_TRANOA_ACTIVE>
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns8:bcd/ns4:BusinessControlData/ns4:pageControl/ns3:PageControl/ns3:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns8:bcd/ns4:BusinessControlData/ns4:pageControl/ns3:PageControl/ns3:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{boolean2SourceValue (data($parm/ns8:bcd/ns4:BusinessControlData/ns4:pageControl/ns3:PageControl/ns3:reverseOrder),"1","0")}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns8:bcd/ns4:BusinessControlData/ns4:pageControl/ns3:PageControl/ns3:navigationKeyDefinition)}</NF_PAGEC_NAVIGATIONKEYDEFI>
,
<NF_PAGEC_NAVIGATIONKEYVALU>{data($parm/ns8:bcd/ns4:BusinessControlData/ns4:pageControl/ns3:PageControl/ns3:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns8:header)}
    {getFieldsFromInvoke($body/ns8:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>