<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-04-06</con:description>
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:transferorder.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)>5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
};


declare function chgDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
 
    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)
 
    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 
};

declare function sourceValue2Boolean ($parm as xs:string?,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32), $occ as xs:integer) as element()
{

<ns1:transferOrderTargetList>
  {
   <ns1:TransferOrderTarget>
      <ns1:amountTarget?>{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}</ns1:amountTarget>
      <ns1:codeProfile?>{data($parm/NF_TRANOT_CODEPROFILE[$occ])}</ns1:codeProfile>
      <ns1:transferOrderDescription?>{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}</ns1:transferOrderDescription>
      <ns1:beneficiary1?>{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}</ns1:beneficiary1>
      <ns1:beneficiary2?>{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}</ns1:beneficiary2>
      <ns1:currencyCode>
        <ns3:CurrencyCode>
          <ns3:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns3:currencyCode>
        </ns3:CurrencyCode>
      </ns1:currencyCode>
   </ns1:TransferOrderTarget>
  }
</ns1:transferOrderTargetList>
};
declare function getElementsForTransferOrder($parm as element(fml:FML32)) as element()
{

<ns6:transferOrder>
  {
    for $x at $occ in $parm/NF_TRANOA_TRANSFERORDERNUM
    return
    <ns1:TransferOrderAnchor>
      <ns1:frequency?>{data($parm/NF_TRANOA_FREQUENCY[$occ])}</ns1:frequency>
      (:<ns1:paymentDate?>{data($parm/NF_TRANOA_PAYMENTDATE[$occ])}</ns1:paymentDate>:)
      { insertDate(data($parm/NF_TRANOA_PAYMENTDATE[$occ]),"yyyy-MM-dd","ns1:paymentDate")}
      (:<ns1:expirationDate?>{data($parm/NF_TRANOA_EXPIRATIONDATE[$occ])}</ns1:expirationDate>:)
      { insertDate(data($parm/NF_TRANOA_EXPIRATIONDATE[$occ]),"yyyy-MM-dd","ns1:expirationDate")}
      <ns1:codeProfile?>{data($parm/NF_TRANOA_CODEPROFILE[$occ])}</ns1:codeProfile>
      <ns1:nonBusinessDayRule?>{sourceValue2Boolean(data($parm/NF_TRANOA_NONBUSINESSDAYRU[$occ]),"1")}</ns1:nonBusinessDayRule>
      <ns1:transferOrderNumber?>{data($parm/NF_TRANOA_TRANSFERORDERNUM[$occ])}</ns1:transferOrderNumber>
      (:<ns1:charging?>{data($parm/NF_TRANOA_CHARGING[$occ])}</ns1:charging>:)
      <ns1:charging?>{sourceValue2Boolean(data($parm/NF_TRANOA_CHARGING[$occ]),"1")}</ns1:charging>
      (:<ns1:firstPaymentDate?>{data($parm/NF_TRANOA_FIRSTPAYMENTDATE[$occ])}</ns1:firstPaymentDate>:)
      { insertDate(data($parm/NF_TRANOA_FIRSTPAYMENTDATE[$occ]),"yyyy-MM-dd","ns1:firstPaymentDate")}
      {getElementsForTransferOrderTargetList($parm, $occ)}
      <ns1:period>
        <ns3:Period>
          <ns3:period?>{data($parm/NF_PERIOD_PERIOD[$occ])}</ns3:period>
        </ns3:Period>
      </ns1:period>
    </ns1:TransferOrderAnchor>
  }
</ns6:transferOrder>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns6:invokeResponse>
  {getElementsForTransferOrder($parm)}
  <ns6:bcd>
    <ns4:BusinessControlData>
      <ns4:pageControl>
        <ns7:PageControl>
          (:<ns7:hasNext?>{data($parm/NF_PAGEC_HASNEXT)}</ns7:hasNext>:)
          <ns7:hasNext>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}</ns7:hasNext>
          <ns7:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns7:navigationKeyDefinition>
          <ns7:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns7:navigationKeyValue>
        </ns7:PageControl>
      </ns4:pageControl>
    </ns4:BusinessControlData>
  </ns6:bcd>
</ns6:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>