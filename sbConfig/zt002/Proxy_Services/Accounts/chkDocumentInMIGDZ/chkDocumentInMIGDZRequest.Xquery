<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.2
 : @since 2010-02-19
 :
 : wersja WSDLa: 25-01-2010 15:50:42
 :
 : $Proxy Services/Accounts/chkDocumentInMIGDZ/chkDocumentInMIGDZRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts_svn/chkDocumentInMIGDZ/chkDocumentInMIGDZ/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:operations.entities.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";

declare function xf:chkDocumentInMIGDZ($header1 as element(ns3:header),
    										 $invoke1 as element(ns3:invoke))
    as element(ns0:FML32) {
        <ns0:FML32>

			(:
			 : dane z naglowka
			 :)
            <ns0:DC_UZYTKOWNIK?>{
				data($header1/ns3:msgHeader/ns3:userId)
			}</ns0:DC_UZYTKOWNIK>

            <ns0:DC_KOD_JEDNOSTKI?>{
				data($header1/ns3:msgHeader/ns3:unitId)
			}</ns0:DC_KOD_JEDNOSTKI>

            <ns0:DC_KOD_APLIKACJI?>{
				data($header1/ns3:msgHeader/ns3:appId)
			}</ns0:DC_KOD_APLIKACJI>

            (:
             : dane z operacji wejściowej
             :)
            <ns0:CI_SERIA_NR_DOK?>{
				data($invoke1/ns3:disposer/ns2:Disposer/ns2:documentNumber)
			}</ns0:CI_SERIA_NR_DOK>

            <ns0:CI_DOK_TOZSAMOSCI?>{
				data($invoke1/ns3:disposer/ns2:Disposer/ns2:documentType/ns1:DocumentTypeForTxn/ns1:documentCodeForMIG)
			}</ns0:CI_DOK_TOZSAMOSCI>

        </ns0:FML32>
};

declare variable $header1 as element(ns3:header) external;
declare variable $invoke1 as element(ns3:invoke) external;

<soap-env:Body>{
	xf:chkDocumentInMIGDZ($header1, $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>