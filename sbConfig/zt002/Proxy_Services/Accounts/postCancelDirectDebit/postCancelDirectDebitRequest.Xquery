<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="postCancelDirectDebit.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="postCancelDirectDebit.wsdl" ::)
(:: pragma bea:global-element-return element="ns5:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:acceptance.entities.be.dcl";
declare namespace ns3 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns5 = "";
declare namespace ns6 = "urn:operations.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/postCancelDirectDebit/postCancelDirectDebit/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns8 = "urn:entities.be.dcl";

declare function local:dataToSV($data as xs:string) as xs:string
{
let $data:=$data
return
   if ($data) then
fn:concat(fn:substring($data, 9, 2),'-',fn:substring($data, 6, 2),'-',fn:substring($data, 1, 4))
else("")

};



declare function xf:postCancelDirectDebit($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke))
    as element(ns5:FML32) {
        <ns5:FML32>

            <ns5:TR_ID_OPER?>{ 
            data($header1/ns0:transHeader/ns0:transId) 
            }</ns5:TR_ID_OPER>
             <ns5:TR_DATA_OPER?>{local:dataToSV(xs:string( data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionDate)))}</ns5:TR_DATA_OPER>
             
 (:           <ns5:TR_DATA_OPER?> :)
  (:              { :)
   (:                 let $transactionDate  := ($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionDate) :)  
    (:                return :)
     (:                   (fn:concat( :)
      (:                  fn:substring(data($transactionDate), 9, 2), :)
       (:                 '-', :)
        (:                fn:substring(data($transactionDate ), 6, 2), :)
         (:               '-', :)
          (:              fn:substring(data($transactionDate ), 1, 4) :)
           (:             )):)
            (:    } :)
			(:	</ns5:TR_DATA_OPER> :)

            <ns5:TR_ODDZ_KASY?>{ 
            xs:short( data($invoke1/ns0:branchCode/ns2:BranchCode/ns2:branchCode) ) 
            }</ns5:TR_ODDZ_KASY>
            
            <ns5:TR_KASA?>{ 
            xs:short( data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:till/ns1:Till/ns1:tillID) ) 
            }</ns5:TR_KASA>
            
            <ns5:TR_KASJER?>{ 
            xs:short( data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:teller/ns1:Teller/ns1:tellerID) ) 
            }</ns5:TR_KASJER>
            
            <ns5:TR_UZYTKOWNIK?>{ 
            data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns8:User/ns8:userID) 
            }</ns5:TR_UZYTKOWNIK>
            
            <ns5:TR_ID_GR_OPER?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:cashTransactionBasketID) 
            }</ns5:TR_ID_GR_OPER>
            
            <ns5:TR_MSG_ID?>{ 
            data($header1/ns0:msgHeader/ns0:msgId) 
            }</ns5:TR_MSG_ID>
            
            
            <ns5:TR_FLAGA_AKCEPT?>
                {
                    if (fn:boolean(data($invoke1/ns0:acceptanceForBE/ns6:AcceptanceForBE/ns6:flag)) = fn:boolean("true")) then
                        (data($invoke1/ns0:acceptanceForBE/ns6:AcceptanceForBE/ns6:flag))
                    else 
                        'T'
                }
			</ns5:TR_FLAGA_AKCEPT>
            
            <ns5:TR_AKCEPTANT?>{
            data($invoke1/ns0:acceptTask/ns4:AcceptTask/ns4:acceptor) 
            }</ns5:TR_AKCEPTANT>
            
            <ns5:TR_TYP_KOM?>{ 
            xs:short( data($invoke1/ns0:transaction/ns6:Transaction/ns6:csrMessageType/ns3:CsrMessageType/ns3:csrMessageType) ) 
            }</ns5:TR_TYP_KOM>
            
            <ns5:TR_KANAL?>{ 
            xs:short( data($invoke1/ns0:transaction/ns6:Transaction/ns6:extendedCSRMessageType/ns3:ExtendedCSRMessageType/ns3:extendedCSRMessageType) ) 
            }</ns5:TR_KANAL>
            
              <ns5:TR_CZAS_OPER?>
                {
                    let $transactionTime  := ($header1/ns0:msgHeader/ns0:timestamp)
  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
				</ns5:TR_CZAS_OPER>
 

            <ns5:TR_RACH_NAD?>{ 
             data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:accountNumber)
             }</ns5:TR_RACH_NAD>
             
            <ns5:TR_KWOTA_NAD?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:amountWn) 
            }</ns5:TR_KWOTA_NAD>
             
            <ns5:TR_WALUTA_NAD?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:currencyCode/ns2:CurrencyCode/ns2:currencyCode)
            }</ns5:TR_WALUTA_NAD>
            
            <ns5:TR_KURS_NAD?>{
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:rate) 
            }</ns5:TR_KURS_NAD>
            
            
            <ns5:TR_NAZWA_NAD?>{
            fn-bea:trim-right( concat(data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:name)," ",data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:nameSecond)))
            }</ns5:TR_NAZWA_NAD>
            
            <ns5:TR_MIEJSCOWOSC_NAD?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:city) 
            }</ns5:TR_MIEJSCOWOSC_NAD>
            
			<ns5:TR_ULICA_NAD?>{ 
			data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:address)  
			}</ns5:TR_ULICA_NAD>
                        			
            <ns5:TR_KOD_POCZT_NAD?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:zipCode)  
            }</ns5:TR_KOD_POCZT_NAD>
            
            <ns5:TR_ROWN_PLN_NAD?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionWn/ns6:TransactionWn/ns6:amountWnEquiv) 
            }</ns5:TR_ROWN_PLN_NAD>
                        
			<ns5:TR_RACH_ADR?>{ 
			data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:accountNumber)  
			}</ns5:TR_RACH_ADR>
			
            <ns5:TR_KWOTA_ADR?>{ 
             data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:amountMa)
             }</ns5:TR_KWOTA_ADR>
            
            <ns5:TR_WALUTA_ADR?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:currencyCode/ns2:CurrencyCode/ns2:currencyCode) 
            }</ns5:TR_WALUTA_ADR>
			                        
            <ns5:TR_KURS_ADR?>{  
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:rate) 
            }</ns5:TR_KURS_ADR>
            
            <ns5:TR_NAZWA_ADR?>{ 
            fn-bea:trim-right( concat( data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:name)," ",data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:nameSecond) )) 
            }</ns5:TR_NAZWA_ADR>
            
            <ns5:TR_MIEJSCOWOSC_ADR?>{
             data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:city)  
            }</ns5:TR_MIEJSCOWOSC_ADR>            
            
            <ns5:TR_ULICA_ADR?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:address) 
            }</ns5:TR_ULICA_ADR>
            
            <ns5:TR_KOD_POCZT_ADR?>{
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:zipCode)  
            }</ns5:TR_KOD_POCZT_ADR>
            
            <ns5:TR_ROWN_PLN_ADR?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:transactionMa/ns6:TransactionMa/ns6:amountMaEquiv)  
            }</ns5:TR_ROWN_PLN_ADR>
            
             <ns5:TR_TYTUL?>{
             data($invoke1/ns0:transaction/ns6:Transaction/ns6:title)
             }</ns5:TR_TYTUL>
             
            <ns5:TR_AKCEPTANT_SKP?>{
             data($invoke1/ns0:acceptTask/ns4:AcceptTask/ns4:acceptor) 
             }</ns5:TR_AKCEPTANT_SKP>
             
            <ns5:TR_UZYTKOWNIK_SKP?>{ 
            data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns8:User/ns8:userID) 
            }</ns5:TR_UZYTKOWNIK_SKP>
            
            <ns5:TR_NR_REF_ODWOL_PZ?>{ 
            data($invoke1/ns0:transaction/ns6:Transaction/ns6:directDebitCallOff/ns6:DirectDebitCallOff/ns6:originDDTxnReferenceNr) 
            }</ns5:TR_NR_REF_ODWOL_PZ>
            
        	<ns5:TR_DATA_ODWOL_PZ?>{
        	 local:dataToSV(xs:string( data($invoke1/ns0:transaction/ns6:Transaction/ns6:directDebitCallOff/ns6:DirectDebitCallOff/ns6:originDDTxnDate) )) 
        	 }</ns5:TR_DATA_ODWOL_PZ>
        	 
        </ns5:FML32>
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;

<soap-env:Body>{
xf:postCancelDirectDebit($header1,
    $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>