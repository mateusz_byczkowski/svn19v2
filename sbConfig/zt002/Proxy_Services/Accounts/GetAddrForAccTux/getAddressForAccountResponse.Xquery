<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace tem = "http://tempuri.org/";
declare namespace wcf="http://schemas.datacontract.org/2004/07/WcfServiceLibrary.Struktures";


declare function xf:map_getAddressForAccountResponse($res as element(tem:getCardProductAdrResult ))
	as element(fml:FML32) {
	<fml:FML32>
	{
	  	if($res/wcf:Name1)
		then	<fml:NF_ACCOUA_NAME1>{ data($res/wcf:Name1) }</fml:NF_ACCOUA_NAME1>
		else()
	}	
	{
	  	if($res/wcf:Name2)
		then 	<fml:NF_ACCOUA_NAME2>{ data($res/wcf:Name2) }</fml:NF_ACCOUA_NAME2>
		else()
	}	
	{
	  	if($res/wcf:AddresStreet)
		then 	<fml:NF_ACCOUA_STREET>{ data($res/wcf:AddresStreet) }</fml:NF_ACCOUA_STREET>
		else()
	}	
	{
	  	if($res/wcf:AddresHouseflatnumber)
		then 	<fml:NF_ACCOUA_HOUSEFLATNUMBER>{ data($res/wcf:AddresHouseflatnumber) }</fml:NF_ACCOUA_HOUSEFLATNUMBER>
		else()
	}	
	{
	  	if($res/wcf:AddresCity)
		then 	<fml:NF_ACCOUA_CITY>{ data($res/wcf:AddresCity) }</fml:NF_ACCOUA_CITY>
		else()
	}	
	{
	  	if($res/wcf:CountcCountrycode)
		then 	<fml:NF_ACCOUA_STATECOUNTRY>{ data($res/wcf:CountcCountrycode) }</fml:NF_ACCOUA_STATECOUNTRY>
		else()
	}	
	{
	  	if($res/wcf:AddresZipcode)
		then 	<fml:NF_ACCOUA_ZIPCODE>{ data($res/wcf:AddresZipcode) }</fml:NF_ACCOUA_ZIPCODE>
		else()
	}	
	{
	  	if($res/wcf:AddresValidfrom)
		then 	<fml:NF_ACCOUA_VALIDFROM>{ data($res/wcf:AddresValidfrom) }</fml:NF_ACCOUA_VALIDFROM>
		else()
	}	
	{
	  	if($res/wcf:AddresValidto)
		then 	<fml:NF_ACCOUA_VALIDTO>{ data($res/wcf:AddresValidto) }</fml:NF_ACCOUA_VALIDTO>
		else()
	}	
	{
	  	if($res/wcf:Status)
		then 	<fml:NF_RESPOM_RESULT>{ data($res/wcf:Status) }</fml:NF_RESPOM_RESULT>
		else  <fml:NF_RESPOM_RESULT>0</fml:NF_RESPOM_RESULT>
	}	
	    
	</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_getAddressForAccountResponse($body/tem:getCardProductAdrResponse/tem:getCardProductAdrResult ) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>