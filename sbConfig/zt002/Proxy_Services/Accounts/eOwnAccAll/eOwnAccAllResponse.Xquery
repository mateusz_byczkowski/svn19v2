<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbCEKEeOwnAccAllResponse($fml as element(fml:FML32))
	as element(m:bCEKEeOwnAccAllResponse) {
		<m:bCEKEeOwnAccAllResponse>
			{
				if($fml/fml:E_CUST_SEQNO)
					then <m:CustSeqno>{ data($fml/fml:E_CUST_SEQNO) }</m:CustSeqno>
					else ()
			}
			{
				if($fml/fml:B_SYS_MASK)
					then <m:SysMask>{ data($fml/fml:B_SYS_MASK) }</m:SysMask>
					else ()
			}
			{

				let $B_SYS := $fml/fml:B_SYS
				let $B_MIKROODDZIAL := $fml/fml:B_MIKROODDZIAL
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				let $B_BANK := $fml/fml:B_BANK
				let $E_SEQ_NO := $fml/fml:E_SEQ_NO
				let $E_CHANNEL_MASK := $fml/fml:E_CHANNEL_MASK
				let $B_KOD_RACH := $fml/fml:B_KOD_RACH
				let $B_ID_ODDZ := $fml/fml:B_ID_ODDZ
				let $E_TIME_STAMP := $fml/fml:E_TIME_STAMP
				let $E_ACCOUNT_TYPE_OPTIONS := $fml/fml:E_ACCOUNT_TYPE_OPTIONS
				let $E_DR_TRN_MASK := $fml/fml:E_DR_TRN_MASK
				let $E_CR_TRN_MASK := $fml/fml:E_CR_TRN_MASK
				let $E_LOGIN_ID := $fml/fml:E_LOGIN_ID
				let $E_OPTIONS := $fml/fml:E_OPTIONS
				let $B_FLAGA_REZ := $fml/fml:B_FLAGA_REZ
				let $B_NAZWA_KLIENTA := $fml/fml:B_NAZWA_KLIENTA
				let $B_UL_ZAM := $fml/fml:B_UL_ZAM
				let $B_M_ZAM := $fml/fml:B_M_ZAM
				let $B_KREWNY_BANKU := $fml/fml:B_KREWNY_BANKU
				let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
				let $B_SALDO := $fml/fml:B_SALDO
				let $B_DATA_OST_OPER := $fml/fml:B_DATA_OST_OPER
				let $B_OPIS_RACH := $fml/fml:B_OPIS_RACH
				let $B_WALUTA := $fml/fml:B_WALUTA
				let $B_DOST_SRODKI := $fml/fml:B_DOST_SRODKI
				let $B_SKROT_OPISU := $fml/fml:B_SKROT_OPISU
				let $B_NAZWA_ODDZ := $fml/fml:B_NAZWA_ODDZ
				let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
				let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
				for $it at $p in $fml/fml:E_SEQ_NO
				return
					<m:bCEKEeOwnAccAllaccount>
					{
						if($B_SYS[$p])
							then <m:Sys>{ data($B_SYS[$p]) }</m:Sys>
						else ()
					}
					{
						if($B_MIKROODDZIAL[$p])
							then <m:Mikrooddzial>{ data($B_MIKROODDZIAL[$p]) }</m:Mikrooddzial>
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then <m:TypRach>{ data($B_TYP_RACH[$p]) }</m:TypRach>
						else ()
					}
					{
						if($B_BANK[$p])
							then <m:Bank>{ data($B_BANK[$p]) }</m:Bank>
						else ()
					}
					{
						if($E_SEQ_NO[$p])
							then <m:SeqNo>{ data($E_SEQ_NO[$p]) }</m:SeqNo>
						else ()
					}
					{
						if($E_CHANNEL_MASK[$p])
							then <m:ChannelMask>{ data($E_CHANNEL_MASK[$p]) }</m:ChannelMask>
						else ()
					}
					{
						if($B_KOD_RACH[$p])
							then <m:KodRach>{ data($B_KOD_RACH[$p]) }</m:KodRach>
						else ()
					}
					{
						if($B_ID_ODDZ[$p])
							then <m:IdOddz>{ data($B_ID_ODDZ[$p]) }</m:IdOddz>
						else ()
					}
					{
						if($E_TIME_STAMP[$p])
							then <m:TimeStamp>{ data($E_TIME_STAMP[$p]) }</m:TimeStamp>
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_OPTIONS[$p])
							then <m:AccountTypeOptions>{ data($E_ACCOUNT_TYPE_OPTIONS[$p]) }</m:AccountTypeOptions>
						else ()
					}
					{
						if($E_DR_TRN_MASK[$p])
							then <m:DrTrnMask>{ data($E_DR_TRN_MASK[$p]) }</m:DrTrnMask>
						else ()
					}
					{
						if($E_CR_TRN_MASK[$p])
							then <m:CrTrnMask>{ data($E_CR_TRN_MASK[$p]) }</m:CrTrnMask>
						else ()
					}
					{
						if($E_LOGIN_ID[$p])
							then <m:LoginId>{ data($E_LOGIN_ID[$p]) }</m:LoginId>
						else ()
					}
					{
						if($E_OPTIONS[$p])
							then <m:Options>{ data($E_OPTIONS[$p]) }</m:Options>
						else ()
					}
					{
						if($B_FLAGA_REZ[$p])
							then <m:FlagaRez>{ data($B_FLAGA_REZ[$p]) }</m:FlagaRez>
						else ()
					}
					{
						if($B_NAZWA_KLIENTA[$p])
							then <m:NazwaKlienta>{ data($B_NAZWA_KLIENTA[$p]) }</m:NazwaKlienta>
						else ()
					}
					{
						if($B_UL_ZAM[$p])
							then <m:UlZam>{ data($B_UL_ZAM[$p]) }</m:UlZam>
						else ()
					}
					{
						if($B_M_ZAM[$p])
							then <m:MZam>{ data($B_M_ZAM[$p]) }</m:MZam>
						else ()
					}
					{
						if($B_KREWNY_BANKU[$p])
							then <m:KrewnyBanku>{ data($B_KREWNY_BANKU[$p]) }</m:KrewnyBanku>
						else ()
					}
					{
						if($B_DL_NR_RACH[$p])
							then <m:DlNrRach>{ data($B_DL_NR_RACH[$p]) }</m:DlNrRach>
						else ()
					}
					{
						if($B_SALDO[$p])
							then <m:Saldo>{ data($B_SALDO[$p]) }</m:Saldo>
						else ()
					}
					{
						if($B_DATA_OST_OPER[$p])
							then <m:DataOstOper>{ data($B_DATA_OST_OPER[$p]) }</m:DataOstOper>
						else ()
					}
					{
						if($B_OPIS_RACH[$p])
							then <m:OpisRach>{ data($B_OPIS_RACH[$p]) }</m:OpisRach>
						else ()
					}
					{
						if($B_WALUTA[$p])
							then <m:Waluta>{ data($B_WALUTA[$p]) }</m:Waluta>
						else ()
					}
					{
						if($B_DOST_SRODKI[$p])
							then <m:DostSrodki>{ data($B_DOST_SRODKI[$p]) }</m:DostSrodki>
						else ()
					}
					{
						if($B_SKROT_OPISU[$p])
							then <m:SkrotOpisu>{ data($B_SKROT_OPISU[$p]) }</m:SkrotOpisu>
						else ()
					}
					{
						if($B_NAZWA_ODDZ[$p])
							then <m:NazwaOddz>{ data($B_NAZWA_ODDZ[$p]) }</m:NazwaOddz>
						else ()
					}
					{
						if($B_KOD_WALUTY[$p])
							then <m:KodWaluty>{ data($B_KOD_WALUTY[$p]) }</m:KodWaluty>
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_NAME[$p])
							then <m:AccountTypeName>{ data($E_ACCOUNT_TYPE_NAME[$p]) }</m:AccountTypeName>
						else ()
					}
					</m:bCEKEeOwnAccAllaccount>
			}

		</m:bCEKEeOwnAccAllResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapbCEKEeOwnAccAllResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>