<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbCEKEeOwnAccAllRequest($req as element(m:bCEKEeOwnAccAllRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:LoginId)
					then <fml:E_LOGIN_ID>{ data($req/m:LoginId) }</fml:E_LOGIN_ID>
					else ()
			}
			{
				if($req/m:SysMask)
					then <fml:B_SYS_MASK>{ data($req/m:SysMask) }</fml:B_SYS_MASK>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapbCEKEeOwnAccAllRequest($body/m:bCEKEeOwnAccAllRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>