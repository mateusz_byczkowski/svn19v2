<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns5:invokeResponse>
  <ns5:response>
    <ns0:ResponseMessage>
       {
        if  (data($parm/NF_RESPOM_ERRORCODE)) then
               <ns0:result?>false</ns0:result>
        else
               <ns0:result?>true</ns0:result>
        }
       (: <ns0:result?>{sourceValue2Boolean (data($parm/NF_RESPOM_RESULT),"1")}</ns0:result> :)
      <ns0:errorCode?>{data($parm/NF_RESPOM_ERRORCODE)}</ns0:errorCode>
      <ns0:errorDescription?>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}</ns0:errorDescription>
    </ns0:ResponseMessage>
  </ns5:response>
  <ns5:statementParameters>
    <ns1:StatementParameters>
      <ns1:frequency?>{data($parm/NF_STATEP_FREQUENCY)}</ns1:frequency>
      {insertDate(data($parm/NF_STATEP_NEXTPRINTOUTDATE),"yyyy-MM-dd","ns1:nextPrintoutDate")}
      <ns1:printFlag?>{sourceValue2Boolean (data($parm/NF_STATEP_PRINTFLAG),"1")}</ns1:printFlag>
      <ns1:aggregateTransaction?>{sourceValue2Boolean (data($parm/NF_STATEP_AGGREGATETRANSAC),"1")}</ns1:aggregateTransaction>
      <ns1:cycle>
        <ns2:Period>
          <ns2:period?>{data($parm/NF_PERIOD_PERIOD)}</ns2:period>
        </ns2:Period>
      </ns1:cycle>
      <ns1:specialDay>
        <ns2:SpecialDay>
          <ns2:specialDay?>{data($parm/NF_SPECID_SPECIALDAY)}</ns2:specialDay>
        </ns2:SpecialDay>
      </ns1:specialDay>
      <ns1:provideManner>
        <ns2:ProvideManner>
          <ns2:provideManner?>{data($parm/NF_PROVIM_PROVIDEMANNER)}</ns2:provideManner>
        </ns2:ProvideManner>
      </ns1:provideManner>
    </ns1:StatementParameters>
  </ns5:statementParameters>
</ns5:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>