<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};


declare function getElementsForSchedule($parm as element(fml:FML32)) as element()
{

<ns0:schedule>
  {
    for $x at $occ in $parm/NF_TRANOS_PAYMENTDATE
    return
    <ns2:TransferOrderSchedule>
       {insertDate(data($parm/NF_TRANOS_PAYMENTDATE[$occ]),"yyyy-MM-dd","ns2:paymentDate")}
      <ns2:paymentAmount?>{data($parm/NF_TRANOS_PAYMENTAMOUNT[$occ])}</ns2:paymentAmount>
      <ns2:paymentDescription?>{data($parm/NF_TRANOS_PAYMENTDESCRIPTI[$occ])}</ns2:paymentDescription>
      <ns2:relativeRecordNumer?>{data($parm/NF_TRANOS_RELATIVERECORDNU[$occ])}</ns2:relativeRecordNumer>
    </ns2:TransferOrderSchedule>
  }
</ns0:schedule>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForSchedule($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>