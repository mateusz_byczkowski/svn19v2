<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:directdebit.entities.be.dcl";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string
{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value) then 
        if(string-length($value)>5 and not (string(fn-bea:date-from-string-with-format($dateFormat,$value)) = "0001-01-01"))
             then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
};


declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32)) as element()
{

<ns2:transferOrderTargetList>
  {
    for $x at $occ in $parm/NF_TRANOT_AMOUNTTARGET
    return
    <ns2:TransferOrderTarget>
      <ns2:amountTarget?>{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}</ns2:amountTarget>
      <ns2:codeProfile?>{data($parm/NF_TRANOT_CODEPROFILE[$occ])}</ns2:codeProfile>
      <ns2:transferOrderDescription?>{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}</ns2:transferOrderDescription>
      <ns2:beneficiary1?>{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}</ns2:beneficiary1>
      <ns2:beneficiary2?>{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}</ns2:beneficiary2>
      <ns2:beneficiary3?>{data($parm/NF_TRANOT_BENEFICIARY3[$occ])}</ns2:beneficiary3>
      <ns2:beneficiary4?>{data($parm/NF_TRANOT_BENEFICIARY4[$occ])}</ns2:beneficiary4>
      <ns2:account>
        <ns0:Account>
          <ns0:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}</ns0:accountNumber>
        </ns0:Account>
      </ns2:account>
      <ns2:accountOutside>
        <ns2:AccountOutside>
          <ns2:accountNumber?>{data($parm/NF_ACCOUO_ACCOUNTNUMBER[$occ])}</ns2:accountNumber>
        </ns2:AccountOutside>
      </ns2:accountOutside>
      <ns2:glaccount>
        <ns0:GLAccount>
          <ns0:costCenterGL?>{data($parm/NF_GLA_COSTCENTERGL[$occ])}</ns0:costCenterGL>
          <ns0:accountGL?>{data($parm/NF_GLA_ACCOUNTGL[$occ])}</ns0:accountGL>
          <ns0:currencyCode>
            <ns4:CurrencyCode>
              {
              if (string-length(data($parm/NF_GLA_ACCOUNTGL[$occ])) > 0) then
                   <ns4:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns4:currencyCode>
              else
                    <ns4:currencyCode?></ns4:currencyCode>
             } 
            </ns4:CurrencyCode>
          </ns0:currencyCode>
        </ns0:GLAccount>
      </ns2:glaccount>
    </ns2:TransferOrderTarget>
  }
</ns2:transferOrderTargetList>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns6:invokeResponse>
  <ns6:directDebitOut>
    <ns1:DirectDebit>
      <ns1:originatorReference?>{data($parm/NF_DIRECD_ORIGINATORREFERE)}</ns1:originatorReference>
      <ns1:additionalDescription?>{data($parm/NF_DIRECD_ADDITIONALDESCRI)}</ns1:additionalDescription>
    </ns1:DirectDebit>
  </ns6:directDebitOut>
  <ns6:orderOut>
    <ns2:TransferOrderAnchor>
      <ns2:frequency?>{data($parm/NF_TRANOA_FREQUENCY)}</ns2:frequency>
      {insertDate ($parm/NF_TRANOA_PAYMENTDATE,"yyyy-MM-dd","ns2:paymentDate")}
      {insertDate ($parm/NF_TRANOA_EXPIRATIONDATE,"yyyy-MM-dd","ns2:expirationDate")}
      <ns2:codeProfile?>{data($parm/NF_TRANOA_CODEPROFILE)}</ns2:codeProfile>
      <ns2:feeCode?>{data($parm/NF_TRANOA_FEECODE)}</ns2:feeCode>
      <ns2:nonBusinessDayRule?>{sourceValue2Boolean(data($parm/NF_TRANOA_NONBUSINESSDAYRU),"1")}</ns2:nonBusinessDayRule>
      <ns2:charging?>{sourceValue2Boolean(data($parm/NF_TRANOA_CHARGING),"1")}</ns2:charging>
      <ns2:userID?>{data($parm/NF_TRANOA_USERID)}</ns2:userID>
      {insertDate ($parm/NF_TRANOA_FIRSTPAYMENTDATE,"yyyy-MM-dd","ns2:firstPaymentDate")}
      <ns2:feeAmount?>{data($parm/NF_TRANOA_FEEAMOUNT)}</ns2:feeAmount>
      {getElementsForTransferOrderTargetList($parm)}
      <ns2:period>
        <ns4:Period>
          <ns4:period?>{data($parm/NF_PERIOD_PERIOD)}</ns4:period>
        </ns4:Period>
      </ns2:period>
      <ns2:specificDay>
        <ns4:SpecialDay>
          <ns4:specialDay?>{data($parm/NF_SPECID_SPECIALDAY)}</ns4:specialDay>
        </ns4:SpecialDay>
      </ns2:specificDay>
      <ns2:currencyCode>
        <ns4:CurrencyCode>
          <ns4:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE)}</ns4:currencyCode>
        </ns4:CurrencyCode>
      </ns2:currencyCode>
    </ns2:TransferOrderAnchor>
  </ns6:orderOut>
</ns6:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>