<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-06-22</con:description>
    <con:xquery><![CDATA[declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:accountdict.dictionaries.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{
<DC_ODDZIAL?>{chkUnitId(data($parm/ns6:msgHeader/ns6:unitId))}</DC_ODDZIAL>,
<DC_UZYTKOWNIK?>{concat("SKP:",data($parm/ns6:msgHeader/ns6:userId))}</DC_UZYTKOWNIK>,
<DC_SKP_PRACOWNIKA?>{data($parm/ns6:msgHeader/ns6:userId)}</DC_SKP_PRACOWNIKA>,
<DC_TRN_ID?>{data($parm/ns6:transHeader/ns6:transId)}</DC_TRN_ID>
};

declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{
let $paczka := $parm/ns6:feeAgreement/ns2:TranFeeAgreement/ns2:tranFeeList/ns2:TranFee
return (

<DC_NUMER_WNIOSKU?>{data($parm/ns6:feeAgreement/ns2:TranFeeAgreement/ns2:agreementNumber)}</DC_NUMER_WNIOSKU>,
<DC_START_WNIOSKU?>{data($parm/ns6:feeAgreement/ns2:TranFeeAgreement/ns2:agreementStartDate)}</DC_START_WNIOSKU>,
<DC_KONIEC_WNIOSKU?>{data($parm/ns6:feeAgreement/ns2:TranFeeAgreement/ns2:agreementEndDate)}</DC_KONIEC_WNIOSKU>,
<DC_NUMER_KLIENTA?>{data($parm/ns6:feeAgreement/ns2:TranFeeAgreement/ns2:customer/ns3:Customer/ns3:customerNumber)}</DC_NUMER_KLIENTA>,

<DC_ID_PAKIETU>{count($paczka)}</DC_ID_PAKIETU>,

for $x in $paczka/ns2:tranFeeAgreement/ns2:TranFeeAgreement/ns2:tranAccount/ns2:TranAccount/ns2:tranFeeAgreementList/ns2:TranFeeAgreement/ns2:tranAccount/ns2:TranAccount/ns2:account/ns2:Account/ns2:accountIBAN
    return    
            <DC_RACHUNEK_IBAN?>{data($x)}</DC_RACHUNEK_IBAN>,
            

for $x in $paczka/ns2:tranFeeAgreement/ns2:TranFeeAgreement/ns2:tranAccount/ns2:TranAccount/ns2:feeList/ns2:Fee
    return    
            <DC_PAKIET_OPLAT?>{concat($x/ns2:feeID,";",$x/ns2:feeType/ns1:FeeType/ns1:feeType,";",$x/ns2:feeCode,";",$x/ns2:feeAmount,";",$x/ns2:feeDiscountPercent,";",$x/ns2:feeFlat,";",$x/ns2:feeMin,";",$x/ns2:feeMax,";",$x/ns2:feePercent)}</DC_PAKIET_OPLAT>,
for $x in $paczka
    return (   
           <DC_LICZ_RACH_PACZ>{count($x/ns2:tranFeeAgreement/ns2:TranFeeAgreement/ns2:tranAccount/ns2:TranAccount/ns2:tranFeeAgreementList/ns2:TranFeeAgreement/ns2:tranAccount/ns2:TranAccount/ns2:account/ns2:Account/ns2:accountIBAN)}</DC_LICZ_RACH_PACZ>,
           <DC_LICZ_OPL_PACZ>{count($x/ns2:tranFeeAgreement/ns2:TranFeeAgreement/ns2:tranAccount/ns2:TranAccount/ns2:feeList/ns2:Fee/ns2:feeAmount)}</DC_LICZ_OPL_PACZ>
)

)
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvoke($body/ns6:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>