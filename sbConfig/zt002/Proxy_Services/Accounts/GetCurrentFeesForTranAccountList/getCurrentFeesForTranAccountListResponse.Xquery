<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForFeeList($parm as element(fml:FML32)) as element()
{

<ns0:feeList>
  {
    for $x at $occ in $parm/NF_FEE_FEEAMOUNT
    return
    <ns2:Fee>
      <ns2:feeAmount>{data($parm/NF_FEE_FEEAMOUNT[$occ])}</ns2:feeAmount>
      <ns2:feeDescription>{data($parm/NF_FEE_FEEDESCRIPTION[$occ])}</ns2:feeDescription>
    </ns2:Fee>
  }
</ns0:feeList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForFeeList($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>