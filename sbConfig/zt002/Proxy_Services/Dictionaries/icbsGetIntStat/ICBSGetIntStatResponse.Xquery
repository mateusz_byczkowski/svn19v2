<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
(:  <ns0:error>
    <ns2:ExceptionData>
      <ns2:errorDescription?>{data($parm/NF_EXCEPD_ERRORDESCRIPTION)}</ns2:errorDescription>
    </ns2:ExceptionData>
  </ns0:error> :)
  <ns0:legalInterest>
    <ns3:LegalInterest>
      <ns3:obligatoryInterestDate?>{data($parm/CI_DATA_OD)}</ns3:obligatoryInterestDate>
      <ns3:obligInterestPercentage?>{data($parm/B_ZAL_ODS)}</ns3:obligInterestPercentage>
      <ns3:obligInterestForDelayDate?>{data($parm/CI_DATA)}</ns3:obligInterestForDelayDate>
      <ns3:obligInterestForDelayPer?>{data($parm/B_ZAL_KAR)}</ns3:obligInterestForDelayPer>
    </ns3:LegalInterest>
  </ns0:legalInterest>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>