<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace mes="http://bzwbk.com/services/messages/";

declare variable $body as element(soapenv:Body) external;


declare function getCurrencyDateTime($date as xs:string, $time as xs:string) as xs:dateTime{
   let $time_with_zeros:=concat(substring("000000",string-length($time) + 1),$time)
   return
      fn-bea:dateTime-from-string-with-format("yyyy-MM-ddHHmmss",concat($date,$time_with_zeros))
};


let $fml:=$body/FML32

return
<soapenv:Body>
   <mes:GetRateTableForAdvisoryResponse>
       {
          for $x at $idx in $fml/NF_CURREC_CURRENCYCODE
               return    
                 <mes:RateTable>
                    <mes:currencyCode?>{data($fml/NF_CURREC_CURRENCYCODE[$idx])}</mes:currencyCode>
                    <mes:currencyDate?>{getCurrencyDateTime($fml/NF_RATET_CURRENCYDT[$idx],$fml/NF_RATET_CURRENCYTM[$idx])}</mes:currencyDate>
                    <mes:averageRateNBP?>{data($fml/NF_RATET_AVERAGERATENBP[$idx])}</mes:averageRateNBP>
                    <mes:averageRate?>{data($fml/NF_RATET_AVERAGERATE[$idx])}</mes:averageRate>
                    <mes:transferBuyRate?>{data($fml/NF_RATET_TRANSFERBUYRATE[$idx])}</mes:transferBuyRate>
                    <mes:transferSellRate?>{data($fml/NF_RATET_TRANSFERSELLRATE[$idx])}</mes:transferSellRate>
                    <mes:transferBuyRateForeign?>{data($fml/NF_RATET_TRANSFERBUYRATENO[$idx])}</mes:transferBuyRateForeign>
                    <mes:transferSellRateForeign?>{data($fml/NF_RATET_TRANSFERSELLRATEN[$idx])}</mes:transferSellRateForeign>
                    <mes:transferBuyRatePrefer?>{data($fml/NF_RATET_TRANSFERBUYRATEPR[$idx])}</mes:transferBuyRatePrefer>
                    <mes:transferSellRatePrefer?>{data($fml/NF_RATET_TRANSFERSELLRATEP[$idx])}</mes:transferSellRatePrefer>
                </mes:RateTable>
      }
   </mes:GetRateTableForAdvisoryResponse>
</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>