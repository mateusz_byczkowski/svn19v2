<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace mes="http://bzwbk.com/services/messages/";

   <soapenv:Body>
      <mes:GetRateTableForAdvisoryResponse>
         <!--Zero or more repetitions:-->
         <mes:RateTable>
            <!--Optional:-->
            <mes:currencyCode>0</mes:currencyCode>
            <!--Optional:-->
            <mes:currencyDate>{current-dateTime()}</mes:currencyDate>
            <!--Optional:-->
            <mes:averageRateNBP>1</mes:averageRateNBP>
            <!--Optional:-->
            <mes:averageRate>1</mes:averageRate>
            <!--Optional:-->
            <mes:transferBuyRate>1</mes:transferBuyRate>
            <!--Optional:-->
            <mes:transferSellRate>1</mes:transferSellRate>
            <!--Optional:-->
            <mes:transferBuyRateForeign>1</mes:transferBuyRateForeign>
            <!--Optional:-->
            <mes:transferSellRateForeign>1</mes:transferSellRateForeign>
            <!--Optional:-->
            <mes:transferBuyRatePrefer>1</mes:transferBuyRatePrefer>
            <!--Optional:-->
            <mes:transferSellRatePrefer>1</mes:transferSellRatePrefer>
         </mes:RateTable>
      </mes:GetRateTableForAdvisoryResponse>
   </soapenv:Body>]]></con:xquery>
</con:xqueryEntry>