<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
<ns0:dicts>
  {
    let $miasto := $parm/B_MIASTO
    let $nazwa := $parm/B_NAZWA
    for $nrRach at $occ in $parm/B_NR_RACH
    return
    <ns3:ZUSAccount>
      <ns3:accountNumber?>{data($nrRach)}</ns3:accountNumber>
      <ns3:description?>{data($nazwa[$occ])}</ns3:description>
      <ns3:name?>{data($nazwa[$occ])}</ns3:name>
      <ns3:street></ns3:street>
      <ns3:zipCode></ns3:zipCode>
      <ns3:city?>{data($miasto[$occ])}</ns3:city>
    </ns3:ZUSAccount>
  }
</ns0:dicts>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
<ns0:invokeResponse>
  {getElementsForDicts($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>