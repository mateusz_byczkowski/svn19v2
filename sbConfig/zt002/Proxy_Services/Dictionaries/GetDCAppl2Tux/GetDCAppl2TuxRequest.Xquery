<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace dc= "http://bzwbk.com/crw/services/dccustomer";

declare function xf:map_GetDCAppl2TuxRequest($fml as element(fml:FML32))
	as element(dc:searchCustomerApplications ) {
		<dc:searchCustomerApplications >
			<pesel>{ data($fml/fml:DC_NR_PESEL) }</pesel> 
			<idSerNum>{ data($fml/fml:DC_NR_DOWODU_REGON) }</idSerNum>
			<passport>{ data($fml/fml:DC_NUMER_PASZPORTU) }</passport>
			<cif>{ data($fml/fml:DC_NUMER_KLIENTA) }</cif>
			<branchNo>{ data($fml/fml:DC_NUMER_ODDZIALU) }</branchNo>
			<since>{ data($fml/fml:DC_DATA_POCZ) }</since>
			<until>{ data($fml/fml:DC_DATA_KONCA) }</until>
			<authData>
				<applicationId>{ data($fml/fml:U_APPLICATION_ID) }</applicationId>
				<applicationPassword>{ data($fml/fml:U_APPLICATION_PWD) }</applicationPassword>
			</authData>
	</dc:searchCustomerApplications >
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_GetDCAppl2TuxRequest($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>