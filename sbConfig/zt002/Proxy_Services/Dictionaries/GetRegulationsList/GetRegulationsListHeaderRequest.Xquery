<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:printouts.entities.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare namespace ns5="urn:bzwbk.com";
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns3:header)) as element()*
{
<msgHeader?>
<msgId?>{data($parm/ns3:msgHeader/ns3:msgId)}</msgId>
<companyId?>{data($parm/ns3:msgHeader/ns3:companyId)}</companyId>
<unitId?>{data($parm/ns3:msgHeader/ns3:unitId)}</unitId>
<userId?>{data($parm/ns3:msgHeader/ns3:userId)}</userId>
<appId?>{data($parm/ns3:msgHeader/ns3:appId)}</appId>
<timestamp?>{data($parm/ns3:msgHeader/ns3:timestamp)}</timestamp>
</msgHeader>
,
<transHeader?>
<transId?>{data($parm/ns3:transHeader/ns3:transId)}</transId>
</transHeader>
};


<soap:Header>
<header>
    {getFieldsFromHeader($header/ns3:header)}
  </header>
</soap:Header>]]></con:xquery>
</con:xqueryEntry>