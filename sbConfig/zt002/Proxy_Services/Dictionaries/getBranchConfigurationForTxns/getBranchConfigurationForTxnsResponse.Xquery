<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2010-02-19
 :
 : wersja WSDLa: 21-01-2010 16:00:20
 :
 : $Proxy Services/Dictionaries/getBranchConfigurationForTxns/getBranchConfigurationForTxnsResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Dictionaries/getBranchConfigurationForTxns/getBranchConfigurationForTxnsResponse/";
declare namespace ns0 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa - słownik konfiguracji oddziału
 :)
declare function xf:getBranchConfigurationForTxnsResponse($fML321 as element(ns1:FML32))
    as element(ns2:invokeResponse)
{
    <ns2:invokeResponse>
        <ns2:dicts>{
        
        	for $i in 1 to count($fML321/ns1:NF_BRACFT_BRANCHCODE)
        	return
	            <ns0:BranchConfigurationForTxn>

	                <ns0:branchCode>{
						data($fML321/ns1:NF_BRACFT_BRANCHCODE[$i])
					}</ns0:branchCode>

					{
						let $dateFrom := $fML321/ns1:NF_BRACFT_DATEFROM[$i]
						return
							if (data($dateFrom)) then
			                	<ns0:dateFrom>{
			                		data($dateFrom)
			                	}</ns0:dateFrom>
			                else ()
					}

					{
						let $dateTo := $fML321/ns1:NF_BRACFT_DATETO[$i]
						return
							if (data($dateTo)) then
				                <ns0:dateTo>{
									data($dateTo)
								}</ns0:dateTo>
							else ()
					}

                	<ns0:lastChangeUserSkp>{
						data($fML321/ns1:NF_BRACFT_LASTCHANGEUSERSK[$i])
					}</ns0:lastChangeUserSkp>

					{
						let $lastChangeDate := $fML321/ns1:NF_BRACFT_LASTCHANGEDATE[$i]
						return
							if (data($lastChangeDate)) then
				                <ns0:lastChangeDate>{
									data($lastChangeDate)
								}</ns0:lastChangeDate>
							else ()
					}

                	<ns0:treasuryFlag>{
						xs:boolean(data($fML321/ns1:NF_BRACFT_TREASURYFLAG[$i]))
					}</ns0:treasuryFlag>

                	<ns0:cifForBranch>{
						data($fML321/ns1:NF_BRACFT_CIFFORBRANCH[$i])
					}</ns0:cifForBranch>

	                <ns0:branchCostCenter>{
						data($fML321/ns1:NF_BRACFT_BRANCHCOSTCENTER[$i])
					}</ns0:branchCostCenter>

                	<ns0:parentBranchCostCenter>{
						data($fML321/ns1:NF_BRACFT_PARENTBRANCHCOST[$i])
					}</ns0:parentBranchCostCenter>

            </ns0:BranchConfigurationForTxn>
            
        }</ns2:dicts>
    </ns2:invokeResponse>
};

<soap-env:Body>{
	xf:getBranchConfigurationForTxnsResponse($fML321)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>