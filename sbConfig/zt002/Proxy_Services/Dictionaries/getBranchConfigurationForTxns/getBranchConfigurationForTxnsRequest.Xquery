<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2010-02-19
 :
 : wersja WSDLa: 21-01-2010 16:00:20
 :
 : $Proxy Services/Dictionaries/getBranchConfigurationForTxns/getBranchConfigurationForTxnsRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Dictionaries/getBranchConfigurationForTxns/getBranchConfigurationForTxnsRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:be.services.dcl";

declare variable $invoke1 as element(ns1:invoke) external;

(:
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 pusty bufor XML/FML
 :)
declare function xf:getBranchConfigurationForTxnsRequest($invoke1 as element(ns1:invoke))
    as element(ns0:FML32)
{
	&lt;ns0:FML32/>
};

&lt;soap-env:Body>{
	xf:getBranchConfigurationForTxnsRequest($invoke1)
}&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>