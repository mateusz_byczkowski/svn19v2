<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:dictionaries.be.dcl";
declare namespace urn2 = "urn:baseauxentities.be.dcl";

declare function xf:mapgetBranchResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {

		<urn:invokeResponse>
			<urn1:branchCodeOut>			
						{						  						
						  if (fn:count($fml/fml:B_MIKROODDZIAL) > 1) then
						  (
						
							for $it at $p in $fml/fml:B_MIKROODDZIAL
							return
							<urn1:BranchCode>
									<urn1:addressCity?>{ data($fml/fml:B_MIASTO[$p]) }</urn1:addressCity>
									<urn1:addressStreet?>{ data($fml/fml:B_ADRES_ODDZ[$p]) }</urn1:addressStreet>
									<urn1:addressZipCode?>{ data($fml/fml:B_KOD_POCZT[$p]) }</urn1:addressZipCode>
									<urn1:branchCode?>{ data($fml/fml:B_ODDZIAL[$p]) }</urn1:branchCode>
									<urn1:description?>{ data($fml/fml:B_NAZWA_ODDZ[$p]) }</urn1:description>
									<urn1:phoneNumber?>{ data($fml/fml:B_TELEFON[$p]) }</urn1:phoneNumber>
									<urn1:branchCostCenterList>
 									   <urn1:BranchCostCenter>
 									     <urn1:branchCostCenter>{ data($fml/fml:B_CENTR_KOSZT[$p]) }</urn1:branchCostCenter>
 									   </urn1:BranchCostCenter>
									</urn1:branchCostCenterList>
									<urn1:branchType?>{ data($fml/fml:B_MIKROODDZIAL[$p]) }</urn1:branchType>
									
 							</urn1:BranchCode>
 							)
 							else
 							(
    							<urn1:BranchCode>
									<urn1:addressCity?>{ data($fml/fml:B_MIASTO) }</urn1:addressCity>
									<urn1:addressStreet?>{ data($fml/fml:B_ADRES_ODDZ) }</urn1:addressStreet>
									<urn1:addressZipCode?>{ data($fml/fml:B_KOD_POCZT) }</urn1:addressZipCode>
									<urn1:branchCode?>{ data($fml/fml:B_ODDZIAL) }</urn1:branchCode>
									<urn1:description?>{ data($fml/fml:B_NAZWA_ODDZ) }</urn1:description>
									<urn1:phoneNumber?>{ data($fml/fml:B_TELEFON) }</urn1:phoneNumber>
									<urn1:branchCostCenterList>
                  {
 							     for $it at $p in $fml/fml:B_CENTR_KOSZT
							     return
 									   <urn1:BranchCostCenter>
 									     <urn1:branchCostCenter>{ data($fml/fml:B_CENTR_KOSZT[$p]) }</urn1:branchCostCenter>
 									   </urn1:BranchCostCenter>
 									}
									</urn1:branchCostCenterList>
									<urn1:branchType?>{ data($fml/fml:B_MIKROODDZIAL) }</urn1:branchType>
									
 							</urn1:BranchCode>
 							)
						}			
			</urn1:branchCodeOut>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;

<soap-env:Body>
{ xf:mapgetBranchResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>