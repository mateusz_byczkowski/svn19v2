<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:dictionaries.be.dcl";
declare namespace urn2 = "urn:baseauxentities.be.dcl";


declare function xf:mapgetBranchRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		<fml:FML32>
			{
				<fml:B_OPCJA?>{ data($req/urn:option/urn2:IntegerHolder/urn2:value) }</fml:B_OPCJA>
			}
			{
				<fml:B_ODDZIAL?>{ data($req/urn:branchCode/urn1:BranchCode/urn1:branchCode) }</fml:B_ODDZIAL>
			}
			{
				<fml:B_MIKROODDZIAL?>{ data($req/urn:branchCode/urn1:BranchCode/urn1:branchType/urn1:BranchType/urn1:branchType) }</fml:B_MIKROODDZIAL>
			}
		</fml:FML32>
};


declare variable $body as element(soap-env:Body) external;

<soap-env:Body>
 { xf:mapgetBranchRequest($body/urn:invoke) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>