<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="urn:basedictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
<ns0:dicts>
  {
    let $opis := $parm/CI_OPIS_WARTOSCI_SLOWNIKA
    for $wartosc at $occ in $parm/CI_WARTOSC_SLOWNIKA
    return
    <ns1:Language>
      {if (fn:string-length($wartosc)>0)
        then <ns1:languageID>{data($wartosc)}</ns1:languageID>
        else <ns1:languageID>{" "}</ns1:languageID>
      }
      <ns1:description?>{data($opis[$occ])}</ns1:description>
    </ns1:Language>
  }
</ns0:dicts>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
<ns0:invokeResponse>
  {getElementsForDicts($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>