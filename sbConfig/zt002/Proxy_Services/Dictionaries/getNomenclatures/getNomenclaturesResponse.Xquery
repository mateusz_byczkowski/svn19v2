<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:operationsdictionary.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
<ns0:dicts>
  {
    let $nazwa := $parm/B_NAZWA
    for $kodps at $occ in $parm/B_KOD_PS
    return
    <ns3:Nomenclature>
      <ns3:nomenclature?>{data($kodps)}</ns3:nomenclature>
      <ns3:description?>{data($nazwa[$occ])}</ns3:description>
    </ns3:Nomenclature>
  }
</ns0:dicts>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
<ns0:invokeResponse>
  {getElementsForDicts($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>