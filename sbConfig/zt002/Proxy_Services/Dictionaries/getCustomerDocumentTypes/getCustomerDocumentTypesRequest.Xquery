<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-12-13</con:description>
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns1="urn:be.services.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

<soap:Body>
  <fml:FML32>
      <CI_MSHEAD_MSGID?>{data($header/ns1:header/ns1:msgHeader/ns1:msgId)}</CI_MSHEAD_MSGID>
      <CI_ID_SLOWNIKA>24</CI_ID_SLOWNIKA>
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>