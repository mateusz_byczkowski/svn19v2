<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$3.2011-06-21</con:description>
    <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$fML321" element="ns1:FML32" location="../savetransfer/savetransferIN.xsd" ::)
(:: pragma bea:global-element-return element="ns0:calcFeeAmount" location="tbmswebservice.wsdl" ::)

(:~
 :
 : @author  Arkadiusz Kasprzak
 : @version 0.1
 : @since   2011-06-06
 :
 : wersja WSDLa: 
 :
 : $Proxy Services/Operation/TBMSInTux/TBMSInTux.xq$
 :
 :)

declare namespace ns1 = "";
declare namespace ns0 = "urn:http://www.suntecgroup.com/suntec/tbms/services/mediation";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Operations/TBMSInTux/TBMSInTux/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:TBMSInTux($fML321 as element(ns1:FML32))
    as element(ns0:calcFeeAmount) {
        <ns0:calcFeeAmount>
            <transactionID>{ data($fML321/ns1:TR_NR_REF) }</transactionID>
            {
            	let $date := data($fML321/ns1:TR_DATA_OPER)
            	let $dateTimeFormat := fn:concat(fn:substring($date, 1, 4),
			                          '-',
			                          fn:substring($date, 5, 2),
			                          '-',
			                          fn:substring($date, 7, 2),"T08:00:00")   
			    let $currentDateTime := fn:substring(fn:string(fn:current-dateTime()),1,19)
			                         
                return 
					if ($dateTimeFormat) then 
					(
	                     <putDownDate>{ data($currentDateTime) }	</putDownDate>,
	                     <transactionDate>{ data($dateTimeFormat) }</transactionDate>
					)
					else
					()
            }
            <accountNumber>{ xs:string( data($fML321/ns1:TR_RACH_1) ) }</accountNumber>
            <transactionCode>{ xs:string( data($fML321/ns1:TR_KOD_TRAN_NAD) ) }</transactionCode>
            <transactionAmount>{ data($fML321/ns1:TR_KWOTA_ADR) }</transactionAmount>
            <transactionVolume>{ xs:string( data($fML321/ns1:TR_KWOTA_NAD) ) }</transactionVolume>
            <channelCode>{ data($fML321/ns1:TR_TYTUL) }</channelCode>
            <transactionCurrency>{ data($fML321/ns1:TR_KOD_WALUTY) }</transactionCurrency>
        </ns0:calcFeeAmount>
};

declare variable $fML321 as element(ns1:FML32) external;


<soap-env:Body>
{
xf:TBMSInTux($fML321)
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>