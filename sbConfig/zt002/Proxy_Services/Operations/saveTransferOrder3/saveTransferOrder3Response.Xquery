<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns7:invokeResponse>
  <ns7:response>
    <ns0:ResponseMessage>
       {
          if (string-length(data($parm/DC_OPIS_BLEDU))>0) then
                    <ns0:result?>false</ns0:result>
          else
                    <ns0:result?>true</ns0:result>
       }
      <ns0:errorDescription?>{data($parm/DC_OPIS_BLEDU)}</ns0:errorDescription>
    </ns0:ResponseMessage>
  </ns7:response>
  <ns7:transferOrderOut>
    <ns2:TransferOrderAnchor>
      <ns2:transferOrderNumber?>{data($parm/NF_TRANOA_TRANSFERORDERNUM)}</ns2:transferOrderNumber>
    </ns2:TransferOrderAnchor>
  </ns7:transferOrderOut>
</ns7:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>