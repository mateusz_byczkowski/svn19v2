<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:transferorder.entities.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};

declare function getFieldsFromHeader($parm as element(ns3:header)) as element()*
{

<DC_TRN_ID?>{data($parm/ns3:transHeader/ns3:transId)}</DC_TRN_ID>
,
<DC_UZYTKOWNIK?>{concat("SKP:", data($parm/ns3:msgHeader/ns3:userId))}</DC_UZYTKOWNIK>
,
<DC_ODDZIAL?>{chkUnitId(data($parm/ns3:msgHeader/ns3:unitId))}</DC_ODDZIAL>

};
declare function getFieldsFromInvoke($parm as element(ns3:invoke)) as element()*
{

<DC_NR_RACHUNKU?>{data($parm/ns3:transferOrder/ns0:TransferOrderAnchor/ns0:transferOrderNumber)}</DC_NR_RACHUNKU>
,
<DC_RODZAJ_RACHUNKU>SO</DC_RODZAJ_RACHUNKU>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns3:header)}
    {getFieldsFromInvoke($body/ns3:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>