<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:transferorder.entities.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns3:invokeResponse>
  <ns3:transferOrderOut>
    <ns0:TransferOrderAnchor>
      <ns0:expirationDate?>{data($parm/DC_DATA_WYGASNIECIA)}</ns0:expirationDate>
      <ns0:active?>{sourceValue2Boolean(data($parm/DC_STATUS_RACHUNKU),"0")}</ns0:active>
    </ns0:TransferOrderAnchor>
  </ns3:transferOrderOut>
</ns3:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>