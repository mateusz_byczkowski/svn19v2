<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-07-18</con:description>
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns4="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkChannel ($parm as xs:string?) as xs:string{
    if ($parm) then 
       if (string-length(data($parm))>0) then data($parm)
       else "0"
    else "0"
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}</NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

<NF_PRODCO_ENGLISHCONFIGURA?>{data($parm/ns0:productConfigurationEntity/ns4:ProductConfiguration/ns4:englishConfigurationName)}</NF_PRODCO_ENGLISHCONFIGURA>
,
<NF_PRODCO_IDPRODUCTCONFIGU?>{data($parm/ns0:productConfigurationEntity/ns4:ProductConfiguration/ns4:idProductConfiguration)}</NF_PRODCO_IDPRODUCTCONFIGU>
,
<NF_PRODCO_POLISHCONFIGURAT?>{data($parm/ns0:productConfigurationEntity/ns4:ProductConfiguration/ns4:polishConfigurationName)}</NF_PRODCO_POLISHCONFIGURAT>
,
<NF_PRODCHA_IDPRODUCTCHANNE?>{chkChannel($parm/ns0:productConfigurationEntity/ns4:ProductConfiguration/ns4:productChannel/ns4:ProductChannel/ns4:idProductChannel)}</NF_PRODCHA_IDPRODUCTCHANNE>
,
let $idProductUnitsList := $parm/ns0:productConfigurationEntity/ns4:ProductConfiguration/ns4:configurationUnitsList/ns4:ProductUnitsList/ns4:idProductUnitsList
for $x in $idProductUnitsList
return

   if (string-length($x)>0) then
      <NF_PRODUL_IDPRODUCTUNITSLI?>{data($x)}</NF_PRODUL_IDPRODUCTUNITSLI>
   else ()

};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>