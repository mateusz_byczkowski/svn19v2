<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-07-18</con:description>
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns5="urn:productstree.entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns6="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if($parm = "false")
       then $falseval
    else $parm
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}</NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

<NF_PRODUG_POLISHGROUPNAME?>{data($parm/ns0:productGroup/ns5:ProductGroup/ns5:polishGroupName)}</NF_PRODUG_POLISHGROUPNAME>
,
<NF_PRODUG_CODEPRODUCTGROUP?>{data($parm/ns0:productGroup/ns5:ProductGroup/ns5:codeProductGroup)}</NF_PRODUG_CODEPRODUCTGROUP>
,
<NF_PRODUG_RECEIVER?>{data($parm/ns0:productGroup/ns5:ProductGroup/ns5:receiver)}</NF_PRODUG_RECEIVER>
,
<NF_PRODUG_IDPRODUCTGROUP?>{data($parm/ns0:productGroup/ns5:ProductGroup/ns5:idProductGroup)}</NF_PRODUG_IDPRODUCTGROUP>
,
<NF_SYSTEM_SYSTEM?>{data($parm/ns0:productGroup/ns5:ProductGroup/ns5:codeProductSystem/ns3:System/ns3:system)}</NF_SYSTEM_SYSTEM>
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns0:bcd/ns6:BusinessControlData/ns6:pageControl/ns4:PageControl/ns4:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns0:bcd/ns6:BusinessControlData/ns6:pageControl/ns4:PageControl/ns4:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{boolean2SourceValue(data($parm/ns0:bcd/ns6:BusinessControlData/ns6:pageControl/ns4:PageControl/ns4:reverseOrder),"1","0")}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns0:bcd/ns6:BusinessControlData/ns6:pageControl/ns4:PageControl/ns4:navigationKeyDefinition)}</NF_PAGEC_NAVIGATIONKEYDEFI>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns0:bcd/ns6:BusinessControlData/ns6:pageControl/ns4:PageControl/ns4:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>