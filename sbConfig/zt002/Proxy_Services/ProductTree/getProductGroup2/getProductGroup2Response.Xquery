<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-05-27</con:description>
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns4="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForAttributesProductGroupList($parm as element(fml:FML32)) as element()
{

<ns4:attributesProductGroupList>
  {
    for $x at $occ in $parm/NF_ATTRPG_CODEPRODUCT
    return
    <ns4:AttributesProductGroup>
      <ns4:codeProduct?>{data($parm/NF_ATTRPG_CODEPRODUCT[$occ])}</ns4:codeProduct>
      <ns4:firstProductFeature?>{data($parm/NF_ATTRPG_FIRSTPRODUCTFEAT[$occ])}</ns4:firstProductFeature>
      <ns4:secondProductFeature?>{data($parm/NF_ATTRPG_SECONDPRODUCTFEA[$occ])}</ns4:secondProductFeature>
      <ns4:thirdProductFeature?>{data($parm/NF_ATTRPG_THIRDPRODUCTFEAT[$occ])}</ns4:thirdProductFeature>
      <ns4:fourthProductFeature?>{data($parm/NF_ATTRPG_FOURTHPRODUCTFEA[$occ])}</ns4:fourthProductFeature>
      <ns4:fifthProductFeature?>{data($parm/NF_ATTRPG_FIFTHPRODUCTFEAT[$occ])}</ns4:fifthProductFeature>
      <ns4:idAttributesProductGroup?>{data($parm/NF_ATTRPG_IDATTRIBUTESPROD[$occ])}</ns4:idAttributesProductGroup>
    </ns4:AttributesProductGroup>
  }
</ns4:attributesProductGroupList>
};
declare function getElementsForProductAttributesList($parm as element(fml:FML32)) as element()
{

<ns4:productAttributesList>
  {
    for $x at $occ in $parm/NF_PRODAT_ATTRIBUTEID
    return
    <ns4:ProductAttributes>
      <ns4:attributeValue?>{data($parm/NF_PRODAT_ATTRIBUTEVALUE[$occ])}</ns4:attributeValue>
      <ns4:attributeValue2?>{data($parm/NF_PRODAT_ATTRIBUTEVALUE2[$occ])}</ns4:attributeValue2>
      <ns4:attributeID?>{data($parm/NF_PRODAT_ATTRIBUTEID[$occ])}</ns4:attributeID>
      {
      if (data($parm/NF_PRODAT_IDPRODUCTCONFIGU[$occ]) != '-1') then
      <ns4:idProductConfiguration?>{data($parm/NF_PRODAT_IDPRODUCTCONFIGU[$occ])}</ns4:idProductConfiguration>
      else ()
      }
      {
      if (data($parm/NF_PRODAT_IDPRODUCTCHANNEL[$occ]) != '-1') then
      <ns4:idProductChannel?>{data($parm/NF_PRODAT_IDPRODUCTCHANNEL[$occ])}</ns4:idProductChannel>
      else ()
      }
      <ns4:productAreaAtributes>
        <ns4:ProductAreaAttributes>
          <ns4:attributeID?>{data($parm/NF_PRODAA_ATTRIBUTEID[$occ])}</ns4:attributeID>
          <ns4:attributeName?>{data($parm/NF_PRODAA_ATTRIBUTENAME[$occ])}</ns4:attributeName>
          <ns4:attributeType>
            <ns3:ProductAreaAttributeType>
              <ns3:attributeType?>{data($parm/NF_PROAAT_ATTRIBUTETYPE[$occ])}</ns3:attributeType>
            </ns3:ProductAreaAttributeType>
          </ns4:attributeType>
        </ns4:ProductAreaAttributes>
      </ns4:productAreaAtributes>
{
if ($parm/NF_PRODCO_POLISHCONFIGURAT[$occ] != "") then
      <ns4:productConfiguration>
        <ns4:ProductConfiguration>
          <ns4:polishConfigurationName?>{data($parm/NF_PRODCO_POLISHCONFIGURAT[$occ])}</ns4:polishConfigurationName>
        </ns4:ProductConfiguration>
      </ns4:productConfiguration>
else ()
}
{
if ($parm/NF_PRODCHA_POLISHCHANNELNA[$occ] != "") then
      <ns4:productChannel>
        <ns4:ProductChannel>
          <ns4:polishChannelName?>{data($parm/NF_PRODCHA_POLISHCHANNELNA[$occ])}</ns4:polishChannelName>
        </ns4:ProductChannel>
      </ns4:productChannel>
else ()
}
    </ns4:ProductAttributes>
  }
</ns4:productAttributesList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  <ns0:productGroupEntity>
    <ns4:ProductGroup>
      <ns4:polishGroupName?>{data($parm/NF_PRODUG_POLISHGROUPNAME)}</ns4:polishGroupName>
      <ns4:codeProductGroup?>{data($parm/NF_PRODUG_CODEPRODUCTGROUP)}</ns4:codeProductGroup>
      <ns4:englishGroupName?>{data($parm/NF_PRODUG_ENGLISHGROUPNAME)}</ns4:englishGroupName>
      <ns4:sortOrder?>{data($parm/NF_PRODUG_SORTORDER)}</ns4:sortOrder>
      <ns4:visibilitySzrek?>{sourceValue2Boolean(data($parm/NF_PRODUG_VISIBILITYSZREK),"1")}</ns4:visibilitySzrek>
      <ns4:visibilityCRM?>{sourceValue2Boolean(data($parm/NF_PRODUG_VISIBILITYCRM),"1")}</ns4:visibilityCRM>
      <ns4:receiver?>{data($parm/NF_PRODUG_RECEIVER)}</ns4:receiver>
      <ns4:idProductCategory?>{data($parm/NF_PRODUG_IDPRODUCTCATEGOR)}</ns4:idProductCategory>
      <ns4:userChangeSKP?>{data($parm/NF_PRODUG_USERCHANGESKP)}</ns4:userChangeSKP>
      <ns4:dateChange?>{data($parm/NF_PRODUG_DATECHANGE)}</ns4:dateChange>
      <ns4:visibilityDCL?>{sourceValue2Boolean(data($parm/NF_PRODUG_VISIBILITYDCL),"1")}</ns4:visibilityDCL>
      <ns4:idProductGroup?>{data($parm/NF_PRODUG_IDPRODUCTGROUP)}</ns4:idProductGroup>
      {getElementsForAttributesProductGroupList($parm)}
      {getElementsForProductAttributesList($parm)}
      <ns4:codeProductSystem>
        <ns3:System>
          <ns3:system?>{data($parm/NF_SYSTEM_SYSTEM)}</ns3:system>
          <ns3:description?>{data($parm/NF_SYSTEM_DESCRIPTION)}</ns3:description>
        </ns3:System>
      </ns4:codeProductSystem>
    </ns4:ProductGroup>
  </ns0:productGroupEntity>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>