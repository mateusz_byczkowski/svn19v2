<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-01-21</con:description>
    <con:xquery><![CDATA[declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace fml="";

declare variable $body external;


<soap-env:Body>
  <soap-env:Body>
	{
	  (: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
	  let $reason := fn:substring-before(fn:substring-after(fn:substring-before($body/ctx:fault/ctx:reason, ":"), "("), ")")
	  let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($body/ctx:fault/ctx:reason, ":"), ":"), ":")
	  return
           <FML32>       
            { if( string-length(data($reason)) = 0)
                 then <TPAERRNO>12</TPAERRNO>
                 else <TPAERRNO>{$reason}</TPAERRNO>
            }           
            { if( string-length(data($urcode)) = 0)
                 then <TPAURCODE>0</TPAURCODE>
                 else <TPAURCODE>{data($urcode)}</TPAURCODE>
            }
            </FML32>
	}
   
  </soap-env:Body>
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>