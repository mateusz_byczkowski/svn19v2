<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace urn3="urn:be.services.dcl";
declare variable $body external;
declare variable $fault external;
declare variable $headerCache external;


declare function local:fault($faultCode as xs:string, $faultString as xs:string, $detail as element()?) as element(soap-env:Fault) {
	<soap-env:Fault>
		<faultcode>{ $faultCode }</faultcode> 
		<faultstring>{ $faultString }</faultstring> 
		<detail>{ $detail }</detail>
	</soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string,
        $errorDescription as xs:string) as element(err:exceptionItem) {
   <err:exceptionItem>
	<err:errorCode1>{ $errorCode1 }</err:errorCode1>
	<err:errorCode2>{ $errorCode2 }</err:errorCode2>
        <err:errorDescription>{ $errorDescription }</err:errorDescription>
   </err:exceptionItem>
};

<soap-env:Body>
 {
	let $reason := data($fault/ctx:reason)
	let $defaultDesc := "Krytyczny błąd usługi w systemie źródłowym"
	let $alsbErrorDesc := "Wystąpił błąd w usłudze proxy na ALSB"
	let $errorDesc:=data($body/soap-env:Fault/faultstring)
	let $messageID := data($headerCache/urn3:header/urn3:msgHeader/urn3:msgId)

        let $errorCode := data($fault/ctx:errorCode)

        return
        if (data($body) != "") then
          (: $body/soap-env:Fault :)
          local:fault($errorCode, "General exception", element err:ServiceException { local:errors("0","0",concat($reason, concat(' (',concat($messageID, ')'))))})
        else if ($reason = "Not Found") then
          local:fault($errorCode, $reason, element err:ServiceException { local:errors("0","0",concat("Błąd połączenia z Infobazą", concat(' (',concat($messageID, ')'))))})
        else
          local:fault($errorCode, $reason, element err:ServiceException { local:errors("0","0",concat($reason, concat(' (',concat($messageID, ')'))))})
    }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>