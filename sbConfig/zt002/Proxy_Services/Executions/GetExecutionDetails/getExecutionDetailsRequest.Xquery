<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns0="urn:executiondict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace ns6="urn:executions.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns5:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns5:msgHeader/ns5:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns5:msgHeader/ns5:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns5:msgHeader/ns5:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns5:msgHeader/ns5:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns5:msgHeader/ns5:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns5:msgHeader/ns5:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns5:transHeader/ns5:transId)}</NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns5:invoke)) as element()*
{

<NF_EXECUT_EXECUTIONNUMBERL?>{data($parm/ns5:execution/ns6:Execution/ns6:executionNumberLPP)}</NF_EXECUT_EXECUTIONNUMBERL>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns5:header)}
    {getFieldsFromInvoke($body/ns5:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>