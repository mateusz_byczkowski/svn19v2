<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Typy dotyczace produktów CU
Version: 3.008Version.$1.2011-05-23</con:description>
    <con:schema><![CDATA[<?xml version="1.0" encoding="UTF-8"?>
<!--
        Typy dotyczace produktów CU
        $Revision: 3.008 $
	$Date: 2010-02-19 12:00:00 $
-->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:cmf-biz="http://jv.channel.cu.com.pl/cmf-biz-types" xmlns:cmf-product="http://jv.channel.cu.com.pl/cmf-product-types" targetNamespace="http://jv.channel.cu.com.pl/cmf-product-types">
	<xs:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd"/>
	<xs:complexType name="CategoryType">
		<xs:annotation>
			<xs:documentation>
                Kategoria produktu
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:CategoryNameType">
				<xs:annotation>
					<xs:documentation>
                        Nazwa kategorii produktu
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="description" type="cmf-biz:DescriptionType">
				<xs:annotation>
					<xs:documentation>
                        Opis biznesowy
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="FactoryType">
		<xs:annotation>
			<xs:documentation>
                Typ opisujący fabrykę
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:FactoryNameType">
				<xs:annotation>
					<xs:documentation>
                        Nazwa fabryki, obecnie LIFE, PTE, CUO, TFI, CUP_CUP_EXT
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="description" type="cmf-biz:DescriptionType">
				<xs:annotation>
					<xs:documentation>
                        Opis biznesowy
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="RiskType">
		<xs:annotation>
			<xs:documentation>Typ opisujący ryzyko na opcji</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:DescriptionType"/>
			<xs:element name="long-name" type="cmf-biz:DescriptionType"/>
			<xs:element name="cover-type" type="xs:int"/>
		</xs:sequence>
		<xs:attribute name="id" type="cmf-biz:RiskIdType"/>
	</xs:complexType>
	<xs:complexType name="CoverageCategoryType">
		<xs:annotation>
			<xs:documentation>
                Typ opisujący kategorię umowy dodatkowej
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:DescriptionType"/>
			<xs:element name="risk" type="cmf-product:RiskType" minOccurs="0" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>Rysyka mogące występować na podanej umowie</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
		<xs:attribute name="id" type="cmf-biz:CoverageCategoryNameType"/>
	</xs:complexType>
	<xs:complexType name="CoverageCategoryListType">
		<xs:sequence>
			<xs:element name="coverage" type="cmf-product:CoverageCategoryType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="CoverageNameListType">
		<xs:sequence>
			<xs:element name="coverage" type="cmf-biz:CoverageCategoryNameType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="OwuDocType">
		<xs:sequence>
			<xs:element name="doc-id" type="cmf-biz:OwuDocIdType"/>
			<xs:element name="signature" type="cmf-biz:OwuSignatureType"/>
			<xs:element name="eff-date" type="xs:date" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="OwuDocListType">
		<xs:sequence>
			<xs:element name="attachment" type="cmf-product:OwuDocType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="OwuType">
		<xs:annotation>
			<xs:documentation>Types related to OWU</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="signature" type="cmf-biz:OwuSignatureType"/>
			<xs:element name="eff-date" type="xs:date" minOccurs="0"/>
			<xs:element name="attachments" type="cmf-product:OwuDocListType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ProductFundType">
		<xs:sequence>
			<xs:element name="fund-name" type="cmf-biz:FundNameType"/>
			<xs:element name="status" type="cmf-biz:FundStatusType"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ProductFundListType">
		<xs:sequence>
			<xs:element name="fund" type="cmf-product:ProductFundType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="LevelDefinitionType">
		<xs:sequence>
			<xs:element name="level" type="cmf-biz:LevelType"/>
			<xs:element name="amount" type="cmf-biz:MoneyType"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="LevelDefinitionListType">
		<xs:sequence>
			<xs:element name="level-definition" type="cmf-product:LevelDefinitionType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ProductType">
		<xs:annotation>
			<xs:documentation>
                Produkt, typ polisy
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="description" type="cmf-biz:DescriptionType">
				<xs:annotation>
					<xs:documentation>
                        Opis biznesowy produktu
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="source-system" type="cmf-biz:SourceSystemNameType">
				<xs:annotation>
					<xs:documentation>
                        System źródłowy z jakiego produkt pochodzi
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="source-business" type="cmf-biz:SourceBusinessNameType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
                        Linia biznesowa z jakiej produkt pochodzi
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="category" type="cmf-biz:CategoryNameType">
				<xs:annotation>
					<xs:documentation>
                        Kategoria produktu
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="is-group" type="xs:boolean" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
                        Czy produkt grupowy
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="comm-calc-type" type="cmf-biz:CommCalcType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
                        Sposób klakulacji prowizji
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="clawback" type="cmf-biz:ClawbackType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
                        Sposób liczenia clawback
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="rank-ratio" type="cmf-biz:RankType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
                        Współczynnik rankingowy
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="def-reduction" type="cmf-biz:ReductionType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
                        Domyślna redukcja prowizji
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="owu" type="cmf-product:OwuType" minOccurs="0"/>
			<xs:element name="param-card-sig" type="cmf-biz:ParamCardSigType" minOccurs="0"/>
			<xs:element name="regulation-sig" type="cmf-biz:RegulationSigType" minOccurs="0"/>
			<xs:element name="min-customer-age" type="xs:int" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  Minimalny wiek klienta
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="max-customer-age" type="xs:int" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  Maksymalny wiek klienta
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="min-first-payment" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					   Kwota minimalnej wpłaty jednorazowej
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="min-withdrawal" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					   Kwota minimalnej wypłaty
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="min-product-value" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  Minimalna wartość polisy
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="min-additional-premium" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  Minimalna kwota dodatkowa
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="min-percentage-benefit" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  Minimalny procent uposażenia
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="max-sum-percentage-benefit" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  Maksymalna suma procentów dla uposażonych
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="max-day-first-payment" type="xs:int" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  Maksymalna ilość dni (od daty złożenia wniosku) na dokonanie wymaganej wpłaty jednorazowej 
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="participation-factor" type="xs:int" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  Wskaźnik partycypacji, wskaźnik giełdowy do zwrotu z inwestycji
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="sum-insured-increase" type="xs:int" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  Stawka wzrostu sumy ubezpieczenia, używana do kalkulacji sumy ubezpieczenia
					  składki inwestowanej - iloczyn 
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="liquidation-cost" type="xs:int" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  opłata likwidacyjna, koszt pobierany od klienta w przypadku dożycia
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="surrender-market-rate" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
                        wskaznik_techniczny - procent skladki jaka zostanie wyplacona klientowi
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="factory" type="cmf-biz:FactoryNameType" minOccurs="0"/>
			<xs:element name="bisiness-line" type="cmf-biz:BusinessLineType" minOccurs="0"/>
			<xs:element name="coverage-list" type="cmf-product:CoverageNameListType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Opcje mogące występować na danym produkcie</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="level-list" type="cmf-product:LevelDefinitionListType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Lista wariantów i ich składek występujących na tym produkcie</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="fund-list" type="cmf-product:ProductFundListType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Fundusze mogące występować na danym produkcie</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="PolicyPrefix" type="cmf-biz:PolicyPrefixType" minOccurs="0"/>
			<xs:element name="currency" type="cmf-biz:CurrencyType" minOccurs="0"/>
			<xs:element name="fund-min-split-percentage" type="xs:float" minOccurs="0">
				<xs:annotation>
					<xs:documentation>minimalny procent alokacji dla funduszu</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
		<xs:attribute name="name" type="cmf-biz:ProductNameType" use="required">
			<xs:annotation>
				<xs:documentation>
                    Nazwa produktu, unikalna w skali firmy, jest
                    xs:elementem identyfikującym polisę wraz z jej numerem
                    rachunku
                </xs:documentation>
			</xs:annotation>
		</xs:attribute>
	</xs:complexType>
	<xs:complexType name="ProductListType">
		<xs:sequence>
			<xs:element name="product" type="cmf-product:ProductType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="SourceSystemType">
		<xs:annotation>
			<xs:documentation>
                System źródłowy w którym produkty są zdefiniowane i obsługiwane
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:SourceSystemNameType">
				<xs:annotation>
					<xs:documentation>
                        Nazwa systemu, obecnie
                        GRUP,PPS,USP,GNS,TFI,PARTM,PARTG,STRL.
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="description" type="cmf-biz:DescriptionType">
				<xs:annotation>
					<xs:documentation>
                        Opis biznesowy
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="factory" type="cmf-biz:FactoryNameType">
				<xs:annotation>
					<xs:documentation>
                        Fabryka sprzedająca produkty tego systemu źródłowego
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
</xs:schema>]]></con:schema>
    <con:dependencies>
        <con:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd" ref="Business Services/ESB JV-CUP/WSDL/base/cmf-biz-types.xsd"/>
    </con:dependencies>
    <con:targetNamespace>http://jv.channel.cu.com.pl/cmf-product-types</con:targetNamespace>
</con:schemaEntry>