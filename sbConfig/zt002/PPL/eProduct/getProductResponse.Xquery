<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetProductResponse($fml as element(fml:FML32))
	as element(m:getProductResponse) {
		<m:getProductResponse>
		<getProductReturn>
			{
				if($fml/fml:B_KOD_USLUGI)
					then <serviceCode>{ data($fml/fml:B_KOD_USLUGI) }</serviceCode>
					else ()
			}
			{
				if($fml/fml:B_PLAN_OPLAT)
					then <paymentPlan>{ data($fml/fml:B_PLAN_OPLAT) }</paymentPlan>
					else ()
			}
			{
				if($fml/fml:B_IRODZAJ_RACH)
					then <productKindNo>{ data($fml/fml:B_IRODZAJ_RACH) }</productKindNo>
					else ()
			}
			{
				if($fml/fml:B_ITYP)
					then <subtype>{ data($fml/fml:B_ITYP) }</subtype>
					else ()
			}
			{
				if($fml/fml:B_NAZWA)
					then <name>{ data($fml/fml:B_NAZWA) }</name>
					else ()
			}
			{
				if($fml/fml:B_OPIS_RACH)
					then <description>{ data($fml/fml:B_OPIS_RACH) }</description>
					else ()
			}
			{
				if($fml/fml:B_SKROT_OPISU)
					then <shortDescription>{ data($fml/fml:B_SKROT_OPISU) }</shortDescription>
					else ()
			}
			{
				if($fml/fml:B_TYP_RACH)
					then <accountTypeId>{ data($fml/fml:B_TYP_RACH) }</accountTypeId>
					else ()
			}
			{
				if($fml/fml:B_RODZAJ_RACH)
					then <productKind>{ data($fml/fml:B_RODZAJ_RACH) }</productKind>
					else ()
			}
			{
				if($fml/fml:B_OSOBA)
					then <customerType>{ data($fml/fml:B_OSOBA) }</customerType>
					else ()
			}
			{
				if($fml/fml:B_OPCJE_EL)
					then <elOption>{ data($fml/fml:B_OPCJE_EL) }</elOption>
					else ()
			}
			{
				if($fml/fml:B_KOD_WALUTY)
					then <currency>{ data($fml/fml:B_KOD_WALUTY) }</currency>
					else ()
			}
			{
				if($fml/fml:B_OPCJA)
					then <option>{ data($fml/fml:B_OPCJA) }</option>
					else ()
			}
			{
				if($fml/fml:B_OKR_WKLADU)
					then <period>{ data($fml/fml:B_OKR_WKLADU) }</period>
					else ()
			}
			{
				if($fml/fml:B_JDN_OKR_WKLADU)
					then <unit>{ data($fml/fml:B_JDN_OKR_WKLADU) }</unit>
					else ()
			}
		</getProductReturn>
		</m:getProductResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetProductResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>