<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetProductRequest($req as element(m:getProduct))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/userDescription/microbranch)
					then <fml:E_MICRO_BRANCH>{ data($req/userDescription/microbranch) }</fml:E_MICRO_BRANCH>
					else ()
			}
			{
				if($req/userDescription/username)
					then <fml:U_USER_NAME>{ data($req/userDescription/username) }</fml:U_USER_NAME>
					else ()
			}
			{
				if($req/productKind)
					then <fml:B_RODZAJ_RACH>{ data($req/productKind) }</fml:B_RODZAJ_RACH>
					else ()
			}
			{
				if($req/serviceCode)
					then <fml:B_KOD_USLUGI>{ data($req/serviceCode) }</fml:B_KOD_USLUGI>
					else ()
			}
			{
				if($req/paymentPlan)
					then <fml:B_PLAN_OPLAT>{ data($req/paymentPlan) }</fml:B_PLAN_OPLAT>
					else ()
			}
			{
				if($req/subtype)
					then <fml:B_ITYP>{ data($req/subtype) }</fml:B_ITYP>
					else ()
			}
			<fml:B_LICZBA_REK>1</fml:B_LICZBA_REK>
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetProductRequest($body/m:getProduct) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>