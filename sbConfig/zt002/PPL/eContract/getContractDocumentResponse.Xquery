<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetContractDocumentResponse($fml as element(fml:FML32))
	as element(m:getContractDocumentResponse) {
		<m:getContractDocumentResponse>
		<getContractDocumentReturn>
			{
				if($fml/fml:E_DOC_CONTENT)
					then <pdfContent>{ data($fml/fml:E_DOC_CONTENT) }</pdfContent>
					else ()
			}
		</getContractDocumentReturn>
		</m:getContractDocumentResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetContractDocumentResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>