<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetLatestContractTemplateResponse($fml as element(fml:FML32))
	as element(m:getLatestContractTemplateResponse) {
		<m:getLatestContractTemplateResponse>
		<getLatestContractTemplateReturn>
			{
				if($fml/fml:E_DOC_TEMPL_TYPE)
					then <type>{ data($fml/fml:E_DOC_TEMPL_TYPE) }</type>
					else ()
			}
			{
				if($fml/fml:E_DOC_TEMPL_VERSION)
					then <version>{ data($fml/fml:E_DOC_TEMPL_VERSION) }</version>
					else ()
			}
			{
				if($fml/fml:E_DOC_TEMPL_STATUS)
					then <status>{ data($fml/fml:E_DOC_TEMPL_STATUS) }</status>
					else ()
			}
			{
				if($fml/fml:E_DOC_TEMPL_DATE)
					then <date>{ data($fml/fml:E_DOC_TEMPL_DATE) }</date>
					else ()
			}
			{
				if($fml/fml:E_DOC_TEMPL_COMMENT)
					then <comment>{ data($fml/fml:E_DOC_TEMPL_COMMENT) }</comment>
					else ()
			}
			{
				if($fml/fml:E_TIME_STAMP)
					then <timestamp>{ data($fml/fml:E_TIME_STAMP) }</timestamp>
					else ()
			}
			{
				if($fml/fml:E_DOC_CONTENT)
					then <content>{ data($fml/fml:E_DOC_CONTENT) }</content>
					else ()
			}
		</getLatestContractTemplateReturn>
		</m:getLatestContractTemplateResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetLatestContractTemplateResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>