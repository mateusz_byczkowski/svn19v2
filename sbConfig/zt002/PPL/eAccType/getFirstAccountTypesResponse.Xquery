<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetFirstAccountTypesResponse($fml as element(fml:FML32))
	as element(m:getFirstAccountTypesResponse) {
		<m:getFirstAccountTypesResponse xmlns:m="http://bzwbk.com/services/ppl/messages">
			<getFirstAccountTypesReturn xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="soapenc:Array" soapenc:arrayType="m:AccountType[0]">
			{

				let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
				let $E_DEF_CHANNEL_ALLOW := $fml/fml:E_DEF_CHANNEL_ALLOW
				let $B_RODZAJ_RACH := $fml/fml:B_RODZAJ_RACH
				let $E_CR_TRN_MASK := $fml/fml:E_CR_TRN_MASK
				let $E_DR_TRN_MASK := $fml/fml:E_DR_TRN_MASK
				let $E_ACCOUNT_TYPE_OPTIONS := $fml/fml:E_ACCOUNT_TYPE_OPTIONS
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				for $it at $p in $fml/fml:B_TYP_RACH
				return
					<item>
					{
						if($E_ACCOUNT_TYPE_NAME[$p])
							then <name>{ data($E_ACCOUNT_TYPE_NAME[$p]) }</name>
						else ()
					}
					{
						if($E_DEF_CHANNEL_ALLOW[$p])
							then <defaultChannelAllowance>{ data($E_DEF_CHANNEL_ALLOW[$p]) }</defaultChannelAllowance>
						else ()
					}
					{
						if($B_RODZAJ_RACH[$p])
							then <productType>{ data($B_RODZAJ_RACH[$p]) }</productType>
						else ()
					}
					{
						if($E_CR_TRN_MASK[$p])
							then <creditTransactionMask>{ data($E_CR_TRN_MASK[$p]) }</creditTransactionMask>
						else ()
					}
					{
						if($E_DR_TRN_MASK[$p])
							then <debitTransactionMask>{ data($E_DR_TRN_MASK[$p]) }</debitTransactionMask>
						else ()
					}
					{
						if($E_ACCOUNT_TYPE_OPTIONS[$p])
							then <options>{ data($E_ACCOUNT_TYPE_OPTIONS[$p]) }</options>
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then <accountTypeId>{ data($B_TYP_RACH[$p]) }</accountTypeId>
						else ()
					}
					</item>
			}
		</getFirstAccountTypesReturn>
		</m:getFirstAccountTypesResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetFirstAccountTypesResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>