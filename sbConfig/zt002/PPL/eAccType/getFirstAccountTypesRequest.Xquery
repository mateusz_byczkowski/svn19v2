<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetFirstAccountTypesRequest($req as element(m:getFirstAccountTypes))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/accountsNo)
					then <fml:E_REC_COUNT>{ data($req/accountsNo) }</fml:E_REC_COUNT>
					else ()
			}
			{
				if($req/userDescription/microbranch)
					then <fml:E_MICRO_BRANCH>{ data($req/userDescription/microbranch) }</fml:E_MICRO_BRANCH>
					else ()
			}
			{
				if($req/userDescription/username)
					then <fml:U_USER_NAME>{ data($req/userDescription/username) }</fml:U_USER_NAME>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetFirstAccountTypesRequest($body/m:getFirstAccountTypes) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>