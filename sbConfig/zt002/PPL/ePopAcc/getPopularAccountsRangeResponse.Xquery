<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetPopularAccountsRangeResponse($fml as element(fml:FML32))
	as element(m:getPopularAccountsRangeResponse) {
		<m:getPopularAccountsRangeResponse xmlns:m="http://bzwbk.com/services/ppl/messages">
		<getPopularAccountsRangeReturn xsi:type="soapenc:Array" soapenc:arrayType="m:PopularAccount[0]" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">

			{

				let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
				let $E_POPACC_INFO := $fml/fml:E_POPACC_INFO
				let $E_VALID_TO := $fml/fml:E_VALID_TO
				let $E_VALID_FROM := $fml/fml:E_VALID_FROM
				let $E_TRN_TITLE := $fml/fml:E_TRN_TITLE
				let $E_BRANCH_CODE := $fml/fml:E_BRANCH_CODE
				let $B_WALUTA := $fml/fml:B_WALUTA
				let $B_KOD_POCZT := $fml/fml:B_KOD_POCZT
				let $B_M_ZAM := $fml/fml:B_M_ZAM
				let $B_UL_ZAM := $fml/fml:B_UL_ZAM
				let $B_NAZWA_KLIENTA := $fml/fml:B_NAZWA_KLIENTA
				let $E_TRANSFER_TYPE := $fml/fml:E_TRANSFER_TYPE
				let $E_POPACC_OPTIONS := $fml/fml:E_POPACC_OPTIONS
				let $E_TIME_STAMP := $fml/fml:E_TIME_STAMP
				let $B_TYP_RACH := $fml/fml:B_TYP_RACH
				for $it at $p in $fml/fml:B_DL_NR_RACH
				return
					<item>
					{
						if($B_DL_NR_RACH[$p])
							then <accountNo>{ data($B_DL_NR_RACH[$p]) }</accountNo>
						else ()
					}
					{
						if($E_POPACC_INFO[$p])
							then <info>{ data($E_POPACC_INFO[$p]) }</info>
						else ()
					}
					{
						if($E_VALID_TO[$p])
							then <validTo>{ data($E_VALID_TO[$p]) }</validTo>
						else ()
					}
					{
						if($E_VALID_FROM[$p])
							then <validFrom>{ data($E_VALID_FROM[$p]) }</validFrom>
						else ()
					}
					{
						if($E_TRN_TITLE[$p])
							then <description>{ data($E_TRN_TITLE[$p]) }</description>
						else ()
					}
					{
						if($E_BRANCH_CODE[$p])
							then <branchCode>{ data($E_BRANCH_CODE[$p]) }</branchCode>
						else ()
					}
					{
						if($B_WALUTA[$p])
							then <currency>{ data($B_WALUTA[$p]) }</currency>
						else ()
					}
					{
						if($B_KOD_POCZT[$p])
							then <zipCode>{ data($B_KOD_POCZT[$p]) }</zipCode>
						else ()
					}
					{
						if($B_M_ZAM[$p])
							then <city>{ data($B_M_ZAM[$p]) }</city>
						else ()
					}
					{
						if($B_UL_ZAM[$p])
							then <street>{ data($B_UL_ZAM[$p]) }</street>
						else ()
					}
					{
						if($B_NAZWA_KLIENTA[$p])
							then <name>{ data($B_NAZWA_KLIENTA[$p]) }</name>
						else ()
					}
					{
						if($E_TRANSFER_TYPE[$p])
							then <transferType>{ data($E_TRANSFER_TYPE[$p]) }</transferType>
						else ()
					}
					{
						if($E_POPACC_OPTIONS[$p])
							then <options>{ data($E_POPACC_OPTIONS[$p]) }</options>
						else ()
					}
					{
						if($E_TIME_STAMP[$p])
							then <timestamp>{ data($E_TIME_STAMP[$p]) }</timestamp>
						else ()
					}
					{
						if($B_TYP_RACH[$p])
							then <accountTypeId>{ data($B_TYP_RACH[$p]) }</accountTypeId>
						else ()
					}
					</item>
			}
		</getPopularAccountsRangeReturn>
		</m:getPopularAccountsRangeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetPopularAccountsRangeResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>