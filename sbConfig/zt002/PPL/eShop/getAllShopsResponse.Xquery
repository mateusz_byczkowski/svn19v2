<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetAllShopsResponse($fml as element(fml:FML32))
	as element(m:getAllShopsResponse) {
		<m:getAllShopsResponse xmlns:m="http://bzwbk.com/services/ppl/messages">
		<getAllShopsReturn xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="soapenc:Array" soapenc:arrayType="m:Shop[0]" xmlns="">
			{

				let $E_SHOP_DESC := $fml/fml:E_SHOP_DESC
				let $E_CUST_EMAIL := $fml/fml:E_CUST_EMAIL
				let $E_CUST_TELNO := $fml/fml:E_CUST_TELNO
				let $E_CUST_STREET := $fml/fml:E_CUST_STREET
				let $E_CUST_ZIPCODE := $fml/fml:E_CUST_ZIPCODE
				let $E_CUST_CITY := $fml/fml:E_CUST_CITY
				let $E_CUST_NAME := $fml/fml:E_CUST_NAME
				let $E_REP_NAME := $fml/fml:E_REP_NAME
				let $E_CUST_GSMNO := $fml/fml:E_CUST_GSMNO
				let $E_CUST_FAXNO := $fml/fml:E_CUST_FAXNO
				let $B_BIN_KARTY := $fml/fml:B_BIN_KARTY
				let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
				let $B_NR_DOK := $fml/fml:B_NR_DOK
				let $E_STATUS := $fml/fml:E_STATUS
				let $E_SHOP_ID := $fml/fml:E_SHOP_ID
				let $E_CUST_OPTIONS := $fml/fml:E_CUST_OPTIONS
				let $E_TIME_STAMP := $fml/fml:E_TIME_STAMP
				let $E_TRN_TYPE := $fml/fml:E_TRN_TYPE
				for $it at $p in $fml/fml:E_SHOP_ID
				return
					<item>
					{
						if($E_SHOP_DESC[$p])
							then <shortName>{ data($E_SHOP_DESC[$p]) }</shortName>
						else ()
					}
					{
						if($E_CUST_EMAIL[$p])
							then <email>{ data($E_CUST_EMAIL[$p]) }</email>
						else ()
					}
					{
						if($E_CUST_TELNO[$p])
							then <telephoneNo>{ data($E_CUST_TELNO[$p]) }</telephoneNo>
						else ()
					}
					{
						if($E_CUST_STREET[$p])
							then <street>{ data($E_CUST_STREET[$p]) }</street>
						else ()
					}
					{
						if($E_CUST_ZIPCODE[$p])
							then <zipCode>{ data($E_CUST_ZIPCODE[$p]) }</zipCode>
						else ()
					}
					{
						if($E_CUST_CITY[$p])
							then <city>{ data($E_CUST_CITY[$p]) }</city>
						else ()
					}
					{
						if($E_CUST_NAME[$p])
							then <name>{ data($E_CUST_NAME[$p]) }</name>
						else ()
					}
					{
						if($E_REP_NAME[$p])
							then <repName>{ data($E_REP_NAME[$p]) }</repName>
						else ()
					}
					{
						if($E_CUST_GSMNO[$p])
							then <gsmNo>{ data($E_CUST_GSMNO[$p]) }</gsmNo>
						else ()
					}
					{
						if($E_CUST_FAXNO[$p])
							then <faxNo>{ data($E_CUST_FAXNO[$p]) }</faxNo>
						else ()
					}
					{
						if($B_BIN_KARTY[$p])
							then <cardBin>{ data($B_BIN_KARTY[$p]) }</cardBin>
						else ()
					}
					{
						if($B_DL_NR_RACH[$p])
							then <accountNo>{ data($B_DL_NR_RACH[$p]) }</accountNo>
						else ()
					}
					{
						if($B_NR_DOK[$p])
							then <regon>{ data($B_NR_DOK[$p]) }</regon>
						else ()
					}
					{
						if($E_STATUS[$p])
							then <status>{ data($E_STATUS[$p]) }</status>
						else ()
					}
					{
						if($E_SHOP_ID[$p])
							then <shopId>{ data($E_SHOP_ID[$p]) }</shopId>
						else ()
					}
					{
						if($E_CUST_OPTIONS[$p])
							then <options>{ data($E_CUST_OPTIONS[$p]) }</options>
						else ()
					}
					{
						if($E_TIME_STAMP[$p])
							then <timestamp>{ data($E_TIME_STAMP[$p]) }</timestamp>
						else ()
					}
					{
						if($E_TRN_TYPE[$p])
							then <transactionTypeId>{ data($E_TRN_TYPE[$p]) }</transactionTypeId>
						else ()
					}
					</item>
			}
		</getAllShopsReturn>
		</m:getAllShopsResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetAllShopsResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>