<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ppl/messages/";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapcreateTransactionTypeResponse($fml as element(fml:FML32))
	as element(m:createTransactionTypeResponse) {
		<m:createTransactionTypeResponse>
			{
				if($fml/fml:E_TRN_TYPE)
					then <createTransactionTypeReturn>{ data($fml/fml:E_TRN_TYPE) }</createTransactionTypeReturn>
					else ()
			}
		</m:createTransactionTypeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapcreateTransactionTypeResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>