<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapdeleteTransactionTypeRequest($req as element(m:deleteTransactionType))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/userDescription/microbranch)
					then <fml:E_MICRO_BRANCH>{ data($req/userDescription/microbranch) }</fml:E_MICRO_BRANCH>
					else ()
			}
			{
				if($req/userDescription/username)
					then <fml:U_USER_NAME>{ data($req/userDescription/username) }</fml:U_USER_NAME>
					else ()
			}
			{
				if($req/transactionTypeId)
					then <fml:E_TRN_TYPE>{ data($req/transactionTypeId) }</fml:E_TRN_TYPE>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapdeleteTransactionTypeRequest($body/m:deleteTransactionType) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>