<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:dictionaries.be.dcl";
declare namespace m2 = "urn:baseauxentities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:dateTime) as xs:string {
    fn:concat (xf:convertTo2CharString(fn:day-from-dateTime($dateIn)),"-",
               xf:convertTo2CharString(fn:month-from-dateTime($dateIn)),"-",
               xf:convertTo4CharString(fn:year-from-dateTime($dateIn)))
};

declare function xf:mapTime($dateIn as xs:dateTime) as xs:string {
    fn:concat (xf:convertTo2CharString(fn:hours-from-dateTime($dateIn)),
               xf:convertTo2CharString(fn:minutes-from-dateTime($dateIn)),
               xf:convertTo2CharString(xs:integer(fn:seconds-from-dateTime($dateIn))))
};

&lt;soap-env:Body&gt;
  {
  let $req := $body/m:invoke
  let $reqh := $header/m:header
  return

  &lt;fml:FML32&gt;
    &lt;fml:B_MSHEAD_MSGID?&gt;{ data($reqh/m:msgHeader/m:msgId) }&lt;/fml:B_MSHEAD_MSGID&gt;

    {for $it in $req/m:currencyCodes/m1:CurrencyCode
       return
       if($it/m1:currencyCode)
         then &lt;fml:B_WALUTA&gt;{ data($it/m1:currencyCode) }&lt;/fml:B_WALUTA&gt;
         else ()
    }  
    {if($req/m:rateDate/m2:DateHolder/m2:value and fn:string-length($req/m:rateDate/m2:DateHolder/m2:value)&gt;0)
       then &lt;fml:B_DATA_WALUTY&gt;{xf:mapDate(data($req/m:rateDate/m2:DateHolder/m2:value)) }&lt;/fml:B_DATA_WALUTY&gt;
       else ()
    }
    {if($req/m:rateDate/m2:DateHolder/m2:value and fn:string-length($req/m:rateDate/m2:DateHolder/m2:value)&gt;0)
       then &lt;fml:B_GODZINA&gt;{xf:mapTime(data($req/m:rateDate/m2:DateHolder/m2:value)) }&lt;/fml:B_GODZINA&gt;
       else ()
    }
  &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>