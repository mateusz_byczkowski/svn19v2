<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl"; 
declare namespace ns6="urn:accountdict.dictionaries.be.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))&gt;0 and not ($dateIn = "0001-01-01"))
    then true()
    else false()
};

declare function getElementsForAssociatedAccountsList($parm as element(fml:FML32)) as element()
{


&lt;ns0:associatedAccountsList&gt;
  {

    let $xNF_PLECTA_PLEDGINGPRIORITY := $parm/NF_PLECTA_PLEDGINGPRIORITY
    let $xNF_COLLCO_PLEDGINGRULE := $parm/NF_COLLCO_PLEDGINGRULE
    let $xNF_COLLCO_CROSSCURRENCYRAT := $parm/NF_COLLCO_CROSSCURRENCYRAT


    for $x at $occ1 in $xNF_PLECTA_PLEDGINGPRIORITY
    return
    &lt;ns0:PledgeCollateralToAccount&gt;
    {if (data($xNF_PLECTA_PLEDGINGPRIORITY[$occ1]) and xf:isData(data($xNF_PLECTA_PLEDGINGPRIORITY[$occ1])))
      then &lt;ns0:pledgingPriority?&gt;{data($xNF_PLECTA_PLEDGINGPRIORITY[$occ1])}&lt;/ns0:pledgingPriority&gt;
      else ()
    }
      &lt;ns0:pledgingRule&gt;
        &lt;ns3:PledgingRule&gt;
          {if (data($xNF_COLLCO_PLEDGINGRULE[$occ1]))
            then &lt;ns3:pledgingRule?&gt;{data($xNF_COLLCO_PLEDGINGRULE[$occ1])}&lt;/ns3:pledgingRule&gt;
            else ()
          }
        &lt;/ns3:PledgingRule&gt;
      &lt;/ns0:pledgingRule&gt;
      &lt;ns0:crossCurrencyRateCode&gt;
        &lt;ns3:CurrencyRateCode&gt;
          (: Pole zdefiniowane w WSDL jako NF_CURRRC_CURRENCYRATECODE - usluga zwraca jako NF_COLLCO_CROSSCURRENCYRAT :)
          {if (data($xNF_COLLCO_CROSSCURRENCYRAT[$occ1]) and xf:isData(data($xNF_COLLCO_CROSSCURRENCYRAT[$occ1])))
            then &lt;ns3:currencyRateCode?&gt;{data($xNF_COLLCO_CROSSCURRENCYRAT[$occ1])}&lt;/ns3:currencyRateCode&gt;
            else ()
          }
        &lt;/ns3:CurrencyRateCode&gt;
      &lt;/ns0:crossCurrencyRateCode&gt;
    &lt;/ns0:PledgeCollateralToAccount&gt;
  }
&lt;/ns0:associatedAccountsList&gt;
};
declare function getElementsForCollaterals($parm as element(fml:FML32)) as element()
{


&lt;ns5:collaterals&gt;
  {


  let $xNF_COLLAT_LOCATIONCODE := $parm/NF_COLLAT_LOCATIONCODE
  let $xNF_COLLAT_COLLATERALDESCRI := $parm/NF_COLLAT_COLLATERALDESCRI
  let $xNF_COLLAT_EXPIRATIONORMATU := $parm/NF_COLLAT_EXPIRATIONORMATU
  let $xNF_COLLAT_INSURANCEREQUIRE := $parm/NF_COLLAT_INSURANCEREQUIRE
  let $xNF_COLLAT_INSURANCEEXPIRYD := $parm/NF_COLLAT_INSURANCEEXPIRYD
  let $xNF_COLLAT_LEGALCLAIMREGIST := $parm/NF_COLLAT_LEGALCLAIMREGIST
  let $xNF_COLLAT_LEGALCLAIMEXPIRY := $parm/NF_COLLAT_LEGALCLAIMEXPIRY
  let $xNF_COLLAT_NUMBEROFUNITS := $parm/NF_COLLAT_NUMBEROFUNITS
  let $xNF_COLLAT_UNITPRICE := $parm/NF_COLLAT_UNITPRICE
  let $xNF_COLLAT_MARGINPERCENTAGE := $parm/NF_COLLAT_MARGINPERCENTAGE
  let $xNF_COLLAT_MAXIMUMCOLLATERA := $parm/NF_COLLAT_MAXIMUMCOLLATERA
  let $xNF_COLLAT_DATELASTPRICED := $parm/NF_COLLAT_DATELASTPRICED
  let $xNF_COLLAT_COLLATERALITEMNU := $parm/NF_COLLAT_COLLATERALITEMNU
  let $xNF_COLLAT_COMMODITYIDCUSIP := $parm/NF_COLLAT_COMMODITYIDCUSIP
  let $xNF_COLLAT_ITEMREFERENCENUM := $parm/NF_COLLAT_ITEMREFERENCENUM
  let $xNF_COLLAT_SAFEKEEPINGRECEI := $parm/NF_COLLAT_SAFEKEEPINGRECEI
  let $xNF_COLLAT_SHORTDESCRIPTION := $parm/NF_COLLAT_SHORTDESCRIPTION
  let $xNF_COLLAT_STARTDATEOFSECLI := $parm/NF_COLLAT_STARTDATEOFSECLI
  let $xNF_COLLAT_COSTOFSALESOFSEC := $parm/NF_COLLAT_COSTOFSALESOFSEC
  let $xNF_COLLAT_ENDDATEOFSECLIQU := $parm/NF_COLLAT_ENDDATEOFSECLIQU
  let $xNF_COLLAT_CCY := $parm/NF_COLLAT_CCY
  let $xNF_COLLAT_AMT := $parm/NF_COLLAT_AMT
  let $xNF_COLLAT_AMOUNTCURRENTLYP := $parm/NF_COLLAT_AMOUNTCURRENTLYP
  let $xNF_COLLAT_AVAILABLETOPLEDG := $parm/NF_COLLAT_AVAILABLETOPLEDG
  let $xNF_COLLAT_ORIGINALCOLLATER := $parm/NF_COLLAT_ORIGINALCOLLATER
  let $xNF_COLLAT_COLLATERALINACTI := $parm/NF_COLLAT_COLLATERALINACTI
  let $xNF_COLLAT_USERFIELD6 := $parm/NF_COLLAT_USERFIELD6
  let $xNF_COLLAT_LASTMAINTENANCE := $parm/NF_COLLAT_LASTMAINTENANCE
  let $xNF_COLLAT_MAXIMUMPLEDGEAMO := $parm/NF_COLLAT_MAXIMUMPLEDGEAMO
  let $xNF_COLLAT_AMOUNTPLECCYOFCO := $parm/NF_COLLAT_AMOUNTPLECCYOFCO
  let $xNF_COLLAT_AMOUNTPLEDGEDCCY := $parm/NF_COLLAT_AMOUNTPLEDGEDCCY
  let $xNF_COLLAT_PLEDGEPURGEDAYS := $parm/NF_COLLAT_PLEDGEPURGEDAYS
  let $xNF_COLLAT_PLEDGEPURGEDATE := $parm/NF_COLLAT_PLEDGEPURGEDATE
  let $xNF_COLLAM_LEGALREGNUM := $parm/NF_COLLAM_LEGALREGNUM
  let $xNF_COLLAM_YEARBUILT := $parm/NF_COLLAM_YEARBUILT
  let $xNF_COLLAM_YEARPURCHASED := $parm/NF_COLLAM_YEARPURCHASED
  let $xNF_COLLAM_PURCHASEPRICE := $parm/NF_COLLAM_PURCHASEPRICE
  let $xNF_COLLAM_LOTSIZE := $parm/NF_COLLAM_LOTSIZE
  let $xNF_COLLAM_SQUAREFEET := $parm/NF_COLLAM_SQUAREFEET
  let $xNF_COLLAM_NUMBEROFRESIDENT := $parm/NF_COLLAM_NUMBEROFRESIDENT
  let $xNF_COLLAM_LEASEEXPIRATIOND := $parm/NF_COLLAM_LEASEEXPIRATIOND
  let $xNF_COLLAM_DATEINSPECTED := $parm/NF_COLLAM_DATEINSPECTED
  let $xNF_COLLAM_SUBDIVISION := $parm/NF_COLLAM_SUBDIVISION
  let $xNF_COLLAM_APPRAISEDVALUE := $parm/NF_COLLAM_APPRAISEDVALUE
  let $xNF_COLLAM_APPRAISALDATE := $parm/NF_COLLAM_APPRAISALDATE
  let $xNF_COLLAM_APPRAISERNAME := $parm/NF_COLLAM_APPRAISERNAME
  let $xNF_COLLAM_MORTGAGESPAYABLE := $parm/NF_COLLAM_MORTGAGESPAYABLE
  let $xNF_COLLAM_USERFIELD2 := $parm/NF_COLLAM_USERFIELD2
  let $xNF_COLLAM_USERFIELD3 := $parm/NF_COLLAM_USERFIELD3
  let $xNF_COLLAM_USERFIELD4 := $parm/NF_COLLAM_USERFIELD4
  let $xNF_COLLAM_USERFIELD5 := $parm/NF_COLLAM_USERFIELD5
  let $xNF_COLLAM_NEWADDUSEFIE2 := $parm/NF_COLLAM_NEWADDUSEFIE2
  let $xNF_COLLAM_NEWADDUSEFIE3 := $parm/NF_COLLAM_NEWADDUSEFIE3
  let $xNF_COLLAM_LEGALREGISTRATIO := $parm/NF_COLLAM_LEGALREGISTRATIO
  let $xNF_COLLAM_HOUSENUMBER := $parm/NF_COLLAM_HOUSENUMBER
  let $xNF_COLLAM_STREET := $parm/NF_COLLAM_STREET
  let $xNF_COLLAM_CITY := $parm/NF_COLLAM_CITY
  let $xNF_COLLAM_POSTALCODE := $parm/NF_COLLAM_POSTALCODE
  let $xNF_COLLAM_COUNTRY := $parm/NF_COLLAM_COUNTRY
  let $xNF_COLLAM_DISTRICT := $parm/NF_COLLAM_DISTRICT
  let $xNF_COLLAM_COUNTY := $parm/NF_COLLAM_COUNTY
  let $xNF_COLLAM_USERFIELD1 := $parm/NF_COLLAM_USERFIELD1
  let $xNF_NEWUT_NEWUSEDTYPE := $parm/NF_NEWUT_NEWUSEDTYPE
  let $xNF_PROPET_PROPERTYTYPE := $parm/NF_PROPET_PROPERTYTYPE
  let $xNF_OWNEOC_OWNEROCCUPIEDCOD := $parm/NF_OWNEOC_OWNEROCCUPIEDCOD
  let $xNF_TENURE_TENURE := $parm/NF_TENURE_TENURE
  let $xNF_COLMAT_COLLATERALMORTGA := $parm/NF_COLMAT_COLLATERALMORTGA
  let $xNF_COLLMP_COLLATERALMORTGA := $parm/NF_COLLMP_COLLATERALMORTGA
  let $xNF_COLLAC_MODELYEAR := $parm/NF_COLLAC_MODELYEAR
  let $xNF_COLLAC_RENTALDEMOFLAG := $parm/NF_COLLAC_RENTALDEMOFLAG
  let $xNF_COLLAC_VEHICLELICENSEEX := $parm/NF_COLLAC_VEHICLELICENSEEX
  let $xNF_COLLAC_FIRSTREGISTRATIO := $parm/NF_COLLAC_FIRSTREGISTRATIO
  let $xNF_COLLAC_SERIALNUMBER := $parm/NF_COLLAC_SERIALNUMBER
  let $xNF_COLLAC_MOTORNUMBER := $parm/NF_COLLAC_MOTORNUMBER
  let $xNF_COLLAC_PLATENUMBER := $parm/NF_COLLAC_PLATENUMBER
  let $xNF_CONDIF_CONDITIONFLAG := $parm/NF_CONDIF_CONDITIONFLAG
  let $xNF_LICETC_LICENSETYPECODE := $parm/NF_LICETC_LICENSETYPECODE
  let $xNF_FINATC_FINANCETYPECODE := $parm/NF_FINATC_FINANCETYPECODE
  (: let $xNF_STATE_STATE := $parm/NF_STATE_STATE :)
  let $xNF_BASES_STATE := $parm/NF_BASES_STATE
  let $xNF_MAKEC_MAKECODE := $parm/NF_MAKEC_MAKECODE
  let $xNF_BODYC_BODYCODE := $parm/NF_BODYC_BODYCODE
  let $xNF_COLLCO_COLLATERALCODE := $parm/NF_COLLCO_COLLATERALCODE
  let $xNF_CURREC_CURRENCYCODE := $parm/NF_CURREC_CURRENCYCODE
  let $xNF_METOSV_METHODOFSECURITY := $parm/NF_METOSV_METHODOFSECURITY
  let $xNF_PERVAS_PERSONVALUATINGA := $parm/NF_PERVAS_PERSONVALUATINGA
  let $xNF_COLLAE_COLLATERALENTITY := $parm/NF_COLLAE_COLLATERALENTITY

    for $x at $occ in $xNF_COLLAT_LOCATIONCODE
    return
    &lt;ns0:Collateral&gt;
      {if (data($xNF_COLLAT_LOCATIONCODE[$occ]))
        then &lt;ns0:locationCode?&gt;{data($xNF_COLLAT_LOCATIONCODE[$occ])}&lt;/ns0:locationCode&gt;
        else ()
      }
      {if (data($xNF_COLLAT_COLLATERALDESCRI[$occ]))
        then &lt;ns0:collateralDescription?&gt;{data($xNF_COLLAT_COLLATERALDESCRI[$occ])}&lt;/ns0:collateralDescription&gt;
        else ()
      }
      {if (data($xNF_COLLAT_EXPIRATIONORMATU[$occ]) and xf:isData(data($xNF_COLLAT_EXPIRATIONORMATU[$occ])))
        then &lt;ns0:expirationOrMaturityDate?&gt;{data($xNF_COLLAT_EXPIRATIONORMATU[$occ])}&lt;/ns0:expirationOrMaturityDate&gt;
        else ()
      }
      {if ($xNF_COLLAT_INSURANCEREQUIRE[$occ] &gt; 0)
        then
          &lt;ns0:insuranceRequired?&gt;true&lt;/ns0:insuranceRequired&gt;
        else
          &lt;ns0:insuranceRequired?&gt;false&lt;/ns0:insuranceRequired&gt;
      }
      {if (data($xNF_COLLAT_INSURANCEEXPIRYD[$occ]) and xf:isData(data($xNF_COLLAT_INSURANCEEXPIRYD[$occ])))
        then &lt;ns0:insuranceExpiryDate?&gt;{data($xNF_COLLAT_INSURANCEEXPIRYD[$occ])}&lt;/ns0:insuranceExpiryDate&gt;
        else ()
      }
      {if (data($xNF_COLLAT_LEGALCLAIMREGIST[$occ]) and xf:isData(data($xNF_COLLAT_LEGALCLAIMREGIST[$occ])))
        then &lt;ns0:legalClaimRegisteredDate?&gt;{data($xNF_COLLAT_LEGALCLAIMREGIST[$occ])}&lt;/ns0:legalClaimRegisteredDate&gt;
        else ()
      }
      {if (data($xNF_COLLAT_LEGALCLAIMEXPIRY[$occ]) and xf:isData(data($xNF_COLLAT_LEGALCLAIMEXPIRY[$occ])))
        then &lt;ns0:legalClaimExpiryDate?&gt;{data($xNF_COLLAT_LEGALCLAIMEXPIRY[$occ])}&lt;/ns0:legalClaimExpiryDate&gt;
        else ()
      }
      {if (data($xNF_COLLAT_NUMBEROFUNITS[$occ]) and xf:isData(data($xNF_COLLAT_NUMBEROFUNITS[$occ])))
        then &lt;ns0:numberOfUnits?&gt;{data($xNF_COLLAT_NUMBEROFUNITS[$occ])}&lt;/ns0:numberOfUnits&gt;
        else ()
      }
      {if (data($xNF_COLLAT_UNITPRICE[$occ]) and xf:isData(data($xNF_COLLAT_UNITPRICE[$occ])))
        then &lt;ns0:unitPrice?&gt;{data($xNF_COLLAT_UNITPRICE[$occ])}&lt;/ns0:unitPrice&gt;
        else ()
      }
      {if (data($xNF_COLLAT_MARGINPERCENTAGE[$occ]) and xf:isData(data($xNF_COLLAT_MARGINPERCENTAGE[$occ])))
        then &lt;ns0:marginPercentage?&gt;{data($xNF_COLLAT_MARGINPERCENTAGE[$occ])}&lt;/ns0:marginPercentage&gt;
        else ()
      }
      {if (data($xNF_COLLAT_MAXIMUMCOLLATERA[$occ]) and xf:isData(data($xNF_COLLAT_MAXIMUMCOLLATERA[$occ])))
        then &lt;ns0:maximumCollateralValue?&gt;{data($xNF_COLLAT_MAXIMUMCOLLATERA[$occ])}&lt;/ns0:maximumCollateralValue&gt;
        else ()
      }
      {if (data($xNF_COLLAT_DATELASTPRICED[$occ]) and xf:isData(data($xNF_COLLAT_DATELASTPRICED[$occ])))
        then &lt;ns0:dateLastPriced?&gt;{data($xNF_COLLAT_DATELASTPRICED[$occ])}&lt;/ns0:dateLastPriced&gt;
        else ()
      }
      {if (data($xNF_COLLAT_COLLATERALITEMNU[$occ]) and xf:isData(data($xNF_COLLAT_COLLATERALITEMNU[$occ])))
        then &lt;ns0:collateralItemNumber?&gt;{data($xNF_COLLAT_COLLATERALITEMNU[$occ])}&lt;/ns0:collateralItemNumber&gt;
        else ()
      }
      {if (data($xNF_COLLAT_COMMODITYIDCUSIP[$occ]))
        then &lt;ns0:commodityIdCusip?&gt;{data($xNF_COLLAT_COMMODITYIDCUSIP[$occ])}&lt;/ns0:commodityIdCusip&gt;
        else ()
      }
      {if (data($xNF_COLLAT_ITEMREFERENCENUM[$occ]))
        then &lt;ns0:itemReferenceNumber?&gt;{data($xNF_COLLAT_ITEMREFERENCENUM[$occ])}&lt;/ns0:itemReferenceNumber&gt;
        else ()
      }
      {if (data($xNF_COLLAT_SAFEKEEPINGRECEI[$occ]))
        then &lt;ns0:safekeepingReceiptNbr?&gt;{data($xNF_COLLAT_SAFEKEEPINGRECEI[$occ])}&lt;/ns0:safekeepingReceiptNbr&gt;
        else ()
      }
      {if (data($xNF_COLLAT_SHORTDESCRIPTION[$occ]))
        then &lt;ns0:shortDescription?&gt;{data($xNF_COLLAT_SHORTDESCRIPTION[$occ])}&lt;/ns0:shortDescription&gt;
        else ()
      }
      {if (data($xNF_COLLAT_STARTDATEOFSECLI[$occ]) and xf:isData(data($xNF_COLLAT_STARTDATEOFSECLI[$occ])))
        then &lt;ns0:startDateOfSecLiquidat?&gt;{data($xNF_COLLAT_STARTDATEOFSECLI[$occ])}&lt;/ns0:startDateOfSecLiquidat&gt;
        else ()
      }
      {if (data($xNF_COLLAT_COSTOFSALESOFSEC[$occ]) and xf:isData(data($xNF_COLLAT_COSTOFSALESOFSEC[$occ])))
        then &lt;ns0:costOfSalesOfSecurity?&gt;{data($xNF_COLLAT_COSTOFSALESOFSEC[$occ])}&lt;/ns0:costOfSalesOfSecurity&gt;
        else ()
      }
      {if (data($xNF_COLLAT_ENDDATEOFSECLIQU[$occ]) and xf:isData(data($xNF_COLLAT_ENDDATEOFSECLIQU[$occ])))
        then &lt;ns0:endDateOfSecLiquidat?&gt;{data($xNF_COLLAT_ENDDATEOFSECLIQU[$occ])}&lt;/ns0:endDateOfSecLiquidat&gt;
        else ()
      }
      {if (data($xNF_COLLAT_CCY[$occ]))
        then &lt;ns0:ccy?&gt;{data($xNF_COLLAT_CCY[$occ])}&lt;/ns0:ccy&gt;
        else ()
      }
      {if (data($xNF_COLLAT_AMT[$occ]) and xf:isData(data($xNF_COLLAT_AMT[$occ])))
        then &lt;ns0:amt?&gt;{data($xNF_COLLAT_AMT[$occ])}&lt;/ns0:amt&gt;
        else ()
      }
      {if (data($xNF_COLLAT_AMOUNTCURRENTLYP[$occ]) and xf:isData(data($xNF_COLLAT_AMOUNTCURRENTLYP[$occ])))
        then &lt;ns0:amountCurrentlyPledged?&gt;{data($xNF_COLLAT_AMOUNTCURRENTLYP[$occ])}&lt;/ns0:amountCurrentlyPledged&gt;
        else ()
      }
      {if (data($xNF_COLLAT_AVAILABLETOPLEDG[$occ]) and xf:isData(data($xNF_COLLAT_AVAILABLETOPLEDG[$occ])))
        then &lt;ns0:availableToPledge?&gt;{data($xNF_COLLAT_AVAILABLETOPLEDG[$occ])}&lt;/ns0:availableToPledge&gt;
        else ()
      }
      {if (data($xNF_COLLAT_ORIGINALCOLLATER[$occ]) and xf:isData(data($xNF_COLLAT_ORIGINALCOLLATER[$occ])))
        then &lt;ns0:originalCollateralValue?&gt;{data($xNF_COLLAT_ORIGINALCOLLATER[$occ])}&lt;/ns0:originalCollateralValue&gt;
        else ()
      }
      {if (data($xNF_COLLAT_COLLATERALINACTI[$occ]) and xf:isData(data($xNF_COLLAT_COLLATERALINACTI[$occ])))
        then &lt;ns0:collateralInactiveDate?&gt;{data($xNF_COLLAT_COLLATERALINACTI[$occ])}&lt;/ns0:collateralInactiveDate&gt;
        else ()
      }
      {if (data($xNF_COLLAT_USERFIELD6[$occ]))
        then &lt;ns0:userField6?&gt;{data($xNF_COLLAT_USERFIELD6[$occ])}&lt;/ns0:userField6&gt;
        else ()
      }
      {if (data($xNF_COLLAT_LASTMAINTENANCE[$occ]) and xf:isData(data($xNF_COLLAT_LASTMAINTENANCE[$occ])))
        then &lt;ns0:lastMaintenance?&gt;{data($xNF_COLLAT_LASTMAINTENANCE[$occ])}&lt;/ns0:lastMaintenance&gt;
        else ()
      }
      {if (data($xNF_COLLAT_MAXIMUMPLEDGEAMO[$occ]) and xf:isData(data($xNF_COLLAT_MAXIMUMPLEDGEAMO[$occ])))
        then &lt;ns0:maximumPledgeAmount?&gt;{data($xNF_COLLAT_MAXIMUMPLEDGEAMO[$occ])}&lt;/ns0:maximumPledgeAmount&gt;
        else ()
      }
      {if (data($xNF_COLLAT_AMOUNTPLECCYOFCO[$occ]) and xf:isData(data($xNF_COLLAT_AMOUNTPLECCYOFCO[$occ])))
        then &lt;ns0:amountPledgedCCYOfCollateral?&gt;{data($xNF_COLLAT_AMOUNTPLECCYOFCO[$occ])}&lt;/ns0:amountPledgedCCYOfCollateral&gt;
        else ()
      }
      {if (data($xNF_COLLAT_AMOUNTPLEDGEDCCY[$occ]) and xf:isData(data($xNF_COLLAT_AMOUNTPLEDGEDCCY[$occ])))
        then &lt;ns0:amountpledgedCCYOfAccount?&gt;{data($xNF_COLLAT_AMOUNTPLEDGEDCCY[$occ])}&lt;/ns0:amountpledgedCCYOfAccount&gt;
        else ()
      }
      {if (data($xNF_COLLAT_PLEDGEPURGEDAYS[$occ]) and xf:isData(data($xNF_COLLAT_PLEDGEPURGEDAYS[$occ])))
        then &lt;ns0:pledgePurgeDays?&gt;{data($xNF_COLLAT_PLEDGEPURGEDAYS[$occ])}&lt;/ns0:pledgePurgeDays&gt;
        else ()
      }
      {if (data($xNF_COLLAT_PLEDGEPURGEDATE[$occ]) and xf:isData(data($xNF_COLLAT_PLEDGEPURGEDATE[$occ])))
        then &lt;ns0:pledgePurgeDate?&gt;{data($xNF_COLLAT_PLEDGEPURGEDATE[$occ])}&lt;/ns0:pledgePurgeDate&gt;
        else ()
      }
      {getElementsForAssociatedAccountsList($parm)}
      &lt;ns0:collateralMortgage&gt;
        &lt;ns0:CollateralMortgage&gt;
          {if (data($xNF_COLLAM_LEGALREGNUM[$occ]))
            then &lt;ns0:legalRegistrationNumber?&gt;{data($xNF_COLLAM_LEGALREGNUM[$occ])}&lt;/ns0:legalRegistrationNumber&gt;
            else ()
          }
          {if (data($xNF_COLLAM_YEARBUILT[$occ]) and xf:isData(data($xNF_COLLAM_YEARBUILT[$occ])))
            then &lt;ns0:yearBuilt?&gt;{data($xNF_COLLAM_YEARBUILT[$occ])}&lt;/ns0:yearBuilt&gt;
            else ()
          }
          {if (data($xNF_COLLAM_YEARPURCHASED[$occ]) and xf:isData(data($xNF_COLLAM_YEARPURCHASED[$occ])))
            then &lt;ns0:yearPurchased?&gt;{data($xNF_COLLAM_YEARPURCHASED[$occ])}&lt;/ns0:yearPurchased&gt;
            else ()
          }
          {if (data($xNF_COLLAM_PURCHASEPRICE[$occ]) and xf:isData(data($xNF_COLLAM_PURCHASEPRICE[$occ])))
            then &lt;ns0:purchasePrice?&gt;{data($xNF_COLLAM_PURCHASEPRICE[$occ])}&lt;/ns0:purchasePrice&gt;
            else ()
          }
          {if (data($xNF_COLLAM_LOTSIZE[$occ]))
            then &lt;ns0:lotSize?&gt;{data($xNF_COLLAM_LOTSIZE[$occ])}&lt;/ns0:lotSize&gt;
            else ()
          }
          {if (data($xNF_COLLAM_SQUAREFEET[$occ]))
            then &lt;ns0:squareFeet?&gt;{data($xNF_COLLAM_SQUAREFEET[$occ])}&lt;/ns0:squareFeet&gt;
            else ()
          }
          {if (data($xNF_COLLAM_NUMBEROFRESIDENT[$occ]) and xf:isData(data($xNF_COLLAM_NUMBEROFRESIDENT[$occ])))
            then &lt;ns0:numberOfResidents?&gt;{data($xNF_COLLAM_NUMBEROFRESIDENT[$occ])}&lt;/ns0:numberOfResidents&gt;
            else ()
          }
          {if (data($xNF_COLLAM_LEASEEXPIRATIOND[$occ]) and xf:isData(data($xNF_COLLAM_LEASEEXPIRATIOND[$occ])))
            then &lt;ns0:leaseExpirationDate?&gt;{data($xNF_COLLAM_LEASEEXPIRATIOND[$occ])}&lt;/ns0:leaseExpirationDate&gt;
            else ()
          }
          {if (data($xNF_COLLAM_DATEINSPECTED[$occ]) and xf:isData(data($xNF_COLLAM_DATEINSPECTED[$occ])))
            then &lt;ns0:dateInspected?&gt;{data($xNF_COLLAM_DATEINSPECTED[$occ])}&lt;/ns0:dateInspected&gt;
            else ()
          }
          {if (data($xNF_COLLAM_SUBDIVISION[$occ]))
            then &lt;ns0:subdivision?&gt;{data($xNF_COLLAM_SUBDIVISION[$occ])}&lt;/ns0:subdivision&gt;
            else ()
          }
          {if (data($xNF_COLLAM_APPRAISEDVALUE[$occ]) and xf:isData(data($xNF_COLLAM_APPRAISEDVALUE[$occ])))
            then &lt;ns0:appraisedValue?&gt;{data($xNF_COLLAM_APPRAISEDVALUE[$occ])}&lt;/ns0:appraisedValue&gt;
            else ()
          }
          {if (data($xNF_COLLAM_APPRAISALDATE[$occ]) and xf:isData(data($xNF_COLLAM_APPRAISALDATE[$occ])))
            then &lt;ns0:appraisalDate?&gt;{data($xNF_COLLAM_APPRAISALDATE[$occ])}&lt;/ns0:appraisalDate&gt;
            else ()
          }
          {if (data($xNF_COLLAM_APPRAISERNAME[$occ]))
            then &lt;ns0:appraiserName?&gt;{data($xNF_COLLAM_APPRAISERNAME[$occ])}&lt;/ns0:appraiserName&gt;
            else ()
          }
          {if (data($xNF_COLLAM_MORTGAGESPAYABLE[$occ]))
            then &lt;ns0:mortgagesPayablePrior?&gt;{data($xNF_COLLAM_MORTGAGESPAYABLE[$occ])}&lt;/ns0:mortgagesPayablePrior&gt;
            else ()
          }
          {if (data($xNF_COLLAM_USERFIELD2[$occ]))
            then &lt;ns0:userField2?&gt;{data($xNF_COLLAM_USERFIELD2[$occ])}&lt;/ns0:userField2&gt;
            else ()
          }
          {if (data($xNF_COLLAM_USERFIELD3[$occ]))
            then &lt;ns0:userField3?&gt;{data($xNF_COLLAM_USERFIELD3[$occ])}&lt;/ns0:userField3&gt;
            else ()
          }
          {if (data($xNF_COLLAM_USERFIELD4[$occ]))
            then &lt;ns0:userField4?&gt;{data($xNF_COLLAM_USERFIELD4[$occ])}&lt;/ns0:userField4&gt;
            else ()
          }
          {if (data($xNF_COLLAM_USERFIELD5[$occ]) and xf:isData(data($xNF_COLLAM_USERFIELD5[$occ])))
            then &lt;ns0:userField5?&gt;{data($xNF_COLLAM_USERFIELD5[$occ])}&lt;/ns0:userField5&gt;
            else ()
          }
          {if (data($xNF_COLLAM_NEWADDUSEFIE2[$occ]))
            then &lt;ns0:newAdditionalUserField2?&gt;{data($xNF_COLLAM_NEWADDUSEFIE2[$occ])}&lt;/ns0:newAdditionalUserField2&gt;
            else ()
          }
          {if ($xNF_COLLAM_NEWADDUSEFIE3[$occ] &gt; 0)
            then
              &lt;ns0:newAdditionalUserField3?&gt;true&lt;/ns0:newAdditionalUserField3&gt;
            else
              &lt;ns0:newAdditionalUserField3?&gt;false&lt;/ns0:newAdditionalUserField3&gt;
          }
          {if (data($xNF_COLLAM_LEGALREGISTRATIO[$occ]) and xf:isData(data($xNF_COLLAM_LEGALREGISTRATIO[$occ])))
            then &lt;ns0:legalRegistrationDate?&gt;{data($xNF_COLLAM_LEGALREGISTRATIO[$occ])}&lt;/ns0:legalRegistrationDate&gt;
            else ()
          }
          {if (data($xNF_COLLAM_HOUSENUMBER[$occ]))
            then &lt;ns0:houseNumber?&gt;{data($xNF_COLLAM_HOUSENUMBER[$occ])}&lt;/ns0:houseNumber&gt;
            else ()
          }
          {if (data($xNF_COLLAM_STREET[$occ]))
            then &lt;ns0:street?&gt;{data($xNF_COLLAM_STREET[$occ])}&lt;/ns0:street&gt;
            else ()
          }
          {if (data($xNF_COLLAM_CITY[$occ]))
            then &lt;ns0:city?&gt;{data($xNF_COLLAM_CITY[$occ])}&lt;/ns0:city&gt;
            else ()
          }
          {if (data($xNF_COLLAM_POSTALCODE[$occ]))
            then &lt;ns0:postalCode?&gt;{data($xNF_COLLAM_POSTALCODE[$occ])}&lt;/ns0:postalCode&gt;
            else ()
          }
          {if (data($xNF_COLLAM_COUNTRY[$occ]))
            then &lt;ns0:country?&gt;{data($xNF_COLLAM_COUNTRY[$occ])}&lt;/ns0:country&gt;
            else ()
          }
          {if (data($xNF_COLLAM_DISTRICT[$occ]))
            then &lt;ns0:district?&gt;{data($xNF_COLLAM_DISTRICT[$occ])}&lt;/ns0:district&gt;
            else ()
          }
          {if (data($xNF_COLLAM_COUNTY[$occ]))
            then &lt;ns0:county?&gt;{data($xNF_COLLAM_COUNTY[$occ])}&lt;/ns0:county&gt;
            else ()
          }
          {if (data($xNF_COLLAM_USERFIELD1[$occ]))
            then &lt;ns0:userField1?&gt;{data($xNF_COLLAM_USERFIELD1[$occ])}&lt;/ns0:userField1&gt;
            else ()
          }
          &lt;ns0:newUsedIndicatori&gt;
            &lt;ns3:NewUsedType&gt;
              {if (data($xNF_NEWUT_NEWUSEDTYPE[$occ]) and xf:isData(data($xNF_NEWUT_NEWUSEDTYPE[$occ])))
                then &lt;ns3:newUsedType?&gt;{data($xNF_NEWUT_NEWUSEDTYPE[$occ])}&lt;/ns3:newUsedType&gt;
                else ()
              }
            &lt;/ns3:NewUsedType&gt;
          &lt;/ns0:newUsedIndicatori&gt;
          &lt;ns0:propertyType&gt;
            &lt;ns3:PropertyType&gt;
              {if (data($xNF_PROPET_PROPERTYTYPE[$occ]))
                then &lt;ns3:propertyType?&gt;{data($xNF_PROPET_PROPERTYTYPE[$occ])}&lt;/ns3:propertyType&gt;
                else ()
              }
            &lt;/ns3:PropertyType&gt;
          &lt;/ns0:propertyType&gt;
          &lt;ns0:ownerOccupiedCode&gt;
            &lt;ns3:OwnerOccupiedCode&gt;
              {if (data($xNF_OWNEOC_OWNEROCCUPIEDCOD[$occ]) and xf:isData(data($xNF_OWNEOC_OWNEROCCUPIEDCOD[$occ])))
                then &lt;ns3:ownerOccupiedCode?&gt;{data($xNF_OWNEOC_OWNEROCCUPIEDCOD[$occ])}&lt;/ns3:ownerOccupiedCode&gt;
                else ()
              }
            &lt;/ns3:OwnerOccupiedCode&gt;
          &lt;/ns0:ownerOccupiedCode&gt;
          &lt;ns0:tenure&gt;
            &lt;ns3:Tenure&gt;
              {if (data($xNF_TENURE_TENURE[$occ]) and xf:isData(data($xNF_TENURE_TENURE[$occ])))
                then &lt;ns3:tenure?&gt;{data($xNF_TENURE_TENURE[$occ])}&lt;/ns3:tenure&gt;
                else ()
              }
            &lt;/ns3:Tenure&gt;
          &lt;/ns0:tenure&gt;
          &lt;ns0:newAdditionalUserField1&gt;
            &lt;ns6:CollateralMortgageAddType&gt;
              {if (data($xNF_COLMAT_COLLATERALMORTGA[$occ]) and xf:isData(data($xNF_COLMAT_COLLATERALMORTGA[$occ])))
                  then &lt;ns6:collateralMortgageAddType?&gt;{data($xNF_COLMAT_COLLATERALMORTGA[$occ])}&lt;/ns6:collateralMortgageAddType&gt;
                  else ()
              }
            &lt;/ns6:CollateralMortgageAddType&gt;
          &lt;/ns0:newAdditionalUserField1&gt;
          &lt;ns0:realEstateUserField1&gt;
            &lt;ns3:CollateralMortgagePosition&gt;
              {if (data($xNF_COLLMP_COLLATERALMORTGA[$occ]) and xf:isData(data($xNF_COLLMP_COLLATERALMORTGA[$occ])))
                then &lt;ns3:collateralMortgagePosition?&gt;{data($xNF_COLLMP_COLLATERALMORTGA[$occ])}&lt;/ns3:collateralMortgagePosition&gt;
                else ()
              }
            &lt;/ns3:CollateralMortgagePosition&gt;
          &lt;/ns0:realEstateUserField1&gt;
        &lt;/ns0:CollateralMortgage&gt;
      &lt;/ns0:collateralMortgage&gt;
      &lt;ns0:collateralPledge&gt;
        &lt;ns0:CollateralCar&gt;
          {if (data($xNF_COLLAC_MODELYEAR[$occ]))
            then &lt;ns0:modelYear?&gt;{data($xNF_COLLAC_MODELYEAR[$occ])}&lt;/ns0:modelYear&gt;
            else ()
          }
          {if ($xNF_COLLAC_RENTALDEMOFLAG[$occ] &gt; 0)
            then
              &lt;ns0:rentalDemoFlag?&gt;true&lt;/ns0:rentalDemoFlag&gt;
            else
              &lt;ns0:rentalDemoFlag?&gt;false&lt;/ns0:rentalDemoFlag&gt;
          }
          {if (data($xNF_COLLAC_VEHICLELICENSEEX[$occ]) and xf:isData(data($xNF_COLLAC_VEHICLELICENSEEX[$occ])))
            then &lt;ns0:vehicleLicenseExpiryDate?&gt;{data($xNF_COLLAC_VEHICLELICENSEEX[$occ])}&lt;/ns0:vehicleLicenseExpiryDate&gt;
            else ()
          }
          {if (data($xNF_COLLAC_FIRSTREGISTRATIO[$occ]) and xf:isData(data($xNF_COLLAC_FIRSTREGISTRATIO[$occ])))
            then &lt;ns0:firstRegistrationDate?&gt;{data($xNF_COLLAC_FIRSTREGISTRATIO[$occ])}&lt;/ns0:firstRegistrationDate&gt;
            else ()
          }
          {if (data($xNF_COLLAC_SERIALNUMBER[$occ]))
            then &lt;ns0:serialNumber?&gt;{data($xNF_COLLAC_SERIALNUMBER[$occ])}&lt;/ns0:serialNumber&gt;
            else ()
          }
          {if (data($xNF_COLLAC_MOTORNUMBER[$occ]))
            then &lt;ns0:motorNumber?&gt;{data($xNF_COLLAC_MOTORNUMBER[$occ])}&lt;/ns0:motorNumber&gt;
            else ()
          }
          {if (data($xNF_COLLAC_PLATENUMBER[$occ]))
            then &lt;ns0:plateNumber?&gt;{data($xNF_COLLAC_PLATENUMBER[$occ])}&lt;/ns0:plateNumber&gt;
            else ()
          }
          &lt;ns0:conditionFlag&gt;
            &lt;ns3:ConditionFlag&gt;
              {if (data($xNF_CONDIF_CONDITIONFLAG[$occ]) and xf:isData(data($xNF_CONDIF_CONDITIONFLAG[$occ])))
                then &lt;ns3:conditionFlag?&gt;{data($xNF_CONDIF_CONDITIONFLAG[$occ])}&lt;/ns3:conditionFlag&gt;
                else ()
              }
            &lt;/ns3:ConditionFlag&gt;
          &lt;/ns0:conditionFlag&gt;
          &lt;ns0:licenseTypeCode&gt;
            &lt;ns3:LicenseTypeCode&gt;
              {if (data($xNF_LICETC_LICENSETYPECODE[$occ]))
                then &lt;ns3:licenseTypeCode?&gt;{data($xNF_LICETC_LICENSETYPECODE[$occ])}&lt;/ns3:licenseTypeCode&gt;
                else ()
              }
            &lt;/ns3:LicenseTypeCode&gt;
          &lt;/ns0:licenseTypeCode&gt;
          &lt;ns0:financeTypeCode&gt;
            &lt;ns3:FinanceTypeCode&gt;
              {if (data($xNF_FINATC_FINANCETYPECODE[$occ]))
                then &lt;ns3:financeTypeCode?&gt;{data($xNF_FINATC_FINANCETYPECODE[$occ])}&lt;/ns3:financeTypeCode&gt;
                else ()
              }
            &lt;/ns3:FinanceTypeCode&gt;
          &lt;/ns0:financeTypeCode&gt;
          &lt;ns0:registrationState&gt;
            &lt;ns3:RegistrationState&gt;
              (: zmiana NF_STATE_STATE -&gt; NF_BASES_STATE :)
              {if (data($xNF_BASES_STATE[$occ]))
                then &lt;ns3:registrationState?&gt;{data($xNF_BASES_STATE[$occ])}&lt;/ns3:registrationState&gt;
                else ()
              }
            &lt;/ns3:RegistrationState&gt;
          &lt;/ns0:registrationState&gt;
          &lt;ns0:makeCode&gt;
            &lt;ns3:MakeCode&gt;
              {if (data($xNF_MAKEC_MAKECODE[$occ]))
                then &lt;ns3:makeCode?&gt;{data($xNF_MAKEC_MAKECODE[$occ])}&lt;/ns3:makeCode&gt;
                else ()
              }
            &lt;/ns3:MakeCode&gt;
          &lt;/ns0:makeCode&gt;
          &lt;ns0:bodyCode&gt;
            &lt;ns3:BodyCode&gt;
              {if (data($xNF_BODYC_BODYCODE[$occ]))
                then &lt;ns3:bodyCode?&gt;{data($xNF_BODYC_BODYCODE[$occ])}&lt;/ns3:bodyCode&gt;
                else ()
              }
            &lt;/ns3:BodyCode&gt;
          &lt;/ns0:bodyCode&gt;
        &lt;/ns0:CollateralCar&gt;
      &lt;/ns0:collateralPledge&gt;
      &lt;ns0:collateralCode&gt;
        &lt;ns3:CollateralCode&gt;
          {if (data($xNF_COLLCO_COLLATERALCODE[$occ])) 
            then &lt;ns3:collateralCode?&gt;{data($xNF_COLLCO_COLLATERALCODE[$occ])}&lt;/ns3:collateralCode&gt;
            else ()
          }
        &lt;/ns3:CollateralCode&gt;
      &lt;/ns0:collateralCode&gt;
      &lt;ns0:currencyCode&gt;
        &lt;ns3:CurrencyCode&gt;
          {if (data($xNF_CURREC_CURRENCYCODE[$occ]))
            then &lt;ns3:currencyCode?&gt;{data($xNF_CURREC_CURRENCYCODE[$occ])}&lt;/ns3:currencyCode&gt;
            else ()
          }
        &lt;/ns3:CurrencyCode&gt;
      &lt;/ns0:currencyCode&gt;
      &lt;ns0:methodOfSecurityValuation&gt;
        &lt;ns3:MethodOfSecurityValuation&gt;
          {if (data($xNF_METOSV_METHODOFSECURITY[$occ]))
            then &lt;ns3:methodOfSecurityValuation?&gt;{data($xNF_METOSV_METHODOFSECURITY[$occ])}&lt;/ns3:methodOfSecurityValuation&gt;
            else ()
          }
        &lt;/ns3:MethodOfSecurityValuation&gt;
      &lt;/ns0:methodOfSecurityValuation&gt;
      &lt;ns0:personValuatingASecurity&gt;
        &lt;ns3:PersonValuatingASecurity&gt;
          {if (data($xNF_PERVAS_PERSONVALUATINGA[$occ]))
            then &lt;ns3:personValuatingASecurity?&gt;{data($xNF_PERVAS_PERSONVALUATINGA[$occ])}&lt;/ns3:personValuatingASecurity&gt;
            else ()
          }
        &lt;/ns3:PersonValuatingASecurity&gt;
      &lt;/ns0:personValuatingASecurity&gt;
      &lt;ns0:entity&gt;
        &lt;ns3:CollateralEntity&gt;
          {if (data($xNF_COLLAE_COLLATERALENTITY[$occ]))
            then &lt;ns3:collateralEntity?&gt;{data($xNF_COLLAE_COLLATERALENTITY[$occ])}&lt;/ns3:collateralEntity&gt;
            else ()
          }
        &lt;/ns3:CollateralEntity&gt;
      &lt;/ns0:entity&gt;
    &lt;/ns0:Collateral&gt;
  }
&lt;/ns5:collaterals&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse&gt;
  {getElementsForCollaterals($parm)}
&lt;/ns5:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>