<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace u =   "urn:be.services.dcl";
declare namespace u1 =  "urn:cif.entities.be.dcl";
declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

&lt;soap-env:Body&gt;
  {
  let $req := $body/u:invoke/u:customer/u1:Customer
  let $reqh := $header/u:header
  return
  
  &lt;fml:FML32&gt;
    &lt;fml:DC_MSHEAD_MSGID?&gt;{ data($reqh/u:msgHeader/u:msgId) }&lt;/fml:DC_MSHEAD_MSGID&gt;
    {if($req/u1:customerNumber)
      then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/u1:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA&gt;
      else ()
    }
    &lt;fml:DC_ENCODING&gt;PL_MZV&lt;/fml:DC_ENCODING&gt;
  &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>