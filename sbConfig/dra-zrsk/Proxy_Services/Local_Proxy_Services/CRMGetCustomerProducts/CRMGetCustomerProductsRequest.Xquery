<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:getCompanyIds($req as element(m:GetCustomerProductsRequest), $idx as xs:integer, $res as xs:string)as xs:string{
   let $len:=string-length(data($req/m:IdSpolki[$idx]))
   let $value:=concat(substring("00",1,2 - $len),data($req/m:IdSpolki[$idx]))
   return
     if ($len &gt; 0 and $len &lt;=2)
         then xf:getCompanyIds($req,$idx+1,concat($res,$value))
         else $res
};

declare function xf:mappGetCustomerProductsRequest($req as element(m:GetCustomerProductsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:NF_MSHEAD_USERID&gt;{ data($req/m:IdWewPrac) }&lt;/fml:NF_MSHEAD_USERID&gt;
					else ()
			}
			{
				if(count($req/m:IdSpolki)&gt;0)
					then &lt;fml:NF_MSHEAD_COMPANYID&gt;{ xf:getCompanyIds($req,1,"") }&lt;/fml:NF_MSHEAD_COMPANYID&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER&gt;{ data($req/m:NumerKlienta) }&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER&gt;
					else ()
			}
			{
				if($req/m:ProductAreaId)
					then &lt;fml:NF_PRODUA_CODEPRODUCTAREA&gt;{ data($req/m:ProductAreaId) }&lt;/fml:NF_PRODUA_CODEPRODUCTAREA&gt;
					else ()
			}
			(:{
				if($req/m:ProductCategoryId)
					then &lt;fml:CI_PRODUCT_CATEGORY_ID&gt;{ data($req/m:ProductCategoryId) }&lt;/fml:CI_PRODUCT_CATEGORY_ID&gt;
					else ()
			}
			{
				if($req/m:ProductAplication)
					then &lt;fml:CI_PRODUCT_APPLICATION&gt;{ data($req/m:ProductAplication) }&lt;/fml:CI_PRODUCT_APPLICATION&gt;
					else ()
			}:)
			(:{
				if($req/m:Opcja)
					then &lt;fml:NF_CTRL_OPTION&gt;{ data($req/m:Opcja) }&lt;/fml:NF_CTRL_OPTION&gt;
					else ()
			}:)
			{
				if($req/m:LiczbaOper)
					then &lt;fml:NF_PAGEC_PAGESIZE&gt;{ data($req/m:LiczbaOper) }&lt;/fml:NF_PAGEC_PAGESIZE&gt;
					else ()
			}
			{
				if($req/m:KluczNawigacyjny)
					then &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;{ data($req/m:KluczNawigacyjny) }&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;
					else  ()
			}
			{
				if($req/m:KodAkcji)
					then &lt;fml:NF_PAGEC_ACTIONCODE&gt;{data($req/m:KodAkcji)}&lt;/fml:NF_PAGEC_ACTIONCODE&gt;
					else  &lt;fml:NF_PAGEC_ACTIONCODE&gt;F&lt;/fml:NF_PAGEC_ACTIONCODE&gt;
			}
                       &lt;NF_CTRL_OPTION&gt;0&lt;/NF_CTRL_OPTION&gt;
                       &lt;NF_ACCOUN_ALTERNATIVEADDRE&gt;0&lt;/NF_ACCOUN_ALTERNATIVEADDRE&gt;
                       &lt;NF_PAGEC_REVERSEORDER&gt;0&lt;/NF_PAGEC_REVERSEORDER&gt;
                       &lt;NF_CTRL_ACTIVENONACTIVE&gt;0&lt;/NF_CTRL_ACTIVENONACTIVE&gt;
                       &lt;NF_CTRL_SHOWCURRBAL&gt;2&lt;/NF_CTRL_SHOWCURRBAL&gt;
                       &lt;NF_PRODUG_VISIBILITYCRM&gt;1&lt;/NF_PRODUG_VISIBILITYCRM&gt;

		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mappGetCustomerProductsRequest($body/m:GetCustomerProductsRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>