<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1  2009-05-29  PKL  TEET 37298 Usunięcie 'V' z wirtualnego numeru rachunku (NrAlternatywny)</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustomerProductDetailRequest($req as element(m:CRMGetCustomerProductDetailRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:ProductAreaId)
					then &lt;fml:CI_PRODUCT_AREA_ID&gt;{ data($req/m:ProductAreaId) }&lt;/fml:CI_PRODUCT_AREA_ID&gt;
					else ()
			}
			{
				if($req/m:ProductCategoryId)
					then &lt;fml:CI_PRODUCT_CATEGORY_ID&gt;{ data($req/m:ProductCategoryId) }&lt;/fml:CI_PRODUCT_CATEGORY_ID&gt;
					else ()
			}
			{
				if($req/m:NrRachunku)
					then &lt;fml:DC_NR_RACHUNKU&gt;{ data($req/m:NrRachunku) }&lt;/fml:DC_NR_RACHUNKU&gt;
					else ()
			}
			{
				if($req/m:NrAlternatywny)
					then (
                                                                                        if  (substring($req/m:NrAlternatywny,1,1) = 'V')
                                                                                           then &lt;fml:DC_NR_RACHUNKU_W_SYSTEMIE&gt;{substring(data($req/m:NrAlternatywny),2) }&lt;/fml:DC_NR_RACHUNKU_W_SYSTEMIE&gt;
                                                                                           else &lt;fml:DC_NR_RACHUNKU_W_SYSTEMIE&gt;{data($req/m:NrAlternatywny) }&lt;/fml:DC_NR_RACHUNKU_W_SYSTEMIE&gt;
                                                                                        )
                                                                                else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA&gt;{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCustomerProductDetailRequest($body/m:CRMGetCustomerProductDetailRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>