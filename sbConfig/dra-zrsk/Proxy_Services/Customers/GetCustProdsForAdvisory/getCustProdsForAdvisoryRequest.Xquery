<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetCustProdsForAdvisoryRequest($req as element(m:getCustProdsForAdvisoryRequest))
  as element(fml:FML32) {
    &lt;fml:FML32&gt;
      {if($req/m:CustomCustomernumber)
        then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER&gt;{ data($req/m:CustomCustomernumber) }&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER&gt;
        else ()
      }
      {if($req/m:PagecPagesize)
        then &lt;fml:NF_PAGEC_PAGESIZE&gt;{ data($req/m:PagecPagesize) }&lt;/fml:NF_PAGEC_PAGESIZE&gt;
        else ()
      }
      {if($req/m:PagecActioncode)
        then &lt;fml:NF_PAGEC_ACTIONCODE&gt;{ data($req/m:PagecActioncode) }&lt;/fml:NF_PAGEC_ACTIONCODE&gt;
        else ()
      }
      {if($req/m:PagecReverseorder)
        then &lt;fml:NF_PAGEC_REVERSEORDER&gt;{ data($req/m:PagecReverseorder) }&lt;/fml:NF_PAGEC_REVERSEORDER&gt;
        else ()
      }
      {if($req/m:PagecNavigationkeyvalu)
        then &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;{ data($req/m:PagecNavigationkeyvalu) }&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;
        else ()
      }
      &lt;fml:NF_MSHEAD_COMPANYID&gt;1&lt;/fml:NF_MSHEAD_COMPANYID&gt;
      &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;0&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;
      &lt;fml:NF_PRODUG_RECEIVER&gt;FU&lt;/fml:NF_PRODUG_RECEIVER&gt;
      &lt;fml:NF_CTRL_AREAS&gt;020310&lt;/fml:NF_CTRL_AREAS&gt;
      &lt;fml:NF_ACCOUR_RELATIONSHIP&gt;SOWJAFOWNJA1JA2JA3JA4JA5JAOJO1JO2JO3JO4JO5JOO&lt;/fml:NF_ACCOUR_RELATIONSHIP&gt;
      &lt;fml:NF_CTRL_ACTIVENONACTIVE&gt;1&lt;/fml:NF_CTRL_ACTIVENONACTIVE&gt;
    &lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetCustProdsForAdvisoryRequest($body/m:getCustProdsForAdvisoryRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>