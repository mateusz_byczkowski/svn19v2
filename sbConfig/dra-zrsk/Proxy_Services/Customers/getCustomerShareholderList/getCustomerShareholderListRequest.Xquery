<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace m="urn:be.services.dcl";
declare namespace m1="urn:cif.entities.be.dcl";
declare namespace xf="urn:be.services.dcl/mappings";

declare variable $header external;
declare variable $body external;

&lt;soap-env:Body&gt;
    &lt;FML32&gt;
        &lt;CI_ID_SPOLKI&gt;{data($header/m:header/m:msgHeader/m:companyId)}&lt;/CI_ID_SPOLKI&gt;
        &lt;DC_NUMER_KLIENTA&gt;{data($body/m:invoke/m:customer/m1:Customer/m1:customerNumber)}&lt;/DC_NUMER_KLIENTA&gt;
    &lt;/FML32&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>