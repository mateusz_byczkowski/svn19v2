<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapChangeCustomerResponse($fml as element(fml:FML32))
	as element(m:ChangeCustomerResponse) {
		&lt;m:ChangeCustomerResponse&gt;
			{

				let $DC_OPIS_BLEDU := $fml/fml:DC_OPIS_BLEDU
				for $it at $p in $fml/fml:DC_OPIS_BLEDU
				return
					&lt;m:ChangeCustomer&gt;
					{
						if($DC_OPIS_BLEDU[$p])
							then &lt;m:OpisBledu&gt;{ data($DC_OPIS_BLEDU[$p]) }&lt;/m:OpisBledu&gt;
						else ()
					}
					&lt;/m:ChangeCustomer&gt;
			}

		&lt;/m:ChangeCustomerResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapChangeCustomerResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>