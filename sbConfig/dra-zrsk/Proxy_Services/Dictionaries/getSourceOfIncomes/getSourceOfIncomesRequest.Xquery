<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns1="urn:be.services.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
      &lt;CI_MSHEAD_MSGID?&gt;{data($header/ns1:header/ns1:msgHeader/ns1:msgId)}&lt;/CI_MSHEAD_MSGID&gt;
      &lt;CI_ID_SLOWNIKA&gt;28&lt;/CI_ID_SLOWNIKA&gt;
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>