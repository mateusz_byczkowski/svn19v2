<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))&gt;0)
    then true()
    else false()
};

declare function getElementsForLoanFee($parm as element(fml:FML32)) as element()
{

&lt;ns4:loanFee&gt;
  {
    for $x at $occ in $parm/NF_LOANF_FEENUMBER
    return
    &lt;ns0:LoanFee&gt;
      {if (data($parm/NF_LOANF_FEEAMOUNT[$occ]) and xf:isData(data($parm/NF_LOANF_FEEAMOUNT[$occ])))
      	then &lt;ns0:feeAmount?&gt;{data($parm/NF_LOANF_FEEAMOUNT[$occ])}&lt;/ns0:feeAmount&gt;
      	else ()
      }
      {if (data($parm/NF_LOANF_FEEPERCENTAGE[$occ]) and xf:isData(data($parm/NF_LOANF_FEEPERCENTAGE[$occ])))
      	then &lt;ns0:feePercentage?&gt;{data($parm/NF_LOANF_FEEPERCENTAGE[$occ])}&lt;/ns0:feePercentage&gt;
      	else ()
      }
      {if (data($parm/NF_LOANF_FEENUMBER[$occ]))
      	then &lt;ns0:feeNumber?&gt;{data($parm/NF_LOANF_FEENUMBER[$occ])}&lt;/ns0:feeNumber&gt;
      	else ()
      }
      {if (data($parm/NF_LOANF_FEESTARTDATE[$occ]) and xf:isData(data($parm/NF_LOANF_FEESTARTDATE[$occ])))
      	then &lt;ns0:feeStartDate?&gt;{data($parm/NF_LOANF_FEESTARTDATE[$occ])}&lt;/ns0:feeStartDate&gt;
      	else ()
      }
      {if (data($parm/NF_LOANF_FEEPERIOD[$occ]))
      	then &lt;ns0:feePeriod?&gt;{data($parm/NF_LOANF_FEEPERIOD[$occ])}&lt;/ns0:feePeriod&gt;
      	else ()
      }
      {if (data($parm/NF_LOANF_FEEFREQUENCY[$occ]))
      	then &lt;ns0:feeFrequency?&gt;{data($parm/NF_LOANF_FEEFREQUENCY[$occ])}&lt;/ns0:feeFrequency&gt;
      	else ()
      }
      {if (data($parm/NF_LOANF_FEESPECIFICDAY[$occ]) and xf:isData(data($parm/NF_LOANF_FEESPECIFICDAY[$occ])))
      	then &lt;ns0:feeSpecificDay?&gt;{data($parm/NF_LOANF_FEESPECIFICDAY[$occ])}&lt;/ns0:feeSpecificDay&gt;
      	else ()
      }
      {if (data($parm/NF_LOANF_FEEDESCRIPTION[$occ]))
      	then &lt;ns0:feeDescription?&gt;{data($parm/NF_LOANF_FEEDESCRIPTION[$occ])}&lt;/ns0:feeDescription&gt;
      	else ()
      }
      {if (data($parm/NF_LOANF_FEEBKT[$occ]) and xf:isData(data($parm/NF_LOANF_FEEBKT[$occ])))
      	then &lt;ns0:feeBkt?&gt;{data($parm/NF_LOANF_FEEBKT[$occ])}&lt;/ns0:feeBkt&gt;
      	else ()
      }
      {if (data($parm/NF_LOANF_FEESTATUS[$occ]))
      	then &lt;ns0:feeStatus?&gt;{data($parm/NF_LOANF_FEESTATUS[$occ])}&lt;/ns0:feeStatus&gt;
      	else ()
      }
      &lt;ns0:feeCalculationCode&gt;
        &lt;ns2:FeeChargeCode&gt;
          {if (data($parm/NF_FEECC_FEECHARGECODE[$occ]))
          	then &lt;ns2:feeChargeCode?&gt;{data($parm/NF_FEECC_FEECHARGECODE[$occ])}&lt;/ns2:feeChargeCode&gt;
          	else ()
          }
        &lt;/ns2:FeeChargeCode&gt;
      &lt;/ns0:feeCalculationCode&gt;
      &lt;ns0:feeStartCode&gt;
        &lt;ns2:FeeCalculateMethod&gt;
          {if (data($parm/NF_FEECM_FEECALCULATEMETHO[$occ]))
          	then &lt;ns2:feeCalculateMethod?&gt;{data($parm/NF_FEECM_FEECALCULATEMETHO[$occ])}&lt;/ns2:feeCalculateMethod&gt;
          	else ()
          }
        &lt;/ns2:FeeCalculateMethod&gt;
      &lt;/ns0:feeStartCode&gt;
    &lt;/ns0:LoanFee&gt;
  }
&lt;/ns4:loanFee&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse&gt;
  {getElementsForLoanFee($parm)}
&lt;/ns4:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>