<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function xf:bool2short($bool as xs:boolean) as xs:string {
	if ($bool)
		then "1"
		else "0"
};


declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
,
&lt;NF_MSHEAD_UNITID?&gt;{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID&gt;
,
&lt;NF_MSHEAD_USERID?&gt;{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID&gt;
,
&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
,
&lt;NF_TRHEAD_TRANSID?&gt;{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID&gt;
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

&lt;NF_ACCOUN_ACCOUNTNUMBER?&gt;{data($parm/ns0:account/ns3:Account/ns3:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER&gt;
,
&lt;NF_PAGEC_ACTIONCODE?&gt;{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:actionCode)}&lt;/NF_PAGEC_ACTIONCODE&gt;
,
&lt;NF_PAGEC_PAGESIZE?&gt;{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:pageSize)}&lt;/NF_PAGEC_PAGESIZE&gt;
,
    if (data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:reverseOrder))
      then 	
        &lt;NF_PAGEC_REVERSEORDER?&gt;{xf:bool2short(data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:reverseOrder))}&lt;/NF_PAGEC_REVERSEORDER&gt;
    else()
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?&gt;{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI&gt;
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?&gt;{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns4:PageControl/ns4:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>