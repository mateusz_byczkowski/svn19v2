<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:schema>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;schema targetNamespace="http://www.bea.com/wli/sb/context"
        xmlns:mc="http://www.bea.com/wli/sb/context"
        xmlns="http://www.w3.org/2001/XMLSchema"
        elementFormDefault="qualified"
        attributeFormDefault="unqualified"&gt;
    &lt;!--============================================================== --&gt;

    &lt;!-- The context variable 'fault' is an instance of this element --&gt;
    &lt;element name="fault" type="mc:FaultType"/&gt;

    &lt;!-- The context variables 'inbound' and 'outbound' are instances of this element --&gt;
    &lt;element name="endpoint" type="mc:EndpointType"/&gt;

    &lt;!-- The three sub-elements within the 'inbound' and 'outbound' variables --&gt;
    &lt;element name="service" type="mc:ServiceType"/&gt;
    &lt;element name="transport" type="mc:TransportType"/&gt;
    &lt;element name="security" type="mc:SecurityType"/&gt;

    &lt;!-- The context variable 'attachments' is an instance of this element --&gt;
    &lt;element name="attachments" type="mc:AttachmentsType"/&gt;

    &lt;!-- Each attachment in the 'attachments' variable is represented by an instance of this element --&gt;
    &lt;element name="attachment" type="mc:AttachmentType"/&gt;

    &lt;!-- Element used to represent binary payloads and pass-by reference content --&gt;
    &lt;element name="binary-content" type="mc:BinaryContentType"/&gt;

    &lt;!-- =================================================================== --&gt;

    &lt;!-- The schema type for  --&gt;
    &lt;complexType name="AttachmentsType"&gt;
        &lt;sequence&gt;
            &lt;!-- the 'attachments' variable is just a series of attachment elements --&gt;
            &lt;element ref="mc:attachment" minOccurs="0" maxOccurs="unbounded"/&gt;
        &lt;/sequence&gt;
    &lt;/complexType&gt;
    
    &lt;complexType name="AttachmentType"&gt;
        &lt;all&gt;
            &lt;!-- Set of MIME headers associated with attachment --&gt;
            &lt;element name="Content-ID" type="string" minOccurs="0"/&gt;
            &lt;element name="Content-Type" type="string" minOccurs="0"/&gt;
            &lt;element name="Content-Transfer-Encoding" type="string" minOccurs="0"/&gt;
            &lt;element name="Content-Description" type="string" minOccurs="0"/&gt;
            &lt;element name="Content-Location" type="string" minOccurs="0"/&gt;
            &lt;element name="Content-Disposition" type="string" minOccurs="0"/&gt;

            &lt;!-- Contains the attachment content itself, either in-lined or as &lt;binary-content/&gt; --&gt;
            &lt;element name="body" type="anyType"/&gt;
        &lt;/all&gt;
    &lt;/complexType&gt;

    &lt;complexType name="BinaryContentType"&gt;
        &lt;!-- URI reference to the binary or pass-by-reference payload --&gt;
        &lt;attribute name="ref" type="anyURI" use="required"/&gt;
    &lt;/complexType&gt;

    &lt;!-- =================================================================== --&gt;

    &lt;complexType name="EndpointType"&gt;
        &lt;all&gt;
            &lt;!-- Sub-elements holding service, transport, and security details for the endpoint --&gt;
            &lt;element ref="mc:service" minOccurs="0" /&gt;
            &lt;element ref="mc:transport" minOccurs="0" /&gt;
            &lt;element ref="mc:security" minOccurs="0" /&gt;
        &lt;/all&gt;

        &lt;!-- Fully-qualified name of the service represented by this endpoint --&gt;
        &lt;attribute name="name" type="string" use="required"/&gt;
    &lt;/complexType&gt;

    &lt;!-- =================================================================== --&gt;

    &lt;complexType name="ServiceType"&gt;
        &lt;all&gt;
            &lt;!-- name of service provider --&gt;
            &lt;element name="providerName" type="string" minOccurs="0"/&gt;

            &lt;!-- the service operation being invoked --&gt;
            &lt;element name="operation" type="string" minOccurs="0"/&gt;
        &lt;/all&gt;
    &lt;/complexType&gt;

    &lt;!-- =================================================================== --&gt;

    &lt;complexType name="TransportType"&gt;
        &lt;all&gt;
            &lt;!-- URI of endpoint --&gt;
            &lt;element name="uri" type="anyURI" minOccurs="0" /&gt;

            &lt;!-- Transport-specific metadata for request and response (includes transport headers) --&gt;
            &lt;element name="request" type="anyType" minOccurs="0"/&gt;
            &lt;element name="response" type="anyType" minOccurs="0" /&gt;

            &lt;!-- Indicates one-way (request only) or bi-directional (request/response) communication --&gt;
            &lt;element name="mode" type="mc:ModeType" minOccurs="0" /&gt;

            &lt;!-- Specifies the quality of service --&gt;
            &lt;element name="qualityOfService" type="mc:QoSType" minOccurs="0" /&gt;

            &lt;!-- Retry values (outbound only) --&gt;
            &lt;element name="retryInterval" type="integer" minOccurs="0" /&gt;
            &lt;element name="retryCount" type="integer" minOccurs="0" /&gt;
        &lt;/all&gt;
    &lt;/complexType&gt;

    &lt;simpleType name="ModeType"&gt;
        &lt;restriction base="string"&gt;
            &lt;enumeration value="request"/&gt;
            &lt;enumeration value="request-response"/&gt;
        &lt;/restriction&gt;
    &lt;/simpleType&gt;

    &lt;simpleType name="QoSType"&gt;
        &lt;restriction base="string"&gt;
            &lt;enumeration value="best-effort"/&gt;
            &lt;enumeration value="exactly-once"/&gt;
        &lt;/restriction&gt;
    &lt;/simpleType&gt;

    &lt;!-- =================================================================== --&gt;

    &lt;complexType name="SecurityType"&gt;
        &lt;all&gt;
            &lt;!-- Transport-level client information (inbound only) --&gt;
            &lt;element name="transportClient" type="mc:SubjectType" minOccurs="0"/&gt;

            &lt;!-- Message-level client information (inbound only) --&gt;
            &lt;element name="messageLevelClient" type="mc:SubjectType" minOccurs="0"/&gt;
            
            &lt;!-- Boolean flag used to disable outbound WSS processing (outbound only) --&gt;
            &lt;element name="doOutboundWss" type="boolean" minOccurs="0"/&gt;
        &lt;/all&gt;
    &lt;/complexType&gt;

    &lt;complexType name="SubjectType"&gt;
        &lt;all&gt;
            &lt;!-- User name associated with this tranport- or message-level subject --&gt;
            &lt;element name="username" type="string"/&gt;
        &lt;/all&gt;
    &lt;/complexType&gt;

    &lt;!-- =================================================================== --&gt;

    &lt;complexType name="FaultType"&gt;
        &lt;all&gt;
            &lt;!-- A short string identifying the error (e.g. BEA38229) --&gt;
            &lt;element name="errorCode" type="string"/&gt;

            &lt;!-- Descriptive text explaining the reason for the error --&gt;
            &lt;element name="reason" type="string" minOccurs="0" /&gt;

            &lt;!-- Any additional details about the error  --&gt;
            &lt;element name="details" type="anyType" minOccurs="0" /&gt;

            &lt;!-- Information about where the error occured in the proxy --&gt;
            &lt;element name="location" type="mc:LocationType" minOccurs="0" /&gt;
        &lt;/all&gt;
    &lt;/complexType&gt;

    &lt;complexType name="LocationType"&gt;
        &lt;all&gt;
            &lt;!-- Name of the Pipeline/Branch/Route node where error occured --&gt;
            &lt;element name="node" type="string" minOccurs="0" /&gt;

            &lt;!-- Name of the Pipeline where error occured (if applicable) --&gt;
            &lt;element name="pipeline" type="string" minOccurs="0" /&gt;

            &lt;!-- Name of the Stage where error occured (if applicable) --&gt;
            &lt;element name="stage" type="string" minOccurs="0" /&gt;

            &lt;!-- Indicates if error occured from inside an error handler --&gt;
            &lt;element name="error-handler" type="boolean" minOccurs="0" /&gt;
        &lt;/all&gt;
    &lt;/complexType&gt;
    &lt;!-- Encapsulates any stack-traces that may be added to a fault &lt;details&gt; --&gt;
    &lt;element name="stack-trace" type="string"/&gt;
&lt;/schema&gt;</con:schema>
  <con:targetNamespace>http://www.bea.com/wli/sb/context</con:targetNamespace>
</con:schemaEntry>