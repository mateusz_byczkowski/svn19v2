declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn4 = "urn:ceke.entities.be.dcl";



declare function xf:mapgetCEKEConfigurationResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:packageCEKE>

			{
				let $E_PACKAGE_ID := $fml/fml:E_PACKAGE_ID
				let $E_PACKAGE_NAME := $fml/fml:E_PACKAGE_NAME
				let $E_PROFILE_ID := $fml/fml:E_PROFILE_ID
				let $E_PROFILE_NAME := $fml/fml:E_PROFILE_NAME
				let $E_PROFILE_DESCRIPTION := $fml/fml:E_PROFILE_DESCRIPTION
				let $E_HIGH_SECURITY_ALLOWED := $fml/fml:E_HIGH_SECURITY_ALLOWED
				let $E_CHNL_INTERNET := $fml/fml:E_CHNL_INTERNET
				let $E_CHNL_TELEFON := $fml/fml:E_CHNL_TELEFON
				let $E_CHNL_GSM := $fml/fml:E_CHNL_GSM
				let $E_CHNL_WAP := $fml/fml:E_CHNL_WAP
				let $E_MAX_SEQNO := $fml/fml:E_MAX_SEQNO

				let $id := distinct-values($fml/fml:E_PACKAGE_ID)
				let $name := distinct-values($fml/fml:E_PACKAGE_NAME)

				for $g in distinct-values($fml/fml:E_PACKAGE_ID)
				return
					<urn4:PackageCEKE>
					{
							<urn4:packageCEKE?>{$g}</urn4:packageCEKE>
					}
					{
						for $idvalue at $p in $id
						return
							if ($idvalue = $g) then
							<urn4:description>{$name[$p]}</urn4:description>
							else()
					}
					{	<urn4:profileCEKEList>
							{
								for $it at $p in $fml/fml:E_PROFILE_ID
								return
								<urn4:ProfileCEKE>
								{
									if (data($fml/fml:E_PACKAGE_ID[$p]) = $g) then
									(


										if ($E_PROFILE_ID[$p]) then
											<urn4:profileCEKE>{ data($E_PROFILE_ID[$p]) }</urn4:profileCEKE>
										else()
									)else ()
								}
								{
									if (data($fml/fml:E_PACKAGE_ID[$p]) = $g) then
									(
										if ($E_PROFILE_DESCRIPTION[$p]) then
											<urn4:description>{ data($E_PROFILE_NAME[$p]) }</urn4:description>
										else()
									) else ()
								}
								{
									if (data($fml/fml:E_PACKAGE_ID[$p]) = $g) then
									(
										if ($E_MAX_SEQNO[$p]) then
												<urn4:maxSeqNo>{ data($E_MAX_SEQNO[$p]) }</urn4:maxSeqNo>
											else()
									) else ()
								}
								{
									if (data($fml/fml:E_PACKAGE_ID[$p]) = $g) then
									(
										if($E_HIGH_SECURITY_ALLOWED) then (
													if(data($E_HIGH_SECURITY_ALLOWED[$p]) = "1") then
														<urn4:highAuthorizationAllowed?>true</urn4:highAuthorizationAllowed>
													else if (data($E_HIGH_SECURITY_ALLOWED[$p]) = "0") then (
														<urn4:highAuthorizationAllowed?>false</urn4:highAuthorizationAllowed>
													) else ()
										) else ()
									) else ()
								}

								{
									if (data($fml/fml:E_PACKAGE_ID[$p]) = $g) then
									(
										<urn4:customerChannelCEKEList>

													<urn4:CustomerChannelCEKE>
														<urn4:channelID>
															<urn3:ChannelCEKE>
																{
																	if(data($E_CHNL_INTERNET[$p]) = 1) then
																	(
																		<urn3:channelCEKE>i</urn3:channelCEKE>
																	) else ()
																}
																{
																	if(data($E_CHNL_TELEFON[$p]) = 1) then
																	(
																		<urn3:channelCEKE>t</urn3:channelCEKE>
																	) else ()
																}
																{
																	if(data($E_CHNL_GSM[$p]) = 1) then
																	(
																		<urn3:channelCEKE>g</urn3:channelCEKE>
																	) else ()
																}
																{
																	if(data($E_CHNL_WAP[$p]) = 1) then
																	(
																		<urn3:channelCEKE>w</urn3:channelCEKE>
																	) else ()
																}
															</urn3:ChannelCEKE>
														</urn4:channelID>
													</urn4:CustomerChannelCEKE>
										</urn4:customerChannelCEKEList>
								 ) else ()
								}
								</urn4:ProfileCEKE>
							}
						</urn4:profileCEKEList>
					}
					</urn4:PackageCEKE>

			}

			</urn:packageCEKE>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCEKEConfigurationResponse($body/fml:FML32) }
</soap-env:Body>
