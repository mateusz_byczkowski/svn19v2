declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";

declare function xf:mapgetCEKEConfigurationRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		<fml:FML32>
			{
			<fml:E_CUSTOMER_TYPE?>{data($req/urn:customer/urn1:Customer/urn1:customerType/urn2:CustomerType/urn2:customerType)}</fml:E_CUSTOMER_TYPE>
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCEKEConfigurationRequest($body/urn:invoke) }
</soap-env:Body>
