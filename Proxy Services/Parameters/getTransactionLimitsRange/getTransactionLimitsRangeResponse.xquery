declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:baseauxentities.be.dcl";
declare namespace urn2 = "urn:ceke.entities.be.dcl";
declare namespace urn3 = "urn:cekedict.dictionaries.be.dcl";

declare function xf:mapgetTransactionLimitsRangeResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:customerCEKEOut>
				<urn2:CustomerCEKE>



					{
					for $limitPIN at $p in $fml/fml:E_LIMIT return
					(
						if($p = 1)
						then (
						<urn2:limitPINMin>{ data($limitPIN)}</urn2:limitPINMin>
						)
						else if ($p = 2) then (
						<urn2:limitPINMax>{ data($limitPIN)}</urn2:limitPINMax>
						) else ()
					)
					}

					{
					for $limitTokenSMS at $q in $fml/fml:E_HI_LIMIT return
					(
						if($q = 1)
						then (
						<urn2:limitTokenSMSMin>{ data($limitTokenSMS)}</urn2:limitTokenSMSMin>
						)
						else if ($q = 2) then (
						<urn2:limitTokenSMSMax>{ data($limitTokenSMS)}</urn2:limitTokenSMSMax>
						) else ()
					)
					}
				</urn2:CustomerCEKE>
			</urn:customerCEKEOut>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetTransactionLimitsRangeResponse($body/fml:FML32) }
</soap-env:Body>
