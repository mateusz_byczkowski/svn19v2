declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:baseauxentities.be.dcl";
declare namespace urn2 = "urn:ceke.entities.be.dcl";
declare namespace urn3 = "urn:cekedict.dictionaries.be.dcl";

declare function xf:mapgetTransactionLimitsRangeRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		<fml:FML32>
			{
				<fml:E_PROFILE_ID?>{ data($req/urn:customerCEKE/urn2:CustomerCEKE/urn2:profileCEKE/urn2:ProfileCEKE/urn2:profileCEKE) }</fml:E_PROFILE_ID>
			}
			{
				if ((data($req/urn:isToken/urn1:BooleanHolder/urn1:value)) = "true" ) then
				<fml:E_TOKEN_AVAILABLE?>1</fml:E_TOKEN_AVAILABLE>
				else if ((data($req/urn:isToken/urn1:BooleanHolder/urn1:value)) = "false") then  (
				<fml:E_TOKEN_AVAILABLE>0</fml:E_TOKEN_AVAILABLE>
				) else ()
			}
			{
				if ((data($req/urn:isSMSCode/urn1:BooleanHolder/urn1:value)) = "true") then
				<fml:E_SMSCODE_AVAILABLE>1</fml:E_SMSCODE_AVAILABLE>
				else if ((data($req/urn:isSMSCode/urn1:BooleanHolder/urn1:value)) = "false") then (
				<fml:E_SMSCODE_AVAILABLE>0</fml:E_SMSCODE_AVAILABLE>
				) else ()
			}
			{
				for $channelCEKEArray in $req/urn:customerCEKE/urn2:CustomerCEKE/urn2:customerChannelCEKEList return
				(
					<fml:E_CHANNEL_TYPE>{ data($channelCEKEArray/urn2:CustomerChannelCEKE/urn2:channelID/urn3:ChannelCEKE/urn3:channelCEKE) }</fml:E_CHANNEL_TYPE>
				)
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetTransactionLimitsRangeRequest($body/urn:invoke) }
</soap-env:Body>
