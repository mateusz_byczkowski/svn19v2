declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fn = "http://www.w3.org/2005/02/xpath-functions/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";

declare function xf:mapgetCustomersByRelationsResponse($fml as element(fml:FML32), $rel as element(urn:customerRelationships))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:customerList>
			{

				let $relations := distinct-values(data($rel/urn1:CustomerRelationship/urn1:relationType/urn2:CustomerRelations/urn2:customerRelations))

				let $DC_NUMER_KLIENTA_REL := $fml/fml:DC_NUMER_KLIENTA_REL
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $DC_TYP_RELACJI := $fml/fml:DC_TYP_RELACJI
				let $CI_BUDOWA_PORTFELA := $fml/fml:CI_BUDOWA_PORTFELA
				let $CI_OBSLUGA_ZADAN := $fml/fml:CI_OBSLUGA_ZADAN
				let $CI_KORESP_SERYJNA := $fml/fml:CI_KORESP_SERYJNA
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA_REL
				return
					if(exists(index-of($relations, data($DC_TYP_RELACJI[$p])))) then
					(
						<urn1:Customer>
						{
							<urn1:customerNumber?>{ data($DC_NUMER_KLIENTA_REL[$p]) }</urn1:customerNumber>
						}
						{
							<urn1:customerRelationshipList>
								<urn1:CustomerRelationship> (:unbounded:)
									<urn1:relationType>
										<urn2:CustomerRelations>
											<urn2:customerRelations>{ data($DC_TYP_RELACJI[$p]) }</urn2:customerRelations>
										</urn2:CustomerRelations>
									</urn1:relationType>
								</urn1:CustomerRelationship>
							</urn1:customerRelationshipList>
						}
						{
							<urn1:customerPersonal>
								<urn1:CustomerPersonal>
									<urn1:firstName?>{ data($DC_IMIE[$p]) }</urn1:firstName>
									<urn1:lastName?>{ data($DC_NAZWISKO[$p]) }</urn1:lastName>
								</urn1:CustomerPersonal>
							</urn1:customerPersonal>
						}

						</urn1:Customer>
					) else ()
			}

			</urn:customerList>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomersByRelationsResponse($body/fml:FML32, $body/urn:customerRelationships) }
</soap-env:Body>
