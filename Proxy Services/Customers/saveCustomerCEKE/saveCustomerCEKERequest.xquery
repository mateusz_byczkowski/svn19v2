declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:ceke.entities.be.dcl";
declare namespace urn4 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn5 = "urn:accounts.entities.be.dcl";
declare namespace urn6 = "urn:entities.be.dcl";



declare function xf:mapsaveCustomerCEKERequest($req as element(urn:invoke))
	as element(fml:FML32) {
		<fml:FML32>

			{
				<fml:E_CUST_CITY?>{ data($req/urn:address/urn1:Address/urn1:city) }</fml:E_CUST_CITY>
			}

			{
				<fml:E_CUST_STREET?>{data($req/urn:address/urn1:Address/urn1:street)} {data($req/urn:address/urn1:Address/urn1:local)} {data($req/urn:address/urn1:Address/urn1:houseFlatNumber)}</fml:E_CUST_STREET>
			}
			{
				<fml:E_CUST_ZIPCODE?>{ data($req/urn:address/urn1:Address/urn1:zipCode) }</fml:E_CUST_ZIPCODE>
			}

			{
				<fml:E_TOKEN_SERIAL_NO?>{ data($req/urn:token/urn3:Token/urn3:serialNumber) }</fml:E_TOKEN_SERIAL_NO>
			}


			{
				<fml:B_NR_DOK?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:documentNumber) }</fml:B_NR_DOK>
			}
			{
				<fml:E_CUST_IDENTIF?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:additionalPassword) }</fml:E_CUST_IDENTIF>
			}
			{
				<fml:E_REP_NAME?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:repName) }</fml:E_REP_NAME>
			}
			{
				<fml:E_LIMIT?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:limitPIN) }</fml:E_LIMIT>

			}
			{
				<fml:E_HI_LIMIT?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:limitTokenSMS) }</fml:E_HI_LIMIT>
			}
			{
				<fml:E_TRN_LIMIT?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:limitTransaction) }</fml:E_TRN_LIMIT>
			}
			{
				<fml:E_CUST_REPORT_DATE?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:contractDate) }</fml:E_CUST_REPORT_DATE>
			}
			{
				<fml:B_RODZ_DOK?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:documentType/urn4:DocumentTypeCEKE/urn4:documentTypeCEKE) }</fml:B_RODZ_DOK>
			}
			{
				<fml:B_ID_ODDZ?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:registerBranchCEKE/urn2:BranchCode/urn2:branchCode) }</fml:B_ID_ODDZ>
			}
			{
				<fml:U_USER_NAME?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:cekeRegisterUser/urn6:User/urn6:userID) }</fml:U_USER_NAME>
			}
			{
				<fml:E_ADMIN_MICRO_BRANCH?>{ data($req/urn:unitId) }</fml:E_ADMIN_MICRO_BRANCH>
			}
			{
				<fml:E_PROFILE_ID?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:profileCEKE/urn3:ProfileCEKE/urn3:profileCEKE) }</fml:E_PROFILE_ID>
			}
			{
				<fml:E_SMS_TOKEN_NO?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:smsCode/urn3:SMSCode/urn3:smsCodeNumber) }</fml:E_SMS_TOKEN_NO>
			}
			{
				if($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:companyName and ($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:companyName != ""))
					then <fml:E_CUST_NAME>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:companyName) }</fml:E_CUST_NAME>
					else (
					<fml:E_CUST_NAME>{data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:customerPersonal/urn1:CustomerPersonal/urn1:firstName)} {data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:customerPersonal/urn1:CustomerPersonal/urn1:lastName)}</fml:E_CUST_NAME>
					)
			}
			{
				<fml:E_CIF_NUMBER?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:customerNumber) }</fml:E_CIF_NUMBER>
			}
			{
				<fml:E_CUST_TELNO?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:phoneNo) }</fml:E_CUST_TELNO>
			}
			{
				<fml:E_CUST_GSMNO?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:mobileNo) }</fml:E_CUST_GSMNO>
			}
			{
				<fml:E_CUST_EMAIL?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:email) }</fml:E_CUST_EMAIL>
			}
			{
				<fml:E_CUSTOMER_TYPE?>{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:customerType/urn2:CustomerType/urn2:customerType) }</fml:E_CUSTOMER_TYPE>
			}


			{
				for $customerChannelCEKE in $req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customerChannelCEKEList/urn3:CustomerChannelCEKE return
				(
					<fml:E_CHANNEL_TYPE>{data($customerChannelCEKE/urn3:channelID/urn4:ChannelCEKE/urn4:channelCEKE)}</fml:E_CHANNEL_TYPE>,
					<fml:E_PDATA>{data($customerChannelCEKE/urn3:pinMailerActivationCode)}</fml:E_PDATA>

				)
			}

			{
				for $ownAccountCEKE in $req/urn:customerCEKE/urn3:CustomerCEKE/urn3:ownAccountCEKEList/urn3:OwnAccountCEKE return
				(
					<fml:B_DL_NR_RACH>{data($ownAccountCEKE/urn3:account/urn5:Account/urn5:accountIBAN)}</fml:B_DL_NR_RACH>,
					<fml:E_SEQ_NO>{data($ownAccountCEKE/urn3:seqNum)}</fml:E_SEQ_NO>,

					if (data($ownAccountCEKE/urn3:flagFeeAccount)) then (
						if ((data($ownAccountCEKE/urn3:flagFeeAccount)) = "true") then
							<fml:E_OPTIONS>1</fml:E_OPTIONS>
						else if ((data($ownAccountCEKE/urn3:flagFeeAccount)) = "false") then (
							<fml:E_OPTIONS>0</fml:E_OPTIONS>
						) else ()
					) else ()

				)
			}

		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapsaveCustomerCEKERequest($body/urn:invoke)}
</soap-env:Body>
