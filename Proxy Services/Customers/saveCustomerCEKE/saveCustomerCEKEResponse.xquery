declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:ceke.entities.be.dcl";
declare namespace urn4 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn5 = "urn:accounts.entities.be.dcl";
declare namespace urn6 = "urn:entities.be.dcl";

declare function xf:mapsaveCustomerCEKEResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:customerCEKEOut>
				<urn3:CustomerCEKE>
				{
					<urn3:nik?>{ data($fml/fml:E_LOGIN_ID) }</urn3:nik>
				}
				{
					<urn3:contractVersion?>{data($fml/fml:E_CUST_REPORT_VERSION)}</urn3:contractVersion>
				}
				</urn3:CustomerCEKE>
			</urn:customerCEKEOut>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapsaveCustomerCEKEResponse($body/fml:FML32) }
</soap-env:Body>
