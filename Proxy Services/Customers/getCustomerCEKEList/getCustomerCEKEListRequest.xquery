declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";

declare function xf:mapgetCustomerCEKEListRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		<fml:FML32>
			{
				<fml:B_CIF_KLIENTA?>{ data($req/urn:customer/urn1:Customer/urn1:customerNumber) }</fml:B_CIF_KLIENTA>
			}
			{
				<fml:B_REGON?>{ data($req/urn:customer/urn1:Customer/urn1:customerFirm/urn1:CustomerFirm/urn1:regon) }</fml:B_REGON>
			}
			{
				<fml:B_NR_DOWODU?>{ data($req/urn:customer/urn1:Customer/urn1:customerPersonal/urn1:CustomerPersonal/urn1:identityCardNumber) }</fml:B_NR_DOWODU>
			}
			{
				<fml:B_N_PESEL?>{ data($req/urn:customer/urn1:Customer/urn1:customerPersonal/urn1:CustomerPersonal/urn1:pesel) }</fml:B_N_PESEL>
			}
			{
				<fml:B_NR_PASZPORTU?>{ data($req/urn:customer/urn1:Customer/urn1:customerPersonal/urn1:CustomerPersonal/urn1:passportNumber) }</fml:B_NR_PASZPORTU>
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomerCEKEListRequest($body/urn:invoke) }
</soap-env:Body>
