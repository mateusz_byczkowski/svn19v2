declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn3 = "urn:ceke.entities.be.dcl";

declare function xf:mapgetCustomerCEKEListResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:customerCEKEList>

				{

					let $E_LOGIN_ID := $fml/fml:E_LOGIN_ID
					let $B_NR_DOK := $fml/fml:B_NR_DOK
					let $B_RODZ_DOK := $fml/fml:B_RODZ_DOK
					let $E_CUST_STATUS := $fml/fml:E_CUST_STATUS
					let $E_PROFILE_ID := $fml/fml:E_PROFILE_ID

					for $it at $p in $fml/fml:E_TIME_STAMP
					return
						<urn3:CustomerCEKE>

						{
							if($E_LOGIN_ID[$p])
								then <urn3:nik>{ data($E_LOGIN_ID[$p]) }</urn3:nik>
							else ()
						}
						{
							if($B_NR_DOK[$p])
								then <urn3:documentNumber>{ data($B_NR_DOK[$p]) }</urn3:documentNumber>
							else ()
						}
						{
							if($B_RODZ_DOK[$p])
								then (
								<urn3:documentType>
									<urn2:DocumentTypeCEKE>
										<urn2:documentTypeCEKE>{ data($B_RODZ_DOK[$p]) }</urn2:documentTypeCEKE>
									</urn2:DocumentTypeCEKE>
								</urn3:documentType>
								)
							else ()
						}

						{
							if($E_CUST_STATUS[$p])
								then (
								<urn3:customerCEKEStatus>
									<urn2:CustomerStatusCEKE>
										<urn2:customerStatusCEKE>{ data($E_CUST_STATUS[$p]) }</urn2:customerStatusCEKE>
									</urn2:CustomerStatusCEKE>
								</urn3:customerCEKEStatus>
								)
							else ()
						}

						{
							if($E_PROFILE_ID[$p])
								then (
								<urn3:profileCEKE>
									<urn3:ProfileCEKE>
										<urn3:profileCEKE>{ data($E_PROFILE_ID[$p]) }</urn3:profileCEKE>
									</urn3:ProfileCEKE>
								</urn3:profileCEKE>
								)
							else ()
						}

						</urn3:CustomerCEKE>
				}

			</urn:customerCEKEList>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomerCEKEListResponse($body/fml:FML32) }
</soap-env:Body>
