declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn3 = "urn:dictionaries.be.dcl";

declare function xf:mapgetCustomerCEKEDetailsResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {

		<urn:invokeResponse>
			<urn:customerCEKEOut>
				<urn1:CustomerCEKE>
					<urn1:documentNumber?>{ data($fml/fml:B_NR_DOK) }</urn1:documentNumber>
					<urn1:additionalPassword?>{ data($fml/fml:E_CUST_IDENTIF) }</urn1:additionalPassword>
					<urn1:repName?>{ data($fml/fml:E_REP_NAME) }</urn1:repName>
					<urn1:limitPIN?>{ data($fml/fml:E_LIMIT) }</urn1:limitPIN>
					<urn1:limitTokenSMS?>{ data($fml/fml:E_HI_LIMIT) }</urn1:limitTokenSMS>
					<urn1:limitTransaction?>{ data($fml/fml:E_TRN_LIMIT) }</urn1:limitTransaction>
					<urn1:limitMBSME?>{ data($fml/fml:E_EX_LIMIT) }</urn1:limitMBSME>
					<urn1:limitMBSMETransaction?>{ data($fml/fml:E_TRN_EX_LIMIT) }</urn1:limitMBSMETransaction>
					<urn1:limitSMSPrepaid?>{ data($fml/fml:E_CHANNEL_LIMIT) }</urn1:limitSMSPrepaid>
					<urn1:contractDate?>{ data($fml/fml:E_CUST_REPORT_DATE) }</urn1:contractDate>
					<urn1:contractVersion?>{ data($fml/fml:E_CUST_REPORT_VERSION) }</urn1:contractVersion>
					<urn1:tokenDelegationType?>{ data($fml/fml:E_TOKEN_DELEGATION_TYPE)}</urn1:tokenDelegationType>
					<urn1:tokenDelegationCustomerNIK?>{ data($fml/fml:E_TOKEN_LOGIN_ID) }</urn1:tokenDelegationCustomerNIK>
					<urn1:tokenDelegationCustomerDocNo?>{ data($fml/fml:E_TOKEN_CUSTOMER_ID) }</urn1:tokenDelegationCustomerDocNo>
					<urn1:tokenDelegationCustomerName?>{ data($fml/fml:E_TOKEN_DELEGATION_NAME)}</urn1:tokenDelegationCustomerName>
					{
						if (data($fml/fml:E_ARKA24_ENABLED) = 0) then (
							<urn1:arka24?>false</urn1:arka24>
						) else if (data($fml/fml:E_ARKA24_ENABLED) = 1) then (
						<urn1:arka24?>true</urn1:arka24>
						) else ()
					}
					{
						if (data($fml/fml:E_ALERTY24_ENABLED) = 0) then (
							<urn1:alerty24?>false</urn1:alerty24>
						) else if (data($fml/fml:E_ALERTY24_ENABLED) = 1) then (
						<urn1:alerty24?>true</urn1:alerty24>
						) else ()
					}

					<urn1:prepaidTelNumber?>{ data($fml/fml:E_CUST_SMSNO)}</urn1:prepaidTelNumber>

					<urn1:customerChannelCEKEList>
						(: unbounded :)
						{
							for $it at $p in $fml/fml:E_CHANNEL_TYPE
							return
								<urn1:CustomerChannelCEKE>
									<urn1:lastCorrectLoginDate?>{ data($fml/fml:E_LAST_LOGIN[$p]) }</urn1:lastCorrectLoginDate>
									<urn1:lastFailureLoginDate?>{ data($fml/fml:E_LAST_BAD_LOGIN[$p]) }</urn1:lastFailureLoginDate>
									<urn1:channelID>
										<urn2:ChannelCEKE>
											<urn2:channelCEKE?>{ data($fml/fml:E_CHANNEL_TYPE[$p]) }</urn2:channelCEKE>
										</urn2:ChannelCEKE>
									</urn1:channelID>
									<urn1:customerChannelStatus>
										<urn2:CustomerChannelStatusCEKE>
											<urn2:customerChannelStatus?>{ data($fml/fml:E_STATUS[$p]) }</urn2:customerChannelStatus>
										</urn2:CustomerChannelStatusCEKE>
									</urn1:customerChannelStatus>
								</urn1:CustomerChannelCEKE>
						}
					</urn1:customerChannelCEKEList>

					<urn1:tokenList>
						(: unbounded :)
						<urn1:Token>
							<urn1:serialNumber?>{ data($fml/fml:E_TOKEN_SERIAL_NO) }</urn1:serialNumber>
							<urn1:tokenStatus>
								<urn2:TokenStatusCEKE>
									<urn2:tokenStatus?>{ data($fml/fml:E_TOKEN_STATUS) }</urn2:tokenStatus>
								</urn2:TokenStatusCEKE>
							</urn1:tokenStatus>
						</urn1:Token>
					</urn1:tokenList>

					<urn1:customerCEKEOperationGroupList>
						(: unbounded :)
						{
							for $it at $p in $fml/fml:E_TRN_GROUP_ID
							return
								<urn1:CustomerCEKEOperationGroup>
									<urn1:cekeOperationGroup?>{ data($fml/fml:E_TRN_GROUP_ID[$p]) }</urn1:cekeOperationGroup>
									<urn1:description?>{ data($fml/fml:E_TRN_GROUP_NAME[$p]) }</urn1:description>
									{
										if (data($fml/fml:E_TRN_GROUP_STATUS[$p]) = 0) then (
											<urn1:selected?>false</urn1:selected>
										) else if (data($fml/fml:E_TRN_GROUP_STATUS[$p]) = 1) then (
											<urn1:selected?>true</urn1:selected>
										) else ()
									}
								</urn1:CustomerCEKEOperationGroup>
						}
					</urn1:customerCEKEOperationGroupList>

					<urn1:documentType>
						<urn2:DocumentTypeCEKE>
							<urn2:documentTypeCEKE?>{ data($fml/fml:B_RODZ_DOK) }</urn2:documentTypeCEKE>
						</urn2:DocumentTypeCEKE>
					</urn1:documentType>

					<urn1:registerBranchCEKE>
						<urn3:BranchCode>
							<urn3:branchCode?>{ data($fml/fml:B_ID_ODDZ) }</urn3:branchCode>
						</urn3:BranchCode>
					</urn1:registerBranchCEKE>

					<urn1:customerCEKEStatus>
						<urn2:CustomerStatusCEKE>
							<urn2:customerStatusCEKE?>{ data($fml/fml:E_CUST_STATUS) }</urn2:customerStatusCEKE>
						</urn2:CustomerStatusCEKE>
					</urn1:customerCEKEStatus>

					<urn1:packageCEKE>
						<urn1:PackageCEKE>
							<urn1:packageCEKE?>{ data($fml/fml:E_PACKAGE_ID) }</urn1:packageCEKE>
							<urn1:description?>{ data($fml/fml:E_PACKAGE_NAME) }</urn1:description>
						</urn1:PackageCEKE>
					</urn1:packageCEKE>

					<urn1:profileCEKE>
						<urn1:ProfileCEKE>
							<urn1:profileCEKE?>{ data($fml/fml:E_PROFILE_ID) }</urn1:profileCEKE>
							<urn1:description?>{ data($fml/fml:E_PROFILE_NAME) }</urn1:description>
							{
								if (data($fml/fml:E_VIEW_DET_ALLOWED) = 0) then (
									<urn1:nfeViewDetailsAllowed>false</urn1:nfeViewDetailsAllowed>
								) else if (data($fml/fml:E_VIEW_DET_ALLOWED) = 1) then (
									<urn1:nfeViewDetailsAllowed>true</urn1:nfeViewDetailsAllowed>
								) else ()
							}
							{
								if (data($fml/fml:E_CLOSE_CEKE_ALLOWED) = 0) then (
									<urn1:nfeCloseCEKEAllowed>false</urn1:nfeCloseCEKEAllowed>
								) else if (data($fml/fml:E_CLOSE_CEKE_ALLOWED) = 1) then (
									<urn1:nfeCloseCEKEAllowed>true</urn1:nfeCloseCEKEAllowed>
								) else ()
							}
							{
								if (data($fml/fml:E_DETACH_PRODUCT_ALLOWED) = 0) then (
									<urn1:nfeDetachProductExamination>false</urn1:nfeDetachProductExamination>
								) else if (data($fml/fml:E_DETACH_PRODUCT_ALLOWED) = 1) then (
									<urn1:nfeDetachProductExamination>true</urn1:nfeDetachProductExamination>
								) else ()
							}
							{
								if (data($fml/fml:E_PRINT_CONTRACT) = 0) then (
									<urn1:nfePrintContact>false</urn1:nfePrintContact>
								) else if (data($fml/fml:E_PRINT_CONTRACT) = 1) then (
									<urn1:nfePrintContact>true</urn1:nfePrintContact>
								) else ()
							}
						</urn1:ProfileCEKE>
					</urn1:profileCEKE>

					<urn1:smsCode>
						<urn1:SMSCode>
							<urn1:smsCodeNumber?>{ data($fml/fml:E_SMS_TOKEN_NO) }</urn1:smsCodeNumber>
							<urn1:smsCodeStatus>
								<urn2:SmsCodeStatusCEKE>
									<urn2:smsCodeStatus?>{ data($fml/fml:E_SMS_TOKEN_STATUS) }</urn2:smsCodeStatus>
								</urn2:SmsCodeStatusCEKE>
							</urn1:smsCodeStatus>
						</urn1:SMSCode>
					</urn1:smsCode>

				</urn1:CustomerCEKE>
			</urn:customerCEKEOut>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomerCEKEDetailsResponse($body/fml:FML32) }
</soap-env:Body>
