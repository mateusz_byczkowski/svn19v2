declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:filtersandmessages.entities.be.dcl";


declare function xf:mapdelCustomerCEKEResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:response>
				<urn2:ResponseMessage>
					{
						if (data($fml/fml:E_RESULT) = "0") then (
							<urn2:result>false</urn2:result>
						)
						else if (data($fml/fml:E_RESULT) = "1") then (
							<urn2:result>true</urn2:result>
						) else ()
					}
				</urn2:ResponseMessage>
			</urn:response>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
	{xf:mapdelCustomerCEKEResponse($body/fml:FML32)}
</soap-env:Body>
