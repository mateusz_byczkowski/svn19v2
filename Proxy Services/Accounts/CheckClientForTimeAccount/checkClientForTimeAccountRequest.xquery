declare namespace urn="urn:dcl:services.alsb.datamodel";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap:Body) external;

declare function prepareCheckClientForTimeAccountRequest( $req as element(urn:entities.cif.Customer))
               as element(xpcml){
   let $cif := $req/urn:customerNumber

  return 
<xpcml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="xpcml.xsd" version="4.0">

<program name="program" path="/QSYS.LIB/AIBINTMRST.LIB/ATA0303.PGM"> 
<parameterList>
      <stringParm name="PSCIF" passDirection="in" length="10">{data($cif)}</stringParm>
      <zonedDecimalParm name="PZRETCODE" passDirection="inout" totalDigits="4" fractionDigits="0">2</zonedDecimalParm>
      <stringParm name="PNRETVAL" passDirection="inout" length="1">0</stringParm>
   </parameterList> 
</program>

</xpcml>
};

<soap:Body>
{prepareCheckClientForTimeAccountRequest($body/urn:invoke/urn:customer/urn:entities.cif.Customer)}
</soap:Body>
