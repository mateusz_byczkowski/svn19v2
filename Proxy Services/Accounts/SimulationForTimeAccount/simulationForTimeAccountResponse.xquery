declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:dcl:services.alsb.datamodel";

declare variable $body as element(soap:Body) external;

declare function prepareSimulationForTimeAccountResponse($res as element(xpcml))
               as element(urn:invokeResponse){
    let $resstruct:=$res/program/parameterList/structParm[1]

    let $balance:=$resstruct/zonedDecimalParm[1]
    let $interestCalculate:=$resstruct/zonedDecimalParm[2]
    let $interestLow:=$resstruct/zonedDecimalParm[3]
    let $interestForPay:=$resstruct/zonedDecimalParm[4]
    let $interestPayed:=$resstruct/zonedDecimalParm[5]
    let $amountForPay:=$resstruct/zonedDecimalParm[6]
    let $tax:=$resstruct/zonedDecimalParm[7]
    let $commission:=$resstruct/zonedDecimalParm[8]
    let $interestCapitalized:=$resstruct/zonedDecimalParm[9]
    let $taxPayed:=$resstruct/zonedDecimalParm[10]

    return
     <urn:invokeResponse>
        <urn:simulation>
            <urn:entities.filtersandmessages.SimulationForTimeAccount>
		<urn:balance>{data($balance)}</urn:balance>
		<urn:interestCalculate>{data($interestCalculate)}</urn:interestCalculate>
		<urn:interestLow>{data($interestLow)}</urn:interestLow>
		<urn:interestForPay>{data($interestForPay)}</urn:interestForPay>
		<urn:interestPayed>{data($interestPayed)}</urn:interestPayed>
		<urn:amountForPay>{data($amountForPay)}</urn:amountForPay>
		<urn:tax>{data($tax)}</urn:tax>
		<urn:commission>{data($commission)}</urn:commission>
		<urn:interestCapitalized>{data($interestCapitalized)}</urn:interestCapitalized>
		<urn:taxPayed>{data($taxPayed)}</urn:taxPayed>
            </urn:entities.filtersandmessages.SimulationForTimeAccount>
         </urn:simulation>       
     </urn:invokeResponse>
};


<soap:Body>
{prepareSimulationForTimeAccountResponse($body/xpcml)}
</soap:Body>

