declare namespace urn="urn:dcl:services.alsb.datamodel";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap:Body) external;

declare function prepareSimulationForTimeAccountRequest( $acc as element(urn:entities.accounts.Account), $date as element(urn:baseauxentities.DateHolder))
               as element(xpcml){
   let $accnbr:= $acc/urn:accountNumber
   let $dateval:= $date/urn:value

  return 
<xpcml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="xpcml.xsd" version="4.0">

<struct name="DSATAB30401">
      <zonedDecimalParm name="A304SALB" passDirection="inherit" totalDigits="15" fractionDigits="2"/>
      <zonedDecimalParm name="A304ODSN" passDirection="inherit" totalDigits="15" fractionDigits="2"/>
      <zonedDecimalParm name="A304ODSO" passDirection="inherit" totalDigits="15" fractionDigits="2"/>
      <zonedDecimalParm name="A304ODSDW" passDirection="inherit" totalDigits="15" fractionDigits="2"/>
      <zonedDecimalParm name="A304ODSW" passDirection="inherit" totalDigits="15" fractionDigits="2"/>
      <zonedDecimalParm name="A304KWWP" passDirection="inherit" totalDigits="15" fractionDigits="2"/>
      <zonedDecimalParm name="A304PODN" passDirection="inherit" totalDigits="15" fractionDigits="2"/>
      <zonedDecimalParm name="A304PROW" passDirection="inherit" totalDigits="15" fractionDigits="2"/>
      <zonedDecimalParm name="A304ODSSK" passDirection="inherit" totalDigits="15" fractionDigits="2"/>
      <zonedDecimalParm name="A304PODPBR" passDirection="inherit" totalDigits="15" fractionDigits="2"/>
</struct>
   
<program name="program" path="/QSYS.LIB/AIBINTMRST.LIB/ATA0304.PGM"> 
<parameterList>
      <zonedDecimalParm name="PZACCT" passDirection="in" totalDigits="10" fractionDigits="0">{data($accnbr)}</zonedDecimalParm>
      <packedDecimalParm name="PZWDAT" passDirection="in" totalDigits="6" fractionDigits="0">{data($dateval)}</packedDecimalParm>
      <zonedDecimalParm name="PZRETCODE" passDirection="inout" totalDigits="4" fractionDigits="0"/>
      <structParm name="DSRETVAL" passDirection="inout" struct="DSATAB30401"/>
   </parameterList> 
</program>

</xpcml>
};


<soap:Body>
{prepareSimulationForTimeAccountRequest($body/urn:invoke/urn:account/urn:entities.accounts.Account, $body/urn:invoke/urn:date/urn:baseauxentities.DateHolder)}
</soap:Body>
