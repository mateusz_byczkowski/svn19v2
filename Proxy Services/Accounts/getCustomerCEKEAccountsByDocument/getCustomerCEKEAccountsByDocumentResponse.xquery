declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn3 = "urn:dictionaries.be.dcl";
declare namespace urn4 = "urn:accounts.entities.be.dcl";
declare namespace urn5 = "urn:dictionaresrepo.dictionaries.be.dcl";

declare function xf:mapgetCustomerCEKEAccountsByDocumentResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:ownAccountCEKEList>
				{

					let $B_SYS := $fml/fml:B_SYS
					let $B_TYP_RACH := $fml/fml:B_TYP_RACH
					let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
					let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
					let $B_KOD_RACH := $fml/fml:B_KOD_RACH
					let $B_WALUTA := $fml/fml:B_WALUTA
					let $B_MIKROODDZIAL := $fml/fml:B_MIKROODDZIAL
					let $E_FEE_ALLOWED := $fml/fml:E_FEE_ALLOWED
					for $it at $p in $fml/fml:B_SYS
					return
						<urn1:OwnAccountCEKE>
						{
							if($B_SYS[$p])
								then
								(
									<urn1:sourceSystem>
										<urn3:System>
											<urn3:system>{ data($B_SYS[$p]) }</urn3:system>
										</urn3:System>
									</urn1:sourceSystem>
								)
							else ()
						}
						{
							<urn1:cekeAccountType?>{ data($B_TYP_RACH[$p]) }</urn1:cekeAccountType>
						}
						{
							if($E_FEE_ALLOWED[$p])
								then (

										if (data($E_FEE_ALLOWED[$p]) = "1") then
										(
											<urn1:feeAllowedAccount>true</urn1:feeAllowedAccount>
										) else if (data($E_FEE_ALLOWED[$p]) = "0") then
										(
											<urn1:feeAllowedAccount>false</urn1:feeAllowedAccount>
										) else()

								)
							else ()
						}
						{
							<urn1:account>
								<urn4:Account>
									{
										<urn4:accountNumber?>{ data($B_KOD_RACH[$p]) }</urn4:accountNumber>
									}
									{
										<urn4:accountDescription?>{ data($E_ACCOUNT_TYPE_NAME[$p]) }</urn4:accountDescription>
									}
									{
										<urn4:accountIBAN?>{ data($B_DL_NR_RACH[$p]) }</urn4:accountIBAN>
									}
									{
										if($B_WALUTA[$p])
											then
											(
											<urn4:currency>
												<urn3:CurrencyCode>
													<urn3:currencyCode>{ data($B_WALUTA[$p]) }</urn3:currencyCode>
												</urn3:CurrencyCode>
											</urn4:currency>
											)
										else ()
									}
									{
										if($B_MIKROODDZIAL[$p])
											then
											(
											<urn4:accountBranchNumber>
												<urn3:BranchCode>
													<urn3:branchCode>{ data($B_MIKROODDZIAL[$p]) }</urn3:branchCode>
												</urn3:BranchCode>
											</urn4:accountBranchNumber>
											)
										else ()
									}
								</urn4:Account>
							</urn1:account>

						}

						</urn1:OwnAccountCEKE>
				}
			</urn:ownAccountCEKEList>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomerCEKEAccountsByDocumentResponse($body/fml:FML32) }
</soap-env:Body>
