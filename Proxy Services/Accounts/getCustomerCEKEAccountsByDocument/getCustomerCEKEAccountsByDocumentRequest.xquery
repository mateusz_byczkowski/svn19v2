declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:cekedict.dictionaries.be.dcl";

declare function xf:mapgetCustomerCEKEAccountsByDocumentRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		<fml:FML32>
			{
				<fml:B_RODZ_DOK?>{ data($req/urn:customerCEKE/urn1:CustomerCEKE/urn1:documentType/urn2:DocumentTypeCEKE/urn2:documentTypeCEKE) }</fml:B_RODZ_DOK>
			}
			{
				<fml:B_NR_DOK?>{ data($req/urn:customerCEKE/urn1:CustomerCEKE/urn1:documentNumber) }</fml:B_NR_DOK>
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomerCEKEAccountsByDocumentRequest($body/urn:invoke) }
</soap-env:Body>
