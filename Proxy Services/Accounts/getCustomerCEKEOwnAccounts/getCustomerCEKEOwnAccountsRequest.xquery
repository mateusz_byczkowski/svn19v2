declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:accounts.entities.be.dcl";
declare namespace urn4 = "urn:dictionaresrepo.dictionaries.be.dcl";


declare function xf:mapgetCustomerCEKEOwnAccountRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		<fml:FML32>
			<fml:E_LOGIN_ID?>{ data($req/urn:customerCEKE/urn1:CustomerCEKE/urn1:nik) }</fml:E_LOGIN_ID>
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomerCEKEOwnAccountRequest($body/urn:invoke) }
</soap-env:Body>
