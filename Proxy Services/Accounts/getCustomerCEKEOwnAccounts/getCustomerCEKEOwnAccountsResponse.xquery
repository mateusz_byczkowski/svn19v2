declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:accounts.entities.be.dcl";
declare namespace urn4 = "urn:dictionaresrepo.dictionaries.be.dcl";

declare function xf:mapgetCustomerCEKEOwnAccountsResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:accounts>
				{
					(: unbounded :)
					for $it at $p in $fml/fml:B_SYS
					return
						<urn1:OwnAccountCEKE>
							<urn1:cekeAccountType?>{ data($fml/fml:B_TYP_RACH[$p]) }</urn1:cekeAccountType>
							{
								if($fml/fml:E_REGISTER_ACCOUNT[$p])
									then (

											if (data($fml/fml:E_REGISTER_ACCOUNT[$p]) = "1") then
											(
												<urn1:registerAccount>true</urn1:registerAccount>
											) else if (data($fml/fml:E_REGISTER_ACCOUNT[$p]) = "0") then
											(
												<urn1:registerAccount>false</urn1:registerAccount>
											) else()

									)
									else ()
							}
							{
								if($fml/fml:E_DR_CR_ACCOUNT[$p])
									then (

											if (data($fml/fml:E_DR_CR_ACCOUNT[$p]) = "1") then
											(
												<urn1:debitCreditAccount>true</urn1:debitCreditAccount>
											) else if (data($fml/fml:E_DR_CR_ACCOUNT[$p]) = "0") then
											(
												<urn1:debitCreditAccount>false</urn1:debitCreditAccount>
											) else()

									)
									else ()
							}
							{
								if($fml/fml:E_FEE_ALLOWED[$p])
									then (

											if (data($fml/fml:E_FEE_ALLOWED[$p]) = "1") then
											(
												<urn1:feeAllowedAccount>true</urn1:feeAllowedAccount>
											) else if (data($fml/fml:E_FEE_ALLOWED[$p]) = "0") then
											(
												<urn1:feeAllowedAccount>false</urn1:feeAllowedAccount>
											) else()

									)
									else ()
							}
							<urn1:seqNum>{ data($fml/fml:E_SEQ_NO) }</urn1:seqNum>
							{
								if($fml/fml:E_FEE_ENABLED[$p])
									then (
										if (data($fml/fml:E_FEE_ENABLED[$p]) = "1") then
											(
												<urn1:flagFeeAccount>true</urn1:flagFeeAccount>
											) else if (data($fml/fml:E_FEE_ENABLED[$p]) = "0") then
											(
												<urn1:flagFeeAccount>false</urn1:flagFeeAccount>
											) else()
									)
									else ()
							}
							<urn1:sourceSystem>
								<urn2:System>
									<urn2:system?>{data($fml/fml:B_SYS[$p])}</urn2:system>
								</urn2:System>
							</urn1:sourceSystem>
							<urn1:account>
								<urn3:Account>
									<urn3:accountNumber?>{ data($fml/fml:B_KOD_RACH[$p]) }</urn3:accountNumber>
									<urn3:accountName?>{ data($fml/fml:E_ACCOUNT_TYPE_NAME[$p]) }</urn3:accountName>
									<urn3:accountDescription?></urn3:accountDescription>
									{
										if ($fml/fml:B_WALUTA[$p]) then (
											<urn3:currency>
												<urn2:CurrencyCode>
													<urn2:currencyCode>{ data($fml/fml:B_WALUTA[$p]) }</urn2:currencyCode>
												</urn2:CurrencyCode>
											</urn3:currency>
										) else ()
									}
								</urn3:Account>
							</urn1:account>
						</urn1:OwnAccountCEKE>
				}
			</urn:accounts>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomerCEKEOwnAccountsResponse($body/fml:FML32) }
</soap-env:Body>
