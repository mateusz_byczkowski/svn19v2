declare namespace urn="urn:dcl:services.alsb.datamodel";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap:Body) external;

declare function prepareCheckHoldsForAccountRequest( $req as element(urn:entities.accounts.Account))
               as element(xpcml){
   let $acct:= $req/urn:accountNumber

  return 
<xpcml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="xpcml.xsd" version="4.0">

<program name="program" path="/QSYS.LIB/AIBINTMRST.LIB/ATA0300.PGM"> 
<parameterList>
      <stringParm name="CIFNBR" passDirection="inout" length="10">{data($acct)}</stringParm>
      <stringParm name="SYSTEM" passDirection="inout" length="1">I</stringParm>
      <stringParm name="RETCODE" passDirection="inout" length="1">2</stringParm>
   </parameterList> 
</program>

</xpcml>
};


<soap:Body>
{prepareCheckHoldsForAccountRequest($body/urn:invoke/urn:account/urn:entities.accounts.Account)}
</soap:Body>
