declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:dcl:services.alsb.datamodel";

declare variable $body as element(soap:Body) external;

declare function prepareCheckHoldsForAccountResponse($res as element(xpcml))
               as element(urn:invokeResponse){
    let $resstruct := $res/program/parameterList[1]

    let $rescode:=$resstruct/stringParm[3]
    return
     <urn:invokeResponse>
        <urn:response>
            <urn:entities.filtersandmessages.ResponseMessage>
{
	if (data($rescode) = 1) then 
               <urn:result>true</urn:result>
	else 
               <urn:result>false</urn:result>
}
            </urn:entities.filtersandmessages.ResponseMessage>
         </urn:response>       
     </urn:invokeResponse>
};


<soap:Body>
{prepareCheckHoldsForAccountResponse($body/xpcml)}
</soap:Body>

