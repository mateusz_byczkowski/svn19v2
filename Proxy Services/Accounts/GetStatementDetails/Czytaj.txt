Uwaga!

Jesli opis bledu zwracanego przez usluge zawiera polskie znaki, ALSB nie bedzie w stanie zbudowac poprawnej wiadomosci XML. Sytuacja ma miejsce na przyklad w przypadku podania blednego numeru wyciagu. Na chwile obecna polski znak zostal usuniety z tresci tego komunikatu. Problem zostal zgloszony.