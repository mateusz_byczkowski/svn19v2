<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn="urn:dcl:services.alsb.datamodel";

declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};


declare function xf:mapSaveSavingsAccountRequest($req as element(urn:entities.accounts.Account), $msghead as element(urn:msgHeader), $tranhead as element(urn:transHeader))
	as element(fml:FML32) {

	let $transId := $tranhead/urn:transId
	let $userId := $msghead/urn:userId
	let $unitId := $msghead/urn:unitId

	let $cif := $req/urn:customerNumber
	let $branch := $req/urn:accountBranchNumber/urn:dictionaries.BranchCode/urn:branchCode
(:	let $productNumber := $req/urn:accountUserField1  :)

	return
		&lt;fml:FML32>
			&lt;fml:DC_RODZAJ_RACHUNKU>SV&lt;/fml:DC_RODZAJ_RACHUNKU>
			&lt;fml:DC_TRN_ID>{ data($transId) }&lt;/fml:DC_TRN_ID>
			&lt;fml:DC_UZYTKOWNIK>{ data($userId) }&lt;/fml:DC_UZYTKOWNIK>
			&lt;fml:DC_ODDZIAL>{ data($unitId) }&lt;/fml:DC_ODDZIAL>

			&lt;fml:DC_NUMER_KLIENTA>{ data($cif)}&lt;/fml:DC_NUMER_KLIENTA>
			(:&lt;fml:DC_RELACJA>{ data()}&lt;/fml:DC_RELACJA>:)
			&lt;fml:DC_RELACJA>SOW&lt;/fml:DC_RELACJA>
			(:&lt;fml:DC_TYP_RELACJI>{ data()}&lt;/fml:DC_TYP_RELACJI>:)
			&lt;fml:DC_TYP_RELACJI>1&lt;/fml:DC_TYP_RELACJI>
			(:&lt;fml:DC_PROCENT_ZOB_PODATKOWYCH>{ data()}&lt;/fml:DC_PROCENT_ZOB_PODATKOWYCH>:)
			&lt;fml:DC_PROCENT_ZOB_PODATKOWYCH>100.0000&lt;/fml:DC_PROCENT_ZOB_PODATKOWYCH>
	(:&lt;fml:DC_NUMER_PRODUKTU>{ data($productNumber)}&lt;/fml:DC_NUMER_PRODUKTU>   :)
			&lt;fml:DC_NUMER_ODDZIALU>{ data($branch)}&lt;/fml:DC_NUMER_ODDZIALU>
			(:&lt;fml:DC_KOD_PRACOWNIKA>{ data()}&lt;/fml:DC_KOD_PRACOWNIKA>:)
			&lt;fml:DC_KOD_PRACOWNIKA>123&lt;/fml:DC_KOD_PRACOWNIKA>
			(:&lt;fml:DC_DATA_OTWARCIA_RACHUNKU>{ data()}&lt;/fml:DC_DATA_OTWARCIA_RACHUNKU>:)
			&lt;fml:DC_DATA_OTWARCIA_RACHUNKU>{xf:DateTime2CYMD('2007-12-12T12:!2:12')}&lt;/fml:DC_DATA_OTWARCIA_RACHUNKU>
			(:&lt;fml:DC_FLAGA_ZGODY>{ data()}&lt;/fml:DC_FLAGA_ZGODY>:)
			&lt;fml:DC_FLAGA_ZGODY>1&lt;/fml:DC_FLAGA_ZGODY>
			(:&lt;fml:DC_DATA_ZGODY>{ data()}&lt;/fml:DC_DATA_ZGODY>:)
			&lt;fml:DC_DATA_ZGODY>121207&lt;/fml:DC_DATA_ZGODY>
			(:&lt;fml:DC_OKRES_WAZNOSCI_ZGODY>{ data()}&lt;/fml:DC_OKRES_WAZNOSCI_ZGODY>:)
			&lt;fml:DC_OKRES_WAZNOSCI_ZGODY>12&lt;/fml:DC_OKRES_WAZNOSCI_ZGODY>
			(:&lt;fml:DC_FLAGA_RACHUNKU>{ data()}&lt;/fml:DC_FLAGA_RACHUNKU>:)
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


&lt;soap-env:Body>
{ xf:mapSaveSavingsAccountRequest($body/urn:invoke/urn:account/urn:entities.accounts.Account, $header/urn:header/urn:msgHeader, $header/urn:header/urn:transHeader) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>