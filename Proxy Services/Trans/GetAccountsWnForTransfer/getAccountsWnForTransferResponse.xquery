<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn="urn:dcl:services.alsb.datamodel";

declare function xf:CYMD2DateTime($indate as xsd:string ) as xsd:string{
     if (string-length($indate) =10) then 
       concat($indate,'T00:00:00')
     else ''
};


declare function xf:mapGetAccountsWnForTransferResponse($fml as element(fml:FML32))
	as element(urn:entities.accounts.Account)* {

    let $accountNumber:=$fml/fml:B_NR_RACH
    let $customerNumber:= $fml/fml:B_CIF_KLIENTA
    let $accountOpenDate:= $fml/fml:DC_DATA_OTWARCIA
   let $accountName:= $fml/fml:DC_PRODUKT
    let $electronicStatementFlag:= $fml/fml:B_CG_FLAG
    let $currentBalance:= $fml/fml:B_SALDO
    let $debitPercentage:= $fml/fml:B_OPROC1
    let $creditPrecentag:= $fml/fml:B_OPROC2
    let $statementForAccount:= $fml/fml:B_KOD_WYCIAGU
    let $accountStatus:= $fml/fml:B_STATUS
    let $accountBranchNumber:= $fml/fml:B_NR_ODDZ
    let $accountDescription:= $fml/fml:B_OPIS_RACH
    let $accountIBAN:= $fml/fml:B_NR_IBAN
    let $avaiableBalance:= $fml/fml:B_DOST_SRODKI
    let $saldoDostepneJutro:= $fml/fml:B_SRODKI_WL
    let $memoBalance:= $fml/fml:B_SALDO_CTX
    let $dateOfLastDeposit:= $fml/fml:B_DATA_OST_OPER
    let $serviceChargeCode:= $fml/fml:B_OPLATY
    let $interestDueDebit:= $fml/fml:B_KWOTA_MA
    let $interestDueCredit:= $fml/fml:B_KWOTA_WN
    let $interestDisposition:= $fml/fml:B_MISC_FLAG
    let $interestTransferAccount:= $fml/fml:B_TRAN_RACH
    let $savingsFlag:= $fml/fml:B_RODZAJ_RACH
    let $waluta:=$fml/fml:B_WALUTA

    for $it at $p in $fml/fml:B_NR_RACH
    return
            &lt;urn:entities.accounts.Account>
               &lt;urn:accountNumber>{data($accountNumber[$p])}&lt;/urn:accountNumber>
               &lt;urn:customerNumber>{data($customerNumber[$p])}&lt;/urn:customerNumber>
               &lt;urn:accountOpenDate>{xf:CYMD2DateTime(data($accountOpenDate[$p]))}&lt;/urn:accountOpenDate>
               &lt;urn:accountName>{data($accountName[$p])}&lt;/urn:accountName>
               &lt;urn:electronicStatementFlag>{data($electronicStatementFlag[$p])}&lt;/urn:electronicStatementFlag>
               &lt;urn:currentBalance>{data($currentBalance[$p])}&lt;/urn:currentBalance>
               &lt;urn:debitPercentage>{data($debitPercentage[$p])}&lt;/urn:debitPercentage>
               &lt;urn:creditPrecentag>{data($creditPrecentag[$p])}&lt;/urn:creditPrecentag>
               &lt;urn:statementForAccount>{data($statementForAccount[$p])}&lt;/urn:statementForAccount>
               &lt;urn:accountStatus>{data($accountStatus[$p])}&lt;/urn:accountStatus>
               &lt;urn:accountDescription>{data($accountDescription[$p])}&lt;/urn:accountDescription>
               &lt;urn:accountIBAN>{data($accountIBAN[$p])}&lt;/urn:accountIBAN>    
               &lt;urn:tranAccount>
                 &lt;urn:entities.accounts.TranAccount>
                   &lt;urn:avaiableBalance>{data($avaiableBalance[$p])}&lt;/urn:avaiableBalance>
                   &lt;urn:saldoDostepneJutro>{data($saldoDostepneJutro[$p])}&lt;/urn:saldoDostepneJutro>
                   &lt;urn:memoBalance>{data($memoBalance[$p])}&lt;/urn:memoBalance>
                   &lt;urn:dateOfLastDeposit>{xf:CYMD2DateTime(data($dateOfLastDeposit[$p]))}&lt;/urn:dateOfLastDeposit>
                   &lt;urn:serviceChargeCode>{data($serviceChargeCode[$p])}&lt;/urn:serviceChargeCode>
                   &lt;urn:interestDueDebit>{data($interestDueDebit[$p])}&lt;/urn:interestDueDebit>
                   &lt;urn:interestDueCredit>{data($interestDueCredit[$p])}&lt;/urn:interestDueCredit>
                   &lt;urn:interestDisposition>{data($interestDisposition[$p])}&lt;/urn:interestDisposition>
                   &lt;urn:interestTransferAccount>{data($interestTransferAccount[$p])}&lt;/urn:interestTransferAccount>
                   &lt;urn:savingsFlag>{data($savingsFlag[$p])}&lt;/urn:savingsFlag>
                   &lt;/urn:entities.accounts.TranAccount>
               &lt;/urn:tranAccount>
               &lt;urn:addressAccount>&lt;/urn:addressAccount>
               &lt;urn:accountType>
                  &lt;urn:dictionaries.AccountType>
                     &lt;urn:accountType>SV&lt;/urn:accountType>
                     &lt;urn:description>&lt;/urn:description>
                  &lt;/urn:dictionaries.AccountType>
               &lt;/urn:accountType>
               &lt;urn:currency>
                  &lt;urn:dictionaries.CurrencyCode>
                       &lt;urn:currencyCode>{data($waluta[$p])}&lt;/urn:currencyCode>
                       &lt;urn:description>&lt;/urn:description>
                  &lt;/urn:dictionaries.CurrencyCode>
               &lt;/urn:currency>
               &lt;urn:accountBranchNumber>
                     &lt;urn:dictionaries.BranchCode>
                          &lt;urn:branchCode>{data($accountBranchNumber[$p])}&lt;/urn:branchCode>
                     &lt;/urn:dictionaries.BranchCode>
               &lt;/urn:accountBranchNumber>    

             &lt;/urn:entities.accounts.Account>  

};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
     &lt;urn:invokeResponse>
        &lt;urn:accountsList>
{ xf:mapGetAccountsWnForTransferResponse($body/fml:FML32) }
        &lt;/urn:accountsList>       
     &lt;/urn:invokeResponse>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>