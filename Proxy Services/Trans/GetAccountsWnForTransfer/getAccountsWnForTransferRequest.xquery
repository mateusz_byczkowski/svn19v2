<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn="urn:dcl:services.alsb.datamodel";

declare function xf:mapGetAccountsWnForTransferRequest($req as element(urn:entities.cif.Customer))
	as element(fml:FML32) {

let $cif:= $req/urn:customerNumber

return
		&lt;fml:FML32>
			{
				if($cif)
					then &lt;fml:B_CIF_KLIENTA>{ data($cif) }&lt;/fml:B_CIF_KLIENTA>
					else ()
			}
			&lt;fml:B_NR_REF>0&lt;/fml:B_NR_REF>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
{ xf:mapGetAccountsWnForTransferRequest($body/urn:invoke/urn:customer/urn:entities.cif.Customer) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>