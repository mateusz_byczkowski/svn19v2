<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn="urn:dcl:services.alsb.datamodel";

declare function xf:mapDeleteAccountResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
    let $rescode:=$fml/fml:DC_OPIS_BLEDU

    return
	&lt;urn:invokeResponse>
            &lt;urn:response>
		&lt;urn:entities.filtersandmessages.ResponseMessage>
{
	if (data($rescode)) then 
		    &lt;urn:result>true&lt;/urn:result>
	else 
		    &lt;urn:result>false&lt;/urn:result>
}
                &lt;/urn:entities.filtersandmessages.ResponseMessage>
            &lt;/urn:response>       
	&lt;/urn:invokeResponse>


};

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body>
{ xf:mapDeleteAccountResponse($body/fml:FML32) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>