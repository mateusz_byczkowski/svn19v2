<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn="urn:dcl:services.alsb.datamodel";

declare function xf:mapDeleteAccountRequest($req as element(urn:entities.accounts.Account), $msghead as element(urn:msgHeader), $tranhead as element(urn:transHeader))
	as element(fml:FML32) {

	let $acct := $req/urn:accountNumber
	let $transId := $tranhead/urn:transId
	let $userId := $msghead/urn:userId
	let $unitId := $msghead/urn:unitId

	return
		&lt;fml:FML32>
			{
				if($acct)
					then &lt;fml:DC_NR_RACHUNKU>{ data($acct) }&lt;/fml:DC_NR_RACHUNKU>
					else ()
			}
			&lt;fml:DC_RODZAJ_RACHUNKU>SV&lt;/fml:DC_RODZAJ_RACHUNKU>
			&lt;fml:DC_TRN_ID>{ data($transId) }&lt;/fml:DC_TRN_ID>
			&lt;fml:DC_UZYTKOWNIK>SKP:{ data($userId) }&lt;/fml:DC_UZYTKOWNIK>
			&lt;fml:DC_ODDZIAL>{ data($unitId) }&lt;/fml:DC_ODDZIAL>
		&lt;/fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


&lt;soap-env:Body>
{ xf:mapDeleteAccountRequest($body/urn:invoke/urn:account/urn:entities.accounts.Account, $header/urn:header/urn:msgHeader, $header/urn:header/urn:transHeader) }
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>