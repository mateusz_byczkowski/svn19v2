declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn="urn:dcl:services.alsb.datamodel";

declare function mapGetCommitmentIDsResponse($fml as element(fml:FML32))
	as element(urn:dictionaries.operationsdictionary.CommitmentID)* {

				let $B_KOD_PS := $fml/fml:B_KOD_PS
				let $B_NAZWA := $fml/fml:B_NAZWA
				for $it at $p in $fml/fml:B_KOD_PS
				return
            <urn:dictionaries.operationsdictionary.CommitmentID>
		{
			if($B_KOD_PS[$p])
				then <urn:commitmentId>{ data($B_KOD_PS[$p]) }</urn:commitmentId>
				else ()
		}
		{
			if($B_NAZWA[$p])
				then <urn:description>{data($B_NAZWA[$p])}</urn:description>
				else ()
		}
    
             </urn:dictionaries.operationsdictionary.CommitmentID>  
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
     <urn:invokeResponse>
        <urn:commitmentIDsList>
{ mapGetCommitmentIDsResponse($body/fml:FML32) }
         </urn:commitmentIDsList>   
     </urn:invokeResponse>
</soap-env:Body>

