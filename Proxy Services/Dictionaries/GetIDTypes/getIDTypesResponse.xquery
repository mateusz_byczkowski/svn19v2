<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn="urn:dcl:services.alsb.datamodel";

declare function mapGetIDTypesResponse($fml as element(fml:FML32))
	as element(urn:dictionaries.operationsdictionary.IDType)* {

				let $B_KOD_PS := $fml/fml:B_KOD_PS
				let $B_NAZWA := $fml/fml:B_NAZWA
				for $it at $p in $fml/fml:B_KOD_PS
				return
            &lt;urn:dictionaries.operationsdictionary.IDType>
		{
			if($B_KOD_PS[$p])
				then &lt;urn:idType>{ data($B_KOD_PS[$p]) }&lt;/urn:idType>
				else ()
		}
		{
			if($B_NAZWA[$p])
				then &lt;urn:description>{data($B_NAZWA[$p])}&lt;/urn:description>
				else ()
		}
    
             &lt;/urn:dictionaries.operationsdictionary.IDType>  
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body>
     &lt;urn:invokeResponse>
        &lt;urn:idTypesList>
{ mapGetIDTypesResponse($body/fml:FML32) }
         &lt;/urn:idTypesList>   
     &lt;/urn:invokeResponse>
&lt;/soap-env:Body></con:xquery>
</con:xqueryEntry>