declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";


declare function xf:mapchkTokenSerialNumberResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:response>
				<urn2:ResponseMessage>
					{
						if ((data($fml/fml:E_TOKEN_STATUS) = "P") and (data($fml/fml:B_ID_ODDZ) > 0)) then
						<urn2:result>true</urn2:result>
						else (
						<urn2:result>false</urn2:result>
						)
					}
				</urn2:ResponseMessage>
			</urn:response>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
	{xf:mapchkTokenSerialNumberResponse($body/fml:FML32)}
</soap-env:Body>
