<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-10</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace dcap = "http://bzwbk.com/services/dcapi/";
declare namespace s = "http://bzwbk.com/services/dcapi/structs/";

declare function local:fault($faultString as xs:string, $detail as element()) as element(soap-env:Fault) {
	&lt;soap-env:Fault&gt;
		&lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
		&lt;faultstring&gt;{ $faultString }&lt;/faultstring&gt; 
		&lt;detail&gt;{ $detail }&lt;/detail&gt;
	&lt;/soap-env:Fault&gt;
};

declare function local:ValidationException($ve as element()) as element() {
	&lt;s:ValidationException&gt;
	{
		$ve/*
	}
	&lt;/s:ValidationException&gt;
};

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"&gt;
{
	typeswitch($body/dcap:addApplicationResponse/*)
	case $m as element(validationException)
		return local:fault("com.bzwbk.services.dcapi.structs.ValidationException", local:ValidationException($m))
	case element(internalApiException)
		return local:fault("InternalApiException", element s:InternalApiException {})
	case element(result)
		return $body/dcap:addApplicationResponse
	default
		return &lt;error&gt;unknown result type&lt;/error&gt;
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>