<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace dcap = "http://bzwbk.com/services/dcapi/";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";

declare function local:fields($fields as element()) as element()* {
	for $item in $fields/item
	return
		&lt;fields&gt;
		{ $item/* }
		&lt;/fields&gt;	
};

declare function local:customer($cust as element()) as element() {
	&lt;customers&gt;
	{
		local:fields($cust/fields),
		if(not($cust/obligations/@xsi:nil) or data($cust/obligations/@xsi:nil) != 'true') then
			for $obligations in $cust/obligations
			return
				&lt;obligations&gt;
				{ local:fields($obligations/fields) }
				&lt;/obligations&gt;
		else
			(),
		if(not($cust/collaterals/@xsi:nil) or data($cust/collaterals/@xsi:nil) != 'true') then
			for $collaterals in $cust/collaterals
			return
				&lt;collaterals&gt;
				{ local:fields($collaterals/fields) }
				&lt;/collaterals&gt;
		else
			()
	}
	&lt;/customers&gt;
};

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"&gt;
	&lt;dcap:addApplication xmlns:dcap="http://bzwbk.com/services/dcapi/"&gt;
		&lt;AddApplicationRequest&gt;
			&lt;application&gt;
			{
				let $application := $body/dcap:addApplication/application
				return
				(
					$application/(applicationType|source|vendor),
					local:fields($application/fields),
					local:customer($application/applicant),
					for $coapplicant in $application/coapplicants
					return
						local:customer($coapplicant)
				)
			}
			&lt;/application&gt;
			{
				$body/dcap:addApplication/(userId|source|applicationId|applicationPassword)
			}
		&lt;/AddApplicationRequest&gt;
	&lt;/dcap:addApplication&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>