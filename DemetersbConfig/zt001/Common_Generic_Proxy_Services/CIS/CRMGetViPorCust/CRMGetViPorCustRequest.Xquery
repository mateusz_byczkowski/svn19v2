<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetViPorCustRequest($req as element(m:CRMGetViPorCustRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA&gt;{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA&gt;
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA&gt;{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA&gt;
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI&gt;{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI&gt;
					else ()
			}
			{
				if($req/m:ZakresDanych)
					then &lt;fml:CI_ZAKRES_DANYCH&gt;{ data($req/m:ZakresDanych) }&lt;/fml:CI_ZAKRES_DANYCH&gt;
					else ()
			}
			{
				for $v in $req/m:StatusGiodo
				return
					&lt;fml:CI_STATUS_GIODO&gt;{ data($v) }&lt;/fml:CI_STATUS_GIODO&gt;
			}
			{
				for $v in $req/m:UdostepGrupa
				return
					&lt;fml:CI_UDOSTEP_GRUPA&gt;{ data($v) }&lt;/fml:CI_UDOSTEP_GRUPA&gt;
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON&gt;{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON&gt;
					else ()
			}
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL&gt;{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL&gt;
					else ()
			}
			{
				if($req/m:Nip)
					then &lt;fml:DC_NIP&gt;{ data($req/m:Nip) }&lt;/fml:DC_NIP&gt;
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then &lt;fml:DC_NUMER_PASZPORTU&gt;{ data($req/m:NumerPaszportu) }&lt;/fml:DC_NUMER_PASZPORTU&gt;
					else ()
			}
			{
				if($req/m:Nik)
					then &lt;fml:CI_NIK&gt;{ data($req/m:Nik) }&lt;/fml:CI_NIK&gt;
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO&gt;{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO&gt;
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE&gt;{ data($req/m:Imie) }&lt;/fml:DC_IMIE&gt;
					else ()
			}
			{
				if($req/m:NazwaPelna)
					then &lt;fml:CI_NAZWA_PELNA&gt;{ data($req/m:NazwaPelna) }&lt;/fml:CI_NAZWA_PELNA&gt;
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then &lt;fml:DC_MIASTO_DANE_PODST&gt;{ data($req/m:MiastoDanePodst) }&lt;/fml:DC_MIASTO_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then &lt;fml:DC_KOD_POCZTOWY_DANE_PODST&gt;{ data($req/m:KodPocztowyDanePodst) }&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then &lt;fml:DC_ULICA_DANE_PODST&gt;{ data($req/m:UlicaDanePodst) }&lt;/fml:DC_ULICA_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:KlasaObslugiAkt)
					then &lt;fml:CI_KLASA_OBSLUGI_AKT&gt;{ data($req/m:KlasaObslugiAkt) }&lt;/fml:CI_KLASA_OBSLUGI_AKT&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetViPorCustRequest($body/m:CRMGetViPorCustRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>