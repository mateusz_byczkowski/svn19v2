<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMIdnCustomersRequest($req as element(m:CRMIdnCustomersRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				for $v in $req/m:NrDowoduRegon
				return
					&lt;fml:DC_NR_DOWODU_REGON&gt;{ data($v) }&lt;/fml:DC_NR_DOWODU_REGON&gt;
			}
			{
				for $v in $req/m:NrPesel
				return
					&lt;fml:DC_NR_PESEL&gt;{ data($v) }&lt;/fml:DC_NR_PESEL&gt;
			}
			{
				for $v in $req/m:Nip
				return
					&lt;fml:DC_NIP&gt;{ data($v) }&lt;/fml:DC_NIP&gt;
			}
			{
				for $v in $req/m:NumerPaszportu
				return
					&lt;fml:DC_NUMER_PASZPORTU&gt;{ data($v) }&lt;/fml:DC_NUMER_PASZPORTU&gt;
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMIdnCustomersRequest($body/m:CRMIdnCustomersRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>