<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetDictValResponse($fml as element(fml:FML32))
	as element(m:CRMGetDictValResponse) {
		&lt;m:CRMGetDictValResponse&gt;
			{

				let $CI_ID_SLOWNIKA := $fml/fml:CI_ID_SLOWNIKA
				let $CI_WARTOSC_SLOWNIKA := $fml/fml:CI_WARTOSC_SLOWNIKA
				let $CI_OPIS_WARTOSCI_SLOWNIKA := $fml/fml:CI_OPIS_WARTOSCI_SLOWNIKA
				let $CI_WAGA_WARTOSCI_SLOWNIKA := $fml/fml:CI_WAGA_WARTOSCI_SLOWNIKA
				let $CI_DOMENA_RODZICA := $fml/fml:CI_DOMENA_RODZICA
				let $CI_ID_SLOWNIKA_RODZICA := $fml/fml:CI_ID_SLOWNIKA_RODZICA
				let $CI_DODATKOWA_WART_SLOWNIKA := $fml/fml:CI_DODATKOWA_WART_SLOWNIKA
				for $it at $p in $fml/fml:CI_ID_SLOWNIKA
				return
					&lt;m:CRMGetDictValWartoscSlownika&gt;
					{
						if($CI_ID_SLOWNIKA[$p])
							then &lt;m:IdSlownika&gt;{ data($CI_ID_SLOWNIKA[$p]) }&lt;/m:IdSlownika&gt;
						else ()
					}
					{
						if($CI_WARTOSC_SLOWNIKA[$p])
							then &lt;m:WartoscSlownika&gt;{ data($CI_WARTOSC_SLOWNIKA[$p]) }&lt;/m:WartoscSlownika&gt;
						else ()
					}
					{
						if($CI_OPIS_WARTOSCI_SLOWNIKA[$p])
							then &lt;m:OpisWartosciSlownika&gt;{ data($CI_OPIS_WARTOSCI_SLOWNIKA[$p]) }&lt;/m:OpisWartosciSlownika&gt;
						else ()
					}
					{
						if($CI_WAGA_WARTOSCI_SLOWNIKA[$p])
							then &lt;m:WagaWartosciSlownika&gt;{ data($CI_WAGA_WARTOSCI_SLOWNIKA[$p]) }&lt;/m:WagaWartosciSlownika&gt;
						else ()
					}
					{
						if($CI_DOMENA_RODZICA[$p])
							then &lt;m:DomenaRodzica&gt;{ data($CI_DOMENA_RODZICA[$p]) }&lt;/m:DomenaRodzica&gt;
						else ()
					}
					{
						if($CI_ID_SLOWNIKA_RODZICA[$p])
							then &lt;m:IdSlownikaRodzica&gt;{ data($CI_ID_SLOWNIKA_RODZICA[$p]) }&lt;/m:IdSlownikaRodzica&gt;
						else ()
					}
					{
						if($CI_DODATKOWA_WART_SLOWNIKA[$p])
							then &lt;m:DodatkowaWartSlownika&gt;{ data($CI_DODATKOWA_WART_SLOWNIKA[$p]) }&lt;/m:DodatkowaWartSlownika&gt;
						else ()
					}
					&lt;/m:CRMGetDictValWartoscSlownika&gt;
			}

		&lt;/m:CRMGetDictValResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetDictValResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>