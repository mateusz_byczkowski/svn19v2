<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCusGiodoRequest($req as element(m:CRMGetCusGiodoRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				for $v in $req/m:NumerKlienta
				return
					&lt;fml:DC_NUMER_KLIENTA&gt;{ data($v) }&lt;/fml:DC_NUMER_KLIENTA&gt;
			}
			{
				for $v in $req/m:IdSpolki
				return
					&lt;fml:CI_ID_SPOLKI&gt;{ data($v) }&lt;/fml:CI_ID_SPOLKI&gt;
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCusGiodoRequest($body/m:CRMGetCusGiodoRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>