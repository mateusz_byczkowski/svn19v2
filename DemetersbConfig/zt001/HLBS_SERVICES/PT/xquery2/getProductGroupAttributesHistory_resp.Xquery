<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductGroupAttributesHistory_resp/";
declare namespace urn= "urn:be.services.dcl";
declare namespace urn1 = "urn:productstree.entities.be.dcl";

declare function xf:getProductGroupAttributesHistory_resp($fml as element())
    as element() {
	&lt;urn:invokeResponse&gt;
             &lt;urn:productGroupAttributeHistory&gt;
{
                for $i in 1 to count($fml/PT_ID_GROUP_ATTRIBUTES)
                return
                    &lt;urn1:ProdGroupAttributesHist&gt;
                            &lt;urn1:idProductGroup&gt;{ data($fml/PT_ID_GROUP[$i]) }&lt;/urn1:idProductGroup&gt;
                            &lt;urn1:thirdProductFeature&gt;{ data($fml/PT_THIRD_PRODUCT_FEATURE[$i]) }&lt;/urn1:thirdProductFeature&gt;                                    
                            &lt;urn1:fifthProductFeature&gt;{ data($fml/PT_FIFTH_PRODUCT_FEATURE[$i]) }&lt;/urn1:fifthProductFeature&gt;
                            &lt;urn1:codeProduct&gt;{ data($fml/PT_CODE_PRODUCT[$i]) }&lt;/urn1:codeProduct&gt;                                    
                            &lt;urn1:firstProductFeature&gt;{ data($fml/PT_FIRST_PRODUCT_FEATURE[$i]) }&lt;/urn1:firstProductFeature&gt;                                    
                            &lt;urn1:userChangeSKP&gt;{ data($fml/PT_USER_CHANGE_SKP[$i]) }&lt;/urn1:userChangeSKP&gt;                                    
                            &lt;urn1:fourthProductFeature&gt;{ data($fml/PT_FOURTH_PRODUCT_FEATURE[$i]) }&lt;/urn1:fourthProductFeature&gt;
                            &lt;urn1:secondProductFeature&gt;{ data($fml/PT_SECOND_PRODUCT_FEATURE[$i]) }&lt;/urn1:secondProductFeature&gt;                                    
                            &lt;urn1:dateChange&gt;{ data($fml/PT_DATE_CHANGE[$i]) }&lt;/urn1:dateChange&gt;         
                    &lt;/urn1:ProdGroupAttributesHist&gt;
            }
            &lt;/urn:productGroupAttributeHistory&gt;
    &lt;/urn:invokeResponse&gt;
};

declare variable $fml as element() external;

xf:getProductGroupAttributesHistory_resp($fml)</con:xquery>
</con:xqueryEntry>