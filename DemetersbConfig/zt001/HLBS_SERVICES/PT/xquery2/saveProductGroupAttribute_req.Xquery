<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProductGroupAttribute_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:saveProductGroupAttribute_req ($entity as element(dcl:AttributesProductGroup)) as element(FML32) {
&lt;FML32&gt;
     &lt;PT_CODE_PRODUCT&gt;{ data( $entity/dcl:codeProduct ) }&lt;/PT_CODE_PRODUCT&gt;
     &lt;PT_FIRST_PRODUCT_FEATURE&gt;{ data( $entity/dcl:firstProductFeature ) }&lt;/PT_FIRST_PRODUCT_FEATURE&gt;
     &lt;PT_SECOND_PRODUCT_FEATURE&gt;{ data( $entity/dcl:secondProductFeature ) }&lt;/PT_SECOND_PRODUCT_FEATURE&gt;
     &lt;PT_THIRD_PRODUCT_FEATURE&gt;{ data( $entity/dcl:thirdProductFeature ) }&lt;/PT_THIRD_PRODUCT_FEATURE&gt;
     &lt;PT_FOURTH_PRODUCT_FEATURE&gt;{ data( $entity/dcl:fourthProductFeature ) }&lt;/PT_FOURTH_PRODUCT_FEATURE&gt;     
     &lt;PT_FIFTH_PRODUCT_FEATURE&gt;{ data( $entity/dcl:fifthProductFeature ) }&lt;/PT_FIFTH_PRODUCT_FEATURE&gt;     
     &lt;PT_ID_GROUP&gt;{ data( $entity/dcl:idProductGroup ) }&lt;/PT_ID_GROUP&gt;     
     &lt;PT_ID_GROUP_ATTRIBUTES&gt;{ data( $entity/dcl:idAttributesProductGroup ) }&lt;/PT_ID_GROUP_ATTRIBUTES&gt;
     &lt;PT_USER_CHANGE_SKP&gt;{ data( $entity/dcl:userChangeSKP ) }&lt;/PT_USER_CHANGE_SKP&gt;
     &lt;PT_DATE_CHANGE&gt;{ data( $entity/dcl:dataChange ) }&lt;/PT_DATE_CHANGE&gt;
&lt;/FML32&gt;
};
declare variable $entity as element(dcl:AttributesProductGroup) external;
xf:saveProductGroupAttribute_req($entity)</con:xquery>
</con:xqueryEntry>