<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2011-07-12</con:description>
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns3="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)&gt;5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };

declare function chgDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{

    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)

    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 

};

declare function getElementsForProductRdsCodeList($idDefinition as xs:string, $paIdProductDef2 as element()*, $paRDSCode as element()*, $paCodeProductSys as element()*) as element()
{

&lt;ns3:productRdsCodeList&gt;
  {
    for $x at $occ in $paRDSCode
    return if ($idDefinition = data($paIdProductDef2[$occ]))
    then
    &lt;ns3:ProductRdsCode&gt;
      &lt;ns3:rdsCode?&gt;{data($x)}&lt;/ns3:rdsCode&gt;
      &lt;ns3:codeProductSystem?&gt;{data($paCodeProductSys[$occ])}&lt;/ns3:codeProductSystem&gt;
    &lt;/ns3:ProductRdsCode&gt;
    else
    	()
  }
&lt;/ns3:productRdsCodeList&gt;
};
declare function getElementsForProductDefinitionList($parm as element(fml:FML32)) as element()
{

&lt;ns0:productDefinitionList&gt;
  {
    let $paIdProductDef := $parm/NF_PRODUD_IDPRODUCTDEFINIT
    let $paRDSCode := $parm/NF_PRODRC_RDSCODE
    let $paCodeProductSys := $parm/NF_PRODRC_CODEPRODUCTSYSTE
    let $paIdProductDef2 := $parm/NF_PRODRC_IDPRODUCTDEFINIT

    let $cdPrDef := $parm/NF_PRODUD_CODEPRODUCTDEFIN
    let $polishPrName := $parm/NF_PRODUD_POLISHPRODUCTNAM
    let $engPrName := $parm/NF_PRODUD_ENGLISHPRODUCTNA
    let $sortOrd := $parm/NF_PRODUD_SORTORDER
    let $idProductGroup := $parm/NF_PRODUD_IDPRODUCTGROUP
    let $packageOnlyFlag := $parm/NF_PRODUD_PACKAGEONLYFLAG
    let $sorceProductCode := $parm/NF_PRODUD_SORCEPRODUCTCODE
    let $startDate := $parm/NF_PRODUD_STARTDATE
    let $endDate := $parm/NF_PRODUD_ENDDATE

    for $x at $occ in $paIdProductDef
    return
    &lt;ns3:ProductDefinition&gt;
      &lt;ns3:codeProductDefinition?&gt;{data($cdPrDef[$occ])}&lt;/ns3:codeProductDefinition&gt;
      &lt;ns3:polishProductName?&gt;{data($polishPrName[$occ])}&lt;/ns3:polishProductName&gt;
      &lt;ns3:englishProductName?&gt;{data($engPrName[$occ])}&lt;/ns3:englishProductName&gt;
      &lt;ns3:sortOrder?&gt;{data($sortOrd[$occ])}&lt;/ns3:sortOrder&gt;
      &lt;ns3:idProductGroup?&gt;{data($idProductGroup[$occ])}&lt;/ns3:idProductGroup&gt;
      &lt;ns3:packageOnlyFlag?&gt;{sourceValue2Boolean(data($packageOnlyFlag[$occ]),"1")}&lt;/ns3:packageOnlyFlag&gt;
      &lt;ns3:sorceProductCode?&gt;{data($sorceProductCode[$occ])}&lt;/ns3:sorceProductCode&gt;
      { insertDate(data($startDate[$occ]),"yyyy-MM-dd","ns3:startDate")}
       { insertDate(data($endDate[$occ]),"yyyy-MM-dd","ns3:endDate")}
      &lt;ns3:idProductDefinition?&gt;{data($x)}&lt;/ns3:idProductDefinition&gt;
          {
            let $idDefinition := data($x)
               return 
               &lt;ns3:productRdsCodeList&gt;
                  {
                    for $x at $occ in $paRDSCode
                    return if ($idDefinition = data($paIdProductDef2[$occ]))
                     then
                    &lt;ns3:ProductRdsCode&gt;
                        &lt;ns3:rdsCode?&gt;{data($x)}&lt;/ns3:rdsCode&gt;
                        &lt;ns3:codeProductSystem?&gt;{data($paCodeProductSys[$occ])}&lt;/ns3:codeProductSystem&gt;
                    &lt;/ns3:ProductRdsCode&gt;
                     else()
                  }
              &lt;/ns3:productRdsCodeList&gt;
             }
    &lt;/ns3:ProductDefinition&gt;
  }
&lt;/ns0:productDefinitionList&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  {getElementsForProductDefinitionList($parm)}
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>