<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:accounts.entities.be.dcl";
declare namespace urn3 = "urn:filtersandmessages.entities.be.dcl";

declare function xf:mapeNOwnAccNewResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		&lt;urn:invokeResponse&gt;
			&lt;urn:response&gt;
				&lt;urn3:ResponseMessage&gt;
					{
						if (data($fml/fml:E_RESULT) = "0") then (
							&lt;urn3:result&gt;false&lt;/urn3:result&gt;
						)
						else if (data($fml/fml:E_RESULT) = "1") then (
							&lt;urn3:result&gt;true&lt;/urn3:result&gt;
						) else ()
					}
				&lt;/urn3:ResponseMessage&gt;
			&lt;/urn:response&gt;
		&lt;/urn:invokeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapeNOwnAccNewResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>