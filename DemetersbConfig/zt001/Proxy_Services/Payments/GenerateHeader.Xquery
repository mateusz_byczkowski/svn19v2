<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(: returns a &lt;head&gt;...&lt;/head&gt; element :)
declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace ebp = "http://www.softax.com.pl/ebppml";



declare function xf:GenerateHeader($fml as element(fml:FML32))
	as element(ebp:head) {
	&lt;ebp:head&gt;
		(: put more (EBPP_SYS_ID =&gt; bank) mappings here :)
			&lt;ebp:bank&gt;{
				if (data($fml/fml:EBPP_SYS_ID) = "1") then
				( 
					"10000109"
				) else ( data($fml/fml:EBPP_SYS_ID) )
				}
			&lt;/ebp:bank&gt;
			&lt;ebp:ref-id&gt;{ data($fml/fml:E_REF_ID) }&lt;/ebp:ref-id&gt;
			&lt;ebp:date&gt;{ fn:current-dateTime() }&lt;/ebp:date&gt;
&lt;/ebp:head&gt;
};

declare variable $head as element(soapenv:Body) external;
declare variable $body as element(soapenv:Body) external;
xf:GenerateHeader($body/fml:FML32)</con:xquery>
</con:xqueryEntry>