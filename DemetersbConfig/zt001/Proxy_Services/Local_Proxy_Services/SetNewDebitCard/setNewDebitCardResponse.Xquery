<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:card.entities.be.dcl";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns2="urn:be.services.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:dictionaries.be.dcl";
declare namespace ns7="urn:accounts.entities.be.dcl";
declare namespace ns6="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



(: declare function insertDate($value as xs:string,$dateFormat as xs:string,$fieldName as xs:string) as xs:string{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      }; :)


declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
     (: if ($value) then 
        if(string-length($value)&gt;5 and not (string(fn-bea:date-from-string-with-format($dateFormat,$value)) = "0001-01-01"))
             then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else() :)
     
     if ($value) then 
        if(string-length($value)&gt;5) then
           if ((substring($value, 3, 1) = "-") and ($value != "01-01-0001"))
              (:postac DD-MM-RRRR:)
               then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,string-join((substring($value,7,4),substring($value,4,2),substring($value,1,2)),'-'))}    
           else if (string(fn-bea:date-from-string-with-format($dateFormat,$value)) != "0001-01-01")
             then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
           else()
        else()
      else()
};



declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns2:invokeResponse&gt;
  &lt;ns2:response&gt;
    &lt;ns6:ResponseMessage&gt;
      {
      if ($parm/NF_RESPOM_ERRORCODE)
           then &lt;ns6:result&gt;false&lt;/ns6:result&gt;
           else &lt;ns6:result&gt;true&lt;/ns6:result&gt;
       }    
      &lt;ns6:errorCode?&gt;{data($parm/NF_RESPOM_ERRORCODE)}&lt;/ns6:errorCode&gt;
      &lt;ns6:errorDescription?&gt;{data($parm/DC_OPIS_BLEDU)}&lt;/ns6:errorDescription&gt;
    &lt;/ns6:ResponseMessage&gt;
  &lt;/ns2:response&gt;
  &lt;ns2:debitCard&gt;
    &lt;ns0:DebitCard&gt;
      (: &lt;ns0:expirationDate?&gt;{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/DC_DATA_WAZNOSCI))}&lt;/ns0:expirationDate&gt; :)
      {insertDate(data($parm/DC_DATA_WAZNOSCI),"yyyy-MM-dd","ns0:expirationDate")}
      (: &lt;ns0:nextReissueDate?&gt;{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/DC_DATA_URUCHOMIENIA))}&lt;/ns0:nextReissueDate&gt; :)
      {insertDate(data($parm/DC_DATA_URUCHOMIENIA),"yyyy-MM-dd","ns0:nextReissueDate")}
     (: &lt;ns0:nextCardFeeDate?&gt;{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/DC_DATA_NALICZENIA))}&lt;/ns0:nextCardFeeDate&gt; :)
	   {insertDate(data($parm/DC_DATA_NALICZENIA),"yyyy-MM-dd","ns0:nextCardFeeDate")}
      &lt;ns0:cardNbr?&gt;{data($parm/DC_NR_KARTY)}&lt;/ns0:cardNbr&gt;
      (: &lt;ns0:cardRecordOpenDate?&gt;{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/DC_DATA_POCZ))}&lt;/ns0:cardRecordOpenDate&gt; :)
        {insertDate(data($parm/DC_DATA_POCZ),"yyyy-MM-dd","ns0:cardRecordOpenDate")}
      &lt;ns0:cardFee?&gt;{data($parm/DC_KWOTA)}&lt;/ns0:cardFee&gt;
      &lt;ns0:virtualCardNbr?&gt;{data($parm/DC_NR_KARTY_BIN)}&lt;/ns0:virtualCardNbr&gt;
      &lt;ns0:cardStatus&gt;
        &lt;ns1:CrdStatus&gt;
          &lt;ns1:crdStatus?&gt;{data($parm/DC_STATUS_KARTY)}&lt;/ns1:crdStatus&gt;
        &lt;/ns1:CrdStatus&gt;
      &lt;/ns0:cardStatus&gt;
{
      if(data($parm/DC_NR_PB1)) then
         if(string-length(data($parm/DC_NR_PB1))=16) then
           if(number(data($parm/DC_NR_PB1))&gt;0) then
	  &lt;ns0:debitCardLpInfo&gt;
             &lt;ns0:DebitCardLpInfo&gt;
                  {
                  if(data($parm/DC_NR_PB2)) then
                    if(string-length(data($parm/DC_NR_PB2))=16) then
                        if(number(data($parm/DC_NR_PB2))&gt;0) then
                          &lt;ns0:customerLpNumber?&gt;{data($parm/DC_NR_PB2)}&lt;/ns0:customerLpNumber&gt;
                        else()
                     else()
                  else()
                  }
                  &lt;ns0:lpNumber?&gt;{data($parm/DC_NR_PB1)}&lt;/ns0:lpNumber&gt;
              &lt;/ns0:DebitCardLpInfo&gt;
          &lt;/ns0:debitCardLpInfo&gt;
          else()
         else()
      else()
}


    &lt;/ns0:DebitCard&gt;
  &lt;/ns2:debitCard&gt;
&lt;/ns2:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>