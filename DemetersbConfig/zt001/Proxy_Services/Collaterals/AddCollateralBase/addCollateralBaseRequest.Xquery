<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-01-10</con:description>
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:cif.entities.be.dcl";
declare namespace m3 = "urn:dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string

  return 
    if ($value &lt; 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDateTime($dateIn as xs:dateTime) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-dateTime($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-dateTime($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-dateTime($dateIn)))
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-date($dateIn)))
};

declare function xf:booleanTo01($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "1"
    else "0"
};

&lt;soap-env:Body&gt;
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:collateral/m1:Collateral

    let $msgId := $reqh/m:msgHeader/m:msgId
    let $transId := $reqh/m:transHeader/m:transId
    let $userId  := $reqh/m:msgHeader/m:userId
    let $unitId  := $reqh/m:msgHeader/m:unitId
    return
    
    &lt;fml:FML32&gt;
      &lt;fml:DC_MSHEAD_MSGID?&gt;{ data($msgId) }&lt;/fml:DC_MSHEAD_MSGID&gt;
      &lt;fml:DC_TRN_ID?&gt;{ data($transId) }&lt;/fml:DC_TRN_ID&gt;
      &lt;fml:DC_UZYTKOWNIK?&gt;{concat("SKP:", data($userId)) }&lt;/fml:DC_UZYTKOWNIK&gt;

      {if($unitId)
          then 
              if($unitId &lt; 1000)
                then &lt;fml:DC_ODDZIAL&gt;{ data($unitId) }&lt;/fml:DC_ODDZIAL&gt;
                else &lt;fml:DC_ODDZIAL&gt;0&lt;/fml:DC_ODDZIAL&gt;
          else ()
      }

      {if($req/m1:locationCode)
          then &lt;fml:DC_KOD_NALICZENIA_REZERWY&gt;{ data($req/m1:locationCode) }&lt;/fml:DC_KOD_NALICZENIA_REZERWY&gt;
          else &lt;fml:DC_KOD_NALICZENIA_REZERWY&gt;0&lt;/fml:DC_KOD_NALICZENIA_REZERWY&gt;
      }
      {if($req/m1:collateralDescription)
          then &lt;fml:DC_OPIS_ZABEZP_1_LINIA&gt;{ substring(data($req/m1:collateralDescription),1,60) }&lt;/fml:DC_OPIS_ZABEZP_1_LINIA&gt;
          else ()
      }
      {if($req/m1:collateralDescription)
          then &lt;fml:DC_OPIS_ZABEZP_2_LINIA&gt;{ substring(data($req/m1:collateralDescription),61,60) }&lt;/fml:DC_OPIS_ZABEZP_2_LINIA&gt;
          else ()
      }
      {if($req/m1:collateralDescription)
          then &lt;fml:DC_OPIS_ZABEZP_3_LINIA&gt;{ substring(data($req/m1:collateralDescription),121,60) }&lt;/fml:DC_OPIS_ZABEZP_3_LINIA&gt;
          else ()
      }
      {if($req/m1:collateralValue)
          then &lt;fml:DC_WARTOSC_ZABEZPIECZENIA&gt;{ data($req/m1:collateralValue) }&lt;/fml:DC_WARTOSC_ZABEZPIECZENIA&gt;
          else ()
      }
      {if($req/m1:expirationOrMaturityDate and fn:string-length($req/m1:expirationOrMaturityDate)&gt;0)
          then &lt;fml:DC_DATA_WYGAS_LUB_ZAPADALNO&gt;{ xf:mapDate($req/m1:expirationOrMaturityDate) }&lt;/fml:DC_DATA_WYGAS_LUB_ZAPADALNO&gt;
          else ()
      }
      {if($req/m1:insuranceRequired and fn:string-length($req/m1:insuranceRequired)&gt;0)
          then &lt;fml:DC_WYMAGANE_UBEZPIECZENIE_T_N&gt;{ xf:booleanTo01($req/m1:insuranceRequired) }&lt;/fml:DC_WYMAGANE_UBEZPIECZENIE_T_N&gt;
          else ()
      }
      {if($req/m1:insuranceExpiryDate and fn:string-length($req/m1:insuranceExpiryDate)&gt;0)
          then &lt;fml:DC_DATA_WAZNOSCI_UBEZP&gt;{ xf:mapDate($req/m1:insuranceExpiryDate) }&lt;/fml:DC_DATA_WAZNOSCI_UBEZP&gt;
          else ()
      }
      {if($req/m1:legalClaimRegisteredDate and fn:string-length($req/m1:legalClaimRegisteredDate)&gt;0)
          then &lt;fml:DC_DATA_REJESTRACJI_ZABEZP&gt;{ xf:mapDate($req/m1:legalClaimRegisteredDate) }&lt;/fml:DC_DATA_REJESTRACJI_ZABEZP&gt;
          else ()
      }
      {if($req/m1:legalClaimExpiryDate and fn:string-length($req/m1:legalClaimExpiryDate)&gt;0)
          then &lt;fml:DC_DATA_WYGAS_REJESTRACJI&gt;{ xf:mapDate($req/m1:legalClaimExpiryDate) }&lt;/fml:DC_DATA_WYGAS_REJESTRACJI&gt;
          else ()
      }
      {if($req/m1:reviewFrequency)
          then &lt;fml:DC_CZESTOTL_WYCENY&gt;{ data($req/m1:reviewFrequency) }&lt;/fml:DC_CZESTOTL_WYCENY&gt;
          else ()
      }
      {if($req/m1:reviewPeriod)
          then &lt;fml:DC_OKRES_WYCENY_ZABEZP&gt;{ data($req/m1:reviewPeriod) }&lt;/fml:DC_OKRES_WYCENY_ZABEZP&gt;
          else ()
      }
      &lt;fml:DC_OKRESLONY_DZIEN_WYCENY&gt;00&lt;/fml:DC_OKRESLONY_DZIEN_WYCENY&gt;
      {if($req/m1:firstReviewDate and fn:string-length($req/m1:firstReviewDate)&gt;0)
          then &lt;fml:DC_DATA_PIERWSZEJ_WYCENY&gt;{ xf:mapDateTime($req/m1:firstReviewDate) }&lt;/fml:DC_DATA_PIERWSZEJ_WYCENY&gt;
          else ()
      }
      {if($req/m1:reviewComments)
          then &lt;fml:DC_KOMENTARZ_DLA_WYCENY&gt;{ data($req/m1:reviewComments) }&lt;/fml:DC_KOMENTARZ_DLA_WYCENY&gt;
          else ()
      }
      {if($req/m1:numberOfUnits)
          then &lt;fml:DC_LICZBA_JEDN_ZABEZP&gt;{ data($req/m1:numberOfUnits) }&lt;/fml:DC_LICZBA_JEDN_ZABEZP&gt;
          else ()
      }
      {if($req/m1:unitPrice)
          then &lt;fml:DC_WARTOSC_JEDNOSTKOWA&gt;{ data($req/m1:unitPrice) }&lt;/fml:DC_WARTOSC_JEDNOSTKOWA&gt;
          else ()
      }
      {if($req/m1:marginPercentage and fn:string-length($req/m1:marginPercentage)&gt;0)
          then &lt;fml:DC_PROCENT_POMNIEJSZ_REZERWY&gt;{ round-half-to-even(data($req/m1:marginPercentage)*100,4) }&lt;/fml:DC_PROCENT_POMNIEJSZ_REZERWY&gt;
          else ()
      }
      {if($req/m1:maximumCollateralValue)
          then &lt;fml:DC_MAKS_WARTOSC_ZABEZPIECZ&gt;{ data($req/m1:maximumCollateralValue) }&lt;/fml:DC_MAKS_WARTOSC_ZABEZPIECZ&gt;
          else ()
      }
      {if($req/m1:dateLastPriced and fn:string-length($req/m1:dateLastPriced)&gt;0)
          then &lt;fml:DC_DATA_POPRZEDNIEJ_WYCENY&gt;{ xf:mapDate($req/m1:dateLastPriced) }&lt;/fml:DC_DATA_POPRZEDNIEJ_WYCENY&gt;
          else ()
      }
      {if($req/m1:itemReferenceNumber and fn:string-length($req/m1:itemReferenceNumber)&gt;0)
          then &lt;fml:DC_NR_RACH_OBJETY_BLOKADA_SRO&gt;{ data($req/m1:itemReferenceNumber) }&lt;/fml:DC_NR_RACH_OBJETY_BLOKADA_SRO&gt;
          else ()
      }
      {if($req/m1:safekeepingReceiptNbr)
          then &lt;fml:DC_INFORM_DODATK_O_ZABEZP&gt;{ data($req/m1:safekeepingReceiptNbr) }&lt;/fml:DC_INFORM_DODATK_O_ZABEZP&gt;
          else ()
      }
      {if($req/m1:shortDescription)
          then &lt;fml:DC_NAZWA_SKROCONA&gt;{ data($req/m1:shortDescription) }&lt;/fml:DC_NAZWA_SKROCONA&gt;
          else ()
      }
      {if($req/m1:userField6)
          then &lt;fml:DC_NR_UMOWY_PORECZ&gt;{ data($req/m1:userField6) }&lt;/fml:DC_NR_UMOWY_PORECZ&gt;
          else ()
      }

      {for $it at $i in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:customer/m2:Customer/m2:customerNumber)
            then 
              if($it/m1:relationship = "SOW" or $it/m1:relationship = "JAF")
                then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($it/m1:customer/m2:Customer/m2:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA&gt;
                else ()
            else ()
      }

      {for $it in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:relationship)
            then (&lt;fml:DC_RELACJA&gt;{ data($it/m1:relationship) }&lt;/fml:DC_RELACJA&gt;,
                     &lt;fml:DC_TYP_RELACJI&gt;1&lt;/fml:DC_TYP_RELACJI&gt;)
            else ()
      }

      {for $it in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:customer/m2:Customer/m2:customerNumber)
            then &lt;fml:DC_NUMER_KLIENTA_REL&gt;{ data($it/m1:customer/m2:Customer/m2:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA_REL&gt;
            else ()
      }

      {if($req/m1:collateralCode/m3:CollateralCode/m3:collateralCode)
          then &lt;fml:DC_KOD_ZABEZPIECZENIA&gt;{ data($req/m1:collateralCode/m3:CollateralCode/m3:collateralCode) }&lt;/fml:DC_KOD_ZABEZPIECZENIA&gt;
          else ()
      }
      {if($req/m1:currencyCode/m3:CurrencyCode/m3:currencyCode)
          then &lt;fml:DC_WALUTA&gt;{ data($req/m1:currencyCode/m3:CurrencyCode/m3:currencyCode) }&lt;/fml:DC_WALUTA&gt;
          else ()
      }
      {if($req/m1:methodOfSecurityValuation/m3:MethodOfSecurityValuation/m3:methodOfSecurityValuation)
          then &lt;fml:DC_METODA_WYCENY_ZABEZP&gt;{ data($req/m1:methodOfSecurityValuation/m3:MethodOfSecurityValuation/m3:methodOfSecurityValuation) }&lt;/fml:DC_METODA_WYCENY_ZABEZP&gt;
          else ()
      }
      {if($req/m1:personValuatingASecurity/m3:PersonValuatingASecurity/m3:personValuatingASecurity)
          then &lt;fml:DC_OSOBA_WYCENIAJACA_ZABEZP&gt;{ data($req/m1:personValuatingASecurity/m3:PersonValuatingASecurity/m3:personValuatingASecurity) }&lt;/fml:DC_OSOBA_WYCENIAJACA_ZABEZP&gt;
          else ()
      }
      {if($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber and fn:string-length($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber)&gt;0)
          then &lt;fml:DC_BLOKADA_SRODK_NA_R_KU&gt;{ data($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber) }&lt;/fml:DC_BLOKADA_SRODK_NA_R_KU&gt;
          else ()
      }
      &lt;fml:DC_ENCODING&gt;PL_MZV&lt;/fml:DC_ENCODING&gt;
    &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>