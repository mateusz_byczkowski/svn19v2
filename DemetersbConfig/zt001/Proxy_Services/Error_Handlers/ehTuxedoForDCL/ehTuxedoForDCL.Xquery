<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare variable $body external;
declare variable $fault external;

declare function local:fault($faultString as xs:string, $detail as xs:anyType) as element(soap-env:Fault) {
		&lt;soap-env:Fault&gt;
			&lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
			&lt;faultstring&gt;{ $faultString }&lt;/faultstring&gt; 
			&lt;detail&gt;{ $detail }&lt;/detail&gt;
		&lt;/soap-env:Fault&gt;
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string,
        $errorDescription as xs:string,$errorCodeFromBS as xs:string) as element(err:exceptionItem) {
   &lt;err:exceptionItem&gt;
	&lt;err:errorCode1&gt;{ $errorCode1 }&lt;/err:errorCode1&gt;
	&lt;err:errorCode2&gt;{ $errorCode2 }&lt;/err:errorCode2&gt;
        {
           if (string-length($errorCodeFromBS)&gt;0) 
               then   &lt;err:errorDescription&gt;{ concat($errorCodeFromBS," : ",$errorDescription) }&lt;/err:errorDescription&gt;
               else    &lt;err:errorDescription&gt;{ $errorDescription }&lt;/err:errorDescription&gt;
        }
   &lt;/err:exceptionItem&gt;
};

&lt;soap-env:Body&gt;
	{
		let $reason := fn:substring-after(fn:substring-before(data($fault/ctx:reason), "):"), "(")
		let $urcode := fn:substring-before(fn:substring-after(fn:substring-after(data($fault/ctx:reason),":"),":"),":")
		let $timeoutDesc := "Wysołanie usługi przekroczyło dopuszczalny czas odpowiedzi"
		let $wrongInputDesc := "Błędny komunikat wejściowy"
		let $dcInterfaceDownDesc := "Interfejs DC ICBS jest wyłączony - przetwarzanie niemożliwe"
		let $fmlBufferDesc := "Błąd bufora FML" 
		let $defaultUrcodeDesc := "Błąd wywołania usługi"
		let $defaultDesc := "Krytyczny bład usługi w systemie źródłowym"
		let $alsbErrorDesc := "Wystąpił bład w usłudze proxy na ALSB"
		let $errorDescriptiona:=data($body/FML32/NF_ERROR_DESCRIPTION[1])
		let $errorDescriptionDCa:=data($body/FML32/DC_OPIS_BLEDU[1])
		let $errorCode:=data($body/FML32/NF_ERROR_CODE[1])

                let $errorDescriptionDC := translate($errorDescriptionDCa, "za§˘’Ťg‘žl†ja¦¤ZAˇŁś•G?LŹJAĄ;&amp;#346;", "zażółćgęśląjaźńZAŻÓŁĆGĘLĄJAŃŚ")
                let $errorDescription := translate($errorDescriptiona, "za§˘’Ťg‘žl†ja¦¤ZAˇŁś•G?LŹJAĄ;&amp;#346;", "zażółćgęśląjaźńZAŻÓŁĆGĘLĄJAŃŚ")
		
		
		return
                  if (data($fault/ctx:errorCode) = "BEA-380000") then
                    if($reason = "13") then
		       local:fault("err:TimeoutException", element err:TimeoutException { local:errors($reason, $urcode, $timeoutDesc,"") })
		    else  if($reason = "11") then
		      if($urcode = "100") then
	                local:fault("err:SystemException", element err:SystemException { local:errors($reason, $urcode, $dcInterfaceDownDesc,"") })
                      else if($urcode = "101") then
                        local:fault("err:SystemException", element err:SystemException { local:errors($reason, $urcode, $fmlBufferDesc,"") })
                      else if($urcode = "102") then
                        local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $wrongInputDesc,"") })
                      else if (string-length($errorDescription)&gt;0 or string-length($errorCode)&gt;0) then
                        local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $errorDescription,$errorCode) })
                      else if (string-length($errorDescriptionDC)&gt;0) then
                        local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $errorDescriptionDC,"") })
                      else
                        local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $defaultUrcodeDesc,"") })
                    else
                      local:fault("err:ServiceException", element err:ServiceException { local:errors($reason, $urcode,$defaultDesc,"") })

                 else
                    local:fault("err:SystemException", element err:SystemException { local:errors("0","0",$alsbErrorDesc ,"") })
             }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>