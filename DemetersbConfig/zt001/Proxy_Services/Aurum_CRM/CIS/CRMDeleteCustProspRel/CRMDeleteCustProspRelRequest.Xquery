<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMDeleteCustProspRelRequest($req as element(m:CRMDeleteCustProspRelRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:CustCif)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:CustCif) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:RelType)
					then &lt;fml:DC_TYP_RELACJI&gt;{ data($req/m:RelType) }&lt;/fml:DC_TYP_RELACJI&gt;
					else ()
			}
			{
				if($req/m:EmpSkpNo)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:EmpSkpNo) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
                        {
                                if($req/m:NumerKlienta)
                                         then &lt;fml:CI_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:CI_NUMER_KLIENTA&gt;
                                         else ()
                        }
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMDeleteCustProspRelRequest($body/m:CRMDeleteCustProspRelRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>