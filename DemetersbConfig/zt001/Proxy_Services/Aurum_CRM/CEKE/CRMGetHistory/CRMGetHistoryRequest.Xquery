<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetHistoryRequest($req as element(m:CRMGetHistoryRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:KodRach)
					then &lt;fml:B_KOD_RACH&gt;{ data($req/m:KodRach) }&lt;/fml:B_KOD_RACH&gt;
					else ()
			}
			{
				if($req/m:DlNrRach)
					then &lt;fml:B_DL_NR_RACH&gt;{ data($req/m:DlNrRach) }&lt;/fml:B_DL_NR_RACH&gt;
					else ()
			}
			{
				if($req/m:DataStr)
					then &lt;fml:B_DATA_STR&gt;{ data($req/m:DataStr) }&lt;/fml:B_DATA_STR&gt;
					else ()
			}
			{
				if($req/m:DataGran)
					then &lt;fml:B_DATA_GRAN&gt;{ data($req/m:DataGran) }&lt;/fml:B_DATA_GRAN&gt;
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:B_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:B_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:LiczbaDni)
					then &lt;fml:B_LICZBA_DNI&gt;{ data($req/m:LiczbaDni) }&lt;/fml:B_LICZBA_DNI&gt;
					else ()
			}
			{
				if($req/m:Lp)
					then &lt;fml:B_LP&gt;{ data($req/m:Lp) }&lt;/fml:B_LP&gt;
					else ()
			}
			{
				if($req/m:Kierunek)
					then &lt;fml:B_KIERUNEK&gt;{ data($req/m:Kierunek) }&lt;/fml:B_KIERUNEK&gt;
					else ()
			}
			{
				if($req/m:Extra)
					then &lt;fml:B_EXTRA&gt;{ data($req/m:Extra) }&lt;/fml:B_EXTRA&gt;
					else ()
			}
			{
				if($req/m:Sys)
					then
					(
						if (data($req/m:Sys) = "1") then
						(
							&lt;fml:B_SYS&gt;3&lt;/fml:B_SYS&gt;
						) else if (data($req/m:Sys) = "5") then
						(
							&lt;fml:B_SYS&gt;1&lt;/fml:B_SYS&gt;
						) else if (data($req/m:Sys) = "7") then
						(
							&lt;fml:B_SYS&gt;4&lt;/fml:B_SYS&gt;
						) else if (data($req/m:Sys) = "9") then
						(
							&lt;fml:B_SYS&gt;5&lt;/fml:B_SYS&gt;
						) else (
							&lt;fml:B_SYS&gt;{ data($req/m:Sys) }&lt;/fml:B_SYS&gt;
						)
					)
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapGetHistoryRequest($body/m:CRMGetHistoryRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>