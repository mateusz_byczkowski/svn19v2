<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/prl/messages/";
declare namespace xf = "http://bzwbk.com/services/prl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetPrelimitRequest($req as element(m:GetPrelimitRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				&lt;fml:DC_NUMER_KLIENTA?&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapGetPrelimitRequest($body/m:GetPrelimitRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>