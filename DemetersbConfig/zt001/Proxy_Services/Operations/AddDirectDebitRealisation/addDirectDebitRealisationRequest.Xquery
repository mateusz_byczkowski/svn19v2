<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:directdebit.entities.be.dcl";
declare namespace ns4="urn:transferorder.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function chkUnitId($unitId as xs:anyType) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:getFields($parm as element(ns7:invoke), $msghead as element(ns7:msgHeader), $tranhead as element(ns7:transHeader)) as element(fml:FML32) 
{
  let $msgId:= $msghead/ns7:msgId
  let $companyId:= $msghead/ns7:companyId
  let $userId := $msghead/ns7:userId
  let $appId:= $msghead/ns7:appId
  let $unitId := $msghead/ns7:unitId
  let $timestamp:= $msghead/ns7:timestamp

  let $transId:=$tranhead/ns7:transId

  return
  &lt;fml:FML32&gt;
     &lt;DC_TRN_ID?&gt;{data($transId)}&lt;/DC_TRN_ID&gt;
     &lt;DC_UZYTKOWNIK?&gt;{data($userId)}&lt;/DC_UZYTKOWNIK&gt;
     &lt;DC_ODDZIAL?&gt;{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL&gt;
     &lt;NF_DIREDR_ORIGINATORREFERE?&gt;{data($parm/ns7:directDebitRealisation/ns2:DirectDebitRealisation/ns2:originatorReference)}&lt;/NF_DIREDR_ORIGINATORREFERE&gt;
     &lt;NF_DIREDR_CHARGEDATE?&gt;{data($parm/ns7:directDebitRealisation/ns2:DirectDebitRealisation/ns2:chargeDate)}&lt;/NF_DIREDR_CHARGEDATE&gt;
     &lt;NF_DIREDR_AMOUNT?&gt;{data($parm/ns7:directDebitRealisation/ns2:DirectDebitRealisation/ns2:amount)}&lt;/NF_DIREDR_AMOUNT&gt;
     &lt;NF_DIREDR_DEBTOR1?&gt;{data($parm/ns7:directDebitRealisation/ns2:DirectDebitRealisation/ns2:debtor1)}&lt;/NF_DIREDR_DEBTOR1&gt;
     &lt;NF_DIREDR_DEBTOR2?&gt;{data($parm/ns7:directDebitRealisation/ns2:DirectDebitRealisation/ns2:debtor2)}&lt;/NF_DIREDR_DEBTOR2&gt;
     &lt;NF_DIREDR_DEBTOR3?&gt;{data($parm/ns7:directDebitRealisation/ns2:DirectDebitRealisation/ns2:debtor3)}&lt;/NF_DIREDR_DEBTOR3&gt;
     &lt;NF_DIREDR_DEBTOR4?&gt;{data($parm/ns7:directDebitRealisation/ns2:DirectDebitRealisation/ns2:debtor4)}&lt;/NF_DIREDR_DEBTOR4&gt;
     &lt;NF_ACCOUN_ACCOUNTNUMBER?&gt;{data($parm/ns7:account/ns1:Account/ns1:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER&gt;
     &lt;NF_CUSTOM_TAXID?&gt;{data($parm/ns7:customer/ns3:Customer/ns3:debtorTaxID)}&lt;/NF_CUSTOM_TAXID&gt;
     &lt;NF_ACCOUO_ACCOUNTNUMBER?&gt;{data($parm/ns7:accountOutside/ns4:AccountOutside/ns4:accountNumber)}&lt;/NF_ACCOUO_ACCOUNTNUMBER&gt;
  &lt;/fml:FML32&gt;
};

&lt;soap:Body&gt;
     { xf:getFields($body/ns7:invoke, $header/ns7:header/ns7:msgHeader, $header/ns7:header/ns7:transHeader)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>