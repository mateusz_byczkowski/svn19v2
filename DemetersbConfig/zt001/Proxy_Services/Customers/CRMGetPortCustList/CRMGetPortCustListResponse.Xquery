<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-10-18</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Tomasz Krajewski  
 : @version 1.0
 : @since   2010-10-18
 :
 : wersja WSDLa: 2010-10-18
 :
 : $Proxy Services/Customers/CRMGetPortCustList/CRMGetPortCustListResponse.xq$
 :
 :)
 
declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPortCustListResponse($fml as element(fml:FML32))
	as element(m:CRMGetPortCustListResponse) {
		&lt;m:CRMGetPortCustListResponse&gt;
			{
				for $cif at $occ in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:Cif&gt;{ data($cif) }&lt;/m:Cif&gt;
			}
		&lt;/m:CRMGetPortCustListResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ 
	xf:mapCRMGetPortCustListResponse($body/fml:FML32) 
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>