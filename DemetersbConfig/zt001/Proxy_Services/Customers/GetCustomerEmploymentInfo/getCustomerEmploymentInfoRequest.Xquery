<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns4="urn:filtersandmessages.entities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{
&lt;fml:CI_PRACOWNIK_ZMIEN?&gt;{data($parm/ns0:msgHeader/ns0:userId)}&lt;/fml:CI_PRACOWNIK_ZMIEN&gt;,
&lt;fml:CI_CZAS_AKTUALIZACJI&gt;{ fn-bea:dateTime-to-string-with-format("yyyy-MM-dd-HH.mm.ss.SSSSSS", data($parm/ns0:msgHeader/ns0:timestamp)) }&lt;/fml:CI_CZAS_AKTUALIZACJI&gt;,

  if (string-length(data($parm/ns0:customer/ns2:Customer/ns2:companyID/ns3:CompanyType/ns3:companyType)) &gt; 0) then
     &lt;fml:CI_ID_SPOLKI&gt;{data($parm/ns0:customer/ns2:Customer/ns2:companyID/ns3:CompanyType/ns3:companyType)}&lt;/fml:CI_ID_SPOLKI&gt;
   else
     &lt;fml:CI_ID_SPOLKI?&gt;{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/fml:CI_ID_SPOLKI&gt;

};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

&lt;fml:DC_NUMER_KLIENTA?&gt;{data($parm/ns0:customer/ns2:Customer/ns2:customerNumber)}&lt;/fml:DC_NUMER_KLIENTA&gt;,
&lt;fml:DC_TYP_KLIENTA&gt;F&lt;/fml:DC_TYP_KLIENTA&gt;,
&lt;fml:CI_OPCJA&gt;4&lt;/fml:CI_OPCJA&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>