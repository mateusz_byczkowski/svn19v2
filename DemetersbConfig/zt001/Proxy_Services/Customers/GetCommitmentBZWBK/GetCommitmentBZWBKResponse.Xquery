<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-12-17</con:description>
  <con:xquery>declare namespace urn  = "urn:be.services.dcl";
declare namespace urn1 = "urn:applicationsme.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body&gt;
  {
    let $fml := $body/fml:FML32
    return

    &lt;urn:invokeResponse xmlns:urn="urn:be.services.dcl"&gt;
      &lt;urn:commitment xmlns:urn1="urn:applicationsme.entities.be.dcl"&gt;
        &lt;urn1:AppCommitmentBZWBK&gt;
          &lt;urn1:currentValueROR?&gt;{data($fml/fml:DC_CURRENT_VALUE_ROR)}&lt;/urn1:currentValueROR&gt;
          &lt;urn1:currentValueBEX?&gt;{data($fml/fml:DC_CURRENT_VALUE_BEX)}&lt;/urn1:currentValueBEX&gt;
          &lt;urn1:currentValueOBR?&gt;{data($fml/fml:DC_CURRENT_VALUE_OBR)}&lt;/urn1:currentValueOBR&gt;
          &lt;urn1:currentValueHIP?&gt;{data($fml/fml:DC_CURRENT_VALUE_HIP)}&lt;/urn1:currentValueHIP&gt;
          &lt;urn1:currentValueINV?&gt;{data($fml/fml:DC_CURRENT_VALUE_INV)}&lt;/urn1:currentValueINV&gt;
          &lt;urn1:currentValueGWR?&gt;{data($fml/fml:DC_CURRENT_VALUE_GWR)}&lt;/urn1:currentValueGWR&gt;
          &lt;urn1:currentValueBCC?&gt;{data($fml/fml:DC_CURRENT_VALUE_BCC)}&lt;/urn1:currentValueBCC&gt;
          &lt;urn1:currentValueINN?&gt;{data($fml/fml:DC_CURRENT_VALUE_INN)}&lt;/urn1:currentValueINN&gt;
          &lt;urn1:currentValueBEH?&gt;{data($fml/fml:DC_CURRENT_VALUE_BEH)}&lt;/urn1:currentValueBEH&gt;
        &lt;/urn1:AppCommitmentBZWBK&gt;
      &lt;/urn:commitment&gt;
    &lt;/urn:invokeResponse&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>