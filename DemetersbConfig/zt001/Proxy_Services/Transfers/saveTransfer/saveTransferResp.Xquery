<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf="urn:dcl:services.alsb.datamodel";

declare namespace  dcl="urn:be.services.dcl";
declare namespace  ns1="urn:operations.entities.be.dcl";
declare namespace  ns2="urn:operationsdictionary.dictionaries.be.dcl";
declare namespace  ns3="urn:dictionaries.be.dcl";
declare namespace  ns4="urn:basedictionaries.be.dcl";
declare namespace  ns5="urn:filtersandmessages.entities.be.dcl";



declare variable $body external;
declare variable $fml as element(FML32):=$body/FML32;

declare function xf:dateTimeFromString($dateIn as xs:string) as xs:string 
{
     fn:concat (fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2),
                        "T00:00:00")
};

&lt;soap-env:Body&gt; 
     &lt;dcl:invokeResponse&gt; 
        
        &lt;dcl:result&gt;
            &lt;ns5:ResponseMessage&gt;
                    &lt;ns5:result&gt;
                        { if  ($fml/TR_STATUS = "P")
                           then     "true"
                           else     "false"}
                      &lt;/ns5:result&gt;
            &lt;/ns5:ResponseMessage&gt;
        &lt;/dcl:result&gt;

          &lt;dcl:transaction&gt;
              &lt;ns1:Transaction&gt;
                  &lt;ns1:transactionID&gt;{data($fml/TR_ID_OPER)}&lt;/ns1:transactionID&gt;
                  &lt;ns1:transactionDate&gt;{xf:dateTimeFromString($fml/TR_DATA_KSIEG)}&lt;/ns1:transactionDate&gt;
                  &lt;ns1:transactionStatus&gt;
                    &lt;ns3:TransactionStatus&gt;
                       &lt;ns3:transactionStatus&gt;{data($fml/TR_STATUS)}&lt;/ns3:transactionStatus&gt;
                    &lt;/ns3:TransactionStatus&gt;
                  &lt;/ns1:transactionStatus&gt;
               &lt;/ns1:Transaction&gt;
         &lt;/dcl:transaction&gt;

        &lt;dcl:errCode&gt;
            { if  ($fml/TR_KOD_BLEDU_1 != "000")  then
                &lt;ns5:ResponseMessage&gt;
                         &lt;ns5:errorCode&gt;{data($fml/TR_KOD_BLEDU_1)}&lt;/ns5:errorCode&gt;
                         &lt;ns5:errorDescription&gt;&lt;/ns5:errorDescription&gt;
                &lt;/ns5:ResponseMessage&gt;
                else()
            }
             { if  ($fml/TR_KOD_BLEDU_2 != "000")  then
                 &lt;ns5:ResponseMessage&gt;
                     &lt;ns5:errorCode&gt;{data($fml/TR_KOD_BLEDU_2)}&lt;/ns5:errorCode&gt;
                     &lt;ns5:errorDescription&gt;&lt;/ns5:errorDescription&gt;
                 &lt;/ns5:ResponseMessage&gt;
                   else()             
             }
             { if  ($fml/TR_KOD_BLEDU_3 != "000")  then
                 &lt;ns5:ResponseMessage&gt;
                     &lt;ns5:errorCode&gt;{data($fml/TR_KOD_BLEDU_3)}&lt;/ns5:errorCode&gt;
                     &lt;ns5:errorDescription&gt;&lt;/ns5:errorDescription&gt;
                 &lt;/ns5:ResponseMessage&gt;
                   else()             
             }
             { if  ($fml/TR_KOD_BLEDU_4 != "000")  then
                 &lt;ns5:ResponseMessage&gt;
                     &lt;ns5:errorCode&gt;{data($fml/TR_KOD_BLEDU_4)}&lt;/ns5:errorCode&gt;
                     &lt;ns5:errorDescription&gt;&lt;/ns5:errorDescription&gt;
                 &lt;/ns5:ResponseMessage&gt;
                   else()             
             }
             { if  ($fml/TR_KOD_BLEDU_5 != "000")  then
                 &lt;ns5:ResponseMessage&gt;
                     &lt;ns5:errorCode&gt;{data($fml/TR_KOD_BLEDU_5)}&lt;/ns5:errorCode&gt;
                     &lt;ns5:errorDescription&gt;&lt;/ns5:errorDescription&gt;
                 &lt;/ns5:ResponseMessage&gt;
                   else()             
             }
         &lt;/dcl:errCode&gt;
         
        &lt;dcl:resultCode&gt;
            &lt;ns5:ResponseMessage&gt;
                    &lt;ns5:resultCode&gt; {data($fml/TR_STATUS)} &lt;/ns5:resultCode&gt;
            &lt;/ns5:ResponseMessage&gt;
        &lt;/dcl:resultCode&gt;

        &lt;dcl:errorDescription&gt;
            &lt;ns5:ResponseMessage&gt;
                    &lt;ns5:errorDescription&gt; {data($fml/TR_OPIS_BLEDU)} &lt;/ns5:errorDescription&gt;
            &lt;/ns5:ResponseMessage&gt;
        &lt;/dcl:errorDescription&gt;

   &lt;/dcl:invokeResponse&gt; 

&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>