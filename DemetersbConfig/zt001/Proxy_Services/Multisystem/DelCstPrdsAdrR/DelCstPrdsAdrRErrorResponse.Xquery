<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-12-10</con:description>
  <con:xquery>(: Log zmian
v.1.1  2010-12-01 PKLI TP58  Modyfikacja Error Handlera DelCstPrdsAdrRErrorResponse

:)

declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace fml="";

declare variable $body external;

  &lt;soap-env:Body&gt;
	{
	  (: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
	  let $reason := fn:substring-before(fn:substring-after(fn:substring-before($body/ctx:reason, ":"), "("), ")")
	  let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($body/ctx:reason, ":"), ":"), ":")
         let $errorCode:= data($body/ctx:errorCode)
	  return
           &lt;FML32&gt;       
              {
              if( string-length(data($reason)) = 0 or $reason = "12")
                 then (
                    &lt;DC_TYLKO_STATUS&gt;12&lt;/DC_TYLKO_STATUS&gt;,
                    &lt;DC_OPIS_BLEDU&gt;Krytyczny blad uslugi w systemie zrodlowym&lt;/DC_OPIS_BLEDU&gt;)
                 else if ($reason="13") then (
                        &lt;DC_TYLKO_STATUS&gt;{$reason}&lt;/DC_TYLKO_STATUS&gt;,
                        &lt;DC_OPIS_BLEDU&gt;Przekroczenie maksymalnego czasu oczekiwania na odpowiedz&lt;/DC_OPIS_BLEDU&gt;)
                 else if ($reason="6") then (
                        &lt;DC_TYLKO_STATUS&gt;{$reason}&lt;/DC_TYLKO_STATUS&gt;,
                        &lt;DC_OPIS_BLEDU&gt;Brak uslugi w systemie zrodlowym&lt;/DC_OPIS_BLEDU&gt;)
                 else if ($reason="11") then
                        if ($urcode = "102") then (
                           &lt;DC_TYLKO_STATUS&gt;{$urcode}&lt;/DC_TYLKO_STATUS&gt;,
                           &lt;DC_OPIS_BLEDU&gt;Bledne dane wejsciowe&lt;/DC_OPIS_BLEDU&gt;)
                        else if ($urcode = "103") then (
                            &lt;DC_TYLKO_STATUS&gt;{$urcode}&lt;/DC_TYLKO_STATUS&gt;,
                            &lt;DC_OPIS_BLEDU&gt;Brak danych&lt;/DC_OPIS_BLEDU&gt;)
                        else if ($urcode = "104") then (
                            &lt;DC_TYLKO_STATUS&gt;{$urcode}&lt;/DC_TYLKO_STATUS&gt;,
                            &lt;DC_OPIS_BLEDU&gt;Klient nie istnieje lub nie posiada produktow z adresem alternatywnym&lt;/DC_OPIS_BLEDU&gt;)
                        else (
                            &lt;DC_TYLKO_STATUS&gt;{$urcode}&lt;/DC_TYLKO_STATUS&gt;,
                            &lt;DC_OPIS_BLEDU&gt;Brak autoryzacji lub blad uslugi zrodlowej&lt;/DC_OPIS_BLEDU&gt;)
                  else (
                     &lt;DC_TYLKO_STATUS&gt;{$urcode}&lt;/DC_TYLKO_STATUS&gt;,
                     &lt;DC_OPIS_BLEDU&gt;Nieznany krytyczny blad uslugi w systemie zrodlowym&lt;/DC_OPIS_BLEDU&gt;)
            }        
            &lt;/FML32&gt;
	}
   
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>