<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace w="http://schemas.datacontract.org/2004/07/SzrekLibrary.Struktures";
declare namespace fml="";
declare namespace WBKFault="http://bzwbk.com/services/faults/";
declare variable $body external;


&lt;soap-env:Body&gt;
	{
	  let $reason := $body/soap:Fault/detail/w:WbkFault
	  let $urcode := $body/soap:Fault/detail/w:WbkFault/w:ErrorCode2
          return
               
      	     if( $reason  and string-length(data($urcode))&gt;0)
                 then
                      &lt;FML32&gt;   
                       &lt;NF_ERROR_DESCRIPTION&gt;11&lt;/NF_ERROR_DESCRIPTION&gt;
                       &lt;NF_ERROR_CODE&gt;{data($urcode)}&lt;/NF_ERROR_CODE&gt;
                     &lt;/FML32&gt;
                 else 
                     &lt;FML32&gt;   
                       &lt;NF_ERROR_DESCRIPTION&gt;12&lt;/NF_ERROR_DESCRIPTION&gt;
                       &lt;NF_ERROR_CODE&gt;0&lt;/NF_ERROR_CODE&gt;
                     &lt;/FML32&gt;
             
          
	}
  
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>