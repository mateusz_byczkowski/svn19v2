<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.2  2010-05-06 PKLI NP1972_3 CR2 Filtrowanie produktów Streamline na podstawie statusu aktywności
v.1.1  2009-11-25 PKLI NP1972_1 Obsługa pól &lt;factory&gt; i &lt;is-group&gt;</con:description>
  <con:xquery>(: Log Zmian: 
==================================
v.1.1  2009-11-25 PKLI NP1972_1 Obsługa pól &lt;factory&gt; i &lt;is-group&gt;
v.1.2  2010-05-06 PKLI NP1972_3 CR2 Filtrowanie produktów Streamline na podstawie statusu aktywności
:)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
    &lt;wsdl:getContractListRequest xmlns:wsdl="http://jv.channel.cu.com.pl/cmf/wsdl-jv"&gt;
        &lt;envelope&gt;
            &lt;request-time&gt;{current-dateTime()}&lt;/request-time&gt;
            &lt;request-no?&gt;{data($body/FML32/NF_MSHEAD_MSGID)}&lt;/request-no&gt;
            &lt;source-code&gt;placowka banku&lt;/source-code&gt;
            &lt;user-id?&gt;{data($body/FML32/NF_MSHEAD_USERID)}&lt;/user-id&gt;
            &lt;branch-id?&gt;{data($body/FML32/NF_MSHEAD_UNITID)}&lt;/branch-id&gt;
            (:&lt;bar-code&gt;string&lt;/bar-code&gt;:)
        &lt;/envelope&gt;
        &lt;client-code&gt;{data($body/FML32/NF_CUSTOM_CUSTOMERNUMBER)}&lt;/client-code&gt;
        (:&lt;product-type&gt;string&lt;/product-type&gt;:)
        (:&lt;category&gt;string&lt;/category&gt;:)
(: v.1.1 start:)
        {
 	if ($body/FML32/NF_CTRL_SYSTEMID and data($body/FML32/NF_CTRL_SYSTEMID)=10)
          then &lt;factory&gt;JVL&lt;/factory&gt;
          elseif ($body/FML32/NF_CTRL_SYSTEMID and data($body/FML32/NF_CTRL_SYSTEMID)=11)
            then   &lt;factory&gt;JVG&lt;/factory&gt;
            else() 
        }
        {
 	if ($body/FML32/NF_CTRL_AREAS and data($body/FML32/NF_CTRL_AREAS)=94)
          then &lt;is-group&gt;true&lt;/is-group&gt;
          elseif  ($body/FML32/NF_CTRL_AREAS and data($body/FML32/NF_CTRL_AREAS)=84)
             then &lt;is-group&gt;false&lt;/is-group&gt;
             else() 
        }
(:v.1.1 end:)
(: v.1.2 start :)
        {
           if ($body/FML32/NF_CTRL_ACTIVENONACTIVE and data($body/FML32/NF_CTRL_ACTIVENONACTIVE)=1)
           then &lt;general-status&gt;2&lt;/general-status&gt;
           elseif ($body/FML32/NF_CTRL_ACTIVENONACTIVE and data($body/FML32/NF_CTRL_ACTIVENONACTIVE)=2)
             then &lt;general-status&gt;4&lt;/general-status&gt;
            else() 
        }
(: v.1.2 end :)



    &lt;/wsdl:getContractListRequest&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>