<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace fml="";
declare namespace WBKFault="http://bzwbk.com/services/faults/";
declare namespace wsdl-jv="http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ser-root="http://jv.pl/webservice/";
declare variable $body external;


  &lt;soap-env:Body&gt;
	{
          let $fault := $body/soap:Fault/detail/ser-root:faultResponse/wsdl-jv:faultResponse 
          return
               
      	     if( $fault and string-length(data($fault/status/error-msg))&gt;0)
                 then
                      &lt;FML32&gt;   
                       &lt;NF_ERROR_DESCRIPTION &gt;{
                        substring(concat(data($fault/status/error-msg),concat(':',data($fault/status/error-details))),1,512)
                      }&lt;/NF_ERROR_DESCRIPTION &gt;
                       &lt;NF_ERROR_CODE &gt;{data($fault/status/@error-code)}&lt;/NF_ERROR_CODE &gt; 
                     &lt;/FML32&gt;
                 else 
                     &lt;FML32&gt;   
                       &lt;NF_ERROR_DESCRIPTION &gt;Błąd krytyczny usługi STREAMLINE&lt;/NF_ERROR_DESCRIPTION &gt;
                       &lt;NF_ERROR_CODE&gt;0&lt;/NF_ERROR_CODE&gt; 
                     &lt;/FML32&gt;
               
               
          
	}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>