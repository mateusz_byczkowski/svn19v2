<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function xf:short2bool($short as xs:int) as xs:string {
	if ($short &gt;= 1)
		then "true"
		else "false"
};


declare function getElementsForLoanAccountPastDueList($parm as element(fml:FML32)) as element()
{

&lt;ns0:loanAccountPastDueList&gt;
  {
	let $PastDueNumber         := $parm/NF_LOAAPD_PASTDUENUMBER
	let $PastDueTyp            := $parm/NF_LOAAPD_PASTDUETYP
    let $PastDueRequiredAmount := $parm/NF_LOAAPD_PASTDUEREQUIREDA
    let $PastDuePrincipalAmt   := $parm/NF_LOAAPD_PASTDUEPRINCIPAL
    let $PastDueInterestAmt    := $parm/NF_LOAAPD_PASTDUEINTERESTA
    let $PastDueLateInt        := $parm/NF_LOAAPD_PASTDUELATEINT
    let $PastDueFee            := $parm/NF_LOAAPD_PASTDUEFEE

    for $PastDueDate at $occ in $parm/NF_LOAAPD_PASTDUEDATE
    return
    &lt;ns3:LoanAccountPastDue&gt;
      &lt;ns3:pastDueNumber?&gt;{data($PastDueNumber[$occ])}&lt;/ns3:pastDueNumber&gt;
      &lt;ns3:pastDueTyp?&gt;{data($PastDueTyp[$occ])}&lt;/ns3:pastDueTyp&gt;
      &lt;ns3:pastDueDate?&gt;{data($PastDueDate)}&lt;/ns3:pastDueDate&gt;
      &lt;ns3:pastDueRequiredAmount?&gt;{data($PastDueRequiredAmount[$occ])}&lt;/ns3:pastDueRequiredAmount&gt;
      &lt;ns3:pastDuePrincipalAmt?&gt;{data($PastDuePrincipalAmt[$occ])}&lt;/ns3:pastDuePrincipalAmt&gt;
      &lt;ns3:pastDueInterestAmt?&gt;{data($PastDueInterestAmt[$occ])}&lt;/ns3:pastDueInterestAmt&gt;
      &lt;ns3:pastDueLateInt?&gt;{data($PastDueLateInt[$occ])}&lt;/ns3:pastDueLateInt&gt;
      &lt;ns3:pastDueFee?&gt;{data($PastDueFee[$occ])}&lt;/ns3:pastDueFee&gt;
    &lt;/ns3:LoanAccountPastDue&gt;
  }
&lt;/ns0:loanAccountPastDueList&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  {getElementsForLoanAccountPastDueList($parm)}
  &lt;ns0:bcd&gt;
    &lt;ns5:BusinessControlData&gt;
      &lt;ns5:pageControl&gt;
        &lt;ns4:PageControl&gt;
          &lt;ns4:hasNext?&gt;{xf:short2bool(data($parm/NF_PAGEC_HASNEXT))}&lt;/ns4:hasNext&gt;
          &lt;ns4:navigationKeyDefinition?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns4:navigationKeyDefinition&gt;
          &lt;ns4:navigationKeyValue?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns4:navigationKeyValue&gt;
        &lt;/ns4:PageControl&gt;
      &lt;/ns5:pageControl&gt;
    &lt;/ns5:BusinessControlData&gt;
  &lt;/ns0:bcd&gt;
  &lt;ns0:pastDueSummary&gt;
    &lt;ns3:LoanAccountPastDue&gt;
      &lt;ns3:pastDueRequiredAmount?&gt;{data($parm/NF_LOAAPD_PASTDUEREQUIREDA2)}&lt;/ns3:pastDueRequiredAmount&gt;
      &lt;ns3:pastDuePrincipalAmt?&gt;{data($parm/NF_LOAAPD_PASTDUEPRINCIPAL2)}&lt;/ns3:pastDuePrincipalAmt&gt;
      &lt;ns3:pastDueInterestAmt?&gt;{data($parm/NF_LOAAPD_PASTDUEINTERESTA2)}&lt;/ns3:pastDueInterestAmt&gt;
      &lt;ns3:pastDueLateInt?&gt;{data($parm/NF_LOAAPD_PASTDUELATEINT2)}&lt;/ns3:pastDueLateInt&gt;
      &lt;ns3:pastDueFee?&gt;{data($parm/NF_LOAAPD_PASTDUEFEE2)}&lt;/ns3:pastDueFee&gt;
    &lt;/ns3:LoanAccountPastDue&gt;
  &lt;/ns0:pastDueSummary&gt;
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>