<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>request dla wzbudzenia procesu workflow</con:description>
  <con:xquery><![CDATA[declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace soapenc="http://schemas.xmlsoap.org/soap/encoding/";
declare namespace crw="http://bzwbk.com/crw";
declare namespace tns="http://bzwbk.com/workflow";

declare variable $body external;

declare function local:creditCardApplicationBZWBK24Process($req) as element() {
        <postedMessage>
               <processName>creditCardApplicationBZWBK24Process</processName>
               <operationName>newCreditApplicationForm</operationName>
               <variables>
                    <entry>
                         <key>ID</key>
                         <value>{ data($req/crw:number)}</value>
                    </entry>
                    <entry>
                         <key>TYPE</key>
                         <value>C</value>
                    </entry>
               </variables>
         </postedMessage>
};

declare function local:creditCardApplicationInternetProcess($req) as element() {
        <postedMessage>
               <processName>creditCardApplicationInternetProcess</processName>
                <operationName>newCreditApplicationForm</operationName>
                <variables>
                    <entry>
                         <key>ID</key>
                         <value>{ data($req/crw:number)}</value>
                    </entry>
                    <entry>
                         <key>TYPE</key>
                         <value>C</value>
                    </entry>
               </variables>
         </postedMessage>
};

declare function local:creditCardApplicationBZWBK24BBCProcess($req) as element() {
        <postedMessage>
               <processName>creditCardApplicationBZWBK24BBCProcess</processName>
                <operationName>newCreditApplicationForm</operationName>
                <variables>
                    <entry>
                         <key>ID</key>
                         <value>{ data($req/crw:number)}</value>
                    </entry>
                    <entry>
                         <key>TYPE</key>
                         <value>C</value>
                    </entry>
               </variables>
         </postedMessage>
};

declare function local:loanApplicationBZWBK24Process($req) as element() {
        <postedMessage>
               <processName>loanApplicationBZWBK24Process</processName>
                <operationName>newCreditApplicationForm</operationName>
                <variables>
                    <entry>
                         <key>ID</key>
                         <value>{ data($req/crw:number)}</value>
                    </entry>
                    <entry>
                         <key>TYPE</key>
                         <value>A</value>
                    </entry>
               </variables>
         </postedMessage>
};

declare function local:loanApplicationInternetProcess($req) as element() {
        <postedMessage>
               <processName>loanApplicationInternetProcess</processName>
                <operationName>newCreditApplicationForm</operationName>
                <variables>
                    <entry>
                         <key>ID</key>
                         <value>{ data($req/crw:number)}</value>
                    </entry>
                    <entry>
                         <key>TYPE</key>
                         <value>A</value>
                    </entry>
               </variables>
         </postedMessage>
};

declare function local:loanApplicationBZWBK24BBCProcess($req) as element() {
        <postedMessage>
               <processName>loanApplicationBZWBK24BBCProcess</processName>
                <operationName>newLoanApplicationForm</operationName>
                <variables>
                    <entry>
                         <key>ID</key>
                         <value>{ data($req/crw:number)}</value>
                    </entry>
                    <entry>
                         <key>TYPE</key>
                         <value>A</value>
                    </entry>
               </variables>
         </postedMessage>
};

declare function local:error() as element() {
         <error>Error translating request message</error>
};

<soap-env:Body>
{
                if ($body/crw:notification/crw:entityType="4700")
                then if($body/crw:notification/crw:variables/crw:entry[crw:key='SOURCE']/crw:value="1000")
                        then local:creditCardApplicationBZWBK24Process($body/crw:notification)     
                        else  if($body/crw:notification/crw:variables/crw:entry[crw:key='SOURCE']/crw:value="1001")
                        then local:creditCardApplicationInternetProcess($body/crw:notification) 
                        else  if($body/crw:notification/crw:variables/crw:entry[crw:key='SOURCE']/crw:value="1010") 
                        then local:creditCardApplicationBZWBK24BBCProcess($body/crw:notification)
                        else local:error()
                else  if ($body/crw:notification/crw:entityType="4100")
                then if($body/crw:notification/crw:variables/crw:entry[crw:key='SOURCE']/crw:value="1000")
                        then local:loanApplicationBZWBK24Process($body/crw:notification)     
                        else if($body/crw:notification/crw:variables/crw:entry[crw:key='SOURCE']/crw:value="1001")
                        then local:loanApplicationInternetProcess($body/crw:notification)
                        else if($body/crw:notification/crw:variables/crw:entry[crw:key='SOURCE']/crw:value="1010")
                        then local:loanApplicationBZWBK24BBCProcess($body/crw:notification)
                        else local:error() 
                else   local:error()                             	
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>