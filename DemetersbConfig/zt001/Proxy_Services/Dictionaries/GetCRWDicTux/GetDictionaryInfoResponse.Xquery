<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/slowniki/services/dictionary/infobase";
declare function xf:map_GetDictionaryInfoTuxResponse($req as element(urn:getDictionaryInfoResponse))
	as element(fml:FML32) {
	&lt;fml:FML32&gt;
			{
				&lt;fml:PT_DESCRIPTION?&gt;{ data($req/description) }&lt;/fml:PT_DESCRIPTION&gt;
			}
			{
				&lt;fml:PT_POLISH_NAME?&gt;{ data($req/name) }&lt;/fml:PT_POLISH_NAME&gt;
			}
			{
				&lt;fml:PT_POLISH_CATEGORY_NAME?&gt;{ data($req/keyName) }&lt;/fml:PT_POLISH_CATEGORY_NAME&gt;
			}
                       {
                                for $valueName in $req/valueNames return
                                (
                                          &lt;fml:PT_ATTRIBUTE_NAME?&gt;{ (data($valueName)) }&lt;/fml:PT_ATTRIBUTE_NAME&gt;                                
                                )
                        }
		        {
				for $value in $req/values return
                                (
				        &lt;fml:PT_ATTRIBUTE_TYPE&gt;{ data($value/key) }&lt;/fml:PT_ATTRIBUTE_TYPE&gt;,
                                        &lt;fml:E_REC_COUNT&gt;{ count($value/value) }&lt;/fml:E_REC_COUNT&gt;,
					for $innerValue in $value/value return
					(
						&lt;fml:PT_VALUE&gt;{ data($innerValue) }&lt;/fml:PT_VALUE&gt;
					)
				)
			}
	&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:map_GetDictionaryInfoTuxResponse($body/urn:getDictionaryInfoResponse) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>