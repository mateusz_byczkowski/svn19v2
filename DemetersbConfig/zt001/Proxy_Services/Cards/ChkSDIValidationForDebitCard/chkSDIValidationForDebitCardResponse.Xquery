<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:crddict.dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";
declare namespace ns4="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function sourceValue2Boolean ($parm as xs:anyType,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForValidationErrorList($parm as element(fml:FML32)) as element()
{

&lt;ns3:validationErrorList&gt;
  {
    for $x at $occ in $parm/NF_CSDIVE_CRDSDIVALIDATION
    return
    &lt;ns0:CrdSDIValidationError&gt;
      &lt;ns0:crdSDIValidationError?&gt;{data($parm/NF_CSDIVE_CRDSDIVALIDATION[$occ])}&lt;/ns0:crdSDIValidationError&gt;
    &lt;/ns0:CrdSDIValidationError&gt;
  }
&lt;/ns3:validationErrorList&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
    &lt;ns3:invokeResponse&gt;
     &lt;ns3:isSDIValidationOK&gt;
       &lt;ns5:BooleanHolder&gt;
          &lt;ns5:value?&gt;{sourceValue2Boolean (data($parm/NF_BOOLEH_VALUE),"1")}&lt;/ns5:value&gt;
       &lt;/ns5:BooleanHolder&gt;
     &lt;/ns3:isSDIValidationOK&gt;
     {getElementsForValidationErrorList($parm)}
   &lt;/ns3:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>