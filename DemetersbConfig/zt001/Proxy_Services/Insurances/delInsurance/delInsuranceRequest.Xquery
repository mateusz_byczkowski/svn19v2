<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:insurance.entities.be.dcl";
declare namespace ns4="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:anyType) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};

declare function getFields($parm as element(ns4:header), $parm2 as element(ns4:invoke)) as element()*
{

&lt;DC_TRN_ID?&gt;{data($parm/ns4:transHeader/ns4:transId)}&lt;/DC_TRN_ID&gt;
,
&lt;DC_UZYTKOWNIK?&gt;{concat("SKP:", data($parm/ns4:msgHeader/ns4:userId))}&lt;/DC_UZYTKOWNIK&gt;
,
&lt;DC_ODDZIAL?&gt;{chkUnitId(data($parm/ns4:msgHeader/ns4:unitId))}&lt;/DC_ODDZIAL&gt;
,
&lt;DC_RODZAJ_RACHUNKU&gt;IS&lt;/DC_RODZAJ_RACHUNKU&gt;
,
&lt;DC_NR_RACHUNKU?&gt;{data($parm2/ns4:insurancePolicyAcc/ns3:InsurancePolicyAcc/ns3:policyRefNum)}&lt;/DC_NR_RACHUNKU&gt;

};


&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFields($header/ns4:header, $body/ns4:invoke)}
&lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>