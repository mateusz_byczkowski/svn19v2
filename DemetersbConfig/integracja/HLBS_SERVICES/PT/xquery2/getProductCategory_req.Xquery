<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductarea_resp/";
declare namespace ns0 = "urn:productstree.entities.be.dcl";

declare function xf:getProductCategory_req($entity as element())
    as element() {
        &lt;FML32&gt;
        	&lt;PT_ID_AREA&gt;{ data($entity/ns0:idProductArea) }&lt;/PT_ID_AREA&gt;
			&lt;PT_ID_CATEGORY&gt;{ data($entity/ns0:idProductCategory) }&lt;/PT_ID_CATEGORY&gt;
        &lt;/FML32&gt;
};

declare variable $entity as element() external;

xf:getProductCategory_req($entity)</con:xquery>
</con:xqueryEntry>