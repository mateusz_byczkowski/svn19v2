<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductDefinitionListInPackage_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";

declare function xf:getProductDefinitionListInPackage_req ($entity as element(dcl:ProductPackage)) as element(FML32) {
&lt;FML32&gt;
     &lt;PT_ID_PACKAGE&gt;{ data( $entity/dcl:idProductPackage ) }&lt;/PT_ID_PACKAGE&gt;
&lt;/FML32&gt;
};

declare variable $entity as element(dcl:ProductPackage) external;
xf:getProductDefinitionListInPackage_req($entity)</con:xquery>
</con:xqueryEntry>