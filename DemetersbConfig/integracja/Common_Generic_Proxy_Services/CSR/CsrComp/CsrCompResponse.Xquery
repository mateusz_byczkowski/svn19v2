<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCsrCompResponse($fml as element(fml:FML32))
	as element(m:CsrCompResponse) {
		&lt;m:CsrCompResponse&gt;
                        &lt;m:CompBlok&gt;0&lt;/m:CompBlok&gt;
		        &lt;m:CompTekst&gt;&lt;![CDATA[ ]]&gt;&lt;/m:CompTekst&gt;
			{
				if($fml/fml:C_STATUS)
					then &lt;m:Status&gt;{ data($fml/fml:C_STATUS) }&lt;/m:Status&gt;
					else ()
			}
			&lt;m:CompPlik&gt;&lt;![CDATA[ ]]&gt;&lt;/m:CompPlik&gt;
			{
				if($fml/fml:C_COMP_PEP)
					then &lt;m:NazwaKlienta&gt;{ data($fml/fml:C_COMP_PEP) }&lt;/m:NazwaKlienta&gt;
					else ()
			}
		&lt;/m:CsrCompResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCsrCompResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>