<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCsrCompRequest($req as element(m:CsrCompRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:NazwaKlienta)
					then &lt;fml:C_COMP_PEP&gt;{ data($req/m:NazwaKlienta) }&lt;/fml:C_COMP_PEP&gt;
					else ()
			}
                       &lt;fml:C_PEP&gt;0&lt;/fml:C_PEP&gt;
                       &lt;fml:C_DATA_PEP&gt;{substring(string(current-date()),1,10)}&lt;/fml:C_DATA_PEP&gt;
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCsrCompRequest($body/m:CsrCompRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>