<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddDictCatResponse($fml as element(fml:FML32))
	as element(m:CRMAddDictCatResponse) {
		&lt;m:CRMAddDictCatResponse&gt;
			{
				if($fml/fml:CI_ID_SLOWNIKA)
					then &lt;m:IdSlownika&gt;{ data($fml/fml:CI_ID_SLOWNIKA) }&lt;/m:IdSlownika&gt;
					else ()
			}
			{
				if($fml/fml:CI_NAZWA_SLOWNIKA)
					then &lt;m:NazwaSlownika&gt;{ data($fml/fml:CI_NAZWA_SLOWNIKA) }&lt;/m:NazwaSlownika&gt;
					else ()
			}
		&lt;/m:CRMAddDictCatResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMAddDictCatResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>