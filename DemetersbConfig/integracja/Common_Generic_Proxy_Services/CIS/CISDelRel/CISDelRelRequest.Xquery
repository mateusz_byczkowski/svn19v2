<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISDelRelRequest($req as element(m:CISDelRelRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:NumerKlientaRel)
					then &lt;fml:DC_NUMER_KLIENTA_REL&gt;{ data($req/m:NumerKlientaRel) }&lt;/fml:DC_NUMER_KLIENTA_REL&gt;
					else ()
			}
			{
				if($req/m:TypRelacji)
					then &lt;fml:DC_TYP_RELACJI&gt;{ data($req/m:TypRelacji) }&lt;/fml:DC_TYP_RELACJI&gt;
					else ()
			}
			{
				if($req/m:RelacjaOdwrotna)
					then &lt;fml:CI_RELACJA_ODWROTNA&gt;{ data($req/m:RelacjaOdwrotna) }&lt;/fml:CI_RELACJA_ODWROTNA&gt;
					else ()
			}
                        {
                                if($req/m:TypRelOdwrotnej)
                                        then &lt;fml:CI_TYP_REL_ODWROTNEJ&gt;{ data($req/m:TypRelOdwrotnej) }&lt;/fml:CI_TYP_REL_ODWROTNEJ&gt;
                                        else ()
                        }
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCISDelRelRequest($body/m:CISDelRelRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>