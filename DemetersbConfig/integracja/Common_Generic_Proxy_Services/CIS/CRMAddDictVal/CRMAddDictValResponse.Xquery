<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddDictValResponse($fml as element(fml:FML32))
	as element(m:CRMAddDictValResponse) {
		&lt;m:CRMAddDictValResponse&gt;
			{
				if($fml/fml:CI_ID_SLOWNIKA)
					then &lt;m:IdSlownika&gt;{ data($fml/fml:CI_ID_SLOWNIKA) }&lt;/m:IdSlownika&gt;
					else ()
			}
			{
				if($fml/fml:CI_WARTOSC_SLOWNIKA)
					then &lt;m:WartoscSlownika&gt;{ data($fml/fml:CI_WARTOSC_SLOWNIKA) }&lt;/m:WartoscSlownika&gt;
					else ()
			}
			{
				if($fml/fml:CI_OPIS_WARTOSCI_SLOWNIKA)
					then &lt;m:OpisWartosciSlownika&gt;{ data($fml/fml:CI_OPIS_WARTOSCI_SLOWNIKA) }&lt;/m:OpisWartosciSlownika&gt;
					else ()
			}
			{
				if($fml/fml:CI_WAGA_WARTOSCI_SLOWNIKA)
					then &lt;m:WagaWartosciSlownika&gt;{ data($fml/fml:CI_WAGA_WARTOSCI_SLOWNIKA) }&lt;/m:WagaWartosciSlownika&gt;
					else ()
			}
			{
				if($fml/fml:CI_DOMENA_RODZICA)
					then &lt;m:DomenaRodzica&gt;{ data($fml/fml:CI_DOMENA_RODZICA) }&lt;/m:DomenaRodzica&gt;
					else ()
			}
			{
				if($fml/fml:CI_ID_SLOWNIKA_RODZICA)
					then &lt;m:IdSlownikaRodzica&gt;{ data($fml/fml:CI_ID_SLOWNIKA_RODZICA) }&lt;/m:IdSlownikaRodzica&gt;
					else ()
			}
			{
				if($fml/fml:CI_DODATKOWA_WART_SLOWNIKA)
					then &lt;m:DodatkowaWartSlownika&gt;{ data($fml/fml:CI_DODATKOWA_WART_SLOWNIKA) }&lt;/m:DodatkowaWartSlownika&gt;
					else ()
			}
		&lt;/m:CRMAddDictValResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMAddDictValResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>