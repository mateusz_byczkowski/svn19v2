<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustModHRequest($req as element(m:CRMGetCustModHRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:NumerPaczkiStr)
					then &lt;fml:CI_NUMER_PACZKI_STR&gt;{ data($req/m:NumerPaczkiStr) }&lt;/fml:CI_NUMER_PACZKI_STR&gt;
					else ()
			}
			{
				if($req/m:Kierunek)
					then &lt;fml:CI_KIERUNEK&gt;{ data($req/m:Kierunek) }&lt;/fml:CI_KIERUNEK&gt;
					else ()
			}
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:DataOd)
					then &lt;fml:CI_DATA_OD&gt;{ data($req/m:DataOd) }&lt;/fml:CI_DATA_OD&gt;
					else ()
			}
			{
				if($req/m:DataDo)
					then &lt;fml:CI_DATA_DO&gt;{ data($req/m:DataDo) }&lt;/fml:CI_DATA_DO&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCustModHRequest($body/m:CRMGetCustModHRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>