<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns8="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};




declare function getElementsForAccounts($parm as element(fml:FML32)) as element()
{

&lt;ns0:accounts&gt;
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
    &lt;ns4:Account&gt;
      &lt;ns4:accountNumber&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns4:accountNumber&gt;
      &lt;ns4:customerNumber&gt;{data($parm/NF_ACCOUN_CUSTOMERNUMBER[$occ])}&lt;/ns4:customerNumber&gt;
      &lt;ns4:accountName&gt;{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}&lt;/ns4:accountName&gt;
      &lt;ns4:currentBalance&gt;{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns4:currentBalance&gt;
      &lt;ns4:accountDescription&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns4:accountDescription&gt;
      &lt;ns4:accountType&gt;
        &lt;ns5:AccountType&gt;
          &lt;ns5:accountType&gt;{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns5:accountType&gt;
        &lt;/ns5:AccountType&gt;
      &lt;/ns4:accountType&gt;
      &lt;ns4:currency&gt;
        &lt;ns5:CurrencyCode&gt;
          &lt;ns5:currencyCode&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode&gt;
        &lt;/ns5:CurrencyCode&gt;
      &lt;/ns4:currency&gt;
      &lt;ns4:accountStatus&gt;
        &lt;ns7:AccountStatus&gt;
          (:&lt;ns7:accountStatus&gt;{data($parm/NF_ACCOUS_ACCOUNTSTATUS[$occ])}&lt;/ns7:accountStatus&gt;:)
          (: &lt;ns7:accountStatus&gt;{data($parm/NF_PRODUCT_STAT[$occ])}&lt;/ns7:accountStatus&gt;:)
             &lt;ns7:accountStatus&gt;{data($parm/NF_PRODUCT_STATUSCODE[$occ])}&lt;/ns7:accountStatus&gt;
        &lt;/ns7:AccountStatus&gt;
      &lt;/ns4:accountStatus&gt;
    &lt;/ns4:Account&gt;
  }
&lt;/ns0:accounts&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  {getElementsForAccounts($parm)}
  &lt;ns0:bcd&gt;
    &lt;ns8:BusinessControlData&gt;
      &lt;ns8:pageControl&gt;
        &lt;ns6:PageControl&gt;
          &lt;ns6:hasNext?&gt;{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns6:hasNext&gt;
          &lt;ns6:navigationKeyDefinition&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition&gt;
          &lt;ns6:navigationKeyValue&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue&gt;
        &lt;/ns6:PageControl&gt;
      &lt;/ns8:pageControl&gt;
    &lt;/ns8:BusinessControlData&gt;
  &lt;/ns0:bcd&gt;
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>