<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function booleanToInt($parm as xs:string) as xs:string{
   if ($parm = "true") 
      then  "1"
   else
      "0"
};

declare function getFieldsFromHeader($parm as element(ns4:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns4:msgHeader/ns4:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns4:msgHeader/ns4:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
,
&lt;NF_MSHEAD_UNITID?&gt;{data($parm/ns4:msgHeader/ns4:unitId)}&lt;/NF_MSHEAD_UNITID&gt;
,
&lt;NF_MSHEAD_USERID?&gt;{data($parm/ns4:msgHeader/ns4:userId)}&lt;/NF_MSHEAD_USERID&gt;
,
&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns4:msgHeader/ns4:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns4:msgHeader/ns4:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
,
&lt;NF_TRHEAD_TRANSID?&gt;{data($parm/ns4:transHeader/ns4:transId)}&lt;/NF_TRHEAD_TRANSID&gt;
};

declare function getFieldsFromInvoke($parm as element(ns4:invoke)) as element()*
{

&lt;NF_CTRL_ACTIVENONACTIVE?&gt;{booleanToInt(data($parm/ns4:activeOnly/ns5:BooleanHolder/ns5:value))}&lt;/NF_CTRL_ACTIVENONACTIVE&gt;
,
&lt;NF_ACCOUN_ACCOUNTNUMBER&gt;{data($parm/ns4:account/ns1:Account/ns1:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns4:header)}
    {getFieldsFromInvoke($body/ns4:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>