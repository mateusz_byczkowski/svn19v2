<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:accounts.entities.be.dcl";

declare variable $body as element(soap:Body) external;

&lt;soapenv:Body&gt;
      &lt;urn:invokeResponse&gt;
         &lt;urn:holdOut&gt;
            &lt;!--Optional:--&gt;
            &lt;urn1:Hold&gt;
               &lt;!--Optional:--&gt;
               &lt;urn1:holdNumber&gt;{data($body/fml:FML32/fml:DC_NUMER_BLOKADY)}&lt;/urn1:holdNumber&gt;
            &lt;/urn1:Hold&gt;
         &lt;/urn:holdOut&gt;
      &lt;/urn:invokeResponse&gt;
   &lt;/soapenv:Body&gt;</con:xquery>
</con:xqueryEntry>