<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Dodanie fn:normalize-spaceVersion.$4.2010-10-22</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$fML32OUT1" element="ns1:FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-return element="ns0:invokeResponse" location="chgDenomSpecification.wsdl" ::)


(:~
 :
 : @author  Grzegorz Strawa
 : @version 1.0
 : @since   2010-03-22
 :
 : wersja WSDLa: 04-02-2010 15:57:45
 :
 : $Proxy Services/Till/chgDenomSpecification/chgDenomSpecificationResponse.xq$
 :
 :) 
 
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "";
declare namespace ns3 = "urn:operations.entities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/chgDenomSpecification/chgDenomSpecificationResponse/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";

declare function xf:chgDenomSpecificationResponse($fML32OUT1 as element(ns1:FML32))
    as element(ns0:invokeResponse) {
        &lt;ns0:invokeResponse&gt;
            &lt;ns0:transactionOut&gt;
                &lt;ns3:Transaction&gt;
                    &lt;ns3:transactionStatus&gt;
                        &lt;ns2:TransactionStatus&gt;
                            &lt;ns2:transactionStatus&gt;{ data($fML32OUT1/ns1:TR_STATUS) }&lt;/ns2:transactionStatus&gt;
                        &lt;/ns2:TransactionStatus&gt;
                    &lt;/ns3:transactionStatus&gt;
                &lt;/ns3:Transaction&gt;
            &lt;/ns0:transactionOut&gt;
                        &lt;ns0:backendResponse&gt;
                &lt;ns3:BackendResponse&gt;
                 {
                if (data($fML32OUT1/ns1:TR_DATA_KSIEG) ) then 
                   
                    &lt;ns3:icbsDate&gt;{  
                        let $transactionDate := $fML32OUT1/ns1:TR_DATA_KSIEG
					return
						fn:concat(
							fn:substring(data($transactionDate ), 7, 4),
							'-',
							fn:substring(data($transactionDate ), 4, 2),
							'-',
							fn:substring(data($transactionDate ), 1, 2)
							)
						}&lt;/ns3:icbsDate&gt;
				else ()
				}
				
                {
                if (data($fML32OUT1/ns1:TR_CZAS_ODPOWIEDZI) ) then 
                   
                    &lt;ns3:dateTime&gt;{  
                        let $czasOdpowiedzi := $fML32OUT1/ns1:TR_CZAS_ODPOWIEDZI
					return
						fn:concat(
							fn:substring(data($czasOdpowiedzi ), 1, 10),
							'T',
							fn:substring(data($czasOdpowiedzi ), 12, 15)
							)
						}&lt;/ns3:dateTime&gt;
				else ()
				}
                                   {               
                   if (data($fML32OUT1/ns1:TR_TXN_SESJA) ) then 
						&lt;ns3:icbsSessionNumber&gt;{ xs:string( data($fML32OUT1/ns1:TR_TXN_SESJA) ) }&lt;/ns3:icbsSessionNumber&gt; 
                    else ()
                   }
                
                   {                
                    if (data($fML32OUT1/ns1:TR_TXN_NR)) then 
						&lt;ns3:psTransactionNumber&gt;{ xs:string( data($fML32OUT1/ns1:TR_TXN_NR) ) }&lt;/ns3:psTransactionNumber&gt; 
                    else ()
                   }
                
                   {                
                    if (data($fML32OUT1/ns1:TR_ID_REF )) then 
						&lt;ns3:transactionRefNumber&gt;{ xs:string( data($fML32OUT1/ns1:TR_ID_REF) ) }&lt;/ns3:transactionRefNumber&gt; 
                    else ()
                   }
                  
                  
                         &lt;ns3:beUserId?&gt;{ data($fML32OUT1/ns1:TR_UZYTKOWNIK) }&lt;/ns3:beUserId&gt;
                         &lt;ns3:beErrorCodeList&gt;
                        {
                            for $FML320  in ($fML32OUT1/ns1:TR_KOD_BLEDU_1 union $fML32OUT1/ns1:TR_KOD_BLEDU_2 union $fML32OUT1/ns1:TR_KOD_BLEDU_3 union $fML32OUT1/ns1:TR_KOD_BLEDU_4 union $fML32OUT1/ns1:TR_KOD_BLEDU_5)  
                            return
                                if ((fn:boolean(fn:normalize-space($FML320)!="") and fn:boolean($FML320!="000"))) then
                                    (&lt;ns3:BeErrorCode&gt;
                                    &lt;ns3:errorCode&gt;
                                    &lt;ns5:BackendErrorCode&gt;                                    
                                    &lt;ns5:errorCode&gt;{ data($FML320) }&lt;/ns5:errorCode&gt;                                   
                                    &lt;/ns5:BackendErrorCode&gt;
                                    &lt;/ns3:errorCode&gt;
                                    &lt;/ns3:BeErrorCode&gt;)
                                else 
                                    ()
                        }
					&lt;/ns3:beErrorCodeList&gt;
                &lt;/ns3:BackendResponse&gt;
            &lt;/ns0:backendResponse&gt;

        &lt;/ns0:invokeResponse&gt;
};

declare variable $fML32OUT1 as element(ns1:FML32) external;

&lt;soap-env:Body&gt;{
  xf:chgDenomSpecificationResponse($fML32OUT1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>