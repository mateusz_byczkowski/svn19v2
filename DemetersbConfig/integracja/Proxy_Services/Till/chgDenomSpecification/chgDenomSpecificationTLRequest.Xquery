<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-27</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="chgDenomSpecification.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="chgDenomSpecification.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns0:invokeResponse" location="chgDenomSpecification.wsdl" ::)
(:: pragma bea:global-element-return element="ns5:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns2 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns6 = "http://bzwbk.com/nfe/transactionLog";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/chgDenomSpecification/chgDenomSpecificationTLRequest/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:entities.be.dcl";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare function xf:chgDenomSpecificationTLRequest($invoke1 as element(ns0:invoke),
    $header1 as element(ns0:header),
    $invokeResponse1 as element(ns0:invokeResponse),
    $faultResponse as element() ?)

    as element(ns6:transactionLogEntry) {
        &lt;ns6:transactionLogEntry&gt;
            {
                for $businessTransactionType in $invoke1/ns0:transaction/ns7:Transaction/ns7:businessTransactionType/ns5:BusinessTransactionType/ns5:businessTransactionType
                return
                    &lt;businessTransactionType&gt;{ data($businessTransactionType) }&lt;/businessTransactionType&gt;
            }
            {
                for $cashTransactionBasketID in $invoke1/ns0:transaction/ns7:Transaction/ns7:cashTransactionBasketID
                return
                    &lt;cashTransactionBasketID&gt;{ data($cashTransactionBasketID) }&lt;/cashTransactionBasketID&gt;
            }
            {
                for $csrMessageType in $invoke1/ns0:transaction/ns7:Transaction/ns7:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType
                return
                    &lt;csrMessageType&gt;{ data($csrMessageType) }&lt;/csrMessageType&gt;
            }
            {
                for $dtTransactionType in $invoke1/ns0:transaction/ns7:Transaction/ns7:dtTransactionType/ns5:DtTransactionType/ns5:dtTransactionType
                return
                    &lt;dtTransactionType&gt;{ data($dtTransactionType) }&lt;/dtTransactionType&gt;
            }
            &lt;executor&gt;
                {
                    for $branchCode in $invoke1/ns0:branchCode/ns4:BranchCode/ns4:branchCode
                    return
                        &lt;branchNumber&gt;{ xs:int( data($branchCode) ) }&lt;/branchNumber&gt;
                }
                {
                    for $userID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns8:User/ns8:userID
                    return
                        &lt;executorID&gt;{ data($userID) }&lt;/executorID&gt;
                }
                {
                    for $userFirstName in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns8:User/ns8:userFirstName
                    return
                        &lt;firstName&gt;{ data($userFirstName) }&lt;/firstName&gt;
                }
                {
                    for $userLastName in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns8:User/ns8:userLastName
                    return
                        &lt;lastName&gt;{ data($userLastName) }&lt;/lastName&gt;
                }
                {
                    for $tellerID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID
                    return
                        &lt;tellerID&gt;{ data($tellerID) }&lt;/tellerID&gt;
                }
                {
                    for $tillID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID
                    return
                        &lt;tillNumber&gt;{ data($tillID) }&lt;/tillNumber&gt;
                }
                {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
                (
                    for $beUserId in $invokeResponse1/ns0:backendResponse/ns6:BackendResponse/ns6:beUserId
                    return

                &lt;userID&gt;{ data($beUserId) }&lt;/userID&gt;
                )
                	else
                	()

                }

(:                {   :)
(:                    for $userID in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns8:User/ns8:userID  :)
(:                    return   :)
(:                        &lt;userID&gt;{ data($userID) }&lt;/userID&gt;   :)
(:                }  :)
            &lt;/executor&gt;
            &lt;hlbsName&gt;chgDenomSpecification&lt;/hlbsName&gt;
            {
                for $putDownDate in $invoke1/ns0:transaction/ns7:Transaction/ns7:putDownDate
                return
                    &lt;putDownDate&gt;{ xs:date( data($putDownDate) ) }&lt;/putDownDate&gt;
            }
            &lt;timestamp&gt;{ data($header1/ns0:msgHeader/ns0:timestamp) }&lt;/timestamp&gt;
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                for $BackendResponse in $invokeResponse1/ns0:backendResponse/ns7:BackendResponse
                return
                    &lt;tlBackendResponse&gt;
                        {
                            for $balanceCredit in $BackendResponse/ns7:balanceCredit
                            return
                                &lt;balanceCredit?&gt;{ xs:decimal( data($balanceCredit) ) }&lt;/balanceCredit&gt;
                        }
                        {
                            for $balanceDebit in $BackendResponse/ns6:balanceDebit
                            return
                                &lt;balanceDebit?&gt;{ xs:decimal( data($balanceDebit) ) }&lt;/balanceDebit&gt;
                        }
                        {
                            for $dateTime in $BackendResponse/ns7:dateTime
                            return
                                &lt;dateTime?&gt;{ data($dateTime) }&lt;/dateTime&gt;
                        }
                        {
                            for $icbsDate in $BackendResponse/ns7:icbsDate
                            return
                                &lt;icbsDate?&gt;{ xs:date( data($icbsDate) ) }&lt;/icbsDate&gt;
                        }
                        {
                            for $icbsSessionNumber in $BackendResponse/ns7:icbsSessionNumber
                            return
                                &lt;icbsSessionNumber?&gt;{ data($icbsSessionNumber) }&lt;/icbsSessionNumber&gt;
                        }
                        {
                            for $psTransactionNumber in $BackendResponse/ns7:psTransactionNumber
                            return
                                &lt;psTransactionNumber?&gt;{ data($psTransactionNumber) }&lt;/psTransactionNumber&gt;
                        }
                        {
                            for $BeErrorCode in $BackendResponse/ns7:beErrorCodeList/ns7:BeErrorCode
                            return
                                &lt;tlErrorCodeList?&gt;
                                    &lt;errorCode?&gt;
                                        {
                                            for $errorCode in $BeErrorCode/ns7:errorCode/ns5:BackendErrorCode/ns5:errorCode
                                            return
                                                &lt;errorCode&gt;{ data($errorCode) }&lt;/errorCode&gt;
                                        }
                                        &lt;systemId&gt;1&lt;/systemId&gt;
                                    &lt;/errorCode&gt;
                                &lt;/tlErrorCodeList&gt;
                        }
                        {
                            for $transactionRefNumber in $BackendResponse/ns7:transactionRefNumber
                            return
                                &lt;transactionRefNumber&gt;{ data($transactionRefNumber) }&lt;/transactionRefNumber&gt;
                        }
                    &lt;/tlBackendResponse&gt;
                    )
                            
                	else
                	()
            }


            {
                for $CurrencyCash in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:currencyCashList/ns1:CurrencyCash
                return
                    &lt;tlCurrencyCashList&gt;
                       {
                            for $amount in $CurrencyCash/ns1:amount
                            return
                                &lt;amount&gt;{ xs:decimal( data($amount) ) }&lt;/amount&gt;
                        }
                        {
                            for $currencyCode in $CurrencyCash/ns1:currency/ns4:CurrencyCode/ns4:currencyCode
                            return
                                &lt;currency?&gt;{ data($currencyCode) }&lt;/currency&gt;
                         }
                        {
                            for $DenominationSpecification in $CurrencyCash/ns1:denominationSpecificationList/ns1:DenominationSpecification
                            return
                                &lt;tlDenominationSpecList?&gt;
                                    {
                                        let $denominationID := $DenominationSpecification/ns1:denomination/ns2:DenominationDefinition/ns2:denominationID
                                        let $itemsNumber := $DenominationSpecification/ns1:itemsNumber
                                        return
        									if (data($itemsNumber) != 0) then
                                        	(
                                        		&lt;denomination&gt;{ data($denominationID) }&lt;/denomination&gt;,
                                            	&lt;itemsNumber&gt;{ data($itemsNumber) }&lt;/itemsNumber&gt;
                                            )
                                            else
                                            ()
                                    }
                                &lt;/tlDenominationSpecList&gt;
                        }
                    &lt;/tlCurrencyCashList&gt;
            }
            {
                for $transactionDate in $invoke1/ns0:transaction/ns7:Transaction/ns7:transactionDate
                return
                    &lt;transactionDate&gt;{ xs:date(data($transactionDate) ) }&lt;/transactionDate&gt;
            }
            &lt;tlExceptionDataList?&gt;
            {
            	let $errorCode1 := data($faultResponse//err:exceptionItem/err:errorCode1)
            	let $errorCode2 := data($faultResponse//err:exceptionItem/err:errorCode2)
                let $errorDescription := data($faultResponse//err:exceptionItem/err:errorDescription)
            	return
                   if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') != '') then
            		(
		                &lt;errorCode1?&gt;{ xs:int($errorCode1) }&lt;/errorCode1&gt;,
        		        &lt;errorCode2?&gt;{ xs:int($errorCode2) }&lt;/errorCode2&gt;,
                		&lt;errorDescription?&gt;{ $errorDescription }&lt;/errorDescription&gt;
                	)
                	else
                	()
			}
            &lt;/tlExceptionDataList&gt;

            &lt;transactionID&gt;{ data($header1/ns0:transHeader/ns0:transId) }&lt;/transactionID&gt;
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                for $transactionStatus in $invokeResponse1/ns0:transactionOut/ns7:Transaction/ns7:transactionStatus/ns4:TransactionStatus/ns4:transactionStatus
                return
                    &lt;transactionStatus&gt;{ data($transactionStatus) }&lt;/transactionStatus&gt;
                	)
                	else
                	()
            }
        &lt;/ns6:transactionLogEntry&gt;
};

declare variable $invoke1 as element(ns0:invoke) external;
declare variable $header1 as element(ns0:header) external;
declare variable $invokeResponse1 as element(ns0:invokeResponse) external;
declare variable $faultResponse  as element() ? external;

&lt;soap-env:Body&gt;{
xf:chgDenomSpecificationTLRequest($invoke1,
    $header1,
    $invokeResponse1,
    $faultResponse)

}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>