<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-10-22</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:ceke.entities.be.dcl";
declare namespace ns0="urn:cif.entities.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForCustomerCEKEList($parm as element(soap:Body)) as element()
{

&lt;ns0:customerCEKEList&gt;
  {
    for $x at $occ in 1
    return
    &lt;ns1:CustomerCEKE&gt;
      &lt;ns1:nik?&gt;79416755&lt;/ns1:nik&gt;
    &lt;/ns1:CustomerCEKE&gt;
  } 
&lt;/ns0:customerCEKEList&gt;
};
declare function getElementsForCustomersListOut($parm as element(soap:Body)) as element()
{

&lt;ns4:customersListOut&gt;
    &lt;ns0:Customer&gt;
      &lt;ns0:customerNumber?&gt;{data($parm/ns4:invoke/ns4:customersList/ns0:Customer/ns0:customerNumber)}&lt;/ns0:customerNumber&gt;

       {
        if (data($parm/ns4:invoke/ns4:customersList/ns0:Customer/ns0:customerNumber) = "0000004110")
        then  getElementsForCustomerCEKEList($parm)
        else()
        }   

  
    &lt;/ns0:Customer&gt;
&lt;/ns4:customersListOut&gt;
};
declare function getElementsForInvokeResponse($parm as element(soap:Body)) as element()*
{

&lt;ns4:invokeResponse&gt;
  {getElementsForCustomersListOut($parm)}
&lt;/ns4:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>