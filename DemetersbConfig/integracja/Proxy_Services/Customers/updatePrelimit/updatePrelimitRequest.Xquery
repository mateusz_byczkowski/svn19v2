<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-08-12</con:description>
  <con:xquery>declare namespace urn  = "urn:be.services.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string

  return 
    if ($value &lt; 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
    fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-date($dateIn)))
};
&lt;soap-env:Body&gt;
  {
    let $req := $body/urn:invoke

    let $numerKlienta:= $req/urn:numerKlienta
    let $pesel:= $req/urn:pesel
    let $flagaPrzeliczenia:= $req/urn:flagaPrzeliczenia
    let $typWniosku:= $req/urn:typWniosku 
    let $dataWniosku:= $req/urn:dataWniosku 
    let $kwotaKredytu:= $req/urn:kwotaKredytu 
    let $waluta:= $req/urn:waluta 
    let $flagaKlienta:= $req/urn:flagaKlienta 
    let $numerWniosku:= $req/urn:numerWniosku 
    let $kanal:= $req/urn:kanal 
    let $numerRachunku:= $req/urn:numerRachunku
    
    return

    &lt;fml:FML32&gt;
      &lt;fml:DC_NUMER_KLIENTA?&gt;{ data($numerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
      &lt;fml:DC_NR_PESEL?&gt;{ data($pesel) }&lt;/fml:DC_NR_PESEL&gt;
      &lt;fml:DC_FLAGA_PRZELICZENIA?&gt;{ data($flagaPrzeliczenia) }&lt;/fml:DC_FLAGA_PRZELICZENIA&gt;
      &lt;fml:DC_TYP_WNIOSKU?&gt;{ data($typWniosku) }&lt;/fml:DC_TYP_WNIOSKU&gt;
      &lt;fml:DC_DATA_WNIOSKU?&gt;{ xf:mapDate($dataWniosku) }&lt;/fml:DC_DATA_WNIOSKU&gt;
      &lt;fml:DC_KWOTA_KREDYTU?&gt;{ data($kwotaKredytu) }&lt;/fml:DC_KWOTA_KREDYTU&gt;
      &lt;fml:DC_WALUTA?&gt;{ data($waluta) }&lt;/fml:DC_WALUTA&gt;
      &lt;fml:DC_FLAGA_KLIENTA?&gt;{ data($flagaKlienta) }&lt;/fml:DC_FLAGA_KLIENTA&gt;
      &lt;fml:DC_NUMER_WNIOSKU_KREDYTOWEGO?&gt;{ data($numerWniosku) }&lt;/fml:DC_NUMER_WNIOSKU_KREDYTOWEGO&gt;
      &lt;fml:DC_KANAL?&gt;{ data($kanal) }&lt;/fml:DC_KANAL&gt;     
      &lt;fml:DC_NR_RACHUNKU?&gt;{ data($numerRachunku) }&lt;/fml:DC_NR_RACHUNKU&gt;
    &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>