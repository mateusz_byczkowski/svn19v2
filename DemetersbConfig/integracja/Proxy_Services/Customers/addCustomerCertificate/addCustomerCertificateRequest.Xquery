<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-01</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:customercertificatedict.dictionaries.be.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:customercertificates.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{

&lt;DC_MSHEAD_MSGID?&gt;{data($parm/ns6:msgHeader/ns6:msgId)}&lt;/DC_MSHEAD_MSGID&gt;
,
&lt;DC_ODDZIAL?&gt;{data($parm/ns6:msgHeader/ns6:unitId)}&lt;/DC_ODDZIAL&gt;
,
&lt;DC_UZYTKOWNIK?&gt;{concat("SKP:",data($parm/ns6:msgHeader/ns6:userId))}&lt;/DC_UZYTKOWNIK&gt;
,
&lt;DC_TRN_ID?&gt;{data($parm/ns6:transHeader/ns6:transId)}&lt;/DC_TRN_ID&gt;
};

declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{
 &lt;DC_TYP_ZMIANY&gt;A&lt;/DC_TYP_ZMIANY&gt;
,
&lt;DC_DATA_WPROWADZENIA?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:registerDate)}&lt;/DC_DATA_WPROWADZENIA&gt;
,
&lt;DC_NUMER_TELEFONU_PRACA?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:registerUserTelNo)}&lt;/DC_NUMER_TELEFONU_PRACA&gt;
,
&lt;DC_OPIS_2?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:customCertificateDesc)}&lt;/DC_OPIS_2&gt;
,
&lt;DC_CFPOS3?&gt;{distinct-values(data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:customerCertContentList/ns7:CustomerCertificateContent/ns7:contentType/ns0:CertificateContentType/ns0:certificateContentType))}&lt;/DC_CFPOS3&gt;
,
&lt;DC_CFTYP?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:customerCertificateType/ns0:CustomerCertificateType/ns0:customerCertificateType)}&lt;/DC_CFTYP&gt;
,
&lt;DC_OPIS_1?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:customerCertificateType/ns0:CustomerCertificateType/ns0:description)}&lt;/DC_OPIS_1&gt;
,
&lt;DC_NAZWISKO?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:registerUser/ns4:User/ns4:userLastName)}&lt;/DC_NAZWISKO&gt;
,
&lt;DC_UZYTKOWNIK_AUTORYZUJACY?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:registerUser/ns4:User/ns4:userID)}&lt;/DC_UZYTKOWNIK_AUTORYZUJACY&gt;
,
&lt;DC_IMIE?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:registerUser/ns4:User/ns4:userFirstName)}&lt;/DC_IMIE&gt;
,
&lt;DC_NUMER_ODDZIALU?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:registerBranch/ns2:BranchCode/ns2:branchCode)}&lt;/DC_NUMER_ODDZIALU&gt;
,
&lt;DC_CFPOS1?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:certificateKind/ns0:CustomerCertificateKind/ns0:customerCertificateKind)}&lt;/DC_CFPOS1&gt;
,
&lt;DC_CFPOS2?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:certificateDelivery/ns0:CustomerCertificateDelivery/ns0:customerCertificateDelivery)}&lt;/DC_CFPOS2&gt;
,
&lt;DC_CFSTAT?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:customerCertificateStatus/ns0:CustomerCertificateStatus/ns0:customerCertificateStatus)}&lt;/DC_CFSTAT&gt;
,
&lt;DC_NUMER_KLIENTA?&gt;{data($parm/ns6:customerCertificate/ns7:CustomerCertificate/ns7:customer/ns1:Customer/ns1:customerNumber)}&lt;/DC_NUMER_KLIENTA&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvoke($body/ns6:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>