<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-02</con:description>
  <con:xquery>(:Change log 
v.1.0  2010-06-28  PKL  NP2042 Utworzenie 
:)

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:accounts.entities.be.dcl";
declare namespace urn2="urn:cif.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(urn:header)) as element()*
{
  &lt;CI_ID_SPOLKI?&gt;{data($parm/urn:msgHeader/urn:companyId)}&lt;/CI_ID_SPOLKI&gt;
  ,
  &lt;CI_ID_WEW_PRAC?&gt;{data($parm/urn:msgHeader/urn:userId)}&lt;/CI_ID_WEW_PRAC&gt;
  , 
  &lt;BLIB_CALL_ID?&gt;{data($parm/urn:msgHeader/urn:msgId)}&lt;/BLIB_CALL_ID&gt;
};


declare function getFieldsFromInvoke($parm as element(urn:invoke)) as element()*
{
  &lt;DC_NUMER_KLIENTA?&gt;{data($parm/urn:customer/urn2:Customer/urn2:customerNumber)}&lt;/DC_NUMER_KLIENTA&gt;
  ,
  &lt;DC_NR_RACHUNKU?&gt;{data($parm/urn:account/urn1:Account/urn1:accountNumber)}&lt;/DC_NR_RACHUNKU&gt;
  ,
  (: Parametry obecnie ustawiane na sztywno: :)
  &lt;CI_OPCJA&gt;1&lt;/CI_OPCJA&gt;
  ,
  &lt;CI_ID_SYS&gt;1&lt;/CI_ID_SYS&gt;
  ,
  &lt;CI_PRODUCT_AREA_ID&gt;3&lt;/CI_PRODUCT_AREA_ID&gt;
  ,
  &lt;CI_PRODUCT_CATEGORY_ID&gt;0&lt;/CI_PRODUCT_CATEGORY_ID&gt;
  ,
  &lt;DC_NR_RACHUNKU_W_SYSTEMIE/&gt; 
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/urn:header)}
    {getFieldsFromInvoke($body/urn:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>