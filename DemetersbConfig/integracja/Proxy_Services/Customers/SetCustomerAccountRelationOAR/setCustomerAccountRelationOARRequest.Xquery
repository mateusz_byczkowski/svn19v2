<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function getFieldsFromHeader($parm as element(ns5:header)) as element()*
{
(:
&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns5:msgHeader/ns5:msgIg)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns5:msgHeader/ns5:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
,:)
&lt;DC_ODDZIAL?&gt;{data($parm/ns5:msgHeader/ns5:unitId)}&lt;/DC_ODDZIAL&gt;
,
&lt;DC_UZYTKOWNIK?&gt;{concat("SKP:",data($parm/ns5:msgHeader/ns5:userId))}&lt;/DC_UZYTKOWNIK&gt;
,
(:&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns5:msgHeader/ns5:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns5:msgHeader/ns5:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
,:)
&lt;DC_TRN_ID?&gt;{data($parm/ns5:transHeader/ns5:transId)}&lt;/DC_TRN_ID&gt;
};
declare function getFieldsFromInvoke($parm as element(ns5:invoke)) as element()*
{
let $accountNumber := data($parm/ns5:account/ns0:Account/ns0:accountNumber)
let $accountNumberLength := string-length($accountNumber)
return           
if ($accountNumberLength &gt;  10) 
    then  &lt;fml:DC_NR_RACHUNKU&gt;{substring(data($accountNumber),$accountNumberLength - 9,10) }&lt;/fml:DC_NR_RACHUNKU&gt;
    else   &lt;fml:DC_NR_RACHUNKU&gt;{data($accountNumber) }&lt;/fml:DC_NR_RACHUNKU&gt;
    
,
&lt;DC_NUMER_KLIENTA?&gt;{data($parm/ns5:customer/ns3:Customer/ns3:customerNumber)}&lt;/DC_NUMER_KLIENTA&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns5:header)}
    {getFieldsFromInvoke($body/ns5:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>