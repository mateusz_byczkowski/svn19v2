<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-12-17</con:description>
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:cif.entities.be.dcl";
declare namespace fml = "";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $header as element(soap-env:Header) external;
declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body&gt;
  {
    let $req  := $body/m:invoke/m:customer/m1:Customer
    let $reqh := $header/m:header
    return

    &lt;fml:FML32&gt;
      &lt;fml:DC_MSHEAD_MSGID?&gt;{ data($reqh/m:msgHeader/m:msgId) }&lt;/fml:DC_MSHEAD_MSGID&gt;
      {if($req/m1:customerNumber)
        then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m1:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA&gt;
        else ()
      }
    &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>