<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>PT58 - dodanie Body.Version.$1.2011-04-13</con:description>
  <con:xquery>declare namespace ns4 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/FindCustomers/RequestTransform2/";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace ns1 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns-1 = "urn:ceke.entities.be.dcl";
declare namespace ns3 = "urn:applicationul.entities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns6 = "urn:baseauxentities.be.dcl";

declare function local:YMDToDMY($dateYMD as xs:string) as xs:string {
   if ($dateYMD eq '') then ''
   else fn:string-join((fn:substring($dateYMD, 9, 2), fn:substring($dateYMD, 6, 2),fn:substring($dateYMD, 1, 4)), '-')

};

declare function xf:RequestTransform2($invoke1 as element(ns4:invoke), $header1 as element(ns4:header))
    as element(ns1:CRMGetCustomersRequest) {
        &lt;ns1:CRMGetCustomersRequest&gt;
            {
                for $userID in $header1/ns4:msgHeader/ns4:userId
                return
                    &lt;ns1:IdWewPrac&gt;{ xs:long( data($userID) ) }&lt;/ns1:IdWewPrac&gt;
            }
            {
                for $value in $invoke1/ns4:searchRange/ns6:StringHolder/ns6:value
                return
                    &lt;ns1:ZakresWyszukiwania&gt;{ data($value) }&lt;/ns1:ZakresWyszukiwania&gt;
            }
            {
                for $companyType in $invoke1/ns4:customer/ns0:Customer/ns0:companyID/ns2:CompanyType/ns2:companyType
                return
                    &lt;ns1:IdSpolki&gt;{ xs:short( data($companyType) ) }&lt;/ns1:IdSpolki&gt;
            }
            {
                for $customerType in $invoke1/ns4:customer/ns0:Customer/ns0:customerType/ns2:CustomerType/ns2:customerType
                return
                    &lt;ns1:TypKlienta&gt;{ data($customerType) }&lt;/ns1:TypKlienta&gt;
            }
            {
                for $customerNumber in $invoke1/ns4:customer/ns0:Customer/ns0:customerNumber
                return
                    &lt;ns1:NumerKlienta&gt;{ data($customerNumber) }&lt;/ns1:NumerKlienta&gt;
            }
            {
                for $identityCardNumber in $invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:identityCardNumber
                return
                    &lt;ns1:NrDowoduRegon&gt;{ data($identityCardNumber) }&lt;/ns1:NrDowoduRegon&gt;
            }
            {
                for $pesel in $invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:pesel
                return
                    &lt;ns1:NrPesel&gt;{ data($pesel) }&lt;/ns1:NrPesel&gt;
            }
            {
                for $taxID in $invoke1/ns4:customer/ns0:Customer/ns0:taxID
                return
                    &lt;ns1:Nip&gt;{ data($taxID) }&lt;/ns1:Nip&gt;
            }
            {
                for $passportNumber in $invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:passportNumber
                return
                    &lt;ns1:NumerPaszportu&gt;{ data($passportNumber) }&lt;/ns1:NumerPaszportu&gt;
            }
            {
                for $nik in $invoke1/ns4:customer/ns0:Customer/ns0:customerCEKEList/ns-1:CustomerCEKE[1]/ns-1:nik
                return
                    &lt;ns1:Nik&gt;{ data($nik) }&lt;/ns1:Nik&gt;
            }
            {
                for $lastName in $invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:lastName
                return
                    &lt;ns1:Nazwisko&gt;{ data($lastName) }&lt;/ns1:Nazwisko&gt;
            }
            {
                for $zipCode in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address[1]/ns0:zipCode
                return
                    &lt;ns1:KodPocztowyDanePodst&gt;{ data($zipCode) }&lt;/ns1:KodPocztowyDanePodst&gt;
            }
            {
                for $policyID in $invoke1/ns4:customer/ns0:Customer/ns0:policyContract/ns3:PolicyContract/ns3:policyID
                return
                    &lt;ns1:IdPortfela&gt;{ data($policyID) }&lt;/ns1:IdPortfela&gt;
            }
            {
                for $pageSize in $invoke1/ns4:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:pageSize
                return
                    &lt;ns1:LiczbaOper&gt;{ xs:short( data($pageSize) ) }&lt;/ns1:LiczbaOper&gt;
            }
            {
                for $goToPage in $invoke1/ns4:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:goToPage
                return
                    &lt;ns1:NumerPaczki&gt;{ xs:short( data($goToPage) ) }&lt;/ns1:NumerPaczki&gt;
            }
            {
                for $value in $invoke1/ns4:sortType/ns6:StringHolder/ns6:value
                return
                    &lt;ns1:Sortowanie&gt;{ data($value) }&lt;/ns1:Sortowanie&gt;
            }
            {
                for $firstName in $invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:firstName
                return
                    &lt;ns1:Imie&gt;{ data($firstName) }&lt;/ns1:Imie&gt;
            }
            {
                for $dateOfBirth in $invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:dateOfBirth
                return
                    &lt;ns1:DataUrodzenia&gt;{ local:YMDToDMY( data($dateOfBirth) ) }&lt;/ns1:DataUrodzenia&gt;
            }
        &lt;/ns1:CRMGetCustomersRequest&gt;
};

declare variable $header1 as element(ns4:header) external;
declare variable $invoke1 as element(ns4:invoke) external;

&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"&gt;
{
xf:RequestTransform2($invoke1, $header1)
}
&lt;/soapenv:Body&gt;</con:xquery>
</con:xqueryEntry>