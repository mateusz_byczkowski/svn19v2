<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-03-28</con:description>
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:primeaccounts.entities.be.dcl";
declare namespace urn2 = "urn:accounts.entities.be.dcl";
declare namespace urn3 = "urn:cif.entities.be.dcl";
declare namespace urn4 = "urn:dictionaries.be.dcl";
declare namespace pdk="http://bzwbk.com/services/pdk/";

declare function local:plDate2xsdate($date as xs:string?) as xs:string? {
    if($date)
        then
            let $tokens := fn:tokenize($date, "-")
            return
                fn:concat($tokens[3], "-", $tokens[2], "-", $tokens[1])
        else ()
};

declare function local:response($result as element(pdk:GetArt105HistoryResult)) as element(soap-env:Body) {
    &lt;soap-env:Body&gt;
        &lt;urn:invokeResponse&gt;
            &lt;urn:processingAgreeList&gt;
            {
                for $out in $result/pdk:OutGetArt105History
                return
                    &lt;urn3:ProcessingAgree&gt;
                        &lt;urn3:agreementCancellingDate?&gt;{ local:plDate2xsdate(data($out/pdk:DataWycofania)) }&lt;/urn3:agreementCancellingDate&gt;
                        &lt;urn3:agreementValidityPeriod?&gt;{ data($out/pdk:OkresWaznosci) }&lt;/urn3:agreementValidityPeriod&gt;
                        &lt;urn3:customerNotificationDate?&gt;{ local:plDate2xsdate(data($out/pdk:DataPowiadomienia)) }&lt;/urn3:customerNotificationDate&gt;
                        &lt;urn3:dataProcessingFlag?&gt;{ data($out/pdk:WskaznikPrzetwarzania) }&lt;/urn3:dataProcessingFlag&gt;
                        &lt;urn3:agreementDate?&gt;{ local:plDate2xsdate(data($out/pdk:DataUdzielenia)) }&lt;/urn3:agreementDate&gt;
                        &lt;urn3:operationDate?&gt;{ data($out/pdk:Data_zmiany) }&lt;/urn3:operationDate&gt;
                        &lt;urn3:operationUserId?&gt;{ data($out/pdk:Uzytkownik) }&lt;/urn3:operationUserId&gt;
                        &lt;urn3:agreementFlag&gt;
                            &lt;urn4:CustomerAgreementStatus?&gt;
                                &lt;urn4:customerAgreementStatus?&gt;{ data($out/pdk:FlagaZgody) }&lt;/urn4:customerAgreementStatus&gt;
                            &lt;/urn4:CustomerAgreementStatus&gt;
                        &lt;/urn3:agreementFlag&gt;
                    &lt;/urn3:ProcessingAgree&gt;
            }
            &lt;/urn:processingAgreeList&gt;
        &lt;/urn:invokeResponse&gt;
    &lt;/soap-env:Body&gt;
};

declare variable $body as element(soap-env:Body) external;

local:response($body/pdk:GetArt105HistoryResponse[1]/pdk:GetArt105HistoryResult[1])</con:xquery>
</con:xqueryEntry>