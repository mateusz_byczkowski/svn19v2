<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns0="urn:accountdict.dictionaries.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)&gt;5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };

declare function chgDate($value as xs:string,$dateFormat as xs:string,$fieldName as xs:string) as xs:string{

    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)

    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 


};

declare function getElementsForLoanCertSavAccountList($parm as element(fml:FML32)) as element()
{

&lt;ns1:loanCertSavAccountList&gt;
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER

    return
    &lt;ns1:LoanCertSavAccount&gt;
      &lt;ns1:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns1:accountNumber&gt;
      &lt;ns1:currency&gt;
        &lt;ns3:CurrencyCode&gt;
          &lt;ns3:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE2[$occ])}&lt;/ns3:currencyCode&gt;
        &lt;/ns3:CurrencyCode&gt;
      &lt;/ns1:currency&gt;
    &lt;/ns1:LoanCertSavAccount&gt;
  }
&lt;/ns1:loanCertSavAccountList&gt;
};
declare function getElementsForLoanCertificateList($parm as element(fml:FML32)) as element()
{

&lt;ns1:loanCertificateList&gt;
  {
    for $x at $occ in $parm/NF_LOANC_CERTIFICATEPAYMEN
    return
    &lt;ns1:LoanCertificate&gt;
      { insertDate(data($parm/NF_LOANC_CERTIFICATEPAYMEN[$occ]),"yyyy-MM-dd","ns1:certificatePaymentDate")} 
      &lt;ns1:certificateInterest?&gt;{data($parm/NF_LOANC_CERTIFICATEINTERE[$occ])}&lt;/ns1:certificateInterest&gt;
      &lt;ns1:certificateInterestPLN?&gt;{data($parm/NF_LOANC_CERTIFICATEINTPLN[$occ])}&lt;/ns1:certificateInterestPLN&gt;
      &lt;ns1:certificateTransactionDesc?&gt;{data($parm/NF_LOANC_CERTIFICATETRANSA[$occ])}&lt;/ns1:certificateTransactionDesc&gt;
      &lt;ns1:certificatePrincipal?&gt;{data($parm/NF_LOANC_CERTIFICATEPRINCI[$occ])}&lt;/ns1:certificatePrincipal&gt;
      &lt;ns1:certificatePrincipalPLN?&gt;{data($parm/NF_LOANC_CERTIFICATEPRIPLN[$occ])}&lt;/ns1:certificatePrincipalPLN&gt;
    &lt;/ns1:LoanCertificate&gt;
  }
&lt;/ns1:loanCertificateList&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse&gt;
  &lt;ns5:certificateOut&gt;
    &lt;ns1:LoanCertificate&gt;
      &lt;ns1:certificateAgreementNbr?&gt;{data($parm/NF_LOANC_CERTIFICATEAGREEM)}&lt;/ns1:certificateAgreementNbr&gt;
      &lt;ns1:certificatePastDuePrincAmt?&gt;{data($parm/NF_LOANC_CERPASDUEPRIAMT)}&lt;/ns1:certificatePastDuePrincAmt&gt;
      &lt;ns1:certificateInterestDue?&gt;{data($parm/NF_LOANC_CERTIFICATEINTDUE)}&lt;/ns1:certificateInterestDue&gt;
      &lt;ns1:certificatePastDueIntAmt?&gt;{data($parm/NF_LOANC_CERTIFICATEPASTDU)}&lt;/ns1:certificatePastDueIntAmt&gt;
      &lt;ns1:certificatePastDueLateInt?&gt;{data($parm/NF_LOANC_CERPASDUELATINT)}&lt;/ns1:certificatePastDueLateInt&gt;
      &lt;ns1:certificateDueAmountFee?&gt;{data($parm/NF_LOANC_CERTIFICATEDUEAMO)}&lt;/ns1:certificateDueAmountFee&gt;
      &lt;ns1:certificatePastDuePrincAmtPLN?&gt;{data($parm/NF_LOANC_CERPASDUEPRIAMTPL)}&lt;/ns1:certificatePastDuePrincAmtPLN&gt;
      &lt;ns1:certificateInterestDuePLN?&gt;{data($parm/NF_LOANC_CERINTDUEPLN)}&lt;/ns1:certificateInterestDuePLN&gt;
      &lt;ns1:certificatePastDueLateIntPLN?&gt;{data($parm/NF_LOANC_CERPASDUELATINTPL)}&lt;/ns1:certificatePastDueLateIntPLN&gt;
      &lt;ns1:certificateDueAmountFeePLN?&gt;{data($parm/NF_LOANC_CERDUEAMOFEEPLN)}&lt;/ns1:certificateDueAmountFeePLN&gt;
      &lt;ns1:certificatePastDueIntAmtPLN?&gt;{data($parm/NF_LOANC_CERTIFICATEPASDUE)}&lt;/ns1:certificatePastDueIntAmtPLN&gt;
      {getElementsForLoanCertSavAccountList($parm)}
      &lt;ns1:loanAccount&gt;
        &lt;ns1:LoanAccount&gt;
          &lt;ns1:interestRate?&gt;{data($parm/NF_LOANA_INTERESTRATE)}&lt;/ns1:interestRate&gt;
          &lt;ns1:faceAmount?&gt;{data($parm/NF_LOANA_FACEAMOUNT)}&lt;/ns1:faceAmount&gt;
          { insertDate(data($parm/NF_LOANA_MATURITYDATE),"yyyy-MM-dd","ns1:maturityDate")} 
          &lt;ns1:timesPastDueCY1?&gt;{data($parm/NF_LOANA_TIMESPASTDUECY1)}&lt;/ns1:timesPastDueCY1&gt;
          &lt;ns1:timesPastDueCY2?&gt;{data($parm/NF_LOANA_TIMESPASTDUECY2)}&lt;/ns1:timesPastDueCY2&gt;
          &lt;ns1:timesPastDueCY3?&gt;{data($parm/NF_LOANA_TIMESPASTDUECY3)}&lt;/ns1:timesPastDueCY3&gt;
          &lt;ns1:timesPastDueCY4?&gt;{data($parm/NF_LOANA_TIMESPASTDUECY4)}&lt;/ns1:timesPastDueCY4&gt;
          &lt;ns1:timesPastDueCY5?&gt;{data($parm/NF_LOANA_TIMESPASTDUECY5)}&lt;/ns1:timesPastDueCY5&gt;
          &lt;ns1:timesPastDueCY6?&gt;{data($parm/NF_LOANA_TIMESPASTDUECY6)}&lt;/ns1:timesPastDueCY6&gt;
          {getElementsForLoanCertificateList($parm)}
          &lt;ns1:account&gt;
            &lt;ns1:Account&gt;
              { insertDate(data($parm/NF_TRAACA_CREATIONDATE),"yyyy-MM-dd","ns1:accountOpenDate")}
              &lt;ns1:currentBalance?&gt;{data($parm/NF_ACCOUN_CURRENTBALANCE)}&lt;/ns1:currentBalance&gt;
              &lt;ns1:currentBalancePLN?&gt;{data($parm/NF_ACCOUN_CURRENTBALANCEPL)}&lt;/ns1:currentBalancePLN&gt;
              &lt;ns1:currency&gt;
                &lt;ns3:CurrencyCode&gt;
                  &lt;ns3:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE)}&lt;/ns3:currencyCode&gt;
                &lt;/ns3:CurrencyCode&gt;
              &lt;/ns1:currency&gt;
            &lt;/ns1:Account&gt;
          &lt;/ns1:account&gt;
          &lt;ns1:censusTract&gt;
            &lt;ns3:CreditObject&gt;
              &lt;ns3:object?&gt;{data($parm/NF_CREDIO_OBJECT)}&lt;/ns3:object&gt;
            &lt;/ns3:CreditObject&gt;
          &lt;/ns1:censusTract&gt;
        &lt;/ns1:LoanAccount&gt;
      &lt;/ns1:loanAccount&gt;
    &lt;/ns1:LoanCertificate&gt;
  &lt;/ns5:certificateOut&gt;
&lt;/ns5:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>