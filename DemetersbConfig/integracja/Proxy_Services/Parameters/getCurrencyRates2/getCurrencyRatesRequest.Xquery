<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.5
 : @since   2009-12-14
 :
 : wersja WSDLa: 17-09-2009 12:12:51
 : 
 : $Proxy Services/Parameters/getCurrencyRates2/getCurrencyRatesRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getCurrencyRates/getCurrencyRatesRequest/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "";
declare namespace ns3 = "urn:be.services.dcl";

declare variable $invoke1 as element(ns3:invoke) external;

(:~
 : @param invoke1 - operacja wejściowa
 :
 : @return FML32 - bufor XML/FML
 :)
declare function xf:getCurrencyRatesRequest($invoke1 as element(ns3:invoke))
    as element(ns2:FML32) 
{
	&lt;ns2:FML32&gt;
		{
			let $currencyDate := data($invoke1/ns3:rateTable/ns0:RateTable/ns0:currencyDate)
			return
	        	if (data($currencyDate)) then
				(
					&lt;ns2:NF_RATET_CURRENCYDT&gt;{
						fn:substring($currencyDate, 1, 10)
					}&lt;/ns2:NF_RATET_CURRENCYDT&gt;,
					
					&lt;ns2:NF_RATET_CURRENCYTM&gt;{
						xs:long(fn:concat(fn:substring($currencyDate, 12, 2),
										  fn:substring($currencyDate, 15, 2),
										  fn:substring($currencyDate, 18, 2)))
					}&lt;/ns2:NF_RATET_CURRENCYTM&gt;
				)
	        	else
					()
		}
		
		&lt;ns2:NF_CURREC_NUMERICCODE/&gt;
		
		(:
		 : filtrowanie po symbolu waluty
		 : lub wyszukanie po wszystkich symbolach
		 : (znacznik 'ALL') w przypadku braku pola
		 : CurrencyCode.currencyCode 
		 : w kopercie wejściowej
		 :)
		{
			let $currencyCode := $invoke1/ns3:rateTable/ns0:RateTable/ns0:currencyCode/ns1:CurrencyCode/ns1:currencyCode
			return
				&lt;ns2:NF_CURREC_CURRENCYCODE&gt;{
					if (data($currencyCode)) then
						data($currencyCode)
					else
						'ALL'
				}&lt;/ns2:NF_CURREC_CURRENCYCODE&gt;
		}
		
	&lt;/ns2:FML32&gt;
};

&lt;soap-env:Body&gt;{
	xf:getCurrencyRatesRequest($invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>