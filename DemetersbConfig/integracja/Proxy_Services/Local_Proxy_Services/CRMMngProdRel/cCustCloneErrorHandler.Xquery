<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/faults/";
declare variable $cif as xs:string external;



<soapenv:Body>
    <soapenv:Fault>
         <faultcode>soapenv:Server.userException</faultcode>
         <faultstring>com.bzwbk.services.faults.ServiceFailException</faultstring>
         <detail>
            <faul:ServiceFailException xmlns:faul="http://bzwbk.com/services/faults/">
               <errorCode1>12</errorCode1>
               <errorCode2>0</errorCode2>
               <errorDescription>Błąd przenoszania danych klienta {$cif} do ICBS. Nieudane wywołanie usługi cCustClone</errorDescription>
            </faul:ServiceFailException >
         </detail>
</soapenv:Fault>

</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>