<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-06</con:description>
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:card.entities.be.dcl";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns2="urn:be.services.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:dictionaries.be.dcl";
declare namespace ns7="urn:accounts.entities.be.dcl";
declare namespace ns6="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm = "1") then $trueval
    else $falseval


};

declare function iban2short ($num as xs:string) as xs:string{
(: dlugosc pola  z numerem rachunku do transferu odsetek:)
     let $numLength:=string-length(fn-bea:trim(data($num)))
     return   
        if ($numLength&gt;12)
               then substring(fn-bea:trim(data($num )),$numLength - 11,12)
               else data($num )
};

declare function convertCycleLimit ($cycleLimit as xs:string?) as xs:string{
    if ($cycleLimit)
       then if (data($cycleLimit) = "M" or data($cycleLimit) = "m")
            then "83"
            else if (data($cycleLimit) = "D" or data($cycleLimit) = "d")
                 then "01"
                 else "00"
       else "00"
};


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function getFieldsFromHeader($parm as element(ns2:header)) as element()*
{   

&lt;DC_MSHEAD_MSGID?&gt;{data($parm/ns2:msgHeader/ns2:msgId)}&lt;/DC_MSHEAD_MSGID&gt;
,
(:&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns2:msgHeader/ns2:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns2:msgHeader/ns2:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
,:)
&lt;DC_ODDZIAL?&gt;{chkUnitId(data($parm/ns2:msgHeader/ns2:unitId))}&lt;/DC_ODDZIAL&gt;
,
&lt;DC_UZYTKOWNIK?&gt;{concat("SKP:", data($parm/ns2:msgHeader/ns2:userId))}&lt;/DC_UZYTKOWNIK&gt;
,(:
&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns2:msgHeader/ns2:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns2:msgHeader/ns2:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
,:)
&lt;DC_TRN_ID?&gt;{data($parm/ns2:transHeader/ns2:transId)}&lt;/DC_TRN_ID&gt;
};


declare function getFieldsFromInvoke($parm as element(ns2:invoke)) as element()*
{  
&lt;DC_RACHUNEK&gt;{iban2short (data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:tranAccount/ns7:TranAccount/ns7:account/ns7:Account/ns7:accountNumber))}&lt;/DC_RACHUNEK&gt;
,
&lt;DC_NR_KARTY_BIN?&gt;{data($parm/ns2:card/ns0:Card/ns0:bin)}&lt;/DC_NR_KARTY_BIN&gt;
,
&lt;DC_TYP_KARTY?&gt;{data($parm/ns2:card/ns0:Card/ns0:cardType)}&lt;/DC_TYP_KARTY&gt;
,
&lt;DC_WYTLOCZONE_NA_KARCIE_I?&gt;{data($parm/ns2:card/ns0:Card/ns0:embossName1)}&lt;/DC_WYTLOCZONE_NA_KARCIE_I&gt;
,
&lt;DC_WYTLOCZONE_NA_KARCIE_II?&gt;{data($parm/ns2:card/ns0:Card/ns0:embossName2)}&lt;/DC_WYTLOCZONE_NA_KARCIE_II&gt;
,
&lt;DC_IDENTYFIKATOR_PIN?&gt;{data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:pinMailer)}&lt;/DC_IDENTYFIKATOR_PIN&gt;
,
&lt;DC_UMORZYC_OPLATE?&gt;{boolean2SourceValue(data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:processingOptionFlag3),"1","0")}&lt;/DC_UMORZYC_OPLATE&gt;
,
&lt;DC_STATUS_UMOWY?&gt;{boolean2SourceValue(data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:contract),"1","0")}&lt;/DC_STATUS_UMOWY&gt;
,
&lt;DC_LIMIT_KARTY?&gt;{data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:cashLimit)}&lt;/DC_LIMIT_KARTY&gt;
,
&lt;DC_CYKL_LIMITU?&gt;{convertCycleLimit($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:cycleLimit/ns1:CrdCycleLimit/ns1:crdCycleLimit)}&lt;/DC_CYKL_LIMITU&gt;
,
&lt;DC_EKSPRES?&gt;{boolean2SourceValue(data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:express),"1","0")}&lt;/DC_EKSPRES&gt;
,
&lt;DC_ZALOZYC_POLISE?&gt;{boolean2SourceValue(data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:policy),"1","0")}&lt;/DC_ZALOZYC_POLISE&gt;
,
&lt;DC_CYKL_ZESTAWIEN?&gt;{boolean2SourceValue(data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:individualTranList),"30","0")}&lt;/DC_CYKL_ZESTAWIEN&gt;
,
&lt;DC_ZEST_ZBIORCZE?&gt;{boolean2SourceValue(data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:collectiveTranList),"1","0")}&lt;/DC_ZEST_ZBIORCZE&gt;
,
&lt;DC_TYP_ADRESU?&gt;{data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:addressFlag/ns1:CrdAddressFlag/ns1:crdAddressFlag)}&lt;/DC_TYP_ADRESU&gt;
,
&lt;DC_NUMER_ODDZIALU?&gt;{data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:cardAddressBranchNumber/ns8:BranchCode/ns8:branchCode)}&lt;/DC_NUMER_ODDZIALU&gt;
,
&lt;DC_NUMER_KLIENTA?&gt;{data($parm/ns2:card/ns0:Card/ns0:customer/ns3:Customer/ns3:customerNumber)}&lt;/DC_NUMER_KLIENTA&gt;
,
&lt;DC_STATUS_KARTY?&gt;{data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:cardStatus/ns1:CrdStatus/ns1:crdStatus)}&lt;/DC_STATUS_KARTY&gt;
,
&lt;DC_NR_PB2?&gt;{data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:debitCardLpInfo/ns0:DebitCardLpInfo/ns0:customerLpNumber)}&lt;/DC_NR_PB2&gt;
,
&lt;DC_NR_PB3?&gt;{data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:debitCardLpInfo/ns0:DebitCardLpInfo/ns0:otherCustomerLpNumber)}&lt;/DC_NR_PB3&gt;
,
&lt;DC_FLAGA_MARKETINGU?&gt;{boolean2SourceValue(data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:debitCardLpInfo/ns0:DebitCardLpInfo/ns0:processingApproval),"1","0")}&lt;/DC_FLAGA_MARKETINGU&gt;
,
&lt;DC_FLAGA_ZGODY?&gt;{boolean2SourceValue(data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:debitCardLpInfo/ns0:DebitCardLpInfo/ns0:forwardingDataApproval),"1","0")}&lt;/DC_FLAGA_ZGODY&gt;
,
&lt;DC_ZRODLO?&gt;{data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:debitCardLpInfo/ns0:DebitCardLpInfo/ns0:applicationSource)}&lt;/DC_ZRODLO&gt;
,
&lt;DC_JEDNOSTKA_SPRZEDAJACA?&gt;{data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:debitCardLpInfo/ns0:DebitCardLpInfo/ns0:unitApplicationSource)}&lt;/DC_JEDNOSTKA_SPRZEDAJACA&gt;
,
&lt;DC_TECH_PIN?&gt;{boolean2SourceValue(data($parm/ns2:card/ns0:Card/ns0:debitCard/ns0:DebitCard/ns0:pinMailerTech),"1","0")}&lt;/DC_TECH_PIN&gt;
,
&lt;DC_TERMINAL_ID?&gt;{data($parm/ns2:loginMsg/ns6:LoginMsg/ns6:terminalId)}&lt;/DC_TERMINAL_ID&gt;
,
&lt;DC_PROMOCJA&gt;2&lt;/DC_PROMOCJA&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns2:header)}
    {getFieldsFromInvoke($body/ns2:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>