<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{

if(string-length($parm) &gt;0 )then
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
else
   $trueval
};


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};


declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
   let $length:= string-length($sourceString)
   let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
   let $accountNumber :=  substring($sourceString, $startPos)
   return
     $accountNumber
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{
(:
&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
,:)
&lt;DC_ODDZIAL?&gt;{chkUnitId(data($parm/ns0:msgHeader/ns0:unitId))}&lt;/DC_ODDZIAL&gt;
,
&lt;DC_UZYTKOWNIK?&gt;{concat("SKP:",data($parm/ns0:msgHeader/ns0:userId))}&lt;/DC_UZYTKOWNIK&gt;
(:,
&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
:),
&lt;DC_TRN_ID?&gt;{data($parm/ns0:transHeader/ns0:transId)}&lt;/DC_TRN_ID&gt;
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
&lt;DC_NR_RACHUNKU?&gt;{cutStr(data($parm/ns0:account/ns3:Account/ns3:accountNumber),10)}&lt;/DC_NR_RACHUNKU&gt;
,
&lt;DC_IMIE_I_NAZWISKO_ALT?&gt;{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:name1)}&lt;/DC_IMIE_I_NAZWISKO_ALT&gt;
,
&lt;DC_IMIE_I_NAZWISKO_ALT_C_D?&gt;{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:name2)}&lt;/DC_IMIE_I_NAZWISKO_ALT_C_D&gt;
,
&lt;DC_ULICA_ADRES_ALT?&gt;{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:street)}&lt;/DC_ULICA_ADRES_ALT&gt;
,
&lt;DC_NR_POSES_LOKALU_ADRES_ALT?&gt;{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:houseFlatNumber)}&lt;/DC_NR_POSES_LOKALU_ADRES_ALT&gt;
,
&lt;DC_MIASTO_ADRES_ALT?&gt;{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:city)}&lt;/DC_MIASTO_ADRES_ALT&gt;
,
&lt;DC_WOJEWODZTWO_KRAJ_ADRES_ALT?&gt;{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:stateCountry)}&lt;/DC_WOJEWODZTWO_KRAJ_ADRES_ALT&gt;
,
&lt;DC_KOD_POCZTOWY_ADRES_ALT&gt;{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:zipCode)}&lt;/DC_KOD_POCZTOWY_ADRES_ALT&gt;
,
(:&lt;DC_TYP_ADRESU?&gt;{data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:accountAddressType)}&lt;/DC_TYP_ADRESU&gt;:)
if ($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:accountAddressType)
	then if (data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:accountAddressType) = "Z")
		then &lt;DC_TYP_ADRESU&gt;1&lt;/DC_TYP_ADRESU&gt;
		else &lt;DC_TYP_ADRESU&gt;0&lt;/DC_TYP_ADRESU&gt;
else()
,
&lt;DC_DATA_WPROWADZENIA?&gt;{fn-bea:date-to-string-with-format("yyyy-MM-dd",data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:validFrom))}&lt;/DC_DATA_WPROWADZENIA&gt;
,
&lt;DC_DATA_KONCOWA?&gt;{fn-bea:date-to-string-with-format("yyyy-MM-dd",data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:validTo))}&lt;/DC_DATA_KONCOWA&gt;
,
&lt;DC_KASOWAC_PRZY_WYGASNIECIU?&gt;{boolean2SourceValue(data($parm/ns0:account/ns3:Account/ns3:accountAddress/ns3:AccountAddress/ns3:deleteWhenExpired),"1","0")}&lt;/DC_KASOWAC_PRZY_WYGASNIECIU&gt;
,
&lt;DC_TYP_ZMIANY&gt;M&lt;/DC_TYP_ZMIANY&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>