<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:entities.be.dcl";
declare namespace ns3="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForFeeList($parm as element(fml:FML32)) as element()
{

&lt;ns0:feeList&gt;
  {
    for $x at $occ in $parm/NF_FEE_COUNTER
    return
    &lt;ns4:Fee&gt;
      &lt;ns4:feeAmount&gt;{data($parm/NF_FEE_FEEAMOUNT[$occ])}&lt;/ns4:feeAmount&gt;
      &lt;ns4:counter&gt;{data($parm/NF_FEE_COUNTER[$occ])}&lt;/ns4:counter&gt;
      &lt;ns4:transactionAmount&gt;{data($parm/NF_FEE_TRANSACTIONAMOUNT[$occ])}&lt;/ns4:transactionAmount&gt;
      &lt;ns4:feeCode&gt;{data($parm/NF_FEE_FEECODE[$occ])}&lt;/ns4:feeCode&gt;
      &lt;ns4:feeDescription&gt;{data($parm/NF_FEE_FEEDESCRIPTION[$occ])}&lt;/ns4:feeDescription&gt;
    &lt;/ns4:Fee&gt;
  }
&lt;/ns0:feeList&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  &lt;ns0:bcd&gt;
    &lt;ns2:BusinessControlData&gt;
      &lt;ns2:pageControl&gt;
        &lt;ns3:PageControl&gt;
          &lt;ns3:hasNext&gt;{data($parm/NF_PAGEC_HASNEXT)}&lt;/ns3:hasNext&gt;
          &lt;ns3:navigationKeyDefinition&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns3:navigationKeyDefinition&gt;
          &lt;ns3:navigationKeyValue&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns3:navigationKeyValue&gt;
        &lt;/ns3:PageControl&gt;
      &lt;/ns2:pageControl&gt;
    &lt;/ns2:BusinessControlData&gt;
  &lt;/ns0:bcd&gt;
  {getElementsForFeeList($parm)}
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>