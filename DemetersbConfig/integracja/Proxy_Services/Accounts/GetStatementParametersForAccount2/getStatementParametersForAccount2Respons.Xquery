<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare namespace ns6="urn:accountdict.dictionaries.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string
{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse&gt;
  &lt;ns5:response&gt;
    &lt;ns0:ResponseMessage&gt;
      &lt;ns0:result?&gt;{data($parm/NF_RESPOM_RESULT)}&lt;/ns0:result&gt;
    &lt;/ns0:ResponseMessage&gt;
  &lt;/ns5:response&gt;
  &lt;ns5:statementParameters&gt;
    &lt;ns1:StatementParameters&gt;
    {if (string-length(data($parm/NF_STATEP_FREQUENCY))&gt;0) then
      &lt;ns1:frequency?&gt;{data($parm/NF_STATEP_FREQUENCY)}&lt;/ns1:frequency&gt;
     else ()
     }
      (:&lt;ns1:nextPrintoutDate?&gt;{data($parm/NF_STATEP_NEXTPRINTOUTDATE)}&lt;/ns1:nextPrintoutDate&gt;:)
      {insertDate(data($parm/NF_STATEP_NEXTPRINTOUTDATE),"yyyy-MM-dd","ns1:nextPrintoutDate")}
      &lt;ns1:printFlag?&gt;{data($parm/NF_STATEP_PRINTFLAG)}&lt;/ns1:printFlag&gt;
     {
      if (string-length(data($parm/NF_STATEP_SPECIALDAY))&gt;0) then
             &lt;ns1:specialDay?&gt;{data($parm/NF_STATEP_SPECIALDAY)}&lt;/ns1:specialDay&gt;
     else ()
     }

      &lt;ns1:aggregateTransaction?&gt;{sourceValue2Boolean (data($parm/NF_STATEP_AGGREGATETRANSAC),"1")}&lt;/ns1:aggregateTransaction&gt;
      &lt;ns1:cycle&gt;
        &lt;ns2:Period&gt;
        {
           if (string-length(data($parm/NF_PERIOD_PERIOD))&gt;0) then
          &lt;ns2:period?&gt;{data($parm/NF_PERIOD_PERIOD)}&lt;/ns2:period&gt;
         else()
        }
        &lt;/ns2:Period&gt;
      &lt;/ns1:cycle&gt;
      &lt;ns1:provideManner&gt;
        &lt;ns2:ProvideManner&gt;
          &lt;ns2:provideManner?&gt;{data($parm/NF_PROVIM_PROVIDEMANNER)}&lt;/ns2:provideManner&gt;
        &lt;/ns2:ProvideManner&gt;
      &lt;/ns1:provideManner&gt;
      &lt;ns1:snapshotType&gt;
        &lt;ns6:SnapshotType&gt;
          {if (string-length(data($parm/NF_STATEP_SNAPSHOTTYPE))&gt; 0) then
              &lt;ns6:snapshotType?&gt;{data($parm/NF_STATEP_SNAPSHOTTYPE)}&lt;/ns6:snapshotType&gt;
           else ()
         }
        &lt;/ns6:SnapshotType&gt;
      &lt;/ns1:snapshotType&gt;
    &lt;/ns1:StatementParameters&gt;
  &lt;/ns5:statementParameters&gt;
&lt;/ns5:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>