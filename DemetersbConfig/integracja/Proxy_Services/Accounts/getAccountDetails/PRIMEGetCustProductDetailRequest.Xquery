<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Tomasz Krajewski  
 : @version 1.0
 : @since   2010-06-24
 :
 : $Proxy Services/Accounts/getAccountDetails/PRIMEGetCustProductDetailRequest.xq$
 :
 :)

declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductDetail($fml as element(fml:FML32))
	as element(m:PRIMEGetCustProductDetail) {
		&lt;m:PRIMEGetCustProductDetail&gt;
			{
				if($fml/fml:NF_ACCOUN_ACCOUNTNUMBER)
					then &lt;m:DC_NR_RACHUNKU&gt;{ data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER) }&lt;/m:DC_NR_RACHUNKU&gt;
                                       else ()
			}
			&lt;m:CI_OPCJA&gt;1&lt;/m:CI_OPCJA&gt;
		&lt;/m:PRIMEGetCustProductDetail&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapPRIMEGetCustProductDetail($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>