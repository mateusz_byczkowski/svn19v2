<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-18</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn="urn:be.services.dcl";
declare namespace fml="";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare variable $body as element(soap:Body) external;

declare function xf:toBoolean($dateIn as xs:string*) as xs:boolean* {
  if ($dateIn and $dateIn = "1") 
    then true()
    else false()
};

declare function xf:toBooleanRev($dateIn as xs:string*) as xs:boolean* {
  if ($dateIn and $dateIn = "0") 
    then true()
    else false()
};

declare function xf:mapDate($dateIn as xs:string*) as xs:string* {
  if($dateIn and string-length(normalize-space(data($dateIn)))&gt;0)
    then fn:concat(fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2))
    else ()
};

declare function getInvokeResponse($parm as element(fml:FML32)) as element()* {
  &lt;urn:invokeResponse&gt;
    &lt;urn:listaRachunkow&gt;
    {
    let $xB_KOD_RACH := $parm/B_KOD_RACH
    let $xB_DL_NR_RACH := $parm/B_DL_NR_RACH
    let $xB_SALDO := $parm/B_SALDO
    let $xB_DOST_SRODKI := $parm/B_DOST_SRODKI
    let $xB_WALUTA := $parm/B_WALUTA
    let $xB_KOD_WALUTY := $parm/B_KOD_WALUTY
    let $xB_DATA_OST_OPER := $parm/B_DATA_OST_OPER
    let $xB_TYP_RACH := $parm/B_TYP_RACH
    let $xB_OPIS_RACH := $parm/B_OPIS_RACH
    let $xB_ID_ODDZ := $parm/B_ID_ODDZ
    let $xB_NAZWA_ODDZ := $parm/B_NAZWA_ODDZ
    let $xB_RODZAJ_RACH := $parm/B_RODZAJ_RACH
    let $xB_OPROC1 := $parm/B_OPROC1
    let $xB_D_OTWARCIA := $parm/B_D_OTWARCIA
    let $xB_D_OSTATNIEJ_KAP := $parm/B_D_OSTATNIEJ_KAP
    let $xB_D_NASTEPNEJ_KAP := $parm/B_D_NASTEPNEJ_KAP
    let $xB_D_POCZ_WKL := $parm/B_D_POCZ_WKL
    let $xB_D_KON_WKL := $parm/B_D_KON_WKL
    let $xB_ODS_SKAP_AKT := $parm/B_ODS_SKAP_AKT
    let $xB_ODS_SKAP_POP := $parm/B_ODS_SKAP_POP
    let $xB_LIMIT2 := $parm/B_LIMIT2
    let $xB_D_KONCA_LIMITU := $parm/B_D_KONCA_LIMITU
    let $xB_BLOKADA := $parm/B_BLOKADA
    let $xB_RODZAJ_OPROC := $parm/B_RODZAJ_OPROC
    let $xB_KAPITALIZACJA := $parm/B_KAPITALIZACJA
    let $xB_PRZEKS_KAP := $parm/B_PRZEKS_KAP
    let $xB_OKR_WKLADU := $parm/B_OKR_WKLADU
    let $xB_JDN_OKR_WKLADU := $parm/B_JDN_OKR_WKLADU
    let $xB_PRAWA_KLIENTA := $parm/B_PRAWA_KLIENTA
    let $xB_NAZWA_KLIENTA := $parm/B_NAZWA_KLIENTA
    let $xB_UL_ZAM := $parm/B_UL_ZAM
    let $xB_M_ZAM := $parm/B_M_ZAM
    let $xB_RODZ_DOK := $parm/B_RODZ_DOK
    let $xB_NR_DOK := $parm/B_NR_DOK
    let $xB_DATA_B_PLAT := $parm/B_DATA_B_PLAT
    let $xB_NR_UMOWY := $parm/B_NR_UMOWY
    let $xB_ITYP := $parm/B_ITYP

    for $x at $occ in $parm/B_KOD_RACH
      return
      &lt;urn:rachunek&gt;
        &lt;urn:numerRachunku?&gt;{data($xB_KOD_RACH[$occ])}&lt;/urn:numerRachunku&gt;
        &lt;urn:pelnyNumerRachunku?&gt;{data($xB_DL_NR_RACH[$occ])}&lt;/urn:pelnyNumerRachunku&gt;
        &lt;urn:saldo?&gt;{data($xB_SALDO[$occ])}&lt;/urn:saldo&gt;
        &lt;urn:dostepneSrodki?&gt;{data($xB_DOST_SRODKI[$occ])}&lt;/urn:dostepneSrodki&gt;
        &lt;urn:symbolWaluty?&gt;{data($xB_WALUTA[$occ])}&lt;/urn:symbolWaluty&gt;
        &lt;urn:kodWaluty?&gt;{data($xB_KOD_WALUTY[$occ])}&lt;/urn:kodWaluty&gt;
        &lt;urn:dataOstatniejOperacji?&gt;{xf:mapDate($xB_DATA_OST_OPER[$occ])}&lt;/urn:dataOstatniejOperacji&gt;
        &lt;urn:typRachunku?&gt;{data($xB_TYP_RACH[$occ])}&lt;/urn:typRachunku&gt;
        &lt;urn:opisRachunku?&gt;{data($xB_OPIS_RACH[$occ])}&lt;/urn:opisRachunku&gt;
        &lt;urn:numerOddzialu?&gt;{data($xB_ID_ODDZ[$occ])}&lt;/urn:numerOddzialu&gt;
        &lt;urn:nazwaOddzialu?&gt;{data($xB_NAZWA_ODDZ[$occ])}&lt;/urn:nazwaOddzialu&gt;
        &lt;urn:rodzajRachunku?&gt;{data($xB_RODZAJ_RACH[$occ])}&lt;/urn:rodzajRachunku&gt;
        &lt;urn:oprocentowanie?&gt;{data($xB_OPROC1[$occ])}&lt;/urn:oprocentowanie&gt;
        &lt;urn:dataOtwarciaRachunku?&gt;{xf:mapDate($xB_D_OTWARCIA[$occ])}&lt;/urn:dataOtwarciaRachunku&gt;
        &lt;urn:dataOstatniejKapitalizacji?&gt;{xf:mapDate($xB_D_OSTATNIEJ_KAP[$occ])}&lt;/urn:dataOstatniejKapitalizacji&gt;
        &lt;urn:dataNastepnejKapitalizacji?&gt;{xf:mapDate($xB_D_NASTEPNEJ_KAP[$occ])}&lt;/urn:dataNastepnejKapitalizacji&gt;
        &lt;urn:dataPoczatkuWkladu?&gt;{xf:mapDate($xB_D_POCZ_WKL[$occ])}&lt;/urn:dataPoczatkuWkladu&gt;
        &lt;urn:dataKoncaWkladu?&gt;{xf:mapDate($xB_D_KON_WKL[$occ])}&lt;/urn:dataKoncaWkladu&gt;
        &lt;urn:odsetkiSkapitalizowane?&gt;{data($xB_ODS_SKAP_AKT[$occ])}&lt;/urn:odsetkiSkapitalizowane&gt;
        &lt;urn:odsetkiZaksiegowane?&gt;{data($xB_ODS_SKAP_POP[$occ])}&lt;/urn:odsetkiZaksiegowane&gt;
        &lt;urn:limitKredytowy?&gt;{data($xB_LIMIT2[$occ])}&lt;/urn:limitKredytowy&gt;
        &lt;urn:dataKoncaLimitu?&gt;{xf:mapDate($xB_D_KONCA_LIMITU[$occ])}&lt;/urn:dataKoncaLimitu&gt;
        &lt;urn:kwotaZablokowana?&gt;{data($xB_BLOKADA[$occ])}&lt;/urn:kwotaZablokowana&gt;
        &lt;urn:rodzajOprocentowania?&gt;{data($xB_RODZAJ_OPROC[$occ])}&lt;/urn:rodzajOprocentowania&gt;
        &lt;urn:kapitalizacja?&gt;{xf:toBoolean($xB_KAPITALIZACJA[$occ])}&lt;/urn:kapitalizacja&gt;
        &lt;urn:odnawialnosc?&gt;{xf:toBooleanRev($xB_PRZEKS_KAP[$occ])}&lt;/urn:odnawialnosc&gt;
        &lt;urn:okresWkladu?&gt;{data($xB_OKR_WKLADU[$occ])}&lt;/urn:okresWkladu&gt;
        &lt;urn:jednostkaOkresuWkladu?&gt;{data($xB_JDN_OKR_WKLADU[$occ])}&lt;/urn:jednostkaOkresuWkladu&gt;
        &lt;urn:prawaKlienta?&gt;{data($xB_PRAWA_KLIENTA[$occ])}&lt;/urn:prawaKlienta&gt;
        &lt;urn:nazwaKlienta?&gt;{data($xB_NAZWA_KLIENTA[$occ])}&lt;/urn:nazwaKlienta&gt;
        &lt;urn:ulica?&gt;{data($xB_UL_ZAM[$occ])}&lt;/urn:ulica&gt;
        &lt;urn:miejscowosc?&gt;{data($xB_M_ZAM[$occ])}&lt;/urn:miejscowosc&gt;
        &lt;urn:rodzajDokumentu?&gt;{data($xB_RODZ_DOK[$occ])}&lt;/urn:rodzajDokumentu&gt;
        &lt;urn:numerDokumentu?&gt;{data($xB_NR_DOK[$occ])}&lt;/urn:numerDokumentu&gt;
        &lt;urn:dataNajblizszejPlatnosci?&gt;{xf:mapDate($xB_DATA_B_PLAT[$occ])}&lt;/urn:dataNajblizszejPlatnosci&gt;
        &lt;urn:numerUmowyKredytowej?&gt;{data($xB_NR_UMOWY[$occ])}&lt;/urn:numerUmowyKredytowej&gt;
        &lt;urn:typRachunkuICBS?&gt;{data($xB_ITYP[$occ])}&lt;/urn:typRachunkuICBS&gt;
      &lt;/urn:rachunek&gt;
    }
    &lt;/urn:listaRachunkow&gt;
  &lt;/urn:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>