<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:directdebit.entities.be.dcl";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:cif.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function sourceValue2Boolean ($parm as xs:string?,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{

      if ($value) then 
        if(string-length($value)&gt;5 and not (string(fn-bea:date-from-string-with-format($dateFormat,$value)) = "0001-01-01"))
             then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
};

declare function getElementsForAccountRelationshipList($parm as element(fml:FML32)) as element()
{

&lt;ns0:accountRelationshipList&gt;
  {
    for $x at $occ in 1
    return
    &lt;ns7:AccountRelationship&gt;
      &lt;ns7:customer&gt;
        &lt;ns7:Customer&gt;
          &lt;ns7:companyName?&gt;{data($parm/NF_CUSTOM_COMPANYNAME[$occ])}&lt;/ns7:companyName&gt;
          &lt;ns7:customerPersonal&gt;
            &lt;ns7:CustomerPersonal&gt;
              &lt;ns7:lastName?&gt;{data($parm/NF_CUSTOP_LASTNAME[$occ])}&lt;/ns7:lastName&gt;
              &lt;ns7:firstName?&gt;{data($parm/NF_CUSTOP_FIRSTNAME[$occ])}&lt;/ns7:firstName&gt;
            &lt;/ns7:CustomerPersonal&gt;
          &lt;/ns7:customerPersonal&gt;
          &lt;ns7:customerType&gt;
            &lt;ns4:CustomerType&gt;
              &lt;ns4:customerType?&gt;{data($parm/NF_CUSTOT_CUSTOMERTYPE[$occ])}&lt;/ns4:customerType&gt;
            &lt;/ns4:CustomerType&gt;
          &lt;/ns7:customerType&gt;
        &lt;/ns7:Customer&gt;
      &lt;/ns7:customer&gt;
    &lt;/ns7:AccountRelationship&gt;
  }
&lt;/ns0:accountRelationshipList&gt;
};

declare function getElementsForTransferOrderTargetList($parm as element(fml:FML32)) as element()
{

&lt;ns2:transferOrderTargetList&gt;
  {
    for $x at $occ in $parm/NF_TRANOT_AMOUNTTARGET
    return
    &lt;ns2:TransferOrderTarget&gt;
      &lt;ns2:amountTarget?&gt;{data($parm/NF_TRANOT_AMOUNTTARGET[$occ])}&lt;/ns2:amountTarget&gt;
      &lt;ns2:codeProfile?&gt;{data($parm/NF_TRANOT_CODEPROFILE[$occ])}&lt;/ns2:codeProfile&gt;
      &lt;ns2:transferOrderDescription?&gt;{data($parm/NF_TRANOT_TRANSFERORDERDES[$occ])}&lt;/ns2:transferOrderDescription&gt;
      &lt;ns2:beneficiary1?&gt;{data($parm/NF_TRANOT_BENEFICIARY1[$occ])}&lt;/ns2:beneficiary1&gt;
      &lt;ns2:beneficiary2?&gt;{data($parm/NF_TRANOT_BENEFICIARY2[$occ])}&lt;/ns2:beneficiary2&gt;
      &lt;ns2:beneficiary3?&gt;{data($parm/NF_TRANOT_BENEFICIARY3[$occ])}&lt;/ns2:beneficiary3&gt;
      &lt;ns2:beneficiary4?&gt;{data($parm/NF_TRANOT_BENEFICIARY4[$occ])}&lt;/ns2:beneficiary4&gt;
      &lt;ns2:account&gt;
        &lt;ns0:Account&gt;
          &lt;ns0:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns0:accountNumber&gt;
        &lt;/ns0:Account&gt;
      &lt;/ns2:account&gt;
      &lt;ns2:accountOutside&gt;
        &lt;ns2:AccountOutside&gt;
          &lt;ns2:accountNumber?&gt;{data($parm/NF_ACCOUO_ACCOUNTNUMBER[$occ])}&lt;/ns2:accountNumber&gt;
        &lt;/ns2:AccountOutside&gt;
      &lt;/ns2:accountOutside&gt;
      &lt;ns2:glaccount&gt;
        &lt;ns0:GLAccount&gt;
          &lt;ns0:costCenterGL?&gt;{data($parm/NF_GLA_COSTCENTERGL[$occ])}&lt;/ns0:costCenterGL&gt;
          &lt;ns0:accountGL?&gt;{data($parm/NF_GLA_ACCOUNTGL[$occ])}&lt;/ns0:accountGL&gt;
          &lt;ns0:currencyCode&gt;
            &lt;ns4:CurrencyCode&gt;
             {
              if (string-length(data($parm/NF_GLA_ACCOUNTGL[$occ])) &gt; 0) then
                   &lt;ns4:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns4:currencyCode&gt;
              else
                    &lt;ns4:currencyCode?&gt;&lt;/ns4:currencyCode&gt;
             }
            &lt;/ns4:CurrencyCode&gt;
          &lt;/ns0:currencyCode&gt;
        &lt;/ns0:GLAccount&gt;
      &lt;/ns2:glaccount&gt;
      &lt;ns2:currencyCode&gt;
        &lt;ns4:CurrencyCode&gt;
          &lt;ns4:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns4:currencyCode&gt;
        &lt;/ns4:CurrencyCode&gt;
      &lt;/ns2:currencyCode&gt;
    &lt;/ns2:TransferOrderTarget&gt;
  }
&lt;/ns2:transferOrderTargetList&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns6:invokeResponse&gt;
  &lt;ns6:directDebitOut&gt;
    &lt;ns1:DirectDebit&gt;
      &lt;ns1:originatorReference?&gt;{data($parm/NF_DIRECD_ORIGINATORREFERE)}&lt;/ns1:originatorReference&gt;
      &lt;ns1:additionalDescription?&gt;{data($parm/NF_DIRECD_ADDITIONALDESCRI)}&lt;/ns1:additionalDescription&gt;
    &lt;/ns1:DirectDebit&gt;
  &lt;/ns6:directDebitOut&gt;
  &lt;ns6:orderOut&gt;
    &lt;ns2:TransferOrderAnchor&gt;
      &lt;ns2:frequency?&gt;{data($parm/NF_TRANOA_FREQUENCY)}&lt;/ns2:frequency&gt;
      {insertDate ($parm/NF_TRANOA_PAYMENTDATE,"yyyy-MM-dd","ns2:paymentDate")}
      {insertDate ($parm/NF_TRANOA_EXPIRATIONDATE,"yyyy-MM-dd","ns2:expirationDate")}
      (:&lt;ns2:paymentDate?&gt;{data($parm/NF_TRANOA_PAYMENTDATE)}&lt;/ns2:paymentDate&gt;:)
      (:&lt;ns2:expirationDate?&gt;{data($parm/NF_TRANOA_EXPIRATIONDATE)}&lt;/ns2:expirationDate&gt;:)
      &lt;ns2:codeProfile?&gt;{data($parm/NF_TRANOA_CODEPROFILE)}&lt;/ns2:codeProfile&gt;
      &lt;ns2:feeCode?&gt;{data($parm/NF_TRANOA_FEECODE)}&lt;/ns2:feeCode&gt;
      &lt;ns2:nonBusinessDayRule?&gt;{sourceValue2Boolean(data($parm/NF_TRANOA_NONBUSINESSDAYRU),"1")}&lt;/ns2:nonBusinessDayRule&gt;
      &lt;ns2:charging?&gt;{sourceValue2Boolean(data($parm/NF_TRANOA_CHARGING),"1")}&lt;/ns2:charging&gt;
      (:&lt;ns2:nonBusinessDayRule?&gt;{data($parm/NF_TRANOA_NONBUSINESSDAYRU)}&lt;/ns2:nonBusinessDayRule&gt;:)
      (:&lt;ns2:charging?&gt;{data($parm/NF_TRANOA_CHARGING)}&lt;/ns2:charging&gt;:)
      &lt;ns2:active?&gt;{sourceValue2Boolean(data($parm/NF_TRANOA_ACTIVE),"0")}&lt;/ns2:active&gt;
      &lt;ns2:userID?&gt;{data($parm/NF_TRANOA_USERID)}&lt;/ns2:userID&gt;
      {insertDate ($parm/NF_TRANOA_FIRSTPAYMENTDATE,"yyyy-MM-dd","ns2:firstPaymentDate")}
      (:&lt;ns2:firstPaymentDate?&gt;{data($parm/NF_TRANOA_FIRSTPAYMENTDATE)}&lt;/ns2:firstPaymentDate&gt;:)
      &lt;ns2:feeAmount?&gt;{data($parm/NF_TRANOA_FEEAMOUNT)}&lt;/ns2:feeAmount&gt;
      {getElementsForTransferOrderTargetList($parm)}
      &lt;ns2:account&gt;
        &lt;ns0:Account&gt;
          &lt;ns0:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTIBAN)}&lt;/ns0:accountNumber&gt;
          {getElementsForAccountRelationshipList($parm)}
        &lt;/ns0:Account&gt;
      &lt;/ns2:account&gt;
      &lt;ns2:period&gt;
        &lt;ns4:Period&gt;
          &lt;ns4:period?&gt;{data($parm/NF_PERIOD_PERIOD)}&lt;/ns4:period&gt;
        &lt;/ns4:Period&gt;
      &lt;/ns2:period&gt;
      &lt;ns2:specificDay&gt;
        &lt;ns4:SpecialDay&gt;
          &lt;ns4:specialDay?&gt;{data($parm/NF_SPECID_SPECIALDAY)}&lt;/ns4:specialDay&gt;
        &lt;/ns4:SpecialDay&gt;
      &lt;/ns2:specificDay&gt;
    &lt;/ns2:TransferOrderAnchor&gt;
  &lt;/ns6:orderOut&gt;
&lt;/ns6:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>