<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:executiondict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace ns4="urn:executions.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDateTime($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns3:invokeResponse&gt;
  &lt;ns3:executionHistoryOut&gt;
    &lt;ns4:ExecutionHistory&gt;
      { insertDateTime(data($parm/NF_EXECUH_OPERATIONDATE),"yyyy-MM-dd-hh.mm.ss","ns4:operationDate")}
      &lt;ns4:operationTotalAmount?&gt;{data($parm/NF_EXECUH_OPERATIONTOTALAM)}&lt;/ns4:operationTotalAmount&gt;
      &lt;ns4:operationExecutionAmount?&gt;{data($parm/NF_EXECUH_OPERATIONEXECUTI)}&lt;/ns4:operationExecutionAmount&gt;
      &lt;ns4:operationExecutionExpense?&gt;{data($parm/NF_EXECUH_OPERATIONEXEEXP)}&lt;/ns4:operationExecutionExpense&gt;
      &lt;ns4:operationOtherExecutionExpense?&gt;{data($parm/NF_EXECUH_OPERATIONOTHEREX)}&lt;/ns4:operationOtherExecutionExpense&gt;
      &lt;ns4:operationExecutorInterest?&gt;{data($parm/NF_EXECUH_OPERATIONEXEINT)}&lt;/ns4:operationExecutorInterest&gt;
      &lt;ns4:operationBankInterest?&gt;{data($parm/NF_EXECUH_OPERATIONBANKINT)}&lt;/ns4:operationBankInterest&gt;
      &lt;ns4:operationTransferFee?&gt;{data($parm/NF_EXECUH_OPERATIONTRANSFE)}&lt;/ns4:operationTransferFee&gt;
      &lt;ns4:operationExecutorCharge?&gt;{data($parm/NF_EXECUH_OPERATIONEXECUTO)}&lt;/ns4:operationExecutorCharge&gt;
      &lt;ns4:operationType&gt;
        &lt;ns0:ExecutionOperationType&gt;
          &lt;ns0:executionOperationType?&gt;{data($parm/NF_EXECOT_EXECUTIONOPERATI)}&lt;/ns0:executionOperationType&gt;
        &lt;/ns0:ExecutionOperationType&gt;
      &lt;/ns4:operationType&gt;
    &lt;/ns4:ExecutionHistory&gt;
  &lt;/ns3:executionHistoryOut&gt;
&lt;/ns3:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>