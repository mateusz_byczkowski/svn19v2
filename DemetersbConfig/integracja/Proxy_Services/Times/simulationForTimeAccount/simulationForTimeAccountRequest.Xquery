<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace urn="urn:dcl:services.alsb.datamodel";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap:Body) external;

declare function DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};

declare function prepareSimulationForTimeAccountRequest( $acc as element(urn:entities.accounts.Account), $date as element(urn:baseauxentities.DateHolder))
               as element(xpcml){
   let $accnbr:= $acc/urn:accountNumber
   let $dateval:= $date/urn:value

  return 
&lt;xpcml xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="xpcml.xsd" version="4.0"&gt;

&lt;struct name="DSATAB30401"&gt;
      &lt;zonedDecimalParm name="A304SALB" passDirection="inherit" totalDigits="15" fractionDigits="2"/&gt;
      &lt;zonedDecimalParm name="A304ODSN" passDirection="inherit" totalDigits="15" fractionDigits="2"/&gt;
      &lt;zonedDecimalParm name="A304ODSO" passDirection="inherit" totalDigits="15" fractionDigits="2"/&gt;
      &lt;zonedDecimalParm name="A304ODSDW" passDirection="inherit" totalDigits="15" fractionDigits="2"/&gt;
      &lt;zonedDecimalParm name="A304ODSW" passDirection="inherit" totalDigits="15" fractionDigits="2"/&gt;
      &lt;zonedDecimalParm name="A304KWWP" passDirection="inherit" totalDigits="15" fractionDigits="2"/&gt;
      &lt;zonedDecimalParm name="A304PODN" passDirection="inherit" totalDigits="15" fractionDigits="2"/&gt;
      &lt;zonedDecimalParm name="A304PROW" passDirection="inherit" totalDigits="15" fractionDigits="2"/&gt;
      &lt;zonedDecimalParm name="A304ODSSK" passDirection="inherit" totalDigits="15" fractionDigits="2"/&gt;
      &lt;zonedDecimalParm name="A304PODPBR" passDirection="inherit" totalDigits="15" fractionDigits="2"/&gt;
&lt;/struct&gt;
   
&lt;program name="program" path="/QSYS.LIB/AIBINTMRST.LIB/ATA0304.PGM"&gt; 
&lt;parameterList&gt;
      &lt;zonedDecimalParm name="PZACCT" passDirection="in" totalDigits="10" fractionDigits="0"&gt;{data($accnbr)}&lt;/zonedDecimalParm&gt;
      &lt;stringParm name="PSWDAT" passDirection="in" length="10"&gt;{DateTime2CYMD(data($dateval))}&lt;/stringParm &gt;
      &lt;zonedDecimalParm name="PZRETCODE" passDirection="inout" totalDigits="4" fractionDigits="0"&gt;2&lt;/zonedDecimalParm&gt;
      &lt;structParm name="DSRETVAL" passDirection="out" struct="DSATAB30401"/&gt;
   &lt;/parameterList&gt; 
&lt;/program&gt;

&lt;/xpcml&gt;
};


&lt;soap:Body&gt;
{prepareSimulationForTimeAccountRequest($body/urn:invoke/urn:account/urn:entities.accounts.Account, $body/urn:invoke/urn:date/urn:baseauxentities.DateHolder)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>