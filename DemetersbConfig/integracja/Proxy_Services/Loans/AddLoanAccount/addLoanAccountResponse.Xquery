<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $fml := $body/fml:FML32;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))&gt;0)
    then true()
    else false()
};

&lt;soap-env:Body&gt;
  &lt;m:invokeResponse xmlns:urn="urn:be.services.dcl"&gt;
    &lt;m:accountOut xmlns:urn1="urn:accounts.entities.be.dcl"&gt;
      &lt;m1:Account&gt;
        {
        let $DC_NR_RACHUNKU := $fml/fml:DC_NR_RACHUNKU

        return

        for $it at $p in $DC_NR_RACHUNKU
           return
            if($DC_NR_RACHUNKU[$p] and xf:isData($DC_NR_RACHUNKU[$p]) and string-length($DC_NR_RACHUNKU[$p])=26)
               then &lt;m1:accountNumber&gt;{data($DC_NR_RACHUNKU[$p])}&lt;/m1:accountNumber&gt;
               else ()
        }
      &lt;/m1:Account&gt;
    &lt;/m:accountOut&gt;
  &lt;/m:invokeResponse&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>