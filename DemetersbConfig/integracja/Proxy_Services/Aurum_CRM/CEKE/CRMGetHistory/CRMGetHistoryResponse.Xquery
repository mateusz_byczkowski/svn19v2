<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetHistoryResponse($fml as element(fml:FML32))
	as element(m:CRMGetHistoryResponse) {
		&lt;m:CRMGetHistoryResponse&gt;
			{
				let $B_SALDO := $fml/fml:B_SALDO
				let $B_DATA_OPER := $fml/fml:B_DATA_OPER
				let $B_DATA_WALUTY := $fml/fml:B_DATA_WALUTY
				let $B_KWOTA_OPER := $fml/fml:B_KWOTA_OPER
				let $B_TYTUL_OPER := $fml/fml:B_TYTUL_OPER
				let $B_LP := $fml/fml:B_LP
				let $B_KOD_OPER := $fml/fml:B_KOD_OPER
				let $B_NR_WYCIAGU := $fml/fml:B_NR_WYCIAGU
				let $B_ODDZIAL := $fml/fml:B_ODDZIAL
				let $B_ADRES_WN := $fml/fml:B_ADRES_WN
				let $B_ADRES_MA := $fml/fml:B_ADRES_MA
				let $B_DL_TYTUL_OPER := $fml/fml:B_DL_TYTUL_OPER
				let $B_NR_RACH_WN := $fml/fml:B_NR_RACH_WN
				let $B_NR_RACH_MA := $fml/fml:B_NR_RACH_MA
				let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
				let $B_OBROT_WN := $fml/fml:B_OBROT_WN
				let $B_OBROT_MA := $fml/fml:B_OBROT_MA
				let $B_D_POCZ_HIST := $fml/fml:B_D_POCZ_HIST
				let $B_DATA_KSIEG := $fml/fml:B_DATA_KSIEG
				let $B_KWOTA_WALUTY := $fml/fml:B_KWOTA_WALUTY
				let $B_KARTA := $fml/fml:B_KARTA
				let $B_SYSTEM := $fml/fml:B_SYSTEM
				let $B_NR_PUNKTU := $fml/fml:B_NR_PUNKTU
				for $it at $p in $fml/fml:B_LP
				return
					&lt;m:Entry&gt;
					{
						if($B_SALDO[$p])
							then &lt;m:Saldo&gt;{ data($B_SALDO[$p]) }&lt;/m:Saldo&gt;
						else ()
					}
					{
						if($B_DATA_OPER[$p])
							then &lt;m:DataOper&gt;{ data($B_DATA_OPER[$p]) }&lt;/m:DataOper&gt;
						else ()
					}
					{
						if($B_DATA_WALUTY[$p])
							then &lt;m:DataWaluty&gt;{ data($B_DATA_WALUTY[$p]) }&lt;/m:DataWaluty&gt;
						else ()
					}
					{
						if($B_KWOTA_OPER[$p])
							then &lt;m:KwotaOper&gt;{ data($B_KWOTA_OPER[$p]) }&lt;/m:KwotaOper&gt;
						else ()
					}
					{
						if($B_TYTUL_OPER[$p])
							then &lt;m:TytulOper&gt;{ data($B_TYTUL_OPER[$p]) }&lt;/m:TytulOper&gt;
						else ()
					}
					{
						if($B_LP[$p])
							then &lt;m:Lp&gt;{ data($B_LP[$p]) }&lt;/m:Lp&gt;
						else ()
					}
					{
						if($B_KOD_OPER[$p])
							then &lt;m:KodOper&gt;{ data($B_KOD_OPER[$p]) }&lt;/m:KodOper&gt;
						else ()
					}
					{
						if($B_NR_WYCIAGU[$p])
							then &lt;m:NrWyciagu&gt;{ data($B_NR_WYCIAGU[$p]) }&lt;/m:NrWyciagu&gt;
						else ()
					}
					{
						if($B_ODDZIAL[$p])
							then &lt;m:Oddzial&gt;{ data($B_ODDZIAL[$p]) }&lt;/m:Oddzial&gt;
						else ()
					}
					{
						if($B_ADRES_WN[$p])
							then &lt;m:AdresWn&gt;{ data($B_ADRES_WN[$p]) }&lt;/m:AdresWn&gt;
						else ()
					}
					{
						if($B_ADRES_MA[$p])
							then &lt;m:AdresMa&gt;{ data($B_ADRES_MA[$p]) }&lt;/m:AdresMa&gt;
						else ()
					}
					{
						if($B_DL_TYTUL_OPER[$p])
							then &lt;m:DlTytulOper&gt;{ data($B_DL_TYTUL_OPER[$p]) }&lt;/m:DlTytulOper&gt;
						else ()
					}
					{
						if($B_NR_RACH_WN[$p])
							then &lt;m:NrRachWn&gt;{ data($B_NR_RACH_WN[$p]) }&lt;/m:NrRachWn&gt;
						else ()
					}
					{
						if($B_NR_RACH_MA[$p])
							then &lt;m:NrRachMa&gt;{ data($B_NR_RACH_MA[$p]) }&lt;/m:NrRachMa&gt;
						else ()
					}
					{
						if($B_KOD_WALUTY[$p])
							then &lt;m:KodWaluty&gt;{ data($B_KOD_WALUTY[$p]) }&lt;/m:KodWaluty&gt;
						else ()
					}
					{
						if($B_OBROT_WN[$p])
							then &lt;m:ObrotWn&gt;{ data($B_OBROT_WN[$p]) }&lt;/m:ObrotWn&gt;
						else ()
					}
					{
						if($B_OBROT_MA[$p])
							then &lt;m:ObrotMa&gt;{ data($B_OBROT_MA[$p]) }&lt;/m:ObrotMa&gt;
						else ()
					}
					{
						if($B_D_POCZ_HIST[$p])
							then &lt;m:DPoczHist&gt;{ data($B_D_POCZ_HIST[$p]) }&lt;/m:DPoczHist&gt;
						else ()
					}
					{
						if($B_DATA_KSIEG[$p])
							then &lt;m:DataKsieg&gt;{ data($B_DATA_KSIEG[$p]) }&lt;/m:DataKsieg&gt;
						else ()
					}
					{
						if($B_KWOTA_WALUTY[$p])
							then &lt;m:KwotaWaluty&gt;{ data($B_KWOTA_WALUTY[$p]) }&lt;/m:KwotaWaluty&gt;
						else ()
					}
					{
						if($B_KARTA[$p])
							then &lt;m:Karta&gt;{ data($B_KARTA[$p]) }&lt;/m:Karta&gt;
						else ()
					}
					{
						if($B_SYSTEM[$p])
							then &lt;m:System&gt;{ data($B_SYSTEM[$p]) }&lt;/m:System&gt;
						else ()
					}
					{
						if($B_NR_PUNKTU[$p])
							then &lt;m:NrPunktu&gt;{ data($B_NR_PUNKTU[$p]) }&lt;/m:NrPunktu&gt;
						else ()
					}
					&lt;/m:Entry&gt;
			}
		&lt;/m:CRMGetHistoryResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapGetHistoryResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>