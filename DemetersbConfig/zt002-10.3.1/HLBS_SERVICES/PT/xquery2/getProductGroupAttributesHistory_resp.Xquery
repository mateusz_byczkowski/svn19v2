<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductGroupAttributesHistory_resp/";
declare namespace urn= "urn:be.services.dcl";
declare namespace urn1 = "urn:productstree.entities.be.dcl";

declare function xf:getProductGroupAttributesHistory_resp($fml as element())
    as element() {
	<urn:invokeResponse>
             <urn:productGroupAttributeHistory>
{
                for $i in 1 to count($fml/PT_ID_GROUP_ATTRIBUTES)
                return
                    <urn1:ProdGroupAttributesHist>
                            <urn1:idProductGroup>{ data($fml/PT_ID_GROUP[$i]) }</urn1:idProductGroup>
                            <urn1:thirdProductFeature>{ data($fml/PT_THIRD_PRODUCT_FEATURE[$i]) }</urn1:thirdProductFeature>                                    
                            <urn1:fifthProductFeature>{ data($fml/PT_FIFTH_PRODUCT_FEATURE[$i]) }</urn1:fifthProductFeature>
                            <urn1:codeProduct>{ data($fml/PT_CODE_PRODUCT[$i]) }</urn1:codeProduct>                                    
                            <urn1:firstProductFeature>{ data($fml/PT_FIRST_PRODUCT_FEATURE[$i]) }</urn1:firstProductFeature>                                    
                            <urn1:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }</urn1:userChangeSKP>                                    
                            <urn1:fourthProductFeature>{ data($fml/PT_FOURTH_PRODUCT_FEATURE[$i]) }</urn1:fourthProductFeature>
                            <urn1:secondProductFeature>{ data($fml/PT_SECOND_PRODUCT_FEATURE[$i]) }</urn1:secondProductFeature>                                    
                            <urn1:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }</urn1:dateChange>         
                    </urn1:ProdGroupAttributesHist>
            }
            </urn:productGroupAttributeHistory>
    </urn:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductGroupAttributesHistory_resp($fml)]]></con:xquery>
</con:xqueryEntry>