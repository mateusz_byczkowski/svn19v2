<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "http://bzwbk.com/services/prime/";
declare namespace fault = "http://bzwbk.com/services/BZWBK24flow/faults/";

declare function xf:mapFault($fau as element(soap-env:Fault))
	as element(fml:FML32) {
	<fml:FML32>
		{
			if ($fau/detail/*/exceptionItem) then
			(
				<fml:U_FAULT_STRING>{ data($fau/detail/*/exceptionItem/errorDescription) }</fml:U_FAULT_STRING>,
				<fml:U_ERROR_CODE>{ data($fau/detail/*/exceptionItem/errorCode1) }</fml:U_ERROR_CODE>,
                                <fml:U_ERROR_CODE>{ data($fau/detail/*/exceptionItem/errorCode2) }</fml:U_ERROR_CODE>
			) else (
				<fml:U_FAULT_STRING>{ data($fau/faultstring) }</fml:U_FAULT_STRING>,
                               <fml:U_ERROR_CODE>30</fml:U_ERROR_CODE>		
			)

		}
	</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapFault($body/soap-env:Fault) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>