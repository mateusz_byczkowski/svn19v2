<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-11-29</con:description>
    <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf="http://bzwbk.com/services/prime/functions";
declare namespace fml = "";
declare namespace fault="http://bzwbk.com/services/cerber/faults";
declare namespace m="http://bzwbk.com/services/cerber";

declare variable $body as element(soap-env:Body) external;

declare function xf:mapDateTime($dateTimeIn as xs:dateTime) as xs:string 
{
  let $string := $dateTimeIn cast as xs:string
  return
     fn:concat (fn:substring($string,1,10),' ',fn:substring($string,12,8))
};

declare function xf:mapResponse($resp as element (m:GetUserDataWithHashResponse))
	as element (FML32) 
{
    <FML32>
      <U_LAST_LOGIN>{xf:mapDateTime($resp/LastLoggedInDate)}</U_LAST_LOGIN>
      {
            if($resp/LastIncorrectLoginDate) then
                  <U_LAST_BAD_LOGIN>{xf:mapDateTime($resp/LastIncorrectLoginDate)}</U_LAST_BAD_LOGIN>
           else ()
       }

      {
        for $rights in $resp/Permissions return
        (
            <U_RIGHTS_ID>{ data($rights) }</U_RIGHTS_ID>
        )
      }
      {      
          for $attr in $resp/Attributes return
          (
              <U_USER_ATTR_NAME>{ data($attr/Name) }</U_USER_ATTR_NAME>
          )
      }
      {
          for $attr in $resp/Attributes return
          (
              <U_USER_ATTR_VALUE>{ data($attr/Value) }</U_USER_ATTR_VALUE>
          )
      }

    <U_ORG_UNIT>{data($resp/Units[IsMasterUnit='T']/IcbsNo)}</U_ORG_UNIT>
    <U_ERROR_CODE>0</U_ERROR_CODE>
    </FML32>
};

declare function xf:getErrorCode ($fau as element(soap-env:Fault)) as xs:integer
{
      if (boolean($fau/detail/fault:InvalidUserPasswordException)) then  110
     else  if (boolean($fau/detail/fault:UserBlockedException)) then  110
     else  if (boolean($fau/detail/fault:UserPasswordExpiredException)) then   116
     else  if (boolean($fau/detail/fault:IllegalApplicationAccessException)) then   127
     else  99
    };

declare function xf:mapFault($fau as element(soap-env:Fault))
    as element (FML32) 
{
    <FML32>
      <U_ERROR_CODE>{xf:getErrorCode($fau)}</U_ERROR_CODE>
    </FML32>
};



<soap-env:Body>
{
    if (boolean($body/m:GetUserDataWithHashResponse)) then 
    (
         xf:mapResponse($body/m:GetUserDataWithHashResponse)
    ) else if (boolean($body/soap-env:Fault)) then 
    (
         xf:mapFault($body/soap-env:Fault)
    ) 
    else ()
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>