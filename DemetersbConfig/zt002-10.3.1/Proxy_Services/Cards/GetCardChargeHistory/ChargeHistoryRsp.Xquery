<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace char="http://bzwbk.com/services/cortex/ChargeHistory";
declare namespace cards="http://bzwbk.com/services/cards";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;

<soap-env:Body>
{

	typeswitch($body/*)
		case $req as element(char:panReturn) return
			<cards:getHistoryResponse>
                                                          
                                                   {for $p in $req/*
                                                          return
                                                               <CardChargeHistoryData>
                                                                      <pan>{ data($p/pan) }</pan>
                                                                      <applyDate>{ data($p/applyDate) }</applyDate>
                                                                      <chargecur>{ data($p/chargecur) }</chargecur>
                                                                      <recid>{ data($p/recid) }</recid>
                                                                      <sysdate>{ data($p/sysdate) }</sysdate>
                                                                      <tlgid>{ data($p/tlgid) }</tlgid>
                                                                      <sourcesystem>{ data($p/sourcesystem) }</sourcesystem>
                                                                      <credit>{ data($p/credit) }</credit>
                                                                      <charge>{ data($p/charge) }</charge>
                                                                      <accno>{ data($p/accno) }</accno>
                                                                      <descr>{ data($p/descr) }</descr>
                                                               </CardChargeHistoryData>
                                                   }
                                                  </cards:getHistoryResponse>		
		default return <error>Error translating request message</error>
}	
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>