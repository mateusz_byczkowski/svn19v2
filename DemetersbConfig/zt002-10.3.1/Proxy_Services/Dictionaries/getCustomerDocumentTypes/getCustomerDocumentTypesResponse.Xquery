<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
<ns0:dicts>
  {
    let $opis := $parm/CI_OPIS_WARTOSCI_SLOWNIKA
    for $wartosc at $occ in $parm/CI_WARTOSC_SLOWNIKA
    return
    <ns3:CustomerDocumentType>
      <ns3:description?>{data($opis[$occ])}</ns3:description>
      {if (fn:string-length($wartosc)>0)
        then <ns3:customerDocumentType>{data($wartosc)}</ns3:customerDocumentType>
        else <ns3:customerDocumentType>{" "}</ns3:customerDocumentType>
      }
    </ns3:CustomerDocumentType>
  }
</ns0:dicts>
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
<ns0:invokeResponse>
  {getElementsForDicts($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>