<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Tomasz Krajewski  
 : @version 1.0
 : @since   2011-01-05
 :
 : wersja WSDLa: 03-01-2011 11:15:08
 :
 : $Proxy Services/Customers/getCustomerShareholderList2/getCustomerShareholderList2Response.xq$
 :
 :)

declare namespace ns2 = "";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Customers/cisGetCustomerShareholderList2/cisGetCustomerShareholderList2Response/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:isData($dataIn as xs:string) as xs:boolean {
	if (string-length(normalize-space(data($dataIn))) > 0)
    then true()
    else false()
};

declare function xf:mapDate($dateIn as xs:string) as xs:string {
	fn:concat(fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2))
};

declare function xf:cisGetCustomerShareholderList2Response($fML321 as element(ns2:FML32))
	as element(ns1:invokeResponse) {
		<ns1:invokeResponse>
			<ns1:customerList>
			{
				let $CustomerList := fn:distinct-values($fML321/ns2:DC_NUMER_KLIENTA)
				for $Customer in $CustomerList
				let $CustOcc := fn:index-of($fML321/ns2:DC_NUMER_KLIENTA, $Customer)[1]
				let $DateCustOpen := fn:normalize-space(data($fML321/ns2:DC_DATA_WPROWADZENIA[$CustOcc]))
				let $LastMntDate := fn:normalize-space(data($fML321/ns2:CI_DATA_AKTUALIZACJI[$CustOcc]))
				let $TaxIdDate := fn:normalize-space(data($fML321/ns2:CI_DATA_NIP[$CustOcc]))
				let $DateOfBirth := fn:normalize-space(data($fML321/ns2:DC_DATA_URODZENIA[$CustOcc]))
				return
				(
					<ns0:Customer>
						<ns0:companyName>{ data($fML321/ns2:CI_NAZWA_PELNA[$CustOcc]) }</ns0:companyName>
						<ns0:customerNumber>{ data($fML321/ns2:DC_NUMER_KLIENTA[$CustOcc]) }</ns0:customerNumber>
						{
							if ($DateCustOpen)
							then
							(
								<ns0:dateCustomerOpened>{
									xs:date(xf:mapDate($DateCustOpen))
								}</ns0:dateCustomerOpened>
							)
							else
							()
						}
						{
							if ($LastMntDate)
							then
							(
								<ns0:fileMaintenanceLastDate>{
									xs:date(xf:mapDate($LastMntDate))
								}</ns0:fileMaintenanceLastDate>
							)
							else
							()
						}
						<ns0:resident>{ xs:boolean( data($fML321/ns2:DC_REZ_NIEREZ[$CustOcc]) ) }</ns0:resident>
						<ns0:taxID>{ data($fML321/ns2:DC_NIP[$CustOcc]) }</ns0:taxID>
						<ns0:shortName>{ data($fML321/ns2:DC_NAZWA_SKROCONA[$CustOcc]) }</ns0:shortName>
						<ns0:crmDecision>{ data($fML321/ns2:CI_DECYZJA[$CustOcc]) }</ns0:crmDecision>
						{
							if ($TaxIdDate)
							then
							(
								<ns0:taxIdDate>{
									xs:date(xf:mapDate($TaxIdDate))
								}</ns0:taxIdDate>
							)
							else
							()
						}						
						<ns0:customerRelationshipList?>
						{
							for $i in 1 to count($fML321/ns2:DC_NUMER_KLIENTA)
							where (data($fML321/ns2:DC_NUMER_KLIENTA[$i]) eq $Customer)
							return
							(			
								if (xf:isData(data($fML321/ns2:DC_TYP_RELACJI[$i])))
								then
								(						
									<ns0:CustomerRelationship>
										<ns0:relationType>
											<ns3:CustomerRelations>
												<ns3:customerRelations>{ data($fML321/ns2:DC_TYP_RELACJI[$i]) }</ns3:customerRelations>
											</ns3:CustomerRelations>
										</ns0:relationType>
									</ns0:CustomerRelationship>
								)
								else
								()
							)
						}
						</ns0:customerRelationshipList>
						<ns0:customerPersonal>
							<ns0:CustomerPersonal>
								{
									if ($DateOfBirth)
									then
									(
										<ns0:dateOfBirth>{
											xs:date(xf:mapDate($DateOfBirth))
										}</ns0:dateOfBirth>
									)
									else
									()
								}									
								<ns0:lastName>{ data($fML321/ns2:DC_NAZWISKO[$CustOcc]) }</ns0:lastName>
								<ns0:pesel>{ data($fML321/ns2:DC_NR_PESEL[$CustOcc]) }</ns0:pesel>
								{
									if (xf:isData(data($fML321/ns2:DC_IMIE[$CustOcc])))
									then
									(
										<ns0:firstName>{ data($fML321/ns2:DC_IMIE[$CustOcc]) }</ns0:firstName>
									)
									else ()
								}
							</ns0:CustomerPersonal>
						</ns0:customerPersonal>
						<ns0:customerType>
							<ns3:CustomerType>
								<ns3:customerType>{ data($fML321/ns2:DC_TYP_KLIENTA[$CustOcc]) }</ns3:customerType>
							</ns3:CustomerType>
						</ns0:customerType>
					</ns0:Customer>
				)
			}
			</ns1:customerList>
		</ns1:invokeResponse>
};

declare variable $fML321 as element(ns2:FML32) external;
<soap-env:Body>
{
	xf:cisGetCustomerShareholderList2Response($fML321)
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>