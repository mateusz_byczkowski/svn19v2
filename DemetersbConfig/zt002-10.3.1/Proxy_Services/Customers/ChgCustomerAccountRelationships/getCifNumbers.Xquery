<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery>declare namespace ns6="urn:be.services.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";


declare function xf:getCifs($req as element(ns6:invoke))
	as element(Customers) {
		&lt;Customers&gt;
                     {
                       for $el in $req/ns6:account/ns1:Account/ns1:accountRelationshipList/ns2:AccountRelationship
                         return
                             if  ( string-length(data($el/ns2:customer/ns2:Customer/ns2:customerNumber)) &gt; 0)
                                 then &lt;NumerKlienta&gt;{data($el/ns2:customer/ns2:Customer/ns2:customerNumber)}&lt;/NumerKlienta&gt;
                                 else ()
                    }
		&lt;/Customers&gt;
};

declare variable $body as element(soap-env:Body) external;

 xf:getCifs($body/ns6:invoke)</con:xquery>
</con:xqueryEntry>