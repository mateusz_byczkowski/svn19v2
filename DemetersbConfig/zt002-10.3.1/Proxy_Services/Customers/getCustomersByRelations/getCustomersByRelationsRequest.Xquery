<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";

declare function xf:mapgetCustomersByRelationsRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		<fml:FML32>
			{
				<fml:CI_ID_WEW_PRAC?>{ data($req/urn:userId) }</fml:CI_ID_WEW_PRAC>
			}
			{
				<fml:DC_NUMER_KLIENTA?>{ data($req/urn:customer/urn1:Customer/urn1:customerNumber) }</fml:DC_NUMER_KLIENTA>
			}
			{
				<fml:CI_REKURENCJA?>N</fml:CI_REKURENCJA>
			}
			
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomersByRelationsRequest($body/urn:invoke) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>