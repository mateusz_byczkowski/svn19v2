<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>v.1.0.7  2009-11-04  LK    PT 58
v.1.0.6  2009-08-11  PKL  TEET 39354
v.1.0.5  2009-08-04  PKL  TEET 39924
v.1.0.4  2009-07-01  PKL  TEET 34513

Dodanie pola NF_PRODUG_VISIBILITYDCL | 
Obsługa filtrów NF_CTRL_PRODFILTER (NFE-604 DRA ZRSK LK)</con:description>
    <con:xquery><![CDATA[(:Change log :)
(: v.1.0.4  2009-07-01  PKL  TEET 34513 :)
(: v.1.0.5  2009-08-04  PKL  TEET 39924 :)
(: v.1.0.6  2009-08-11  PKL  TEET 39354 :)
(: v.1.0.7  2009-11-04  LK    PT 58 :)

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:entities.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:be.services.dcl";
declare namespace ns9="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare namespace xf="ble";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue($parm as xs:string*,$trueval as xs:string,$falseval as xs:string) as xs:string* {
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};

declare function boolean2ActiveNActiveValue($parm as xs:string*,$trueval as xs:string,$falseval as xs:string,$elseval as xs:string) as xs:string* {
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else if ($parm  = "false") then $falseval
    else $elseval
};

declare function checkValue($parm as xs:string*) as xs:string* {
 if (data($parm)) then 
    if (string(number(data($parm))) != 'NaN') 
  then data($parm)
    else "0"
 else "0"
};

declare function xf:productAreaIdPadding($x as element(ns8:product)) as element(xf:areaIds){
     <xf:areaIds>
       {
          for $el in $x/ns1:ProductDefinition/ns1:productGroup/ns1:ProductGroup/ns1:productCategory/ns1:ProductCategory/ns1:productArea/ns1:ProductArea/ns1:idProductArea
             let $ellen:=string-length(data($el))
             return
                if ($ellen = 1)
                   then <xf:areaId>{concat("0",data($el))}</xf:areaId>
                else if  ($ellen = 2)
                   then <xf:areaId>{data($el)}</xf:areaId>
                else ()
       }
     </xf:areaIds>
};

declare function xf:getNfCtrlAreas($x as element(ns8:product)) as element(NF_CTRL_AREAS)
{
   let $areaIds:=xf:productAreaIdPadding($x)
   let $areasConcatenated:=translate(concat(distinct-values(data($areaIds/xf:areaId)) ," ")," ","")
   return   
        if (string-length($areasConcatenated)>0)
           then <NF_CTRL_AREAS?>{$areasConcatenated}</NF_CTRL_AREAS>
           else <NF_CTRL_AREAS?>00</NF_CTRL_AREAS>
};

declare function xf:productIdPadding($x as element(ns8:product)) as element(xf:productIds){
     <xf:productIds>
       {
          for $el in $x/ns1:ProductDefinition/ns1:idProductDefinition
             let $ellen:=string-length(data($el))
             return
                if ($ellen = 1)
                   then <xf:productId>{concat("0000",data($el))}</xf:productId>
                else if ($ellen = 2)
                   then <xf:productId>{concat("000",data($el))}</xf:productId>
                else if ($ellen = 3)
                   then <xf:productId>{concat("00",data($el))}</xf:productId>
                else if ($ellen = 4)
                   then <xf:productId>{concat("0",data($el))}</xf:productId>
                else if  ($ellen = 5)
                   then <xf:productId>{data($el)}</xf:productId>
                else ()
       }
     </xf:productIds>
};

declare function xf:getNfCtrlProducts($x as element(ns8:product)) as element(NF_CTRL_PRODUCTS)
{
   let $productIds:=xf:productIdPadding($x)
   let $productsConcatenated:=translate(concat(distinct-values(data($productIds/xf:productId)) ," ")," ","")
   return   
        if (string-length($productsConcatenated)>0)
           then <NF_CTRL_PRODUCTS?>{$productsConcatenated}</NF_CTRL_PRODUCTS>
           else <NF_CTRL_PRODUCTS?>00000</NF_CTRL_PRODUCTS>
};

declare function xf:categoryIdPadding($x as element(ns8:product)) as element(xf:categoryIds){
     <xf:categoryIds>
       {
          for $el in $x/ns1:ProductDefinition/ns1:productGroup/ns1:ProductGroup/ns1:productCategory/ns1:ProductCategory/ns1:idProductCategory
             let $ellen:=string-length(data($el))
             return
                if ($ellen = 1)
                   then <xf:categoryId>{concat("00",data($el))}</xf:categoryId>
                else if ($ellen = 2)
                   then <xf:categoryId>{concat("0",data($el))}</xf:categoryId>
                else if ($ellen = 3)
                   then <xf:categoryId>{data($el)}</xf:categoryId>
                else ()
       }
     </xf:categoryIds>
};

declare function xf:getNfCtrlCategories($x as element(ns8:product)) as element(NF_CTRL_CATEGORIES)
{
   let $categoryIds:=xf:categoryIdPadding($x)
   let $categoriesConcatenated:=translate(concat(distinct-values(data($categoryIds/xf:categoryId)) ," ")," ","")
   return   
        if (string-length($categoriesConcatenated)>0)
           then <NF_CTRL_CATEGORIES?>{$categoriesConcatenated}</NF_CTRL_CATEGORIES>
           else <NF_CTRL_CATEGORIES?>000</NF_CTRL_CATEGORIES>
};

declare function xf:groupIdPadding($x as element(ns8:product)) as element(xf:groupIds){
     <xf:groupIds>
       {
          for $el in $x/ns1:ProductDefinition/ns1:productGroup/ns1:ProductGroup/ns1:idProductGroup
             let $ellen:=string-length(data($el))
             return
                if ($ellen = 1)
                   then <xf:groupId>{concat("000",data($el))}</xf:groupId>
                else if ($ellen = 2)
                   then <xf:groupId>{concat("00",data($el))}</xf:groupId>
                else if ($ellen = 3)
                   then <xf:groupId>{concat("0",data($el))}</xf:groupId>
                else if ($ellen = 4)
                   then <xf:groupId>{data($el)}</xf:groupId>
                else ()
       }
     </xf:groupIds>
};

declare function xf:getNfCtrlGroups($x as element(ns8:product)) as element(NF_CTRL_GROUPS)
{
   let $groupIds:=xf:groupIdPadding($x)
   let $groupsConcatenated:=translate(concat(distinct-values(data($groupIds/xf:groupId)) ," ")," ","")
   return   
        if (string-length($groupsConcatenated)>0)
           then <NF_CTRL_GROUPS?>{$groupsConcatenated}</NF_CTRL_GROUPS>
           else <NF_CTRL_GROUPS?>0000</NF_CTRL_GROUPS>
};

declare function xf:getNfCurrecCurrencycode($x as element(ns8:currencies)) as xs:string*
{
   let $currencyCodes:=data($x/ns5:CurrencyCode/ns5:currencyCode)
   let $result:= translate(concat(distinct-values($currencyCodes) ," ")," ","")
   return   
        if (string-length($result)>0) 
           then  $result
           else ()
};

declare function xf:getNfAccourRelationship($x as element(ns8:customer)) as xs:string*
{
   let $relationships:=data($x/ns3:Customer/ns3:accountRelationshipList/ns3:AccountRelationship/ns3:relationship/ns5:CustomerAccountRelationship/ns5:customerAccountRelationship)
   let $result:= translate(concat(distinct-values($relationships) ," ")," ","")
   return   
        if (string-length($result)>0) 
           then  $result
           else ()  
       };


declare function getFieldsFromHeader($parm as element(ns8:header)) as element()*
{
<NF_MSHEAD_COMPANYID?>{data($parm/ns8:msgHeader/ns8:companyId)}</NF_MSHEAD_COMPANYID>
};

declare function getFieldsFromInvoke($parm as element(ns8:invoke)) as element()*
{
xf:getNfCtrlAreas($parm/ns8:product)
,
xf:getNfCtrlProducts($parm/ns8:product)
,
xf:getNfCtrlCategories($parm/ns8:product)
,
xf:getNfCtrlGroups($parm/ns8:product)
,

(:
<NF_PRODUD_IDPRODUCTDEFINIT?>{data($parm/ns8:product/ns1:ProductDefinition/ns1:idProductDefinition)}</NF_PRODUD_IDPRODUCTDEFINIT>
,
:)

if(fn:string-length($parm/ns8:extraCharge/ns1:ProductAttributes/ns1:attributeValue)>0)
    (: T34513    then <NF_CTRL_PRODFILTER?>{concat("pt_id_area[10] UPPER(PT_ID_ATTRIBUTE[173]s) = '", data($parm/ns8:extraCharge/ns1:ProductAttributes/ns1:attributeValue), "'")}</NF_CTRL_PRODFILTER> :)
    then <NF_CTRL_PRODFILTER?>{concat("pt_id_area[10] UPPER(PT_ID_ATTRIBUTE[173]s) = UPPER('", data($parm/ns8:extraCharge/ns1:ProductAttributes/ns1:attributeValue), "')")}</NF_CTRL_PRODFILTER>
    else ()
,
if(fn:string-length($parm/ns8:flag/ns1:ProductAttributes/ns1:attributeValue)>0)
    (: T34513 then <NF_CTRL_PRODFILTER?>{concat("pt_id_area[13] UPPER(PT_ID_ATTRIBUTE[131]s) = '", data($parm/ns8:flag/ns1:ProductAttributes/ns1:attributeValue), "'")}</NF_CTRL_PRODFILTER> :)
    then <NF_CTRL_PRODFILTER?>{concat("pt_id_area[13] UPPER(PT_ID_ATTRIBUTE[131]s) = UPPER('", data($parm/ns8:flag/ns1:ProductAttributes/ns1:attributeValue), "')")}</NF_CTRL_PRODFILTER>
    else ()
,
<NF_CTRL_SHOWCURRBAL?>{checkValue($parm/ns8:showCurrentBalance/ns9:IntegerHolder/ns9:value)}</NF_CTRL_SHOWCURRBAL>
,
<NF_CURREC_CURRENCYCODE?>{xf:getNfCurrecCurrencycode($parm/ns8:currencies)}</NF_CURREC_CURRENCYCODE>
,
<NF_ACCOUR_RELATIONSHIP?>{xf:getNfAccourRelationship($parm/ns8:customer)}</NF_ACCOUR_RELATIONSHIP>
,
<NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns8:accountNumber/ns0:Account/ns0:accountNumber)}</NF_ACCOUN_ACCOUNTNUMBER>
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns9:PageControl/ns9:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns9:PageControl/ns9:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{boolean2SourceValue (data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns9:PageControl/ns9:reverseOrder),"1","0")}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns9:PageControl/ns9:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
,
(: T39924 <NF_CTRL_ACTIVENONACTIVE?>{boolean2SourceValue (data($parm/ns8:accountActive/ns9:BooleanHolder/ns9:value),"1","0")}</NF_CTRL_ACTIVENONACTIVE> :)
<NF_CTRL_ACTIVENONACTIVE?>{boolean2ActiveNActiveValue (data($parm/ns8:accountActive/ns9:BooleanHolder/ns9:value),"1","2", "0")}</NF_CTRL_ACTIVENONACTIVE>
,
<NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns8:customer/ns3:Customer/ns3:customerNumber)}</NF_CUSTOM_CUSTOMERNUMBER>
,
<NF_ACCOUN_ALTERNATIVEADDRE>0</NF_ACCOUN_ALTERNATIVEADDRE>
,
<NF_CTRL_HOLDCODE?>{data($parm/ns8:holdCode/ns9:IntegerHolder/ns9:value)}</NF_CTRL_HOLDCODE>
,
<NF_CTRL_SYSTEMS>001</NF_CTRL_SYSTEMS>
,
<NF_CTRL_OPTION>0</NF_CTRL_OPTION>
,
<NF_PRODUG_VISIBILITYDCL>1</NF_PRODUG_VISIBILITYDCL>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns8:header)}
    {getFieldsFromInvoke($body/ns8:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>