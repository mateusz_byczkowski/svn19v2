<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:customercertificatedict.dictionaries.be.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:customercertificates.entities.be.dcl";
declare namespace ns8="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForCertOut($parm as element(fml:FML32)) as element()
{

  <ns6:customerCertificate>
  {
    for $x at $occ in $parm/NF_CUSTCE_CERTIFICATENUMBE
    return

    <ns7:CustomerCertificate>
      <ns7:registerDate?>{data($parm/NF_CUSTCE_REGISTERDATE[$occ])}</ns7:registerDate>
      <ns7:certificateNumber?>{data($parm/NF_CUSTCE_CERTIFICATENUMBE[$occ])}</ns7:certificateNumber>
      <ns7:customCertificateDesc?>{data($parm/NF_CUSTCE_CUSTOMCERTIFICAT[$occ])}</ns7:customCertificateDesc>
      <ns7:preparationDate?>{data($parm/NF_CUSTCE_PREPARATIONDATE[$occ])}</ns7:preparationDate>
      <ns7:customerCertificateType>
        <ns0:CustomerCertificateType>
          <ns0:customerCertificateType?>{data($parm/NF_CUSTCT_CUSTOMERCERTIFIC[$occ])}</ns0:customerCertificateType>
        </ns0:CustomerCertificateType>
      </ns7:customerCertificateType>
      <ns7:registerBranch>
        <ns2:BranchCode>
          <ns2:branchCode?>{data($parm/NF_BASEBC_BRANCHCODE[$occ])}</ns2:branchCode>
        </ns2:BranchCode>
      </ns7:registerBranch>
      <ns7:certificateKind>
        <ns0:CustomerCertificateKind>
          <ns0:customerCertificateKind?>{data($parm/NF_CUSTCK_CUSTOMERCERTIFIC[$occ])}</ns0:customerCertificateKind>
        </ns0:CustomerCertificateKind>
      </ns7:certificateKind>
    </ns7:CustomerCertificate>

  }
  </ns6:customerCertificate>
};


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
<ns6:invokeResponse>

  {getElementsForCertOut($parm)}

  <ns6:bcd>
    <ns4:BusinessControlData>
      <ns4:pageControl>
        <ns8:PageControl>
          <ns8:hasNext?>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}</ns8:hasNext>
          <ns8:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns8:navigationKeyDefinition>
          <ns8:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns8:navigationKeyValue>
        </ns8:PageControl>
      </ns4:pageControl>
    </ns4:BusinessControlData>
  </ns6:bcd>

</ns6:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>