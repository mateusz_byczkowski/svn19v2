<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";


declare variable $cif as xs:string external;
declare variable $sysId as xs:string external;

		<fml:FML32>
		   <fml:DC_NUMER_KLIENTA>{ $cif  }</fml:DC_NUMER_KLIENTA>
		   <fml:CI_ID_SYS>{ $sysId }</fml:CI_ID_SYS>
		</fml:FML32>]]></con:xquery>
</con:xqueryEntry>