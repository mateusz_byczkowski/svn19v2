<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetProdRelRequest($req as element(m:CRMGetProdRelRequest))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($req/m:ApplinApplicationnumbe)
					then <fml:NF_APPLIN_APPLICATIONNUMBE>{ data($req/m:ApplinApplicationnumbe) }</fml:NF_APPLIN_APPLICATIONNUMBE>
					else ()
			}
			{
				if($req/m:AccounAccountnumber)
					then <fml:NF_ACCOUN_ACCOUNTNUMBER>{ data($req/m:AccounAccountnumber) }</fml:NF_ACCOUN_ACCOUNTNUMBER>
					else ()
			}
			{
				if($req/m:PagecPagesize)
					then <fml:NF_PAGEC_PAGESIZE>{ data($req/m:PagecPagesize) }</fml:NF_PAGEC_PAGESIZE>
					else ()
			}
			{
				if($req/m:PagecActioncode)
					then <fml:NF_PAGEC_ACTIONCODE>{ data($req/m:PagecActioncode) }</fml:NF_PAGEC_ACTIONCODE>
					else ()
			}
			{
				if($req/m:PagecReverseorder)
					then <fml:NF_PAGEC_REVERSEORDER>{ data($req/m:PagecReverseorder) }</fml:NF_PAGEC_REVERSEORDER>
					else ()
			}
			{
				if($req/m:PagecNavigationkeyvalu)
					then <fml:NF_PAGEC_NAVIGATIONKEYVALU>{ data($req/m:PagecNavigationkeyvalu) }</fml:NF_PAGEC_NAVIGATIONKEYVALU>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetProdRelRequest($body/m:CRMGetProdRelRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>