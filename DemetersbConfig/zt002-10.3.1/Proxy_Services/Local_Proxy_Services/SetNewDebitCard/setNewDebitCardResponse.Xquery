<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:card.entities.be.dcl";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns2="urn:be.services.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:dictionaries.be.dcl";
declare namespace ns7="urn:accounts.entities.be.dcl";
declare namespace ns6="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



(: declare function insertDate($value as xs:string,$dateFormat as xs:string,$fieldName as xs:string) as xs:string{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      }; :)


declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
     (: if ($value) then 
        if(string-length($value)>5 and not (string(fn-bea:date-from-string-with-format($dateFormat,$value)) = "0001-01-01"))
             then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else() :)
     
     if ($value) then 
        if(string-length($value)>5) then
           if ((substring($value, 3, 1) = "-") and ($value != "01-01-0001"))
              (:postac DD-MM-RRRR:)
               then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,string-join((substring($value,7,4),substring($value,4,2),substring($value,1,2)),'-'))}    
           else if (string(fn-bea:date-from-string-with-format($dateFormat,$value)) != "0001-01-01")
             then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
           else()
        else()
      else()
};



declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns2:invokeResponse>
  <ns2:response>
    <ns6:ResponseMessage>
      {
      if ($parm/NF_RESPOM_ERRORCODE)
           then <ns6:result>false</ns6:result>
           else <ns6:result>true</ns6:result>
       }    
      <ns6:errorCode?>{data($parm/NF_RESPOM_ERRORCODE)}</ns6:errorCode>
      <ns6:errorDescription?>{data($parm/DC_OPIS_BLEDU)}</ns6:errorDescription>
    </ns6:ResponseMessage>
  </ns2:response>
  <ns2:debitCard>
    <ns0:DebitCard>
      (: <ns0:expirationDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/DC_DATA_WAZNOSCI))}</ns0:expirationDate> :)
      {insertDate(data($parm/DC_DATA_WAZNOSCI),"yyyy-MM-dd","ns0:expirationDate")}
      (: <ns0:nextReissueDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/DC_DATA_URUCHOMIENIA))}</ns0:nextReissueDate> :)
      {insertDate(data($parm/DC_DATA_URUCHOMIENIA),"yyyy-MM-dd","ns0:nextReissueDate")}
     (: <ns0:nextCardFeeDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/DC_DATA_NALICZENIA))}</ns0:nextCardFeeDate> :)
	   {insertDate(data($parm/DC_DATA_NALICZENIA),"yyyy-MM-dd","ns0:nextCardFeeDate")}
      <ns0:cardNbr?>{data($parm/DC_NR_KARTY)}</ns0:cardNbr>
      (: <ns0:cardRecordOpenDate?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/DC_DATA_POCZ))}</ns0:cardRecordOpenDate> :)
        {insertDate(data($parm/DC_DATA_POCZ),"yyyy-MM-dd","ns0:cardRecordOpenDate")}
      <ns0:cardFee?>{data($parm/DC_KWOTA)}</ns0:cardFee>
      <ns0:virtualCardNbr?>{data($parm/DC_NR_KARTY_BIN)}</ns0:virtualCardNbr>
      <ns0:cardStatus>
        <ns1:CrdStatus>
          <ns1:crdStatus?>{data($parm/DC_STATUS_KARTY)}</ns1:crdStatus>
        </ns1:CrdStatus>
      </ns0:cardStatus>
{
      if(data($parm/DC_NR_PB1)) then
         if(string-length(data($parm/DC_NR_PB1))=16) then
           if(number(data($parm/DC_NR_PB1))>0) then
	  <ns0:debitCardLpInfo>
             <ns0:DebitCardLpInfo>
                  {
                  if(data($parm/DC_NR_PB2)) then
                    if(string-length(data($parm/DC_NR_PB2))=16) then
                        if(number(data($parm/DC_NR_PB2))>0) then
                          <ns0:customerLpNumber?>{data($parm/DC_NR_PB2)}</ns0:customerLpNumber>
                        else()
                     else()
                  else()
                  }
                  <ns0:lpNumber?>{data($parm/DC_NR_PB1)}</ns0:lpNumber>
              </ns0:DebitCardLpInfo>
          </ns0:debitCardLpInfo>
          else()
         else()
      else()
}


    </ns0:DebitCard>
  </ns2:debitCard>
</ns2:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>