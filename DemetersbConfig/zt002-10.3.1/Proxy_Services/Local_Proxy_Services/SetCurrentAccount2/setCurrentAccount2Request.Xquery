<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";

declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};


declare function xf:mapSetCurrentAccountRequest($req as element(ns1:Account), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

	let $unitId := $msghead/dcl:unitId
    let $transId:=$tranhead/dcl:transId

	let $customerNumber:= $req/ns1:customerNumber
    let $skpOpener:=$req/ns1:skpOpener
    let $productCode:=$req/ns1:productCode
    let $officerCode:=$req/ns1:officerCode
        
    (: encja TranAccount :)
    let $serviceChargeCode:=$req/ns1:tranAccount/ns1:TranAccount/ns1:serviceChargeCode
    let $interestPlanNumber:=$req/ns1:tranAccount/ns1:TranAccount/ns1:interestPlanNumber
    let $interestTransferAccount:=$req/ns1:tranAccount/ns1:TranAccount/ns1:interestTransferAccount
    let $promotionCode:=$req/ns1:tranAccount/ns1:TranAccount/ns1:promotionCode/ns2:PromotionCode/ns2:promotionCode
    let $accountTypeFlag:=$req/ns1:tranAccount/ns1:TranAccount/ns1:accountTypeFlag/ns2:AccountTypeFlag/ns2:accountTypeFlag
	
	(: Encja accountBranchNumber :)
    let $accountBranchNumber:=$req/ns1:accountBranchNumber/ns2:BranchCode/ns2:branchCode

	(: encja statementParameters :)
	let $cycle:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:cycle/ns2:Period/ns2:period
	let $frequency:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:frequency
	let $nextPrintoutDate:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:nextPrintoutDate
	(: let $specialDay:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:specialDay/ns2:SpecialDay/ns2:specialDay :)
	let $specialDay:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:specialDay
	let $provideManner:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:provideManner/ns2:ProvideManner/ns2:provideManner
	let $aggregateTransaction:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:aggregateTransaction
	
	return
          <fml:FML32>
            <DC_TRN_ID?>{data($transId)}</DC_TRN_ID>
            <DC_UZYTKOWNIK?>{concat("SKP:",data($skpOpener))}</DC_UZYTKOWNIK>
            <DC_ODDZIAL?>{chkUnitId(data($unitId))}</DC_ODDZIAL>
            <DC_NUMER_KLIENTA?>{data($customerNumber)}</DC_NUMER_KLIENTA>
            <DC_RELACJA>SOW</DC_RELACJA>
            <DC_TYP_RELACJI>1</DC_TYP_RELACJI>
            <DC_PROCENT_ZOB_PODATKOWYCH>100.0000</DC_PROCENT_ZOB_PODATKOWYCH>
            <DC_NUMER_PRODUKTU?>{data($productCode)}</DC_NUMER_PRODUKTU>
            <DC_NUMER_ODDZIALU?>{data($accountBranchNumber)}</DC_NUMER_ODDZIALU>
            <DC_RODZAJ_RACHUNKU>DM</DC_RODZAJ_RACHUNKU>
            <DC_RACHUNEK>{data($accountTypeFlag)}</DC_RACHUNEK>
            <DC_PLAN_OPLAT?>{data($serviceChargeCode)}</DC_PLAN_OPLAT>
            <DC_PLAN_ODSETKOWY?>{data($interestPlanNumber)}</DC_PLAN_ODSETKOWY>
            
		  	<DC_PROMOCJA?>{data($promotionCode)}</DC_PROMOCJA>
			
			<DC_KOD_CYKLU_WYCIAGOW?>{data($cycle)}</DC_KOD_CYKLU_WYCIAGOW>
			<DC_LICZBA_OKRESOW_CYKL_WYCIAG?>{data($frequency)}</DC_LICZBA_OKRESOW_CYKL_WYCIAG>
            {
               if ( string-length($officerCode)>0 )
                 then <DC_KOD_PRACOWNIKA>{data($officerCode)}</DC_KOD_PRACOWNIKA>
                 else   <DC_KOD_PRACOWNIKA>DCL</DC_KOD_PRACOWNIKA>                 
             } 

			{
				if ($nextPrintoutDate)
					then 
						<DC_DATA_NASTEPNEGO_WYCIAGU?>{xf:DateTime2CYMD(data($nextPrintoutDate))}</DC_DATA_NASTEPNEGO_WYCIAGU>					
					else ()
			}
			<DC_OKRESLONY_DZIEN_WYCIAGU?>{data($specialDay)}</DC_OKRESLONY_DZIEN_WYCIAGU>
			<DC_KOD_SPECJALNYCH_INSTRUKCJI?>{data($provideManner)}</DC_KOD_SPECJALNYCH_INSTRUKCJI>			
			{
				if ($aggregateTransaction)
                 then  if (data($aggregateTransaction) = "true")
                   then <DC_ZBIJANIE_TRANSAKCJI>1</DC_ZBIJANIE_TRANSAKCJI>
                   else <DC_ZBIJANIE_TRANSAKCJI>0</DC_ZBIJANIE_TRANSAKCJI>
                 else()
			}			
          </fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


<soap-env:Body>
{ xf:mapSetCurrentAccountRequest($body/dcl:invoke/dcl:account/ns1:Account, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>