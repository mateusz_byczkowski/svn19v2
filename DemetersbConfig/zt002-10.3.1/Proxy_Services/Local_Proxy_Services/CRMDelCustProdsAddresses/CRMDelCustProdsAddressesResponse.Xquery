<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-11-18</con:description>
    <con:xquery><![CDATA[(: Log Zmian: 
==================================
v.1.0  2010-11-12 PKLI NP2173_1 Utworzenie

:)
declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:getElementsForResponseList($fml as element(fml:FML32)) as element()* {
 let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
  let $DC_NR_PODSYSTEMU := $fml/fml:DC_NR_PODSYSTEMU
  let $DC_KOD_WYKLUCZENIA := $fml/fml:DC_KOD_WYKLUCZENIA
  let $DC_TYLKO_STATUS := $fml/fml:DC_TYLKO_STATUS
  let $DC_OPIS_BLEDU := $fml/fml:DC_OPIS_BLEDU
 	                        
  for $it at $p in $fml/fml:DC_NUMER_KLIENTA return
    <m:DelCustProdsAddressesResponse>
    {
      if($DC_NUMER_KLIENTA[$p])
      then <m:numerKlienta>{ data($DC_NUMER_KLIENTA[$p]) }</m:numerKlienta>
      else ()
    } 
    {
      if($DC_NR_PODSYSTEMU[$p])
      then <m:idSystemu>{ data($DC_NR_PODSYSTEMU[$p]) }</m:idSystemu>
      else ()
    } 
    {
      if($DC_KOD_WYKLUCZENIA[$p])
      then <m:statusPrzetwarzania>{ data($DC_KOD_WYKLUCZENIA[$p]) }</m:statusPrzetwarzania>
      else ()
    } 
    {
      if($DC_TYLKO_STATUS[$p])
      then <m:kodBledu>{ data($DC_TYLKO_STATUS[$p]) }</m:kodBledu>
      else ()
    } 
    {
      if($DC_OPIS_BLEDU[$p])
      then <m:opisBledu>{ data($DC_OPIS_BLEDU[$p]) }</m:opisBledu>
      else ()
    }
    </m:DelCustProdsAddressesResponse>
};

declare function xf:mapCRMDelCustProdsAddressesResponse($fml as element(fml:FML32)) as element() {
  <m:CRMDelCustProdsAddressesResponse>
    {xf:getElementsForResponseList($fml)}  
  </m:CRMDelCustProdsAddressesResponse>
 };

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
  { xf:mapCRMDelCustProdsAddressesResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>