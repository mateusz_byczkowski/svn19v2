<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$2.2010-12-10</con:description>
    <con:xquery><![CDATA[(: Log zmian
v.1.1  2010-12-01 PKLI TP58  Modyfikacja Error Handlera DelCstPrdsAdrRErrorResponse

:)

declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace fml="";

declare variable $body external;

  <soap-env:Body>
	{
	  (: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
	  let $reason := fn:substring-before(fn:substring-after(fn:substring-before($body/ctx:reason, ":"), "("), ")")
	  let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($body/ctx:reason, ":"), ":"), ":")
         let $errorCode:= data($body/ctx:errorCode)
	  return
           <FML32>       
              {
              if( string-length(data($reason)) = 0 or $reason = "12")
                 then (
                    <DC_TYLKO_STATUS>12</DC_TYLKO_STATUS>,
                    <DC_OPIS_BLEDU>Krytyczny blad uslugi w systemie zrodlowym</DC_OPIS_BLEDU>)
                 else if ($reason="13") then (
                        <DC_TYLKO_STATUS>{$reason}</DC_TYLKO_STATUS>,
                        <DC_OPIS_BLEDU>Przekroczenie maksymalnego czasu oczekiwania na odpowiedz</DC_OPIS_BLEDU>)
                 else if ($reason="6") then (
                        <DC_TYLKO_STATUS>{$reason}</DC_TYLKO_STATUS>,
                        <DC_OPIS_BLEDU>Brak uslugi w systemie zrodlowym</DC_OPIS_BLEDU>)
                 else if ($reason="11") then
                        if ($urcode = "102") then (
                           <DC_TYLKO_STATUS>{$urcode}</DC_TYLKO_STATUS>,
                           <DC_OPIS_BLEDU>Bledne dane wejsciowe</DC_OPIS_BLEDU>)
                        else if ($urcode = "103") then (
                            <DC_TYLKO_STATUS>{$urcode}</DC_TYLKO_STATUS>,
                            <DC_OPIS_BLEDU>Brak danych</DC_OPIS_BLEDU>)
                        else if ($urcode = "104") then (
                            <DC_TYLKO_STATUS>{$urcode}</DC_TYLKO_STATUS>,
                            <DC_OPIS_BLEDU>Klient nie istnieje lub nie posiada produktow z adresem alternatywnym</DC_OPIS_BLEDU>)
                        else (
                            <DC_TYLKO_STATUS>{$urcode}</DC_TYLKO_STATUS>,
                            <DC_OPIS_BLEDU>Brak autoryzacji lub blad uslugi zrodlowej</DC_OPIS_BLEDU>)
                  else (
                     <DC_TYLKO_STATUS>{$urcode}</DC_TYLKO_STATUS>,
                     <DC_OPIS_BLEDU>Nieznany krytyczny blad uslugi w systemie zrodlowym</DC_OPIS_BLEDU>)
            }        
            </FML32>
	}
   
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>