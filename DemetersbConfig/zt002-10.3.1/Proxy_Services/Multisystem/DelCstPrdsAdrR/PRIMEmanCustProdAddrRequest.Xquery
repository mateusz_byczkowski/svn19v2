<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2010-11-18</con:description>
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace tem = "http://tempuri.org/";
declare namespace wcf="http://schemas.datacontract.org/2004/07/WcfServiceLibrary.Struktures";


declare function xf:map_manCustProdAddrRequest($fml as element(fml:FML32))
	as element(tem:manageCardProductAdr) {
		<tem:manageCardProductAdr>
			<tem:input>
				<wcf:trnId>{ data($fml/fml:DC_TRN_ID) }</wcf:trnId>
				<wcf:uzytkownik>{ data($fml/fml:DC_UZYTKOWNIK) }</wcf:uzytkownik>
				<wcf:oddzial>{ data($fml/fml:DC_ODDZIAL) }</wcf:oddzial>
				<wcf:typZmiany>{ data($fml/fml:DC_TYP_ZMIANY) }</wcf:typZmiany>
			 	<wcf:numerKlienta>{ data($fml/fml:DC_NUMER_KLIENTA) }</wcf:numerKlienta>
  			</tem:input>
		</tem:manageCardProductAdr>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_manCustProdAddrRequest($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>