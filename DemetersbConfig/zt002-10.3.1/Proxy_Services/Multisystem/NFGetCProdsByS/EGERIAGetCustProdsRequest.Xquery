<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeEGERIAGetCustProds($fml as element(fml:FML32))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($fml/fml:NF_CUSTOM_CUSTOMERNUMBER)
					then <fml:EG_KOD_KLIENTA>{ data($fml/fml:NF_CUSTOM_CUSTOMERNUMBER) }</fml:EG_KOD_KLIENTA>
					else  ()
                         }
                         {
				if($fml/fml:NF_CTRL_ACTIVENONACTIVE)
					then <fml:EG_AKTYWNE_NIEAKTYWNE>{ data($fml/fml:NF_CTRL_ACTIVENONACTIVE) }</fml:EG_AKTYWNE_NIEAKTYWNE>
					else  <fml:EG_AKTYWNE_NIEAKTYWNE>0</fml:EG_AKTYWNE_NIEAKTYWNE>

			 }
                         {
				if($fml/fml:NF_PAGEC_ACTIONCODE)
					then <fml:EG_KOD_STRONY>{ data($fml/fml:NF_PAGEC_ACTIONCODE) }</fml:EG_KOD_STRONY>
					else  ()
			 }
                         {
				if($fml/fml:NF_PAGEC_PAGESIZE)
					then <fml:EG_PAGEC_PAGESIZE>{ data($fml/fml:NF_PAGEC_PAGESIZE) }</fml:EG_PAGEC_PAGESIZE>
					else  ()
			 }
                         {
				if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
					then <fml:EG_KLUCZ_NAWIG>{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }</fml:EG_KLUCZ_NAWIG>
					else  ()
			 }
                         {
				if($fml/fml:NF_PAGEC_REVERSEORDER)
					then <fml:EG_SORT_ODWROTNE>{ data($fml/fml:NF_PAGEC_REVERSEORDER) }</fml:EG_SORT_ODWROTNE>
					else  ()
			 }
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapeEGERIAGetCustProds($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>