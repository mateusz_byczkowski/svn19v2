<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/cis/faults/";
declare namespace fml="";
declare namespace WBKFault="http://bzwbk.com/services/faults/";
declare namespace wsdl-jv="http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ser-root="http://jv.pl/webservice/";
declare variable $body external;


  <soap-env:Body>
	{
          let $fault := $body/soap:Fault/detail/ser-root:faultResponse/wsdl-jv:faultResponse 
          return
               
      	     if( $fault and string-length(data($fault/status/error-msg))>0)
                 then
                      <FML32>   
                       <NF_ERROR_DESCRIPTION >{
                        substring(concat(data($fault/status/error-msg),concat(':',data($fault/status/error-details))),1,512)
                      }</NF_ERROR_DESCRIPTION >
                       <NF_ERROR_CODE >{data($fault/status/@error-code)}</NF_ERROR_CODE > 
                     </FML32>
                 else 
                     <FML32>   
                       <NF_ERROR_DESCRIPTION >Błąd krytyczny usługi STREAMLINE</NF_ERROR_DESCRIPTION >
                       <NF_ERROR_CODE>0</NF_ERROR_CODE> 
                     </FML32>
               
               
          
	}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>