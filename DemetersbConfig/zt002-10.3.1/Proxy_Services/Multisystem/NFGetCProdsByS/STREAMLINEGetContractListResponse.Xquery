<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(: Change Log
v.1.1 2010-01-15 PKLI NP1972_1 Zamiana mapowania dla pola NF_ACCOUN_ACCOUNTOPENDATE (z contract-start na proposal)
v.1.2 2010-03-05 PKLI NP2037_1 CR1 Zaprzestanie wyliczania salda ubezpieczeń JV
v.1.3 2010-05-06 PKLI NP1972_3 CR2 Filtrowanie produktów Streamline na podstawie statusu aktywności 
v.1.4 2010-05-17 PKLI T47597 Brak obsługi numeru wniosku ubezpieczeń Streamline
:)

declare namespace wsdl-jv = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace cmf-holding = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";



declare function xf:calculatePageStart ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then 1
else if ($actioncode="N")
   then xs:integer(min(($records , $oldstop + 1)))
else if ($actioncode="P")
   then max((1,$oldstart - $pagesize))
else if ($actioncode="L")
   then max((1,$records  - $pagesize))
else 0
};

declare function xf:calculatePageStop ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then xs:integer(min(($pagesize,$records)))
else if ($actioncode="N")
   then xs:integer(min(($pagesize + $oldstop,$records)))
else if ($actioncode="P")
   then max((1,$oldstart - 1))
else if ($actioncode="L")
   then $records
else xs:integer(0)
};


declare function xf:calculateHasNext( $pagestart as xs:integer,$pagestop as xs:integer, 
                                                     $records as xs:integer, $actioncode as xs:string) as xs:integer{
    if ($actioncode != "P") 
     then  if ($pagestop < $records)
           then 1
           else 0
    else if ($pagestart > 1)
           then 1
           else 0 
};

declare function xf:mapProduct($prod as element(contract),$system_id as xs:string)  as element()* {
	<fml:NF_ACCOUN_ACCOUNTNUMBER  nil="true" />
        ,
        <NF_PRODUA_CODEPRODUCTAREA>4</NF_PRODUA_CODEPRODUCTAREA>
	, 
	(: if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
	  then <fml:NF_ACCOUN_ACCOUNTNUMBER  nil="true" />
	  else <fml:NF_ACCOUN_ACCOUNTNUMBER >{substring(data($prod/m:DC_NR_RACHUNKU),2)}</fml:NF_ACCOUN_ACCOUNTNUMBER>
	, 
	if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
	  then <fml:NF_DEBITC_VIRTUALCARDNBR>{data($prod/m:DC_NR_ALTERNATYWNY)}</fml:NF_DEBITC_VIRTUALCARDNBR>
	  else <fml:NF_DEBITC_VIRTUALCARDNBR nil="true" />
	,
	if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
	  then <fml:NF_DEBITC_CARDNBR>{substring(data($prod/m:DC_NR_RACHUNKU),2)}</fml:NF_DEBITC_CARDNBR>
	  else <fml:NF_DEBITC_CARDNBR nil="true" />
	,
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))>0)
      then <NF_PRODUA_CODEPRODUCTAREA>1</NF_PRODUA_CODEPRODUCTAREA>
      else <NF_PRODUA_CODEPRODUCTAREA>2</NF_PRODUA_CODEPRODUCTAREA>
	, :)

      <fml:NF_IS_COOWNER>N</fml:NF_IS_COOWNER>
	,
    (: if ($prod/m:CI_PRODUCT_ATT1 and string-length(data($prod/m:CI_PRODUCT_ATT1))>0)
      then <fml:NF_ATTRPG_FIRSTPRODUCTFEAT>{data($prod/m:CI_PRODUCT_ATT1)}</fml:NF_ATTRPG_FIRSTPRODUCTFEAT>    
      else <fml:NF_ATTRPG_FIRSTPRODUCTFEAT nil="true"/>
	,
   if ($prod/m:CI_PRODUCT_ATT2 and string-length(data($prod/m:CI_PRODUCT_ATT2))>0)
      then <fml:NF_ATTRPG_SECONDPRODUCTFEA>{data($prod/m:CI_PRODUCT_ATT2)}</fml:NF_ATTRPG_SECONDPRODUCTFEA>    
      else <fml:NF_ATTRPG_SECONDPRODUCTFEA nil="true"/>
	,			
    if ($prod/m:CI_PRODUCT_ATT3 and string-length(data($prod/m:CI_PRODUCT_ATT3))>0)
      then <fml:NF_ATTRPG_THIRDPRODUCTFEAT>{data($prod/m:CI_PRODUCT_ATT3)}</fml:NF_ATTRPG_THIRDPRODUCTFEAT>    
      else <fml:NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/>
	,
    if ($prod/m:CI_PRODUCT_ATT4 and string-length(data($prod/m:CI_PRODUCT_ATT4))>0)
      then <fml:NF_ATTRPG_FOURTHPRODUCTFEA>{data($prod/m:CI_PRODUCT_ATT4)}</fml:NF_ATTRPG_FOURTHPRODUCTFEA>    
      else <fml:NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/>
	,
    if ($prod/m:CI_PRODUCT_ATT5 and string-length(data($prod/m:CI_PRODUCT_ATT5))>0)
      then <fml:NF_ATTRPG_FIFTHPRODUCTFEAT>{data($prod/m:CI_PRODUCT_ATT5)}</fml:NF_ATTRPG_FIFTHPRODUCTFEAT>    
      else <fml:NF_ATTRPG_FIFTHPRODUCTFEAT nil="true"/>
    ,:)
     <fml:NF_ATTRPG_FIRSTPRODUCTFEAT>{data($prod/@product-type)}</fml:NF_ATTRPG_FIRSTPRODUCTFEAT>
     ,
    <fml:NF_ATTRPG_SECONDPRODUCTFEA nil="true"/>
     ,
    <fml:NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/>
     ,
    <fml:NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/>
     ,
    <fml:NF_ATTRPG_FIFTHPRODUCTFEAT>PLN</fml:NF_ATTRPG_FIFTHPRODUCTFEAT>
     ,  
    if ($prod/level)
     then <fml:NF_PRODUD_SORCEPRODUCTCODE>{concat(data($prod/@product-type), data($prod/level))}</fml:NF_PRODUD_SORCEPRODUCTCODE>
     else <fml:NF_PRODUD_SORCEPRODUCTCODE>{concat(data($prod/@product-type), "~")}</fml:NF_PRODUD_SORCEPRODUCTCODE>
     ,
    <fml:NF_ACCOUN_ALTERNATIVEADDRE>1</fml:NF_ACCOUN_ALTERNATIVEADDRE>
     , 			
    (:1.2   if ($prod/amount)
      then <fml:NF_ACCOUN_CURRENTBALANCE>{data($prod/amount)}</fml:NF_ACCOUN_CURRENTBALANCE>
      else <fml:NF_ACCOUN_CURRENTBALANCE nil="true" />
    , 1.2 :)	
     <fml:NF_ACCOUN_CURRENTBALANCE nil="true" /> (:1.2 :)

    ,	
    (:if ($prod/m:B_BLOKADA)
      then <fml:NF_HOLD_HOLDAMOUNT>{data($prod/m:B_BLOKADA)}</fml:NF_HOLD_HOLDAMOUNT>
      else <fml:NF_HOLD_HOLDAMOUNT>0</fml:NF_HOLD_HOLDAMOUNT>
    ,			
    if ($prod/m:B_DOST_SRODKI)
      then <fml:NF_ACCOUN_AVAILABLEBALANCE>{data($prod/m:B_DOST_SRODKI)}</fml:NF_ACCOUN_AVAILABLEBALANCE>
      else <fml:NF_ACCOUN_AVAILABLEBALANCE nil="true" />
    ,
    if ($prod/m:B_KOD_WALUTY)
      then <fml:NF_CURREC_CURRENCYCODE>{data($prod/m:B_KOD_WALUTY)}</fml:NF_CURREC_CURRENCYCODE>
      else <fml:NF_CURREC_CURRENCYCODE nil="true"/>
    ,			
    if ($prod/m:B_D_OTWARCIA)
      then <fml:NF_ACCOUN_ACCOUNTOPENDATE>{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_D_OTWARCIA))}</fml:NF_ACCOUN_ACCOUNTOPENDATE>
      else <fml:NF_ACCOUN_ACCOUNTOPENDATE>0001-01-01</fml:NF_ACCOUN_ACCOUNTOPENDATE>
    ,			
    if ($prod/m:B_DATA_OST_OPER)
      then <fml:NF_TRANA_DATEOFLASTACTIVIT>{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_DATA_OST_OPER))}</fml:NF_TRANA_DATEOFLASTACTIVIT>
      else <fml:NF_TRANA_DATEOFLASTACTIVIT>0001-01-01</fml:NF_TRANA_DATEOFLASTACTIVIT>
    ,		
    if ($prod/m:B_LIMIT1)
      then <fml:NF_TRAACA_LIMITAMOUNT>{data($prod/m:B_LIMIT1)}</fml:NF_TRAACA_LIMITAMOUNT>
      else <fml:NF_TRAACA_LIMITAMOUNT nil="true" />
    ,			
    if ($prod/m:CI_PRODUCT_NAME_ORYGINAL)
      then <fml:NF_ACCOUN_ACCOUNTNAME>{data($prod/m:CI_PRODUCT_NAME_ORYGINAL)}</fml:NF_ACCOUN_ACCOUNTNAME>
      else <fml:NF_ACCOUN_ACCOUNTNAME nil="true" />
    ,			
    if ($prod/m:B_KOD_RACH)
      then <fml:NF_PRIMAC_ACCOUNTCODE>{data($prod/m:B_KOD_RACH)}</fml:NF_PRIMAC_ACCOUNTCODE>
      else <fml:NF_PRIMAC_ACCOUNTCODE nil="true" />
    ,:)
    <NF_CTRL_SYSTEMID>{$system_id}</NF_CTRL_SYSTEMID>
    ,
    <NF_ACCOUR_RELATIONSHIP nil="true"/>
    ,
    if ($prod[@contract-number])
    then <NF_INSURA_ID>{data($prod/@contract-number)}</NF_INSURA_ID>
    else  <NF_INSURA_ID nil="true"/>
    ,
    <NF_DEBITC_VIRTUALCARDNBR>{concat(concat(data($prod/@product-type),":"),$prod/@contract-number)}
    </NF_DEBITC_VIRTUALCARDNBR>
    ,
(: v.1.3 start :)
    if (data($prod/status)='0' or data($prod/status)='1')
    then <NF_PRODUCT_STAT>T</NF_PRODUCT_STAT>
    else  <NF_PRODUCT_STAT>N</NF_PRODUCT_STAT>  (: statusy 2, 3, 5, 7, 8 i pozostałe :)
   ,
(: v.1.3 end :)
(: v.1.4 start :)
    if ($prod/proposal-number)
    then <NF_POLICC_PROPOSALID>{data($prod/proposal-number)}</NF_POLICC_PROPOSALID>
    else ()
    ,
(: v.1.4 end :)
   <NF_CURREC_CURRENCYCODE>PLN</NF_CURREC_CURRENCYCODE>
   ,
   if ($prod/contract-dates/proposal)
   then <NF_ACCOUN_ACCOUNTOPENDATE>{data($prod/contract-dates/proposal)}</NF_ACCOUN_ACCOUNTOPENDATE>
   else <NF_ACCOUN_ACCOUNTOPENDATE nil="true" />
};

declare function xf:mapSTREAMLINEGetContractListResponse ($res as element(wsdl-jv:getContractListResponse),$system_id as xs:string,
  $pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
								  $actioncode as xs:string )
as element(fml:FML32){
	let $records:=count( $res/contracts/contract)
	let $start:=xf:calculatePageStart($pagesize,$oldstart,$oldstop,$actioncode,$records)
	let $stop:=xf:calculatePageStop($pagesize,$oldstart,$oldstop,$actioncode,$records)
	let $newnavkey:=concat("012:",string($start),":",string($stop))
	let $hasnext:=xf:calculateHasNext($start,$stop,$records,$actioncode)
	return
	   <fml:FML32>
		  <NF_PAGEC_NAVIGATIONKEYVALU>{$newnavkey}</NF_PAGEC_NAVIGATIONKEYVALU>
		  <NF_PAGEC_HASNEXT>{$hasnext}</NF_PAGEC_HASNEXT>
		  <NF_PAGECC_OPERATIONS>{max((0,$stop - $start+1))}</NF_PAGECC_OPERATIONS>
		  {
			for $idx in ($start to $stop)
			   return
				  xf:mapProduct($res/contracts/contract[$idx], $system_id)
		  }
	   </fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $system_id as xs:string external;
declare variable $pagesize as xs:integer external;
declare variable $oldstart  as xs:integer external;
declare variable $oldstop  as xs:integer external;
declare variable $actioncode  as xs:string external;
<soap-env:Body>
{xf:mapSTREAMLINEGetContractListResponse($body/wsdl-jv:getContractListResponse,$system_id,$pagesize,$oldstart,$oldstop,$actioncode)}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>