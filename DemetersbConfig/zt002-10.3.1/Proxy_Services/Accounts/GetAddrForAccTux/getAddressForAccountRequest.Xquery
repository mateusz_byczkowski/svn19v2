<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace tem = "http://tempuri.org/";
declare namespace wcf="http://schemas.datacontract.org/2004/07/WcfServiceLibrary.Struktures";


declare function xf:map_getAddressForAccountRequest($fml as element(fml:FML32))
	as element(tem:getCardProductAdr) {
		<tem:getCardProductAdr>
			<tem:input>
				<wcf:AccounAccountiban>{ data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER) }</wcf:AccounAccountiban>
			</tem:input>
		</tem:getCardProductAdr>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_getAddressForAccountRequest($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>