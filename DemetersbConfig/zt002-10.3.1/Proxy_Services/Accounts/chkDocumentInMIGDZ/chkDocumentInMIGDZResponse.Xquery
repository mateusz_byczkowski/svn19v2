<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.2
 : @since   2010-02-22
 :
 : wersja WSDLa: 25-01-2010 15:50:42
 :
 : $Proxy Services/Accounts/chkDocumentInMIGDZ/chkDocumentInMIGDZResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/chkDocumentInMIGDZ/chkDocumentinMIGDZResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:chkDocumentinMIGDZResponse($fML321 as element(ns1:FML32))
    as element(ns2:invokeResponse)
{
	<ns2:invokeResponse>
		<ns2:response>
			<ns0:ResponseMessage>
				<ns0:result>{
					if(fn:string-length(data($fML321/ns1:CI_STATUS)) > 0) then
						"true"
					else
						"false"
				}</ns0:result>
			</ns0:ResponseMessage>
		</ns2:response>
	</ns2:invokeResponse>
};

<soap-env:Body>{
	xf:chkDocumentinMIGDZResponse($fML321)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>