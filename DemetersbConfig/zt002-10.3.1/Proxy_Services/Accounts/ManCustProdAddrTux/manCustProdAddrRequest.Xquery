<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace tem = "http://tempuri.org/";
declare namespace wcf="http://schemas.datacontract.org/2004/07/WcfServiceLibrary.Struktures";


declare function xf:map_manCustProdAddrRequest($fml as element(fml:FML32))
    as element(tem:manageCardProductAdr) {
        <tem:manageCardProductAdr>
            <tem:input>
                <wcf:trnId>{ data($fml/fml:DC_TRN_ID) }</wcf:trnId>
                <wcf:uzytkownik>{ data($fml/fml:DC_UZYTKOWNIK) }</wcf:uzytkownik>
                <wcf:oddzial>{ data($fml/fml:DC_ODDZIAL) }</wcf:oddzial>
                <wcf:typZmiany>{ data($fml/fml:DC_TYP_ZMIANY) }</wcf:typZmiany>
                {
                    if($fml/fml:DC_NR_RACHUNKU)
                    then    <wcf:nrRachunku>{ data($fml/fml:DC_NR_RACHUNKU) }</wcf:nrRachunku>
                    else   
                    (            
                        if(($fml/fml:DC_NUMER_KLIENTA) and ($fml/fml:DC_TYP_ZMIANY='D'))
                        then    <wcf:numerKlienta>{ data($fml/fml:DC_NUMER_KLIENTA) }</wcf:numerKlienta>
                        else()
                    )                   
                }
                {
                    if($fml/fml:DC_IMIE_I_NAZWISKO_ALT)
                    then    <wcf:imieINazwiskoAlt>{ data($fml/fml:DC_IMIE_I_NAZWISKO_ALT) }</wcf:imieINazwiskoAlt>
                    else()
                }
                {
                    if($fml/fml:DC_ULICA_ADRES_ALT)
                    then    <wcf:UlicaAdresAlt>{ data($fml/fml:DC_ULICA_ADRES_ALT) }</wcf:UlicaAdresAlt>
                    else()
                }
                {
                    if($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT)
                    then    <wcf:NrPosesLokaluAdresAlt>{ data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT) }</wcf:NrPosesLokaluAdresAlt>
                    else()
                }
                {
                    if($fml/fml:DC_MIASTO_ADRES_ALT)
                    then    <wcf:miastAdresAlt>{ data($fml/fml:DC_MIASTO_ADRES_ALT) }</wcf:miastAdresAlt>
                    else()
                }
                {
                    if($fml/fml:CI_KOD_KRAJU_KORESP)
                    then    <wcf:wojewodztwoKrajAdresAlt>{ data($fml/fml:CI_KOD_KRAJU_KORESP) }</wcf:wojewodztwoKrajAdresAlt>
                    else()
                }
                {
                    if($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT)
                    then    <wcf:kodPocztowyAdresAlt>{ data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT) }</wcf:kodPocztowyAdresAlt>
                    else()
                }
                {
                    if($fml/fml:DC_DATA_WPROWADZENIA)
                    then    <wcf:dataWprowadzenia>{ data($fml/fml:DC_DATA_WPROWADZENIA) }</wcf:dataWprowadzenia>
                    else()
                }
                {
                    if($fml/fml:DC_DATA_KONCOWA)
                    then    <wcf:dataKoncowa>{ data($fml/fml:DC_DATA_KONCOWA) }</wcf:dataKoncowa>
                    else()
                }
                {
                    if($fml/fml:DC_KASOWAC_PRZY_WYGASNIECIU)
                    then    <wcf:kasowacPrzyWygasnieciu>{ data($fml/fml:DC_KASOWAC_PRZY_WYGASNIECIU) }</wcf:kasowacPrzyWygasnieciu>
                    else()
                }
            </tem:input>
        </tem:manageCardProductAdr>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_manCustProdAddrRequest($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>