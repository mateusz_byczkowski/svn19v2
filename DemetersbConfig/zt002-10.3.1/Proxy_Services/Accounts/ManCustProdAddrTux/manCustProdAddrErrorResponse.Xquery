<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace tem = "http://tempuri.org/";
declare namespace wcf="http://schemas.datacontract.org/2004/07/WcfServiceLibrary.Struktures";


declare function xf:map_manCustProdAddrTuxErrorResponse($res as element(wcf:WbkFault ))
	as element(fml:FML32) {
	<fml:FML32>
	{
	  	if($res/wcf:errorCode2)
		then	<fml:NF_ERROR_CODE>{ data($res/wcf:errorCode2) }</fml:NF_ERROR_CODE>
		else()
	}	
	{
	  	if($res/wcf:errorDescription)
		then	<fml:NF_ERROR_DESCRIPTION>{ data($res/wcf:errorDescription) }</fml:NF_ERROR_DESCRIPTION>
		else()
	}	
	</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_manCustProdAddrTuxErrorResponse($body/soap-env:Fault/detail/wcf:WbkFault ) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>