<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$header1" element="ns9:header" location="postTillDifference.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns9:invoke" location="postTillDifference.wsdl" ::)
(:: pragma bea:global-element-return element="ns4:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns4 = "";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns0 = "urn:acceptance.entities.be.dcl";
declare namespace ns9 = "urn:be.services.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/postTillDifference/postTillDifference2/";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:acceptancedict.dictionaries.be.dcl";

declare function xf:postTillDifference2($header1 as element(ns9:header),
    $invoke1 as element(ns9:invoke))
    as element(ns4:FML32) {
        <ns4:FML32>
            <ns4:TR_ID_OPER?>{ data($header1/ns9:transHeader/ns9:transId) }</ns4:TR_ID_OPER>
            <ns4:TR_DATA_OPER?>
                {
                    let $transactionDate  := ($invoke1/ns9:transaction/ns7:Transaction/ns7:transactionDate)  
                    return
                        fn:replace((fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
               	         '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        )),"--","")
                }
</ns4:TR_DATA_OPER>
            <ns4:TR_ODDZ_KASY?>{ xs:short( data($invoke1/ns9:branchCode/ns2:BranchCode/ns2:branchCode) ) }</ns4:TR_ODDZ_KASY>
            <ns4:TR_KASA?>{ xs:short( data($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID) ) }</ns4:TR_KASA>
            <ns4:TR_KASJER?>{ xs:short( data($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID) ) }</ns4:TR_KASJER>
            <ns4:TR_UZYTKOWNIK?>{ fn:concat("SKP:",data($header1/ns9:msgHeader/ns9:userId)) }</ns4:TR_UZYTKOWNIK>
                        
            <ns4:TR_MSG_ID?>{ data($header1/ns9:msgHeader/ns9:msgId) }</ns4:TR_MSG_ID>
            <ns4:TR_FLAGA_AKCEPT?>T</ns4:TR_FLAGA_AKCEPT>
            {
                let $__nullable := ( data($invoke1/ns9:acceptTask/ns0:AcceptTask/ns0:acceptor) )
                return
                    if (fn:boolean($__nullable))
                    then
                        <ns4:TR_AKCEPTANT>{ fn:concat("SKP:",$__nullable )}</ns4:TR_AKCEPTANT>
                    else
                        ()
            }
            {
                for $cashTransactionBasketID in $invoke1/ns9:transaction/ns7:Transaction/ns7:cashTransactionBasketID
                return
                    <TR_ID_GR_OPER?>{ data($cashTransactionBasketID) }</TR_ID_GR_OPER>
            }
            <ns4:TR_TYP_KOM?>{ xs:short( data($invoke1/ns9:transaction/ns7:Transaction/ns7:csrMessageType/ns6:CsrMessageType/ns6:csrMessageType) ) }</ns4:TR_TYP_KOM>
            <ns4:TR_KANAL>0</ns4:TR_KANAL>
            <ns4:TR_KWOTA_NAD?>{ data($invoke1/ns9:currencyReconciliation/ns1:CurrencyReconciliation/ns1:differenceAmount) }</ns4:TR_KWOTA_NAD>
            <ns4:TR_WALUTA_NAD?>{ data($invoke1/ns9:currencyReconciliation/ns1:CurrencyReconciliation/ns1:currency/ns2:CurrencyCode/ns2:currencyCode) }</ns4:TR_WALUTA_NAD>
            <ns4:TR_KWOTA_ADR?>{ data($invoke1/ns9:currencyReconciliation/ns1:CurrencyReconciliation/ns1:differenceAmountPLN) }</ns4:TR_KWOTA_ADR>
            <ns4:TR_TYTUL?>{ data($invoke1/ns9:transaction/ns7:Transaction/ns7:title) }</ns4:TR_TYTUL>
            <ns4:TR_DATA_WALUTY?>
                {
                    let $transactionDate  := ($invoke1/ns9:transaction/ns7:Transaction/ns7:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
</ns4:TR_DATA_WALUTY>
            <ns4:TR_RACH_1?>{ data($invoke1/ns9:auxiliaryAccountList/ns7:AuxiliaryAccount[1]/ns7:accountNumber) }</ns4:TR_RACH_1>
            <ns4:TR_RACH_2?>{ data($invoke1/ns9:auxiliaryAccountList/ns7:AuxiliaryAccount[2]/ns7:accountNumber) }</ns4:TR_RACH_2>
              <ns4:TR_CZAS_OPER?>
                {
                    let $transactionTime  := ($header1/ns9:msgHeader/ns9:timestamp)
  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
				</ns4:TR_CZAS_OPER>

            <ns4:TR_AKCEPTANT_SKP?>{ data($invoke1/ns9:acceptTask/ns0:AcceptTask/ns0:acceptor) }</ns4:TR_AKCEPTANT_SKP>
            <ns4:TR_UZYTKOWNIK_SKP?>{ data($header1/ns9:msgHeader/ns9:userId) }</ns4:TR_UZYTKOWNIK_SKP>
        </ns4:FML32>
};

declare variable $header1 as element(ns9:header) external;
declare variable $invoke1 as element(ns9:invoke) external;

<soap-env:Body>{
xf:postTillDifference2($header1,
    $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>