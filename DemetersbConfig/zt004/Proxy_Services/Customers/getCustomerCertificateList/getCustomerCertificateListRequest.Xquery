<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-10</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:customercertificatedict.dictionaries.be.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:customercertificates.entities.be.dcl";
declare namespace ns8="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns6:msgHeader/ns6:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns6:msgHeader/ns6:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
,
&lt;NF_MSHEAD_UNITID?&gt;{data($parm/ns6:msgHeader/ns6:unitId)}&lt;/NF_MSHEAD_UNITID&gt;
,
&lt;NF_MSHEAD_USERID?&gt;{data($parm/ns6:msgHeader/ns6:userId)}&lt;/NF_MSHEAD_USERID&gt;
,
&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns6:msgHeader/ns6:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns6:msgHeader/ns6:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
,
&lt;NF_TRHEAD_TRANSID?&gt;{data($parm/ns6:transHeader/ns6:transId)}&lt;/NF_TRHEAD_TRANSID&gt;
};


declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{

&lt;NF_CUSTCS_CUSTOMERCERTIFIC?&gt;{data($parm/ns6:CustomerCertificateStatus/ns0:CustomerCertificateStatus/ns0:customerCertificateStatus)}&lt;/NF_CUSTCS_CUSTOMERCERTIFIC&gt;
,
&lt;NF_DATEH_VALUE?&gt;{data($parm/ns6:dateTo/ns8:DateHolder/ns8:value)}&lt;/NF_DATEH_VALUE&gt;
,
&lt;NF_PAGEC_ACTIONCODE?&gt;{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:actionCode)}&lt;/NF_PAGEC_ACTIONCODE&gt;
,
&lt;NF_PAGEC_PAGESIZE?&gt;{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:pageSize)}&lt;/NF_PAGEC_PAGESIZE&gt;
,
&lt;NF_PAGEC_REVERSEORDER?&gt;{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:reverseOrder)}&lt;/NF_PAGEC_REVERSEORDER&gt;
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?&gt;{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI&gt;
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?&gt;{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;
,
&lt;NF_CUSTOM_CUSTOMERNUMBER?&gt;{data($parm/ns6:customer/ns1:Customer/ns1:customerNumber)}&lt;/NF_CUSTOM_CUSTOMERNUMBER&gt;
,
&lt;NF_DATEH_VALUE?&gt;{data($parm/ns6:dateFrom/ns8:DateHolder/ns8:value)}&lt;/NF_DATEH_VALUE&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvoke($body/ns6:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>