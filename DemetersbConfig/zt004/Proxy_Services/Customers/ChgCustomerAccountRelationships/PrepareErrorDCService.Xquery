<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare variable $body external;

declare function local:fault($faultString as xs:string, $detail as xs:anyType) as element(soap-env:Fault) {
		&lt;soap-env:Fault&gt;
			&lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
			&lt;faultstring&gt;{ $faultString }&lt;/faultstring&gt; 
			&lt;detail&gt;{ $detail }&lt;/detail&gt;
		&lt;/soap-env:Fault&gt;
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string,
        $errorDescription as xs:string,$errorCodeFromBS as xs:string) as element(err:exceptionItem) {
   &lt;err:exceptionItem&gt;
	&lt;err:errorCode1&gt;{ $errorCode1 }&lt;/err:errorCode1&gt;
	&lt;err:errorCode2&gt;{ $errorCode2 }&lt;/err:errorCode2&gt;
        {
           if (string-length($errorCodeFromBS)&gt;0) 
               then   &lt;err:errorDescription&gt;{ concat($errorCodeFromBS," : ",$errorDescription) }&lt;/err:errorDescription&gt;
               else    &lt;err:errorDescription&gt;{ $errorDescription }&lt;/err:errorDescription&gt;
        }
   &lt;/err:exceptionItem&gt;
};

&lt;soap-env:Body&gt;
	{
		let $reason := "11"
		let $urcode := "777"
		
		let $errorDescription:=data($body/FML32/NF_ERROR_DESCRIPTION[1])
		let $errorCode:=data($body/FML32/NF_ERROR_CODE[1])
		let $errorDescriptionDCa:=data($body/FML32/DC_OPIS_BLEDU[1])
                (:zamiana na polskei znaki :)

                 let $errorDescriptionDC := translate($errorDescriptionDCa, "za§˘’Ťg‘žl†ja¦¤ZAˇŁś•G?LŹJAĄ", "zażółćgęśląjaźńZAŻÓŁĆGĘLĄJAŃ")
                 
 

		return
                       if (string-length($errorDescription)&gt;0 or string-length($errorCode)&gt;0) then
                          local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $errorCode, $errorDescription,$errorCode) })
                       else if (string-length($errorDescriptionDC)&gt;0) then
                          local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $errorCode, $errorDescriptionDC,"") })
                       else()
                         }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>