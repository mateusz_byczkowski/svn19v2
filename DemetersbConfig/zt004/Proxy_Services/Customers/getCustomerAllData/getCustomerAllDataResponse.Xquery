<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>TEET 51746. Poprawka w funkcji xf:toBoolean oraz w polu employment.Version.$2.2011-02-09</con:description>
  <con:xquery>declare namespace xf = "urn:be.services.dcl/mappings";
declare namespace m = "urn:be.services.dcl";
declare namespace m1 = "urn:cif.entities.be.dcl";
declare namespace m2 = "urn:dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/"; 
 
declare variable $body external;
 
declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))&gt;0)
    then true()
    else false()
};

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
    fn:concat (xf:convertTo4CharString(fn:year-from-date($dateIn)),"-",
               xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
               xf:convertTo2CharString(fn:day-from-date($dateIn)))
};

declare function xf:toBoolean($dateIn as xs:string) as xs:boolean {
  if ($dateIn = "T" or $dateIn = "1") 
    then true()
    else false()
};

&lt;soap-env:Body&gt;
  {
  let $fml := $body/FML32
  return

  &lt;m:invokeResponse xmlns:urn="urn:be.services.dcl"&gt;
    &lt;m:customer xmlns:urn1="urn:cif.entities.be.dcl" xmlns:urn2="urn:dictionaries.be.dcl"&gt;
      &lt;m1:Customer&gt;
        {if($fml/CI_NAZWA_PELNA and xf:isData($fml/CI_NAZWA_PELNA))
            then &lt;m1:companyName&gt;{ data($fml/CI_NAZWA_PELNA) }&lt;/m1:companyName&gt;
            else ()
        }
        {if($fml/DC_NUMER_KLIENTA and xf:isData($fml/DC_NUMER_KLIENTA))
            then &lt;m1:customerNumber&gt;{ data($fml/DC_NUMER_KLIENTA) }&lt;/m1:customerNumber&gt;
            else ()
        }
        {if($fml/DC_DATA_WPROWADZENIA and xf:isData($fml/DC_DATA_WPROWADZENIA))
            then &lt;m1:dateCustomerOpened&gt;{ xf:mapDate($fml/DC_DATA_WPROWADZENIA) }&lt;/m1:dateCustomerOpened&gt;
            else ()
        }
        {if($fml/CI_DATA_AKTUALIZACJI and xf:isData($fml/CI_DATA_AKTUALIZACJI))
            then &lt;m1:fileMaintenanceLastDate&gt;{ xf:mapDate($fml/CI_DATA_AKTUALIZACJI) }&lt;/m1:fileMaintenanceLastDate&gt;
            else ()
        }
        {if($fml/DC_REZ_NIEREZ and xf:isData($fml/DC_REZ_NIEREZ))
            then &lt;m1:resident&gt;{boolean(xs:integer(data($fml/DC_REZ_NIEREZ)))}&lt;/m1:resident&gt;
            else ()
        }
        {if($fml/DC_NIP and xf:isData($fml/DC_NIP))
            then &lt;m1:taxID&gt;{ data($fml/DC_NIP) }&lt;/m1:taxID&gt;
            else ()
        }
        {if($fml/DC_NAZWA_SKROCONA and xf:isData($fml/DC_NAZWA_SKROCONA))
            then &lt;m1:shortName&gt;{ data($fml/DC_NAZWA_SKROCONA) }&lt;/m1:shortName&gt;
            else (if($fml/CI_NAZWA_PELNA and xf:isData($fml/CI_NAZWA_PELNA))
                    then &lt;m1:shortName&gt;{ data($fml/CI_NAZWA_PELNA) }&lt;/m1:shortName&gt;
                    else ()
                  )
        }
        {if($fml/CI_DECYZJA and xf:isData($fml/CI_DECYZJA))
            then &lt;m1:crmDecision&gt;{ data($fml/CI_DECYZJA) }&lt;/m1:crmDecision&gt;
            else ()
        }
        {if($fml/CI_DATA_NIP and xf:isData($fml/CI_DATA_NIP))
            then &lt;m1:taxIdDate&gt;{ xf:mapDate($fml/CI_DATA_NIP) }&lt;/m1:taxIdDate&gt;
            else ()
        }

        &lt;m1:addressList&gt;
          {for $it at $a in $fml/CI_MIASTO
            return
              &lt;m1:Address&gt;
                {if($fml/CI_NR_POSES_LOKALU[$a] and xf:isData($fml/CI_NR_POSES_LOKALU[$a]))
                    then &lt;m1:houseFlatNumber&gt;{ data($fml/CI_NR_POSES_LOKALU[$a]) }&lt;/m1:houseFlatNumber&gt;
                    else ()
                }
                {if($fml/CI_MIASTO[$a] and xf:isData($fml/CI_MIASTO[$a]))
                    then &lt;m1:city&gt;{ data($fml/CI_MIASTO[$a]) }&lt;/m1:city&gt;
                    else ()
                }
                {if($fml/CI_WOJEWODZTWO[$a] and xf:isData($fml/CI_WOJEWODZTWO[$a]))
                    then &lt;m1:county&gt;{ data($fml/CI_WOJEWODZTWO[$a]) }&lt;/m1:county&gt;
                    else ()
                }
                {if($fml/CI_ULICA[$a] and xf:isData($fml/CI_ULICA[$a]))
                    then &lt;m1:street&gt;{ data($fml/CI_ULICA[$a]) }&lt;/m1:street&gt;
                    else ()
                }
                {if($fml/CI_KOD_POCZTOWY[$a] and xf:isData($fml/CI_KOD_POCZTOWY[$a]))
                    then &lt;m1:zipCode&gt;{ data($fml/CI_KOD_POCZTOWY[$a]) }&lt;/m1:zipCode&gt;
                    else ()
                }
                {if($fml/CI_NAZWA_KORESP[$a] and xf:isData($fml/CI_NAZWA_KORESP[$a]))
                    then &lt;m1:name&gt;{ data($fml/CI_NAZWA_KORESP[$a]) }&lt;/m1:name&gt;
                    else ()
                }
                {if($fml/CI_TYP_ADRESU[$a] and xf:isData($fml/CI_TYP_ADRESU[$a]))
                    then 
                      &lt;m1:addressType&gt;
                      &lt;m2:AddressType&gt;
                      &lt;m2:addressType&gt;{data($fml/CI_TYP_ADRESU[$a])}&lt;/m2:addressType&gt;
                      &lt;/m2:AddressType&gt;
                      &lt;/m1:addressType&gt;
                    else ()
                }
                {if($fml/CI_KOD_KRAJU[$a] and xf:isData($fml/CI_KOD_KRAJU[$a]))
                    then
                      &lt;m1:countryId&gt;
                      &lt;m2:CountryCode&gt;
                      &lt;m2:isoCountryCode&gt;{ data($fml/CI_KOD_KRAJU[$a]) }&lt;/m2:isoCountryCode&gt;
                      &lt;/m2:CountryCode&gt;
                      &lt;/m1:countryId&gt;
                    else ()
                }
              &lt;/m1:Address&gt; 
          }
        &lt;/m1:addressList&gt;

        &lt;m1:documentList&gt;
          {if($fml/DC_NR_DOWODU_REGON and $fml/DC_TYP_KLIENTA and xf:isData($fml/DC_TYP_KLIENTA) and $fml/DC_TYP_KLIENTA!='P')
            then
              for $it at $a in $fml/DC_NR_DOWODU_REGON
                return
                  &lt;m1:Document&gt;
                    {if($fml/DC_NR_DOWODU_REGON[$a] and xf:isData($fml/DC_NR_DOWODU_REGON[$a]))
                        then (&lt;m1:documentNumber&gt;{ data($fml/DC_NR_DOWODU_REGON[$a]) }&lt;/m1:documentNumber&gt;,
                          &lt;m1:documentType&gt;
                          &lt;m2:CustomerDocumentType&gt;
                          &lt;m2:customerDocumentType&gt;D&lt;/m2:customerDocumentType&gt;
                          &lt;/m2:CustomerDocumentType&gt;
                          &lt;/m1:documentType&gt;)
                        else ()
                    }
                  &lt;/m1:Document&gt; 
            else ()
          }         
        &lt;/m1:documentList&gt;

        &lt;m1:customerEmploymentInfoList&gt;
          {for $it at $a in $fml/DC_DATA_PODJECIA_PRACY
            return
              &lt;m1:CustomerEmploymentInfo&gt;
                {if($fml/CI_NAZWA_PRACODAWCY[$a] and xf:isData($fml/CI_NAZWA_PRACODAWCY[$a]))
                    then &lt;m1:employment&gt;{ data($fml/CI_NAZWA_PRACODAWCY[$a]) }&lt;/m1:employment&gt;
                    else ()
                }
                {if($fml/DC_DATA_PODJECIA_PRACY[$a] and xf:isData($fml/DC_DATA_PODJECIA_PRACY[$a]))
                    then &lt;m1:employmentStartDate&gt;{ xf:mapDate($fml/DC_DATA_PODJECIA_PRACY[$a]) }&lt;/m1:employmentStartDate&gt;
                    else ()
                }
                {if($fml/DC_KOD_EKD_PRACODAW[$a] and xf:isData($fml/DC_KOD_EKD_PRACODAW[$a]))
                    then 
                      &lt;m1:employerEKDCode&gt;
                      &lt;m2:StandardIndustryCode&gt;
                      &lt;m2:standardIndustryCode&gt;{ data($fml/DC_KOD_EKD_PRACODAW[$a]) }&lt;/m2:standardIndustryCode&gt;
                      &lt;/m2:StandardIndustryCode&gt;
                      &lt;/m1:employerEKDCode&gt;
                    else ()
                }
                {if($fml/CI_STATUS_PRACOWNIKA[$a] and xf:isData($fml/CI_STATUS_PRACOWNIKA[$a]))
                    then
                      &lt;m1:customerWorkerStatus&gt;
                      &lt;m2:CustomerWorkerStatus&gt;
                      &lt;m2:customerWorkerStatus&gt;{ data($fml/CI_STATUS_PRACOWNIKA[$a]) }&lt;/m2:customerWorkerStatus&gt;
                      &lt;/m2:CustomerWorkerStatus&gt;
                      &lt;/m1:customerWorkerStatus&gt;
                    else ()
                }
              &lt;/m1:CustomerEmploymentInfo&gt; 
          }
        &lt;/m1:customerEmploymentInfoList&gt;


        &lt;m1:customerRelationshipList&gt;
          {for $it at $a in $fml/DC_TYP_RELACJI
            return
              &lt;m1:CustomerRelationship&gt;
                {if($fml/DC_NUMER_KLIENTA_REL[$a] and xf:isData($fml/DC_NUMER_KLIENTA_REL[$a]))
                    then &lt;m1:customerID&gt;{ data($fml/DC_NUMER_KLIENTA_REL[$a]) }&lt;/m1:customerID&gt;
                    else ()
                }
                {if($fml/DC_TYP_RELACJI[$a] and xf:isData($fml/DC_TYP_RELACJI[$a]))
                    then 
                      &lt;m1:relationType&gt;
                      &lt;m2:CustomerRelations&gt;
                      &lt;m2:customerRelations&gt;{ data($fml/DC_TYP_RELACJI[$a]) }&lt;/m2:customerRelations&gt;
                      &lt;/m2:CustomerRelations&gt;
                      &lt;/m1:relationType&gt;
                    else ()
                }
              &lt;/m1:CustomerRelationship&gt; 
          }
        &lt;/m1:customerRelationshipList&gt;


        &lt;m1:customerAdditionalInfo&gt;
          &lt;m1:CustomerAdditionalInfo&gt;
            {if($fml/DC_NUMER_KLIENTA and xf:isData($fml/DC_NUMER_KLIENTA))
                then &lt;m1:externalCustomerNumber&gt;{ data($fml/DC_NUMER_KLIENTA) }&lt;/m1:externalCustomerNumber&gt;
                else ()
            }
 
(:            {if($fml/DC_KARTA_WZOROW_PODPISOW and xf:isData($fml/DC_KARTA_WZOROW_PODPISOW))
                then 
                  &lt;m1:specimenSignatureCard&gt;
                  &lt;m2:SpecimenSignatureCard&gt;
                  &lt;m2:specimenSignatureCard&gt;{ data($fml/DC_KARTA_WZOROW_PODPISOW) }&lt;/m2:specimenSignatureCard&gt;
                  &lt;/m2:SpecimenSignatureCard&gt;
                  &lt;/m1:specimenSignatureCard&gt;
                else ()
            }
:)
            &lt;m1:corporateUnit&gt;
              &lt;m2:CustomerCorporateUnit&gt;
                {if($fml/DC_JEDNOSTKA_KORPORACYJNA and xf:isData($fml/DC_JEDNOSTKA_KORPORACYJNA))
                    then &lt;m2:customerCorporateUnit&gt;{ data($fml/DC_JEDNOSTKA_KORPORACYJNA) }&lt;/m2:customerCorporateUnit&gt;
                    else ()
                } 
                {if($fml/DC_KOD_JEZYKA and xf:isData($fml/DC_KOD_JEZYKA))
                    then &lt;m2:language&gt;{ data($fml/DC_KOD_JEZYKA) }&lt;/m2:language&gt;
                    else ()
                }
              &lt;/m2:CustomerCorporateUnit&gt;
            &lt;/m1:corporateUnit&gt;

            {if($fml/DC_ZRODLO_DANYCH and xf:isData($fml/DC_ZRODLO_DANYCH))
                then 
                  &lt;m1:dataSource&gt;
                  &lt;m2:DataSource&gt;
                  &lt;m2:dataSource&gt;{ data($fml/DC_ZRODLO_DANYCH) }&lt;/m2:dataSource&gt;
                  &lt;/m2:DataSource&gt;
                  &lt;/m1:dataSource&gt;
                else ()
            }
            {if($fml/CI_ODPOWIEDZ and xf:isData($fml/CI_ODPOWIEDZ))
                then 
                  &lt;m1:answers&gt;
                  &lt;m2:Answers&gt;
                  &lt;m2:answers&gt;{ data($fml/CI_ODPOWIEDZ) }&lt;/m2:answers&gt;
                  &lt;/m2:Answers&gt;
                  &lt;/m1:answers&gt;
                else ()
            }
            {if($fml/DC_PREF_KONTAKT and xf:isData($fml/DC_PREF_KONTAKT))
                then 
                  &lt;m1:preferredContact&gt;
                  &lt;m2:CustomerPreferenceContact&gt;
                  &lt;m2:customerPreferenceContact&gt;{ data($fml/DC_PREF_KONTAKT) }&lt;/m2:customerPreferenceContact&gt;
                  &lt;/m2:CustomerPreferenceContact&gt;
                  &lt;/m1:preferredContact&gt;
                else ()
            }
          &lt;/m1:CustomerAdditionalInfo&gt;
        &lt;/m1:customerAdditionalInfo&gt;

        &lt;m1:customerFirm&gt;
          &lt;m1:CustomerFirm&gt;
            {if($fml/DC_LICZBA_ZATRUDNIONYCH and xf:isData($fml/DC_LICZBA_ZATRUDNIONYCH))
                then &lt;m1:numberOfEmployees&gt;{ data($fml/DC_LICZBA_ZATRUDNIONYCH) }&lt;/m1:numberOfEmployees&gt;
                else ()
            }
            {if($fml/CI_DATA_ROZP_DZIAL and xf:isData($fml/CI_DATA_ROZP_DZIAL))
                then &lt;m1:foundationDate&gt;{ xf:mapDate($fml/CI_DATA_ROZP_DZIAL) }&lt;/m1:foundationDate&gt;
                else ()
            }
            {if($fml/DC_DATA_BANKRUCTWA and xf:isData($fml/DC_DATA_BANKRUCTWA))
                then &lt;m1:bankruptcyDate&gt;{ xf:mapDate($fml/DC_DATA_BANKRUCTWA) }&lt;/m1:bankruptcyDate&gt;
                else ()
            }
            {if($fml/DC_NR_DOWODU_REGON and xf:isData($fml/DC_NR_DOWODU_REGON) and $fml/DC_TYP_KLIENTA and xf:isData($fml/DC_TYP_KLIENTA) and $fml/DC_TYP_KLIENTA='P')
                then &lt;m1:regon&gt;{ data($fml/DC_NR_DOWODU_REGON) }&lt;/m1:regon&gt;
                else ()
            }
            {if($fml/CI_KRS and xf:isData($fml/CI_KRS))
                then &lt;m1:krs&gt;{ data($fml/CI_KRS) }&lt;/m1:krs&gt;
                else ()
            }
            {if($fml/CI_LICZBA_PLACOWEK and xf:isData($fml/CI_LICZBA_PLACOWEK))
                then &lt;m1:agencies&gt;{ data($fml/CI_LICZBA_PLACOWEK) }&lt;/m1:agencies&gt;
                else ()
            }
            {if($fml/CI_FOP and xf:isData($fml/CI_FOP))
                then 
                  &lt;m1:legalForm&gt;
                  &lt;m2:CustomerLegalForm&gt;
                  &lt;m2:customerLegalForm&gt;{ data($fml/CI_FOP) }&lt;/m2:customerLegalForm&gt;
                  &lt;/m2:CustomerLegalForm&gt;
                  &lt;/m1:legalForm&gt;
                else ()
            }                                                          
            {if($fml/DC_EKD and xf:isData($fml/DC_EKD))
                then 
                  &lt;m1:ekd&gt;
                  &lt;m2:EkdCode&gt;
                  &lt;m2:ekdCode&gt;{ data($fml/DC_EKD) }&lt;/m2:ekdCode&gt;
                  &lt;/m2:EkdCode&gt;
                  &lt;/m1:ekd&gt;
                else ()
            }                             
          &lt;/m1:CustomerFirm&gt;
        &lt;/m1:customerFirm&gt;

        &lt;m1:customerStudiesInfo&gt;
          &lt;m1:CustomerStudiesInfo&gt;
            {if($fml/DC_PLANOW_DATA_UKON_SZK and xf:isData($fml/DC_PLANOW_DATA_UKON_SZK))
                then &lt;m1:dateOfEndSchool&gt;{ xf:mapDate($fml/DC_PLANOW_DATA_UKON_SZK) }&lt;/m1:dateOfEndSchool&gt;
                else ()
            }
            {if($fml/CI_SYSTEM_NAUCZANIA and xf:isData($fml/CI_SYSTEM_NAUCZANIA))
                then 
                  &lt;m1:studySystem&gt;
                  &lt;m2:StudySystem&gt;
                  &lt;m2:studySystem&gt;{ data($fml/CI_SYSTEM_NAUCZANIA) }&lt;/m2:studySystem&gt;
                  &lt;/m2:StudySystem&gt;
                  &lt;/m1:studySystem&gt;
                else ()
            }
            {if($fml/CI_SPECJALNOSC and xf:isData($fml/CI_SPECJALNOSC))
                then 
                  &lt;m1:specialization&gt;
                  &lt;m2:CustomerStudiesSpecial&gt;
                  &lt;m2:customerStudiesSpecial&gt;{ data($fml/CI_SPECJALNOSC) }&lt;/m2:customerStudiesSpecial&gt;
                  &lt;/m2:CustomerStudiesSpecial&gt;
                  &lt;/m1:specialization&gt;
                else ()
            } 
            {if($fml/CI_KIERUNEK_STUDIOW and xf:isData($fml/CI_KIERUNEK_STUDIOW))
                then 
                  &lt;m1:fieldOfStudies&gt;
                  &lt;m2:CustomerFieldOfStudies&gt;
                  &lt;m2:customerFieldOfStudies&gt;{ data($fml/CI_KIERUNEK_STUDIOW) }&lt;/m2:customerFieldOfStudies&gt;
                  &lt;/m2:CustomerFieldOfStudies&gt;
                  &lt;/m1:fieldOfStudies&gt;
                else ()
            }
          &lt;/m1:CustomerStudiesInfo&gt;
        &lt;/m1:customerStudiesInfo&gt;

        &lt;m1:customerPersonal&gt;
          &lt;m1:CustomerPersonal&gt;
            {if($fml/DC_NUMER_KLIENTA and xf:isData($fml/DC_NUMER_KLIENTA))
                then &lt;m1:customerNumber&gt;{ data($fml/DC_NUMER_KLIENTA) }&lt;/m1:customerNumber&gt;
                else ()
            }
            {if($fml/DC_PRACOWNIK_BANKU and xf:isData($fml/DC_PRACOWNIK_BANKU))
                then &lt;m1:workInBankStatus&gt;{ boolean(xs:integer(data($fml/DC_PRACOWNIK_BANKU))) }&lt;/m1:workInBankStatus&gt;
                else ()
            }
            {if($fml/DC_MIEJSCE_URODZENIA and xf:isData($fml/DC_MIEJSCE_URODZENIA))
                then &lt;m1:birthPlace&gt;{ data($fml/DC_MIEJSCE_URODZENIA) }&lt;/m1:birthPlace&gt;
                else ()
            }
            {if($fml/DC_IMIE_OJCA and xf:isData($fml/DC_IMIE_OJCA))
                then &lt;m1:fatherName&gt;{ data($fml/DC_IMIE_OJCA) }&lt;/m1:fatherName&gt;
                else ()
            }
            {if($fml/DC_LICZBA_OS_WE_WSP_GOSP_D and xf:isData($fml/DC_LICZBA_OS_WE_WSP_GOSP_D))
                then &lt;m1:numberOfPersonsHousehold&gt;{ data($fml/DC_LICZBA_OS_WE_WSP_GOSP_D) }&lt;/m1:numberOfPersonsHousehold&gt;
                else ()
            }
            {if($fml/DC_POSIADA_DOM_MIESZK and xf:isData($fml/DC_POSIADA_DOM_MIESZK))
                then &lt;m1:ownHouseApartment&gt;{ xf:toBoolean($fml/DC_POSIADA_DOM_MIESZK) }&lt;/m1:ownHouseApartment&gt;
                else ()
            }
            {if($fml/DC_MIEJSCE_ZATRUDNIENIA and xf:isData($fml/DC_MIEJSCE_ZATRUDNIENIA))
                then &lt;m1:workplace&gt;{ data($fml/DC_MIEJSCE_ZATRUDNIENIA) }&lt;/m1:workplace&gt;
                else ()
            }
            {if($fml/DC_LICZBA_OSOB_NA_UTRZ and xf:isData($fml/DC_LICZBA_OSOB_NA_UTRZ))
                then &lt;m1:numberOfDependents&gt;{ data($fml/DC_LICZBA_OSOB_NA_UTRZ) }&lt;/m1:numberOfDependents&gt;
                else ()
            }
            {if($fml/DC_DATA_URODZENIA and xf:isData($fml/DC_DATA_URODZENIA))
                then &lt;m1:dateOfBirth&gt;{ xf:mapDate($fml/DC_DATA_URODZENIA) }&lt;/m1:dateOfBirth&gt;
                else ()
            }
            {if($fml/DC_NAZWISKO and xf:isData($fml/DC_NAZWISKO))
                then &lt;m1:lastName&gt;{ data($fml/DC_NAZWISKO) }&lt;/m1:lastName&gt;
                else ()
            }
            {if($fml/DC_DRUGIE_IMIE and xf:isData($fml/DC_DRUGIE_IMIE))
                then &lt;m1:secondName&gt;{ data($fml/DC_DRUGIE_IMIE) }&lt;/m1:secondName&gt;
                else ()
            }
            {if($fml/DC_NR_PESEL and xf:isData($fml/DC_NR_PESEL))
                then &lt;m1:pesel&gt;{ data($fml/DC_NR_PESEL) }&lt;/m1:pesel&gt;
                else ()
            }
            {if($fml/DC_IMIE and xf:isData($fml/DC_IMIE))
                then &lt;m1:firstName&gt;{ data($fml/DC_IMIE) }&lt;/m1:firstName&gt;
                else ()
            }
            {if($fml/DC_DATA_SMIERCI and xf:isData($fml/DC_DATA_SMIERCI))
                then &lt;m1:dateOfDeath&gt;{ xf:mapDate($fml/DC_DATA_SMIERCI) }&lt;/m1:dateOfDeath&gt;
                else ()
            }
            {if($fml/CI_CHARAKTER_WYKSZTALCENIA and xf:isData($fml/CI_CHARAKTER_WYKSZTALCENIA))
                then 
                  &lt;m1:educationNature&gt;
                  &lt;m2:CustomerEducationNature&gt;
                  &lt;m2:customerEducationNature&gt;{ data($fml/CI_CHARAKTER_WYKSZTALCENIA) }&lt;/m2:customerEducationNature&gt;
                  &lt;/m2:CustomerEducationNature&gt;
                  &lt;/m1:educationNature&gt;
                else ()
            }
            {if($fml/DC_SLUZBA_WOJSKOWA and xf:isData($fml/DC_SLUZBA_WOJSKOWA))
                then 
                  &lt;m1:militaryServiceStatus&gt;
                  &lt;m2:CustomerMilitaryServiceStat&gt;
                  &lt;m2:customerMilitaryServiceStat&gt;{ data($fml/DC_SLUZBA_WOJSKOWA) }&lt;/m2:customerMilitaryServiceStat&gt;
                  &lt;/m2:CustomerMilitaryServiceStat&gt;
                  &lt;/m1:militaryServiceStatus&gt;
                else ()
            } 
            {if($fml/DC_STAN_CYWILNY and xf:isData($fml/DC_STAN_CYWILNY))
                then 
                  &lt;m1:maritalStatus&gt;
                  &lt;m2:CustomerMaritalStatus&gt;
                  &lt;m2:customerMaritalStatus&gt;{ data($fml/DC_STAN_CYWILNY) }&lt;/m2:customerMaritalStatus&gt;
                  &lt;/m2:CustomerMaritalStatus&gt;
                  &lt;/m1:maritalStatus&gt;
                else ()
            }
            {if($fml/CI_ZRODLO_DOCHODU and xf:isData($fml/CI_ZRODLO_DOCHODU))
                then 
                  &lt;m1:sourceOfIncome&gt;
                  &lt;m2:SourceOfIncome&gt;
                  &lt;m2:sourceOfIncome&gt;{ data($fml/CI_ZRODLO_DOCHODU) }&lt;/m2:sourceOfIncome&gt;
                  &lt;/m2:SourceOfIncome&gt;
                  &lt;/m1:sourceOfIncome&gt;
                else ()
            }
            {if($fml/DC_WYKSZTALCENIE and xf:isData($fml/DC_WYKSZTALCENIE))
                then 
                  &lt;m1:educationCode&gt;
                  &lt;m2:CustomerEducationCode&gt;
                  &lt;m2:customerEducationCode&gt;{ data($fml/DC_WYKSZTALCENIE) }&lt;/m2:customerEducationCode&gt;
                  &lt;/m2:CustomerEducationCode&gt;
                  &lt;/m1:educationCode&gt;
                else ()
            }
            {if($fml/DC_WARUNKI_MIESZKANIOWE and xf:isData($fml/DC_WARUNKI_MIESZKANIOWE))
                then 
                  &lt;m1:flatConditions&gt;
                  &lt;m2:FlatConditions&gt;
                  &lt;m2:flatConditions&gt;{ data($fml/DC_WARUNKI_MIESZKANIOWE) }&lt;/m2:flatConditions&gt;
                  &lt;/m2:FlatConditions&gt;
                  &lt;/m1:flatConditions&gt;
                else ()
            }
            {if($fml/DC_PLEC and xf:isData($fml/DC_PLEC))
                then 
                  &lt;m1:sexCode&gt;
                  &lt;m2:CustomerSexCode&gt;
                  &lt;m2:customerSexCode&gt;{ data($fml/DC_PLEC) }&lt;/m2:customerSexCode&gt;
                  &lt;/m2:CustomerSexCode&gt;
                  &lt;/m1:sexCode&gt;
                else ()
            }                             
          &lt;/m1:CustomerPersonal&gt;
        &lt;/m1:customerPersonal&gt;

        {if($fml/DC_NUMER_ODDZIALU and xf:isData($fml/DC_NUMER_ODDZIALU))
            then 
              &lt;m1:branchOfOwnership&gt;
              &lt;m2:BranchCode&gt;
              &lt;m2:branchCode&gt;{ data($fml/DC_NUMER_ODDZIALU) }&lt;/m2:branchCode&gt;
              &lt;/m2:BranchCode&gt;
              &lt;/m1:branchOfOwnership&gt;
            else ()
        }                             
        {if($fml/DC_TYP_KLIENTA and xf:isData($fml/DC_TYP_KLIENTA))
            then 
              &lt;m1:customerType&gt;
              &lt;m2:CustomerType&gt;
              &lt;m2:customerType&gt;{ data($fml/DC_TYP_KLIENTA) }&lt;/m2:customerType&gt;
              &lt;/m2:CustomerType&gt;
              &lt;/m1:customerType&gt;
            else ()
        }                             
        {if($fml/DC_SEGMENT_MARKETINGOWY and xf:isData($fml/DC_SEGMENT_MARKETINGOWY))
            then 
              &lt;m1:customerSegment&gt;
              &lt;m2:CustomerSegment&gt;
              &lt;m2:customerSegment&gt;{ data($fml/DC_SEGMENT_MARKETINGOWY) }&lt;/m2:customerSegment&gt;
              &lt;/m2:CustomerSegment&gt;
              &lt;/m1:customerSegment&gt;
            else ()
        }                             
        {if($fml/CI_UDOSTEP_GRUPA and xf:isData($fml/CI_UDOSTEP_GRUPA))
            then 
              &lt;m1:processingApproval&gt;
              &lt;m2:CustomerProcessingApproval&gt;
              &lt;m2:customerProcessingApproval&gt;{ data($fml/CI_UDOSTEP_GRUPA) }&lt;/m2:customerProcessingApproval&gt;
              &lt;/m2:CustomerProcessingApproval&gt;
              &lt;/m1:processingApproval&gt;
            else ()
        }                             
        {if($fml/DC_SPW and xf:isData($fml/DC_SPW))
            then 
              &lt;m1:spw&gt;
              &lt;m2:SpwCode&gt;
              &lt;m2:spwCode&gt;{ data($fml/DC_SPW) }&lt;/m2:spwCode&gt;
              &lt;/m2:SpwCode&gt;
              &lt;/m1:spw&gt;
            else ()
        }                             

        &lt;m1:customerLegalInfo&gt;
          &lt;m1:CustomerLegalInfo&gt;
            {if($fml/DC_JEDNOSTKA_KORPORACYJNA and xf:isData($fml/DC_JEDNOSTKA_KORPORACYJNA))
                then &lt;m1:corporateUnit&gt;{ data($fml/DC_JEDNOSTKA_KORPORACYJNA) }&lt;/m1:corporateUnit&gt;
                else ()
            }
            {if($fml/CI_PRZYNAL_CENTR_KORP and xf:isData($fml/CI_PRZYNAL_CENTR_KORP))
                then &lt;m1:corporateCenterInd&gt;{ data($fml/CI_PRZYNAL_CENTR_KORP) }&lt;/m1:corporateCenterInd&gt;
                else ()
            }
            {if($fml/CI_NUMER_W_REJESTRZE and xf:isData($fml/CI_NUMER_W_REJESTRZE))
                then &lt;m1:registryNo&gt;{ data($fml/CI_NUMER_W_REJESTRZE) }&lt;/m1:registryNo&gt;
                else ()
            }
            {if($fml/CI_NAZWA_ORG_REJESTR and xf:isData($fml/CI_NAZWA_ORG_REJESTR))
                then &lt;m1:registryName&gt;{ data($fml/CI_NAZWA_ORG_REJESTR) }&lt;/m1:registryName&gt;
                else ()
            }
            {if($fml/CI_MIASTO_ORG_REJESTR and xf:isData($fml/CI_MIASTO_ORG_REJESTR))
                then &lt;m1:registryCity&gt;{ data($fml/CI_MIASTO_ORG_REJESTR) }&lt;/m1:registryCity&gt;
                else ()
            }
            {if($fml/CI_KOD_POCZT_ORG_REJESTR and xf:isData($fml/CI_KOD_POCZT_ORG_REJESTR))
                then &lt;m1:registryZip&gt;{ data($fml/CI_KOD_POCZT_ORG_REJESTR) }&lt;/m1:registryZip&gt;
                else ()
            }
            {if($fml/CI_ULICA_ORG_REJESTR and xf:isData($fml/CI_ULICA_ORG_REJESTR))
                then &lt;m1:registryStreet&gt;{ data($fml/CI_ULICA_ORG_REJESTR) }&lt;/m1:registryStreet&gt;
                else ()
            }
            {if($fml/CI_NR_POSES_LOKALU_ORG_REJ and xf:isData($fml/CI_NR_POSES_LOKALU_ORG_REJ))
                then &lt;m1:registryPropertyNo&gt;{ data($fml/CI_NR_POSES_LOKALU_ORG_REJ) }&lt;/m1:registryPropertyNo&gt;
                else ()
            }
            {if($fml/CI_DATA_WPISU_DO_REJESTR and xf:isData($fml/CI_DATA_WPISU_DO_REJESTR))
                then &lt;m1:registryDate&gt;{ xf:mapDate($fml/CI_DATA_WPISU_DO_REJESTR) }&lt;/m1:registryDate&gt;
                else ()
            }
            {if($fml/CI_STATUS_SIEDZIBY and xf:isData($fml/CI_STATUS_SIEDZIBY))
                then &lt;m1:seatOwnershipStatus&gt;{ data($fml/CI_STATUS_SIEDZIBY) }&lt;/m1:seatOwnershipStatus&gt;
                else ()
            }
            {if($fml/CI_KAPITAL_ZALOZYCIELSKI and xf:isData($fml/CI_KAPITAL_ZALOZYCIELSKI))
                then &lt;m1:initialCapital&gt;{ data($fml/CI_KAPITAL_ZALOZYCIELSKI) }&lt;/m1:initialCapital&gt;
                else ()
            }
            {if($fml/CI_KAPITAL_ZALOZ_OPLACONY and xf:isData($fml/CI_KAPITAL_ZALOZ_OPLACONY))
                then &lt;m1:initialCapitalPaid&gt;{ data($fml/CI_KAPITAL_ZALOZ_OPLACONY) }&lt;/m1:initialCapitalPaid&gt;
                else ()
            }
            {if($fml/CI_WARTOSC_AKCJI and xf:isData($fml/CI_WARTOSC_AKCJI))
                then &lt;m1:stocksValue&gt;{ data($fml/CI_WARTOSC_AKCJI) }&lt;/m1:stocksValue&gt;
                else ()
            }
            {if($fml/CI_KRAJ_ORG_REJESTR and xf:isData($fml/CI_KRAJ_ORG_REJESTR))
                then
                  &lt;m1:registryCountry&gt;
                  &lt;m2:CountryCode&gt;
                  &lt;m2:countryCode&gt;{ data($fml/CI_KRAJ_ORG_REJESTR) }&lt;/m2:countryCode&gt;
                  &lt;/m2:CountryCode&gt;
                  &lt;/m1:registryCountry&gt;
                else ()
            }
          &lt;/m1:CustomerLegalInfo&gt;
        &lt;/m1:customerLegalInfo&gt;
      &lt;/m1:Customer&gt;
    &lt;/m:customer&gt;
  &lt;/m:invokeResponse&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>