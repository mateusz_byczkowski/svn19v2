<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-09</con:description>
  <con:xquery>declare namespace xf = "urn:be.services.dcl/mappings";
declare namespace m = "urn:be.services.dcl";
declare namespace m1 = "urn:cif.entities.be.dcl";
declare namespace m2 = "urn:dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/"; 

declare variable $body external;
declare variable $header external;

declare function xf:convStringToInt($value as xs:string) as xs:integer {
   let $wart := $value cast as xs:integer

   return 
      $wart
};


&lt;soap-env:Body&gt;
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:customer/m1:Customer
    
    return
    &lt;FML32&gt;
      {if(string-length(data($req/m1:companyID/m2:CompanyType/m2:companyType)) &gt; 0)
          then &lt;CI_ID_SPOLKI&gt;{data($req/m1:companyID/m2:CompanyType/m2:companyType)}&lt;/CI_ID_SPOLKI&gt;
          else
              if($reqh/m:msgHeader/m:companyId)
                  then &lt;CI_ID_SPOLKI&gt;{data($reqh/m:msgHeader/m:companyId)}&lt;/CI_ID_SPOLKI&gt;
                  else () 
      }
      {if($req/m1:customerNumber)
          then &lt;DC_NUMER_KLIENTA&gt;{data($req/m1:customerNumber)}&lt;/DC_NUMER_KLIENTA&gt;
          else () 
      }
    &lt;/FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>