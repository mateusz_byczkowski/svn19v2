<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.3  2010-03-03  PKLI  TEET 45401
v.1.2  2009-11-04  LKAB  PT 58
v.1.1  2009-07-10  PKLI  TEET 39294Version.$1.2011-05-10</con:description>
  <con:xquery>(:Change log :)
(: v.1.1  2009-07-10  PKLI  TEET 39294: zmiana mapowania pola accountType :)
(: v.1.2  2009-11-04  LKAB  PT 58 :)
(: v.1.3  2010-03-03  PKLI  TEET 45401 zmiana mapowania pola Account.codeProductSystem :)


declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:entities.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:be.services.dcl";
declare namespace ns9="urn:baseauxentities.be.dcl";
declare namespace ns10="urn:insurance.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function sourceValue2Boolean($parm as xs:string*,$trueval as xs:string) as xs:string* {
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function insertDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)&gt;5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };

declare function chgDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {

    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)

    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 


};

declare function getElementsForInsurancePackagesList($parm as element(fml:FML32)) as element()
{
&lt;ns8:insurancePackagesList&gt;
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
       if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "4") and (data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ]) = "91"))
     then
    &lt;ns10:InsurancePackageAcc&gt;
      &lt;ns10:number?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns10:number&gt;
	  {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns10:startDate")}
      &lt;ns10:insuranceDescription?&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns10:insuranceDescription&gt;
      &lt;ns10:accountRelationshipList&gt;
          &lt;ns3:AccountRelationship&gt;
            &lt;ns3:relationship&gt;
              &lt;ns5:CustomerAccountRelationship&gt;
                &lt;ns5:customerAccountRelationship?&gt;{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship&gt;
              &lt;/ns5:CustomerAccountRelationship&gt;
            &lt;/ns3:relationship&gt;
          &lt;/ns3:AccountRelationship&gt;
      &lt;/ns10:accountRelationshipList&gt;
      &lt;ns10:productDefinition&gt;
        &lt;ns1:ProductDefinition&gt;
          &lt;ns1:idProductDefinition?&gt;{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns1:idProductDefinition&gt;
          &lt;ns1:productGroup&gt;
            &lt;ns1:ProductGroup&gt;
              &lt;ns1:idProductGroup?&gt;{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup&gt;
            &lt;/ns1:ProductGroup&gt;
          &lt;/ns1:productGroup&gt;
        &lt;/ns1:ProductDefinition&gt;
      &lt;/ns10:productDefinition&gt;
    &lt;/ns10:InsurancePackageAcc&gt;
   else()
  }
&lt;/ns8:insurancePackagesList&gt;
};

declare function getElementsForInsurancePoliciesList($parm as element(fml:FML32)) as element()
{

&lt;ns8:insurancePoliciesList&gt;
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER 
    return
     if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) = "4") and (data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ]) = "3"))
     then
    &lt;ns10:InsurancePolicyAcc&gt;
      &lt;ns10:policyRefNum?&gt;{data($parm/NF_INSURA_ID[$occ])}&lt;/ns10:policyRefNum&gt;
      {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns10:startDate")}
      &lt;ns10:insuranceDescription?&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns10:insuranceDescription&gt;
	  &lt;ns10:accountRelationshipList&gt;
          &lt;ns3:AccountRelationship&gt;
            &lt;ns3:relationship&gt;
              &lt;ns5:CustomerAccountRelationship&gt;
                &lt;ns5:customerAccountRelationship?&gt;{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship&gt;
              &lt;/ns5:CustomerAccountRelationship&gt;
            &lt;/ns3:relationship&gt;
          &lt;/ns3:AccountRelationship&gt;
      &lt;/ns10:accountRelationshipList&gt;
      &lt;ns10:productDefinition&gt;
        &lt;ns1:ProductDefinition&gt;
          &lt;ns1:idProductDefinition?&gt;{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns1:idProductDefinition&gt;
          &lt;ns1:productGroup&gt;
            &lt;ns1:ProductGroup&gt;
              &lt;ns1:idProductGroup?&gt;{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup&gt;
            &lt;/ns1:ProductGroup&gt;
          &lt;/ns1:productGroup&gt;
        &lt;/ns1:ProductDefinition&gt;
      &lt;/ns10:productDefinition&gt;
    &lt;/ns10:InsurancePolicyAcc&gt;
   else()
  }
&lt;/ns8:insurancePoliciesList&gt;
};

declare function getElementsForAccountListOut($parm as element(fml:FML32)) as element()
{

&lt;ns8:accountListOut&gt;
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
        if ((data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="2") or 
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="3") or 
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="10") or
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="13") or
           (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ]) ="14")) then
    &lt;ns0:Account&gt;
      &lt;ns0:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns0:accountNumber&gt;
      &lt;ns0:customerNumber?&gt;{data($parm/NF_CUSTOM_CUSTOMERNUMBER[$occ])}&lt;/ns0:customerNumber&gt;
      {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns0:accountOpenDate")}
      &lt;ns0:accountName?&gt;{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}&lt;/ns0:accountName&gt;
      &lt;ns0:currentBalance?&gt;{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns0:currentBalance&gt;
      &lt;ns0:debitPercentage?&gt;{data($parm/NF_ACCOUN_DEBITPERCENTAGE[$occ])}&lt;/ns0:debitPercentage&gt;
      &lt;ns0:statementForAccount?&gt;{sourceValue2Boolean(data($parm/NF_ACCOUN_STATEMENTFORACCO[$occ]),"1")}&lt;/ns0:statementForAccount&gt;
      &lt;ns0:accountDescription?&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns0:accountDescription&gt;
 (:1.3  &lt;ns0:codeProductSystem?&gt;{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}&lt;/ns0:codeProductSystem&gt; :)
(:1.3:) &lt;ns0:codeProductSystem?&gt;{data($parm/NF_APPLIN_APPLICATIONNUMBE[$occ])}&lt;/ns0:codeProductSystem&gt; 
      &lt;ns0:productCode?&gt;{data($parm/NF_ACCOUN_PRODUCTCODE[$occ])}&lt;/ns0:productCode&gt;
      &lt;ns0:availableBalance?&gt;{data($parm/NF_ACCOUN_AVAILABLEBALANCE[$occ])}&lt;/ns0:availableBalance&gt;
      &lt;ns0:currentBalancePLN?&gt;{data($parm/NF_ACCOUN_CURRENTBALANCEPL[$occ])}&lt;/ns0:currentBalancePLN&gt;
      &lt;ns0:accountRelationshipList&gt;
          &lt;ns3:AccountRelationship&gt;
            &lt;ns3:relationship&gt;
              &lt;ns5:CustomerAccountRelationship&gt;
                &lt;ns5:customerAccountRelationship?&gt;{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship&gt;
              &lt;/ns5:CustomerAccountRelationship&gt;
            &lt;/ns3:relationship&gt;
          &lt;/ns3:AccountRelationship&gt;
      &lt;/ns0:accountRelationshipList&gt;
       {
         if (data($parm/NF_PRODUA_CODEPRODUCTAREA) = "03") then
           &lt;ns0:loanAccount&gt;
             &lt;ns0:LoanAccount&gt;
               &lt;ns0:dateOfLastTrans?&gt;{data($parm/NF_LOANA_DATEOFLASTTRANS[$occ])}&lt;/ns0:dateOfLastTrans&gt;
             &lt;/ns0:LoanAccount&gt;
           &lt;/ns0:loanAccount&gt;
         else ()
      }
      {
          if (data($parm/NF_PRODUA_CODEPRODUCTAREA) = "10") then
     &lt;ns0:timeAccount&gt;
        &lt;ns0:TimeAccount&gt;
          &lt;ns0:renewalFrequency?&gt;{data($parm/NF_TIMEA_RENEWALFREQUENCY[$occ])}&lt;/ns0:renewalFrequency&gt;
		  {insertDate(data($parm/NF_TIMEA_NEXTRENEWALMATURI[$occ]),"yyyy-MM-dd","ns0:nextRenewalMaturityDate")}
          &lt;ns0:interestDisposition?&gt;{sourceValue2Boolean(data($parm/NF_TIMEA_INTERESTDISPOSITI[$occ]),"1")}&lt;/ns0:interestDisposition&gt;
          &lt;ns0:renewalID&gt;
            &lt;ns5:RenewalType&gt;
              &lt;ns5:renewalType?&gt;{data($parm/NF_RENEWT_RENEWALTYPE[$occ])}&lt;/ns5:renewalType&gt;
            &lt;/ns5:RenewalType&gt;
          &lt;/ns0:renewalID&gt;
          &lt;ns0:renewalPeriod&gt;
            &lt;ns5:Period&gt;
              &lt;ns5:period?&gt;{data($parm/NF_PERIOD_PERIOD[$occ])}&lt;/ns5:period&gt;
            &lt;/ns5:Period&gt;
          &lt;/ns0:renewalPeriod&gt;
        &lt;/ns0:TimeAccount&gt;
      &lt;/ns0:timeAccount&gt;
     else()
     }

      &lt;ns0:accountType&gt;
        &lt;ns5:AccountType&gt;
         (: T39249 &lt;ns5:accountType?&gt;{data($parm/NF_ACCOUN_ACCOUNTTYPE[$occ])}&lt;/ns5:accountType&gt; :)
             &lt;ns5:accountType?&gt;{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns5:accountType&gt;  (: T39249 :)
        &lt;/ns5:AccountType&gt;
      &lt;/ns0:accountType&gt;
      &lt;ns0:currency&gt;
        &lt;ns5:CurrencyCode&gt;
          &lt;ns5:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode&gt;
        &lt;/ns5:CurrencyCode&gt;
      &lt;/ns0:currency&gt;
      &lt;ns0:accountStatus&gt;
        &lt;ns2:AccountStatus&gt;
            &lt;ns2:accountStatus?&gt;{data($parm/NF_PRODUCT_STATUSCODE[$occ])}&lt;/ns2:accountStatus&gt;
        &lt;/ns2:AccountStatus&gt;
      &lt;/ns0:accountStatus&gt;
      &lt;ns0:accountBranchNumber&gt;
        &lt;ns5:BranchCode&gt;
          &lt;ns5:branchCode?&gt;{data($parm/NF_BRANCC_BRANCHCODE[$occ])}&lt;/ns5:branchCode&gt;
        &lt;/ns5:BranchCode&gt;
      &lt;/ns0:accountBranchNumber&gt;
	  &lt;ns0:productDefinition&gt;
        &lt;ns1:ProductDefinition&gt;
          &lt;ns1:idProductDefinition?&gt;{data($parm/NF_PRODAT_IDPRODUCTDEFINIT[$occ])}&lt;/ns1:idProductDefinition&gt;
          &lt;ns1:productGroup&gt;
            &lt;ns1:ProductGroup&gt;
              &lt;ns1:idProductGroup?&gt;{data($parm/PT_ID_GROUP[$occ])}&lt;/ns1:idProductGroup&gt;
            &lt;/ns1:ProductGroup&gt;
          &lt;/ns1:productGroup&gt;
        &lt;/ns1:ProductDefinition&gt;
      &lt;/ns0:productDefinition&gt;
    &lt;/ns0:Account&gt;
   else ()
  }
&lt;/ns8:accountListOut&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns8:invokeResponse&gt;

           {getElementsForInsurancePackagesList($parm)}
{getElementsForInsurancePoliciesList($parm)}
          {getElementsForAccountListOut($parm)}
          
      


  &lt;ns8:bcd&gt;
    &lt;ns6:BusinessControlData&gt;
      &lt;ns6:pageControl&gt;
        &lt;ns9:PageControl&gt;
          &lt;ns9:hasNext?&gt;{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns9:hasNext&gt;
          &lt;ns9:navigationKeyDefinition?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns9:navigationKeyDefinition&gt;
          &lt;ns9:navigationKeyValue?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns9:navigationKeyValue&gt;
        &lt;/ns9:PageControl&gt;
      &lt;/ns6:pageControl&gt;
    &lt;/ns6:BusinessControlData&gt;
  &lt;/ns8:bcd&gt;
&lt;/ns8:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>