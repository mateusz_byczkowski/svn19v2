<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:dictionaries.be.dcl";
declare namespace urn2 = "urn:baseauxentities.be.dcl";

declare function xf:mapgetBranchResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {

		&lt;urn:invokeResponse&gt;
			&lt;urn1:branchCodeOut&gt;			
						{						  						
						  if (fn:count($fml/fml:B_MIKROODDZIAL) &gt; 1) then
						  (
						
							for $it at $p in $fml/fml:B_MIKROODDZIAL
							return
							&lt;urn1:BranchCode&gt;
									&lt;urn1:addressCity?&gt;{ data($fml/fml:B_MIASTO[$p]) }&lt;/urn1:addressCity&gt;
									&lt;urn1:addressStreet?&gt;{ data($fml/fml:B_ADRES_ODDZ[$p]) }&lt;/urn1:addressStreet&gt;
									&lt;urn1:addressZipCode?&gt;{ data($fml/fml:B_KOD_POCZT[$p]) }&lt;/urn1:addressZipCode&gt;
									&lt;urn1:branchCode?&gt;{ data($fml/fml:B_ODDZIAL[$p]) }&lt;/urn1:branchCode&gt;
									&lt;urn1:description?&gt;{ data($fml/fml:B_NAZWA_ODDZ[$p]) }&lt;/urn1:description&gt;
									&lt;urn1:phoneNumber?&gt;{ data($fml/fml:B_TELEFON[$p]) }&lt;/urn1:phoneNumber&gt;
									&lt;urn1:branchCostCenterList&gt;
 									   &lt;urn1:BranchCostCenter&gt;
 									     &lt;urn1:branchCostCenter&gt;{ data($fml/fml:B_CENTR_KOSZT[$p]) }&lt;/urn1:branchCostCenter&gt;
 									   &lt;/urn1:BranchCostCenter&gt;
									&lt;/urn1:branchCostCenterList&gt;
									&lt;urn1:branchType?&gt;{ data($fml/fml:B_MIKROODDZIAL[$p]) }&lt;/urn1:branchType&gt;
									
 							&lt;/urn1:BranchCode&gt;
 							)
 							else
 							(
    							&lt;urn1:BranchCode&gt;
									&lt;urn1:addressCity?&gt;{ data($fml/fml:B_MIASTO) }&lt;/urn1:addressCity&gt;
									&lt;urn1:addressStreet?&gt;{ data($fml/fml:B_ADRES_ODDZ) }&lt;/urn1:addressStreet&gt;
									&lt;urn1:addressZipCode?&gt;{ data($fml/fml:B_KOD_POCZT) }&lt;/urn1:addressZipCode&gt;
									&lt;urn1:branchCode?&gt;{ data($fml/fml:B_ODDZIAL) }&lt;/urn1:branchCode&gt;
									&lt;urn1:description?&gt;{ data($fml/fml:B_NAZWA_ODDZ) }&lt;/urn1:description&gt;
									&lt;urn1:phoneNumber?&gt;{ data($fml/fml:B_TELEFON) }&lt;/urn1:phoneNumber&gt;
									&lt;urn1:branchCostCenterList&gt;
                  {
 							     for $it at $p in $fml/fml:B_CENTR_KOSZT
							     return
 									   &lt;urn1:BranchCostCenter&gt;
 									     &lt;urn1:branchCostCenter&gt;{ data($fml/fml:B_CENTR_KOSZT[$p]) }&lt;/urn1:branchCostCenter&gt;
 									   &lt;/urn1:BranchCostCenter&gt;
 									}
									&lt;/urn1:branchCostCenterList&gt;
									&lt;urn1:branchType?&gt;{ data($fml/fml:B_MIKROODDZIAL) }&lt;/urn1:branchType&gt;
									
 							&lt;/urn1:BranchCode&gt;
 							)
						}			
			&lt;/urn1:branchCodeOut&gt;
		&lt;/urn:invokeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body&gt;
{ xf:mapgetBranchResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>