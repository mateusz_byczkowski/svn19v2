<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:insurance.entities.be.dcl";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse&gt;
  &lt;ns5:response&gt;
    &lt;ns0:ResponseMessage&gt;
      &lt;ns0:result?&gt;{sourceValue2Boolean(data($parm/NF_RESPOM_RESULT),"1")}&lt;/ns0:result&gt;
      &lt;ns0:errorCode?&gt;{data($parm/NF_RESPOM_ERRORCODE)}&lt;/ns0:errorCode&gt;
      &lt;ns0:errorDescription?&gt;{data($parm/NF_RESPOM_ERRORDESCRIPTION)}&lt;/ns0:errorDescription&gt;
    &lt;/ns0:ResponseMessage&gt;
  &lt;/ns5:response&gt;
  &lt;ns5:account&gt;
    &lt;ns1:Account&gt;
      &lt;ns1:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER)}&lt;/ns1:accountNumber&gt;
    &lt;/ns1:Account&gt;
  &lt;/ns5:account&gt;
&lt;/ns5:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>