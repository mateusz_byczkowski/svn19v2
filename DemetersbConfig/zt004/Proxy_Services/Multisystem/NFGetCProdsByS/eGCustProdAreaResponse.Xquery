<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Response dla systemu CEKEVersion.$1.2011-02-02</con:description>
  <con:xquery>(: Log Zmian: 
==================================
v.1.1  2010-06-08 PKLI T45137 Ustawienie statusu 'Produkt aktywny' dla wszystkich usług elektronicznych z systemu CEKE           
v.1.2 2011-02-02 PKL T52074 Błąd w obsłudze stronicowania dla PRIME, CEKE i STRL

:)

declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCEKEStatus($area as xs:string,$status as xs:string) as xs:string{
   (:   kanały elektroniczne :)
   if ($area = "7" )
     then if ($status = "1")
            then "1"
          else if ($status="0")
            then "4"
          else ""
   else ""
};
	
declare function xf:calculatePageStart ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then 1
else if ($actioncode="N")
(: 1.2  then xs:integer(min(($records  , $oldstop + 1)))  :)  
           then xs:integer(min(($records + 1, $oldstop + 1))) (:1.2:)

else if ($actioncode="P")
   then max((1,$oldstart - $pagesize)) 
else if ($actioncode="L")
(: 1.2  then max((1,$records  - $pagesize)) :)
           then max((1,$records  - $pagesize + 1)) (:1.2:)

else 0
};

declare function xf:calculatePageStop ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then xs:integer(min(($pagesize,$records)))
else if ($actioncode="N")
   then xs:integer(min(($pagesize + $oldstop,$records))) 
else if ($actioncode="P")
(: 1.2  then max((1,$oldstart - 1)) :)
           then max((0,$oldstart - 1)) (:1.2:)
else if ($actioncode="L")
   then $records
else xs:integer(0)
};


declare function xf:calculateHasNext( $pagestart as xs:integer,$pagestop as xs:integer, 
                                                     $records as xs:integer, $actioncode as xs:string) as xs:integer{
(: 1.2   if ($actioncode != "P")  :)
    if ($actioncode != "P" and $actioncode != "L")  (:1.2:)
     then  if ($pagestop &lt; $records)
           then 1
           else 0
    else if ($pagestart &gt; 1)
           then 1
           else 0 
};

declare function xf:mapProduct($bdy as element(fml:FML32),$ind as xs:integer,$area as xs:string)  as element()* {

         if ($bdy/fml:DC_NR_RACHUNKU[$ind])
             then &lt;fml:NF_ACCOUN_ACCOUNTNUMBER&gt;{data($bdy/fml:DC_NR_RACHUNKU[$ind])}&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER&gt;
             else &lt;fml:NF_ACCOUN_ACCOUNTNUMBER nill="true"/&gt;
,
         if ($bdy/fml:CI_PRODUCT_ATT1[$ind] and string-length(data($bdy/fml:CI_PRODUCT_ATT1[$ind]))&gt;0)
             then &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT&gt;{data($bdy/fml:CI_PRODUCT_ATT1[$ind])}&lt;/fml:NF_ATTRPG_FIRSTPRODUCTFEAT&gt;
             else &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT nill="true"/&gt;
,
         if ($bdy/fml:CI_PRODUCT_ATT2[$ind] and string-length(data($bdy/fml:CI_PRODUCT_ATT2[$ind]))&gt;0)
             then &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA&gt;{data($bdy/fml:CI_PRODUCT_ATT2[$ind])}&lt;/fml:NF_ATTRPG_SECONDPRODUCTFEA&gt;
             else &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA nill="true"/&gt;
,
         if ($bdy/fml:CI_PRODUCT_ATT3[$ind] and string-length(data($bdy/fml:CI_PRODUCT_ATT3[$ind]))&gt;0)
             then &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT&gt;{data($bdy/fml:CI_PRODUCT_ATT3[$ind])}&lt;/fml:NF_ATTRPG_THIRDPRODUCTFEAT&gt;
             else &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT nill="true"/&gt;
,
         if ($bdy/fml:CI_PRODUCT_ATT4[$ind] and string-length(data($bdy/fml:CI_PRODUCT_ATT4[$ind]))&gt;0)
             then &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA&gt;{data($bdy/fml:CI_PRODUCT_ATT4[$ind])}&lt;/fml:NF_ATTRPG_FOURTHPRODUCTFEA&gt;
             else &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA nill="true"/&gt;
,
         if ($bdy/fml:CI_PRODUCT_ATT5[$ind] and string-length(data($bdy/fml:CI_PRODUCT_ATT5[$ind]))&gt;0)
             then &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT&gt;{data($bdy/fml:CI_PRODUCT_ATT5[$ind])}&lt;/fml:NF_ATTRPG_FIFTHPRODUCTFEAT&gt;
             else &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT nill="true"/&gt;
,
         if ($bdy/fml:B_KOD_WALUTY[$ind] and string-length(data($bdy/fml:B_KOD_WALUTY[$ind]))&gt;0)
             then &lt;fml:NF_CURREC_CURRENCYCODE&gt;{data($bdy/fml:B_KOD_WALUTY[$ind])}&lt;/fml:NF_CURREC_CURRENCYCODE&gt;
             else &lt;fml:NF_CURREC_CURRENCYCODE nill="true"/&gt;
,
         if ($bdy/fml:CI_RACHUNEK_ADR_ALT[$ind] and string-length(data($bdy/fml:CI_RACHUNEK_ADR_ALT[$ind]))&gt;0)
             then &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;{data($bdy/fml:CI_RACHUNEK_ADR_ALT[$ind])}&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;
             else &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;0&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;
,
       (:  if ($bdy/fml:CI_RELACJA[$ind] and string-length(data($bdy/fml:CI_RELACJA[$ind]))&gt;0)
             then &lt;fml:CI_RELACJA&gt;{data($bdy/fml:CI_RELACJA[$ind])}&lt;/fml:CI_RELACJA&gt;
             else &lt;fml:CI_RELACJA nill="true"/&gt;
,:)
         if ($bdy/fml:B_SALDO[$ind] and string-length(data($bdy/fml:B_SALDO[$ind]))&gt;0)
             then &lt;fml:NF_ACCOUN_CURRENTBALANCE&gt;{data($bdy/fml:B_SALDO[$ind])}&lt;/fml:NF_ACCOUN_CURRENTBALANCE&gt;
             else &lt;fml:NF_ACCOUN_CURRENTBALANCE nill="true"/&gt;
,
         if ($bdy/fml:B_DOST_SRODKI[$ind] and string-length(data($bdy/fml:B_DOST_SRODKI[$ind]))&gt;0)
             then &lt;fml:NF_ACCOUN_AVAILABLEBALANCE&gt;{data($bdy/fml:B_DOST_SRODKI[$ind])}&lt;/fml:NF_ACCOUN_AVAILABLEBALANCE&gt;
             else &lt;fml:NF_ACCOUN_AVAILABLEBALANCE nill="true"/&gt;
,
         if ($bdy/fml:B_BLOKADA[$ind] and string-length(data($bdy/fml:B_BLOKADA[$ind]))&gt;0)
             then &lt;fml:NF_HOLD_HOLDAMOUNT&gt;{data($bdy/fml:B_BLOKADA[$ind])}&lt;/fml:NF_HOLD_HOLDAMOUNT&gt;
             else &lt;fml:NF_HOLD_HOLDAMOUNT nill="true"/&gt;
,
         if ($bdy/fml:B_D_OTWARCIA[$ind] and string-length(data($bdy/fml:B_D_OTWARCIA[$ind]))&gt;0)
             then &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;{data($bdy/fml:B_D_OTWARCIA[$ind])}&lt;/fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;
             else &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE nill="true"/&gt;
,
         if ($bdy/fml:B_DATA_OST_OPER[$ind] and string-length(data($bdy/fml:B_DATA_OST_OPER[$ind]))&gt;0)
             then &lt;fml:NF_TRANA_DATEOFLASTACTIVIT&gt;{data($bdy/fml:B_DATA_OST_OPER[$ind])}&lt;/fml:NF_TRANA_DATEOFLASTACTIVIT&gt;
             else &lt;fml:NF_TRANA_DATEOFLASTACTIVIT nill="true"/&gt;
,
         if ($bdy/fml:B_LIMIT1[$ind] and string-length(data($bdy/fml:B_LIMIT1[$ind]))&gt;0)
             then &lt;fml:NF_TRAACA_LIMITAMOUNT&gt;{data($bdy/fml:B_LIMIT1[$ind])}&lt;/fml:NF_TRAACA_LIMITAMOUNT&gt;
             else &lt;fml:NF_TRAACA_LIMITAMOUNT nill="true"/&gt;
,
         if ($bdy/fml:CI_PRODUCT_NAME_ORYGINAL[$ind] and string-length(data($bdy/fml:CI_PRODUCT_NAME_ORYGINAL[$ind]))&gt;0)
             then &lt;fml:NF_ACCOUN_ACCOUNTNAME&gt;{data($bdy/fml:CI_PRODUCT_NAME_ORYGINAL[$ind])}&lt;/fml:NF_ACCOUN_ACCOUNTNAME&gt;
             else &lt;fml:NF_ACCOUN_ACCOUNTNAME nill="true"/&gt;
,
         if ($bdy/fml:B_POJEDYNCZY[$ind] and string-length(data($bdy/fml:B_POJEDYNCZY[$ind]))&gt;0)
             then &lt;fml:NF_IS_COOWNER&gt;{data($bdy/fml:B_POJEDYNCZY[$ind])}&lt;/fml:NF_IS_COOWNER&gt;
             else &lt;fml:NF_IS_COOWNER&gt;N&lt;/fml:NF_IS_COOWNER&gt;
  (:,     
         if ($bdy/fml:CI_STATUS[$ind] and string-length(data($bdy/fml:CI_STATUS[$ind]))&gt;0)
             then &lt;fml:CI_STATUS&gt;{xf:mapCEKEStatus($area,data($bdy/fml:CI_STATUS[$ind]))}&lt;/fml:CI_STATUS&gt;
             else &lt;fml:CI_STATUS nill="true"/&gt;:)
,        
         &lt;NF_PRODUA_CODEPRODUCTAREA&gt;8&lt;/NF_PRODUA_CODEPRODUCTAREA&gt;
,        
         &lt;NF_PRODUD_SORCEPRODUCTCODE nil="true"/&gt;
,
         &lt;NF_CTRL_SYSTEMID&gt;5&lt;/NF_CTRL_SYSTEMID&gt;
(:1.1:)
, 
          &lt;NF_PRODUCT_STAT&gt;T&lt;/NF_PRODUCT_STAT&gt;
,
          &lt;NF_PRODUCT_STATUSCODE nil="true"/&gt;
(:1.1:)
};

declare function xf:mapICBSGetCProAreaResponse ($bdy as element(fml:FML32),$area as xs:string,
      $pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
                                      $actioncode as xs:string )
	as element(fml:FML32){
        let $records:=count( $bdy/fml:DC_NR_RACHUNKU)
        let $start:=xf:calculatePageStart($pagesize,$oldstart,$oldstop,$actioncode,$records)
        let $stop:=xf:calculatePageStop($pagesize,$oldstart,$oldstop,$actioncode,$records)
        let $newnavkey:=concat("012:",string($start),":",string($stop))
        let $hasnext:=xf:calculateHasNext($start,$stop,$records,$actioncode)
        return
           &lt;fml:FML32&gt;
                     (:1.2 start:)
                     {
                             if ($start &gt; $records or $stop &lt;= 0) 
                             then (
		  &lt;NF_PAGEC_NAVIGATIONKEYVALU&gt;012&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;,
		  &lt;NF_PAGEC_HASNEXT&gt;0&lt;/NF_PAGEC_HASNEXT&gt;,
		  &lt;NF_PAGECC_OPERATIONS&gt;0&lt;/NF_PAGECC_OPERATIONS&gt;
                                    )
                             else (  
              &lt;NF_PAGEC_NAVIGATIONKEYVALU&gt;{$newnavkey}&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;,
              &lt;NF_PAGEC_HASNEXT&gt;{$hasnext}&lt;/NF_PAGEC_HASNEXT&gt;,
              &lt;NF_PAGECC_OPERATIONS&gt;{max((0,$stop - $start+1))}&lt;/NF_PAGECC_OPERATIONS&gt;,
             
                for $idx in ($start to $stop)

                   return
                      xf:mapProduct($bdy,$idx,$area)
                 ) 
              }   (:1.2 koniec :)
           &lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
declare variable $area as xs:string external;
declare variable $pagesize as xs:integer external;
declare variable $oldstart  as xs:integer external;
declare variable $oldstop  as xs:integer external;
declare variable $actioncode  as xs:string external;
&lt;soap-env:Body&gt;
{xf:mapICBSGetCProAreaResponse($body/fml:FML32,$area,$pagesize,$oldstart,$oldstop,$actioncode)}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>