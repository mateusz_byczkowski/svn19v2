<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:accounts.entities.be.dcl";
declare namespace urn3 = "urn:filtersandmessages.entities.be.dcl";

declare function xf:mapeNOwnAccNewRequest($req as element(urn:invoke),$head as element(urn:header))
	as element(fml:FML32) {

		&lt;fml:FML32&gt;
			{
				&lt;fml:E_MSHEAD_MSGID&gt;{data($head/urn:msgHeader/urn:msgId)}&lt;/fml:E_MSHEAD_MSGID&gt;
			}
			{
				&lt;fml:E_LOGIN_ID?&gt;{ data($req/urn:customerCEKE/urn1:CustomerCEKE/urn1:nik) }&lt;/fml:E_LOGIN_ID&gt;
			}
			{
				&lt;fml:E_ADMIN_MICRO_BRANCH?&gt;{ data($req/urn:unitId) }&lt;/fml:E_ADMIN_MICRO_BRANCH&gt;
			}
			{
				&lt;fml:U_USER_NAME?&gt;{ data($req/urn:userId) }&lt;/fml:U_USER_NAME&gt;
			}
			{
				for $ownAccountCEKE in $req/urn:accountsList/urn1:OwnAccountCEKE return
				(
					&lt;fml:B_DL_NR_RACH?&gt;{ data($ownAccountCEKE/urn1:account/urn2:Account/urn2:accountNumber) }&lt;/fml:B_DL_NR_RACH&gt;,
					&lt;fml:E_SEQ_NO?&gt;{ data($ownAccountCEKE/urn1:seqNum) }&lt;/fml:E_SEQ_NO&gt;,
					if (data($ownAccountCEKE/urn1:flagFeeAccount) = "true") then (
						&lt;fml:E_FEE_ALLOWED?&gt;1&lt;/fml:E_FEE_ALLOWED&gt;
					)
					else if (data($ownAccountCEKE/urn1:flagFeeAccount) = "false") then (
						&lt;fml:E_FEE_ALLOWED?&gt;0&lt;/fml:E_FEE_ALLOWED&gt;
					) else (),
					&lt;fml:B_TYP_RACH?&gt;{ data($ownAccountCEKE/urn1:cekeAccountType) }&lt;/fml:B_TYP_RACH&gt;
				)
			}


		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body&gt;
{ xf:mapeNOwnAccNewRequest($body/urn:invoke,$header/urn:header) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>