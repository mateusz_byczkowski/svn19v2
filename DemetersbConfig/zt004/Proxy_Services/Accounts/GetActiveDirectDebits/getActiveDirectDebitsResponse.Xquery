<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:directdebit.entities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



declare function sourceValue2Boolean ($parm as xs:anyType,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};



declare function getElementsForDirectdebit($parm as element(fml:FML32)) as element()
{

&lt;ns0:directdebit&gt;
  {
    for $x at $occ in $parm/NF_DIRECD_DIRECTDEBITID
    return
    &lt;ns1:DirectDebit&gt;
      &lt;ns1:directDebitId&gt;{data($parm/NF_DIRECD_DIRECTDEBITID[$occ])}&lt;/ns1:directDebitId&gt;
      &lt;ns1:originatorReference&gt;{data($parm/NF_DIRECD_ORIGINATORREFERE[$occ])}&lt;/ns1:originatorReference&gt;
      &lt;ns1:debtorTaxID&gt;{data($parm/NF_DIRECD_DEBTORTAXID[$occ])}&lt;/ns1:debtorTaxID&gt;
      &lt;ns1:debtorName&gt;{data($parm/NF_DIRECD_DEBTORNAME[$occ])}&lt;/ns1:debtorName&gt;
      &lt;ns1:authorisationDate&gt;{data($parm/NF_DIRECD_AUTHORISATIONDAT[$occ])}&lt;/ns1:authorisationDate&gt;
      &lt;ns1:authorisationSetUpUserID&gt;{data($parm/NF_DIRECD_AUTHORISATIONSET[$occ])}&lt;/ns1:authorisationSetUpUserID&gt;
      &lt;ns1:authorisationCancelUserID&gt;{data($parm/NF_DIRECD_AUTHORISATIONCAN[$occ])}&lt;/ns1:authorisationCancelUserID&gt;
      &lt;ns1:directDebitStatus&gt;
        &lt;ns5:DirectDebitStatus&gt;
          &lt;ns5:directDebitStatus&gt;{data($parm/NF_DIREDS_DIRECTDEBITSTATU[$occ])}&lt;/ns5:directDebitStatus&gt;
        &lt;/ns5:DirectDebitStatus&gt;
      &lt;/ns1:directDebitStatus&gt;
    &lt;/ns1:DirectDebit&gt;
  }
&lt;/ns0:directdebit&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  {getElementsForDirectdebit($parm)}
  &lt;ns0:bcd&gt;
    &lt;ns7:BusinessControlData&gt;
      &lt;ns7:pageControl&gt;
        &lt;ns6:PageControl&gt;
          &lt;ns6:hasNext&gt;{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT), "1")}&lt;/ns6:hasNext&gt;
          &lt;ns6:navigationKeyDefinition&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns6:navigationKeyDefinition&gt;
          &lt;ns6:navigationKeyValue&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns6:navigationKeyValue&gt;
        &lt;/ns6:PageControl&gt;
      &lt;/ns7:pageControl&gt;
    &lt;/ns7:BusinessControlData&gt;
  &lt;/ns0:bcd&gt;
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>