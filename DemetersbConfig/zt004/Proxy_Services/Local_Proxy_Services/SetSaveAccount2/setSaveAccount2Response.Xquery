<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";

declare variable $body as element(soap-env:Body) external;

declare function xf:mapSetSaveAccountResponse($fml as element(fml:FML32))
	as element(dcl:invokeResponse) {
    let $rescode:=$fml/fml:DC_OPIS_BLEDU
    let $accountNumberIBAN:=$fml/fml:DC_NR_RACHUNKU[2] 
 
    return
	&lt;dcl:invokeResponse&gt;
            &lt;dcl:response&gt;
		&lt;ns3:ResponseMessage&gt;
                   {
                      if (data($rescode)) then 
		         &lt;ns3:result&gt;false&lt;/ns3:result&gt;
                      else 
		         &lt;ns3:result&gt;true&lt;/ns3:result&gt;
                    }
                &lt;/ns3:ResponseMessage&gt;
            &lt;/dcl:response&gt;       
            {
             if (not(data($rescode))) then 
	       &lt;dcl:accountOut&gt;
                  &lt;ns1:Account&gt;
                    &lt;ns1:accountNumber&gt;{data($accountNumberIBAN)}&lt;/ns1:accountNumber&gt;
                  &lt;/ns1:Account&gt;  
               &lt;/dcl:accountOut&gt;  
             else ()
            }
	&lt;/dcl:invokeResponse&gt;


};


&lt;soap-env:Body&gt;
{ xf:mapSetSaveAccountResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>