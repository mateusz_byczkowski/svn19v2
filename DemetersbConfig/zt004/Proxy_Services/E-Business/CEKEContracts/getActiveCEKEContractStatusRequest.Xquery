<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "http://bzwbk.com/services/ceke/messages/";

declare function xf:map_getActiveCEKEContractStatusRequest($req as element(urn:GetActiveCEKEContractStatusRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				&lt;fml:E_AGR_SIGNATURE?&gt;{ data($req/signature) }&lt;/fml:E_AGR_SIGNATURE&gt;,
			        &lt;fml:E_SYSTEM_ID?&gt;{ data($req/systemId) }&lt;/fml:E_SYSTEM_ID&gt;,
                                &lt;fml:E_REC_COUNT&gt;1&lt;/fml:E_REC_COUNT&gt;
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:map_getActiveCEKEContractStatusRequest($body/urn:GetActiveCEKEContractStatusRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>