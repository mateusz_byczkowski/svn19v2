<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/esb/messages/";
declare namespace xf = "http://bzwbk.com/services/esb/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetProductCategoriesResponse($fml as element(fml:FML32))
	as element(m:getProductCategoriesResponse) {
		&lt;m:getProductCategoriesResponse&gt;
			{

				let $CI_DICTIONARY_ID := $fml/fml:CI_DICTIONARY_ID
				let $CI_PRODUCT_CATEGORY_CODE := $fml/fml:CI_PRODUCT_CATEGORY_CODE
				let $CI_PRODUCT_CATEGORY_NAME_PL := $fml/fml:CI_PRODUCT_CATEGORY_NAME_PL
				let $CI_PRODUCT_CATEGORY_NAME_EN := $fml/fml:CI_PRODUCT_CATEGORY_NAME_EN
				let $CI_ORDER := $fml/fml:CI_ORDER
				let $CI_PARENT_ID := $fml/fml:CI_PARENT_ID
				for $it at $p in $fml/fml:CI_DICTIONARY_ID
				return
					&lt;m:getProductCategoriesslownik_l2&gt;
					{
						if($CI_DICTIONARY_ID[$p])
							then &lt;m:DictionaryId&gt;{ data($CI_DICTIONARY_ID[$p]) }&lt;/m:DictionaryId&gt;
						else ()
					}
					{
						if($CI_PRODUCT_CATEGORY_CODE[$p])
							then &lt;m:ProductCategoryCode&gt;{ data($CI_PRODUCT_CATEGORY_CODE[$p]) }&lt;/m:ProductCategoryCode&gt;
						else ()
					}
					{
						if($CI_PRODUCT_CATEGORY_NAME_PL[$p])
							then &lt;m:ProductCategoryNamePl&gt;{ data($CI_PRODUCT_CATEGORY_NAME_PL[$p]) }&lt;/m:ProductCategoryNamePl&gt;
						else ()
					}
					{
						if($CI_PRODUCT_CATEGORY_NAME_EN[$p])
							then &lt;m:ProductCategoryNameEn&gt;{ data($CI_PRODUCT_CATEGORY_NAME_EN[$p]) }&lt;/m:ProductCategoryNameEn&gt;
						else ()
					}
					{
						if($CI_ORDER[$p])
							then &lt;m:Order&gt;{ data($CI_ORDER[$p]) }&lt;/m:Order&gt;
						else ()
					}
					{
						if($CI_PARENT_ID[$p])
							then &lt;m:ParentId&gt;{ data($CI_PARENT_ID[$p]) }&lt;/m:ParentId&gt;
						else ()
					}
					&lt;/m:getProductCategoriesslownik_l2&gt;
			}

		&lt;/m:getProductCategoriesResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetProductCategoriesResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>