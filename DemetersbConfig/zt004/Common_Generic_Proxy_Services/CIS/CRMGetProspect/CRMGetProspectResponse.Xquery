<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetProspectResponse($fml as element(fml:FML32))
	as element(m:CRMGetProspectResponse) {
		&lt;m:CRMGetProspectResponse&gt;
			{
				if($fml/fml:CI_ID_KLIENTA)
					then &lt;m:IdKlienta&gt;{ data($fml/fml:CI_ID_KLIENTA) }&lt;/m:IdKlienta&gt;
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA_REJ)
					then &lt;m:SkpPracownikaRej&gt;{ data($fml/fml:CI_SKP_PRACOWNIKA_REJ) }&lt;/m:SkpPracownikaRej&gt;
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA)
					then &lt;m:SkpPracownika&gt;{ data($fml/fml:CI_SKP_PRACOWNIKA) }&lt;/m:SkpPracownika&gt;
					else ()
			}
			{
				if($fml/fml:CI_ID_SPOLKI)
					then &lt;m:IdSpolki&gt;{ data($fml/fml:CI_ID_SPOLKI) }&lt;/m:IdSpolki&gt;
					else ()
			}
			{
				if($fml/fml:DC_NUMER_ODDZIALU)
					then &lt;m:NumerOddzialu&gt;{ data($fml/fml:DC_NUMER_ODDZIALU) }&lt;/m:NumerOddzialu&gt;
					else ()
			}
			{
				if($fml/fml:DC_TYP_KLIENTA)
					then &lt;m:TypKlienta&gt;{ data($fml/fml:DC_TYP_KLIENTA) }&lt;/m:TypKlienta&gt;
					else ()
			}
			{
				if($fml/fml:DC_IMIE)
					then &lt;m:Imie&gt;{ data($fml/fml:DC_IMIE) }&lt;/m:Imie&gt;
					else ()
			}
			{
				if($fml/fml:DC_NAZWISKO)
					then &lt;m:Nazwisko&gt;{ data($fml/fml:DC_NAZWISKO) }&lt;/m:Nazwisko&gt;
					else ()
			}
			{
				if($fml/fml:DC_NAZWA)
					then &lt;m:Nazwa&gt;{ data($fml/fml:DC_NAZWA) }&lt;/m:Nazwa&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then &lt;m:NrPesel&gt;{ data($fml/fml:DC_NR_PESEL) }&lt;/m:NrPesel&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then &lt;m:NrDowoduRegon&gt;{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:NrDowoduRegon&gt;
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then &lt;m:Nip&gt;{ data($fml/fml:DC_NIP) }&lt;/m:Nip&gt;
					else ()
			}
			{
				if($fml/fml:DC_NUMER_PASZPORTU)
					then &lt;m:NumerPaszportu&gt;{ data($fml/fml:DC_NUMER_PASZPORTU) }&lt;/m:NumerPaszportu&gt;
					else ()
			}
			{
				if($fml/fml:CI_UDOSTEP_GRUPA)
					then &lt;m:UdostepGrupa&gt;{ data($fml/fml:CI_UDOSTEP_GRUPA) }&lt;/m:UdostepGrupa&gt;
					else ()
			}
			{
				if($fml/fml:DC_KRAJ)
					then &lt;m:Kraj&gt;{ data($fml/fml:DC_KRAJ) }&lt;/m:Kraj&gt;
					else ()
			}
			{
				if($fml/fml:DC_WOJEWODZTWO)
					then &lt;m:Wojewodztwo&gt;{ data($fml/fml:DC_WOJEWODZTWO) }&lt;/m:Wojewodztwo&gt;
					else ()
			}
			{
				if($fml/fml:DC_KOD_POCZTOWY_DANE_PODST)
					then &lt;m:KodPocztowyDanePodst&gt;{ data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST) }&lt;/m:KodPocztowyDanePodst&gt;
					else ()
			}
			{
				if($fml/fml:DC_MIASTO_DANE_PODST)
					then &lt;m:MiastoDanePodst&gt;{ data($fml/fml:DC_MIASTO_DANE_PODST) }&lt;/m:MiastoDanePodst&gt;
					else ()
			}
			{
				if($fml/fml:DC_ULICA_DANE_PODST)
					then &lt;m:UlicaDanePodst&gt;{ data($fml/fml:DC_ULICA_DANE_PODST) }&lt;/m:UlicaDanePodst&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST)
					then &lt;m:NrPosesLokaluDanePodst&gt;{ data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST) }&lt;/m:NrPosesLokaluDanePodst&gt;
					else ()
			}
			{
				if($fml/fml:CI_ADRES_DOD)
					then &lt;m:AdresDod&gt;{ data($fml/fml:CI_ADRES_DOD) }&lt;/m:AdresDod&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_TELEFONU)
					then &lt;m:NrTelefonu&gt;{ data($fml/fml:DC_NR_TELEFONU) }&lt;/m:NrTelefonu&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_TELEF_KOMORKOWEGO)
					then &lt;m:NrTelefKomorkowego&gt;{ data($fml/fml:DC_NR_TELEF_KOMORKOWEGO) }&lt;/m:NrTelefKomorkowego&gt;
					else ()
			}
			{
				if($fml/fml:DC_NUMER_FAKSU)
					then &lt;m:NumerFaksu&gt;{ data($fml/fml:DC_NUMER_FAKSU) }&lt;/m:NumerFaksu&gt;
					else ()
			}
			{
				if($fml/fml:DC_ADRES_E_MAIL)
					then &lt;m:AdresEMail&gt;{ data($fml/fml:DC_ADRES_E_MAIL) }&lt;/m:AdresEMail&gt;
					else ()
			}
			{
				if($fml/fml:CI_NOTATKI)
					then &lt;m:Notatki&gt;{ data($fml/fml:CI_NOTATKI) }&lt;/m:Notatki&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_WPROWADZENIA)
					then &lt;m:DataWprowadzenia&gt;{ data($fml/fml:CI_DATA_WPROWADZENIA) }&lt;/m:DataWprowadzenia&gt;
					else ()
			}
		&lt;/m:CRMGetProspectResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetProspectResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>