<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPorAutEmpResponse($fml as element(fml:FML32))
	as element(m:CRMGetPorAutEmpResponse) {
		&lt;m:CRMGetPorAutEmpResponse&gt;
			{

				let $CI_SKP_PRACOWNIKA_UPR := $fml/fml:CI_SKP_PRACOWNIKA_UPR
				for $it at $p in $fml/fml:CI_SKP_PRACOWNIKA_UPR
				return
					&lt;m:Pracownik&gt;
					{
						if($CI_SKP_PRACOWNIKA_UPR[$p])
							then &lt;m:SkpPracownikaUpr&gt;{ data($CI_SKP_PRACOWNIKA_UPR[$p]) }&lt;/m:SkpPracownikaUpr&gt;
						else ()
					}
					&lt;/m:Pracownik&gt;
			}

		&lt;/m:CRMGetPorAutEmpResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetPorAutEmpResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>