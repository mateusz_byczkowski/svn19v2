<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMPortChgListResponse($fml as element(fml:FML32))
	as element(m:CRMPortChgListResponse) {
		&lt;m:CRMPortChgListResponse&gt;
			{
				if($fml/fml:CI_NUMER_PACZKI_STR)
					then &lt;m:NumerPaczkiStr&gt;{ data($fml/fml:CI_NUMER_PACZKI_STR) }&lt;/m:NumerPaczkiStr&gt;
					else ()
			}
			{

				let $CI_ID_OPERACJI := $fml/fml:CI_ID_OPERACJI
				let $CI_BUDOWA_PORTFELA := $fml/fml:CI_BUDOWA_PORTFELA
				let $CI_CZAS_ZMIANY := $fml/fml:CI_CZAS_ZMIANY
				let $CIE_TYP_ZMIANY := $fml/fml:CIE_TYP_ZMIANY
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $DC_NUMER_ODDZIALU := $fml/fml:DC_NUMER_ODDZIALU
				let $CI_ID_PORTFELA_AKT := $fml/fml:CI_ID_PORTFELA_AKT
				let $CI_SKP_PRACOWNIKA_AKT := $fml/fml:CI_SKP_PRACOWNIKA_AKT
				let $CI_KLASA_OBSLUGI_AKT := $fml/fml:CI_KLASA_OBSLUGI_AKT
				let $CI_NOWY_NUMER_ODDZIALU := $fml/fml:CI_NOWY_NUMER_ODDZIALU
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $CI_POWOD_ZMIANY := $fml/fml:CI_POWOD_ZMIANY
				let $CI_SKP_PRACOWNIKA_REJ := $fml/fml:CI_SKP_PRACOWNIKA_REJ
                                let $DC_SEGMENT_MARKETINGOWY := $fml/fml:DC_SEGMENT_MARKETINGOWY
                                let $CI_SEGMENT_MARKETINGOWY_AKT := $fml/fml:CI_SEGMENT_MARKETINGOWY_AKT
				for $it at $p in $fml/fml:CI_ID_OPERACJI
				return
					&lt;m:CRMPortChgListZmiana&gt;
					{
						if($CI_ID_OPERACJI[$p])
							then &lt;m:IdOperacji&gt;{ data($CI_ID_OPERACJI[$p]) }&lt;/m:IdOperacji&gt;
						else ()
					}
					{
						if($CI_BUDOWA_PORTFELA[$p])
							then &lt;m:BudowaPortfela&gt;{ data($CI_BUDOWA_PORTFELA[$p]) }&lt;/m:BudowaPortfela&gt;
						else ()
					}
					{
						if($CI_CZAS_ZMIANY[$p])
							then &lt;m:CzasZmiany&gt;{ data($CI_CZAS_ZMIANY[$p]) }&lt;/m:CzasZmiany&gt;
						else ()
					}
					{
						if($CIE_TYP_ZMIANY[$p])
							then &lt;m:TypZmiany&gt;{ data($CIE_TYP_ZMIANY[$p]) }&lt;/m:TypZmiany&gt;
						else ()
					}
					{
						if($DC_TYP_KLIENTA[$p])
							then &lt;m:TypKlienta&gt;{ data($DC_TYP_KLIENTA[$p]) }&lt;/m:TypKlienta&gt;
						else ()
					}
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta&gt;{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta&gt;
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie&gt;{ data($DC_IMIE[$p]) }&lt;/m:Imie&gt;
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko&gt;{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko&gt;
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna&gt;{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna&gt;
						else ()
					}
					{
						if($DC_NUMER_ODDZIALU[$p])
							then &lt;m:NumerOddzialu&gt;{ data($DC_NUMER_ODDZIALU[$p]) }&lt;/m:NumerOddzialu&gt;
						else ()
					}
					{
						if($CI_ID_PORTFELA_AKT[$p])
							then &lt;m:IdPortfelaAkt&gt;{ data($CI_ID_PORTFELA_AKT[$p]) }&lt;/m:IdPortfelaAkt&gt;
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_AKT[$p])
							then &lt;m:SkpPracownikaAkt&gt;{ data($CI_SKP_PRACOWNIKA_AKT[$p]) }&lt;/m:SkpPracownikaAkt&gt;
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI_AKT[$p])
							then &lt;m:KlasaObslugiAkt&gt;{ data($CI_KLASA_OBSLUGI_AKT[$p]) }&lt;/m:KlasaObslugiAkt&gt;
						else ()
					}
					{
						if($CI_NOWY_NUMER_ODDZIALU[$p])
							then &lt;m:NowyNumerOddzialu&gt;{ data($CI_NOWY_NUMER_ODDZIALU[$p]) }&lt;/m:NowyNumerOddzialu&gt;
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then &lt;m:IdPortfela&gt;{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela&gt;
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA[$p])
							then &lt;m:SkpPracownika&gt;{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:SkpPracownika&gt;
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI[$p])
							then &lt;m:KlasaObslugi&gt;{ data($CI_KLASA_OBSLUGI[$p]) }&lt;/m:KlasaObslugi&gt;
						else ()
					}
					{
						if($CI_POWOD_ZMIANY[$p])
							then &lt;m:PowodZmiany&gt;{ data($CI_POWOD_ZMIANY[$p]) }&lt;/m:PowodZmiany&gt;
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_REJ[$p])
							then &lt;m:SkpPracownikaRej&gt;{ data($CI_SKP_PRACOWNIKA_REJ[$p]) }&lt;/m:SkpPracownikaRej&gt;
						else ()
					}
                                        {
                                                if($DC_SEGMENT_MARKETINGOWY[$p])
                                                        then &lt;m:Segment&gt;{ data($DC_SEGMENT_MARKETINGOWY[$p]) }&lt;/m:Segment&gt;
                                                else ()
                                        }
                                        {
                                                if($CI_SEGMENT_MARKETINGOWY_AKT[$p])
                                                        then &lt;m:SegmentAkt&gt;{ data($CI_SEGMENT_MARKETINGOWY_AKT[$p]) }&lt;/m:SegmentAkt&gt;
                                                else ()
                                        }
					&lt;/m:CRMPortChgListZmiana&gt;
			}

		&lt;/m:CRMPortChgListResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMPortChgListResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>