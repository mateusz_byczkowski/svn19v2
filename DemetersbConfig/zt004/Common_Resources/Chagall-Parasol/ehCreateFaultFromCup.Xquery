<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>XQuery generujące komunikat Fault na podstawie komunikatu zwrotnego z CU</con:description>
  <con:xquery>declare namespace ctx = "http://www.bea.com/wli/sb/context";
declare namespace cup = "http://jv.channel.cu.com.pl/webservice/";
declare namespace cup2 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace cl = "http://sygnity.pl/functions";

declare function cl:cup2NfeCodeConvert($cupCode as xs:string*, $codes as element(cl:codes)) as xs:string {
	let $allCodes :=
		for $code in $codes/cl:code
		where $code/@any-code = $cupCode
		return data($code/@nfe-code)
	return 
		if($allCodes[1] != '') then
			$allCodes[1]
		else if(xs:int($cupCode) &gt;= 1000) then
			'K00319'
		else
			'K00318'
};

declare variable $fault as element(soap-env:Fault) external;
declare variable $exception as xs:string external;
declare variable $codes as element(cl:codes) external;

&lt;soap-env:Fault xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"&gt;
	&lt;faultcode&gt;{ cl:cup2NfeCodeConvert(data($fault/detail/cup:faultResponse/cup2:faultResponse/status/@error-code), $codes) }&lt;/faultcode&gt;
	&lt;faultstring&gt;{$exception}: { 
		if($fault/detail/cup:faultResponse) then
		    fn:concat( data($fault/detail/cup:faultResponse/cup2:faultResponse/status/error-msg), ': ', data($fault/detail/cup:faultResponse/cup2:faultResponse/status/error-details) ) 
		else
		    data($fault/faultstring)
	}&lt;/faultstring&gt;
	{
		let $response := $fault/detail/cup:faultResponse/cup2:faultResponse/status
		return 
			&lt;detail&gt;
				&lt;e:ServiceFailException xmlns:e="urn:errors.hlbsentities.be.dcl"&gt;
					&lt;e:exceptionItem&gt;
						&lt;e:errorCode1&gt;1&lt;/e:errorCode1&gt;
						&lt;e:errorCode2?&gt;&lt;/e:errorCode2&gt;
						&lt;e:errorDescription&gt;{$exception}: { fn:concat( data($response/error-msg), ': ', data($response/error-details) ) }&lt;/e:errorDescription&gt;
			        &lt;/e:exceptionItem&gt;
				&lt;/e:ServiceFailException&gt;
			&lt;/detail&gt;
	}
&lt;/soap-env:Fault&gt;</con:xquery>
</con:xqueryEntry>