<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="http://bzwbk.com/services/BZWBK24flow/faults/";

declare variable $fault external;

declare function local:fault($faultString as xs:string, $detail as element()) as element(soap-env:Fault) {
		&lt;soap-env:Fault&gt;
			&lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
			&lt;faultstring&gt;{ $faultString }&lt;/faultstring&gt; 
			&lt;detail&gt;{ $detail }&lt;/detail&gt;
		&lt;/soap-env:Fault&gt;
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string,
        $errorDescription as xs:string,$errorCodeFromBS as xs:string) as element(err:exceptionItem) {
   &lt;err:exceptionItem&gt;
	&lt;err:errorCode1&gt;{ $errorCode1 }&lt;/err:errorCode1&gt;
	&lt;err:errorCode2&gt;{ $errorCode2 }&lt;/err:errorCode2&gt;
        {
           if (string-length($errorCodeFromBS)&gt;0) 
               then   &lt;err:errorDescription&gt;{ concat($errorCodeFromBS," : ",$errorDescription) }&lt;/err:errorDescription&gt;
               else    &lt;err:errorDescription&gt;{ $errorDescription }&lt;/err:errorDescription&gt;
        }
   &lt;/err:exceptionItem&gt;
};

&lt;soap-env:Body&gt;
	{
		let $reason := fn:substring-after(fn:substring-before(data($fault/ctx:reason), "):"), "(")
		let $urcode := fn:substring-before(fn:substring-after(fn:substring-after(data($fault/ctx:reason),":"),":"),":")
		let $timeoutDesc := "Wysołanie usługi przekroczyło dopuszczalny czas odpowiedzi"
		let $badLoginDesc := "Błąd logowania"
		let $transactionNotAllowedDesc := "Transakcja została odrzucona"
		let $transactionNotFoundDesc := "Nie znaleziono transakcji"
		let $systemDesc := "Błąd systemu CEKE"
		let $tokenBadResponseDesc := "Błądna odpowiedź tokena"
		let $shopNotFoundDesc := "Sklep nie znaleziony"
		let $cardNotFoundDesc := "Nie znaleziono karty"
		let $cardNotEnrolledDesc := "Card Not Enrolled" 
		let $wrongCardOwnerDesc := "Nieprawidłowy właściciel karty"
		let $binNot3DAllowedDesc :=  "Brak BIN-u 3D"
		let $defaultUrcodeDesc := "Błąd wywołania usługi"
		let $defaultDesc := "Krytyczny bład usługi w systemie źródłowym"
		let $alsbErrorDesc := "Wystąpił bład w usłudze proxy na ALSB"
                let $agreementNotFoundDesc := "Umowy nie znaleziono"
		
		return
                  if (data($fault/ctx:errorCode) = "BEA-380000") then
                    if($reason = "13") then
		       local:fault("err:TimeoutException", element err:TimeoutException { local:errors($reason, $urcode, $timeoutDesc,"") })
		    else  if($reason = "11") then
		      if($urcode = "9") then
		        local:fault("err:ServiceFailException", element err:ServiceFailException {local:errors($reason, $urcode,$badLoginDesc,"")})
	              else if($urcode = "13") then
			local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $transactionNotAllowedDesc,"") })
		      else if($urcode = "26") then
			local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $transactionNotFoundDesc, "") })
		      else if($urcode = "30") then
			local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $systemDesc,"") })
		      else if($urcode = "79") then
			local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $tokenBadResponseDesc,"") })
		      else if($urcode = "91") then
			local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $shopNotFoundDesc,"") })
		      else if($urcode = "140") then
			local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $cardNotFoundDesc,"") })
		      else if($urcode = "141") then
			local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $cardNotEnrolledDesc, "") })
		      else if($urcode = "142") then
			local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $wrongCardOwnerDesc, "") })
		      else if($urcode = "144") then
			local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $binNot3DAllowedDesc, "") })
                      else if($urcode = "300") then
                        local:fault("err:ServiceFailException", element err:ServiceFailException { local:errors($reason, $urcode, $agreementNotFoundDesc, "") })
                      else
                        local:fault("err:ServiceException", element err:ServiceException { local:errors($reason, $urcode,$defaultUrcodeDesc,"") })
                    else
                      local:fault("err:ServiceException", element err:ServiceException { local:errors($reason, $urcode,$defaultDesc,"") })

                 else
                    local:fault("err:SystemException", element err:SystemException { local:errors("0","0",$alsbErrorDesc ,"") })
             }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>