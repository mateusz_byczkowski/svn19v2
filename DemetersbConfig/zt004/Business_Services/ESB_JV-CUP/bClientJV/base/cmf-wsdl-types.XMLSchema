<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:schema>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!--
        Typy techniczne będące podstawą budowy webserwisów
        Version: 1.004
--&gt;
&lt;xs:schema 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:cmf-tech="http://jv.channel.cu.com.pl/cmf-technical-types" 
xmlns:cmf-biz="http://jv.channel.cu.com.pl/cmf-biz-types" 
xmlns:cmf-wsdl="http://jv.channel.cu.com.pl/cmf-wsdl-types"
targetNamespace="http://jv.channel.cu.com.pl/cmf-wsdl-types" &gt;
   &lt;xs:import namespace="http://jv.channel.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd"/&gt;
   &lt;xs:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd"/&gt;
	&lt;xs:annotation&gt;
		&lt;xs:documentation&gt;
            Typy techniczne będące podstawą budowy webserwisów

            CVS log: 
            $Revision: 1.21 $ 
            $Source: /it/krwnuk/cvs_do_migracji/SIS/ESB/CMF/base/cmf-wsdl-types.xsd,v $ 
            $Author: kadamowi $ 
            $Date: 2008-09-10 13:35:35 $
        &lt;/xs:documentation&gt;
	&lt;/xs:annotation&gt;
	&lt;xs:complexType name="CmfBodyType" abstract="true"/&gt;
	&lt;xs:simpleType name="SourceCodeType"&gt;
		&lt;xs:restriction base="cmf-tech:CmfString"&gt;
			&lt;xs:enumeration value="internet"/&gt;
			&lt;xs:enumeration value="placowka banku"/&gt;
			&lt;xs:enumeration value="placowka jv"/&gt;
		&lt;/xs:restriction&gt;
	&lt;/xs:simpleType&gt;
	&lt;xs:complexType name="EnvelopeType"&gt;
		&lt;xs:annotation&gt;
			&lt;xs:documentation&gt;
			  Dane generyczne do przekazywania informacji z requestem
			&lt;/xs:documentation&gt;
		&lt;/xs:annotation&gt;
		&lt;xs:sequence&gt;
			&lt;xs:element name="request-time" type="xs:dateTime"/&gt;
			&lt;xs:element name="request-no" type="cmf-tech:CmfString" minOccurs="0"/&gt;
			&lt;xs:element name="source-code" type="cmf-wsdl:SourceCodeType" minOccurs="0"/&gt;
			&lt;xs:element name="user-id" type="cmf-biz:UserIdType" minOccurs="0"/&gt;
			&lt;xs:element name="branch-id" type="cmf-biz:JVBranchIdType" minOccurs="0"/&gt;
			&lt;xs:element name="bar-code" type="cmf-biz:BarcodeType" minOccurs="0"/&gt;
		&lt;/xs:sequence&gt;
	&lt;/xs:complexType&gt;
	&lt;xs:simpleType name="ErrorCodeType"&gt;
		&lt;xs:restriction base="xs:int"/&gt;
	&lt;/xs:simpleType&gt;
	&lt;xs:simpleType name="SeverityType"&gt;
		&lt;xs:annotation&gt;
			&lt;xs:documentation&gt;
                Rodzaj i waga błędu. 
                Możliwe wartości: 
                info - Informacja; 
                warning - Ostrzeżenie; 
                esbSecurity - Błąd zgłaszany przez ESB związany z autentykacją i
                    autoryzacją klientów Web serwisów, np 'Brak uprawnień do
                    wywołania serwisu X'; 
                appBusiness - Wyjątek biznesowy zgłoszony przez aplikację źródłową, nie świadczy o
                    błędnej pracy aplikacji, np 'Nie znaleziono klienta o podanym ID.'; 
                appSystem - Wyjątek systemowy zgłoszony przez aplikację źródłową,świadczy o 
                    wystąpieniu błędu w aplikacji, np 'Null Pointer Exception'; 
                esbSystem - Wyjątek systemowy ESB lub warstwy transportowej;
            &lt;/xs:documentation&gt;
		&lt;/xs:annotation&gt;
		&lt;xs:restriction base="xs:string"&gt;
			&lt;xs:enumeration value="info"/&gt;
			&lt;xs:enumeration value="warning"/&gt;
			&lt;xs:enumeration value="esbSecurity"/&gt;
			&lt;xs:enumeration value="appBusiness"/&gt;
			&lt;xs:enumeration value="appSystem"/&gt;
			&lt;xs:enumeration value="esbSystem"/&gt;
		&lt;/xs:restriction&gt;
	&lt;/xs:simpleType&gt;
	&lt;xs:complexType name="StatusType"&gt;
		&lt;xs:annotation&gt;
			&lt;xs:documentation&gt;
                Obiekt dołączany do odpowiedzi w razie wystąpienia błędu
                lub ostrzeżenia.
            &lt;/xs:documentation&gt;
		&lt;/xs:annotation&gt;
		&lt;xs:sequence&gt;
			&lt;xs:element name="error-msg" type="cmf-tech:CmfString" minOccurs="0"&gt;
				&lt;xs:annotation&gt;
					&lt;xs:documentation&gt;
                        Treść komunikatu o błędzie
                    &lt;/xs:documentation&gt;
				&lt;/xs:annotation&gt;
			&lt;/xs:element&gt;
			&lt;xs:element name="error-info" type="cmf-tech:CmfString" minOccurs="0"/&gt;
			&lt;xs:element name="error-details" type="cmf-tech:CmfString" minOccurs="0"/&gt;
			&lt;xs:element name="error-type" type="cmf-tech:CmfString" minOccurs="0"/&gt;
		&lt;/xs:sequence&gt;
		&lt;xs:attribute name="error-code" type="cmf-wsdl:ErrorCodeType" use="required"/&gt;
		&lt;xs:attribute name="error-severity" type="cmf-wsdl:SeverityType" use="optional"/&gt;
		&lt;xs:attribute name="application-id" type="cmf-tech:CmfString" use="optional"&gt;
			&lt;xs:annotation&gt;
				&lt;xs:documentation&gt;
                    Id aplikacji źródłowej zgłaszającej błąd
                &lt;/xs:documentation&gt;
			&lt;/xs:annotation&gt;
		&lt;/xs:attribute&gt;
	&lt;/xs:complexType&gt;
	&lt;xs:complexType name="CmfRequestType" abstract="true"&gt;
		&lt;xs:complexContent&gt;
			&lt;xs:extension base="cmf-wsdl:CmfBodyType"&gt;
				&lt;xs:sequence&gt;
					&lt;xs:element name="envelope" type="cmf-wsdl:EnvelopeType" minOccurs="0"/&gt;
				&lt;/xs:sequence&gt;
			&lt;/xs:extension&gt;
		&lt;/xs:complexContent&gt;
	&lt;/xs:complexType&gt;
	&lt;xs:complexType name="CmfResponseType" abstract="true"&gt;
		&lt;xs:annotation&gt;
			&lt;xs:documentation&gt;
                Abstrakcyjny typ, którego implementacje definiują
                biznesową zawartość odpowiedzi komunikatów cmf
            &lt;/xs:documentation&gt;
		&lt;/xs:annotation&gt;
		&lt;xs:complexContent&gt;
			&lt;xs:extension base="cmf-wsdl:CmfBodyType"&gt;
				&lt;xs:sequence&gt;
					&lt;xs:element name="status" type="cmf-wsdl:StatusType"/&gt;
				&lt;/xs:sequence&gt;
			&lt;/xs:extension&gt;
		&lt;/xs:complexContent&gt;
	&lt;/xs:complexType&gt;
&lt;/xs:schema&gt;</con:schema>
  <con:dependencies>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd" ref="Business Services/ESB JV-CUP/bClientJV/base/cmf-technical-types"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd" ref="Business Services/ESB JV-CUP/bClientJV/base/cmf-biz-types"/>
  </con:dependencies>
  <con:targetNamespace>http://jv.channel.cu.com.pl/cmf-wsdl-types</con:targetNamespace>
</con:schemaEntry>