<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductGroupListInPackage_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace dcl = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";

declare function xf:getProductGroupListInPackage_resp($fml as element())
    as element(srv:invokeResponse) {
<srv:invokeResponse>
<srv:productGroupList>
{
for $i in 1 to count($fml/PT_ID_GROUP)
return
    <dcl:ProductGroup>
		<dcl:polishGroupName>{ data($fml/PT_POLISH_NAME[$i]) }</dcl:polishGroupName>
		<dcl:codeProductGroup>{ data($fml/PT_CODE_PRODUCT_GROUP[$i]) }</dcl:codeProductGroup>
		<dcl:englishGroupName>{ data($fml/PT_ENGLISH_NAME[$i]) }</dcl:englishGroupName>
		<dcl:sortOrder>{ data($fml/PT_SORT_ORDER[$i]) }</dcl:sortOrder>
		<dcl:visibilitySzrek>{ xs:boolean(data($fml/PT_VISIBILITY_SZREK[$i])) }</dcl:visibilitySzrek>
		<dcl:visibilityCRM>{ xs:boolean(data($fml/PT_VISIBILITY_CRM[$i])) }</dcl:visibilityCRM>
		<dcl:receiver>{ data($fml/PT_RECEIVER[$i]) }</dcl:receiver>
		<dcl:idProductCategory>{ data($fml/PT_ID_CATEGORY[$i]) }</dcl:idProductCategory>
		<dcl:userChangeSKP>{ data($fml/PT_USER_CHANGE_SKP[$i]) }</dcl:userChangeSKP>
		<dcl:dateChange>{ data($fml/PT_DATE_CHANGE[$i]) }</dcl:dateChange>
		<dcl:visibilityDCL>{ xs:boolean(data($fml/PT_VISIBILITY_DCL[$i])) }</dcl:visibilityDCL>
		<dcl:idProductGroup>{ data($fml/PT_ID_GROUP[$i]) }</dcl:idProductGroup>
		<dcl:obligatoryFlag>{ xs:boolean(data($fml/PT_OBLIGATORY[$i])) }</dcl:obligatoryFlag>
		<dcl:productCategory>
			<dcl:ProductCategory>
				<dcl:productArea>
					<dcl:ProductArea>
						<dcl:idProductArea>{ data( $fml/PT_ID_AREA [$i] ) }</dcl:idProductArea>
					</dcl:ProductArea>
				</dcl:productArea>
			</dcl:ProductCategory>
		</dcl:productCategory>
    </dcl:ProductGroup>
}
</srv:productGroupList>
</srv:invokeResponse>
};

declare variable $fml as element() external;

xf:getProductGroupListInPackage_resp($fml)]]></con:xquery>
</con:xqueryEntry>