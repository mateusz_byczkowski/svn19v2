<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductCategory_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace ns0 = "urn:productstree.entities.be.dcl";


declare function xf:getProductCategory_resp($fml as element())
    as element(srv:invokeResponse) {
        &lt;srv:invokeResponse&gt;
            &lt;srv:productCategoryEntityOut&gt;
                &lt;ns0:ProductCategory&gt;
                            &lt;ns0:codeProductCategory&gt;{ data($fml/PT_CODE_PRODUCT_CATEGORY) }&lt;/ns0:codeProductCategory&gt;
                            &lt;ns0:polishCategoryName&gt;{ data($fml/PT_POLISH_NAME) }&lt;/ns0:polishCategoryName&gt;
                            &lt;ns0:englishCategoryName&gt;{ data($fml/PT_ENGLISH_NAME) }&lt;/ns0:englishCategoryName&gt;
                            &lt;ns0:sortOrder&gt;{ data($fml/PT_SORT_ORDER) }&lt;/ns0:sortOrder&gt;
                            &lt;ns0:idProductArea&gt;{ data($fml/PT_ID_AREA) }&lt;/ns0:idProductArea&gt;
                            &lt;ns0:userChangeSKP&gt;{ data($fml/PT_USER_CHANGE_SKP) }&lt;/ns0:userChangeSKP&gt;

     {if (data( $fml/PT_DATE_CHANGE))
     then &lt;ns0:dateChange&gt;{ data($fml/PT_DATE_CHANGE) }&lt;/ns0:dateChange&gt;
     else () }
                           
                            &lt;ns0:idProductCategory&gt;{ data($fml/PT_ID_CATEGORY) }&lt;/ns0:idProductCategory&gt;
                &lt;/ns0:ProductCategory&gt;
            &lt;/srv:productCategoryEntityOut&gt;
        &lt;/srv:invokeResponse&gt;
};

declare variable $fml as element() external;

xf:getProductCategory_resp($fml)</con:xquery>
</con:xqueryEntry>