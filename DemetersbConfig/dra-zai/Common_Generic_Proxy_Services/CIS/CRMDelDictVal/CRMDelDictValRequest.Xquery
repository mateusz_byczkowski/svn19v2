<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMDelDictValRequest($req as element(m:CRMDelDictValRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdSlownika)
					then &lt;fml:CI_ID_SLOWNIKA&gt;{ data($req/m:IdSlownika) }&lt;/fml:CI_ID_SLOWNIKA&gt;
					else ()
			}
			{
				if($req/m:WartoscSlownika)
					then &lt;fml:CI_WARTOSC_SLOWNIKA&gt;{ data($req/m:WartoscSlownika) }&lt;/fml:CI_WARTOSC_SLOWNIKA&gt;
					else ()
			}
			{
				if($req/m:QueryCondition)
					then &lt;fml:CI_QUERY_CONDITION&gt;{ data($req/m:QueryCondition) }&lt;/fml:CI_QUERY_CONDITION&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMDelDictValRequest($body/m:CRMDelDictValRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>