<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetBrnCusNbrResponse($fml as element(fml:FML32))
	as element(m:CRMGetBrnCusNbrResponse) {
		&lt;m:CRMGetBrnCusNbrResponse&gt;
			{

				let $CI_LICZBA_REKORDOW := $fml/fml:CI_LICZBA_REKORDOW
				for $it at $p in $fml/fml:CI_LICZBA_REKORDOW
				return
					&lt;m:CRMGetBrnCusNbrOddzial&gt;
					{
						if($CI_LICZBA_REKORDOW[$p])
							then &lt;m:LiczbaRekordow&gt;{ data($CI_LICZBA_REKORDOW[$p]) }&lt;/m:LiczbaRekordow&gt;
						else ()
					}
					&lt;/m:CRMGetBrnCusNbrOddzial&gt;
			}

		&lt;/m:CRMGetBrnCusNbrResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetBrnCusNbrResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>