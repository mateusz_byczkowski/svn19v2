<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace xf = "http://bzwbk.com/services/cis/mapping/";
declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace fml = "";

declare function xf:mapping($buf as element(fml:FML32), $req as element(fml:FML32))
    as element(customer) {
        <customer>
            <cif>{ data($buf/DC_NUMER_KLIENTA) }</cif>
		    <period>{ data($req/CI_ZNACZNIK_OKRESU) }</period>
            <profit_acc_details>
            {
	            for $nr at $pos in $buf/CI_NUMER_RACHUNKU
	        	return
	        		<account>
	        			<param code="ACC_NO">{ data($nr) }</param>
						<param code="PAY_IN">{ data($buf/CI_WPLATY[$pos]) }</param>
						<param code="PAY_OFF">{ data($buf/CI_WYPLATY[$pos]) }</param>
						<param code="TRANSF_LOC_BRN">{ data($buf/CI_PRZEL_KRAJ_ODDZ[$pos]) }</param>
						<param code="TRANSF_FOREIGN_CHAN">{ data($buf/CI_PRZEL_KRAJ_KANALY_EL[$pos]) }</param>
						<param code="TRANSF_FOREIGN_SEND">{ data($buf/CI_PRZEK_ZAGR_WYSLANE[$pos]) }</param>
						<param code="TRANSF_FOREIGN_RECEIVE">{ data($buf/CI_PRZEK_ZAGR_OTRZYM[$pos]) }</param>
						<param code="FEE">{ data($buf/CI_ZA_PROWADZENIE_RACH[$pos]) }</param>
						<param code="SWAP_POS">{ data($buf/CI_POZYCJA_WYMIANY[$pos]) }</param>
						<param code="OTHER_INCOME">{ data($buf/CI_DOCHODY_INNE[$pos]) }</param>
						<param code="RESULT">{ data($buf/CI_WYNIK[$pos]) }</param>
	        		</account>
            }
            </profit_acc_details>
        </customer>
                
};

declare variable $buf as element(fml:FML32) external;
declare variable $req as element(fml:FML32) external;

<soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	<m:CRMGetProfCustResponse>{ fn-bea:serialize(xf:mapping($buf, $req)) }</m:CRMGetProfCustResponse>
</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>