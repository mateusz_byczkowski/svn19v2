<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:primeaccounts.entities.be.dcl";
declare namespace urn2 = "urn:accounts.entities.be.dcl";
declare namespace urn3 = "urn:cif.entities.be.dcl";
declare namespace pdk="http://bzwbk.com/services/pdk/";

declare function local:invoke($invoke as element(urn:invoke)) as element(soap-env:Body) {
   &lt;soap-env:Body&gt;
      &lt;pdk:GetArt105Data&gt;
         &lt;pdk:Cif?&gt;{ data($invoke/urn:customer[1]/urn3:Customer[1]/urn3:customerNumber[1]) }&lt;/pdk:Cif&gt;
         {
            let $ccacc := data($invoke/urn:creditCardAccount[1]/urn1:CreditCardAccount[1]/urn1:accountNumber[1])
            let $acc := data($invoke/urn:account[1]/urn2:Account[1]/urn2:accountNumber[1])
            return
                if($ccacc)
                then (&lt;pdk:NrRachunku&gt;{$ccacc}&lt;/pdk:NrRachunku&gt;, &lt;pdk:System&gt;P&lt;/pdk:System&gt;)
                else (&lt;pdk:NrRachunku&gt;{$acc}&lt;/pdk:NrRachunku&gt;, &lt;pdk:System&gt;I&lt;/pdk:System&gt;)
         }
      &lt;/pdk:GetArt105Data&gt;
   &lt;/soap-env:Body&gt;
};

declare variable $body as element() external;

local:invoke($body/urn:invoke)</con:xquery>
</con:xqueryEntry>