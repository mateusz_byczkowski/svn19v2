<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>error handler dla usług chgProcessingAgreeParameters, getProcessingAgreeHistList, getProcessingAgreeParameters</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err = "urn:errors.hlbsentities.be.dcl";
declare namespace pdk = "http://bzwbk.com/services/pdk/";

declare function local:response($fault as element(soap-env:Fault)) as element(soap-env:Body) {
    &lt;soap-env:Body&gt;
    {
        let $se := $fault/detail/pdk:ServiceException
        let $faultstring := fn:concat(data($se/pdk:Description), ", ", data($se/pdk:ErrDetails))
        return
            &lt;soap-env:Fault&gt;
                &lt;faultcode&gt;Server&lt;/faultcode&gt;
                &lt;faultstring&gt;{ $faultstring}&lt;/faultstring&gt;
                &lt;detail&gt;
                    &lt;err:ServiceFailException&gt;
                        &lt;err:exceptionItem&gt;
                            &lt;err:errorCode1&gt;{ data($se/pdk:ErrorCode) }&lt;/err:errorCode1&gt;
                            &lt;err:errorCode2&gt;0&lt;/err:errorCode2&gt;
                            &lt;err:errorDescription&gt;{ $faultstring }&lt;/err:errorDescription&gt;
                        &lt;/err:exceptionItem&gt;
                    &lt;/err:ServiceFailException&gt;
                &lt;/detail&gt;
        &lt;/soap-env:Fault&gt;
    }
    &lt;/soap-env:Body&gt;
};

declare variable $body as element(soap-env:Body) external;

local:response($body/soap-env:Fault)</con:xquery>
</con:xqueryEntry>