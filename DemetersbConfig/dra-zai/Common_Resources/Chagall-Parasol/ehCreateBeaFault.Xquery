<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Tworzenie komunikatu Fault na podstawie błędu zwróconego przez BEA ALSB (błąd wewnętrzny)</con:description>
  <con:xquery>declare namespace ctx = "http://www.bea.com/wli/sb/context";
declare namespace cl = "http://sygnity.pl/functions";

declare variable $fault as element(ctx:fault) external;
declare variable $exception as xs:string external;

declare function cl:messageConvert($errorNumber as xs:string) as xs:string {
    if(fn:starts-with($errorNumber, 'BEA-380')) then 'Wystąpił błąd w komunikacji pomiędzy systemami. Realizowany proces nie został zakończony poprawnie. Spróbuj ponownie w innym terminie'
    else if(fn:starts-with($errorNumber, 'BEA-382')) then 'Błąd wewnętrzny serwisu'
    else if(fn:starts-with($errorNumber, 'BEA-386')) then 'Błąd zabezpieczeń'
    else if(fn:starts-with($errorNumber, 'BEA-3945')) then 'Błąd w komunikacji z serwerem UDDI'
    else 'Nieznany błąd. Skontaktuj się z administratorem'
};

&lt;soap-env:Fault xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"&gt;
	&lt;faultcode&gt;K00319&lt;/faultcode&gt;
	&lt;faultstring&gt;{$exception}: { cl:messageConvert($fault/ctx:errorCode) }&lt;/faultstring&gt;
	&lt;detail&gt;
		&lt;e:ServiceFailException xmlns:e="urn:errors.hlbsentities.be.dcl"&gt;
			&lt;e:exceptionItem&gt;
				&lt;e:errorCode1&gt;1&lt;/e:errorCode1&gt;
				&lt;e:errorCode2?&gt;&lt;/e:errorCode2&gt;
				&lt;e:errorDescription&gt;{$exception}: { cl:messageConvert($fault/ctx:errorCode) }&lt;/e:errorDescription&gt;
			&lt;/e:exceptionItem&gt;
		&lt;/e:ServiceFailException&gt;
	&lt;/detail&gt;
&lt;/soap-env:Fault&gt;</con:xquery>
</con:xqueryEntry>