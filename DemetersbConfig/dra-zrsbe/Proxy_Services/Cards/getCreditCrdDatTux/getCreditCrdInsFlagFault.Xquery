<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace WBKfault="http://bzwbk.com/services/faults/";

declare variable $body external;



&lt;soap-env:Body&gt;
    &lt;FML32&gt;
    {
        let $urcode:=data($body/soap-env:Fault/detail/WBKfault:ServiceException/ErrorCode)
        return
        if ($urcode = "103") then 
            &lt;B_FLAGA_UBEZP&gt;0&lt;/B_FLAGA_UBEZP&gt;
        else
            &lt;B_URCODE&gt;{$urcode}&lt;/B_URCODE&gt;
    }
    &lt;/FML32&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>