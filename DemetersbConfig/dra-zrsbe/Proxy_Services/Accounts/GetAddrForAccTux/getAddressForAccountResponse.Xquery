<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace tem = "http://tempuri.org/";
declare namespace wcf="http://schemas.datacontract.org/2004/07/WcfServiceLibrary.Struktures";


declare function xf:map_getAddressForAccountResponse($res as element(tem:getCardProductAdrResult ))
	as element(fml:FML32) {
	&lt;fml:FML32&gt;
	{
	  	if($res/wcf:Name1)
		then	&lt;fml:NF_ACCOUA_NAME1&gt;{ data($res/wcf:Name1) }&lt;/fml:NF_ACCOUA_NAME1&gt;
		else()
	}	
	{
	  	if($res/wcf:Name2)
		then 	&lt;fml:NF_ACCOUA_NAME2&gt;{ data($res/wcf:Name2) }&lt;/fml:NF_ACCOUA_NAME2&gt;
		else()
	}	
	{
	  	if($res/wcf:AddresStreet)
		then 	&lt;fml:NF_ACCOUA_STREET&gt;{ data($res/wcf:AddresStreet) }&lt;/fml:NF_ACCOUA_STREET&gt;
		else()
	}	
	{
	  	if($res/wcf:AddresHouseflatnumber)
		then 	&lt;fml:NF_ACCOUA_HOUSEFLATNUMBER&gt;{ data($res/wcf:AddresHouseflatnumber) }&lt;/fml:NF_ACCOUA_HOUSEFLATNUMBER&gt;
		else()
	}	
	{
	  	if($res/wcf:AddresCity)
		then 	&lt;fml:NF_ACCOUA_CITY&gt;{ data($res/wcf:AddresCity) }&lt;/fml:NF_ACCOUA_CITY&gt;
		else()
	}	
	{
	  	if($res/wcf:CountcCountrycode)
		then 	&lt;fml:NF_ACCOUA_STATECOUNTRY&gt;{ data($res/wcf:CountcCountrycode) }&lt;/fml:NF_ACCOUA_STATECOUNTRY&gt;
		else()
	}	
	{
	  	if($res/wcf:AddresZipcode)
		then 	&lt;fml:NF_ACCOUA_ZIPCODE&gt;{ data($res/wcf:AddresZipcode) }&lt;/fml:NF_ACCOUA_ZIPCODE&gt;
		else()
	}	
	{
	  	if($res/wcf:AddresValidfrom)
		then 	&lt;fml:NF_ACCOUA_VALIDFROM&gt;{ data($res/wcf:AddresValidfrom) }&lt;/fml:NF_ACCOUA_VALIDFROM&gt;
		else()
	}	
	{
	  	if($res/wcf:AddresValidto)
		then 	&lt;fml:NF_ACCOUA_VALIDTO&gt;{ data($res/wcf:AddresValidto) }&lt;/fml:NF_ACCOUA_VALIDTO&gt;
		else()
	}	
	{
	  	if($res/wcf:Status)
		then 	&lt;fml:NF_RESPOM_RESULT&gt;{ data($res/wcf:Status) }&lt;/fml:NF_RESPOM_RESULT&gt;
		else  &lt;fml:NF_RESPOM_RESULT&gt;0&lt;/fml:NF_RESPOM_RESULT&gt;
	}	
	    
	&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:map_getAddressForAccountResponse($body/tem:getCardProductAdrResponse/tem:getCardProductAdrResult ) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>