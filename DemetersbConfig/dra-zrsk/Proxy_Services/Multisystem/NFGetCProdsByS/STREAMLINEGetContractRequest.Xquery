<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
    <wsdl:getContractRequest xmlns:wsdl="http://jv.channel.cu.com.pl/cmf/wsdl-jv">
        <envelope>
            <request-time>{current-dateTime()}</request-time>
            <request-no?>{data($body/FML32/NF_MSHEAD_MSGID)}</request-no>
            <source-code>placowka banku</source-code>
            <user-id?>{data($body/FML32/NF_MSHEAD_USERID)}</user-id>
            <branch-id?>{data($body/FML32/NF_MSHEAD_UNITID)}</branch-id>
            (:<bar-code>string</bar-code>:)
        </envelope>
        (:<category>string</category>:)
        <product-type>{substring-before(data($body/FML32/NF_ACCOUN_ACCOUNTNUMBER),':')}</product-type>
        <contract-number>{substring-after(data($body/FML32/NF_ACCOUN_ACCOUNTNUMBER),':')}</contract-number>
        (:<contract-number>{data($body/FML32/NF_ACCOUN_ACCOUNTNUMBER)}</contract-number>:)
        (:<proposal-number>string</proposal-number>:)
    </wsdl:getContractRequest>
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>