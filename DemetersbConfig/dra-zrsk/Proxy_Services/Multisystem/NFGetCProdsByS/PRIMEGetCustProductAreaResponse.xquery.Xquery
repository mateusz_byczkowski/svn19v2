<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(: Change Log 
v.1.1 2009-08-26 PKL NP1995 Dodanie pól ProductStat i ProductStatusCode
v.1.2 2009-11-02 PKL T41479 Ukrywanie danych kart dodatkowych 
v.1.3 2010-01-12 PKL T43830 Pokazywanie wartości pola NF_IS_COOWNER kart dodatkowych
v.1.4 2011-02-02 PKL T52074 Błąd w obsłudze stronicowania dla PRIME, CEKE i STRL
v.1.5 2011-04-08 PKL NP2233 Zmiana wyznaczania rdscode dla kart kredytowych
v.1.6 2011-05-05 PKL NP2233 T56473 Dodanie daty wygaśnięcia karty kredytowej (NF_DEBITC_EXPIRATIONDATE)
:)

declare namespace x = "http://bzwbk.com/services/prime/";
declare namespace m = "http://schemas.datacontract.org/2004/07/SzrekLibrary.Struktures";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";



declare function xf:calculatePageStart ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then 1
else if ($actioncode="N")
(: 1.4  then xs:integer(min(($records  , $oldstop + 1)))  :)  
           then xs:integer(min(($records + 1, $oldstop + 1))) (:1.4:)

else if ($actioncode="P")
   then max((1,$oldstart - $pagesize)) 
else if ($actioncode="L")
(: 1.4  then max((1,$records  - $pagesize)) :)
           then max((1,$records  - $pagesize + 1)) (:1.4:)

else 0
};

declare function xf:calculatePageStop ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then xs:integer(min(($pagesize,$records)))
else if ($actioncode="N")
   then xs:integer(min(($pagesize + $oldstop,$records))) 
else if ($actioncode="P")
(: 1.4  then max((1,$oldstart - 1)) :)
           then max((0,$oldstart - 1)) (:1.4:)
else if ($actioncode="L")
   then $records
else xs:integer(0)
};


declare function xf:calculateHasNext( $pagestart as xs:integer,$pagestop as xs:integer, 
                                                     $records as xs:integer, $actioncode as xs:string) as xs:integer{
(: 1.4   if ($actioncode != "P")  :)
    if ($actioncode != "P" and $actioncode != "L")  (:1.4:)
     then  if ($pagestop &lt; $records)
           then 1
           else 0
    else if ($pagestart &gt; 1)
           then 1
           else 0 
};

declare function xf:mapProduct($prod as element(m:Product))  as element()* {
(: Pola wspólne, zwracane dla rachunków,  kart głównych i dodatkowych: :)
(:1.5 out   &lt;fml:NF_PRODUD_SORCEPRODUCTCODE&gt; &lt;/fml:NF_PRODUD_SORCEPRODUCTCODE&gt;
    , 1.5:)
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))&gt;0)
      then &lt;fml:NF_ACCOUN_ACCOUNTNUMBER  nil="true" /&gt;
      else &lt;fml:NF_ACCOUN_ACCOUNTNUMBER &gt;{substring(data($prod/m:DC_NR_RACHUNKU),2)}&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER&gt;
    ,
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))&gt;0)
      then &lt;fml:NF_DEBITC_VIRTUALCARDNBR&gt;{concat("V",data($prod/m:DC_NR_ALTERNATYWNY))}&lt;/fml:NF_DEBITC_VIRTUALCARDNBR&gt;
      else &lt;fml:NF_DEBITC_VIRTUALCARDNBR nil="true" /&gt;
    ,
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))&gt;0)
      then &lt;fml:NF_DEBITC_CARDNBR&gt;{substring(data($prod/m:DC_NR_RACHUNKU),2)}&lt;/fml:NF_DEBITC_CARDNBR&gt;
      else &lt;fml:NF_DEBITC_CARDNBR nil="true" /&gt;
    ,
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))&gt;0)
      then &lt;NF_PRODUA_CODEPRODUCTAREA&gt;1&lt;/NF_PRODUA_CODEPRODUCTAREA&gt;
      else &lt;NF_PRODUA_CODEPRODUCTAREA&gt;2&lt;/NF_PRODUA_CODEPRODUCTAREA&gt;
    ,
(:1.5 Start:)

    if ($prod/m:CI_PRODUCT_ATT4 and $prod/m:CI_PRODUCT_ATT1)
      then &lt;fml:NF_PRODUD_SORCEPRODUCTCODE&gt;{concat(data($prod/m:CI_PRODUCT_ATT4), data($prod/m:CI_PRODUCT_ATT1))}&lt;/fml:NF_PRODUD_SORCEPRODUCTCODE&gt;
      else &lt;fml:NF_PRODUD_SORCEPRODUCTCODE nil="true" /&gt;
    ,
(:1.5 Koniec:)
    if ($prod/m:CI_PRODUCT_ATT1 and string-length(data($prod/m:CI_PRODUCT_ATT1))&gt;0)
      then &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT&gt;{data($prod/m:CI_PRODUCT_ATT1)}&lt;/fml:NF_ATTRPG_FIRSTPRODUCTFEAT&gt;    
      else &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT nil="true"/&gt;
    ,
    &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA nil="true"/&gt;
     ,
    &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/&gt;
     ,
    &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/&gt;
     ,
    &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT nil="true"/&gt;
     ,
    if ($prod/m:CI_PRODUCT_NAME_ORYGINAL)
      then &lt;fml:NF_ACCOUN_ACCOUNTNAME&gt;{data($prod/m:CI_PRODUCT_NAME_ORYGINAL)}&lt;/fml:NF_ACCOUN_ACCOUNTNAME&gt;
      else &lt;fml:NF_ACCOUN_ACCOUNTNAME nil="true" /&gt;
    ,
(: NP1995 początek :)
    if ($prod/m:NF_PRODUCT_STAT)
      then &lt;fml:NF_PRODUCT_STAT&gt;{data($prod/m:NF_PRODUCT_STAT)}&lt;/fml:NF_PRODUCT_STAT&gt;
      else &lt;fml:NF_PRODUCT_STAT nil="true" /&gt;
    ,
    if ($prod/m:NF_PRODUCT_STATUSCODE)
      then &lt;fml:NF_PRODUCT_STATUSCODE&gt;{data($prod/m:B_KOD_RACH)}&lt;/fml:NF_PRODUCT_STATUSCODE&gt;
      else &lt;fml:NF_PRODUCT_STATUSCODE nil="true" /&gt;
    ,
(: NP1995 koniec :)
    &lt;NF_CTRL_SYSTEMID&gt;2&lt;/NF_CTRL_SYSTEMID&gt;
    ,			
    if ($prod/m:B_KOD_RACH)
      then &lt;fml:NF_PRIMAC_ACCOUNTCODE&gt;{data($prod/m:B_KOD_RACH)}&lt;/fml:NF_PRIMAC_ACCOUNTCODE&gt;
      else &lt;fml:NF_PRIMAC_ACCOUNTCODE nil="true" /&gt;
    ,		
    if ($prod/m:CI_PRODUCT_ATT4)
      then &lt;fml:NF_CARD_BIN&gt;{data($prod/m:CI_PRODUCT_ATT4)}&lt;/fml:NF_CARD_BIN&gt;
      else &lt;fml:NF_CARD_BIN nil="true" /&gt;
    ,	
    if ($prod/m:CI_PRODUCT_ATT2)
      then &lt;fml:NF_CARD_CARDTYPE&gt;{data($prod/m:CI_PRODUCT_ATT2)}&lt;/fml:NF_CARD_CARDTYPE&gt;
      else &lt;fml:NF_CARD_CARDTYPE nil="true" /&gt;
    ,	
     if ($prod/m:B_POJEDYNCZY)
        then &lt;fml:NF_IS_COOWNER&gt;{data($prod/m:B_POJEDYNCZY)}&lt;/fml:NF_IS_COOWNER&gt;
        else &lt;fml:NF_IS_COOWNER nil="true" /&gt;
      ,
(:1.6 początek:)
     if ($prod/m:B_CARD_EXPIRYDATE)
        then &lt;fml:NF_DEBITC_EXPIRATIONDATE&gt;{data($prod/m:B_CARD_EXPIRYDATE)}&lt;/fml:NF_DEBITC_EXPIRATIONDATE&gt;
        else &lt;fml:NF_DEBITC_EXPIRATIONDATE nil="true" /&gt;
      ,
(:1.6 koniec:)

(: Pola zwracane tylko dla kart głównych: :)
    if ($prod/m:CI_PRODUCT_ATT2 and data($prod/m:CI_PRODUCT_ATT2)=1) then
   (
     if ($prod/m:CI_RACHUNEK_ADR_ALT)
        then &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;{data($prod/m:CI_RACHUNEK_ADR_ALT)}&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;
        else&lt;fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;0&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;
      , 			
      if ($prod/m:B_SALDO)
        then &lt;fml:NF_ACCOUN_CURRENTBALANCE&gt;{data($prod/m:B_SALDO)}&lt;/fml:NF_ACCOUN_CURRENTBALANCE&gt;
        else &lt;fml:NF_ACCOUN_CURRENTBALANCE nil="true" /&gt;
      ,			
      if ($prod/m:B_BLOKADA)
        then &lt;fml:NF_HOLD_HOLDAMOUNT&gt;{data($prod/m:B_BLOKADA)}&lt;/fml:NF_HOLD_HOLDAMOUNT&gt;
        else &lt;fml:NF_HOLD_HOLDAMOUNT&gt;0&lt;/fml:NF_HOLD_HOLDAMOUNT&gt;
      ,			
      if ($prod/m:B_DOST_SRODKI)
        then &lt;fml:NF_ACCOUN_AVAILABLEBALANCE&gt;{data($prod/m:B_DOST_SRODKI)}&lt;/fml:NF_ACCOUN_AVAILABLEBALANCE&gt;
        else &lt;fml:NF_ACCOUN_AVAILABLEBALANCE nil="true" /&gt;
      ,
      if ($prod/m:B_KOD_WALUTY)
        then &lt;fml:NF_CURREC_CURRENCYCODE&gt;{data($prod/m:B_KOD_WALUTY)}&lt;/fml:NF_CURREC_CURRENCYCODE&gt;
        else &lt;fml:NF_CURREC_CURRENCYCODE nil="true"/&gt;
      ,			
      if ($prod/m:B_D_OTWARCIA)
        then &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_D_OTWARCIA))}&lt;/fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;
        else &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;0001-01-01&lt;/fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;
      ,			
      if ($prod/m:B_DATA_OST_OPER)
        then &lt;fml:NF_TRANA_DATEOFLASTACTIVIT&gt;{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_DATA_OST_OPER))}&lt;/fml:NF_TRANA_DATEOFLASTACTIVIT&gt;
        else &lt;fml:NF_TRANA_DATEOFLASTACTIVIT&gt;0001-01-01&lt;/fml:NF_TRANA_DATEOFLASTACTIVIT&gt;
      ,		
      if ($prod/m:B_LIMIT1)
        then &lt;fml:NF_TRAACA_LIMITAMOUNT&gt;{data($prod/m:B_LIMIT1)}&lt;/fml:NF_TRAACA_LIMITAMOUNT&gt;
        else &lt;fml:NF_TRAACA_LIMITAMOUNT nil="true" /&gt;
   ) else (: koniec sekcji pól zwracanych tylko dla kart głównych :)
  ( (: pola z wyczyszczoną wartością zwracane dla kart dodatkowych :)
     &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE nil="true" /&gt;,
     &lt;fml:NF_ACCOUN_CURRENTBALANCE nil="true" /&gt;,
     &lt;fml:NF_HOLD_HOLDAMOUNT nil="true" /&gt;,
     &lt;fml:NF_ACCOUN_AVAILABLEBALANCE nil="true" /&gt; ,
     &lt;fml:NF_CURREC_CURRENCYCODE nil="true" /&gt; ,
     &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE nil="true" /&gt;,
     &lt;fml:NF_TRANA_DATEOFLASTACTIVIT nil="true" /&gt;,
     &lt;fml:NF_TRAACA_LIMITAMOUNT nil="true" /&gt; 
   ) 
};

declare function xf:mapICBSGetCProAreaResponse ($res as element(x:PRIMEGetCustProductsAreaResult),$area as xs:string,
  $pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
								  $actioncode as xs:string )
as element(fml:FML32){
	let $records:=count( $res/m:Product)
	let $start:=xf:calculatePageStart($pagesize,$oldstart,$oldstop,$actioncode,$records)
	let $stop:=xf:calculatePageStop($pagesize,$oldstart,$oldstop,$actioncode,$records)
	let $newnavkey:=concat("012:",string($start),":",string($stop))
	let $hasnext:=xf:calculateHasNext($start,$stop,$records,$actioncode)
	return
	   &lt;fml:FML32&gt;
                           {
                             (:1.4 start:)
                             if ($start &gt; $records or $stop &lt;= 0) 
                             then (
		  &lt;NF_PAGEC_NAVIGATIONKEYVALU&gt;012&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;,
		  &lt;NF_PAGEC_HASNEXT&gt;0&lt;/NF_PAGEC_HASNEXT&gt;,
		  &lt;NF_PAGECC_OPERATIONS&gt;0&lt;/NF_PAGECC_OPERATIONS&gt;
                                    )
                             else (    
		  &lt;NF_PAGEC_NAVIGATIONKEYVALU&gt;{$newnavkey}&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;,
		  &lt;NF_PAGEC_HASNEXT&gt;{$hasnext}&lt;/NF_PAGEC_HASNEXT&gt;,
		  &lt;NF_PAGECC_OPERATIONS&gt;{max((0,$stop - $start+1))}&lt;/NF_PAGECC_OPERATIONS&gt;,
		 
			for $idx in ($start to $stop)
			   return
				  xf:mapProduct($res/m:Product[$idx]) 
		  
                                    )  (:1.4 koniec :)
                            }
	   &lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
declare variable $area as xs:string external;
declare variable $pagesize as xs:integer external;
declare variable $oldstart  as xs:integer external;
declare variable $oldstop  as xs:integer external;
declare variable $actioncode  as xs:string external;
&lt;soap-env:Body&gt;
{xf:mapICBSGetCProAreaResponse($body/x:PRIMEGetCustProductsAreaResponse/x:PRIMEGetCustProductsAreaResult,$area,$pagesize,$oldstart,$oldstop,$actioncode)}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>