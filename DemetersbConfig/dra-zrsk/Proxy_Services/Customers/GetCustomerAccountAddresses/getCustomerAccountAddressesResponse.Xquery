<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2010-02-03 PKL PT58 - błąd zaminy anyType</con:description>
  <con:xquery>(: Rejestr zmian 
v.1.1 2010-02-03 PKL PT58 - błąd zaminy anyType
:)

declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()*
{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};


declare function sourceValue2Boolean ($parm as xs:string*,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};


declare function getElementsForAccount($parm as element(fml:FML32)) as element()
{

&lt;ns0:account&gt;
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
    &lt;ns6:Account&gt;
      &lt;ns6:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns6:accountNumber&gt;
      &lt;ns6:accountName?&gt;{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}&lt;/ns6:accountName&gt;
      &lt;ns6:currentBalance?&gt;{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns6:currentBalance&gt;
      &lt;ns6:accountDescription?&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns6:accountDescription&gt;
      &lt;ns6:accountIBAN?&gt;{data($parm/NF_ACCOUN_ACCOUNTIBAN[$occ])}&lt;/ns6:accountIBAN&gt;
      &lt;ns6:currency&gt;
        &lt;ns5:CurrencyCode&gt;
          &lt;ns5:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode&gt;
        &lt;/ns5:CurrencyCode&gt;
      &lt;/ns6:currency&gt;
      &lt;ns6:accountAddress&gt;
        &lt;ns6:AccountAddress&gt;
          &lt;ns6:name1?&gt;{data($parm/NF_ACCOUA_NAME1[$occ])}&lt;/ns6:name1&gt;
          &lt;ns6:name2?&gt;{data($parm/NF_ACCOUA_NAME2[$occ])}&lt;/ns6:name2&gt;
          &lt;ns6:street?&gt;{data($parm/NF_ACCOUA_STREET[$occ])}&lt;/ns6:street&gt;
          &lt;ns6:houseFlatNumber?&gt;{data($parm/NF_ACCOUA_HOUSEFLATNUMBER[$occ])}&lt;/ns6:houseFlatNumber&gt;
          &lt;ns6:city?&gt;{data($parm/NF_ACCOUA_CITY[$occ])}&lt;/ns6:city&gt;
          &lt;ns6:stateCountry?&gt;{data($parm/NF_ACCOUA_STATECOUNTRY[$occ])}&lt;/ns6:stateCountry&gt;
          &lt;ns6:zipCode?&gt;{data($parm/NF_ACCOUA_ZIPCODE[$occ])}&lt;/ns6:zipCode&gt;
          &lt;ns6:accountAddressType?&gt;{data($parm/NF_ACCOUA_ACCOUNTADDRESSTY[$occ])}&lt;/ns6:accountAddressType&gt;
          {insertDate(data($parm/NF_ACCOUA_VALIDFROM[$occ]),"yyyy-MM-dd","ns6:validFrom")}
          {insertDate(data($parm/NF_ACCOUA_VALIDTO[$occ]),"yyyy-MM-dd","ns6:validTo")}
          &lt;ns6:deleteWhenExpired?&gt;{sourceValue2Boolean (data($parm/NF_ACCOUA_DELETEWHENEXPIRE[$occ]),"1")}&lt;/ns6:deleteWhenExpired&gt;
        &lt;/ns6:AccountAddress&gt;
      &lt;/ns6:accountAddress&gt;
    &lt;/ns6:Account&gt;
  }
&lt;/ns0:account&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse&gt;
  {getElementsForAccount($parm)}
  &lt;ns0:bcd&gt;
    &lt;ns3:BusinessControlData&gt;
      &lt;ns3:pageControl&gt;
        &lt;ns4:PageControl&gt;
          &lt;ns4:hasNext?&gt;{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}&lt;/ns4:hasNext&gt;
        
          &lt;ns4:navigationKeyValue?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns4:navigationKeyValue&gt;
        &lt;/ns4:PageControl&gt;
      &lt;/ns3:pageControl&gt;
    &lt;/ns3:BusinessControlData&gt;
  &lt;/ns0:bcd&gt;
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>