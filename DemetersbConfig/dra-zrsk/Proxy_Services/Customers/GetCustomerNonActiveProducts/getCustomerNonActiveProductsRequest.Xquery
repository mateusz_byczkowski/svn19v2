<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(: Change Log 
v.1.1 2011-03-23 PK  PT58 Content Streaming

:)

declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:card.entities.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns7="urn:dictionaries.be.dcl";
declare namespace ns8="urn:insurance.entities.be.dcl";
declare namespace ns9="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml="";
declare namespace ns10="urn:errors.hlbsentities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



declare function boolean2SourceValue ($parm as xs:string*,$trueval as xs:string,$falseval as xs:string) as xs:string* {
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
(:v.1.1 Start:)
,
&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
(:v.1.1 Koniec:)
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

&lt;NF_PRODUA_CODEPRODUCTAREA?&gt;{data($parm/ns0:productArea/ns1:ProductArea/ns1:codeProductArea)}&lt;/NF_PRODUA_CODEPRODUCTAREA&gt;
,
&lt;NF_PAGEC_ACTIONCODE?&gt;{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:actionCode)}&lt;/NF_PAGEC_ACTIONCODE&gt;
,
&lt;NF_PAGEC_PAGESIZE?&gt;{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:pageSize)}&lt;/NF_PAGEC_PAGESIZE&gt;
,
&lt;NF_PAGEC_REVERSEORDER?&gt;{boolean2SourceValue (data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:reverseOrder),"1","0")}&lt;/NF_PAGEC_REVERSEORDER&gt;
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?&gt;{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;
,
&lt;NF_CUSTOM_CUSTOMERNUMBER?&gt;{data($parm/ns0:customer/ns4:Customer/ns4:customerNumber)}&lt;/NF_CUSTOM_CUSTOMERNUMBER&gt;
,
&lt;NF_CTRL_ACTIVENONACTIVE&gt;2&lt;/NF_CTRL_ACTIVENONACTIVE&gt;
,
&lt;NF_ACCOUN_ALTERNATIVEADDRE&gt;0&lt;/NF_ACCOUN_ALTERNATIVEADDRE&gt;
,
&lt;NF_CTRL_OPTION&gt;0&lt;/NF_CTRL_OPTION&gt;
,
&lt;NF_CTRL_SYSTEMS&gt;001&lt;/NF_CTRL_SYSTEMS&gt;
,
&lt;NF_CTRL_SHOWCURRBAL?&gt;{data($parm/ns0:showCurrentBalance/ns6:IntegerHolder/ns6:value)}&lt;/NF_CTRL_SHOWCURRBAL&gt;
,
&lt;NF_PRODUG_VISIBILITYDCL&gt;1&lt;/NF_PRODUG_VISIBILITYDCL&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>