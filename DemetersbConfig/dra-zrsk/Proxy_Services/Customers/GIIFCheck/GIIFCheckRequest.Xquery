<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace urn  = "urn:be.services.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body&gt;
  {
    let $req := $body/urn:invoke
    let $record := $req/urn:recordList/urn:record
    let $type := $record/urn:type
    let $number := $record/urn:number
    return

    &lt;fml:FML32&gt;
      {for $it at $i in $record
        return
        (
          &lt;fml:GI_TYP_REKORDU&gt;{ data($type[$i]) }&lt;/fml:GI_TYP_REKORDU&gt;,
          &lt;fml:GI_NUMER&gt;{ data($number[$i]) }&lt;/fml:GI_NUMER&gt;
        )
      }
    &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>