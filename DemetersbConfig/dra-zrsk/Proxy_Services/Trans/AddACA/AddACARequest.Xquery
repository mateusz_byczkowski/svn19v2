<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:cif.entities.be.dcl";
declare namespace m3 = "urn:dictionaries.be.dcl";
declare namespace m4 = "urn:accountdict.dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";


declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string

  return 
    if ($value &lt; 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
    fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
               xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
               xf:convertTo4CharString(fn:year-from-date($dateIn)))
};

declare function xf:shortAccount($account as xs:string) as xs:string {
    if (fn:string-length($account) &gt; 12)
      then fn:substring($account, (fn:string-length($account))-11)
      else $account
};

&lt;soap-env:Body&gt;
  {
    let $reqh  := $header/m:header
    let $req   := $body/m:invoke/m:account/m1:Account
    let $req1  := $req/m1:tranAccount/m1:TranAccount/m1:tranAccountACAList/m1:TranAccountACA
    let $req2  := $req/m1:indirectCommitmenntList
    let $req3  := $req/m1:accountRelationshipList/m2:AccountRelationship
    let $req4  := $req3/m2:processingAgree/m2:ProcessingAgree

    let $msgId := $reqh/m:msgHeader/m:msgId
    let $transId := $reqh/m:transHeader/m:transId
    let $userId  := $reqh/m:msgHeader/m:userId
    let $unitId  := $reqh/m:msgHeader/m:unitId
    let $agreementDate := $req4/m2:agreementDate
    let $IndirectCommitment := $req2/m1:IndirectCommitment
    let $ICtypeIndirectCommitment := $IndirectCommitment/m1:typeIndirectCommitment
    let $ICamountMaxIndirectCommitment := $IndirectCommitment/m1:amountMaxIndirectCommitment
    let $ICindirectCommitmentPercent := $IndirectCommitment/m1:indirectCommitmentPercent
    let $ICcustomerNumber := $IndirectCommitment/m1:customer/m2:Customer/m2:customerNumber
    let $ICagreementFlag := $IndirectCommitment/m1:customer/m2:Customer/m2:accountRelationshipList/m2:AccountRelationship/m2:processingAgree/m2:ProcessingAgree/m2:agreementFlag/m3:CustomerAgreementStatus/m3:customerAgreementStatus
    let $ICagreementValidityPeriod := $IndirectCommitment/m1:customer/m2:Customer/m2:accountRelationshipList/m2:AccountRelationship/m2:processingAgree/m2:ProcessingAgree/m2:agreementValidityPeriod
    let $ICagreementDate := $IndirectCommitment/m1:customer/m2:Customer/m2:accountRelationshipList/m2:AccountRelationship/m2:processingAgree/m2:ProcessingAgree/m2:agreementDate
    let $nextMaturityDate := $req1/m1:nextMaturityDate
    let $nextInterestDate := $req1/m1:nextInterestDate
    let $nextAcaFeeDate := $req1/m1:nextAcaFeeDate
    
    return

    &lt;fml:FML32&gt;
      &lt;fml:DC_MSHEAD_MSGID?&gt;{ data($msgId) }&lt;/fml:DC_MSHEAD_MSGID&gt;
      &lt;fml:DC_TRN_ID?&gt;{ data($transId) }&lt;/fml:DC_TRN_ID&gt;
      &lt;fml:DC_UZYTKOWNIK?&gt;{concat("SKP:", data($userId)) }&lt;/fml:DC_UZYTKOWNIK&gt;

      {if($unitId)
          then 
              if($unitId &lt; 1000)
                then &lt;fml:DC_ODDZIAL&gt;{ data($unitId) }&lt;/fml:DC_ODDZIAL&gt;
                else &lt;fml:DC_ODDZIAL&gt;0&lt;/fml:DC_ODDZIAL&gt;
          else ()
      }

      &lt;fml:DC_LK_ID_UZYTKOWNIKA_Z_DC?&gt;{concat("SKP:", data($userId)) }&lt;/fml:DC_LK_ID_UZYTKOWNIKA_Z_DC&gt;
      &lt;fml:DC_LK_RODZAJ_OPERACJI?&gt;LK7&lt;/fml:DC_LK_RODZAJ_OPERACJI&gt;
      &lt;fml:DC_LK_NR_RACHUNKU_ICBS?&gt;{ xf:shortAccount($req/m1:accountNumber) }&lt;/fml:DC_LK_NR_RACHUNKU_ICBS&gt;
      &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req3/m2:customer/m2:Customer/m2:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA&gt;
      &lt;fml:DC_FLAGA_ZGODY&gt;{ data($req4/m2:agreementFlag/m3:CustomerAgreementStatus/m3:customerAgreementStatus) }&lt;/fml:DC_FLAGA_ZGODY&gt;
      &lt;fml:DC_OKRES_WAZNOSCI_ZGODY&gt;{ data($req4/m2:agreementValidityPeriod) }&lt;/fml:DC_OKRES_WAZNOSCI_ZGODY&gt;
      {if($agreementDate and fn:string-length($agreementDate)&gt;0)
        then &lt;fml:DC_DATA_ZGODY&gt;{ if ($agreementDate) then xf:mapDate($agreementDate) else () }&lt;/fml:DC_DATA_ZGODY&gt;
        else &lt;fml:DC_DATA_ZGODY/&gt;
      }
      
      {for $it at $i in $IndirectCommitment
        return
        (
          &lt;fml:DC_TYP_POSRED_ZOBOWIAZANIA&gt;{ data($ICtypeIndirectCommitment[$i]) }&lt;/fml:DC_TYP_POSRED_ZOBOWIAZANIA&gt;,
          &lt;fml:DC_MAX_KWOTA_PORECZENIA&gt;{ data($ICamountMaxIndirectCommitment[$i]) }&lt;/fml:DC_MAX_KWOTA_PORECZENIA&gt;,
          &lt;fml:DC_PROCENT_ZOBOWIAZANIA&gt;{ data($ICindirectCommitmentPercent[$i]) }&lt;/fml:DC_PROCENT_ZOBOWIAZANIA&gt;,
          &lt;fml:DC_NUMER_PORECZYCIELA_Z_CIF&gt;{ data($ICcustomerNumber[$i]) }&lt;/fml:DC_NUMER_PORECZYCIELA_Z_CIF&gt;,
          &lt;fml:DC_FLAGA_ZGODY&gt;{ data($ICagreementFlag[$i]) }&lt;/fml:DC_FLAGA_ZGODY&gt;,
          &lt;fml:DC_OKRES_WAZNOSCI_ZGODY&gt;{ data($ICagreementValidityPeriod[$i]) }&lt;/fml:DC_OKRES_WAZNOSCI_ZGODY&gt;,
          if($ICagreementDate[$i] and fn:string-length($ICagreementDate[$i])&gt;0)
            then &lt;fml:DC_DATA_ZGODY&gt;{ xf:mapDate($ICagreementDate[$i]) }&lt;/fml:DC_DATA_ZGODY&gt;
            else &lt;fml:DC_DATA_ZGODY/&gt;
        )
      }

      &lt;fml:DC_LK_DATA_WYMAGALNOSCI?&gt;{ if ($nextMaturityDate) then xf:mapDate($nextMaturityDate) else () }&lt;/fml:DC_LK_DATA_WYMAGALNOSCI&gt;
      &lt;fml:DC_LK_KWOTA_1?&gt;{ data($req1/m1:limitAmount) }&lt;/fml:DC_LK_KWOTA_1&gt;
      &lt;fml:DC_LK_NR_INDEKSU?&gt;{ data($req1/m1:indexNumber) }&lt;/fml:DC_LK_NR_INDEKSU&gt;
      &lt;fml:DC_LK_MARZA_BANKU_ODSETKI?&gt;{ data($req1/m1:varianceForIndex) }&lt;/fml:DC_LK_MARZA_BANKU_ODSETKI&gt;
      &lt;fml:DC_LK_STOPA_IDYWIDUALNA_ODS?&gt;{ round-half-to-even(data($req1/m1:negotiatedInterestRate)*100,4) }&lt;/fml:DC_LK_STOPA_IDYWIDUALNA_ODS&gt;
      &lt;fml:DC_CZEST_SPLAT_ODS?&gt;{ data($req1/m1:interestFrequency) }&lt;/fml:DC_CZEST_SPLAT_ODS&gt;
      &lt;fml:DC_DATA_PIERWSZEJ_PLAT_ODS?&gt;{ if ($nextInterestDate) then xf:mapDate($nextInterestDate) else () }&lt;/fml:DC_DATA_PIERWSZEJ_PLAT_ODS&gt;
      &lt;fml:DC_LK_PODSTAWA_MIESIECZNA?&gt;{ data($req1/m1:accrualBase) }&lt;/fml:DC_LK_PODSTAWA_MIESIECZNA&gt;
      &lt;fml:DC_LK_PODSTAWA_ROCZNA?&gt;{ data($req1/m1:yearBase) }&lt;/fml:DC_LK_PODSTAWA_ROCZNA&gt;
      &lt;fml:DC_LK_KWOTY_OPLATY?&gt;{ data($req1/m1:feePercentage) }&lt;/fml:DC_LK_KWOTY_OPLATY&gt;
      &lt;fml:DC_LK_MIN_WYSOKOSC_OPLATY?&gt;{ data($req1/m1:minimumFeeAmount) }&lt;/fml:DC_LK_MIN_WYSOKOSC_OPLATY&gt;
      &lt;fml:DC_LK_CZEST_OPLATY_TOD_ACA?&gt;{ data($req1/m1:acaFeeFrequency) }&lt;/fml:DC_LK_CZEST_OPLATY_TOD_ACA&gt;
      &lt;fml:DC_LK_OKR_DZIEN_OPL_TOD_ACA?&gt;{ data($req1/m1:acaFeeSpecificDay) }&lt;/fml:DC_LK_OKR_DZIEN_OPL_TOD_ACA&gt;
      &lt;fml:DC_LK_DATA_NAST_OPLATY_ACA?&gt;{ if ($nextAcaFeeDate) then xf:mapDate($nextAcaFeeDate) else () }&lt;/fml:DC_LK_DATA_NAST_OPLATY_ACA&gt;
      &lt;fml:DC_LK_ID_WNIOSKU?&gt;{ data($transId) }&lt;/fml:DC_LK_ID_WNIOSKU&gt;
      &lt;fml:DC_LK_NR_UMOWY_KREDYTOWEJ?&gt;{ data($req1/m1:agreementNumber) }&lt;/fml:DC_LK_NR_UMOWY_KREDYTOWEJ&gt;
      &lt;fml:DC_OKRESLONY_DZIEN_ODS?&gt;{ data($req1/m1:interestSpecialDay) }&lt;/fml:DC_OKRESLONY_DZIEN_ODS&gt;
      &lt;fml:DC_OKRES_SPLAT_ODS?&gt;{ data($req1/m1:interestPeriod/m3:Period/m3:period) }&lt;/fml:DC_OKRES_SPLAT_ODS&gt;
      &lt;fml:DC_LK_KOD_OPLATY_TOD_ACA?&gt;{ data($req1/m1:acaFeeCode/m4:AcaFeeCode/m4:acaFeeCode) }&lt;/fml:DC_LK_KOD_OPLATY_TOD_ACA&gt;
      &lt;fml:DC_LK_OKRES_OPLATY_TOD_ACA?&gt;{ data($req1/m1:acaFeePeriod/m3:Period/m3:period) }&lt;/fml:DC_LK_OKRES_OPLATY_TOD_ACA&gt;
      &lt;fml:DC_LK_NR_APLIKACJI&gt;20&lt;/fml:DC_LK_NR_APLIKACJI&gt;
    &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>