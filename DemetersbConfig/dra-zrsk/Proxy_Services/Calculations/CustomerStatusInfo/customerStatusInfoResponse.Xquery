<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace swwr = "http://swwr.talex.com/";

declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body&gt;
{
    let $req := $body/swwr:getCustomerStatusResponse
    return
    
    &lt;urn:getCustomerStatus xmlns:urn="urn:RatingWS"&gt;
	    &lt;return xsi:type="urn:CustomerStatusInfo" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"&gt;
          {if($req/result)
              then
                    &lt;cif&gt;{ data($req/result/cif) }&lt;/cif&gt;
              else ()
          }
          {if($req/result)
              then
                    &lt;status&gt;{ data($req/result/status) }&lt;/status&gt;
              else ()
          }
          {if($req/result)
              then
                    &lt;startDate&gt;{ data($req/result/startDate) }&lt;/startDate&gt;
              else ()
          }
          {if($req/result)
              then
                    &lt;userLogin&gt;&lt;item&gt;{ data($req/result/userLogin) }&lt;/item&gt;&lt;/userLogin&gt;
              else ()
          }
          {if($req/result)
              then
                    &lt;contactData&gt;&lt;item&gt;{ data($req/result/contactData) }&lt;/item&gt;&lt;/contactData&gt;
              else ()
          }
          {if($req/result)
              then
                    &lt;ratingReason&gt;{ data($req/result/ratingReason) }&lt;/ratingReason&gt;
              else ()
          }
          {if($req/result)
              then
                    &lt;ratingValue&gt;{ data($req/result/ratingValue) }&lt;/ratingValue&gt;
              else ()
          }
            &lt;/return&gt;
      &lt;/urn:getCustomerStatus&gt;
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>