<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:cif.entities.be.dcl";
declare namespace m3 = "urn:dictionaries.be.dcl";
declare namespace m4 = "urn:accountdict.dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string
  return 
    if ($value &lt; 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDateTime($dateIn as xs:dateTime) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-dateTime($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-dateTime($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-dateTime($dateIn)))
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-date($dateIn)))
};

declare function xf:booleanTo01($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "1"
    else "0"
};

declare function xf:booleanToTN($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "T"
    else "N"
};

&lt;soap-env:Body&gt;
  {
    let $req  := $body/m:invoke/m:collateral/m1:Collateral
    let $reqh := $header/m:header
    let $reqm := $req/m1:collateralMortgage/m1:CollateralMortgage
 
    return
    
    &lt;fml:FML32&gt;
      {if($reqh/m:transHeader/m:transId)
          then &lt;fml:DC_TRN_ID&gt;{ data($reqh/m:transHeader/m:transId) }&lt;/fml:DC_TRN_ID&gt;
          else ()
      }
    {if($reqh/m:msgHeader/m:msgId)
          then &lt;fml:DC_MSHEAD_MSGID&gt;{ data($reqh/m:msgHeader/m:msgId) }&lt;/fml:DC_MSHEAD_MSGID&gt;
          else ()
      }
      {if($reqh/m:msgHeader/m:userId)
          then &lt;fml:DC_UZYTKOWNIK&gt;{concat("SKP:", data($reqh/m:msgHeader/m:userId)) }&lt;/fml:DC_UZYTKOWNIK&gt;
          else ()
      }
      {if($reqh/m:msgHeader/m:unitId)
          then 
              if($reqh/m:msgHeader/m:unitId &lt; 1000)
                then &lt;fml:DC_ODDZIAL&gt;{ data($reqh/m:msgHeader/m:unitId) }&lt;/fml:DC_ODDZIAL&gt;
                else &lt;fml:DC_ODDZIAL&gt;0&lt;/fml:DC_ODDZIAL&gt;
          else ()
      }

      {if($req/m1:locationCode and fn:string-length($req/m1:locationCode)&gt;0)
          then &lt;fml:DC_KOD_NALICZENIA_REZERWY&gt;{ data($req/m1:locationCode) }&lt;/fml:DC_KOD_NALICZENIA_REZERWY&gt;
          else &lt;fml:DC_KOD_NALICZENIA_REZERWY&gt;0&lt;/fml:DC_KOD_NALICZENIA_REZERWY&gt;
      }
      {if($req/m1:collateralDescription and fn:string-length($req/m1:collateralDescription)&gt;0)
          then (&lt;fml:DC_OPIS_ZABEZP_1_LINIA&gt;{ substring(data($req/m1:collateralDescription),1,60) }&lt;/fml:DC_OPIS_ZABEZP_1_LINIA&gt;,
                &lt;fml:DC_OPIS_ZABEZP_2_LINIA&gt;{ substring(data($req/m1:collateralDescription),61,60) }&lt;/fml:DC_OPIS_ZABEZP_2_LINIA&gt;,
                &lt;fml:DC_OPIS_ZABEZP_3_LINIA&gt;{ substring(data($req/m1:collateralDescription),121,60) }&lt;/fml:DC_OPIS_ZABEZP_3_LINIA&gt;)
          else ()
      }
      {if($req/m1:collateralValue and fn:string-length($req/m1:collateralValue)&gt;0)
          then &lt;fml:DC_WARTOSC_ZABEZPIECZENIA&gt;{ data($req/m1:collateralValue) }&lt;/fml:DC_WARTOSC_ZABEZPIECZENIA&gt;
          else ()
      }
      {if($req/m1:expirationOrMaturityDate and fn:string-length($req/m1:expirationOrMaturityDate)&gt;0)
          then &lt;fml:DC_DATA_WYGAS_LUB_ZAPADALNO&gt;{ xf:mapDate($req/m1:expirationOrMaturityDate) }&lt;/fml:DC_DATA_WYGAS_LUB_ZAPADALNO&gt;
          else ()
      }
      {if($req/m1:insuranceRequired and fn:string-length($req/m1:insuranceRequired)&gt;0)
          then &lt;fml:DC_WYMAGANE_UBEZPIECZENIE_T_N&gt;{ xf:booleanTo01($req/m1:insuranceRequired) }&lt;/fml:DC_WYMAGANE_UBEZPIECZENIE_T_N&gt;
          else ()
      }
      {if($req/m1:insuranceExpiryDate and fn:string-length($req/m1:insuranceExpiryDate)&gt;0)
          then &lt;fml:DC_DATA_WAZNOSCI_UBEZP&gt;{ xf:mapDate($req/m1:insuranceExpiryDate) }&lt;/fml:DC_DATA_WAZNOSCI_UBEZP&gt;
          else ()
      }
      {if($req/m1:legalClaimRegisteredDate and fn:string-length($req/m1:legalClaimRegisteredDate)&gt;0)
          then &lt;fml:DC_DATA_REJESTRACJI_ZABEZP&gt;{ xf:mapDate($req/m1:legalClaimRegisteredDate) }&lt;/fml:DC_DATA_REJESTRACJI_ZABEZP&gt;
          else ()
      }
      {if($req/m1:legalClaimExpiryDate and fn:string-length($req/m1:legalClaimExpiryDate)&gt;0)
          then &lt;fml:DC_DATA_WYGAS_REJESTRACJI&gt;{ xf:mapDate($req/m1:legalClaimExpiryDate) }&lt;/fml:DC_DATA_WYGAS_REJESTRACJI&gt;
          else ()
      }
      {if($req/m1:reviewFrequency and fn:string-length($req/m1:reviewFrequency)&gt;0)
          then &lt;fml:DC_CZESTOTL_WYCENY&gt;{ data($req/m1:reviewFrequency) }&lt;/fml:DC_CZESTOTL_WYCENY&gt;
          else ()
      }
      {if($req/m1:reviewPeriod and fn:string-length($req/m1:reviewPeriod)&gt;0)
          then &lt;fml:DC_OKRES_WYCENY_ZABEZP&gt;{ data($req/m1:reviewPeriod) }&lt;/fml:DC_OKRES_WYCENY_ZABEZP&gt;
          else ()
      }
      &lt;fml:DC_OKRESLONY_DZIEN_WYCENY&gt;00&lt;/fml:DC_OKRESLONY_DZIEN_WYCENY&gt;
      {if($req/m1:firstReviewDate and fn:string-length($req/m1:firstReviewDate)&gt;0)
          then &lt;fml:DC_DATA_PIERWSZEJ_WYCENY&gt;{ xf:mapDateTime($req/m1:firstReviewDate) }&lt;/fml:DC_DATA_PIERWSZEJ_WYCENY&gt;
          else ()
      }
      {if($req/m1:reviewComments and fn:string-length($req/m1:reviewComments)&gt;0)
          then &lt;fml:DC_KOMENTARZ_DLA_WYCENY&gt;{ data($req/m1:reviewComments) }&lt;/fml:DC_KOMENTARZ_DLA_WYCENY&gt;
          else ()
      }
      {if($req/m1:numberOfUnits and fn:string-length($req/m1:numberOfUnits)&gt;0)
          then &lt;fml:DC_LICZBA_JEDN_ZABEZP&gt;{ data($req/m1:numberOfUnits) }&lt;/fml:DC_LICZBA_JEDN_ZABEZP&gt;
          else ()
      }
      {if($req/m1:unitPrice and fn:string-length($req/m1:unitPrice)&gt;0)
          then &lt;fml:DC_WARTOSC_JEDNOSTKOWA&gt;{ data($req/m1:unitPrice) }&lt;/fml:DC_WARTOSC_JEDNOSTKOWA&gt;
          else ()
      }
      {if($req/m1:marginPercentage and fn:string-length($req/m1:marginPercentage)&gt;0)
          then &lt;fml:DC_PROCENT_POMNIEJSZ_REZERWY&gt;{ round-half-to-even(data($req/m1:marginPercentage)*100,4) }&lt;/fml:DC_PROCENT_POMNIEJSZ_REZERWY&gt;
          else ()
      }
      {if($req/m1:maximumCollateralValue and fn:string-length($req/m1:maximumCollateralValue)&gt;0)
          then &lt;fml:DC_MAKS_WARTOSC_ZABEZPIECZ&gt;{ data($req/m1:maximumCollateralValue) }&lt;/fml:DC_MAKS_WARTOSC_ZABEZPIECZ&gt;
          else ()
      }
      {if($req/m1:dateLastPriced and fn:string-length($req/m1:dateLastPriced)&gt;0)
          then &lt;fml:DC_DATA_POPRZEDNIEJ_WYCENY&gt;{ xf:mapDate($req/m1:dateLastPriced) }&lt;/fml:DC_DATA_POPRZEDNIEJ_WYCENY&gt;
          else ()
      }
      {if($req/m1:itemReferenceNumber and fn:string-length($req/m1:itemReferenceNumber)&gt;0)
          then &lt;fml:DC_NR_RACH_OBJETY_BLOKADA_SRO&gt;{ data($req/m1:itemReferenceNumber) }&lt;/fml:DC_NR_RACH_OBJETY_BLOKADA_SRO&gt;
          else ()
      }
      {if($req/m1:safekeepingReceiptNbr and fn:string-length($req/m1:safekeepingReceiptNbr)&gt;0)
          then &lt;fml:DC_INFORM_DODATK_O_ZABEZP&gt;{ data($req/m1:safekeepingReceiptNbr) }&lt;/fml:DC_INFORM_DODATK_O_ZABEZP&gt;
          else ()
      }
      {if($req/m1:shortDescription and fn:string-length($req/m1:shortDescription)&gt;0)
          then &lt;fml:DC_NAZWA_SKROCONA&gt;{ data($req/m1:shortDescription) }&lt;/fml:DC_NAZWA_SKROCONA&gt;
          else ()
      }
      {if($req/m1:userField6 and fn:string-length($req/m1:userField6)&gt;0)
          then &lt;fml:DC_NR_UMOWY_PORECZ&gt;{ data($req/m1:userField6) }&lt;/fml:DC_NR_UMOWY_PORECZ&gt;
          else ()
      }

      {for $it at $i in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:customer/m2:Customer/m2:customerNumber)
            then 
              if($it/m1:relationship = "SOW" or $it/m1:relationship = "JAF")
                then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($it/m1:customer/m2:Customer/m2:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA&gt;
                else ()
            else ()
      }

      {for $it in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:relationship and fn:string-length($it/m1:relationship)&gt;0)
            then (&lt;fml:DC_RELACJA&gt;{ data($it/m1:relationship) }&lt;/fml:DC_RELACJA&gt;,
                     &lt;fml:DC_TYP_RELACJI&gt;1&lt;/fml:DC_TYP_RELACJI&gt;)
            else ()
      }
      {for $it in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:customer/m2:Customer/m2:customerNumber and fn:string-length($it/m1:customer/m2:Customer/m2:customerNumber)&gt;0)
            then &lt;fml:DC_NUMER_KLIENTA_REL&gt;{ data($it/m1:customer/m2:Customer/m2:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA_REL&gt;
            else ()
      }

      {if($reqm/m1:shortLegalDescription and fn:string-length($reqm/m1:shortLegalDescription)&gt;0)
          then &lt;fml:DC_OPIS_SKROCONY_ZABEZP&gt;{ data($reqm/m1:shortLegalDescription) }&lt;/fml:DC_OPIS_SKROCONY_ZABEZP&gt;
          else ()
      }
      {if($reqm/m1:legalRegistrationNumber and fn:string-length($reqm/m1:legalRegistrationNumber)&gt;0)
          then &lt;fml:DC_NR_KSIEGI_WIECZYSTEJ_1&gt;{ data($reqm/m1:legalRegistrationNumber) }&lt;/fml:DC_NR_KSIEGI_WIECZYSTEJ_1&gt;
          else ()
      }
      {if($reqm/m1:yearBuilt and fn:string-length($reqm/m1:yearBuilt)&gt;0)
          then &lt;fml:DC_ROK_BUDOWY_ROK_ZAKUPU_BUD&gt;{ data($reqm/m1:yearBuilt) }&lt;/fml:DC_ROK_BUDOWY_ROK_ZAKUPU_BUD&gt;
          else ()
      }
      {if($reqm/m1:yearPurchased and fn:string-length($reqm/m1:yearPurchased)&gt;0)
          then &lt;fml:DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK&gt;{ data($reqm/m1:yearPurchased) }&lt;/fml:DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK&gt;
          else ()
      }
      {if($reqm/m1:purchasePrice and fn:string-length($reqm/m1:purchasePrice)&gt;0)
          then &lt;fml:DC_CENA_ZAKUPU&gt;{ data($reqm/m1:purchasePrice) }&lt;/fml:DC_CENA_ZAKUPU&gt;
          else ()
      }
      {if($reqm/m1:lotSize and fn:string-length($reqm/m1:lotSize)&gt;0)
          then &lt;fml:DC_POWIERZCHNIA_DZIALKI&gt;{ data($reqm/m1:lotSize) }&lt;/fml:DC_POWIERZCHNIA_DZIALKI&gt;
          else ()
      }
      {if($reqm/m1:squareFeet and fn:string-length($reqm/m1:squareFeet)&gt;0)
          then &lt;fml:DC_POWIERZCHNIA_BUDYNKU&gt;{ data($reqm/m1:squareFeet) }&lt;/fml:DC_POWIERZCHNIA_BUDYNKU&gt;
          else ()
      }
      {if($reqm/m1:numberOfResidents and fn:string-length($reqm/m1:numberOfResidents)&gt;0)
          then &lt;fml:DC_LICZBA_MIESZKANCOW&gt;{ data($reqm/m1:numberOfResidents) }&lt;/fml:DC_LICZBA_MIESZKANCOW&gt;
          else ()
      }
      {if($reqm/m1:leaseExpirationDate and fn:string-length($reqm/m1:leaseExpirationDate)&gt;0)
          then &lt;fml:DC_DATA_ZL_WN_O_WPIS_HIPO&gt;{ xf:mapDate($reqm/m1:leaseExpirationDate) }&lt;/fml:DC_DATA_ZL_WN_O_WPIS_HIPO&gt;
          else ()
      }
      {if($reqm/m1:dateInspected and fn:string-length($reqm/m1:dateInspected)&gt;0)
          then &lt;fml:DC_PRZEWIDYWANA_DATA_ZAK&gt;{ xf:mapDate($reqm/m1:dateInspected) }&lt;/fml:DC_PRZEWIDYWANA_DATA_ZAK&gt;
          else ()
      }
      {if($reqm/m1:subdivision and fn:string-length($reqm/m1:subdivision)&gt;0)
          then &lt;fml:DC_SAD_REJONOWY&gt;{ data($reqm/m1:subdivision) }&lt;/fml:DC_SAD_REJONOWY&gt;
          else ()
      }
      {if($reqm/m1:appraisedValue and fn:string-length($reqm/m1:appraisedValue)&gt;0)
          then &lt;fml:DC_SZACOWANA_WARTOSC&gt;{ data($reqm/m1:appraisedValue) }&lt;/fml:DC_SZACOWANA_WARTOSC&gt;
          else ()
      }
      {if($reqm/m1:appraisalDate and fn:string-length($reqm/m1:appraisalDate)&gt;0)
          then &lt;fml:DC_DATA_SPORZ_WYCENY&gt;{ xf:mapDate($reqm/m1:appraisalDate) }&lt;/fml:DC_DATA_SPORZ_WYCENY&gt;
          else ()
      }
      {if($reqm/m1:appraiserName and fn:string-length($reqm/m1:appraiserName)&gt;0)
          then &lt;fml:DC_NAZWISKO_RZECZOZNAWCY&gt;{ data($reqm/m1:appraiserName) }&lt;/fml:DC_NAZWISKO_RZECZOZNAWCY&gt;
          else ()
      }
      {if($reqm/m1:mortgagesPayablePrior and fn:string-length($reqm/m1:mortgagesPayablePrior)&gt;0)
          then &lt;fml:DC_WARTOSC_WPISU_DO_HIPOTEKI&gt;{ data($reqm/m1:mortgagesPayablePrior) }&lt;/fml:DC_WARTOSC_WPISU_DO_HIPOTEKI&gt;
          else ()
      }
      {if($reqm/m1:userField2 and fn:string-length($reqm/m1:userField2)&gt;0)
          then &lt;fml:DC_POTW_ZAK_BUDOWY&gt;{ data($reqm/m1:userField2) }&lt;/fml:DC_POTW_ZAK_BUDOWY&gt;
          else ()
      }
      {if($reqm/m1:userField3 and fn:string-length($reqm/m1:userField3)&gt;0)
          then &lt;fml:DC_ID_POLISY&gt;{ data($reqm/m1:userField3) }&lt;/fml:DC_ID_POLISY&gt;
          else ()
      }
      {if($reqm/m1:userField4 and fn:string-length($reqm/m1:userField4)&gt;0)
          then &lt;fml:DC_WNIOSEK_WYKRESL_HIPO&gt;{ data($reqm/m1:userField4) }&lt;/fml:DC_WNIOSEK_WYKRESL_HIPO&gt;
          else ()
      }
      {if($reqm/m1:userField5 and fn:string-length($reqm/m1:userField5)&gt;0)
          then &lt;fml:DC_DATA_PODPISANIA_AKTU_WLASNO&gt;{ xf:mapDate($reqm/m1:userField5) }&lt;/fml:DC_DATA_PODPISANIA_AKTU_WLASNO&gt;
          else ()
      }
      {if($reqm/m1:newAdditionalUserField2 and fn:string-length($reqm/m1:newAdditionalUserField2)&gt;0)
          then &lt;fml:DC_HIPOTEKA_USTANOW_PRZEZ_BANK&gt;{ data($reqm/m1:newAdditionalUserField2) }&lt;/fml:DC_HIPOTEKA_USTANOW_PRZEZ_BANK&gt;
          else ()
      }
     
      {if($reqm/m1:newAdditionalUserField3 and fn:string-length($reqm/m1:newAdditionalUserField3)&gt;0)
          then &lt;fml:DC_WNIOSEK_O_UTWORZENIE_KW&gt;{ xf:booleanToTN($reqm/m1:newAdditionalUserField3) }&lt;/fml:DC_WNIOSEK_O_UTWORZENIE_KW&gt;
          else ()
      }
      {if($reqm/m1:legalRegistrationDate and fn:string-length($reqm/m1:legalRegistrationDate)&gt;0)
          then &lt;fml:DC_NR_KSIEGI_WIECZYSTEJ_2&gt;{ xf:mapDate($reqm/m1:legalRegistrationDate) }&lt;/fml:DC_NR_KSIEGI_WIECZYSTEJ_2&gt;
          else ()
      }
      {if($reqm/m1:houseNumber and fn:string-length($reqm/m1:houseNumber)&gt;0)
          then &lt;fml:DC_NUMER_DOMU&gt;{ data($reqm/m1:houseNumber) }&lt;/fml:DC_NUMER_DOMU&gt;
          else ()
      }
      {if($reqm/m1:street and fn:string-length($reqm/m1:street)&gt;0)
          then &lt;fml:DC_ULICA&gt;{ data($reqm/m1:street) }&lt;/fml:DC_ULICA&gt;
          else ()
      }
      {if($reqm/m1:city and fn:string-length($reqm/m1:city)&gt;0)
          then &lt;fml:DC_MIASTO_CITY&gt;{ data($reqm/m1:city) }&lt;/fml:DC_MIASTO_CITY&gt;
          else ()
      }
      {if($reqm/m1:postalCode and fn:string-length($reqm/m1:postalCode)&gt;0)
          then &lt;fml:DC_KOD_POCZTOWY&gt;{ data($reqm/m1:postalCode) }&lt;/fml:DC_KOD_POCZTOWY&gt;
          else ()
      }
      {if($reqm/m1:country and fn:string-length($reqm/m1:country)&gt;0)
          then &lt;fml:DC_KRAJ&gt;{ data($reqm/m1:country) }&lt;/fml:DC_KRAJ&gt;
          else ()
      }
      {if($reqm/m1:district and fn:string-length($reqm/m1:district)&gt;0)
          then &lt;fml:DC_POWIAT&gt;{ data($reqm/m1:district) }&lt;/fml:DC_POWIAT&gt;
          else ()
      }
      {if($reqm/m1:county and fn:string-length($reqm/m1:county)&gt;0)
          then &lt;fml:DC_WOJEWODZTWO&gt;{ data($reqm/m1:county) }&lt;/fml:DC_WOJEWODZTWO&gt;
          else ()
      }
      {if($reqm/m1:userField1 and fn:string-length($reqm/m1:userField1)&gt;0)
          then &lt;fml:DC_DOSTAR_ODPIS_KW&gt;{ data($reqm/m1:userField1) }&lt;/fml:DC_DOSTAR_ODPIS_KW&gt;
          else ()
      }
      {if($reqm/m1:newUsedIndicatori/m3:NewUsedType/m3:newUsedType and fn:string-length($reqm/m1:newUsedIndicatori/m3:NewUsedType/m3:newUsedType)&gt;0)
          then &lt;fml:DC_STAN_NIERUCHOMOSCI&gt;{ data($reqm/m1:newUsedIndicatori/m3:NewUsedType/m3:newUsedType) }&lt;/fml:DC_STAN_NIERUCHOMOSCI&gt;
          else ()
      }
      {if($reqm/m1:propertyType/m3:PropertyType/m3:propertyType and fn:string-length($reqm/m1:propertyType/m3:PropertyType/m3:propertyType)&gt;0)
          then &lt;fml:DC_RODZAJ_NIERUCHOMOSCI&gt;{ data($reqm/m1:propertyType/m3:PropertyType/m3:propertyType) }&lt;/fml:DC_RODZAJ_NIERUCHOMOSCI&gt;
          else ()
      }
      {if($reqm/m1:ownerOccupiedCode/m3:OwnerOccupiedCode/m3:ownerOccupiedCode and fn:string-length($reqm/m1:ownerOccupiedCode/m3:OwnerOccupiedCode/m3:ownerOccupiedCode)&gt;0)
          then &lt;fml:DC_KOD_WLASCICIELA&gt;{ data($reqm/m1:ownerOccupiedCode/m3:OwnerOccupiedCode/m3:ownerOccupiedCode) }&lt;/fml:DC_KOD_WLASCICIELA&gt;
          else ()
      }
      {if($reqm/m1:tenure/m3:Tenure/m3:tenure and fn:string-length($reqm/m1:tenure/m3:Tenure/m3:tenure)&gt;0)
          then &lt;fml:DC_TYTUL_PRAWNY&gt;{ data($reqm/m1:tenure/m3:Tenure/m3:tenure) }&lt;/fml:DC_TYTUL_PRAWNY&gt;
          else ()
      }
      {if($reqm/m1:newAdditionalUserField1/m4:CollateralMortgageAddType/m4:collateralMortgageAddType and fn:string-length($reqm/m1:newAdditionalUserField1/m4:CollateralMortgageAddType/m4:collateralMortgageAddType)&gt;0)
          then &lt;fml:DC_HIPOTEKA_W_RAMACH_GUK &gt;{ data($reqm/m1:newAdditionalUserField1/m4:CollateralMortgageAddType/m4:collateralMortgageAddType) }&lt;/fml:DC_HIPOTEKA_W_RAMACH_GUK &gt;
          else ()
      }
      {if($reqm/m1:realEstateUserField1/m3:CollateralMortgagePosition/m3:collateralMortgagePosition and fn:string-length($reqm/m1:realEstateUserField1/m3:CollateralMortgagePosition/m3:collateralMortgagePosition)&gt;0)
          then &lt;fml:DC_HIPOTEKA_POZYCJA&gt;{ data($reqm/m1:realEstateUserField1/m3:CollateralMortgagePosition/m3:collateralMortgagePosition) }&lt;/fml:DC_HIPOTEKA_POZYCJA&gt;
          else ()
      }

      {if($req/m1:collateralCode/m3:CollateralCode/m3:collateralCode and fn:string-length($req/m1:collateralCode/m3:CollateralCode/m3:collateralCode)&gt;0)
          then &lt;fml:DC_KOD_ZABEZPIECZENIA&gt;{ data($req/m1:collateralCode/m3:CollateralCode/m3:collateralCode) }&lt;/fml:DC_KOD_ZABEZPIECZENIA&gt;
          else ()
      }
      {if($req/m1:currencyCode/m3:CurrencyCode/m3:currencyCode and fn:string-length($req/m1:currencyCode/m3:CurrencyCode/m3:currencyCode)&gt;0)
          then &lt;fml:DC_WALUTA&gt;{ data($req/m1:currencyCode/m3:CurrencyCode/m3:currencyCode) }&lt;/fml:DC_WALUTA&gt;
          else ()
      }
      {if($req/m1:methodOfSecurityValuation/m3:MethodOfSecurityValuation/m3:methodOfSecurityValuation and fn:string-length($req/m1:methodOfSecurityValuation/m3:MethodOfSecurityValuation/m3:methodOfSecurityValuation)&gt;0)
          then &lt;fml:DC_METODA_WYCENY_ZABEZP&gt;{ data($req/m1:methodOfSecurityValuation/m3:MethodOfSecurityValuation/m3:methodOfSecurityValuation) }&lt;/fml:DC_METODA_WYCENY_ZABEZP&gt;
          else ()
      }
      {if($req/m1:personValuatingASecurity/m3:PersonValuatingASecurity/m3:personValuatingASecurity and fn:string-length($req/m1:personValuatingASecurity/m3:PersonValuatingASecurity/m3:personValuatingASecurity)&gt;0)
          then &lt;fml:DC_OSOBA_WYCENIAJACA_ZABEZP&gt;{ data($req/m1:personValuatingASecurity/m3:PersonValuatingASecurity/m3:personValuatingASecurity) }&lt;/fml:DC_OSOBA_WYCENIAJACA_ZABEZP&gt;
          else ()
      }
      {if($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber and fn:string-length($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber)&gt;0)
          then &lt;fml:DC_BLOKADA_SRODK_NA_R_KU&gt;{ data($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber) }&lt;/fml:DC_BLOKADA_SRODK_NA_R_KU&gt;
          else ()
      }
      &lt;fml:DC_ENCODING&gt;PL_MZV&lt;/fml:DC_ENCODING&gt;
    &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>