<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:~
 : @author  Michal Fijas
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns="http://bzwbk.com/services/getUserTills";

declare variable $body as element(soap-env:Body) external;

(:~
 : @param $invoke operacja wejśiowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function local:getUserTillsRequest($invoke as element(ns:invoke))
    as element(FML32)
{
	&lt;FML32&gt;
    	&lt;NF_MSHEAD_MSGID?&gt;{ data($invoke/msgId) }&lt;/NF_MSHEAD_MSGID&gt;
		
		(: dane do stronicowania :)
        &lt;NF_PAGEC_ACTIONCODE?&gt;{ data($invoke/pageControl/actionCode) }&lt;/NF_PAGEC_ACTIONCODE&gt;
        &lt;NF_PAGEC_PAGESIZE?&gt;{ data($invoke/pageControl/pageSize) }&lt;/NF_PAGEC_PAGESIZE&gt;
        {
        	let $reverseOrder := data($invoke/pageControl/reverseOrder)
	        return 
            	if ($reverseOrder) then
		            &lt;NF_PAGEC_REVERSEORDER&gt;{			            	
		            	if ($reverseOrder eq "true") then
		            		'1'
		            	else
		            		'0'
		            }&lt;/NF_PAGEC_REVERSEORDER&gt;
				else ()
		}
        &lt;NF_PAGEC_NAVIGATIONKEYDEFI?&gt;{ data($invoke/pageControl/navigationKeyDefinition) }&lt;/NF_PAGEC_NAVIGATIONKEYDEFI&gt;
        &lt;NF_PAGEC_NAVIGATIONKEYVALU?&gt;{ data($invoke/pageControl/navigationKeyValue) }&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;
		
		(: dane filtrujące :)
		&lt;NF_APPF_SKP?&gt;{ data($invoke/skp) }&lt;/NF_APPF_SKP&gt;
        &lt;NF_BRANCC_BRANCHCODE?&gt;{ data($invoke/branchCode) }&lt;/NF_BRANCC_BRANCHCODE&gt;
		(:
		 : status stanowiska
		 :
		 : O --&gt; 1 (otwarte)
		 : C --&gt; 0 (zamknięte)
		 :)
        {
			let $sessionStatus := data($invoke/userTxnSessionStatus)
			return
	           	if ($sessionStatus) then
					&lt;NF_USETSS_USERTXNSESSIONST&gt;{
						if ($sessionStatus eq 'O') then
							1
						else
							0
					}&lt;/NF_USETSS_USERTXNSESSIONST&gt;
            	else ()
        }
        &lt;NF_TILL_TILLID?&gt;{ data($invoke/tillID) }&lt;/NF_TILL_TILLID&gt;
    &lt;/FML32&gt;
};

&lt;soap-env:Body&gt;{ local:getUserTillsRequest($body/ns:invoke) }&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>