<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-11-18</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace tem = "http://tempuri.org/";
declare namespace wcf="http://schemas.datacontract.org/2004/07/WcfServiceLibrary.Struktures";


declare function xf:map_manCustProdAddrRequest($fml as element(fml:FML32))
	as element(tem:manageCardProductAdr) {
		&lt;tem:manageCardProductAdr&gt;
			&lt;tem:input&gt;
				&lt;wcf:trnId&gt;{ data($fml/fml:DC_TRN_ID) }&lt;/wcf:trnId&gt;
				&lt;wcf:uzytkownik&gt;{ data($fml/fml:DC_UZYTKOWNIK) }&lt;/wcf:uzytkownik&gt;
				&lt;wcf:oddzial&gt;{ data($fml/fml:DC_ODDZIAL) }&lt;/wcf:oddzial&gt;
				&lt;wcf:typZmiany&gt;{ data($fml/fml:DC_TYP_ZMIANY) }&lt;/wcf:typZmiany&gt;
			 	&lt;wcf:numerKlienta&gt;{ data($fml/fml:DC_NUMER_KLIENTA) }&lt;/wcf:numerKlienta&gt;
  			&lt;/tem:input&gt;
		&lt;/tem:manageCardProductAdr&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:map_manCustProdAddrRequest($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>