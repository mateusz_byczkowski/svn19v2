<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-03-28</con:description>
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:cif.entities.be.dcl";
declare namespace pdk = "http://bzwbk.com/services/pdk/";

declare function local:plDate2xsdate($date as xs:string?) as xs:string? {
    if($date)
        then
            let $tokens := fn:tokenize($date, "-")
            return
                fn:concat($tokens[3], "-", $tokens[2], "-", $tokens[1])
        else ()
};

declare function local:invoke($out as element()) as element(soap-env:Body) {
   &lt;soap-env:Body&gt;
        &lt;urn:invokeResponse&gt;
            &lt;urn:processingAgree&gt;
                &lt;urn3:ProcessingAgree&gt;
                    &lt;urn3:agreementCancellingDate?&gt;{ local:plDate2xsdate(data($out/pdk:DataWycofania)) }&lt;/urn3:agreementCancellingDate&gt;
                    &lt;urn3:agreementValidityPeriod?&gt;{ data($out/pdk:OkresWaznosci) }&lt;/urn3:agreementValidityPeriod&gt;
                    &lt;urn3:agreementDate?&gt;{ local:plDate2xsdate(data($out/pdk:DataUdzielenia)) }&lt;/urn3:agreementDate&gt;
                    &lt;urn3:contractNumber?&gt;{ data($out/pdk:NrUmowy) }&lt;/urn3:contractNumber&gt;
                    &lt;urn3:contractOpenDate?&gt;{ local:plDate2xsdate(data($out/pdk:DataZawarciaUmowy)) }&lt;/urn3:contractOpenDate&gt;
                    &lt;urn3:agreementFlag?&gt;
                        &lt;urn2:CustomerAgreementStatus&gt;
                            &lt;urn2:customerAgreementStatus&gt;{ data($out/pdk:FlagaZgody) }&lt;/urn2:customerAgreementStatus&gt;
                        &lt;/urn2:CustomerAgreementStatus&gt;
                    &lt;/urn3:agreementFlag&gt;
                &lt;/urn3:ProcessingAgree&gt;
            &lt;/urn:processingAgree&gt;
        &lt;/urn:invokeResponse&gt;
   &lt;/soap-env:Body&gt;
};

declare variable $body as element(soap-env:Body) external;

local:invoke($body/pdk:GetArt105DataResponse/pdk:GetArt105DataResult/pdk:OutGetArt105Data[1])</con:xquery>
</con:xqueryEntry>