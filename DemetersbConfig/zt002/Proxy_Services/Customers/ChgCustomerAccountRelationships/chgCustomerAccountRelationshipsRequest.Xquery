<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function shortAccountNbr($account as xs:string) as xs:string
{        
    if (string-length($account) &gt;  10) 
           then  substring($account, string-length($account)- 9,10)
    else   $account
};


declare function insertPrecentage($value as xs:string?, $rel as xs:string) as xs:string
(:declare function insertPrecentage($value as xs:string, $rel as xs:string) as xs:string:)
{
      if (($value) and (string-length($value)&gt;0)) then
(:      if (string-length($value)&gt;0) then:)
          $value
      else if (($rel ="SOW") or ($rel ="JAF")) 
          then "100"
     else "0"
};

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:mapRow($numerKlienta as xs:string,
						   $typRelacji as xs:string,
						   $procentZobPodatkowych as xs:string?) as element()*{	   
(:						   $procentZobPodatkowych as xs:string) as element()*{	   :)
           
	  &lt;fml:DC_NUMER_KLIENTA&gt;{$numerKlienta}&lt;/fml:DC_NUMER_KLIENTA&gt;,
          &lt;fml:DC_RELACJA&gt;{$typRelacji }&lt;/fml:DC_RELACJA&gt;,
           &lt;fml:DC_TYP_RELACJI&gt;1&lt;/fml:DC_TYP_RELACJI&gt;,
	  &lt;fml:DC_PROCENT_ZOB_PODATKOWYCH?&gt;{insertPrecentage($procentZobPodatkowych, $typRelacji)}&lt;/fml:DC_PROCENT_ZOB_PODATKOWYCH&gt; 
};


declare function xf:mapNullRow() as element()*{	   
      &lt;fml:DC_NUMER_KLIENTA nil="true"/&gt;,
      &lt;fml:DC_RELACJA nil="true"/&gt;,
      &lt;fml:DC_TYP_RELACJI nil="true"/&gt;, 
      &lt;fml:DC_PROCENT_ZOB_PODATKOWYCH nil="true"/&gt;
};

declare function mapPackage($input as element(ns2:Customer), $number as xs:integer) as element()*{

       if ($number &lt;= 7) then
        if (data($input)) then
			xf:mapRow(data($input/ns2:customerNumber),
			data($input/ns2:accountRelationshipList/ns2:AccountRelationship/ns2:relationship/ns3:CustomerAccountRelationship/ns3:customerAccountRelationship),
			data($input/ns2:accountRelationshipList/ns2:AccountRelationship/ns2:account/ns1:Account/ns1:tranAccount/ns1:TranAccount/ns1:tax/ns1:Tax/ns1:taxPercentage))
        else
			xf:mapNullRow()
        else()
};

declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{
   &lt;DC_NR_RACHUNKU?&gt;{shortAccountNbr(data($parm/ns6:account/ns1:Account/ns1:accountNumber))}&lt;/DC_NR_RACHUNKU&gt;,
   &lt;DC_KOD_APLIKACJI&gt;0&lt;/DC_KOD_APLIKACJI&gt;,
   for $i in (1 to count($parm/ns6:customers/ns2:Customer)) 
     return 
        mapPackage($parm/ns6:customers/ns2:Customer[$i], $i)

};

declare function mapPackageNew($input as element(ns2:AccountRelationship), $number as xs:integer) as element()*{

       if ($number &lt;= 7) then
        if (data($input)) then
            xf:mapRow(data($input/ns2:customer/ns2:Customer/ns2:customerNumber),
            data($input/ns2:relationship/ns3:CustomerAccountRelationship/ns3:customerAccountRelationship),
            data($input/ns2:account/ns1:Account/ns1:tranAccount/ns1:TranAccount/ns1:tax/ns1:Tax/ns1:taxPercentage))
(:            data($input/ns2:account/ns1:Account/ns1:tranAccount/ns1:TranAccount/ns1:tax/ns1:Tax/ns1:taxPercentage)):)
        else
            xf:mapNullRow()
        else()
};

declare function getFieldsFromInvokeNew($parm as element(ns6:invoke)) as element()*
{  
   &lt;DC_NR_RACHUNKU?&gt;{shortAccountNbr(data($parm/ns6:account/ns1:Account/ns1:accountNumber))}&lt;/DC_NR_RACHUNKU&gt;,
   &lt;DC_KOD_APLIKACJI&gt;0&lt;/DC_KOD_APLIKACJI&gt;,
   for $i in (1 to count($parm/ns6:account/ns1:Account/ns1:accountRelationshipList/ns2:AccountRelationship))
     return 
        mapPackageNew($parm/ns6:account/ns1:Account/ns1:accountRelationshipList/ns2:AccountRelationship[$i], $i)

};



declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{

(: &lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns6:msgHeader/ns6:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns6:msgHeader/ns6:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
, :)
&lt;DC_ODDZIAL?&gt;{chkUnitId(data($parm/ns6:msgHeader/ns6:unitId))}&lt;/DC_ODDZIAL&gt;
,
&lt;DC_UZYTKOWNIK?&gt;{concat("SKP:", data($parm/ns6:msgHeader/ns6:userId))}&lt;/DC_UZYTKOWNIK&gt;
,
(: &lt;NF_MSHEAD_APPID?&gt;{data($parm/ns6:msgHeader/ns6:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns6:msgHeader/ns6:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
, :)
&lt;DC_TRN_ID?&gt;{data($parm/ns6:transHeader/ns6:transId)}&lt;/DC_TRN_ID&gt;
};



&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvokeNew($body/ns6:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>