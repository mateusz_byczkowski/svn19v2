<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";

declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="urn:application.dictionaries.be.dcl";

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap:Body) external;

declare variable $header as element(soap:Header) external;


declare function strToBoolean($dateIn as xs:string) as xs:boolean {
  if ($dateIn = "1") 
    then true()
    else false()
};


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns0:invokeResponse?&gt;
   &lt;ns0:appYesNo&gt;&lt;ns1:AppYesNo&gt;
     &lt;ns1:appYesNo&gt;{ strToBoolean($parm/DC_STATUS_RACHUNKU) }&lt;/ns1:appYesNo&gt;
  &lt;/ns1:AppYesNo&gt;
&lt;/ns0:appYesNo&gt;&lt;/ns0:invokeResponse&gt;

};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>