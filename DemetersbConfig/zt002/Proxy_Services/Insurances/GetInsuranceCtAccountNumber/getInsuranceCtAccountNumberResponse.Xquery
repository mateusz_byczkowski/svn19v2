<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:insurance.entities.be.dcl";
declare namespace ns5="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns5:invokeResponse>
  <ns5:response>
    <ns0:ResponseMessage>
      <ns0:result?>{sourceValue2Boolean(data($parm/NF_RESPOM_RESULT),"1")}</ns0:result>
      <ns0:errorCode?>{data($parm/NF_RESPOM_ERRORCODE)}</ns0:errorCode>
      <ns0:errorDescription?>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}</ns0:errorDescription>
    </ns0:ResponseMessage>
  </ns5:response>
  <ns5:account>
    <ns1:Account>
      <ns1:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER)}</ns1:accountNumber>
    </ns1:Account>
  </ns5:account>
</ns5:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>