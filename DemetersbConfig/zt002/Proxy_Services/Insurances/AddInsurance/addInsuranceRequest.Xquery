<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:insurance.entities.be.dcl";
declare namespace ns7="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if($parm = "1")
       then $trueval
    else $falseval
};

declare function xf:getFields($parm as element(ns7:invoke), $msghead as element(ns7:msgHeader), $tranhead as element(ns7:transHeader)) as element(fml:FML32)
{
  let $msgId:= $msghead/ns7:msgId
  let $companyId:= $msghead/ns7:companyId
  let $userId := $msghead/ns7:userId
  let $appId:= $msghead/ns7:appId
  let $unitId := $msghead/ns7:unitId
  let $timestamp:= $msghead/ns7:timestamp

  let $transId:=$tranhead/ns7:transId

  return
  &lt;fml:FML32&gt;
     &lt;DC_TRN_ID?&gt;{data($transId)}&lt;/DC_TRN_ID&gt;
     &lt;DC_UZYTKOWNIK?&gt;{concat("SKP:",data($userId))}&lt;/DC_UZYTKOWNIK&gt;
     &lt;DC_ODDZIAL?&gt;{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL&gt;
      {
      if (data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:linkedAccount)) then
     &lt;DC_IS_RACH_POWIAZANY?&gt;{cutStr(data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:linkedAccount),12)}&lt;/DC_IS_RACH_POWIAZANY&gt;
     else ()
      } 
     &lt;DC_IS_DATA_FAKTYCZNA_POLISY?&gt;{data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:actualDate)}&lt;/DC_IS_DATA_FAKTYCZNA_POLISY&gt;
     &lt;DC_IS_DATA_URUCHOMIENIA_UBEZP?&gt;{data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:startDate)}&lt;/DC_IS_DATA_URUCHOMIENIA_UBEZP&gt;
	 &lt;DC_IS_NUMER_POLISY?&gt;{data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:policyNum)}&lt;/DC_IS_NUMER_POLISY&gt;
     &lt;DC_DATA_WYGASNIECIA?&gt;{data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:validityDate)}&lt;/DC_DATA_WYGASNIECIA&gt;
     &lt;DC_IS_STALA_SKLADKA_UBEZP?&gt;{data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:fixedPremium)}&lt;/DC_IS_STALA_SKLADKA_UBEZP&gt;
     &lt;DC_IS_DATA_NALEZNOSCI_SKLADKI?&gt;{data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:premiumDueDate)}&lt;/DC_IS_DATA_NALEZNOSCI_SKLADKI&gt;
     &lt;DC_IS_CZESTOSC_SKLADKI?&gt;{data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:premiumFrequency)}&lt;/DC_IS_CZESTOSC_SKLADKI&gt;
     &lt;DC_IS_OSOBA_SPRZEDAJACA_PRODU?&gt;{data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:saleEmployeeID)}&lt;/DC_IS_OSOBA_SPRZEDAJACA_PRODU&gt;
     {
       if (data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:mandatoryFlag)) then
     &lt;DC_IS_UBEZPIECZENIE_OBOWIAZKO?&gt;{boolean2SourceValue(data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:mandatoryFlag),"T","N")}&lt;/DC_IS_UBEZPIECZENIE_OBOWIAZKO&gt;
      else()
     }
     &lt;DC_IS_OKRES_SKLADKI?&gt;{data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:premiumPeriod/ns4:Period/ns4:period)}&lt;/DC_IS_OKRES_SKLADKI&gt;
     &lt;DC_IS_NIETYPOWY_DZIEN_SKLADKI?&gt;{data($parm/ns7:insurancePolicy/ns6:InsurancePolicyAcc/ns6:oddPremiumDay/ns4:SpecialDay/ns4:specialDay)}&lt;/DC_IS_NIETYPOWY_DZIEN_SKLADKI&gt;
     {
       if(data($parm/ns7:account/ns1:Account/ns1:accountNumber))  then
         &lt;DC_IS_NR_RACHUNKU_W_SYSTEMIE?&gt;{cutStr(data($parm/ns7:account/ns1:Account/ns1:accountNumber),12)}&lt;/DC_IS_NR_RACHUNKU_W_SYSTEMIE&gt;
       else()
     }
     &lt;DC_IS_TYP_PRODUKTU_UBEZP?&gt;{data($parm/ns7:account/ns1:Account/ns1:productCode)}&lt;/DC_IS_TYP_PRODUKTU_UBEZP&gt;
     &lt;DC_NUMER_KLIENTA?&gt;{data($parm/ns7:customer/ns2:Customer/ns2:customerNumber)}&lt;/DC_NUMER_KLIENTA&gt;
  &lt;/fml:FML32&gt;

};


&lt;soap:Body&gt;
     { xf:getFields($body/ns7:invoke, $header/ns7:header/ns7:msgHeader, $header/ns7:header/ns7:transHeader)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>