<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:mapbICBSAddDebitCardRequest($req as element(m:bICBSAddDebitCardRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:TrnId)
					then &lt;fml:DC_TRN_ID&gt;{ data($req/m:TrnId) }&lt;/fml:DC_TRN_ID&gt;
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK&gt;{ data($req/m:Uzytkownik) }&lt;/fml:DC_UZYTKOWNIK&gt;
					else ()
			}
			{
				if($req/m:Oddzial)
					then &lt;fml:DC_ODDZIAL&gt;{ chkUnitId(data($req/m:Oddzial)) }&lt;/fml:DC_ODDZIAL&gt;
					else ()
			}
			{
				if($req/m:NrKartyBin)
					then &lt;fml:DC_NR_KARTY_BIN&gt;{ data($req/m:NrKartyBin) }&lt;/fml:DC_NR_KARTY_BIN&gt;
					else ()
			}
			{
				if($req/m:TypKarty)
					then &lt;fml:DC_TYP_KARTY&gt;{ data($req/m:TypKarty) }&lt;/fml:DC_TYP_KARTY&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:Rachunek)
					then &lt;fml:DC_RACHUNEK&gt;{ data($req/m:Rachunek) }&lt;/fml:DC_RACHUNEK&gt;
					else ()
			}
			{
				if($req/m:WytloczoneNaKarcieI)
					then &lt;fml:DC_WYTLOCZONE_NA_KARCIE_I&gt;{ data($req/m:WytloczoneNaKarcieI) }&lt;/fml:DC_WYTLOCZONE_NA_KARCIE_I&gt;
					else ()
			}
			{
				if($req/m:WytloczoneNaKarcieIi)
					then &lt;fml:DC_WYTLOCZONE_NA_KARCIE_II&gt;{ data($req/m:WytloczoneNaKarcieIi) }&lt;/fml:DC_WYTLOCZONE_NA_KARCIE_II&gt;
					else ()
			}
			{
				if($req/m:IdentyfikatorPin)
					then &lt;fml:DC_IDENTYFIKATOR_PIN&gt;{ data($req/m:IdentyfikatorPin) }&lt;/fml:DC_IDENTYFIKATOR_PIN&gt;
					else ()
			}
			{
				if($req/m:ZalozycPolise)
					then &lt;fml:DC_ZALOZYC_POLISE&gt;{ data($req/m:ZalozycPolise) }&lt;/fml:DC_ZALOZYC_POLISE&gt;
					else ()
			}
			{
				if($req/m:LimitKarty)
					then &lt;fml:DC_LIMIT_KARTY&gt;{ data($req/m:LimitKarty) }&lt;/fml:DC_LIMIT_KARTY&gt;
					else ()
			}
			{
				if($req/m:StatusUmowy)
					then &lt;fml:DC_STATUS_UMOWY&gt;{ data($req/m:StatusUmowy) }&lt;/fml:DC_STATUS_UMOWY&gt;
					else ()
			}
			{
				if($req/m:TerminalId)
					then &lt;fml:DC_TERMINAL_ID&gt;{ data($req/m:TerminalId) }&lt;/fml:DC_TERMINAL_ID&gt;
					else ()
			}
			{
				if($req/m:UmorzycOplate)
					then &lt;fml:DC_UMORZYC_OPLATE&gt;{ data($req/m:UmorzycOplate) }&lt;/fml:DC_UMORZYC_OPLATE&gt;
					else ()
			}
			{
				if($req/m:CyklZestawien)
					then &lt;fml:DC_CYKL_ZESTAWIEN&gt;{ data($req/m:CyklZestawien) }&lt;/fml:DC_CYKL_ZESTAWIEN&gt;
					else ()
			}
			{
				if($req/m:Ekspres)
					then &lt;fml:DC_EKSPRES&gt;{ data($req/m:Ekspres) }&lt;/fml:DC_EKSPRES&gt;
					else ()
			}
			{
				if($req/m:TypAdresu)
					then &lt;fml:DC_TYP_ADRESU&gt;{ data($req/m:TypAdresu) }&lt;/fml:DC_TYP_ADRESU&gt;
					else ()
			}
			{
				if($req/m:Promocja)
					then &lt;fml:DC_PROMOCJA&gt;{ data($req/m:Promocja) }&lt;/fml:DC_PROMOCJA&gt;
					else ()
			}
			{
				if($req/m:ZestZbiorcze)
					then &lt;fml:DC_ZEST_ZBIORCZE&gt;{ data($req/m:ZestZbiorcze) }&lt;/fml:DC_ZEST_ZBIORCZE&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapbICBSAddDebitCardRequest($body/m:bICBSAddDebitCardRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>