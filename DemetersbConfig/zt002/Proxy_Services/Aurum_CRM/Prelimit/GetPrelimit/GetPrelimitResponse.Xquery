<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/prl/messages/";
declare namespace xf = "http://bzwbk.com/services/prl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapGetPrelimitResponse($fml as element(fml:FML32))
	as element(m:GetPrelimitResponse) {
		<m:GetPrelimitResponse>
			{
				let $DC_FLAGA_PRL := $fml/fml:DC_FLAGA_PRL
				let $DC_KWOTA_PRELIMITU := $fml/fml:DC_KWOTA_PRELIMITU
				let $DC_MIN_CENA := $fml/fml:DC_MIN_CENA
				let $DC_NR_RACHUNKU := $fml/fml:DC_NR_RACHUNKU
				let $DC_LIMIT_BIEZ := $fml/fml:DC_LIMIT_BIEZ
				let $DC_DATA_KARENCJI := $fml/fml:DC_DATA_KARENCJI
				let $DC_KOD_WYKLUCZENIA := $fml/fml:DC_KOD_WYKLUCZENIA
				let $DC_PRAWDOPODOBIENSTWO := $fml/fml:DC_PRAWDOPODOBIENSTWO
				let $DC_FLAGA_MARKETINGU := $fml/fml:DC_FLAGA_MARKETINGU
				let $DC_FLAGA_KLIENTA := $fml/fml:DC_FLAGA_KLIENTA
				let $DC_DATA_WAZNOSCI := $fml/fml:DC_DATA_WAZNOSCI
				let $DC_PRAWDOPOD_OPIS := $fml/fml:DC_PRAWDOPOD_OPIS
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL
				let $DC_NR_DOWODU_REGON := $fml/fml:DC_NR_DOWODU_REGON
				let $DC_NUMER_PASZPORTU := $fml/fml:DC_NUMER_PASZPORTU
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA
				let $DC_STATUS_KLIENTA := $fml/fml:DC_STATUS_KLIENTA
				let $DC_SUGEROWANA_CENA := $fml/fml:DC_SUGEROWANA_CENA
				let $DC_MAKS_CENA := $fml/fml:DC_MAKS_CENA
				for $it at $p in $fml/fml:DC_FLAGA_PRL
				return
					<m:GetPrelimitPrelimit>
                                                { if($DC_FLAGA_PRL[$p] = "LEAM")
                                                       then <m:FlagaPrl>LEXM</m:FlagaPrl>
                                                       else <m:FlagaPrl>{ data($DC_FLAGA_PRL[$p]) }</m:FlagaPrl>
					        }
						<m:KwotaPrelimitu?>{ data($DC_KWOTA_PRELIMITU[$p]) }</m:KwotaPrelimitu>
						<m:MinCena?>{ data($DC_MIN_CENA[$p]) }</m:MinCena>
						<m:NrRachunku?>{ data($DC_NR_RACHUNKU[$p]) }</m:NrRachunku>
						<m:LimitBiez?>{ data($DC_LIMIT_BIEZ[$p]) }</m:LimitBiez>
						<m:DataKarencji?>{ data($DC_DATA_KARENCJI[$p]) }</m:DataKarencji>
						<m:KodWykluczenia?>{ data($DC_KOD_WYKLUCZENIA[$p]) }</m:KodWykluczenia>
						<m:Prawdopodobienstwo?>{ data($DC_PRAWDOPODOBIENSTWO[$p]) }</m:Prawdopodobienstwo>
						<m:FlagaMarketingu?>{ data($DC_FLAGA_MARKETINGU[$p]) }</m:FlagaMarketingu>
						<m:FlagaKlienta?>{ data($DC_FLAGA_KLIENTA[$p]) }</m:FlagaKlienta>
						<m:DataWaznosci?>{ data($DC_DATA_WAZNOSCI[$p]) }</m:DataWaznosci>
						<m:PrawdopodOpis?>{ data($DC_PRAWDOPOD_OPIS[$p]) }</m:PrawdopodOpis>
						<m:NrPesel?>{ data($DC_NR_PESEL[$p]) }</m:NrPesel>
						<m:NrDowoduRegon?>{ data($DC_NR_DOWODU_REGON[$p]) }</m:NrDowoduRegon>
						<m:NumerPaszportu?>{ data($DC_NUMER_PASZPORTU[$p]) }</m:NumerPaszportu>
						<m:TypKlienta?>{ data($DC_TYP_KLIENTA[$p]) }</m:TypKlienta>
						<m:StatusKlienta?>{ data($DC_STATUS_KLIENTA[$p]) }</m:StatusKlienta>
						<m:SugerowanaCena?>{ data($DC_SUGEROWANA_CENA[$p]) }</m:SugerowanaCena>
						<m:MaksCena?>{ data($DC_MAKS_CENA[$p]) }</m:MaksCena>
					</m:GetPrelimitPrelimit>
			}

		</m:GetPrelimitResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapGetPrelimitResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>