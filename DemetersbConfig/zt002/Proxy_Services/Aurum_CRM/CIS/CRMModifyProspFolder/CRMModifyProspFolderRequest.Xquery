<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>TEET 52630 - dodanie mapowań:
MainIcbsNo -&gt; CI_NR_ODDZIALU,
OperationIcbsNo -&gt; CI_ODDZIAL_OBSLUGUJACY,
RetireInsurerName -&gt; CI_UBEZP_EMERYTALNE,
FolderModificationDate -&gt; CI_DATA_MODYFIKACJI.</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMModProspFolderRequest($req as element(m:CRMModifyProspFolderRequest))
as element(fml:FML32) {
&lt;fml:FML32&gt;
{
if ($req/m:CustId) then
&lt;fml:CI_NUMER_KLIENTA&gt;{data($req/m:CustId)}&lt;/fml:CI_NUMER_KLIENTA&gt;
else ()
}
{
if($req/m:CompanyId)
then
if(data($req/m:CompanyId))
then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:CompanyId) }&lt;/fml:CI_ID_SPOLKI&gt;
else &lt;fml:CI_ID_SPOLKI&gt;{0}&lt;/fml:CI_ID_SPOLKI&gt;
else ()
}
{
if ($req/m:MarketingAgree) then
&lt;fml:CI_UDOSTEP_GRUPA&gt;{data($req/m:MarketingAgree)}&lt;/fml:CI_UDOSTEP_GRUPA&gt;
else ()
}
{
if ($req/m:InfoSaleAgree) then
&lt;fml:CI_ZGODA_INF_HANDLOWA&gt;{data($req/m:InfoSaleAgree)}&lt;/fml:CI_ZGODA_INF_HANDLOWA&gt;
else ()
}
{
if ($req/m:CustName) then
&lt;fml:CI_NAZWA_PELNA&gt;{data($req/m:CustName)}&lt;/fml:CI_NAZWA_PELNA&gt;
else ()
}
{
if ($req/m:CustAddressStreet) then
&lt;fml:DC_ULICA_DANE_PODST&gt;{data($req/m:CustAddressStreet)}&lt;/fml:DC_ULICA_DANE_PODST&gt;
else ()
}
{
if ($req/m:CustAddressHouseFlatNo) then
&lt;fml:DC_NR_POSES_LOKALU_DANE_PODST&gt;{data($req/m:CustAddressHouseFlatNo)}&lt;/fml:DC_NR_POSES_LOKALU_DANE_PODST&gt;
else ()
}
{
if ($req/m:CustAddressPostCode) then
&lt;fml:DC_KOD_POCZTOWY_DANE_PODST&gt;{data($req/m:CustAddressPostCode)}&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST&gt;
else ()
}
{
if ($req/m:CustAddressCity) then
&lt;fml:DC_MIASTO_DANE_PODST&gt;{data($req/m:CustAddressCity)}&lt;/fml:DC_MIASTO_DANE_PODST&gt;
else ()
}
{
if ($req/m:CustAddressCounty) then
&lt;fml:DC_WOJ_KRAJ_DANE_PODST&gt;{data($req/m:CustAddressCounty)}&lt;/fml:DC_WOJ_KRAJ_DANE_PODST&gt;
else ()
}
{
if ($req/m:CustAddressCountry) then
&lt;fml:CI_KOD_KRAJU&gt;{data($req/m:CustAddressCountry)}&lt;/fml:CI_KOD_KRAJU&gt;
else ()
}
{
if($req/m:CustAddressForFlag)
then
if(data($req/m:CustAddressForFlag))
then &lt;fml:DC_TYP_ADRESU&gt;{ data($req/m:CustAddressForFlag) }&lt;/fml:DC_TYP_ADRESU&gt;
else &lt;fml:DC_TYP_ADRESU&gt;{0}&lt;/fml:DC_TYP_ADRESU&gt;
else ()
}
{
if ($req/m:ResidentFlag) then
&lt;fml:DC_REZ_NIEREZ&gt;{data($req/m:ResidentFlag)}&lt;/fml:DC_REZ_NIEREZ&gt;
else ()
}
{
if ($req/m:Email) then
&lt;fml:DC_ADRES_E_MAIL&gt;{data($req/m:Email)}&lt;/fml:DC_ADRES_E_MAIL&gt;
else ()
}
{
if ($req/m:PhoneNo) then
&lt;fml:DC_NR_TELEFONU&gt;{data($req/m:PhoneNo)}&lt;/fml:DC_NR_TELEFONU&gt;
else ()
}
{
if ($req/m:FaxNo) then
&lt;fml:DC_NUMER_FAKSU&gt;{data($req/m:FaxNo)}&lt;/fml:DC_NUMER_FAKSU&gt;
else ()
}
{
if ($req/m:MobileNo) then
&lt;fml:DC_NR_TELEF_KOMORKOWEGO&gt;{data($req/m:MobileNo)}&lt;/fml:DC_NR_TELEF_KOMORKOWEGO&gt;
else ()
}
{
if ($req/m:BusinessPhoneNo) then
&lt;fml:DC_NUMER_TELEFONU_PRACA&gt;{data($req/m:BusinessPhoneNo)}&lt;/fml:DC_NUMER_TELEFONU_PRACA&gt;
else ()
}
{
if ($req/m:CustIdCardNo) then
&lt;fml:DC_NR_DOWODU_REGON&gt;{data($req/m:CustIdCardNo)}&lt;/fml:DC_NR_DOWODU_REGON&gt;
else ()
}
{
if ($req/m:RegonNo) then
&lt;fml:DC_NR_DOWODU_REGON&gt;{data($req/m:RegonNo)}&lt;/fml:DC_NR_DOWODU_REGON&gt;
else ()
}
{
if ($req/m:CustPesel) then
&lt;fml:DC_NR_PESEL&gt;{data($req/m:CustPesel)}&lt;/fml:DC_NR_PESEL&gt;
else ()
}
{
if ($req/m:NipNo) then
&lt;fml:DC_NIP&gt;{data($req/m:NipNo)}&lt;/fml:DC_NIP&gt;
else ()
}
{
if ($req/m:NipGrantDate) then
&lt;fml:CI_DATA_NIP&gt;{data($req/m:NipGrantDate)}&lt;/fml:CI_DATA_NIP&gt;
else ()
}
{
if($req/m:Spw)
then
if(data($req/m:Spw))
then &lt;fml:DC_SPW&gt;{ data($req/m:Spw) }&lt;/fml:DC_SPW&gt;
else &lt;fml:DC_SPW&gt;{0}&lt;/fml:DC_SPW&gt;
else ()
}
{
if ($req/m:SignCardIcbsNo) then
&lt;fml:DC_KARTA_WZOROW_PODPISOW&gt;{data($req/m:SignCardIcbsNo)}&lt;/fml:DC_KARTA_WZOROW_PODPISOW&gt;
else ()
}
{
if ($req/m:DataSrc) then
&lt;fml:DC_ZRODLO_DANYCH&gt;{data($req/m:DataSrc)}&lt;/fml:DC_ZRODLO_DANYCH&gt;
else ()
}
{
if ($req/m:CorpoUnitId) then
&lt;fml:DC_JEDNOSTKA_KORPORACYJNA&gt;{data($req/m:CorpoUnitId)}&lt;/fml:DC_JEDNOSTKA_KORPORACYJNA&gt;
else ()
}
{
if ($req/m:TaxPercentage) then
&lt;fml:CI_PROCENT_PODATKU&gt;{data($req/m:TaxPercentage)}&lt;/fml:CI_PROCENT_PODATKU&gt;
else ()
}
{
if ($req/m:OtherBankPrds) then
&lt;fml:CI_PROD_INNE_BANKI&gt;{data($req/m:OtherBankPrds)}&lt;/fml:CI_PROD_INNE_BANKI&gt;
else ()
}
{
if ($req/m:NiNo) then
&lt;fml:CI_SERIA_NR_DOK&gt;{data($req/m:NiNo)}&lt;/fml:CI_SERIA_NR_DOK&gt;
else ()
}
{
if ($req/m:TypNiNo) then
&lt;fml:CI_DOK_TOZSAMOSCI&gt;{data($req/m:TypNiNo)}&lt;/fml:CI_DOK_TOZSAMOSCI&gt;
else ()
}
{
if ($req/m:CustPassportNo) then
&lt;fml:DC_NUMER_PASZPORTU&gt;{data($req/m:CustPassportNo)}&lt;/fml:DC_NUMER_PASZPORTU&gt;
else ()
}
{
if ($req/m:CarOwnerFlag) then
&lt;fml:CI_POSIADA_SAMOCHOD&gt;{data($req/m:CarOwnerFlag)}&lt;/fml:CI_POSIADA_SAMOCHOD&gt;
else ()
}
{
if ($req/m:BankRelative) then
&lt;fml:DC_PRACOWNIK_BANKU&gt;{data($req/m:BankRelative)}&lt;/fml:DC_PRACOWNIK_BANKU&gt;
else ()
}
{
if ($req/m:CustSex) then
&lt;fml:DC_PLEC&gt;{data($req/m:CustSex)}&lt;/fml:DC_PLEC&gt;
else ()
}
{
if ($req/m:CustFirstName) then
&lt;fml:DC_IMIE&gt;{data($req/m:CustFirstName)}&lt;/fml:DC_IMIE&gt;
else ()
}
{
if ($req/m:CustSecondName) then
&lt;fml:DC_DRUGIE_IMIE&gt;{data($req/m:CustSecondName)}&lt;/fml:DC_DRUGIE_IMIE&gt;
else ()
}
{
if ($req/m:CustSurname) then
&lt;fml:DC_NAZWISKO&gt;{data($req/m:CustSurname)}&lt;/fml:DC_NAZWISKO&gt;
else ()
}
{
if ($req/m:FatherName) then
&lt;fml:DC_IMIE_OJCA&gt;{data($req/m:FatherName)}&lt;/fml:DC_IMIE_OJCA&gt;
else ()
}
{
if ($req/m:MotherMaidenName) then
&lt;fml:DC_NAZWISKO_PANIENSKIE_MATKI&gt;{data($req/m:MotherMaidenName)}&lt;/fml:DC_NAZWISKO_PANIENSKIE_MATKI&gt;
else ()
}
{
if ($req/m:Nationality) then
&lt;fml:CI_OBYWATELSTWO&gt;{data($req/m:Nationality)}&lt;/fml:CI_OBYWATELSTWO&gt;
else ()
}
{
if ($req/m:CustBirthDate) then
&lt;fml:DC_DATA_URODZENIA&gt;{data($req/m:CustBirthDate)}&lt;/fml:DC_DATA_URODZENIA&gt;
else ()
}
{
if ($req/m:CustBirthPlace) then
&lt;fml:DC_MIEJSCE_URODZENIA&gt;{data($req/m:CustBirthPlace)}&lt;/fml:DC_MIEJSCE_URODZENIA&gt;
else ()
}
{
if ($req/m:CivilStatus) then
&lt;fml:DC_STAN_CYWILNY&gt;{data($req/m:CivilStatus)}&lt;/fml:DC_STAN_CYWILNY&gt;
else ()
}
{
if ($req/m:Language) then
&lt;fml:DC_KOD_JEZYKA&gt;{data($req/m:Language)}&lt;/fml:DC_KOD_JEZYKA&gt;
else ()
}
{
if ($req/m:HighSchoolName) then
&lt;fml:DC_UCZELNIA&gt;{data($req/m:HighSchoolName)}&lt;/fml:DC_UCZELNIA&gt;
else ()
}
{
if ($req/m:SchoolType) then
&lt;fml:DC_TYP_SZKOLY&gt;{data($req/m:SchoolType)}&lt;/fml:DC_TYP_SZKOLY&gt;
else ()
}
{
if ($req/m:PlanEndSchoolDate) then
&lt;fml:DC_PLANOW_DATA_UKON_SZK&gt;{data($req/m:PlanEndSchoolDate)}&lt;/fml:DC_PLANOW_DATA_UKON_SZK&gt;
else ()
}
{
if ($req/m:EduLevel) then
&lt;fml:DC_WYKSZTALCENIE&gt;{data($req/m:EduLevel)}&lt;/fml:DC_WYKSZTALCENIE&gt;
else ()
}
{
if ($req/m:EducationProfile) then
&lt;fml:DC_CHARAKTER_WYKSZTALC&gt;{data($req/m:EducationProfile)}&lt;/fml:DC_CHARAKTER_WYKSZTALC&gt;
else ()
}
{
if ($req/m:StudyType) then
&lt;fml:DC_SYSTEM_STUDIOW&gt;{data($req/m:StudyType)}&lt;/fml:DC_SYSTEM_STUDIOW&gt;
else ()
}
{
if ($req/m:SchoolSpecialization) then
&lt;fml:DC_SPECJALIZACJA&gt;{data($req/m:SchoolSpecialization)}&lt;/fml:DC_SPECJALIZACJA&gt;
else ()
}
{
if ($req/m:MilitaryService) then
&lt;fml:DC_SLUZBA_WOJSKOWA&gt;{data($req/m:MilitaryService)}&lt;/fml:DC_SLUZBA_WOJSKOWA&gt;
else ()
}
{
if ($req/m:JointPropertyFlag) then
&lt;fml:DC_WSPOLNOTA_MAJATK&gt;{data($req/m:JointPropertyFlag)}&lt;/fml:DC_WSPOLNOTA_MAJATK&gt;
else ()
}
{
if($req/m:HousePeopleCount)
then
if(data($req/m:HousePeopleCount))
then &lt;fml:DC_LICZBA_OS_WE_WSP_GOSP_D&gt;{ data($req/m:HousePeopleCount) }&lt;/fml:DC_LICZBA_OS_WE_WSP_GOSP_D&gt;
else &lt;fml:DC_LICZBA_OS_WE_WSP_GOSP_D&gt;{0}&lt;/fml:DC_LICZBA_OS_WE_WSP_GOSP_D&gt;
else ()
}
{
if($req/m:KeptPeopleCount)
then
if(data($req/m:KeptPeopleCount))
then &lt;fml:DC_LICZBA_OSOB_NA_UTRZ&gt;{ data($req/m:KeptPeopleCount) }&lt;/fml:DC_LICZBA_OSOB_NA_UTRZ&gt;
else &lt;fml:DC_LICZBA_OSOB_NA_UTRZ&gt;{0}&lt;/fml:DC_LICZBA_OSOB_NA_UTRZ&gt;
else ()
}
{
if ($req/m:HouseOwnerFlag) then
&lt;fml:DC_POSIADA_DOM_MIESZK&gt;{data($req/m:HouseOwnerFlag)}&lt;/fml:DC_POSIADA_DOM_MIESZK&gt;
else ()
}
{
if ($req/m:HousePropertyType) then
&lt;fml:CI_TYP_WLASNOSCI&gt;{data($req/m:HousePropertyType)}&lt;/fml:CI_TYP_WLASNOSCI&gt;
else ()
}
{
if($req/m:JobCode)
then
if(data($req/m:JobCode))
then &lt;fml:DC_KOD_ZAWODU&gt;{ data($req/m:JobCode) }&lt;/fml:DC_KOD_ZAWODU&gt;
else &lt;fml:DC_KOD_ZAWODU&gt;{0}&lt;/fml:DC_KOD_ZAWODU&gt;
else ()
}

{
if($req/m:Profession)
then
if(data($req/m:Profession))
then &lt;fml:CI_KOD_ZAWODU_WYUCZONEGO&gt;{ data($req/m:Profession) }&lt;/fml:CI_KOD_ZAWODU_WYUCZONEGO&gt;
else &lt;fml:CI_KOD_ZAWODU_WYUCZONEGO&gt;{0}&lt;/fml:CI_KOD_ZAWODU_WYUCZONEGO&gt;
else ()
}
{
if ($req/m:WorkPlace) then
&lt;fml:CI_SEKTOR_ZATRUDNIENIA&gt;{data($req/m:WorkPlace)}&lt;/fml:CI_SEKTOR_ZATRUDNIENIA&gt;
else ()
}
{
if($req/m:MonthIncome)
then
if(data($req/m:MonthIncome))
then &lt;fml:DC_MIES_DOCHOD_NETTO_W_PLN&gt;{ data($req/m:MonthIncome) }&lt;/fml:DC_MIES_DOCHOD_NETTO_W_PLN&gt;
else &lt;fml:DC_MIES_DOCHOD_NETTO_W_PLN&gt;{0}&lt;/fml:DC_MIES_DOCHOD_NETTO_W_PLN&gt;
else ()
}
{
if ($req/m:IncomeSrc) then
&lt;fml:CI_ZRODLO_DOCHODU&gt;{data($req/m:IncomeSrc)}&lt;/fml:CI_ZRODLO_DOCHODU&gt;
else ()
}
{
if($req/m:ExtraMonthIncome)
then
if(data($req/m:ExtraMonthIncome))
then &lt;fml:CI_DOD_DOCHOD_MIES_NETTO&gt;{ data($req/m:ExtraMonthIncome) }&lt;/fml:CI_DOD_DOCHOD_MIES_NETTO&gt;
else &lt;fml:CI_DOD_DOCHOD_MIES_NETTO&gt;{0}&lt;/fml:CI_DOD_DOCHOD_MIES_NETTO&gt;
else ()
}
{
if ($req/m:ExtraIncomeSrc) then
&lt;fml:CI_ZRODLO_DODAT_DOCH&gt;{data($req/m:ExtraIncomeSrc)}&lt;/fml:CI_ZRODLO_DODAT_DOCH&gt;
else ()
}
{
if ($req/m:ChargeType) then
&lt;fml:CI_RODZAJ_OBCIAZENIA&gt;{data($req/m:ChargeType)}&lt;/fml:CI_RODZAJ_OBCIAZENIA&gt;
else ()
}
{
if ($req/m:ChargeTypeOther) then
&lt;fml:CI_RODZAJ_OBC_INNE&gt;{data($req/m:ChargeTypeOther)}&lt;/fml:CI_RODZAJ_OBC_INNE&gt;
else ()
}
{
if($req/m:SumMonthCharge)
then
if(data($req/m:SumMonthCharge))
then &lt;fml:CI_LACZNA_WYS_OBC&gt;{ data($req/m:SumMonthCharge) }&lt;/fml:CI_LACZNA_WYS_OBC&gt;
else &lt;fml:CI_LACZNA_WYS_OBC&gt;{0}&lt;/fml:CI_LACZNA_WYS_OBC&gt;
else ()
}
{
if ($req/m:CustDeathDate) then
&lt;fml:DC_DATA_SMIERCI&gt;{data($req/m:CustDeathDate)}&lt;/fml:DC_DATA_SMIERCI&gt;
else ()
}
{
if ($req/m:PoliciesCount) then
&lt;fml:CI_LICZBA_POLIS&gt;{data($req/m:PoliciesCount)}&lt;/fml:CI_LICZBA_POLIS&gt;
else ()
}




{
if ($req/m:PoliciesSum) then
&lt;fml:CI_SUMA_POLIS_NA_ZYCIE&gt;{data($req/m:PoliciesSum)}&lt;/fml:CI_SUMA_POLIS_NA_ZYCIE&gt;
else ()
}
{
if ($req/m:LifeInsurranceFlag) then
&lt;fml:DC_UBEZPIECZENIE_NA_ZYCIE&gt;{data($req/m:LifeInsurranceFlag)}&lt;/fml:DC_UBEZPIECZENIE_NA_ZYCIE&gt;
else ()
}
{
if ($req/m:NwInsurranceFlag) then
&lt;fml:DC_UBEZP_OD_NASTEPSTW_NW&gt;{data($req/m:NwInsurranceFlag)}&lt;/fml:DC_UBEZP_OD_NASTEPSTW_NW&gt;
else ()
}
{
if ($req/m:RetireInsurranceFlag) then
&lt;fml:DC_UBEZPIECZENIE_EMERYTALNE&gt;{data($req/m:RetireInsurranceFlag)}&lt;/fml:DC_UBEZPIECZENIE_EMERYTALNE&gt;
else ()
}
{
if ($req/m:WealthInsurranceFlag) then
&lt;fml:DC_UBEZP_MAJATKOWE_MIES&gt;{data($req/m:WealthInsurranceFlag)}&lt;/fml:DC_UBEZP_MAJATKOWE_MIES&gt;
else ()
}
{
if ($req/m:OtherInsurranceFlag) then
&lt;fml:DC_UBEZPIECZENIA_INNE&gt;{data($req/m:OtherInsurranceFlag)}&lt;/fml:DC_UBEZPIECZENIA_INNE&gt;
else ()
}
{
if ($req/m:RegisterNo) then
&lt;fml:CI_KRS&gt;{data($req/m:RegisterNo)}&lt;/fml:CI_KRS&gt;
else ()
}
{
if($req/m:EkdCode)
then
if(data($req/m:EkdCode))
then &lt;fml:DC_EKD&gt;{ data($req/m:EkdCode) }&lt;/fml:DC_EKD&gt;
else &lt;fml:DC_EKD&gt;{0}&lt;/fml:DC_EKD&gt;
else ()
}
{
if($req/m:AgencyCount)
then
if(data($req/m:AgencyCount))
then &lt;fml:CI_LICZBA_PLACOWEK&gt;{ data($req/m:AgencyCount) }&lt;/fml:CI_LICZBA_PLACOWEK&gt;
else &lt;fml:CI_LICZBA_PLACOWEK&gt;{0}&lt;/fml:CI_LICZBA_PLACOWEK&gt;
else ()
}
{
if ($req/m:EmployedCount) then
&lt;fml:DC_LICZBA_ZATRUDNIONYCH&gt;{data($req/m:EmployedCount)}&lt;/fml:DC_LICZBA_ZATRUDNIONYCH&gt;
else ()
}
{
if ($req/m:BusinessEndDate) then
&lt;fml:DC_DATA_BANKRUCTWA&gt;{data($req/m:BusinessEndDate)}&lt;/fml:DC_DATA_BANKRUCTWA&gt;
else ()
}
{
if ($req/m:WebPage) then
&lt;fml:CI_STRONA_WWW&gt;{data($req/m:WebPage)}&lt;/fml:CI_STRONA_WWW&gt;
else ()
}
{
if ($req/m:BusinessInitDate) then
&lt;fml:CI_DATA_ROZP_DZIAL&gt;{data($req/m:BusinessInitDate)}&lt;/fml:CI_DATA_ROZP_DZIAL&gt;
else ()
}
{
if($req/m:DebtValue)
then
if(data($req/m:DebtValue))
then &lt;fml:CI_KWOTA_ZADLUZENIA&gt;{ data($req/m:DebtValue) }&lt;/fml:CI_KWOTA_ZADLUZENIA&gt;
else &lt;fml:CI_KWOTA_ZADLUZENIA&gt;{0}&lt;/fml:CI_KWOTA_ZADLUZENIA&gt;
else ()
}
{
if ($req/m:SwiftNo) then
&lt;fml:CI_NUMER_SWIFT&gt;{data($req/m:SwiftNo)}&lt;/fml:CI_NUMER_SWIFT&gt;
else ()
}
{
if ($req/m:OrgLawForm) then
&lt;fml:CI_FOP&gt;{data($req/m:OrgLawForm)}&lt;/fml:CI_FOP&gt;
else ()
}
{
if ($req/m:RegOrgName) then
&lt;fml:CI_NAZWA_ORG_REJESTR&gt;{data($req/m:RegOrgName)}&lt;/fml:CI_NAZWA_ORG_REJESTR&gt;
else ()
}
{
if ($req/m:RegOrgStreet) then
&lt;fml:CI_ULICA_ORG_REJESTR&gt;{data($req/m:RegOrgStreet)}&lt;/fml:CI_ULICA_ORG_REJESTR&gt;
else ()
}
{
if ($req/m:RegOrgHouseNo) then
&lt;fml:CI_NR_POSES_LOKALU_ORG_REJ&gt;{data($req/m:RegOrgHouseNo)}&lt;/fml:CI_NR_POSES_LOKALU_ORG_REJ&gt;
else ()
}
{
if ($req/m:RegOrgPostCode) then
&lt;fml:CI_KOD_POCZT_ORG_REJESTR&gt;{data($req/m:RegOrgPostCode)}&lt;/fml:CI_KOD_POCZT_ORG_REJESTR&gt;
else ()
}
{
if ($req/m:RegOrgCity) then
&lt;fml:CI_MIASTO_ORG_REJESTR&gt;{data($req/m:RegOrgCity)}&lt;/fml:CI_MIASTO_ORG_REJESTR&gt;
else ()
}
{
if($req/m:RegOrgCountry)
then
if(data($req/m:RegOrgCountry))
then &lt;fml:CI_KRAJ_ORG_REJESTR&gt;{ data($req/m:RegOrgCountry) }&lt;/fml:CI_KRAJ_ORG_REJESTR&gt;
else &lt;fml:CI_KRAJ_ORG_REJESTR&gt;{0}&lt;/fml:CI_KRAJ_ORG_REJESTR&gt;
else ()
}
{
if ($req/m:RegOrgInitDate) then
&lt;fml:CI_DATA_WPISU_DO_REJESTR&gt;{data($req/m:RegOrgInitDate)}&lt;/fml:CI_DATA_WPISU_DO_REJESTR&gt;
else ()
}
{
if ($req/m:HeadquaterFlag) then
&lt;fml:CI_STATUS_SIEDZIBY&gt;{data($req/m:HeadquaterFlag)}&lt;/fml:CI_STATUS_SIEDZIBY&gt;
else ()
}
{
if ($req/m:Auditor) then
&lt;fml:CI_AUDYTOR&gt;{data($req/m:Auditor)}&lt;/fml:CI_AUDYTOR&gt;
else ()
}
{
if($req/m:OriginalCapital)
then
if(data($req/m:OriginalCapital))
then &lt;fml:CI_KAPITAL_ZALOZYCIELSKI&gt;{ data($req/m:OriginalCapital) }&lt;/fml:CI_KAPITAL_ZALOZYCIELSKI&gt;
else &lt;fml:CI_KAPITAL_ZALOZYCIELSKI&gt;{0}&lt;/fml:CI_KAPITAL_ZALOZYCIELSKI&gt;
else ()
}
{
if($req/m:SharesValue)
then
if(data($req/m:SharesValue))
then &lt;fml:CI_WARTOSC_AKCJI&gt;{ data($req/m:SharesValue) }&lt;/fml:CI_WARTOSC_AKCJI&gt;
else &lt;fml:CI_WARTOSC_AKCJI&gt;{0}&lt;/fml:CI_WARTOSC_AKCJI&gt;
else ()
}
{
if($req/m:ImmovablesValue)
then
if(data($req/m:ImmovablesValue))
then &lt;fml:CI_WARTOSC_NIERUCHOMOSCI&gt;{ data($req/m:ImmovablesValue) }&lt;/fml:CI_WARTOSC_NIERUCHOMOSCI&gt;
else &lt;fml:CI_WARTOSC_NIERUCHOMOSCI&gt;{0}&lt;/fml:CI_WARTOSC_NIERUCHOMOSCI&gt;
else ()
}
{
if($req/m:SurfaceValue)
then
if(data($req/m:SurfaceValue))
then &lt;fml:CI_WARTOSC_SRODKOW_MATER&gt;{ data($req/m:SurfaceValue) }&lt;/fml:CI_WARTOSC_SRODKOW_MATER&gt;
else &lt;fml:CI_WARTOSC_SRODKOW_MATER&gt;{0}&lt;/fml:CI_WARTOSC_SRODKOW_MATER&gt;
else ()
}
{
if($req/m:OtherAssets)
then
if(data($req/m:OtherAssets))
then &lt;fml:CI_INNE_AKTYWA&gt;{ data($req/m:OtherAssets) }&lt;/fml:CI_INNE_AKTYWA&gt;
else &lt;fml:CI_INNE_AKTYWA&gt;{0}&lt;/fml:CI_INNE_AKTYWA&gt;
else ()
}
{
if($req/m:TaxBorrowing)
then
if(data($req/m:TaxBorrowing))
then &lt;fml:CI_ZOBOWIAZANIA_PODATKOWE&gt;{ data($req/m:TaxBorrowing) }&lt;/fml:CI_ZOBOWIAZANIA_PODATKOWE&gt;
else &lt;fml:CI_ZOBOWIAZANIA_PODATKOWE&gt;{0}&lt;/fml:CI_ZOBOWIAZANIA_PODATKOWE&gt;
else ()
}
{
if($req/m:OtherBorrowing)
then
if(data($req/m:OtherBorrowing))
then &lt;fml:CI_INNE_ZOBOWIAZANIA&gt;{ data($req/m:OtherBorrowing) }&lt;/fml:CI_INNE_ZOBOWIAZANIA&gt;
else &lt;fml:CI_INNE_ZOBOWIAZANIA&gt;{0}&lt;/fml:CI_INNE_ZOBOWIAZANIA&gt;
else ()
}
{
if ($req/m:FinancialUpdateDate) then
&lt;fml:CI_DATA_AKTUAL_DANYCH_FIN&gt;{data($req/m:FinancialUpdateDate)}&lt;/fml:CI_DATA_AKTUAL_DANYCH_FIN&gt;
else ()
}
{
if($req/m:Income)
then
if(data($req/m:Income))
then &lt;fml:CI_PRZYCHODY_INST&gt;{ data($req/m:Income) }&lt;/fml:CI_PRZYCHODY_INST&gt;
else &lt;fml:CI_PRZYCHODY_INST&gt;{0}&lt;/fml:CI_PRZYCHODY_INST&gt;
else ()
}
{
if ($req/m:CustCorrAddressStreet) then
&lt;fml:DC_ULICA_ADRES_ALT&gt;{data($req/m:CustCorrAddressStreet)}&lt;/fml:DC_ULICA_ADRES_ALT&gt;
else ()
}
{
if ($req/m:CustCorrAddressHouseFlatNo) then
&lt;fml:DC_NR_POSES_LOKALU_ADRES_ALT&gt;{data($req/m:CustCorrAddressHouseFlatNo)}&lt;/fml:DC_NR_POSES_LOKALU_ADRES_ALT&gt;
else ()
}
{
if ($req/m:CustCorrAddressPostCode) then
&lt;fml:DC_KOD_POCZTOWY_ADRES_ALT&gt;{data($req/m:CustCorrAddressPostCode)}&lt;/fml:DC_KOD_POCZTOWY_ADRES_ALT&gt;
else ()
}
{
if ($req/m:CustCorrAddressCity) then
&lt;fml:DC_MIASTO_ADRES_ALT&gt;{data($req/m:CustCorrAddressCity)}&lt;/fml:DC_MIASTO_ADRES_ALT&gt;
else ()
}
{
if ($req/m:CustCorrAddressCounty) then
&lt;fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT&gt;{data($req/m:CustCorrAddressCounty)}&lt;/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT&gt;
else ()
}
{
if ($req/m:CustCorrAddressCountry) then
&lt;fml:CI_KOD_KRAJU_KORESP&gt;{data($req/m:CustCorrAddressCountry)}&lt;/fml:CI_KOD_KRAJU_KORESP&gt;
else ()
}
{
if ($req/m:CustCorrValidDateFrom) then
&lt;fml:CI_DATA_OD&gt;{data($req/m:CustCorrValidDateFrom)}&lt;/fml:CI_DATA_OD&gt;
else ()
}
{
if ($req/m:CustCorrValidDateTo) then
&lt;fml:CI_DATA_DO&gt;{data($req/m:CustCorrValidDateTo)}&lt;/fml:CI_DATA_DO&gt;
else ()
}
{
if ($req/m:CustCorrAddressForFlag) then
&lt;fml:CI_TYP_ADRESU_KORESP&gt;{data($req/m:CustCorrAddressForFlag)}&lt;/fml:CI_TYP_ADRESU_KORESP&gt;
else ()
}
{
if ($req/m:EmpAddressStreet) then
&lt;fml:CI_ULICA_PRACODAWCY&gt;{data($req/m:EmpAddressStreet)}&lt;/fml:CI_ULICA_PRACODAWCY&gt;
else ()
}
{
if ($req/m:EmpAddressHouseFlatNo) then
&lt;fml:CI_NUMER_POSESJI_PRAC&gt;{data($req/m:EmpAddressHouseFlatNo)}&lt;/fml:CI_NUMER_POSESJI_PRAC&gt;
else ()
}
{
if ($req/m:EmpAddressPostCode) then
&lt;fml:CI_KOD_POCZT_PRAC&gt;{data($req/m:EmpAddressPostCode)}&lt;/fml:CI_KOD_POCZT_PRAC&gt;
else ()
}
{
if ($req/m:EmpAddressCity) then
&lt;fml:CI_MIASTO_PRACODAWCY&gt;{data($req/m:EmpAddressCity)}&lt;/fml:CI_MIASTO_PRACODAWCY&gt;
else ()
}
{
if ($req/m:EmpAddressCounty) then
&lt;fml:CI_WOJEWODZTWO_PRAC&gt;{data($req/m:EmpAddressCounty)}&lt;/fml:CI_WOJEWODZTWO_PRAC&gt;
else ()
}
{
if ($req/m:EmpAddressCountry) then
&lt;fml:CI_KOD_KRAJU_PRAC&gt;{data($req/m:EmpAddressCountry)}&lt;/fml:CI_KOD_KRAJU_PRAC&gt;
else ()
}
{
if ($req/m:EmpAddressForFlag) then
&lt;fml:CI_TYP_ADRESU_PRAC&gt;{data($req/m:EmpAddressForFlag)}&lt;/fml:CI_TYP_ADRESU_PRAC&gt;
else ()
}
{
if ($req/m:EmpPhone) then
&lt;fml:CI_NR_TELEFONU_PRAC&gt;{data($req/m:EmpPhone)}&lt;/fml:CI_NR_TELEFONU_PRAC&gt;
else ()
}
{
if ($req/m:EmployeeStatus) then
&lt;fml:DC_STATUS_PRACOWNIKA&gt;{data($req/m:EmployeeStatus)}&lt;/fml:DC_STATUS_PRACOWNIKA&gt;
else ()
}
{
if ($req/m:JobStartDate) then
&lt;fml:DC_DATA_PODJECIA_PRACY&gt;{data($req/m:JobStartDate)}&lt;/fml:DC_DATA_PODJECIA_PRACY&gt;
else ()
}
{
if ($req/m:TempJob) then
&lt;fml:DC_WYKONYWANA_PRACA&gt;{data($req/m:TempJob)}&lt;/fml:DC_WYKONYWANA_PRACA&gt;
else ()
}
{
if ($req/m:ResidenceCountry) then
&lt;fml:CI_KRAJ_ZAMIESZKANIA&gt;{data($req/m:ResidenceCountry)}&lt;/fml:CI_KRAJ_ZAMIESZKANIA&gt;
else ()
}
{
if ($req/m:BusinessCountry) then
&lt;fml:CI_KRAJ_PROWADZENIA_DZIAL&gt;{data($req/m:BusinessCountry)}&lt;/fml:CI_KRAJ_PROWADZENIA_DZIAL&gt;
else ()
}
{
if ($req/m:ComplexStructFlag) then
&lt;fml:CI_STRUKTURA_ZLOZONA&gt;{data($req/m:ComplexStructFlag)}&lt;/fml:CI_STRUKTURA_ZLOZONA&gt;
else ()
}
{
if ($req/m:BusinessType) then
&lt;fml:CI_RODZ_DZIALALNOSCI&gt;{data($req/m:BusinessType)}&lt;/fml:CI_RODZ_DZIALALNOSCI&gt;
else ()
}
{
if ($req/m:PodpisanaUmowaLimitWKo) then
&lt;fml:DC_PODPISANA_UMOWA_LIMIT_W_KO&gt;{data($req/m:PodpisanaUmowaLimitWKo)}&lt;/fml:DC_PODPISANA_UMOWA_LIMIT_W_KO&gt;
else ()
}
{
if ($req/m:AssignUnitDate) then
&lt;fml:CI_DATA_ODDZIAL_OBS&gt;{data($req/m:AssignUnitDate)}&lt;/fml:CI_DATA_ODDZIAL_OBS&gt;
else ()
}
{
if ($req/m:EmpSkpNo) then
&lt;fml:CI_PRACOWNIK_ZMIEN&gt;{data($req/m:EmpSkpNo)}&lt;/fml:CI_PRACOWNIK_ZMIEN&gt;
else ()
}
{
if ($req/m:StudyYear) then
&lt;fml:DC_ROK_STUDIOW&gt;{data($req/m:StudyYear)}&lt;/fml:DC_ROK_STUDIOW&gt;
else ()
}
{
if ($req/m:CustBirthCountry) then
&lt;fml:CI_KRAJ_POCHODZENIA&gt;{data($req/m:CustBirthCountry)}&lt;/fml:CI_KRAJ_POCHODZENIA&gt;
else ()
}
{
if ($req/m:EmpName) then
&lt;fml:CI_NAZWA_PRACODAWCY&gt;{data($req/m:EmpName)}&lt;/fml:CI_NAZWA_PRACODAWCY&gt;
else ()
}
{
if ($req/m:LifeInsurerName) then
&lt;fml:CI_UBEZP_NA_ZYCIE&gt;{data($req/m:LifeInsurerName)}&lt;/fml:CI_UBEZP_NA_ZYCIE&gt;
else ()
}
{
if ($req/m:NwInsurerName) then
&lt;fml:CI_UBEZP_NW&gt;{data($req/m:NwInsurerName)}&lt;/fml:CI_UBEZP_NW&gt;
else ()
}
{
if ($req/m:WealthInsurerName) then
&lt;fml:CI_UBEZP_MAJATKOWE&gt;{data($req/m:WealthInsurerName)}&lt;/fml:CI_UBEZP_MAJATKOWE&gt;
else ()
}
{
if ($req/m:OtherInsurerName) then
&lt;fml:CI_UBEZP_INNE&gt;{data($req/m:OtherInsurerName)}&lt;/fml:CI_UBEZP_INNE&gt;
else ()
}
{
if ($req/m:GainingsValue) then
&lt;fml:CI_ZYSKI_INST&gt;{data($req/m:GainingsValue)}&lt;/fml:CI_ZYSKI_INST&gt;
else ()
}
{
if ($req/m:KierunekStudiow) then
&lt;fml:DC_KIERUNEK_STUDIOW&gt;{data($req/m:KierunekStudiow)}&lt;/fml:DC_KIERUNEK_STUDIOW&gt;
else ()
}
{
if ($req/m:KapitalZalozOplacony) then
&lt;fml:CI_KAPITAL_ZALOZ_OPLACONY&gt;{data($req/m:KapitalZalozOplacony)}&lt;/fml:CI_KAPITAL_ZALOZ_OPLACONY&gt;
else ()
}
{
if ($req/m:KodEkdPracodaw) then
&lt;fml:DC_KOD_EKD_PRACODAW&gt;{data($req/m:KodEkdPracodaw)}&lt;/fml:DC_KOD_EKD_PRACODAW&gt;
else ()
}
{
if ($req/m:MainIcbsNo) then
&lt;fml:CI_NR_ODDZIALU&gt;{data($req/m:MainIcbsNo)}&lt;/fml:CI_NR_ODDZIALU&gt;
else ()
}
{
if ($req/m:OperationIcbsNo) then
&lt;fml:CI_ODDZIAL_OBSLUGUJACY&gt;{data($req/m:OperationIcbsNo)}&lt;/fml:CI_ODDZIAL_OBSLUGUJACY&gt;
else ()
}
{
if ($req/m:RetireInsurerName) then
&lt;fml:CI_UBEZP_EMERYTALNE&gt;{data($req/m:RetireInsurerName)}&lt;/fml:CI_UBEZP_EMERYTALNE&gt;
else ()
}
{
if ($req/m:FolderModificationDate) then
&lt;fml:CI_DATA_MODYFIKACJI&gt;{data($req/m:FolderModificationDate)}&lt;/fml:CI_DATA_MODYFIKACJI&gt;
else ()
}
&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMModProspFolderRequest($body/m:CRMModifyProspFolderRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>