<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2009-09-18 PKLI TEET 39529</con:description>
  <con:xquery>(:Change log :)
(: v.1.1 2009-09-18 PKLI TEET 39529: zmiana mapowania pola accountStatus :)

declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:entities.be.dcl";
declare namespace ns10="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns8="urn:be.services.dcl";
declare namespace fml="";
declare namespace ns9="urn:card.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForAccountRelationshipList($parm as element(fml:FML32), $occ) as element()
{

&lt;ns1:accountRelationshipList&gt;
  {
    (:for $x at $occ in $parm/NF_ACCOUR_RELATIONSHIP
    return:)
    &lt;ns3:AccountRelationship&gt;
      &lt;ns3:customer&gt;
        &lt;ns3:Customer&gt;
          &lt;ns3:companyName?&gt;{data($parm/NF_CUSTOM_COMPANYNAME[$occ])}&lt;/ns3:companyName&gt;
          &lt;ns3:customerPersonal&gt;
            &lt;ns3:CustomerPersonal&gt;
              &lt;ns3:lastName?&gt;{data($parm/NF_CUSTOP_LASTNAME[$occ])}&lt;/ns3:lastName&gt;
              &lt;ns3:firstName?&gt;{data($parm/NF_CUSTOP_FIRSTNAME[$occ])}&lt;/ns3:firstName&gt;
            &lt;/ns3:CustomerPersonal&gt;
          &lt;/ns3:customerPersonal&gt;
          &lt;ns3:customerType&gt;
            &lt;ns5:CustomerType&gt;
              &lt;ns5:customerType?&gt;{data($parm/NF_CUSTOT_CUSTOMERTYPE[$occ])}&lt;/ns5:customerType&gt;
            &lt;/ns5:CustomerType&gt;
          &lt;/ns3:customerType&gt;
        &lt;/ns3:Customer&gt;
      &lt;/ns3:customer&gt;
      &lt;ns3:relationship&gt;
        &lt;ns5:CustomerAccountRelationship&gt;
          &lt;ns5:customerAccountRelationship?&gt;{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}&lt;/ns5:customerAccountRelationship&gt;
        &lt;/ns5:CustomerAccountRelationship&gt;
      &lt;/ns3:relationship&gt;
    &lt;/ns3:AccountRelationship&gt;
  }
&lt;/ns1:accountRelationshipList&gt;
};
declare function getElementsForAccountList($parm as element(fml:FML32)) as element()
{

&lt;ns8:accountList&gt;
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
    &lt;ns1:Account&gt;
      &lt;ns1:accountNumber?&gt;{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}&lt;/ns1:accountNumber&gt;
      &lt;ns1:accountName?&gt;{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}&lt;/ns1:accountName&gt;
      &lt;ns1:currentBalance?&gt;{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}&lt;/ns1:currentBalance&gt;
      &lt;ns1:accountDescription?&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}&lt;/ns1:accountDescription&gt;
      {getElementsForAccountRelationshipList($parm,$occ)}
      &lt;ns1:accountType&gt;
        &lt;ns5:AccountType&gt;
          &lt;ns5:accountType?&gt;{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}&lt;/ns5:accountType&gt;
        &lt;/ns5:AccountType&gt;
      &lt;/ns1:accountType&gt;
      &lt;ns1:currency&gt;
        &lt;ns5:CurrencyCode&gt;
          &lt;ns5:currencyCode?&gt;{data($parm/NF_CURREC_CURRENCYCODE[$occ])}&lt;/ns5:currencyCode&gt;
        &lt;/ns5:CurrencyCode&gt;
      &lt;/ns1:currency&gt;
      &lt;ns1:accountStatus&gt;
        &lt;ns2:AccountStatus&gt;
(:T39529  &lt;ns2:accountStatus?&gt;{data($parm/NF_ACCOUS_ACCOUNTSTATUS[$occ])}&lt;/ns2:accountStatus&gt; :)
(:T39529:)&lt;ns2:accountStatus?&gt;{data($parm/NF_PRODUCT_STATUSCODE[$occ])}&lt;/ns2:accountStatus&gt;
        &lt;/ns2:AccountStatus&gt;
      &lt;/ns1:accountStatus&gt;
    &lt;/ns1:Account&gt;
  }
&lt;/ns8:accountList&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns8:invokeResponse&gt;
  &lt;ns8:response&gt;
    &lt;ns0:ResponseMessage&gt;
      &lt;ns0:result?&gt;{data($parm/NF_RESPOM_RESULT)}&lt;/ns0:result&gt;
    &lt;/ns0:ResponseMessage&gt;
  &lt;/ns8:response&gt;
  {getElementsForAccountList($parm)}
  &lt;ns8:bcd&gt;
    &lt;ns6:BusinessControlData&gt;
      &lt;ns6:pageControl&gt;
        &lt;ns10:PageControl&gt;
          &lt;ns10:hasNext?&gt;{data($parm/NF_PAGEC_HASNEXT)}&lt;/ns10:hasNext&gt;
          &lt;ns10:navigationKeyDefinition?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}&lt;/ns10:navigationKeyDefinition&gt;
          &lt;ns10:navigationKeyValue?&gt;{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}&lt;/ns10:navigationKeyValue&gt;
        &lt;/ns10:PageControl&gt;
      &lt;/ns6:pageControl&gt;
    &lt;/ns6:BusinessControlData&gt;
  &lt;/ns8:bcd&gt;
&lt;/ns8:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>