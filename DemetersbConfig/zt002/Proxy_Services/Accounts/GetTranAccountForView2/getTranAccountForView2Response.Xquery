<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-04-06</con:description>
    <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accountdict.dictionaries.be.dcl";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
 if ($value)
   then if(string-length($value)>5 and not ($value = "0001-01-01"))
       then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
   else() 
 else()
};

declare function sourceValue2Boolean ($parm as xs:string?,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns6:invokeResponse>
  <ns6:fee>
    <ns0:Fee>
      <ns0:feeFrequency?>{data($parm/NF_FEE_FEEFREQUENCY)}</ns0:feeFrequency>
      <ns0:feeSpecialDay?>{data($parm/NF_FEE_FEESPECIALDAY)}</ns0:feeSpecialDay>
      (:<ns0:nextFeeDate?>{data($parm/NF_FEE_NEXTFEEDATE)}</ns0:nextFeeDate>:)
      {insertDate(data($parm/NF_FEE_NEXTFEEDATE),"yyyy-MM-dd","ns0:nextFeeDate")}
      <ns0:accruedFee?>{data($parm/NF_FEE_ACCRUEDFEE)}</ns0:accruedFee>
      <ns0:currentPeriodDays?>{data($parm/NF_FEE_CURRENTPERIODDAYS)}</ns0:currentPeriodDays>
      <ns0:feeForServicesSummation?>{data($parm/NF_FEE_FEEFORSERVICESSUMMA)}</ns0:feeForServicesSummation>
      <ns0:feePeriod>
        <ns4:Period>
          <ns4:period?>{data($parm/NF_PERIOD_PERIOD[1])}</ns4:period>
        </ns4:Period>
      </ns0:feePeriod>
      <ns0:feeType>
        <ns1:FeeType>
          <ns1:feeType?>{data($parm/NF_FEET_FEETYPE)}</ns1:feeType>
        </ns1:FeeType>
      </ns0:feeType>
    </ns0:Fee>
  </ns6:fee>
  <ns6:tranAccountACA>
    <ns0:TranAccountACA>
      <ns0:flagTODACA>
        <ns1:FlagTODACA>
          <ns1:flagTODACA?>{data($parm/NF_FTODAC_FLAGTODACA)}</ns1:flagTODACA>
        </ns1:FlagTODACA>
      </ns0:flagTODACA>
    </ns0:TranAccountACA>
  </ns6:tranAccountACA>
  <ns6:accountOut>
    <ns0:Account>
      (:<ns0:accountOpenDate?>{data($parm/NF_ACCOUN_ACCOUNTOPENDATE)}</ns0:accountOpenDate>:)
      {insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE),"yyyy-MM-dd","ns0:accountOpenDate")}
      <ns0:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE)}</ns0:currentBalance>
      <ns0:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI)}</ns0:accountDescription>
      <ns0:skpOpener?>{data($parm/NF_ACCOUN_SKPOPENER)}</ns0:skpOpener>
      <ns0:productCode?>{data($parm/NF_ACCOUN_PRODUCTCODE)}</ns0:productCode>
      <ns0:officerCode?>{data($parm/NF_ACCOUN_OFFICERCODE)}</ns0:officerCode>
      <ns0:tranAccount>
        <ns0:TranAccount>
          <ns0:avaiableBalance?>{data($parm/NF_TRANA_AVAIABLEBALANCE)}</ns0:avaiableBalance>
          <ns0:memoBalance?>{data($parm/NF_TRANA_MEMOBALANCE)}</ns0:memoBalance>
          <ns0:interestRate?>{data($parm/NF_TRANA_INTERESTRATE)}</ns0:interestRate>
          (:<ns0:dateOfLastActivity?>{data($parm/NF_TRANA_DATEOFLASTACTIVIT)}</ns0:dateOfLastActivity>:)
          {insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT),"yyyy-MM-dd","ns0:dateOfLastActivity")}
          <ns0:serviceChargeCode?>{data($parm/NF_TRANA_SERVICECHARGECODE)}</ns0:serviceChargeCode>
          <ns0:interestDueDebit?>{data($parm/NF_TRANA_INTERESTDUEDEBIT)}</ns0:interestDueDebit>
          <ns0:interestDueCredit?>{data($parm/NF_TRANA_INTERESTDUECREDIT)}</ns0:interestDueCredit>
          <ns0:interestTransferAccount?>{data($parm/NF_TRANA_INTERESTTRANSFERA)}</ns0:interestTransferAccount>
          <ns0:interestPlanNumber?>{data($parm/NF_TRANA_INTERESTPLANNUMBE)}</ns0:interestPlanNumber>
          <ns0:interestPlanIndex?>{data($parm/NF_TRANA_INTERESTPLANINDEX)}</ns0:interestPlanIndex>
          <ns0:interestAccd?>{data($parm/NF_TRANA_INTERESTACCD)}</ns0:interestAccd>
          (:<ns0:nextCreditIntDate?>{data($parm/NF_TRANA_NEXTCREDITINTDATE)}</ns0:nextCreditIntDate>:)
          {insertDate(data($parm/NF_TRANA_NEXTCREDITINTDATE),"yyyy-MM-dd","ns0:nextCreditIntDate")}
          <ns0:interestAccrued?>{data($parm/NF_TRANA_INTERESTACCRUED)}</ns0:interestAccrued>
          <ns0:interestAccruedPrevious?>{data($parm/NF_TRANA_INTERESTACCRUEDPR)}</ns0:interestAccruedPrevious>
          <ns0:interestAccruePrevYear?>{data($parm/NF_TRANA_INTERESTACCRUEPRE)}</ns0:interestAccruePrevYear>
          <ns0:witholdingIntTaxFlag?>{data($parm/NF_TRANA_WITHOLDINGINTTAXF)}</ns0:witholdingIntTaxFlag>
          <ns0:witholdingIntTaxPercent?>{data($parm/NF_TRANA_WITHOLDINGINTTAXP)}</ns0:witholdingIntTaxPercent>
          <ns0:specificDayDisposition?>{data($parm/NF_TRANA_SPECIFICDAYDISPOS)}</ns0:specificDayDisposition>
          <ns0:accuredTax?>{data($parm/NF_TRANA_ACCUREDTAX)}</ns0:accuredTax>
          (:<ns0:dateOfLastPerform?>{data($parm/NF_TRANA_DATEOFLASTPERFORM)}</ns0:dateOfLastPerform>:)
          {insertDate(data($parm/NF_TRANA_DATEOFLASTPERFORM),"yyyy-MM-dd","ns0:dateOfLastPerform")}
          <ns0:creditAmount?>{data($parm/NF_TRANA_CREDITAMOUNT)}</ns0:creditAmount>
          <ns0:interestCodeDisposition?>{data($parm/NF_TRANA_INTERESTCODEDISPO)}</ns0:interestCodeDisposition>
          <ns0:interestFrequency?>{data($parm/NF_TRANA_INTERESTFREQUENCY)}</ns0:interestFrequency>
          <ns0:accountTypeFlag>
            <ns4:AccountTypeFlag>
              <ns4:accountTypeFlag?>{data($parm/NF_ACCOTF_ACCOUNTTYPEFLAG)}</ns4:accountTypeFlag>
            </ns4:AccountTypeFlag>
          </ns0:accountTypeFlag>
          <ns0:creditInterestPeriod>
            <ns4:Period>
              <ns4:period?>{data($parm/NF_PERIOD_PERIOD[2])}</ns4:period>
            </ns4:Period>
          </ns0:creditInterestPeriod>
          <ns0:promotionCode>
            <ns4:PromotionCode>
              <ns4:promotionCode?>{data($parm/NF_PROMOC_PROMOTIONCODE)}</ns4:promotionCode>
            </ns4:PromotionCode>
          </ns0:promotionCode>
        </ns0:TranAccount>
      </ns0:tranAccount>
      <ns0:statementParameters>
        <ns0:StatementParameters>
          <ns0:frequency?>{data($parm/NF_STATEP_FREQUENCY)}</ns0:frequency>
          (:<ns0:nextPrintoutDate?>{data($parm/NF_STATEP_NEXTPRINTOUTDATE)}</ns0:nextPrintoutDate>:)
          {insertDate(data($parm/NF_STATEP_NEXTPRINTOUTDATE),"yyyy-MM-dd","ns0:nextPrintoutDate")}
          <ns0:printFlag?>{data($parm/NF_STATEP_PRINTFLAG)}</ns0:printFlag>
          <ns0:specialDay?>{data($parm/NF_STATEP_SPECIALDAY)}</ns0:specialDay>
          <ns0:aggregateTransaction?>{data($parm/NF_STATEP_AGGREGATETRANSAC)}</ns0:aggregateTransaction>
          <ns0:cycle>
            <ns4:Period>
              <ns4:period?>{data($parm/NF_PERIOD_PERIOD[3])}</ns4:period>
            </ns4:Period>
          </ns0:cycle>
          <ns0:provideManner>
            <ns4:ProvideManner>
              <ns4:provideManner?>{data($parm/NF_PROVIM_PROVIDEMANNER)}</ns4:provideManner>
            </ns4:ProvideManner>
          </ns0:provideManner>
        </ns0:StatementParameters>
      </ns0:statementParameters>
      <ns0:currency>
        <ns4:CurrencyCode>
          <ns4:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE)}</ns4:currencyCode>
        </ns4:CurrencyCode>
      </ns0:currency>
      <ns0:accountStatus>
        <ns2:AccountStatus>
          <ns2:accountStatus?>{data($parm/NF_ACCOUS_ACCOUNTSTATUS)}</ns2:accountStatus>
        </ns2:AccountStatus>
      </ns0:accountStatus>
      <ns0:accountBranchNumber>
        <ns4:BranchCode>
          <ns4:branchCode?>{data($parm/NF_BRANCC_BRANCHCODE)}</ns4:branchCode>
        </ns4:BranchCode>
      </ns0:accountBranchNumber>
      <ns0:accessType>
        <ns1:AccessType>
          <ns1:accessType?>{data($parm/NF_ACCEST_ACCESSTYPE)}</ns1:accessType>
        </ns1:AccessType>
      </ns0:accessType>
      <ns0:accountAddress>
        <ns0:AccountAddress>
          <ns0:name1?>{data($parm/NF_ACCOUA_NAME1)}</ns0:name1>
          <ns0:name2?>{data($parm/NF_ACCOUA_NAME2)}</ns0:name2>
          <ns0:street?>{data($parm/NF_ACCOUA_STREET)}</ns0:street>
          <ns0:houseFlatNumber?>{data($parm/NF_ACCOUA_HOUSEFLATNUMBER)}</ns0:houseFlatNumber>
          <ns0:city?>{data($parm/NF_ACCOUA_CITY)}</ns0:city>
          <ns0:stateCountry?>{data($parm/NF_ACCOUA_STATECOUNTRY)}</ns0:stateCountry>
          <ns0:zipCode?>{data($parm/NF_ACCOUA_ZIPCODE)}</ns0:zipCode>
          <ns0:accountAddressType?>{data($parm/NF_ACCOUA_ACCOUNTADDRESSTY)}</ns0:accountAddressType>
          (:<ns0:validFrom?>{data($parm/NF_ACCOUA_VALIDFROM)}</ns0:validFrom>:)
          {insertDate(data($parm/NF_ACCOUA_VALIDFROM),"yyyy-MM-dd","ns0:validFrom")}
          (:<ns0:validTo?>{data($parm/NF_ACCOUA_VALIDTO)}</ns0:validTo>:)
          {insertDate(data($parm/NF_ACCOUA_VALIDTO),"yyyy-MM-dd","ns0:validTo")}
          <ns0:accountAddressStatus?>{sourceValue2Boolean(data($parm/NF_ACCOUA_ACCOUNTADDRESSST),"1")}</ns0:accountAddressStatus>
        </ns0:AccountAddress>
      </ns0:accountAddress>
    </ns0:Account>
  </ns6:accountOut>
</ns6:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>