<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))&gt;0)
    then true()
    else false()
};

&lt;soap-env:Body&gt;
  &lt;m:invokeResponse xmlns:urn="urn:be.services.dcl"&gt;
    &lt;m:accountACA xmlns:urn1="urn:accounts.entities.be.dcl"&gt;
      &lt;m1:TranAccountACA&gt;
        {
        let $fml := $body/fml:FML32
        let $DC_NR_LINII_KREDYTOWEJ_ACA := $fml/fml:DC_NR_LINII_KREDYTOWEJ_ACA

        return

        if($DC_NR_LINII_KREDYTOWEJ_ACA and xf:isData($DC_NR_LINII_KREDYTOWEJ_ACA))
          then &lt;m1:subaccountNumber&gt;{ data($DC_NR_LINII_KREDYTOWEJ_ACA) }&lt;/m1:subaccountNumber&gt;
          else ()
        }
      &lt;/m1:TranAccountACA&gt;
    &lt;/m:accountACA&gt;
  &lt;/m:invokeResponse&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>