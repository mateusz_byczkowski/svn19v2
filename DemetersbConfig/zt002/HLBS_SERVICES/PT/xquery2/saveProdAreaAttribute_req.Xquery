<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/saveProdAreaAttribute_req/";
declare namespace dcl = "urn:productstree.entities.be.dcl";
declare namespace ns1 = "urn:dictionaries.be.dcl";

declare function xf:saveProdAreaAttribute_req ($entity as element(dcl:ProductAreaAttributes)) as element(FML32) {
&lt;FML32&gt;
     &lt;PT_ATTRIBUTE_NAME&gt;{ data( $entity/dcl:attributeName ) }&lt;/PT_ATTRIBUTE_NAME&gt;
     &lt;PT_ATTRIBUTE_TYPE&gt;{ data( $entity/dcl:attributeType/ns1:ProductAreaAttributeType/ns1:attributeType ) }&lt;/PT_ATTRIBUTE_TYPE&gt;
     &lt;PT_ALGORITHM_SEQ&gt;{ data( $entity/dcl:algorithmSequence ) }&lt;/PT_ALGORITHM_SEQ&gt;
     &lt;PT_ID_AREA&gt;{ data( $entity/dcl:idProductArea ) }&lt;/PT_ID_AREA&gt;
     
     &lt;PT_ID_AREA_ATTRIBUTES&gt;{ data( $entity/dcl:idProductAreaAttributes ) }&lt;/PT_ID_AREA_ATTRIBUTES&gt;     
     &lt;PT_USER_CHANGE_SKP&gt;{ data( $entity/dcl:userChangeSKP) }&lt;/PT_USER_CHANGE_SKP&gt;
     &lt;PT_DATE_CHANGE&gt;{ data( $entity/dcl:dateChange) }&lt;/PT_DATE_CHANGE&gt;     
&lt;/FML32&gt;
};
declare variable $entity as element(dcl:ProductAreaAttributes) external;
xf:saveProdAreaAttribute_req($entity)</con:xquery>
</con:xqueryEntry>