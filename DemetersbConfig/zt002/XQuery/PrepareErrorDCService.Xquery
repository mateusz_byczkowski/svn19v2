<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:description>Version.$1.2011-04-06</con:description>
    <con:xquery><![CDATA[declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare variable $body external;
declare variable $headerCache external;
declare namespace urn3="urn:be.services.dcl";

declare function local:fault($faultString as xs:string, $detail as element()?) as element(soap-env:Fault) {
		<soap-env:Fault>
			<faultcode>soapenv:Server.userException</faultcode> 
			<faultstring>{ $faultString }</faultstring> 
			<detail>{ $detail }</detail>
		</soap-env:Fault>
};

declare function Itemerr($errorCode1 as xs:string, $errorCode2 as xs:string,
        $error as element()?,$errorCodeFromBS as xs:string, $messageID as xs:string) as element()
{
  for $x in $error
  return		
  <err:exceptionItem>
	<err:errorCode1>{ $errorCode1 }</err:errorCode1>
	<err:errorCode2>{ $errorCode2 }</err:errorCode2>
        {
           if (string-length($errorCodeFromBS)>0) 
               then   <err:errorDescription>{ concat($errorCodeFromBS," : ",concat(translate($x, "za§˘’Ťg‘žl†ja¦¤", "zażółćgęśląjaźń"), concat(' (',concat($messageID, ')')))) }</err:errorDescription>
               else    
              <err:errorDescription>{ concat(translate($x, "za§˘’Ťg‘žl†ja¦¤", "zażółćgęśląjaźń"), concat(' (',concat($messageID, ')'))) }</err:errorDescription>
        }
   </err:exceptionItem>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string,
        $errorDescription as xs:string?, $errorCodeFromBS as xs:string, $messageID as xs:string) as element(soap-env:Fault) 
{
    <soap-env:Fault>
			<faultcode>soapenv:Server.userException</faultcode> 
			<faultstring>err:ServiceFailException</faultstring> 
			<detail>
(: PT058 begin :)
	(:		{	
                element err:ServiceFailException {Itemerr($errorCode1, $errorCode2, $errorDescription, $errorCodeFromBS, $messageID)} </detail>}
:)
<err:ServiceFailException>
{
  for $x in $errorDescription
  return	
  <err:exceptionItem>
	<err:errorCode1>{ $errorCode1 }</err:errorCode1>
	<err:errorCode2>{ $errorCode2 }</err:errorCode2>
        {
           if (string-length($errorCodeFromBS)>0) 
               then   <err:errorDescription>{ concat($errorCodeFromBS," : ",concat(translate($x, "za§˘’Ťg‘žl†ja¦¤", "zażółćgęśląjaźń"), concat(' (',concat($messageID, ')')))) }</err:errorDescription>
               else    
              <err:errorDescription>{ concat(translate($x, "za§˘’Ťg‘žl†ja¦¤", "zażółćgęśląjaźń"), concat(' (',concat($messageID, ')'))) }</err:errorDescription>
        }
   </err:exceptionItem>
}

</err:ServiceFailException>
</detail>
(: PT058 end :)
	</soap-env:Fault>
};

<soap-env:Body>
	{
		let $reason := "11"
		let $urcode := "1000"
		
		let $errorDescription:=data($body/FML32/NF_ERROR_DESCRIPTION[1])
		let $errorCode:=data($body/FML32/NF_ERROR_CODE[1])
        let $messageID := data($headerCache/urn3:header/urn3:msgHeader/urn3:msgId)

		return
                       if (string-length($errorDescription)>0 or string-length($errorCode)>0) then
                         local:errors($reason, $urcode, $errorDescription,$errorCode, $messageID)
                       else if (string-length(data($body/FML32/DC_OPIS_BLEDU[1]))>0) then
                         local:errors($reason, $urcode, data($body/FML32/DC_OPIS_BLEDU),"", $messageID)
                       else()
                         }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>