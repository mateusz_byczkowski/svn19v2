<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-08</con:description>
  <con:xquery>(: Change Log
v.1.0 2011-03-25 PKL PT58 Content Streaming

 :)

declare namespace x = "http://bzwbk.com/services/prime/";
declare namespace m = "http://schemas.datacontract.org/2004/07/SzrekLibrary.Struktures";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductDetailResponse($bdy as element(x:PRIMEGetCustProductDetailResponse))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_KARTA)
					then &lt;fml:B_KARTA&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_KARTA) }&lt;/fml:B_KARTA&gt;
					else ()
			}
                        {
 				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_KOD_RACH)
					then &lt;fml:B_KOD_RACH&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_KOD_RACH) }&lt;/fml:B_KOD_RACH&gt;
					else ()
                        }
                &lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ 
 if ($body/x:PRIMEGetCustProductDetailResponse)
    then xf:mapPRIMEGetCustProductDetailResponse($body/x:PRIMEGetCustProductDetailResponse) 
    else  
 if ($body/fml:FML32)
    then $body/fml:FML32
    else  ()
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>