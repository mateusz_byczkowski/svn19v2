<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2011-04-08</con:description>
  <con:xquery>(: Change Log
v.1.1 2009-08-26 PKL NP1995 Dodanie pól ProductStat i ProductStatusCode
v.1.2 2009-11-02 PKL T41479 Ukrywanie danych kart dodatkowych
v.1.3 2010-08-17 PKL T50428 Dodanie NF_PRODUD_SORCEPRODUCTCODE dla Prime, Egerii i Strl (identyfikacja na IV poziomie DP - NP2042)
v.1.4 2011-03-25 PKL PT58 Content Streaming
v.1.5 2011-04-08 PKL NP2233 Zmiana wyznaczania rdscode dla kart kredytowych
:)

declare namespace x = "http://bzwbk.com/services/prime/";
declare namespace m = "http://schemas.datacontract.org/2004/07/SzrekLibrary.Struktures";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $getCekeAccResponse as element(FML32) external; (:1.4:)


declare function xf:mapPRIMEGetCustProductDetailResponse($bdy as element(x:PRIMEGetCustProductDetailResponse),  $getCekeAccData as element(FML32) )
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
(: Pola wspólne, zwracane dla rachunków,  kart głównych i dodatkowych: :)		
	        {
				if($bdy/x:PRIMEGetCustProductDetailResult/m:DC_NR_RACHUNKU)
					then &lt;fml:DC_NR_RACHUNKU&gt;{ substring(data($bdy/x:PRIMEGetCustProductDetailResult/m:DC_NR_RACHUNKU),2) }&lt;/fml:DC_NR_RACHUNKU&gt;
					else ()
			}	                
			{
				if($bdy/x:PRIMEGetCustProductDetailResult/m:DC_NR_ALTERNATYWNY)
					then &lt;fml:DC_NR_RACHUNKU_W_SYSTEMIE&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:DC_NR_ALTERNATYWNY) }&lt;/fml:DC_NR_RACHUNKU_W_SYSTEMIE&gt;
					else ()
			}
 			{
				if($bdy/x:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT1)
					then &lt;fml:CI_PRODUCT_ATT1&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT1) }&lt;/fml:CI_PRODUCT_ATT1&gt;
					else ()
			}
            &lt;fml:CI_PRODUCT_ATT2/&gt;
            &lt;fml:CI_PRODUCT_ATT3/&gt;
            &lt;fml:CI_PRODUCT_ATT4/&gt;
            &lt;fml:CI_PRODUCT_ATT5/&gt;
(:1.3:)  (:1.5 &lt;fml:NF_PRODUD_SORCEPRODUCTCODE/&gt; :)
(:1.5 Start:)
                       {
                                  if ($bdy/x:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT4 and $bdy/x:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT1)
                                           then &lt;fml:NF_PRODUD_SORCEPRODUCTCODE&gt;{concat(data($bdy/x:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT4), data($bdy/x:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_ATT1))}&lt;/fml:NF_PRODUD_SORCEPRODUCTCODE&gt;
                                           else &lt;fml:NF_PRODUD_SORCEPRODUCTCODE nil="true" /&gt;
                       }
(:1.5 Koniec:)


			{
				if($bdy/x:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_NAME_ORYGINAL)
					then &lt;fml:CI_PRODUCT_NAME_ORYGINAL&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:CI_PRODUCT_NAME_ORYGINAL) }&lt;/fml:CI_PRODUCT_NAME_ORYGINAL&gt;
					else ()
			}
			{
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_KARTA)
					then &lt;fml:B_KARTA&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_KARTA) }&lt;/fml:B_KARTA&gt;
					else ()
			}
(: NP1995 początek :)         
            {
				if($bdy/x:PRIMEGetCustProductDetailResult/m:NF_PRODUCT_STAT)
					then &lt;fml:CI_STATUS_AKTYWNOSCI&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:NF_PRODUCT_STAT) }&lt;/fml:CI_STATUS_AKTYWNOSCI&gt;
					else ()
			}	
	        {
				if($bdy/x:PRIMEGetCustProductDetailResult/m:NF_PRODUCT_STATUSCODE)
					then &lt;fml:CI_STATUS&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:NF_PRODUCT_STATUSCODE) }&lt;/fml:CI_STATUS&gt;
					else ()
            }	

(: NP1995 koniec :) 	                			
                                               {
		   		if($bdy/x:PRIMEGetCustProductDetailResult/m:B_NR_RACH)
					then &lt;fml:B_NR_RACH&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_NR_RACH) }&lt;/fml:B_NR_RACH&gt;
					else ()
                                                }
                                                {
 				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_KOD_RACH)
					then &lt;fml:B_KOD_RACH&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_KOD_RACH) }&lt;/fml:B_KOD_RACH&gt;
					else ()
                                                }
                                                {
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_POJEDYNCZY)
					then &lt;fml:B_POJEDYNCZY&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_POJEDYNCZY) }&lt;/fml:B_POJEDYNCZY&gt;
					else ()
                                                 }
                                                 {
				if($bdy/x:PRIMEGetCustProductDetailResult/m:CI_WLASCICIEL )
					then &lt;fml:CI_WLASCICIEL&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:CI_WLASCICIEL ) }&lt;/fml:CI_WLASCICIEL &gt;
					else ()
                                                  }

(: Pola zwracane tylko dla kart głównych: :)
{   if ($bdy/x:PRIMEGetCustProductDetailResult/m:B_KARTA and data($bdy/x:PRIMEGetCustProductDetailResult/m:B_KARTA)=1) then ( 
				if($bdy/x:PRIMEGetCustProductDetailResult/m:DC_ODDZIAL)
					then &lt;fml:DC_ODDZIAL&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:DC_ODDZIAL) }&lt;/fml:DC_ODDZIAL&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:CI_TYP_ADRESU)
					then &lt;fml:CI_TYP_ADRESU&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:CI_TYP_ADRESU) }&lt;/fml:CI_TYP_ADRESU&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_KOD_WALUTY)
					then &lt;fml:B_KOD_WALUTY&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_KOD_WALUTY) }&lt;/fml:B_KOD_WALUTY&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_SALDO)
					then &lt;fml:B_SALDO&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_SALDO) }&lt;/fml:B_SALDO&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_DOST_SRODKI)
					then &lt;fml:B_DOST_SRODKI&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_DOST_SRODKI) }&lt;/fml:B_DOST_SRODKI&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_BLOKADA)
					then &lt;fml:B_BLOKADA&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_BLOKADA) }&lt;/fml:B_BLOKADA&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_DATA_OST_OPER)
					then &lt;fml:B_DATA_OST_OPER&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_DATA_OST_OPER) }&lt;/fml:B_DATA_OST_OPER&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_LIMIT1)
					then &lt;fml:B_LIMIT1&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_LIMIT1) }&lt;/fml:B_LIMIT1&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_OPROC1)
					then &lt;fml:B_OPROC1&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_OPROC1) }&lt;/fml:B_OPROC1&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_OSTATNIEJ_KAP)
					then &lt;fml:B_D_OSTATNIEJ_KAP&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_OSTATNIEJ_KAP) }&lt;/fml:B_D_OSTATNIEJ_KAP&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_NASTEPNEJ_KAP)
					then &lt;fml:B_D_NASTEPNEJ_KAP&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_NASTEPNEJ_KAP) }&lt;/fml:B_D_NASTEPNEJ_KAP&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_KONCA_LIMITU)
					then &lt;fml:B_D_KONCA_LIMITU&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_KONCA_LIMITU) }&lt;/fml:B_D_KONCA_LIMITU&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_ODS_POP_OKR)
					then &lt;fml:B_ODS_POP_OKR&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_ODS_POP_OKR) }&lt;/fml:B_ODS_POP_OKR&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_OST_ZEST)
					then &lt;fml:B_D_OST_ZEST&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_OST_ZEST) }&lt;/fml:B_D_OST_ZEST&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_BIEZ_MIN)
					then &lt;fml:B_BIEZ_MIN&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_BIEZ_MIN) }&lt;/fml:B_BIEZ_MIN&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_SPLATA_MIN)
					then &lt;fml:B_SPLATA_MIN&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_SPLATA_MIN) }&lt;/fml:B_SPLATA_MIN&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_DATA_B_PLAT)
					then &lt;fml:B_DATA_B_PLAT&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_DATA_B_PLAT) }&lt;/fml:B_DATA_B_PLAT&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_NAST_ZEST)
					then &lt;fml:B_D_NAST_ZEST&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_NAST_ZEST) }&lt;/fml:B_D_NAST_ZEST&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_KK_EXP_DATE)
					then &lt;fml:B_KK_EXP_DATE&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_KK_EXP_DATE) }&lt;/fml:B_KK_EXP_DATE&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_KREDYT_PRZYZN)
					then &lt;fml:B_KREDYT_PRZYZN&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_KREDYT_PRZYZN) }&lt;/fml:B_KREDYT_PRZYZN&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_ODS_SKAP_AKT)
					then &lt;fml:B_ODS_SKAP_AKT&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_ODS_SKAP_AKT) }&lt;/fml:B_ODS_SKAP_AKT&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_OST_SPLATY)
					then &lt;fml:B_D_OST_SPLATY&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_D_OST_SPLATY) }&lt;/fml:B_D_OST_SPLATY&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:DC_DATA_URUCHOMIENIA)
					then &lt;fml:DC_DATA_URUCHOMIENIA&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:DC_DATA_URUCHOMIENIA) }&lt;/fml:DC_DATA_URUCHOMIENIA&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:DC_DATA_PLATNOSCI)
					then &lt;fml:DC_DATA_PLATNOSCI&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:DC_DATA_PLATNOSCI) }&lt;/fml:DC_DATA_PLATNOSCI&gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:CI_WSPOLPOSIADACZ )
					then &lt;fml:CI_WSPOLPOSIADACZ&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:CI_WSPOLPOSIADACZ ) }&lt;/fml:CI_WSPOLPOSIADACZ &gt;
					else ()
                ,
				if($bdy/x:PRIMEGetCustProductDetailResult/m:CI_PELNOMOCNIK)
					then &lt;fml:CI_PELNOMOCNIK&gt;{ data($bdy/x:PRIMEGetCustProductDetailResult/m:CI_PELNOMOCNIK) }&lt;/fml:CI_PELNOMOCNIK&gt;
					else ()
                ,
			&lt;fml:CI_RACHUNEK_ADR_ALT&gt;N&lt;/fml:CI_RACHUNEK_ADR_ALT&gt;,

(:v.1.4 start:)
  $getCekeAccData/B_SALDO,
  $getCekeAccData/B_DOST_SRODKI,
  &lt;B_LIMIT1&gt;{data($getCekeAccData/B_LIMIT2)}&lt;/B_LIMIT1&gt;,
  $getCekeAccData/B_BLOKADA,
  $getCekeAccData/B_DATA_B_PLAT,
  $getCekeAccData/B_ODS_POP_OKR,
  $getCekeAccData/B_D_OST_ZEST,
  $getCekeAccData/B_ZAL_MIN , 
  $getCekeAccData/B_BIEZ_MIN ,
  $getCekeAccData/B_SPLATA_MIN, 
  $getCekeAccData/B_SPLATA_RAZEM,
  $getCekeAccData/B_D_NAST_ZEST,
  $getCekeAccData/B_OPROC1,
  $getCekeAccData/B_KOD_WALUTY,
  $getCekeAccData/B_D_OTWARCIA,
  $getCekeAccData/B_DATA_OST_OPER
(:v.1.4 Koniec:)

) else (: koniec sekcji pól zwracanych tylko dla kart głównych :)
  ( (: pola z wyczyszczoną wartością zwracane dla kart dodatkowych :) 
            &lt;fml:B_DATA_OST_OPER/&gt;,
            &lt;fml:B_KK_EXP_DATE/&gt;,
             &lt;fml:CI_RACHUNEK_ADR_ALT/&gt;,
            &lt;fml:B_SALDO/&gt;,
            &lt;fml:B_DOST_SRODKI/&gt;,
            &lt;fml:B_LIMIT1/&gt;,
            &lt;fml:B_BLOKADA/&gt;,
            &lt;fml:B_DATA_B_PLAT/&gt;,
            &lt;fml:B_ODS_POP_OKR/&gt;,
            &lt;fml:B_D_OST_ZEST/&gt;,
            &lt;fml:B_ZAL_MIN/&gt;,
            &lt;fml:B_BIEZ_MIN/&gt;,
            &lt;fml:B_SPLATA_MIN/&gt;,
            &lt;fml:B_SPLATA_RAZEM/&gt;,
            &lt;fml:B_D_NAST_ZEST/&gt;,
            &lt;fml:B_OPROC1/&gt;,
            &lt;fml:B_KOD_WALUTY/&gt;,
            &lt;fml:B_D_OTWARCIA/&gt; 
         (: &lt;fml:DC_ODDZIAL/&gt;,      :)
         (: &lt;fml:CI_TYP_ADRESU/&gt;, :)
         (: &lt;fml:B_D_OSTATNIEJ_KAP/&gt;, :)
         (: &lt;fml:B_D_NASTEPNEJ_KAP/&gt;, :)
         (: &lt;fml:B_D_KONCA_LIMITU/&gt;,   :)
          (: &lt;fml:B_KREDYT_PRZYZN/&gt;, :)
         (: &lt;fml:B_ODS_SKAP_AKT/&gt;, :)
         (: &lt;fml:B_D_OST_SPLATY/&gt;, :)
         (: &lt;fml:DC_DATA_URUCHOMIENIA/&gt;, :)
         (: &lt;fml:DC_DATA_PLATNOSCI/&gt;, :)
         (: &lt;fml:CI_WSPOLPOSIADACZ/&gt;, :)
         (: &lt;fml:CI_PELNOMOCNIK/&gt;, :)
     )
  }

&lt;/fml:FML32&gt;
};


let $getCekeAccData:=$getCekeAccResponse

return

&lt;soap-env:Body&gt;
{ 
 if ($body/x:PRIMEGetCustProductDetailResponse)
    then 
        xf:mapPRIMEGetCustProductDetailResponse($body/x:PRIMEGetCustProductDetailResponse, $getCekeAccData)
    else  
 if ($body/fml:FML32)
    then $body/fml:FML32
    else  ()
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>