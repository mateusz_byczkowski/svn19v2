<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(: Change log 
v.1.1 2010-08-17 PKLI T50428 Dodanie NF_PRODUD_SORCEPRODUCTCODE dla Prime, Egerii i Strl (identyfikacja na IV poziomie DP - NP2042)

:)

declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapEGERIAGetCustProdDetResponse($bdy as element(fml:FML32))
	as element()*{
  if ($bdy/fml:EG_NR_UMOWY)
             then &lt;fml:DC_NR_RACHUNKU&gt;{data($bdy/fml:EG_NR_UMOWY)}&lt;/fml:DC_NR_RACHUNKU&gt;
             else &lt;fml:DC_NR_RACHUNKU nill="true"/&gt;,
       if ($bdy/fml:EG_KOD_PRODUKTU)
             then &lt;fml:CI_PRODUCT_ATT1&gt;{data($bdy/fml:EG_KOD_PRODUKTU)}&lt;/fml:CI_PRODUCT_ATT1&gt;
             else &lt;fml:CI_PRODUCT_ATT1 nill="true"/&gt;,
       &lt;fml:CI_PRODUCT_ATT2 nil="true"/&gt;,
       &lt;fml:CI_PRODUCT_ATT3 nil="true"/&gt;,
       &lt;fml:CI_PRODUCT_ATT4 nil="true"/&gt;,
       &lt;fml:CI_PRODUCT_ATT5 nil="true"/&gt;,
       &lt;fml:NF_PRODUD_SORCEPRODUCTCODE nil="true"/&gt;, (:1.1:)
       if ($bdy/fml:EG_RODZAJ_UMOWY)
             then &lt;fml:CI_PRODUCT_NAME_ORYGINAL&gt;{data($bdy/fml:EG_RODZAJ_UMOWY)}&lt;/fml:CI_PRODUCT_NAME_ORYGINAL&gt;
             else &lt;fml:CI_PRODUCT_NAME_ORYGINAL nill="true"/&gt;,
       if ($bdy/fml:EG_KOD_AKTYWNOSCI)
             then &lt;fml:CI_STATUS_AKTYWNOSCI&gt;{data($bdy/fml:EG_KOD_AKTYWNOSCI)}&lt;/fml:CI_STATUS_AKTYWNOSCI&gt;
             else &lt;fml:CI_STATUS_AKTYWNOSCI nill="true"/&gt;,
       if ($bdy/fml:EG_BIEZ_ETAP_UMOWY)
             then &lt;fml:CI_STATUS&gt;{data($bdy/fml:EG_BIEZ_ETAP_UMOWY)}&lt;/fml:CI_STATUS&gt;
             else &lt;fml:CI_STATUS nill="true"/&gt;,
  $bdy/fml:EG_KOD_PRODUKTU,
  $bdy/fml:EG_NR_UMOWY,
  $bdy/fml:EG_ID_SPOLKI,
  $bdy/fml:EG_KOD_AKTYWNOSCI,
  $bdy/fml:EG_BIEZ_ETAP_UMOWY,
  $bdy/fml:EG_DATA_OTWARCIA_UMOWY,
  $bdy/fml:EG_DATA_KONCA_UMOWY,
  $bdy/fml:EG_OKRES_UMOWY,
  $bdy/fml:EG_KOD_WALUTY,
  $bdy/fml:EG_RODZAJ_UMOWY,
  $bdy/fml:EG_PROCEDURA,
  $bdy/fml:EG_PRZEDMIOT_UMOWY,
  $bdy/fml:EG_TYP_RATY,
  $bdy/fml:EG_POCZ_PORTFEL_UMOWY_NET,
  $bdy/fml:EG_SALDO_UMOWY_NET,
  $bdy/fml:EG_PORTFEL_NET_1Q,
  $bdy/fml:EG_PORTFEL_NET_2Q,
  $bdy/fml:EG_PORTFEL_NET_3Q,
  $bdy/fml:EG_PORTFEL_NET_4Q,
  $bdy/fml:EG_P_PORTFEL_NET_1Q,
  $bdy/fml:EG_P_PORTFEL_NET_2Q,
  $bdy/fml:EG_P_PORTFEL_NET_3Q,
  $bdy/fml:EG_P_PORTFEL_NET_4Q,
  $bdy/fml:EG_WART_NET_PRZEDM_UMOWY,
  $bdy/fml:EG_PROC_WPLATA_WLASNA,
  $bdy/fml:EG_PROC_WART_KONCOWA,
  $bdy/fml:EG_PROC_MARZA_SWWR,
  $bdy/fml:EG_PROC_SPL_UMOWY,
  $bdy/fml:EG_PROC_SPL_PRZEDM_UMOWY,
  $bdy/fml:EG_KURS_WAL_DATA,
  $bdy/fml:EG_OBC_NET_POPRZ_ROK_KAP,
  $bdy/fml:EG_OBC_NET_POPRZ_ROK_ODS,
  $bdy/fml:EG_OBC_NET_POPRZ_ROK_SUMA,
  $bdy/fml:EG_OBC_NET_BIEZ_ROK_KAP,
  $bdy/fml:EG_OBC_NET_BIEZ_ROK_ODS,
  $bdy/fml:EG_OBC_NET_BIEZ_ROK_SUMA,
  $bdy/fml:EG_OBC_NET_NAST_ROK_KAP,
  $bdy/fml:EG_OBC_NET_NAST_ROK_ODS,
  $bdy/fml:EG_OBC_NET_NAST_ROK_SUMA,

  $bdy/fml:EG_SY_KOD_KLIENTA,
  $bdy/fml:EG_SY_OPIEKUN_KLIENTA,
  $bdy/fml:EG_SY_SUMA_POCZ_POR_NET,
  $bdy/fml:EG_SY_SUMA_BIEZ_POR_NET,
  $bdy/fml:EG_SY_SUMA_PORTF_NET_1Q,
  $bdy/fml:EG_SY_SUMA_PORTF_NET_2Q,
  $bdy/fml:EG_SY_SUMA_PORTF_NET_3Q,
  $bdy/fml:EG_SY_SUMA_PORTF_NET_4Q,
  $bdy/fml:EG_SY_P_SUMA_PORTF_NET_1Q,
  $bdy/fml:EG_SY_P_SUMA_PORTF_NET_2Q,
  $bdy/fml:EG_SY_P_SUMA_PORTF_NET_3Q,
  $bdy/fml:EG_SY_P_SUMA_PORTF_NET_4Q,
  $bdy/fml:EG_SY_SUM_WAR_NET_PRZ_UM,
  $bdy/fml:EG_SY_PROC_SREDNI_SPL_UM,
  $bdy/fml:EG_SY_OBC_NET_POP_ROK_KAP,
  $bdy/fml:EG_SY_OBC_NET_POP_ROK_ODS,
  $bdy/fml:EG_SY_OBC_NET_POP_ROK_SUM,
  $bdy/fml:EG_SY_OBC_NET_BIE_ROK_KAP,
  $bdy/fml:EG_SY_OBC_NET_BIE_ROK_ODS,
  $bdy/fml:EG_SY_OBC_NET_BIE_ROK_SUM,
  $bdy/fml:EG_SY_OBC_NET_NAS_ROK_KAP,
  $bdy/fml:EG_SY_OBC_NET_NAS_ROK_ODS,
  $bdy/fml:EG_SY_OBC_NET_NAS_ROK_SUM,
  $bdy/fml:EG_SY_LFPO_OBCIAZ_KAP_P,
  $bdy/fml:EG_SY_LFPO_OBCIAZ_ODS_P,
  $bdy/fml:EG_SY_LFPO_OBCIAZ_SUMA_P,
  $bdy/fml:EG_SY_LFPO_OBCIAZ_KAP_C,
  $bdy/fml:EG_SY_LFPO_OBCIAZ_ODS_C,
  $bdy/fml:EG_SY_LFPO_OBCIAZ_SUMA_C,
  $bdy/fml:EG_SY_LFPO_OBCIAZ_KAP_N,
  $bdy/fml:EG_SY_LFPO_OBCIAZ_ODS_N,
  $bdy/fml:EG_SY_LFPO_OBCIAZ_SUMA_N,
  $bdy/fml:EG_SY_PROC_SRED_SPL_PRZED,
  $bdy/fml:EG_SY_PROC_SRED_MARZ_SWWR,
  $bdy/fml:EG_SY_BIEZ_ZALEGL_KWOTA,
  $bdy/fml:EG_SY_BIEZ_ZALEGL_DNI,
  $bdy/fml:EG_SY_MAX_OPOZ_RATING_DNI,
  $bdy/fml:EG_SY_MAX_OPOZ_DL_REZ_DNI,
  $bdy/fml:EG_SY_KURSY_WAL_PLN
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
  &lt;fml:FML32&gt;
     {
      xf:mapEGERIAGetCustProdDetResponse($body/fml:FML32)
     }
  &lt;/fml:FML32&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>