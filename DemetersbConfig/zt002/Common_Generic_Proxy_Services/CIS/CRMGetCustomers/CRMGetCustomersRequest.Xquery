<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustomersRequest($req as element(m:CRMGetCustomersRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:ZakresWyszukiwania)
					then &lt;fml:CI_ZAKRES_WYSZUKIWANIA&gt;{ data($req/m:ZakresWyszukiwania) }&lt;/fml:CI_ZAKRES_WYSZUKIWANIA&gt;
					else ()
			}
                        {
                                for $v in $req/m:IdSpolki
                                return
				    &lt;fml:CI_ID_SPOLKI&gt;{ data($v) }&lt;/fml:CI_ID_SPOLKI&gt;
                        }
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON&gt;{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON&gt;
					else ()
			}
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL&gt;{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL&gt;
					else ()
			}
			{
				if($req/m:Nip)
					then &lt;fml:DC_NIP&gt;{ data($req/m:Nip) }&lt;/fml:DC_NIP&gt;
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then &lt;fml:DC_NUMER_PASZPORTU&gt;{ data($req/m:NumerPaszportu) }&lt;/fml:DC_NUMER_PASZPORTU&gt;
					else ()
			}
			{
				if($req/m:Nik)
					then &lt;fml:CI_NIK&gt;{ data($req/m:Nik) }&lt;/fml:CI_NIK&gt;
					else ()
			}
			{
				if($req/m:Nazwa)
					then &lt;fml:DC_NAZWA&gt;{ data($req/m:Nazwa) }&lt;/fml:DC_NAZWA&gt;
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO&gt;{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO&gt;
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then &lt;fml:DC_MIASTO_DANE_PODST&gt;{ data($req/m:MiastoDanePodst) }&lt;/fml:DC_MIASTO_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then &lt;fml:DC_KOD_POCZTOWY_DANE_PODST&gt;{ data($req/m:KodPocztowyDanePodst) }&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU&gt;{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU&gt;
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA&gt;{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA&gt;
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI&gt;{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI&gt;
					else ()
			}
			{
				if($req/m:Sortowanie)
					then &lt;fml:CI_SORTOWANIE&gt;{ data($req/m:Sortowanie) }&lt;/fml:CI_SORTOWANIE&gt;
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA&gt;{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA&gt;
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then &lt;fml:DC_ULICA_DANE_PODST&gt;{ data($req/m:UlicaDanePodst) }&lt;/fml:DC_ULICA_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:NrTelefonu)
					then &lt;fml:DC_NR_TELEFONU&gt;{ data($req/m:NrTelefonu) }&lt;/fml:DC_NR_TELEFONU&gt;
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE&gt;{ data($req/m:Imie) }&lt;/fml:DC_IMIE&gt;
					else ()
			}
			{
				if($req/m:Ekd)
					then &lt;fml:DC_EKD&gt;{ data($req/m:Ekd) }&lt;/fml:DC_EKD&gt;
					else ()
			}
			{
				if($req/m:Spw)
					then &lt;fml:DC_SPW&gt;{ data($req/m:Spw) }&lt;/fml:DC_SPW&gt;
					else ()
			}
			{
				if($req/m:Decyl)
					then &lt;fml:CI_DECYL&gt;{ data($req/m:Decyl) }&lt;/fml:CI_DECYL&gt;
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				if($req/m:JednostkaKorporacyjna)
					then &lt;fml:DC_JEDNOSTKA_KORPORACYJNA&gt;{ data($req/m:JednostkaKorporacyjna) }&lt;/fml:DC_JEDNOSTKA_KORPORACYJNA&gt;
					else ()
			}
			{
				if($req/m:DataUrodzenia)
					then &lt;fml:DC_DATA_URODZENIA&gt;{ data($req/m:DataUrodzenia) }&lt;/fml:DC_DATA_URODZENIA&gt;
					else ()
			}
                        {
                               if($req/m:Sterowanie)
                                        then &lt;fml:CI_STEROWANIE&gt;{ data($req/m:Sterowanie) }&lt;/fml:CI_STEROWANIE&gt;
                                        else ()
                        }
                        {
                               if($req/m:NumerPaczkiStr)
                                        then &lt;fml:CI_NUMER_PACZKI_STR&gt;{ data($req/m:NumerPaczkiStr) }&lt;/fml:CI_NUMER_PACZKI_STR&gt;
                                        else ()
                        }
                        {
                               if($req/m:Cont)
                                        then &lt;fml:CI_CONT&gt;{ data($req/m:Cont) }&lt;/fml:CI_CONT&gt;
                                        else ()
                        }
			{
				if($req/m:DokTozsamosci)
					then &lt;fml:CI_DOK_TOZSAMOSCI&gt;{ data($req/m:DokTozsamosci) }&lt;/fml:CI_DOK_TOZSAMOSCI&gt;
					else ()
			}
			{
				if($req/m:SeriaNrDok)
					then &lt;fml:CI_SERIA_NR_DOK&gt;{ data($req/m:SeriaNrDok) }&lt;/fml:CI_SERIA_NR_DOK&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCustomersRequest($body/m:CRMGetCustomersRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>