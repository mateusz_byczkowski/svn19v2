<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCusGiodoResponse($fml as element(fml:FML32))
	as element(m:CRMGetCusGiodoResponse) {
		&lt;m:CRMGetCusGiodoResponse&gt;
			{

				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $CI_ID_SPOLKI := $fml/fml:CI_ID_SPOLKI
				let $CI_STATUS_GIODO := $fml/fml:CI_STATUS_GIODO
				let $CI_NR_KOM := $fml/fml:CI_NR_KOM
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:CRMGetCusGiodoStatus&gt;
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta&gt;{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta&gt;
						else ()
					}
					{
						if($CI_ID_SPOLKI[$p])
							then &lt;m:IdSpolki&gt;{ data($CI_ID_SPOLKI[$p]) }&lt;/m:IdSpolki&gt;
						else ()
					}
					{
						if($CI_STATUS_GIODO[$p])
							then &lt;m:StatusGiodo&gt;{ data($CI_STATUS_GIODO[$p]) }&lt;/m:StatusGiodo&gt;
						else ()
					}
					{
						if($CI_NR_KOM[$p])
							then &lt;m:NrKom&gt;{ data($CI_NR_KOM[$p]) }&lt;/m:NrKom&gt;
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then &lt;m:UdostepGrupa&gt;{ data($CI_UDOSTEP_GRUPA[$p]) }&lt;/m:UdostepGrupa&gt;
						else ()
					}
					&lt;/m:CRMGetCusGiodoStatus&gt;
			}

		&lt;/m:CRMGetCusGiodoResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCusGiodoResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>