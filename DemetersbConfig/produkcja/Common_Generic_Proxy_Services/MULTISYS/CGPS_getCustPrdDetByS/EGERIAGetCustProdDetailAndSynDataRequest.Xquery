<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapEGERIAGetCustProdDet($fml as element(fml:FML32))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($fml/fml:DC_NR_RACHUNKU)
					then &lt;fml:EG_NR_UMOWY&gt;{ data($fml/fml:DC_NR_RACHUNKU) }&lt;/fml:EG_NR_UMOWY&gt;
					else  ()
                         }
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapEGERIAGetCustProdDet($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>