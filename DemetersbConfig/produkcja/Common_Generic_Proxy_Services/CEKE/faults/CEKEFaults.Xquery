<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/ceke/faults/";
declare variable $fault external;

declare function local:fault($faultString as xs:string, $detail as xs:anyType) as element(soap-env:Fault) {
		&lt;soap-env:Fault&gt;
			&lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
			&lt;faultstring&gt;{ $faultString }&lt;/faultstring&gt; 
			&lt;detail&gt;{ $detail }&lt;/detail&gt;
		&lt;/soap-env:Fault&gt;
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string) as element()* {
	&lt;errorCode1&gt;{ $errorCode1 }&lt;/errorCode1&gt;,
	&lt;errorCode2&gt;{ $errorCode2 }&lt;/errorCode2&gt;,
	&lt;errorDescription/&gt;
};

&lt;soap-env:Body&gt;
{
	(: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
	let $reason := fn:substring-before(fn:substring-after(fn:substring-before($fault/ctx:reason, ":"), "("), ")")
	let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($fault/ctx:reason, ":"), ":"), ":")
	return
		if($reason = "13") then
			local:fault("com.bzwbk.services.ceke.faults.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode) })
		else if($reason = "11") then
			if($urcode = "9") then
				local:fault("com.bzwbk.services.ceke.faults.BadLoginException", element f:BadLoginException { local:errors($reason, $urcode) })
			else if($urcode = "13") then
				local:fault("com.bzwbk.services.ceke.faults.TransactionNotAllowedException", element f:TransactionNotAllowedException { local:errors($reason, $urcode) })
			else if($urcode = "26") then
				local:fault("com.bzwbk.services.ceke.faults.TransactionNotFoundException", element f:TransactionNotFoundException { local:errors($reason, $urcode) })
			else if($urcode = "30") then
				local:fault("com.bzwbk.services.ceke.faults.SystemException", element f:SystemException { local:errors($reason, $urcode) })
			else if($urcode = "79") then
				local:fault("com.bzwbk.services.ceke.faults.TokenBadResponseException", element f:TokenBadResponseException { local:errors($reason, $urcode) })
			else if($urcode = "91") then
				local:fault("com.bzwbk.services.ceke.faults.ShopNotFoundException", element f:ShopNotFoundException { local:errors($reason, $urcode) })
			else if($urcode = "140") then
				local:fault("com.bzwbk.services.ceke.faults.CardNotFoundException", element f:CardNotFoundException { local:errors($reason, $urcode) })
			else if($urcode = "141") then
				local:fault("com.bzwbk.services.ceke.faults.CardNotEnrolledException", element f:CardNotEnrolledException { local:errors($reason, $urcode) })
			else if($urcode = "142") then
				local:fault("com.bzwbk.services.ceke.faults.WrongCardOwnerException", element f:WrongCardOwnerException { local:errors($reason, $urcode) })
			else if($urcode = "144") then
				local:fault("com.bzwbk.services.ceke.faults.BinNot3DAllowedException", element f:BinNot3DAllowedException { local:errors($reason, $urcode) })
			else
				local:fault("com.bzwbk.services.cis.faults.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode) })
		else
			local:fault("com.bzwbk.services.ceke.faults.ServiceException", element f:ServiceException { local:errors($reason, $urcode) })
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>