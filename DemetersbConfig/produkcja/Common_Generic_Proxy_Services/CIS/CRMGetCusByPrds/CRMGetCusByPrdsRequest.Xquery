<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCusByPrdsRequest($req as element(m:CRMGetCusByPrdsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU&gt;{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU&gt;
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				for $v in $req/m:Decyl
				return
					&lt;fml:CI_DECYL&gt;{ data($v) }&lt;/fml:CI_DECYL&gt;
			}
			{
				for $v in $req/m:WarunekDecyl
				return
					&lt;fml:CI_WARUNEK_DECYL&gt;{ data($v) }&lt;/fml:CI_WARUNEK_DECYL&gt;
			}
			{
				for $v in $req/m:KodProduktu
				return
					&lt;fml:CI_KOD_PRODUKTU&gt;{ data($v) }&lt;/fml:CI_KOD_PRODUKTU&gt;
			}
			{
				for $v in $req/m:Posiada
				return
					&lt;fml:CI_POSIADA&gt;{ data($v) }&lt;/fml:CI_POSIADA&gt;
			}
			{
				for $v in $req/m:NazwaPola
				return
					&lt;fml:CI_NAZWA_POLA&gt;{ data($v) }&lt;/fml:CI_NAZWA_POLA&gt;
			}
			{
				for $v in $req/m:Operator
				return
					&lt;fml:CI_OPERATOR&gt;{ data($v) }&lt;/fml:CI_OPERATOR&gt;
			}
			{
				for $v in $req/m:Wartosc
				return
					&lt;fml:CI_WARTOSC&gt;{ data($v) }&lt;/fml:CI_WARTOSC&gt;
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI&gt;{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI&gt;
					else ()
			}
			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI&gt;{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCusByPrdsRequest($body/m:CRMGetCusByPrdsRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>