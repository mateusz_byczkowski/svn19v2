<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetViPorCustResponse($fml as element(fml:FML32))
	as element(m:CRMGetViPorCustResponse) {
		&lt;m:CRMGetViPorCustResponse&gt;
			{
				let $CI_NUMER_PACZKI := $fml/fml:CI_NUMER_PACZKI return
					if($CI_NUMER_PACZKI)
						then &lt;m:NumerPaczki&gt;{ data($CI_NUMER_PACZKI) }&lt;/m:NumerPaczki&gt;
						else ()
			}
			{
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST
				let $DC_NUMER_ODDZIALU := $fml/fml:DC_NUMER_ODDZIALU
				let $CI_STATUS_GIODO := $fml/fml:CI_STATUS_GIODO
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $CI_WIECEJ_ADDR_KORESP := $fml/fml:CI_WIECEJ_ADDR_KORESP
				let $DC_IMIE_I_NAZWISKO_ALT := $fml/fml:DC_IMIE_I_NAZWISKO_ALT
				let $DC_ULICA_ADRES_ALT := $fml/fml:DC_ULICA_ADRES_ALT
				let $DC_NR_POSES_LOKALU_ADRES_ALT := $fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT
				let $DC_MIASTO_ADRES_ALT := $fml/fml:DC_MIASTO_ADRES_ALT
				let $DC_KOD_POCZTOWY_ADRES_ALT := $fml/fml:DC_KOD_POCZTOWY_ADRES_ALT
				let $DC_NR_TELEFONU := $fml/fml:DC_NR_TELEFONU
				let $DC_NR_TELEF_KOMORKOWEGO := $fml/fml:DC_NR_TELEF_KOMORKOWEGO
				let $CI_NR_TELEFONU_SMS := $fml/fml:CI_NR_TELEFONU_SMS
				let $DC_ADRES_E_MAIL := $fml/fml:DC_ADRES_E_MAIL
				let $CI_RELACJA := $fml/fml:CI_RELACJA
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $CI_STATUS := $fml/fml:CI_STATUS
				let $CI_KLASA_OBSLUGI_AKT := $fml/fml:CI_KLASA_OBSLUGI_AKT
				let $CI_SKP_PRACOWNIKA_REJ := $fml/fml:CI_SKP_PRACOWNIKA_REJ
				let $CI_TYP_ZMIANY := $fml/fml:CI_TYP_ZMIANY
				let $CI_VIP := $fml/fml:CI_VIP
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:CRMGetViPorCustKlient&gt;
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta&gt;{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta&gt;
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko&gt;{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko&gt;
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie&gt;{ data($DC_IMIE[$p]) }&lt;/m:Imie&gt;
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna&gt;{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna&gt;
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI[$p])
							then &lt;m:KlasaObslugi&gt;{ data($CI_KLASA_OBSLUGI[$p]) }&lt;/m:KlasaObslugi&gt;
						else ()
					}
					{
						if($DC_ULICA_DANE_PODST[$p])
							then &lt;m:UlicaDanePodst&gt;{ data($DC_ULICA_DANE_PODST[$p]) }&lt;/m:UlicaDanePodst&gt;
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_DANE_PODST[$p])
							then &lt;m:NrPosesLokaluDanePodst&gt;{ data($DC_NR_POSES_LOKALU_DANE_PODST[$p]) }&lt;/m:NrPosesLokaluDanePodst&gt;
						else ()
					}
					{
						if($DC_MIASTO_DANE_PODST[$p])
							then &lt;m:MiastoDanePodst&gt;{ data($DC_MIASTO_DANE_PODST[$p]) }&lt;/m:MiastoDanePodst&gt;
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_DANE_PODST[$p])
							then &lt;m:KodPocztowyDanePodst&gt;{ data($DC_KOD_POCZTOWY_DANE_PODST[$p]) }&lt;/m:KodPocztowyDanePodst&gt;
						else ()
					}
					{
						if($DC_NUMER_ODDZIALU[$p])
							then &lt;m:NumerOddzialu&gt;{ data($DC_NUMER_ODDZIALU[$p]) }&lt;/m:NumerOddzialu&gt;
						else ()
					}
					{
						if($CI_STATUS_GIODO[$p])
							then &lt;m:StatusGiodo&gt;{ data($CI_STATUS_GIODO[$p]) }&lt;/m:StatusGiodo&gt;
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then &lt;m:UdostepGrupa&gt;{ data($CI_UDOSTEP_GRUPA[$p]) }&lt;/m:UdostepGrupa&gt;
						else ()
					}
					{
						if($CI_WIECEJ_ADDR_KORESP[$p])
							then &lt;m:WiecejAddrKoresp&gt;{ data($CI_WIECEJ_ADDR_KORESP[$p]) }&lt;/m:WiecejAddrKoresp&gt;
						else ()
					}
					{
						if($DC_IMIE_I_NAZWISKO_ALT[$p])
							then &lt;m:ImieINazwiskoAlt&gt;{ data($DC_IMIE_I_NAZWISKO_ALT[$p]) }&lt;/m:ImieINazwiskoAlt&gt;
						else ()
					}
					{
						if($DC_ULICA_ADRES_ALT[$p])
							then &lt;m:UlicaAdresAlt&gt;{ data($DC_ULICA_ADRES_ALT[$p]) }&lt;/m:UlicaAdresAlt&gt;
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_ADRES_ALT[$p])
							then &lt;m:NrPosesLokaluAdresAlt&gt;{ data($DC_NR_POSES_LOKALU_ADRES_ALT[$p]) }&lt;/m:NrPosesLokaluAdresAlt&gt;
						else ()
					}
					{
						if($DC_MIASTO_ADRES_ALT[$p])
							then &lt;m:MiastoAdresAlt&gt;{ data($DC_MIASTO_ADRES_ALT[$p]) }&lt;/m:MiastoAdresAlt&gt;
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_ADRES_ALT[$p])
							then &lt;m:KodPocztowyAdresAlt&gt;{ data($DC_KOD_POCZTOWY_ADRES_ALT[$p]) }&lt;/m:KodPocztowyAdresAlt&gt;
						else ()
					}
					{
						if($DC_NR_TELEFONU[$p])
							then &lt;m:NrTelefonu&gt;{ data($DC_NR_TELEFONU[$p]) }&lt;/m:NrTelefonu&gt;
						else ()
					}
					{
						if($DC_NR_TELEF_KOMORKOWEGO[$p])
							then &lt;m:NrTelefKomorkowego&gt;{ data($DC_NR_TELEF_KOMORKOWEGO[$p]) }&lt;/m:NrTelefKomorkowego&gt;
						else ()
					}
					{
						if($CI_NR_TELEFONU_SMS[$p])
							then &lt;m:NrTelefonuSms&gt;{ data($CI_NR_TELEFONU_SMS[$p]) }&lt;/m:NrTelefonuSms&gt;
						else ()
					}
					{
						if($DC_ADRES_E_MAIL[$p])
							then &lt;m:AdresEMail&gt;{ data($DC_ADRES_E_MAIL[$p]) }&lt;/m:AdresEMail&gt;
						else ()
					}
					{
						if($CI_RELACJA[$p])
							then &lt;m:Relacja&gt;{ data($CI_RELACJA[$p]) }&lt;/m:Relacja&gt;
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then &lt;m:IdPortfela&gt;{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela&gt;
						else ()
					}
					{
						if($CI_STATUS[$p])
							then &lt;m:Status&gt;{ data($CI_STATUS[$p]) }&lt;/m:Status&gt;
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI_AKT[$p])
							then &lt;m:KlasaObslugiAkt&gt;{ data($CI_KLASA_OBSLUGI_AKT[$p]) }&lt;/m:KlasaObslugiAkt&gt;
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA_REJ[$p])
							then &lt;m:SkpPracownikaRej&gt;{ data($CI_SKP_PRACOWNIKA_REJ[$p]) }&lt;/m:SkpPracownikaRej&gt;
						else ()
					}
					{
						if($CI_TYP_ZMIANY[$p])
							then &lt;m:TypZmiany&gt;{ data($CI_TYP_ZMIANY[$p]) }&lt;/m:TypZmiany&gt;
						else ()
					}
					{
						if($CI_VIP[$p])
							then &lt;m:Vip&gt;{ data($CI_VIP[$p]) }&lt;/m:Vip&gt;
						else ()
					}
					&lt;/m:CRMGetViPorCustKlient&gt;
			}
		&lt;/m:CRMGetViPorCustResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetViPorCustResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>