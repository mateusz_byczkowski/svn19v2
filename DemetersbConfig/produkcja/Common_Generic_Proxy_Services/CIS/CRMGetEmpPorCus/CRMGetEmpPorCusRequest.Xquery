<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetEmpPorCusRequest($req as element(m:CRMGetEmpPorCusRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:SkpPracownikaUpr)
					then &lt;fml:CI_SKP_PRACOWNIKA_UPR&gt;{ data($req/m:SkpPracownikaUpr) }&lt;/fml:CI_SKP_PRACOWNIKA_UPR&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetEmpPorCusRequest($body/m:CRMGetEmpPorCusRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>