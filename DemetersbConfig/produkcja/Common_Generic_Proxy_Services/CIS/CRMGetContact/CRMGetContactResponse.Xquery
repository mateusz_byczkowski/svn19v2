<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetContactResponse($fml as element(fml:FML32))
	as element(m:CRMGetContactResponse) {
		&lt;m:CRMGetContactResponse&gt;
			{
				if($fml/fml:CI_ID_KONT)
					then &lt;m:IdKont&gt;{ data($fml/fml:CI_ID_KONT) }&lt;/m:IdKont&gt;
					else ()
			}
			{
				if($fml/fml:CI_DATA_KONT)
					then &lt;m:DataKont&gt;{ data($fml/fml:CI_DATA_KONT) }&lt;/m:DataKont&gt;
					else ()
			}
			{
				if($fml/fml:CI_ID_ZWS)
					then &lt;m:IdZws&gt;{ data($fml/fml:CI_ID_ZWS) }&lt;/m:IdZws&gt;
					else ()
			}
			{
				if($fml/fml:CI_SKP_PRACOWNIKA)
					then &lt;m:SkpPracownika&gt;{ data($fml/fml:CI_SKP_PRACOWNIKA) }&lt;/m:SkpPracownika&gt;
					else ()
			}
			{
				if($fml/fml:DC_NUMER_KLIENTA)
					then &lt;m:NumerKlienta&gt;{ data($fml/fml:DC_NUMER_KLIENTA) }&lt;/m:NumerKlienta&gt;
					else ()
			}
			{
				if($fml/fml:CI_NAZWA_PELNA)
					then &lt;m:NazwaPelna&gt;{ data($fml/fml:CI_NAZWA_PELNA) }&lt;/m:NazwaPelna&gt;
					else ()
			}
			{
				if($fml/fml:DC_IMIE)
					then &lt;m:Imie&gt;{ data($fml/fml:DC_IMIE) }&lt;/m:Imie&gt;
					else ()
			}
			{
				if($fml/fml:DC_NAZWISKO)
					then &lt;m:Nazwisko&gt;{ data($fml/fml:DC_NAZWISKO) }&lt;/m:Nazwisko&gt;
					else ()
			}
			{
				if($fml/fml:CI_STATUS_GIODO)
					then &lt;m:StatusGiodo&gt;{ data($fml/fml:CI_STATUS_GIODO) }&lt;/m:StatusGiodo&gt;
					else ()
			}
			{
				if($fml/fml:CI_UDOSTEP_GRUPA)
					then &lt;m:UdostepGrupa&gt;{ data($fml/fml:CI_UDOSTEP_GRUPA) }&lt;/m:UdostepGrupa&gt;
					else ()
			}
			{
				if($fml/fml:CI_RODZAJ_KONT)
					then &lt;m:RodzajKont&gt;{ data($fml/fml:CI_RODZAJ_KONT) }&lt;/m:RodzajKont&gt;
					else ()
			}
			{
				if($fml/fml:CI_CEL_KONT)
					then &lt;m:CelKont&gt;{ data($fml/fml:CI_CEL_KONT) }&lt;/m:CelKont&gt;
					else ()
			}
			{
				if($fml/fml:CI_TYP_KONT)
					then &lt;m:TypKont&gt;{ data($fml/fml:CI_TYP_KONT) }&lt;/m:TypKont&gt;
					else ()
			}
			{
				if($fml/fml:CI_SPOS_KONT)
					then &lt;m:SposKont&gt;{ data($fml/fml:CI_SPOS_KONT) }&lt;/m:SposKont&gt;
					else ()
			}
			{
				if($fml/fml:CI_ID_KAMP)
					then &lt;m:IdKamp&gt;{ data($fml/fml:CI_ID_KAMP) }&lt;/m:IdKamp&gt;
					else ()
			}
			{
				if($fml/fml:CI_MIEJSCE_SPOTKANIA)
					then &lt;m:MiejsceSpotkania&gt;{ data($fml/fml:CI_MIEJSCE_SPOTKANIA) }&lt;/m:MiejsceSpotkania&gt;
					else ()
			}
			{
				if($fml/fml:CI_WYNIK_KONTAKTU)
					then &lt;m:WynikKontaktu&gt;{ data($fml/fml:CI_WYNIK_KONTAKTU) }&lt;/m:WynikKontaktu&gt;
					else ()
			}
			{
				if($fml/fml:CI_NOTAT_KONT)
					then &lt;m:NotatKont&gt;{ data($fml/fml:CI_NOTAT_KONT) }&lt;/m:NotatKont&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUKTY)
					then &lt;m:Produkty&gt;{ data($fml/fml:CI_PRODUKTY) }&lt;/m:Produkty&gt;
					else ()
			}
			{
				if($fml/fml:CI_ID_ZADANIA)
					then &lt;m:IdZadania&gt;{ data($fml/fml:CI_ID_ZADANIA) }&lt;/m:IdZadania&gt;
					else ()
			}
			{
				if($fml/fml:CI_NR_ODDZIALU)
					then &lt;m:NrOddzialu&gt;{ data($fml/fml:CI_NR_ODDZIALU) }&lt;/m:NrOddzialu&gt;
					else ()
			}
			{
				if($fml/fml:CI_ID_PORTFELA)
					then &lt;m:IdPortfela&gt;{ data($fml/fml:CI_ID_PORTFELA) }&lt;/m:IdPortfela&gt;
					else ()
			}
		&lt;/m:CRMGetContactResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetContactResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>