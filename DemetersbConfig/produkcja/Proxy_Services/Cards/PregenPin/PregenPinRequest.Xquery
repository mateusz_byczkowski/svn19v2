<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/PregenPinmessages/";
declare namespace xf = "http://bzwbk.com/services/PregenPinmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPregenPinRequest($req as element(m:PregenPinRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:Ibanaccno)
					then &lt;fml:CT_IBANACCNO&gt;{ data($req/m:Ibanaccno) }&lt;/fml:CT_IBANACCNO&gt;
					else ()
			}
			{
				if($req/m:PanDisp)
					then &lt;fml:CT_PAN_DISP&gt;{ data($req/m:PanDisp) }&lt;/fml:CT_PAN_DISP&gt;
					else ()
			}
			{
				if($req/m:Vpan)
					then &lt;fml:CT_VPAN&gt;{ data($req/m:Vpan) }&lt;/fml:CT_VPAN&gt;
					else ()
			}
			{
				if($req/m:PinmailerRef)
					then &lt;fml:CT_PINMAILER_REF&gt;{ data($req/m:PinmailerRef) }&lt;/fml:CT_PINMAILER_REF&gt;
					else ()
			}
			{
				if($req/m:Custid)
					then &lt;fml:CT_CUSTID&gt;{ data($req/m:Custid) }&lt;/fml:CT_CUSTID&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapPregenPinRequest($body/m:PregenPinRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>