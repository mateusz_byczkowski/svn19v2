<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Zamiana XML dla FML - odpowiedź z usługiVersion.$1.2010-09-09</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "http://bzwbk.com/services/prime/";

declare function xf:map_getPayBackDataResponse($req as element(urn:GetPayBackDataResponse))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				let $OUT := $req/urn:GetPayBackDataResult/urn:OutGetPayBackData
				for $it at $p in $req/urn:GetPayBackDataResult/urn:OutGetPayBackData
				return
				(
					&lt;fml:DC_NR_PB1?&gt;{ data($OUT[$p]/urn:PB1)}&lt;/fml:DC_NR_PB1&gt;,
					&lt;fml:DC_CRD_DESIGN?&gt;{ data($OUT[$p]/urn:crdDesign)}&lt;/fml:DC_CRD_DESIGN&gt;,
					&lt;fml:DC_CRD_LAYOUT?&gt;{ data($OUT[$p]/urn:crdLayout)}&lt;/fml:DC_CRD_LAYOUT&gt;,
					&lt;fml:DC_KOD_SPRZEDAWCY?&gt;{ data($OUT[$p]/urn:salesmanCode)}&lt;/fml:DC_KOD_SPRZEDAWCY&gt;,
					&lt;fml:DC_NR_PB2?&gt;{ data($OUT[$p]/urn:PB2)}&lt;/fml:DC_NR_PB2&gt;,
					&lt;fml:DC_FLAGA_KLIENTA?&gt;{ data($OUT[$p]/urn:fromPayback)}&lt;/fml:DC_FLAGA_KLIENTA&gt;
				)
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:map_getPayBackDataResponse($body/urn:GetPayBackDataResponse) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>