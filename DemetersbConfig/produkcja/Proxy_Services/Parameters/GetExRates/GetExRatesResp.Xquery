<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace msg = "http://bzwbk.com/services/ceke/messages/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $fml as element(FML32):=$body/FML32;




&lt;soap-env:Body&gt;
     &lt;GetExRatesResponse&gt;
            &lt;NrTabeli&gt;{data($fml/B_NR_TABELI)}&lt;/NrTabeli&gt;
            &lt;Data&gt;{data($fml/B_DATA_STR)}&lt;/Data&gt;
            &lt;Godzina&gt;{data($fml/B_GODZINA)}&lt;/Godzina&gt;
              {    for $x in $fml/B_KURSY_1
                    return
                            &lt;Kursy1&gt;{data($x)}&lt;/Kursy1&gt;
             }
              {    for $x in $fml/B_KURSY_2
                    return
                            &lt;Kursy2&gt;{data($x)}&lt;/Kursy2&gt;
             }
            
        &lt;/GetExRatesResponse&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>