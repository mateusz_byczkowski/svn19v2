<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";

declare function xf:mapgetCEKEConfigurationRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
			if(upper-case(data($req/urn:customer/urn1:Customer/urn1:customerType/urn2:CustomerType/urn2:customerType)) = "F") then
				(
					&lt;fml:E_CUSTOMER_TYPE&gt;P&lt;/fml:E_CUSTOMER_TYPE&gt;
				) else if (upper-case(data($req/urn:customer/urn1:Customer/urn1:customerType/urn2:CustomerType/urn2:customerType)) = "P") then
				(
					&lt;fml:E_CUSTOMER_TYPE&gt;C&lt;/fml:E_CUSTOMER_TYPE&gt;
				) else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetCEKEConfigurationRequest($body/urn:invoke) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>