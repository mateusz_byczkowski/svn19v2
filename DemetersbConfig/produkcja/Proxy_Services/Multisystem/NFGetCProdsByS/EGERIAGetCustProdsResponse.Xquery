<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapProduct($bdy as element(fml:FML32),$ind as xs:integer)  as element()* {
 
       if ($bdy/fml:EG_NR_UMOWY[$ind])
             then &lt;fml:NF_ACCOUN_ACCOUNTNUMBER&gt;{data($bdy/fml:EG_NR_UMOWY[$ind])}&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER&gt;
             else &lt;fml:NF_ACCOUN_ACCOUNTNUMBER nill="true"/&gt;
,
       if ($bdy/fml:EG_SALDO[$ind])
             then &lt;fml:NF_ACCOUN_CURRENTBALANCE&gt;{data($bdy/fml:EG_SALDO[$ind])}&lt;/fml:NF_ACCOUN_CURRENTBALANCE&gt;
             else &lt;fml:NF_ACCOUN_CURRENTBALANCE nill="true"/&gt;
,
       if ($bdy/fml:EG_KOD_WALUTY[$ind])
             then &lt;fml:NF_CURREC_CURRENCYCODE&gt;{data($bdy/fml:EG_KOD_WALUTY[$ind])}&lt;/fml:NF_CURREC_CURRENCYCODE&gt;
             else &lt;fml:NF_CURREC_CURRENCYCODE nill="true"/&gt;
,
       if ($bdy/fml:EG_DATA_PODP_UMOWY[$ind])
             then &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;{data($bdy/fml:EG_DATA_PODP_UMOWY[$ind])}&lt;/fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;
             else &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE nill="true"/&gt;
,
       if ($bdy/fml:EG_KOD_PRODUKTU[$ind])
             then &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT&gt;{data($bdy/fml:EG_KOD_PRODUKTU[$ind])}&lt;/fml:NF_ATTRPG_FIRSTPRODUCTFEAT&gt;
             else &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT nill="true"/&gt;
, 
       &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA nil="true"/&gt;
,
       &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/&gt;
,
       &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/&gt;
,
       &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT nil="true"/&gt;
,
       if ($bdy/fml:EG_KOD_PRODUKTU[$ind])
             then &lt;fml:NF_PRODUD_SORCEPRODUCTCODE&gt;{data($bdy/fml:EG_KOD_PRODUKTU[$ind])}&lt;/fml:NF_PRODUD_SORCEPRODUCTCODE&gt;
             else &lt;fml:NF_PRODUD_SORCEPRODUCTCODE nill="true"/&gt;
,
       if ($bdy/fml:EG_KOD_AKTYWNOSCI[$ind])
             then &lt;fml:NF_PRODUCT_STAT&gt;{data($bdy/fml:EG_KOD_AKTYWNOSCI[$ind])}&lt;/fml:NF_PRODUCT_STAT&gt;
             else &lt;fml:NF_PRODUCT_STAT nill="true"/&gt;
, 
       &lt;fml:NF_PRODUA_CODEPRODUCTAREA&gt;3&lt;/fml:NF_PRODUA_CODEPRODUCTAREA&gt;
,
       &lt;fml:NF_CTRL_SYSTEMID&gt;3&lt;/fml:NF_CTRL_SYSTEMID&gt;
};

declare function xf:mapEGERIAGetCustProdsResponse($bdy as element(fml:FML32))
	as element()*{
           &lt;fml:NF_PAGEC_HASNEXT&gt;{data($bdy/fml:EG_CZY_SA_NAST_REKORDY)}&lt;/fml:NF_PAGEC_HASNEXT&gt;,
           &lt;fml:NF_PAGECC_OPERATIONS&gt;{data($bdy/fml:EG_ILE_ZWROCONYCH_REK)}&lt;/fml:NF_PAGECC_OPERATIONS&gt;,
           &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;{data($bdy/fml:EG_KLUCZ_NAWIG)}&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;,
           for $req at $idx in $bdy/fml:EG_NR_UMOWY
               return
                   xf:mapProduct($bdy,$idx)
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
  &lt;fml:FML32&gt;
     {
      xf:mapEGERIAGetCustProdsResponse($body/fml:FML32)
     }
  &lt;/fml:FML32&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>