<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fn = "http://www.w3.org/2005/02/xpath-functions/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";

declare function xf:mapgetCustomersByRelationsResponse($fml as element(fml:FML32), $rel as element(urn:customerRelationships))
	as element(urn:invokeResponse) {
		&lt;urn:invokeResponse&gt;
			&lt;urn:customerList&gt;
			{

				let $relations := distinct-values(data($rel/urn1:CustomerRelationship/urn1:relationType/urn2:CustomerRelations/urn2:customerRelations))

				let $DC_NUMER_KLIENTA_REL := $fml/fml:DC_NUMER_KLIENTA_REL
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				let $DC_TYP_RELACJI := $fml/fml:DC_TYP_RELACJI
				let $CI_BUDOWA_PORTFELA := $fml/fml:CI_BUDOWA_PORTFELA
				let $CI_OBSLUGA_ZADAN := $fml/fml:CI_OBSLUGA_ZADAN
				let $CI_KORESP_SERYJNA := $fml/fml:CI_KORESP_SERYJNA
                                                                let $CI_TYP_REL_ODWROTNEJ := $fml/fml:CI_TYP_REL_ODWROTNEJ
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA_REL
				return
					if((exists(index-of($relations, data($DC_TYP_RELACJI[$p])))) or (exists(index-of($relations, data($CI_TYP_REL_ODWROTNEJ[$p]))))) then
					(
						&lt;urn1:Customer&gt;
						{
							&lt;urn1:customerNumber?&gt;{ data($DC_NUMER_KLIENTA_REL[$p]) }&lt;/urn1:customerNumber&gt;
						}
						{
							&lt;urn1:customerRelationshipList&gt;
								&lt;urn1:CustomerRelationship&gt; (:unbounded:)
									&lt;urn1:relationType&gt;
										&lt;urn2:CustomerRelations&gt;
											&lt;urn2:customerRelations&gt;{ data($DC_TYP_RELACJI[$p]) }&lt;/urn2:customerRelations&gt;
										&lt;/urn2:CustomerRelations&gt;
									&lt;/urn1:relationType&gt;
								&lt;/urn1:CustomerRelationship&gt;
							&lt;/urn1:customerRelationshipList&gt;
						}
						{
							&lt;urn1:customerPersonal&gt;
								&lt;urn1:CustomerPersonal&gt;
									&lt;urn1:lastName?&gt;{ data($DC_NAZWISKO[$p]) }&lt;/urn1:lastName&gt;
									&lt;urn1:firstName?&gt;{ data($DC_IMIE[$p]) }&lt;/urn1:firstName&gt;
								&lt;/urn1:CustomerPersonal&gt;
							&lt;/urn1:customerPersonal&gt;
						}

						&lt;/urn1:Customer&gt;
					) else ()
			}

			&lt;/urn:customerList&gt;
		&lt;/urn:invokeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetCustomersByRelationsResponse($body/fml:FML32, $body/urn:customerRelationships) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>