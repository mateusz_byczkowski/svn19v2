<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-03-24</con:description>
  <con:xquery>declare namespace urn = "urn:be.services.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://bzwbk.com/services/mappings/";

declare variable $body as element(soap-env:Body) external;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))&gt;0)
    then true()
    else false()
};

&lt;soap-env:Body&gt; 
  {
  let $fml := $body/FML32
  return
  
  &lt;urn:invokeResponse xmlns:urn="urn:be.services.dcl"&gt; 
    {if($fml/GI_FLAGA and xf:isData($fml/GI_FLAGA) and data($fml/GI_FLAGA)="Y")
       then &lt;urn:flag&gt;true&lt;/urn:flag&gt;
       else &lt;urn:flag&gt;false&lt;/urn:flag&gt;
    }
  &lt;/urn:invokeResponse&gt; 
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>