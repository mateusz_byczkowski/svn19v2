<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-10</con:description>
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:baseentities.be.dcl";
declare namespace ns0="urn:customercertificatedict.dictionaries.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:be.services.dcl";
declare namespace ns8="urn:customercertificates.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns7:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns7:msgHeader/ns7:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns7:msgHeader/ns7:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
,
&lt;NF_MSHEAD_UNITID?&gt;{data($parm/ns7:msgHeader/ns7:unitId)}&lt;/NF_MSHEAD_UNITID&gt;
,
&lt;NF_MSHEAD_USERID?&gt;{data($parm/ns7:msgHeader/ns7:userId)}&lt;/NF_MSHEAD_USERID&gt;
,
&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns7:msgHeader/ns7:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns7:msgHeader/ns7:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
,
&lt;NF_TRHEAD_TRANSID?&gt;{data($parm/ns7:transHeader/ns7:transId)}&lt;/NF_TRHEAD_TRANSID&gt;
};
declare function getFieldsFromInvoke($parm as element(ns7:invoke)) as element()*
{

&lt;NF_CUSTCE_CERTIFICATENUMBE?&gt;{data($parm/ns7:customerCertificate/ns8:CustomerCertificate/ns8:certificateNumber)}&lt;/NF_CUSTCE_CERTIFICATENUMBE&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns7:header)}
    {getFieldsFromInvoke($body/ns7:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>