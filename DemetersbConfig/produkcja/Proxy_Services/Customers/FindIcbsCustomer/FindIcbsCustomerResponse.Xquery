<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbs/messages/";
declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapFindIcbsCustomerResponse($fml as element(fml:FML32))
	as element(m:FindIcbsCustomerResponse) {
		&lt;m:FindIcbsCustomerResponse&gt;
			{
				let $B_KOD_KLIENTA := $fml/fml:B_KOD_KLIENTA
				let $B_IMIE := $fml/fml:B_IMIE
				let $B_NAZWISKO := $fml/fml:B_NAZWISKO
				let $B_NR_DOWODU := $fml/fml:B_NR_DOWODU
				let $B_N_PESEL := $fml/fml:B_N_PESEL
				let $B_REZYDENT := $fml/fml:B_REZYDENT
				let $B_ODDZIAL := $fml/fml:B_ODDZIAL
				let $B_DOWOD_ZASTRZ := $fml/fml:B_DOWOD_ZASTRZ
				let $B_D_ZASTRZ := $fml/fml:B_D_ZASTRZ
				let $B_MCE_ZASTRZ := $fml/fml:B_MCE_ZASTRZ
				let $B_KREWNY_BANKU := $fml/fml:B_KREWNY_BANKU
				let $B_SEGMENT_MARK := $fml/fml:B_SEGMENT_MARK
				let $B_FIRMA := $fml/fml:B_FIRMA
				let $B_IDENTYFIKATOR1 := $fml/fml:B_IDENTYFIKATOR1
				let $B_IDENTYFIKATOR2 := $fml/fml:B_IDENTYFIKATOR2
				let $B_N_FOP := $fml/fml:B_N_FOP
				let $B_N_EKD := $fml/fml:B_N_EKD
				for $it at $p in $fml/fml:B_KOD_KLIENTA
				return
					&lt;m:Customer&gt;
					{
						if($B_KOD_KLIENTA[$p])
							then &lt;m:KodKlienta&gt;{ data($B_KOD_KLIENTA[$p]) }&lt;/m:KodKlienta&gt;
						else ()
					}
					{
						if($B_IMIE[$p])
							then &lt;m:Imie&gt;{ data($B_IMIE[$p]) }&lt;/m:Imie&gt;
						else ()
					}
					{
						if($B_NAZWISKO[$p])
							then &lt;m:Nazwisko&gt;{ data($B_NAZWISKO[$p]) }&lt;/m:Nazwisko&gt;
						else ()
					}
					{
						if($B_NR_DOWODU[$p])
							then &lt;m:NrDowodu&gt;{ data($B_NR_DOWODU[$p]) }&lt;/m:NrDowodu&gt;
						else ()
					}
					{
						if($B_N_PESEL[$p])
							then &lt;m:NPesel&gt;{ data($B_N_PESEL[$p]) }&lt;/m:NPesel&gt;
						else ()
					}
					{
						if($B_REZYDENT[$p])
							then &lt;m:Rezydent&gt;{ data($B_REZYDENT[$p]) }&lt;/m:Rezydent&gt;
						else ()
					}
					{
						if($B_ODDZIAL[$p])
							then &lt;m:Oddzial&gt;{ data($B_ODDZIAL[$p]) }&lt;/m:Oddzial&gt;
						else ()
					}
					{
						if($B_DOWOD_ZASTRZ[$p])
							then &lt;m:DowodZastrz&gt;{ data($B_DOWOD_ZASTRZ[$p]) }&lt;/m:DowodZastrz&gt;
						else ()
					}
					{
						if($B_D_ZASTRZ[$p])
							then &lt;m:DZastrz&gt;{ data($B_D_ZASTRZ[$p]) }&lt;/m:DZastrz&gt;
						else ()
					}
					{
						if($B_MCE_ZASTRZ[$p])
							then &lt;m:MceZastrz&gt;{ data($B_MCE_ZASTRZ[$p]) }&lt;/m:MceZastrz&gt;
						else ()
					}
					{
						if($B_KREWNY_BANKU[$p])
							then &lt;m:KrewnyBanku&gt;{ data($B_KREWNY_BANKU[$p]) }&lt;/m:KrewnyBanku&gt;
						else ()
					}
					{
						if($B_SEGMENT_MARK[$p])
							then &lt;m:SegmentMark&gt;{ data($B_SEGMENT_MARK[$p]) }&lt;/m:SegmentMark&gt;
						else ()
					}
					{
						if($B_FIRMA[$p])
							then &lt;m:Firma&gt;{ data($B_FIRMA[$p]) }&lt;/m:Firma&gt;
						else ()
					}
					{
						if($B_IDENTYFIKATOR1[$p])
							then &lt;m:Identyfikator1&gt;{ data($B_IDENTYFIKATOR1[$p]) }&lt;/m:Identyfikator1&gt;
						else ()
					}
					{
						if($B_IDENTYFIKATOR2[$p])
							then &lt;m:Identyfikator2&gt;{ data($B_IDENTYFIKATOR2[$p]) }&lt;/m:Identyfikator2&gt;
						else ()
					}
					{
						if($B_N_FOP[$p])
							then &lt;m:NFop&gt;{ data($B_N_FOP[$p]) }&lt;/m:NFop&gt;
						else ()
					}
					{
						if($B_N_EKD[$p])
							then &lt;m:NEkd&gt;{ data($B_N_EKD[$p]) }&lt;/m:NEkd&gt;
						else ()
					}
					&lt;/m:Customer&gt;
			}
		&lt;/m:FindIcbsCustomerResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapFindIcbsCustomerResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>