<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery>(:Change log 
v.1.0  2010-06-28  PKL  NP2042 Utworzenie 
v.1.1  2010-09-02  PKL  NP2042 CR1 Rozszerzenie usługi o parametr wyjściowy LoanAccount.faceAmount 

:)

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace urn= "urn:be.services.dcl";
declare namespace urn1="urn:productstree.entities.be.dcl";
declare namespace urn2="urn:accounts.entities.be.dcl";
declare namespace urn3="urn:dictionaries.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
  &lt;urn:invokeResponse&gt;
    &lt;urn:productDefinition&gt;
        &lt;urn1:ProductDefinition&gt;   
        &lt;urn1:idProductGroup?&gt;{data($parm/PT_ID_GROUP)}&lt;/urn1:idProductGroup&gt;
        &lt;urn1:idProductDefinition?&gt;{data($parm/CI_PRODUCT_ID)}&lt;/urn1:idProductDefinition&gt;
        &lt;/urn1:ProductDefinition&gt;
    &lt;/urn:productDefinition&gt;
    &lt;urn:accountOut&gt;
        &lt;urn2:Account&gt;
            &lt;urn2:accountOpenDate?&gt;{data($parm/B_D_OTWARCIA)}&lt;/urn2:accountOpenDate&gt;
            &lt;urn2:currentBalance?&gt;{data($parm/B_SALDO)}&lt;/urn2:currentBalance&gt;
            &lt;urn2:accountDescription?&gt;{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI)}&lt;/urn2:accountDescription&gt;
            &lt;urn2:currentBalancePLN?&gt;{data($parm/NF_ACCOUN_CURRENTBALANCEPL)}&lt;/urn2:currentBalancePLN&gt;
            &lt;urn2:loanAccount&gt;
                &lt;urn2:LoanAccount&gt;
(:1.1:)       &lt;urn2:faceAmount?&gt;{data($parm/B_KREDYT_PRZYZN)}&lt;/urn2:faceAmount&gt;
                &lt;urn2:payoff?&gt;{data($parm/NF_LOANA_PAYOFF)}&lt;/urn2:payoff&gt;
                &lt;urn2:accountToPayObligations?&gt;{data($parm/NF_LOANA_ACCOUNTTOPAYOBLIG)}&lt;/urn2:accountToPayObligations&gt;
                &lt;/urn2:LoanAccount&gt;
            &lt;/urn2:loanAccount&gt;
            &lt;urn2:currency&gt;
                &lt;urn3:CurrencyCode&gt;
                    &lt;urn3:currencyCode?&gt;{data($parm/B_KOD_WALUTY)}&lt;/urn3:currencyCode&gt;
                &lt;/urn3:CurrencyCode&gt;
            &lt;/urn2:currency&gt;
        &lt;/urn2:Account&gt;
    &lt;/urn:accountOut&gt;
  &lt;/urn:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>