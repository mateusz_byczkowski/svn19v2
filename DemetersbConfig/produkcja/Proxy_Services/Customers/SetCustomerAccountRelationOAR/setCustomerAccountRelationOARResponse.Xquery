<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns4="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns5:invokeResponse&gt;
  &lt;ns5:response&gt;
    &lt;ns2:ResponseMessage&gt;
      &lt;ns2:result&gt;true&lt;/ns2:result&gt;
      &lt;ns2:errorDescription?&gt;{data($parm/DC_OPIS_BLEDU)}&lt;/ns2:errorDescription&gt;
    &lt;/ns2:ResponseMessage&gt;
  &lt;/ns5:response&gt;
&lt;/ns5:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/fml:FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>