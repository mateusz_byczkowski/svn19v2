<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:entities.be.dcl";
declare namespace ns10="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns8="urn:be.services.dcl";
declare namespace fml="";
declare namespace ns9="urn:card.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForAccountRelationshipList($parm as element(fml:FML32), $occ) as element()
{

<ns1:accountRelationshipList>
  {
    (:for $x at $occ in $parm/NF_ACCOUR_RELATIONSHIP
    return:)
    <ns3:AccountRelationship>
      <ns3:customer>
        <ns3:Customer>
          <ns3:companyName?>{data($parm/NF_CUSTOM_COMPANYNAME[$occ])}</ns3:companyName>
          <ns3:customerPersonal>
            <ns3:CustomerPersonal>
              <ns3:lastName?>{data($parm/NF_CUSTOP_LASTNAME[$occ])}</ns3:lastName>
              <ns3:firstName?>{data($parm/NF_CUSTOP_FIRSTNAME[$occ])}</ns3:firstName>
            </ns3:CustomerPersonal>
          </ns3:customerPersonal>
          <ns3:customerType>
            <ns5:CustomerType>
              <ns5:customerType?>{data($parm/NF_CUSTOT_CUSTOMERTYPE[$occ])}</ns5:customerType>
            </ns5:CustomerType>
          </ns3:customerType>
        </ns3:Customer>
      </ns3:customer>
      <ns3:relationship>
        <ns5:CustomerAccountRelationship>
          <ns5:customerAccountRelationship?>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}</ns5:customerAccountRelationship>
        </ns5:CustomerAccountRelationship>
      </ns3:relationship>
    </ns3:AccountRelationship>
  }
</ns1:accountRelationshipList>
};
declare function getElementsForAccountList($parm as element(fml:FML32)) as element()
{

<ns8:accountList>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
    <ns1:Account>
      <ns1:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}</ns1:accountNumber>
      <ns1:accountName?>{data($parm/NF_ACCOUN_ACCOUNTNAME[$occ])}</ns1:accountName>
      <ns1:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}</ns1:currentBalance>
      <ns1:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}</ns1:accountDescription>
      {getElementsForAccountRelationshipList($parm,$occ)}
      <ns1:accountType>
        <ns5:AccountType>
          <ns5:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}</ns5:accountType>
        </ns5:AccountType>
      </ns1:accountType>
      <ns1:currency>
        <ns5:CurrencyCode>
          <ns5:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns5:currencyCode>
        </ns5:CurrencyCode>
      </ns1:currency>
      <ns1:accountStatus>
        <ns2:AccountStatus>
          <ns2:accountStatus?>{data($parm/NF_ACCOUS_ACCOUNTSTATUS[$occ])}</ns2:accountStatus>
        </ns2:AccountStatus>
      </ns1:accountStatus>
    </ns1:Account>
  }
</ns8:accountList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns8:invokeResponse>
  <ns8:response>
    <ns0:ResponseMessage>
      <ns0:result?>{data($parm/NF_RESPOM_RESULT)}</ns0:result>
    </ns0:ResponseMessage>
  </ns8:response>
  {getElementsForAccountList($parm)}
  <ns8:bcd>
    <ns6:BusinessControlData>
      <ns6:pageControl>
        <ns10:PageControl>
          <ns10:hasNext?>{data($parm/NF_PAGEC_HASNEXT)}</ns10:hasNext>
          <ns10:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns10:navigationKeyDefinition>
          <ns10:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns10:navigationKeyValue>
        </ns10:PageControl>
      </ns6:pageControl>
    </ns6:BusinessControlData>
  </ns8:bcd>
</ns8:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>