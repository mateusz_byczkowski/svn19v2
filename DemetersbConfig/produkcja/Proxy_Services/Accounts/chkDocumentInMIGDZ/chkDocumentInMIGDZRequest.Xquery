<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.2
 : @since 2010-02-19
 :
 : wersja WSDLa: 25-01-2010 15:50:42
 :
 : $Proxy Services/Accounts/chkDocumentInMIGDZ/chkDocumentInMIGDZRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts_svn/chkDocumentInMIGDZ/chkDocumentInMIGDZ/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:operations.entities.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";

declare function xf:chkDocumentInMIGDZ($header1 as element(ns3:header),
    										 $invoke1 as element(ns3:invoke))
    as element(ns0:FML32) {
        &lt;ns0:FML32&gt;

			(:
			 : dane z naglowka
			 :)
            &lt;ns0:DC_UZYTKOWNIK?&gt;{
				data($header1/ns3:msgHeader/ns3:userId)
			}&lt;/ns0:DC_UZYTKOWNIK&gt;

            &lt;ns0:DC_KOD_JEDNOSTKI?&gt;{
				data($header1/ns3:msgHeader/ns3:unitId)
			}&lt;/ns0:DC_KOD_JEDNOSTKI&gt;

            &lt;ns0:DC_KOD_APLIKACJI?&gt;{
				data($header1/ns3:msgHeader/ns3:appId)
			}&lt;/ns0:DC_KOD_APLIKACJI&gt;

            (:
             : dane z operacji wejściowej
             :)
            &lt;ns0:CI_SERIA_NR_DOK?&gt;{
				data($invoke1/ns3:disposer/ns2:Disposer/ns2:documentNumber)
			}&lt;/ns0:CI_SERIA_NR_DOK&gt;

            &lt;ns0:CI_DOK_TOZSAMOSCI?&gt;{
				data($invoke1/ns3:disposer/ns2:Disposer/ns2:documentType/ns1:DocumentTypeForTxn/ns1:documentCodeForMIG)
			}&lt;/ns0:CI_DOK_TOZSAMOSCI&gt;

        &lt;/ns0:FML32&gt;
};

declare variable $header1 as element(ns3:header) external;
declare variable $invoke1 as element(ns3:invoke) external;

&lt;soap-env:Body&gt;{
	xf:chkDocumentInMIGDZ($header1, $invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>