<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";

declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $callBody as element(soap:Body) external;

declare function getFieldsFromBody($parm as element(fml:FML32)) as element(fml:FML32)
{
&lt;fml:FML32&gt;
&lt;fml:PT_SOURCE_PRODUCT_CODE&gt;{data($parm/fml:NF_PRODUD_SORCEPRODUCTCODE)}&lt;/fml:PT_SOURCE_PRODUCT_CODE&gt;
&lt;fml:PT_CODE_PRODUCT_SYSTEM&gt;1&lt;/fml:PT_CODE_PRODUCT_SYSTEM&gt;
&lt;fml:PT_FIRST_PRODUCT_FEATURE&gt;{data($parm/fml:NF_ATTRPG_FIRSTPRODUCTFEAT)}&lt;/fml:PT_FIRST_PRODUCT_FEATURE&gt;
&lt;fml:PT_SECOND_PRODUCT_FEATURE&gt;{data($parm/fml:NF_ATTRPG_SECONDPRODUCTFEA)}&lt;/fml:PT_SECOND_PRODUCT_FEATURE&gt;
&lt;fml:PT_THIRD_PRODUCT_FEATURE&gt;{data($parm/fml:NF_ATTRPG_THIRDPRODUCTFEAT)}&lt;/fml:PT_THIRD_PRODUCT_FEATURE&gt;
&lt;fml:PT_FOURTH_PRODUCT_FEATURE&gt;{data($parm/fml:NF_ATTRPG_FOURTHPRODUCTFEA)}&lt;/fml:PT_FOURTH_PRODUCT_FEATURE&gt;
&lt;fml:PT_FIFTH_PRODUCT_FEATURE&gt;{data($parm/fml:NF_ATTRPG_FIFTHPRODUCTFEAT)}&lt;/fml:PT_FIFTH_PRODUCT_FEATURE&gt;
&lt;/fml:FML32&gt;
};


getFieldsFromBody($callBody/fml:FML32)</con:xquery>
</con:xqueryEntry>