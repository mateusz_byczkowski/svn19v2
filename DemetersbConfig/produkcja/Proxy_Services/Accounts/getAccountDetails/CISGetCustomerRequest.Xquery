<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.6
 : @since   2010-02-10
 :
 : Tworzenie requesta dla usługi biznesowej CISGetCustomer
 : na podstawie wyjścia z usługi biznesowej bICBSGetAccountDet
 :
 : $Proxy Services/Accounts/getAccountDetails/CISGetCustomerRerquest.xq$
 :
 :)

declare namespace xf = "http://tempuri.org/Accounts/getAccountDetails/CISGetCustomerRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "";

declare variable $fML321 as element(ns0:FML32) external;
declare variable $body as element() external;

(:~
 : @param $fML321 numer klienta z usługi bICBSGetAccountDet
 :
 : @return FML32 bufor wejściowy dla usługi CISGetCustomer
 :)
declare function xf:CISGetCustomerRequest($fML321 as element(ns0:FML32))
    as element(ns1:FML32)
{
	&lt;ns1:FML32&gt;

		(:
		 : CIF klienta
		 :)
		&lt;ns1:DC_NUMER_KLIENTA&gt;{
			data($fML321/ns0:NF_TXNAD_OWNERCIF)
		}&lt;/ns1:DC_NUMER_KLIENTA&gt;

		(: 
		 : id spółki klienta bankowego
		 :)
		&lt;ns1:CI_ID_SPOLKI&gt;1&lt;/ns1:CI_ID_SPOLKI&gt;

		(:
		 : opcja wywołania
		 :)
		&lt;ns1:CI_OPCJA&gt;3&lt;/ns1:CI_OPCJA&gt;

	&lt;/ns1:FML32&gt;
};

let $fML321 := $body/FML32
return
	xf:CISGetCustomerRequest($fML321)</con:xquery>
</con:xqueryEntry>