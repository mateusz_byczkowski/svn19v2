<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @author  Tomasz Krajewski  
 : @version 1.7
 : @since   2010-06-24
 :
 : wersja WSDLa: 15-02-2010 13:53:15
 :
 : $Proxy Services/Accounts/getAccountDetails/getAccountDetailsRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getAccountDetails/getAccountDetailsRequest/";
declare namespace ns0 = "urn:operations.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $header1 as element(ns2:header) external;
declare variable $invoke1 as element(ns2:invoke) external;

(:
:Opis:
:	Zwracanie konta 'PL' w formacie NRB albo pustego stringa dla kont zagranicznych 
:We:
:	$AccNum - numer konta w formacie IBAN/NRB
:Wy:
:	numer konta w formacie NRB dla konta 'PL'
:	'0...0' dla konta zagranicznego 
:)
declare function local:IBANtoNRB($AccNum as xs:string) as xs:string
{
	let $CountryCode := (upper-case(substring($AccNum, 1, 2)))
	let $FirstChar := (upper-case(substring($AccNum, 1, 1)))
	return 
		if ($FirstChar ge '0' and $FirstChar le '9')
		then $AccNum (: Numer konta w formacie NRB :)
		else (: Numer konta w formacie IBAN :)
			if ($CountryCode eq 'PL')
			then (substring($AccNum, 3)) (: Zwróć NRB :)
			else '00000000000000000000000000' (: Zwróć zera dla konta zagranicznego :)
};

(:
:Opis:
:	Wyznaczenie ID Systemu:
:We:
:	$AccNum - numer konta w formacie NRB
:Wy:
:	0 - Konto zagraniczne (zewnętrzne) 
:	1 - ICBS (wartość domyślna)
:	2 - PRIME (wołanie: 'F' + ostatnie 16 cyfr rachunku)
:		?? 1090 1489 4*
:		?? 1090 1489 6*
:		?? 1090 1489 7*
:	3 - BM2 (wołanie: ostatnie 10 cyfr rachunku)
:		?? 1090 0004 *
:		?? 1090 0075 *
:)
declare function local:getSystemID($AccNum as xs:string) as xs:integer
{
	let $AccNumNRB := local:IBANtoNRB($AccNum)
	let $BranchNum := (substring($AccNumNRB, 3, 8))
	let $NextChar := (substring($AccNumNRB, 11, 1))
	return
	    if ($BranchNum eq '00000000')
	    then 0 (: konto zagraniczne :)
	    else
	    (
 			if ($BranchNum eq '10901489')
			then
			( 
				if ($NextChar eq '4' or $NextChar eq '6' or $NextChar eq '7')
				then 2 (: PRIME :)
				else 1 (: Nieznany system - sprawdzimy w ICBS :)
			)
  			else 
 			(
 				if ($BranchNum eq '10900004' or $BranchNum eq '10900075')
	 			then 3 (: BM2 :)
 				else 1 (: ICBS :)
			)
		)
};

declare function local:prepareAccNum($AccNum as xs:string) as xs:string
{
	let $AccNumNRB := local:IBANtoNRB($AccNum)
	let $SystemID := local:getSystemID($AccNum)
	return 
		if ($SystemID eq 1)
		then $AccNumNRB (: ICBS - zwróć pełny numer NRB :)
		else if ($SystemID eq 2) 
		then (concat('F', substring($AccNumNRB, 11))) (: PRIME - Zwróć 'F' + ostatnie 16 cyfr :)
		else if ($SystemID eq 3)
		then $AccNumNRB (: BM2 - Zwróć pełny numer NRB :)
		else '' (: Rachunek zagraniczny - zwróć pusty :)
};

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:getAccountDetailsRequest($header1 as element(ns2:header),
											   $invoke1 as element(ns2:invoke))
    as element(ns1:FML32)
{
	&lt;ns1:FML32&gt;

		(:
		 : dane z nagłówka
		 :)
		&lt;ns1:NF_MSHEAD_MSGID?&gt;{
			data($header1/ns2:msgHeader/ns2:msgId)
		}&lt;/ns1:NF_MSHEAD_MSGID&gt;

		(:
		 : dane z operacji wejściowej
		 :)
		&lt;ns1:NF_TXNAD_ACCOUNTNUMBER?&gt;{
			data($invoke1/ns2:txnAccountData/ns0:TxnAccountData/ns0:accountNumber)
		}&lt;/ns1:NF_TXNAD_ACCOUNTNUMBER&gt;
		

        &lt;ns1:NF_CTRL_SYSTEMID&gt;{
			local:getSystemID(data($invoke1/ns2:txnAccountData/ns0:TxnAccountData/ns0:accountNumber))
		}&lt;/ns1:NF_CTRL_SYSTEMID&gt;
		
		(:
		 : Numer konta dla PRIME i BM2
		 :)
		&lt;ns1:NF_ACCOUN_ACCOUNTNUMBER?&gt;{
			local:prepareAccNum($invoke1/ns2:txnAccountData/ns0:TxnAccountData/ns0:accountNumber)
		}&lt;/ns1:NF_ACCOUN_ACCOUNTNUMBER&gt;		

	&lt;/ns1:FML32&gt;
};

&lt;soap-env:Body&gt;{
	xf:getAccountDetailsRequest($header1, $invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>