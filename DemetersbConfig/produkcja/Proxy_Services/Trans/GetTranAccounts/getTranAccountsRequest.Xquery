<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:Change log 
v.1.1 2010-06-30 PKL T49193 Zwracanie wyłącznie aktywnych rachunków
:)

declare namespace fml="";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns8="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};


declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

&lt;NF_MSHEAD_MSGID?&gt;{data($parm/ns0:msgHeader/ns0:msgId)}&lt;/NF_MSHEAD_MSGID&gt;
,
(: &lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns0:msgHeader/ns0:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt; :)
&lt;NF_MSHEAD_COMPANYID?&gt;1&lt;/NF_MSHEAD_COMPANYID&gt;
,
&lt;NF_MSHEAD_UNITID?&gt;{data($parm/ns0:msgHeader/ns0:unitId)}&lt;/NF_MSHEAD_UNITID&gt;
,
&lt;NF_MSHEAD_USERID?&gt;{data($parm/ns0:msgHeader/ns0:userId)}&lt;/NF_MSHEAD_USERID&gt;
,
&lt;NF_MSHEAD_APPID?&gt;{data($parm/ns0:msgHeader/ns0:appId)}&lt;/NF_MSHEAD_APPID&gt;
,
&lt;NF_MSHEAD_TIMESTAMP?&gt;{data($parm/ns0:msgHeader/ns0:timestamp)}&lt;/NF_MSHEAD_TIMESTAMP&gt;
,
&lt;NF_TRHEAD_TRANSID?&gt;{data($parm/ns0:transHeader/ns0:transId)}&lt;/NF_TRHEAD_TRANSID&gt;
};


declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
(: &lt;NF_PRODUA_CODEPRODUCTAREA?&gt;{data($parm/ns0:productArea/ns1:ProductArea/ns1:codeProductArea)}&lt;/NF_PRODUA_CODEPRODUCTAREA&gt; :)
&lt;NF_PRODUA_CODEPRODUCTAREA?&gt;2&lt;/NF_PRODUA_CODEPRODUCTAREA&gt;
,
&lt;NF_CURREC_CURRENCYCODE?&gt;{data($parm/ns0:account/ns4:Account/ns4:currency/ns5:CurrencyCode/ns5:currencyCode)}&lt;/NF_CURREC_CURRENCYCODE&gt;
,
&lt;NF_PAGEC_ACTIONCODE?&gt;{data($parm/ns0:bcd/ns8:BusinessControlData/ns8:pageControl/ns6:PageControl/ns6:actionCode)}&lt;/NF_PAGEC_ACTIONCODE&gt;
,
&lt;NF_PAGEC_PAGESIZE?&gt;{data($parm/ns0:bcd/ns8:BusinessControlData/ns8:pageControl/ns6:PageControl/ns6:pageSize)}&lt;/NF_PAGEC_PAGESIZE&gt;
,
&lt;NF_PAGEC_REVERSEORDER?&gt;{boolean2SourceValue (data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:reverseOrder),"1","0")}&lt;/NF_PAGEC_REVERSEORDER&gt;
,
&lt;NF_PAGEC_NAVIGATIONKEYDEFI?&gt;{data($parm/ns0:bcd/ns8:BusinessControlData/ns8:pageControl/ns6:PageControl/ns6:navigationKeyDefinition)}&lt;/NF_PAGEC_NAVIGATIONKEYDEFI&gt;
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?&gt;{data($parm/ns0:bcd/ns8:BusinessControlData/ns8:pageControl/ns6:PageControl/ns6:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;
,
&lt;NF_CUSTOM_CUSTOMERNUMBER?&gt;{data($parm/ns0:customer/ns1:Customer/ns1:customerNumber)}&lt;/NF_CUSTOM_CUSTOMERNUMBER&gt;
,
(:1.1 
&lt;NF_CTRL_ACTIVENONACTIVE&gt;0&lt;/NF_CTRL_ACTIVENONACTIVE&gt; :)
&lt;NF_CTRL_ACTIVENONACTIVE&gt;1&lt;/NF_CTRL_ACTIVENONACTIVE&gt; (:1.1:)
,
&lt;NF_ACCOUN_ALTERNATIVEADDRE&gt;0&lt;/NF_ACCOUN_ALTERNATIVEADDRE&gt;
,
&lt;NF_CTRL_OPTION&gt;0&lt;/NF_CTRL_OPTION&gt;
,
&lt;NF_CTRL_ACTIVENONACTIVE&gt;1&lt;/NF_CTRL_ACTIVENONACTIVE&gt;
,
&lt;NF_ACCOUR_RELATIONSHIP&gt;SOWJAFJAO&lt;/NF_ACCOUR_RELATIONSHIP&gt;
,
&lt;NF_PRODUG_VISIBILITYDCL&gt;1&lt;/NF_PRODUG_VISIBILITYDCL&gt;
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>