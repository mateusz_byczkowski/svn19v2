<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace dec = "http://bzwbk.com/dc/services/mortgage/decision";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;

declare function local:mapApplicationData($req as element(fml:FML32))
	as element(applicationData) {
		&lt;applicationData&gt;
			&lt;celKredytu?&gt;{ data($req/fml:B_CEL_KREDYTU) }&lt;/celKredytu&gt;
			{
				if($req/fml:B_CENA_TOWARU and data($req/fml:B_CENA_TOWARU) != "")
					then &lt;cenaTowaru&gt;{ data($req/fml:B_CENA_TOWARU) }&lt;/cenaTowaru&gt;
					else ()
			}
			&lt;DZatw?&gt;{ data($req/fml:B_D_ZATW) }&lt;/DZatw&gt;
			{
				if($req/fml:B_KWOTA and data($req/fml:B_KWOTA) != "")
					then &lt;kwota&gt;{ data($req/fml:B_KWOTA) }&lt;/kwota&gt;
					else ()
			}
			&lt;LRat?&gt;{ data($req/fml:B_L_RAT) }&lt;/LRat&gt;
			{
				if($req/fml:B_MAX_KRED and data($req/fml:B_MAX_KRED) != "")
					then &lt;maxKred&gt;{ data($req/fml:B_MAX_KRED) }&lt;/maxKred&gt;
					else ()
			}
			{
				if ($req/fml:B_NR_IBAN)
					then
						let $iban_last_position := count($req/fml:B_NR_IBAN)
						return
							if ($req/fml:B_TRAN_KWOTA[$iban_last_position])
							then ()
							else &lt;nrIban&gt;{ data($req/fml:B_NR_IBAN[$iban_last_position]) }&lt;/nrIban&gt;
					else ()
					
			}
			&lt;okrOproc?&gt;{ data($req/fml:B_OKR_OPROC) }&lt;/okrOproc&gt;
			{
				if($req/fml:B_OPROC1 and data($req/fml:B_OPROC1) != "")
					then &lt;oproc1&gt;{ data($req/fml:B_OPROC1) }&lt;/oproc1&gt;
					else ()
			}
			&lt;oproc2?&gt;{ data($req/fml:B_OPROC2) }&lt;/oproc2&gt;
			{
				if($req/fml:B_POLE_UZYTK1 and data($req/fml:B_POLE_UZYTK1) != "")
					then &lt;poleUzytk1&gt;{ data($req/fml:B_POLE_UZYTK1) }&lt;/poleUzytk1&gt;
					else ()
			}
			{
				if($req/fml:B_POLE_UZYTK2 and data($req/fml:B_POLE_UZYTK2) != "")
					then &lt;poleUzytk2&gt;{ data($req/fml:B_POLE_UZYTK2) }&lt;/poleUzytk2&gt;
					else ()
			}
			{
				if($req/fml:B_POLE_UZYTK3 and data($req/fml:B_POLE_UZYTK3) != "")
					then &lt;poleUzytk3&gt;{ data($req/fml:B_POLE_UZYTK3) }&lt;/poleUzytk3&gt;
					else ()
			}
			&lt;poleUzytk4?&gt;{ data($req/fml:B_POLE_UZYTK4) }&lt;/poleUzytk4&gt;
			{
				if($req/fml:B_POLE_UZYTK5 and data($req/fml:B_POLE_UZYTK5) != "")
					then &lt;poleUzytk5&gt;{ data($req/fml:B_POLE_UZYTK5) }&lt;/poleUzytk5&gt;
					else ()
			}
			{
				if($req/fml:B_POLE_UZYTK6 and data($req/fml:B_POLE_UZYTK6) != "")
					then &lt;poleUzytk6&gt;{ data($req/fml:B_POLE_UZYTK6) }&lt;/poleUzytk6&gt;
					else ()
			}
			{
				if($req/fml:B_POLE_UZYTK7 and data($req/fml:B_POLE_UZYTK7) != "")
					then &lt;poleUzytk7&gt;{ data($req/fml:B_POLE_UZYTK7) }&lt;/poleUzytk7&gt;
					else ()
			}
			&lt;poleUzytk8?&gt;{ data($req/fml:B_POLE_UZYTK8) }&lt;/poleUzytk8&gt;
			{
				if($req/fml:B_PROW and data($req/fml:B_PROW) != "")
					then &lt;prow&gt;{ data($req/fml:B_PROW) }&lt;/prow&gt;
					else ()
			}
			&lt;regDec?&gt;{ data($req/fml:B_REG_DEC) }&lt;/regDec&gt;
			{
				if($req/fml:B_SCORE and data($req/fml:B_SCORE) != "")
					then &lt;score&gt;{ data($req/fml:B_SCORE) }&lt;/score&gt;
					else ()
			}
			{
				if($req/fml:B_SRODKI_WL and data($req/fml:B_SRODKI_WL) != "")
					then &lt;srodkiWl&gt;{ data($req/fml:B_SRODKI_WL) }&lt;/srodkiWl&gt;
					else ()
			}					
			&lt;typOproc?&gt;{ data($req/fml:B_TYP_OPROC) }&lt;/typOproc&gt;
			&lt;typRat?&gt;{ data($req/fml:B_TYP_RAT) }&lt;/typRat&gt;
			&lt;uwagiOddzialu?&gt;{ data($req/fml:B_UWAGI_ODDZIALU) }&lt;/uwagiOddzialu&gt;
			&lt;waluta?&gt;{ data($req/fml:B_WALUTA) }&lt;/waluta&gt;
			&lt;wniosekKw?&gt;{ data($req/fml:B_WNIOSEK_KW) }&lt;/wniosekKw&gt;
		&lt;/applicationData&gt;	
};

declare function local:mapIndexesData($req as element(fml:FML32))
	as element(indexesData) {
		&lt;indexesData&gt;
			{
				if($req/fml:B_TLTV and data($req/fml:B_TLTV) != "")
					then &lt;tltv&gt;{ data($req/fml:B_TLTV) }&lt;/tltv&gt;
					else ()
			}
			{
				let $di := $req/fml:B_DI
				let $ldsr := $req/fml:B_LDSR
				let $mdi := $req/fml:B_MDI
				let $sdsr := $req/fml:B_SDSR
				for $nrGosp at $i in $req/fml:B_NR_GOSP
				return
					if(data($nrGosp) != "0")
						then
			               &lt;householdIndexesData&gt;
			                 &lt;di&gt;{ data($di[$i]) }&lt;/di&gt;
			                 &lt;ldsr&gt;{ data($ldsr[$i]) }&lt;/ldsr&gt;
			                 &lt;mdi&gt;{ data($mdi[$i]) }&lt;/mdi&gt;
			                 &lt;nrGosp&gt;{ data($nrGosp) }&lt;/nrGosp&gt;
			                 &lt;sdsr&gt;{ data($sdsr[$i]) }&lt;/sdsr&gt;
			               &lt;/householdIndexesData&gt;							
						else ()		
			}
		&lt;/indexesData&gt;
};

declare function local:mapTrancheData($req as element(fml:FML32))
	as element(trancheData)* {
		let $nazwaKlienta := $req/fml:B_NAZWA_KLIENTA
		let $nrIban := $req/fml:B_NR_IBAN
		let $tranData := $req/fml:B_TRAN_DATA
		for $tranKwota at $i in $req/fml:B_TRAN_KWOTA
		return
			&lt;trancheData&gt;
				&lt;nazwaKlienta&gt;{ data($nazwaKlienta[$i]) }&lt;/nazwaKlienta&gt;
				&lt;nrIban&gt;{ data($nrIban[$i]) }&lt;/nrIban&gt;
				&lt;tranData&gt;{ data($tranData[$i]) }&lt;/tranData&gt;
				&lt;tranKwota&gt;{ data($tranKwota) }&lt;/tranKwota&gt;
			&lt;/trancheData&gt;
}; 

declare function local:mapMortgageDecisionTuxRequest($req as element(fml:FML32))
	as element(dec:mortgageDecisionRequest) {
		&lt;dec:mortgageDecisionRequest&gt;
			&lt;applicationDecision&gt;
				&lt;nrWniosku&gt;{ data($req/fml:B_NR_WNIOSKU[1]) }&lt;/nrWniosku&gt;
				&lt;status&gt;{ data($req/fml:B_STATUS) }&lt;/status&gt;
				{ local:mapApplicationData($req) }
				{ local:mapIndexesData($req) }
				{ local:mapTrancheData($req) }
			&lt;/applicationDecision&gt;
			&lt;userData&gt;
				&lt;imie&gt;{ data($req/fml:B_IMIE) }&lt;/imie&gt;
				&lt;nazwisko&gt;{ data($req/fml:B_NAZWISKO) }&lt;/nazwisko&gt;
				&lt;userId&gt;{ data($req/fml:B_USER_ID) }&lt;/userId&gt;
			&lt;/userData&gt;
		&lt;/dec:mortgageDecisionRequest&gt;
};




&lt;soap-env:Body&gt;
{ local:mapMortgageDecisionTuxRequest($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>