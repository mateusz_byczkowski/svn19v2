<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:dictionaries.be.dcl";
declare namespace m3 = "urn:basedictionaries.be.dcl";

declare namespace xm="urn:dcl:services.alsb.datamodel";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $fml as element(FML32):=$body/FML32;

declare function xm:dateTimeFromString($dateIn as xs:string) as xs:string {
  fn:concat(fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2),"T00:00:00")
};

declare function xm:dateFromString($dateIn as xs:string) as xs:string {
  fn:concat(fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2))
};

declare function xm:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))&gt;0)
    then true()
    else false()
};

declare function xm:isRok($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))=4)
    then true()
    else false()
};

declare function xm:strToBoolean($dateIn as xs:string) as xs:boolean {
  if ($dateIn = "1") 
    then true()
    else false()
};

&lt;soap-env:Body&gt;
  &lt;m:invokeResponse xmlns:urn="urn:be.services.dcl"&gt;
    &lt;m:collaterals xmlns:urn1="urn:accounts.entities.be.dcl" xmlns:urn2="urn:dictionaries.be.dcl" xmlns:urn3="urn:basedictionaries.be.dcl"&gt;
    {
      let $DC_KOD_NALICZENIA_REZERWY := $fml/fml:DC_KOD_NALICZENIA_REZERWY
      let $DC_ID_ZABEZPIECZENIA := $fml/fml:DC_ID_ZABEZPIECZENIA
      let $DC_KOD_ZABEZPIECZENIA := $fml/fml:DC_KOD_ZABEZPIECZENIA
      let $DC_INFORM_DODATK_O_ZABEZP := $fml/fml:DC_INFORM_DODATK_O_ZABEZP
      let $DC_NAZWA_SKROCONA := $fml/fml:DC_NAZWA_SKROCONA
      let $DC_OPIS_ZABEZP_1_LINIA := $fml/fml:DC_OPIS_ZABEZP_1_LINIA
      let $DC_OPIS_ZABEZP_2_LINIA := $fml/fml:DC_OPIS_ZABEZP_2_LINIA
      let $DC_OPIS_ZABEZP_3_LINIA := $fml/fml:DC_OPIS_ZABEZP_3_LINIA
      let $DC_DATA_WYGAS_LUB_ZAPADALNO := $fml/fml:DC_DATA_WYGAS_LUB_ZAPADALNO
      let $DC_WYMAGANE_UBEZPIECZENIE_T_N := $fml/fml:DC_WYMAGANE_UBEZPIECZENIE_T_N
      let $DC_DATA_WAZNOSCI_UBEZP := $fml/fml:DC_DATA_WAZNOSCI_UBEZP
      let $DC_DATA_REJESTRACJI_ZABEZP := $fml/fml:DC_DATA_REJESTRACJI_ZABEZP
      let $DC_DATA_WYGAS_REJESTRACJI := $fml/fml:DC_DATA_WYGAS_REJESTRACJI
      let $DC_CZESTOTL_WYCENY := $fml/fml:DC_CZESTOTL_WYCENY
      let $DC_OKRES_WYCENY_ZABEZP := $fml/fml:DC_OKRES_WYCENY_ZABEZP
      let $DC_DATA_PIERWSZEJ_WYCENY := $fml/fml:DC_DATA_PIERWSZEJ_WYCENY
      let $DC_KOMENTARZ_DLA_WYCENY := $fml/fml:DC_KOMENTARZ_DLA_WYCENY
      let $DC_LICZBA_JEDN_ZABEZP := $fml/fml:DC_LICZBA_JEDN_ZABEZP
      let $DC_WARTOSC_JEDNOSTKOWA := $fml/fml:DC_WARTOSC_JEDNOSTKOWA
      let $DC_WARTOSC_ZABEZPIECZENIA := $fml/fml:DC_WARTOSC_ZABEZPIECZENIA
      let $DC_SYMBOL_WALUTY := $fml/fml:DC_SYMBOL_WALUTY
      let $DC_PROCENT_POMNIEJSZ_REZERWY := $fml/fml:DC_PROCENT_POMNIEJSZ_REZERWY
      let $DC_MAKS_WARTOSC_ZABEZPIECZ := $fml/fml:DC_MAKS_WARTOSC_ZABEZPIECZ
      let $DC_DATA_POPRZEDNIEJ_WYCENY := $fml/fml:DC_DATA_POPRZEDNIEJ_WYCENY
      let $DC_METODA_WYCENY_ZABEZP := $fml/fml:DC_METODA_WYCENY_ZABEZP
      let $DC_OSOBA_WYCENIAJACA_ZABEZP := $fml/fml:DC_OSOBA_WYCENIAJACA_ZABEZP
      let $DC_OPIS_SKROCONY_ZABEZP := $fml/fml:DC_OPIS_SKROCONY_ZABEZP
      let $DC_NR_KSIEGI_WIECZYSTEJ_1 := $fml/fml:DC_NR_KSIEGI_WIECZYSTEJ_1
      let $DC_NR_KSIEGI_WIECZYSTEJ_2 := $fml/fml:DC_NR_KSIEGI_WIECZYSTEJ_2
      let $DC_HIPOTEKA_POZYCJA := $fml/fml:DC_HIPOTEKA_POZYCJA
      let $DC_ROK_BUDOWY_ROK_ZAKUPU_BUD := $fml/fml:DC_ROK_BUDOWY_ROK_ZAKUPU_BUD
      let $DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK := $fml/fml:DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK
      let $DC_CENA_ZAKUPU := $fml/fml:DC_CENA_ZAKUPU
      let $DC_STAN_NIERUCHOMOSCI := $fml/fml:DC_STAN_NIERUCHOMOSCI
      let $DC_POWIERZCHNIA_DZIALKI := $fml/fml:DC_POWIERZCHNIA_DZIALKI
      let $DC_POWIERZCHNIA_BUDYNKU := $fml/fml:DC_POWIERZCHNIA_BUDYNKU
      let $DC_RODZAJ_NIERUCHOMOSCI := $fml/fml:DC_RODZAJ_NIERUCHOMOSCI
      let $DC_KOD_WLASCICIELA := $fml/fml:DC_KOD_WLASCICIELA
      let $DC_LICZBA_MIESZKANCOW := $fml/fml:DC_LICZBA_MIESZKANCOW
      let $DC_TYTUL_PRAWNY := $fml/fml:DC_TYTUL_PRAWNY
      let $DC_DATA_ZL_WN_O_WPIS_HIPO := $fml/fml:DC_DATA_ZL_WN_O_WPIS_HIPO
      let $DC_PRZEWIDYWANA_DATA_ZAK := $fml/fml:DC_PRZEWIDYWANA_DATA_ZAK
      let $DC_SAD_REJONOWY := $fml/fml:DC_SAD_REJONOWY
      let $DC_SZACOWANA_WARTOSC := $fml/fml:DC_SZACOWANA_WARTOSC
      let $DC_DATA_SPORZ_WYCENY := $fml/fml:DC_DATA_SPORZ_WYCENY
      let $DC_NAZWISKO_RZECZOZNAWCY := $fml/fml:DC_NAZWISKO_RZECZOZNAWCY
      let $DC_WARTOSC_WPISU_DO_HIPOTEKI := $fml/fml:DC_WARTOSC_WPISU_DO_HIPOTEKI
      let $DC_POTW_ZAK_BUDOWY := $fml/fml:DC_POTW_ZAK_BUDOWY
      let $DC_ID_POLISY := $fml/fml:DC_ID_POLISY
      let $DC_WNIOSEK_WYKRESL_HIPO := $fml/fml:DC_WNIOSEK_WYKRESL_HIPO
      let $DC_HIPOTEKA_W_RAMACH_GUK := $fml/fml:DC_HIPOTEKA_W_RAMACH_GUK
      let $DC_HIPOTEKA_USTANOW_PRZEZ_BANK := $fml/fml:DC_HIPOTEKA_USTANOW_PRZEZ_BANK
      let $DC_DATA_PODPISANIA_AKTU_WLASNO := $fml/fml:DC_DATA_PODPISANIA_AKTU_WLASNO 
      let $DC_WNIOSEK_UTWORZENIE_KW := $fml/fml:DC_WNIOSEK_UTWORZENIE_KW 
      let $DC_ROK_PRODUKCJI_MODELU := $fml/fml:DC_ROK_PRODUKCJI_MODELU
      let $DC_RODZAJ_SRODKA_KOMUNIK := $fml/fml:DC_RODZAJ_SRODKA_KOMUNIK
      let $DC_PRZEWL_WARUNEK_ZAWIES := $fml/fml:DC_PRZEWL_WARUNEK_ZAWIES
      let $DC_DATA_UPLYWU_BADAN_TECH := $fml/fml:DC_DATA_UPLYWU_BADAN_TECH
      let $DC_RODZAJ_ZASTAWU_PRZEWL := $fml/fml:DC_RODZAJ_ZASTAWU_PRZEWL
      let $DC_DATA_ZLOZ_WN_O_WPIS_ZAST := $fml/fml:DC_DATA_ZLOZ_WN_O_WPIS_ZAST
      let $DC_NUMER_PODWOZIA_NADWOZIA := $fml/fml:DC_NUMER_PODWOZIA_NADWOZIA
      let $DC_NUMER_SILNIKA := $fml/fml:DC_NUMER_SILNIKA
      let $DC_NUMER_REJESTRACYJNY := $fml/fml:DC_NUMER_REJESTRACYJNY
      let $DC_SPOSOB_FINANSOWANIA := $fml/fml:DC_SPOSOB_FINANSOWANIA
      let $DC_MIEJSCE_REJESTRACJI := $fml/fml:DC_MIEJSCE_REJESTRACJI
      let $DC_MARKA := $fml/fml:DC_MARKA
      let $DC_TYP_NADWOZIA := $fml/fml:DC_TYP_NADWOZIA
      let $DC_BLOKADA_SRODK_NA_R_KU := $fml/fml:DC_BLOKADA_SRODK_NA_R_KU
      let $DC_NR_RACH_OBJETY_BLOKADA_SRO  := $fml/fml:DC_NR_RACH_OBJETY_BLOKADA_SRO
      let $DC_DATA_ROZPOCZECIA_REALIZACJI := $fml/fml:DC_DATA_ROZPOCZECIA_REALIZACJI
      let $DC_JEDNOSTKA_SPRZEDAJACA := $fml/fml:DC_JEDNOSTKA_SPRZEDAJACA
      let $DC_KOSZT_SPRZEDAZY_ZABEZP  := $fml/fml:DC_KOSZT_SPRZEDAZY_ZABEZP
      let $DC_DATA_ZAKONCZENIA_REALIZACJI := $fml/fml:DC_DATA_ZAKONCZENIA_REALIZACJI
      let $DC_WALUTA  := $fml/fml:DC_WALUTA
      let $DC_KWOTA := $fml/fml:DC_KWOTA
      let $DC_KWOTA_OBEC_ZABEZP   := $fml/fml:DC_KWOTA_OBEC_ZABEZP 
      let $DC_KWOTA_DOST_DO_ZABEZP := $fml/fml:DC_KWOTA_DOST_DO_ZABEZP 
      let $DC_PIERW_WART_ZABEZP := $fml/fml:DC_PIERW_WART_ZABEZP
      let $DC_DATA_WAGAS_ZABEZP := $fml/fml:DC_DATA_WAGAS_ZABEZP
      let $DC_NR_UMOWY_PORECZ := $fml/fml:DC_NR_UMOWY_PORECZ
      let $DC_NUMER_DOMU  := $fml/fml:DC_NUMER_DOMU
      let $DC_ULICA := $fml/fml:DC_ULICA
      let $DC_MIASTO_STATE  := $fml/fml:DC_MIASTO_STATE
      let $DC_KOD_POCZTOWY  := $fml/fml:DC_KOD_POCZTOWY
      let $DC_WOJEWODZTWO := $fml/fml:DC_WOJEWODZTWO
      let $DC_POWIAT  := $fml/fml:DC_POWIAT
      let $DC_KRAJ  := $fml/fml:DC_KRAJ
      let $DC_DOSTAR_ODPIS_KW := $fml/fml:DC_DOSTAR_ODPIS_KW

      for $it at $p in $fml/fml:DC_ID_ZABEZPIECZENIA
      return
        &lt;m1:Collateral&gt;
          {if($DC_KOD_NALICZENIA_REZERWY[$p] and xm:isData($DC_KOD_NALICZENIA_REZERWY[$p]))
             then &lt;m1:locationCode&gt;{data($DC_KOD_NALICZENIA_REZERWY[$p])}&lt;/m1:locationCode&gt;
             else ()
          }
          {if($DC_OPIS_ZABEZP_1_LINIA[$p] and xm:isData($DC_OPIS_ZABEZP_1_LINIA[$p]))
             then &lt;m1:collateralDescription&gt;{ concat(string(data($DC_OPIS_ZABEZP_1_LINIA[$p])),string(data($DC_OPIS_ZABEZP_2_LINIA[$p])),string(data($DC_OPIS_ZABEZP_3_LINIA[$p]))) }&lt;/m1:collateralDescription&gt;
             else ()
          }
          {if($DC_WARTOSC_ZABEZPIECZENIA[$p] and xm:isData($DC_WARTOSC_ZABEZPIECZENIA[$p]))
             then &lt;m1:collateralValue&gt;{ data($DC_WARTOSC_ZABEZPIECZENIA[$p]) }&lt;/m1:collateralValue&gt;
             else ()
          }
          {if($DC_DATA_WYGAS_LUB_ZAPADALNO[$p] and xm:isData($DC_DATA_WYGAS_LUB_ZAPADALNO[$p]))
             then &lt;m1:expirationOrMaturityDate&gt;{ xm:dateFromString($DC_DATA_WYGAS_LUB_ZAPADALNO[$p]) }&lt;/m1:expirationOrMaturityDate&gt;
             else ()
          }
          {if($DC_WYMAGANE_UBEZPIECZENIE_T_N[$p] and xm:isData($DC_WYMAGANE_UBEZPIECZENIE_T_N[$p]))
             then &lt;m1:insuranceRequired&gt;{ xm:strToBoolean($DC_WYMAGANE_UBEZPIECZENIE_T_N[$p]) }&lt;/m1:insuranceRequired&gt;
             else ()
          }
          {if($DC_DATA_WAZNOSCI_UBEZP[$p] and xm:isData($DC_DATA_WAZNOSCI_UBEZP[$p]))
             then &lt;m1:insuranceExpiryDate&gt;{ xm:dateFromString($DC_DATA_WAZNOSCI_UBEZP[$p]) }&lt;/m1:insuranceExpiryDate&gt;
             else ()
          }
          {if($DC_DATA_REJESTRACJI_ZABEZP[$p] and xm:isData($DC_DATA_REJESTRACJI_ZABEZP[$p]))
             then &lt;m1:legalClaimRegisteredDate&gt;{ xm:dateFromString($DC_DATA_REJESTRACJI_ZABEZP[$p]) }&lt;/m1:legalClaimRegisteredDate&gt;
             else ()
          }
          {if($DC_DATA_WYGAS_REJESTRACJI[$p] and xm:isData($DC_DATA_WYGAS_REJESTRACJI[$p]))
             then &lt;m1:legalClaimExpiryDate&gt;{ xm:dateFromString($DC_DATA_WYGAS_REJESTRACJI[$p]) }&lt;/m1:legalClaimExpiryDate&gt;
             else ()
          }
          {if($DC_CZESTOTL_WYCENY[$p] and xm:isData($DC_CZESTOTL_WYCENY[$p]))
             then &lt;m1:reviewFrequency&gt;{ data($DC_CZESTOTL_WYCENY[$p]) }&lt;/m1:reviewFrequency&gt;
             else ()
          }
          {if($DC_OKRES_WYCENY_ZABEZP[$p] and xm:isData($DC_OKRES_WYCENY_ZABEZP[$p]))
             then &lt;m1:reviewPeriod&gt;{ data($DC_OKRES_WYCENY_ZABEZP[$p]) }&lt;/m1:reviewPeriod&gt;
             else ()
          }
          {if($DC_DATA_PIERWSZEJ_WYCENY[$p] and xm:isData($DC_DATA_PIERWSZEJ_WYCENY[$p]))
             then &lt;m1:firstReviewDate&gt;{ xm:dateTimeFromString($DC_DATA_PIERWSZEJ_WYCENY[$p]) }&lt;/m1:firstReviewDate&gt;
             else ()
          }
          {if($DC_KOMENTARZ_DLA_WYCENY[$p] and xm:isData($DC_KOMENTARZ_DLA_WYCENY[$p]))
             then &lt;m1:reviewComments&gt;{ data($DC_KOMENTARZ_DLA_WYCENY[$p]) }&lt;/m1:reviewComments&gt;
             else ()
          }
          {if($DC_LICZBA_JEDN_ZABEZP[$p] and xm:isData($DC_LICZBA_JEDN_ZABEZP[$p]))
             then &lt;m1:numberOfUnits&gt;{ data($DC_LICZBA_JEDN_ZABEZP[$p]) }&lt;/m1:numberOfUnits&gt;
             else ()
          }
          {if($DC_WARTOSC_JEDNOSTKOWA[$p] and xm:isData($DC_WARTOSC_JEDNOSTKOWA[$p]))
             then &lt;m1:unitPrice&gt;{ data($DC_WARTOSC_JEDNOSTKOWA[$p]) }&lt;/m1:unitPrice&gt;
             else ()
          }
          {if($DC_PROCENT_POMNIEJSZ_REZERWY[$p] and xm:isData($DC_PROCENT_POMNIEJSZ_REZERWY[$p]))
             then &lt;m1:marginPercentage&gt;{ data($DC_PROCENT_POMNIEJSZ_REZERWY[$p]) }&lt;/m1:marginPercentage&gt;
             else ()
          }
          {if($DC_MAKS_WARTOSC_ZABEZPIECZ[$p] and xm:isData($DC_MAKS_WARTOSC_ZABEZPIECZ[$p]))
             then &lt;m1:maximumCollateralValue&gt;{ data($DC_MAKS_WARTOSC_ZABEZPIECZ[$p]) }&lt;/m1:maximumCollateralValue&gt;
             else ()
          }
          {if($DC_DATA_POPRZEDNIEJ_WYCENY[$p] and xm:isData($DC_DATA_POPRZEDNIEJ_WYCENY[$p]))
             then &lt;m1:dateLastPriced&gt;{ xm:dateFromString($DC_DATA_POPRZEDNIEJ_WYCENY[$p]) }&lt;/m1:dateLastPriced&gt;
             else ()
          }
          {if($DC_ID_ZABEZPIECZENIA[$p] and xm:isData($DC_ID_ZABEZPIECZENIA[$p]))
             then &lt;m1:collateralItemNumber&gt;{ data($DC_ID_ZABEZPIECZENIA[$p]) }&lt;/m1:collateralItemNumber&gt;
             else ()
          }
          {if($DC_INFORM_DODATK_O_ZABEZP[$p] and xm:isData($DC_INFORM_DODATK_O_ZABEZP[$p]))
             then &lt;m1:commodityIdCusip&gt;{ data($DC_INFORM_DODATK_O_ZABEZP[$p]) }&lt;/m1:commodityIdCusip&gt;
             else ()
          }
          {if($DC_NR_RACH_OBJETY_BLOKADA_SRO[$p] and xm:isData($DC_NR_RACH_OBJETY_BLOKADA_SRO[$p]))
             then &lt;m1:itemReferenceNumber&gt;{ data($DC_NR_RACH_OBJETY_BLOKADA_SRO[$p]) }&lt;/m1:itemReferenceNumber&gt;
             else ()
          }
          {if($DC_INFORM_DODATK_O_ZABEZP[$p] and xm:isData($DC_INFORM_DODATK_O_ZABEZP[$p]))
             then &lt;m1:safekeepingReceiptNbr&gt;{ data($DC_INFORM_DODATK_O_ZABEZP[$p]) }&lt;/m1:safekeepingReceiptNbr&gt;
             else ()
          }
          {if($DC_NAZWA_SKROCONA[$p] and xm:isData($DC_NAZWA_SKROCONA[$p]))
             then &lt;m1:shortDescription&gt;{ data($DC_NAZWA_SKROCONA[$p]) }&lt;/m1:shortDescription&gt;
             else ()
          }
          {if($DC_DATA_ROZPOCZECIA_REALIZACJI[$p] and xm:isData($DC_DATA_ROZPOCZECIA_REALIZACJI[$p]))
             then &lt;m1:startDateOfSecLiquidat&gt;{ xm:dateFromString($DC_DATA_ROZPOCZECIA_REALIZACJI[$p]) }&lt;/m1:startDateOfSecLiquidat&gt;
             else ()
          }
          {if($DC_KOSZT_SPRZEDAZY_ZABEZP[$p] and xm:isData($DC_KOSZT_SPRZEDAZY_ZABEZP[$p]))
             then &lt;m1:costOfSalesOfSecurity&gt;{ data($DC_KOSZT_SPRZEDAZY_ZABEZP[$p]) }&lt;/m1:costOfSalesOfSecurity&gt;
             else ()
          }
          {if($DC_DATA_ZAKONCZENIA_REALIZACJI[$p] and xm:isData($DC_DATA_ZAKONCZENIA_REALIZACJI[$p]))
             then &lt;m1:endDateOfSecLiquidat&gt;{ xm:dateFromString($DC_DATA_ZAKONCZENIA_REALIZACJI[$p]) }&lt;/m1:endDateOfSecLiquidat&gt;
             else ()
          }
          {if($DC_WALUTA[$p] and xm:isData($DC_WALUTA[$p]))
             then &lt;m1:ccy&gt;{ data($DC_WALUTA[$p]) }&lt;/m1:ccy&gt;
             else ()
          }
          {if($DC_KWOTA[$p] and xm:isData($DC_KWOTA[$p]))
             then &lt;m1:amt&gt;{ data($DC_KWOTA[$p]) }&lt;/m1:amt&gt;
             else ()
          }
          {if($DC_KWOTA_OBEC_ZABEZP[$p] and xm:isData($DC_KWOTA_OBEC_ZABEZP[$p]))
             then &lt;m1:amountCurrentlyPledged&gt;{ data($DC_KWOTA_OBEC_ZABEZP[$p]) }&lt;/m1:amountCurrentlyPledged&gt;
             else ()
          } 
          {if($DC_KWOTA_DOST_DO_ZABEZP[$p] and xm:isData($DC_KWOTA_DOST_DO_ZABEZP[$p]))
             then &lt;m1:availableToPledge&gt;{ data($DC_KWOTA_DOST_DO_ZABEZP[$p]) }&lt;/m1:availableToPledge&gt;
             else ()
          } 
          {if($DC_PIERW_WART_ZABEZP[$p] and xm:isData($DC_PIERW_WART_ZABEZP[$p]))
             then &lt;m1:originalCollateralValue&gt;{ data($DC_PIERW_WART_ZABEZP[$p]) }&lt;/m1:originalCollateralValue&gt;
             else ()
          }
          {if($DC_DATA_WAGAS_ZABEZP[$p] and xm:isData($DC_DATA_WAGAS_ZABEZP[$p]))
             then &lt;m1:collateralInactiveDate&gt;{ xm:dateFromString($DC_DATA_WAGAS_ZABEZP[$p]) }&lt;/m1:collateralInactiveDate&gt;
             else ()
          }
          {if($DC_NR_UMOWY_PORECZ[$p] and xm:isData($DC_NR_UMOWY_PORECZ[$p]))
             then &lt;m1:userField6&gt;{ data($DC_NR_UMOWY_PORECZ[$p]) }&lt;/m1:userField6&gt;
             else ()
          }

          &lt;m1:collateralMortgage&gt;
            &lt;m1:CollateralMortgage&gt;
              {if($DC_OPIS_SKROCONY_ZABEZP[$p] and xm:isData($DC_OPIS_SKROCONY_ZABEZP[$p]))
                 then &lt;m1:shortLegalDescription&gt;{ data($DC_OPIS_SKROCONY_ZABEZP[$p]) }&lt;/m1:shortLegalDescription&gt;
                 else ()
              }
              {if($DC_NR_KSIEGI_WIECZYSTEJ_1[$p] and xm:isData($DC_NR_KSIEGI_WIECZYSTEJ_1[$p]))
                 then &lt;m1:legalRegistrationNumber&gt;{data($DC_NR_KSIEGI_WIECZYSTEJ_1[$p]) }&lt;/m1:legalRegistrationNumber&gt;
                 else ()
              }
              {if($DC_ROK_BUDOWY_ROK_ZAKUPU_BUD[$p] and xm:isRok($DC_ROK_BUDOWY_ROK_ZAKUPU_BUD[$p]))
                 then &lt;m1:yearBuilt&gt;{ data($DC_ROK_BUDOWY_ROK_ZAKUPU_BUD[$p]) }&lt;/m1:yearBuilt&gt;
                 else ()
              }
              {if($DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK[$p] and xm:isRok($DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK[$p]))
                 then &lt;m1:yearPurchased&gt;{ data($DC_ROK_BUDOWY_ROK_ZAKUPU_ZAK[$p]) }&lt;/m1:yearPurchased&gt;
                 else ()
              }
              {if($DC_CENA_ZAKUPU[$p] and xm:isData($DC_CENA_ZAKUPU[$p]))
                 then &lt;m1:purchasePrice&gt;{ data($DC_CENA_ZAKUPU[$p]) }&lt;/m1:purchasePrice&gt;
                 else ()
              }
              {if($DC_POWIERZCHNIA_DZIALKI[$p] and xm:isData($DC_POWIERZCHNIA_DZIALKI[$p]))
                 then &lt;m1:lotSize&gt;{ data($DC_POWIERZCHNIA_DZIALKI[$p]) }&lt;/m1:lotSize&gt;
                 else ()
              }
              {if($DC_POWIERZCHNIA_BUDYNKU[$p] and xm:isData($DC_POWIERZCHNIA_BUDYNKU[$p]))
                 then &lt;m1:squareFeet&gt;{ data($DC_POWIERZCHNIA_BUDYNKU[$p]) }&lt;/m1:squareFeet&gt;
                 else ()
              }
              {if($DC_LICZBA_MIESZKANCOW[$p] and xm:isData($DC_LICZBA_MIESZKANCOW[$p]))
                 then &lt;m1:numberOfResidents&gt;{ data($DC_LICZBA_MIESZKANCOW[$p]) }&lt;/m1:numberOfResidents&gt;
                 else ()
              }
              {if($DC_DATA_ZL_WN_O_WPIS_HIPO[$p] and xm:isData($DC_DATA_ZL_WN_O_WPIS_HIPO[$p]))
                 then &lt;m1:leaseExpirationDate&gt;{ xm:dateFromString($DC_DATA_ZL_WN_O_WPIS_HIPO[$p]) }&lt;/m1:leaseExpirationDate&gt;
                 else ()
              }
              {if($DC_PRZEWIDYWANA_DATA_ZAK[$p] and xm:isData($DC_PRZEWIDYWANA_DATA_ZAK[$p]))
                 then &lt;m1:dateInspected&gt;{ xm:dateFromString($DC_PRZEWIDYWANA_DATA_ZAK[$p]) }&lt;/m1:dateInspected&gt;
                 else ()
              }
              {if($DC_SAD_REJONOWY[$p] and xm:isData($DC_SAD_REJONOWY[$p]))
                 then &lt;m1:subdivision&gt;{ data($DC_SAD_REJONOWY[$p]) }&lt;/m1:subdivision&gt;
                 else ()
              }
              {if($DC_SZACOWANA_WARTOSC[$p] and xm:isData($DC_SZACOWANA_WARTOSC[$p]))
                 then &lt;m1:appraisedValue&gt;{ data($DC_SZACOWANA_WARTOSC[$p]) }&lt;/m1:appraisedValue&gt;
                 else ()
              }
              {if($DC_DATA_SPORZ_WYCENY[$p] and xm:isData($DC_DATA_SPORZ_WYCENY[$p]))
                 then &lt;m1:appraisalDate&gt;{ xm:dateFromString($DC_DATA_SPORZ_WYCENY[$p]) }&lt;/m1:appraisalDate&gt;
                 else ()
              }
              {if($DC_NAZWISKO_RZECZOZNAWCY[$p] and xm:isData($DC_NAZWISKO_RZECZOZNAWCY[$p]))
                 then &lt;m1:appraiserName&gt;{ data($DC_NAZWISKO_RZECZOZNAWCY[$p]) }&lt;/m1:appraiserName&gt;
                 else ()
              }
              {if($DC_WARTOSC_WPISU_DO_HIPOTEKI[$p] and xm:isData($DC_WARTOSC_WPISU_DO_HIPOTEKI[$p]))
                 then &lt;m1:mortgagesPayablePrior&gt;{ data($DC_WARTOSC_WPISU_DO_HIPOTEKI[$p]) }&lt;/m1:mortgagesPayablePrior&gt;
                 else ()
              }
              {if($DC_POTW_ZAK_BUDOWY[$p] and xm:isData($DC_POTW_ZAK_BUDOWY[$p]))
                 then &lt;m1:userField2&gt;{ data($DC_POTW_ZAK_BUDOWY[$p]) }&lt;/m1:userField2&gt;
                 else ()
              }
              {if($DC_ID_POLISY[$p] and xm:isData($DC_ID_POLISY[$p]))
                 then &lt;m1:userField3&gt;{ data($DC_ID_POLISY[$p]) }&lt;/m1:userField3&gt;
                 else ()
              }
              {if($DC_WNIOSEK_WYKRESL_HIPO[$p] and xm:isData($DC_WNIOSEK_WYKRESL_HIPO[$p]))
                 then &lt;m1:userField4&gt;{ data($DC_WNIOSEK_WYKRESL_HIPO[$p]) }&lt;/m1:userField4&gt;
                 else ()
              }
              {if($DC_DATA_PODPISANIA_AKTU_WLASNO[$p] and xm:isData($DC_DATA_PODPISANIA_AKTU_WLASNO[$p]))
                 then &lt;m1:userField5&gt;{ xm:dateFromString($DC_DATA_PODPISANIA_AKTU_WLASNO[$p]) }&lt;/m1:userField5&gt;
                 else ()
              }
              {if($DC_HIPOTEKA_USTANOW_PRZEZ_BANK[$p] and xm:isData($DC_HIPOTEKA_USTANOW_PRZEZ_BANK[$p]))
                 then &lt;m1:newAdditionalUserField2&gt;{ data($DC_HIPOTEKA_USTANOW_PRZEZ_BANK[$p]) }&lt;/m1:newAdditionalUserField2&gt;
                 else ()
              }
              {if($DC_HIPOTEKA_W_RAMACH_GUK[$p] and xm:isData($DC_HIPOTEKA_W_RAMACH_GUK[$p]))
                 then &lt;m1:newAdditionalUserField1&gt;{ xm:dateFromString($DC_HIPOTEKA_W_RAMACH_GUK[$p]) }&lt;/m1:newAdditionalUserField1&gt;
                 else ()
              }
              {if($DC_WNIOSEK_UTWORZENIE_KW[$p] and xm:isData($DC_WNIOSEK_UTWORZENIE_KW[$p]))
                 then &lt;m1:newAdditionalUserField3&gt;{ xm:strToBoolean($DC_WNIOSEK_UTWORZENIE_KW[$p]) }&lt;/m1:newAdditionalUserField3&gt;
                 else ()
              }    
              {if($DC_NR_KSIEGI_WIECZYSTEJ_2[$p] and xm:isData($DC_NR_KSIEGI_WIECZYSTEJ_2[$p]))
                 then &lt;m1:legalRegistrationDate&gt;{ xm:dateFromString($DC_NR_KSIEGI_WIECZYSTEJ_2[$p]) }&lt;/m1:legalRegistrationDate&gt;
                 else ()
              }
              {if($DC_NUMER_DOMU[$p] and xm:isData($DC_NUMER_DOMU[$p]))
                 then &lt;m1:houseNumber&gt;{ data($DC_NUMER_DOMU[$p]) }&lt;/m1:houseNumber&gt;
                 else ()
              }
              {if($DC_ULICA[$p] and xm:isData($DC_ULICA[$p]))
                 then &lt;m1:street&gt;{ data($DC_ULICA[$p]) }&lt;/m1:street&gt;
                 else ()
              }
              {if($DC_MIASTO_STATE[$p] and xm:isData($DC_MIASTO_STATE[$p]))
                 then &lt;m1:city&gt;{ data($DC_MIASTO_STATE[$p]) }&lt;/m1:city&gt;
                 else ()
              }
              {if($DC_KOD_POCZTOWY[$p] and xm:isData($DC_KOD_POCZTOWY[$p]))
                 then &lt;m1:postalCode&gt;{ data($DC_KOD_POCZTOWY[$p]) }&lt;/m1:postalCode&gt;
                 else ()
              }
              {if($DC_KRAJ[$p] and xm:isData($DC_KRAJ[$p]))
                 then &lt;m1:country&gt;{ data($DC_KRAJ[$p]) }&lt;/m1:country&gt;
                 else ()
              }
              {if($DC_POWIAT[$p] and xm:isData($DC_POWIAT[$p]))
                 then &lt;m1:district&gt;{ data($DC_POWIAT[$p]) }&lt;/m1:district&gt;
                 else ()
              }
              {if($DC_WOJEWODZTWO[$p] and xm:isData($DC_WOJEWODZTWO[$p]))
                 then &lt;m1:county&gt;{ data($DC_WOJEWODZTWO[$p]) }&lt;/m1:county&gt;
                 else ()
              }
              {if($DC_DOSTAR_ODPIS_KW[$p] and xm:isData($DC_DOSTAR_ODPIS_KW[$p]))
                 then &lt;m1:userField1&gt;{ data($DC_DOSTAR_ODPIS_KW[$p]) }&lt;/m1:userField1&gt;
                 else ()
              }        
              {if($DC_STAN_NIERUCHOMOSCI[$p] and xm:isData($DC_STAN_NIERUCHOMOSCI[$p]))
                 then &lt;m1:newUsedIndicatori&gt;&lt;m2:NewUsedType&gt;&lt;m2:newUsedType&gt;{ data($DC_STAN_NIERUCHOMOSCI[$p]) }&lt;/m2:newUsedType&gt;&lt;/m2:NewUsedType&gt;&lt;/m1:newUsedIndicatori&gt;
                 else ()
              }
              {if($DC_RODZAJ_NIERUCHOMOSCI[$p] and xm:isData($DC_RODZAJ_NIERUCHOMOSCI[$p]))
                 then &lt;m1:propertyType&gt;&lt;m2:PropertyType&gt;&lt;m2:propertyType&gt;{ data($DC_RODZAJ_NIERUCHOMOSCI[$p]) }&lt;/m2:propertyType&gt;&lt;/m2:PropertyType&gt;&lt;/m1:propertyType&gt;
                 else ()
              }
              {if($DC_KOD_WLASCICIELA[$p] and xm:isData($DC_KOD_WLASCICIELA[$p]))
                 then &lt;m1:ownerOccupiedCode&gt;&lt;m2:OwnerOccupiedCode&gt;&lt;m2:ownerOccupiedCode&gt;{ data($DC_KOD_WLASCICIELA[$p]) }&lt;/m2:ownerOccupiedCode&gt;&lt;/m2:OwnerOccupiedCode&gt;&lt;/m1:ownerOccupiedCode&gt;
                 else ()
              }
              {if($DC_TYTUL_PRAWNY[$p] and xm:isData($DC_TYTUL_PRAWNY[$p]))
                 then &lt;m1:tenure&gt;&lt;m2:Tenure&gt;&lt;m2:tenure&gt;{ data($DC_TYTUL_PRAWNY[$p]) }&lt;/m2:tenure&gt;&lt;/m2:Tenure&gt;&lt;/m1:tenure&gt;
                 else ()
              }
              {if($DC_HIPOTEKA_POZYCJA[$p] and xm:isData($DC_HIPOTEKA_POZYCJA[$p]))
                 then &lt;m1:realEstateUserField1&gt;&lt;m2:CollateralMortgagePosition&gt;&lt;m2:collateralMortgagePosition&gt;{ data($DC_HIPOTEKA_POZYCJA[$p]) }&lt;/m2:collateralMortgagePosition&gt;&lt;/m2:CollateralMortgagePosition&gt;&lt;/m1:realEstateUserField1&gt;
                 else ()
              }
            &lt;/m1:CollateralMortgage&gt;
          &lt;/m1:collateralMortgage&gt;

          &lt;m1:collateralPledge&gt;
            &lt;m1:CollateralCar&gt;
              {if($DC_ROK_PRODUKCJI_MODELU[$p] and xm:isRok($DC_ROK_PRODUKCJI_MODELU[$p]))
                 then &lt;m1:modelYear&gt;{ data($DC_ROK_PRODUKCJI_MODELU[$p]) }&lt;/m1:modelYear&gt;
                 else ()
              }
              {if($DC_PRZEWL_WARUNEK_ZAWIES[$p] and xm:isData($DC_PRZEWL_WARUNEK_ZAWIES[$p]))
                 then &lt;m1:rentalDemoFlag&gt;{ xm:strToBoolean($DC_PRZEWL_WARUNEK_ZAWIES[$p]) }&lt;/m1:rentalDemoFlag&gt;
                 else ()
              }
              {if($DC_DATA_UPLYWU_BADAN_TECH[$p] and xm:isData($DC_DATA_UPLYWU_BADAN_TECH[$p]))
                 then &lt;m1:vehicleLicenseExpiryDate&gt;{ xm:dateFromString($DC_DATA_UPLYWU_BADAN_TECH[$p]) }&lt;/m1:vehicleLicenseExpiryDate&gt;
                 else ()
              }
              {if($DC_DATA_ZLOZ_WN_O_WPIS_ZAST[$p] and xm:isData($DC_DATA_ZLOZ_WN_O_WPIS_ZAST[$p]))
                 then &lt;m1:firstRegistrationDate&gt;{ xm:dateFromString($DC_DATA_ZLOZ_WN_O_WPIS_ZAST[$p]) }&lt;/m1:firstRegistrationDate&gt;
                 else ()
              }
              {if($DC_NUMER_PODWOZIA_NADWOZIA[$p] and xm:isData($DC_NUMER_PODWOZIA_NADWOZIA[$p]))
                 then &lt;m1:serialNumber&gt;{ data($DC_NUMER_PODWOZIA_NADWOZIA[$p]) }&lt;/m1:serialNumber&gt;
                 else ()
              }
              {if($DC_NUMER_SILNIKA[$p] and xm:isData($DC_NUMER_SILNIKA[$p]))
                 then &lt;m1:motorNumber&gt;{ data($DC_NUMER_SILNIKA[$p]) }&lt;/m1:motorNumber&gt;
                 else ()
              }
              {if($DC_NUMER_REJESTRACYJNY[$p] and xm:isData($DC_NUMER_REJESTRACYJNY[$p]))
                 then &lt;m1:plateNumber&gt;{ data($DC_NUMER_REJESTRACYJNY[$p]) }&lt;/m1:plateNumber&gt;
                 else ()
              }
              {if($DC_RODZAJ_SRODKA_KOMUNIK[$p] and xm:isData($DC_RODZAJ_SRODKA_KOMUNIK[$p]))
                 then &lt;m1:conditionFlag&gt;&lt;m2:ConditionFlag&gt;&lt;m2:conditionFlag&gt;{ data($DC_RODZAJ_SRODKA_KOMUNIK[$p]) }&lt;/m2:conditionFlag&gt;&lt;/m2:ConditionFlag&gt;&lt;/m1:conditionFlag&gt;
                 else ()
              }
              {if($DC_RODZAJ_ZASTAWU_PRZEWL[$p] and xm:isData($DC_RODZAJ_ZASTAWU_PRZEWL[$p]))
                 then &lt;m1:licenseTypeCode&gt;&lt;m2:LicenseTypeCode&gt;&lt;m2:licenseTypeCode&gt;{ data($DC_RODZAJ_ZASTAWU_PRZEWL[$p]) }&lt;/m2:licenseTypeCode&gt;&lt;/m2:LicenseTypeCode&gt;&lt;/m1:licenseTypeCode&gt;
                 else ()
              }
              {if($DC_SPOSOB_FINANSOWANIA[$p] and xm:isData($DC_SPOSOB_FINANSOWANIA[$p]))
                 then &lt;m1:financeTypeCode&gt;&lt;m2:FinanceTypeCode&gt;&lt;m2:financeTypeCode&gt;{ data($DC_SPOSOB_FINANSOWANIA[$p]) }&lt;/m2:financeTypeCode&gt;&lt;/m2:FinanceTypeCode&gt;&lt;/m1:financeTypeCode&gt;
                 else ()
              }
              {if($DC_MIEJSCE_REJESTRACJI[$p] and xm:isData($DC_MIEJSCE_REJESTRACJI[$p]))
                 then &lt;m1:registrationState&gt;&lt;m3:BaseState&gt;&lt;m3:state&gt;{ data($DC_MIEJSCE_REJESTRACJI[$p]) }&lt;/m3:state&gt;&lt;/m3:BaseState&gt;&lt;/m1:registrationState&gt;
                 else ()
              }
              {if($DC_MARKA[$p] and xm:isData($DC_MARKA[$p]))
                 then &lt;m1:makeCode&gt;&lt;m2:MakeCode&gt;&lt;m2:makeCode&gt;{ data($DC_MARKA[$p]) }&lt;/m2:makeCode&gt;&lt;/m2:MakeCode&gt;&lt;/m1:makeCode&gt;
                 else ()
              }
              {if($DC_TYP_NADWOZIA[$p] and xm:isData($DC_TYP_NADWOZIA[$p]))
                 then &lt;m1:bodyCode&gt;&lt;m2:BodyCode&gt;&lt;m2:bodyCode&gt;{ data($DC_TYP_NADWOZIA[$p]) }&lt;/m2:bodyCode&gt;&lt;/m2:BodyCode&gt;&lt;/m1:bodyCode&gt;
                 else ()
              }
            &lt;/m1:CollateralCar&gt;
          &lt;/m1:collateralPledge&gt;

          {if($DC_KOD_ZABEZPIECZENIA[$p] and xm:isData($DC_KOD_ZABEZPIECZENIA[$p]))
             then &lt;m1:collateralCode&gt;&lt;m2:CollateralCode&gt;&lt;m2:collateralCode&gt;{ data($DC_KOD_ZABEZPIECZENIA[$p]) }&lt;/m2:collateralCode&gt;&lt;/m2:CollateralCode&gt;&lt;/m1:collateralCode&gt;
             else ()
          }
          {if($DC_SYMBOL_WALUTY[$p] and xm:isData($DC_SYMBOL_WALUTY[$p]))
             then &lt;m1:currencyCode&gt;&lt;m2:CurrencyCode&gt;&lt;m2:currencyCode&gt;{ data($DC_SYMBOL_WALUTY[$p]) }&lt;/m2:currencyCode&gt;&lt;/m2:CurrencyCode&gt;&lt;/m1:currencyCode&gt;
             else ()
          }
          {if($DC_METODA_WYCENY_ZABEZP[$p] and xm:isData($DC_METODA_WYCENY_ZABEZP[$p]) and data($DC_METODA_WYCENY_ZABEZP[$p]) != 0)
             then &lt;m1:methodOfSecurityValuation&gt;&lt;m2:MethodOfSecurityValuation&gt;&lt;m2:methodOfSecurityValuation&gt;{ data($DC_METODA_WYCENY_ZABEZP[$p]) }&lt;/m2:methodOfSecurityValuation&gt;&lt;/m2:MethodOfSecurityValuation&gt;&lt;/m1:methodOfSecurityValuation&gt;
             else ()
          }
          {if($DC_OSOBA_WYCENIAJACA_ZABEZP[$p] and xm:isData($DC_OSOBA_WYCENIAJACA_ZABEZP[$p]))
             then &lt;m1:personValuatingASecurity&gt;&lt;m2:PersonValuatingASecurity&gt;&lt;m2:personValuatingASecurity&gt;{ data($DC_OSOBA_WYCENIAJACA_ZABEZP[$p]) }&lt;/m2:personValuatingASecurity&gt;&lt;/m2:PersonValuatingASecurity&gt;&lt;/m1:personValuatingASecurity&gt;
             else ()
          }
          {if($DC_BLOKADA_SRODK_NA_R_KU[$p] and xm:isData($DC_BLOKADA_SRODK_NA_R_KU[$p]))
             then &lt;m1:itemApplicationNumber&gt;&lt;m2:ApplicationNumber&gt;&lt;m2:applicationNumber&gt;{ data($DC_BLOKADA_SRODK_NA_R_KU[$p]) }&lt;/m2:applicationNumber&gt;&lt;/m2:ApplicationNumber&gt;&lt;/m1:itemApplicationNumber&gt;
             else ()
          }
          {if($DC_JEDNOSTKA_SPRZEDAJACA[$p] and xm:isData($DC_JEDNOSTKA_SPRZEDAJACA[$p]))
             then &lt;m1:entity&gt;&lt;m2:CollateralEntity&gt;&lt;m2:collateralEntity&gt;{ data($DC_JEDNOSTKA_SPRZEDAJACA[$p]) }&lt;/m2:collateralEntity&gt;&lt;/m2:CollateralEntity&gt;&lt;/m1:entity&gt;
             else ()
          }
        &lt;/m1:Collateral&gt;
    }
    &lt;/m:collaterals&gt;
  &lt;/m:invokeResponse&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>