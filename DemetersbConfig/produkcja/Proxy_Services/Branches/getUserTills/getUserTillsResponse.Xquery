<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns="http://bzwbk.com/services/getUserTills";


declare variable $body as element(soap-env:Body) external;

(:~
 : @param $fml32 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function local:getUserTillsResponse($fml32 as element(FML32))
    as element(ns:invokeResponse)
{
    &lt;ns:invokeResponse&gt;
    {
    	let $sessionDate := $fml32/NF_USERTS_SESSIONDATE
		let $lastChangeDate := $fml32/NF_USERTS_LASTCHANGEDATE
		let $sessionStatus := $fml32/NF_USETSS_USERTXNSESSIONST
		let $userLastName :=$fml32/NF_USER_USERLASTNAME
		let $userID := $fml32/NF_USER_USERID
		let $userFirstName := $fml32/NF_USER_USERFIRSTNAME
		let $tillID := $fml32/NF_TILL_TILLID
		let $tillName := $fml32/NF_TILL_TILLNAME
		let $tillType := $fml32/NF_TILTY_TILLTYP
		let $tellerID := $fml32/NF_TELLER_TELLERID
		let $branchCode := $fml32/NF_BRANCC_BRANCHCODE
	    for $sessionNumber at $i in $fml32/NF_USERTS_SESSIONNUMBER
        return
            &lt;userTxnSession&gt;
                &lt;sessionNumber&gt;{ data($sessionNumber) }&lt;/sessionNumber&gt;
				&lt;sessionDate?&gt;{ data($sessionDate[$i]) }&lt;/sessionDate&gt;
                (:
                 : status sesji:
                 : - 0 --&gt; C (zamknięta)
                 : - 1 --&gt; O (otwarta)
                 :)
                &lt;sessionStatus&gt;
                {
                	if (data($sessionStatus[$i]) eq '0') then
                		'C'
                	else if(data($sessionStatus[$i]) eq '1') then
                		'O'
                	else
                   		()
                }
                &lt;/sessionStatus&gt;
				&lt;lastChangeDate?&gt;{ if(data($lastChangeDate[$i]) != '') then data($lastChangeDate[$i]) else () }&lt;/lastChangeDate&gt;
                &lt;till&gt;
                    &lt;tillID?&gt;{ data($tillID[$i]) }&lt;/tillID&gt;
                    &lt;tillName?&gt;{ data($tillName[$i]) }&lt;/tillName&gt;
                    &lt;tillType?&gt;{ data($tillType[$i]) }&lt;/tillType&gt;
                &lt;/till&gt;
                &lt;tellerID?&gt;{ data($tellerID[$i]) }&lt;/tellerID&gt;
                &lt;user&gt;
                    &lt;userID?&gt;{ data($userID[$i]) }&lt;/userID&gt;
                    &lt;userFirstName?&gt;{ data($userFirstName[$i]) }&lt;/userFirstName&gt;
                    &lt;userLastName?&gt;{ data($userLastName[$i]) }&lt;/userLastName&gt;
                &lt;/user&gt;
                &lt;branchCode?&gt;{ data($branchCode[$i]) }&lt;/branchCode&gt;
            &lt;/userTxnSession&gt;
	}
	
	    &lt;pageControl?&gt;
			&lt;hasNext?&gt;{ xs:boolean(data($fml32/NF_PAGEC_HASNEXT)) }&lt;/hasNext&gt;
	        &lt;navigationKeyDefinition?&gt;{ data($fml32/NF_PAGEC_NAVIGATIONKEYDEFI) }&lt;/navigationKeyDefinition&gt;
	        &lt;navigationKeyValue?&gt;{ data($fml32/NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/navigationKeyValue&gt;
	        &lt;totalSize?&gt;{ data($fml32/NF_PAGECC_OPERATIONS) }&lt;/totalSize&gt;
	    &lt;/pageControl&gt;
    &lt;/ns:invokeResponse&gt;
};

&lt;soap-env:Body&gt;{
	local:getUserTillsResponse($body/FML32)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>