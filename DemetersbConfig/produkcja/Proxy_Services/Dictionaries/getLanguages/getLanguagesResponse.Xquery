<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="urn:basedictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
&lt;ns0:dicts&gt;
  {
    let $opis := $parm/CI_OPIS_WARTOSCI_SLOWNIKA
    for $wartosc at $occ in $parm/CI_WARTOSC_SLOWNIKA
    return
    &lt;ns1:Language&gt;
      {if (fn:string-length($wartosc)&gt;0)
        then &lt;ns1:languageID&gt;{data($wartosc)}&lt;/ns1:languageID&gt;
        else &lt;ns1:languageID&gt;{" "}&lt;/ns1:languageID&gt;
      }
      &lt;ns1:description?&gt;{data($opis[$occ])}&lt;/ns1:description&gt;
    &lt;/ns1:Language&gt;
  }
&lt;/ns0:dicts&gt;
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
&lt;ns0:invokeResponse&gt;
  {getElementsForDicts($parm)}
&lt;/ns0:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>