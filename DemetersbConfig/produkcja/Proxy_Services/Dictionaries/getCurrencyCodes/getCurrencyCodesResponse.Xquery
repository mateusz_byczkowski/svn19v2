<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(: Change log
v.1.1 2010-03-15 PK NP1696 Dodanie pól wyjściowych
v.1.2 2010-05-11 PK T47137 Zmiana mapowania pola currencyCode
:)

declare namespace fml="";
declare namespace urn="urn:be.services.dcl";
declare namespace urn1="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function Num2Bool($dateIn as xs:string) as xs:boolean {
 if ($dateIn = "1") 
    then true()
    else false()
};

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{
&lt;urn:dicts&gt;
  {
    let $waluta := $parm/B_WALUTA
    let $retdata := $parm/B_RET_DATA 
    let $kraj := $parm/B_KRAJ (: 1.1 :)
    let $nazwa := $parm/B_NAZWA
    let $allowedCashPosting:= $parm/NF_CURREC_ALLOWEDCASHPOSTI 
    let $allowedPosting:= $parm/NF_CURREC_ALLOWEDPOSTING
    let $precisionOfCurrency:= $parm/NF_CURREC_PRECISIONOFCURRE
    let $precisionOfRate:= $parm/NF_CURREC_PRECISIONOFRATE
    let $presentationsOrder := $parm/NF_CURREC_PRESENTATIONSORD

    
    for $kodps at $occ in $parm/B_KOD_PS
    return
    &lt;urn1:CurrencyCode&gt;
      &lt;urn1:allowedCashPosting?&gt;{Num2Bool(data($allowedCashPosting[$occ]))}&lt;/urn1:allowedCashPosting&gt;
      &lt;urn1:allowedPosting?&gt;{Num2Bool(data($allowedPosting[$occ]))}&lt;/urn1:allowedPosting&gt;
      (: 1.2 &lt;urn1:currencyCode?&gt;{data($kodps)}&lt;/urn1:currencyCode&gt; :)
      &lt;urn1:currencyCode?&gt;{data($retdata[$occ])}&lt;/urn1:currencyCode&gt; (: 1.2 :)
      &lt;urn1:description?&gt;{data($nazwa[$occ])}&lt;/urn1:description&gt;
      &lt;urn1:iso4217Code?&gt;{data($waluta[$occ])}&lt;/urn1:iso4217Code&gt;
      &lt;urn1:precisionOfCurrency?&gt;{data($precisionOfCurrency[$occ])}&lt;/urn1:precisionOfCurrency&gt;
      &lt;urn1:precisionOfRate?&gt;{data($precisionOfRate[$occ])}&lt;/urn1:precisionOfRate&gt;
      &lt;urn1:presentationsOrder?&gt;{data($presentationsOrder[$occ])}&lt;/urn1:presentationsOrder&gt;
      &lt;urn1:countryOfCurrency?&gt;
        &lt;urn1:CountryCode?&gt;
          (: &lt;urn1:countryCode?&gt;{if(data($kodps) = "978") then "" else fn:substring(data($retdata[$occ]),1,2)}&lt;/urn1:countryCode&gt; 1.1 :)
             &lt;urn1:countryCode?&gt;{data($kraj[$occ])}&lt;/urn1:countryCode&gt; (: 1.1 :)
        &lt;/urn1:CountryCode&gt;
      &lt;/urn1:countryOfCurrency&gt;
    &lt;/urn1:CurrencyCode&gt;
  }
&lt;/urn:dicts&gt;
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{
&lt;urn:invokeResponse&gt;
  {getElementsForDicts($parm)}
&lt;/urn:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>