<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:dictionaries.be.dcl";
declare namespace urn2 = "urn:baseauxentities.be.dcl";


declare function xf:mapgetBranchRequest($req as element(urn:invoke))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				&lt;fml:B_OPCJA?&gt;{ data($req/urn:option/urn2:IntegerHolder/urn2:value) }&lt;/fml:B_OPCJA&gt;
			}
			{
				&lt;fml:B_ODDZIAL?&gt;{ data($req/urn:branchCode/urn1:BranchCode/urn1:branchCode) }&lt;/fml:B_ODDZIAL&gt;
			}
			{
				&lt;fml:B_MIKROODDZIAL?&gt;{ data($req/urn:branchCode/urn1:BranchCode/urn1:branchType/urn1:BranchType/urn1:branchType) }&lt;/fml:B_MIKROODDZIAL&gt;
			}
		&lt;/fml:FML32&gt;
};


declare variable $body as element(soap-env:Body) external;

&lt;soap-env:Body&gt;
 { xf:mapgetBranchRequest($body/urn:invoke) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>