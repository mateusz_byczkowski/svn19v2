<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns0="urn:dictionaries.be.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForDicts($parm as element(fml:FML32)) as element()
{

&lt;ns3:dicts&gt;
  {
    let $addressCity := $parm/NF_BRANCC_ADDRESSCITY
    let $addressStreet := $parm/NF_BRANCC_ADDRESSSTREET
    let $addressZipCode := $parm/NF_BRANCC_ADDRESSZIPCODE
    let $branchCode := $parm/NF_BRANCC_BRANCHCODE
    let $description := $parm/NF_BRANCC_DESCRIPTION
    let $fullAddress := $parm/NF_BRANCC_FULLADDRESS
    let $branchRoutingNumber := $parm/NF_BRANCC_BRANCHROUTINGNUM
    for $x at $occ in $parm/NF_BRANCC_BRANCHCODE
    return
    &lt;ns0:BranchCode&gt;
      &lt;ns0:addressCity?&gt;{data($addressCity[$occ])}&lt;/ns0:addressCity&gt;
      &lt;ns0:addressStreet?&gt;{data($addressStreet[$occ])}&lt;/ns0:addressStreet&gt;
      &lt;ns0:addressZipCode?&gt;{data($addressZipCode[$occ])}&lt;/ns0:addressZipCode&gt;
      &lt;ns0:branchCode?&gt;{data($branchCode[$occ])}&lt;/ns0:branchCode&gt;
      &lt;ns0:description?&gt;{data($description[$occ])}&lt;/ns0:description&gt;
      &lt;ns0:fullAddress?&gt;{data($fullAddress[$occ])}&lt;/ns0:fullAddress&gt;
      &lt;ns0:branchRoutingNumber?&gt;{data($branchRoutingNumber[$occ])}&lt;/ns0:branchRoutingNumber&gt;
    &lt;/ns0:BranchCode&gt;
  }
&lt;/ns3:dicts&gt;
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns3:invokeResponse&gt;
  {getElementsForDicts($parm)}
&lt;/ns3:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>