<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-15</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace cer = "http://bzwbk.com/services/cerber";
declare namespace java="java:com.bzwbk.cerber.services.auth";

declare namespace xf="http://bzwbk.com/services/prime/functions";

declare variable $body as element(soap-env:Body) external;
declare variable $fml as element(FML32):=$body/FML32;


declare function xf:parseElement( $parseName as xs:string, $fmlElement as element()* ) 
{
   if ($fmlElement) then
        element {$parseName} {data($fmlElement)}
   else ()
};


&lt;soap-env:Body&gt; 
    &lt;cer:GetUserDataWithHashRequest &gt; 
        {xf:parseElement('cer:UserId', $fml/U_USER_NAME)} 
        {xf:parseElement('cer:PasswordHash', $fml/U_PASSWORD)} 
        &lt;cer:AppAuthData&gt; 
            {xf:parseElement('ApplicationId', $fml/U_APPLICATION_ID)}
            {xf:parseElement('ApplicationPassword', $fml/U_APPLICATION_PWD)}
            {xf:parseElement('Operator', $fml/U_USER_NAME)}
        &lt;/cer:AppAuthData&gt; 
    &lt;/cer:GetUserDataWithHashRequest&gt; 
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>