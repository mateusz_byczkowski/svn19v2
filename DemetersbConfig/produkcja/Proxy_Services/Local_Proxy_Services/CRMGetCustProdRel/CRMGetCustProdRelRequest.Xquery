<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustProdRelRequest($req as element(m:CRMGetCustProdRelRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
                                       (:         {
				if($req/m:IdSpolki)
					then &lt;fml:NF_MSHEAD_COMPANYID&gt;{ data($req/m:IdSpolki) }&lt;/fml:NF_MSHEAD_COMPANYID&gt;
					else ()			
                                                } :)

			{
				if($req/m:CustomCustomernumber)
					then &lt;fml:NF_CUSTOM_CUSTOMERNUMBER&gt;{ data($req/m:CustomCustomernumber) }&lt;/fml:NF_CUSTOM_CUSTOMERNUMBER&gt;
					else ()
			}
			{
				if($req/m:PagecNavigationkeyvalu)
					then &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;{ data($req/m:PagecNavigationkeyvalu) }&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;
					else ()
			}
			{
				if($req/m:PagecReverseorder)
					then &lt;fml:NF_PAGEC_REVERSEORDER&gt;{ data($req/m:PagecReverseorder) }&lt;/fml:NF_PAGEC_REVERSEORDER&gt;
					else ()
			}
			{
				if($req/m:PagecActioncode)
					then &lt;fml:NF_PAGEC_ACTIONCODE&gt;{ data($req/m:PagecActioncode) }&lt;/fml:NF_PAGEC_ACTIONCODE&gt;
					else ()
			}
			{
				if($req/m:PagecPagesize)
					then &lt;fml:NF_PAGEC_PAGESIZE&gt;{ data($req/m:PagecPagesize) }&lt;/fml:NF_PAGEC_PAGESIZE&gt;
					else ()
			}

                                               &lt;NF_CTRL_ACTIVENONACTIVE&gt;1&lt;/NF_CTRL_ACTIVENONACTIVE&gt;
                                               &lt;NF_ACCOUN_ALTERNATIVEADDRE&gt;0&lt;/NF_ACCOUN_ALTERNATIVEADDRE&gt;
                                               &lt;NF_PRODUA_CODEPRODUCTAREA&gt;0&lt;/NF_PRODUA_CODEPRODUCTAREA&gt;
                                               &lt;NF_CURREC_CURRENCYCODE&gt;&lt;/NF_CURREC_CURRENCYCODE&gt;
                                               &lt;NF_ACCOUR_RELATIONSHIP&gt;&lt;/NF_ACCOUR_RELATIONSHIP&gt;
                                               &lt;NF_PRODUC_IDPRODUCTCATEGOR&gt;0&lt;/NF_PRODUC_IDPRODUCTCATEGOR&gt;
                                               &lt;NF_CTRL_OPTION&gt;0&lt;/NF_CTRL_OPTION&gt;
                                               &lt;NF_MSHEAD_COMPANYID&gt;1&lt;/NF_MSHEAD_COMPANYID&gt; 
                                               &lt;NF_CTRL_AREAS&gt;0203101314&lt;/NF_CTRL_AREAS&gt;
                                               &lt;NF_PRODUG_VISIBILITYCRM&gt;1&lt;/NF_PRODUG_VISIBILITYCRM&gt;
 		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCustProdRelRequest($body/m:CRMGetCustProdRelRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>