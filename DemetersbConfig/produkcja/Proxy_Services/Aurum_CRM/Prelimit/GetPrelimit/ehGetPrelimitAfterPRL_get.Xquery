<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $fault external;

(: &lt;soap-env:Body&gt; :)
&lt;soap-env:Body&gt;
{
  let $reason := fn:substring-after(fn:substring-before(data($fault/ctx:reason), "):"), "(")
  let $urcode := fn:substring-before(fn:substring-after(fn:substring-after(data($fault/ctx:reason),":"),":"),":")

  return
       if($reason = "11") then
           &lt;no_PRL_get_data&gt;{ if ($urcode = "103") then "1" else "0"}&lt;/no_PRL_get_data&gt;
       else
           &lt;no_PRL_get_data&gt;"0"&lt;/no_PRL_get_data&gt;,

      &lt;tux_err_reason&gt;{data($fault/ctx:reason)}&lt;/tux_err_reason&gt;
}
&lt;/soap-env:Body&gt;

(: &lt;/soap-env:Body&gt; :)</con:xquery>
</con:xqueryEntry>