<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function local:mapping($fml as element(fml:FML32), $globals as element(fml:FML32),
	$req as element(fml:FML32)) as element() {
		&lt;statistics&gt;
		    &lt;level_id&gt;{ data($req/fml:CI_ID_POZIOMU) }&lt;/level_id&gt;
		    &lt;agg_level&gt;{ data($req/fml:CI_POZIOM_AGREGACJI) }&lt;/agg_level&gt;
		    &lt;service_class&gt;{ data($req/fml:CI_KLASA_OBSLUGI) }&lt;/service_class&gt;
		    &lt;report_range&gt;{ data($req/fml:CI_ZAKRES_RAPORTU) }&lt;/report_range&gt;
		    &lt;cust_type_in&gt;{ data($req/fml:DC_TYP_KLIENTA) }&lt;/cust_type_in&gt;
		    &lt;update_date&gt;{ data($fml/fml:CI_DATA_AKTUALIZACJI[1]) }&lt;/update_date&gt;
		    &lt;report_update_date&gt;{ data($fml/fml:CI_DATA_ZMIANY_STAT) }&lt;/report_update_date&gt;
			&lt;report&gt;
				&lt;cust_type code="{ $req/fml:DC_TYP_KLIENTA }"&gt;
		            &lt;global_params&gt;
{
	for $klasaObslugi at $p in $globals/fml:CI_KLASA_OBSLUGI
		let $klasa := fn:normalize-space($klasaObslugi)
		let $sum := data($globals/CI_ILOSC1[$p])
			return
				if($klasa = '') then
						&lt;param code="SUM_COUNT" type="C"&gt;{ $sum }&lt;/param&gt;
				else if($klasa = '02') then
		                &lt;param code="PREMIUM_COUNT" type="C"&gt;{ $sum }&lt;/param&gt;
		        else if($klasa = '01') then
		                &lt;param code="CLASSIC_COUNT" type="C"&gt;{ $sum }&lt;/param&gt;
		        else
		        	()
}
		            &lt;/global_params&gt;
					&lt;prd_groups&gt;
{
	for $kodProduktu at $p in $fml/fml:CI_KOD_PRODUKTU
	let $kodGrupy1 := fn:normalize-space(data($fml/fml:CI_KOD_PRODUKTU1[$p]))
	let $kodGrupy2 := fn:normalize-space(data($fml/fml:CI_KOD_PRODUKTU2[$p]))
	let $kodGrupy3 := fn:normalize-space(data($fml/fml:CI_KOD_PRODUKTU3[$p])) return
						&lt;prd_group code="{ $kodProduktu }"&gt;
{
	if($kodGrupy1 = '2') then
							(&lt;param code="ACC_DEP_COUNT" type="C"&gt;{ data($fml/CI_ILOSC1[$p]) }&lt;/param&gt;,
							(:
							21.09.2007 biznes nie chce tego pola
							&lt;param code="ACC_LIM_COUNT" type="C"&gt;{ data($fml/CI_ILOSC2[$p]) }&lt;/param&gt;,
							:)
							&lt;param code="INT_IN_OPER_COUNT" type="C"&gt;{ data($fml/CI_ILOSC6[$p]) }&lt;/param&gt;,
							&lt;param code="INT_OUT_OPER_COUNT" type="C"&gt;{ data($fml/CI_ILOSC5[$p]) }&lt;/param&gt;,
							&lt;param code="CONST_ORD_COUNT" type="C"&gt;{ data($fml/CI_ILOSC3[$p]) }&lt;/param&gt;,
							&lt;param code="DIR_DEBIT_COUNT" type="C"&gt;{ data($fml/CI_ILOSC4[$p]) }&lt;/param&gt;,
							&lt;param code="BALANCE_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="LIMIT_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC2[$p]) }&lt;/param&gt;,
							&lt;param code="INT_IN_OPER_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC4[$p]) }&lt;/param&gt;,
							&lt;param code="INT_OUT_OPER_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC3[$p]) }&lt;/param&gt;)
	else if($kodGrupy1 = '10') then
							(&lt;param code="ACC_COUNT" type="C"&gt;{ data($fml/CI_ILOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="BALANCE_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="AVG_BALANCE_1M" type="V"&gt;{ data($fml/CI_WARTOSC2[$p]) }&lt;/param&gt;,
							&lt;param code="AVG_BALANCE_3M" type="V"&gt;{ data($fml/CI_WARTOSC3[$p]) }&lt;/param&gt;)
	else if($kodGrupy1 = '3') then
							(&lt;param code="ACC_COUNT" type="C"&gt;{ data($fml/CI_ILOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="INIT_COMMIT_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC4[$p]) }&lt;/param&gt;,
							&lt;param code="BALANCE_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="AVG_CONS_1M" type="V"&gt;{ data($fml/CI_WARTOSC2[$p]) }&lt;/param&gt;)
	else if($kodGrupy1 = '8') then
							&lt;param code="CUSTOMER_COUNT" type="C"&gt;{ data($fml/CI_ILOSC1[$p]) }&lt;/param&gt;
	else if($kodGrupy1 = '1' and $kodGrupy3 != '1055') then
							(&lt;param code="CARD_COUNT" type="C"&gt;{ data($fml/CI_ILOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="CARD_LIMIT_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC4[$p]) }&lt;/param&gt;)
	else if($kodGrupy3 = '1055') then
							(&lt;param code="RATIO_CREDIT_COUNT" type="C"&gt;{ data($fml/CI_ILOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="ACC_CREDIT_COUNT" type="V"&gt;{ data($fml/CI_WARTOSC4[$p]) }&lt;/param&gt;)	 
(:	else if($kodGrupy1 = '?????') then
							(&lt;param code="ACC_COUNT" type="C"&gt;{ data($fml/CI_ILOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="BALANCE_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="AVG_BALANCE_1M" type="V"&gt;{ data($fml/CI_WARTOSC2[$p]) }&lt;/param&gt;,
							&lt;param code="AVG_BALANCE_3M" type="V"&gt;{ data($fml/CI_WARTOSC3[$p]) }&lt;/param&gt;)
:)
	else if($kodGrupy1 = '4') then
							(&lt;param code="ACC_COUNT" type="C"&gt;{ data($fml/CI_ILOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="BALANCE_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC1[$p]) }&lt;/param&gt;)
	else if($kodGrupy1 = '12') then
							(&lt;param code="ACC_COUNT" type="C"&gt;{ data($fml/CI_ILOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="BALANCE_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC1[$p]) }&lt;/param&gt;)
	else if($kodGrupy1 = '15' or $kodGrupy1 = '16' or $kodGrupy1 = '17') then
							(&lt;param code="ACC_COUNT" type="C"&gt;{ data($fml/CI_ILOSC1[$p]) }&lt;/param&gt;,
							&lt;param code="BALANCE_TOTAL" type="V"&gt;{ data($fml/CI_WARTOSC1[$p]) }&lt;/param&gt;)
	else
		(&lt;!-- nieznana grupa --&gt;)
}
						&lt;/prd_group&gt;
}
					&lt;/prd_groups&gt;
				&lt;/cust_type&gt;
			&lt;/report&gt;
		&lt;/statistics&gt;
};

declare variable $fml as element(fml:FML32) external;
declare variable $req as element(fml:FML32) external;
declare variable $globals as element(fml:FML32) external;

&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"&gt;
	&lt;m:CRMGetPortStatsResponse&gt;{ fn-bea:serialize(local:mapping($fml, $globals, $req)) }&lt;/m:CRMGetPortStatsResponse&gt;
&lt;/soapenv:Body&gt;</con:xquery>
</con:xqueryEntry>