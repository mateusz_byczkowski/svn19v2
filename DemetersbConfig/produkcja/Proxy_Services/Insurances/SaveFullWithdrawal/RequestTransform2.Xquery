<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns9 = "urn:dictionaries.be.dcl";
declare namespace ns4 = "urn:applicationul.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Us�ugi/StreamLine/SaveFullWithdrawal/RequestTransform/";
declare namespace ns0 = "urn:baseentities.be.dcl";
declare namespace ns-1 = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace ns1 = "http://jv.channel.cu.com.pl/cmf-wsdl-types";
declare namespace ns7 = "urn:baseauxentities.be.dcl";
declare namespace ns3 = "urn:basedictionaries.be.dcl";
declare namespace ns2 = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace ns5 = "urn:uldictionary.dictionaries.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns8 = "urn:entities.be.dcl";

declare function xf:RequestTransform($invoke1 as element(ns6:invoke),
    $header1 as element(ns6:header))
    as element(ns-1:performContractSurrenderRequest) {
        &lt;ns-1:performContractSurrenderRequest&gt;
            &lt;envelope&gt;
                &lt;request-time&gt;{ data($header1/ns6:msgHeader/ns6:timestamp) }&lt;/request-time&gt;
                &lt;request-no&gt;{ data($header1/ns6:msgHeader/ns6:msgId) }&lt;/request-no&gt;
                &lt;source-code&gt;{ data($invoke1/ns6:sourceCode/ns7:StringHolder/ns7:value) }&lt;/source-code&gt;
                &lt;user-id&gt;{ data($header1/ns6:msgHeader/ns6:userId) }&lt;/user-id&gt;
                &lt;branch-id&gt;{ data($header1/ns6:msgHeader/ns6:unitId) }&lt;/branch-id&gt;
                &lt;bar-code&gt;{ data($invoke1/ns6:barCodeID/ns7:StringHolder/ns7:value) }&lt;/bar-code&gt;
            &lt;/envelope&gt;
            &lt;save&gt;{ data($invoke1/ns6:mode/ns7:BooleanHolder/ns7:value) }&lt;/save&gt;
            &lt;product-type&gt;{ data($invoke1/ns6:policyContract/ns4:PolicyContract/ns4:productCode/ns5:UlParameters/ns5:productId) }&lt;/product-type&gt;
            &lt;contract-number&gt;{ data($invoke1/ns6:policyContract/ns4:PolicyContract/ns4:policyID) }&lt;/contract-number&gt;
            {
                for $value in $invoke1/ns6:operationDate/ns7:DateHolder/ns7:value
                return
                    &lt;effective&gt;{ xs:date( fn:substring-before( fn:concat(data($value), 'T'), 'T' ) ) }&lt;/effective&gt;
            }
            {
                for $opResignCause in $invoke1/ns6:policyContract/ns4:PolicyContract/ns4:resignCause/ns5:OpResignCause/ns5:strlKey
                return
                    &lt;status-reason&gt;{ xs:int( data($opResignCause) ) }&lt;/status-reason&gt;
            }
            {
                let $PolicyContract := $invoke1/ns6:policyContract/ns4:PolicyContract/ns4:policyOrder/ns4:PolicyOrder
                return
                    &lt;bank? kind?="{ data($PolicyContract/ns4:accountType/ns5:UlAccountType/ns5:ulAccountType) }"&gt;
                        &lt;account?&gt;{ xs:string(data($PolicyContract/ns4:accountNoWithdrawal)) }&lt;/account&gt;
                        &lt;recipient-name?&gt;
                            &lt;first-name?&gt;{ data($PolicyContract/ns4:withdrawalFirstName) }&lt;/first-name&gt;
                            &lt;last-name?&gt;{ data($PolicyContract/ns4:withdrawalName) }&lt;/last-name&gt;
                        &lt;/recipient-name&gt;
                        &lt;recipient-address? kind?="{ data($invoke1/ns6:addressType/ns7:StringHolder/ns7:value) }"&gt;
                            &lt;street?&gt;{ data($PolicyContract/ns4:withdrawalStreet) }&lt;/street&gt;
                            &lt;home?&gt;{ data($PolicyContract/ns4:withdrawalHouse) }&lt;/home&gt;
                            &lt;flat?&gt;{ data($PolicyContract/ns4:withdrawalFlat) }&lt;/flat&gt;
                            &lt;postal-code?&gt;{ data($PolicyContract/ns4:withdrawalZipCode) }&lt;/postal-code&gt;
                            &lt;city?&gt;{ data($PolicyContract/ns4:withdrawalCity) }&lt;/city&gt;
                            &lt;country?&gt;{ data($PolicyContract/ns4:withdrawalCountry/ns9:CountryCode/ns9:countryCode) }&lt;/country&gt;
                        &lt;/recipient-address&gt;
                    &lt;/bank&gt;
            }
        &lt;/ns-1:performContractSurrenderRequest&gt;
};

declare variable $invoke1 as element(ns6:invoke) external;
declare variable $header1 as element(ns6:header) external;

xf:RequestTransform($invoke1,
    $header1)</con:xquery>
</con:xqueryEntry>