<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-01-17</con:description>
  <con:schema><![CDATA[<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema targetNamespace="http://bzwbk.com/services/userDataNFE" elementFormDefault="unqualified" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:tns="http://bzwbk.com/services/userDataNFE">
  <!--Struktury cerberopodobne-->
  <xsd:complexType name="User">
    <xsd:sequence>
      <xsd:element name="Email" type="xsd:string"/>
      <xsd:element name="Func" type="xsd:string"/>
      <xsd:element name="Gender" type="xsd:string"/>
      <xsd:element name="LastIncorrectLoginApp" type="xsd:int" minOccurs="0"/>
      <xsd:element name="LastIncorrectLoginDate" type="xsd:dateTime" minOccurs="0"/>
      <xsd:element name="LastLoggedInApplication" type="xsd:int" minOccurs="0"/>
      <xsd:element name="LastLoggedInDate" type="xsd:dateTime" minOccurs="0"/>
      <xsd:element name="Login" type="xsd:string"/>
      <xsd:element name="MobileNo" type="xsd:string"/>
      <xsd:element name="Name" type="xsd:string"/>
      <xsd:element name="Password" type="xsd:string" minOccurs="0"/>
      <xsd:element name="PhoneNo" type="xsd:string"/>
      <xsd:element name="Position" type="xsd:string" minOccurs="0"/>
      <xsd:element name="PositionId" type="xsd:int" minOccurs="0"/>
      <xsd:element name="Skp" type="xsd:int"/>
      <xsd:element name="Surname" type="xsd:string"/>
      <xsd:element name="Attributes" type="tns:Attribute" minOccurs="0" maxOccurs="unbounded"/>
      <xsd:element name="PositionAttributes" type="tns:Attribute" minOccurs="0" maxOccurs="unbounded"/>
      <xsd:element name="Permissions" type="xsd:string" minOccurs="0" maxOccurs="unbounded"/>
      <xsd:element name="PositionPermissions" type="xsd:string" minOccurs="0" maxOccurs="unbounded"/>
      <xsd:element name="Units" type="tns:Unit" minOccurs="0" maxOccurs="unbounded"/>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="Attribute">
    <xsd:sequence>
      <xsd:element name="Name" type="xsd:string"/>
      <xsd:element name="Value" type="xsd:string"/>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="Substitution">
    <xsd:sequence>
      <xsd:element name="Id" type="xsd:int"/>
      <xsd:element name="Since" type="xsd:dateTime"/>
      <xsd:element name="SubstitutedUserId" type="xsd:int"/>
      <xsd:element name="SubstitutionForApplication" type="xsd:int"/>
      <xsd:element name="UnitId" type="xsd:int"/>
      <xsd:element name="Until" type="xsd:dateTime"/>
      <xsd:element name="UserId" type="xsd:int"/>
      <xsd:element name="CreatedBy" type="xsd:int"/>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="Unit">
    <xsd:sequence>
      <xsd:element name="AddressCity" type="xsd:string"/>
      <xsd:element name="AddressNo" type="xsd:string"/>
      <xsd:element name="AddressState" type="xsd:string"/>
      <xsd:element name="AddressStreet" type="xsd:string"/>
      <xsd:element name="AddressZip" type="xsd:string"/>
      <xsd:element name="IcbsNo" type="xsd:int"/>
      <xsd:element name="KirNo" type="xsd:int"/>
      <xsd:element name="Name" type="xsd:string"/>
      <xsd:element name="FunctionId" type="xsd:int"/>
      <xsd:element name="Function" type="xsd:string"/>
      <xsd:element name="ChiefId" minOccurs="0" type="xsd:int"/>
      <xsd:element name="CompanyId" type="xsd:int"/>
      <xsd:element name="ParentIcbsNo" minOccurs="0" type="xsd:int"/>
      <xsd:element name="CashierNo" type="xsd:int" minOccurs="0"/>
      <xsd:element name="TeamId" type="xsd:int" minOccurs="0"/>
      <xsd:element name="IsDelegation" type="xsd:boolean"/>
      <xsd:element name="Since" type="xsd:dateTime" minOccurs="0"/>
      <xsd:element name="Until" type="xsd:dateTime" minOccurs="0"/>
      <xsd:element name="IsMasterUnit" type="xsd:string"/>
      <xsd:element name="WorkWithOwnWallet" type="xsd:boolean"/>
      <xsd:element name="Attributes" type="tns:Attribute" minOccurs="0" maxOccurs="unbounded"/>
      <xsd:element name="Permissions" type="xsd:string" minOccurs="0" maxOccurs="unbounded"/>
      <xsd:element name="Substitutions" type="tns:Substitution" minOccurs="0" maxOccurs="unbounded"/>
    </xsd:sequence>
  </xsd:complexType>
  <!--Struktury oparte o getBranchCurrentStatus, tylko nie takie oblesne ;)-->
  <xsd:complexType name="BranchUser">
    <xsd:sequence>
      <xsd:element name="id" type="xsd:string"/>
      <xsd:element name="firstName" type="xsd:string" minOccurs="0" />
      <xsd:element name="lastName" type="xsd:string" minOccurs="0" />
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="BranchCurrentStatus">
    <xsd:sequence>
      <xsd:element name="workDate" type="xsd:date" minOccurs="0" />
      <xsd:element name="sessionNumber" type="xsd:int"/>
      <xsd:element name="lastChangeDate" type="xsd:dateTime" minOccurs="0" />
      <xsd:element name="status" type="xsd:string"/>
      <xsd:element name="lastChangeUser" type="tns:BranchUser" minOccurs="0" />
      <xsd:element name="lastChangeAcceptor" type="tns:BranchUser" minOccurs="0"/>
      <xsd:element name="converted" type="xsd:boolean"/>
    </xsd:sequence>
  </xsd:complexType>
  <!--Struktury z getUserTills-->
  <xsd:complexType name="UserTxnSession">
    <xsd:sequence>
      <xsd:element name="sessionNumber" type="xsd:int"/>
      <xsd:element name="sessionDate" type="xsd:date"/>
      <xsd:element name="lastChangeDate" type="xsd:dateTime" minOccurs="0"/>
      <xsd:element name="sessionStatus" type="xsd:string"/>
      <xsd:element name="user" type="tns:BranchUser"/>
      <xsd:element name="till" type="tns:Till"/>
      <xsd:element name="tellerID" type="xsd:int"/>
      <xsd:element name="branchCode" type="xsd:string"/>
    </xsd:sequence>
  </xsd:complexType>
  <xsd:complexType name="Till">
    <xsd:sequence>
      <xsd:element name="tillID" type="xsd:int"/>
      <xsd:element name="tillName" type="xsd:string" minOccurs="0" />
      <xsd:element name="tillType" type="xsd:string"/>
    </xsd:sequence>
  </xsd:complexType>
  <!--Request i response-->
  <xsd:element name="GetUserDataNFERequest">
    <xsd:complexType>
      <xsd:sequence>
        <xsd:element name="userId" type="xsd:int"/>
        <xsd:element name="password" type="xsd:string"/>
        <xsd:element name="applicationId" type="xsd:int"/>
        <xsd:element name="applicationPassword" type="xsd:string"/>
        <xsd:element name="operator" type="xsd:int"/>
        <xsd:element name="secondaryApplicationId" type="xsd:int"/>
        <xsd:element name="msgId" type="xsd:string"/>
      </xsd:sequence>
    </xsd:complexType>
  </xsd:element>
  <xsd:element name="GetUserDataWithoutPassNFERequest">
    <xsd:complexType>
      <xsd:sequence>
        <xsd:element name="userId" type="xsd:int"/>
        <xsd:element name="applicationId" type="xsd:int"/>
        <xsd:element name="applicationPassword" type="xsd:string"/>
        <xsd:element name="operator" type="xsd:int"/>
        <xsd:element name="secondaryApplicationId" type="xsd:int"/>
        <xsd:element name="msgId" type="xsd:string"/>
      </xsd:sequence>
    </xsd:complexType>
  </xsd:element>
  <xsd:element name="GetUserDataNFEResponse">
    <xsd:complexType>
      <xsd:sequence>
        <xsd:element name="dataNFE" type="xsd:dateTime"/>
        <xsd:element name="userData" type="tns:User"/>
        <xsd:element name="branchStatus" type="tns:BranchCurrentStatus" minOccurs="0"/>
        <xsd:element name="userTxnSession" type="tns:UserTxnSession" minOccurs="0" maxOccurs="unbounded"/>
      </xsd:sequence>
    </xsd:complexType>
  </xsd:element>
</xsd:schema>]]></con:schema>
  <con:targetNamespace>http://bzwbk.com/services/userDataNFE</con:targetNamespace>
</con:schemaEntry>