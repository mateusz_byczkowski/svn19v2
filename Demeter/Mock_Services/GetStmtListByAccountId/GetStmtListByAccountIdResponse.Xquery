<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2011-07-14</con:description>
  <con:xquery><![CDATA[declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace prim = "http://bzwbk.com/services/prime/";
declare namespace stm = "http://schemas.datacontract.org/2004/07/StmtListServicesLibrary.Structures";

  <soapenv:Body>
      <prim:GetStmtListByAccountIdResponse>
         <prim:GetStmtListByAccountIdResult>
            <stm:Statement>
               <stm:B_KOD_RACH>188001</stm:B_KOD_RACH>
               <stm:B_D_ZESTAW>11-05-2011</stm:B_D_ZESTAW>
               <stm:B_LICZBA_REK>6</stm:B_LICZBA_REK>
               <stm:B_STMT_SERNO>10</stm:B_STMT_SERNO>
            </stm:Statement>
            <stm:Statement>
               <stm:B_KOD_RACH>188001</stm:B_KOD_RACH>
               <stm:B_D_ZESTAW>11-04-2011</stm:B_D_ZESTAW>
               <stm:B_LICZBA_REK>6</stm:B_LICZBA_REK>
               <stm:B_STMT_SERNO>9</stm:B_STMT_SERNO>
            </stm:Statement>
            <stm:Statement>
               <stm:B_KOD_RACH>188001</stm:B_KOD_RACH>
               <stm:B_D_ZESTAW>11-03-2011</stm:B_D_ZESTAW>
               <stm:B_LICZBA_REK>6</stm:B_LICZBA_REK>
               <stm:B_STMT_SERNO>8</stm:B_STMT_SERNO>
            </stm:Statement>
            <stm:Statement>
               <stm:B_KOD_RACH>188001</stm:B_KOD_RACH>
               <stm:B_D_ZESTAW>11-02-2011</stm:B_D_ZESTAW>
               <stm:B_LICZBA_REK>6</stm:B_LICZBA_REK>
               <stm:B_STMT_SERNO>7</stm:B_STMT_SERNO>
            </stm:Statement>
         </prim:GetStmtListByAccountIdResult>
      </prim:GetStmtListByAccountIdResponse>
   </soapenv:Body>]]></con:xquery>
</con:xqueryEntry>