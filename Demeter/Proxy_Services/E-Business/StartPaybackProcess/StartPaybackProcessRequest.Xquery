<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery><![CDATA[declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace op = "http://service.ws.flow.pbpolsoft.com.pl";
declare namespace tns1 = "http://bzwbk.com/services/BZWBK24flow/";
declare namespace tns3 = "http://bzwbk.com/services/BZWBK24flow/messages/";
declare namespace tns2 = "urn:pbpolsoft.com.pl";
declare namespace xsd="http://www.w3.org/2001/XMLSchema";
declare namespace soapenc = "http://schemas.xmlsoap.org/soap/encoding/";


declare function xf:transformBody ($req as element(soapenv:Body)) as element(soapenv:Body) {
	
	<soapenv:Body>
	{		
			if($req/tns1:startNP2049)
			then 	<op:startNP2049><application href="#id0" /></op:startNP2049>
			else 	<op:startNP2049><application href="#id0" /></op:startNP2049>
			
	}
        <multiRef id="id0" xsi:type="tns2:PaybackApplicationBean" xmlns:tns2="urn:pbpolsoft.com.pl">
        {
                if ($req/*/tns3:crwId)
			then <crwId xsi:type="soapenc:string">{data($req/*/tns3:crwId)}</crwId>
			else()
        }
        </multiRef>
	
	
	</soapenv:Body>
};

	declare variable $body as element(soapenv:Body) external;
	xf:transformBody($body)]]></con:xquery>
</con:xqueryEntry>