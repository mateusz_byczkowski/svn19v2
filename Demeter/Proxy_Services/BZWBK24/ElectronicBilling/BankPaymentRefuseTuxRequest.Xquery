<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-07</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace ebp = "http://www.softax.com.pl/ebppml";



declare function xf:map_getBankPaymentRefuseTuxRequest($fml as element(fml:FML32))
	as element(ebp:payment-refuse-request) {
		<ebp:payment-refuse-request>
			 <ebp:head>
			  </ebp:head>
			  <ebp:request>
				<ebp:payment-refuse>
					(: <ebp:payer-id>{ data($fml/fml:EBPP_PAYER_ID) }</ebp:payer-id> :)
					<ebp:message-id>{ data($fml/fml:EBPP_PAYMENT_MSG_ID) }</ebp:message-id>
					<ebp:reason?>{ data($fml/fml:EBPP_PAYMENT_REFUSE_REASON) }</ebp:reason>
				</ebp:payment-refuse>
		      </ebp:request>       		
		</ebp:payment-refuse-request>
};

declare variable $body as element(soapenv:Body) external;
<soapenv:Body>
{ xf:map_getBankPaymentRefuseTuxRequest($body/fml:FML32) }
</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>