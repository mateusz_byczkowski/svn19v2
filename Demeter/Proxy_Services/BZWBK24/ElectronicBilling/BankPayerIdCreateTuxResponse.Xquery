<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-07</con:description>
  <con:xquery><![CDATA[declare namespace xf = "http://bzwbk.com/services/icbs/mappings/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsd="http://www.w3.org/2001/XMLSchema";
declare namespace f="http://bzwbk.com/services/BZWBK24flow/faults/";
declare default element namespace "http://www.softax.com.pl/ebppml";

declare function xf:map_getBankPayerIdCreateTuxResponse ($rpl as element(soapenv:Body)) as element(fml:FML32) {
	
	<fml:FML32>
	{
		if (data($rpl/payer-id-create-reply/reply/status/result) = "OK") then (
				<fml:EBPP_PAYER_ID>{ data($rpl/payer-id-create-reply/reply/payer-id-create/payer-id) }</fml:EBPP_PAYER_ID>
			
		) else (
		<fml:ERROR_DESCRIPTION>{ data($rpl/payer-id-create-reply/reply/status/err-message) }</fml:ERROR_DESCRIPTION>
		)		
	}
	</fml:FML32>
	
	
};

declare variable $reply as element(soapenv:Body) external;
<soapenv:Body>
{ xf:map_getBankPayerIdCreateTuxResponse($reply) }
</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>