<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$3.2011-08-16</con:description>
  <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns3="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string,$dateFormat as xs:string,$fieldName as xs:string) as element(){
      if ($value)
        then if(string-length($value)>5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)>5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };

declare function chgDate($value as xs:string,$dateFormat as xs:string,$fieldName as xs:string) as element(){

    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)

    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 

};

declare function getElementsForProductRdsCodeList($idDefinition as xs:string, $paIdProductDef2 as element()*, $paRDSCode as element()*, $paCodeProductSys as element()*) as element()
{

<ns3:productRdsCodeList>
  {
    for $x at $occ in $paRDSCode
    return if ($idDefinition = data($paIdProductDef2[$occ]))
    then
    <ns3:ProductRdsCode>
      <ns3:rdsCode?>{data($x)}</ns3:rdsCode>
      <ns3:codeProductSystem?>{data($paCodeProductSys[$occ])}</ns3:codeProductSystem>
    </ns3:ProductRdsCode>
    else
    	()
  }
</ns3:productRdsCodeList>
};
declare function getElementsForProductDefinitionList($parm as element(fml:FML32)) as element()
{

<ns0:productDefinitionList>
  {
    let $paIdProductDef := $parm/NF_PRODUD_IDPRODUCTDEFINIT
    let $paRDSCode := $parm/NF_PRODRC_RDSCODE
    let $paCodeProductSys := $parm/NF_PRODRC_CODEPRODUCTSYSTE
    let $paIdProductDef2 := $parm/NF_PRODRC_IDPRODUCTDEFINIT

    let $cdPrDef := $parm/NF_PRODUD_CODEPRODUCTDEFIN
    let $polishPrName := $parm/NF_PRODUD_POLISHPRODUCTNAM
    let $engPrName := $parm/NF_PRODUD_ENGLISHPRODUCTNA
    let $sortOrd := $parm/NF_PRODUD_SORTORDER
    let $idProductGroup := $parm/NF_PRODUD_IDPRODUCTGROUP
    let $packageOnlyFlag := $parm/NF_PRODUD_PACKAGEONLYFLAG
    let $sorceProductCode := $parm/NF_PRODUD_SORCEPRODUCTCODE
    let $startDate := $parm/NF_PRODUD_STARTDATE
    let $endDate := $parm/NF_PRODUD_ENDDATE

    for $x at $occ in $paIdProductDef
    return
    <ns3:ProductDefinition>
      <ns3:codeProductDefinition?>{data($cdPrDef[$occ])}</ns3:codeProductDefinition>
      <ns3:polishProductName?>{data($polishPrName[$occ])}</ns3:polishProductName>
      <ns3:englishProductName?>{data($engPrName[$occ])}</ns3:englishProductName>
      <ns3:sortOrder?>{data($sortOrd[$occ])}</ns3:sortOrder>
      <ns3:idProductGroup?>{data($idProductGroup[$occ])}</ns3:idProductGroup>
      <ns3:packageOnlyFlag?>{sourceValue2Boolean(data($packageOnlyFlag[$occ]),"1")}</ns3:packageOnlyFlag>
      <ns3:sorceProductCode?>{data($sorceProductCode[$occ])}</ns3:sorceProductCode>
      { insertDate(data($startDate[$occ]),"yyyy-MM-dd","ns3:startDate")}
       { insertDate(data($endDate[$occ]),"yyyy-MM-dd","ns3:endDate")}
      <ns3:idProductDefinition?>{data($x)}</ns3:idProductDefinition>
          {
            let $idDefinition := data($paIdProductDef2[$occ])
            return 
               <ns3:productRdsCodeList>
                  {
                    for $x at $occ in $paRDSCode
                    return if ($idDefinition = data($x))
                     then
                    <ns3:ProductRdsCode>
                        <ns3:rdsCode?>{data($x)}</ns3:rdsCode>
                        <ns3:codeProductSystem?>{data($paCodeProductSys[$occ])}</ns3:codeProductSystem>
                    </ns3:ProductRdsCode>
                     else()
                  }
              </ns3:productRdsCodeList>
             }
    </ns3:ProductDefinition>
  }
</ns0:productDefinitionList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForProductDefinitionList($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>