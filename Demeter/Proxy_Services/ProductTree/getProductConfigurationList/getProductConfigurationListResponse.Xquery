<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-07-18</con:description>
  <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns3="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForProductConfigurationList($parm as element(fml:FML32)) as element()
{

<ns0:productConfigurationList>
  {
    for $x at $occ in $parm/NF_PRODCO_IDPRODUCTCONFIGU
    return
    <ns3:ProductConfiguration>
      <ns3:idProductConfiguration?>{data($parm/NF_PRODCO_IDPRODUCTCONFIGU[$occ])}</ns3:idProductConfiguration>
      <ns3:polishConfigurationName?>{data($parm/NF_PRODCO_POLISHCONFIGURAT[$occ])}</ns3:polishConfigurationName>
      <ns3:idProductChannel?>{data($parm/NF_PRODCO_IDPRODUCTCHANNEL[$occ])}</ns3:idProductChannel>
    </ns3:ProductConfiguration>
  }
</ns0:productConfigurationList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForProductConfigurationList($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>