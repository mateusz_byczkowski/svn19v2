<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-24</con:description>
  <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:filtersandmessages.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns5="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{
<NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}</NF_TRHEAD_TRANSID>
};

declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
<NF_PRODUA_CODEPRODUCTAREA?>{data($parm/ns0:productAreaEntity/ns5:ProductArea/ns5:codeProductArea)}</NF_PRODUA_CODEPRODUCTAREA>
,
<NF_PRODUA_POLISHAREANAME?>{data($parm/ns0:productAreaEntity/ns5:ProductArea/ns5:polishAreaName)}</NF_PRODUA_POLISHAREANAME>
,
<NF_PRODUA_ENGLISHAREANAME?>{data($parm/ns0:productAreaEntity/ns5:ProductArea/ns5:englishAreaName)}</NF_PRODUA_ENGLISHAREANAME>
,
<NF_PRODUA_CERBERATRIBUTENA?>{data($parm/ns0:productAreaEntity/ns5:ProductArea/ns5:cerberAtributeName)}</NF_PRODUA_CERBERATRIBUTENA>
,
<NF_PRODUA_IDPRODUCTAREA?>{data($parm/ns0:productAreaEntity/ns5:ProductArea/ns5:idProductArea)}</NF_PRODUA_IDPRODUCTAREA>
,
for $it in $parm/ns0:productAreaEntity/ns5:ProductArea/ns5:productAreaAttributesList/ns5:ProductAreaAttributes/ns5:attributeID
return
<NF_PRODAA_ATTRIBUTEID?>{data($it)}</NF_PRODAA_ATTRIBUTEID>
,
for $it in $parm/ns0:productAreaEntity/ns5:ProductArea/ns5:productAreaAttributesList/ns5:ProductAreaAttributes/ns5:attributeName
return
<NF_PRODAA_ATTRIBUTENAME?>{data($it)}</NF_PRODAA_ATTRIBUTENAME>
,
for $it in $parm/ns0:productAreaEntity/ns5:ProductArea/ns5:productAreaAttributesList/ns5:ProductAreaAttributes/ns5:descriptionProductAttributes
return
<NF_PRODAA_DESCRIPTIONPRODU?>{data($it)}</NF_PRODAA_DESCRIPTIONPRODU>
,
for $it in $parm/ns0:productAreaEntity/ns5:ProductArea/ns5:productAreaAttributesList/ns5:ProductAreaAttributes/ns5:businessNameProductAttributes
return
<NF_PRODAA_BUSINESSNAMEPROD?>{data($it)}</NF_PRODAA_BUSINESSNAMEPROD>
,
for $it in $parm/ns0:productAreaEntity/ns5:ProductArea/ns5:productAreaAttributesList/ns5:ProductAreaAttributes/ns5:attributeType/ns3:ProductAreaAttributeType/ns3:attributeType
return
<NF_PROAAT_ATTRIBUTETYPE?>{data($it)}</NF_PROAAT_ATTRIBUTETYPE>

};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>