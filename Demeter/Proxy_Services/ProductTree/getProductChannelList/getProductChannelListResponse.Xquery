<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-07-18</con:description>
  <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns3="urn:productstree.entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForProductChannelList($parm as element(fml:FML32)) as element()
{

<ns0:productChannelList>
  {
    for $x at $occ in $parm/NF_PRODCHA_IDPRODUCTCHANNE
    return
    <ns3:ProductChannel>
      <ns3:codeProductChannel?>{data($parm/NF_PRODCHA_CODEPRODUCTCHAN[$occ])}</ns3:codeProductChannel>
      <ns3:polishChannelName?>{data($parm/NF_PRODCHA_POLISHCHANNELNA[$occ])}</ns3:polishChannelName>
      <ns3:englishChannelName?>{data($parm/NF_PRODCHA_ENGLISHCHANNELN[$occ])}</ns3:englishChannelName>
      <ns3:idProductChannel?>{data($parm/NF_PRODCHA_IDPRODUCTCHANNE[$occ])}</ns3:idProductChannel>
    </ns3:ProductChannel>
  }
</ns0:productChannelList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForProductChannelList($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>