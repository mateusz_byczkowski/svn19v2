<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @author  Tomasz Krajewski
 : @version 1.2
 : @since   2010-08-13
 :
 : wersja WSDLa: 12-11-2009 11:07:43
 :
 : $Proxy Services/Branches/getDenominationInBranch/getDenominationInBranchResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getDenominationInBranch/getDenominationInBranchResponse/";
declare namespace ns0 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns2 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";
declare namespace ns6 = "";

declare variable $fML321 as element(ns6:FML32) external;

(:~
 : @param $fML321 - bufor FML/XML
 :
 : @return invokeResponse - operacja wyjściowa
 :)
declare function xf:getDenominationInBranchResponse($fML321 as element(ns6:FML32))
    as element(ns5:invokeResponse)
{
	<ns5:invokeResponse>
        <ns5:totalDenomSpec>
			<ns1:DenominationSpecification?>
				<ns1:itemsNumber?>{
				(: v1.2 Zmiana pola NF_DENOMS_ITEMSNUMBER na NF_COLLAT_NUMBEROFUNITS :)
					data($fML321/ns6:NF_COLLAT_NUMBEROFUNITS[1])
				}</ns1:itemsNumber>
				
				<ns1:denomination?>
					<ns0:DenominationDefinition?>
						<ns0:denominationID?>{
							data($fML321/ns6:NF_DENOMD_DENOMINATIONID[1])
						}</ns0:denominationID>
					</ns0:DenominationDefinition>
				</ns1:denomination>
			</ns1:DenominationSpecification>
		</ns5:totalDenomSpec>
		
		<ns5:denominationSpecificationList>{
			(: v1.2 Zmiana pola NF_DENOMS_ITEMSNUMBER na NF_COLLAT_NUMBEROFUNITS :)
			for $i in 2 to count($fML321/ns6:NF_COLLAT_NUMBEROFUNITS)
		    return
				<ns1:DenominationSpecification>
                    <ns1:itemsNumber>{
						data($fML321/ns6:NF_COLLAT_NUMBEROFUNITS[$i])
					}</ns1:itemsNumber>
					
                    <ns1:currencyCash>
                        <ns1:CurrencyCash>

                            <ns1:userTxnSession>
                                <ns3:UserTxnSession>
									(:
									 : status oddziału
									 :
									 : 0 --> C (zamknięty)
									 : 1 --> O (otwarty)
									 :)
		                        	{
		                        		let $sessionStatus := data($fML321/ns6:NF_USETSS_USERTXNSESSIONST[$i])
		                        		return
		                        			if ($sessionStatus) then
		                        				<ns3:sessionStatus>
													<ns2:UserTxnSessionStatus>
														<ns2:userTxnSessionStatus>{
															if ($sessionStatus eq '1') then
																'O'
															else
																'C'
														}</ns2:userTxnSessionStatus>
													</ns2:UserTxnSessionStatus>
												</ns3:sessionStatus>
		                        			else
												()
		                        	}
                                    
                                    <ns3:user>
                                        <ns4:User>
                                            <ns4:userLastName>{
												data($fML321/ns6:NF_USER_USERLASTNAME[$i])
											}</ns4:userLastName>
											
                                            <ns4:userID>{
												data($fML321/ns6:NF_USER_USERID[$i])
											}</ns4:userID>
											
                                            <ns4:userFirstName>{
												data($fML321/ns6:NF_USER_USERFIRSTNAME[$i])
											}</ns4:userFirstName>
                                        </ns4:User>
                                    </ns3:user>
                                    
                                    <ns3:till>
                                        <ns3:Till>
                                            <ns3:tillID>{
												data($fML321/ns6:NF_TILL_TILLID[$i])
											}</ns3:tillID>
                                        </ns3:Till>
                                    </ns3:till>
                                    
                                    <ns3:teller>
                                        <ns3:Teller>
                                            <ns3:tellerID>{
												data($fML321/ns6:NF_TELLER_TELLERID[$i])
											}</ns3:tellerID>
                                        </ns3:Teller>
                                    </ns3:teller>

                                </ns3:UserTxnSession>
                            </ns1:userTxnSession>
                        </ns1:CurrencyCash>
                    </ns1:currencyCash>
                </ns1:DenominationSpecification>
                
		}</ns5:denominationSpecificationList>
	</ns5:invokeResponse>
};

<soap-env:Body>{
	xf:getDenominationInBranchResponse($fML321)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>