<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns2:invoke" location="getCashInBranch.WSDL" ::)
(:: pragma bea:global-element-return element="ns1:FML32" location="getCashInBranchRequest.xsd" ::)

declare namespace xf = "http://tempuri.org/branch/getCashInBranch/getCashInBranchRequest/";
declare namespace ns0 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:getCashInBranchRequest($invoke1 as element(ns2:invoke), $header1 as element(ns2:header))
    as element(ns1:FML32) {
        <ns1:FML32>
        	<ns1:NF_MSHEAD_MSGID?>{ data($header1/ns2:msgHeader/ns2:msgId) }</ns1:NF_MSHEAD_MSGID>
            <ns1:NF_BRANCC_BRANCHCODE?>{ data($invoke1/ns2:branchCode/ns0:BranchCode/ns0:branchCode) }</ns1:NF_BRANCC_BRANCHCODE>
        </ns1:FML32>
};

declare variable $invoke1 as element(ns2:invoke) external;
declare variable $header1 as element(ns2:header) external;

<soap-env:Body>
{ xf:getCashInBranchRequest($invoke1, $header1) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>