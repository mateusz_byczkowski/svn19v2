<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-23</con:description>
  <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="chgBranchStatus.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="chgBranchStatus.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns0:invokeResponse" location="chgBranchStatus.WSDL" ::)
(:: pragma bea:global-element-return element="ns5:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns9 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns5 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns6 = "urn:entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns10 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns11 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:acceptance.entities.be.dcl";
declare namespace ns2 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Branches/chgBranchStatus/chgBranchStatusTLRequest/";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare function xf:chgBranchStatusTLRequest($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke),
    $invokeResponse1 as element(ns0:invokeResponse) ?,
    $faultResponse as element() ?)
    
    as element(ns5:transactionLogEntry) {
        <ns5:transactionLogEntry>
            {
                for $businessTransactionType in $invoke1/ns0:transaction/ns11:Transaction/ns11:businessTransactionType/ns9:BusinessTransactionType/ns9:businessTransactionType
                return
                    <businessTransactionType>{ data($businessTransactionType) }</businessTransactionType>
            }
            {
                for $csrMessageType in $invoke1/ns0:transaction/ns11:Transaction/ns11:csrMessageType/ns9:CsrMessageType/ns9:csrMessageType
                return
                    <csrMessageType>{ data($csrMessageType) }</csrMessageType>
            }
            {
                for $dtTransactionType in $invoke1/ns0:transaction/ns11:Transaction/ns11:dtTransactionType/ns9:DtTransactionType/ns9:dtTransactionType
                return
                    <dtTransactionType>{ data($dtTransactionType) }</dtTransactionType>
            }
            <executor>
                {
                    for $branchCode in $invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:branchCode/ns4:BranchCode/ns4:branchCode
                    return
                        <branchNumber>{ xs:int( data($branchCode) ) }</branchNumber>
                }
                <executorID>{ data($header1/ns0:msgHeader/ns0:userId) }</executorID>
                {
                    for $userFirstName in $invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:lastChangeUser/ns6:User/ns6:userFirstName
                    return
                        <firstName>{ data($userFirstName) }</firstName>
                }
                {
                    for $userLastName in $invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:lastChangeUser/ns6:User/ns6:userLastName
                    return
                        <lastName>{ data($userLastName) }</lastName>
                }
                {
                    for $tellerID in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:teller/ns1:Teller/ns1:tellerID
                    return
                        <tellerID>{ data($tellerID) }</tellerID>
                }
                {
                    for $tillID in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:till/ns1:Till/ns1:tillID
                    return
                        <tillNumber>{ data($tillID) }</tillNumber>
                }
                {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
                (
                    for $beUserId in $invokeResponse1/ns0:backendResponse/ns11:BackendResponse/ns11:beUserId
                    return
                        <userID>{ data($beUserId) }</userID>
                )
                	else
                	()
                }
                
            </executor>
            <hlbsName>chgBranchStatus</hlbsName>
            {
                for $workDate in $invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:workDate
                return
                    <putDownDate>{ data($workDate) }</putDownDate>
            }
            <timestamp>{ data($header1/ns0:msgHeader/ns0:timestamp) }</timestamp>
            {
                let $acceptTask := $invoke1/ns0:acceptTask,
                    $AcceptTask := $acceptTask/ns8:AcceptTask
                return
                    <tlAcceptance>
                        {
                            for $acceptorFirstName in $AcceptTask/ns8:acceptorFirstName
                            return
                                <acceptorFirstName>{ data($acceptorFirstName) }</acceptorFirstName>
                        }
                        {
                            for $acceptorLastName in $AcceptTask/ns8:acceptorLastName
                            return
                                <acceptorLastName>{ data($acceptorLastName) }</acceptorLastName>
                        }
                        {
                            for $acceptor in $AcceptTask/ns8:acceptor
                            return
                                <acceptorSkp>{ data($acceptor) }</acceptorSkp>
                        }
                        {
                            for $acceptItemList in $AcceptTask/ns8:acceptItemList,
                                $AcceptItem in $acceptItemList/ns8:AcceptItem
                            return
                                <tlAcceptanceTitleList>
                                    {
                                        for $acceptItemTitle in $AcceptItem/ns8:acceptItemTitle/ns10:AcceptItemTitle/ns10:acceptItemTitle
                                        return
                                            <acceptItemTitle>{ data($acceptItemTitle) }</acceptItemTitle>
                                    }
                                </tlAcceptanceTitleList>
                        }
                    </tlAcceptance>
            }
            {
                let $acceptanceForBE := $invoke1/ns0:acceptanceForBE,
                    $AcceptanceForBE := $acceptanceForBE/ns11:AcceptanceForBE
                return
                    <tlAcceptanceForBE>
                        {
                            for $acceptorFirstName in $AcceptanceForBE/ns11:acceptorFirstName
                            return
                                <acceptorFirstName>{ data($acceptorFirstName) }</acceptorFirstName>
                        }
                        {
                            for $acceptorLastName in $AcceptanceForBE/ns11:acceptorLastName
                            return
                                <acceptorLastName>{ data($acceptorLastName) }</acceptorLastName>
                        }
                        {
                            for $acceptorSKP in $AcceptanceForBE/ns11:acceptorSKP
                            return
                                <acceptorSkp>{ data($acceptorSKP) }</acceptorSkp>
                        }
                        {
                            for $flag in $AcceptanceForBE/ns11:flag
                            return
                                <flag>{ data($flag) }</flag>
                        }
                        {
                            for $BeErrorCode in $AcceptanceForBE/ns11:beErrorCodeList/ns11:BeErrorCode
                            return
                                <tlErrorCodeList>
                                    <errorCode>
                                        {
                                            for $errorCode in $BeErrorCode/ns11:errorCode/ns9:BackendErrorCode/ns9:errorCode
                                            return
                                                <errorCode>{ data($errorCode) }</errorCode>
                                        }
                                        {
                                            for $systemId in $BeErrorCode/ns11:errorCode/ns9:BackendErrorCode/ns9:systemId
                                            return
                                                <systemId>{ data($systemId) }</systemId>
                                        }
                                    </errorCode>
                                </tlErrorCodeList>
                        }
                    </tlAcceptanceForBE>
            }
            <tlExceptionDataList?>
            {
            	let $errorCode1 := data($faultResponse//err:exceptionItem/err:errorCode1)
            	let $errorCode2 := data($faultResponse//err:exceptionItem/err:errorCode2)
                let $errorDescription := data($faultResponse//err:exceptionItem/err:errorDescription)
            	return
                   if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') != '') then
            		(
		                <errorCode1?>{ xs:int($errorCode1) }</errorCode1>,
        		        <errorCode2?>{ xs:int($errorCode2) }</errorCode2>,
                		<errorDescription?>{ $errorDescription }</errorDescription>
                	)
                	else
                	()
			}
            </tlExceptionDataList>
            {
                for $workDate in $invoke1/ns0:branchCurrentStatus/ns1:BranchCurrentStatus/ns1:workDate
                return
                    <transactionDate>{ data($workDate) }</transactionDate>
            }
            <transactionID>{ data($header1/ns0:transHeader/ns0:transId) }</transactionID>
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                for $transactionStatus in $invokeResponse1/ns0:transactionOut/ns11:Transaction/ns11:transactionStatus/ns4:TransactionStatus/ns4:transactionStatus
                return
                    <transactionStatus>{ data($transactionStatus) }</transactionStatus>
                	)
                	else
                	()
            }
        </ns5:transactionLogEntry>
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;
declare variable $invokeResponse1 as element(ns0:invokeResponse) ? external;
declare variable $faultResponse  as element() ? external;

<soap-env:Body>{
xf:chgBranchStatusTLRequest($header1,
    $invoke1,
    $invokeResponse1,
    $faultResponse)
    
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>