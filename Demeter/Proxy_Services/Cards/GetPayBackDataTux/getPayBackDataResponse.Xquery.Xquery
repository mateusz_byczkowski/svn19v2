<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Zamiana XML dla FML - odpowiedź z usługiVersion.$2.2011-07-22</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "http://bzwbk.com/services/prime/";

declare function xf:map_getPayBackDataResponse($req as element(urn:GetPaybackNumberResponse))
	as element(fml:FML32) {

		<fml:FML32>
				<fml:DC_NR_PB1?>{ data($req/urn:GetPaybackNumberResult/urn:StrPbId1)}</fml:DC_NR_PB1>
				<fml:DC_CRD_DESIGN?>{ data($req/urn:GetPaybackNumberResult/urn:CrdDesign)}</fml:DC_CRD_DESIGN>
				<fml:DC_CRD_LAYOUT?>{ data($req/urn:GetPaybackNumberResult/urn:CrdLayout)}</fml:DC_CRD_LAYOUT>
				<fml:DC_KOD_SPRZEDAWCY?>{ data($req/urn:GetPaybackNumberResult/urn:SalesmanCode)}</fml:DC_KOD_SPRZEDAWCY>
				<fml:DC_NR_PB2?>{ data($req/urn:GetPaybackNumberResult/urn:StrPbid2)}</fml:DC_NR_PB2>
				<fml:DC_FLAGA_KLIENTA?>{ data($req/urn:GetPaybackNumberResult/urn:FromPayback)}</fml:DC_FLAGA_KLIENTA>
		</fml:FML32>

};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_getPayBackDataResponse($body/urn:GetPaybackNumberResponse) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>