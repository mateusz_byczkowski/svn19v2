<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:card.entities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:filtersandmessages.entities.be.dcl";
declare namespace ns6="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDateTime($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };



declare function getElementsForHistory($parm as element(fml:FML32)) as element()
{

<ns0:history>
  {
    for $x at $occ in $parm/NF_TRANHC_TRANCODE
    return
    <ns2:TransactionHistoryCard>
      { insertDateTime(data($parm/NF_TRANHC_TRANDATE[$occ]),"yyyy-MM-dd-HH.mm.ss","ns2:tranDate")}
      <ns2:tranCode?>{data($parm/NF_TRANHC_TRANCODE[$occ])}</ns2:tranCode>
      <ns2:tranAmountWn?>{data($parm/NF_TRANHC_TRANAMOUNTWN[$occ])}</ns2:tranAmountWn>
      <ns2:tranPlace?>{data($parm/NF_TRANHC_TRANPLACE[$occ])}</ns2:tranPlace>
      <ns2:device?>{data($parm/NF_TRANHC_DEVICE[$occ])}</ns2:device>
      <ns2:tranPlaceID?>{data($parm/NF_TRANHC_TRANPLACEID[$occ])}</ns2:tranPlaceID>
      <ns2:tranID?>{data($parm/NF_TRANHC_TRANID[$occ])}</ns2:tranID>
      <ns2:aquaierID?>{data($parm/NF_TRANHC_AQUAIERID[$occ])}</ns2:aquaierID>
      <ns2:before?>{data($parm/NF_TRANHC_BEFORE[$occ])}</ns2:before>
      <ns2:past?>{data($parm/NF_TRANHC_PAST[$occ])}</ns2:past>
      <ns2:cardFeeTran?>{data($parm/NF_TRANHC_CARDFEETRAN[$occ])}</ns2:cardFeeTran>
      <ns2:availableBalance?>{data($parm/NF_TRANHC_AVAILABLEBALANCE[$occ])}</ns2:availableBalance>
      <ns2:tranAmountMa?>{data($parm/NF_TRANHC_TRANAMOUNTMA[$occ])}</ns2:tranAmountMa>
      <ns2:tranTitle?>{data($parm/NF_TRANHC_TRANTITLE[$occ])}</ns2:tranTitle>
    </ns2:TransactionHistoryCard>
  }
</ns0:history>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForHistory($parm)}
  <ns0:response>
    <ns5:ResponseMessage>
      {
       if (data($parm/NF_RESPOM_ERRORCODE)) then
             <ns5:result>false</ns5:result>
       else
            <ns5:result>true</ns5:result>
       }
      <ns5:errorCode?>{data($parm/NF_RESPOM_ERRORCODE)}</ns5:errorCode>
      <ns5:errorDescription?>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}</ns5:errorDescription>
    </ns5:ResponseMessage>
  </ns0:response>
  <ns0:bcd>
    <ns3:BusinessControlData>
      <ns3:pageControl>
        <ns4:PageControl>
          <ns4:hasNext>{data($parm/NF_PAGEC_HASNEXT)}</ns4:hasNext>
          <ns4:navigationKeyDefinition>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns4:navigationKeyDefinition>
          <ns4:navigationKeyValue>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns4:navigationKeyValue>
        </ns4:PageControl>
      </ns3:pageControl>
    </ns3:BusinessControlData>
  </ns0:bcd>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>