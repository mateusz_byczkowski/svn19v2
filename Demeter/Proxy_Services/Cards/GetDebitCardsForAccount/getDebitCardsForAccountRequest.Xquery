<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:crddict.dictionaries.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns8="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};

declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns6:msgHeader/ns6:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns6:msgHeader/ns6:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns6:msgHeader/ns6:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns6:msgHeader/ns6:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns6:msgHeader/ns6:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns6:msgHeader/ns6:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns6:transHeader/ns6:transId)}</NF_TRHEAD_TRANSID>
};

declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{
if (string-length($parm/ns6:debitCard/ns7:DebitCard/ns7:contract)>0) then
   <NF_DEBITC_CONTRACT?>{boolean2SourceValue(data($parm/ns6:debitCard/ns7:DebitCard/ns7:contract),"1","0")}</NF_DEBITC_CONTRACT>
else()
,

<NF_DEBITC_CARDNBR?>{data($parm/ns6:debitCard/ns7:DebitCard/ns7:cardNbr)}</NF_DEBITC_CARDNBR>
,
<NF_DEBITC_VIRTUALCARDNBR?>{data($parm/ns6:debitCard/ns7:DebitCard/ns7:virtualCardNbr)}</NF_DEBITC_VIRTUALCARDNBR>

,
 for $x in data($parm/ns6:cardStatuses/ns0:CrdStatus/ns0:crdStatus)
    return
     <NF_CRDS_CRDSTATUS?>{$x}</NF_CRDS_CRDSTATUS>
,
<NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns6:cardUser/ns2:Customer/ns2:customerNumber)}</NF_CUSTOM_CUSTOMERNUMBER>
,
<NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns6:account/ns1:Account/ns1:accountNumber)}</NF_ACCOUN_ACCOUNTNUMBER>
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{boolean2SourceValue(data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:reverseOrder),"1","0")}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:navigationKeyDefinition)}</NF_PAGEC_NAVIGATIONKEYDEFI>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns6:bcd/ns4:BusinessControlData/ns4:pageControl/ns8:PageControl/ns8:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvoke($body/ns6:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>