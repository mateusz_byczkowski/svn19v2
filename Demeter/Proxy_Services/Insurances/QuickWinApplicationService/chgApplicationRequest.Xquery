<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-08</con:description>
  <con:xquery><![CDATA[declare namespace crw = "http://bzwbk.com/crw/services/quickwin/";
declare namespace urn = "urn:be.services.dcl";

declare namespace urn1 = "urn:accounts.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:productstree.entities.be.dcl";
declare namespace urn4 = "urn:insurance.entities.be.dcl";
declare namespace urn5 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace urn6 = "urn:cif.entities.be.dcl";
declare namespace urn7 = "urn:filtersandmessages.entities.be.dcl";
declare namespace urn8 = "urn:baseauxentities.be.dcl";

declare variable $invoke as element(urn:invoke) external;
declare variable $header as element(urn:header) external;

<crw:updateApplication>
	 <application>
		<actualDate?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:actualDate) }</actualDate>
		{
		for $info in $invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:additionalInfoList/urn7:AdditionalInfo
		return
			<additionalInfos>
				<definition?>{ data($info/urn7:additionalInfoDefinition/urn2:AdditionalInfoDefinition/urn2:additionalInfoDefinition) }</definition>
				<value?>{ data($info/urn7:value) }</value>
			</additionalInfos>
		}
		<applicationStatus?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:insPolicyApplication/urn4:InsPolicyApplication/urn4:applicationStatus/urn5:InsPolicyAppStatus/urn5:insPolicyAppStatus) }</applicationStatus>
		<currency>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:currency/urn2:CurrencyCode/urn2:currencyCode) }</currency>
		{
		for $role in $invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:accountRelationshipList/urn6:AccountRelationship
		return
			<customerRoles>
			   <customer>
				  <companyName?>{ data($role/urn6:customer/urn6:Customer/urn6:companyName)}</companyName>
				  <customerNumber?>{ data($role/urn6:customer/urn6:Customer/urn6:customerNumber) }</customerNumber>
				  <documentNumber?>{ data($role/urn6:customer/urn6:Customer/urn6:documentList/urn6:Document/urn6:documentNumber) }</documentNumber>
				  <documentType?>{ data($role/urn6:customer/urn6:Customer/urn6:documentList/urn6:Document/urn6:documentType/urn2:CustomerDocumentType/urn2:customerDocumentType) }</documentType>
				  <firstName?>{ data($role/urn6:customer/urn6:Customer/urn6:customerPersonal/urn6:CustomerPersonal/urn6:firstName) }</firstName>
				  <lastName?>{ data($role/urn6:customer/urn6:Customer/urn6:customerPersonal/urn6:CustomerPersonal/urn6:lastName) }</lastName>
			   </customer>
			   <relationship?>{ data($role/urn6:relationship/urn2:CustomerAccountRelationship/urn2:customerAccountRelationship) }</relationship>
			</customerRoles>
		}
		<externalPolices?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:externalPolices) }</externalPolices>
		<fixedPremium?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:fixedPremium) }</fixedPremium>
		<icbsProductNumber?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:icbsProductNumber) }</icbsProductNumber>
		<insuranceDescription?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:insuranceDescription) }</insuranceDescription>
		<linkedAccount>
		   <accountDescription?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:accountDescription) }</accountDescription>
		   <accountNumber?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:accountNumber) }</accountNumber>
		   <currency?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:currency/urn2:CurrencyCode/urn2:currencyCode) }</currency>
		   <faceAmount?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:loanAccount/urn1:LoanAccount/urn1:faceAmount) }</faceAmount>
		   <idProductDefinition?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:productDefinition/urn3:ProductDefinition/urn3:idProductDefinition) }</idProductDefinition>
		   <totalLimitAmount?>{ data($invoke/urn:linkedAccount/urn1:Account/urn1:tranAccount/urn1:TranAccount/urn1:totalLimitAmount) }</totalLimitAmount>
		</linkedAccount>
		<loanApplicationNum?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:insPolicyApplication/urn4:InsPolicyApplication/urn4:loanApplicationNum) }</loanApplicationNum>
		<maintenanceAccount>
		   <accountDescription?>{ data($invoke/urn:maintenanceAccount/urn1:Account/urn1:accountDescription) }</accountDescription>
		   <accountNumber?>{ data($invoke/urn:maintenanceAccount/urn1:Account/urn1:accountNumber) }</accountNumber>
		   <currency?>{ data($invoke/urn:maintenanceAccount/urn1:Account/urn1:currency/urn2:CurrencyCode/urn2:currencyCode) }</currency>
		   <idProductDefinition?>{ data($invoke/urn:maintenanceAccount/urn1:Account/urn1:productDefinition/urn3:ProductDefinition/urn3:idProductDefinition) }</idProductDefinition>
		</maintenanceAccount>
		<number>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:insPolicyApplication/urn4:InsPolicyApplication/urn4:applicationNumber)  }</number>
		<oddPremiumDay?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:oddPremiumDay/urn2:SpecialDay/urn2:specialDay) }</oddPremiumDay>
		<owuSignature?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:insPolicyApplication/urn4:InsPolicyApplication/urn4:owuSignature) }</owuSignature>
		<premiumDueDate?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:premiumDueDate) }</premiumDueDate>
		<premiumFrequency?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:premiumFrequency) }</premiumFrequency>
		<premiumPeriod?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:premiumPeriod/urn2:Period/urn2:period) }</premiumPeriod>
		<saleEmployeeID?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:saleEmployeeID) }</saleEmployeeID>
		<startDate?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:startDate) }</startDate>
		<validityDate?>{ data($invoke/urn:insurancePolicyAcc/urn4:InsurancePolicyAcc/urn4:validityDate) }</validityDate>
	 </application>
	<authData>
		<applicationId>{ data($header/urn:msgHeader/urn:appId) }</applicationId>
		<applicationPassword>{ data($invoke/urn:password/urn8:StringHolder/urn8:value)}</applicationPassword>
		<operator>{ data($header/urn:msgHeader/urn:userId) }</operator>
	</authData>
</crw:updateApplication>]]></con:xquery>
</con:xqueryEntry>