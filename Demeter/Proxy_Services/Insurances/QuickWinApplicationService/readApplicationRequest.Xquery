<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-08</con:description>
  <con:xquery><![CDATA[declare namespace crw = "http://bzwbk.com/crw/services/quickwin/";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:insurance.entities.be.dcl";
declare namespace urn8 = "urn:baseauxentities.be.dcl";

declare variable $invoke as element(urn:invoke) external;
declare variable $header as element(urn:header) external;


<crw:readApplication>
	<number?>{ data($invoke/urn:insPolicyApplication/urn1:InsPolicyApplication/urn1:applicationNumber) }</number>
	<authData>
		<applicationId>{ data($header/urn:msgHeader/urn:appId) }</applicationId>
		<applicationPassword>{ data($invoke/urn:password/urn8:StringHolder/urn8:value)}</applicationPassword>
		<operator>{ data($header/urn:msgHeader/urn:userId) }</operator>
	</authData>
</crw:readApplication>]]></con:xquery>
</con:xqueryEntry>