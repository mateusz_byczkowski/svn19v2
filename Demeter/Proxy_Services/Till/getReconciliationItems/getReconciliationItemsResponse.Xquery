<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.5
 : @since   2010-02-01
 :
 : wersja WSDLa: 05-10-2009 13:19:46
 :
 : $Proxy Services/Till/getReconciliationItems/getReconciliationItemsResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/getReconciliationItems/getReconciliationItemsResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:cash.operations.entities.be.dcl";
declare namespace ns3 = "";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns5 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns6 = "urn:entities.be.dcl";
declare namespace ns7 = "urn:be.services.dcl";
declare namespace ns8 = "urn:baseauxentities.be.dcl";

declare variable $fML321 as element(ns3:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getReconciliationItemsResponse($fML321 as element(ns3:FML32))
    as element(ns7:invokeResponse)
{
    <ns7:invokeResponse>
    
        <ns7:response>
            <ns0:ResponseMessage>
                	<ns0:result>
						true
					</ns0:result>
			</ns0:ResponseMessage>
        </ns7:response>
        
        <ns7:userTxnSessionList>{
			for $i in 1 to count($fML321/ns3:NF_USERTS_SESSIONDATE)
			return
				<ns5:UserTxnSession>

					{
						let $sessionDate := fn:normalize-space($fML321/ns3:NF_USERTS_SESSIONDATE[$i])
						return
							if (data($sessionDate)) then
								<ns5:sessionDate>{
									data($sessionDate)
								}</ns5:sessionDate>
							else ()
					}
					
					<ns5:currencyReconciliationList>
						<ns2:CurrencyReconciliation>

							<ns2:differenceAmount>{
								data($fML321/ns3:NF_CURRRE_DIFFERENCEAMOUNT[$i])
							}</ns2:differenceAmount>

							{
								let $reconDate := $fML321/ns3:NF_CURRRE_RECONCILIATIONDA[$i]
								return
									if (data($reconDate)) then
										<ns2:reconciliationDate>{
											fn:concat(fn:substring(data($reconDate), 1, 10),
													  'T',
													  fn:substring(data($reconDate), 12, 2),
													  ':',
													  fn:substring(data($reconDate), 15, 2),
													  ':',
													  fn:substring(data($reconDate), 18, 2))
										}</ns2:reconciliationDate>
									else ()
							}

							<ns2:currency>
								<ns4:CurrencyCode>
									<ns4:currencyCode>{
										data($fML321/ns3:NF_CURREC_CURRENCYCODE[$i])
									}</ns4:currencyCode>
								</ns4:CurrencyCode>
							</ns2:currency>

							<ns2:reconciliationStatus>
								<ns1:CurrencyReconcStatus>
									<ns1:currencyReconcStatus>{
										data($fML321/ns3:NF_CURRRS_CURRENCYRECONCST[$i])
									}</ns1:currencyReconcStatus>
								</ns1:CurrencyReconcStatus>
							</ns2:reconciliationStatus>

							<ns2:reconciliationExtStatus>
								<ns1:CurrencyReconcExtStatus>
									<ns1:currencyReconcExtStatus>{
										data($fML321/ns3:NF_CURRES_CURRENCYRECONCEX[$i])
									}</ns1:currencyReconcExtStatus>
								</ns1:CurrencyReconcExtStatus>
							</ns2:reconciliationExtStatus>

							<ns2:acceptor>
								<ns6:User>
									<ns6:userID>{
										data($fML321/ns3:NF_BUSICD_USERID[$i])
									}</ns6:userID>
								</ns6:User>
							</ns2:acceptor>

						</ns2:CurrencyReconciliation>
					</ns5:currencyReconciliationList>

					<ns5:user>
						<ns6:User>

							<ns6:userLastName>{
								data($fML321/ns3:NF_USER_USERLASTNAME[$i])
							}</ns6:userLastName>

							<ns6:userID>{
								data($fML321/ns3:NF_USER_USERID[$i])
							}</ns6:userID>

							<ns6:userFirstName>{
								data($fML321/ns3:NF_USER_USERFIRSTNAME[$i])
							}</ns6:userFirstName>

						</ns6:User>
					</ns5:user>

					<ns5:till>
						<ns5:Till>
							<ns5:tillID>{
								data($fML321/ns3:NF_TILL_TILLID[$i])
							}</ns5:tillID>
						</ns5:Till>
					</ns5:till>

					<ns5:teller>
						<ns5:Teller>
							<ns5:tellerID>{
								data($fML321/ns3:NF_TELLER_TELLERID[$i])
							}</ns5:tellerID>
						</ns5:Teller>
					</ns5:teller>
    			</ns5:UserTxnSession>
		}</ns7:userTxnSessionList>                        
                
        <ns7:bcd>
            <ns6:BusinessControlData>
                <ns6:pageControl?>
                    <ns8:PageControl?>
                    	{
                    		let $hasNext := data($fML321/ns3:NF_PAGEC_HASNEXT)
                    		return
                    			if ($hasNext) then
			                    	<ns8:hasNext>{
										xs:boolean($hasNext)
			                    	}</ns8:hasNext>
			                    else ()
                    	}

                        <ns8:navigationKeyDefinition?>{
							data($fML321/ns3:NF_PAGEC_NAVIGATIONKEYDEFI)
						}</ns8:navigationKeyDefinition>
						
                        <ns8:navigationKeyValue?>{
							data($fML321/ns3:NF_PAGEC_NAVIGATIONKEYVALU)
						}</ns8:navigationKeyValue>
                    </ns8:PageControl>
                </ns6:pageControl>
            </ns6:BusinessControlData>
        </ns7:bcd>
        
    </ns7:invokeResponse>
};

<soap-env:Body>{
	xf:getReconciliationItemsResponse($fML321)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>