<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Zmiana pola FML z NF_DENOMS_ITEMSNUMBER na NF_COLLAT_NUMBEROFUNITS.Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 : @author  Kacper Pawlaczyk
 : @version 1.5
 : @since   2010-08-03
 :
 : wersja WSDLa: 12-11-2009 11:07:43
 :
 : $Proxy Services/Till/getCashInTill/getCashInTillResponse.xq$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getCashInTill/getCashInTillResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:cash.operations.entities.be.dcl";
declare namespace ns3 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns5 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns6 = "urn:entities.be.dcl";
declare namespace ns7 = "urn:be.services.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getCashInTillResponse($fML321 as element(ns0:FML32))
    as element(ns7:invokeResponse)
{
    <ns7:invokeResponse>
        <ns7:userTxnSessionOut>
            <ns5:UserTxnSession?>
            
            	{
            		let $cashValNeeded := data($fML321/ns0:NF_USERTS_CASHVALIDATIONNE)
            		return
            			if ($cashValNeeded) then
			            	<ns5:cashValidationNeeded>{
								xs:boolean($cashValNeeded)
							}</ns5:cashValidationNeeded>
            			else
							()
            	}

                <ns5:currencyCashList>{
                
					(:
					 : lista walut - distinct
					 :)
               		let $currencyList := fn:distinct-values($fML321/ns0:NF_CURREC_CURRENCYCODE)

					(:
					 : dla każdej różnej waluty
					 :)               		
					for $currencyCode in $currencyList
					return                
                        <ns2:CurrencyCash>
                           (:
							:
							: licznosc $fML321/ns0:NF_CURRCA_AMOUNT == licznosc $fML321/ns0:NF_CURREC_CURRENCYCODE
							:
							: Ponieważ kwoty dla dwóch różnych walut mogą się powtarzać,
							: szukamy pierwszego indeksu na liście wszystkich dla każdej z walut.
							: Znaleziony indeks jest również indeksem odpowiadającej walucie kwoty.
							:
							: Przykładowo:
							: NF_CURRCA_CURRENCYCODE    PLN
							: NF_CURRCA_CURRENCYCODE    PLN
							: NF_CURRCA_CURRENCYCODE    USD
							: NF_CURRCA_AMOUNT          1000.00 (dla PLN)
							: NF_CURRCA_AMOUNT          1000.00 (dla PLN)
							: NF_CURRCA_AMOUNT          1000.00 (dla USD)
							:
							: wowczas index-of dla $currencyCode == PLN zwroci (1,2) (wybieramy pierwsze wystapnienie - [1])
							:         index-of dla $currencyCode == USD zwroci (3)   (analogicznie)
							:
							: dodatkowo: brak tagu <amount> w przypadku, gdy backend zwroci <NF_CURRCA_AMOUNT/>
							:)
                        	{
                        		let $amountIndex := fn:index-of($fML321/ns0:NF_CURREC_CURRENCYCODE, $currencyCode)[1]
								return
									let $amount := data($fML321/ns0:NF_CURRCA_AMOUNT[xs:int($amountIndex)])
									return
										if ($amount) then
											<ns2:amount>{
												$amount
											}</ns2:amount>
										else
											()
                        	}

                        	<ns2:denominationSpecificationList>{
								(:
								 : wszystkie wystąpienia NF_DENOMS_ITEMSNUMBER oraz NF_DENOMD_DENOMINATIONID
								 : na liscie wszystkich walut, ktore odpowiadaja
								 : obecnie rozważanej walucie (distinct)
								 :
								 : Przykladowo:
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE PLN
								 : NF_CURRCA_CURRENCYCODE USD
								 : NF_CURRCA_CURRENCYCODE USD
								 :

								 : Pierwsze przejście:
								 :      NF_DENOMS_ITEMSNUMBER, NF_DENOMD_DENOMINATIONID z 1., 2. oraz 3. wystąpienia
								 : Drugie przejście:
								 :		NF_DENOMS_ITEMSNUMBER, NF_DENOMD_DENOMINATIONID z 4. oraz 5. wystąpienia
								 :)
								for $i in 1 to count($fML321/ns0:NF_CURREC_CURRENCYCODE)
								where (data($fML321/ns0:NF_CURREC_CURRENCYCODE[$i]) eq $currencyCode)
								return
	                                <ns2:DenominationSpecification>
	                                
                                		<ns2:itemsNumber>{
											data($fML321/ns0:NF_COLLAT_NUMBEROFUNITS[$i])
										}</ns2:itemsNumber>
										
	                                    <ns2:denomination>
	                                        <ns1:DenominationDefinition>
                                        		<ns1:denominationID>{
													data($fML321/ns0:NF_DENOMD_DENOMINATIONID[$i])
												}</ns1:denominationID>
	                                        </ns1:DenominationDefinition>
	                                    </ns2:denomination>
	                                    
	                                </ns2:DenominationSpecification>
                        	}</ns2:denominationSpecificationList>
                        	
                            <ns2:currency>
                                <ns4:CurrencyCode>
                                    <ns4:currencyCode>{
										data($currencyCode)
									}</ns4:currencyCode>
                                </ns4:CurrencyCode>
                            </ns2:currency>
                    </ns2:CurrencyCash>
                }</ns5:currencyCashList>
                
                <ns5:currencyReconciliationList?>{
                	for $i in 1 to count($fML321/ns0:NF_CURREC_CURRENCYCODE2)
                	return
                        <ns2:CurrencyReconciliation>
                        
                            <ns2:negativeReconciliationCounter>{
								data($fML321/ns0:NF_CURRRE_NEGATIVERECONCIL[$i])
							}</ns2:negativeReconciliationCounter>
							
                            <ns2:currency>
                                <ns4:CurrencyCode>
                                    <ns4:currencyCode>{
										data($fML321/ns0:NF_CURREC_CURRENCYCODE2[$i])
									}</ns4:currencyCode>
                                </ns4:CurrencyCode>
                            </ns2:currency>
                            
                            <ns2:reconciliationStatus>
                                <ns1:CurrencyReconcStatus>
                                    <ns1:reconParameterRead>{
										data($fML321/ns0:NF_CURRRS_RECONPARAMETERRE[$i])
									}</ns1:reconParameterRead>
									
                                    <ns1:internalRecMKSStatusRead>{
										data($fML321/ns0:NF_CURRRS_INTRECMKSSTAREA[$i])
									}</ns1:internalRecMKSStatusRead>
                                </ns1:CurrencyReconcStatus>
                            </ns2:reconciliationStatus>
                        </ns2:CurrencyReconciliation>	                            
                }</ns5:currencyReconciliationList>
                
                (:
                 : 1 --> O (sesja otwarta)
                 : 0 --> C (sesja zamknięta)
                 :)
                {
					let $sessionStatus := data($fML321/ns0:NF_USETSS_USERTXNSESSIONST)
					return
						if ($sessionStatus) then
							<ns5:sessionStatus>
			                    <ns3:UserTxnSessionStatus>
									<ns3:userTxnSessionStatus>{
										if ($sessionStatus eq '1') then
											'O'
										else
											'C'
									}</ns3:userTxnSessionStatus>
			                    </ns3:UserTxnSessionStatus>
							</ns5:sessionStatus>
						else
							()
				}

                <ns5:user?>
                    <ns6:User?>
                        <ns6:userLastName?>{
							data($fML321/ns0:NF_USER_USERLASTNAME)
						}</ns6:userLastName>
						
                        <ns6:userID?>{
							data($fML321/ns0:NF_USER_USERID)
						}</ns6:userID>
						
                        <ns6:userFirstName?>{
							data($fML321/ns0:NF_USER_USERFIRSTNAME)
						}</ns6:userFirstName>
                    </ns6:User>
                </ns5:user>
                
                <ns5:till?>
                    <ns5:Till?>
                        <ns5:tillID?>{
							data($fML321/ns0:NF_TILL_TILLID)
						}</ns5:tillID>
                    </ns5:Till>
                </ns5:till>
                
                <ns5:teller?>
                    <ns5:Teller?>
                        <ns5:tellerID?>{
							data($fML321/ns0:NF_TELLER_TELLERID)
						}</ns5:tellerID>
                    </ns5:Teller>
                </ns5:teller>
                
            </ns5:UserTxnSession>
        </ns7:userTxnSessionOut>
    </ns7:invokeResponse>
};

<soap-env:Body>{
	xf:getCashInTillResponse($fML321)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>