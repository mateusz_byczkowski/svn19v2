<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-23</con:description>
  <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="chgCashStateForInternalTransport.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="chgCashStateForInternalTransport.WSDL" ::)
(:: pragma bea:global-element-return element="ns7:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns9 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:internaltransports.operations.entities.be.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Till/chgCashStateForInternalTransport_CR124/chgCashStateForInternalTransportSTRequest/";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns7 = "";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function local:dataToSV($data as xs:string) as xs:string
{
let $data:=$data
return
   if ($data) then
fn:concat(fn:substring($data, 9, 2),'-',fn:substring($data, 6, 2),'-',fn:substring($data, 1, 4))
else("")

};

declare function xf:chgCashStateForInternalTransportTDCRequest($invoke1 as element(ns0:invoke),
    $header1 as element(ns0:header))
    as element(ns7:FML32) {
        <ns7:FML32>
            <ns7:TR_DATA_OPER>{ local:dataToSV(xs:string( data($invoke1/ns0:targetDenomChg/ns8:Transaction/ns8:putDownDate))) }</ns7:TR_DATA_OPER>
            <ns7:TR_DATA_WALUTY>{ local:dataToSV(xs:string( data($invoke1/ns0:targetDenomChg/ns8:Transaction/ns8:transactionDate))) }</ns7:TR_DATA_WALUTY>
            <ns7:TR_CZAS_OPER>
            {
                let $transactionTime  := ($header1/ns0:msgHeader/ns0:timestamp)
                return
                      (fn:replace(
                       fn:substring(data($transactionTime), 12, 8)
                       ,':',''))
            }
			</ns7:TR_CZAS_OPER>        
            <ns7:TR_ID_OPER?>{ data($invoke1/ns0:targetDenomChg/ns8:Transaction/ns8:transactionID) }</ns7:TR_ID_OPER>
            <ns7:TR_ODDZ_KASY?>{ xs:short( data($invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:branchCurrentStatus/ns2:BranchCurrentStatus/ns2:branchCode/ns4:BranchCode/ns4:branchCode) ) }</ns7:TR_ODDZ_KASY>
            <ns7:TR_KASA?>{ xs:short( data($invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:targetTill/ns2:Till/ns2:tillID) ) }</ns7:TR_KASA>
            <ns7:TR_KASJER?>{ xs:short( data($invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:targetTeller/ns2:Teller/ns2:tellerID) ) }</ns7:TR_KASJER>
            <ns7:TR_UZYTKOWNIK?>{ fn:concat("SKP:",data($invoke1/ns0:targetUser/ns9:User/ns9:userID)) }</ns7:TR_UZYTKOWNIK>
            <ns7:TR_MSG_ID?>{ data($header1/ns0:msgHeader/ns0:msgId) }</ns7:TR_MSG_ID>
            <ns7:TR_TYP_KOM?>{ xs:short( data($invoke1/ns0:targetDenomChg/ns8:Transaction/ns8:csrMessageType/ns6:CsrMessageType/ns6:csrMessageType) ) }</ns7:TR_TYP_KOM>
            <ns7:TR_KANAL?>{ xs:short( data($invoke1/ns0:targetDenomChg/ns8:Transaction/ns8:extendedCSRMessageType/ns6:ExtendedCSRMessageType/ns6:extendedCSRMessageType) ) }</ns7:TR_KANAL>
            <ns7:TR_UZYTKOWNIK_SKP?>{ data($invoke1/ns0:targetUser/ns9:User/ns9:userID) }</ns7:TR_UZYTKOWNIK_SKP>
            {
            	let $CSRMsgType := xs:short( data($invoke1/ns0:targetDenomChg/ns8:Transaction/ns8:csrMessageType/ns6:CsrMessageType/ns6:csrMessageType))
            	return
            		if ($CSRMsgType ne 364 and $CSRMsgType ne 365)
            		then
            		(
		                for $transactionGroupID in $invoke1/ns0:targetTransaction/ns8:Transaction/ns8:transactionGroupID
        		        return
                		    <ns7:TR_ID_GR_OPER?>{ data($transactionGroupID) }</ns7:TR_ID_GR_OPER>
                    )
                    else ()
            }
            {
                for $DenominationSpecification in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:denominationSpecificationList/ns1:DenominationSpecification,
                    $denominationID in $DenominationSpecification/ns1:denomination/ns3:DenominationDefinition/ns3:denominationID
                return
                    <ns7:TR_NOMINAL?>{ data($denominationID) }</ns7:TR_NOMINAL>
            }
            {
                for $DenominationSpecification in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:denominationSpecificationList/ns1:DenominationSpecification,
                    $itemsNumber in $DenominationSpecification/ns1:itemsNumber
                return
                    <ns7:TR_NOMINAL_ILOSC?>{ xs:int( data($itemsNumber) ) }</ns7:TR_NOMINAL_ILOSC>
            }
        </ns7:FML32>
};

declare variable $invoke1 as element(ns0:invoke) external;
declare variable $header1 as element(ns0:header) external;

	xf:chgCashStateForInternalTransportTDCRequest($invoke1,
    $header1)]]></con:xquery>
</con:xqueryEntry>