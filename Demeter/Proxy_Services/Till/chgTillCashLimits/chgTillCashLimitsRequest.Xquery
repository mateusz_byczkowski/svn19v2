<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2010-01-07
 :
 : wersja WSDLa: 28-09-2009 14:43:41
 :
 : $Proxy Services/Till/chgTillCashLimits/chgTillCashLimitsRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/chgTillCashLimits/chgTillCashLimitsRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";

declare variable $header1 as element(ns3:header) external;
declare variable $invoke1 as element(ns3:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :) 
declare function xf:chgTillCashLimitsRequest($header1 as element(ns3:header),
											   $invoke1 as element(ns3:invoke))
    as element(ns0:FML32)
{
    <ns0:FML32>

    	(:
    	 : dane z nagłówka
    	 :)
        <ns0:DC_TRN_ID?>{
			data($header1/ns3:transHeader/ns3:transId)
		}</ns0:DC_TRN_ID>

        <ns0:DC_UZYTKOWNIK?>{
        	data($header1/ns3:msgHeader/ns3:userId)
        }</ns0:DC_UZYTKOWNIK>

        <ns0:DC_ODDZIAL?>{
        	data($header1/ns3:msgHeader/ns3:companyId)
        }</ns0:DC_ODDZIAL>

        (:
         : dane z komunikatu wejściowego
         :)
        <ns0:NF_TILLCL_LOCALAMOUNT?>{
			data($invoke1/ns3:tillCashLimits/ns2:TillCashLimits/ns2:localAmount)
		}</ns0:NF_TILLCL_LOCALAMOUNT>

        <ns0:NF_TILLCL_FOREIGNAMOUNT?>{
			data($invoke1/ns3:tillCashLimits/ns2:TillCashLimits/ns2:foreignAmount)
		}</ns0:NF_TILLCL_FOREIGNAMOUNT>

        {
			let $lastChangeDate := $invoke1/ns3:tillCashLimits/ns2:TillCashLimits/ns2:lastChangeDate
			return
				if (data($lastChangeDate)) then
					<ns0:NF_TILLCL_LASTCHANGEDATE>{
						fn:concat(fn:substring(data($lastChangeDate), 1, 10),
								  '-',
								  fn:replace(fn:substring(data($lastChangeDate), 12, 8),':','.'),
								  '.000000')
					}</ns0:NF_TILLCL_LASTCHANGEDATE>
		         else ()
        }

        <ns0:NF_TILLCL_LASTCHANGEUSER?>{
			data($invoke1/ns3:tillCashLimits/ns2:TillCashLimits/ns2:lastChangeUser)
		}</ns0:NF_TILLCL_LASTCHANGEUSER>

        {
        	for $i in 1 to count($invoke1/ns3:tillsList/ns2:Till)
        	return
        		<ns0:NF_TILL_TILLID>{
        			data($invoke1/ns3:tillsList/ns2:Till[$i]/ns2:tillID)
				}</ns0:NF_TILL_TILLID>
        }

        <ns0:NF_BRANCC_BRANCHCODE?>{
			data($invoke1/ns3:branchCode/ns1:BranchCode/ns1:branchCode)
		}</ns0:NF_BRANCC_BRANCHCODE>

    </ns0:FML32>
};

<soap-env:Body>{
	xf:chgTillCashLimitsRequest($header1, $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>