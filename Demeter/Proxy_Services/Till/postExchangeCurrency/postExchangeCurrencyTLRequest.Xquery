<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-23</con:description>
  <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$header1" element="ns1:header" location="postExchangeCurrency.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns1:invoke" location="postExchangeCurrency.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns1:invokeResponse" location="postExchangeCurrency.wsdl" ::)
(:: pragma bea:global-element-return element="ns7:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns9 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6 = "urn:acceptance.entities.be.dcl";
declare namespace ns10 = "urn:entities.be.dcl";
declare namespace ns7 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:feedict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:fee.operations.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/postExchangeCurrency/postExchangeCurrencyTLRequest/";
declare namespace ns11="urn:giif.operations.entities.be.dcl";
declare namespace ns13 = "urn:transactionfeedict.operationsdictionary.dictionaries.be.dcl";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare function xf:postExchangeCurrencyTLRequest($header1 as element(ns1:header),
    $invoke1 as element(ns1:invoke),
    $invokeResponse1 as element(ns1:invokeResponse)?,
    $faultResponse as element() ?)
    
    as element(ns7:transactionLogEntry) {
        <ns7:transactionLogEntry>
            {
                for $businessTransactionType in $invoke1/ns1:transaction/ns8:Transaction/ns8:businessTransactionType/ns5:BusinessTransactionType/ns5:businessTransactionType
                return
                    <businessTransactionType>{ data($businessTransactionType) }</businessTransactionType>
            }
            {
                for $cashTransactionBasketID in $invoke1/ns1:transaction/ns8:Transaction/ns8:cashTransactionBasketID
                return
                    <cashTransactionBasketID>{ data($cashTransactionBasketID) }</cashTransactionBasketID>
            }
            {
                for $transactionMa in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionMa
                return
                    <credit>
                        {
                            for $amountMa in $transactionMa/ns8:TransactionMa/ns8:amountMa
                            return
                                <amount>{ xs:decimal( data($amountMa) ) }</amount>
                        }
                        {
                            for $amountMaEquiv in $transactionMa/ns8:TransactionMa/ns8:amountMaEquiv
                            return
                                <amountPLN>{ xs:decimal( data($amountMaEquiv) ) }</amountPLN>
                        }
                        {
                            for $currencyCode in $transactionMa/ns8:TransactionMa/ns8:currencyCode/ns4:CurrencyCode/ns4:currencyCode
                            return
                                <currencyCode>{ data($currencyCode) }</currencyCode>
                        }
                        {
                            for $rate in $transactionMa/ns8:TransactionMa/ns8:rate
                            return
                                <exchangeRate>{ xs:decimal( data($rate) ) }</exchangeRate>
                        }
                    </credit>
            }
            {
                for $csrMessageType in $invoke1/ns1:transaction/ns8:Transaction/ns8:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType
                return
                    <csrMessageType>{ data($csrMessageType) }</csrMessageType>
            }
            {
                for $transactionWn in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionWn
                return
                    <debit>
                        {
                            for $amountWn in $transactionWn/ns8:TransactionWn/ns8:amountWn
                            return
                                <amount>{ xs:decimal( data($amountWn) ) }</amount>
                        }
                        {
                            for $amountWnEquiv in $transactionWn/ns8:TransactionWn/ns8:amountWnEquiv
                            return
                                <amountPLN>{ xs:decimal( data($amountWnEquiv) ) }</amountPLN>
                        }
                        {
                            for $currencyCode in $transactionWn/ns8:TransactionWn/ns8:currencyCode/ns4:CurrencyCode/ns4:currencyCode
                            return
                                <currencyCode>{ data($currencyCode) }</currencyCode>
                        }
                        {
                            for $rate in $transactionWn/ns8:TransactionWn/ns8:rate
                            return
                                <exchangeRate>{ xs:decimal( data($rate) ) }</exchangeRate>
                        }
                    </debit>
            }
            {
                let $result :=
                    for $Disposer in $invoke1/ns1:transaction/ns8:Transaction/ns8:disposerList/ns8:Disposer
                    return
                        <disposer1>
                            {
                                for $address in $Disposer/ns8:address
                                return
                                    <address>{ data($address) }</address>
                            }
                            {
                                for $cif in $Disposer/ns8:cif
                                return
                                    <cif>{ data($cif) }</cif>
                            }
                            {
                                for $citizenshipCode in $Disposer/ns8:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode
                                return
                                    <citizenship>{ data($citizenshipCode) }</citizenship>
                            }
                            {
                                for $city in $Disposer/ns8:city
                                return
                                    <city>{ data($city) }</city>
                            }
                            {
                                for $countryCode in $Disposer/ns8:countryCode/ns4:CountryCode/ns4:countryCode
                                return
                                    <countryCode>{ data($countryCode) }</countryCode>
                            }
                            {
                                for $dateOfBirth in $Disposer/ns8:dateOfBirth
                                return
                                    <dateOfBirth>{ xs:date( data($dateOfBirth)) }</dateOfBirth>
                            }
                            {
                                for $documentNumber in $Disposer/ns8:documentNumber
                                return
                                    <documentNumber>{ data($documentNumber) }</documentNumber>
                            }
                            {
                                for $documentTypeForTxn in $Disposer/ns8:documentType/ns5:DocumentTypeForTxn/ns5:documentTypeForTxn
                                return
                                    <documentType>{ data($documentTypeForTxn) }</documentType>
                            }
                            {
                                for $firstName in $Disposer/ns8:firstName
                                return
                                    <firstName>{ data($firstName) }</firstName>
                            }
                            {
                                for $lastName in $Disposer/ns8:lastName
                                return
                                    <lastName>{ data($lastName) }</lastName>
                            }
                            {
                                for $pesel in $Disposer/ns8:pesel
                                return
                                    <pesel>{ data($pesel) }</pesel>
                            }
                            {
                                for $zipCode in $Disposer/ns8:zipCode
                                return
                                    <zipCode>{ data($zipCode) }</zipCode>
                            }
                        </disposer1>
                return
                    $result[1]
            }
            {
                let $result :=
                    for $Disposer in $invoke1/ns1:transaction/ns8:Transaction/ns8:disposerList/ns8:Disposer
                    return
                        <disposer2>
                            {
                                for $address in $Disposer/ns8:address
                                return
                                    <address>{ data($address) }</address>
                            }
                            {
                                for $citizenshipCode in $Disposer/ns8:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode
                                return
                                    <citizenship>{ data($citizenshipCode) }</citizenship>
                            }
                            {
                                for $city in $Disposer/ns8:city
                                return
                                    <city>{ data($city) }</city>
                            }
                            {
                                for $countryCode in $Disposer/ns8:countryCode/ns4:CountryCode/ns4:countryCode
                                return
                                    <countryCode>{ data($countryCode) }</countryCode>
                            }
                            {
                                for $dateOfBirth in $Disposer/ns8:dateOfBirth
                                return
                                    <dateOfBirth>{ data($dateOfBirth) }</dateOfBirth>
                            }
                            {
                                for $documentNumber in $Disposer/ns8:documentNumber
                                return
                                    <documentNumber>{ data($documentNumber) }</documentNumber>
                            }
                            {
                                for $documentTypeForTxn in $Disposer/ns8:documentType/ns5:DocumentTypeForTxn/ns5:documentTypeForTxn
                                return
                                    <documentType>{ data($documentTypeForTxn) }</documentType>
                            }
                            {
                                for $firstName in $Disposer/ns8:firstName
                                return
                                    <firstName>{ data($firstName) }</firstName>
                            }
                            {
                                for $lastName in $Disposer/ns8:lastName
                                return
                                    <lastName>{ data($lastName) }</lastName>
                            }
                            {
                                for $pesel in $Disposer/ns8:pesel
                                return
                                    <pesel>{ data($pesel) }</pesel>
                            }
                            {
                                for $zipCode in $Disposer/ns8:zipCode
                                return
                                    <zipCode>{ data($zipCode) }</zipCode>
                            }
                        </disposer2>
                return
                    $result[2]
            }
            {
                for $dtTransactionType in $invoke1/ns1:transaction/ns8:Transaction/ns8:dtTransactionType/ns5:DtTransactionType/ns5:dtTransactionType
                return
                    <dtTransactionType>{ data($dtTransactionType) }</dtTransactionType>
            }
            <executor>
                {
                    for $branchCode in $invoke1/ns1:branchCode/ns4:BranchCode/ns4:branchCode
                    return
                        <branchNumber>{ xs:int( data($branchCode) ) }</branchNumber>
                }
                {
                    for $userID in $invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:user/ns10:User/ns10:userID
                    return
                        <executorID>{ data($userID) }</executorID>
                }
                {
                    for $userFirstName in $invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:user/ns10:User/ns10:userFirstName
                    return
                        <firstName>{ data($userFirstName) }</firstName>
                }
                {
                    for $userLastName in $invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:user/ns10:User/ns10:userLastName
                    return
                        <lastName>{ data($userLastName) }</lastName>
                }
                {
                    for $tellerID in $invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:teller/ns2:Teller/ns2:tellerID
                    return
                        <tellerID>{ data($tellerID) }</tellerID>
                }
                {
                    for $tillID in $invoke1/ns1:userTxnSession/ns2:UserTxnSession/ns2:till/ns2:Till/ns2:tillID
                    return
                        <tillNumber>{ data($tillID) }</tillNumber>
                }
                {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
                (
                    for $beUserId in $invokeResponse1/ns1:backendResponse/ns8:BackendResponse/ns8:beUserId
                    return
                        <userID>{ data($beUserId) }</userID>
                )
                	else
                	()
                }
            </executor>
            {
                for $extendedCSRMessageType in $invoke1/ns1:transaction/ns8:Transaction/ns8:extendedCSRMessageType/ns5:ExtendedCSRMessageType/ns5:extendedCSRMessageType
                return
                    <extendedCSRMessageType>{ data($extendedCSRMessageType) }</extendedCSRMessageType>
            }
            <hlbsName>postExchangeCurrency</hlbsName>
            {
                for $orderedBy in $invoke1/ns1:transaction/ns8:Transaction/ns8:orderedBy
                return
                    <orderedBy>{ data($orderedBy) }</orderedBy>
            }
            {
                for $putDownDate in $invoke1/ns1:transaction/ns8:Transaction/ns8:putDownDate
                return
                    <putDownDate>{ xs:date( data($putDownDate) ) }</putDownDate>
            }
			<timestamp>{ data($header1/ns1:msgHeader/ns1:timestamp) }</timestamp>
            {
                for $acceptTask in $invoke1/ns1:transaction/ns8:Transaction/ns8:acceptTask,
                    $AcceptTask in $acceptTask/ns6:AcceptTask
                return
                    <tlAcceptance?>
                        {
                            for $acceptorFirstName in $AcceptTask/ns6:acceptorFirstName
                            return
                                <acceptorFirstName>{ data($acceptorFirstName) }</acceptorFirstName>
                        }
                        {
                            for $acceptorLastName in $AcceptTask/ns6:acceptorLastName
                            return
                                <acceptorLastName>{ data($acceptorLastName) }</acceptorLastName>
                        }
                        {
                            for $acceptor in $AcceptTask/ns6:acceptor
                            return
                                <acceptorSkp>{ data($acceptor) }</acceptorSkp>
                        }
                        {
                            for $AcceptItem in $AcceptTask/ns6:acceptItemList,
                                $AcceptItem0 in $AcceptItem/ns6:AcceptItem
                            return
                                <tlAcceptanceTitleList?>
                                    {
                                        for $acceptItemTitle in $AcceptItem0/ns6:acceptItemTitle/ns9:AcceptItemTitle/ns9:acceptItemTitle
                                        return
                                            <acceptItemTitle>{ data($acceptItemTitle) }</acceptItemTitle>
                                    }
                                </tlAcceptanceTitleList>
                        }
                    </tlAcceptance>
            }
            {
                for $acceeptanceForBe in $invoke1/ns1:transaction/ns8:Transaction/ns8:acceeptanceForBe,
                    $AcceptanceForBE in $acceeptanceForBe/ns8:AcceptanceForBE
                return
                    <tlAcceptanceForBE?>
                        {
                            for $acceptorFirstName in $AcceptanceForBE/ns8:acceptorFirstName
                            return
                                <acceptorFirstName>{ data($acceptorFirstName) }</acceptorFirstName>
                        }
                        {
                            for $acceptorLastName in $AcceptanceForBE/ns8:acceptorLastName
                            return
                                <acceptorLastName>{ data($acceptorLastName) }</acceptorLastName>
                        }
                        {
                            for $acceptorSKP in $AcceptanceForBE/ns8:acceptorSKP
                            return
                                <acceptorSkp>{ data($acceptorSKP) }</acceptorSkp>
                        }
                        {
                            for $flag in $AcceptanceForBE/ns8:flag
                            return
                                <flag>{ data($flag) }</flag>
                        }
                        {
                            for $BeErrorCode in $AcceptanceForBE/ns8:beErrorCodeList/ns8:BeErrorCode
                            return
                                <tlErrorCodeList?>
                                    <errorCode?>
                                                <errorCode>{ data($BeErrorCode/ns8:errorCode/ns5:BackendErrorCode/ns5:errorCode) }</errorCode>,
                                                <systemId>1</systemId>
                                    </errorCode>
                                </tlErrorCodeList>
                        }
                    </tlAcceptanceForBE>
            }
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
            <tlBackendResponse?>
                {
                    for $balanceCredit in $invokeResponse1/ns1:backendResponse/ns8:BackendResponse/ns8:balanceCredit
                    return
                        <balanceCredit>{ xs:decimal( data($balanceCredit) ) }</balanceCredit>
                }
                {
                    for $balanceDebit in $invokeResponse1/ns1:backendResponse/ns8:BackendResponse/ns8:balanceDebit
                    return
                        <balanceDebit>{ xs:decimal( data($balanceDebit) ) }</balanceDebit>
                }
                {
                    for $dateTime in $invokeResponse1/ns1:backendResponse/ns8:BackendResponse/ns8:dateTime
                    return
                        <dateTime>{ data($dateTime) }</dateTime>
                }
                {
                    for $icbsDate in $invokeResponse1/ns1:backendResponse/ns8:BackendResponse/ns8:icbsDate
                    return
                        <icbsDate>{ xs:date( data($icbsDate) ) }</icbsDate>
                }
                {
                    for $icbsSessionNumber in $invokeResponse1/ns1:backendResponse/ns8:BackendResponse/ns8:icbsSessionNumber
                    return
                        <icbsSessionNumber>{ data($icbsSessionNumber) }</icbsSessionNumber>
                }
                {
                    for $psTransactionNumber in $invokeResponse1/ns1:backendResponse/ns8:BackendResponse/ns8:psTransactionNumber
                    return
                        <psTransactionNumber>{ data($psTransactionNumber) }</psTransactionNumber>
                }
                {
                    for $BeErrorCode in $invokeResponse1/ns1:backendResponse/ns8:BackendResponse/ns8:beErrorCodeList/ns8:BeErrorCode
                    return
                        <tlErrorCodeList?>
                            <errorCode?>
                                {
                                    for $errorCode in $BeErrorCode/ns8:errorCode/ns5:BackendErrorCode/ns5:errorCode
                                    return
                                        <errorCode>{ data($errorCode) }</errorCode>
                                }
                                <systemId>1</systemId>
                            </errorCode>
                        </tlErrorCodeList>
                }
                {
                    for $transactionRefNumber in $invokeResponse1/ns1:backendResponse/ns8:BackendResponse/ns8:transactionRefNumber
                    return
                        <transactionRefNumber>{ data($transactionRefNumber) }</transactionRefNumber>
                }
            </tlBackendResponse>
                    )
                            
                	else
                	()
                	}
            {
                for $TransactionExchange in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionExchange/ns8:TransactionExchange
                return
                    <tlTransactionExchange?>
                        {
                            for $countingDirection in $TransactionExchange/ns8:countingDirection
                            return
                                <countingDirection>{ data($countingDirection) }</countingDirection>
                        }
                        {
                            for $exchangeRateType in $TransactionExchange/ns8:exchangeRateType/ns5:ExchangeRateType/ns5:exchangeRateType
                            return
                                <exchangeRateType>{ data($exchangeRateType) }</exchangeRateType>
                        }
                        {
                            for $rateTableDate in $TransactionExchange/ns8:rateTableDate
                            return
                                <rateTableDate>{ data($rateTableDate) }</rateTableDate>
                        }
                    </tlTransactionExchange>
            }
            <tlTransactionFeeList>
                {
                    for $currencyCode in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionFee/ns0:TransactionFee/ns0:feeCurrency/ns4:CurrencyCode/ns4:currencyCode
                    return
                        <feeCurrency>{ data($currencyCode) }</feeCurrency>
                }
                {
                    for $TransactionFeeBusiness in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionFee/ns0:TransactionFee/ns0:transactionFeeFieldList/ns0:TransactionFeeField[1]/ns0:transactionFeeBussinessList/ns0:TransactionFeeBusiness
                    return
                        <tlTransactionFeeBussinessList?>
                            {
                                for $feeBusinessAmount in $TransactionFeeBusiness/ns0:feeBusinessAmount
                                return
                                    <feeBusinessAmount>{ xs:decimal( data($feeBusinessAmount) ) }</feeBusinessAmount>
                            }
                            {
                                for $feeCalculatedAmount in $TransactionFeeBusiness/ns0:feeCalculatedAmount
                                return
                                    <feeCalculatedAmount>{ xs:decimal( data($feeCalculatedAmount) ) }</feeCalculatedAmount>
                            }
                            {
                                for $feeID in $TransactionFeeBusiness/ns0:feeID,
                                    $feeID0 in $feeID/FeeDefinition/feeID
                                return
                                    <feeID>{ data($feeID0) }</feeID>
                            }
                        </tlTransactionFeeBussinessList>
                }
                {
                    for $TransactionFeeField in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionFee/ns0:TransactionFee/ns0:transactionFeeFieldList/ns0:TransactionFeeField
                    return
                        <tlTransactionFeeFieldList?>
                            {
                                for $feeField in $TransactionFeeField/ns0:feeField
                                return
                                    <feeField>{ data($feeField) }</feeField>
                            }
                            {
                                for $fieldFeeAmount in $TransactionFeeField/ns0:fieldFeeAmount
                                return
                                    <fieldFeeAmount>{ xs:decimal( data($fieldFeeAmount) ) }</fieldFeeAmount>
                            }
                            {
                                for $TransactionFeeBusiness in $TransactionFeeField/ns0:transactionFeeBussinessList/ns0:TransactionFeeBusiness
                                return
                                    <tlTransactionFeeBussinessList?>
                                        {
                                            for $feeBusinessAmount in $TransactionFeeBusiness/ns0:feeBusinessAmount
                                            return
                                                <feeBusinessAmount>{ xs:decimal( data($feeBusinessAmount) ) }</feeBusinessAmount>
                                        }
                                        {
                                            for $feeCalculatedAmount in $TransactionFeeBusiness/ns0:feeCalculatedAmount
                                            return
                                                <feeCalculatedAmount>{ xs:decimal( data($feeCalculatedAmount) ) }</feeCalculatedAmount>
                                        }
                                        {
                                            for $feeID in $TransactionFeeBusiness/ns0:feeID/ns13:FeeDefinition/ns13:feeID
                                            return
                                                <feeID>{data($feeID)}</feeID>
                                        }
                                        
                                    </tlTransactionFeeBussinessList>
                            }
                        </tlTransactionFeeFieldList>
                }
                {
                    for $totalFeeAmount in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionFee/ns0:TransactionFee/ns0:totalFeeAmount
                    return
                        <totalFeeAmount>{ xs:decimal( data($totalFeeAmount) ) }</totalFeeAmount>
                }
            </tlTransactionFeeList>
            {
                for $TransactionGIIF in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionGIIF/ns11:TransactionGIIF
                return
                    <tlTransactionGIIF?>
                        {
                            for $moneySource in $TransactionGIIF/ns11:moneySource
                            return
                                <moneySource>{ data($moneySource) }</moneySource>
                        }
                    </tlTransactionGIIF>
            }
            {
                for $transactionDate in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionDate
                return
                    <transactionDate>{ xs:date (data($transactionDate) ) }</transactionDate>
            }
            <tlExceptionDataList?>
            {
            	let $errorCode1 := data($faultResponse//err:exceptionItem/err:errorCode1)
            	let $errorCode2 := data($faultResponse//err:exceptionItem/err:errorCode2)
                let $errorDescription := data($faultResponse//err:exceptionItem/err:errorDescription)
            	return
                   if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') != '') then
            		(
		                <errorCode1?>{ xs:int($errorCode1) }</errorCode1>,
        		        <errorCode2?>{ xs:int($errorCode2) }</errorCode2>,
                		<errorDescription?>{ $errorDescription }</errorDescription>
                	)
                	else
                	()
			}
            </tlExceptionDataList>
            
            
            {
                for $transactionGroupID in $invoke1/ns1:transaction/ns8:Transaction/ns8:transactionGroupID
                return
                    <transactionGroupID>{ data($transactionGroupID) }</transactionGroupID>
            }
            <transactionID>{ data($header1/ns1:transHeader/ns1:transId) }</transactionID>
            {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                for $transactionStatus in $invokeResponse1/ns1:transactionOut/ns8:Transaction/ns8:transactionStatus/ns4:TransactionStatus/ns4:transactionStatus
                return
                    <transactionStatus>{ data($transactionStatus) }</transactionStatus>
                	)
                	else
                	()
            }
            {
                for $title in $invoke1/ns1:transaction/ns8:Transaction/ns8:title
                return
                    <transactionTitle>{ data($title) }</transactionTitle>
            }
        </ns7:transactionLogEntry>
};

declare variable $header1 as element(ns1:header) external;
declare variable $invoke1 as element(ns1:invoke) external;
declare variable $invokeResponse1 as element(ns1:invokeResponse) ? external;
declare variable $faultResponse  as element() ? external;


<soap-env:Body>{
xf:postExchangeCurrencyTLRequest($header1,
    $invoke1,
    $invokeResponse1,
    $faultResponse)
    
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>