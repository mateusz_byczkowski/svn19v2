<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @since   2010-02-17
 : @version 1.0
 :
 : wersja WSDLa: 06-07-2009 15:36:51
 :
 : $Proxy Services/Till/getCashInTills/getCashInTillsRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getCashInTills/getCashInTillsRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns2 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns6 = "urn:be.services.dcl";
declare namespace ns7 = "urn:baseauxentities.be.dcl";

declare variable $header1 as element(ns6:header) external;
declare variable $invoke1 as element(ns6:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:getCashInTillsRequest($header1 as element(ns6:header),
											$invoke1 as element(ns6:invoke))
    as element(ns0:FML32)
{
    <ns0:FML32>

		(:
		 : dane z nagłówka
		 :)
    	<ns0:NF_MSHEAD_MSGID?>{
			data($header1/ns6:msgHeader/ns6:msgId)
		}</ns0:NF_MSHEAD_MSGID>
    	
    	(:
    	 : dane do stronicowania
    	 :)
        <ns0:NF_PAGEC_ACTIONCODE?>{
			data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:actionCode)
		}</ns0:NF_PAGEC_ACTIONCODE>
		
        <ns0:NF_PAGEC_PAGESIZE?>{
			data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:pageSize)
		}</ns0:NF_PAGEC_PAGESIZE>
		
		
        {
        	let $reverseOrder := $invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:reverseOrder
        	return
	        	if (data($reverseOrder)) then
		        	<ns0:NF_PAGEC_REVERSEORDER>{
		        		if (data($reverseOrder) eq "false") then
		        			0
		        		else
		        			1
					}</ns0:NF_PAGEC_REVERSEORDER>
        		else ()
        }
        
        <ns0:NF_PAGEC_NAVIGATIONKEYDEFI?>{
			data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:navigationKeyDefinition)
		}</ns0:NF_PAGEC_NAVIGATIONKEYDEFI>
		
        <ns0:NF_PAGEC_NAVIGATIONKEYVALU?>{
			data($invoke1/ns6:bcd/ns5:BusinessControlData/ns5:pageControl/ns7:PageControl/ns7:navigationKeyValue)
		}</ns0:NF_PAGEC_NAVIGATIONKEYVALU>
		
		(:
		 : dane operacyjne
		 :)
        <ns0:NF_CURREC_CURRENCYCODE?>{
			data($invoke1/ns6:currencyCash/ns1:CurrencyCash/ns1:currency/ns3:CurrencyCode/ns3:currencyCode)
		}</ns0:NF_CURREC_CURRENCYCODE>
		
        <ns0:NF_BRANCC_BRANCHCODE?>{
			data($invoke1/ns6:branchCode/ns3:BranchCode/ns3:branchCode)
		}</ns0:NF_BRANCC_BRANCHCODE>
		
		(:
		 : status stanowiska
		 :
		 : O --> 1 (otwarte)
		 : C --> 0 (zamknięte)
		 :)
        {
			let $sessionStatus := $invoke1/ns6:userTxnSession/ns4:UserTxnSession/ns4:sessionStatus/ns2:UserTxnSessionStatus/ns2:userTxnSessionStatus
			return
            	if (data($sessionStatus)) then
		            <ns0:NF_USETSS_USERTXNSESSIONST>{
	            		if (data($sessionStatus) eq 'O') then
	            			1
	            		else
	            			0
	            	}</ns0:NF_USETSS_USERTXNSESSIONST>            	
	            else ()
		}
		
    </ns0:FML32>
};

<soap-env:Body>{
	xf:getCashInTillsRequest($header1, $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>