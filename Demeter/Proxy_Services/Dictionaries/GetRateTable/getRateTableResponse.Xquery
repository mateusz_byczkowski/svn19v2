<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDateTime($value1 as xs:string,$value2 as xs:string,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
    let $val2withZeros:=concat(substring("000000",1,6 - string-length($value2) ),$value2)
    let $input:=concat($value1,$val2withZeros)
    return
      if ($value1)
        then if(string-length($value1)>5 and not ($value1 = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$input)}
        else() 
      else()
};

declare function getElementsForRateTableList($parm as element(fml:FML32)) as element()
{

<ns4:rateTableList>
  {
    for $x at $occ in $parm/NF_CURREC_CURRENCYCODE
    return
    <ns0:RateTable>
      <ns0:averageRate?>{data($parm/NF_RATET_AVERAGERATE[$occ])}</ns0:averageRate>
      <ns0:transferBuyRate?>{data($parm/NF_RATET_TRANSFERBUYRATE[$occ])}</ns0:transferBuyRate>
      <ns0:transferBuyRatePref?>{data($parm/NF_RATET_TRANSFERBUYRATEPR[$occ])}</ns0:transferBuyRatePref>
      <ns0:transferSellRate?>{data($parm/NF_RATET_TRANSFERSELLRATE[$occ])}</ns0:transferSellRate>
      <ns0:transferSellRatePref?>{data($parm/NF_RATET_TRANSFERSELLRATEP[$occ])}</ns0:transferSellRatePref>
      <ns0:transferBuyRateNote?>{data($parm/NF_RATET_TRANSFERBUYRATENO[$occ])}</ns0:transferBuyRateNote>
      <ns0:transferSellRateNote?>{data($parm/NF_RATET_TRANSFERSELLRATEN[$occ])}</ns0:transferSellRateNote>
      {insertDateTime(data($parm/NF_RATET_CURRENCYDT[$occ]),data($parm/NF_RATET_CURRENCYTM[$occ]), "yyyy-MM-ddHHmmss","ns0:currencyDate")}
      (:<ns0:currencyDate?>{data($parm/NF_RATET_CURRENCYDATE[$occ])}</ns0:currencyDate>:)
      <ns0:averageRateNBP?>{data($parm/NF_RATET_AVERAGERATENBP[$occ])}</ns0:averageRateNBP>
      <ns0:currencyCode>
        <ns2:CurrencyCode>
          <ns2:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns2:currencyCode>
        </ns2:CurrencyCode>
      </ns0:currencyCode>
    </ns0:RateTable>
  }
</ns4:rateTableList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns4:invokeResponse>
  {getElementsForRateTableList($parm)}
</ns4:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>