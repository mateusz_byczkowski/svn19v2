<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns3="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accountdict.dictionaries.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
if ($parm)
  then
    if ($parm  = "true")
       then $trueval
    else if ($parm  = "1") then $trueval
       else $falseval
else $falseval
};

declare function defaultIfEmpty($parm as xs:string?,$defaultValue as xs:string) as xs:string{
    if ($parm)
       then insertDate($parm)
       else $defaultValue
};

declare function defaultIfFlagZero($parm as xs:string?,$defaultValue as xs:string, $flag as xs:string) as xs:string{
    if ($parm)
       then 
          if ($flag ="0") 
              then $defaultValue
          else
            insertDate($parm)
       else $defaultValue
};

declare function checkBalanceFlag($amount as xs:string?,$flag as xs:string) as xs:string?{
    if ($amount)
       then 
          if ($flag= "Y")
              then "99999999999.99"
          else
            $amount
       else ()
};

declare function defaultIfEmpty2($parm as xs:string?,$defaultValue as xs:string) as xs:string{
    if ($parm)
       then $parm
       else $defaultValue
};

declare function insertDate($value as xs:string?) as xs:string?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01") and not ($value = "01-01-0001"))
            then chgDate($value)
        else() 
      else()
      };

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function chgDate($value as xs:string) as xs:string
{
    let $year := substring($value, 1, 4)
    let $month := substring($value, 6, 2)
    let $day := substring($value, 9, 2)
return
     concat($day,concat("-",concat($month, concat("-", $year))))
};

declare function getFieldsFromHeader($parm as element(ns6:header)) as element()*
{

<DC_ODDZIAL?>{chkUnitId(data($parm/ns6:msgHeader/ns6:unitId))}</DC_ODDZIAL>
,
<DC_UZYTKOWNIK?>{concat("SKP:",data($parm/ns6:msgHeader/ns6:userId))}</DC_UZYTKOWNIK>
,
<DC_TRN_ID?>{data($parm/ns6:transHeader/ns6:transId)}</DC_TRN_ID>
,
<DC_NUMER_AKCJI_NA_BLOKADZIE?>{data($parm/ns6:transHeader/ns6:transId)}</DC_NUMER_AKCJI_NA_BLOKADZIE>

};
declare function getFieldsFromInvoke($parm as element(ns6:invoke)) as element()*
{

<DC_KWOTA?>{checkBalanceFlag(data($parm/ns6:hold/ns1:Hold/ns1:holdAmount), boolean2SourceValue(data($parm/ns6:hold/ns1:Hold/ns1:holdBalanceFlag),"Y","N"))}</DC_KWOTA>
,
<DC_OPIS_1?>{defaultIfEmpty2(data($parm/ns6:hold/ns1:Hold/ns1:holdDescription),"")}</DC_OPIS_1>
,
<DC_DATA_WAZNOSCI?>{defaultIfFlagZero(data($parm/ns6:hold/ns1:Hold/ns1:holdExpirationDate),"31-12-2049",boolean2SourceValue(data($parm/ns6:hold/ns1:Hold/ns1:holdExpirationDateFlag),"0","1"))}</DC_DATA_WAZNOSCI>
,
<DC_NUMER_SERYJNY_FAKTURY?>{defaultIfEmpty2(data($parm/ns6:hold/ns1:Hold/ns1:firstChecqueNumber),"")}</DC_NUMER_SERYJNY_FAKTURY>
,
<DC_NUMER_CZEKU?>{defaultIfEmpty2(data($parm/ns6:hold/ns1:Hold/ns1:lastChecqueNumber),"")}</DC_NUMER_CZEKU>
,
<DC_FLAGA_BLOKOWANIA_SALDA?>{boolean2SourceValue(data($parm/ns6:hold/ns1:Hold/ns1:holdBalanceFlag),"Y","N")}</DC_FLAGA_BLOKOWANIA_SALDA>
,
<DC_FLAGA_DATY_WYGASNIECIA?>{boolean2SourceValue(data($parm/ns6:hold/ns1:Hold/ns1:holdExpirationDateFlag),"0","1")}</DC_FLAGA_DATY_WYGASNIECIA>
,
<DC_DATA_CZEKU_FAKTURY?>{defaultIfEmpty(data($parm/ns6:hold/ns1:Hold/ns1:checqueRestrictDate),"")}</DC_DATA_CZEKU_FAKTURY>
,
<DC_UZYTKOWNIK_MODYFIKUJACY?>{data($parm/ns6:hold/ns1:Hold/ns1:holdCreateUserId)}</DC_UZYTKOWNIK_MODYFIKUJACY>
,
<DC_UZYTKOWNIK_AUTORYZUJACY?>{data($parm/ns6:hold/ns1:Hold/ns1:holdAcceptUserId)}</DC_UZYTKOWNIK_AUTORYZUJACY>
,
<DC_NUMER_BLOKADY?>{data($parm/ns6:hold/ns1:Hold/ns1:holdNumber)}</DC_NUMER_BLOKADY>
,
<DC_OPIS_2?>{defaultIfEmpty2(data($parm/ns6:hold/ns1:Hold/ns1:holdDescription2),"")}</DC_OPIS_2>
,
<DC_TYP_BLOKADY?>{data($parm/ns6:hold/ns1:Hold/ns1:holdType/ns3:HoldType/ns3:holdType)}</DC_TYP_BLOKADY>
,
<DC_WALUTA?>{defaultIfEmpty2(data($parm/ns6:hold/ns1:Hold/ns1:holdCurrencyCode/ns3:CurrencyCode/ns3:currencyCode),"PLN")}</DC_WALUTA>
,
<DC_TYP_ZMIANY?>{data($parm/ns6:hold/ns1:Hold/ns1:holdActionCode/ns4:HoldActionCode/ns4:holdActionCode)}</DC_TYP_ZMIANY>
,
<DC_FLAGA_AUTORYZACJI>O</DC_FLAGA_AUTORYZACJI>
,
<DC_NR_RACHUNKU?>{data($parm/ns6:account/ns1:Account/ns1:accountNumber)}</DC_NR_RACHUNKU>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns6:header)}
    {getFieldsFromInvoke($body/ns6:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>