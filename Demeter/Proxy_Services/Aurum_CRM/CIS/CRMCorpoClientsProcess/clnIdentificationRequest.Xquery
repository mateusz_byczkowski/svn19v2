<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-08-12</con:description>
  <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$fML321" element="ns1:FML32" location="../Operations/savetransfer/savetransferIN.xsd" ::)
(:: pragma bea:global-element-return element="ns0:ClnIdentification" location="xsd/clnIdentification.xsd" ::)

(:~
 :
 : @author  Arkadiusz Kasprzak/ITG
 : @version 0.1
 : @since   2011-07-20
 :
 : wersja WSDLa: 
 :
 : $Proxy Services/Aurum CRM/CIS/CRMCorpoClientsProcess/clnIdentificationRequest.xq$
 :
 :)

declare namespace ns2 = "urn:ca:ws:bzwbk:xsd:header.01";
declare namespace ns1 = "";
declare namespace ns0 = "urn:ca:ws:bzwbk:xsd:clnIdent.01";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Corpo/cInIdentifitacionRequest/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:cInIdentifitacionRequest($fML321 as element(ns1:FML32))
    as element(ns0:ClnIdentification) {
        <ns0:ClnIdentification>
            <ns0:CIF>{ data($fML321/ns1:DC_NUMER_KLIENTA) }</ns0:CIF>
            <ns0:ProspectId>{ xs:long( data($fML321/ns1:CI_NUMER_KLIENTA) ) }</ns0:ProspectId>
        </ns0:ClnIdentification>
};

declare function xf:corpoHeader($fML321 as element(ns1:FML32))
    as element(ns2:Header) {
        <ns2:Header>
        {
        	let $CI_PRACOWNIK_ZMIEN := xs:long( data($fML321/ns1:CI_PRACOWNIK_ZMIEN) )
        	let $CI_SKP_PRACOWNIKA := xs:long( data($fML321/ns1:CI_SKP_PRACOWNIKA) )
        	let $CI_PRACOWNIK_WPROW := xs:long( data($fML321/ns1:CI_PRACOWNIK_WPROW) )
        	return
        	(
        		if($CI_PRACOWNIK_ZMIEN)
					then <ns2:UserId>{ data($CI_PRACOWNIK_ZMIEN) }</ns2:UserId>
				else (if($CI_SKP_PRACOWNIKA)
					then <ns2:UserId>{ data($CI_SKP_PRACOWNIKA) }</ns2:UserId>
				else (<ns2:UserId>{ data($CI_PRACOWNIK_WPROW) }</ns2:UserId>))
			
			),	 
            <ns2:ApplicationId>1</ns2:ApplicationId>,
            <ns2:CompanyId>{ xs:long( data($fML321/ns1:CI_ID_SPOLKI) ) }</ns2:CompanyId>
        }
        </ns2:Header>
};

declare variable $fML321 as element(ns1:FML32) external;

<soap-env:Body>
{ 
xf:cInIdentifitacionRequest($fML321),
xf:corpoHeader($fML321)
}
</soap-env:Body>]]></con:xquery>
  <con:dependency location="../Operations/savetransfer/savetransferIN.xsd"/>
  <con:dependency location="xsd/clnIdentification.xsd"/>
</con:xqueryEntry>