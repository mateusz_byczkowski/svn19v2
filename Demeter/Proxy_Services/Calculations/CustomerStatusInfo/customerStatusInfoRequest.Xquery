<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-08-01</con:description>
  <con:xquery><![CDATA[declare namespace m = "urn:RatingWS";
declare namespace w = "";
declare namespace u = "http://swwr.talex.com/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

<soap-env:Body>
{
    let $req := $body/m:getCustomerStatus
    return
    
    <swwr:getCustomerStatus xmlns:swwr="http://swwr.talex.com/">
      {if($req/nip)
          then <w:nip>{ data($req/nip) }</w:nip>
          else ()
      }
    </swwr:getCustomerStatus>
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>