<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-17</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustomerProductDetailResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustomerProductDetailResponse) {
<m:CRMGetCustomerProductDetailResponse>
 <m:ProductCategoryId>200</m:ProductCategoryId> 
 <m:ProductCategoryCode>201</m:ProductCategoryCode> 
 <m:ProductNamePl>Rachunek karty kredytowej</m:ProductNamePl> 
 <m:ProductNameEn>EMPTY</m:ProductNameEn> 
 <m:ProductId>20034</m:ProductId> 
 <m:ProductCode>20001</m:ProductCode> 
 <m:ProductAtt1>423725</m:ProductAtt1> 
 <m:ProductAtt2>B</m:ProductAtt2> 
 <m:ProductAtt3/> 
 <m:ProductAtt4/> 
 <m:ProductAtt5/> 
 <m:ProductStat>T</m:ProductStat> 
 <m:Saldo>20000.00</m:Saldo>
 <m:DostSrodki>35000.00</m:DostSrodki>
 <m:Blokada>123.45</m:Blokada> 
 <m:Limit1>5000.00</m:Limit1> 
 <m:ProductNameOryginal>Rachunek karty kredytowej</m:ProductNameOryginal> 
 <m:Wlasciciel>0000000004</m:Wlasciciel> 
 <m:ZalMin>20.00</m:ZalMin> 
 <m:BiezMin>30.00</m:BiezMin> 
 <m:SplataMin>40.00</m:SplataMin> 
 <m:DataBPlat>2011-05-13</m:DataBPlat> 
 <m:DNastZest>2011-06-13</m:DNastZest> 
 <m:NrRach>71109010140000000105233179</m:NrRach>
 <m:SplataRazem>1234.12</m:SplataRazem>
 <m:UbezpPlatne>Ubezpieczenie płatne 2</m:UbezpPlatne> 
 <m:AutSplZadl>MAKSYMALNA</m:AutSplZadl>
 <m:RodzajProduktu>0203</m:RodzajProduktu>
 </m:CRMGetCustomerProductDetailResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetCustomerProductDetailResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>