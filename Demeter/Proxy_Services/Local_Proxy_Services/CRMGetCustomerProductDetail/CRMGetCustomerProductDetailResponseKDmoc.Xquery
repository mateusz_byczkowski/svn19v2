<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-17</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustomerProductDetailResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustomerProductDetailResponse) {
<m:CRMGetCustomerProductDetailResponse>
 <m:ProductCategoryId>100</m:ProductCategoryId> 
 <m:ProductCategoryCode>100</m:ProductCategoryCode> 
 <m:ProductNamePl>Visa Business Electron</m:ProductNamePl> 
 <m:ProductNameEn>EMPTY</m:ProductNameEn> 
 <m:ProductId>10034</m:ProductId> 
 <m:ProductCode>10001</m:ProductCode> 
 <m:Pojedynczy>N</m:Pojedynczy> 
 <m:NrRachunku>423725******2647</m:NrRachunku> 
 <m:KodRach>13008971</m:KodRach> 
 <m:NrAlternatywny>423725I002445815</m:NrAlternatywny> 
 <m:ProductAtt1>423725</m:ProductAtt1> 
 <m:ProductAtt2>B</m:ProductAtt2> 
 <m:ProductAtt3/> 
 <m:ProductAtt4/> 
 <m:ProductAtt5/> 
 <m:RachunekAdrAlt>T</m:RachunekAdrAlt> 
 <m:ImieINazwiskoAlt>BANK ZACHODNI WBK SA</m:ImieINazwiskoAlt> 
 <m:UlicaAdresAlt>RYNEK</m:UlicaAdresAlt> 
 <m:NrPosesLokaluAdresAlt>9/11</m:NrPosesLokaluAdresAlt> 
 <m:KodPocztowyAdresAlt>50-950</m:KodPocztowyAdresAlt> 
 <m:ProductStat>T</m:ProductStat> 
 <m:ProductStatusCode> 
 Karta wprowadzona - nieprzekazana do systemu kartowego 
 </m:ProductStatusCode> 
 <m:Saldo/> 
 <m:DostSrodki/> 
 <m:Blokada>123.45</m:Blokada> 
 <m:KodWaluty/> 
 <m:DOtwarcia>1999-07-28</m:DOtwarcia> 
 <m:DataOstOper>2011-04-26</m:DataOstOper> 
 <m:Limit1>150.00</m:Limit1> 
 <m:ProductNameOryginal>VISA Business Electron</m:ProductNameOryginal> 
 <m:Oproc1>19.9992</m:Oproc1> 
 <m:Oddzial>124</m:Oddzial> 
 <m:Wlasciciel>0000000004</m:Wlasciciel> 
 <m:Wspolposiadacz/> 
 <m:Pelnomocnik/> 
 <m:DOstatniejKap/> 
 <m:DNastepnejKap/> 
 <m:DKoncaLimitu/> 
 <m:DOstZest/> 
 <m:DNastZest/> 
 <m:NrRach>71109010140000000105233179</m:NrRach>
 <m:Karta/>
 <m:KkExpDate>2015-12-26</m:KkExpDate>
 <m:DKonWkl/> 
 <m:OkrWkladu>0</m:OkrWkladu> 
 <m:DPoczWkl/> 
 <m:SplataRazem/> 
 <m:DNastOper/> 
 <m:OdsPopWymag/> 
 <m:CyklLimitu>Dzienny</m:CyklLimitu> 
 <m:LimitWyplatGotowki>5000.00</m:LimitWyplatGotowki> 
 <m:LimitWykorzystany>1500.00</m:LimitWykorzystany>
 <m:DataWznowienia>2012-12-25</m:DataWznowienia>
 <m:UbezpPlatne>T</m:UbezpPlatne> 
 <m:RodzajProduktu>0116</m:RodzajProduktu>
 
</m:CRMGetCustomerProductDetailResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMGetCustomerProductDetailResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>