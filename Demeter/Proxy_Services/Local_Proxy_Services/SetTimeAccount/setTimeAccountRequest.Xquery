<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns5="urn:filtersandmessages.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns3="urn:filterandmessages.dictionaries.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";


declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:mapSetTimeAccountRequest($req as element(ns1:Account), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

	let $msgId:= $msghead/dcl:msgId
	let $companyId:= $msghead/dcl:companyId
	let $userId := $msghead/dcl:userId
	let $appId:= $msghead/dcl:appId
	let $unitId := $msghead/dcl:unitId
	let $timestamp:= $msghead/dcl:timestamp
        let $transId:=$tranhead/dcl:transId

	let $customerNumber:= $req/ns1:customerNumber
        let $accountOpenDate:= $req/ns1:accountOpenDate
        let $creditPercentage:=$req/ns1:creditPrecentage
        let $skpOpener:=$req/ns1:skpOpener
        let $productCode:=$req/ns1:productCode
        let $officerCode:=$req/ns1:officerCode
        
        (: encja Time Account :)
        let $renewalFrequency:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:renewalFrequency
        let $originalAmount:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:originalAmount
        let $nextRenewalMaturityDate:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:nextRenewalMaturityDate
        let $dispositionCode:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:dispositionCode
        let $interestPaymentPeriod:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:interestPaymentPeriod/ns2:Period/ns2:period
        let $interestPaymentFrequency:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:interestPaymentFrequency
        let $nextInrterestPaymentDate:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:nextInrterestPaymentDate
        let $interestDisposition:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:interestDisposition
        let $penaultyPlan:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:penaultyPlan
        let $influenceWay:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:influenceWayas
        let $renewalId:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:renewalID/ns2:RenewalType/ns2:renewalType        
        let $renewalPeroid:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:renewalPeriod/ns2:Period/ns2:period
        let $competencyCode:=$req/ns1:timeAccount/ns1:TimeAccount/ns1:competencyCode/ns3:CompetencyCode/ns3:competencyCode
        
       (: Encja statementParameters :)
        let $frequency:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:frequency
        let $nextPrintoutDate:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:nextPrintoutDate
        let $printFlag:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:printFlag
        let $cycle:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:cycle/ns2:Period/ns2:period
        let $provideManner:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:provideManner/ns2:ProvideManner/ns2:provideManner

	(: Encja addressAcount :)
        let $houseFlatNumber:=$req/ns1:addressAccount/ns4:Address/ns4:houseFlatNumber
        let $city:=$req/ns1:addressAccount/ns4:Address/ns4:city
        let $validFrom:=$req/ns1:addressAccount/ns4:Address/ns4:validFrom
        let $county:=$req/ns1:addressAccount/ns4:Address/ns4:county
        let $validTo:=$req/ns1:addressAccount/ns4:Address/ns4:validTo
        let $street:=$req/ns1:addressAccount/ns4:Address/ns4:street
        let $zipCode:=$req/ns1:addressAccount/ns4:Address/ns4:zipCode
        let $post:=$req/ns1:addressAccount/ns4:Address/ns4:post
        let $local:=$req/ns1:addressAccount/ns4:Address/ns4:local
            let $addressType:=$req/ns1:addressAccount/ns4:Address/ns4:addressType/ns2:AddressType/ns2:addressType
            let $state:=$req/ns1:addressAccount/ns4:Address/ns4:state/ns2:State/ns2:state

	(: Encja accountType :)
        let $accountType:=$req/ns1:accountType/ns2:AccountType/ns2:accountType
	(: Encja currency :)
        let $currency:=$req/ns1:currency/ns2:CurrencyCode/ns2:currencyCode
	(: Encja accountBranchNumber :)
        let $accountBranchNumber:=$req/ns1:accountBranchNumber/ns2:BranchCode/ns2:branchCode
	return
          <fml:FML32>
			<DC_MSHEAD_MSGID>{data($msgId)}</DC_MSHEAD_MSGID>
            <DC_TRN_ID>{data($transId)}</DC_TRN_ID>
            <DC_UZYTKOWNIK>{concat("SKP:",data($skpOpener))}</DC_UZYTKOWNIK>
            <DC_ODDZIAL>{chkUnitId(data($unitId))}</DC_ODDZIAL>
            <DC_KOD_APLIKACJI>19</DC_KOD_APLIKACJI>
            <DC_NUMER_KLIENTA>{data($customerNumber)}</DC_NUMER_KLIENTA>
            <DC_RELACJA>SOW</DC_RELACJA>
            <DC_TYP_RELACJI>1</DC_TYP_RELACJI>
            <DC_PROCENT_ZOB_PODATKOWYCH>100.0000</DC_PROCENT_ZOB_PODATKOWYCH>

            <DC_NUMER_PRODUKTU>{data($productCode)}</DC_NUMER_PRODUKTU>
            <DC_NUMER_ODDZIALU>{data($accountBranchNumber)}</DC_NUMER_ODDZIALU>        
            <DC_KWOTA>{data($originalAmount)}</DC_KWOTA>
            {
               if ( string-length($officerCode)>0 )
                 then <DC_KOD_PRACOWNIKA>{data($officerCode)}</DC_KOD_PRACOWNIKA>
                 else   <DC_KOD_PRACOWNIKA>DCL</DC_KOD_PRACOWNIKA>                 
             } 

             {
              if ($renewalId) 
                then <DC_OPCJA_ODNAWIANIA>{data($renewalId)}</DC_OPCJA_ODNAWIANIA>
                else()
             }
             { 
              if ($interestDisposition)
                 then if (data($interestDisposition) = "true")
                     then <DC_KOD_DYSPOZYCJI_ODSETKI>T</DC_KOD_DYSPOZYCJI_ODSETKI>
                     else <DC_KOD_DYSPOZYCJI_ODSETKI>C</DC_KOD_DYSPOZYCJI_ODSETKI>
                 else()
             }
             { 
              if ($dispositionCode)
                 then if (data($dispositionCode) = "true")
                   then <DC_KOD_DYSPOZYCJI_KAPITAL>T</DC_KOD_DYSPOZYCJI_KAPITAL>
                   else <DC_KOD_DYSPOZYCJI_KAPITAL>N</DC_KOD_DYSPOZYCJI_KAPITAL>
                 else()
             }
             {
              if ($competencyCode) 
                then <DC_KOD_KOMPETENCJI>{data($competencyCode)}</DC_KOD_KOMPETENCJI>
                else()
             }
             { 

              if ($printFlag)
                 then  if (data($printFlag) = "true")
                   then <DC_ZNACZNIK_WYCIAGU_BRAKU_AKT>Y</DC_ZNACZNIK_WYCIAGU_BRAKU_AKT>
                   else <DC_ZNACZNIK_WYCIAGU_BRAKU_AKT>N</DC_ZNACZNIK_WYCIAGU_BRAKU_AKT>
                 else()
             }
             { 

              if ($printFlag)
                 then  if (data($printFlag) = "true")
                   then <DC_KOD_PRZYGOTOWANIA_WYCIAGU>Y</DC_KOD_PRZYGOTOWANIA_WYCIAGU>
                   else <DC_KOD_PRZYGOTOWANIA_WYCIAGU>N</DC_KOD_PRZYGOTOWANIA_WYCIAGU>
                 else()
             }
             {
              if ($cycle) 
                then <DC_KOD_CYKLU_WYCIAGOW>{data($cycle)}</DC_KOD_CYKLU_WYCIAGOW>
                else()
             }
             {
              if ($frequency) 
                then <DC_LICZBA_OKRESOW_CYKL_WYCIAG>{data($frequency)}</DC_LICZBA_OKRESOW_CYKL_WYCIAG>
                else()
             }
             {
              if ($nextPrintoutDate) 
                then <DC_DATA_NASTEPNEGO_WYCIAGU>{data($nextPrintoutDate)}</DC_DATA_NASTEPNEGO_WYCIAGU>
                else()
             }
             {
              if ($provideManner) 
                then <DC_KOD_SPECJALNYCH_INSTRUKCJI>{data($provideManner)}</DC_KOD_SPECJALNYCH_INSTRUKCJI>
                else()
             }
             {
              if ($penaultyPlan) 
                then <DC_NUMER_KARY>{data($penaultyPlan)}</DC_NUMER_KARY>
                else()
             }
             {
              if ($renewalPeroid) 
                then <DC_CZEST_OKRES_SD_OKRES>{upper-case(substring(data($renewalPeroid),1,1))}</DC_CZEST_OKRES_SD_OKRES>
                else()
             }
             {
              if ($renewalFrequency) 
                then <DC_CZEST_OKRES_SD_CZEST>{data($renewalFrequency)}</DC_CZEST_OKRES_SD_CZEST>
                else()
             }
           (: <DC_CZEST_OKRES_SD_DZIEN></DC_CZEST_OKRES_SD_DZIEN> :)
             {
              if ($nextRenewalMaturityDate) 
                then <DC_DATA_NAST_ODNOWIENIA_WYMAG>{fn-bea:date-to-string-with-format("dd-MM-yyyy",data($nextRenewalMaturityDate))}</DC_DATA_NAST_ODNOWIENIA_WYMAG>
                else()
             }
             {
              if ($interestPaymentPeriod) 
                then <DC_OKRES_NALICZENIA>{upper-case(substring(data($interestPaymentPeriod),1,1))}</DC_OKRES_NALICZENIA>
                else()
             }
             {
              if ($interestPaymentFrequency) 
                then <DC_CZESTOT_NALICZENIA>{data($interestPaymentFrequency)}</DC_CZESTOT_NALICZENIA>
                else()
             }

             {
              if ($nextInrterestPaymentDate) 
                then <DC_DATA_NAST_PLATNOSCI_ODSETE>{fn-bea:date-to-string-with-format("dd-MM-yyyy",data($nextInrterestPaymentDate))}</DC_DATA_NAST_PLATNOSCI_ODSETE>
                else()
             }
             {
              if ($creditPercentage) 
                then <DC_STOPA_PROCENTOWA>{data($creditPercentage)}</DC_STOPA_PROCENTOWA>
                else()
             }
             {
              if ($accountOpenDate) 
                then <DC_DATA_OTWARCIA_RACHUNKU>{fn-bea:date-to-string-with-format("dd-MM-yyyy",data($accountOpenDate))}</DC_DATA_OTWARCIA_RACHUNKU>
                else()
             }
            (:<DC_IMIE_I_NAZWISKO_ALT>{data()}</DC_IMIE_I_NAZWISKO_ALT>:)
            (:<DC_IMIE_I_NAZWISKO_ALT_C_D>{data()}</DC_IMIE_I_NAZWISKO_ALT_C_D>:)
             {
              if ($street) 
                then <DC_ULICA_ADRES_ALT>{data($street)}</DC_ULICA_ADRES_ALT>
                else()
             }
             {
              if ($zipCode) 
                then <DC_KOD_POCZTOWY_ADRES_ALT>{data($zipCode)}</DC_KOD_POCZTOWY_ADRES_ALT>
                else()
             }
             {
              if ($city) 
                then <DC_MIASTO_ADRES_ALT>{data($city)}</DC_MIASTO_ADRES_ALT>
                else()
             }
             {
              if ($houseFlatNumber) 
                then <DC_NR_POSESJI_LOKALU_ADRES_ALT>{data($houseFlatNumber)}</DC_NR_POSESJI_LOKALU_ADRES_ALT>
                else()
             }
             {
              if ($state or $county) 
                then <DC_WOJEWODZTWO_KRAJ_ADRES_ALT>{concat($state,'/',$county)}</DC_WOJEWODZTWO_KRAJ_ADRES_ALT>
                else()
             }
             {
              if ($validFrom) 
                then <DC_DATA_WPROWADZENIA>{data($validFrom)}</DC_DATA_WPROWADZENIA>
                else()
             }
             {
              if ($validTo) 
                then <DC_DATA_KONCOWA>{data($validTo)}</DC_DATA_KONCOWA>
                else()
             }
             {
              if ($local) 
                then <DC_TYP_ADRESU>{data($local)}</DC_TYP_ADRESU>
                else()
             }
       (:      {
               if ( string-length($validFrom)>0 )
                 then <DC_RODZAJ_RACHUNKU>TM</DC_RODZAJ_RACHUNKU>
                 else()
             } :)               

          </fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


<soap-env:Body>
{ xf:mapSetTimeAccountRequest($body/dcl:invoke/dcl:accountIn/ns1:Account, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>