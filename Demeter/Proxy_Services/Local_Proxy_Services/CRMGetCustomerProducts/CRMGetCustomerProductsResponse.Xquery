<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.5 2011-05-12   PKLI NP2307 Dodanie RodzajProduktu
v.1.4 2010-03-05   PKLI NP2037_1 CR1 
v.1.3  2010-01-28  PKLI  Obsługa ServiceWarning
v.1.2  2009-07-10  PKLI  TEET 39149
v.1.1  2009-06-24  PKLI  TEET 38316Version.$3.2011-07-27</con:description>
  <con:xquery><![CDATA[(: Log Zmian: 
==================================
 rk01 21/04/2009 RKu Pole ProductNamePl bedzie bralo nazwe z III poziomu drzewa prod (NF_ACCOUN_ACCOUNTDESCRIPTI), 
                                zamiast jak dotej pory z IV-go (PT_POLISH_NAME)
 v.1.1  2009-06-24  PKLI  TEET 38316 
                                Zmiana mapowania pola ProductId z PT_ID_DEFINITION na PT_ID_GROUP
 v.1.2  2009-07-10 PKLI TEET 39149
                                Zaprzestanie zwracania pól Saldo i DostSrodki dla kart debetowych 	
 v.1.3  2010-01-28  PKLI  NP1972_1
                                Obsługa ServiceWarning	
 v.1.4  2010-03-05  PKLI NP2037_1 CR1 
                                Zaprzestanie wyliczania salda ubezpieczeń JV 
 v.1.5  2011-05-12  PKLI NP2307 
                                Dodanie RodzajProduktu; Zwracanie rachunków kart (uwaga! zaślepka)
 v.1.6  2011-05-17  PKLI NP2401 
                                Zmiany interfejsu wyjściowego usługi (DataNastZapad-> NF_TIMEA_NEXTRENEWALMATURI)
:)

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:getElementsForProductsList($fml as element(fml:FML32))
	as element()* {
		
				let $CI_PRODUCT_CATEGORY_ID := $fml/fml:PT_ID_CATEGORY
				let $CI_PRODUCT_CATEGORY_CODE := $fml/fml:PT_CODE_PRODUCT_CATEGORY
				let $CI_PRODUCT_CATEGORY_NAME_PL := $fml/fml:PT_POLISH_CATEGORY_NAME

				let $CI_PRODUCT_CATEGORY_NAME_EN := $fml/fml:PT_ENGLISH_CATEGORY_NAME

				let $CI_PRODUCT_ID := $fml/fml:PT_ID_DEFINITION
                                                                let $CI_GROUP_ID := $fml/fml:PT_ID_GROUP   (: v.1.1 T38316 :)
				let $CI_PRODUCT_CODE := $fml/fml:NF_ACCOUT_ACCOUNTTYPE
				let $CI_PRODUCT_NAME_EN := $fml/fml:PT_ENGLISH_NAME
                                                                (: rk1 - ponizej bylo PT_POLISH_NAME, jest NF_ACCOUN_ACCOUNTDESCRIPTI :)
				let $CI_PRODUCT_NAME_PL := $fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI 
				let $NF_CTRL_SYSTEMID := $fml/fml:NF_CTRL_SYSTEMID
				let $B_POJEDYNCZY := $fml/fml:NF_IS_COOWNER
				let $NF_ACCOUN_ACCOUNTNUMBER:= $fml/fml:NF_ACCOUN_ACCOUNTNUMBER
                                let $NF_DEBITC_VIRTUALCARDNBR:=$fml/fml:NF_DEBITC_VIRTUALCARDNBR
                                let $NF_DEBITC_CARDNBR:=$fml/fml:NF_DEBITC_CARDNBR
				let $CI_PRODUCT_ATT1 := $fml/fml:NF_ATTRPG_FIRSTPRODUCTFEAT
				let $CI_PRODUCT_ATT2 := $fml/fml:NF_ATTRPG_SECONDPRODUCTFEA
				let $CI_PRODUCT_ATT3 := $fml/fml:NF_ATTRPG_THIRDPRODUCTFEAT
				let $CI_PRODUCT_ATT4 := $fml/fml:NF_ATTRPG_FOURTHPRODUCTFEA
				let $CI_PRODUCT_ATT5 := $fml/fml:NF_ATTRPG_FIFTHPRODUCTFEAT
				let $CI_KOD_PRODUKTU := $fml/fml:NF_PRODUD_SORCEPRODUCTCODE
				let $CI_RACHUNEK_ADR_ALT := $fml/fml:NF_ACCOUN_ALTERNATIVEADDRE
				let $B_SALDO := $fml/fml:NF_ACCOUN_CURRENTBALANCE
				let $DC_BLOKADA_SRODK_NA_R_KU := $fml/fml:NF_HOLD_HOLDAMOUNT
				let $DC_WALUTA := $fml/fml:NF_CURREC_CURRENCYCODE
				let $DC_DATA_OTWARCIA_RACHUNKU := $fml/fml:NF_ACCOUN_ACCOUNTOPENDATE
				let $B_DATA_OST_OPER := $fml/fml:NF_TRANA_DATEOFLASTACTIVIT
				let $B_LIMIT1 := $fml/fml:NF_TRAACA_LIMITAMOUNT
				let $CI_PRODUCT_NAME_ORYGINAL := $fml/fml:NF_ACCOUN_ACCOUNTNAME

				let $CI_STATUS := $fml/fml:NF_PRODUCT_STAT	

                                let $CI_VISIBILITY_DCL := $fml/fml:PT_VISIBILITY_DCL
                                let $CI_RELACJA := $fml/fml:NF_ACCOUR_RELATIONSHIP
                                let $B_DOST_SRODKI:= $fml/fml:NF_ACCOUN_CURRENTBALANCE
                                let $B_KOD_RACH:=$fml/fml:NF_PRIMAC_ACCOUNTCODE
                                let $NF_INSURA_ID:=$fml/fml:NF_INSURA_ID
                                let $NF_TRANA_DMEMP := $fml/fml:NF_TRANA_DMEMP
(:1.5:)                        let $NF_APPRO_PRODUCTCODE := $fml/fml:NF_APPRO_PRODUCTCODE
(:1.6:)                       let $NF_TIMEA_NEXTRENEWALMATURI := $fml/fml:NF_TIMEA_NEXTRENEWALMATURI


				for $it at $p in $fml/fml:NF_CTRL_SYSTEMID
				return
					<m:Product>	
      	            {
				                if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p])
				 	               then <m:ProductAreaId>{ data($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p]) }</m:ProductAreaId>
					        else ()
			        }
					{
						if($CI_PRODUCT_CATEGORY_ID[$p])
							then <m:ProductCategoryId>{ data($CI_PRODUCT_CATEGORY_ID[$p]) }</m:ProductCategoryId>
						else ()
					}
					{
						if($CI_PRODUCT_CATEGORY_CODE[$p])
							then <m:ProductCategoryCode>{ data($CI_PRODUCT_CATEGORY_CODE[$p]) }</m:ProductCategoryCode>
						else ()
					}
					{
						if($CI_PRODUCT_CATEGORY_NAME_PL[$p])
							then <m:ProductCategoryNamePl>{ data($CI_PRODUCT_CATEGORY_NAME_PL[$p]) }</m:ProductCategoryNamePl>
						else ()
					}
					{
						if($CI_PRODUCT_CATEGORY_NAME_EN[$p])
							then <m:ProductCategoryNameEn>{ data($CI_PRODUCT_CATEGORY_NAME_EN[$p]) }</m:ProductCategoryNameEn>
						else ()
					}
					{
					(:	if($CI_PRODUCT_ID[$p])                                                                                v.1.1 T38316 :)
					(:		then <m:ProductId>{ data($CI_PRODUCT_ID[$p]) }</m:ProductId>   v.1.1 T38316 :)
					(:	else ()                                                                                                          v.1.1 T38316 :)
						if($CI_GROUP_ID[$p])
							then <m:ProductId>{ data($CI_GROUP_ID[$p]) }</m:ProductId>
						else ()
					}
					{
						if($CI_PRODUCT_CODE[$p])
							then <m:ProductCode>{ data($CI_PRODUCT_CODE[$p]) }</m:ProductCode>
						else ()
					}
					{
						if($CI_PRODUCT_NAME_EN[$p])
							then <m:ProductNameEn>{ data($CI_PRODUCT_NAME_EN[$p]) }</m:ProductNameEn>
						else ()
					}
					{
						if($CI_PRODUCT_NAME_PL[$p])
							then <m:ProductNamePl>{ data($CI_PRODUCT_NAME_PL[$p]) }</m:ProductNamePl>
						else ()
					}
					{
						if($NF_CTRL_SYSTEMID[$p])
							then <m:IdSys>{ data($NF_CTRL_SYSTEMID[$p]) }</m:IdSys>
						else ()
					}
					{
						if($B_POJEDYNCZY[$p])
							then <m:Pojedynczy>{ data($B_POJEDYNCZY[$p]) }</m:Pojedynczy>
						else ()
					}
					{
(:1.5						if($NF_DEBITC_CARDNBR[$p] and string-length(data($NF_DEBITC_CARDNBR[$p]))>1 ) :)
(:1.5:)						if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] and data($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p])=1)
						   then <m:NrRachunku>{ data($NF_DEBITC_CARDNBR[$p]) }</m:NrRachunku>
						else if ($NF_INSURA_ID[$p] and string-length(data($NF_INSURA_ID[$p]))>1)
                                                       then <m:NrRachunku>{ data($NF_INSURA_ID[$p]) }</m:NrRachunku>
                                                   else
                                                      <m:NrRachunku>{ data($NF_ACCOUN_ACCOUNTNUMBER[$p]) }</m:NrRachunku>
					}

					{
						if($NF_DEBITC_CARDNBR[$p] and string-length(data($NF_DEBITC_CARDNBR[$p]))>1 )
							then <m:NrAlternatywny>{ data($NF_DEBITC_VIRTUALCARDNBR[$p]) }</m:NrAlternatywny>
						else if ($NF_INSURA_ID[$p] and string-length(data($NF_INSURA_ID[$p]))>1)
                                                        then if (string-length(data($NF_DEBITC_VIRTUALCARDNBR[$p]))>0)
                                                             then <m:NrAlternatywny>{ data($NF_DEBITC_VIRTUALCARDNBR[$p]) }</m:NrAlternatywny>
                                                             else()
                                                else()
					         
					}

					{
						if($CI_PRODUCT_ATT1[$p])
							then <m:ProductAtt1>{ data($CI_PRODUCT_ATT1[$p]) }</m:ProductAtt1>
						else ()
					}
					{
						if($CI_PRODUCT_ATT2[$p])
							then <m:ProductAtt2>{ data($CI_PRODUCT_ATT2[$p]) }</m:ProductAtt2>
						else ()
					}
					{
						if($CI_PRODUCT_ATT3[$p])
							then <m:ProductAtt3>{ data($CI_PRODUCT_ATT3[$p]) }</m:ProductAtt3>
						else ()
					}
					{
						if($CI_PRODUCT_ATT4[$p])
							then <m:ProductAtt4>{ data($CI_PRODUCT_ATT4[$p]) }</m:ProductAtt4>
						else ()
					}
					{
						if($CI_PRODUCT_ATT5[$p])
							then <m:ProductAtt5>{ data($CI_PRODUCT_ATT5[$p]) }</m:ProductAtt5>
						else ()
					}
					{
						if($CI_KOD_PRODUKTU[$p])
							then <m:KodProduktu>{ data($CI_KOD_PRODUKTU[$p]) }</m:KodProduktu>
						else ()
					}
					{
						if($CI_RACHUNEK_ADR_ALT[$p])
							then <m:RachunekAdrAlt>{ data($CI_RACHUNEK_ADR_ALT[$p]) }</m:RachunekAdrAlt>
						else ()
					}
					{
						(: T39149 if($B_SALDO[$p])   :)
						(: T39149	then <m:Saldo>{ data($B_SALDO[$p]) }</m:Saldo> :)
						(: T39149 else () :)
					
(:1.5						if($NF_DEBITC_CARDNBR[$p] and string-length(data($NF_DEBITC_CARDNBR[$p]))>1) :)
(:1.5:)                                                                                        if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] and data($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p])=1)
						  then ()                                                                                                             
						else                                                                                                                    
                                                                                                  <m:Saldo>{ data($B_SALDO[$p]) }</m:Saldo>	                                        
					}
					{
						if($DC_BLOKADA_SRODK_NA_R_KU[$p] and data($DC_BLOKADA_SRODK_NA_R_KU[$p])!="0.00")
							then <m:BlokadaSrodkNaRKu>1</m:BlokadaSrodkNaRKu>
						else <m:BlokadaSrodkNaRKu>0</m:BlokadaSrodkNaRKu>
					}
					{
						if($DC_WALUTA[$p])
							then <m:Waluta>{ data($DC_WALUTA[$p]) }</m:Waluta>
						else ()
					}
					{
						if($DC_DATA_OTWARCIA_RACHUNKU[$p])
							then <m:DataOtwarciaRachunku>{ data($DC_DATA_OTWARCIA_RACHUNKU[$p]) }</m:DataOtwarciaRachunku>
						else ()
					}
					{
						if($B_DATA_OST_OPER[$p])
							then <m:DataOstOper>{ data($B_DATA_OST_OPER[$p]) }</m:DataOstOper>
						else ()
					}
					{
						if($B_LIMIT1[$p])
							then <m:Limit1>{ data($B_LIMIT1[$p]) }</m:Limit1>
						else ()
					}
					{
						if($CI_PRODUCT_NAME_ORYGINAL[$p])
							then <m:ProductNameOryginal>{ data($CI_PRODUCT_NAME_ORYGINAL[$p]) }</m:ProductNameOryginal>
						else ()
					}
					{
						if($CI_STATUS[$p])
							then <m:Status>{ data($CI_STATUS[$p]) }</m:Status>
						else ()
					}
					{
(:1.5						if($CI_RELACJA[$p])
							then <m:Relacja>{ data($CI_RELACJA[$p]) }</m:Relacja>
						else ()  :)
(:1.5:)						if($fml/fml:NF_CTRL_SYSTEMID[$p] and data($fml/fml:NF_CTRL_SYSTEMID[$p])=2 and
(:1.5:)                                                                                            $fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] and data($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p])=1) 
(:1.5:)						  then ()                                                                                                             
(:1.5:)						else                                                                                                                    
(:1.5:)                                                                                         <m:Relacja>{ data($CI_RELACJA[$p]) }</m:Relacja>	                                        
					}
					{
						if($CI_VISIBILITY_DCL[$p])
							then <m:VisibilityDCL>{ data($CI_VISIBILITY_DCL[$p]) }</m:VisibilityDCL>
						else ()
					}
					{
					(: T39149 	if($B_DOST_SRODKI[$p]) :)
					(: T39149 		then <m:DostSrodki>{ data($B_DOST_SRODKI[$p]) }</m:DostSrodki> :)
					(: T39149 	else () :)

                                                                                                if($NF_DEBITC_CARDNBR[$p] and string-length(data($NF_DEBITC_CARDNBR[$p]))>1 ) 
						  then ()                                                                                                             
						else                                                                                                                    
                                                                                                  <m:DostSrodki>{ data($B_DOST_SRODKI[$p]) }</m:DostSrodki>	                         
					}
					{
						if($B_KOD_RACH[$p] and string-length($B_KOD_RACH[$p])>0)
							then <m:KodRach>{ data($B_KOD_RACH[$p]) }</m:KodRach>
						else if($NF_DEBITC_CARDNBR[$p] and string-length(data($NF_DEBITC_CARDNBR[$p]))>1 )
                                                       then  <m:KodRach>{ substring(data($NF_ACCOUN_ACCOUNTNUMBER[$p]),string-length($NF_ACCOUN_ACCOUNTNUMBER[$p]) - 9,10) }</m:KodRach>
                                               else()
					}
                                                                                {
						if($NF_TRANA_DMEMP[$p])
							then <m:FlagaPracownika>{ data($NF_TRANA_DMEMP[$p]) }</m:FlagaPracownika>
						else ()
					}
(:1.5 start:)                                                                {
						if($B_KOD_RACH[$p])
							then <m:IdRachunku>{ data($B_KOD_RACH[$p]) }</m:IdRachunku>
						else ()
					}                                                                
                                                                                {
						if($NF_APPRO_PRODUCTCODE[$p])
							then <m:RodzajProduktu>{ data($NF_APPRO_PRODUCTCODE[$p]) }</m:RodzajProduktu>
						else ()
					}
(:1.5 koniec:)  
(:1.6 start:)                                                                {
						if($NF_TIMEA_NEXTRENEWALMATURI[$p])
							then <m:DataNastZapad>{ data($NF_TIMEA_NEXTRENEWALMATURI[$p]) }</m:DataNastZapad>
						else ()
					}
(:1.6 koniec:)  




					</m:Product>
			
};

declare function xf:getElementsForServiceWarningsList($fml as element(fml:FML32))
	as element()* {
                                let $NF_SERVIW_WARNINGCODE1 := $fml/fml:NF_SERVIW_WARNINGCODE1
                                let $NF_SERVIW_WARNINGCODE2 := $fml/fml:NF_SERVIW_WARNINGCODE2
                                let $NF_SERVIW_WARNINGUSERVISIB := $fml/fml:NF_SERVIW_WARNINGUSERVISIB
                                let $NF_SERVIW_WARNINGDESCRIPTI := $fml/fml:NF_SERVIW_WARNINGDESCRIPTI 
 	                        
                                for $it at $p in $fml/fml:NF_SERVIW_WARNINGCODE1
				return
					<m:ServiceWarning>	
 					{
						if($NF_SERVIW_WARNINGCODE1[$p])
							then <m:WarningCode1>{ data($NF_SERVIW_WARNINGCODE1[$p]) }</m:WarningCode1>
	   					else ()
					}
					{
						if($NF_SERVIW_WARNINGCODE2[$p])
							then <m:WarningCode2>{ data($NF_SERVIW_WARNINGCODE2[$p]) }</m:WarningCode2>
						else ()
					}
					{
						if($NF_SERVIW_WARNINGUSERVISIB[$p])
							then <m:WarningUserVisibility>{ data($NF_SERVIW_WARNINGUSERVISIB[$p]) }</m:WarningUserVisibility>
						else ()
					}
					{
						if($NF_SERVIW_WARNINGDESCRIPTI[$p])
							then <m:WarningDescription>{ data($NF_SERVIW_WARNINGDESCRIPTI[$p]) }</m:WarningDescription>
						else ()
					} 
                                      	</m:ServiceWarning>	
};               
declare function xf:mappGetCustomerProductsResponse($fml as element(fml:FML32))
	as element() {
		<m:GetCustomerProductsResponse>
			{
				if($fml/fml:NF_PAGECC_OPERATIONS)
  				                then <m:LiczbaOper>{ data($fml/fml:NF_PAGECC_OPERATIONS) }</m:LiczbaOper> 
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
					then <m:KluczNawigacyjny>{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }</m:KluczNawigacyjny>
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_HASNEXT)
					then <m:NastepnaStrona>{ data($fml/fml:NF_PAGEC_HASNEXT) }</m:NastepnaStrona>
					else ()
			}
        
           {xf:getElementsForProductsList($fml)} 
           {xf:getElementsForServiceWarningsList($fml)} 
		</m:GetCustomerProductsResponse>        
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mappGetCustomerProductsResponse($body/fml:FML32) }

</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>