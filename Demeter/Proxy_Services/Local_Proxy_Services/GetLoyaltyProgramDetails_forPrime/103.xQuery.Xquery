<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-08-01</con:description>
  <con:xquery>declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace faul="http://bzwbk.com/services/cis/faults/";

declare variable $fault as element() external;

 data($fault/soapenv:Fault/detail/faul:NoDataException/ErrorCode2)</con:xquery>
</con:xqueryEntry>