<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-08-01</con:description>
  <con:xquery><![CDATA[<soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:be.services.dcl" xmlns:urn1="urn:loyaltyprogram.entities.be.dcl" xmlns:urn2="urn:dictionaries.be.dcl">
	<urn:invokeResponse>
		<urn:loyaltyProgram>
			<urn1:LoyaltyProgram>
				<urn1:lpNumber></urn1:lpNumber>
				<urn1:otherLpNumber></urn1:otherLpNumber>
				<urn1:forwardingDataApproval></urn1:forwardingDataApproval>
				<urn1:processingApproval></urn1:processingApproval>
				<urn1:applicationSource></urn1:applicationSource>
				<urn1:unitApplicationSource></urn1:unitApplicationSource>
				<urn1:sellerID></urn1:sellerID>
				<urn1:applicationDate></urn1:applicationDate>
				<urn1:resignationDate></urn1:resignationDate>
				<urn1:lastModificationDate></urn1:lastModificationDate>
				<urn1:lpStatus>
					<urn2:CustomerLpStatus>
						<urn2:customerLpStatus></urn2:customerLpStatus>
					</urn2:CustomerLpStatus>
				</urn1:lpStatus>
			</urn1:LoyaltyProgram>
		</urn:loyaltyProgram>
	</urn:invokeResponse>
</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>