<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:accounts.entities.be.dcl";
declare namespace e2 = "urn:accountdict.dictionaries.be.dcl";

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
   if ($value)
     then if(string-length($value)>5 and not ($value = "0001-01-01"))
         then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
     else() 
   else()
};

declare function local:mapGetNBPReserveResponse($fml as element())
    as element(m:invokeResponse) {
		<m:invokeResponse>
			<m:nbpReserve>
				<e:NBPReserve>
                                                                                { insertDate(data($fml/NF_NBPR_DATEOFPDOSTATUS),"yyyy-MM-dd","e:dateOfPDOStatus")}
					<e:currentReserve>{ data($fml/fml:NF_NBPR_CURRENTRESERVE) }</e:currentReserve>
					<e:previousReserve>{ data($fml/fml:NF_NBPR_PREVIOUSRESERVE) }</e:previousReserve>
					<e:provMadeYTD_T>{ data($fml/fml:NF_NBPR_PROVMADEYTD_T) }</e:provMadeYTD_T>
					<e:provCancYTD_T>{ data($fml/fml:NF_NBPR_PROVCANCYTD_T) }</e:provCancYTD_T>
					<e:provMadeYTD_N>{ data($fml/fml:NF_NBPR_PROVMADEYTD_N) }</e:provMadeYTD_N>
					<e:provCancYTD_N>{ data($fml/fml:NF_NBPR_PROVCANCYTD_N) }</e:provCancYTD_N>
					<e:overdrawnDays>{ data($fml/fml:NF_NBPR_OVERDRAWNDAYS) }</e:overdrawnDays>
					<e:pdoStatus>
						<e2:PdoStatus>
							<e2:pdoStatus>{ data($fml/fml:NF_PDOS_PDOSTATUS[1]) }</e2:pdoStatus>
						</e2:PdoStatus>
					</e:pdoStatus>
					<e:customerRiskCode>
						<e2:PdoStatus>
							<e2:pdoStatus>{ data($fml/fml:NF_PDOS_PDOSTATUS[2]) }</e2:pdoStatus>
						</e2:PdoStatus>
					</e:customerRiskCode>
					<e:provisionCalculationFlag>
						<e2:ProvisionCalculationFlag>
							<e2:provisionCalculationFlag>{ data($fml/fml:NF_PROVCF_PROVISIONCALCULA) }</e2:provisionCalculationFlag>
						</e2:ProvisionCalculationFlag>
					</e:provisionCalculationFlag>
				</e:NBPReserve>
			</m:nbpReserve>
		</m:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ local:mapGetNBPReserveResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>