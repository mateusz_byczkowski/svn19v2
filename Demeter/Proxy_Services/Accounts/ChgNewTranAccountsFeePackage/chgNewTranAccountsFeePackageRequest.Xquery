<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-22</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{
<DC_ODDZIAL?>{chkUnitId(data($parm/ns0:msgHeader/ns0:unitId))}</DC_ODDZIAL>
,
<DC_UZYTKOWNIK?>{concat("SKP:",data($parm/ns0:msgHeader/ns0:userId))}</DC_UZYTKOWNIK>
,
<DC_SKP_PRACOWNIKA?>{data($parm/ns0:msgHeader/ns0:userId)}</DC_SKP_PRACOWNIKA>
,
<DC_TRN_ID?>{data($parm/ns0:transHeader/ns0:transId)}</DC_TRN_ID>
};

declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

<DC_NUMER_WNIOSKU?>{data($parm/ns0:account/ns4:TranFeeAgreement/ns4:agreementNumber)}</DC_NUMER_WNIOSKU>
,
<DC_START_WNIOSKU?>{data($parm/ns0:account/ns4:TranFeeAgreement/ns4:agreementStartDate)}</DC_START_WNIOSKU>
,
<DC_KONIEC_WNIOSKU?>{data($parm/ns0:account/ns4:TranFeeAgreement/ns4:agreementEndDate)}</DC_KONIEC_WNIOSKU>
,
<DC_NUMER_KLIENTA?>{data($parm/ns0:account/ns4:TranFeeAgreement/ns4:customer/ns2:Customer/ns2:customerNumber)}</DC_NUMER_KLIENTA>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>