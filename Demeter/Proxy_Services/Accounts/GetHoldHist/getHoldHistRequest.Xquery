<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:accountdict.dictionaries.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm = "1") then $trueval
       else $falseval
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}</NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

<NF_HOLD_RECCOUNT?>{data($parm/ns0:hold/ns6:Hold/ns6:recCount)}</NF_HOLD_RECCOUNT>
,
<NF_HOLDT_HOLDTYPE?>{data($parm/ns0:hold/ns6:Hold/ns6:holdType/ns5:HoldType/ns5:holdType)}</NF_HOLDT_HOLDTYPE>
,
<NF_HOLD_HOLDNUMBER>{data($parm/ns0:hold/ns6:Hold/ns6:holdNumber)}</NF_HOLD_HOLDNUMBER>
,
<NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns0:account/ns6:Account/ns6:accountNumber)}</NF_ACCOUN_ACCOUNTNUMBER>
,
<NF_ACCOUN_ACCOUNTIBAN?>{data($parm/ns0:account/ns6:Account/ns6:accountNumber)}</NF_ACCOUN_ACCOUNTIBAN>
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{boolean2SourceValue(data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:reverseOrder),"1","0")}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:navigationKeyDefinition)}</NF_PAGEC_NAVIGATIONKEYDEFI>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns0:bcd/ns3:BusinessControlData/ns3:pageControl/ns4:PageControl/ns4:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
,
<NF_DATE_FROM>1900-01-01</NF_DATE_FROM>
,
<NF_DATE_TO>2049-12-31</NF_DATE_TO>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>