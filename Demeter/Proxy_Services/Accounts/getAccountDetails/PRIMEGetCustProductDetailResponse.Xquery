<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v1.1 Zamiana przecinka na kropkę dla liczbVersion.$2.2010-10-20</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Tomasz Krajewski  
 : @version 1.1
 : @since   2010-10-20
 :
 : $Proxy Services/Accounts/getAccountDetails/PRIMEGetCustProductDetailResponse.xq$
 :
 :)

declare namespace sz = "http://schemas.datacontract.org/2004/07/SzrekLibrary.Struktures";
declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductDetailResponse($bdy as element(m:PRIMEGetCustProductDetailResponse))
	as element(fml:FML32) {
		<fml:FML32>
			<fml:PT_FIRST_PRODUCT_FEATURE/>
			<fml:PT_SECOND_PRODUCT_FEATURE/>
			<fml:PT_THIRD_PRODUCT_FEATURE/>
			<fml:PT_FOURTH_PRODUCT_FEATURE/>
			<fml:PT_FIFTH_PRODUCT_FEATURE/>
			<fml:PT_SOURCE_PRODUCT_CODE/>
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:CI_WLASCICIEL )
					then <fml:NF_TXNAD_OWNERCIF>{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:CI_WLASCICIEL ) }</fml:NF_TXNAD_OWNERCIF>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:B_KOD_WALUTY)
					then <fml:NF_CURREC_CURRENCYCODE>{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:B_KOD_WALUTY) }</fml:NF_CURREC_CURRENCYCODE>
					else ()
			}
			(: v1.1 Zamiana przecinka na kropkę dla liczb :)
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:B_SALDO)
					then
					( 
						<fml:NF_TXNAD_CURRENTBALANCE>{ 
							fn:replace(data($bdy/m:PRIMEGetCustProductDetailResult/sz:B_SALDO), ',', '.') 
						}</fml:NF_TXNAD_CURRENTBALANCE>
					)
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:B_DOST_SRODKI)
					then 
					(
						<fml:NF_TXNAD_AVAILABLEBALANCE>{ 
							fn:replace(data($bdy/m:PRIMEGetCustProductDetailResult/sz:B_DOST_SRODKI), ',', '.') 
						}</fml:NF_TXNAD_AVAILABLEBALANCE>
					)
					else ()
			}
			(:{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:B_BLOKADA)
					then <fml:NF_HOLD_HOLDAMOUNT>{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:B_BLOKADA) }</fml:NF_HOLD_HOLDAMOUNT>
					else ()
			}:)
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:CI_PRODUCT_NAME_ORYGINAL)
					then <fml:NF_TXNAD_ACCOUNTNAME>{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:CI_PRODUCT_NAME_ORYGINAL) }</fml:NF_TXNAD_ACCOUNTNAME>
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:DC_ODDZIAL)
					then <fml:NF_BRANCC_BRANCHCODE>{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:DC_ODDZIAL) }</fml:NF_BRANCC_BRANCHCODE>
					else ()
			}
			<fml:NF_ACCEST_ACCESSTYPE/>
			<fml:NF_TXNAD_SHOWBALANCE>1</fml:NF_TXNAD_SHOWBALANCE>
			<fml:NF_TXNAD_ACCOUNTSPECIFICTY>IS</fml:NF_TXNAD_ACCOUNTSPECIFICTY>
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:CI_WLASCICIEL )
					then <fml:NF_TXNAD_OWNERDATAEDITABLE>0</fml:NF_TXNAD_OWNERDATAEDITABLE>
					else <fml:NF_TXNAD_OWNERDATAEDITABLE>1</fml:NF_TXNAD_OWNERDATAEDITABLE>
			}			
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:NF_PRODUCT_STAT)
					then <fml:NF_ACCOUS_ACCOUNTSTATUS>{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:NF_PRODUCT_STAT) }</fml:NF_ACCOUS_ACCOUNTSTATUS>
					else ()
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ 
 if ($body/m:PRIMEGetCustProductDetailResponse)
    then xf:mapPRIMEGetCustProductDetailResponse($body/m:PRIMEGetCustProductDetailResponse) 
    else  
 if ($body/fml:FML32)
    then $body/fml:FML32
    else  ()
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>