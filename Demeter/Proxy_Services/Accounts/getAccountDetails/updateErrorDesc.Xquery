<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Aktualizacja tekstu w polu errorDescription.Version.$1.2011-05-16</con:description>
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author	Tomasz Krajewski  
 : @version	1.0
 : @since	2011-03-03
 :
 : $Proxy Services/Accounts/getAccountDetails/updateErrorDesc.xq$
 :
 :)

declare namespace xf = "http://tempuri.org/Accounts/getAccountDetails/updateErrorDesc/";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:UpdateErrorDesc($element as element(), $textBefore as xs:string?) {
	element {node-name($element)}
	{$element/@*,
		for $child in $element/node()
		return
		(
			if ($child instance of element())
			then xf:UpdateErrorDesc($child, $textBefore)
			else 
			(
				if (fn:local-name($element) eq 'errorDescription')
				then 
				(
					let $NewErrorDesc := fn:concat($textBefore, ': ', data($element))
					return
						$NewErrorDesc
				)
				else $child
			)
		)
	}
};

declare variable $textBefore as xs:string external;
declare variable $body as element(soapenv:Body) external;

xf:UpdateErrorDesc($body, $textBefore)</con:xquery>
</con:xqueryEntry>