<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Tomasz Krajewski  
 : @version 1.0
 : @since   2010-06-24
 :
 : $Proxy Services/Accounts/getAccountDetails/PRIMEGetCustProductDetailRequest.xq$
 :
 :)

declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductDetail($fml as element(fml:FML32))
	as element(m:PRIMEGetCustProductDetail) {
		<m:PRIMEGetCustProductDetail>
			{
				if($fml/fml:NF_ACCOUN_ACCOUNTNUMBER)
					then <m:DC_NR_RACHUNKU>{ data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER) }</m:DC_NR_RACHUNKU>
                                       else ()
			}
			<m:CI_OPCJA>1</m:CI_OPCJA>
		</m:PRIMEGetCustProductDetail>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapPRIMEGetCustProductDetail($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>