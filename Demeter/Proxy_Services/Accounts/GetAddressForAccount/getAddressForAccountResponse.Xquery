<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-10</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  <ns0:response>
    <ns2:ResponseMessage>
      <ns2:result>{data($parm/NF_RESPOM_RESULT)}</ns2:result>
      <ns2:errorCode>{data($parm/NF_RESPOM_ERRORCODE)}</ns2:errorCode>
      <ns2:errorDescription>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}</ns2:errorDescription>
    </ns2:ResponseMessage>
  </ns0:response>
  <ns0:accountAddress>
    <ns3:AccountAddress>
      <ns3:name1>{data($parm/NF_ACCOUA_NAME1)}</ns3:name1>
      <ns3:name2>{data($parm/NF_ACCOUA_NAME2)}</ns3:name2>
      <ns3:street>{data($parm/NF_ACCOUA_STREET)}</ns3:street>
      <ns3:houseFlatNumber>{data($parm/NF_ACCOUA_HOUSEFLATNUMBER)}</ns3:houseFlatNumber>
      <ns3:city>{data($parm/NF_ACCOUA_CITY)}</ns3:city>
      <ns3:stateCountry>{data($parm/NF_ACCOUA_STATECOUNTRY)}</ns3:stateCountry>
      <ns3:zipCode>{data($parm/NF_ACCOUA_ZIPCODE)}</ns3:zipCode>
      <ns3:accountAddressType>{data($parm/NF_ACCOUA_ACCOUNTADDRESSTY)}</ns3:accountAddressType>
      { insertDate(data($parm/NF_ACCOUA_VALIDFROM),"yyyy-MM-dd","ns3:validFrom")}
      { insertDate(data($parm/NF_ACCOUA_VALIDTO),"yyyy-MM-dd","ns3:validTo")}
	  {
      if (data($parm/NF_ACCOUA_DELETEWHENEXPIRE) = 1)
           then <ns3:deleteWhenExpired>true</ns3:deleteWhenExpired>
           else <ns3:deleteWhenExpired>false</ns3:deleteWhenExpired>
      }
	  (: <ns3:deleteWhenExpired>{data($parm/NF_ACCOUA_DELETEWHENEXPIRE)}</ns3:deleteWhenExpired> :)
    </ns3:AccountAddress>
  </ns0:accountAddress>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>