<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$header1" element="ns8:header" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns8:invoke" ::)
(:: pragma bea:global-element-return element="ns4:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:productstree.entities.be.dcl";
declare namespace ns4 = "";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns0 = "urn:accounts.entities.be.dcl";
declare namespace ns9 = "urn:baseauxentities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/getSimulationForTranAccount/getSimulationForTranAccount/";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:be.services.dcl";

declare function xf:getSimulationForTranAccount($header1 as element(ns8:header),
    $invoke1 as element(ns8:invoke))
    as element(ns4:FML32) {
        <ns4:FML32>
            <ns4:TR_ID_OPER?>{ data($header1/ns8:transHeader/ns8:transId) }</ns4:TR_ID_OPER>
            <ns4:TR_DATA_OPER?>
                {
                    let $transactionDate  := ($invoke1/ns8:transaction/ns7:Transaction/ns7:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			</ns4:TR_DATA_OPER>
            
            <ns4:TR_ODDZ_KASY?>{ 
            xs:short( data($invoke1/ns8:branchCode/ns2:BranchCode/ns2:branchCode) ) 
            }</ns4:TR_ODDZ_KASY>
            
            <ns4:TR_KASA?>{ 
            xs:short( data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID) ) 
            }</ns4:TR_KASA>
            
            <ns4:TR_KASJER?>{ 
            xs:short( data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID) ) 
            }</ns4:TR_KASJER>
            
            <ns4:TR_UZYTKOWNIK?>{ 
            fn:concat("SKP:",data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:user/ns5:User/ns5:userID))
            }</ns4:TR_UZYTKOWNIK>
            
            <ns4:TR_MSG_ID?>{ 
            data($header1/ns8:msgHeader/ns8:msgId) 
            }</ns4:TR_MSG_ID>
            
            <ns4:TR_FLAGA_AKCEPT>
            O
            </ns4:TR_FLAGA_AKCEPT>
            
            <ns4:TR_TYP_KOM?>{ 
            xs:short( data($invoke1/ns8:transaction/ns7:Transaction/ns7:csrMessageType/ns6:CsrMessageType/ns6:csrMessageType) ) 
            }</ns4:TR_TYP_KOM>
            
            <ns4:TR_KANAL>
            0
            </ns4:TR_KANAL>
            
            <ns4:TR_RACH_NAD?>{ 
            data($invoke1/ns8:account/ns0:Account/ns0:accountNumber) 
            }</ns4:TR_RACH_NAD>
            <ns4:TR_DATA_PRACY>
                {
                    let $transactionDate  := ($invoke1/ns8:transaction/ns7:Transaction/ns7:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate ), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			</ns4:TR_DATA_PRACY>
            <ns4:TR_DATA_WALUTY?>
                {
                    let $transactionDate  := ($invoke1/ns8:transaction/ns7:Transaction/ns7:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate ), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			</ns4:TR_DATA_WALUTY>
            <ns4:TR_CZAS_OPER?>
                {
                    let $transactionTime  := ($header1/ns8:msgHeader/ns8:timestamp)  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
			</ns4:TR_CZAS_OPER>
            <ns4:TR_AKCEPTANT_SKP?>{
             data($invoke1/ns8:userTxnSession/ns3:UserTxnSession/ns3:user/ns5:User/ns5:userID) 
             }</ns4:TR_AKCEPTANT_SKP>
             
            <ns4:TR_UZYTKOWNIK_SKP?>{ 
            data($header1/ns8:msgHeader/ns8:userId) 
            }</ns4:TR_UZYTKOWNIK_SKP>
            
        </ns4:FML32>
};

declare variable $header1 as element(ns8:header) external;
declare variable $invoke1 as element(ns8:invoke) external;

<soap-env:Body>{
xf:getSimulationForTranAccount($header1,
    $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>