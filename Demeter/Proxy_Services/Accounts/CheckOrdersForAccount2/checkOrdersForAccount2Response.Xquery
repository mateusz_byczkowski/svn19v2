<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-06</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:filtersandmessages.entities.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function intToBoolean($parm as xs:string) as xs:string{
   if ($parm = "1") 
      then  "true"
   else
      "false"
};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  <ns0:response>
    <ns2:ResponseMessage>
      <ns2:result>{intToBoolean(data($parm/NF_RESPOM_RESULT))}</ns2:result>
      <ns2:errorCode?>{data($parm/NF_RESPOM_ERRORCODE)}</ns2:errorCode>
      <ns2:errorDescription?>{data($parm/NF_RESPOM_ERRORDESCRIPTION)}</ns2:errorDescription>
    </ns2:ResponseMessage>
  </ns0:response>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>