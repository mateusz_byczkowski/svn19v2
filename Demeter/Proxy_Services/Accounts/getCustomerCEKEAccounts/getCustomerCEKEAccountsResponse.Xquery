<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-13</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:ceke.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:accounts.entities.be.dcl";
declare namespace urn4 = "urn:dictionaresrepo.dictionaries.be.dcl";

declare function xf:mapgetCustomerCEKEAccountsResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:accounts>
				{
					let $B_SYS := $fml/fml:B_SYS
					let $B_TYP_RACH := $fml/fml:B_TYP_RACH
					let $B_DL_NR_RACH := $fml/fml:B_DL_NR_RACH
					let $E_ACCOUNT_TYPE_NAME := $fml/fml:E_ACCOUNT_TYPE_NAME
					let $B_SKROT_OPISU := $fml/fml:B_SKROT_OPISU
					let $B_KOD_RACH := $fml/fml:B_KOD_RACH (: not used :)
					let $B_WALUTA := $fml/fml:B_WALUTA
					let $B_KOD_WALUTY := $fml/fml:B_KOD_WALUTY
					let $B_MIKROODDZIAL := $fml/fml:B_MIKROODDZIAL (: not used :)
					let $E_FEE_ALLOWED := $fml/fml:E_FEE_ALLOWED
					let $E_REGISTER_ACCOUNT := $fml/fml:E_REGISTER_ACCOUNT
					let $E_DR_CR_ACCOUNT := $fml/fml:E_DR_CR_ACCOUNT
					let $E_SEQ_NO := $fml/fml:E_SEQ_NO
					let $E_FEE_ENABLED := $fml/fml:E_FEE_ENABLED

					for $it at $p in $fml/fml:B_DL_NR_RACH
					return
					(
						<urn1:OwnAccountCEKE>
							{
								<urn1:cekeAccountType?>{data($B_TYP_RACH[$p])}</urn1:cekeAccountType>
							}
							{
								if($fml/fml:E_REGISTER_ACCOUNT[$p])
								then (

										if (data($fml/fml:E_REGISTER_ACCOUNT[$p]) = "1") then
										(
											<urn1:registerAccount>true</urn1:registerAccount>
										) else if (data($fml/fml:E_REGISTER_ACCOUNT[$p]) = "0") then
										(
											<urn1:registerAccount>false</urn1:registerAccount>
										) else()

								)
								else ()
							}
							{
								if($fml/fml:E_DR_CR_ACCOUNT[$p])
									then (

											if (data($fml/fml:E_DR_CR_ACCOUNT[$p]) = "1") then
											(
												<urn1:debitCreditAccount>true</urn1:debitCreditAccount>
											) else if (data($fml/fml:E_DR_CR_ACCOUNT[$p]) = "0") then
											(
												<urn1:debitCreditAccount>false</urn1:debitCreditAccount>
											) else()

									)
									else ()
							}
							{
								if($E_FEE_ALLOWED[$p])
									then (

											if (data($E_FEE_ALLOWED[$p]) = "1") then
											(
												<urn1:feeAllowedAccount>true</urn1:feeAllowedAccount>
											) else if (data($E_FEE_ALLOWED[$p]) = "0") then
											(
												<urn1:feeAllowedAccount>false</urn1:feeAllowedAccount>
											) else()

									)
								else ()
							}
							
							{
								if ($E_SEQ_NO[$p]) then
								(
								if (data($E_SEQ_NO[$p]) != "0") then
								(
									<urn1:seqNum>{ data($E_SEQ_NO[$p]) }</urn1:seqNum>
								)
								else
								(
									<urn1:seqNum></urn1:seqNum>
								)
								)
								else()	
							}
							
							
							
							{
								if($E_FEE_ENABLED[$p])
									then (
										if (data($E_FEE_ENABLED[$p]) = "1") then
										(
											<urn1:flagFeeAccount>true</urn1:flagFeeAccount>
										) else if (data($E_FEE_ENABLED[$p]) = "0") then
										(
											<urn1:flagFeeAccount>false</urn1:flagFeeAccount>
										) else ()
									)
									else ()
							}
							{
								if($B_SYS[$p])
									then
									(
										<urn1:sourceSystem>
											<urn2:System>
												{
													if (data($B_SYS[$p] = "1")) then
													(
														<urn2:system>5</urn2:system>
													) else if (data($B_SYS[$p] = "3")) then
													(
														<urn2:system>1</urn2:system>
													) else if (data($B_SYS[$p] = "4")) then
													(
														<urn2:system>7</urn2:system>
													) else if (data($B_SYS[$p] = "5")) then
													(
														<urn2:system>9</urn2:system>
													) else
													(
														<urn2:system>{ data($B_SYS[$p]) }</urn2:system>
													)
												}
											</urn2:System>
										</urn1:sourceSystem>
									)
								else ()
							}
							{
								<urn1:account>
									<urn3:Account>
										{
											<urn3:accountNumber?>{ data($B_DL_NR_RACH[$p]) }</urn3:accountNumber>
										}
										{
											<urn3:accountName?>{ data($E_ACCOUNT_TYPE_NAME[$p]) }</urn3:accountName>
										}
										{
											<urn3:accountDescription?>{ data($B_SKROT_OPISU[$p]) }</urn3:accountDescription>
										}
										{
											if ($B_WALUTA[$p]) then
											(
												<urn3:currency>
													<urn2:CurrencyCode>
														<urn2:currencyCode>{ data($B_KOD_WALUTY[$p]) }</urn2:currencyCode>
													</urn2:CurrencyCode>
												</urn3:currency>
											) else ()
										}
										{
											if ($B_MIKROODDZIAL[$p]) then 
											(
											<urn3:accountBranchNumber>
												<urn2:BranchCode>
													<urn2:branchCode> { data($B_MIKROODDZIAL[$p]) }</urn2:branchCode>
												</urn2:BranchCode>
											</urn3:accountBranchNumber>
											) else()
										}
										(: nothing to insert
										{
											<urn3:accountStatus?>
												<urn4:AccountStatus?>
													<urn4:accountStatus?>{ data() }</urn4:accountStatus>
												</urn4:AccountStatus>
											</urn3:accountStatus>
										}
										:)
									</urn3:Account>
								</urn1:account>
							}
						</urn1:OwnAccountCEKE>
					)
				}

			</urn:accounts>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomerCEKEAccountsResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>