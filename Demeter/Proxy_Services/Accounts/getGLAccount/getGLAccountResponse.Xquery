<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$fML321" element="ns1:FML32" location="getGLAccountResponse.xsd" ::)
(:: pragma bea:global-element-return element="ns3:invokeResponse" location="getGLAccount.WSDL" ::)

declare namespace xf = "http://tempuri.org/Accounts/getGLAccount/getGLAccountResponse/";
declare namespace ns0 = "urn:accounts.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:getGLAccountResponse($fML321 as element(ns1:FML32))
    as element(ns3:invokeResponse) {
        <ns3:invokeResponse>
            <ns3:glAcountOut>
                <ns0:GLAccount?>
                    <ns0:description?>{ data($fML321/ns1:NF_GLA_DESCRIPTION) }</ns0:description>
                    <ns0:accountNumberNRB?>{ data($fML321/ns1:NF_ACCOUN_ACCOUNTIBAN) }</ns0:accountNumberNRB>
                    <ns0:currencyCode?>
                        <ns2:CurrencyCode?>
                            <ns2:currencyCode?>{ data($fML321/ns1:NF_CURREC_CURRENCYCODE) }</ns2:currencyCode>
                        </ns2:CurrencyCode>
                    </ns0:currencyCode>
                </ns0:GLAccount>
            </ns3:glAcountOut>
        </ns3:invokeResponse>
};

declare variable $fML321 as element(ns1:FML32) external;

<soap-env:Body>
{ xf:getGLAccountResponse($fML321) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>