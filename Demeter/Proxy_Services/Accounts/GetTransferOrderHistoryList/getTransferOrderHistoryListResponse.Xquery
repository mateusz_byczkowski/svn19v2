<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:transferorder.entities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns4="urn:baseauxentities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDateTime($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };


declare function getElementsForHistory($parm as element(fml:FML32)) as element()
{

<ns0:history>
  {
    for $x at $occ in $parm/NF_TRAOMH_OPERATION
    return
    <ns2:TransferOrderModHist>
      <ns2:modificationID?>{data($parm/NF_TRAOMH_MODIFICATIONID[$occ])}</ns2:modificationID>
      <ns2:operation?>{data($parm/NF_TRAOMH_OPERATION[$occ])}</ns2:operation>
      <ns2:userID?>{data($parm/NF_TRAOMH_USERID[$occ])}</ns2:userID>
      { insertDateTime(data($parm/NF_TRAOMH_TIME[$occ]),"yyyy-MM-dd HH:mm","ns2:time")}
       <ns2:description?>{data($parm/NF_TRAOMH_DESCRIPTION[$occ])}</ns2:description>
    </ns2:TransferOrderModHist>
  }
</ns0:history>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForHistory($parm)}
  <ns0:bcd>
    <ns3:BusinessControlData>
      <ns3:pageControl>
        <ns4:PageControl>
          <ns4:hasNext?>{data($parm/NF_PAGEC_HASNEXT)}</ns4:hasNext>
          <ns4:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns4:navigationKeyDefinition>
          <ns4:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns4:navigationKeyValue>
        </ns4:PageControl>
      </ns3:pageControl>
    </ns3:BusinessControlData>
  </ns0:bcd>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>