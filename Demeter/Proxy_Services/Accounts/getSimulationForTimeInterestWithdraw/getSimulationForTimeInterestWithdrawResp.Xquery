<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2010-02-01
 :
 : wersja WSDLa: 03-12-2009 17:14:48
 :
 : $Proxy Services/Account/getSimulationForTimeInterestWithdraw/getSimulationForTimeInterestWithdrawRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getSimulationForTimeInterestWithdraw/getSimulationForTimeInterestWithdrawResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getSimulationForTimeInterestWithdrawResponse($fML321 as element(ns1:FML32))
    as element(ns2:invokeResponse)
{
    <ns2:invokeResponse>
        <ns2:simulationForTimeAccount>
            <ns0:SimulationForTimeAccount?>
            
                <ns0:balance?>{
					data($fML321/ns1:NF_SIMFTA_BALANCE)
				}</ns0:balance>
				
                <ns0:interestForPay?>{
					data($fML321/ns1:NF_SIMFTA_INTERESTFORPAY)
				}</ns0:interestForPay>
				
                <ns0:interestCapitalized?>{
					data($fML321/ns1:NF_SIMFTA_INTERESTCAPITALI)
				}</ns0:interestCapitalized>
				
                <ns0:taxPayed?>{
					data($fML321/ns1:NF_SIMFTA_TAXPAYED)
				}</ns0:taxPayed>
				
            </ns0:SimulationForTimeAccount>
        </ns2:simulationForTimeAccount>
    </ns2:invokeResponse>
};

<soap-env:Body>{
	xf:getSimulationForTimeInterestWithdrawResponse($fML321)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>