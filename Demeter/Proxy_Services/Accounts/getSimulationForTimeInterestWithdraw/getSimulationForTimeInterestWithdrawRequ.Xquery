<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-23</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2010-02-01
 :
 : wersja WSDLa: 03-12-2009 17:14:48
 :
 : $Proxy Services/Account/getSimulationForTimeInterestWithdraw/getSimulationForTimeInterestWithdrawRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getSimulationForTimeInterestWithdraw/getSimulationForTimeInterestWithdrawRequest/";
declare namespace ns0 = "urn:accounts.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:productstree.entities.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns4 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns5 = "urn:entities.be.dcl";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:be.services.dcl";

declare variable $header1 as element(ns8:header) external;
declare variable $invoke1 as element(ns8:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:getSimulationForTimeInterestWithdrawRequest($header1 as element(ns8:header),
																  $invoke1 as element(ns8:invoke))
    as element(ns1:FML32)
{
   <ns1:FML32>
		<ns1:NF_MSHEAD_MSGID?>{
			data($header1/ns8:msgHeader/ns8:msgId)
		}</ns1:NF_MSHEAD_MSGID>

		<ns1:NF_TRANSA_TRANSACTIONDATE?>{
			data($invoke1/ns8:transaction/ns7:Transaction/ns7:transactionDate)
		}</ns1:NF_TRANSA_TRANSACTIONDATE>

		<ns1:NF_ACCOUN_ACCOUNTNUMBER?>{
			data($invoke1/ns8:account/ns0:Account/ns0:accountNumber)
		}</ns1:NF_ACCOUN_ACCOUNTNUMBER>

   </ns1:FML32>
};

<soap-env:Body>{
	xf:getSimulationForTimeInterestWithdrawRequest($header1, $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>