<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2010-09-23</con:description>
  <con:xquery><![CDATA[(:: pragma bea:global-element-parameter parameter="$header1" element="ns9:header" location="getSimulationForTimeInterestWithdraw.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns9:invoke" location="getSimulationForTimeInterestWithdraw.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns9:invokeResponse" location="getSimulationForTimeInterestWithdraw.wsdl" ::)
(:: pragma bea:global-element-return element="ns0:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns2 = "urn:accounts.entities.be.dcl";
declare namespace ns1 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:productstree.entities.be.dcl";
declare namespace ns0 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns9 = "urn:be.services.dcl";
declare namespace ns5 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace xf = "http://tempuri.org/Accounts/getSimulationForTimeInterestWithdraw/getSimulationForTimeInterestWithdraw/";
declare namespace ns6 = "urn:entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace err = "urn:errors.hlbsentities.be.dcl";

declare function xf:getSimulationForTimeInterestWithdraw(
    $header1 as element(ns9:header),
    $invoke1 as element(ns9:invoke),
    $faultResponse as element() ?)

    as element(ns0:transactionLogEntry) {
        <ns0:transactionLogEntry>
            {
                for $businessTransactionType in $invoke1/ns9:transaction/ns8:Transaction/ns8:businessTransactionType/ns7:BusinessTransactionType/ns7:businessTransactionType
                return
                    <businessTransactionType?>{ data($businessTransactionType) }</businessTransactionType>
            }
            {
                for $csrMessageType in $invoke1/ns9:transaction/ns8:Transaction/ns8:csrMessageType/ns7:CsrMessageType/ns7:csrMessageType
                return
                    <csrMessageType?>{ data($csrMessageType) }</csrMessageType>
            }
            {
                for $dtTransactionType in $invoke1/ns9:transaction/ns8:Transaction/ns8:dtTransactionType/ns7:DtTransactionType/ns7:dtTransactionType
                return
                    <dtTransactionType?>{ data($dtTransactionType) }</dtTransactionType>
            }
            <executor>
                {
                    for $branchCode in $invoke1/ns9:branchCode/ns4:BranchCode/ns4:branchCode
                    return
                        <branchNumber?>{ xs:int( data($branchCode) ) }</branchNumber>
                }
                <executorID?>{ data($header1/ns9:msgHeader/ns9:userId) }</executorID>
                {
                    for $userFirstName in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:user/ns6:User/ns6:userFirstName
                    return
                        <firstName?>{ data($userFirstName) }</firstName>
                }
                {
                    for $userLastName in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:user/ns6:User/ns6:userLastName
                    return
                        <lastName?>{ data($userLastName) }</lastName>
                }
                {
                    for $tellerID in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:teller/ns5:Teller/ns5:tellerID
                    return
                        <tellerID?>{ data($tellerID) }</tellerID>
                }
                {
                    for $tillID in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:till/ns5:Till/ns5:tillID
                    return
                        <tillNumber?>{ data($tillID) }</tillNumber>
                }


            </executor>
            {
                for $extendedCSRMessageType in $invoke1/ns9:transaction/ns8:Transaction/ns8:extendedCSRMessageType/ns7:ExtendedCSRMessageType/ns7:extendedCSRMessageType
                return
                    <extendedCSRMessageType?>{ data($extendedCSRMessageType) }</extendedCSRMessageType>
            }
            <hlbsName>getSimulationForTimeInterestWithdraw</hlbsName>
            {
                for $orderedBy in $invoke1/ns9:transaction/ns8:Transaction/ns8:orderedBy
                return
                    <orderedBy?>{ data($orderedBy) }</orderedBy>
            }
            {
                for $putDownDate in $invoke1/ns9:transaction/ns8:Transaction/ns8:putDownDate
                return
                    <putDownDate?>{ xs:date( data($putDownDate)) }</putDownDate>
            }
            <timestamp>{ data($header1/ns9:msgHeader/ns9:timestamp) }</timestamp>
            {
                let $Account := $invoke1/ns9:account/ns2:Account
                return
                    <tlAccountList>
                        {
                            for $accountNumber in $Account/ns2:accountNumber
                            return
                                <accountNumber?>{ data($accountNumber) }</accountNumber>
                        }
                        {
                            for $idProductDefinition in $Account/ns2:productDefinition/ns3:ProductDefinition/ns3:idProductDefinition
                            return
                                <productId?>{ data($idProductDefinition) }</productId>
                        }
                    </tlAccountList>
            }
            {
                for $transactionDate in $invoke1/ns9:transaction/ns8:Transaction/ns8:transactionDate
                return
                    <transactionDate?>{ xs:date( data($transactionDate)) }</transactionDate>
            }
            <tlExceptionDataList?>
            {
            	let $errorCode1 := data($faultResponse//err:exceptionItem/err:errorCode1)
            	let $errorCode2 := data($faultResponse//err:exceptionItem/err:errorCode2)
                let $errorDescription := data($faultResponse//err:exceptionItem/err:errorDescription)
            	return
                   if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') != '') then
            		(
		                <errorCode1?>{ xs:int($errorCode1) }</errorCode1>,
        		        <errorCode2?>{ xs:int($errorCode2) }</errorCode2>,
                		<errorDescription?>{ $errorDescription }</errorDescription>
                	)
                	else
                	()
			}
            </tlExceptionDataList>
            
            
            {
                for $transactionGroupID in $invoke1/ns9:transaction/ns8:Transaction/ns8:transactionGroupID
                return
                    <transactionGroupID?>{ data($transactionGroupID) }</transactionGroupID>
            }
            <transactionID?>{ data($header1/ns9:transHeader/ns9:transId) }</transactionID>
                      {
           		if (fn:replace(data($faultResponse//err:exceptionItem/err:errorDescription),' ','') = '') then
            (
                    <transactionStatus?>{ "P" }</transactionStatus>
                	)
                	else
                	()
            }
            
 




        </ns0:transactionLogEntry>
};

declare variable $header1 as element(ns9:header) external;
declare variable $invoke1 as element(ns9:invoke) external;
declare variable $faultResponse  as element() ? external;

<soap-env:Body>{
xf:getSimulationForTimeInterestWithdraw(
    $header1,
    $invoke1,
    $faultResponse)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>