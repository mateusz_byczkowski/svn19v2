<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-15</con:description>
  <con:xquery><![CDATA[declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace soapenc="http://schemas.xmlsoap.org/soap/encoding/";
declare namespace crw="http://bzwbk.com/crw";
declare namespace ser="http://bzwbk.com/dc/application/status/services";

declare variable $body external;


declare function local:error() as element() {
         <error>Error translating request message</error>
};

declare function local:nodeE() as element() {
         <type>E_MORTGAGE</type>
};

declare function local:nodeE1() as element() {
         <type>E1_PRELIMINARY_MORTGAGE</type>
};

<ser:updateApplicationStatus>   
       <arg0>
          <applicationInfo>
            <number>{data($body/crw:notification/crw:number)}</number>
              {
                  if ($body/crw:notification/crw:entityType="4500") then
                       local:nodeE()
                  else if ($body/crw:notification/crw:entityType="4555") then
                       local:nodeE1()
                  else local:error()
               }
            </applicationInfo>
           <skp>{data($body/crw:notification/crw:variables/crw:entry[crw:key='SKP']/crw:value)}</skp>
           <status>{data($body/crw:notification/crw:variables/crw:entry[crw:key='STATUS']/crw:value)}</status>
           <udpateStatus>false</udpateStatus>
        </arg0>
</ser:updateApplicationStatus>]]></con:xquery>
</con:xqueryEntry>