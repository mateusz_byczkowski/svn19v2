<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-13</con:description>
  <con:xquery><![CDATA[declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/payments/faults/";
declare variable $fault external;

declare function local:fault($faultString as xs:string, $detail as element()*) as element(soap-env:Fault) {
		<soap-env:Fault>
			<faultcode>soapenv:Server.userException</faultcode> 
			<faultstring>{ $faultString }</faultstring> 
			<detail>{ $detail }</detail>
		</soap-env:Fault>
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string) as element()* {
	<errorCode1>{ $errorCode1 }</errorCode1>,
	<errorCode2>{ $errorCode2 }</errorCode2>,
	<errorDescription/>
};

<soap-env:Body>
{
	(: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
	let $reason := fn:substring-before(fn:substring-after(fn:substring-before($fault/ctx:reason, ":"), "("), ")")
	let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($fault/ctx:reason, ":"), ":"), ":")
	return
		if($reason = "13") then
			local:fault("com.bzwbk.services.payments.faults.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode) })
		else if($reason = "11") then
			if($urcode = "101") then
				local:fault("com.bzwbk.services.payments.faults.ErrBufferFillException", element f:ErrBufferFillException { local:errors($reason, $urcode) })
			else if($urcode = "102") then
				local:fault("com.bzwbk.services.payments.faults.CbdErrGetInputException", element f:CbdErrGetInputException { local:errors($reason, $urcode) })
			else if($urcode = "103") then
				local:fault("com.bzwbk.services.payments.faults.ErrNoDataException", element f:ErrNoDataException { local:errors($reason, $urcode) })
			else
				local:fault("com.bzwbk.services.payments.faults.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode) })
		else
			local:fault("com.bzwbk.services.payments.faults.ServiceException", element f:ServiceException { local:errors($reason, $urcode) })
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>