<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-13</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:operations.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:swift.operations.entities.be.dcl";
declare namespace urn4 =  "urn:transactionswiftdict.operationsdictionary.dictionaries.be.dcl";
declare namespace urn5 = "urn:cif.entities.be.dcl";
declare namespace urn6 = "urn:productstree.entities.be.dcl";



declare function xf:mapGetSLINKProdRequest($req as element(urn:invoke),$head as element(urn:header))
	as element(fml:FML32) {
	<fml:FML32>   
        {
        <fml:S_SYSTEM_ID?>{ data($head/urn:msgHeader/urn:appId) }</fml:S_SYSTEM_ID>
        }
        {
	<fml:S_SRC_ACCOUNT_NO?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionWn/urn1:TransactionWn/urn1:accountNumber)}</fml:S_SRC_ACCOUNT_NO>
	}
	{
	<fml:S_SRC_ACCOUNT_CUR?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionWn/urn1:TransactionWn/urn1:currencyCode/urn2:CurrencyCode/urn2:currencyCode)}</fml:S_SRC_ACCOUNT_CUR> 
	}
	{
	<fml:S_DST_ACCOUNT_NO?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionMa/urn1:TransactionMa/urn1:accountNumber)}</fml:S_DST_ACCOUNT_NO>
	}
	{
	<fml:S_DST_AMOUNT?>{ data($req/urn:transaction/urn1:Transaction/urn1:transactionMa/urn1:TransactionMa/urn1:amountMa)}</fml:S_DST_AMOUNT>
	}
        {
	<fml:S_DST_ACCOUNT_CUR?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionMa/urn1:TransactionMa/urn1:currencyCode/urn2:CurrencyCode/urn2:currencyCode)}</fml:S_DST_ACCOUNT_CUR>
	}
	{
	<fml:S_PRODUCT_ID?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionSwift/urn3:TransactionSwift/urn3:slinkProductNumber)}</fml:S_PRODUCT_ID>
	}
	{
	<fml:S_SWIFT_COST?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionSwift/urn3:TransactionSwift/urn3:costOption/urn4:SwiftCostOption/urn4:swiftCostOption)}</fml:S_SWIFT_COST>
	}
	{
	<fml:S_PAYMENT_TYPE?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionSwift/urn3:TransactionSwift/urn3:executionMode/urn4:SwiftExecutionMode/urn4:swiftExecutionMode)}</fml:S_PAYMENT_TYPE>
	}
	{
	<fml:S_SWIFT_ID?>{data($req/urn:transaction/urn1:Transaction/urn1:transactionSwift/urn3:TransactionSwift/urn3:swiftBenefBank/urn3:SwiftBenefBank/urn3:branchBankId)}</fml:S_SWIFT_ID>
	}
	{
	<fml:S_FEE_ACCOUNT_NO?>{data($req/urn:chargedAccount/urn1:TxnAccountData/urn1:accountNumber)}</fml:S_FEE_ACCOUNT_NO>
	}
	{	
	<fml:S_FEE_ACCOUNT_CUR?>{data($req/urn:chargedAccount/urn1:TxnAccountData/urn1:currency/urn2:CurrencyCode/urn2:currencyCode)}</fml:S_FEE_ACCOUNT_CUR>
	}
	{
	<fml:S_CUSTOMER_CIF?> {data($req/urn:customer/urn5:Customer/urn5:customerNumber)}</fml:S_CUSTOMER_CIF>
	}
        {
         if (data($req/urn:customer/urn5:Customer/urn5:resident)="TRUE") then    
        	<fml:S_CUSTOMER_STATUS?>1</fml:S_CUSTOMER_STATUS>
         else  if (data($req/urn:customer/urn5:Customer/urn5:resident)="true") then    
        	         <fml:S_CUSTOMER_STATUS?>1</fml:S_CUSTOMER_STATUS>
               else   if (data($req/urn:customer/urn5:Customer/urn5:resident)="FALSE") then    
                        	<fml:S_CUSTOMER_STATUS?>0</fml:S_CUSTOMER_STATUS>
                       else if (data($req/urn:customer/urn5:Customer/urn5:resident)="false") then    
                                	<fml:S_CUSTOMER_STATUS?>0</fml:S_CUSTOMER_STATUS>
                             else (<fml:S_CUSTOMER_STATUS?>{data($req/urn:customer/urn5:Customer/urn5:resident)}</fml:S_CUSTOMER_STATUS>)
	}
	{
	<fml:S_CUSTOMER_TYPE?>{data($req/urn:customer/urn5:Customer/urn5:customerType/urn2:CustomerType/urn2:customerType)}</fml:S_CUSTOMER_TYPE>
	}
	{
	<fml:S_SRC_ACCOUNT_PRODUCT_ID?>{data($req/urn:productId/urn6:ProductDefinition/urn6:idProductDefinition)}</fml:S_SRC_ACCOUNT_PRODUCT_ID>
	}     

</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
<soap-env:Body>
{ xf:mapGetSLINKProdRequest($body/urn:invoke,$header/urn:header) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>