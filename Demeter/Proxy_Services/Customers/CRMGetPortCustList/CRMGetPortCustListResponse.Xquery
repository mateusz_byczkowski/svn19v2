<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-09</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Tomasz Krajewski  
 : @version 1.0
 : @since   2010-10-18
 :
 : wersja WSDLa: 2010-10-18
 :
 : $Proxy Services/Customers/CRMGetPortCustList/CRMGetPortCustListResponse.xq$
 :
 :)
 
declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPortCustListResponse($fml as element(fml:FML32))
	as element(m:CRMGetPortCustListResponse) {
		<m:CRMGetPortCustListResponse>
			{
				for $cif at $occ in $fml/fml:DC_NUMER_KLIENTA
				return
					<m:Cif>{ data($cif) }</m:Cif>
			}
		</m:CRMGetPortCustListResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ 
	xf:mapCRMGetPortCustListResponse($body/fml:FML32) 
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>