<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-02-10</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:card.entities.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns7="urn:dictionaries.be.dcl";
declare namespace ns8="urn:insurance.entities.be.dcl";
declare namespace ns9="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml="";
declare namespace ns10="urn:errors.hlbsentities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;



declare function boolean2SourceValue ($parm as xs:string*,$trueval as xs:string,$falseval as xs:string) as xs:string* {
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{
<NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}</NF_MSHEAD_COMPANYID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

<NF_PRODUA_CODEPRODUCTAREA?>{data($parm/ns0:productArea/ns1:ProductArea/ns1:codeProductArea)}</NF_PRODUA_CODEPRODUCTAREA>
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{boolean2SourceValue (data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:reverseOrder),"1","0")}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns0:bcd/ns5:BusinessControlData/ns5:pageControl/ns6:PageControl/ns6:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
,
<NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns0:customer/ns4:Customer/ns4:customerNumber)}</NF_CUSTOM_CUSTOMERNUMBER>
,
<NF_CTRL_ACTIVENONACTIVE>2</NF_CTRL_ACTIVENONACTIVE>
,
<NF_ACCOUN_ALTERNATIVEADDRE>0</NF_ACCOUN_ALTERNATIVEADDRE>
,
<NF_CTRL_OPTION>0</NF_CTRL_OPTION>
,
<NF_CTRL_SYSTEMS>001</NF_CTRL_SYSTEMS>
,
<NF_CTRL_SHOWCURRBAL?>{data($parm/ns0:showCurrentBalance/ns6:IntegerHolder/ns6:value)}</NF_CTRL_SHOWCURRBAL>
,
<NF_PRODUG_VISIBILITYDCL>1</NF_PRODUG_VISIBILITYDCL>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>