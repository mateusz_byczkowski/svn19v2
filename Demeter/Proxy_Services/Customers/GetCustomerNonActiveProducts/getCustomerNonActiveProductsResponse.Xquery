<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1.1  2009-06-03  PKL  TEET 37683
v.1.1.2  2009-06-10  PKL  TEET 37994
v.1.1.3  2009-06-17  KADA CR61
v.1.1.4  2009-11-04  LKAB  PT58Version.$1.2011-02-10</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="http://www.w3.org/2001/XMLSchema";
declare namespace ns3="urn:card.entities.be.dcl";
declare namespace ns4="urn:cif.entities.be.dcl";
declare namespace ns7="urn:dictionaries.be.dcl";
declare namespace ns8="urn:insurance.entities.be.dcl";
declare namespace ns9="urn:accounts.entities.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns5="urn:entities.be.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml="";
declare namespace ns10="urn:errors.hlbsentities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean($parm as xs:string*,$trueval as xs:string) as xs:string* {
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDate($value as xs:string*,$dateFormat as xs:string,$fieldName as xs:string) as element()* {
    if ($value)
      then if(string-length($value)>5)
          then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
      else() 
    else()
};

(:
declare function getElementsForInsurances($parm as element(fml:FML32)) as element()
{

<ns0:insurances>
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="4") then 
    <ns8:Insurance>
      <ns8:insuranceNumber?>{data($parm/NF_INSURA_INSURANCENUMBER[$occ])}</ns8:insuranceNumber>
      <ns8:insuranceDescription?>{data($parm/NF_INSURA_INSURANCEDESCRIP[$occ])}</ns8:insuranceDescription>
       { insertDate(data($parm/NF_INSURA_LASTINSURANCEPRE[$occ]),"yyyy-MM-dd","ns8:lastInsurancePremiumDate")}
       { insertDate(data($parm/NF_INSURA_INSURANCEOPENDAT[$occ]),"yyyy-MM-dd","ns8:insuranceOpenDate")}
        </ns8:Insurance>
    else ()
  }
</ns0:insurances>
};
:)

declare function getElementsForAccountRelationshipList($parm as element(fml:FML32)) as element()
{

<ns9:accountRelationshipList>
  {
    for $x at $occ in $parm/XXXXX
    return
    <ns4:AccountRelationship>
      <ns4:relationship>
        <ns7:CustomerAccountRelationship>
        </ns7:CustomerAccountRelationship>
      </ns4:relationship>
    </ns4:AccountRelationship>
  }
</ns9:accountRelationshipList>
};
declare function getElementsForAccounts($parm as element(fml:FML32)) as element()
{
<ns0:accounts>
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="2" or data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="10"
          or data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="13") then
    <ns9:Account>
      <ns9:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}</ns9:accountNumber>
     { insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns9:accountOpenDate")}
        <ns9:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}</ns9:currentBalance>
      <ns9:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}</ns9:accountDescription> 
      <ns9:codeProductSystem?>{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}</ns9:codeProductSystem>
      <ns9:accountRelationshipList>
        <ns4:AccountRelationship>
          <ns4:relationship>
             <ns7:CustomerAccountRelationship>
                <ns7:customerAccountRelationship>{data($parm/NF_ACCOUR_RELATIONSHIP[$occ])}</ns7:customerAccountRelationship>
             </ns7:CustomerAccountRelationship>
             </ns4:relationship>
        </ns4:AccountRelationship>
      </ns9:accountRelationshipList>
          {
           if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="10") then
             <ns9:timeAccount>
               <ns9:TimeAccount>
                   { insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]),"yyyy-MM-dd","ns9:dateOfMaint")}
               </ns9:TimeAccount>
             </ns9:timeAccount>
           else
             <ns9:tranAccount>
                <ns9:TranAccount>
                   { insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]),"yyyy-MM-dd","ns9:dateOfLastActivity")}
                   (: <ns9:dateOfLastActivity?>{fn-bea:date-from-string-with-format("yyyy-MM-dd",data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]))}</ns9:dateOfLastActivity> :)
                 </ns9:TranAccount>
             </ns9:tranAccount>
          }
      <ns9:accountType>
        <ns7:AccountType>
          <ns7:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}</ns7:accountType>
        </ns7:AccountType>
      </ns9:accountType>
      <ns9:currency>
        <ns7:CurrencyCode>
          <ns7:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns7:currencyCode>
        </ns7:CurrencyCode>
      </ns9:currency>
      {getElementsForProductDefinition($parm, $occ)}
    </ns9:Account>
    else()
  }
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="9" ) then
    <ns9:Account>
      <ns9:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}</ns9:accountNumber>
     { insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns9:accountOpenDate")}
      <ns9:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}</ns9:currentBalance>
      <ns9:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}</ns9:accountDescription> 
      <ns9:codeProductSystem?>{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}</ns9:codeProductSystem>
      <ns9:tranAccount>
        <ns9:TranAccount>
           { insertDate(data($parm/NF_TRANA_DATEOFLASTACTIVIT[$occ]),"yyyy-MM-dd","ns9:dateOfLastActivity")}
           </ns9:TranAccount>
      </ns9:tranAccount>
      <ns9:accountType>
        <ns7:AccountType>
          <ns7:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}</ns7:accountType>
        </ns7:AccountType>
      </ns9:accountType>
      <ns9:currency>
        <ns7:CurrencyCode>
          <ns7:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns7:currencyCode>
        </ns7:CurrencyCode>
      </ns9:currency>
      {getElementsForProductDefinition($parm, $occ)}
    </ns9:Account>
    else()
  }
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
    if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="3"  ) then
    <ns9:Account>
      <ns9:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}</ns9:accountNumber>
     { insertDate(data($parm/NF_ACCOUN_ACCOUNTOPENDATE[$occ]),"yyyy-MM-dd","ns9:accountOpenDate")}
      <ns9:currentBalance?>{data($parm/NF_ACCOUN_CURRENTBALANCE[$occ])}</ns9:currentBalance>
      <ns9:accountDescription?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}</ns9:accountDescription> 
      <ns9:codeProductSystem?>{data($parm/NF_ACCOUN_CODEPRODUCTSYSTE[$occ])}</ns9:codeProductSystem>
      <ns9:accountType>
        <ns7:AccountType>
          <ns7:accountType?>{data($parm/NF_ACCOUT_ACCOUNTTYPE[$occ])}</ns7:accountType>
        </ns7:AccountType>
      </ns9:accountType>
      <ns9:currency>
        <ns7:CurrencyCode>
          <ns7:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns7:currencyCode>
        </ns7:CurrencyCode>
      </ns9:currency>
      {getElementsForProductDefinition($parm, $occ)}
    </ns9:Account>
    else()
  }


</ns0:accounts>
};
declare function getElementsForCards($parm as element(fml:FML32)) as element()
{

<ns0:cards>
  {
    for $x at $occ in $parm/NF_PRODUA_CODEPRODUCTAREA
    return
     if (data($parm/NF_PRODUA_CODEPRODUCTAREA[$occ])="1") then
    <ns3:Card>
      <ns3:cardName?>{data($parm/NF_ACCOUN_ACCOUNTDESCRIPTI[$occ])}</ns3:cardName>
      <ns3:debitCard>
        <ns3:DebitCard>
          <ns3:cardNbr?>{data($parm/NF_DEBITC_CARDNBR[$occ])}</ns3:cardNbr>
          <ns3:virtualCardNbr?>{data($parm/NF_DEBITC_VIRTUALCARDNBR[$occ])}</ns3:virtualCardNbr>
          <ns3:tranAccount>
            <ns9:TranAccount>
              <ns9:account>
                <ns9:Account>
                  <ns9:accountNumber?>{data($parm/NF_ACCOUN_ACCOUNTNUMBER[$occ])}</ns9:accountNumber>
                  <ns9:currency>
                     <ns7:CurrencyCode>
                      <ns7:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns7:currencyCode>
                     </ns7:CurrencyCode>
                   </ns9:currency>
                 </ns9:Account>
               </ns9:account>
             </ns9:TranAccount>
           </ns3:tranAccount>
         </ns3:DebitCard>
       </ns3:debitCard>
    </ns3:Card>
         else ()
  }
</ns0:cards>
};
declare function getElementsForCollaterals($parm as element(fml:FML32)) as element()
{

<ns0:collaterals>
  {
    for $x at $occ in $parm/NF_COLLAT_COLLATERALVALUE
    return
    if (string-length($parm/NF_COLLAT_COLLATERALVALUE[$occ])>0) then
    <ns9:Collateral>      
      <ns9:collateralDescription?>{data($parm/NF_COLLAT_COLLATERALDESCRI[$occ])}</ns9:collateralDescription>
      <ns9:collateralValue?>{data($parm/NF_COLLAT_COLLATERALVALUE[$occ])}</ns9:collateralValue>
      <ns9:collateralItemNumber?>{data($parm/NF_COLLAT_COLLATERALITEMNU[$occ])}</ns9:collateralItemNumber>
      <ns9:lastMaintenance?>{data($parm/NF_COLLAT_LASTMAINTENANCE[$occ])}</ns9:lastMaintenance>
      <ns9:currencyCode>
        <ns7:CurrencyCode>
          <ns7:currencyCode?>{data($parm/NF_CURREC_CURRENCYCODE[$occ])}</ns7:currencyCode>
        </ns7:CurrencyCode>
      </ns9:currencyCode>
    </ns9:Collateral>
    else ()
  }
</ns0:collaterals>
};

(:
declare function getElementsForProductDefinition($parm as element(fml:FML32)) as element()
{

<ns0:productDefinition>
  {
    for $x at $occ in $parm/NF_ACCOUN_ACCOUNTNUMBER
    return
    <ns1:ProductDefinition>
      <ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}</ns1:idProductGroup>
      <ns1:idProductDefinition?>{data($parm/PT_ID_DEFINITION[$occ])}</ns1:idProductDefinition>
    </ns1:ProductDefinition>
  }
</ns0:productDefinition>
};
:)

declare function getElementsForProductDefinition($parm as element(fml:FML32), $occ as xs:integer) as element()
{
<ns9:productDefinition>
    <ns1:ProductDefinition>
      <ns1:idProductGroup?>{data($parm/PT_ID_GROUP[$occ])}</ns1:idProductGroup>
      <ns1:idProductDefinition?>{data($parm/PT_ID_DEFINITION[$occ])}</ns1:idProductDefinition>
    </ns1:ProductDefinition>
</ns9:productDefinition>
};


declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  (:{getElementsForInsurances($parm)}:)
  (:{getElementsForProductDefinition($parm)}:)
  {getElementsForAccounts($parm)}
  <ns0:bcd>
    <ns5:BusinessControlData>
      <ns5:pageControl>
        <ns6:PageControl>
          <ns6:hasNext?>{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}</ns6:hasNext>
          <ns6:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns6:navigationKeyDefinition>
          <ns6:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns6:navigationKeyValue>
        </ns6:PageControl>
      </ns5:pageControl>
    </ns5:BusinessControlData>
  </ns0:bcd>
  {getElementsForCards($parm)}
  {getElementsForCollaterals($parm)}
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>