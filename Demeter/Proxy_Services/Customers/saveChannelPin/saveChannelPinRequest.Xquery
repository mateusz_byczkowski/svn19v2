<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-15</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapsaveChannelPinRequest($req as element(m:SaveChannelPinRequest))
	as element(fml:FML32) {
		<fml:FML32>
			<fml:E_LOGIN_ID?>{ data($req/m:Nik) }</fml:E_LOGIN_ID>
			{
				for $pinData in $req/m:PinDataTable/m:PinData return
				(
					<fml:E_CHANNEL_TYPE>{data($pinData/m:ChannelType)}</fml:E_CHANNEL_TYPE>,
					<fml:E_TIME_STAMP>{data($pinData/m:Timestamp)}</fml:E_TIME_STAMP>
				)
			}
			<fml:E_SMS_TOKEN_NO?>{data($req/m:SmsTokenNumber)}</fml:E_SMS_TOKEN_NO>
			<fml:U_USER_NAME?>{data($req/m:LoginData/m:UserName)}</fml:U_USER_NAME>
			<fml:E_ADMIN_MICRO_BRANCH?>{data($req/m:LoginData/m:AdminMicroBranch)}</fml:E_ADMIN_MICRO_BRANCH>

		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapsaveChannelPinRequest($body/m:SaveChannelPinRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>