<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-15</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapsaveChannelPinResponse($fml as element(fml:FML32))
	as element(m:SaveChannelPinResponse) {
		<m:SaveChannelPinResponse>
			<m:Nik?>{ data($fml/fml:E_LOGIN_ID) }</m:Nik>
			<m:PinDataTable>
				{
					let $channelType := $fml/fml:E_CHANNEL_TYPE
					let $status := $fml/fml:E_STATUS
					let $timestamp := $fml/fml:E_TIME_STAMP
					for $it at $p in $fml/fml:E_CHANNEL_TYPE 
					return
						<m:PinData>
							<m:ChannelType?>{data($channelType[$p])}</m:ChannelType>
							<m:Status?>{data($status[$p])}</m:Status>
							<m:Timestamp?>{data($timestamp[$p])}</m:Timestamp>
						</m:PinData>
				}
			</m:PinDataTable>
	</m:SaveChannelPinResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapsaveChannelPinResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>