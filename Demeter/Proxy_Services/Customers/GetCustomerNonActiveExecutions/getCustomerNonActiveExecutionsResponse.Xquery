<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:entities.be.dcl";
declare namespace ns5="urn:baseauxentities.be.dcl";
declare namespace ns4="urn:executions.entities.be.dcl";
declare namespace ns6="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

     declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
      };

declare function getElementsForExecutions($parm as element(fml:FML32)) as element()
{

<ns0:executions>
  {
    for $x at $occ in $parm/NF_EXECUT_SIGNATURE
    return
    <ns4:Execution>
      <ns4:signature>{data($parm/NF_EXECUT_SIGNATURE[$occ])}</ns4:signature>
      <ns4:executionNumberLPP>{data($parm/NF_EXECUT_EXECUTIONNUMBERL[$occ])}</ns4:executionNumberLPP> 
      { insertDate(data($parm/NF_EXECUT_INCOMINGDATE[$occ]),"yyyy-MM-dd","ns4:incomingDate")}
      { insertDate(data($parm/NF_EXECUT_STARTEXECUTIONDA[$occ]),"yyyy-MM-dd","ns4:startExecutionDate")}  
  </ns4:Execution>
  }
</ns0:executions>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns0:invokeResponse>
  {getElementsForExecutions($parm)}
  <ns0:bcd>
    <ns3:BusinessControlData>
      <ns3:pageControl>
        <ns5:PageControl>
          <ns5:hasNext>{sourceValue2Boolean(data($parm/NF_PAGEC_HASNEXT),"1")}</ns5:hasNext>
          <ns5:navigationKeyDefinition>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns5:navigationKeyDefinition>
          <ns5:navigationKeyValue>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns5:navigationKeyValue>
        </ns5:PageControl>
      </ns3:pageControl>
    </ns3:BusinessControlData>
  </ns0:bcd>
</ns0:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>