<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-07-29</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn3 = "urn:ceke.entities.be.dcl";

declare function xf:mapgetCustomerCEKEListResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		<urn:invokeResponse>
			<urn:customerCEKEList>

				{

					let $E_LOGIN_ID := $fml/fml:E_LOGIN_ID
					let $B_NR_DOK := $fml/fml:B_NR_DOK
					let $B_RODZ_DOK := $fml/fml:B_RODZ_DOK
					let $E_CUST_REPORT_VERSION := $fml/fml:E_CUST_REPORT_VERSION
					let $E_CUST_STATUS := $fml/fml:E_CUST_STATUS
					let $E_PROFILE_ID := $fml/fml:E_PROFILE_ID
					let $E_PROFILE_NAME := $fml/fml:E_PROFILE_NAME
					let $E_PACKAGE_ID := $fml/fml:E_PACKAGE_ID
					let $E_PACKAGE_NAME := $fml/fml:E_PACKAGE_NAME
					let $E_VIEW_DET_ALLOWED := $fml/fml:E_VIEW_DET_ALLOWED
					let $E_CLOSE_CEKE_ALLOWED := $fml/fml:E_CLOSE_CEKE_ALLOWED
					let $E_DETACH_PRODUCT_ALLOWED := $fml/fml:E_DETACH_PRODUCT_ALLOWED
					let $E_PRINT_CONTRACT := $fml/fml:E_PRINT_CONTRACT

					for $it at $p in $fml/fml:E_TIME_STAMP
					return
						<urn3:CustomerCEKE>

						{
							if($E_LOGIN_ID[$p])
								then <urn3:nik>{ data($E_LOGIN_ID[$p]) }</urn3:nik>
							else ()
						}
						{
							if($B_NR_DOK[$p])
								then <urn3:documentNumber>{ 
									if(data($B_RODZ_DOK[$p]) = "A") then								
										substring(concat('0000000000',data($B_NR_DOK[$p])),string-length(concat('0000000000',data($B_NR_DOK[$p])))-10+1,10)
									else
										 data($B_NR_DOK[$p]) 
								 }</urn3:documentNumber>
							else ()
						}
						{
							if($E_CUST_REPORT_VERSION[$p])
								then (
								<urn3:contractVersion>{ data($E_CUST_REPORT_VERSION[$p]) }</urn3:contractVersion>
								)
							else ()
						}
						{
							if($B_RODZ_DOK[$p])
								then (
								<urn3:documentType>
									<urn2:DocumentTypeCEKE>
										<urn2:documentTypeCEKE>{ data($B_RODZ_DOK[$p]) }</urn2:documentTypeCEKE>
									</urn2:DocumentTypeCEKE>
								</urn3:documentType>
								)
							else ()
						}

						{
							if($E_CUST_STATUS[$p])
								then (
								<urn3:customerCEKEStatus>
									<urn2:CustomerStatusCEKE>
										{
											if(data($E_CUST_STATUS[$p]) = "0") then
											(
												<urn2:customerStatusCEKE>1</urn2:customerStatusCEKE>
											) else if(data($E_CUST_STATUS[$p]) = "4") then
											(
												<urn2:customerStatusCEKE>2</urn2:customerStatusCEKE>
											) else if(data($E_CUST_STATUS[$p]) = "5") then
											(
												<urn2:customerStatusCEKE>3</urn2:customerStatusCEKE>
											) else if(data($E_CUST_STATUS[$p]) = "6") then
											(
												<urn2:customerStatusCEKE>4</urn2:customerStatusCEKE>
											) else if(data($E_CUST_STATUS[$p]) = "9") then
											(
												<urn2:customerStatusCEKE>5</urn2:customerStatusCEKE>
											) else
											(
												<urn2:customerStatusCEKE>{data($E_CUST_STATUS[$p])}</urn2:customerStatusCEKE>
											)
										}
									</urn2:CustomerStatusCEKE>
								</urn3:customerCEKEStatus>
								)
							else ()
						}

						{
							<urn3:packageCEKE>
								<urn3:PackageCEKE>
									<urn3:packageCEKE?>{ data($E_PACKAGE_ID[$p]) }</urn3:packageCEKE>
									<urn3:description?>{ data($E_PACKAGE_NAME[$p]) }</urn3:description>
								</urn3:PackageCEKE>
							</urn3:packageCEKE>
						}

						{
							<urn3:profileCEKE>
								<urn3:ProfileCEKE>
									<urn3:profileCEKE?>{ data($E_PROFILE_ID[$p]) }</urn3:profileCEKE>
									<urn3:description?>{ data($E_PROFILE_NAME[$p]) }</urn3:description>
									{
										if ($E_VIEW_DET_ALLOWED[$p] = 0) then (
											<urn3:nfeViewDetailsAllowed>false</urn3:nfeViewDetailsAllowed>
										) else if ($E_VIEW_DET_ALLOWED[$p] = 1) then (
											<urn3:nfeViewDetailsAllowed>true</urn3:nfeViewDetailsAllowed>
										) else ()
									}
									{
										if ($E_CLOSE_CEKE_ALLOWED[$p] = 0) then (
											<urn3:nfeCloseCEKEAllowed?>false</urn3:nfeCloseCEKEAllowed>
										) else if($E_CLOSE_CEKE_ALLOWED[$p] = 1) then (
											<urn3:nfeCloseCEKEAllowed?>true</urn3:nfeCloseCEKEAllowed>
										) else ()
									}
									{
										if ($E_DETACH_PRODUCT_ALLOWED[$p] = 0) then (
											<urn3:nfeDetachProductExamination?>false</urn3:nfeDetachProductExamination>
										) else if ($E_DETACH_PRODUCT_ALLOWED[$p] = 1) then (
											<urn3:nfeDetachProductExamination?>true</urn3:nfeDetachProductExamination>
										) else ()
									}
									{
										if ($E_PRINT_CONTRACT[$p] = 0) then (
											<urn3:nfePrintContract?>false</urn3:nfePrintContract>
										) else if ($E_PRINT_CONTRACT[$p] = 1) then (
											<urn3:nfePrintContract?>true</urn3:nfePrintContract>
										) else ()
									}

								</urn3:ProfileCEKE>
							</urn3:profileCEKE>
						}

						</urn3:CustomerCEKE>
				}

			</urn:customerCEKEList>
		</urn:invokeResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustomerCEKEListResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>