<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-06</con:description>
  <con:xquery><![CDATA[declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:cif.entities.be.dcl";
declare namespace m1 = "urn:cif.entities.be.dcl";
declare namespace m = "urn:be.services.dcl";
declare namespace ns5 = "urn:be.services.dcl";
declare namespace xf = "urn:be.services.dcl/mappings";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "";

declare function m:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};
declare function xf:mapDate($dateIn as xs:string) as xs:string {
     fn:concat (fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2))
};

declare function xf:cisGetCustomerRelationshipResponse($FML32OUT1 as element(ns7:FML32))
    as element(m:invokeResponse) {
        let $fml := $FML32OUT1
        return
<m:invokeResponse> 
    <m:customerList> 
      {if(data($fml/DC_NUMER_KLIENTA))
        then 
          for $x at $i in $fml/DC_NUMER_KLIENTA
            return
              <m1:Customer>

                {if($fml/CI_NAZWA_PELNA[$i] and m:isData($fml/CI_NAZWA_PELNA[$i]))
                    then <m1:companyName?>{data($fml/CI_NAZWA_PELNA[$i])}</m1:companyName>
                    else ()}

 


               {if($fml/DC_NUMER_KLIENTA[$i] and m:isData($fml/DC_NUMER_KLIENTA[$i]))
                    then <m1:customerNumber?>{data($fml/DC_NUMER_KLIENTA[$i])}</m1:customerNumber>
                    else ()
                }
 
   
                {if($fml/DC_DATA_WPROWADZENIA[$i] and m:isData($fml/DC_DATA_WPROWADZENIA[$i]))
                    then <m1:dateCustomerOpened?>{xf:mapDate($fml/DC_DATA_WPROWADZENIA[$i])}</m1:dateCustomerOpened>
                    else ()
                }
                {if($fml/CI_DATA_AKTUALIZACJI[$i] and m:isData($fml/CI_DATA_AKTUALIZACJI[$i]))
                    then <m1:fileMaintenanceLastDate?>{xf:mapDate($fml/CI_DATA_AKTUALIZACJI[$i])}</m1:fileMaintenanceLastDate>
                    else ()
                }
                {if($fml/DC_REZ_NIEREZ[$i] and m:isData($fml/DC_REZ_NIEREZ[$i]))
                    then <m1:resident?>{fn:boolean(xs:integer($fml/DC_REZ_NIEREZ[$i]))}</m1:resident>
                    else ()
                }
                {if($fml/DC_NIP[$i] and m:isData($fml/DC_NIP[$i]))
                    then <m1:taxID?>{data($fml/DC_NIP[$i])}</m1:taxID>
                    else ()
                }
                {if($fml/DC_NAZWA_SKROCONA[$i] and m:isData($fml/DC_NAZWA_SKROCONA[$i]))
                    then <m1:shortName?>{data($fml/DC_NAZWA_SKROCONA[$i])}</m1:shortName>
                    else (if($fml/CI_NAZWA_PELNA[$i] and m:isData($fml/CI_NAZWA_PELNA[$i]))
                          then <m1:shortName?>{data($fml/CI_NAZWA_PELNA[$i])}</m1:shortName>
                          else ()
                         )
                }
                {if($fml/CI_DECYZJA[$i] and m:isData($fml/CI_DECYZJA[$i]))
                    then <m1:crmDecision?>{data($fml/CI_DECYZJA[$i])}</m1:crmDecision>
                    else ()
                }
                {if($fml/CI_DATA_NIP[$i] and m:isData($fml/CI_DATA_NIP[$i]))
                    then <m1:taxIdDate?>{xf:mapDate($fml/CI_DATA_NIP[$i])}</m1:taxIdDate>
                    else ()
                }
              </m1:Customer> 
        else ()
      }
    </m:customerList>
  </m:invokeResponse> 
  
};

declare variable $FML32OUT1 as element(ns7:FML32) external;
<soap-env:Body>{
xf:cisGetCustomerRelationshipResponse($FML32OUT1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>