<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-05-06</con:description>
  <con:xquery><![CDATA[xquery version "1.0";


declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns5="urn:be.services.dcl";
declare namespace ns1="urn:cif.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace ns7 = "";

declare variable $header1 as element(ns5:header) external;
declare variable $invoke1 as element(ns5:invoke) external;



declare function xf:cisGetCustomerRelationship($header1 as element(ns5:header),
    $invoke1 as element(ns5:invoke))
    as element(ns7:FML32) {
        <ns7:FML32>
           <ns7:CI_ID_SPOLKI?>{data($header1/ns5:msgHeader/ns5:companyId)}</ns7:CI_ID_SPOLKI>
           {
            for $Customer in ($invoke1/ns5:customerList/ns1:Customer)
            return
            (
           <ns7:DC_NUMER_KLIENTA?>{data($Customer/ns1:customerNumber)}</ns7:DC_NUMER_KLIENTA>,

            <DC_TYP_RELACJI?>{
            for $Rel in ($Customer/ns1:customerRelationshipList/ns1:CustomerRelationship)
            return
            (
            if (data($Rel/ns1:relationType/ns2:CustomerRelations/ns2:customerRelations)) then
 						(fn-bea:trim(fn:concat(fn-bea:trim($Rel),',')))
 						else()
  			)
            }</DC_TYP_RELACJI>)
            }
            
            
        </ns7:FML32>
};




<soap-env:Body>{
xf:cisGetCustomerRelationship($header1,
     $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>