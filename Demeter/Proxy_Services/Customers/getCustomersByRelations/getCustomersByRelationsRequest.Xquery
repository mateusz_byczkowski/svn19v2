<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$2.2011-05-16</con:description>
  <con:xquery><![CDATA[xquery version "1.0" encoding "utf-8";

declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";

declare function xf:mapgetCustomersByRelationsRequest(
	$req as element(urn:invoke),
	$head as element (urn:header))
	as element(fml:FML32) {
		<fml:FML32>
			{
				<fml:CI_ID_WEW_PRAC?>{ data($head/urn:msgHeader/urn:userId) }</fml:CI_ID_WEW_PRAC>
			}
			{
				<fml:DC_NUMER_KLIENTA?>{ data($req/urn:customer/urn1:Customer/urn1:customerNumber) }</fml:DC_NUMER_KLIENTA>
			}
			{
				<fml:CI_REKURENCJA>N</fml:CI_REKURENCJA>
			}
		</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
<soap-env:Body>
{
	xf:mapgetCustomersByRelationsRequest($body/urn:invoke, $header/urn:header)
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>