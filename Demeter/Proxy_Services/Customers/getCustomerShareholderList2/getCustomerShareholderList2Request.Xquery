<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-15</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Tomasz Krajewski  
 : @version 1.0
 : @since   2011-01-10
 :
 : wersja WSDLa: 03-01-2011 11:15:08
 :
 : $Proxy Services/Customers/getCustomerShareholderList2/getCustomerShareholderList2Request.xq$
 :
 :)

declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:be.services.dcl";
declare namespace ns3 = "";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace xf = "getCustomerShareholderList2Request/function";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:isData($dataIn as xs:string) as xs:boolean {
	if (string-length(normalize-space(data($dataIn))) > 0)
	then true()
	else false()
};

declare function xf:cisGetCustomerShareholderList2Request(
	$header1 as element(ns1:header),
	$invoke1 as element(ns1:invoke))
	as element(ns3:FML32) {
		<ns3:FML32>
			<ns3:DC_NUMER_KLIENTA>{ data($invoke1/ns1:customer/ns0:Customer/ns0:customerNumber) }</ns3:DC_NUMER_KLIENTA>
			{
				for $CustRel at $occ in $invoke1/ns1:customerRelationsList/ns2:CustomerRelations
				return 
					<ns3:DC_TYP_RELACJI>{ data($CustRel/ns2:customerRelations) }</ns3:DC_TYP_RELACJI>
			}
			{
				if (xf:isData(data($invoke1/ns1:customerType/ns2:CustomerType/ns2:customerType)))
				then
				(
					<ns3:DC_TYP_KLIENTA>{ data($invoke1/ns1:customerType/ns2:CustomerType/ns2:customerType) }</ns3:DC_TYP_KLIENTA>
				)
				else
				()
			}
			{
				if (xf:isData(data($header1/ns1:msgHeader/ns1:companyId)))
				then
				(
					<ns3:CI_ID_SPOLKI>{ data($header1/ns1:msgHeader/ns1:companyId) }</ns3:CI_ID_SPOLKI>
				)
				else
				()
			}
		</ns3:FML32>
};

declare variable $header1 as element(ns1:header) external;
declare variable $invoke1 as element(ns1:invoke) external;
<soap-env:Body>
{
	xf:cisGetCustomerShareholderList2Request($header1, $invoke1)
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>