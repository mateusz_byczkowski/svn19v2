<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-11-30</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:errors.hlbsentities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:dictionaries.be.dcl";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace cer="http://bzwbk.com/services/cerber";
declare variable $body as element(soap:Body) external;
declare variable $cerberBody as element(cer:SearchUsersResponse) external;
declare variable $getCustCertBody as element(FML32) external;

declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?
{
      if ($value)
        then if(string-length($value)>5 and not ($value = "0001-01-01"))
            then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else() 
      else()
};

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function getElementsForCustomerEmploymentInfoList($parm as element(fml:FML32)) as element()
{

<ns2:customerEmploymentInfoList>
  {
    for $x at $occ in $parm/DC_WYKONYWANA_PRACA

    return
    <ns2:CustomerEmploymentInfo>
      <ns2:employment>{data($parm/DC_WYKONYWANA_PRACA[occ])}</ns2:employment>
    </ns2:CustomerEmploymentInfo>
  }
</ns2:customerEmploymentInfoList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32), $cerRes as element(cer:SearchUsersResponse), $getCustCert as element(fml:FML32)) as element()*
{

<ns4:invokeResponse>
  <ns4:documentsOut>
    <ns2:Document>
      <ns2:documentNumber>{data($parm/CI_SERIA_NR_DOK)}</ns2:documentNumber>
      <ns2:primaryDocument>{sourceValue2Boolean (data($parm/CI_DOK_TOZSAMOSCI),"1")}</ns2:primaryDocument>
    </ns2:Document>
  </ns4:documentsOut>
  <ns4:customerOut>
    <ns2:Customer>
      <ns2:companyName>{data($parm/CI_NAZWA_PELNA)}</ns2:companyName>
      <ns2:customerNumber>{data($parm/DC_NUMER_KLIENTA)}</ns2:customerNumber>
      { insertDate(data($parm/CI_DATA_WPROWADZENIA),"yyyy-MM-dd","ns2:dateCustomerOpened")}
      <ns2:resident>{sourceValue2Boolean (data($parm/DC_REZ_NIEREZ),"1")}</ns2:resident>
      <ns2:taxID>{data($parm/DC_NIP)}</ns2:taxID>
      <ns2:shortName>{data($parm/DC_NAZWA_SKROCONA)}</ns2:shortName>
      <ns2:certificate>{sourceValue2Boolean (data($getCustCert/NF_CUSTOM_CERTIFICATE),"1")}</ns2:certificate>
      <ns2:phoneNo>{data($parm/DC_NR_TELEFONU)}</ns2:phoneNo>
      <ns2:mobileNo>{data($parm/DC_NR_TELEF_KOMORKOWEGO)}</ns2:mobileNo>
      <ns2:email>{data($parm/DC_ADRES_E_MAIL)}</ns2:email>
      <ns2:portfolio>{fn:concat(data($cerRes/cer:User/Name), ' ', data($cerRes/cer:User/Surname))}</ns2:portfolio>
      <ns2:fax>{data($parm/DC_NUMER_FAKSU)}</ns2:fax>
      {getElementsForCustomerEmploymentInfoList($parm)}
      <ns2:customerFirm>
        <ns2:CustomerFirm>
          <ns2:numberOfEmployees?>{data($parm/DC_LICZBA_ZATRUDNIONYCH)}</ns2:numberOfEmployees>
          <ns2:regon?>{data($parm/DC_NR_DOWODU_REGON)}</ns2:regon>
          <ns2:krs?>{data($parm/CI_KRS)}</ns2:krs>
          <ns2:legalForm>
            <ns1:CustomerLegalForm>
              <ns1:customerLegalForm>{data($parm/CI_FOP)}</ns1:customerLegalForm>
            </ns1:CustomerLegalForm>
          </ns2:legalForm>
        </ns2:CustomerFirm>
      </ns2:customerFirm>
      <ns2:customerFinancialInfo>
        <ns2:CustomerFinancialInfo>
          <ns2:taxPercent>{data($parm/CI_PROCENT_PODATKU)}</ns2:taxPercent>
        </ns2:CustomerFinancialInfo>
      </ns2:customerFinancialInfo>

      <ns2:customerPersonal>
        <ns2:CustomerPersonal>
         <ns2:customerNumber?>{data($parm/DC_NUMER_KLIENTA)}</ns2:customerNumber>
         <ns2:workInBankStatus?>{sourceValue2Boolean(data($parm/DC_PRACOWNIK_BANKU),"1") }</ns2:workInBankStatus>
         <ns2:birthPlace?>{data($parm/DC_MIEJSCE_URODZENIA)}</ns2:birthPlace>
         <ns2:motherMaidenName?>{data($parm/DC_NAZWISKO_PANIENSKIE_MATKI)}</ns2:motherMaidenName>
         <ns2:fatherName?>{data($parm/DC_IMIE_OJCA)}</ns2:fatherName>
         <ns2:numberOfPersonsHousehold?>{data($parm/DC_LICZBA_OS_WE_WSP_GOSP_D)}</ns2:numberOfPersonsHousehold>
         <ns2:workplace?>{data($parm/CI_NAZWA_PRACODAWCY)}</ns2:workplace>
         { insertDate(data($parm/DC_DATA_URODZENIA),"yyyy-MM-dd","ns2:dateOfBirth")}
         <ns2:lastName>{data($parm/DC_NAZWISKO)}</ns2:lastName>
         <ns2:secondName?>{data($parm/DC_DRUGIE_IMIE)}</ns2:secondName>
         <ns2:pesel?>{data($parm/DC_NR_PESEL)}</ns2:pesel>
         <ns2:firstName>{data($parm/DC_IMIE)}</ns2:firstName>
         { insertDate(data($parm/DC_DATA_SMIERCI),"yyyy-MM-dd","ns2:dateOfDeath")}
         <ns2:identityCardNumber?>{data($parm/DC_NR_DOWODU_REGON)}</ns2:identityCardNumber>
         <ns2:passportNumber?>{data($parm/DC_NUMER_PASZPORTU)}</ns2:passportNumber>
        </ns2:CustomerPersonal>
      </ns2:customerPersonal>

      <ns2:branchOfOwnership>
        <ns1:BranchCode>
          <ns1:branchCode?>{data($parm/DC_NUMER_ODDZIALU)}</ns1:branchCode>
        </ns1:BranchCode>
      </ns2:branchOfOwnership>

      <ns2:customerType>
        <ns1:CustomerType>
          <ns1:customerType?>{data($parm/DC_TYP_KLIENTA)}</ns1:customerType>
        </ns1:CustomerType>
      </ns2:customerType>

      <ns2:customerSegment>
        <ns1:CustomerSegment>
          <ns1:customerSegment>{data($parm/CI_KLASA_OBSLUGI)}</ns1:customerSegment>
        </ns1:CustomerSegment>
      </ns2:customerSegment>

      <ns2:processingApproval>
        <ns1:CustomerProcessingApproval>
          <ns1:customerProcessingApproval>{data($parm/CI_UDOSTEP_GRUPA)}</ns1:customerProcessingApproval>
        </ns1:CustomerProcessingApproval>
      </ns2:processingApproval>

      <ns2:spw>
        <ns1:SpwCode>
          <ns1:spwCode?>{data($parm/DC_SPW)}</ns1:spwCode>
        </ns1:SpwCode>
      </ns2:spw>

    </ns2:Customer>
  </ns4:customerOut>
</ns4:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32, $cerberBody, $getCustCertBody)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>