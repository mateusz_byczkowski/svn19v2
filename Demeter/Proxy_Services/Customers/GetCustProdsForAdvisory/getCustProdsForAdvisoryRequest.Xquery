<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-11-05</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetCustProdsForAdvisoryRequest($req as element(m:getCustProdsForAdvisoryRequest))
  as element(fml:FML32) {
    <fml:FML32>
      {if($req/m:CustomCustomernumber)
        then <fml:NF_CUSTOM_CUSTOMERNUMBER>{ data($req/m:CustomCustomernumber) }</fml:NF_CUSTOM_CUSTOMERNUMBER>
        else ()
      }
      {if($req/m:PagecPagesize)
        then <fml:NF_PAGEC_PAGESIZE>{ data($req/m:PagecPagesize) }</fml:NF_PAGEC_PAGESIZE>
        else ()
      }
      {if($req/m:PagecActioncode)
        then <fml:NF_PAGEC_ACTIONCODE>{ data($req/m:PagecActioncode) }</fml:NF_PAGEC_ACTIONCODE>
        else ()
      }
      {if($req/m:PagecReverseorder)
        then <fml:NF_PAGEC_REVERSEORDER>{ data($req/m:PagecReverseorder) }</fml:NF_PAGEC_REVERSEORDER>
        else ()
      }
      {if($req/m:PagecNavigationkeyvalu)
        then <fml:NF_PAGEC_NAVIGATIONKEYVALU>{ data($req/m:PagecNavigationkeyvalu) }</fml:NF_PAGEC_NAVIGATIONKEYVALU>
        else ()
      }
      <fml:NF_MSHEAD_COMPANYID>1</fml:NF_MSHEAD_COMPANYID>
      <fml:NF_ACCOUN_ALTERNATIVEADDRE>0</fml:NF_ACCOUN_ALTERNATIVEADDRE>
      <fml:NF_PRODUG_RECEIVER>FU</fml:NF_PRODUG_RECEIVER>
      <fml:NF_CTRL_AREAS>020310</fml:NF_CTRL_AREAS>
      <fml:NF_ACCOUR_RELATIONSHIP>SOWJAFOWNJA1JA2JA3JA4JA5JAOJO1JO2JO3JO4JO5JOO</fml:NF_ACCOUR_RELATIONSHIP>
      <fml:NF_CTRL_ACTIVENONACTIVE>1</fml:NF_CTRL_ACTIVENONACTIVE>
    </fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapgetCustProdsForAdvisoryRequest($body/m:getCustProdsForAdvisoryRequest) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>