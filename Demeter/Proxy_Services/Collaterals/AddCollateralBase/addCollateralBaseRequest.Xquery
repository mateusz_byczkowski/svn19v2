<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-01-10</con:description>
  <con:xquery><![CDATA[declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:cif.entities.be.dcl";
declare namespace m3 = "urn:dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string

  return 
    if ($value < 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value < 10) 
         then fn:concat("000",$string)
      else if ($value < 100) 
         then fn:concat("00",$string)
      else if ($value < 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDateTime($dateIn as xs:dateTime) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-dateTime($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-dateTime($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-dateTime($dateIn)))
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-date($dateIn)))
};

declare function xf:booleanTo01($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "1"
    else "0"
};

<soap-env:Body>
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:collateral/m1:Collateral

    let $msgId := $reqh/m:msgHeader/m:msgId
    let $transId := $reqh/m:transHeader/m:transId
    let $userId  := $reqh/m:msgHeader/m:userId
    let $unitId  := $reqh/m:msgHeader/m:unitId
    return
    
    <fml:FML32>
      <fml:DC_MSHEAD_MSGID?>{ data($msgId) }</fml:DC_MSHEAD_MSGID>
      <fml:DC_TRN_ID?>{ data($transId) }</fml:DC_TRN_ID>
      <fml:DC_UZYTKOWNIK?>{concat("SKP:", data($userId)) }</fml:DC_UZYTKOWNIK>

      {if($unitId)
          then 
              if($unitId < 1000)
                then <fml:DC_ODDZIAL>{ data($unitId) }</fml:DC_ODDZIAL>
                else <fml:DC_ODDZIAL>0</fml:DC_ODDZIAL>
          else ()
      }

      {if($req/m1:locationCode)
          then <fml:DC_KOD_NALICZENIA_REZERWY>{ data($req/m1:locationCode) }</fml:DC_KOD_NALICZENIA_REZERWY>
          else <fml:DC_KOD_NALICZENIA_REZERWY>0</fml:DC_KOD_NALICZENIA_REZERWY>
      }
      {if($req/m1:collateralDescription)
          then <fml:DC_OPIS_ZABEZP_1_LINIA>{ substring(data($req/m1:collateralDescription),1,60) }</fml:DC_OPIS_ZABEZP_1_LINIA>
          else ()
      }
      {if($req/m1:collateralDescription)
          then <fml:DC_OPIS_ZABEZP_2_LINIA>{ substring(data($req/m1:collateralDescription),61,60) }</fml:DC_OPIS_ZABEZP_2_LINIA>
          else ()
      }
      {if($req/m1:collateralDescription)
          then <fml:DC_OPIS_ZABEZP_3_LINIA>{ substring(data($req/m1:collateralDescription),121,60) }</fml:DC_OPIS_ZABEZP_3_LINIA>
          else ()
      }
      {if($req/m1:collateralValue)
          then <fml:DC_WARTOSC_ZABEZPIECZENIA>{ data($req/m1:collateralValue) }</fml:DC_WARTOSC_ZABEZPIECZENIA>
          else ()
      }
      {if($req/m1:expirationOrMaturityDate and fn:string-length($req/m1:expirationOrMaturityDate)>0)
          then <fml:DC_DATA_WYGAS_LUB_ZAPADALNO>{ xf:mapDate($req/m1:expirationOrMaturityDate) }</fml:DC_DATA_WYGAS_LUB_ZAPADALNO>
          else ()
      }
      {if($req/m1:insuranceRequired and fn:string-length($req/m1:insuranceRequired)>0)
          then <fml:DC_WYMAGANE_UBEZPIECZENIE_T_N>{ xf:booleanTo01($req/m1:insuranceRequired) }</fml:DC_WYMAGANE_UBEZPIECZENIE_T_N>
          else ()
      }
      {if($req/m1:insuranceExpiryDate and fn:string-length($req/m1:insuranceExpiryDate)>0)
          then <fml:DC_DATA_WAZNOSCI_UBEZP>{ xf:mapDate($req/m1:insuranceExpiryDate) }</fml:DC_DATA_WAZNOSCI_UBEZP>
          else ()
      }
      {if($req/m1:legalClaimRegisteredDate and fn:string-length($req/m1:legalClaimRegisteredDate)>0)
          then <fml:DC_DATA_REJESTRACJI_ZABEZP>{ xf:mapDate($req/m1:legalClaimRegisteredDate) }</fml:DC_DATA_REJESTRACJI_ZABEZP>
          else ()
      }
      {if($req/m1:legalClaimExpiryDate and fn:string-length($req/m1:legalClaimExpiryDate)>0)
          then <fml:DC_DATA_WYGAS_REJESTRACJI>{ xf:mapDate($req/m1:legalClaimExpiryDate) }</fml:DC_DATA_WYGAS_REJESTRACJI>
          else ()
      }
      {if($req/m1:reviewFrequency)
          then <fml:DC_CZESTOTL_WYCENY>{ data($req/m1:reviewFrequency) }</fml:DC_CZESTOTL_WYCENY>
          else ()
      }
      {if($req/m1:reviewPeriod)
          then <fml:DC_OKRES_WYCENY_ZABEZP>{ data($req/m1:reviewPeriod) }</fml:DC_OKRES_WYCENY_ZABEZP>
          else ()
      }
      <fml:DC_OKRESLONY_DZIEN_WYCENY>00</fml:DC_OKRESLONY_DZIEN_WYCENY>
      {if($req/m1:firstReviewDate and fn:string-length($req/m1:firstReviewDate)>0)
          then <fml:DC_DATA_PIERWSZEJ_WYCENY>{ xf:mapDateTime($req/m1:firstReviewDate) }</fml:DC_DATA_PIERWSZEJ_WYCENY>
          else ()
      }
      {if($req/m1:reviewComments)
          then <fml:DC_KOMENTARZ_DLA_WYCENY>{ data($req/m1:reviewComments) }</fml:DC_KOMENTARZ_DLA_WYCENY>
          else ()
      }
      {if($req/m1:numberOfUnits)
          then <fml:DC_LICZBA_JEDN_ZABEZP>{ data($req/m1:numberOfUnits) }</fml:DC_LICZBA_JEDN_ZABEZP>
          else ()
      }
      {if($req/m1:unitPrice)
          then <fml:DC_WARTOSC_JEDNOSTKOWA>{ data($req/m1:unitPrice) }</fml:DC_WARTOSC_JEDNOSTKOWA>
          else ()
      }
      {if($req/m1:marginPercentage and fn:string-length($req/m1:marginPercentage)>0)
          then <fml:DC_PROCENT_POMNIEJSZ_REZERWY>{ round-half-to-even(data($req/m1:marginPercentage)*100,4) }</fml:DC_PROCENT_POMNIEJSZ_REZERWY>
          else ()
      }
      {if($req/m1:maximumCollateralValue)
          then <fml:DC_MAKS_WARTOSC_ZABEZPIECZ>{ data($req/m1:maximumCollateralValue) }</fml:DC_MAKS_WARTOSC_ZABEZPIECZ>
          else ()
      }
      {if($req/m1:dateLastPriced and fn:string-length($req/m1:dateLastPriced)>0)
          then <fml:DC_DATA_POPRZEDNIEJ_WYCENY>{ xf:mapDate($req/m1:dateLastPriced) }</fml:DC_DATA_POPRZEDNIEJ_WYCENY>
          else ()
      }
      {if($req/m1:itemReferenceNumber and fn:string-length($req/m1:itemReferenceNumber)>0)
          then <fml:DC_NR_RACH_OBJETY_BLOKADA_SRO>{ data($req/m1:itemReferenceNumber) }</fml:DC_NR_RACH_OBJETY_BLOKADA_SRO>
          else ()
      }
      {if($req/m1:safekeepingReceiptNbr)
          then <fml:DC_INFORM_DODATK_O_ZABEZP>{ data($req/m1:safekeepingReceiptNbr) }</fml:DC_INFORM_DODATK_O_ZABEZP>
          else ()
      }
      {if($req/m1:shortDescription)
          then <fml:DC_NAZWA_SKROCONA>{ data($req/m1:shortDescription) }</fml:DC_NAZWA_SKROCONA>
          else ()
      }
      {if($req/m1:userField6)
          then <fml:DC_NR_UMOWY_PORECZ>{ data($req/m1:userField6) }</fml:DC_NR_UMOWY_PORECZ>
          else ()
      }

      {for $it at $i in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:customer/m2:Customer/m2:customerNumber)
            then 
              if($it/m1:relationship = "SOW" or $it/m1:relationship = "JAF")
                then <fml:DC_NUMER_KLIENTA>{ data($it/m1:customer/m2:Customer/m2:customerNumber) }</fml:DC_NUMER_KLIENTA>
                else ()
            else ()
      }

      {for $it in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:relationship)
            then (<fml:DC_RELACJA>{ data($it/m1:relationship) }</fml:DC_RELACJA>,
                     <fml:DC_TYP_RELACJI>1</fml:DC_TYP_RELACJI>)
            else ()
      }

      {for $it in $req/m1:customerCollateralList/m1:CustomerCollateral
        return
        if($it/m1:customer/m2:Customer/m2:customerNumber)
            then <fml:DC_NUMER_KLIENTA_REL>{ data($it/m1:customer/m2:Customer/m2:customerNumber) }</fml:DC_NUMER_KLIENTA_REL>
            else ()
      }

      {if($req/m1:collateralCode/m3:CollateralCode/m3:collateralCode)
          then <fml:DC_KOD_ZABEZPIECZENIA>{ data($req/m1:collateralCode/m3:CollateralCode/m3:collateralCode) }</fml:DC_KOD_ZABEZPIECZENIA>
          else ()
      }
      {if($req/m1:currencyCode/m3:CurrencyCode/m3:currencyCode)
          then <fml:DC_WALUTA>{ data($req/m1:currencyCode/m3:CurrencyCode/m3:currencyCode) }</fml:DC_WALUTA>
          else ()
      }
      {if($req/m1:methodOfSecurityValuation/m3:MethodOfSecurityValuation/m3:methodOfSecurityValuation)
          then <fml:DC_METODA_WYCENY_ZABEZP>{ data($req/m1:methodOfSecurityValuation/m3:MethodOfSecurityValuation/m3:methodOfSecurityValuation) }</fml:DC_METODA_WYCENY_ZABEZP>
          else ()
      }
      {if($req/m1:personValuatingASecurity/m3:PersonValuatingASecurity/m3:personValuatingASecurity)
          then <fml:DC_OSOBA_WYCENIAJACA_ZABEZP>{ data($req/m1:personValuatingASecurity/m3:PersonValuatingASecurity/m3:personValuatingASecurity) }</fml:DC_OSOBA_WYCENIAJACA_ZABEZP>
          else ()
      }
      {if($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber and fn:string-length($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber)>0)
          then <fml:DC_BLOKADA_SRODK_NA_R_KU>{ data($req/m1:itemApplicationNumber/m3:ApplicationNumber/m3:applicationNumber) }</fml:DC_BLOKADA_SRODK_NA_R_KU>
          else ()
      }
      <fml:DC_ENCODING>PL_MZV</fml:DC_ENCODING>
    </fml:FML32>
  }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>