<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-01-10</con:description>
  <con:xquery><![CDATA[declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;

declare function xf:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))>0)
    then true()
    else false()
};

<soap-env:Body>
  <m:invokeResponse xmlns:urn="urn:be.services.dcl">
    <m:collateral xmlns:urn1="urn:accounts.entities.be.dcl">
      {
        let $fml := $body/fml:FML32
        let $DC_ID_ZABEZPIECZENIA := $fml/fml:DC_ID_ZABEZPIECZENIA
        
        return
        <urn1:Collateral>
          {if($DC_ID_ZABEZPIECZENIA and xf:isData($DC_ID_ZABEZPIECZENIA))
             then <urn1:collateralItemNumber>{ data($DC_ID_ZABEZPIECZENIA) }</urn1:collateralItemNumber>
             else ()
          }
        </urn1:Collateral>
      }
    </m:collateral>
  </m:invokeResponse>
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>