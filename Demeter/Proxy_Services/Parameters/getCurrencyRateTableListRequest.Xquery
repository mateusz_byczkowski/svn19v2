<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2010-03-03 17:36
 :
 : wersja WSDLa: 15-02-2010 13:53:15
 :
 : $Proxy Services/Parameters/getCurrencyRateTableList/getCurrencyRateTableListRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Parameters_svn/getCurrencyRateTableList/getCurrencyRateTableListRequest/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $header1 as element(ns2:header) external;
declare variable $invoke1 as element(ns2:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 :
 : @return $invoke1 operacja wejściowa
 :)
declare function xf:getCurrencyRateTableListRequest($header1 as element(ns2:header),
													  $invoke1 as element(ns2:invoke))
    as element(ns1:FML32)
{
	<ns1:FML32>

		(:
		 : dane z nagłówka
		 :)
		<ns1:NF_MSHEAD_MSGID?>{
			data($header1/ns2:msgHeader/ns2:msgId)
		}</ns1:NF_MSHEAD_MSGID>

		(:
		 : dane z operacji wejściowej
		 : wysylamy jedynie date z pola currencyDate o typie dateTime
		 :)
		{
			let $currencyDate := $invoke1/ns2:rateTable/ns0:RateTable/ns0:currencyDate
			return
				if (data($currencyDate)) then
					<ns1:NF_RATET_CURRENCYDATE>{
						fn:substring(data($currencyDate), 1, 10)
					}</ns1:NF_RATET_CURRENCYDATE>
				else ()
		}

	</ns1:FML32>
};

<soap-env:Body>{
	xf:getCurrencyRateTableListRequest($header1, $invoke1)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>