<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-10</con:description>
  <con:xquery><![CDATA[declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns="http://bzwbk.com/services/icbs/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns:getICBSPSDateResponse>
	<ns:currentProcessingDate>{fn-bea:date-from-string-with-format("dd-MM-yyyy", data($parm/B_DATA_OPER))}</ns:currentProcessingDate>
	<ns:nextProcessingDate>{fn-bea:date-from-string-with-format("dd-MM-yyyy", data($parm/B_D_NAST_OPER))}</ns:nextProcessingDate>
	<ns:state>{data($parm/B_STAN_PS)}</ns:state>
</ns:getICBSPSDateResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>