<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-09-09</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.1
 : @since   2010-03-05
 :
 : wersja WSDLa: 15-02-2010 13:53:15
 :
 : $Proxy Services/Parameters/getCurrencyRateTableList/getCurrencyRateTableListResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Parameters_svn/getCurrencyRateTableList/getCurrencyRateTableListResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return inovkeResponse operacja wyjściowa
 :)
declare function xf:getCurrencyRateTableListResponse($fML321 as element(ns1:FML32))
    as element(ns2:invokeResponse)
{
	<ns2:invokeResponse>
		<ns2:rateTables>{
			for $i in 1 to count($fML321/ns1:NF_RATET_CURRENCYDATE)
			return
				<ns0:RateTable>
					{
						let $currencyDate := $fML321/ns1:NF_RATET_CURRENCYDATE[$i]
						return
							<ns0:currencyDate>{
								fn:concat(fn:substring(data($currencyDate), 1, 10),
										  'T',
										  fn:substring(data($currencyDate), 12, 2),
										  ':',
										  fn:substring(data($currencyDate), 15, 2),
										  ':',
										  fn:substring(data($currencyDate), 18, 2))
							}</ns0:currencyDate>
					}

					{
						let $currentTable := $fML321/ns1:NF_RATET_CURRENCYTM[$i]
						return
							if (data($currentTable)) then
								<ns0:currentTable>{
									xs:boolean($currentTable)
								}</ns0:currentTable>
							else
								()
					}
				</ns0:RateTable>
		}</ns2:rateTables>
	</ns2:invokeResponse>
};

<soap-env:Body>{
	xf:getCurrencyRateTableListResponse($fML321)
}</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>