<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:executiondict.dictionaries.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:entities.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:executions.entities.be.dcl";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function sourceValue2Boolean ($parm as xs:string,$trueval as xs:string) as xs:string{
    if ($parm  =$trueval)
       then "true"
       else "false"
};

declare function insertDateTime($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
      if ($value)
        then if(string-length($value)>5 and not (substring($value,1,10) = "0001-01-01"))
            then prepareDateTime($value, $dateFormat, $fieldName)
        else() 
      else()
};

declare function prepareDateTime($value as xs:string,$dateFormat as xs:string,$fieldName as xs:string) as element(){
     let $date := substring($value,1,10)
     let $time := substring($value, 12,8)
     let $datetime := concat(concat($date, "+"),$time)
   
     return
      element  {$fieldName}  {fn-bea:dateTime-from-string-with-format($dateFormat,$datetime)}
     
};


declare function getElementsForExecutionHistoryList($parm as element(fml:FML32)) as element()
{

<ns4:executionHistoryList>
  {
    for $x at $occ in $parm/NF_EXECUH_OPERATIONID
    return
    <ns5:ExecutionHistory>
      <ns5:operationID?>{data($parm/NF_EXECUH_OPERATIONID[$occ])}</ns5:operationID>
      { insertDateTime(data($parm/NF_EXECUH_OPERATIONDATE[$occ]),"yyyy-MM-dd+hh.mm.ss","ns5:operationDate")}
      <ns5:operationTotalAmount?>{data($parm/NF_EXECUH_OPERATIONTOTALAM[$occ])}</ns5:operationTotalAmount>
      <ns5:operationType>
        <ns0:ExecutionOperationType>
          <ns0:executionOperationType?>{data($parm/NF_EXECOT_EXECUTIONOPERATI[$occ])}</ns0:executionOperationType>
        </ns0:ExecutionOperationType>
      </ns5:operationType>
    </ns5:ExecutionHistory>
  }
</ns4:executionHistoryList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns4:invokeResponse>
  {getElementsForExecutionHistoryList($parm)}
  <ns4:bcd>
    <ns2:BusinessControlData>
      <ns2:pageControl>
        <ns6:PageControl>
          <ns6:hasNext?>{sourceValue2Boolean (data($parm/NF_PAGEC_HASNEXT),"1")}</ns6:hasNext>
          <ns6:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns6:navigationKeyDefinition>
          <ns6:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns6:navigationKeyValue>
        </ns6:PageControl>
      </ns2:pageControl>
    </ns2:BusinessControlData>
  </ns4:bcd>
</ns4:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>