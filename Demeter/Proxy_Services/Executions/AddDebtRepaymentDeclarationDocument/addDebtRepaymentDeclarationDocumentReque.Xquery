<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace ns0="urn:be.services.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns2="urn:cif.entities.be.dcl";
declare namespace ns3="urn:executions.entities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns5="urn:filtersandmessages.entities.be.dcl";
declare namespace ns7="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:accounts.entities.be.dcl";
declare namespace fml="";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if($parm = "false")
       then $falseval
    else $parm
};

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
let $length:= string-length($sourceString)
let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
let $accountNumber :=  substring($sourceString, $startPos)
return
     $accountNumber
                          
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}</NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:freeAmountRepayment)) > 0)
then
   <NF_DEBTRD_FREEAMOUNTREPAYM?>{boolean2SourceValue(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:freeAmountRepayment),"1","0")}</NF_DEBTRD_FREEAMOUNTREPAYM>
else()
,
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:acaRepayment)) > 0)
then
<NF_DEBTRD_ACAREPAYMENT?>{boolean2SourceValue(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:acaRepayment),"1","0")}</NF_DEBTRD_ACAREPAYMENT>
else()
,
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:entireRepayment)) > 0)
then
<NF_DEBTRD_ENTIREREPAYMENT?>{boolean2SourceValue(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:entireRepayment),"1","0")}</NF_DEBTRD_ENTIREREPAYMENT>
else()
,
<NF_DEBTRD_REPAYMENTAMOUNT?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:repaymentAmount)}</NF_DEBTRD_REPAYMENTAMOUNT>
,
<NF_DEBTRD_REPAYMENTDESCRIP?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:repaymentDescription)}</NF_DEBTRD_REPAYMENTDESCRIP>
,
<NF_DEBTRD_ISSUEDATE?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueDate)}</NF_DEBTRD_ISSUEDATE>
,
<NF_BRANCC_BRANCHCODE?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueBranch/ns4:BranchCode/ns4:branchCode)}</NF_BRANCC_BRANCHCODE>
,
<NF_BRANCC_DESCRIPTION?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueBranch/ns4:BranchCode/ns4:description)}</NF_BRANCC_DESCRIPTION>
,
<NF_BRANCC_FULLADDRESS?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:issueBranch/ns4:BranchCode/ns4:fullAddress)}</NF_BRANCC_FULLADDRESS>
,
<NF_CUSTOM_COMPANYNAME?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:companyName)}</NF_CUSTOM_COMPANYNAME>
,
<NF_CUSTOM_CUSTOMERNUMBER?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerNumber)}</NF_CUSTOM_CUSTOMERNUMBER>
,
<NF_ADDRES_FULLADDRESS?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:addressList/ns2:Address/ns2:fullAddress)}</NF_ADDRES_FULLADDRESS>
,
for $x in data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:executionList/ns3:Execution/ns3:executionNumberLPP)
   return 
     <NF_EXECUT_EXECUTIONNUMBERL?>{$x}</NF_EXECUT_EXECUTIONNUMBERL>
,
<NF_CUSTOP_LASTNAME?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerPersonal/ns2:CustomerPersonal/ns2:lastName)}</NF_CUSTOP_LASTNAME>
,
<NF_CUSTOP_FIRSTNAME?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerPersonal/ns2:CustomerPersonal/ns2:firstName)}</NF_CUSTOP_FIRSTNAME>
,
<NF_CUSTOT_CUSTOMERTYPE?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:customer/ns2:Customer/ns2:customerType/ns4:CustomerType/ns4:customerType)}</NF_CUSTOT_CUSTOMERTYPE>
,
if(string-length(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountNumber)) > 0) 
then
 <NF_ACCOUN_ACCOUNTNUMBER?>{cutStr(data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountNumber),12)}</NF_ACCOUN_ACCOUNTNUMBER>
else()

,
<NF_BRANCC_BRANCHCODE?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountBranchNumber/ns4:BranchCode/ns4:branchCode)}</NF_BRANCC_BRANCHCODE>
,
<NF_BRANCC_DESCRIPTION?>{data($parm/ns0:debtRepaymentDeclaration/ns3:DebtRepaymentDeclaration/ns3:account/ns6:Account/ns6:accountBranchNumber/ns4:BranchCode/ns4:description)}</NF_BRANCC_DESCRIPTION>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>