<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:filtersandmessages.entities.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:accountdict.dictionaries.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)>3)
            then "0"
        else
             $unitId
      else ""
};

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm = "1") then $trueval
    else $falseval
};


declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};

declare function xf:prepareShortAccountNumber($accountNumber as xs:string?) as xs:string?{
   let $accountNumberLength:= string-length($accountNumber)
   let $shortAccountNumber := substring($accountNumber,$accountNumberLength - 11)
   let $shortAccountNumberWithZeros := concat(substring("000000000000", string-length($shortAccountNumber) + 1) , $shortAccountNumber)
   return
       $shortAccountNumber
};

declare function xf:mapchgTranAccountParametersRequest($req as element(ns1:Account), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

let $msgId:= $msghead/dcl:transId
let $companyId:= $msghead/dcl:companyId
let $userId := $msghead/dcl:userId
let $appId:= $msghead/dcl:appId
let $unitId := $msghead/dcl:unitId
let $timestamp:= $msghead/dcl:timestamp

let $transId:=$tranhead/dcl:transId
let $skpOpener:=$req/ns1:skpOpener

let $accountNumber:= $req/ns1:accountNumber
let $statementForAccount:=$req/ns1:statementForAccount
let $productCode:= $req/ns1:productCode
let $serviceChargeCode:= $req/ns1:tranAccount/ns1:TranAccount/ns1:serviceChargeCode
let $interestPlanNumber:= $req/ns1:tranAccount/ns1:TranAccount/ns1:interestPlanNumber
let $accountTypeFlag:= $req/ns1:tranAccount/ns1:TranAccount/ns1:accountTypeFlag/ns2:AccountTypeFlag/ns2:accountTypeFlag
let $promotionCode:= $req/ns1:tranAccount/ns1:TranAccount/ns1:promotionCode/ns2:PromotionCode/ns2:promotionCode
let $frequency:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:frequency
let $nextPrintoutDate:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:nextPrintoutDate
let $printFlag:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:printFlag
let $aggregateTransaction:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:aggregateTransaction
let $printoutDay:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:printoutDay
let $period:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:cycle/ns2:Period/ns2:period
(: let $specialDay:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:specialDay/ns2:SpecialDay/ns2:specialDay :)
let $specialDay:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:specialDay
let $provideManner:= $req/ns1:statementParameters/ns1:StatementParameters/ns1:provideManner/ns2:ProvideManner/ns2:provideManner
let $accessType:=$req/ns1:accessType/ns3:AccessType/ns3:accessType

return
<fml:FML32>
  <DC_TRN_ID?>{data($transId)}</DC_TRN_ID>
  <DC_UZYTKOWNIK?>{concat("SKP:",data($userId))}</DC_UZYTKOWNIK>
  <DC_ODDZIAL?>{chkUnitId(data($unitId))}</DC_ODDZIAL>
  <DC_NR_RACHUNKU?>{xf:prepareShortAccountNumber($accountNumber)}</DC_NR_RACHUNKU>
  <DC_NUMER_PRODUKTU?>{data($productCode)}</DC_NUMER_PRODUKTU>
  <DC_PLAN_OPLAT?>{data($serviceChargeCode)}</DC_PLAN_OPLAT>
  <DC_PLAN_ODSETKOWY?>{data($interestPlanNumber)}</DC_PLAN_ODSETKOWY>
  <DC_FLAGA_RACHUNKU?>{data($accountTypeFlag)}</DC_FLAGA_RACHUNKU>
  <DC_PROMOCJA?>{data($promotionCode)}</DC_PROMOCJA>
  <DC_LICZBA_OKRESOW_CYKL_WYCIAG?>{data($frequency)}</DC_LICZBA_OKRESOW_CYKL_WYCIAG>
  <DC_DATA_NASTEPNEGO_WYCIAGU?>{data($nextPrintoutDate)}</DC_DATA_NASTEPNEGO_WYCIAGU>

  <DC_ZBIJANIE_TRANSAKCJI?>{boolean2SourceValue(data($aggregateTransaction),"1","0")}</DC_ZBIJANIE_TRANSAKCJI>
  (:<DC_KONTROLA_WYCIAGOW?>{boolean2SourceValue(data($statementForAccount) ,"1","0")}</DC_KONTROLA_WYCIAGOW>:)
  <DC_KONTROLA_WYCIAGOW?> </DC_KONTROLA_WYCIAGOW> 
  <DC_KOD_CYKLU_WYCIAGOW?>{data($period)}</DC_KOD_CYKLU_WYCIAGOW>
  <DC_OKRESLONY_DZIEN_WYCIAGU?>{data($specialDay)}</DC_OKRESLONY_DZIEN_WYCIAGU>
  <DC_KOD_SPECJALNYCH_INSTRUKCJI?>{data($provideManner)}</DC_KOD_SPECJALNYCH_INSTRUKCJI>
  <DC_KOD?>{data($accessType)}</DC_KOD>
</fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


<soap-env:Body>
{ xf:mapchgTranAccountParametersRequest($body/dcl:invoke/dcl:account/ns1:Account, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>