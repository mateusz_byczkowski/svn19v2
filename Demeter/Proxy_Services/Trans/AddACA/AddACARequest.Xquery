<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-03-25</con:description>
  <con:xquery><![CDATA[declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:cif.entities.be.dcl";
declare namespace m3 = "urn:dictionaries.be.dcl";
declare namespace m4 = "urn:accountdict.dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";


declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string

  return 
    if ($value < 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value < 10) 
         then fn:concat("000",$string)
      else if ($value < 100) 
         then fn:concat("00",$string)
      else if ($value < 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
    fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
               xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
               xf:convertTo4CharString(fn:year-from-date($dateIn)))
};

declare function xf:shortAccount($account as xs:string) as xs:string {
    if (fn:string-length($account) > 12)
      then fn:substring($account, (fn:string-length($account))-11)
      else $account
};

<soap-env:Body>
  {
    let $reqh  := $header/m:header
    let $req   := $body/m:invoke/m:account/m1:Account
    let $req1  := $req/m1:tranAccount/m1:TranAccount/m1:tranAccountACAList/m1:TranAccountACA
    let $req2  := $req/m1:indirectCommitmenntList
    let $req3  := $req/m1:accountRelationshipList/m2:AccountRelationship
    let $req4  := $req3/m2:processingAgree/m2:ProcessingAgree

    let $msgId := $reqh/m:msgHeader/m:msgId
    let $transId := $reqh/m:transHeader/m:transId
    let $userId  := $reqh/m:msgHeader/m:userId
    let $unitId  := $reqh/m:msgHeader/m:unitId
    let $agreementDate := $req4/m2:agreementDate
    let $IndirectCommitment := $req2/m1:IndirectCommitment
    let $ICtypeIndirectCommitment := $IndirectCommitment/m1:typeIndirectCommitment
    let $ICamountMaxIndirectCommitment := $IndirectCommitment/m1:amountMaxIndirectCommitment
    let $ICindirectCommitmentPercent := $IndirectCommitment/m1:indirectCommitmentPercent
    let $ICcustomerNumber := $IndirectCommitment/m1:customer/m2:Customer/m2:customerNumber
    let $ICagreementFlag := $IndirectCommitment/m1:customer/m2:Customer/m2:accountRelationshipList/m2:AccountRelationship/m2:processingAgree/m2:ProcessingAgree/m2:agreementFlag/m3:CustomerAgreementStatus/m3:customerAgreementStatus
    let $ICagreementValidityPeriod := $IndirectCommitment/m1:customer/m2:Customer/m2:accountRelationshipList/m2:AccountRelationship/m2:processingAgree/m2:ProcessingAgree/m2:agreementValidityPeriod
    let $ICagreementDate := $IndirectCommitment/m1:customer/m2:Customer/m2:accountRelationshipList/m2:AccountRelationship/m2:processingAgree/m2:ProcessingAgree/m2:agreementDate
    let $nextMaturityDate := $req1/m1:nextMaturityDate
    let $nextInterestDate := $req1/m1:nextInterestDate
    let $nextAcaFeeDate := $req1/m1:nextAcaFeeDate
    
    return

    <fml:FML32>
      <fml:DC_MSHEAD_MSGID?>{ data($msgId) }</fml:DC_MSHEAD_MSGID>
      <fml:DC_TRN_ID?>{ data($transId) }</fml:DC_TRN_ID>
      <fml:DC_UZYTKOWNIK?>{concat("SKP:", data($userId)) }</fml:DC_UZYTKOWNIK>

      {if($unitId)
          then 
              if($unitId < 1000)
                then <fml:DC_ODDZIAL>{ data($unitId) }</fml:DC_ODDZIAL>
                else <fml:DC_ODDZIAL>0</fml:DC_ODDZIAL>
          else ()
      }

      <fml:DC_LK_ID_UZYTKOWNIKA_Z_DC?>{concat("SKP:", data($userId)) }</fml:DC_LK_ID_UZYTKOWNIKA_Z_DC>
      <fml:DC_LK_RODZAJ_OPERACJI?>LK7</fml:DC_LK_RODZAJ_OPERACJI>
      <fml:DC_LK_NR_RACHUNKU_ICBS?>{ xf:shortAccount($req/m1:accountNumber) }</fml:DC_LK_NR_RACHUNKU_ICBS>
      <fml:DC_NUMER_KLIENTA>{ data($req3/m2:customer/m2:Customer/m2:customerNumber) }</fml:DC_NUMER_KLIENTA>
      <fml:DC_FLAGA_ZGODY>{ data($req4/m2:agreementFlag/m3:CustomerAgreementStatus/m3:customerAgreementStatus) }</fml:DC_FLAGA_ZGODY>
      <fml:DC_OKRES_WAZNOSCI_ZGODY>{ data($req4/m2:agreementValidityPeriod) }</fml:DC_OKRES_WAZNOSCI_ZGODY>
      {if($agreementDate and fn:string-length($agreementDate)>0)
        then <fml:DC_DATA_ZGODY>{ if ($agreementDate) then xf:mapDate($agreementDate) else () }</fml:DC_DATA_ZGODY>
        else <fml:DC_DATA_ZGODY/>
      }
      
      {for $it at $i in $IndirectCommitment
        return
        (
          <fml:DC_TYP_POSRED_ZOBOWIAZANIA>{ data($ICtypeIndirectCommitment[$i]) }</fml:DC_TYP_POSRED_ZOBOWIAZANIA>,
          <fml:DC_MAX_KWOTA_PORECZENIA>{ data($ICamountMaxIndirectCommitment[$i]) }</fml:DC_MAX_KWOTA_PORECZENIA>,
          <fml:DC_PROCENT_ZOBOWIAZANIA>{ data($ICindirectCommitmentPercent[$i]) }</fml:DC_PROCENT_ZOBOWIAZANIA>,
          <fml:DC_NUMER_PORECZYCIELA_Z_CIF>{ data($ICcustomerNumber[$i]) }</fml:DC_NUMER_PORECZYCIELA_Z_CIF>,
          <fml:DC_FLAGA_ZGODY>{ data($ICagreementFlag[$i]) }</fml:DC_FLAGA_ZGODY>,
          <fml:DC_OKRES_WAZNOSCI_ZGODY>{ data($ICagreementValidityPeriod[$i]) }</fml:DC_OKRES_WAZNOSCI_ZGODY>,
          if($ICagreementDate[$i] and fn:string-length($ICagreementDate[$i])>0)
            then <fml:DC_DATA_ZGODY>{ xf:mapDate($ICagreementDate[$i]) }</fml:DC_DATA_ZGODY>
            else <fml:DC_DATA_ZGODY/>
        )
      }

      <fml:DC_LK_DATA_WYMAGALNOSCI?>{ if ($nextMaturityDate) then xf:mapDate($nextMaturityDate) else () }</fml:DC_LK_DATA_WYMAGALNOSCI>
      <fml:DC_LK_KWOTA_1?>{ data($req1/m1:limitAmount) }</fml:DC_LK_KWOTA_1>
      <fml:DC_LK_NR_INDEKSU?>{ data($req1/m1:indexNumber) }</fml:DC_LK_NR_INDEKSU>
      <fml:DC_LK_MARZA_BANKU_ODSETKI?>{ data($req1/m1:varianceForIndex) }</fml:DC_LK_MARZA_BANKU_ODSETKI>
      <fml:DC_LK_STOPA_IDYWIDUALNA_ODS?>{ round-half-to-even(data($req1/m1:negotiatedInterestRate)*100,4) }</fml:DC_LK_STOPA_IDYWIDUALNA_ODS>
      <fml:DC_CZEST_SPLAT_ODS?>{ data($req1/m1:interestFrequency) }</fml:DC_CZEST_SPLAT_ODS>
      <fml:DC_DATA_PIERWSZEJ_PLAT_ODS?>{ if ($nextInterestDate) then xf:mapDate($nextInterestDate) else () }</fml:DC_DATA_PIERWSZEJ_PLAT_ODS>
      <fml:DC_LK_PODSTAWA_MIESIECZNA?>{ data($req1/m1:accrualBase) }</fml:DC_LK_PODSTAWA_MIESIECZNA>
      <fml:DC_LK_PODSTAWA_ROCZNA?>{ data($req1/m1:yearBase) }</fml:DC_LK_PODSTAWA_ROCZNA>
      <fml:DC_LK_KWOTY_OPLATY?>{ data($req1/m1:feePercentage) }</fml:DC_LK_KWOTY_OPLATY>
      <fml:DC_LK_MIN_WYSOKOSC_OPLATY?>{ data($req1/m1:minimumFeeAmount) }</fml:DC_LK_MIN_WYSOKOSC_OPLATY>
      <fml:DC_LK_CZEST_OPLATY_TOD_ACA?>{ data($req1/m1:acaFeeFrequency) }</fml:DC_LK_CZEST_OPLATY_TOD_ACA>
      <fml:DC_LK_OKR_DZIEN_OPL_TOD_ACA?>{ data($req1/m1:acaFeeSpecificDay) }</fml:DC_LK_OKR_DZIEN_OPL_TOD_ACA>
      <fml:DC_LK_DATA_NAST_OPLATY_ACA?>{ if ($nextAcaFeeDate) then xf:mapDate($nextAcaFeeDate) else () }</fml:DC_LK_DATA_NAST_OPLATY_ACA>
      <fml:DC_LK_ID_WNIOSKU?>{ data($transId) }</fml:DC_LK_ID_WNIOSKU>
      <fml:DC_LK_NR_UMOWY_KREDYTOWEJ?>{ data($req1/m1:agreementNumber) }</fml:DC_LK_NR_UMOWY_KREDYTOWEJ>
      <fml:DC_OKRESLONY_DZIEN_ODS?>{ data($req1/m1:interestSpecialDay) }</fml:DC_OKRESLONY_DZIEN_ODS>
      <fml:DC_OKRES_SPLAT_ODS?>{ data($req1/m1:interestPeriod/m3:Period/m3:period) }</fml:DC_OKRES_SPLAT_ODS>
      <fml:DC_LK_KOD_OPLATY_TOD_ACA?>{ data($req1/m1:acaFeeCode/m4:AcaFeeCode/m4:acaFeeCode) }</fml:DC_LK_KOD_OPLATY_TOD_ACA>
      <fml:DC_LK_OKRES_OPLATY_TOD_ACA?>{ data($req1/m1:acaFeePeriod/m3:Period/m3:period) }</fml:DC_LK_OKRES_OPLATY_TOD_ACA>
      <fml:DC_LK_NR_APLIKACJI>20</fml:DC_LK_NR_APLIKACJI>
    </fml:FML32>
  }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>