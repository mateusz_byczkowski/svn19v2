<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-04-06</con:description>
  <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:filtersandmessages.entities.be.dcl";

declare function local:mapprepareCheckHoldsForAccount2Response($fml as element())  as element(m:invokeResponse) {
 <m:invokeResponse>
  <m:response>
   <e:ResponseMessage>
    {	
      if($fml/fml:NF_RESPOM_RESULT) then 
        <e:result> {
           if(data($fml/fml:NF_RESPOM_RESULT) = "1") then "true"									
           else "false"
         } </e:result>
      else (),
      if($fml/fml:NF_RESPOM_ERRORCODE) then
         <e:errorCode>{ data($fml/fml:NF_RESPOM_ERRORCODE) }</e:errorCode>
      else(),	
      if($fml/fml:NF_RESPOM_ERRORDESCRIPTION) then <e:errorDescription>{ data($fml/fml:NF_RESPOM_ERRORDESCRIPTION) }</e:errorDescription>
      else()
    }
   </e:ResponseMessage>
  </m:response>
 </m:invokeResponse>		
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ local:mapprepareCheckHoldsForAccount2Response($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>