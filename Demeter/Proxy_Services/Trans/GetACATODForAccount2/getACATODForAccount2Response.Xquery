<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-01</con:description>
  <con:xquery><![CDATA[declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accountdict.dictionaries.be.dcl";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:errors.hlbsentities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";
declare namespace ns4="urn:entities.be.dcl";
declare namespace ns5="http://www.w3.org/2001/XMLSchema";
declare namespace ns6="urn:be.services.dcl";
declare namespace ns7="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;


declare function insertDate($value as xs:string?,$dateFormat as xs:string,$fieldName as xs:string) as element()?{
  if ($value)
    then if(string-length($value)>5 and not ($value = "0001-01-01"))
        then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
    else() 
  else()
};

declare function getElementsForTranAccountACAList($parm as element(fml:FML32)) as element()
{

<ns6:tranAccountACAList>
  {
    for $x at $occ in $parm/NF_FTODAC_FLAGTODACA
    return
    <ns0:TranAccountACA>
      (:<ns0:creationDate?>{data($parm/NF_TRAACA_CREATIONDATE[$occ])}</ns0:creationDate>:)
      {insertDate(data($parm/NF_TRAACA_CREATIONDATE[$occ]),"yyyy-MM-dd","ns0:creationDate")}
      (:<ns0:nextMaturityDate?>{data($parm/NF_TRAACA_NEXTMATURITYDATE[$occ])}</ns0:nextMaturityDate>:)
      {insertDate(data($parm/NF_TRAACA_NEXTMATURITYDATE[$occ]),"yyyy-MM-dd","ns0:nextMaturityDate")}  
      <ns0:limitAmount?>{data($parm/NF_TRAACA_LIMITAMOUNT[$occ])}</ns0:limitAmount>
      <ns0:indexNumber?>{data($parm/NF_TRAACA_INDEXNUMBER[$occ])}</ns0:indexNumber>
      <ns0:varianceForIndex?>{data($parm/NF_TRAACA_VARIANCEFORINDEX[$occ])}</ns0:varianceForIndex>
      <ns0:negotiatedInterestRate?>{data($parm/NF_TRAACA_NEGOTIATEDINTERE[$occ])}</ns0:negotiatedInterestRate>
      <ns0:authorisingOfficerCode?>{data($parm/NF_TRAACA_AUTHORISINGOFFIC[$occ])}</ns0:authorisingOfficerCode>
      <ns0:interestPdYTD?>{data($parm/NF_TRAACA_INTERESTPDYTD[$occ])}</ns0:interestPdYTD>
      <ns0:interestAccrUnpd?>{data($parm/NF_TRAACA_INTERESTACCRUNPD[$occ])}</ns0:interestAccrUnpd>
      <ns0:interestFrequency?>{data($parm/NF_TRAACA_INTERESTFREQUENC[$occ])}</ns0:interestFrequency>
      (:<ns0:nextInterestDate?>{data($parm/NF_TRAACA_NEXTINTERESTDATE[$occ])}</ns0:nextInterestDate>:)
      {insertDate(data($parm/NF_TRAACA_NEXTINTERESTDATE[$occ]),"yyyy-MM-dd","ns0:nextInterestDate")}
      <ns0:accrualBase?>{data($parm/NF_TRAACA_ACCRUALBASE[$occ])}</ns0:accrualBase>
      <ns0:yearBase?>{data($parm/NF_TRAACA_YEARBASE[$occ])}</ns0:yearBase>
      <ns0:interestAccuredLastDay?>{data($parm/NF_TRAACA_INTERESTACCUREDL[$occ])}</ns0:interestAccuredLastDay>
      <ns0:interestRate?>{data($parm/NF_TRAACA_INTERESTRATE[$occ])}</ns0:interestRate>
      <ns0:intAccrForPer?>{data($parm/NF_TRAACA_INTACCRFORPER[$occ])}</ns0:intAccrForPer>
      <ns0:accrualBalance?>{data($parm/NF_TRAACA_ACCRUALBALANCE[$occ])}</ns0:accrualBalance>
      (:<ns0:interestPdLast?>{data($parm/NF_TRAACA_INTERESTPDLAST[$occ])}</ns0:interestPdLast>:)
      {insertDate(data($parm/NF_TRAACA_INTERESTPDLAST[$occ]),"yyyy-MM-dd","ns0:interestPdLast")}
      <ns0:intAsOfLstRun?>{data($parm/NF_TRAACA_INTASOFLSTRUN[$occ])}</ns0:intAsOfLstRun>
      <ns0:usedLimitAmount?>{data($parm/NF_TRAACA_ACCRUALBALANCE[$occ])}</ns0:usedLimitAmount>
      (:<ns0:usedLimitAmount?>{data($parm/NF_TRAACA_USEDLIMITAMOUNT[$occ])}</ns0:usedLimitAmount>:)
      (:<ns0:dateLastRateChange?>{data($parm/NF_TRAACA_DATELASTRATECHAN[$occ])}</ns0:dateLastRateChange>:)
      {insertDate(data($parm/NF_TRAACA_DATELASTRATECHAN[$occ]),"yyyy-MM-dd","ns0:dateLastRateChange")}
      <ns0:subaccountNumber?>{data($parm/NF_TRAACA_SUBACCOUNTNUMBER[$occ])}</ns0:subaccountNumber>
      <ns0:feePercentage?>{data($parm/NF_TRAACA_FEEPERCENTAGE[$occ])}</ns0:feePercentage>
      <ns0:feeAmount?>{data($parm/NF_TRAACA_FEEAMOUNT[$occ])}</ns0:feeAmount>
      <ns0:minimumFeeAmount?>{data($parm/NF_TRAACA_MINIMUMFEEAMOUNT[$occ])}</ns0:minimumFeeAmount>
      <ns0:maximumFeeAmount?>{data($parm/NF_TRAACA_MAXIMUMFEEAMOUNT[$occ])}</ns0:maximumFeeAmount>
      (:<ns0:acaFeeFrequency?>{data($parm/NF_TRAACA_ACAFEEFREQUENCY[$occ])}</ns0:acaFeeFrequency>:)
      <ns0:acaFeeFrequency?>{data($parm/NF_FREQUE_FREQUENCY[$occ])}</ns0:acaFeeFrequency>
      <ns0:acaFeeSpecificDay?>{data($parm/NF_SPECID_SPECIALDAY[$occ])}</ns0:acaFeeSpecificDay>
      (:<ns0:nextAcaFeeDate?>{data($parm/NF_TRAACA_NEXTACAFEEDATE[$occ])}</ns0:nextAcaFeeDate>:)
      {insertDate(data($parm/NF_TRAACA_NEXTACAFEEDATE[$occ]),"yyyy-MM-dd","ns0:nextAcaFeeDate")}
      (:<ns0:cbReportCdrptDate?>{data($parm/NF_TRAACA_CBREPORTCDRPTDAT[$occ])}</ns0:cbReportCdrptDate>:)
      {insertDate(data($parm/NF_TRAACA_CBREPORTCDRPTDAT[$occ]),"yyyy-MM-dd","ns0:cbReportCdrptDate")}
      <ns0:intRate?>{data($parm/NF_TRAACA_INTRATE[$occ])}</ns0:intRate>
      <ns0:feesThisYear?>{data($parm/NF_TRAACA_FEESTHISYEAR[$occ])}</ns0:feesThisYear>
      <ns0:feesLastYear?>{data($parm/NF_TRAACA_FEESLASTYEAR[$occ])}</ns0:feesLastYear>
      <ns0:commitmentFee?>{data($parm/NF_TRAACA_COMMITMENTFEE[$occ])}</ns0:commitmentFee>
      <ns0:interestSpecialDay?>{data($parm/NF_SPECID_SPECIALDAY2[$occ])}</ns0:interestSpecialDay>
      <ns0:tranAccount>
        <ns0:TranAccount>
          <ns0:monthCredit?>{data($parm/NF_TRANA_MONTHCREDIT[$occ])}</ns0:monthCredit>
        </ns0:TranAccount>
      </ns0:tranAccount>
      <ns0:flagTODACA>
        <ns1:FlagTODACA>
          <ns1:flagTODACA?>{data($parm/NF_FTODAC_FLAGTODACA[$occ])}</ns1:flagTODACA>
        </ns1:FlagTODACA>
      </ns0:flagTODACA>
      <ns0:interestPeriod>
        <ns2:Period>
          <ns2:period?>{data($parm/NF_PERIOD_PERIOD[$occ])}</ns2:period>
        </ns2:Period>
      </ns0:interestPeriod>
      <ns0:acaFeeCode>
        <ns1:AcaFeeCode>
          <ns1:acaFeeCode?>{data($parm/NF_ACAFC_ACAFEECODE[$occ])}</ns1:acaFeeCode>
        </ns1:AcaFeeCode>
      </ns0:acaFeeCode>
      <ns0:acaFeePeriod>
        <ns2:Period>
          <ns2:period?>{data($parm/NF_PERIOD_PERIOD2[$occ])}</ns2:period>
        </ns2:Period>
      </ns0:acaFeePeriod>
      <ns0:renewTheLimit>
        <ns1:RenewTheLimit>
          <ns1:renewTheLimit?>{data($parm/NF_RENETL_RENEWTHELIMIT[$occ])}</ns1:renewTheLimit>
        </ns1:RenewTheLimit>
      </ns0:renewTheLimit>
      <ns0:signedAgreementForLimitInKO>
        <ns1:SignedAgreementForLimitInKO>
          <ns1:signedAgreementForLimitInKO?>{data($parm/NF_SAFLIK_SIGNEDAGREEMENTF[$occ])}</ns1:signedAgreementForLimitInKO>
        </ns1:SignedAgreementForLimitInKO>
      </ns0:signedAgreementForLimitInKO>
    </ns0:TranAccountACA>
  }
</ns6:tranAccountACAList>
};
declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

<ns6:invokeResponse>
  {getElementsForTranAccountACAList($parm)}
  <ns6:bcd>
    <ns4:BusinessControlData>
      <ns4:pageControl>
        <ns7:PageControl>
          <ns7:hasNext?>{data($parm/NF_PAGEC_HASNEXT)}</ns7:hasNext>
          <ns7:navigationKeyDefinition?>{data($parm/NF_PAGEC_NAVIGATIONKEYDEFI)}</ns7:navigationKeyDefinition>
          <ns7:navigationKeyValue?>{data($parm/NF_PAGEC_NAVIGATIONKEYVALU)}</ns7:navigationKeyValue>
        </ns7:PageControl>
      </ns4:pageControl>
    </ns4:BusinessControlData>
  </ns6:bcd>
  <ns6:tranAccount>
    <ns0:TranAccount>
      <ns0:totalLimitAmount?>{data($parm/NF_TRANA_TOTALLIMITAMOUNT[1])}</ns0:totalLimitAmount>
    </ns0:TranAccount>
  </ns6:tranAccount>
</ns6:invokeResponse>
};

<soap:Body>
  {getElementsForInvokeResponse($body/FML32)}
</soap:Body>]]></con:xquery>
</con:xqueryEntry>