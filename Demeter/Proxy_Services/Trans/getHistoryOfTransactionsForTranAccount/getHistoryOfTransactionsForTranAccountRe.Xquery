<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-14</con:description>
  <con:xquery><![CDATA[declare namespace fml="";
declare namespace ns0="urn:be.services.dcl";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="http://www.w3.org/2001/XMLSchema";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6="urn:baseauxentities.be.dcl";
declare namespace ns7="urn:operations.entities.be.dcl";
declare namespace ns8="urn:entities.be.dcl";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

(: T35030 - zamiana true<->false przy mapowaniu NF_PAGEC_REVERSEORDER :)

declare function boolean2SourceValue ($parm as xs:string?,$trueval as xs:string,$falseval as xs:string) as xs:string{
    if ($parm  = "true")
       then $trueval
    else if ($parm  = "1")
       then $trueval
    else $falseval
};

declare function getFieldsFromHeader($parm as element(ns0:header)) as element()*
{

<NF_MSHEAD_MSGID?>{data($parm/ns0:msgHeader/ns0:msgId)}</NF_MSHEAD_MSGID>
,
<NF_MSHEAD_COMPANYID?>{data($parm/ns0:msgHeader/ns0:companyId)}</NF_MSHEAD_COMPANYID>
,
<NF_MSHEAD_UNITID?>{data($parm/ns0:msgHeader/ns0:unitId)}</NF_MSHEAD_UNITID>
,
<NF_MSHEAD_USERID?>{data($parm/ns0:msgHeader/ns0:userId)}</NF_MSHEAD_USERID>
,
<NF_MSHEAD_APPID?>{data($parm/ns0:msgHeader/ns0:appId)}</NF_MSHEAD_APPID>
,
<NF_MSHEAD_TIMESTAMP?>{data($parm/ns0:msgHeader/ns0:timestamp)}</NF_MSHEAD_TIMESTAMP>
,
<NF_TRHEAD_TRANSID?>{data($parm/ns0:transHeader/ns0:transId)}</NF_TRHEAD_TRANSID>
};
declare function getFieldsFromInvoke($parm as element(ns0:invoke)) as element()*
{

<NF_ACCOUN_ACCOUNTNUMBER?>{data($parm/ns0:account/ns4:Account/ns4:accountNumber)}</NF_ACCOUN_ACCOUNTNUMBER>
,
<NF_PAGEC_ACTIONCODE?>{data($parm/ns0:bcd/ns8:BusinessControlData/ns8:pageControl/ns6:PageControl/ns6:actionCode)}</NF_PAGEC_ACTIONCODE>
,
<NF_PAGEC_PAGESIZE?>{data($parm/ns0:bcd/ns8:BusinessControlData/ns8:pageControl/ns6:PageControl/ns6:pageSize)}</NF_PAGEC_PAGESIZE>
,
<NF_PAGEC_REVERSEORDER?>{boolean2SourceValue(data($parm/ns0:bcd/ns8:BusinessControlData/ns8:pageControl/ns6:PageControl/ns6:reverseOrder), "1", "0")}</NF_PAGEC_REVERSEORDER>
,
<NF_PAGEC_NAVIGATIONKEYDEFI?>{data($parm/ns0:bcd/ns8:BusinessControlData/ns8:pageControl/ns6:PageControl/ns6:navigationKeyDefinition)}</NF_PAGEC_NAVIGATIONKEYDEFI>
,
<NF_PAGEC_NAVIGATIONKEYVALU?>{data($parm/ns0:bcd/ns8:BusinessControlData/ns8:pageControl/ns6:PageControl/ns6:navigationKeyValue)}</NF_PAGEC_NAVIGATIONKEYVALU>
,
<NF_FILTEH_STARTDATE?>{data($parm/ns0:filter/ns3:FilterHistory/ns3:startDate)}</NF_FILTEH_STARTDATE>
,
<NF_FILTEH_ENDDATE?>{data($parm/ns0:filter/ns3:FilterHistory/ns3:endDate)}</NF_FILTEH_ENDDATE>
,
if (string-length(data($parm/ns0:filter/ns3:FilterHistory/ns3:transactionType))>0) then
<NF_FILTEH_TRANSACTIONTYPE>{boolean2SourceValue(data($parm/ns0:filter/ns3:FilterHistory/ns3:transactionType), "1", "0")}</NF_FILTEH_TRANSACTIONTYPE>
else ()

,
<NF_FILTEH_AMOUNTMIN?>{data($parm/ns0:filter/ns3:FilterHistory/ns3:amountMin)}</NF_FILTEH_AMOUNTMIN>
,
<NF_FILTEH_AMOUNTMAX?>{data($parm/ns0:filter/ns3:FilterHistory/ns3:amountMax)}</NF_FILTEH_AMOUNTMAX>
};

<soap:Body>
  <fml:FML32>
    {getFieldsFromHeader($header/ns0:header)}
    {getFieldsFromInvoke($body/ns0:invoke)}
  </fml:FML32>
</soap:Body>]]></con:xquery>
</con:xqueryEntry>