<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-10-20</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/prime/";



declare function xf:map_CreateInstOnBalRequest($fml as element(fml:FML32))
	as element(urn:CreateInstOnBal) {
		<urn:CreateInstOnBal>
			<urn:accountNumber>{ data($fml/fml:B_NR_RACH)}</urn:accountNumber>
			<urn:instalmentAmount>{ data($fml/fml:B_KWOTA_KRED)}</urn:instalmentAmount>
			<urn:periodLength>{ data($fml/fml:B_L_RAT)}</urn:periodLength>
			<urn:interestProfile>{ data($fml/fml:B_NR_IND)}</urn:interestProfile>
			<urn:userName>{ data($fml/fml:B_USER_ID)}</urn:userName>
			<urn:options>{ data($fml/fml:B_OPCJA)}</urn:options>
		</urn:CreateInstOnBal>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:map_CreateInstOnBalRequest($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>