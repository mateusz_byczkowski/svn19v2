<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2009-08-26 PKL NP1995 Dodanie pól ProductStat i ProductStatusCode</con:description>
  <con:xquery>(: Change Log :)
(: v.1.1.0 2009-08-26 PKL NP1995 Dodanie pól ProductStat i ProductStatusCode :)

declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductsArea($fml as element(fml:FML32))
	as element(m:PRIMEGetCustProductsArea) {
                let $cif := data($fml/fml:NF_CUSTOM_CUSTOMERNUMBER)
                let $cifWithoutZeros := replace($cif,'^0+','')
                return
		&lt;m:PRIMEGetCustProductsArea&gt;
			{
				if($fml/fml:NF_MSHEAD_USERID)
					then &lt;m:CI_ID_WEW_PRAC&gt;{ data($fml/fml:NF_MSHEAD_USERID) }&lt;/m:CI_ID_WEW_PRAC&gt;
					else  &lt;m:CI_ID_WEW_PRAC&gt;1&lt;/m:CI_ID_WEW_PRAC&gt;
			}
			{
				if($fml/fml:NF_MSHEAD_COMPANYID)
					then &lt;m:CI_ID_SPOLKI&gt;{ data($fml/fml:NF_MSHEAD_COMPANYID) }&lt;/m:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($fml/fml:NF_CUSTOM_CUSTOMERNUMBER)
					then &lt;m:DC_NUMER_KLIENTA&gt;{ $cif }&lt;/m:DC_NUMER_KLIENTA&gt;
					else ()
			}
			(:{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then &lt;m:DC_NR_DOWODU_REGON&gt;{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:DC_NR_DOWODU_REGON&gt;
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then &lt;m:DC_NIP&gt;{ data($fml/fml:DC_NIP) }&lt;/m:DC_NIP&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then &lt;m:DC_NR_PESEL&gt;{ data($fml/fml:DC_NR_PESEL) }&lt;/m:DC_NR_PESEL&gt;
					else ()
			}:)
			{
				if($fml/fml:NF_PRODUA_CODEPRODUCTAREA)
					then &lt;m:CI_PRODUCT_AREA_CODE&gt;{ data($fml/fml:NF_PRODUA_CODEPRODUCTAREA) }&lt;/m:CI_PRODUCT_AREA_CODE&gt;
					else 	&lt;m:CI_PRODUCT_AREA_CODE&gt;0&lt;/m:CI_PRODUCT_AREA_CODE&gt;
			}
(: NP1995 początek :)		{
				if($fml/fml:NF_CTRL_ACTIVENONACTIVE)
					then &lt;m:NF_CTRL_ACTIVENONACTIVE&gt;{ data($fml/fml:NF_CTRL_ACTIVENONACTIVE) }&lt;/m:NF_CTRL_ACTIVENONACTIVE&gt;
					else 	&lt;m:NF_CTRL_ACTIVENONACTIVE&gt;0&lt;/m:NF_CTRL_ACTIVENONACTIVE&gt;
(: NP1995 koniec :)		}
                       (:{
				if($fml/fml:CI_OPCJA)
					then &lt;m:CI_OPCJA&gt;{ data($fml/fml:CI_OPCJA) }&lt;/m:CI_OPCJA&gt;
					else ()
			}:)
                      &lt;m:CI_OPCJA&gt;1&lt;/m:CI_OPCJA&gt;


		&lt;/m:PRIMEGetCustProductsArea&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapPRIMEGetCustProductsArea($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>