<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soapenv="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare variable $cif as xs:string external;



<soapenv:Body>
    <soapenv:Fault>
         <faultcode>soapenv:Server.userException</faultcode>
         <faultstring>err:ServiceFailException</faultstring>
         <detail>
            <err:ServiceFailException>
              <err:exceptionItem>
               <err:errorCode1>12</err:errorCode1>
               <err:errorCode2>0</err:errorCode2>
               <err:errorDescription>Błąd przenoszania danych klienta {$cif} z CIS. Nieudane wywołanie usługi cCustClone</err:errorDescription>
              </err:exceptionItem> 
            </err:ServiceFailException >
         </detail>
</soapenv:Fault>

</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>