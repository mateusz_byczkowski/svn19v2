<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:
  : @author  Kacper Pawlaczyk
  : @version 1.4
  : @since   2009-12-15
  :
  : wersja WSDLa: 25-05-2009 13:36:55
  :
  : $Proxy Services/Branches/chgBranchStatus/chgBranchStatusResponse.xq$
  :
  :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Branches/chgBranchStatus/chgBranchStatusResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";
declare namespace ns4 = "";

declare variable $fML321 as element(ns4:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:chgBranchStatusResponse($fML321 as element(ns4:FML32))
    as element(ns3:invokeResponse)
{
    &lt;ns3:invokeResponse&gt;
        &lt;ns3:response&gt;
            &lt;ns0:ResponseMessage&gt;
				&lt;ns0:result&gt;
					true
				&lt;/ns0:result&gt;
			&lt;/ns0:ResponseMessage&gt;
        &lt;/ns3:response&gt;
              
		&lt;ns3:branchCurrentStatusOut&gt;
				&lt;ns2:BranchCurrentStatus&gt;
				
				(:
				 : konwersja do formatu dateTime (W3C)
				 :)
				{
					if (data($fML321/ns4:TR_DATA_DZIALANIA)) then
						&lt;ns2:lastChangeDate&gt;{
							let $date := $fML321/ns4:TR_DATA_DZIALANIA
							return
								
								fn:concat(fn:substring($date, 1, 10),
										  'T',
										  fn:substring($date, 12, 2),
										  ':',
										  fn:substring($date, 15, 2),
										  ':',
										  fn:substring($date, 18, 2))
						}&lt;/ns2:lastChangeDate&gt;
					else ()
				}
				
				(:
				 : 0 --&gt; 'C' oddział zamknięty
				 : 1 --&gt; 'O' oddział otwarty
				 :)
				{
					let $branchStatus := data($fML321/ns4:TR_STAN_ODDZIALU)
					return
						if ($branchStatus) then
							if ($branchStatus eq '0') then
								&lt;ns2:status&gt;
									&lt;ns1:BranchStatus&gt;
										&lt;ns1:branchStatus&gt;
											C
										&lt;/ns1:branchStatus&gt;
									&lt;/ns1:BranchStatus&gt;
								&lt;/ns2:status&gt;
							else if ($branchStatus eq '1') then
								&lt;ns2:status&gt;
									&lt;ns1:BranchStatus&gt;
										&lt;ns1:branchStatus&gt;
											O
										&lt;/ns1:branchStatus&gt;
									&lt;/ns1:BranchStatus&gt;
								&lt;/ns2:status&gt;
							else
								()								
						else
							()
				}
				
			&lt;/ns2:BranchCurrentStatus&gt;
		&lt;/ns3:branchCurrentStatusOut&gt;
    &lt;/ns3:invokeResponse&gt;
};

&lt;soap-env:Body&gt;{
	xf:chgBranchStatusResponse($fML321)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>