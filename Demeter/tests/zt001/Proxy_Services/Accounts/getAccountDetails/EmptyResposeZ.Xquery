<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Pusta odpowiedź dla konta zewnętrznego</con:description>
  <con:xquery><![CDATA[<soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
<FML32>
	<PT_FIRST_PRODUCT_FEATURE/>
	<PT_SECOND_PRODUCT_FEATURE/>
	<PT_THIRD_PRODUCT_FEATURE/>	
	<PT_FOURTH_PRODUCT_FEATURE/>
	<PT_FIFTH_PRODUCT_FEATURE/>
	<PT_SOURCE_PRODUCT_CODE/>
	<NF_TXNAD_OWNERCIF/>
	<NF_TXNAD_ACCOUNTNAME/>
	<NF_TXNAD_OWNERNAME/>
	<NF_TXNAD_OWNERNAMESECOND/>
	<NF_TXNAD_OWNERADDRES/>
	<NF_TXNAD_OWNERZIPCODE/>
	<NF_TXNAD_OWNERCITY/>
	<NF_CURREC_CURRENCYCODE/>	
	<NF_ACCEST_ACCESSTYPE/>
	<NF_TXNAD_SHOWBALANCE>1</NF_TXNAD_SHOWBALANCE>
	<NF_TXNAD_CURRENTBALANCE>0</NF_TXNAD_CURRENTBALANCE>
	<NF_TXNAD_AVAILABLEBALANCE>0</NF_TXNAD_AVAILABLEBALANCE>
	<NF_ACCOUS_ACCOUNTSTATUS/>
	<NF_TXNAD_ACCOUNTSPECIFICTY>Z</NF_TXNAD_ACCOUNTSPECIFICTY>
	<NF_TXNAD_OWNERDATAEDITABLE>1</NF_TXNAD_OWNERDATAEDITABLE>
</FML32>
</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>