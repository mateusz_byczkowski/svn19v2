<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetQuestionnaireDataResponse($fml as element(fml:FML32))
	as element(m:CRMGetQuestionnaireDataResponse) {
            &lt;m:CRMGetQuestionnaireDataResponse&gt;
                        {
				let $CI_NUMER_PACZKI := $fml/fml:CI_NUMER_PACZKI return
					if($CI_NUMER_PACZKI)
						then &lt;m:NumerPaczki&gt;{ data($CI_NUMER_PACZKI) }&lt;/m:NumerPaczki&gt;
						else ()
			}
			{
                                let $CI_ID_ANKIETY := $fml/fml:CI_ID_ANKIETY
                                let $CI_SKP_AUTORA := $fml/fml:CI_SKP_AUTORA
                                let $CI_DATA_ANKIETY := $fml/fml:CI_DATA_ANKIETY
                                let $CI_ANKIETA_XML := $fml/fml:CI_ANKIETA_XML
                                let $CI_LFR := $fml/fml:CI_LFR

				for $it at $p in $fml/fml:CI_ID_ANKIETY
                                      return
                                            &lt;m:Questionnaire&gt;
                                                          {
                                                             if ($CI_ID_ANKIETY[$p]) then
                                                                     &lt;m:QuestId&gt;{ data($CI_ID_ANKIETY[$p]) }&lt;/m:QuestId&gt;
                                                             else ()
                                                          }
                                                          {
                                                             if ($CI_SKP_AUTORA[$p]) then
                                                                     &lt;m:UsrSkpNo&gt;{ data($CI_SKP_AUTORA[$p]) }&lt;/m:UsrSkpNo&gt;
                                                             else ()
                                                          }
                                                          {
                                                             if ($CI_DATA_ANKIETY[$p]) then
                                                                     &lt;m:QuestionnaireDate&gt;{ data($CI_DATA_ANKIETY[$p]) }&lt;/m:QuestionnaireDate&gt;
                                                             else ()
                                                          }
                                                          {
                                                             if ($CI_LFR[$p]) then
                                                                     &lt;m:LfrFlag&gt;{ data($CI_LFR[$p]) }&lt;/m:LfrFlag&gt;
                                                             else ()
                                                          }
                                                          {
                                                             if ($CI_ANKIETA_XML[$p]) then
                                                                     &lt;m:XmlQuestionnaire&gt;{ data($fml/fml:CI_ANKIETA_XML[$p]) }&lt;/m:XmlQuestionnaire&gt;
                                                             else ()
                                                          }
                                            &lt;/m:Questionnaire&gt;   
                        }

		&lt;/m:CRMGetQuestionnaireDataResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetQuestionnaireDataResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>