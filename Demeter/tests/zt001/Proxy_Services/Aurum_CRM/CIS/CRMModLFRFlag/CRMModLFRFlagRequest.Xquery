<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMModLFRFlagRequest($req as element(m:modLFRFlagRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:EmpSKPNo)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ&gt;{ data($req/m:EmpSKPNo) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ&gt;
					else ()
			}
			{
				if($req/m:CustCIF)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:CustCIF) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:FlagValue)
					then &lt;fml:CI_LFR&gt;{ data($req/m:FlagValue) }&lt;/fml:CI_LFR&gt;
					else ()
			}			
			{
				if($req/m:CompanyId)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:CompanyId) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:BranchId)
					then &lt;fml:CI_NUMER_ODDZIALU&gt;{ data($req/m:BranchId) }&lt;/fml:CI_NUMER_ODDZIALU&gt;
					else ()
			}

                        &lt;fml:CI_SKP_PRACOWNIKA/&gt;
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMModLFRFlagRequest($body/m:modLFRFlagRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>