<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:crddict.dictionaries.be.dcl";
declare namespace ns2="urn:accounts.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:be.services.dcl";
declare namespace ns8="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function insertDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)&gt;5 and not ($value = "0001-01-01") and not ($value = "01-01-0001")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };


declare function maskCardNbr($value as xs:anyType,$ins as xs:string) as xs:anyType{
   
       let $bin := substring($value,1,6)
       let $ixsy := substring($value,7,6)
       let $kon := substring($value,13)
       
       return
         concat($bin, concat($ins, $kon))
      };

declare function chgDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{

    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)

    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 


};

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns7:invokeResponse&gt;
  &lt;ns7:response&gt;
    &lt;ns0:ResponseMessage&gt;
	  {
       if (data($parm/DC_OPIS_BLEDU)) then
          &lt;ns0:result?&gt;false&lt;/ns0:result&gt;
      else
           &lt;ns0:result?&gt;true&lt;/ns0:result&gt;
      }
    &lt;/ns0:ResponseMessage&gt;
  &lt;/ns7:response&gt;
  &lt;ns7:debitCardOut&gt;
    &lt;ns8:DebitCard&gt;
       {if ($parm/DC_NR_KARTY) then
      &lt;ns8:cardNbr?&gt;{data($parm/DC_NR_KARTY)}&lt;/ns8:cardNbr&gt;
         else()
       }
	  { insertDate(data($parm/DC_DATA_POCZ),"yyyy-MM-dd","ns8:cardRecordOpenDate")}
      &lt;ns8:cardFee?&gt;{data($parm/DC_KWOTA)}&lt;/ns8:cardFee&gt;
      &lt;ns8:virtualCardNbr?&gt;{data($parm/DC_NR_KARTY_BIN)}&lt;/ns8:virtualCardNbr&gt;
      &lt;ns8:cardStatus&gt;
        &lt;ns1:CrdStatus&gt;
          &lt;ns1:crdStatus?&gt;{data($parm/DC_STATUS_KARTY)}&lt;/ns1:crdStatus&gt;
        &lt;/ns1:CrdStatus&gt;
      &lt;/ns8:cardStatus&gt;

{
      if(data($parm/DC_NR_PB1)) then
         if(string-length(data($parm/DC_NR_PB1))=16) then
           if(number(data($parm/DC_NR_PB1))&gt;0) then
	  &lt;ns8:debitCardLpInfo&gt;
             &lt;ns8:DebitCardLpInfo&gt;
                  &lt;ns8:lpNumber?&gt;{data($parm/DC_NR_PB1)}&lt;/ns8:lpNumber&gt;
              &lt;/ns8:DebitCardLpInfo&gt;
          &lt;/ns8:debitCardLpInfo&gt;
          else()
         else()
      else()
}

    &lt;/ns8:DebitCard&gt;
  &lt;/ns7:debitCardOut&gt;
&lt;/ns7:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>