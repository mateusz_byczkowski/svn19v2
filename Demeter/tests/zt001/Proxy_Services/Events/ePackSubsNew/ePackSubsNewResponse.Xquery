<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/epacksubsnew/messages/";
declare namespace xf = "http://bzwbk.com/services/epacksubsnew/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapePackSubsNewResponse($fml as element(fml:FML32))
	as element(m:ePackSubsNewResponse) {
		&lt;m:ePackSubsNewResponse&gt;
		&lt;/m:ePackSubsNewResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapePackSubsNewResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>