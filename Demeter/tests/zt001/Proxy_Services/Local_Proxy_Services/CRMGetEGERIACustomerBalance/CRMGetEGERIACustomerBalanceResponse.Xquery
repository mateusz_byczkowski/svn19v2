<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappCRMGetEGERIACustomerBalanceResponse($fml as element(fml:FML32))
	as element(m:CRMGetEGERIACustomerBalanceResponse) {
  &lt;m:CRMGetEGERIACustomerBalanceResponse&gt;
    {
      if($fml/fml:EG_STAN_NA_DATACZAS)
        then &lt;m:StanNaDataCzas&gt;{ data($fml/fml:EG_STAN_NA_DATACZAS) }&lt;/m:StanNaDataCzas&gt;
      else ()
    } 
    {
      if($fml/fml:EG_ILE_ZWROCONYCH_REK)
        then &lt;m:IloscZwroconychRekordow&gt;{ data($fml/fml:EG_ILE_ZWROCONYCH_REK) }&lt;/m:IloscZwroconychRekordow&gt;
      else ()
    }
    {
      let $EG_ID_SPOLKI := $fml/fml:EG_ID_SPOLKI
      let $EG_NR_RACH_WPLATY := $fml/fml:EG_NR_RACH_WPLATY
      let $EG_KOD_WALUTY := $fml/fml:EG_KOD_WALUTY
      let $EG_KWOTA_LACZNA:= $fml/fml:EG_KWOTA_LACZNA
      let $EG_KWOTA_PRZETERMINOWAN := $fml/fml:EG_KWOTA_PRZETERMINOWAN

      for $it at $p in $fml/fml:EG_NR_RACH_WPLATY return
      &lt;m:EgeriaProduct&gt;	
         {
          if ($EG_ID_SPOLKI[$p])
            then if (data($EG_ID_SPOLKI[$p])="L")
              then &lt;m:NazwaSpolki&gt;BZWBK Leasing S.A.&lt;/m:NazwaSpolki&gt;
            elseif (data($EG_ID_SPOLKI[$p])="FL")
              then &lt;m:NazwaSpolki&gt;BZWBK Finanse &amp; Leasing S.A.&lt;/m:NazwaSpolki&gt;
            else ()
          else ()
         }
        {
          if($EG_NR_RACH_WPLATY[$p])
            then &lt;m:NrRachunkuWplaty&gt;{ data($EG_NR_RACH_WPLATY[$p]) }&lt;/m:NrRachunkuWplaty&gt;
          else ()
        }
        {
          if($EG_KOD_WALUTY[$p])
            then &lt;m:Waluta&gt;{ data($EG_KOD_WALUTY[$p]) }&lt;/m:Waluta&gt;
          else ()
        }
        {
          if($EG_KWOTA_LACZNA[$p])
            then &lt;m:KwotaLaczna&gt;{ data($EG_KWOTA_LACZNA[$p]) }&lt;/m:KwotaLaczna&gt;
          else ()
        }
        {
          if($EG_KWOTA_PRZETERMINOWAN[$p])
            then &lt;m:KwotaPrzeterminowan&gt;{ data($EG_KWOTA_PRZETERMINOWAN[$p]) }&lt;/m:KwotaPrzeterminowan&gt;
          else ()
        }
      &lt;/m:EgeriaProduct&gt;
    }
  &lt;/m:CRMGetEGERIACustomerBalanceResponse&gt;
};


declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mappCRMGetEGERIACustomerBalanceResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>