<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace dcl="urn:be.services.dcl";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace ns3="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaries.be.dcl";

declare function xf:DateTime2CYMD($indate as xsd:string ) as xsd:string{
      substring($indate,1,10)
};


declare function chkUnitId($unitId as xs:anyType) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};


declare function xf:mapAddTrustAccountRequest($req as element(ns1:Account), $msghead as element(dcl:msgHeader), $tranhead as element(dcl:transHeader))
	as element(fml:FML32) {

let $unitId := $msghead/dcl:unitId
let $transId:=$tranhead/dcl:transId
let $customerNumber:= $req/ns1:customerNumber
let $statementForAccount:= $req/ns1:statementForAccount
let $skpOpener:=$req/ns1:skpOpener
let $productCode:=$req/ns1:productCode      
let $officerCode:=$req/ns1:officerCode  
let $serviceChargeCode:=$req/ns1:tranAccount/ns1:TranAccount/ns1:serviceChargeCode
let $interestPlanNumber:=$req/ns1:tranAccount/ns1:TranAccount/ns1:interestPlanNumber
let $frequency:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:frequency
let $nextPrintoutDate:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:nextPrintoutDate
let $aggregateTransaction:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:aggregateTransaction
let $cycle:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:cycle/ns2:Period/ns2:period
(: let $specialDay:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:specialDay/ns2:SpecialDay/ns2:specialDay :)
let $specialDay:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:specialDay
let $provideManner:=$req/ns1:statementParameters/ns1:StatementParameters/ns1:provideManner/ns2:ProvideManner/ns2:provideManner
let $currencyCode:= $req/ns1:currency/ns2:CurrencyCode/ns2:currencyCode
let $accountBranchNumber:=$req/ns1:accountBranchNumber/ns2:BranchCode/ns2:branchCode
	
return
&lt;fml:FML32&gt;
    &lt;DC_TRN_ID?&gt;{data($transId)}&lt;/DC_TRN_ID&gt;
    &lt;DC_UZYTKOWNIK?&gt;{concat("SKP:",data($skpOpener))}&lt;/DC_UZYTKOWNIK&gt;
    &lt;DC_ODDZIAL?&gt;{chkUnitId(data($unitId))}&lt;/DC_ODDZIAL&gt;
    &lt;DC_NUMER_KLIENTA?&gt;{data($customerNumber)}&lt;/DC_NUMER_KLIENTA&gt;
    &lt;DC_KONTROLA_WYCIAGOW?&gt;{data($statementForAccount)}&lt;/DC_KONTROLA_WYCIAGOW&gt; 
    &lt;DC_RELACJA?&gt;SOW&lt;/DC_RELACJA&gt;
    &lt;DC_TYP_RELACJI?&gt;1&lt;/DC_TYP_RELACJI&gt;
    &lt;DC_PROCENT_ZOB_PODATKOWYCH?&gt;100.0000&lt;/DC_PROCENT_ZOB_PODATKOWYCH&gt;
    &lt;DC_NUMER_PRODUKTU?&gt;{data($productCode)}&lt;/DC_NUMER_PRODUKTU&gt;
    &lt;DC_NUMER_ODDZIALU?&gt;{data($accountBranchNumber)}&lt;/DC_NUMER_ODDZIALU&gt;
    &lt;DC_RODZAJ_RACHUNKU?&gt;DM&lt;/DC_RODZAJ_RACHUNKU&gt;
    &lt;DC_WALUTA?&gt;{data($currencyCode )}&lt;/DC_WALUTA&gt;        
    &lt;DC_PLAN_OPLAT?&gt;{data($serviceChargeCode)}&lt;/DC_PLAN_OPLAT&gt;
    &lt;DC_PLAN_ODSETKOWY?&gt;{data($interestPlanNumber)}&lt;/DC_PLAN_ODSETKOWY&gt;          
    &lt;DC_KOD_CYKLU_WYCIAGOW?&gt;{data($cycle)}&lt;/DC_KOD_CYKLU_WYCIAGOW&gt;
    &lt;DC_LICZBA_OKRESOW_CYKL_WYCIAG?&gt;{data($frequency)}&lt;/DC_LICZBA_OKRESOW_CYKL_WYCIAG&gt;
    {
               if ( string-length($officerCode)&gt;0 )
                 then &lt;DC_KOD_PRACOWNIKA&gt;{data($officerCode)}&lt;/DC_KOD_PRACOWNIKA&gt;
                 else  &lt;DC_KOD_PRACOWNIKA&gt;DCL&lt;/DC_KOD_PRACOWNIKA&gt;                 
     } 
    { if ($nextPrintoutDate) then
    &lt;DC_DATA_NASTEPNEGO_WYCIAGU?&gt;{xf:DateTime2CYMD(data($nextPrintoutDate))}&lt;/DC_DATA_NASTEPNEGO_WYCIAGU&gt;					
      else ()
    }
    &lt;DC_OKRESLONY_DZIEN_WYCIAGU?&gt;{data($specialDay)}&lt;/DC_OKRESLONY_DZIEN_WYCIAGU&gt;
   &lt;DC_KOD_SPECJALNYCH_INSTRUKCJI?&gt;{data($provideManner)}&lt;/DC_KOD_SPECJALNYCH_INSTRUKCJI&gt;			
   { if ($aggregateTransaction)
                 then  if (data($aggregateTransaction) = "true")
                   then &lt;DC_ZBIJANIE_TRANSAKCJI?&gt;1&lt;/DC_ZBIJANIE_TRANSAKCJI&gt;
                   else &lt;DC_ZBIJANIE_TRANSAKCJI?&gt;0&lt;/DC_ZBIJANIE_TRANSAKCJI&gt;
                 else()
   }			
          &lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;


&lt;soap-env:Body&gt;
{ xf:mapAddTrustAccountRequest($body/dcl:invoke/dcl:account/ns1:Account, $header/dcl:header/dcl:msgHeader, $header/dcl:header/dcl:transHeader) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>