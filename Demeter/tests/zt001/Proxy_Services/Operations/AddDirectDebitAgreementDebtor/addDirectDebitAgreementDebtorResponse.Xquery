<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns1="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:directdebit.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:errors.hlbsentities.be.dcl";
declare namespace ns4="urn:dictionaries.be.dcl";
declare namespace ns6="http://www.w3.org/2001/XMLSchema";
declare namespace ns7="urn:be.services.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns7:invokeResponse&gt;
  &lt;ns7:response&gt;
    &lt;ns0:ResponseMessage&gt;
    {
          if (data($parm/DC_OPIS_BLEDU[1])) then
            &lt;ns0:result?&gt;false&lt;/ns0:result&gt;
         else
            &lt;ns0:result?&gt;true&lt;/ns0:result&gt;      
     }
    &lt;/ns0:ResponseMessage&gt;
  &lt;/ns7:response&gt;
  &lt;ns7:directDebitOut&gt;
    &lt;ns2:DirectDebit&gt;
      &lt;ns2:directDebitId?&gt;{data($parm/NF_DIRECD_DIRECTDEBITID)}&lt;/ns2:directDebitId&gt;
    &lt;/ns2:DirectDebit&gt;
  &lt;/ns7:directDebitOut&gt;
&lt;/ns7:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>