<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function insertDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
      if ($value)
        then if(string-length($value)&gt;5 and not ($value = "01-01-0001") and (substring($value,3,1)="-"))
            then chgDate($value, $dateFormat, $fieldName)
        else if(string-length($value)&gt;5 and not ($value = "0001-01-01")) 
           then element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat,$value)}
        else()
      else()
      };
 
declare function chgDate($value as xs:anyType,$dateFormat as xs:string,$fieldName as xs:string) as xs:anyType{
 
    let $dd := substring($value,1,2)
    let $mm := substring($value,4,2)
    let $rrrr := substring($value,7,4)
 
    return 
         element  {$fieldName}  {fn-bea:date-from-string-with-format($dateFormat, concat($rrrr,concat("-", concat($mm, concat("-",$dd)))))} 
};

declare function xf:mapgetCustProdsForAdvisoryResponse($fml as element(fml:FML32))
  as element(m:getCustProdsForAdvisoryResponse) {
    &lt;m:getCustProdsForAdvisoryResponse&gt;
      {for $it at $p in $fml/fml:NF_PRODUA_CODEPRODUCTAREA
        return
        &lt;m:CUSTOMER_PRODUCTS&gt;
          {if($fml/fml:NF_PRODAT_IDPRODUCTDEFINIT[$p])
            then &lt;m:ProdatIdproductdefinit&gt;{ data($fml/fml:NF_PRODAT_IDPRODUCTDEFINIT[$p]) }&lt;/m:ProdatIdproductdefinit&gt;
            else ()
          }
          {if($fml/fml:PT_ID_GROUP[$p])
            then &lt;m:ProgahIdproductgroup&gt;{ data($fml/fml:PT_ID_GROUP[$p]) }&lt;/m:ProgahIdproductgroup&gt;
            else ()
          }
          {if($fml/fml:NF_PRODUG_RECEIVER[$p])
            then &lt;m:ProdugReceiver&gt;{ data($fml/fml:NF_PRODUG_RECEIVER[$p]) }&lt;/m:ProdugReceiver&gt;
            else ()
          }
          {if($fml/fml:NF_ACCOUR_RELATIONSHIP[$p])
            then &lt;m:AccourRelationship&gt;{ data($fml/fml:NF_ACCOUR_RELATIONSHIP[$p]) }&lt;/m:AccourRelationship&gt;
            else ()
          }
          {if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p])
            then &lt;m:ProduaCodeproductarea&gt;{ data($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p]) }&lt;/m:ProduaCodeproductarea&gt;
            else ()
          }
          {if($fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI[$p])
            then &lt;m:AccounAccountdescripti&gt;{ data($fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI[$p]) }&lt;/m:AccounAccountdescripti&gt;
            else ()
          }
          {if($fml/fml:NF_ACCOUN_ACCOUNTNUMBER[$p])
            then &lt;m:AccounAccountnumber&gt;{ fn:substring(data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER[$p]), fn:string-length(data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER[$p]))-9, 10) }&lt;/m:AccounAccountnumber&gt;
            else ()
          }
          {if($fml/fml:NF_CURREC_CURRENCYCODE[$p])
            then &lt;m:CurrecCurrencycode&gt;{ data($fml/fml:NF_CURREC_CURRENCYCODE[$p]) }&lt;/m:CurrecCurrencycode&gt;
            else ()
          }
          {if($fml/fml:NF_ACCOUN_CURRENTBALANCE[$p])
            then &lt;m:AccounCurrentbalance&gt;{ data($fml/fml:NF_ACCOUN_CURRENTBALANCE[$p]) }&lt;/m:AccounCurrentbalance&gt;
            else ()
          }
          {if($fml/fml:NF_ACCOUN_CURRENTBALANCEPL[$p])
            then &lt;m:AccounCurrentbalancepl&gt;{ data($fml/fml:NF_ACCOUN_CURRENTBALANCEPL[$p]) }&lt;/m:AccounCurrentbalancepl&gt;
            else ()
          }
          {if($fml/fml:NF_ACCOUN_DEBITPERCENTAGE[$p])
            then &lt;m:AccounPercentage&gt;{ data($fml/fml:NF_ACCOUN_DEBITPERCENTAGE[$p]) }&lt;/m:AccounPercentage&gt;
            else ()
          }
          {if($fml/fml:NF_PRODUCT_STAT[$p])
            then &lt;m:ProductStat&gt;{ data($fml/fml:NF_PRODUCT_STAT[$p]) }&lt;/m:ProductStat&gt;
            else ()
          }
          { insertDate(data($fml/fml:NF_ACCOUN_ACCOUNTOPENDATE[$p]),"yyyy-MM-dd","m:AccounAccountopendate")
          }
          {if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] = 10)
            then insertDate(data($fml/fml:NF_TIMEA_NEXTRENEWALMATURI[$p]),"yyyy-MM-dd","m:AccounAccountclosedate")
            else if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] = 3) 
              then insertDate(data($fml/fml:NF_LOANA_NEXTMATURITYDATE[$p]),"yyyy-MM-dd","m:AccounAccountclosedate")
              else ()
          }
         {if($fml/fml:NF_PERIOD_PERIOD[$p])
            then &lt;m:AccounRenewalperiod&gt;{ data($fml/fml:NF_PERIOD_PERIOD[$p]) }&lt;/m:AccounRenewalperiod&gt;
            else ()
          }
          {if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] = 10)
            then if($fml/fml:NF_TIMEA_RENEWALFREQUENCY[$p])
                    then &lt;m:AccounRenewalfrequency&gt;{ data($fml/fml:NF_TIMEA_RENEWALFREQUENCY[$p]) }&lt;/m:AccounRenewalfrequency&gt;
                    else ()
            else if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p] = 3)
                    then &lt;m:AccounRenewalfrequency&gt;{ data($fml/fml:NF_LOANA_MATURITYDATE2[$p]) }&lt;/m:AccounRenewalfrequency&gt;
                    else &lt;m:AccounRenewalfrequency&gt;0&lt;/m:AccounRenewalfrequency&gt;
          }
          {if($fml/fml:NF_RENEWT_RENEWALTYPE[$p])
            then &lt;m:TimeaRenewaltype&gt;{ data($fml/fml:NF_RENEWT_RENEWALTYPE[$p]) }&lt;/m:TimeaRenewaltype&gt;
            else ()
          }
          {if($fml/fml:NF_TIMEA_INTERESTDISPOSITI[$p])
            then &lt;m:TimeaInterestdispositi&gt;{ data($fml/fml:NF_TIMEA_INTERESTDISPOSITI[$p]) }&lt;/m:TimeaInterestdispositi&gt;
            else ()
          }
          {if($fml/fml:NF_LOANAS_PAYMNTAMOUNTORPC[$p])
            then &lt;m:LoanasPaymntamountorpc&gt;{ data($fml/fml:NF_LOANAS_PAYMNTAMOUNTORPC[$p]) }&lt;/m:LoanasPaymntamountorpc&gt;
            else ()
          }
          {if($fml/fml:NF_LOANAS_PAYMNTAMOUNTORPCPL[$p])
            then &lt;m:LoanasPaymntamountorpcpl&gt;{ data($fml/fml:NF_LOANAS_PAYMNTAMOUNTORPCPL[$p]) }&lt;/m:LoanasPaymntamountorpcpl&gt;
            else ()
          }
          {if($fml/fml:NF_LOANAS_INTERESTPAYMENT[$p])
            then &lt;m:LoanasInterestpayment&gt;{ data($fml/fml:NF_LOANAS_INTERESTPAYMENT[$p]) }&lt;/m:LoanasInterestpayment&gt;
            else ()
          }
          {if($fml/fml:NF_COOWNERS_COUNT[$p])
            then &lt;m:CoownersCount&gt;{ data($fml/fml:NF_COOWNERS_COUNT[$p]) }&lt;/m:CoownersCount&gt;
            else ()
          }
        &lt;/m:CUSTOMER_PRODUCTS&gt;
      }
      {if($fml/fml:NF_PAGECC_OPERATIONS)
        then &lt;m:PageccOperations&gt;{ data($fml/fml:NF_PAGECC_OPERATIONS) }&lt;/m:PageccOperations&gt;
        else ()
      }
      {if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
        then &lt;m:PagecNavigationkeyvalu&gt;{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/m:PagecNavigationkeyvalu&gt;
        else ()
      }
      {if($fml/fml:NF_PAGEC_HASNEXT)
        then &lt;m:PagecHasnext&gt;{ data($fml/fml:NF_PAGEC_HASNEXT) }&lt;/m:PagecHasnext&gt;
        else ()
      }
      {if($fml/fml:NF_RESPOM_ERRORCODE)
        then &lt;m:ErrorCode&gt;{ data($fml/fml:NF_RESPOM_ERRORCODE) }&lt;/m:ErrorCode&gt;
        else ()
      }
      {if($fml/fml:NF_RESPOM_ERRORDESCRIPTION)
        then &lt;m:ErrorDescription&gt;{ data($fml/fml:NF_RESPOM_ERRORDESCRIPTION) }&lt;/m:ErrorDescription&gt;
        else ()
      }
    &lt;/m:getCustProdsForAdvisoryResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetCustProdsForAdvisoryResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>