<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:ceke.entities.be.dcl";
declare namespace urn4 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn5 = "urn:accounts.entities.be.dcl";
declare namespace urn6 = "urn:entities.be.dcl";



declare function xf:mapsaveCustomerCEKERequest($req as element(urn:invoke),$head as element(urn:header))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				&lt;fml:E_MSHEAD_MSGID&gt;{data($head/urn:msgHeader/urn:msgId)}&lt;/fml:E_MSHEAD_MSGID&gt;
			}
			{
				&lt;fml:E_CUST_STATUS?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customerCEKEStatus/urn4:CustomerStatusCEKE/urn4:customerStatusCEKE)   }&lt;/fml:E_CUST_STATUS&gt;
			}
			{
				&lt;fml:E_CUST_CITY?&gt;{ data($req/urn:address/urn1:Address/urn1:city) }&lt;/fml:E_CUST_CITY&gt;
			}

			{
				&lt;fml:E_CUST_STREET?&gt;{data($req/urn:address/urn1:Address/urn1:street)} {data($req/urn:address/urn1:Address/urn1:local)} {data($req/urn:address/urn1:Address/urn1:houseFlatNumber)}&lt;/fml:E_CUST_STREET&gt;
			}
			{
				&lt;fml:E_CUST_ZIPCODE?&gt;{ data($req/urn:address/urn1:Address/urn1:zipCode) }&lt;/fml:E_CUST_ZIPCODE&gt;
			}

			{
				&lt;fml:E_TOKEN_SERIAL_NO?&gt;{ data($req/urn:token/urn3:Token/urn3:serialNumber) }&lt;/fml:E_TOKEN_SERIAL_NO&gt;
			}


			{
				&lt;fml:B_NR_DOK?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:documentNumber) }&lt;/fml:B_NR_DOK&gt;
			}
			{
				&lt;fml:E_CUST_IDENTIF?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:additionalPassword) }&lt;/fml:E_CUST_IDENTIF&gt;
			}
			{
				&lt;fml:E_REP_NAME?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:repName) }&lt;/fml:E_REP_NAME&gt;
			}
			{
				&lt;fml:E_LIMIT?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:limitPIN) }&lt;/fml:E_LIMIT&gt;

			}
			{
				&lt;fml:E_HI_LIMIT?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:limitTokenSMS) }&lt;/fml:E_HI_LIMIT&gt;
			}
			{
				&lt;fml:E_TRN_LIMIT?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:limitTransaction) }&lt;/fml:E_TRN_LIMIT&gt;
			}
			{
				&lt;fml:E_CUST_REPORT_DATE?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:contractDate) }&lt;/fml:E_CUST_REPORT_DATE&gt;
			}
			{
				&lt;fml:B_RODZ_DOK?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:documentType/urn4:DocumentTypeCEKE/urn4:documentTypeCEKE) }&lt;/fml:B_RODZ_DOK&gt;
			}
			{
				&lt;fml:B_ID_ODDZ?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:registerBranchCEKE/urn2:BranchCode/urn2:branchCode) }&lt;/fml:B_ID_ODDZ&gt;
			}
			{
				&lt;fml:U_USER_NAME?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:cekeRegisterUser/urn6:User/urn6:userID) }&lt;/fml:U_USER_NAME&gt;
			}
			{
				&lt;fml:E_ADMIN_MICRO_BRANCH?&gt;{ data($head/urn:msgHeader/urn:unitId) }&lt;/fml:E_ADMIN_MICRO_BRANCH&gt;
			}
			{
				&lt;fml:E_PROFILE_ID?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:profileCEKE/urn3:ProfileCEKE/urn3:profileCEKE) }&lt;/fml:E_PROFILE_ID&gt;
			}
			{
				&lt;fml:E_SMS_TOKEN_NO?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:smsCode/urn3:SMSCode/urn3:smsCodeNumber) }&lt;/fml:E_SMS_TOKEN_NO&gt;
			}
			{
				if($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:companyName and ($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:companyName != ""))
					then &lt;fml:E_CUST_NAME&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:companyName) }&lt;/fml:E_CUST_NAME&gt;
					else (
					&lt;fml:E_CUST_NAME&gt;{data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:customerPersonal/urn1:CustomerPersonal/urn1:firstName)} {data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:customerPersonal/urn1:CustomerPersonal/urn1:lastName)}&lt;/fml:E_CUST_NAME&gt;
					)
			}
			{
				&lt;fml:E_CIF_NUMBER?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:customerNumber) }&lt;/fml:E_CIF_NUMBER&gt;
			}
			{
				&lt;fml:E_CUST_TELNO?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:phoneNo) }&lt;/fml:E_CUST_TELNO&gt;
			}
			{
				&lt;fml:E_CUST_GSMNO?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:mobileNo) }&lt;/fml:E_CUST_GSMNO&gt;
			}
			{
				&lt;fml:E_CUST_EMAIL?&gt;{ data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:email) }&lt;/fml:E_CUST_EMAIL&gt;
			}
			{
				if(upper-case(data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:customerType/urn2:CustomerType/urn2:customerType)) = "F") then
				(
					&lt;fml:E_CUSTOMER_TYPE&gt;P&lt;/fml:E_CUSTOMER_TYPE&gt;
				) else if (upper-case(data($req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customer/urn1:Customer/urn1:customerType/urn2:CustomerType/urn2:customerType)) = "P") then
				(
					&lt;fml:E_CUSTOMER_TYPE&gt;C&lt;/fml:E_CUSTOMER_TYPE&gt;
				) else ()

			}			{
				for $customerChannelCEKE in $req/urn:customerCEKE/urn3:CustomerCEKE/urn3:customerChannelCEKEList/urn3:CustomerChannelCEKE return
				(
					&lt;fml:E_CHANNEL_TYPE&gt;{data($customerChannelCEKE/urn3:channelID/urn4:ChannelCEKE/urn4:channelCEKE)}&lt;/fml:E_CHANNEL_TYPE&gt;,
					&lt;fml:E_PDATA&gt;{data($customerChannelCEKE/urn3:pinMailerActivationCode)}&lt;/fml:E_PDATA&gt;

				)
			}

			{
				for $ownAccountCEKE in $req/urn:customerCEKE/urn3:CustomerCEKE/urn3:ownAccountCEKEList/urn3:OwnAccountCEKE return
				(
					&lt;fml:B_DL_NR_RACH&gt;{data($ownAccountCEKE/urn3:account/urn5:Account/urn5:accountIBAN)}&lt;/fml:B_DL_NR_RACH&gt;,
					&lt;fml:E_SEQ_NO&gt;{data($ownAccountCEKE/urn3:seqNum)}&lt;/fml:E_SEQ_NO&gt;,

					if (data($ownAccountCEKE/urn3:flagFeeAccount)) then (
						if ((data($ownAccountCEKE/urn3:flagFeeAccount)) = "true") then
							&lt;fml:E_OPTIONS&gt;1&lt;/fml:E_OPTIONS&gt;
						else if ((data($ownAccountCEKE/urn3:flagFeeAccount)) = "false") then (
							&lt;fml:E_OPTIONS&gt;0&lt;/fml:E_OPTIONS&gt;
						) else ()
					) else ()

				)
			}

		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

&lt;soap-env:Body&gt;
{ xf:mapsaveCustomerCEKERequest($body/urn:invoke,$header/urn:header)}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>