<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/SaveCustomer/ResponseTransform2/";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace ns1 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns-1 = "urn:baseentities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";

declare function xf:ResponseTransform2($pcCustNewResponse1 as element(ns1:pcCustNewResponse))
    as element(ns2:invokeResponse) {
        &lt;ns2:invokeResponse&gt;
            &lt;ns2:messageHelper&gt;
                {
                    for $Error in $pcCustNewResponse1/ns1:Error
                    return
                        &lt;ns-1:MessageHelper&gt;
                            &lt;ns-1:errorCode?&gt;{ xs:string( data($Error/ns1:NrKom) ) }&lt;/ns-1:errorCode&gt;
                            &lt;ns-1:message?&gt;{ data($Error/ns1:OpisBledu) }&lt;/ns-1:message&gt;
                        &lt;/ns-1:MessageHelper&gt;
                }
            &lt;/ns2:messageHelper&gt;
            {
                for $NumerKlienta in $pcCustNewResponse1/ns1:NumerKlienta
                return
                    &lt;ns2:customer&gt;
                        &lt;ns0:Customer&gt;
                            &lt;ns0:customerNumber&gt;{ data($NumerKlienta) }&lt;/ns0:customerNumber&gt;
                        &lt;/ns0:Customer&gt;
                    &lt;/ns2:customer&gt;
            }
        &lt;/ns2:invokeResponse&gt;
};

declare variable $pcCustNewResponse1 as element(ns1:pcCustNewResponse) external;

xf:ResponseTransform2($pcCustNewResponse1)</con:xquery>
</con:xqueryEntry>