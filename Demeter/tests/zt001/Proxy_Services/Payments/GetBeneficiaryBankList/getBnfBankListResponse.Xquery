<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:operations.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:baseauxentities.be.dcl";
declare namespace urn4 = "urn:swift.operations.entities.be.dcl";
declare namespace urn5 = "urn:entities.be.dcl";
declare namespace urn6 = "urn:transactionswiftdict.operationsdictionary.dictionaries.be.dcl";

declare function xf:mapGetBnkListResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		
&lt;urn:invokeResponse&gt;

	&lt;urn:bcd&gt;
		&lt;urn5:BusinessControlData&gt;
			&lt;urn5:pageControl&gt;
				&lt;urn3:PageControl&gt;
			                &lt;urn3:hasNext&gt;			
						{ data($fml/fml:NF_PAGEC_HASNEXT)}
					&lt;/urn3:hasNext&gt;			
					&lt;urn3:navigationKeyDefinition&gt;
						{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYDEFI)}				
					&lt;/urn3:navigationKeyDefinition&gt;
					&lt;urn3:navigationKeyValue&gt;
						{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)}			
					&lt;/urn3:navigationKeyValue&gt;
				&lt;/urn3:PageControl&gt;
			&lt;/urn5:pageControl&gt;	
		&lt;/urn5:BusinessControlData&gt;
	&lt;/urn:bcd&gt;
	&lt;urn:swiftBenefBankList&gt;
            {
                for $bank in $fml/fml:S_FML32 return
                (
			&lt;urn4:SwiftBenefBank&gt;
				&lt;urn4:branchBankId&gt;
					{ data($bank/fml:S_SWIFT_ID)}	
				&lt;/urn4:branchBankId&gt;
				&lt;urn4:bic&gt;
					{ data($bank/fml:S_SWIFT_BIC)}	
				&lt;/urn4:bic&gt;
				&lt;urn4:nationalId&gt;
					{ data($bank/fml:S_SWIFT_BRANCH_CODE)}	
				&lt;/urn4:nationalId&gt;
				&lt;urn4:city&gt;
					{ data($bank/fml:S_SWIFT_TOWN)}	
				&lt;/urn4:city&gt;
				&lt;urn4:name&gt;
					{ data($bank/fml:S_SWIFT_BANK_NAME)}	
				&lt;/urn4:name&gt;
				&lt;urn4:shortName&gt;
					{ data($bank/fml:S_BRANCH_SHORT)}	
				&lt;/urn4:shortName&gt;
				&lt;urn4:address&gt;
					{ data($bank/fml:S_SWIFT_ADDRESS)}	
				&lt;/urn4:address&gt;
                                &lt;urn4:country&gt;
				    &lt;urn2:CountryCode&gt;
					&lt;urn2:countryCode&gt;{ data($bank/fml:S_SWIFT_COUNTRY)}&lt;/urn2:countryCode&gt;
				    &lt;/urn2:CountryCode&gt;
                                &lt;/urn4:country&gt;
			&lt;/urn4:SwiftBenefBank&gt;
              )
              }
	&lt;/urn:swiftBenefBankList&gt;	

  
		&lt;urn:swiftIbanVerificationStatus&gt;
			&lt;urn6:SwiftIbanVerificationStatus&gt;
				&lt;urn6:swiftIbanVerificationStatus&gt;
					{ data($fml/fml:S_IBAN_STATUS)}						
				&lt;/urn6:swiftIbanVerificationStatus&gt;
			&lt;/urn6:SwiftIbanVerificationStatus&gt;
		&lt;/urn:swiftIbanVerificationStatus&gt;
           

&lt;/urn:invokeResponse&gt;						
		
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapGetBnkListResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>