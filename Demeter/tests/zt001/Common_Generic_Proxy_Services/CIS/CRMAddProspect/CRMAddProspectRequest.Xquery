<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddProspectRequest($req as element(m:CRMAddProspectRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:SkpPracownikaRej)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ&gt;{ data($req/m:SkpPracownikaRej) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU&gt;{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU&gt;
					else ()
			}
			{
				if($req/m:Identyfikacja)
					then &lt;fml:CI_IDENTYFIKACJA&gt;{ data($req/m:Identyfikacja) }&lt;/fml:CI_IDENTYFIKACJA&gt;
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE&gt;{ data($req/m:Imie) }&lt;/fml:DC_IMIE&gt;
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO&gt;{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO&gt;
					else ()
			}
			{
				if($req/m:NazwaPelna)
					then &lt;fml:CI_NAZWA_PELNA&gt;{ data($req/m:NazwaPelna) }&lt;/fml:CI_NAZWA_PELNA&gt;
					else ()
			}
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL&gt;{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL&gt;
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON&gt;{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON&gt;
					else ()
			}
			{
				if($req/m:Nip)
					then &lt;fml:DC_NIP&gt;{ data($req/m:Nip) }&lt;/fml:DC_NIP&gt;
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then &lt;fml:DC_NUMER_PASZPORTU&gt;{ data($req/m:NumerPaszportu) }&lt;/fml:DC_NUMER_PASZPORTU&gt;
					else ()
			}
			{
				if($req/m:UdostepGrupa)
					then &lt;fml:CI_UDOSTEP_GRUPA&gt;{ data($req/m:UdostepGrupa) }&lt;/fml:CI_UDOSTEP_GRUPA&gt;
					else ()
			}
			{
				if($req/m:Kraj)
					then &lt;fml:DC_KRAJ&gt;{ data($req/m:Kraj) }&lt;/fml:DC_KRAJ&gt;
					else ()
			}
			{
				if($req/m:Wojewodztwo)
					then &lt;fml:DC_WOJEWODZTWO&gt;{ data($req/m:Wojewodztwo) }&lt;/fml:DC_WOJEWODZTWO&gt;
					else ()
			}
			{
				if($req/m:KodPocztowyDanePodst)
					then &lt;fml:DC_KOD_POCZTOWY_DANE_PODST&gt;{ data($req/m:KodPocztowyDanePodst) }&lt;/fml:DC_KOD_POCZTOWY_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then &lt;fml:DC_MIASTO_DANE_PODST&gt;{ data($req/m:MiastoDanePodst) }&lt;/fml:DC_MIASTO_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:UlicaDanePodst)
					then &lt;fml:DC_ULICA_DANE_PODST&gt;{ data($req/m:UlicaDanePodst) }&lt;/fml:DC_ULICA_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:NrPosesLokaluDanePodst)
					then &lt;fml:DC_NR_POSES_LOKALU_DANE_PODST&gt;{ data($req/m:NrPosesLokaluDanePodst) }&lt;/fml:DC_NR_POSES_LOKALU_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:AdresDod)
					then &lt;fml:CI_ADRES_DOD&gt;{ data($req/m:AdresDod) }&lt;/fml:CI_ADRES_DOD&gt;
					else ()
			}
			{
				if($req/m:NrTelefonu)
					then &lt;fml:DC_NR_TELEFONU&gt;{ data($req/m:NrTelefonu) }&lt;/fml:DC_NR_TELEFONU&gt;
					else ()
			}
			{
				if($req/m:NrTelefKomorkowego)
					then &lt;fml:DC_NR_TELEF_KOMORKOWEGO&gt;{ data($req/m:NrTelefKomorkowego) }&lt;/fml:DC_NR_TELEF_KOMORKOWEGO&gt;
					else ()
			}
			{
				if($req/m:NumerFaksu)
					then &lt;fml:DC_NUMER_FAKSU&gt;{ data($req/m:NumerFaksu) }&lt;/fml:DC_NUMER_FAKSU&gt;
					else ()
			}
			{
				if($req/m:AdresEMail)
					then &lt;fml:DC_ADRES_E_MAIL&gt;{ data($req/m:AdresEMail) }&lt;/fml:DC_ADRES_E_MAIL&gt;
					else ()
			}
			{
				if($req/m:Notatki)
					then &lt;fml:CI_NOTATKI&gt;{ data($req/m:Notatki) }&lt;/fml:CI_NOTATKI&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMAddProspectRequest($body/m:CRMAddProspectRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>