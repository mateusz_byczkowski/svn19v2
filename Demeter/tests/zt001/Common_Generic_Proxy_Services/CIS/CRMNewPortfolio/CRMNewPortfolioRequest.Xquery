<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMNewPortfolioRequest($req as element(m:CRMNewPortfolioRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:SkpPracownikaRej)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ&gt;{ data($req/m:SkpPracownikaRej) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ&gt;
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				if($req/m:ImiePrac)
					then &lt;fml:CI_IMIE_PRAC&gt;{ data($req/m:ImiePrac) }&lt;/fml:CI_IMIE_PRAC&gt;
					else ()
			}
			{
				if($req/m:NazwPrac)
					then &lt;fml:CI_NAZW_PRAC&gt;{ data($req/m:NazwPrac) }&lt;/fml:CI_NAZW_PRAC&gt;
					else ()
			}
			{
				if($req/m:SkpZastepcy)
					then &lt;fml:CI_SKP_ZASTEPCY&gt;{ data($req/m:SkpZastepcy) }&lt;/fml:CI_SKP_ZASTEPCY&gt;
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU&gt;{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMNewPortfolioRequest($body/m:CRMNewPortfolioRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>