<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Typy i elementy operacji
Version: 3.009</con:description>
  <con:schema><![CDATA[<?xml version="1.0" encoding="UTF-8"?>
<!--
        Typy i elementy operacji
        $Revision: 3.009 $
	$Date: 2010-04-12 11:08:00 $
-->
<schema xmlns="http://www.w3.org/2001/XMLSchema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:cmf-biz="http://jv.channel.cu.com.pl/cmf-biz-types" xmlns:cmf-product="http://jv.channel.cu.com.pl/cmf-product-types" xmlns:cmf-party="http://jv.channel.cu.com.pl/cmf-party-types" xmlns:cmf-comm="http://jv.channel.cu.com.pl/cmf-comm-types" xmlns:cmf-holding="http://jv.channel.cu.com.pl/cmf-holding-types" xmlns:cmf-wsdl="http://jv.channel.cu.com.pl/cmf-wsdl-types" xmlns:cmf-tech="http://jv.channel.cu.com.pl/cmf-technical-types" xmlns:wsdl-jv="http://jv.channel.cu.com.pl/cmf/wsdl-jv" targetNamespace="http://jv.channel.cu.com.pl/cmf/wsdl-jv">
	<import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="../base/cmf-biz-types.xsd"/>
	<import namespace="http://jv.channel.cu.com.pl/cmf-product-types" schemaLocation="../base/cmf-product-types.xsd"/>
	<import namespace="http://jv.channel.cu.com.pl/cmf-party-types" schemaLocation="../base/cmf-party-types.xsd"/>
	<import namespace="http://jv.channel.cu.com.pl/cmf-comm-types" schemaLocation="../base/cmf-comm-types.xsd"/>
	<import namespace="http://jv.channel.cu.com.pl/cmf-holding-types" schemaLocation="../base/cmf-holding-types.xsd"/>
	<import namespace="http://jv.channel.cu.com.pl/cmf-wsdl-types" schemaLocation="../base/cmf-wsdl-types.xsd"/>
	<!-- **********-->
	<!-- ********** ACCESSORY TYPEs-->
	<!-- **********-->
	<complexType name="SaveRequestType">
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="save" type="cmf-biz:SaveType" default="true"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="ValidationsResponseType">
		<sequence>
			<element name="validations" type="cmf-holding:ValidationListType" minOccurs="0"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<!-- ********** REQUEST & RESPONSE TYPEs-->
	<!-- **********-->
	<complexType name="AddContractRequestType">
		<annotation>
			<documentation>C100, P100, P1001</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:SaveRequestType">
				<sequence>
					<element name="contract" type="cmf-holding:ContractType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="AddContractResponseType">
		<annotation>
			<documentation>C100, P100, P1001</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:ValidationsResponseType">
				<sequence>
					<element name="account" type="cmf-biz:BankAccountType" minOccurs="0"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<!-- **********-->
	<complexType name="ChangeContractBeneficiariesRequestType">
		<annotation>
			<documentation>C130, P130</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:SaveRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="coverage-number" type="cmf-biz:CoverageNumberType"/>
					<element name="beneficiaries" type="cmf-party:BeneficiaryListType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="ChangeContractBeneficiariesResponseType">
		<annotation>
			<documentation>C130, P130</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:ValidationsResponseType"/>
		</complexContent>
	</complexType>
	<!-- **********-->
	<complexType name="ChangeContractBanksRequestType">
		<annotation>
			<documentation>C140, P140</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:SaveRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="bank" type="cmf-biz:BankType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="ChangeContractBanksResponseType">
		<annotation>
			<documentation>C140, P140</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:ValidationsResponseType"/>
		</complexContent>
	</complexType>
	<!-- **********-->
	<complexType name="ChangeContractAddressesRequestType">
		<annotation>
			<documentation>C150, P150</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:SaveRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="contract-address" type="cmf-holding:ContractPersonType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="ChangeContractAddressesResponseType">
		<annotation>
			<documentation>C150, P150</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:ValidationsResponseType"/>
		</complexContent>
	</complexType>
	<!-- **********-->
	<complexType name="ChangeContractCoverageFundsRequestType">
		<annotation>
			<documentation>C160, P160</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:SaveRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="coverage-number" type="cmf-biz:CoverageNumberType"/>
					<element name="funds" type="cmf-holding:FundSplitListType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="ChangeContractCoverageFundsResponseType">
		<annotation>
			<documentation>C160, P160</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:ValidationsResponseType"/>
		</complexContent>
	</complexType>
	<!-- **********-->
	<complexType name="ChangeContractCoverageLevelRequestType">
		<annotation>
			<documentation>P165, C165</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:SaveRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="coverage-number" type="cmf-biz:CoverageNumberType" minOccurs="0"/>
					<element name="effective" type="xsd:date" minOccurs="0"/>
					<element name="level" type="cmf-biz:LevelType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="ChangeContractCoverageLevelResponseType">
		<annotation>
			<documentation>P165, C165</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:ValidationsResponseType"/>
		</complexContent>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<complexType name="PerformContractSwitchRequestType">
		<annotation>
			<documentation>P180</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:SaveRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="coverage-number" type="cmf-biz:CoverageNumberType" minOccurs="0"/>
					<element name="fund-from" type="cmf-holding:FundChangeListType"/>
					<element name="fund-to" type="cmf-holding:FundChangeListType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="PerformContractSwitchResponseType">
		<complexContent>
			<extension base="wsdl-jv:ValidationsResponseType"/>
		</complexContent>
	</complexType>
	<!-- **********-->
	<complexType name="PerformContractWithdrawalRequestType">
		<annotation>
			<documentation>P190</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:SaveRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="coverage-number" type="cmf-biz:CoverageNumberType" minOccurs="0"/>
					<element name="fund-from" type="cmf-holding:FundChangeListType"/>
					<element name="bank" type="cmf-biz:BankType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="PerformContractWithdrawalResponseType">
		<complexContent>
			<extension base="wsdl-jv:ValidationsResponseType"/>
		</complexContent>
	</complexType>
	<!-- **********-->
	<complexType name="PerformContractSurrenderRequestType">
		<annotation>
			<documentation>C194, P194</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:SaveRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="effective" type="xsd:date" minOccurs="0"/>
					<element name="status-reason" type="cmf-biz:StatusReasonType" minOccurs="0"/>
					<element name="bank" type="cmf-biz:BankType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="PerformContractSurrenderResponseType">
		<complexContent>
			<extension base="wsdl-jv:ValidationsResponseType"/>
		</complexContent>
	</complexType>
	<!-- **********-->
	<complexType name="PerformContractCancelRequestType">
		<annotation>
			<documentation>P200</documentation>
		</annotation>
		<complexContent>
			<extension base="wsdl-jv:SaveRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="effective" type="xsd:date" minOccurs="0"/>
					<element name="status-reason" type="cmf-biz:StatusReasonType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="PerformContractCancelResponseType">
		<complexContent>
			<extension base="wsdl-jv:ValidationsResponseType"/>
		</complexContent>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<complexType name="GetContractListRequestType">
		<annotation>
			<documentation>P300</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="client-code" type="cmf-biz:ClientCodeType" minOccurs="0"/>
					<element name="product-type" type="cmf-biz:ProductNameType" minOccurs="0"/>
					<element name="category" type="cmf-biz:CategoryNameType" minOccurs="0"/>
					<element name="is-group" type="xsd:boolean" minOccurs="0"/>
					<element name="factory" type="cmf-biz:FactoryNameType" minOccurs="0"/>
					<element name="status" type="cmf-biz:StatusType" minOccurs="0"/>
					<element name="general-status" type="cmf-biz:GeneralStatusType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="GetContractListResponseType">
		<sequence>
			<element name="contracts" type="cmf-holding:ContractShortListType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<complexType name="GetContractRequestType">
		<annotation>
			<documentation>P350</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="category" type="cmf-biz:CategoryNameType" minOccurs="0"/>
					<element name="product-type" type="cmf-biz:ProductNameType" minOccurs="0"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType" minOccurs="0"/>
					<element name="proposal-number" type="cmf-biz:ContractNumberType" minOccurs="0"/>
					<element name="policy-number" type="cmf-biz:PolicyNumberType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="GetContractResponseType">
		<sequence>
			<element name="contract" type="cmf-holding:ContractType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<complexType name="FindContractRequestType">
		<annotation>
			<documentation>P355</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="category" type="cmf-biz:CategoryNameType" minOccurs="0"/>
					<element name="product-type" type="cmf-biz:ProductNameType" minOccurs="0"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType" minOccurs="0"/>
					<element name="proposal-number" type="cmf-biz:ContractNumberType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="FindContractResponseType">
		<sequence>
			<element name="contracts" type="cmf-holding:ContractShortListType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<complexType name="GetContractHistoryRequestType">
		<annotation>
			<documentation>P360</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="date-from" type="xsd:date" minOccurs="0"/>
					<element name="date-to" type="xsd:date" minOccurs="0"/>
					<element name="endorsement" type="cmf-biz:EndorsementIdType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="GetContractHistoryResponseType">
		<sequence>
			<element name="histories" type="cmf-holding:ContractHistoryType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<complexType name="GetContractFinancialOperationListRequestType">
		<annotation>
			<documentation>P364</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="date-from" type="xsd:date" minOccurs="0"/>
					<element name="date-to" type="xsd:date" minOccurs="0"/>
					<element name="operation-type" type="cmf-biz:FinancialOperationTypeType" minOccurs="0"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="GetContractFinancialOperationListResponseType">
		<sequence>
			<element name="operations" type="cmf-holding:ContractFinancialOperationShortListType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<complexType name="GetContractFinancialOperationDetailsRequestType">
		<annotation>
			<documentation>C366, P366, C3661</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
					<element name="contract-number" type="cmf-biz:ContractNumberType"/>
					<element name="operation-type" type="cmf-biz:FinancialOperationTypeType"/>
					<element name="operation-id" type="cmf-biz:OperationIdType">
						<annotation>
							<documentation>identyfikator operacji</documentation>
						</annotation>
					</element>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="GetContractFinancialOperationDetailsResponseType">
		<sequence>
			<element name="operation" type="cmf-holding:ContractFinancialOperationType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<complexType name="GetProductRequestType">
		<annotation>
			<documentation>P310</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="GetProductResponseType">
		<sequence>
			<element name="product" type="cmf-product:ProductType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<complexType name="GetProductListRequestType">
		<annotation>
			<documentation>P312</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="category" type="cmf-biz:CategoryNameType" minOccurs="0">
						<annotation>
							<documentation>
                        Kategoria produktu
                    </documentation>
						</annotation>
					</element>
					<element name="factory" type="cmf-biz:FactoryNameType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="GetProductListResponseType">
		<sequence>
			<element name="product-list" type="cmf-product:ProductListType" minOccurs="0"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<complexType name="GetFundProductListRequestType">
		<annotation>
			<documentation>P320</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="GetFundProductListResponseType">
		<sequence>
			<element name="fund-list" type="cmf-holding:FundListType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<complexType name="GetFundUnitPricesRequestType">
		<annotation>
			<documentation>P330</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="product-type" type="cmf-biz:ProductNameType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="GetFundUnitPricesResponseType">
		<sequence>
			<element name="fund-price-list" type="cmf-holding:FundPriceListType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<complexType name="AddClientJVRequestType">
		<annotation>
			<documentation>C050, P050, P0501</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="customer" type="cmf-party:ClientType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="AddClientJVResponseType">
		<annotation>
			<documentation>C050, P050, P0501</documentation>
		</annotation>
		<sequence>
			<element name="client-code" type="cmf-biz:ClientCodeType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<complexType name="ChangeClientJVRequestType">
		<annotation>
			<documentation>C0510, P051, C0511, P0511</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="customer" type="cmf-party:ClientType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="ChangeClientJVResponseType">
		<annotation>
			<documentation>C0510, P051, C0511, P0511</documentation>
		</annotation>
		<sequence>
			<element name="client-code" type="cmf-biz:ClientCodeType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<complexType name="GetClientJVRequestType">
		<annotation>
			<documentation>C052, P052, C0521, C0522, P0521</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="client-code" type="cmf-biz:ClientCodeType"/>
					<element name="factory" type="cmf-biz:FactoryNameType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="GetClientJVResponseType">
		<annotation>
			<documentation>C052, P052, C0521, C0522, P0521</documentation>
		</annotation>
		<sequence>
			<element name="customer" type="cmf-party:ClientType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<complexType name="FindClientJVRequestType">
		<annotation>
			<documentation>C054, C055</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="last-name" type="cmf-biz:PersonSurnameType" minOccurs="0">
						<annotation>
							<documentation>Nazwisko</documentation>
						</annotation>
					</element>
					<element name="pesel" type="cmf-biz:PeselType" minOccurs="0"/>
					<element name="factory" type="cmf-biz:FactoryNameType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="FindClientJVResponseType">
		<sequence>
			<element name="client-list" type="cmf-party:ClientShortListType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<complexType name="SendSMSRequestType">
		<annotation>
			<documentation>C059</documentation>
		</annotation>
		<sequence>
			<element name="sms" type="cmf-comm:SMSType"/>
		</sequence>
	</complexType>
	<complexType name="SendSMSResponseType">
		<sequence>
			<element name="sms-id" type="cmf-biz:SMSIdType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<complexType name="SendEmailToClientRequestType">
		<annotation>
			<documentation>C059</documentation>
		</annotation>
		<complexContent>
			<extension base="cmf-wsdl:CmfRequestType">
				<sequence>
					<element name="email" type="cmf-comm:EmailMessageType"/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<complexType name="SendEmailToClientResponseType">
		<sequence>
			<element name="msg-status" type="cmf-biz:SMSStatusType" minOccurs="0"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<complexType name="FaultResponseType">
		<sequence>
			<element name="status" type="cmf-wsdl:StatusType"/>
		</sequence>
	</complexType>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- ********** ELEMENTS-->
	<!-- **********-->
	<element name="addContractRequest" type="wsdl-jv:AddContractRequestType"/>
	<element name="addContractResponse" type="wsdl-jv:AddContractResponseType"/>
	<!-- **********-->
	<element name="changeContractBeneficiariesRequest" type="wsdl-jv:ChangeContractBeneficiariesRequestType"/>
	<element name="changeContractBeneficiariesResponse" type="wsdl-jv:ChangeContractBeneficiariesResponseType"/>
	<!-- **********-->
	<element name="changeContractBanksRequest" type="wsdl-jv:ChangeContractBanksRequestType"/>
	<element name="changeContractBanksResponse" type="wsdl-jv:ChangeContractBanksResponseType"/>
	<!-- **********-->
	<element name="changeContractAddressesRequest" type="wsdl-jv:ChangeContractAddressesRequestType"/>
	<element name="changeContractAddressesResponse" type="wsdl-jv:ChangeContractAddressesResponseType"/>
	<!-- **********-->
	<element name="changeContractCoverageFundsRequest" type="wsdl-jv:ChangeContractCoverageFundsRequestType"/>
	<element name="changeContractCoverageFundsResponse" type="wsdl-jv:ChangeContractCoverageFundsResponseType"/>
	<!-- **********-->
	<element name="changeContractCoverageLevelRequest" type="wsdl-jv:ChangeContractCoverageLevelRequestType"/>
	<element name="changeContractCoverageLevelResponse" type="wsdl-jv:ChangeContractCoverageLevelResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<element name="performContractSwitchRequest" type="wsdl-jv:PerformContractSwitchRequestType"/>
	<element name="performContractSwitchResponse" type="wsdl-jv:PerformContractSwitchResponseType"/>
	<!-- **********-->
	<element name="performContractWithdrawalRequest" type="wsdl-jv:PerformContractWithdrawalRequestType"/>
	<element name="performContractWithdrawalResponse" type="wsdl-jv:PerformContractWithdrawalResponseType"/>
	<!-- **********-->
	<element name="performContractSurrenderRequest" type="wsdl-jv:PerformContractSurrenderRequestType"/>
	<element name="performContractSurrenderResponse" type="wsdl-jv:PerformContractSurrenderResponseType"/>
	<!-- **********-->
	<element name="performContractCancelRequest" type="wsdl-jv:PerformContractCancelRequestType"/>
	<element name="performContractCancelResponse" type="wsdl-jv:PerformContractCancelResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<element name="getContractListRequest" type="wsdl-jv:GetContractListRequestType"/>
	<element name="getContractListResponse" type="wsdl-jv:GetContractListResponseType"/>
	<!-- **********-->
	<element name="getContractRequest" type="wsdl-jv:GetContractRequestType"/>
	<element name="getContractResponse" type="wsdl-jv:GetContractResponseType"/>
	<!-- **********-->
	<element name="findContractRequest" type="wsdl-jv:FindContractRequestType"/>
	<element name="findContractResponse" type="wsdl-jv:FindContractResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<element name="getContractHistoryRequest" type="wsdl-jv:GetContractHistoryRequestType"/>
	<element name="getContractHistoryResponse" type="wsdl-jv:GetContractHistoryResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<element name="getContractFinancialOperationListRequest" type="wsdl-jv:GetContractFinancialOperationListRequestType"/>
	<element name="getContractFinancialOperationListResponse" type="wsdl-jv:GetContractFinancialOperationListResponseType"/>
	<!-- **********-->
	<element name="getContractFinancialOperationDetailsRequest" type="wsdl-jv:GetContractFinancialOperationDetailsRequestType"/>
	<element name="getContractFinancialOperationDetailsResponse" type="wsdl-jv:GetContractFinancialOperationDetailsResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<element name="getProductRequest" type="wsdl-jv:GetProductRequestType"/>
	<element name="getProductResponse" type="wsdl-jv:GetProductResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<element name="getProductListRequest" type="wsdl-jv:GetProductListRequestType"/>
	<element name="getProductListResponse" type="wsdl-jv:GetProductListResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<element name="getFundProductListRequest" type="wsdl-jv:GetFundProductListRequestType"/>
	<element name="getFundProductListResponse" type="wsdl-jv:GetFundProductListResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<element name="addClientJVRequest" type="wsdl-jv:AddClientJVRequestType"/>
	<element name="addClientJVResponse" type="wsdl-jv:AddClientJVResponseType"/>
	<!-- **********-->
	<element name="changeClientJVRequest" type="wsdl-jv:ChangeClientJVRequestType"/>
	<element name="changeClientJVResponse" type="wsdl-jv:ChangeClientJVResponseType"/>
	<!-- **********-->
	<element name="getClientJVRequest" type="wsdl-jv:GetClientJVRequestType"/>
	<element name="getClientJVResponse" type="wsdl-jv:GetClientJVResponseType"/>
	<!-- **********-->
	<element name="getClientJVTempRequest" type="wsdl-jv:GetClientJVRequestType"/>
	<element name="getClientJVTempResponse" type="wsdl-jv:GetClientJVResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<element name="findClientJVRequest" type="wsdl-jv:FindClientJVRequestType"/>
	<element name="findClientJVResponse" type="wsdl-jv:FindClientJVResponseType"/>
	<!-- **********-->
	<element name="findClientJVTempRequest" type="wsdl-jv:FindClientJVRequestType"/>
	<element name="findClientJVTempResponse" type="wsdl-jv:FindClientJVResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<element name="sendSMSRequest" type="wsdl-jv:SendSMSRequestType"/>
	<element name="sendSMSResponse" type="wsdl-jv:SendSMSResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
	<element name="faultResponse" type="wsdl-jv:FaultResponseType"/>
	<!-- **********-->
	<!-- **********-->
	<!-- **********-->
</schema>]]></con:schema>
  <con:dependencies>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="../base/cmf-biz-types.xsd" ref="Business Services/ESB JV-CUP/WSDL/base/cmf-biz-types.xsd"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-product-types" schemaLocation="../base/cmf-product-types.xsd" ref="Business Services/ESB JV-CUP/WSDL/base/cmf-product-types.xsd"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-party-types" schemaLocation="../base/cmf-party-types.xsd" ref="Business Services/ESB JV-CUP/WSDL/base/cmf-party-types.xsd"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-comm-types" schemaLocation="../base/cmf-comm-types.xsd" ref="Business Services/ESB JV-CUP/WSDL/base/cmf-comm-types.xsd"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-holding-types" schemaLocation="../base/cmf-holding-types.xsd" ref="Business Services/ESB JV-CUP/WSDL/base/cmf-holding-types.xsd"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-wsdl-types" schemaLocation="../base/cmf-wsdl-types.xsd" ref="Business Services/ESB JV-CUP/WSDL/base/cmf-wsdl-types.xsd"/>
  </con:dependencies>
  <con:targetNamespace>http://jv.channel.cu.com.pl/cmf/wsdl-jv</con:targetNamespace>
</con:schemaEntry>