<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetProdRelResponse($fml as element(fml:FML32))
	as element(m:CRMGetProdRelResponse) {
		&lt;m:CRMGetProdRelResponse&gt;
			{
				if($fml/fml:NF_PAGEC_PAGESIZE)
					then &lt;m:PagecPagesize&gt;{ data($fml/fml:NF_PAGEC_PAGESIZE) }&lt;/m:PagecPagesize&gt;
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
					then &lt;m:PagecNavigationkeyvalu&gt;{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/m:PagecNavigationkeyvalu&gt;
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_HASNEXT)
					then &lt;m:PagecHasnext&gt;{ data($fml/fml:NF_PAGEC_HASNEXT) }&lt;/m:PagecHasnext&gt;
					else ()
			}
			{

				let $NF_CUSTOM_CUSTOMERNUMBER := $fml/fml:NF_CUSTOM_CUSTOMERNUMBER
				let $NF_CUSTOT_CUSTOMERTYPE := $fml/fml:NF_CUSTOT_CUSTOMERTYPE
				let $NF_CUSTOP_FIRSTNAME := $fml/fml:NF_CUSTOP_FIRSTNAME
				let $NF_CUSTOP_LASTNAME := $fml/fml:NF_CUSTOP_LASTNAME
				let $NF_CUSTOM_COMPANYNAME := $fml/fml:NF_CUSTOM_COMPANYNAME
				let $NF_CUSTAR_CUSTOMERACCOUNTR := $fml/fml:NF_CUSTAR_CUSTOMERACCOUNTR
				let $NF_TAX_TAXPERCENTAGE := $fml/fml:NF_TAX_TAXPERCENTAGE
				for $it at $p in $fml/fml:NF_CUSTOM_CUSTOMERNUMBER
				return
					&lt;m:Customer&gt;
					{
						if($NF_CUSTOM_CUSTOMERNUMBER[$p])
							then &lt;m:CustomCustomernumber&gt;{ data($NF_CUSTOM_CUSTOMERNUMBER[$p]) }&lt;/m:CustomCustomernumber&gt;
						else ()
					}
					{
						if($NF_CUSTOT_CUSTOMERTYPE[$p] = "P")
							then &lt;m:CustotCustomertype&gt;F&lt;/m:CustotCustomertype&gt;
						else if($NF_CUSTOT_CUSTOMERTYPE[$p] = "N")
							then &lt;m:CustotCustomertype&gt;P&lt;/m:CustotCustomertype&gt;
						else ()
					}
					{
						if($NF_CUSTOP_FIRSTNAME[$p])
							then &lt;m:CustopFirstname&gt;{ data($NF_CUSTOP_FIRSTNAME[$p]) }&lt;/m:CustopFirstname&gt;
						else ()
					}
					{
						if($NF_CUSTOP_LASTNAME[$p])
							then &lt;m:CustopLastname&gt;{ data($NF_CUSTOP_LASTNAME[$p]) }&lt;/m:CustopLastname&gt;
						else ()
					}
					{
						if($NF_CUSTOM_COMPANYNAME[$p])
							then &lt;m:CustomCompanyname&gt;{ data($NF_CUSTOM_COMPANYNAME[$p]) }&lt;/m:CustomCompanyname&gt;
						else ()
					}
					{
						if($NF_CUSTAR_CUSTOMERACCOUNTR[$p])
							then &lt;m:CustarCustomeraccountr&gt;{ data($NF_CUSTAR_CUSTOMERACCOUNTR[$p]) }&lt;/m:CustarCustomeraccountr&gt;
						else ()
					}
					{
						if($NF_TAX_TAXPERCENTAGE[$p])
							then &lt;m:TaxTaxpercentage&gt;{ data($NF_TAX_TAXPERCENTAGE[$p]) }&lt;/m:TaxTaxpercentage&gt;
						else ()
					}
					&lt;/m:Customer&gt;
			}

		&lt;/m:CRMGetProdRelResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetProdRelResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>