<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
   let $length:= string-length($sourceString)
   let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
   let $accountNumber :=  substring($sourceString, $startPos)
   return
     $accountNumber
};


declare function chkUnitId($unitId as xs:string?) as xs:string
{
      if ($unitId)
        then if(string-length($unitId)&gt;3)
            then "0"
        else
             $unitId
      else ""
};

declare function xf:mapCRMModifyCustProdAddrRequest($req as element(m:CRMModifyCustProdAddrRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:TrnId)
					then &lt;fml:DC_TRN_ID&gt;{ data($req/m:TrnId) }&lt;/fml:DC_TRN_ID&gt;
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK&gt;{ concat("SKP:",data($req/m:Uzytkownik)) }&lt;/fml:DC_UZYTKOWNIK&gt;
					else ()
			}
			{
				if($req/m:Oddzial)
					then &lt;fml:DC_ODDZIAL&gt;{chkUnitId( data($req/m:Oddzial)) }&lt;/fml:DC_ODDZIAL&gt;
					else ()
			}
			{
				if($req/m:NumerProduktu)
					then &lt;fml:DC_NUMER_PRODUKTU&gt;{ data($req/m:NumerProduktu) }&lt;/fml:DC_NUMER_PRODUKTU&gt;
					else ()
			}
			{
				if($req/m:NrRachunku)
					then &lt;fml:DC_NR_RACHUNKU&gt;{cutStr(data($req/m:NrRachunku),10) }&lt;/fml:DC_NR_RACHUNKU&gt;
					else ()
			}
			{
				if($req/m:ImieINazwiskoAlt)
					then &lt;fml:DC_IMIE_I_NAZWISKO_ALT&gt;{ data($req/m:ImieINazwiskoAlt) }&lt;/fml:DC_IMIE_I_NAZWISKO_ALT&gt;
					else ()
			}
			{
				if($req/m:ImieINazwiskoAltCD)
					then &lt;fml:DC_IMIE_I_NAZWISKO_ALT_C_D&gt;{ data($req/m:ImieINazwiskoAltCD) }&lt;/fml:DC_IMIE_I_NAZWISKO_ALT_C_D&gt;
					else ()
			}
			{
				if($req/m:UlicaAdresAlt)
					then &lt;fml:DC_ULICA_ADRES_ALT&gt;{ data($req/m:UlicaAdresAlt) }&lt;/fml:DC_ULICA_ADRES_ALT&gt;
					else ()
			}
			{
				if($req/m:NrPosesLokaluAdresAlt)
					then &lt;fml:DC_NR_POSES_LOKALU_ADRES_ALT&gt;{ data($req/m:NrPosesLokaluAdresAlt) }&lt;/fml:DC_NR_POSES_LOKALU_ADRES_ALT&gt;
					else ()
			}
			{
				if($req/m:MiastoAdresAlt)
					then &lt;fml:DC_MIASTO_ADRES_ALT&gt;{ data($req/m:MiastoAdresAlt) }&lt;/fml:DC_MIASTO_ADRES_ALT&gt;
					else ()
			}
			{
				if($req/m:WojewodztwoKrajAdresAlt)
					then &lt;fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT&gt;{ data($req/m:WojewodztwoKrajAdresAlt) }&lt;/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT&gt;
					else ()
			}
			{
				if($req/m:KodPocztowyAdresAlt)
					then &lt;fml:DC_KOD_POCZTOWY_ADRES_ALT&gt;{ data($req/m:KodPocztowyAdresAlt) }&lt;/fml:DC_KOD_POCZTOWY_ADRES_ALT&gt;
					else ()
			}
			{
				if($req/m:KodKraju)
					then &lt;fml:DC_KOD_KRAJU&gt;{ data($req/m:KodKraju) }&lt;/fml:DC_KOD_KRAJU&gt;
					else ()
			}
			{
				if($req/m:DataWprowadzenia)
					then &lt;fml:DC_DATA_WPROWADZENIA&gt;{ data($req/m:DataWprowadzenia) }&lt;/fml:DC_DATA_WPROWADZENIA&gt;
					else ()
			}
			{
				if($req/m:DataKoncowa)
					then &lt;fml:DC_DATA_KONCOWA&gt;{ data($req/m:DataKoncowa) }&lt;/fml:DC_DATA_KONCOWA&gt;
					else ()
			}
			{
				if($req/m:AlternatywnyNrTelefonu)
					then &lt;fml:DC_ALTERNATYWNY_NR_TELEFONU&gt;{ data($req/m:AlternatywnyNrTelefonu) }&lt;/fml:DC_ALTERNATYWNY_NR_TELEFONU&gt;
					else ()
			}
			{
				if($req/m:KasowacPrzyWygasnieciu)
					then &lt;fml:DC_KASOWAC_PRZY_WYGASNIECIU&gt;{ data($req/m:KasowacPrzyWygasnieciu) }&lt;/fml:DC_KASOWAC_PRZY_WYGASNIECIU&gt;
					else ()
			}
			{
				if($req/m:TypAdresu)
					then &lt;fml:DC_TYP_ADRESU&gt;{ data($req/m:TypAdresu) }&lt;/fml:DC_TYP_ADRESU&gt;
					else ()
			}
                        &lt;DC_TYP_ZMIANY&gt;M&lt;/DC_TYP_ZMIANY&gt;
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMModifyCustProdAddrRequest($body/m:CRMModifyCustProdAddrRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>