<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns0="urn:filtersandmessages.entities.be.dcl";
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns2="urn:errors.hlbsentities.be.dcl";
declare namespace ns1="urn:dictionaries.be.dcl";
declare namespace ns3="http://www.w3.org/2001/XMLSchema";
declare namespace ns4="urn:be.services.dcl";
declare namespace ns5="urn:card.entities.be.dcl";
declare namespace fml="";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function getElementsForInvokeResponse($parm as element(fml:FML32)) as element()*
{

&lt;ns4:invokeResponse&gt;
  &lt;ns4:response&gt;
    &lt;ns0:ResponseMessage&gt;
      {
        if (data($parm/DC_OPIS_BLEDU)) then
           &lt;ns0:result?&gt;false&lt;/ns0:result&gt;
        else
           &lt;ns0:result?&gt;true&lt;/ns0:result&gt;
      }
    &lt;/ns0:ResponseMessage&gt;
  &lt;/ns4:response&gt;
&lt;/ns4:invokeResponse&gt;
};

&lt;soap:Body&gt;
  {getElementsForInvokeResponse($body/FML32)}
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>