<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Tomasz Krajewski  
 : @version 1.0
 : @since   2010-06-24
 :
 : $Proxy Services/Accounts/getAccountDetails/PRIMEGetCustProductDetailResponse.xq$
 :
 :)

declare namespace sz = "http://schemas.datacontract.org/2004/07/SzrekLibrary.Struktures";
declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductDetailResponse($bdy as element(m:PRIMEGetCustProductDetailResponse))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			&lt;fml:PT_FIRST_PRODUCT_FEATURE/&gt;
			&lt;fml:PT_SECOND_PRODUCT_FEATURE/&gt;
			&lt;fml:PT_THIRD_PRODUCT_FEATURE/&gt;
			&lt;fml:PT_FOURTH_PRODUCT_FEATURE/&gt;
			&lt;fml:PT_FIFTH_PRODUCT_FEATURE/&gt;
			&lt;fml:PT_SOURCE_PRODUCT_CODE/&gt;
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:CI_WLASCICIEL )
					then &lt;fml:NF_TXNAD_OWNERCIF&gt;{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:CI_WLASCICIEL ) }&lt;/fml:NF_TXNAD_OWNERCIF&gt;
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:B_KOD_WALUTY)
					then &lt;fml:NF_CURREC_CURRENCYCODE&gt;{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:B_KOD_WALUTY) }&lt;/fml:NF_CURREC_CURRENCYCODE&gt;
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:B_SALDO)
					then &lt;fml:NF_TXNAD_CURRENTBALANCE&gt;{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:B_SALDO) }&lt;/fml:NF_TXNAD_CURRENTBALANCE&gt;
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:B_DOST_SRODKI)
					then &lt;fml:NF_TXNAD_AVAILABLEBALANCE&gt;{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:B_DOST_SRODKI) }&lt;/fml:NF_TXNAD_AVAILABLEBALANCE&gt;
					else ()
			}
			(:{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:B_BLOKADA)
					then &lt;fml:NF_HOLD_HOLDAMOUNT&gt;{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:B_BLOKADA) }&lt;/fml:NF_HOLD_HOLDAMOUNT&gt;
					else ()
			}:)
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:CI_PRODUCT_NAME_ORYGINAL)
					then &lt;fml:NF_TXNAD_ACCOUNTNAME&gt;{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:CI_PRODUCT_NAME_ORYGINAL) }&lt;/fml:NF_TXNAD_ACCOUNTNAME&gt;
					else ()
			}
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:DC_ODDZIAL)
					then &lt;fml:NF_BRANCC_BRANCHCODE&gt;{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:DC_ODDZIAL) }&lt;/fml:NF_BRANCC_BRANCHCODE&gt;
					else ()
			}
			&lt;fml:NF_ACCEST_ACCESSTYPE/&gt;
			&lt;fml:NF_TXNAD_SHOWBALANCE&gt;1&lt;/fml:NF_TXNAD_SHOWBALANCE&gt;
			&lt;fml:NF_TXNAD_ACCOUNTSPECIFICTY&gt;IS&lt;/fml:NF_TXNAD_ACCOUNTSPECIFICTY&gt;
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:CI_WLASCICIEL )
					then &lt;fml:NF_TXNAD_OWNERDATAEDITABLE&gt;0&lt;/fml:NF_TXNAD_OWNERDATAEDITABLE&gt;
					else &lt;fml:NF_TXNAD_OWNERDATAEDITABLE&gt;1&lt;/fml:NF_TXNAD_OWNERDATAEDITABLE&gt;
			}			
			{
				if($bdy/m:PRIMEGetCustProductDetailResult/sz:NF_PRODUCT_STAT)
					then &lt;fml:NF_ACCOUS_ACCOUNTSTATUS&gt;{ data($bdy/m:PRIMEGetCustProductDetailResult/sz:NF_PRODUCT_STAT) }&lt;/fml:NF_ACCOUS_ACCOUNTSTATUS&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ 
 if ($body/m:PRIMEGetCustProductDetailResponse)
    then xf:mapPRIMEGetCustProductDetailResponse($body/m:PRIMEGetCustProductDetailResponse) 
    else  
 if ($body/fml:FML32)
    then $body/fml:FML32
    else  ()
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>