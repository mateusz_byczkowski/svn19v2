<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="postCloseAccount.wsdl"::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="postCloseAccount.wsdl"::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns0:invokeResponse" location="postCloseAccount.wsdl"::)
(:: pragma bea:global-element-return element="ns8:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns9 = "urn:operations.entities.be.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns6 = "urn:acceptance.entities.be.dcl";
declare namespace ns10 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns7 = "urn:productstree.entities.be.dcl";
declare namespace ns11 = "urn:entities.be.dcl";
declare namespace ns8 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns2 = "urn:transactionbasketdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:accounts.entities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/postCloseAccount/postCloseAccountTLRequest/";

declare function xf:postCloseAccountTLRequest($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke),
    $invokeResponse1 as element(ns0:invokeResponse))
    as element(ns8:transactionLogEntry) {
        &lt;ns8:transactionLogEntry&gt;
            {
                for $businessTransactionType in $invoke1/ns0:transaction/ns9:Transaction/ns9:businessTransactionType/ns5:BusinessTransactionType/ns5:businessTransactionType
                return
                    &lt;businessTransactionType&gt;{ data($businessTransactionType) }&lt;/businessTransactionType&gt;
            }
            {
                for $cashTransactionBasketID in $invoke1/ns0:transaction/ns9:Transaction/ns9:cashTransactionBasketID
                return
                    &lt;cashTransactionBasketID&gt;{ data($cashTransactionBasketID) }&lt;/cashTransactionBasketID&gt;
            }
            {
                for $TransactionMa in $invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa
                return
                    &lt;credit&gt;
                        {
                            for $accountNumber in $TransactionMa/ns9:accountNumber
                            return
                                &lt;accountNumber&gt;{ data($accountNumber) }&lt;/accountNumber&gt;
                        }
                        {
                            for $address in $TransactionMa/ns9:address
                            return
                                &lt;address&gt;{ data($address) }&lt;/address&gt;
                        }
                        {
                            for $amountMa in $TransactionMa/ns9:amountMa
                            return
                                &lt;amount&gt;{ xs:decimal( data($amountMa) ) }&lt;/amount&gt;
                        }
                        {
                            for $amountMaEquiv in $TransactionMa/ns9:amountMaEquiv
                            return
                                &lt;amountPLN&gt;{ xs:decimal( data($amountMaEquiv) ) }&lt;/amountPLN&gt;
                        }
                        {
                            for $city in $TransactionMa/ns9:city
                            return
                                &lt;city&gt;{ data($city) }&lt;/city&gt;
                        }
                        {
                            for $currencyCode in $TransactionMa/ns9:currencyCode/ns4:CurrencyCode/ns4:currencyCode
                            return
                                &lt;currencyCode&gt;{ data($currencyCode) }&lt;/currencyCode&gt;
                        }
                        {
                            for $rate in $TransactionMa/ns9:rate
                            return
                                &lt;exchangeRate&gt;{ xs:decimal( data($rate) ) }&lt;/exchangeRate&gt;
                        }
                        {
                            for $name in $TransactionMa/ns9:name
                            return
                                &lt;name&gt;{ data($name) }&lt;/name&gt;
                        }
                        {
                            for $nameSecond in $TransactionMa/ns9:nameSecond
                            return
                                &lt;nameSecond&gt;{ data($nameSecond) }&lt;/nameSecond&gt;
                        }
                        {
                            for $zipCode in $TransactionMa/ns9:zipCode
                            return
                                &lt;zipCode&gt;{ data($zipCode) }&lt;/zipCode&gt;
                        }
                    &lt;/credit&gt;
            }
            {
                for $csrMessageType in $invoke1/ns0:transaction/ns9:Transaction/ns9:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType
                return
                    &lt;csrMessageType&gt;{ data($csrMessageType) }&lt;/csrMessageType&gt;
            }
            {
                for $TransactionWn in $invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn
                return
                    &lt;debit&gt;
                        {
                            for $accountNumber in $TransactionWn/ns9:accountNumber
                            return
                                &lt;accountNumber&gt;{ data($accountNumber) }&lt;/accountNumber&gt;
                        }
                        {
                            for $address in $TransactionWn/ns9:address
                            return
                                &lt;address&gt;{ data($address) }&lt;/address&gt;
                        }
                        {
                            for $amountWn in $TransactionWn/ns9:amountWn
                            return
                                &lt;amount&gt;{ xs:decimal( data($amountWn) ) }&lt;/amount&gt;
                        }
                        {
                            for $amountWnEquiv in $TransactionWn/ns9:amountWnEquiv
                            return
                                &lt;amountPLN&gt;{ xs:decimal( data($amountWnEquiv) ) }&lt;/amountPLN&gt;
                        }
                        {
                            for $city in $TransactionWn/ns9:city
                            return
                                &lt;city&gt;{ data($city) }&lt;/city&gt;
                        }
                        {
                            for $currencyCode in $TransactionWn/ns9:currencyCode/ns4:CurrencyCode/ns4:currencyCode
                            return
                                &lt;currencyCode&gt;{ data($currencyCode) }&lt;/currencyCode&gt;
                        }
                        {
                            for $rate in $TransactionWn/ns9:rate
                            return
                                &lt;exchangeRate&gt;{ xs:decimal( data($rate) ) }&lt;/exchangeRate&gt;
                        }
                        {
                            for $name in $TransactionWn/ns9:name
                            return
                                &lt;name&gt;{ data($name) }&lt;/name&gt;
                        }
                        {
                            for $nameSecond in $TransactionWn/ns9:nameSecond
                            return
                                &lt;nameSecond&gt;{ data($nameSecond) }&lt;/nameSecond&gt;
                        }
                        {
                            for $zipCode in $TransactionWn/ns9:zipCode
                            return
                                &lt;zipCode&gt;{ data($zipCode) }&lt;/zipCode&gt;
                        }
                    &lt;/debit&gt;
            }
            {
                let $result :=
                    for $Disposer in $invoke1/ns0:transaction/ns9:Transaction/ns9:disposerList/ns9:Disposer
                    return
                        &lt;disposer1&gt;
                            {
                                for $address in $Disposer/ns9:address
                                return
                                    &lt;address&gt;{ data($address) }&lt;/address&gt;
                            }
                            {
                                for $cif in $Disposer/ns9:cif
                                return
                                    &lt;cif&gt;{ data($cif) }&lt;/cif&gt;
                            }
                            {
                                for $citizenshipCode in $Disposer/ns9:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode
                                return
                                    &lt;citizenship&gt;{ data($citizenshipCode) }&lt;/citizenship&gt;
                            }
                            {
                                for $city in $Disposer/ns9:city
                                return
                                    &lt;city&gt;{ data($city) }&lt;/city&gt;
                            }
                            {
                                for $countryCode in $Disposer/ns9:countryCode/ns4:CountryCode/ns4:countryCode
                                return
                                    &lt;countryCode&gt;{ data($countryCode) }&lt;/countryCode&gt;
                            }
                            {
                                for $dateOfBirth in $Disposer/ns9:dateOfBirth
                                return
                                    &lt;dateOfBirth&gt;{ xs:date( data($dateOfBirth) ) }&lt;/dateOfBirth&gt;
                            }
                            {
                                for $documentNumber in $Disposer/ns9:documentNumber
                                return
                                    &lt;documentNumber&gt;{ data($documentNumber) }&lt;/documentNumber&gt;
                            }
                            {
                                for $firstName in $Disposer/ns9:firstName
                                return
                                    &lt;firstName&gt;{ data($firstName) }&lt;/firstName&gt;
                            }
                            {
                                for $lastName in $Disposer/ns9:lastName
                                return
                                    &lt;lastName&gt;{ data($lastName) }&lt;/lastName&gt;
                            }
                            {
                                for $pesel in $Disposer/ns9:pesel
                                return
                                    &lt;pesel&gt;{ data($pesel) }&lt;/pesel&gt;
                            }
                            {
                                for $zipCode in $Disposer/ns9:zipCode
                                return
                                    &lt;zipCode&gt;{ data($zipCode) }&lt;/zipCode&gt;
                            }
                        &lt;/disposer1&gt;
                return
                    $result[1]
            }
            {
                let $result :=
                    for $Disposer in $invoke1/ns0:transaction/ns9:Transaction/ns9:disposerList/ns9:Disposer
                    return
                        &lt;disposer2&gt;
                            {
                                for $address in $Disposer/ns9:address
                                return
                                    &lt;address&gt;{ data($address) }&lt;/address&gt;
                            }
                            {
                                for $cif in $Disposer/ns9:cif
                                return
                                    &lt;cif&gt;{ data($cif) }&lt;/cif&gt;
                            }
                            {
                                for $citizenshipCode in $Disposer/ns9:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode
                                return
                                    &lt;citizenship&gt;{ data($citizenshipCode) }&lt;/citizenship&gt;
                            }
                            {
                                for $city in $Disposer/ns9:city
                                return
                                    &lt;city&gt;{ data($city) }&lt;/city&gt;
                            }
                            {
                                for $countryCode in $Disposer/ns9:countryCode/ns4:CountryCode/ns4:countryCode
                                return
                                    &lt;countryCode&gt;{ data($countryCode) }&lt;/countryCode&gt;
                            }
                            {
                                for $dateOfBirth in $Disposer/ns9:dateOfBirth
                                return
                                    &lt;dateOfBirth&gt;{ data($dateOfBirth) }&lt;/dateOfBirth&gt;
                            }
                            {
                                for $documentNumber in $Disposer/ns9:documentNumber
                                return
                                    &lt;documentNumber&gt;{ data($documentNumber) }&lt;/documentNumber&gt;
                            }
                            {
                                for $documentTypeForTxn in $Disposer/ns9:documentType/ns5:DocumentTypeForTxn/ns5:documentTypeForTxn
                                return
                                    &lt;documentType&gt;{ data($documentTypeForTxn) }&lt;/documentType&gt;
                            }
                            {
                                for $firstName in $Disposer/ns9:firstName
                                return
                                    &lt;firstName&gt;{ data($firstName) }&lt;/firstName&gt;
                            }
                            {
                                for $lastName in $Disposer/ns9:lastName
                                return
                                    &lt;lastName&gt;{ data($lastName) }&lt;/lastName&gt;
                            }
                            {
                                for $pesel in $Disposer/ns9:pesel
                                return
                                    &lt;pesel&gt;{ data($pesel) }&lt;/pesel&gt;
                            }
                            {
                                for $zipCode in $Disposer/ns9:zipCode
                                return
                                    &lt;zipCode&gt;{ data($zipCode) }&lt;/zipCode&gt;
                            }
                        &lt;/disposer2&gt;
                return
                    $result[2]
            }
            {
                for $dtTransactionType in $invoke1/ns0:transaction/ns9:Transaction/ns9:dtTransactionType/ns5:DtTransactionType/ns5:dtTransactionType
                return
                    &lt;dtTransactionType&gt;{ data($dtTransactionType) }&lt;/dtTransactionType&gt;
            }
            {
                let $UserTxnSession := $invoke1/ns0:userTxnSession/ns1:UserTxnSession
                return
                    &lt;executor&gt;
                        {
                            for $branchCode in $invoke1/ns0:branchCode/ns4:BranchCode/ns4:branchCode
                            return
                                &lt;branchNumber&gt;{ xs:int( data($branchCode) ) }&lt;/branchNumber&gt;
                        }
                        {
                            for $userID in $UserTxnSession/ns1:user/ns11:User/ns11:userID
                            return
                                &lt;executorID&gt;{ data($userID) }&lt;/executorID&gt;
                        }
                        {
                            for $userFirstName in $UserTxnSession/ns1:user/ns11:User/ns11:userFirstName
                            return
                                &lt;firstName&gt;{ data($userFirstName) }&lt;/firstName&gt;
                        }
                        {
                            for $userLastName in $UserTxnSession/ns1:user/ns11:User/ns11:userLastName
                            return
                                &lt;lastName&gt;{ data($userLastName) }&lt;/lastName&gt;
                        }
                        {
                            for $tellerID in $UserTxnSession/ns1:teller/ns1:Teller/ns1:tellerID
                            return
                                &lt;tellerID&gt;{ data($tellerID) }&lt;/tellerID&gt;
                        }
                        {
                            for $tillID in $UserTxnSession/ns1:till/ns1:Till/ns1:tillID
                            return
                                &lt;tillNumber&gt;{ data($tillID) }&lt;/tillNumber&gt;
                        }
                        {
                            for $beUserId in $invokeResponse1/ns0:backendResponse/ns9:BackendResponse/ns9:beUserId
                            return
                                &lt;userID&gt;{ data($beUserId) }&lt;/userID&gt;
                        }
                    &lt;/executor&gt;
            }
            {
                for $extendedCSRMessageType in $invoke1/ns0:transaction/ns9:Transaction/ns9:extendedCSRMessageType/ns5:ExtendedCSRMessageType/ns5:extendedCSRMessageType
                return
                    &lt;extendedCSRMessageType&gt;{ data($extendedCSRMessageType) }&lt;/extendedCSRMessageType&gt;
            }
            &lt;hlbsName&gt;postCloseAccount&lt;/hlbsName&gt;
            {
                for $orderedBy in $invoke1/ns0:transaction/ns9:Transaction/ns9:orderedBy
                return
                    &lt;orderedBy&gt;{ data($orderedBy) }&lt;/orderedBy&gt;
            }
            {
                for $putDownDate in $invoke1/ns0:transaction/ns9:Transaction/ns9:putDownDate
                return
                    &lt;putDownDate&gt;{ xs:date( data($putDownDate) ) }&lt;/putDownDate&gt;
            }
            &lt;timestamp&gt;{ data($header1/ns0:msgHeader/ns0:timestamp) }&lt;/timestamp&gt;
            {
                let $acceptTask := $invoke1/ns0:acceptTask
                return
                    &lt;tlAcceptance?&gt;
                        {
                            for $acceptorFirstName in $acceptTask/ns6:AcceptTask/ns6:acceptorFirstName
                            return
                                &lt;acceptorFirstName&gt;{ data($acceptorFirstName) }&lt;/acceptorFirstName&gt;
                        }
                        {
                            for $acceptorLastName in $acceptTask/ns6:AcceptTask/ns6:acceptorLastName
                            return
                                &lt;acceptorLastName&gt;{ data($acceptorLastName) }&lt;/acceptorLastName&gt;
                        }
                        {
                            for $acceptor in $acceptTask/ns6:AcceptTask/ns6:acceptor
                            return
                                &lt;acceptorSkp&gt;{ data($acceptor) }&lt;/acceptorSkp&gt;
                        }
                        {
                            for $AcceptItem in $acceptTask/ns6:AcceptTask/ns6:acceptItemList/ns6:AcceptItem
                            return
                                &lt;tlAcceptanceTitleList&gt;
                                    {
                                        for $acceptItemTitle in $AcceptItem/ns6:acceptItemTitle/ns10:AcceptItemTitle/ns10:acceptItemTitle
                                        return
                                            &lt;acceptItemTitle&gt;{ data($acceptItemTitle) }&lt;/acceptItemTitle&gt;
                                    }
                                &lt;/tlAcceptanceTitleList&gt;
                        }
                    &lt;/tlAcceptance&gt;
            }
            {
                let $acceptanceForBE := $invoke1/ns0:acceptanceForBE
                return
                    &lt;tlAcceptanceForBE?&gt;
                        {
                            for $acceptorFirstName in $acceptanceForBE/ns9:AcceptanceForBE/ns9:acceptorFirstName
                            return
                                &lt;acceptorFirstName&gt;{ data($acceptorFirstName) }&lt;/acceptorFirstName&gt;
                        }
                        {
                            for $acceptorLastName in $acceptanceForBE/ns9:AcceptanceForBE/ns9:acceptorLastName
                            return
                                &lt;acceptorLastName&gt;{ data($acceptorLastName) }&lt;/acceptorLastName&gt;
                        }
                        {
                            for $acceptorSKP in $acceptanceForBE/ns9:AcceptanceForBE/ns9:acceptorSKP
                            return
                                &lt;acceptorSkp&gt;{ data($acceptorSKP) }&lt;/acceptorSkp&gt;
                        }
                        {
                            for $flag in $acceptanceForBE/ns9:AcceptanceForBE/ns9:flag
                            return
                                &lt;flag&gt;{ data($flag) }&lt;/flag&gt;
                        }
                        {
                            for $BeErrorCode in $acceptanceForBE/ns9:AcceptanceForBE/ns9:beErrorCodeList/ns9:BeErrorCode
                            return
                                &lt;tlErrorCodeList?&gt;
                                    &lt;errorCode?&gt;
                                        {
                                            for $errorCode in $BeErrorCode/ns9:errorCode/ns5:BackendErrorCode/ns5:errorCode
                                            return
                                                &lt;errorCode&gt;{ data($errorCode) }&lt;/errorCode&gt;
                                        }
                                        &lt;systemId&gt;1&lt;/systemId&gt;
                                    &lt;/errorCode&gt;
                                &lt;/tlErrorCodeList&gt;
                        }
                    &lt;/tlAcceptanceForBE&gt;
            }
            {
                let $Account := $invoke1/ns0:account/ns3:Account
                return
                    &lt;tlAccountList?&gt;
                        {
                            for $accountNumber in $Account/ns3:accountNumber
                            return
                                &lt;accountNumber&gt;{ data($accountNumber) }&lt;/accountNumber&gt;
                        }
                        {
                            for $idProductDefinition in $Account/ns3:productDefinition/ns7:ProductDefinition/ns7:idProductDefinition
                            return
                                &lt;productId&gt;{ data($idProductDefinition) }&lt;/productId&gt;
                        }
                    &lt;/tlAccountList&gt;
            }
            {
                let $backendResponse := $invokeResponse1/ns0:backendResponse
                return
                    &lt;tlBackendResponse?&gt;
                        {
                            for $balanceCredit in $backendResponse/ns9:BackendResponse/ns9:balanceCredit
                            return
                                &lt;balanceCredit&gt;{ xs:decimal( data($balanceCredit) ) }&lt;/balanceCredit&gt;
                        }
                        {
                            for $balanceDebit in $backendResponse/ns9:BackendResponse/ns9:balanceDebit
                            return
                                &lt;balanceDebit&gt;{ xs:decimal( data($balanceDebit) ) }&lt;/balanceDebit&gt;
                        }
                        {
                            for $dateTime in $backendResponse/ns9:BackendResponse/ns9:dateTime
                            return
                                &lt;dateTime&gt;{ data($dateTime) }&lt;/dateTime&gt;
                        }
                        {
                            for $icbsDate in $backendResponse/ns9:BackendResponse/ns9:icbsDate
                            return
                                &lt;icbsDate&gt;{ xs:date( data($icbsDate)) }&lt;/icbsDate&gt;
                        }
                        {
                            for $icbsSessionNumber in $backendResponse/ns9:BackendResponse/ns9:icbsSessionNumber
                            return
                                &lt;icbsSessionNumber&gt;{ data($icbsSessionNumber) }&lt;/icbsSessionNumber&gt;
                        }
                        {
                            for $psTransactionNumber in $backendResponse/ns9:BackendResponse/ns9:psTransactionNumber
                            return
                                &lt;psTransactionNumber&gt;{ data($psTransactionNumber) }&lt;/psTransactionNumber&gt;
                        }
                        {
                            for $BeErrorCode in $backendResponse/ns9:BackendResponse/ns9:beErrorCodeList/ns9:BeErrorCode
                            return
                                &lt;tlErrorCodeList?&gt;
                                    &lt;errorCode?&gt;
                                        {
                                            for $errorCode in $BeErrorCode/ns9:errorCode/ns5:BackendErrorCode/ns5:errorCode
                                            return
                                                &lt;errorCode&gt;{ data($errorCode) }&lt;/errorCode&gt;
                                        }
                                        &lt;systemId&gt;1&lt;/systemId&gt;
                                    &lt;/errorCode&gt;
                                &lt;/tlErrorCodeList&gt;
                        }
                        {
                            for $transactionRefNumber in $backendResponse/ns9:BackendResponse/ns9:transactionRefNumber
                            return
                                &lt;transactionRefNumber&gt;{ data($transactionRefNumber) }&lt;/transactionRefNumber&gt;
                        }
                    &lt;/tlBackendResponse&gt;
            }
            {
                for $transactionDate in $invoke1/ns0:transaction/ns9:Transaction/ns9:transactionDate
                return
                    &lt;transactionDate&gt;{ xs:date(data($transactionDate) ) }&lt;/transactionDate&gt;
            }
            {
                for $transactionGroupID in $invoke1/ns0:transaction/ns9:Transaction/ns9:transactionGroupID
                return
                    &lt;transactionGroupID&gt;{ data($transactionGroupID) }&lt;/transactionGroupID&gt;
            }
            &lt;transactionID&gt;{ data($header1/ns0:transHeader/ns0:transId) }&lt;/transactionID&gt;
            {
                for $transactionStatus in $invokeResponse1/ns0:transactionOut/ns9:Transaction/ns9:transactionStatus/ns4:TransactionStatus/ns4:transactionStatus
                return
                    &lt;transactionStatus&gt;{ data($transactionStatus) }&lt;/transactionStatus&gt;
            }
            {
                for $title in $invoke1/ns0:transaction/ns9:Transaction/ns9:title
                return
                    &lt;transactionTitle&gt;{ data($title) }&lt;/transactionTitle&gt;
            }
        &lt;/ns8:transactionLogEntry&gt;
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;
declare variable $invokeResponse1 as element(ns0:invokeResponse) external;

&lt;soap-env:Body&gt;{
xf:postCloseAccountTLRequest($header1,
    $invoke1,
    $invokeResponse1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>