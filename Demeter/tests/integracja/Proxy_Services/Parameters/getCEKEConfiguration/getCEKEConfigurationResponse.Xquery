<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace urn = "urn:be.services.dcl";
declare namespace urn1 = "urn:cif.entities.be.dcl";
declare namespace urn2 = "urn:dictionaries.be.dcl";
declare namespace urn3 = "urn:cekedict.dictionaries.be.dcl";
declare namespace urn4 = "urn:ceke.entities.be.dcl";



declare function xf:mapgetCEKEConfigurationResponse($fml as element(fml:FML32))
	as element(urn:invokeResponse) {
		&lt;urn:invokeResponse&gt;
			&lt;urn:packageCEKE&gt;

			{
				let $E_PACKAGE_ID := $fml/fml:E_PACKAGE_ID
				let $E_PACKAGE_NAME := $fml/fml:E_PACKAGE_NAME
				let $E_PROFILE_ID := $fml/fml:E_PROFILE_ID
				let $E_PROFILE_NAME := $fml/fml:E_PROFILE_NAME
				let $E_PROFILE_DESCRIPTION := $fml/fml:E_PROFILE_DESCRIPTION
				let $E_HIGH_SECURITY_ALLOWED := $fml/fml:E_HIGH_SECURITY_ALLOWED
				let $E_CHNL_INTERNET := $fml/fml:E_CHNL_INTERNET
				let $E_CHNL_TELEFON := $fml/fml:E_CHNL_TELEFON
				let $E_CHNL_GSM := $fml/fml:E_CHNL_GSM
				let $E_CHNL_WAP := $fml/fml:E_CHNL_WAP
				let $E_MAX_SEQNO := $fml/fml:E_MAX_SEQNO

				let $id := distinct-values($fml/fml:E_PACKAGE_ID)
				let $name := distinct-values($fml/fml:E_PACKAGE_NAME)
				let $customerType := $fml/urn2:customerType

				for $g in distinct-values($fml/fml:E_PACKAGE_ID)
				return
					&lt;urn4:PackageCEKE&gt;
					{
							&lt;urn4:packageCEKE?&gt;{$g}&lt;/urn4:packageCEKE&gt;
					}
					{
						for $idvalue at $p in $id
						return
							if ($idvalue = $g) then
							&lt;urn4:description&gt;{$name[$p]}&lt;/urn4:description&gt;
							else()
					}
					{
						if ($customerType = "F") then
						(
							&lt;urn4:limitTransactionObligate&gt;false&lt;/urn4:limitTransactionObligate&gt;
						) else if ($customerType = "P") then (
							&lt;urn4:limitTransactionObligate&gt;true&lt;/urn4:limitTransactionObligate&gt;
						) else ()
					}
					{	&lt;urn4:profileCEKEList&gt;
							{
								for $it at $p in $fml/fml:E_PROFILE_ID
								return
								&lt;urn4:ProfileCEKE&gt;
								{
									if (data($fml/fml:E_PACKAGE_ID[$p]) = $g) then
									(


										if ($E_PROFILE_ID[$p]) then
											&lt;urn4:profileCEKE&gt;{ data($E_PROFILE_ID[$p]) }&lt;/urn4:profileCEKE&gt;
										else()
									)else ()
								}
								{
									if (data($fml/fml:E_PACKAGE_ID[$p]) = $g) then
									(
										if ($E_PROFILE_DESCRIPTION[$p]) then
											&lt;urn4:description&gt;{ data($E_PROFILE_NAME[$p]) }&lt;/urn4:description&gt;
										else()
									) else ()
								}
								{
									if (data($fml/fml:E_PACKAGE_ID[$p]) = $g) then
									(
										if ($E_MAX_SEQNO[$p]) then
												&lt;urn4:maxSeqNo&gt;{ data($E_MAX_SEQNO[$p]) }&lt;/urn4:maxSeqNo&gt;
											else()
									) else ()
								}
								{
									if (data($fml/fml:E_PACKAGE_ID[$p]) = $g) then
									(
										if($E_HIGH_SECURITY_ALLOWED) then (
													if(data($E_HIGH_SECURITY_ALLOWED[$p]) = "1") then
														&lt;urn4:highAuthorizationAllowed?&gt;true&lt;/urn4:highAuthorizationAllowed&gt;
													else if (data($E_HIGH_SECURITY_ALLOWED[$p]) = "0") then (
														&lt;urn4:highAuthorizationAllowed?&gt;false&lt;/urn4:highAuthorizationAllowed&gt;
													) else ()
										) else ()
									) else ()
								}

								{
									if (data($fml/fml:E_PACKAGE_ID[$p]) = $g) then
									(
										&lt;urn4:customerChannelCEKEList&gt;
										{
											if(data($E_CHNL_INTERNET[$p]) = 1) then
											(
												&lt;urn4:CustomerChannelCEKE&gt;
													&lt;urn4:channelID&gt;
														&lt;urn3:ChannelCEKE&gt;
															&lt;urn3:channelCEKE&gt;i&lt;/urn3:channelCEKE&gt;
														&lt;/urn3:ChannelCEKE&gt;
													&lt;/urn4:channelID&gt;
												&lt;/urn4:CustomerChannelCEKE&gt;
											) else ()
										}
										{
											if(data($E_CHNL_TELEFON[$p]) = 1) then
											(
												&lt;urn4:CustomerChannelCEKE&gt;
													&lt;urn4:channelID&gt;
														&lt;urn3:ChannelCEKE&gt;
															&lt;urn3:channelCEKE&gt;t&lt;/urn3:channelCEKE&gt;
														&lt;/urn3:ChannelCEKE&gt;
													&lt;/urn4:channelID&gt;
												&lt;/urn4:CustomerChannelCEKE&gt;
											) else ()
										}
										{
											if(data($E_CHNL_GSM[$p]) = 1) then
											(
												&lt;urn4:CustomerChannelCEKE&gt;
													&lt;urn4:channelID&gt;
														&lt;urn3:ChannelCEKE&gt;
															&lt;urn3:channelCEKE&gt;g&lt;/urn3:channelCEKE&gt;
														&lt;/urn3:ChannelCEKE&gt;
													&lt;/urn4:channelID&gt;
												&lt;/urn4:CustomerChannelCEKE&gt;
											) else ()
										}
										{
											if(data($E_CHNL_WAP[$p]) = 1) then
											(
												&lt;urn4:CustomerChannelCEKE&gt;
													&lt;urn4:channelID&gt;
														&lt;urn3:ChannelCEKE&gt;
															&lt;urn3:channelCEKE&gt;w&lt;/urn3:channelCEKE&gt;
														&lt;/urn3:ChannelCEKE&gt;
													&lt;/urn4:channelID&gt;
												&lt;/urn4:CustomerChannelCEKE&gt;
											) else ()
										}
										&lt;/urn4:customerChannelCEKEList&gt;
								 ) else ()
								}
								&lt;/urn4:ProfileCEKE&gt;
							}
						&lt;/urn4:profileCEKEList&gt;
					}
					&lt;/urn4:PackageCEKE&gt;

			}

			&lt;/urn:packageCEKE&gt;
		&lt;/urn:invokeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetCEKEConfigurationResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>