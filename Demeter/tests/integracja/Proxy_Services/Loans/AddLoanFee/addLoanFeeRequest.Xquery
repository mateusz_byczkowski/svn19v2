<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string

  return 
    if ($value &lt; 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
             fn:string(fn:year-from-date($dateIn)))
};

declare function xf:shortAccount($account as xs:string) as xs:string {
    if (fn:string-length($account) &gt; 12)
      then fn:substring($account, (fn:string-length($account))-11)
      else $account
};

&lt;soap-env:Body&gt;
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:account/m1:Account
    let $req1 := $body/m:invoke/m:loanFee/m1:LoanFee

    let $msgId := $reqh/m:msgHeader/m:msgId
    let $transId := $reqh/m:transHeader/m:transId
    let $userId  := $reqh/m:msgHeader/m:userId
    let $unitId  := $reqh/m:msgHeader/m:unitId
    return
    
    &lt;fml:FML32&gt;
      &lt;fml:DC_MSHEAD_MSGID?&gt;{ data($msgId) }&lt;/fml:DC_MSHEAD_MSGID&gt;
      &lt;fml:DC_TRN_ID?&gt;{ data($transId) }&lt;/fml:DC_TRN_ID&gt;
      &lt;fml:DC_UZYTKOWNIK?&gt;{concat("SKP:", data($userId)) }&lt;/fml:DC_UZYTKOWNIK&gt;

      {if($unitId)
          then 
              if($unitId &lt; 1000)
                then &lt;fml:DC_ODDZIAL&gt;{ data($unitId) }&lt;/fml:DC_ODDZIAL&gt;
                else &lt;fml:DC_ODDZIAL&gt;0&lt;/fml:DC_ODDZIAL&gt;
          else ()
      }

      {if($req/m1:accountNumber)
         then &lt;fml:DC_NR_RACHUNKU&gt;{ xf:shortAccount($req/m1:accountNumber) }&lt;/fml:DC_NR_RACHUNKU&gt;
         else ()
      }
      {if($req1/m1:feeAmount)
           then &lt;fml:DC_KWOTA_PROWIZJI&gt;{ data($req1/m1:feeAmount) }&lt;/fml:DC_KWOTA_PROWIZJI&gt;
           else ()
      }
      {if($req1/m1:effectiveDate and fn:string-length($req1/m1:effectiveDate)&gt;0)
           then &lt;fml:DC_DATA_WPROWADZENIA&gt;{ xf:mapDate($req1/m1:effectiveDate) }&lt;/fml:DC_DATA_WPROWADZENIA&gt;
           else ()
      }
      {if($req1/m1:deductionNumber)
           then &lt;fml:DC_NUMER_PROWIZJI&gt;{ data($req1/m1:deductionNumber) }&lt;/fml:DC_NUMER_PROWIZJI&gt;
           else ()
      }
      &lt;fml:DC_OPIS_1&gt;Prowizja&lt;/fml:DC_OPIS_1&gt;
    &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>