<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.4
 : @since   2010-01-11
 :
 : wersja WSDLa: 08-01-2010 18:35:54
 :
 : $Proxy Services/Branches/getBranchTills/getBranchTillsResponse.xq$
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/branch/getBranchTills/getBranchTillsResponse/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns3 = "urn:entities.be.dcl";
declare namespace ns4 = "urn:be.services.dcl";
declare namespace ns5 = "urn:baseauxentities.be.dcl";

declare variable $fML321 as element(ns0:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getBranchTillsResponse($fML321 as element(ns0:FML32))
    as element(ns4:invokeResponse)
{
    &lt;ns4:invokeResponse&gt;
        &lt;ns4:userTxnSessionOut&gt;{
            for $i in 1 to count($fML321/ns0:NF_USERTS_SESSIONNUMBER)
            return
                &lt;ns2:UserTxnSession&gt;

                    &lt;ns2:sessionNumber&gt;{
						data($fML321/ns0:NF_USERTS_SESSIONNUMBER[$i])
					}&lt;/ns2:sessionNumber&gt;
					
					(:
					 : niepusta data, różna od 0001-01-01
					 :)
					{
						let $sessionDate := data($fML321/ns0:NF_USERTS_SESSIONDATE[$i])
						return
							if ($sessionDate
								and $sessionDate ne '0001-01-01') then
								&lt;ns2:sessionDate&gt;{
									$sessionDate
								}&lt;/ns2:sessionDate&gt;
							else
								()
					}

					(:
					 : niepusta data, różna od '0001-01-01T00:00:00'
					 :)
					{
						let $lastChangeDate := data($fML321/ns0:NF_USERTS_LASTCHANGEDATE[$i])
						return
                    		if ($lastChangeDate
                    			and $lastChangeDate ne '0001-01-01T00:00:00') then
								&lt;ns2:lastChangeDate&gt;{
									$lastChangeDate
								}&lt;/ns2:lastChangeDate&gt;
							else
								()
                    }

                    (:
                     : status sesji:
                     : - 0 --&gt; C (zamknięta)
                     : - 1 --&gt; O (otwarta)
                     :)
					{
						let $sessionStatus := data($fML321/ns0:NF_USETSS_USERTXNSESSIONST[$i])
						return
							if ($sessionStatus) then
			                    &lt;ns2:sessionStatus&gt;
			                        &lt;ns1:UserTxnSessionStatus&gt;
			                            &lt;ns1:userTxnSessionStatus&gt;{
			                            	if ($sessionStatus eq '0') then
			                            		'C'
			                            	else
			                            		'O'
			                            }&lt;/ns1:userTxnSessionStatus&gt;
			                        &lt;/ns1:UserTxnSessionStatus&gt;
			                    &lt;/ns2:sessionStatus&gt;
							else
								()
					}

					(:
					 : dane uzytkownika
					 :)
                    &lt;ns2:user&gt;
                        &lt;ns3:User&gt;
                            &lt;ns3:userLastName&gt;{
								data($fML321/ns0:NF_USER_USERLASTNAME[$i])
							}&lt;/ns3:userLastName&gt;
							
                            &lt;ns3:userID&gt;{
								data($fML321/ns0:NF_USER_USERID[$i])
							}&lt;/ns3:userID&gt;
							
                            &lt;ns3:userFirstName&gt;{
								data($fML321/ns0:NF_USER_USERFIRSTNAME[$i])
							}&lt;/ns3:userFirstName&gt;
                        &lt;/ns3:User&gt;
                    &lt;/ns2:user&gt;

					(:
					 : dane kasy
					 :)
                    &lt;ns2:till&gt;
                        &lt;ns2:Till&gt;
                            &lt;ns2:tillID&gt;{
								data($fML321/ns0:NF_TILL_TILLID[$i])
							}&lt;/ns2:tillID&gt;
							
                            &lt;ns2:tillName&gt;{
                            	data($fML321/ns0:NF_TILL_TILLNAME[$i])
							}&lt;/ns2:tillName&gt;
							
                            &lt;ns2:tillType&gt;
                                &lt;ns1:TillType&gt;
                                    &lt;ns1:tillType&gt;{
										data($fML321/ns0:NF_TILTY_TILLTYP[$i])
									}&lt;/ns1:tillType&gt;
                                &lt;/ns1:TillType&gt;
                            &lt;/ns2:tillType&gt;
                        &lt;/ns2:Till&gt;
                    &lt;/ns2:till&gt;

					(:
					 : kasjer
					 :)
                    &lt;ns2:teller&gt;
                        &lt;ns2:Teller&gt;
                            &lt;ns2:tellerID&gt;{
								data($fML321/ns0:NF_TELLER_TELLERID[$i])
							}&lt;/ns2:tellerID&gt;
                        &lt;/ns2:Teller&gt;
                    &lt;/ns2:teller&gt;
                &lt;/ns2:UserTxnSession&gt;
		}&lt;/ns4:userTxnSessionOut&gt;
		
        &lt;ns4:bcd&gt;
            &lt;ns3:BusinessControlData&gt;
                &lt;ns3:pageControl?&gt;
                    &lt;ns5:PageControl?&gt;
                    	{
                    		let $hasNext := data($fML321/ns0:NF_PAGEC_HASNEXT)
                    		return
                    			if ($hasNext) then
									&lt;ns5:hasNext&gt;{ 
										xs:boolean($hasNext)
									}&lt;/ns5:hasNext&gt;
								else
									()
                    	}
						
                        &lt;ns5:navigationKeyDefinition?&gt;{
							data($fML321/ns0:NF_PAGEC_NAVIGATIONKEYDEFI)
						}&lt;/ns5:navigationKeyDefinition&gt;
	
                        &lt;ns5:navigationKeyValue?&gt;{
							data($fML321/ns0:NF_PAGEC_NAVIGATIONKEYVALU)
						}&lt;/ns5:navigationKeyValue&gt;
						
					&lt;/ns5:PageControl&gt;
                &lt;/ns3:pageControl&gt;
            &lt;/ns3:BusinessControlData&gt;
        &lt;/ns4:bcd&gt;
    &lt;/ns4:invokeResponse&gt;
};

&lt;soap-env:Body&gt;{
	xf:getBranchTillsResponse($fML321)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>