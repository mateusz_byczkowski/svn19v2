<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetQuestionnaireDataRequest($req as element(m:CRMGetQuestionnaireDataRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if ($req/m:CustCif) then
					&lt;fml:DC_NUMER_KLIENTA&gt;{data($req/m:CustCif)}&lt;/fml:DC_NUMER_KLIENTA&gt;
				else ()
			}
			{
				if ($req/m:QuestFromDate) then
					&lt;fml:CI_DATA_OD&gt;{data($req/m:QuestFromDate)}&lt;/fml:CI_DATA_OD&gt;
				else ()
			}
			{
				if ($req/m:QuestToDate) then
					&lt;fml:CI_DATA_DO&gt;{data($req/m:QuestToDate)}&lt;/fml:CI_DATA_DO&gt;
				else ()
			}
			{
				if ($req/m:UsrSkpNo) then
					&lt;fml:CI_SKP_AUTORA&gt;{data($req/m:UsrSkpNo)}&lt;/fml:CI_SKP_AUTORA&gt;
				else ()
			}
			{
				if ($req/m:QuestId) then
					&lt;fml:CI_ID_ANKIETY&gt;{data($req/m:QuestId)}&lt;/fml:CI_ID_ANKIETY&gt;
				else ()
			}
			{
				if ($req/m:Option) then 
					&lt;fml:CI_OPCJA&gt;{data($req/m:Option)}&lt;/fml:CI_OPCJA&gt;
				else()
			}
			{
				if ($req/m:CompanyId) then 
					&lt;fml:CI_ID_SPOLKI&gt;{data($req/m:CompanyId)}&lt;/fml:CI_ID_SPOLKI&gt;
				else()
			}
			{
				if ($req/m:LiczbaOper) then
					&lt;fml:CI_LICZBA_OPER&gt;{data($req/m:LiczbaOper)}&lt;/fml:CI_LICZBA_OPER&gt;
				else ()
			}
			{
				if($req/m:NumerPaczki) then
					&lt;fml:CI_NUMER_PACZKI&gt;{data($req/m:NumerPaczki)}&lt;/fml:CI_NUMER_PACZKI&gt;
				else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetQuestionnaireDataRequest($body/m:CRMGetQuestionnaireDataRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>