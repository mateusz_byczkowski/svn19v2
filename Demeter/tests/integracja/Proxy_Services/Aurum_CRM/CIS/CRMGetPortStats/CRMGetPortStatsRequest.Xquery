<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function local:lpadz2($i as xs:integer) as xs:string {
	if ($i &lt; 10) then fn:concat("0", xs:string($i)) else xs:string($i)
};

declare function local:formatCISDate($d as xs:date) as xs:string {
	let $y := fn:year-from-date($d)
	let $m := fn:month-from-date($d)
	let $d := fn:day-from-date($d)
	return
		fn:concat(local:lpadz2($d), "-", local:lpadz2($m), "-", xs:string($y))
};

declare function xf:mapCRMGetPortStatsRequest($req as element(m:CRMGetPortStatsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdPoziomu)
					then &lt;fml:CI_ID_POZIOMU&gt;{ data($req/m:IdPoziomu) }&lt;/fml:CI_ID_POZIOMU&gt;
					else ()
			}
			{
				if($req/m:PoziomAgregacji)
					then &lt;fml:CI_POZIOM_AGREGACJI&gt;{ data($req/m:PoziomAgregacji) }&lt;/fml:CI_POZIOM_AGREGACJI&gt;
					else ()
			}
			{
				if($req/m:DataAktualizacji) then
					&lt;fml:CI_DATA_AKTUALIZACJI&gt;{data($req/m:DataAktualizacji) }&lt;/fml:CI_DATA_AKTUALIZACJI&gt;
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				for $v in $req/m:KodProduktu
				return
					&lt;fml:CI_KOD_PRODUKTU&gt;{ data($v) }&lt;/fml:CI_KOD_PRODUKTU&gt;
			}
			

			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI&gt;{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI&gt;
					else ()
			}
			{
				if($req/m:ZakresRaportu)
					then &lt;fml:CI_ZAKRES_RAPORTU&gt;{ data($req/m:ZakresRaportu) }&lt;/fml:CI_ZAKRES_RAPORTU&gt;
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				if($req/m:ZnacznikOkresu)
					then &lt;fml:CI_ZNACZNIK_OKRESU&gt;{ data($req/m:ZnacznikOkresu) }&lt;/fml:CI_ZNACZNIK_OKRESU&gt;
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA&gt;{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $req as element(m:CRMGetPortStatsRequest) external;
&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"&gt;
{ xf:mapCRMGetPortStatsRequest($req) }
&lt;/soapenv:Body&gt;</con:xquery>
</con:xqueryEntry>