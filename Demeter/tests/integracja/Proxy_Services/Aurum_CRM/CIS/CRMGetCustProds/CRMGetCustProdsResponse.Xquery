<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";
 
declare function local:mapping($fml as element(fml:FML32)) as element() {
  <customer>
   <cif>{ data($fml/fml:DC_NUMER_KLIENTA) }</cif>
    <prd_groups>
{
 for $kodProduktu at $p in $fml/fml:CI_KOD_PRODUKTU
 let $kodGrupy1 := fn:normalize-space(data($fml/fml:CI_KOD_PRODUKTU1[$p]))
 let $kodGrupy2 := fn:normalize-space(data($fml/fml:CI_KOD_PRODUKTU2[$p]))
 let $kodGrupy3 := fn:normalize-space(data($fml/fml:CI_KOD_PRODUKTU3[$p])) return
     <prd_group code="{ fn:normalize-space($kodProduktu) }">
{
 (: depozyty biezace :)
 if($kodGrupy1 = '2') then
      (<param code="UPDATE_DATE">{ data($fml/CI_DATA_AKTUALIZACJI[$p]) }</param>,
      <param code="ACC_COUNT">{ data($fml/CI_ILOSC1[$p]) }</param>,
      <param code="ACC_LIM_COUNT">{ data($fml/CI_ILOSC2[$p]) }</param>,
      <param code="CONST_ORD_COUNT">{ data($fml/CI_ILOSC3[$p]) }</param>,
      <param code="DIR_DEBIT_COUNT">{ data($fml/CI_ILOSC4[$p]) }</param>,
      <param code="INT_OUT_OPER_COUNT">{ data($fml/CI_ILOSC5[$p]) }</param>,
      <param code="INT_IN_OPER_COUNT">{ data($fml/CI_ILOSC6[$p]) }</param>,
      <param code="BALANCE_TOTAL">{ data($fml/CI_WARTOSC1[$p]) }</param>,
      <param code="AVG_BALANCE_3M_10">{ data($fml/CI_WARTOSC2[$p]) }</param>,
      <param code="AVG_TURNOVER_OUT_3M">{ data($fml/CI_WARTOSC3[$p]) }</param>,
      <param code="AVG_TURNOVER_IN_3M">{ data($fml/CI_WARTOSC4[$p]) }</param>,
      <param code="AVG_TURNOVER_OUT_12M">{ data($fml/CI_WARTOSC5[$p]) }</param>,
      <param code="AVG_TURNOVER_IN_12M">{ data($fml/CI_WARTOSC6[$p]) }</param>,
      <param code="LIMIT_TOTAL">{ data($fml/CI_WARTOSC7[$p]) }</param>,
      <param code="INT_OUT_OPER_TOTAL">{ data($fml/CI_WARTOSC8[$p]) }</param>,
      <param code="INT_IN_OPER_TOTAL">{ data($fml/CI_WARTOSC9[$p]) }</param>,
      <param code="LAST_ACC_OPEN_DATE">{ data($fml/CI_DATA1[$p]) }</param>,
      <param code="LAST_CRED_LAUNCH_DATE">{ data($fml/CI_DATA2[$p]) }</param>,
      <param code="LAST_REQ_LIMIT_DATE">{ data($fml/CI_DATA4[$p]) }</param>)
 (: depozyty terminowe :)
 else if($kodGrupy1 = '10') then
      (<param code="UPDATE_DATE">{ data($fml/CI_DATA_AKTUALIZACJI[$p]) }</param>,
      <param code="ACC_COUNT">{ data($fml/CI_ILOSC1[$p]) }</param>,
      <param code="BALANCE_TOTAL">{ data($fml/CI_WARTOSC1[$p]) }</param>,
      <param code="AVG_BALANCE_1M">{ data($fml/CI_WARTOSC2[$p]) }</param>,
      <param code="AVG_BALANCE_3M">{ data($fml/CI_WARTOSC3[$p]) }</param>,
      <param code="FIRST_DEP_DATE">{ data($fml/CI_DATA1[$p]) }</param>,
      <param code="LAST_DEP_OPEN_DATE">{ data($fml/CI_DATA2[$p]) }</param>,
      <param code="DECLINE_DEP_DATE">{ data($fml/CI_DATA3[$p]) }</param>)
 (: kredyty, leasing, factoring :)
 else if($kodGrupy1 = '3') then
      (<param code="UPDATE_DATE">{ data($fml/CI_DATA_AKTUALIZACJI[$p]) }</param>,
      <param code="ACC_COUNT">{ data($fml/CI_ILOSC1 [$p]) }</param>,
      <param code="BALANCE_TOTAL">{ data($fml/CI_WARTOSC1[$p]) }</param>,
      <param code="AVG_CONS_1M">{ data($fml/CI_WARTOSC2[$p]) }</param>,
      <param code="AVG_CONS_12M">{ data($fml/CI_WARTOSC3[$p]) }</param>,
      <param code="INIT_COMMIT_TOTAL">{ data($fml/CI_WARTOSC7[$p]) }</param>,
      <param code="FIRST_CRED_GRANT_DATE">{ data($fml/CI_DATA1[$p]) }</param>,
      <param code="LAST_CRED_GRANT_DATE">{ data($fml/CI_DATA2[$p]) }</param>,
      <param code="NEXT_REPAYMENT_DATE">{ data($fml/CI_DATA3[$p]) }</param>,
      <param code="FINAL_REPAYMENT_DATE">{ data($fml/CI_DATA4[$p]) }</param>)
 (: Systemy samoobsgowe i elektroniczne :)
 else if($kodGrupy1 = '8') then
      (<param code="UPDATE_DATE">{ data($fml/CI_DATA_AKTUALIZACJI[$p]) }</param>,
      <param code="SERVICE_POSSESSION_FLAG">{ data($fml/CI_ILOSC1[$p]) }</param>)
 (: Karty :)
 else if($kodGrupy1 = '1') then
      (<param code="UPDATE_DATE">{ data($fml/CI_DATA_AKTUALIZACJI[$p]) }</param>,
      <param code="CARD_COUNT">{ data($fml/CI_ILOSC1[$p]) }</param>,
      <param code="CARD_LIMIT_TOTAL">{ data($fml/CI_WARTOSC7[$p]) }</param>,
      <param code="MAIN_CARD_NEXT_EXP_DATE">{ data($fml/CI_DATA1[$p]) }</param>,
      <param code="ADD_CARD_NEXT_EXP_DATE">{ data($fml/CI_DATA2[$p]) }</param>)
 (: Inwestycje :)
(: else if($kodGrupy1 = '?????') then
      (<param code="UPDATE_DATE">{ data($fml/CI_DATA_AKTUALIZACJI[$p]) }</param>,
      <param code="ACC_COUNT">{ data($fml/CI_ILOSC1[$p]) }</param>,
      <param code="BALANCE_TOTAL">{ data($fml/CI_WARTOSC1[$p]) }</param>,
      <param code="AVG_BALANCE_1M">{ data($fml/CI_WARTOSC2[$p]) }</param>,
      <param code="AVG_BALANCE_3M">{ data($fml/CI_WARTOSC3[$p]) }</param>)
:)
 (: Ubezpieczenia :)
 else if($kodGrupy1 = '4') then
      (<param code="UPDATE_DATE">{ data($fml/CI_DATA_AKTUALIZACJI[$p]) }</param>,
      <param code="ACC_COUNT">{ data($fml/CI_ILOSC1[$p]) }</param>,
      <param code="BALANCE_TOTAL">{ data($fml/CI_WARTOSC1[$p]) }</param>)
 (: Spisania centralne :)
 else if($kodGrupy1 = '12') then
      (<param code="UPDATE_DATE">{ data($fml/CI_DATA_AKTUALIZACJI[$p]) }</param>,
      <param code="ACC_COUNT">{ data($fml/CI_ILOSC1[$p]) }</param>,
      <param code="BALANCE_TOTAL">{ data($fml/CI_WARTOSC1[$p]) }</param>)
 (: Bdne - kredyty, depozyty, pozosta :)
 else if($kodGrupy1 = '15' or $kodGrupy1 = '16' or $kodGrupy1 = '17') then
      (<param code="UPDATE_DATE">{ data($fml/CI_DATA_AKTUALIZACJI[$p]) }</param>,
      <param code="ACC_COUNT">{ data($fml/CI_ILOSC1[$p]) }</param>,
      <param code="BALANCE_TOTAL">{ data($fml/CI_WARTOSC1[$p]) }</param>)
 else
  (<!-- nieznana grupa -->)
}
    </prd_group>
}
   </prd_groups>
  </customer>
};
 
declare variable $fml as element(fml:FML32) external;
<soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
<m:CRMGetCustProdsResponse>{ fn-bea:serialize(local:mapping($fml)) }</m:CRMGetCustProdsResponse>
</soapenv:Body>]]></con:xquery>
</con:xqueryEntry>