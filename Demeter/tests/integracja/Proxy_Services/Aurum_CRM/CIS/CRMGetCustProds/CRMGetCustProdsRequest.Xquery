<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function xf:mapCRMGetCustProdsRequest($req as element(m:CRMGetCustProdsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:PoziomAgregacji)
					then &lt;fml:CI_POZIOM_AGREGACJI&gt;{ data($req/m:PoziomAgregacji) }&lt;/fml:CI_POZIOM_AGREGACJI&gt;
					else ()
			}
			{
				if($req/m:KodProduktu)
					then &lt;fml:CI_KOD_PRODUKTU&gt;{ data($req/m:KodProduktu) }&lt;/fml:CI_KOD_PRODUKTU&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $req as element(m:CRMGetCustProdsRequest) external;
&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"&gt;
{ xf:mapCRMGetCustProdsRequest($req) }
&lt;/soapenv:Body&gt;</con:xquery>
</con:xqueryEntry>