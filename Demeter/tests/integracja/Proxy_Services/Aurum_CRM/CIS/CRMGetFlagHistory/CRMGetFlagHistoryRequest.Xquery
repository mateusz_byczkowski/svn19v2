<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetFlagHistoryRequest($req as element(m:CRMGetFlagHistoryRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IcbsNo)
					then &lt;fml:CI_NUMER_ODDZIALU&gt;{ data($req/m:IcbsNo) }&lt;/fml:CI_NUMER_ODDZIALU&gt;
					else ()
			}
			{
				if($req/m:CompanyId)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:CompanyId) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:AccDateFrom)
					then &lt;fml:CI_DATA_OD&gt;{ data($req/m:AccDateFrom) }&lt;/fml:CI_DATA_OD&gt;
					else ()
			}
			{
				if($req/m:AccDateTo)
					then &lt;fml:CI_DATA_DO&gt;{ data($req/m:AccDateTo) }&lt;/fml:CI_DATA_DO&gt;
					else ()
			}
			{
				if($req/m:CustCif)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:CustCif) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:SetUserId)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ&gt;{ data($req/m:SetUserId) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ&gt;
					else ()
			}
			{
				if($req/m:AccUserId)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:AccUserId) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				if($req/m:FlagType)
					then &lt;fml:CI_TYP&gt;{ data($req/m:FlagType) }&lt;/fml:CI_TYP&gt;
					else ()
			}
			{
				if($req/m:SortBy)
					then &lt;fml:CI_SORTOWANIE&gt;{ data($req/m:SortBy) }&lt;/fml:CI_SORTOWANIE&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetFlagHistoryRequest($body/m:CRMGetFlagHistoryRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>