<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare namespace urn3="urn:be.services.dcl";

declare variable $body external;
declare variable $fault external;
declare variable $headerCache external;

declare function local:fault($faultString as xs:string,
							   $detail as element())
	as element(soap-env:Fault)
{
	&lt;soap-env:Fault&gt;
		&lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
		&lt;faultstring&gt;{
			$faultString
		}&lt;/faultstring&gt;

		&lt;detail&gt;{
			$detail
		}&lt;/detail&gt;
	&lt;/soap-env:Fault&gt;
};

declare function local:errors($errorCode1 as xs:string,
								$errorCode2 as xs:string,
								$errorDescription as xs:string,
								$errorCodeFromBS as xs:string)
	as element(err:exceptionItem)
{
	&lt;err:exceptionItem&gt;
		&lt;err:errorCode1&gt;{
			$errorCode1
		}&lt;/err:errorCode1&gt;
		
		&lt;err:errorCode2&gt;{
			$errorCode2
		}&lt;/err:errorCode2&gt;
		
        {
			if (string-length($errorCodeFromBS) &gt; 0) then
				&lt;err:errorDescription&gt;{
					concat($errorCodeFromBS," : ",$errorDescription)
				}&lt;/err:errorDescription&gt;
			else
				&lt;err:errorDescription&gt;{
					$errorDescription
				}&lt;/err:errorDescription&gt;
        }
   &lt;/err:exceptionItem&gt;
};

&lt;soap-env:Body&gt;{
		let $reason := fn:substring-after(fn:substring-before(data($fault/ctx:reason), "):"), "(")
		let $urcode := fn:substring-before(fn:substring-after(fn:substring-after(data($fault/ctx:reason),":"),":"),":")
		
		(:
		 : opisy błędów
		 :)		
		let $timeoutDesc := "Wywołanie usługi przekroczyło dopuszczalny czas odpowiedzi"
		let $timeoutDescSPOUTQ := "Wywołanie usługi przekroczyło dopuszczalny czas odpowiedzi[SPOUTQ]"
		let $wrongInputDesc := "Niepoprawne dane wejściowe"
		let $dcInterfaceDownDesc := "Interfejs DC ICBS jest wyłączony - przetwarzanie niemożliwe"
		let $fmlBufferDesc := "Błąd bufora FML" 
		let $defaultUrcodeDesc := "Błąd wywołania usługi"
		let $defaultDesc := "Krytyczny bład usługi w systemie źródłowym"
		let $alsbErrorDesc := "Wystąpił bład w usłudze proxy na ALSB"
		let $noDataDesc := "Brak danych wyjściowych"
		let $transactionRejectedDesc := "Transakcja odrzucona przez interfejs DC-ICBS"
		let $transactionFailed := "Transakcja zakończona błędem przez interfejs DC-ICBS"
		let $transactionTimeout := "Timeout odczytu odpowiedzi interface'u DC-ICBS"

		(:
		 : opisy błędów z bufora wyjściowego
		 :)
		let $errorDescriptiona := fn:concat(data($body/FML32/NF_ERROR_DESCRIPTION[1]), data($body/FML32/NF_ERROR_DESCRIPTION[2]), data($body/FML32/NF_ERROR_DESCRIPTION[3]))
		let $errorDescriptionDCa := data($body/FML32/DC_OPIS_BLEDU[1])
		let $errorCode:=data($body/FML32/NF_ERROR_CODE[1])
                
        let $errorDescriptionDC := translate($errorDescriptionDCa, "za§˘’Ťg‘žl†ja¦¤ZAˇŁś•G?LŹJAĄ", "zażółćgęśląjaźńZAŻÓŁĆGĘLĄJAŃ")
        let $errorDescription := translate($errorDescriptiona, "za§˘’Ťg‘žl†ja¦¤ZAˇŁś•G?LŹJAĄ", "zażółćgęśląjaźńZAŻÓŁĆGĘLĄJAŃ")
		
		
		(: GSTR BEG
		:)
      let $bErrorSaveTransfer:= "Błąd w trakcie Księgowania"
      let $errorCodeSaveTransfer:=concat("[",data($body/FML32/TR_KOD_BLEDU_1),"] [",data($body/FML32/TR_KOD_BLEDU_2),"] [",data($body/FML32/TR_KOD_BLEDU_3),"] [" ,data($body/FML32/TR_KOD_BLEDU_4),"] [",data($body/FML32/TR_KOD_BLEDU_5),"]" )
      
      let $TMP_BLAD :=fn-bea:trim(fn:replace(data($body/FML32/TR_KOD_BLEDU_1),"000",""))

      let $TR_OPIS_BLEDU   :=fn-bea:trim(fn:normalize-space(data($body/FML32/TR_OPIS_BLEDU)))
      let $TR_KOD_BLEDU    := if ($TR_OPIS_BLEDU !="") then  $TMP_BLAD else ()

      let $TR_OPIS_BLEDU_1 :=fn-bea:trim(fn:normalize-space(data($body/FML32/TR_OPIS_BLEDU_1)))
      let $TR_KOD_BLEDU_1 := if ($TR_OPIS_BLEDU_1 !="") then  $TMP_BLAD else ()


      let $TR_OPIS_BLEDU_2 :=fn-bea:trim(fn:normalize-space(data($body/FML32/TR_OPIS_BLEDU_2)))
      let $TR_OPIS_BLEDU_3 :=fn-bea:trim(fn:normalize-space(data($body/FML32/TR_OPIS_BLEDU_3)))
      let $TR_OPIS_BLEDU_4 :=fn-bea:trim(fn:normalize-space(data($body/FML32/TR_OPIS_BLEDU_4)))
      let $TR_OPIS_BLEDU_5 :=fn-bea:trim(fn:normalize-space(data($body/FML32/TR_OPIS_BLEDU_5)))

      let $TR_KOD_BLEDU_2 :=fn-bea:trim(fn:replace(data($body/FML32/TR_KOD_BLEDU_2),"000",""))
      let $TR_KOD_BLEDU_3 :=fn-bea:trim(fn:replace(data($body/FML32/TR_KOD_BLEDU_3),"000",""))
      let $TR_KOD_BLEDU_4 :=fn-bea:trim(fn:replace(data($body/FML32/TR_KOD_BLEDU_4),"000",""))
      let $TR_KOD_BLEDU_5 :=fn-bea:trim(fn:replace(data($body/FML32/TR_KOD_BLEDU_5),"000",""))

   
      let $errorDescriptionSaveTransfera := fn:replace(fn:concat("[",$TR_KOD_BLEDU,"-",$TR_OPIS_BLEDU,"] [",$TR_KOD_BLEDU_1,"-", $TR_OPIS_BLEDU_1,"] [",$TR_KOD_BLEDU_2,"-", $TR_OPIS_BLEDU_2,"] [",$TR_KOD_BLEDU_3,"-",$TR_OPIS_BLEDU_3,"] [",$TR_KOD_BLEDU_4,"-",$TR_OPIS_BLEDU_4,"] [",$TR_KOD_BLEDU_5,"-",$TR_OPIS_BLEDU_5,"]"),"\[-\]","")
      let $errorDescriptionSaveTransfer := translate($errorDescriptionSaveTransfera, "za§˘’Ťg‘žl†ja¦¤ZAˇŁś•G?LŹJAĄ", "zażółćgęśląjaźńZAŻÓŁĆGĘLĄJAŃ")


		(: GSTR	END
		:)                


		(: 
		 : identyfikator komunikatu z nagłówka
		 :)
		let $messageID := data($headerCache/urn3:header/urn3:msgHeader/urn3:msgId)
		return
			if (data($fault/ctx:errorCode) = "BEA-380000") then
				(:
				 : timeout
				 :)
				if($reason = "13") then
					local:fault("err:TimeoutException",
								element err:TimeoutException {
									local:errors($reason, $urcode, concat($timeoutDesc, concat(' (', concat($messageID, ')'))), "")
								})
				(:
				 : błąd usługi
				 :)		
				else if($reason = "11") then
					(:
					 : interfejs DC-ICBS jest wyłączony
					 :)
					if($urcode = "100") then
	                	local:fault("err:SystemException",
									element err:SystemException {
										local:errors($reason, $urcode, concat($dcInterfaceDownDesc, concat(' (', concat($messageID, ')'))), "")
									})
									
					(:
					 : błąd bufora FML
					 :)  
					else if($urcode = "101") then
                        local:fault("err:SystemException",
									element err:SystemException {
										local:errors($reason, $urcode, concat($fmlBufferDesc, concat(' (', concat($messageID, ')'))), "")
									})

					(:
					 : nieprawidłowe dane wejściowe
					 :)
					else if($urcode = "102") then
                        local:fault("err:ServiceFailException",
									element err:ServiceFailException {
										local:errors($reason, $urcode, concat($wrongInputDesc, concat(' (', concat($messageID, ')'))), "")
									})


					(:
					 : transakcja odrzucona przez interfejs DC-ICBS
					 :)
					else if ($urcode = "10") then
						local:fault("err:ServiceFailException",
									element err:ServiceFailException {
										local:errors($reason, $urcode, concat($transactionRejectedDesc, ' (', $messageID, ', ', $errorDescription,')'), "")
									})
					(:
					 : transakcja zakończona błędem po stronie DC-ICBS
					 :)
					else if ($urcode = "11") then
						local:fault("err:ServiceFailException",
									element err:ServiceFailException {
										local:errors($reason, $urcode, concat($transactionFailed, ' (', $messageID, ', ', $errorDescription, ')'), "")
									})

(:
					 : transakcja zakończona błędem po stronie DC-ICBS
					 :)
					else if ($urcode = "112") then
						local:fault("err:TimeoutException",
								element err:TimeoutException {
									local:errors($reason, $urcode, concat($timeoutDescSPOUTQ, concat(' (', concat($messageID, ')'))), "")
								})

					(:
					 : timeout odczytu odpowiedzi interfejsu DC-ICBS
					 :)
					else if ($urcode = "99") then
						local:fault("err:ServiceFailException",
									element err:ServiceFailException {
										local:errors($reason, $urcode, concat($transactionTimeout, ' (', $messageID, ', ', $errorDescription, ')'), "")
									})
									
					else if (string-length($errorDescription) &gt; 0 or string-length($errorCode) &gt; 0) then
						local:fault("err:ServiceFailException",
									element err:ServiceFailException {
										local:errors($reason, $urcode, concat($errorDescription, ' (', $messageID, ')'), $errorCode)
									})
											
					else if (string-length($errorDescriptionDC) &gt; 0) then
						local:fault("err:ServiceFailException",
									element err:ServiceFailException {
										local:errors($reason, $urcode, concat($errorDescriptionDC, concat(' (',concat($messageID, ')'))),"")
									})
					else
						local:fault("err:ServiceFailException",
									element err:ServiceFailException {
										local:errors($reason, $urcode, concat($defaultUrcodeDesc, concat(' (', concat($messageID, ')'))), "")
									})
									
				else
					local:fault("err:ServiceException",
								element err:ServiceException {
									local:errors($reason, $urcode, concat($defaultDesc, concat(' (', concat($messageID, ')'))) , "")
								})
			else if (data($fault/ctx:errorCode) = "BEA-382502") then
				let $reason := $fault/ctx:reason
				return
					local:fault("err:ServiceFailException",
								element err:ServiceFailException {
									local:errors("0", "0", $reason, "")
							})			
			else if (data($fault/ctx:errorCode) = "BEA-382510") then (: Blad mapowania :)
				let $reason := $fault/ctx:reason
				return
					local:fault("err:ServiceFailException",
								element err:ServiceFailException {
									local:errors("0", "0", $reason, "")
							})
			else if (data($fault/ctx:errorCode) = "BEA-382515") then (: FmlSigner :)
				let $reason := $fault/ctx:reason
				return
					local:fault("err:ServiceFailException",
								element err:ServiceFailException {
									local:errors("0", "0", $reason, "")
							})
			else if (data($fault/ctx:errorCode) = "997") then (: NFE Unexpected Error :)
				local:fault("err:ServiceFailException",
							element err:ServiceFailException {
								local:errors("0", $errorCode, $errorDescriptiona, "")
							})
			else if (data($fault/ctx:errorCode) = "1000") then
				local:fault("err:ServiceFailException",
							element err:ServiceFailException {
								local:errors("0", $errorCode, $errorDescriptiona, "")
							})
			else if (data($fault/ctx:errorCode) = "SVTR") then
				local:fault("err:ServiceFailException",
							element err:ServiceFailException {
								local:errors("5001", "0",  $errorDescriptionSaveTransfer,concat($bErrorSaveTransfer,concat(' (', concat($messageID, ')'))) )
							})
			else if (data($fault/ctx:errorCode) = "WrongInput") then
				local:fault("err:ServiceFailException",
							element err:ServiceFailException {
								local:errors("11", "102", concat($wrongInputDesc, concat(' (', concat($messageID, ')'))), "")
							})
			else if (data($fault/ctx:errorCode) = "TIMEOUT") then
				local:fault("err:TimeoutException",
							element err:TimeoutException {
								local:errors("11", "112", concat($timeoutDescSPOUTQ, concat(' (', concat($messageID, ')'))), "")
							})
		else         
				local:fault("err:SystemException",
							element err:SystemException {
								local:errors("0","0",concat($alsbErrorDesc, concat(' (', concat($messageID, ')'))) , "")
							})
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>