<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;

declare function xf:mappcCustNewResponse($fml as element(fml:FML32))
	as element(m:pcCustNewResponse)
{
	&lt;m:pcCustNewResponse&gt;
		&lt;m:NumerKlienta?&gt;{
			data($fml/fml:DC_NUMER_KLIENTA)
		}&lt;/m:NumerKlienta&gt;

		{
			for $i in 1 to count($fml/fml:CI_NR_KOM)
			return
				&lt;m:Error&gt;
					&lt;m:NrKom&gt;{
						data($fml/fml:CI_NR_KOM[$i])
					}&lt;/m:NrKom&gt;

					&lt;m:NazwaPola&gt;{
						data($fml/fml:CI_NAZWA_POLA[$i])
					}&lt;/m:NazwaPola&gt;

					&lt;m:OpisBledu&gt;{
						data($fml/fml:DC_OPIS_BLEDU[$i])
					}&lt;/m:OpisBledu&gt;
				&lt;/m:Error&gt;
		}
	&lt;/m:pcCustNewResponse&gt;
};

&lt;soap-env:Body&gt;{
	xf:mappcCustNewResponse($body/fml:FML32)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>