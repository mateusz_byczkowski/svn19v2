<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeCustNewResponse($fml as element(fml:FML32))
	as element(m:eCustNewResponse) {
		&lt;m:eCustNewResponse&gt;
			{
				if($fml/fml:B_SYS_MASK)
					then &lt;m:SysMask&gt;{ data($fml/fml:B_SYS_MASK) }&lt;/m:SysMask&gt;
					else ()
			}
			{
				if($fml/fml:B_NR_DOK)
					then &lt;m:NrDok&gt;{ data($fml/fml:B_NR_DOK) }&lt;/m:NrDok&gt;
					else ()
			}
			{
				if($fml/fml:B_RODZ_DOK)
					then &lt;m:RodzDok&gt;{ data($fml/fml:B_RODZ_DOK) }&lt;/m:RodzDok&gt;
					else ()
			}
			{
				if($fml/fml:B_ID_ODDZ)
					then &lt;m:IdOddz&gt;{ data($fml/fml:B_ID_ODDZ) }&lt;/m:IdOddz&gt;
					else ()
			}
			{
				if($fml/fml:E_LIMIT)
					then &lt;m:Limit&gt;{ data($fml/fml:E_LIMIT) }&lt;/m:Limit&gt;
					else ()
			}
			{
				if($fml/fml:E_AMOUNT_REMAINING)
					then &lt;m:AmountRemaining&gt;{ data($fml/fml:E_AMOUNT_REMAINING) }&lt;/m:AmountRemaining&gt;
					else ()
			}
			{
				if($fml/fml:E_CYCLE)
					then &lt;m:Cycle&gt;{ data($fml/fml:E_CYCLE) }&lt;/m:Cycle&gt;
					else ()
			}
			{
				if($fml/fml:E_CYCLE_BEGIN)
					then &lt;m:CycleBegin&gt;{ data($fml/fml:E_CYCLE_BEGIN) }&lt;/m:CycleBegin&gt;
					else ()
			}
			{
				if($fml/fml:E_EX_CYCLE)
					then &lt;m:ExCycle&gt;{ data($fml/fml:E_EX_CYCLE) }&lt;/m:ExCycle&gt;
					else ()
			}
			{
				if($fml/fml:E_EX_CYCLE_BEGIN)
					then &lt;m:ExCycleBegin&gt;{ data($fml/fml:E_EX_CYCLE_BEGIN) }&lt;/m:ExCycleBegin&gt;
					else ()
			}
			{
				if($fml/fml:E_EX_LIMIT)
					then &lt;m:ExLimit&gt;{ data($fml/fml:E_EX_LIMIT) }&lt;/m:ExLimit&gt;
					else ()
			}
			{
				if($fml/fml:E_TRN_LIMIT)
					then &lt;m:TrnLimit&gt;{ data($fml/fml:E_TRN_LIMIT) }&lt;/m:TrnLimit&gt;
					else ()
			}
			{
				if($fml/fml:E_EX_AMOUNT_REMAINING)
					then &lt;m:ExAmountRemaining&gt;{ data($fml/fml:E_EX_AMOUNT_REMAINING) }&lt;/m:ExAmountRemaining&gt;
					else ()
			}
			{
				if($fml/fml:E_HI_AMOUNT_REMAINING)
					then &lt;m:HiAmountRemaining&gt;{ data($fml/fml:E_HI_AMOUNT_REMAINING) }&lt;/m:HiAmountRemaining&gt;
					else ()
			}
			{
				if($fml/fml:E_HI_CYCLE)
					then &lt;m:HiCycle&gt;{ data($fml/fml:E_HI_CYCLE) }&lt;/m:HiCycle&gt;
					else ()
			}
			{
				if($fml/fml:E_HI_CYCLE_BEGIN)
					then &lt;m:HiCycleBegin&gt;{ data($fml/fml:E_HI_CYCLE_BEGIN) }&lt;/m:HiCycleBegin&gt;
					else ()
			}
			{
				if($fml/fml:E_HI_LIMIT)
					then &lt;m:HiLimit&gt;{ data($fml/fml:E_HI_LIMIT) }&lt;/m:HiLimit&gt;
					else ()
			}
			{
				if($fml/fml:E_REP_NAME)
					then &lt;m:RepName&gt;{ data($fml/fml:E_REP_NAME) }&lt;/m:RepName&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_SMSNO)
					then &lt;m:CustSmsno&gt;{ data($fml/fml:E_CUST_SMSNO) }&lt;/m:CustSmsno&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_NAME)
					then &lt;m:CustName&gt;{ data($fml/fml:E_CUST_NAME) }&lt;/m:CustName&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_CITY)
					then &lt;m:CustCity&gt;{ data($fml/fml:E_CUST_CITY) }&lt;/m:CustCity&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_ZIPCODE)
					then &lt;m:CustZipcode&gt;{ data($fml/fml:E_CUST_ZIPCODE) }&lt;/m:CustZipcode&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_STREET)
					then &lt;m:CustStreet&gt;{ data($fml/fml:E_CUST_STREET) }&lt;/m:CustStreet&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_TELNO)
					then &lt;m:CustTelno&gt;{ data($fml/fml:E_CUST_TELNO) }&lt;/m:CustTelno&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_FAXNO)
					then &lt;m:CustFaxno&gt;{ data($fml/fml:E_CUST_FAXNO) }&lt;/m:CustFaxno&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_GSMNO)
					then &lt;m:CustGsmno&gt;{ data($fml/fml:E_CUST_GSMNO) }&lt;/m:CustGsmno&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_EMAIL)
					then &lt;m:CustEmail&gt;{ data($fml/fml:E_CUST_EMAIL) }&lt;/m:CustEmail&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_STATUS)
					then &lt;m:CustStatus&gt;{ data($fml/fml:E_CUST_STATUS) }&lt;/m:CustStatus&gt;
					else ()
			}
			{
				if($fml/fml:B_D_URODZENIA)
					then &lt;m:DUrodzenia&gt;{ data($fml/fml:B_D_URODZENIA) }&lt;/m:DUrodzenia&gt;
					else ()
			}
			{
				if($fml/fml:B_M_URODZENIA)
					then &lt;m:MUrodzenia&gt;{ data($fml/fml:B_M_URODZENIA) }&lt;/m:MUrodzenia&gt;
					else ()
			}
			{
				if($fml/fml:E_OLD_CUSTOMER_ID)
					then &lt;m:OldCustomerId&gt;{ data($fml/fml:E_OLD_CUSTOMER_ID) }&lt;/m:OldCustomerId&gt;
					else ()
			}
			{
				if($fml/fml:E_OLD_ID_TYPE)
					then &lt;m:OldIdType&gt;{ data($fml/fml:E_OLD_ID_TYPE) }&lt;/m:OldIdType&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_IDENTIF)
					then &lt;m:CustIdentif&gt;{ data($fml/fml:E_CUST_IDENTIF) }&lt;/m:CustIdentif&gt;
					else ()
			}
			{
				if($fml/fml:E_ADMIN_USER_INFO)
					then &lt;m:AdminUserInfo&gt;{ data($fml/fml:E_ADMIN_USER_INFO) }&lt;/m:AdminUserInfo&gt;
					else ()
			}
			{
				if($fml/fml:E_MICRO_BRANCH)
					then &lt;m:MicroBranch&gt;{ data($fml/fml:E_MICRO_BRANCH) }&lt;/m:MicroBranch&gt;
					else ()
			}
			{
				if($fml/fml:E_CIF_NUMBER)
					then &lt;m:CifNumber&gt;{ data($fml/fml:E_CIF_NUMBER) }&lt;/m:CifNumber&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_OPTIONS)
					then &lt;m:CustOptions&gt;{ data($fml/fml:E_CUST_OPTIONS) }&lt;/m:CustOptions&gt;
					else ()
			}
			{
				if($fml/fml:E_LAST_ACTIVATED)
					then &lt;m:LastActivated&gt;{ data($fml/fml:E_LAST_ACTIVATED) }&lt;/m:LastActivated&gt;
					else ()
			}
			{
				if($fml/fml:E_LAST_DEACTIVATED)
					then &lt;m:LastDeactivated&gt;{ data($fml/fml:E_LAST_DEACTIVATED) }&lt;/m:LastDeactivated&gt;
					else ()
			}
			{
				if($fml/fml:E_TOKEN_SERIAL_NO)
					then &lt;m:TokenSerialNo&gt;{ data($fml/fml:E_TOKEN_SERIAL_NO) }&lt;/m:TokenSerialNo&gt;
					else ()
			}
			{
				if($fml/fml:E_TOKEN_CUSTOMER_ID)
					then &lt;m:TokenCustomerId&gt;{ data($fml/fml:E_TOKEN_CUSTOMER_ID) }&lt;/m:TokenCustomerId&gt;
					else ()
			}
			{
				if($fml/fml:E_TOKEN_ID_TYPE)
					then &lt;m:TokenIdType&gt;{ data($fml/fml:E_TOKEN_ID_TYPE) }&lt;/m:TokenIdType&gt;
					else ()
			}
			{
				if($fml/fml:E_TOKEN_LOGIN_ID)
					then &lt;m:TokenLoginId&gt;{ data($fml/fml:E_TOKEN_LOGIN_ID) }&lt;/m:TokenLoginId&gt;
					else ()
			}
			{
				if($fml/fml:E_ALLOWED_SECURITY_LEVEL)
					then &lt;m:AllowedSecurityLevel&gt;{ data($fml/fml:E_ALLOWED_SECURITY_LEVEL) }&lt;/m:AllowedSecurityLevel&gt;
					else ()
			}
			{
				if($fml/fml:E_PROFILE_ID)
					then &lt;m:ProfileId&gt;{ data($fml/fml:E_PROFILE_ID) }&lt;/m:ProfileId&gt;
					else ()
			}
			{
				if($fml/fml:E_PROFILE_TRIAL)
					then &lt;m:ProfileTrial&gt;{ data($fml/fml:E_PROFILE_TRIAL) }&lt;/m:ProfileTrial&gt;
					else ()
			}
			{
				if($fml/fml:E_PROFILE_EXPIRY)
					then &lt;m:ProfileExpiry&gt;{ data($fml/fml:E_PROFILE_EXPIRY) }&lt;/m:ProfileExpiry&gt;
					else ()
			}
			{
				if($fml/fml:E_CUSTOMER_TYPE)
					then &lt;m:CustomerType&gt;{ data($fml/fml:E_CUSTOMER_TYPE) }&lt;/m:CustomerType&gt;
					else ()
			}
			{
				if($fml/fml:E_CIF_OPTIONS)
					then &lt;m:CifOptions&gt;{ data($fml/fml:E_CIF_OPTIONS) }&lt;/m:CifOptions&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_REBATE)
					then &lt;m:CustRebate&gt;{ data($fml/fml:E_CUST_REBATE) }&lt;/m:CustRebate&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_REPORT_DATE)
					then &lt;m:CustReportDate&gt;{ data($fml/fml:E_CUST_REPORT_DATE) }&lt;/m:CustReportDate&gt;
					else ()
			}
			{
				if($fml/fml:E_CUST_REPORT_VERSION)
					then &lt;m:CustReportVersion&gt;{ data($fml/fml:E_CUST_REPORT_VERSION) }&lt;/m:CustReportVersion&gt;
					else ()
			}
			{

				let $E_LOGIN_ID := $fml/fml:E_LOGIN_ID
				let $E_TIME_STAMP := $fml/fml:E_TIME_STAMP
				for $it at $p in $fml/fml:E_TIME_STAMP
				return
					&lt;m:eCustNewCustomer&gt;
					{
						if($E_LOGIN_ID[$p])
							then &lt;m:LoginId&gt;{ data($E_LOGIN_ID[$p]) }&lt;/m:LoginId&gt;
						else ()
					}
					{
						if($E_TIME_STAMP[$p])
							then &lt;m:TimeStamp&gt;{ data($E_TIME_STAMP[$p]) }&lt;/m:TimeStamp&gt;
						else ()
					}
					&lt;/m:eCustNewCustomer&gt;
			}

		&lt;/m:eCustNewResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapeCustNewResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>