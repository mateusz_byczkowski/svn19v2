<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ceke/messages/";
declare namespace xf = "http://bzwbk.com/services/ceke/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeCustNewRequest($req as element(m:eCustNewRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:AdminMicroBranch)
					then &lt;fml:E_ADMIN_MICRO_BRANCH&gt;{ data($req/m:AdminMicroBranch) }&lt;/fml:E_ADMIN_MICRO_BRANCH&gt;
					else ()
			}
			{
				if($req/m:LoginId)
					then &lt;fml:E_LOGIN_ID&gt;{ data($req/m:LoginId) }&lt;/fml:E_LOGIN_ID&gt;
					else ()
			}
			{
				if($req/m:Limit)
					then &lt;fml:E_LIMIT&gt;{ data($req/m:Limit) }&lt;/fml:E_LIMIT&gt;
					else ()
			}
			{
				if($req/m:HiLimit)
					then &lt;fml:E_HI_LIMIT&gt;{ data($req/m:HiLimit) }&lt;/fml:E_HI_LIMIT&gt;
					else ()
			}
			{
				if($req/m:TrnLimit)
					then &lt;fml:E_TRN_LIMIT&gt;{ data($req/m:TrnLimit) }&lt;/fml:E_TRN_LIMIT&gt;
					else ()
			}
			{
				if($req/m:NrDok)
					then &lt;fml:B_NR_DOK&gt;{ data($req/m:NrDok) }&lt;/fml:B_NR_DOK&gt;
					else ()
			}
			{
				if($req/m:RodzDok)
					then &lt;fml:B_RODZ_DOK&gt;{ data($req/m:RodzDok) }&lt;/fml:B_RODZ_DOK&gt;
					else ()
			}
			{
				if($req/m:HiCycle)
					then &lt;fml:E_HI_CYCLE&gt;{ data($req/m:HiCycle) }&lt;/fml:E_HI_CYCLE&gt;
					else ()
			}
			{
				if($req/m:CustSmsno)
					then &lt;fml:E_CUST_SMSNO&gt;{ data($req/m:CustSmsno) }&lt;/fml:E_CUST_SMSNO&gt;
					else ()
			}
			{
				if($req/m:ExCycle)
					then &lt;fml:E_EX_CYCLE&gt;{ data($req/m:ExCycle) }&lt;/fml:E_EX_CYCLE&gt;
					else ()
			}
			{
				if($req/m:Cycle)
					then &lt;fml:E_CYCLE&gt;{ data($req/m:Cycle) }&lt;/fml:E_CYCLE&gt;
					else ()
			}
			{
				if($req/m:CustName)
					then &lt;fml:E_CUST_NAME&gt;{ data($req/m:CustName) }&lt;/fml:E_CUST_NAME&gt;
					else ()
			}
			{
				if($req/m:CustCity)
					then &lt;fml:E_CUST_CITY&gt;{ data($req/m:CustCity) }&lt;/fml:E_CUST_CITY&gt;
					else ()
			}
			{
				if($req/m:CustZipcode)
					then &lt;fml:E_CUST_ZIPCODE&gt;{ data($req/m:CustZipcode) }&lt;/fml:E_CUST_ZIPCODE&gt;
					else ()
			}
			{
				if($req/m:CustTelno)
					then &lt;fml:E_CUST_TELNO&gt;{ data($req/m:CustTelno) }&lt;/fml:E_CUST_TELNO&gt;
					else ()
			}
			{
				if($req/m:CustFaxno)
					then &lt;fml:E_CUST_FAXNO&gt;{ data($req/m:CustFaxno) }&lt;/fml:E_CUST_FAXNO&gt;
					else ()
			}
			{
				if($req/m:CustGsmno)
					then &lt;fml:E_CUST_GSMNO&gt;{ data($req/m:CustGsmno) }&lt;/fml:E_CUST_GSMNO&gt;
					else ()
			}
			{
				if($req/m:CustEmail)
					then &lt;fml:E_CUST_EMAIL&gt;{ data($req/m:CustEmail) }&lt;/fml:E_CUST_EMAIL&gt;
					else ()
			}
			{
				if($req/m:CustStatus)
					then &lt;fml:E_CUST_STATUS&gt;{ data($req/m:CustStatus) }&lt;/fml:E_CUST_STATUS&gt;
					else ()
			}
			{
				if($req/m:CustIdentif)
					then &lt;fml:E_CUST_IDENTIF&gt;{ data($req/m:CustIdentif) }&lt;/fml:E_CUST_IDENTIF&gt;
					else ()
			}
			{
				if($req/m:CustOptions)
					then &lt;fml:E_CUST_OPTIONS&gt;{ data($req/m:CustOptions) }&lt;/fml:E_CUST_OPTIONS&gt;
					else ()
			}
			{
				if($req/m:RepName)
					then &lt;fml:E_REP_NAME&gt;{ data($req/m:RepName) }&lt;/fml:E_REP_NAME&gt;
					else ()
			}
			{
				if($req/m:ExLimit)
					then &lt;fml:E_EX_LIMIT&gt;{ data($req/m:ExLimit) }&lt;/fml:E_EX_LIMIT&gt;
					else ()
			}
			{
				if($req/m:TrnExLimit)
					then &lt;fml:E_TRN_EX_LIMIT&gt;{ data($req/m:TrnExLimit) }&lt;/fml:E_TRN_EX_LIMIT&gt;
					else ()
			}
			{
				if($req/m:Sys)
					then &lt;fml:B_SYS&gt;{ data($req/m:Sys) }&lt;/fml:B_SYS&gt;
					else ()
			}
			{
				if($req/m:Bank)
					then &lt;fml:B_BANK&gt;{ data($req/m:Bank) }&lt;/fml:B_BANK&gt;
					else ()
			}
			{
				if($req/m:TokenCustomerId)
					then &lt;fml:E_TOKEN_CUSTOMER_ID&gt;{ data($req/m:TokenCustomerId) }&lt;/fml:E_TOKEN_CUSTOMER_ID&gt;
					else ()
			}
			{
				if($req/m:TokenIdType)
					then &lt;fml:E_TOKEN_ID_TYPE&gt;{ data($req/m:TokenIdType) }&lt;/fml:E_TOKEN_ID_TYPE&gt;
					else ()
			}
			{
				if($req/m:TokenLoginId)
					then &lt;fml:E_TOKEN_LOGIN_ID&gt;{ data($req/m:TokenLoginId) }&lt;/fml:E_TOKEN_LOGIN_ID&gt;
					else ()
			}
			{
				if($req/m:CifOptions)
					then &lt;fml:E_CIF_OPTIONS&gt;{ data($req/m:CifOptions) }&lt;/fml:E_CIF_OPTIONS&gt;
					else ()
			}
			{
				if($req/m:AllowedSecurityLevel)
					then &lt;fml:E_ALLOWED_SECURITY_LEVEL&gt;{ data($req/m:AllowedSecurityLevel) }&lt;/fml:E_ALLOWED_SECURITY_LEVEL&gt;
					else ()
			}
			{
				if($req/m:DefaultSecurityLevel)
					then &lt;fml:E_DEFAULT_SECURITY_LEVEL&gt;{ data($req/m:DefaultSecurityLevel) }&lt;/fml:E_DEFAULT_SECURITY_LEVEL&gt;
					else ()
			}
			{
				if($req/m:ProfileId)
					then &lt;fml:E_PROFILE_ID&gt;{ data($req/m:ProfileId) }&lt;/fml:E_PROFILE_ID&gt;
					else ()
			}
			{
				if($req/m:CustomerType)
					then &lt;fml:E_CUSTOMER_TYPE&gt;{ data($req/m:CustomerType) }&lt;/fml:E_CUSTOMER_TYPE&gt;
					else ()
			}
			{
				if($req/m:ProfileTrial)
					then &lt;fml:E_PROFILE_TRIAL&gt;{ data($req/m:ProfileTrial) }&lt;/fml:E_PROFILE_TRIAL&gt;
					else ()
			}
			{
				if($req/m:ProfileExpiry)
					then &lt;fml:E_PROFILE_EXPIRY&gt;{ data($req/m:ProfileExpiry) }&lt;/fml:E_PROFILE_EXPIRY&gt;
					else ()
			}
			{
				if($req/m:CustReportVersion)
					then &lt;fml:E_CUST_REPORT_VERSION&gt;{ data($req/m:CustReportVersion) }&lt;/fml:E_CUST_REPORT_VERSION&gt;
					else ()
			}
			{
				if($req/m:CustReportDate)
					then &lt;fml:E_CUST_REPORT_DATE&gt;{ data($req/m:CustReportDate) }&lt;/fml:E_CUST_REPORT_DATE&gt;
					else ()
			}
			{
				if($req/m:CustRebate)
					then &lt;fml:E_CUST_REBATE&gt;{ data($req/m:CustRebate) }&lt;/fml:E_CUST_REBATE&gt;
					else ()
			}
			{
				if($req/m:SysMask)
					then &lt;fml:B_SYS_MASK&gt;{ data($req/m:SysMask) }&lt;/fml:B_SYS_MASK&gt;
					else ()
			}
			{
				for $x in $req/m:ChannelType
				return &lt;fml:E_CHANNEL_TYPE&gt;{ data($x) }&lt;/fml:E_CHANNEL_TYPE&gt;
			}
			{
			        for $x in $req/m:Status
				return &lt;fml:E_STATUS&gt;{ data($x) }&lt;/fml:E_STATUS&gt;
			}
			{
				for $x in $req/m:Tries
				return &lt;fml:E_TRIES&gt;{ data($x) }&lt;/fml:E_TRIES&gt;
			}
			{
				for $x in $req/m:TriesAllowed
				return &lt;fml:E_TRIES_ALLOWED&gt;{ data($x) }&lt;/fml:E_TRIES_ALLOWED&gt;
			}
			{
				for $x in $req/m:Expiry
				return &lt;fml:E_EXPIRY&gt;{ data($x) }&lt;/fml:E_EXPIRY&gt;
			}
			{
				for $x in $req/m:ChangePeriod
				return &lt;fml:E_CHANGE_PERIOD&gt;{ data($x) }&lt;/fml:E_CHANGE_PERIOD&gt;
			}
			{
				for $x in $req/m:TrnMask
				return &lt;fml:E_TRN_MASK&gt;{ data($x) }&lt;/fml:E_TRN_MASK&gt;
			}
			{
				for $x in $req/m:Pdata
				return &lt;fml:E_PDATA&gt;{ data($x) }&lt;/fml:E_PDATA&gt;
			}
			{
				for $x in $req/m:ChannelCycle
				return &lt;fml:E_CHANNEL_CYCLE&gt;{ data($x) }&lt;/fml:E_CHANNEL_CYCLE&gt;
			}
			{
				for $x in $req/m:ChannelLimit
				return &lt;fml:E_CHANNEL_LIMIT&gt;{ data($x) }&lt;/fml:E_CHANNEL_LIMIT&gt;
			}
			{
				for $x in $req/m:ChannelCycleBegin
				return &lt;fml:E_CHANNEL_CYCLE_BEGIN&gt;{ data($x) }&lt;/fml:E_CHANNEL_CYCLE_BEGIN&gt;
			}
			{
				if($req/m:SeqNo)
					then &lt;fml:E_SEQ_NO&gt;{ data($req/m:SeqNo) }&lt;/fml:E_SEQ_NO&gt;
					else ()
			}
			{
				if($req/m:KodRach)
					then &lt;fml:B_KOD_RACH&gt;{ data($req/m:KodRach) }&lt;/fml:B_KOD_RACH&gt;
					else ()
			}
			{
				if($req/m:Waluta)
					then &lt;fml:B_WALUTA&gt;{ data($req/m:Waluta) }&lt;/fml:B_WALUTA&gt;
					else ()
			}
			{
				if($req/m:DlNrRach)
					then &lt;fml:B_DL_NR_RACH&gt;{ data($req/m:DlNrRach) }&lt;/fml:B_DL_NR_RACH&gt;
					else ()
			}
			{
				if($req/m:TypRach)
					then &lt;fml:B_TYP_RACH&gt;{ data($req/m:TypRach) }&lt;/fml:B_TYP_RACH&gt;
					else ()
			}
			{
				if($req/m:Options)
					then &lt;fml:E_OPTIONS&gt;{ data($req/m:Options) }&lt;/fml:E_OPTIONS&gt;
					else ()
			}
			{
				if($req/m:IdOddz)
					then &lt;fml:B_ID_ODDZ&gt;{ data($req/m:IdOddz) }&lt;/fml:B_ID_ODDZ&gt;
					else ()
			}
			{
				if($req/m:CustStreet)
					then &lt;fml:E_CUST_STREET&gt;{ data($req/m:CustStreet) }&lt;/fml:E_CUST_STREET&gt;
					else ()
			}
			{
				if($req/m:CifNumber)
					then &lt;fml:E_CIF_NUMBER&gt;{ data($req/m:CifNumber) }&lt;/fml:E_CIF_NUMBER&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapeCustNewRequest($body/m:eCustNewRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>