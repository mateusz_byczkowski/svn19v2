<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="urn:errors.hlbsentities.be.dcl";
declare namespace urn3="urn:be.services.dcl";

declare variable $fault external;
declare variable $header external;
declare variable $body external;
 
declare function local:fault($faultString as xs:string, $detail as xs:anyType) as element(soap-env:Fault) {
  &lt;soap-env:Fault&gt;
   &lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
   &lt;faultstring&gt;{ $faultString }&lt;/faultstring&gt; 
   &lt;detail&gt;{ $detail }&lt;/detail&gt;
  &lt;/soap-env:Fault&gt;
};
 
declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string, $errorDescription as xs:string) as element()* {
 &lt;f:exceptionItem&gt;
 &lt;f:errorCode1&gt;{ $errorCode1 }&lt;/f:errorCode1&gt;
 &lt;f:errorCode2&gt;{ $errorCode2 }&lt;/f:errorCode2&gt;
 &lt;f:errorDescription&gt;{ concat($errorDescription, concat(' (',concat(data($header/urn3:header/urn3:msgHeader/urn3:msgId), ')'))) }&lt;/f:errorDescription&gt;
 &lt;/f:exceptionItem&gt;
};
 
&lt;soap-env:Body&gt;
{
 let $reason := fn:substring-after(fn:substring-before($fault/ctx:reason, "):"), "(")
 let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($fault/ctx:reason, ":"), ":"), ":")
 return
    if($reason = "13") then
      local:fault("errors.hlbsentities.be.dcl.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode, "Timeout") })
    else if($reason = "11") then
      if($urcode = "100") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode, "Interfejs DC nieaktywny, przetwarzanie niemożliwe") })
      else if($urcode = "101") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode, "Błąd zapisu do bufora FML") })
      else if($urcode = "102") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors($reason, $urcode, "Błędny bufor wejściowy") )
      else if($urcode = "103") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors($reason, $urcode, "Brak danych") )
      else if($urcode = "10") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors($reason, $urcode, "Błąd w czasie przetwarzania przez interfejs DC (operacja niezaksięgowana)") )
      else if($urcode = "11") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors($reason, $urcode, "Powtórzona transakcja o podanym ID") )
      else if($urcode = "12") then
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors($reason, $urcode, "Błąd w czasie przetwarzania przez CRS (operacja niezaksięgowana lub zaksięgowana częściowo)") )
      else if($urcode = "1") then
        local:fault("errors.hlbsentities.be.dcl.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode, "Timeout") })
      else
        local:fault("errors.hlbsentities.be.dcl.ServiceFailException", local:errors($reason, $urcode, "Błąd usługi") )
    else
      local:fault("errors.hlbsentities.be.dcl.ServiceException", element f:ServiceException { local:errors($reason, $urcode, "Service Exception") })
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>