<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMDelPortfRequest($req as element(m:CRMDelPortfRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:SkpPracownikaRej)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ&gt;{ data($req/m:SkpPracownikaRej) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ&gt;
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA&gt;{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMDelPortfRequest($body/m:CRMDelPortfRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>