<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMNewPortfolioResponse($fml as element(fml:FML32))
	as element(m:CRMNewPortfolioResponse) {
		&lt;m:CRMNewPortfolioResponse&gt;
			{
				if($fml/fml:CI_ID_PORTFELA)
					then &lt;m:IdPortfela&gt;{ data($fml/fml:CI_ID_PORTFELA) }&lt;/m:IdPortfela&gt;
					else ()
			}
		&lt;/m:CRMNewPortfolioResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMNewPortfolioResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>