<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetContactsRequest($req as element(m:CRMGetContactsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI&gt;{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:NumerKlientaZewn)
					then &lt;fml:CI_NUMER_KLIENTA&gt;{ data($req/m:NumerKlientaZewn) }&lt;/fml:CI_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:DataOd)
					then &lt;fml:CI_DATA_OD&gt;{ data($req/m:DataOd) }&lt;/fml:CI_DATA_OD&gt;
					else ()
			}
			{
				if($req/m:DataDo)
					then &lt;fml:CI_DATA_DO&gt;{ data($req/m:DataDo) }&lt;/fml:CI_DATA_DO&gt;
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				if($req/m:KontRelacji)
					then &lt;fml:CI_KONT_RELACJI&gt;{ data($req/m:KontRelacji) }&lt;/fml:CI_KONT_RELACJI&gt;
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI&gt;{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI&gt;
					else ()
			}
			{
				if($req/m:Sortowanie)
					then &lt;fml:CI_SORTOWANIE&gt;{ data($req/m:Sortowanie) }&lt;/fml:CI_SORTOWANIE&gt;
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA&gt;{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA&gt;
					else ()
			}
			{
				if($req/m:RodzajKont)
					then &lt;fml:CI_RODZAJ_KONT&gt;{ data($req/m:RodzajKont) }&lt;/fml:CI_RODZAJ_KONT&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetContactsRequest($body/m:CRMGetContactsRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>