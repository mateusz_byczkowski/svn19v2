<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCusLstCntResponse($fml as element(fml:FML32))
	as element(m:CRMGetCusLstCntResponse) {
		&lt;m:CRMGetCusLstCntResponse&gt;
			{
				let $CI_LISTA_KONTAKTOW := $fml/fml:CI_LISTA_KONTAKTOW return
					if($CI_LISTA_KONTAKTOW)
						then &lt;m:ListaKontaktow&gt;{ data($CI_LISTA_KONTAKTOW) }&lt;/m:ListaKontaktow&gt;
						else ()
			}
		&lt;/m:CRMGetCusLstCntResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCusLstCntResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>