<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:schema><![CDATA[<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" targetNamespace="http://bzwbk.com/services/cerber"
	xmlns:tns="http://bzwbk.com/services/cerber">

	<xsd:complexType name="User">
		<xsd:complexContent>
			<xsd:extension base="tns:UserBase">
				<xsd:sequence>
					<xsd:element name="Attributes" type="tns:Attribute" minOccurs="0" maxOccurs="unbounded" />
					<xsd:element name="PositionAttributes" type="tns:Attribute" minOccurs="0" maxOccurs="unbounded" />
					<xsd:element name="Permissions" type="xsd:string" minOccurs="0" maxOccurs="unbounded" />
					<xsd:element name="PositionPermissions" type="xsd:string" minOccurs="0" maxOccurs="unbounded" />
					<xsd:element name="Units" type="tns:Unit" minOccurs="0" maxOccurs="unbounded" />
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="UserBase">
		<xsd:sequence>
			<xsd:element name="Email" type="xsd:string" />
			<xsd:element name="Func" type="xsd:string" />
			<xsd:element name="Gender" type="xsd:string" />
			<xsd:element name="LastIncorrectLoginApp" type="xsd:int" minOccurs="0" />
			<xsd:element name="LastIncorrectLoginDate" type="xsd:dateTime" minOccurs="0" />
			<xsd:element name="LastLoggedInApplication" type="xsd:int" minOccurs="0" />
			<xsd:element name="LastLoggedInDate" type="xsd:dateTime" minOccurs="0" />
			<xsd:element name="Login" type="xsd:string" />
			<xsd:element name="MobileNo" type="xsd:string" />
			<xsd:element name="Name" type="xsd:string" />
			<xsd:element name="Password" type="xsd:string" minOccurs="0" />
			<xsd:element name="PhoneNo" type="xsd:string" />
			<xsd:element name="Position" type="xsd:string" minOccurs="0" />
			<xsd:element name="PositionId" type="xsd:int" minOccurs="0" />
			<xsd:element name="Skp" type="xsd:int" />
			<xsd:element name="Surname" type="xsd:string" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="UserExtended">
		<xsd:complexContent>
			<xsd:extension base="tns:User">
				<xsd:sequence>
					<xsd:element name="Groups" type="tns:Group" minOccurs="0" maxOccurs="unbounded" />
					<xsd:element name="PositionGroups" type="tns:Group" minOccurs="0" maxOccurs="unbounded" />
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="UnitBase">
		<xsd:sequence>
			<xsd:element name="AddressCity" type="xsd:string" />
			<xsd:element name="AddressNo" type="xsd:string" />
			<xsd:element name="AddressState" type="xsd:string" />
			<xsd:element name="AddressStreet" type="xsd:string" />
			<xsd:element name="AddressZip" type="xsd:string" />
			<xsd:element name="IcbsNo" type="xsd:int" />
			<xsd:element name="KirNo" type="xsd:int" />
			<xsd:element name="Name" type="xsd:string" />
			<xsd:element name="FunctionId" type="xsd:int" />
			<xsd:element name="Function" type="xsd:string" />
			<xsd:element name="ChiefId" minOccurs="0" type="xsd:int" />
			<xsd:element name="CompanyId" type="xsd:int" />
			<xsd:element name="ParentIcbsNo" minOccurs="0" type="xsd:int" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="Unit">
		<xsd:complexContent>
			<xsd:extension base="tns:UnitBase">
				<xsd:sequence>
					<xsd:element name="CashierNo" type="xsd:int" minOccurs="0" />
					<xsd:element name="TeamId" type="xsd:int" minOccurs="0" />
					<xsd:element name="IsDelegation" type="xsd:boolean" />
					<xsd:element name="Since" type="xsd:dateTime" minOccurs="0" />
					<xsd:element name="Until" type="xsd:dateTime" minOccurs="0" />
					<xsd:element name="IsMasterUnit" type="xsd:string" />
					<xsd:element name="WorkWithOwnWallet" type="xsd:boolean" />
					<xsd:element name="Attributes" type="tns:Attribute" minOccurs="0" maxOccurs="unbounded" />
					<xsd:element name="Permissions" type="xsd:string" minOccurs="0" maxOccurs="unbounded" />
					<xsd:element name="Substitutions" type="tns:Substitution" minOccurs="0" maxOccurs="unbounded" />
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="Attribute">
		<xsd:sequence>
			<xsd:element name="Name" type="xsd:string" />
			<xsd:element name="Value" type="xsd:string" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="Substitution">
		<xsd:sequence>
			<xsd:element name="Id" type="xsd:int" />
			<xsd:element name="Since" type="xsd:dateTime" />
			<xsd:element name="SubstitutedUserId" type="xsd:int" />
			<xsd:element name="SubstitutionForApplication" type="xsd:int" />
			<xsd:element name="UnitId" type="xsd:int" />
			<xsd:element name="Until" type="xsd:dateTime" />
			<xsd:element name="UserId" type="xsd:int" />
			<xsd:element name="CreatedBy" type="xsd:int" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="Delegation">
		<xsd:sequence>
			<xsd:element name="DelegatedUserId" type="xsd:int" />
			<xsd:element name="DestinationBranchId" type="xsd:int" />
			<xsd:element name="Since" type="xsd:dateTime" />
			<xsd:element name="SourceBranchId" type="xsd:int" />
			<xsd:element name="Until" type="xsd:dateTime" />
			<xsd:element name="WorkWithOwnWallet" type="xsd:boolean" />
			<xsd:element name="Id" type="xsd:int" minOccurs="0" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="Group">
		<xsd:sequence>
			<xsd:element name="Attributes" type="tns:Attribute" minOccurs="0" maxOccurs="unbounded" />
			<xsd:element name="Name" type="xsd:string" />
			<xsd:element name="Permissions" type="xsd:string" minOccurs="0" maxOccurs="unbounded" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="ApplicationAuthData">
		<xsd:sequence>
			<xsd:element name="ApplicationId" type="xsd:int" />
			<xsd:element name="ApplicationPassword" type="xsd:string" />
			<xsd:element name="Operator" type="xsd:int" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="TeamBase">
		<xsd:sequence>
			<xsd:element minOccurs="1" name="Id" type="xsd:int" />
			<xsd:element minOccurs="1" name="Description" type="xsd:string" />
			<xsd:element minOccurs="1" name="Leader" type="xsd:int" />
			<xsd:element minOccurs="1" name="Name" type="xsd:string" />
			<xsd:element minOccurs="1" name="PercentageRate" type="xsd:int" />
			<xsd:element minOccurs="1" name="UnitId" type="xsd:int" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="Team">
		<xsd:complexContent>
			<xsd:extension base="tns:TeamBase">
				<xsd:sequence>
					<xsd:element name="Members" type="tns:UserBase" minOccurs="0" maxOccurs="unbounded" />
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="TeamInfo">
		<xsd:sequence>
			<xsd:element name="LeaderFirstName" type="xsd:string" />
			<xsd:element name="LeaderLastName" type="xsd:string" />
			<xsd:element name="MembersCount" type="xsd:int" />
			<xsd:element name="Team" type="tns:Team" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="UserPermissionUnitInfo">
		<xsd:sequence>
			<xsd:element name="SKP" type="xsd:int" />
			<xsd:element name="PermissionName" type="xsd:string" />
			<xsd:element name="IcbsNo" type="xsd:int" />
		</xsd:sequence>
	</xsd:complexType>
</xsd:schema>]]></con:schema>
  <con:targetNamespace>http://bzwbk.com/services/cerber</con:targetNamespace>
</con:schemaEntry>