<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCusByPrdsResponse($fml as element(fml:FML32))
	as element(m:CRMGetCusByPrdsResponse) {
		&lt;m:CRMGetCusByPrdsResponse&gt;
			{
				let $CI_NUMER_PACZKI := $fml/fml:CI_NUMER_PACZKI return
					if($CI_NUMER_PACZKI)
						then &lt;m:NumerPaczki&gt;{ data($CI_NUMER_PACZKI) }&lt;/m:NumerPaczki&gt;
						else ()
			}
			{
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $DC_TYP_KLIENTA := $fml/fml:DC_TYP_KLIENTA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST
				let $CI_WIECEJ_ADDR_KORESP := $fml/fml:CI_WIECEJ_ADDR_KORESP
				let $DC_NR_TELEFONU := $fml/fml:DC_NR_TELEFONU
				let $DC_NR_TELEF_KOMORKOWEGO := $fml/fml:DC_NR_TELEF_KOMORKOWEGO
				let $CI_RELACJA := $fml/fml:CI_RELACJA
				let $CI_STATUS_GIODO := $fml/fml:CI_STATUS_GIODO
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $CI_KLASA_OBSLUGI := $fml/fml:CI_KLASA_OBSLUGI
				let $CI_DECYL := $fml/fml:CI_DECYL
				let $DC_PRACOWNIK_BANKU := $fml/fml:DC_PRACOWNIK_BANKU
				let $CI_VIP := $fml/fml:CI_VIP
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:CRMGetCusByPrdsKlient&gt;
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta&gt;{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta&gt;
						else ()
					}
					{
						if($DC_TYP_KLIENTA[$p])
							then &lt;m:TypKlienta&gt;{ data($DC_TYP_KLIENTA[$p]) }&lt;/m:TypKlienta&gt;
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie&gt;{ data($DC_IMIE[$p]) }&lt;/m:Imie&gt;
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko&gt;{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko&gt;
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna&gt;{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna&gt;
						else ()
					}
					{
						if($DC_ULICA_DANE_PODST[$p])
							then &lt;m:UlicaDanePodst&gt;{ data($DC_ULICA_DANE_PODST[$p]) }&lt;/m:UlicaDanePodst&gt;
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_DANE_PODST[$p])
							then &lt;m:NrPosesLokaluDanePodst&gt;{ data($DC_NR_POSES_LOKALU_DANE_PODST[$p]) }&lt;/m:NrPosesLokaluDanePodst&gt;
						else ()
					}
					{
						if($DC_MIASTO_DANE_PODST[$p])
							then &lt;m:MiastoDanePodst&gt;{ data($DC_MIASTO_DANE_PODST[$p]) }&lt;/m:MiastoDanePodst&gt;
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_DANE_PODST[$p])
							then &lt;m:KodPocztowyDanePodst&gt;{ data($DC_KOD_POCZTOWY_DANE_PODST[$p]) }&lt;/m:KodPocztowyDanePodst&gt;
						else ()
					}
					{
						if($CI_WIECEJ_ADDR_KORESP[$p])
							then &lt;m:WiecejAddrKoresp&gt;{ data($CI_WIECEJ_ADDR_KORESP[$p]) }&lt;/m:WiecejAddrKoresp&gt;
						else ()
					}
					{
						if($DC_NR_TELEFONU[$p])
							then &lt;m:NrTelefonu&gt;{ data($DC_NR_TELEFONU[$p]) }&lt;/m:NrTelefonu&gt;
						else ()
					}
					{
						if($DC_NR_TELEF_KOMORKOWEGO[$p])
							then &lt;m:NrTelefKomorkowego&gt;{ data($DC_NR_TELEF_KOMORKOWEGO[$p]) }&lt;/m:NrTelefKomorkowego&gt;
						else ()
					}
					{
						if($CI_RELACJA[$p])
							then &lt;m:Relacja&gt;{ data($CI_RELACJA[$p]) }&lt;/m:Relacja&gt;
						else ()
					}
					{
						if($CI_STATUS_GIODO[$p])
							then &lt;m:StatusGiodo&gt;{ data($CI_STATUS_GIODO[$p]) }&lt;/m:StatusGiodo&gt;
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then &lt;m:UdostepGrupa&gt;{ data($CI_UDOSTEP_GRUPA[$p]) }&lt;/m:UdostepGrupa&gt;
						else ()
					}
					{
						if($CI_KLASA_OBSLUGI[$p])
							then &lt;m:KlasaObslugi&gt;{ data($CI_KLASA_OBSLUGI[$p]) }&lt;/m:KlasaObslugi&gt;
						else ()
					}
					{
						if($CI_DECYL[$p])
							then &lt;m:Decyl&gt;{ data($CI_DECYL[$p]) }&lt;/m:Decyl&gt;
						else ()
					}
					{
						if($DC_PRACOWNIK_BANKU[$p])
							then &lt;m:PracownikBanku&gt;{ data($DC_PRACOWNIK_BANKU[$p]) }&lt;/m:PracownikBanku&gt;
						else ()
					}
					{
						if($CI_VIP[$p])
							then &lt;m:Vip&gt;{ data($CI_VIP[$p]) }&lt;/m:Vip&gt;
						else ()
					}
					&lt;/m:CRMGetCusByPrdsKlient&gt;
			}
		&lt;/m:CRMGetCusByPrdsResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCusByPrdsResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>