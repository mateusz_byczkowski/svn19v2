<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPortChgRequest($req as element(m:CRMGetPortChgRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:DataOd)
					then &lt;fml:CI_DATA_OD&gt;{ data($req/m:DataOd) }&lt;/fml:CI_DATA_OD&gt;
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI&gt;{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI&gt;
					else ()
			}
                        {
                                if($req/m:NumerPaczkiStr)
                                        then &lt;fml:CI_NUMER_PACZKI_STR&gt;{ data($req/m:NumerPaczkiStr) }&lt;/fml:CI_NUMER_PACZKI_STR&gt;
                                        else ()
                        }
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetPortChgRequest($body/m:CRMGetPortChgRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>