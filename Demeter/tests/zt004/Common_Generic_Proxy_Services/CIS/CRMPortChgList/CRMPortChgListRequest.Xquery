<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMPortChgListRequest($req as element(m:CRMPortChgListRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU&gt;{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU&gt;
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				if($req/m:TypZmiany)
					then &lt;fml:CIE_TYP_ZMIANY&gt;{ data($req/m:TypZmiany) }&lt;/fml:CIE_TYP_ZMIANY&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE&gt;{ data($req/m:Imie) }&lt;/fml:DC_IMIE&gt;
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO&gt;{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO&gt;
					else ()
			}
			{
				if($req/m:NazwaPelna)
					then &lt;fml:CI_NAZWA_PELNA&gt;{ data($req/m:NazwaPelna) }&lt;/fml:CI_NAZWA_PELNA&gt;
					else ()
			}
			{
				if($req/m:IdPortfelaAkt)
					then &lt;fml:CI_ID_PORTFELA_AKT&gt;{ data($req/m:IdPortfelaAkt) }&lt;/fml:CI_ID_PORTFELA_AKT&gt;
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA&gt;{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA&gt;
					else ()
			}
			{
				for $v in $req/m:KlasaObslugiAkt
				return
					&lt;fml:CI_KLASA_OBSLUGI_AKT&gt;{ data($v) }&lt;/fml:CI_KLASA_OBSLUGI_AKT&gt;
			}
			{
				for $v in $req/m:KlasaObslugi
				return
					&lt;fml:CI_KLASA_OBSLUGI&gt;{ data($v) }&lt;/fml:CI_KLASA_OBSLUGI&gt;
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:NumerPaczkiStr)
					then &lt;fml:CI_NUMER_PACZKI_STR&gt;{ data($req/m:NumerPaczkiStr) }&lt;/fml:CI_NUMER_PACZKI_STR&gt;
					else ()
			}
			{
				if($req/m:Kierunek)
					then &lt;fml:CI_KIERUNEK&gt;{ data($req/m:Kierunek) }&lt;/fml:CI_KIERUNEK&gt;
					else ()
			}
                        {
                               if($req/m:Sortowanie)
                                        then &lt;fml:CI_SORTOWANIE&gt;{ data($req/m:Sortowanie) }&lt;/fml:CI_SORTOWANIE&gt;
                                        else ()
                        }
                        {
                                for $v in $req/m:Segment
                                return
                                        &lt;fml:DC_SEGMENT_MARKETINGOWY&gt;{ data($v) }&lt;/fml:DC_SEGMENT_MARKETINGOWY&gt;
                        }
                        {
                               for $v in $req/m:SegmentAkt
                               return
                                        &lt;fml:CI_SEGMENT_MARKETINGOWY_AKT&gt;{ data($v) }&lt;/fml:CI_SEGMENT_MARKETINGOWY_AKT&gt;
                        }
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMPortChgListRequest($body/m:CRMPortChgListRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>