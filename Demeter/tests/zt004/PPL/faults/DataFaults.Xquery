<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace flt="http://bzwbk.com/services/ppl/faults";
declare variable $fault external;

declare function local:fault($faultString as xs:string, $message as xs:string, $detail as xs:anyType) as element(soap-env:Fault) {
		&lt;soap-env:Fault&gt;
			&lt;faultcode&gt;{ $faultString }&lt;/faultcode&gt; 
			&lt;faultstring&gt;{ $message }&lt;/faultstring&gt; 
			&lt;detail&gt;{ $detail }&lt;/detail&gt;
		&lt;/soap-env:Fault&gt;
};

declare function local:errors($message as xs:string) as element()* {
	&lt;message&gt;{ $message }&lt;/message&gt;
};

declare function local:codedErrors($message as xs:string, $errorCode1 as xs:string, $errorCode2 as xs:string) as element()* {
	&lt;primaryErrorCode&gt;{ $errorCode1 }&lt;/primaryErrorCode&gt;,
	&lt;secondaryErrorCode&gt;{ $errorCode2 }&lt;/secondaryErrorCode&gt;,
	&lt;message&gt;{ $message }&lt;/message&gt;
};

&lt;soap-env:Body xmlns:flt="http://bzwbk.com/services/ppl/faults"&gt;
{
	(: nadmiarowe soap-env:Body, wymaga tego transport local (bug bea?) :)
	let $message := $fault/ctx:reason
	let $reason := fn:substring-before(fn:substring-after(fn:substring-before($fault/ctx:reason, ":"), "("), ")")
	let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($fault/ctx:reason, ":"), ":"), ":")
	return
		if($reason = "13") then
			local:fault("flt:CriticalDataAccessException", $message, element flt:CriticalDataAccessException { local:errors($message) })
		else if($reason = "11") then
			if($urcode = "35") then
				local:fault("flt:NoProfilesFoundException", $message, element flt:NoProfilesFoundException{ local:codedErrors($message, $reason, $urcode) })
			else if($urcode = "36") then
				local:fault("flt:NoLimitsFoundException", $message, element flt:NoLimitsFoundException{ local:codedErrors($message, $reason, $urcode) })
			else if($urcode = "26") then
				local:fault("flt:NoTransactionsFoundException", $message, element flt:NoTransactionsFoundException{ local:codedErrors($message, $reason, $urcode) })
			else if($urcode = "21") then
				local:fault("flt:CustomerNotFoundException", $message, element flt:CustomerNotFoundException{ local:codedErrors($message, $reason, $urcode) })
			else if($urcode = "103") then
				local:fault("flt:NoProductsFoundException", $message, element flt:NoProductsFoundException{ local:codedErrors($message, $reason, $urcode) })
			else if($urcode = "303") then
				local:fault("flt:NoAuthorizationsFoundException", $message, element flt:NoAuthorizationsFoundException{ local:codedErrors($message, $reason, $urcode) })
			else
				local:fault("flt:GeneralDecodedDataAccessException", $message, element flt:GeneralDecodedDataAccessException { local:codedErrors($message, $reason, $urcode) })
		else
			local:fault("flt:CriticalDataAccessException", $message, element flt:CriticalDataAccessException { local:errors($message) })
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>