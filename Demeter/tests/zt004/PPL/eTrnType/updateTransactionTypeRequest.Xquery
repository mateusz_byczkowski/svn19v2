<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapupdateTransactionTypeRequest($req as element(m:updateTransactionType))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/userDescription/microbranch)
					then &lt;fml:E_MICRO_BRANCH&gt;{ data($req/userDescription/microbranch) }&lt;/fml:E_MICRO_BRANCH&gt;
					else ()
			}
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME&gt;{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME&gt;
					else ()
			}
			{
				if($req/transactionType/currency)
					then &lt;fml:E_TRN_TYPE_CURRENCY&gt;{ data($req/transactionType/currency) }&lt;/fml:E_TRN_TYPE_CURRENCY&gt;
					else ()
			}
			{
				if($req/transactionType/title)
					then &lt;fml:E_TRN_TITLE&gt;{ data($req/transactionType/title) }&lt;/fml:E_TRN_TITLE&gt;
					else ()
			}
			{
				if($req/transactionType/name)
					then &lt;fml:E_TRN_TYPE_NAME&gt;{ data($req/transactionType/name) }&lt;/fml:E_TRN_TYPE_NAME&gt;
					else ()
			}
			{
				if($req/transactionType/closeDate)
					then &lt;fml:E_TRN_TYPE_CLOSE_DATE&gt;{ data($req/transactionType/closeDate) }&lt;/fml:E_TRN_TYPE_CLOSE_DATE&gt;
					else ()
			}
			{
				if($req/transactionType/maxAmount)
					then &lt;fml:E_TRN_TYPE_MAX_AMOUNT&gt;{ data($req/transactionType/maxAmount) }&lt;/fml:E_TRN_TYPE_MAX_AMOUNT&gt;
					else ()
			}
			{
				if($req/transactionType/defaultChannelAllowance)
					then &lt;fml:E_DEF_CHANNEL_ALLOW&gt;{ data($req/transactionType/defaultChannelAllowance) }&lt;/fml:E_DEF_CHANNEL_ALLOW&gt;
					else ()
			}
			{
				if($req/transactionType/defaultTransferType)
					then &lt;fml:E_TRANSFER_TYPE&gt;{ data($req/transactionType/defaultTransferType) }&lt;/fml:E_TRANSFER_TYPE&gt;
					else ()
			}
			{
				if ($req/transactionType/productType) 
					then if (fn:string($req/transactionType/productType) != '')
						then &lt;fml:B_RODZAJ_RACH&gt;{ data($req/transactionType/productType) }&lt;/fml:B_RODZAJ_RACH&gt;
						else &lt;fml:B_RODZAJ_RACH nil="true"/&gt;

					else ()
			}
			{
				if($req/transactionType/allowedSecurity)
					then &lt;fml:E_ALLOWED_SECURITY_LEVEL&gt;{ data($req/transactionType/allowedSecurity) }&lt;/fml:E_ALLOWED_SECURITY_LEVEL&gt;
					else ()
			}
			{
				if($req/transactionType/securityLevel)
					then &lt;fml:E_SECURITY_LEVEL&gt;{ data($req/transactionType/securityLevel) }&lt;/fml:E_SECURITY_LEVEL&gt;
					else ()
			}
			{
				if($req/transactionType/options)
					then &lt;fml:E_TRN_TYPE_OPTIONS&gt;{ data($req/transactionType/options) }&lt;/fml:E_TRN_TYPE_OPTIONS&gt;
					else ()
			}
			{
				if($req/transactionType/transactionTypeId)
					then &lt;fml:E_TRN_TYPE&gt;{ data($req/transactionType/transactionTypeId) }&lt;/fml:E_TRN_TYPE&gt;
					else ()
			}
			{
				if($req/transactionType/productMask)
					then &lt;fml:E_PRODUCT_ACTION_MASK&gt;{ data($req/transactionType/productMask) }&lt;/fml:E_PRODUCT_ACTION_MASK&gt;
					else ()
			}
			{
				if($req/transactionType/transactionMaxForwardDays)
					then &lt;fml:E_TRN_TYPE_MAX_FRWD_DAYS&gt;{ data($req/transactionType/transactionMaxForwardDays) }&lt;/fml:E_TRN_TYPE_MAX_FRWD_DAYS&gt;
					else ()
			}
			{
				if($req/transactionType/allowedTransferTypes)
					then &lt;fml:E_ALLOWED_TRANSFER_TYPES&gt;{ data($req/transactionType/allowedTransferTypes) }&lt;/fml:E_ALLOWED_TRANSFER_TYPES&gt;
				else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapupdateTransactionTypeRequest($body/m:updateTransactionType) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>