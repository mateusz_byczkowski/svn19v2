<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetTransactionTypesRangeResponse($fml as element(fml:FML32))
	as element(m:getTransactionTypesRangeResponse) {
		&lt;m:getTransactionTypesRangeResponse xmlns:m="http://bzwbk.com/services/ppl/messages"&gt;
			&lt;getTransactionTypesRangeReturn xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="soapenc:Array" soapenc:arrayType="m:TransactionType[0]" xmlns=""&gt;
			{

				let $E_TRN_TYPE_CURRENCY := $fml/fml:E_TRN_TYPE_CURRENCY
				let $E_TRN_TITLE := $fml/fml:E_TRN_TITLE
				let $E_TRN_TYPE_NAME := $fml/fml:E_TRN_TYPE_NAME
				let $E_TRN_TYPE_CLOSE_DATE := $fml/fml:E_TRN_TYPE_CLOSE_DATE
				let $E_TRN_TYPE_MAX_AMOUNT := $fml/fml:E_TRN_TYPE_MAX_AMOUNT
				let $E_DEF_CHANNEL_ALLOW := $fml/fml:E_DEF_CHANNEL_ALLOW
				let $E_TRANSFER_TYPE := $fml/fml:E_TRANSFER_TYPE
				let $B_RODZAJ_RACH := $fml/fml:B_RODZAJ_RACH
				let $E_ALLOWED_SECURITY_LEVEL := $fml/fml:E_ALLOWED_SECURITY_LEVEL
				let $E_SECURITY_LEVEL := $fml/fml:E_SECURITY_LEVEL
				let $E_TRN_TYPE_OPTIONS := $fml/fml:E_TRN_TYPE_OPTIONS
				let $E_TRN_TYPE := $fml/fml:E_TRN_TYPE
				let $E_PRODUCT_ACTION_MASK := $fml/fml:E_PRODUCT_ACTION_MASK
				let $E_TRN_TYPE_MAX_FRWD_DAYS := $fml/fml:E_TRN_TYPE_MAX_FRWD_DAYS
				let $E_ALLOWED_TRANSFER_TYPES := $fml/fml:E_ALLOWED_TRANSFER_TYPES
				for $it at $p in $fml/fml:E_TRN_TYPE
				return
					&lt;item&gt;
					{
						if($E_TRN_TYPE_CURRENCY[$p])
							then &lt;currency&gt;{ data($E_TRN_TYPE_CURRENCY[$p]) }&lt;/currency&gt;
						else ()
					}
					{
						if($E_TRN_TITLE[$p])
							then &lt;title&gt;{ data($E_TRN_TITLE[$p]) }&lt;/title&gt;
						else ()
					}
					{
						if($E_TRN_TYPE_NAME[$p])
							then &lt;name&gt;{ data($E_TRN_TYPE_NAME[$p]) }&lt;/name&gt;
						else ()
					}
					{
						if($E_TRN_TYPE_CLOSE_DATE[$p])
							then &lt;closeDate&gt;{ data($E_TRN_TYPE_CLOSE_DATE[$p]) }&lt;/closeDate&gt;
						else ()
					}
					{
						if($E_TRN_TYPE_MAX_AMOUNT[$p])
							then &lt;maxAmount&gt;{ data($E_TRN_TYPE_MAX_AMOUNT[$p]) }&lt;/maxAmount&gt;
						else ()
					}
					{
						if($E_DEF_CHANNEL_ALLOW[$p])
							then &lt;defaultChannelAllowance&gt;{ data($E_DEF_CHANNEL_ALLOW[$p]) }&lt;/defaultChannelAllowance&gt;
						else ()
					}
					{
						if($E_TRANSFER_TYPE[$p])
							then &lt;defaultTransferType&gt;{ data($E_TRANSFER_TYPE[$p]) }&lt;/defaultTransferType&gt;
						else ()
					}
					{
						if($B_RODZAJ_RACH[$p])
							then &lt;productType&gt;{ data($B_RODZAJ_RACH[$p]) }&lt;/productType&gt;
						else ()
					}
					{
						if($E_ALLOWED_SECURITY_LEVEL[$p])
							then &lt;allowedSecurity&gt;{ data($E_ALLOWED_SECURITY_LEVEL[$p]) }&lt;/allowedSecurity&gt;
						else ()
					}
					{
						if($E_SECURITY_LEVEL[$p])
							then &lt;securityLevel&gt;{ data($E_SECURITY_LEVEL[$p]) }&lt;/securityLevel&gt;
						else ()
					}
					{
						if($E_TRN_TYPE_OPTIONS[$p])
							then &lt;options&gt;{ data($E_TRN_TYPE_OPTIONS[$p]) }&lt;/options&gt;
						else ()
					}
					{
						if($E_TRN_TYPE[$p])
							then &lt;transactionTypeId&gt;{ data($E_TRN_TYPE[$p]) }&lt;/transactionTypeId&gt;
						else ()
					}
					{
						if($E_PRODUCT_ACTION_MASK[$p])
							then &lt;productMask&gt;{ data($E_PRODUCT_ACTION_MASK[$p]) }&lt;/productMask&gt;
						else ()
					}
					{
						if($E_TRN_TYPE_MAX_FRWD_DAYS[$p])
							then &lt;transactionMaxForwardDays&gt;{ data($E_TRN_TYPE_MAX_FRWD_DAYS[$p]) }&lt;/transactionMaxForwardDays&gt;
							else ()
					}
					{
						if($E_ALLOWED_TRANSFER_TYPES[$p])
							then &lt;allowedTransferTypes&gt;{ data($E_ALLOWED_TRANSFER_TYPES[$p]) }&lt;/allowedTransferTypes&gt;
							else ()
					}
					&lt;/item&gt;
			}
			&lt;/getTransactionTypesRangeReturn&gt;
		&lt;/m:getTransactionTypesRangeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetTransactionTypesRangeResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>