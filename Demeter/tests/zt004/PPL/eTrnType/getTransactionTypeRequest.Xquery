<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetTransactionTypeRequest($req as element(m:getTransactionType))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/userDescription/microbranch)
					then &lt;fml:E_MICRO_BRANCH&gt;{ data($req/userDescription/microbranch) }&lt;/fml:E_MICRO_BRANCH&gt;
					else ()
			}
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME&gt;{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME&gt;
					else ()
			}
			{
				if($req/transactionTypeId)
					then &lt;fml:E_TRN_TYPE&gt;{ data($req/transactionTypeId) }&lt;/fml:E_TRN_TYPE&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetTransactionTypeRequest($body/m:getTransactionType) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>