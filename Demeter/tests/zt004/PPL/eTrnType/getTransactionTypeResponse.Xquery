<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapgetTransactionTypeResponse($fml as element(fml:FML32))
	as element(m:getTransactionTypeResponse) {
		&lt;m:getTransactionTypeResponse&gt;
			&lt;getTransactionTypeReturn xmlns=""&gt;
			{
				if($fml/fml:E_TRN_TYPE_CURRENCY)
					then &lt;currency&gt;{ data($fml/fml:E_TRN_TYPE_CURRENCY) }&lt;/currency&gt;
					else ()
			}
			{
				if($fml/fml:E_TRN_TITLE)
					then &lt;title&gt;{ data($fml/fml:E_TRN_TITLE) }&lt;/title&gt;
					else ()
			}
			{
				if($fml/fml:E_TRN_TYPE_NAME)
					then &lt;name&gt;{ data($fml/fml:E_TRN_TYPE_NAME) }&lt;/name&gt;
					else ()
			}
			{
				if($fml/fml:E_TRN_TYPE_CLOSE_DATE)
					then &lt;closeDate&gt;{ data($fml/fml:E_TRN_TYPE_CLOSE_DATE) }&lt;/closeDate&gt;
					else ()
			}
			{
				if($fml/fml:E_TRN_TYPE_MAX_AMOUNT)
					then &lt;maxAmount&gt;{ data($fml/fml:E_TRN_TYPE_MAX_AMOUNT) }&lt;/maxAmount&gt;
					else ()
			}
			{
				if($fml/fml:E_DEF_CHANNEL_ALLOW)
					then &lt;defaultChannelAllowance&gt;{ data($fml/fml:E_DEF_CHANNEL_ALLOW) }&lt;/defaultChannelAllowance&gt;
					else ()
			}
			{
				if($fml/fml:E_TRANSFER_TYPE)
					then &lt;defaultTransferType&gt;{ data($fml/fml:E_TRANSFER_TYPE) }&lt;/defaultTransferType&gt;
					else ()
			}
			{
				if($fml/fml:B_RODZAJ_RACH)
					then &lt;productType&gt;{ data($fml/fml:B_RODZAJ_RACH) }&lt;/productType&gt;
					else ()
			}
			{
				if($fml/fml:E_ALLOWED_SECURITY_LEVEL)
					then &lt;allowedSecurity&gt;{ data($fml/fml:E_ALLOWED_SECURITY_LEVEL) }&lt;/allowedSecurity&gt;
					else ()
			}
			{
				if($fml/fml:E_SECURITY_LEVEL)
					then &lt;securityLevel&gt;{ data($fml/fml:E_SECURITY_LEVEL) }&lt;/securityLevel&gt;
					else ()
			}
			{
				if($fml/fml:E_TRN_TYPE_OPTIONS)
					then &lt;options&gt;{ data($fml/fml:E_TRN_TYPE_OPTIONS) }&lt;/options&gt;
					else ()
			}
			{
				if($fml/fml:E_TRN_TYPE)
					then &lt;transactionTypeId&gt;{ data($fml/fml:E_TRN_TYPE) }&lt;/transactionTypeId&gt;
					else ()
			}
			{
				if($fml/fml:E_PRODUCT_ACTION_MASK)
					then &lt;productMask&gt;{ data($fml/fml:E_PRODUCT_ACTION_MASK) }&lt;/productMask&gt;
					else ()
			}
			{
				if($fml/fml:E_TRN_TYPE_MAX_FRWD_DAYS)
					then &lt;transactionMaxForwardDays&gt;{ data($fml/fml:E_TRN_TYPE_MAX_FRWD_DAYS) }&lt;/transactionMaxForwardDays&gt;
					else ()
			}
			{
				if($fml/fml:E_ALLOWED_TRANSFER_TYPES)
					then &lt;allowedTransferTypes&gt;{ data($fml/fml:E_ALLOWED_TRANSFER_TYPES) }&lt;/allowedTransferTypes&gt;
					else ()
			}
			&lt;/getTransactionTypeReturn&gt;
		&lt;/m:getTransactionTypeResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapgetTransactionTypeResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>