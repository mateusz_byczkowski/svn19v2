<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Typ klienta domyślny to F</con:description>
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace m = "urn:be.services.dcl";
declare namespace e = "urn:cif.entities.be.dcl";
declare namespace e2 = "urn:dictionaries.be.dcl";

declare function local:mapUpdateCustomersEntryPasswordRequest($req as element(), $head as element())
    as element(fml:FML32) {
        &lt;fml:FML32&gt;
        	&lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:customer/e:Customer/e:customerNumber) }&lt;/fml:DC_NUMER_KLIENTA&gt;
        	&lt;fml:DC_NAZWISKO_PANIENSKIE_MATKI&gt;{ data($req/m:customer/e:Customer/e:customerPersonal/e:CustomerPersonal/e:motherMaidenName) }&lt;/fml:DC_NAZWISKO_PANIENSKIE_MATKI&gt;
                 {
                 if (string-length(data($req/m:customer/e:Customer/e:customerType/e2:CustomerType/e2:customerType)) &gt; 0) then
        	     &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:customer/e:Customer/e:customerType/e2:CustomerType/e2:customerType) }&lt;/fml:DC_TYP_KLIENTA&gt;
                else
                      &lt;fml:DC_TYP_KLIENTA&gt;F&lt;/fml:DC_TYP_KLIENTA&gt;
                 }
        	&lt;fml:CI_PRACOWNIK_ZMIEN&gt;{ data($head/m:msgHeader/m:userId) }&lt;/fml:CI_PRACOWNIK_ZMIEN&gt;
                {
                 if (string-length(data($req/m:customer/e:Customer/e:companyID/e2:CompanyType/e2:companyType)) &gt; 0) then
                    &lt;CI_ID_SPOLKI&gt;{data($req/m:customer/e:Customer/e:companyID/e2:CompanyType/e2:companyType)}&lt;/CI_ID_SPOLKI&gt;
                 else
                    &lt;CI_ID_SPOLKI&gt;{ data($head/m:msgHeader/m:companyId) }&lt;/CI_ID_SPOLKI&gt;
                }
         &lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;
&lt;soap-env:Body&gt;
{ local:mapUpdateCustomersEntryPasswordRequest($body/m:invoke, $header/m:header) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>