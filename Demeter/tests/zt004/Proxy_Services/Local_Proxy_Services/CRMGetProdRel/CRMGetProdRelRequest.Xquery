<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetProdRelRequest($req as element(m:CRMGetProdRelRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:ApplinApplicationnumbe)
					then &lt;fml:NF_APPLIN_APPLICATIONNUMBE&gt;{ data($req/m:ApplinApplicationnumbe) }&lt;/fml:NF_APPLIN_APPLICATIONNUMBE&gt;
					else ()
			}
			{
				if($req/m:AccounAccountnumber)
					then &lt;fml:NF_ACCOUN_ACCOUNTNUMBER&gt;{ data($req/m:AccounAccountnumber) }&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER&gt;
					else ()
			}
			{
				if($req/m:PagecPagesize)
					then &lt;fml:NF_PAGEC_PAGESIZE&gt;{ data($req/m:PagecPagesize) }&lt;/fml:NF_PAGEC_PAGESIZE&gt;
					else ()
			}
			{
				if($req/m:PagecActioncode)
					then &lt;fml:NF_PAGEC_ACTIONCODE&gt;{ data($req/m:PagecActioncode) }&lt;/fml:NF_PAGEC_ACTIONCODE&gt;
					else ()
			}
			{
				if($req/m:PagecReverseorder)
					then &lt;fml:NF_PAGEC_REVERSEORDER&gt;{ data($req/m:PagecReverseorder) }&lt;/fml:NF_PAGEC_REVERSEORDER&gt;
					else ()
			}
			{
				if($req/m:PagecNavigationkeyvalu)
					then &lt;fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;{ data($req/m:PagecNavigationkeyvalu) }&lt;/fml:NF_PAGEC_NAVIGATIONKEYVALU&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetProdRelRequest($body/m:CRMGetProdRelRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>