<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.4 2010-03-05   PKLI NP2037_1 CR1 
v.1.3  2010-01-28  PKLI  Obsługa ServiceWarning
v.1.2  2009-07-10  PKLI  TEET 39149
v.1.1  2009-06-24  PKLI  TEET 38316</con:description>
  <con:xquery>(: Log Zmian: 
==================================
 rk01 21/04/2009 RKu Pole ProductNamePl bedzie bralo nazwe z III poziomu drzewa prod (NF_ACCOUN_ACCOUNTDESCRIPTI), 
                                zamiast jak dotej pory z IV-go (PT_POLISH_NAME)
 v.1.1  2009-06-24  PKLI  TEET 38316 
                                Zmiana mapowania pola ProductId z PT_ID_DEFINITION na PT_ID_GROUP
 v.1.2  2009-07-10 PKLI TEET 39149
                                Zaprzestanie zwracania pól Saldo i DostSrodki dla kart debetowych 	
 v.1.3  2010-01-28  PKLI  NP1972_1
                                Obsługa ServiceWarning	
 v.1.4  2010-03-05  PKLI NP2037_1 CR1 
                                Zaprzestanie wyliczania salda ubezpieczeń JV 
:)

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:getElementsForProductsList($fml as element(fml:FML32))
	as element()* {
		
				let $CI_PRODUCT_CATEGORY_ID := $fml/fml:PT_ID_CATEGORY
				let $CI_PRODUCT_CATEGORY_CODE := $fml/fml:PT_CODE_PRODUCT_CATEGORY
				let $CI_PRODUCT_CATEGORY_NAME_PL := $fml/fml:PT_POLISH_CATEGORY_NAME

				let $CI_PRODUCT_CATEGORY_NAME_EN := $fml/fml:PT_ENGLISH_CATEGORY_NAME

				let $CI_PRODUCT_ID := $fml/fml:PT_ID_DEFINITION
                                                                let $CI_GROUP_ID := $fml/fml:PT_ID_GROUP   (: v.1.1 T38316 :)
				let $CI_PRODUCT_CODE := $fml/fml:NF_ACCOUT_ACCOUNTTYPE
				let $CI_PRODUCT_NAME_EN := $fml/fml:PT_ENGLISH_NAME
                                                                (: rk1 - ponizej bylo PT_POLISH_NAME, jest NF_ACCOUN_ACCOUNTDESCRIPTI :)
				let $CI_PRODUCT_NAME_PL := $fml/fml:NF_ACCOUN_ACCOUNTDESCRIPTI 
				let $NF_CTRL_SYSTEMID := $fml/fml:NF_CTRL_SYSTEMID
				let $B_POJEDYNCZY := $fml/fml:NF_IS_COOWNER
				let $NF_ACCOUN_ACCOUNTNUMBER:= $fml/fml:NF_ACCOUN_ACCOUNTNUMBER
                                let $NF_DEBITC_VIRTUALCARDNBR:=$fml/fml:NF_DEBITC_VIRTUALCARDNBR
                                let $NF_DEBITC_CARDNBR:=$fml/fml:NF_DEBITC_CARDNBR
				let $CI_PRODUCT_ATT1 := $fml/fml:NF_ATTRPG_FIRSTPRODUCTFEAT
				let $CI_PRODUCT_ATT2 := $fml/fml:NF_ATTRPG_SECONDPRODUCTFEA
				let $CI_PRODUCT_ATT3 := $fml/fml:NF_ATTRPG_THIRDPRODUCTFEAT
				let $CI_PRODUCT_ATT4 := $fml/fml:NF_ATTRPG_FOURTHPRODUCTFEA
				let $CI_PRODUCT_ATT5 := $fml/fml:NF_ATTRPG_FIFTHPRODUCTFEAT
				let $CI_KOD_PRODUKTU := $fml/fml:NF_PRODUD_SORCEPRODUCTCODE
				let $CI_RACHUNEK_ADR_ALT := $fml/fml:NF_ACCOUN_ALTERNATIVEADDRE
				let $B_SALDO := $fml/fml:NF_ACCOUN_CURRENTBALANCE
				let $DC_BLOKADA_SRODK_NA_R_KU := $fml/fml:NF_HOLD_HOLDAMOUNT
				let $DC_WALUTA := $fml/fml:NF_CURREC_CURRENCYCODE
				let $DC_DATA_OTWARCIA_RACHUNKU := $fml/fml:NF_ACCOUN_ACCOUNTOPENDATE
				let $B_DATA_OST_OPER := $fml/fml:NF_TRANA_DATEOFLASTACTIVIT
				let $B_LIMIT1 := $fml/fml:NF_TRAACA_LIMITAMOUNT
				let $CI_PRODUCT_NAME_ORYGINAL := $fml/fml:NF_ACCOUN_ACCOUNTNAME

				let $CI_STATUS := $fml/fml:NF_PRODUCT_STAT	

                                let $CI_VISIBILITY_DCL := $fml/fml:PT_VISIBILITY_DCL
                                let $CI_RELACJA := $fml/fml:NF_ACCOUR_RELATIONSHIP
                                let $B_DOST_SRODKI:= $fml/fml:NF_ACCOUN_CURRENTBALANCE
                                let $B_KOD_RACH:=$fml/fml:NF_PRIMAC_ACCOUNTCODE
                                let $NF_INSURA_ID:=$fml/fml:NF_INSURA_ID
                                let $NF_TRANA_DMEMP := $fml/fml:NF_TRANA_DMEMP
				for $it at $p in $fml/fml:NF_CTRL_SYSTEMID
				return
					&lt;m:Product&gt;	
      	            {
				                if($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p])
				 	               then &lt;m:ProductAreaId&gt;{ data($fml/fml:NF_PRODUA_CODEPRODUCTAREA[$p]) }&lt;/m:ProductAreaId&gt;
					        else ()
			        }
					{
						if($CI_PRODUCT_CATEGORY_ID[$p])
							then &lt;m:ProductCategoryId&gt;{ data($CI_PRODUCT_CATEGORY_ID[$p]) }&lt;/m:ProductCategoryId&gt;
						else ()
					}
					{
						if($CI_PRODUCT_CATEGORY_CODE[$p])
							then &lt;m:ProductCategoryCode&gt;{ data($CI_PRODUCT_CATEGORY_CODE[$p]) }&lt;/m:ProductCategoryCode&gt;
						else ()
					}
					{
						if($CI_PRODUCT_CATEGORY_NAME_PL[$p])
							then &lt;m:ProductCategoryNamePl&gt;{ data($CI_PRODUCT_CATEGORY_NAME_PL[$p]) }&lt;/m:ProductCategoryNamePl&gt;
						else ()
					}
					{
						if($CI_PRODUCT_CATEGORY_NAME_EN[$p])
							then &lt;m:ProductCategoryNameEn&gt;{ data($CI_PRODUCT_CATEGORY_NAME_EN[$p]) }&lt;/m:ProductCategoryNameEn&gt;
						else ()
					}
					{
					(:	if($CI_PRODUCT_ID[$p])                                                                                v.1.1 T38316 :)
					(:		then &lt;m:ProductId&gt;{ data($CI_PRODUCT_ID[$p]) }&lt;/m:ProductId&gt;   v.1.1 T38316 :)
					(:	else ()                                                                                                          v.1.1 T38316 :)
						if($CI_GROUP_ID[$p])
							then &lt;m:ProductId&gt;{ data($CI_GROUP_ID[$p]) }&lt;/m:ProductId&gt;
						else ()
					}
					{
						if($CI_PRODUCT_CODE[$p])
							then &lt;m:ProductCode&gt;{ data($CI_PRODUCT_CODE[$p]) }&lt;/m:ProductCode&gt;
						else ()
					}
					{
						if($CI_PRODUCT_NAME_EN[$p])
							then &lt;m:ProductNameEn&gt;{ data($CI_PRODUCT_NAME_EN[$p]) }&lt;/m:ProductNameEn&gt;
						else ()
					}
					{
						if($CI_PRODUCT_NAME_PL[$p])
							then &lt;m:ProductNamePl&gt;{ data($CI_PRODUCT_NAME_PL[$p]) }&lt;/m:ProductNamePl&gt;
						else ()
					}
					{
						if($NF_CTRL_SYSTEMID[$p])
							then &lt;m:IdSys&gt;{ data($NF_CTRL_SYSTEMID[$p]) }&lt;/m:IdSys&gt;
						else ()
					}
					{
						if($B_POJEDYNCZY[$p])
							then &lt;m:Pojedynczy&gt;{ data($B_POJEDYNCZY[$p]) }&lt;/m:Pojedynczy&gt;
						else ()
					}
					{
						if($NF_DEBITC_CARDNBR[$p] and string-length(data($NF_DEBITC_CARDNBR[$p]))&gt;1 )
							then &lt;m:NrRachunku&gt;{ data($NF_DEBITC_CARDNBR[$p]) }&lt;/m:NrRachunku&gt;
						else if ($NF_INSURA_ID[$p] and string-length(data($NF_INSURA_ID[$p]))&gt;1)
                                                       then &lt;m:NrRachunku&gt;{ data($NF_INSURA_ID[$p]) }&lt;/m:NrRachunku&gt;
                                                   else
                                                      &lt;m:NrRachunku&gt;{ data($NF_ACCOUN_ACCOUNTNUMBER[$p]) }&lt;/m:NrRachunku&gt;
					}

					{
						if($NF_DEBITC_CARDNBR[$p] and string-length(data($NF_DEBITC_CARDNBR[$p]))&gt;1 )
							then &lt;m:NrAlternatywny&gt;{ data($NF_DEBITC_VIRTUALCARDNBR[$p]) }&lt;/m:NrAlternatywny&gt;
						else if ($NF_INSURA_ID[$p] and string-length(data($NF_INSURA_ID[$p]))&gt;1)
                                                        then if (string-length(data($NF_DEBITC_VIRTUALCARDNBR[$p]))&gt;0)
                                                             then &lt;m:NrAlternatywny&gt;{ data($NF_DEBITC_VIRTUALCARDNBR[$p]) }&lt;/m:NrAlternatywny&gt;
                                                             else()
                                                else()
					         
					}

					{
						if($CI_PRODUCT_ATT1[$p])
							then &lt;m:ProductAtt1&gt;{ data($CI_PRODUCT_ATT1[$p]) }&lt;/m:ProductAtt1&gt;
						else ()
					}
					{
						if($CI_PRODUCT_ATT2[$p])
							then &lt;m:ProductAtt2&gt;{ data($CI_PRODUCT_ATT2[$p]) }&lt;/m:ProductAtt2&gt;
						else ()
					}
					{
						if($CI_PRODUCT_ATT3[$p])
							then &lt;m:ProductAtt3&gt;{ data($CI_PRODUCT_ATT3[$p]) }&lt;/m:ProductAtt3&gt;
						else ()
					}
					{
						if($CI_PRODUCT_ATT4[$p])
							then &lt;m:ProductAtt4&gt;{ data($CI_PRODUCT_ATT4[$p]) }&lt;/m:ProductAtt4&gt;
						else ()
					}
					{
						if($CI_PRODUCT_ATT5[$p])
							then &lt;m:ProductAtt5&gt;{ data($CI_PRODUCT_ATT5[$p]) }&lt;/m:ProductAtt5&gt;
						else ()
					}
					{
						if($CI_KOD_PRODUKTU[$p])
							then &lt;m:KodProduktu&gt;{ data($CI_KOD_PRODUKTU[$p]) }&lt;/m:KodProduktu&gt;
						else ()
					}
					{
						if($CI_RACHUNEK_ADR_ALT[$p])
							then &lt;m:RachunekAdrAlt&gt;{ data($CI_RACHUNEK_ADR_ALT[$p]) }&lt;/m:RachunekAdrAlt&gt;
						else ()
					}
					{
						(: T39149 if($B_SALDO[$p])   :)
						(: T39149	then &lt;m:Saldo&gt;{ data($B_SALDO[$p]) }&lt;/m:Saldo&gt; :)
						(: T39149 else () :)
					
						if($NF_DEBITC_CARDNBR[$p] and string-length(data($NF_DEBITC_CARDNBR[$p]))&gt;1) 
						  then ()                                                                                                             
						else                                                                                                                    
                                                                                                  &lt;m:Saldo&gt;{ data($B_SALDO[$p]) }&lt;/m:Saldo&gt;	                                        
					}
					{
						if($DC_BLOKADA_SRODK_NA_R_KU[$p] and data($DC_BLOKADA_SRODK_NA_R_KU[$p])!="0.00")
							then &lt;m:BlokadaSrodkNaRKu&gt;1&lt;/m:BlokadaSrodkNaRKu&gt;
						else &lt;m:BlokadaSrodkNaRKu&gt;0&lt;/m:BlokadaSrodkNaRKu&gt;
					}
					{
						if($DC_WALUTA[$p])
							then &lt;m:Waluta&gt;{ data($DC_WALUTA[$p]) }&lt;/m:Waluta&gt;
						else ()
					}
					{
						if($DC_DATA_OTWARCIA_RACHUNKU[$p])
							then &lt;m:DataOtwarciaRachunku&gt;{ data($DC_DATA_OTWARCIA_RACHUNKU[$p]) }&lt;/m:DataOtwarciaRachunku&gt;
						else ()
					}
					{
						if($B_DATA_OST_OPER[$p])
							then &lt;m:DataOstOper&gt;{ data($B_DATA_OST_OPER[$p]) }&lt;/m:DataOstOper&gt;
						else ()
					}
					{
						if($B_LIMIT1[$p])
							then &lt;m:Limit1&gt;{ data($B_LIMIT1[$p]) }&lt;/m:Limit1&gt;
						else ()
					}
					{
						if($CI_PRODUCT_NAME_ORYGINAL[$p])
							then &lt;m:ProductNameOryginal&gt;{ data($CI_PRODUCT_NAME_ORYGINAL[$p]) }&lt;/m:ProductNameOryginal&gt;
						else ()
					}
					{
						if($CI_STATUS[$p])
							then &lt;m:Status&gt;{ data($CI_STATUS[$p]) }&lt;/m:Status&gt;
						else ()
					}
					{
						if($CI_RELACJA[$p])
							then &lt;m:Relacja&gt;{ data($CI_RELACJA[$p]) }&lt;/m:Relacja&gt;
						else ()
					}
					{
						if($CI_VISIBILITY_DCL[$p])
							then &lt;m:VisibilityDCL&gt;{ data($CI_VISIBILITY_DCL[$p]) }&lt;/m:VisibilityDCL&gt;
						else ()
					}
					{
					(: T39149 	if($B_DOST_SRODKI[$p]) :)
					(: T39149 		then &lt;m:DostSrodki&gt;{ data($B_DOST_SRODKI[$p]) }&lt;/m:DostSrodki&gt; :)
					(: T39149 	else () :)

                                                                                                if($NF_DEBITC_CARDNBR[$p] and string-length(data($NF_DEBITC_CARDNBR[$p]))&gt;1 ) 
						  then ()                                                                                                             
						else                                                                                                                    
                                                                                                  &lt;m:DostSrodki&gt;{ data($B_DOST_SRODKI[$p]) }&lt;/m:DostSrodki&gt;	                         
					}
					{
						if($B_KOD_RACH[$p] and string-length($B_KOD_RACH[$p])&gt;0)
							then &lt;m:KodRach&gt;{ data($B_KOD_RACH[$p]) }&lt;/m:KodRach&gt;
						else if($NF_DEBITC_CARDNBR[$p] and string-length(data($NF_DEBITC_CARDNBR[$p]))&gt;1 )
                                                       then  &lt;m:KodRach&gt;{ substring(data($NF_ACCOUN_ACCOUNTNUMBER[$p]),string-length($NF_ACCOUN_ACCOUNTNUMBER[$p]) - 9,10) }&lt;/m:KodRach&gt;
                                               else()
					}
                                                                                {
						if($NF_TRANA_DMEMP[$p])
							then &lt;m:FlagaPracownika&gt;{ data($NF_TRANA_DMEMP[$p]) }&lt;/m:FlagaPracownika&gt;
						else ()
					}

					&lt;/m:Product&gt;
			
};

declare function xf:getElementsForServiceWarningsList($fml as element(fml:FML32))
	as element()* {
                                let $NF_SERVIW_WARNINGCODE1 := $fml/fml:NF_SERVIW_WARNINGCODE1
                                let $NF_SERVIW_WARNINGCODE2 := $fml/fml:NF_SERVIW_WARNINGCODE2
                                let $NF_SERVIW_WARNINGUSERVISIB := $fml/fml:NF_SERVIW_WARNINGUSERVISIB
                                let $NF_SERVIW_WARNINGDESCRIPTI := $fml/fml:NF_SERVIW_WARNINGDESCRIPTI 
 	                        
                                for $it at $p in $fml/fml:NF_SERVIW_WARNINGCODE1
				return
					&lt;m:ServiceWarning&gt;	
 					{
						if($NF_SERVIW_WARNINGCODE1[$p])
							then &lt;m:WarningCode1&gt;{ data($NF_SERVIW_WARNINGCODE1[$p]) }&lt;/m:WarningCode1&gt;
	   					else ()
					}
					{
						if($NF_SERVIW_WARNINGCODE2[$p])
							then &lt;m:WarningCode2&gt;{ data($NF_SERVIW_WARNINGCODE2[$p]) }&lt;/m:WarningCode2&gt;
						else ()
					}
					{
						if($NF_SERVIW_WARNINGUSERVISIB[$p])
							then &lt;m:WarningUserVisibility&gt;{ data($NF_SERVIW_WARNINGUSERVISIB[$p]) }&lt;/m:WarningUserVisibility&gt;
						else ()
					}
					{
						if($NF_SERVIW_WARNINGDESCRIPTI[$p])
							then &lt;m:WarningDescription&gt;{ data($NF_SERVIW_WARNINGDESCRIPTI[$p]) }&lt;/m:WarningDescription&gt;
						else ()
					} 
                                      	&lt;/m:ServiceWarning&gt;	
};                               

declare function xf:mappGetCustomerProductsResponse($fml as element(fml:FML32))
	as element() {
		&lt;m:GetCustomerProductsResponse&gt;
			{
				if($fml/fml:NF_PAGECC_OPERATIONS)
					then &lt;m:LiczbaOper&gt;{ data($fml/fml:NF_PAGECC_OPERATIONS) }&lt;/m:LiczbaOper&gt;
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
					then &lt;m:KluczNawigacyjny&gt;{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/m:KluczNawigacyjny&gt;
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_HASNEXT)
					then &lt;m:NastepnaStrona&gt;{ data($fml/fml:NF_PAGEC_HASNEXT) }&lt;/m:NastepnaStrona&gt;
					else ()
			}
        
           {xf:getElementsForProductsList($fml)} 
           {xf:getElementsForServiceWarningsList($fml)} 
		&lt;/m:GetCustomerProductsResponse&gt;        
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mappGetCustomerProductsResponse($body/fml:FML32) }

&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>