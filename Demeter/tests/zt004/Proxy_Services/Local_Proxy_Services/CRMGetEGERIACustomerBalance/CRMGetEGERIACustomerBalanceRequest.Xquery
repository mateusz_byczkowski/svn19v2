<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappCRMGetEGERIACustomerBalanceRequest($req as element(m:CRMGetEGERIACustomerBalanceRequest))
	as element(fml:FML32) {
  &lt;fml:FML32&gt;
  {
    if($req/m:NumerKlienta)
      then &lt;fml:EG_KOD_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:EG_KOD_KLIENTA&gt;
    else ()
  }
  &lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mappCRMGetEGERIACustomerBalanceRequest($body/m:CRMGetEGERIACustomerBalanceRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>