<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/ceke/messages/";



declare function xf:map_getCEKEContractInPDFResponse($fml as element(fml:FML32))
	as element(urn:GetCEKEContractInPDFResponse) {
                 &lt;urn:GetCEKEContractInPDFResponse&gt;
			&lt;contractInPDF&gt;{ data($fml/fml:E_DOC_CONTENT) }&lt;/contractInPDF&gt;
		 &lt;/urn:GetCEKEContractInPDFResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:map_getCEKEContractInPDFResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>