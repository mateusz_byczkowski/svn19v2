<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMSaveQuestionnaireSrcRequest($req as element(m:CRMSaveQuestionnaireSrcRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:QuestId)
					then &lt;fml:CI_ID_ANKIETY&gt;{ data($req/m:QuestId) }&lt;/fml:CI_ID_ANKIETY&gt;
					else ()
			}
			{
				if($req/m:CompanyId)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:CompanyId) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:ValidFrom)
					then &lt;fml:CI_DATA_OD&gt;{ data($req/m:ValidFrom) }&lt;/fml:CI_DATA_OD&gt;
					else ()
			}
			{
				if($req/m:ValidTo)
					then &lt;fml:CI_DATA_DO&gt;{ data($req/m:ValidTo) }&lt;/fml:CI_DATA_DO&gt;
					else ()
			}
			{
				if($req/m:UsrSkpNo)
					then &lt;fml:CI_SKP_AUTORA&gt;{ data($req/m:UsrSkpNo) }&lt;/fml:CI_SKP_AUTORA&gt;
					else ()
			}
			{
				if($req/m:XmlQuestionnaire)
					then &lt;fml:CI_ANKIETA_XML&gt;{ data($req/m:XmlQuestionnaire) }&lt;/fml:CI_ANKIETA_XML&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMSaveQuestionnaireSrcRequest($body/m:CRMSaveQuestionnaireSrcRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>