<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMApproveRejectLFRFlagRequest($req as element(m:appRejectLFRFlagRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			
                        {
				if($req/m:SupervisorSKPNo)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SupervisorSKPNo) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
                        {
				if($req/m:CustCIF)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:CustCIF) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:UsrSKPNo)
					then &lt;fml:CI_SKP_PRACOWNIKA_REJ&gt;{ data($req/m:UsrSKPNo) }&lt;/fml:CI_SKP_PRACOWNIKA_REJ&gt;
					else ()
			}
			{
				if($req/m:CompanyId)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:CompanyId) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}			
			{
				if($req/m:DecisionFlag)
					then &lt;fml:CI_AKCEPTACJA_LFR&gt;{ data($req/m:DecisionFlag) }&lt;/fml:CI_AKCEPTACJA_LFR&gt;
					else ()
			}

			{
				if($req/m:LfrRejectDesc)
                                        then &lt;fml:CI_POWOD_ODRZUCENIA_LFR&gt;{data($req/m:LfrRejectDesc)}&lt;/fml:CI_POWOD_ODRZUCENIA_LFR&gt;
                                        else () 
			}		
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMApproveRejectLFRFlagRequest($body/m:appRejectLFRFlagRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>