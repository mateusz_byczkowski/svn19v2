<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace fml = "";

declare function xf:mapCRMGetProfPortfRequest($req as element(m:CRMGetProfPortfRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA&gt;{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA&gt;
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:ZnacznikOkresu)
					then &lt;fml:CI_ZNACZNIK_OKRESU&gt;{ data($req/m:ZnacznikOkresu) }&lt;/fml:CI_ZNACZNIK_OKRESU&gt;
					else ()
			}
			{
				for $Decyl in $req/m:Decyl
				return 
					&lt;fml:CI_DECYL&gt;{ data($Decyl) }&lt;/fml:CI_DECYL&gt;
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:NumerPaczkiStr)
					then &lt;fml:CI_NUMER_PACZKI_STR&gt;{ data($req/m:NumerPaczkiStr) }&lt;/fml:CI_NUMER_PACZKI_STR&gt;
					else ()
			}
			{
				if($req/m:Kierunek)
					then &lt;fml:CI_KIERUNEK&gt;{ data($req/m:Kierunek) }&lt;/fml:CI_KIERUNEK&gt;
					else ()
			}
			{
				if($req/m:Vip)
					then &lt;fml:CI_VIP&gt;{ data($req/m:Vip) }&lt;/fml:CI_VIP&gt;
					else ()
			}
			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI&gt;{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $req as element(m:CRMGetProfPortfRequest) external;
&lt;soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"&gt;
{ xf:mapCRMGetProfPortfRequest($req) }
&lt;/soapenv:Body&gt;</con:xquery>
</con:xqueryEntry>