<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="chgTillStatus.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="chgTillStatus.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invokeResponse1" element="ns0:invokeResponse" location="chgTillStatus.WSDL" ::)
(:: pragma bea:global-element-return element="ns6:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns2 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns9 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:acceptance.entities.be.dcl";
declare namespace ns6 = "http://bzwbk.com/nfe/transactionLog";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Till/chgTillStatus/chgTillStatusTLRequest/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace ns8 = "urn:acceptancedict.dictionaries.be.dcl";

declare function xf:chgTillStatusTLRequest($invoke1 as element(ns0:invoke),
    $header1 as element(ns0:header),
    $invokeResponse1 as element(ns0:invokeResponse))
    as element(ns6:transactionLogEntry) {
        &lt;ns6:transactionLogEntry&gt;
            {
                for $businessTransactionType in $invoke1/ns0:transaction/ns7:Transaction/ns7:businessTransactionType/ns4:BusinessTransactionType/ns4:businessTransactionType
                return
                    &lt;businessTransactionType&gt;{ data($businessTransactionType) }&lt;/businessTransactionType&gt;
            }
            {
                for $csrMessageType in $invoke1/ns0:transaction/ns7:Transaction/ns7:csrMessageType/ns4:CsrMessageType/ns4:csrMessageType
                return
                    &lt;csrMessageType&gt;{ data($csrMessageType) }&lt;/csrMessageType&gt;
            }
            {
                for $dtTransactionType in $invoke1/ns0:transaction/ns7:Transaction/ns7:dtTransactionType/ns4:DtTransactionType/ns4:dtTransactionType
                return
                    &lt;dtTransactionType&gt;{ data($dtTransactionType) }&lt;/dtTransactionType&gt;
            }
            &lt;executor&gt;
                {
                    for $branchCode in $invoke1/ns0:branchCode/ns3:BranchCode/ns3:branchCode
                    return
                        &lt;branchNumber&gt;{ xs:int( data($branchCode) ) }&lt;/branchNumber&gt;
                }
                &lt;executorID&gt;{ data($header1/ns0:msgHeader/ns0:userId) }&lt;/executorID&gt;
                {
                    for $userFirstName in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns9:User/ns9:userFirstName
                    return
                        &lt;firstName&gt;{ data($userFirstName) }&lt;/firstName&gt;
                }
                {
                    for $userLastName in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns9:User/ns9:userLastName
                    return
                        &lt;lastName&gt;{ data($userLastName) }&lt;/lastName&gt;
                }
                {
                    for $tellerID in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:teller/ns1:Teller/ns1:tellerID
                    return
                        &lt;tellerID&gt;{ data($tellerID) }&lt;/tellerID&gt;
                }
                {
                    for $tillID in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:till/ns1:Till/ns1:tillID
                    return
                        &lt;tillNumber&gt;{ data($tillID) }&lt;/tillNumber&gt;
                }
                {
                    for $userID in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns9:User/ns9:userID
                    return
                        &lt;userID&gt;{ data($userID) }&lt;/userID&gt;
                }
            &lt;/executor&gt;
            &lt;hlbsName&gt;chgTillStatus&lt;/hlbsName&gt;
            {
                for $sessionDate in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:sessionDate
                return
                    &lt;putDownDate&gt;{ data($sessionDate) }&lt;/putDownDate&gt;
            }
            &lt;timestamp&gt;{ data($header1/ns0:msgHeader/ns0:timestamp) }&lt;/timestamp&gt;
            {
                let $AcceptTask := $invoke1/ns0:acceptTask/ns5:AcceptTask
                return
                    &lt;tlAcceptance?&gt;
                        {
                            for $acceptorFirstName in $AcceptTask/ns5:acceptorFirstName
                            return
                                &lt;acceptorFirstName&gt;{ data($acceptorFirstName) }&lt;/acceptorFirstName&gt;
                        }
                        {
                            for $acceptorLastName in $AcceptTask/ns5:acceptorLastName
                            return
                                &lt;acceptorLastName&gt;{ data($acceptorLastName) }&lt;/acceptorLastName&gt;
                        }
                        {
                            for $acceptor in $AcceptTask/ns5:acceptor
                            return
                                &lt;acceptorSkp&gt;{ data($acceptor) }&lt;/acceptorSkp&gt;
                        }
                        {
                            for $AcceptItem in $AcceptTask/ns5:acceptItemList/ns5:AcceptItem
                            return
                                &lt;tlAcceptanceTitleList&gt;
                                    {
                                        for $acceptItemTitle in $AcceptItem/ns5:acceptItemTitle/ns8:AcceptItemTitle/ns8:acceptItemTitle
                                        return
                                            &lt;acceptItemTitle&gt;{ data($acceptItemTitle) }&lt;/acceptItemTitle&gt;
                                    }
                                &lt;/tlAcceptanceTitleList&gt;
                        }
                    &lt;/tlAcceptance&gt;
            }
            {
                for $sessionDate in $invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:sessionDate
                return
                    &lt;transactionDate&gt;{ data($sessionDate) }&lt;/transactionDate&gt;
            }
            &lt;transactionID&gt;{ data($header1/ns0:transHeader/ns0:transId) }&lt;/transactionID&gt;
            {
                for $transactionStatus in $invokeResponse1/ns0:transactionOut/ns7:Transaction/ns7:transactionStatus/ns3:TransactionStatus/ns3:transactionStatus
                return
                    &lt;transactionStatus&gt;{ data($transactionStatus) }&lt;/transactionStatus&gt;
            }
        &lt;/ns6:transactionLogEntry&gt;
};

declare variable $invoke1 as element(ns0:invoke) external;
declare variable $header1 as element(ns0:header) external;
declare variable $invokeResponse1 as element(ns0:invokeResponse) external;

&lt;soap-env:Body&gt;{
xf:chgTillStatusTLRequest($invoke1,
    $header1,
    $invokeResponse1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>