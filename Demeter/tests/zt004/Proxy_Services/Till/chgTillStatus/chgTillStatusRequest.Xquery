<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns9:header" location="chgTillStatus.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns9:invoke" location="chgTillStatus.WSDL" ::)
(:: pragma bea:global-element-return element="ns7:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns4 = "urn:entities.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns0 = "urn:acceptance.entities.be.dcl";
declare namespace ns9 = "urn:be.services.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/chgTillStatus/chgTillStatusRequest_new/";
declare namespace ns6 = "urn:operations.entities.be.dcl";
declare namespace ns7 = "";
declare namespace ns8 = "urn:acceptancedict.dictionaries.be.dcl";

declare function xf:chgTillStatusRequest($header1 as element(ns9:header),
    $invoke1 as element(ns9:invoke))
    as element(ns7:FML32) {
        &lt;ns7:FML32&gt;
            &lt;ns7:TR_ID_OPER&gt;{ data($header1/ns9:transHeader/ns9:transId) }&lt;/ns7:TR_ID_OPER&gt;
            &lt;ns7:TR_DATA_OPER&gt;
                {
                    let $transactionDate  := ($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:sessionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			&lt;/ns7:TR_DATA_OPER&gt;
            &lt;ns7:TR_ODDZ_KASY&gt;{ xs:short( data($invoke1/ns9:branchCode/ns2:BranchCode/ns2:branchCode) ) }&lt;/ns7:TR_ODDZ_KASY&gt;
            &lt;ns7:TR_KASA&gt;{ xs:short( data($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID) ) }&lt;/ns7:TR_KASA&gt;
            &lt;ns7:TR_KASJER&gt;{ xs:short( data($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID) ) }&lt;/ns7:TR_KASJER&gt;
            &lt;ns7:TR_UZYTKOWNIK&gt;{ fn:concat("SKP:",data($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:user/ns4:User/ns4:userID)) }&lt;/ns7:TR_UZYTKOWNIK&gt;
            &lt;ns7:TR_MSG_ID&gt;{ data($header1/ns9:msgHeader/ns9:msgId) }&lt;/ns7:TR_MSG_ID&gt;
            &lt;ns7:TR_TYP_KOM&gt;{ xs:short( data($invoke1/ns9:transaction/ns6:Transaction/ns6:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType) ) }&lt;/ns7:TR_TYP_KOM&gt;
            &lt;ns7:TR_DATA_WALUTY&gt;
                {
                    let $transactionDate  := ($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:sessionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			&lt;/ns7:TR_DATA_WALUTY&gt;
            &lt;ns7:TR_AKCEPTANT_SKP&gt;{ data($invoke1/ns9:acceptTask/ns0:AcceptTask/ns0:acceptor) }&lt;/ns7:TR_AKCEPTANT_SKP&gt;
            &lt;ns7:TR_UZYTKOWNIK_SKP&gt;{ data($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:user/ns4:User/ns4:userID) }&lt;/ns7:TR_UZYTKOWNIK_SKP&gt;
        &lt;/ns7:FML32&gt;
};

declare variable $header1 as element(ns9:header) external;
declare variable $invoke1 as element(ns9:invoke) external;

&lt;soap-env:Body&gt;{
xf:chgTillStatusRequest($header1,
    $invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>