<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Request do wywolania SaveTransfer</con:description>
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="addReconciliationItem.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="addReconciliationItem.WSDL" ::)
(:: pragma bea:global-element-return element="ns5:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns2 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns5 = "";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Till/addReconciliationItem/addReconciliationItemST/";
declare namespace ns6 = "urn:entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:addReconciliationItemST($invoke1 as element(ns0:invoke),
    $header1 as element(ns0:header))
    as element(ns5:FML32) {
        &lt;ns5:FML32&gt;
            &lt;ns5:TR_ID_OPER&gt;{ data($header1/ns0:transHeader/ns0:transId) }&lt;/ns5:TR_ID_OPER&gt;
            &lt;ns5:TR_DATA_OPER&gt;
                {
                    let $transactionDate  := ($header1/ns0:msgHeader/ns0:timestamp)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			&lt;/ns5:TR_DATA_OPER&gt;
            &lt;ns5:TR_ODDZ_KASY&gt;{ xs:short( data($invoke1/ns0:branchCode/ns4:BranchCode/ns4:branchCode) ) }&lt;/ns5:TR_ODDZ_KASY&gt;
            &lt;ns5:TR_KASA&gt;{ xs:short( data($invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID) ) }&lt;/ns5:TR_KASA&gt;
            &lt;ns5:TR_KASJER&gt;{ xs:short( data($invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID) ) }&lt;/ns5:TR_KASJER&gt;
            &lt;ns5:TR_UZYTKOWNIK&gt;{ concat("SKP:",data($invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns6:User/ns6:userID)) }&lt;/ns5:TR_UZYTKOWNIK&gt;
            &lt;ns5:TR_MSG_ID&gt;{ data($header1/ns0:msgHeader/ns0:msgId) }&lt;/ns5:TR_MSG_ID&gt;
            &lt;ns5:TR_FLAGA_AKCEPT&gt;T&lt;/ns5:TR_FLAGA_AKCEPT&gt;
            &lt;ns5:TR_TYP_KOM&gt;407&lt;/ns5:TR_TYP_KOM&gt;
            &lt;ns5:TR_KANAL&gt;0&lt;/ns5:TR_KANAL&gt;
              &lt;ns5:TR_CZAS_OPER&gt;
                {
                    let $transactionTime  := ($header1/ns0:msgHeader/ns0:timestamp)
  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
				&lt;/ns5:TR_CZAS_OPER&gt;
            &lt;ns5:TR_AKCEPTANT_SKP&gt;{ data($invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation[1]/ns1:acceptor/ns6:User/ns6:userID) }&lt;/ns5:TR_AKCEPTANT_SKP&gt;
            &lt;ns5:TR_UZYTKOWNIK_SKP&gt;{ data($invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:user/ns6:User/ns6:userID) }&lt;/ns5:TR_UZYTKOWNIK_SKP&gt;
            {
                for $CurrencyReconciliation in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation
                return
                    &lt;ns5:TR_WALUTA_UZG&gt;{ data($CurrencyReconciliation/ns1:currency/ns4:CurrencyCode/ns4:currencyCode) }&lt;/ns5:TR_WALUTA_UZG&gt;
            }
            {
                for $CurrencyReconciliation in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation
                return
                    &lt;ns5:TR_FLAGA_UZG&gt;{ data($CurrencyReconciliation/ns1:reconciliationStatus/ns2:CurrencyReconcStatus/ns2:reconParameterWrite) }&lt;/ns5:TR_FLAGA_UZG&gt;
            }
            {
                for $CurrencyReconciliation in $invoke1/ns0:userTxnSession/ns3:UserTxnSession/ns3:currencyReconciliationList/ns1:CurrencyReconciliation
                return
                    &lt;ns5:TR_STAN_UZG&gt;{ xs:string( data($CurrencyReconciliation/ns1:reconciliationStatus/ns2:CurrencyReconcStatus/ns2:internalRecMKSStatusWrite) ) }&lt;/ns5:TR_STAN_UZG&gt;
            }
        &lt;/ns5:FML32&gt;
};

declare variable $invoke1 as element(ns0:invoke) external;
declare variable $header1 as element(ns0:header) external;

&lt;soap-env:Body&gt;
{
xf:addReconciliationItemST($invoke1,
    $header1)
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>