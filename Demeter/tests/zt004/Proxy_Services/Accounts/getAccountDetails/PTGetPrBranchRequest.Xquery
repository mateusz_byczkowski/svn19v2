<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.6
 : @since   2010-03-04
 :
 : wersja WSDLa: 15-02-2010 13:53:15
 :
 : $Proxy Services/Accounts/getAccountDetails/PTGetPrBranchRequest.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts_svn/getAccountDetails/PTGetPrBranchRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "";

declare variable $fML321 as element(ns1:FML32) external;
declare variable $body as element() external;

declare function xf:PTGetPrBranchRequest($fML321 as element(ns1:FML32))
    as element(ns0:FML32)
{
	&lt;ns0:FML32&gt;

		&lt;ns0:PT_FIRST_PRODUCT_FEATURE&gt;{
			data($fML321/ns1:PT_FIRST_PRODUCT_FEATURE)
		}&lt;/ns0:PT_FIRST_PRODUCT_FEATURE&gt;

		&lt;ns0:PT_SECOND_PRODUCT_FEATURE&gt;{
			data($fML321/ns1:PT_SECOND_PRODUCT_FEATURE)
		}&lt;/ns0:PT_SECOND_PRODUCT_FEATURE&gt;

		&lt;ns0:PT_THIRD_PRODUCT_FEATURE&gt;{
			data($fML321/ns1:PT_THIRD_PRODUCT_FEATURE)
		}&lt;/ns0:PT_THIRD_PRODUCT_FEATURE&gt;

		&lt;ns0:PT_FOURTH_PRODUCT_FEATURE&gt;{
			data($fML321/ns1:PT_FOURTH_PRODUCT_FEATURE)
		}&lt;/ns0:PT_FOURTH_PRODUCT_FEATURE&gt;

		&lt;ns0:PT_FIFTH_PRODUCT_FEATURE&gt;{
			data($fML321/ns1:PT_FIFTH_PRODUCT_FEATURE)
		}&lt;/ns0:PT_FIFTH_PRODUCT_FEATURE&gt;
		
		(:
		 : 1 - ICBS
		 :)
		&lt;ns0:PT_CODE_PRODUCT_SYSTEM&gt;1&lt;/ns0:PT_CODE_PRODUCT_SYSTEM&gt;
		
		&lt;ns0:PT_SOURCE_PRODUCT_CODE&gt;{
			data($fML321/ns1:PT_SOURCE_PRODUCT_CODE)
		}&lt;/ns0:PT_SOURCE_PRODUCT_CODE&gt;

	&lt;/ns0:FML32&gt;
};

let $fML321 := $body/FML32
return
	xf:PTGetPrBranchRequest($fML321)</con:xquery>
</con:xqueryEntry>