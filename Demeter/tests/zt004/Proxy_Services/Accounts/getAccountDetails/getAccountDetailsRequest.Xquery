<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.6
 : @since   2010-01-28
 :
 : wersja WSDLa: 15-02-2010 13:53:15
 :
 : $Proxy Services/Accounts/getAccountDetails/getAccountDetailsRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getAccountDetails/getAccountDetailsRequest/";
declare namespace ns0 = "urn:operations.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $header1 as element(ns2:header) external;
declare variable $invoke1 as element(ns2:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:getAccountDetailsRequest($header1 as element(ns2:header),
											   $invoke1 as element(ns2:invoke))
    as element(ns1:FML32)
{
	&lt;ns1:FML32&gt;

		(:
		 : dane z nagłówka
		 :)
		&lt;ns1:NF_MSHEAD_MSGID?&gt;{
			data($header1/ns2:msgHeader/ns2:msgId)
		}&lt;/ns1:NF_MSHEAD_MSGID&gt;

		(:
		 : dane z operacji wejściowej
		 :)
        &lt;ns1:NF_TXNAD_ACCOUNTNUMBER?&gt;{
			data($invoke1/ns2:txnAccountData/ns0:TxnAccountData/ns0:accountNumber)
		}&lt;/ns1:NF_TXNAD_ACCOUNTNUMBER&gt;

	&lt;/ns1:FML32&gt;
};

&lt;soap-env:Body&gt;{
	xf:getAccountDetailsRequest($header1, $invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>