<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2010-05-06 PKLI NP1972_3 CR2 Filtrowanie produktów Streamline na podstawie statusu aktywności</con:description>
  <con:xquery>declare namespace wsdl-jv = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace cmf-holding = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace cmf-biz = "http://jv.channel.cu.com.pl/cmf-biz-types";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function zwrocKilka($bdy as element(contract)) as element()*{
                 &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;1&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;,
                 &lt;fml:NF_ACCOUA_NAME1&gt;{substring(data($bdy/client-list/contract-person/person/name/last-name), 1,40)}&lt;/fml:NF_ACCOUA_NAME1&gt;,
                 &lt;fml:NF_ACCOUA_NAME2&gt;{substring(data($bdy/client-list/contract-person/person/name/last-name),41,40)}&lt;/fml:NF_ACCOUA_NAME2&gt;,
                 &lt;fml:NF_ACCOUA_STREET&gt;{data($bdy/client-list/contract-person/address[@kind="4"]/street)}&lt;/fml:NF_ACCOUA_STREET&gt;,
                  if ($bdy/client-list/contract-person/address[@kind="4"]/flat)
                  then &lt;fml:NF_ACCOUA_HOUSEFLATNUMBER&gt;{
                         concat( data($bdy/client-list/contract-person/address[@kind="4"]/home),
                                 concat( "/", data($bdy/client-list/contract-person/address[@kind="4"]/flat)))
                         }&lt;/fml:NF_ACCOUA_HOUSEFLATNUMBER&gt;
                 else &lt;fml:NF_ACCOUA_HOUSEFLATNUMBER&gt;{data($bdy/client-list/contract-person/address[@kind="4"]/home)}&lt;/fml:NF_ACCOUA_HOUSEFLATNUMBER&gt;
                 ,
                 &lt;fml:NF_ACCOUA_CITY&gt;{data($bdy/client-list/contract-person/address[@kind="4"]/city)}&lt;/fml:NF_ACCOUA_CITY&gt;,
                 &lt;fml:NF_ACCOUA_STATECOUNTRY&gt;{data($bdy/client-list/contract-person/address[@kind="4"]/country)}&lt;/fml:NF_ACCOUA_STATECOUNTRY&gt;,
                 &lt;fml:NF_ACCOUA_ZIPCODE&gt;{data($bdy/client-list/contract-person/address[@kind="4"]/postal-code)}&lt;/fml:NF_ACCOUA_ZIPCODE&gt;
};



declare variable $body as element(soap-env:Body) external;
declare variable $policy_id as xs:string external;
declare variable $system_id as xs:string external;

let $bdy:=$body/wsdl-jv:getContractResponse/contract

return

&lt;soap-env:Body&gt;
        &lt;fml:FML32&gt;	
            &lt;NF_PRODUD_SORCEPRODUCTCODE nill="true"/&gt;
	     &lt;NF_ACCOUN_ACCOUNTNUMBER  nil="true" /&gt;
            &lt;NF_TRAACA_LIMITAMOUNT nil="true"/&gt;
            &lt;NF_CURREC_CURRENCYCODE&gt;PLN&lt;/NF_CURREC_CURRENCYCODE&gt;
            &lt;NF_ACCOUN_CURRENTBALANCE?&gt;{data($bdy/amount)}&lt;/NF_ACCOUN_CURRENTBALANCE&gt;
            &lt;NF_ACCOUN_ACCOUNTOPENDATE?&gt;{data($bdy/contract-dates/contract-start)}&lt;/NF_ACCOUN_ACCOUNTOPENDATE&gt;
            &lt;NF_TIMEA_LASTINTERESTPAMD nil="true"/&gt;
            &lt;NF_TIMEA_NEXTRENEWALMATURI nil="true"/&gt;
            &lt;NF_INSURA_ID&gt;{$policy_id}&lt;/NF_INSURA_ID&gt;
            &lt;NF_CUSTOM_CUSTOMERNUMBER?&gt;{data($bdy/client-list/contract-person/@client-code)}&lt;/NF_CUSTOM_CUSTOMERNUMBER&gt;
            &lt;NF_ACCOUN_AVAILABLEBALANCE nil="true"/&gt;
            &lt;NF_HOLD_HOLDAMOUNT nil="true"/&gt;
            &lt;NF_ACCOUN_CREDITPRECENTAGE nil="true"/&gt;

            &lt;NF_CTRL_SYSTEMID&gt;{$system_id}&lt;/NF_CTRL_SYSTEMID&gt;
            &lt;NF_IS_COOWNER&gt;N&lt;/NF_IS_COOWNER&gt;

            &lt;NF_ATTRPG_FIRSTPRODUCTFEAT&gt;{data($bdy/@product-type)}&lt;/NF_ATTRPG_FIRSTPRODUCTFEAT&gt;
            &lt;NF_ATTRPG_SECONDPRODUCTFEA nil="true"/&gt;
            &lt;NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/&gt;
            &lt;NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/&gt;
            &lt;NF_ATTRPG_FIFTHPRODUCTFEAT&gt;PLN&lt;/NF_ATTRPG_FIFTHPRODUCTFEAT&gt;
            &lt;NF_PRODUA_CODEPRODUCTAREA&gt;4&lt;/NF_PRODUA_CODEPRODUCTAREA&gt;
(: v.1.1 start :)
            {
            if (data($bdy/status)="0" or data($bdy/status)="1")
            then &lt;NF_PRODUCT_STAT&gt;T&lt;/NF_PRODUCT_STAT&gt;
            else  &lt;NF_PRODUCT_STAT&gt;N&lt;/NF_PRODUCT_STAT&gt; (: statusy 2, 3, 5, 7, 8 i pozostałe :)
            }
(: v.1.1 end :)
            {
            if (data($bdy/client-list/contract-person/address/@kind)="4")
            then zwrocKilka($bdy)
            else &lt;NF_ACCOUN_ALTERNATIVEADDRE&gt;0&lt;/NF_ACCOUN_ALTERNATIVEADDRE&gt;
            }
            &lt;NF_PAGEC_HASNEXT&gt;0&lt;/NF_PAGEC_HASNEXT&gt;
            &lt;NF_PAGEC_NAVIGATIONKEYVALU&gt;12:000000000000&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;
            &lt;NF_PAGEC_PAGESIZE&gt;1&lt;/NF_PAGEC_PAGESIZE&gt;
        &lt;/fml:FML32&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>