<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace ctx = "http://www.bea.com/wli/sb/context";

declare variable $fault as element(ctx:fault) external;
declare variable $exception as xs:string external;

<soap-env:Fault xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/">
	<faultcode>K00319</faultcode>
	<faultstring>{$exception}: { $fault/ctx:reason/text() }</faultstring>
	<detail>
		<e:ServiceFailException xmlns:e="urn:errors.hlbsentities.be.dcl">
			<e:exceptionItem>
				<e:errorCode1>1</e:errorCode1>
				<e:errorCode2?></e:errorCode2>
				<e:errorDescription>{$exception}: { data($fault/ctx:reason) }</e:errorDescription>
			</e:exceptionItem>
		</e:ServiceFailException>
	</detail>
</soap-env:Fault>]]></con:xquery>
</con:xqueryEntry>