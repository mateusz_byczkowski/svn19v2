<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Obsługa błędów zwracanych przez CIS.</con:description>
  <con:xquery>declare namespace ctx = "http://www.bea.com/wli/sb/context";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace cl = "http://sygnity.pl/functions";
declare namespace faul = "http://bzwbk.com/services/cis/faults/";

declare function cl:cis2NfeCodeConvert($cisCode as xs:string*, $codes as element(cl:codes)) as xs:string {
	let $allCodes :=
		for $code in $codes/cl:code
		where $code/@any-code = $cisCode
		return data($code/@nfe-code)
	return 
		if($allCodes[1] != '') then
			$allCodes[1]
		else
			'K00318'
};

declare variable $fault as element(soap-env:Fault) external;
declare variable $exception as xs:string external;
declare variable $codes as element(cl:codes) external;

&lt;soap-env:Fault xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"&gt;
	&lt;faultcode&gt;{ 
		let $code := 
			if($fault/detail/faul:TimeoutException) then data($fault/detail/faul:TimeoutException/ErrorCode2)
			else if($fault/detail/faul:WrongInputException) then data($fault/detail/faul:WrongInputException/ErrorCode2)
			else if($fault/detail/faul:ServiceFailException) then data($fault/detail/faul:ServiceFailException/ErrorCode2) 
			else if($fault/detail/faul:FMLBufferException) then data($fault/detail/faul:FMLBufferException/ErrorCode2)
			else if($fault/detail/faul:ServiceException) then data($fault/detail/faul:ServiceException/ErrorCode2)
			else if($fault/detail/faul:NoDataException) then data($fault/detail/faul:NoDataException/ErrorCode2)
			else '' 
		return cl:cis2NfeCodeConvert($code, $codes)
	}&lt;/faultcode&gt;
	&lt;faultstring&gt;{$exception}: { data($fault/faultstring) }&lt;/faultstring&gt;
	&lt;detail&gt;
		&lt;e:ServiceFailException xmlns:e="urn:errors.hlbsentities.be.dcl"&gt;
			&lt;e:exceptionItem&gt;
				&lt;e:errorCode1&gt;{ 
					if($fault/detail/faul:TimeoutException) then data($fault/detail/faul:TimeoutException/ErrorCode1)
					else if($fault/detail/faul:WrongInputException) then data($fault/detail/faul:WrongInputException/ErrorCode1)
					else if($fault/detail/faul:ServiceFailException) then data($fault/detail/faul:ServiceFailException/ErrorCode1) 
					else if($fault/detail/faul:FMLBufferException) then data($fault/detail/faul:FMLBufferException/ErrorCode1)
					else if($fault/detail/faul:ServiceException) then data($fault/detail/faul:ServiceException/ErrorCode1) 
                			else if($fault/detail/faul:NoDataException) then data($fault/detail/faul:NoDataException/ErrorCode1)
					else '1' 
				}&lt;/e:errorCode1&gt;
				&lt;e:errorCode2?&gt;{ 
					if($fault/detail/faul:TimeoutException) then data($fault/detail/faul:TimeoutException/ErrorCode2)
					else if($fault/detail/faul:WrongInputException) then data($fault/detail/faul:WrongInputException/ErrorCode2)
					else if($fault/detail/faul:ServiceFailException) then data($fault/detail/faul:ServiceFailException/ErrorCode2) 
					else if($fault/detail/faul:FMLBufferException) then data($fault/detail/faul:FMLBufferException/ErrorCode2)
					else if($fault/detail/faul:ServiceException) then data($fault/detail/faul:ServiceException/ErrorCode2) 
                 			else if($fault/detail/faul:NoDataException) then data($fault/detail/faul:NoDataException/ErrorCode2)
					else '' 
				}&lt;/e:errorCode2&gt;
				&lt;e:errorDescription&gt;{$exception}: { data($fault/faultstring) }&lt;/e:errorDescription&gt;
			&lt;/e:exceptionItem&gt;
		&lt;/e:ServiceFailException&gt;
	&lt;/detail&gt;
&lt;/soap-env:Fault&gt;</con:xquery>
</con:xqueryEntry>