<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/ppl/messages";
declare namespace xf = "http://bzwbk.com/services/ppl/messages";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeTrnTypeProxyRequest($req as element())
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/transactionTypesNo)
					then &lt;fml:E_REC_COUNT&gt;{ data($req/transactionTypesNo) }&lt;/fml:E_REC_COUNT&gt;
					else ()
			}
			{
				if($req/userDescription/microbranch)
					then &lt;fml:E_MICRO_BRANCH&gt;{ data($req/userDescription/microbranch) }&lt;/fml:E_MICRO_BRANCH&gt;
					else ()
			}
			{
				if($req/userDescription/username)
					then &lt;fml:U_USER_NAME&gt;{ data($req/userDescription/username) }&lt;/fml:U_USER_NAME&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapeTrnTypeProxyRequest($body/m:getFirstTransactionTypes) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>