<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCISGetCustomersRequest($req as element(m:CISGetCustomersRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:NumerKlienta)
					then &lt;fml:DC_NUMER_KLIENTA&gt;{ data($req/m:NumerKlienta) }&lt;/fml:DC_NUMER_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:NrDowoduRegon)
					then &lt;fml:DC_NR_DOWODU_REGON&gt;{ data($req/m:NrDowoduRegon) }&lt;/fml:DC_NR_DOWODU_REGON&gt;
					else ()
			}
			{
				if($req/m:NrPesel)
					then &lt;fml:DC_NR_PESEL&gt;{ data($req/m:NrPesel) }&lt;/fml:DC_NR_PESEL&gt;
					else ()
			}
			{
				if($req/m:Nip)
					then &lt;fml:DC_NIP&gt;{ data($req/m:Nip) }&lt;/fml:DC_NIP&gt;
					else ()
			}
			{
				if($req/m:NumerPaszportu)
					then &lt;fml:DC_NUMER_PASZPORTU&gt;{ data($req/m:NumerPaszportu) }&lt;/fml:DC_NUMER_PASZPORTU&gt;
					else ()
			}
			{
				if($req/m:Nik)
					then &lt;fml:CI_NIK&gt;{ data($req/m:Nik) }&lt;/fml:CI_NIK&gt;
					else ()
			}
			{
				if($req/m:Nazwa)
					then &lt;fml:DC_NAZWA&gt;{ data($req/m:Nazwa) }&lt;/fml:DC_NAZWA&gt;
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:DC_IMIE&gt;{ data($req/m:Imie) }&lt;/fml:DC_IMIE&gt;
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:DC_NAZWISKO&gt;{ data($req/m:Nazwisko) }&lt;/fml:DC_NAZWISKO&gt;
					else ()
			}
			{
				if($req/m:MiastoDanePodst)
					then &lt;fml:DC_MIASTO_DANE_PODST&gt;{ data($req/m:MiastoDanePodst) }&lt;/fml:DC_MIASTO_DANE_PODST&gt;
					else ()
			}
			{
				if($req/m:SegmentMarketingowy)
					then &lt;fml:DC_SEGMENT_MARKETINGOWY&gt;{ data($req/m:SegmentMarketingowy) }&lt;/fml:DC_SEGMENT_MARKETINGOWY&gt;
					else ()
			}
			{
				if($req/m:PodsegmentMark)
					then &lt;fml:CI_PODSEGMENT_MARK&gt;{ data($req/m:PodsegmentMark) }&lt;/fml:CI_PODSEGMENT_MARK&gt;
					else ()
			}
			{
				if($req/m:Urzednik1)
					then &lt;fml:DC_URZEDNIK_1&gt;{ data($req/m:Urzednik1) }&lt;/fml:DC_URZEDNIK_1&gt;
					else ()
			}
			{
				if($req/m:Urzednik2)
					then &lt;fml:DC_URZEDNIK_2&gt;{ data($req/m:Urzednik2) }&lt;/fml:DC_URZEDNIK_2&gt;
					else ()
			}
			{
				if($req/m:Urzednik3)
					then &lt;fml:DC_URZEDNIK_3&gt;{ data($req/m:Urzednik3) }&lt;/fml:DC_URZEDNIK_3&gt;
					else ()
			}
			{
				if($req/m:NumerOddzialu)
					then &lt;fml:DC_NUMER_ODDZIALU&gt;{ data($req/m:NumerOddzialu) }&lt;/fml:DC_NUMER_ODDZIALU&gt;
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI&gt;{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI&gt;
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA&gt;{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCISGetCustomersRequest($body/m:CISGetCustomersRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>