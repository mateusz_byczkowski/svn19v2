<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetDictCatResponse($fml as element(fml:FML32))
	as element(m:CRMGetDictCatResponse) {
		&lt;m:CRMGetDictCatResponse&gt;
			{

				let $CI_ID_SLOWNIKA := $fml/fml:CI_ID_SLOWNIKA
				let $CI_NAZWA_SLOWNIKA := $fml/fml:CI_NAZWA_SLOWNIKA
				for $it at $p in $fml/fml:CI_ID_SLOWNIKA
				return
					&lt;m:CRMGetDictCatKategoria&gt;
					{
						if($CI_ID_SLOWNIKA[$p])
							then &lt;m:IdSlownika&gt;{ data($CI_ID_SLOWNIKA[$p]) }&lt;/m:IdSlownika&gt;
						else ()
					}
					{
						if($CI_NAZWA_SLOWNIKA[$p])
							then &lt;m:NazwaSlownika&gt;{ data($CI_NAZWA_SLOWNIKA[$p]) }&lt;/m:NazwaSlownika&gt;
						else ()
					}
					&lt;/m:CRMGetDictCatKategoria&gt;
			}

		&lt;/m:CRMGetDictCatResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetDictCatResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>