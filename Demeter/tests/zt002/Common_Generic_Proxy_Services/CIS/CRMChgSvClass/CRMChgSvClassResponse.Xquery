<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMChgSvClassResponse($fml as element(fml:FML32))
	as element(m:CRMChgSvClassResponse) {
		&lt;m:CRMChgSvClassResponse&gt;
			{

				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $CI_NR_KOM := $fml/fml:CI_NR_KOM
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:CRMChgSvClassRezultat&gt;
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta&gt;{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta&gt;
						else ()
					}
					{
						if($CI_NR_KOM[$p])
							then &lt;m:NrKom&gt;{ data($CI_NR_KOM[$p]) }&lt;/m:NrKom&gt;
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie&gt;{ data($DC_IMIE[$p]) }&lt;/m:Imie&gt;
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko&gt;{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko&gt;
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna&gt;{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna&gt;
						else ()
					}
					&lt;/m:CRMChgSvClassRezultat&gt;
			}

		&lt;/m:CRMChgSvClassResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMChgSvClassResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>