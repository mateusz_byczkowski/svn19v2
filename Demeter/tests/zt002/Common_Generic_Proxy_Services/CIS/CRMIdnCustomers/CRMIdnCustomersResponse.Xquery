<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMIdnCustomersResponse($fml as element(fml:FML32))
	as element(m:CRMIdnCustomersResponse) {
		&lt;m:CRMIdnCustomersResponse&gt;
			{

				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $DC_NUMER_ODDZIALU := $fml/fml:DC_NUMER_ODDZIALU
				let $CI_LISTA_KONTAKTOW := $fml/fml:CI_LISTA_KONTAKTOW
				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
				return
					&lt;m:CRMIdnCustomersKlient&gt;
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta&gt;{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta&gt;
						else ()
					}
					{
						if($DC_NUMER_ODDZIALU[$p])
							then &lt;m:NumerOddzialu&gt;{ data($DC_NUMER_ODDZIALU[$p]) }&lt;/m:NumerOddzialu&gt;
						else ()
					}
					{
						if($CI_LISTA_KONTAKTOW[$p])
							then &lt;m:ListaKontaktow&gt;{ data($CI_LISTA_KONTAKTOW[$p]) }&lt;/m:ListaKontaktow&gt;
						else ()
					}
					&lt;/m:CRMIdnCustomersKlient&gt;
			}

		&lt;/m:CRMIdnCustomersResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMIdnCustomersResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>