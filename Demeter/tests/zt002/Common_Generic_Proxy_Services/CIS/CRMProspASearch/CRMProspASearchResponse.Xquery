<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMProspASearchResponse($fml as element(fml:FML32))
	as element(m:CRMProspASearchResponse) {
		&lt;m:CRMProspASearchResponse&gt;
			{

				let $CI_ID_KLIENTA := $fml/fml:CI_ID_KLIENTA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $DC_NAZWA := $fml/fml:DC_NAZWA
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL
				let $DC_NR_DOWODU_REGON := $fml/fml:DC_NR_DOWODU_REGON
				let $DC_KRAJ := $fml/fml:DC_KRAJ
				let $DC_WOJEWODZTWO := $fml/fml:DC_WOJEWODZTWO
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST
				let $CI_ADRES_DOD := $fml/fml:CI_ADRES_DOD
				let $DC_NR_TELEFONU := $fml/fml:DC_NR_TELEFONU
				let $DC_NR_TELEF_KOMORKOWEGO := $fml/fml:DC_NR_TELEF_KOMORKOWEGO
				let $DC_JEDNOSTKA_KORPORACYJNA := $fml/fml:DC_JEDNOSTKA_KORPORACYJNA
				let $DC_DATA_URODZENIA := $fml/fml:DC_DATA_URODZENIA
                                let $CI_ID_SPOLKI := $fml/fml:CI_ID_SPOLKI

				for $it at $p in $fml/fml:CI_ID_KLIENTA
				return
					&lt;m:CRMProspASearchKlient&gt;
					{
						if($CI_ID_KLIENTA[$p])
							then &lt;m:IdKlienta&gt;{ data($CI_ID_KLIENTA[$p]) }&lt;/m:IdKlienta&gt;
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie&gt;{ data($DC_IMIE[$p]) }&lt;/m:Imie&gt;
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko&gt;{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko&gt;
						else ()
					}
					{
						if($DC_NAZWA[$p])
							then &lt;m:Nazwa&gt;{ data($DC_NAZWA[$p]) }&lt;/m:Nazwa&gt;
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then &lt;m:UdostepGrupa&gt;{ data($CI_UDOSTEP_GRUPA[$p]) }&lt;/m:UdostepGrupa&gt;
						else ()
					}
					{
						if($DC_NR_PESEL[$p])
							then &lt;m:NrPesel&gt;{ data($DC_NR_PESEL[$p]) }&lt;/m:NrPesel&gt;
						else ()
					}
					{
						if($DC_NR_DOWODU_REGON[$p])
							then &lt;m:NrDowoduRegon&gt;{ data($DC_NR_DOWODU_REGON[$p]) }&lt;/m:NrDowoduRegon&gt;
						else ()
					}
					{
						if($DC_KRAJ[$p])
							then &lt;m:Kraj&gt;{ data($DC_KRAJ[$p]) }&lt;/m:Kraj&gt;
						else ()
					}
					{
						if($DC_WOJEWODZTWO[$p])
							then &lt;m:Wojewodztwo&gt;{ data($DC_WOJEWODZTWO[$p]) }&lt;/m:Wojewodztwo&gt;
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_DANE_PODST[$p])
							then &lt;m:KodPocztowyDanePodst&gt;{ data($DC_KOD_POCZTOWY_DANE_PODST[$p]) }&lt;/m:KodPocztowyDanePodst&gt;
						else ()
					}
					{
						if($DC_MIASTO_DANE_PODST[$p])
							then &lt;m:MiastoDanePodst&gt;{ data($DC_MIASTO_DANE_PODST[$p]) }&lt;/m:MiastoDanePodst&gt;
						else ()
					}
					{
						if($DC_ULICA_DANE_PODST[$p])
							then &lt;m:UlicaDanePodst&gt;{ data($DC_ULICA_DANE_PODST[$p]) }&lt;/m:UlicaDanePodst&gt;
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_DANE_PODST[$p])
							then &lt;m:NrPosesLokaluDanePodst&gt;{ data($DC_NR_POSES_LOKALU_DANE_PODST[$p]) }&lt;/m:NrPosesLokaluDanePodst&gt;
						else ()
					}
					{
						if($CI_ADRES_DOD[$p])
							then &lt;m:AdresDod&gt;{ data($CI_ADRES_DOD[$p]) }&lt;/m:AdresDod&gt;
						else ()
					}
					{
						if($DC_NR_TELEFONU[$p])
							then &lt;m:NrTelefonu&gt;{ data($DC_NR_TELEFONU[$p]) }&lt;/m:NrTelefonu&gt;
						else ()
					}
					{
						if($DC_NR_TELEF_KOMORKOWEGO[$p])
							then &lt;m:NrTelefKomorkowego&gt;{ data($DC_NR_TELEF_KOMORKOWEGO[$p]) }&lt;/m:NrTelefKomorkowego&gt;
						else ()
					}
					{
						if($DC_JEDNOSTKA_KORPORACYJNA[$p])
							then &lt;m:JednostkaKorporacyjna&gt;{ data($DC_JEDNOSTKA_KORPORACYJNA[$p]) }&lt;/m:JednostkaKorporacyjna&gt;
						else ()
					}
					{
						if($DC_DATA_URODZENIA[$p])
							then &lt;m:DataUrodzenia&gt;{ data($DC_DATA_URODZENIA[$p]) }&lt;/m:DataUrodzenia&gt;
						else ()
					}
                                        {
                                                if($CI_ID_SPOLKI[$p])
                                                        then &lt;m:IdSpolki&gt;{ data($CI_ID_SPOLKI[$p]) }&lt;/m:IdSpolki&gt;
                                                else ()
                                        }
					&lt;/m:CRMProspASearchKlient&gt;
			}

		&lt;/m:CRMProspASearchResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMProspASearchResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>