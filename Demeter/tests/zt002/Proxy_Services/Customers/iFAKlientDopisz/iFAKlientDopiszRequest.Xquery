<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/arka24/messages/";
declare namespace xf = "http://bzwbk.com/services/arka24/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbARKA24iFAKlientDopiszRequest($req as element(m:bARKA24iFAKlientDopiszRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:Pesel)
					then &lt;fml:FA_PESEL&gt;{ data($req/m:Pesel) }&lt;/fml:FA_PESEL&gt;
					else ()
			}
			{
				if($req/m:Nip)
					then &lt;fml:FA_NIP&gt;{ data($req/m:Nip) }&lt;/fml:FA_NIP&gt;
					else ()
			}
			{
				if($req/m:DUrodzenia)
					then &lt;fml:B_D_URODZENIA&gt;{ data($req/m:DUrodzenia) }&lt;/fml:B_D_URODZENIA&gt;
					else ()
			}
			{
				if($req/m:NrDokumentu)
					then &lt;fml:FA_NR_DOKUMENTU&gt;{ data($req/m:NrDokumentu) }&lt;/fml:FA_NR_DOKUMENTU&gt;
					else ()
			}
			{
				if($req/m:NrDowodu)
					then &lt;fml:FA_NR_DOWODU&gt;{ data($req/m:NrDowodu) }&lt;/fml:FA_NR_DOWODU&gt;
					else ()
			}
			{
				if($req/m:Paszport)
					then &lt;fml:FA_PASZPORT&gt;{ data($req/m:Paszport) }&lt;/fml:FA_PASZPORT&gt;
					else ()
			}
			{
				if($req/m:UlZam)
					then &lt;fml:FA_UL_ZAM&gt;{ data($req/m:UlZam) }&lt;/fml:FA_UL_ZAM&gt;
					else ()
			}
			{
				if($req/m:NrDomuZam)
					then &lt;fml:FA_NR_DOMU_ZAM&gt;{ data($req/m:NrDomuZam) }&lt;/fml:FA_NR_DOMU_ZAM&gt;
					else ()
			}
			{
				if($req/m:KodPocztZam)
					then &lt;fml:FA_KOD_POCZT_ZAM&gt;{ data($req/m:KodPocztZam) }&lt;/fml:FA_KOD_POCZT_ZAM&gt;
					else ()
			}
			{
				if($req/m:MZam)
					then &lt;fml:FA_M_ZAM&gt;{ data($req/m:MZam) }&lt;/fml:FA_M_ZAM&gt;
					else ()
			}
			{
				if($req/m:KrajZam)
					then &lt;fml:FA_KRAJ_ZAM&gt;{ data($req/m:KrajZam) }&lt;/fml:FA_KRAJ_ZAM&gt;
					else ()
			}
			{
				if($req/m:UlKoresp)
					then &lt;fml:FA_UL_KORESP&gt;{ data($req/m:UlKoresp) }&lt;/fml:FA_UL_KORESP&gt;
					else ()
			}
			{
				if($req/m:NrDomuKoresp)
					then &lt;fml:FA_NR_DOMU_KORESP&gt;{ data($req/m:NrDomuKoresp) }&lt;/fml:FA_NR_DOMU_KORESP&gt;
					else ()
			}
			{
				if($req/m:NrLokaluKoresp)
					then &lt;fml:FA_NR_LOKALU_KORESP&gt;{ data($req/m:NrLokaluKoresp) }&lt;/fml:FA_NR_LOKALU_KORESP&gt;
					else ()
			}
			{
				if($req/m:KodPocztKoresp)
					then &lt;fml:FA_KOD_POCZT_KORESP&gt;{ data($req/m:KodPocztKoresp) }&lt;/fml:FA_KOD_POCZT_KORESP&gt;
					else ()
			}
			{
				if($req/m:MKoresp)
					then &lt;fml:FA_M_KORESP&gt;{ data($req/m:MKoresp) }&lt;/fml:FA_M_KORESP&gt;
					else ()
			}
			{
				if($req/m:StPrawny)
					then &lt;fml:FA_ST_PRAWNY&gt;{ data($req/m:StPrawny) }&lt;/fml:FA_ST_PRAWNY&gt;
					else ()
			}
			{
				if($req/m:StDewizowy)
					then &lt;fml:FA_ST_DEWIZOWY&gt;{ data($req/m:StDewizowy) }&lt;/fml:FA_ST_DEWIZOWY&gt;
					else ()
			}
			{
				if($req/m:Obywatelstwo)
					then &lt;fml:FA_OBYWATELSTWO&gt;{ data($req/m:Obywatelstwo) }&lt;/fml:FA_OBYWATELSTWO&gt;
					else ()
			}
			{
				if($req/m:Imie)
					then &lt;fml:FA_IMIE&gt;{ data($req/m:Imie) }&lt;/fml:FA_IMIE&gt;
					else ()
			}
			{
				if($req/m:ImieOjca)
					then &lt;fml:FA_IMIE_OJCA&gt;{ data($req/m:ImieOjca) }&lt;/fml:FA_IMIE_OJCA&gt;
					else ()
			}
			{
				if($req/m:ImieMatki)
					then &lt;fml:FA_IMIE_MATKI&gt;{ data($req/m:ImieMatki) }&lt;/fml:FA_IMIE_MATKI&gt;
					else ()
			}
			{
				if($req/m:MUrodzenia)
					then &lt;fml:FA_M_URODZENIA&gt;{ data($req/m:MUrodzenia) }&lt;/fml:FA_M_URODZENIA&gt;
					else ()
			}
			{
				if($req/m:TelKontakt)
					then &lt;fml:FA_TEL_KONTAKT&gt;{ data($req/m:TelKontakt) }&lt;/fml:FA_TEL_KONTAKT&gt;
					else ()
			}
			{
				if($req/m:TelefonKom)
					then &lt;fml:FA_TELEFON_KOM&gt;{ data($req/m:TelefonKom) }&lt;/fml:FA_TELEFON_KOM&gt;
					else ()
			}
			{
				if($req/m:EMail)
					then &lt;fml:FA_E_MAIL&gt;{ data($req/m:EMail) }&lt;/fml:FA_E_MAIL&gt;
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:FA_OPCJA&gt;{ data($req/m:Opcja) }&lt;/fml:FA_OPCJA&gt;
					else ()
			}
			{
				if($req/m:NrOddz)
					then &lt;fml:FA_NR_ODDZ&gt;{ data($req/m:NrOddz) }&lt;/fml:FA_NR_ODDZ&gt;
					else ()
			}
			{
				if($req/m:NrKrs)
					then &lt;fml:FA_NR_KRS&gt;{ data($req/m:NrKrs) }&lt;/fml:FA_NR_KRS&gt;
					else ()
			}
			{
				if($req/m:NrRejestruEwid)
					then &lt;fml:FA_NR_REJESTRU_EWID&gt;{ data($req/m:NrRejestruEwid) }&lt;/fml:FA_NR_REJESTRU_EWID&gt;
					else ()
			}
			{
				if($req/m:NrLokaluZam)
					then &lt;fml:FA_NR_LOKALU_ZAM&gt;{ data($req/m:NrLokaluZam) }&lt;/fml:FA_NR_LOKALU_ZAM&gt;
					else ()
			}
			{
				if($req/m:KrajKoresp)
					then &lt;fml:FA_KRAJ_KORESP&gt;{ data($req/m:KrajKoresp) }&lt;/fml:FA_KRAJ_KORESP&gt;
					else ()
			}
			{
				if($req/m:FormaOrg)
					then &lt;fml:FA_FORMA_ORG&gt;{ data($req/m:FormaOrg) }&lt;/fml:FA_FORMA_ORG&gt;
					else ()
			}
			{
				if($req/m:DrugieImie)
					then &lt;fml:FA_DRUGIE_IMIE&gt;{ data($req/m:DrugieImie) }&lt;/fml:FA_DRUGIE_IMIE&gt;
					else ()
			}
			{
				if($req/m:Nazwisko)
					then &lt;fml:FA_NAZWISKO&gt;{ data($req/m:Nazwisko) }&lt;/fml:FA_NAZWISKO&gt;
					else ()
			}
			{
				if($req/m:Klient)
					then &lt;fml:FA_KLIENT&gt;{ data($req/m:Klient) }&lt;/fml:FA_KLIENT&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapbARKA24iFAKlientDopiszRequest($body/m:bARKA24iFAKlientDopiszRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>