<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$fML321" element="ns2:FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-return element="ns0:invokeResponse" location="chgBranchStatus.WSDL" ::)

declare namespace ns2 = "";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns5 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Branches/chgBranchStatus/chgBranchStatusResponse/";
declare namespace ns7 = "urn:operations.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:chgBranchStatusResponse($fML321 as element(ns2:FML32))
    as element(ns0:invokeResponse) {
        &lt;ns0:invokeResponse&gt;
            &lt;ns0:response&gt;
                &lt;ns5:ResponseMessage?&gt;
                    &lt;ns5:result?&gt;
                               						true
                               					&lt;/ns5:result&gt;
                &lt;/ns5:ResponseMessage&gt;
            &lt;/ns0:response&gt;
            &lt;ns0:branchCurrentStatusOut&gt;
                &lt;ns1:BranchCurrentStatus?&gt;
                
                					{
					let $dateTime := data($fML321/ns2:TR_CZAS_ODPOWIEDZI) 
					return
						if ($dateTime) then 
		                    &lt;ns1:lastChangeDate?&gt;{  
								fn:concat(fn:substring($dateTime, 1, 10),
										  'T',
										  fn:substring($dateTime, 12, 15))
							}&lt;/ns1:lastChangeDate&gt;
						else
							()
				}
                
                &lt;/ns1:BranchCurrentStatus&gt;
            &lt;/ns0:branchCurrentStatusOut&gt;
            &lt;ns0:transactionOut&gt;
                &lt;ns7:Transaction?&gt;
                    &lt;ns7:transactionStatus?&gt;
                        &lt;ns3:TransactionStatus?&gt;
                            &lt;ns3:transactionStatus?&gt;{ data($fML321/ns2:TR_STATUS) }&lt;/ns3:transactionStatus&gt;
                        &lt;/ns3:TransactionStatus&gt;
                    &lt;/ns7:transactionStatus&gt;
                &lt;/ns7:Transaction&gt;
            &lt;/ns0:transactionOut&gt;
            &lt;ns0:backendResponse&gt;
                &lt;ns7:BackendResponse?&gt;
    
    				{
					let $transactionDate := data($fML321/ns2:TR_DATA_KSIEG)
					return
						if ($transactionDate) then 
							&lt;ns7:icbsDate&gt;{
								fn:concat(fn:substring($transactionDate, 7, 4),
										  '-',
										  fn:substring($transactionDate, 4, 2),
										  '-',
										  fn:substring($transactionDate, 1, 2))
							}&lt;/ns7:icbsDate&gt;
						else
							()
				}
                    &lt;ns7:icbsSessionNumber?&gt;{ xs:string( data($fML321/ns2:TR_TXN_SESJA) ) }&lt;/ns7:icbsSessionNumber&gt;
                    &lt;ns7:transactionRefNumber?&gt;{ data($fML321/ns2:TR_ID_REF) }&lt;/ns7:transactionRefNumber&gt;
					{
					let $dateTime := data($fML321/ns2:TR_CZAS_ODPOWIEDZI) 
					return
						if ($dateTime) then 
		                    &lt;ns7:dateTime&gt;{  
								fn:concat(fn:substring($dateTime, 1, 10),
										  'T',
										  fn:substring($dateTime, 12, 15))
							}&lt;/ns7:dateTime&gt;
						else
							()
				}


				&lt;ns4:beErrorCodeList?&gt;{
					for $FML320 in ($fML321/ns1:TR_KOD_BLEDU_1
									 union $fML321/ns1:TR_KOD_BLEDU_2
									 union $fML321/ns1:TR_KOD_BLEDU_3
									 union $fML321/ns1:TR_KOD_BLEDU_4
									 union $fML321/ns1:TR_KOD_BLEDU_5)  
					return
						let $errorCode := data($FML320)
						return
							if ($errorCode ne ''
								and $errorCode ne '000') then
								&lt;ns4:BeErrorCode&gt;
									&lt;ns4:errorCode&gt;
										&lt;ns3:BackendErrorCode&gt;
											&lt;ns3:errorCode&gt;{
												data($FML320)
											}&lt;/ns3:errorCode&gt;
										&lt;/ns3:BackendErrorCode&gt;
									&lt;/ns4:errorCode&gt;
								&lt;/ns4:BeErrorCode&gt;
							else
								()
				}&lt;/ns4:beErrorCodeList&gt;            
                &lt;/ns7:BackendResponse&gt;
            &lt;/ns0:backendResponse&gt;
        &lt;/ns0:invokeResponse&gt;
};

declare variable $fML321 as element(ns2:FML32) external;

&lt;soap-env:Body&gt;{
xf:chgBranchStatusResponse($fML321)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>