<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="postCloseAccount.wsdl" ::)
(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="postCloseAccount.wsdl" ::)
(:: pragma bea:global-element-return element="ns7:FML32" location="../../Operations/savetransfer/savetransferIN.xsd" ::)

declare namespace ns9 = "urn:operations.entities.be.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns6 = "urn:acceptance.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "";
declare namespace ns10 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns8 = "urn:productstree.entities.be.dcl";
declare namespace ns11 = "urn:entities.be.dcl";
declare namespace ns2 = "urn:transactionbasketdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns1 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:accounts.entities.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/postCloseAccount/postCloseAccountRequest/";

declare function xf:postCloseAccountRequest($invoke1 as element(ns0:invoke),
    $header1 as element(ns0:header))
    as element(ns7:FML32) {
        &lt;ns7:FML32&gt;
            &lt;ns7:TR_ID_OPER?&gt;{
             data($header1/ns0:transHeader/ns0:transId) 
             }&lt;/ns7:TR_ID_OPER&gt;
             
            &lt;ns7:TR_DATA_OPER?&gt;
                {
                    let $transactionDate  := ($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionDate)  
                    return
                        (fn:concat(
                        fn:substring(data($transactionDate), 9, 2),
                        '-',
                        fn:substring(data($transactionDate ), 6, 2),
                        '-',
                        fn:substring(data($transactionDate ), 1, 4)
                        ))
                }
			&lt;/ns7:TR_DATA_OPER&gt;
            
            &lt;ns7:TR_ODDZ_KASY?&gt;{ 
            xs:short( data($invoke1/ns0:branchCode/ns4:BranchCode/ns4:branchCode) ) 
            }&lt;/ns7:TR_ODDZ_KASY&gt;
            
            &lt;ns7:TR_KASA?&gt;{ 
            xs:short( data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:till/ns1:Till/ns1:tillID)) 
            }&lt;/ns7:TR_KASA&gt;
            
            &lt;ns7:TR_KASJER?&gt;{ 
            xs:short( data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:teller/ns1:Teller/ns1:tellerID)) 
            }&lt;/ns7:TR_KASJER&gt;
            
            &lt;ns7:TR_UZYTKOWNIK?&gt;{ 
            fn:concat("SKP:",data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns11:User/ns11:userID)) 
            }&lt;/ns7:TR_UZYTKOWNIK&gt;
            
            &lt;ns7:TR_ID_GR_OPER?&gt;{
             data($invoke1/ns0:transaction/ns9:Transaction/ns9:cashTransactionBasketID)
             }&lt;/ns7:TR_ID_GR_OPER&gt;
             
            &lt;ns7:TR_MSG_ID?&gt;{ 
            data($header1/ns0:msgHeader/ns0:msgId) 
            }&lt;/ns7:TR_MSG_ID&gt;
            
            &lt;ns7:TR_FLAGA_AKCEPT?&gt;
                {
                    if (fn:boolean(data($invoke1/ns0:transaction/ns9:Transaction/ns9:acceeptanceForBe/ns9:AcceptanceForBE/ns9:flag) ) = fn:boolean("true")) then
                        (data($invoke1/ns0:transaction/ns9:Transaction/ns9:acceeptanceForBe/ns9:AcceptanceForBE/ns9:flag) )
                    else 
                        'T'
                }
			&lt;/ns7:TR_FLAGA_AKCEPT&gt;
			
            &lt;ns7:TR_AKCEPTANT?&gt;{ 
            if (data($invoke1/ns0:transaction/ns9:Transaction/ns9:acceptTask/ns6:AcceptTask/ns6:acceptor))
            then
            fn:concat("SKP:",data($invoke1/ns0:transaction/ns9:Transaction/ns9:acceptTask/ns6:AcceptTask/ns6:acceptor))
            else () 
            }&lt;/ns7:TR_AKCEPTANT&gt;
            
            &lt;ns7:TR_TYP_KOM?&gt;{ 
            xs:short( data($invoke1/ns0:transaction/ns9:Transaction/ns9:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType)) 
            }&lt;/ns7:TR_TYP_KOM&gt;
            
            &lt;ns7:TR_KANAL?&gt;{ 
            xs:short( data($invoke1/ns0:transaction/ns9:Transaction/ns9:extendedCSRMessageType/ns5:ExtendedCSRMessageType/ns5:extendedCSRMessageType) ) 
            }&lt;/ns7:TR_KANAL&gt;
            
             &lt;ns7:TR_CZAS_OPER?&gt;
                {
                    let $transactionTime  := ($header1/ns0:msgHeader/ns0:timestamp)
  
                    return
                        (fn:replace(
                        fn:substring(data($transactionTime), 12, 8)
                        ,':',''))
                }
				&lt;/ns7:TR_CZAS_OPER&gt;
    
             &lt;ns7:TR_RACH_NAD?&gt;{ 
             data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn/ns9:accountNumber)  
             }&lt;/ns7:TR_RACH_NAD&gt;
             
             &lt;ns7:TR_KWOTA_NAD?&gt;{ 
             data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn/ns9:amountWn)  
             }&lt;/ns7:TR_KWOTA_NAD&gt;
             
             &lt;ns7:TR_WALUTA_NAD?&gt;{  
             data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn/ns9:currencyCode/ns4:CurrencyCode/ns4:currencyCode) 
             }&lt;/ns7:TR_WALUTA_NAD&gt;
             
             &lt;ns7:TR_KURS_NAD?&gt;{  
             data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn/ns9:rate)
            }&lt;/ns7:TR_KURS_NAD&gt;
            
             &lt;ns7:TR_NAZWA_NAD?&gt;{ 
             fn-bea:trim( concat(data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn/ns9:name)," ",data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn/ns9:nameSecond)))  
             }&lt;/ns7:TR_NAZWA_NAD&gt;
             
             &lt;ns7:TR_MIEJSCOWOSC_NAD?&gt;{ 
             data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn/ns9:city) 
             }&lt;/ns7:TR_MIEJSCOWOSC_NAD&gt;
             
              &lt;ns7:TR_ULICA_NAD?&gt;{ 
              data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn/ns9:address)  
              }&lt;/ns7:TR_ULICA_NAD&gt;
              
              &lt;ns7:TR_KOD_POCZT_NAD?&gt;{  
              data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn/ns9:zipCode)  
              }&lt;/ns7:TR_KOD_POCZT_NAD&gt;
              
              &lt;ns7:TR_ROWN_PLN_NAD?&gt;{ 
              data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionWn/ns9:TransactionWn/ns9:amountWnEquiv) 
              }&lt;/ns7:TR_ROWN_PLN_NAD&gt;
              
              &lt;ns7:TR_RACH_ADR?&gt;{ 
              data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa/ns9:accountNumber) 
              }&lt;/ns7:TR_RACH_ADR&gt;
              
              &lt;ns7:TR_KWOTA_ADR?&gt;{
               data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa/ns9:amountWn)
               }&lt;/ns7:TR_KWOTA_ADR&gt;
               
              &lt;ns7:TR_WALUTA_ADR?&gt;{
               data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa/ns9:currencyCode/ns4:CurrencyCode/ns4:currencyCode) 
               }&lt;/ns7:TR_WALUTA_ADR&gt;
               
              &lt;ns7:TR_KURS_ADR?&gt;{ 
              data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa/ns9:rate)
              }&lt;/ns7:TR_KURS_ADR&gt;
              
              &lt;ns7:TR_NAZWA_ADR?&gt;{ 
              fn-bea:trim(concat( data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa/ns9:name)," ",data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa/ns9:nameSecond))) 
              }&lt;/ns7:TR_NAZWA_ADR&gt;
              
              &lt;ns7:TR_MIEJSCOWOSC_ADR?&gt;{  
              data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa/ns9:city) 
              }&lt;/ns7:TR_MIEJSCOWOSC_ADR&gt;
              
              &lt;ns7:TR_ULICA_ADR?&gt;{ 
              data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa/ns9:address)  
              }&lt;/ns7:TR_ULICA_ADR&gt;
              
              &lt;ns7:TR_KOD_POCZT_ADR?&gt;{  
              data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa/ns9:zipCode)  
              }&lt;/ns7:TR_KOD_POCZT_ADR&gt;
              
              &lt;ns7:TR_ROWN_PLN_ADR?&gt;{ 
               data($invoke1/ns0:transaction/ns9:Transaction/ns9:transactionMa/ns9:TransactionMa/ns9:amountMaEquiv)  
               }&lt;/ns7:TR_ROWN_PLN_ADR&gt;
               
              &lt;ns7:TR_TYTUL?&gt;{ 
              data($invoke1/ns0:transaction/ns9:Transaction/ns9:title) 
              }&lt;/ns7:TR_TYTUL&gt;
{
    		for $Disposer in ($invoke1/ns0:transaction/ns9:Transaction/ns9:disposerList/ns9:Disposer)
    		return
    		(
              &lt;ns7:TR_DYSP_CIF?&gt;{ 
              data($Disposer/ns9:cif)  
              }&lt;/ns7:TR_DYSP_CIF&gt;,
              
              &lt;ns7:TR_DYSP_NAZWA?&gt;{ 
              fn-bea:trim( concat($Disposer/ns9:firstName , " ", $Disposer/ns9:lastName)) 
              }&lt;/ns7:TR_DYSP_NAZWA&gt;,
              
			  &lt;ns7:TR_DYSP_OBYWATELSTWO?&gt;{  
			  data($Disposer/ns9:citizenship/ns4:CitizenshipCode/ns4:citizenshipCode)  
			  }&lt;/ns7:TR_DYSP_OBYWATELSTWO&gt;,
			  
              &lt;ns7:TR_DYSP_KRAJ?&gt;{  
              data($Disposer/ns9:countryCode/ns4:CountryCode/ns4:countryCode)  
              }&lt;/ns7:TR_DYSP_KRAJ&gt;,
              
              &lt;ns7:TR_DYSP_MIEJSCOWOSC?&gt;{ 
              data($Disposer/ns9:city)  
              }&lt;/ns7:TR_DYSP_MIEJSCOWOSC&gt;,
              
              &lt;ns7:TR_DYSP_KOD_POCZT?&gt;{ 
              data($Disposer/ns9:zipCode) 
              }&lt;/ns7:TR_DYSP_KOD_POCZT&gt;,
              
              &lt;ns7:TR_DYSP_ULICA?&gt;{ 
              data($Disposer/ns9:address)  
              }&lt;/ns7:TR_DYSP_ULICA&gt;,
              
              &lt;ns7:TR_DYSP_PESEL?&gt;{ 
              data($Disposer/ns9:pesel)  
              }&lt;/ns7:TR_DYSP_PESEL&gt;,
              
              &lt;ns7:TR_DYSP_NR_DOWOD?&gt;{ 
              data($Disposer/ns9:documentNumber)  
              }&lt;/ns7:TR_DYSP_NR_DOWOD&gt;
              
            )
 }
            &lt;ns7:TR_AKCEPTANT_SKP?&gt;{ 
            data($invoke1/ns0:transaction/ns9:Transaction/ns9:acceptTask/ns6:AcceptTask/ns6:acceptor) 
            }&lt;/ns7:TR_AKCEPTANT_SKP&gt;
            
            &lt;ns7:TR_UZYTKOWNIK_SKP?&gt;{ 
            data($invoke1/ns0:userTxnSession/ns1:UserTxnSession/ns1:user/ns11:User/ns11:userID) 
            }&lt;/ns7:TR_UZYTKOWNIK_SKP&gt;
            
        &lt;/ns7:FML32&gt;
};

declare variable $invoke1 as element(ns0:invoke) external;
declare variable $header1 as element(ns0:header) external;


&lt;soap-env:Body&gt;{
xf:postCloseAccountRequest($invoke1,
    $header1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>