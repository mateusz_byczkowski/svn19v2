<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$fML32OUT1" element="ns1:FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-return element="ns0:invokeResponse" location="postCloseAccount.wsdl" ::)

(:~
 :
 : @author  Grzegorz Strawa
 : @version 1.0
 : @since   2010-03-22
 :
 : wersja WSDLa: 21-01-2010 09:35:56
 :
 : $Proxy Services/Accounts/postCloseAccount/postCloseAccountResponse.xq$
 :
 :) 

 
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "";
declare namespace ns4 = "urn:operations.entities.be.dcl";
declare namespace ns3 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/postCloseAccount/postCloseAccountResponse/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/"; 

declare function xf:postCloseAccountResponse($fML32OUT1 as element(ns1:FML32))
    as element(ns0:invokeResponse) {
        &lt;ns0:invokeResponse&gt;
            &lt;ns0:transactionOut&gt;
                &lt;ns4:Transaction&gt;
                    &lt;ns4:transactionStatus&gt;
                        &lt;ns2:TransactionStatus&gt;
                            &lt;ns2:transactionStatus&gt;{ data($fML32OUT1/TR_STATUS) }&lt;/ns2:transactionStatus&gt;
                        &lt;/ns2:TransactionStatus&gt;
                    &lt;/ns4:transactionStatus&gt;
                &lt;/ns4:Transaction&gt;
            &lt;/ns0:transactionOut&gt;
            &lt;ns0:backendResponse&gt;
                &lt;ns4:BackendResponse&gt;
                 {
                if (data($fML32OUT1/ns1:TR_DATA_KSIEG) ) then 
                   
                    &lt;ns4:icbsDate&gt;{  
                        let $transactionDate := $fML32OUT1/ns1:TR_DATA_KSIEG
					return
						fn:concat(
							fn:substring(data($transactionDate ), 7, 4),
							'-',
							fn:substring(data($transactionDate ), 4, 2),
							'-',
							fn:substring(data($transactionDate ), 1, 2)
							)
						}&lt;/ns4:icbsDate&gt;
				else ()
				}
				
                {
                if (data($fML32OUT1/ns1:TR_CZAS_ODPOWIEDZI) ) then 
                   
                    &lt;ns4:dateTime&gt;{  
                        let $czasOdpowiedzi := $fML32OUT1/ns1:TR_CZAS_ODPOWIEDZI
					return
						fn:concat(
							fn:substring(data($czasOdpowiedzi ), 1, 10),
							'T',
							fn:substring(data($czasOdpowiedzi ), 12, 15)
							)
						}&lt;/ns4:dateTime&gt;
				else ()
				}
                                   {               
                   if (data($fML32OUT1/ns1:TR_TXN_SESJA) ) then 
						&lt;ns4:icbsSessionNumber&gt;{ xs:string( data($fML32OUT1/ns1:TR_TXN_SESJA) ) }&lt;/ns4:icbsSessionNumber&gt; 
                    else ()
                   }
                
                   {                
                    if (data($fML32OUT1/ns1:TR_TXN_NR)) then 
						&lt;ns4:psTransactionNumber&gt;{ xs:string( data($fML32OUT1/ns1:TR_TXN_NR) ) }&lt;/ns4:psTransactionNumber&gt; 
                    else ()
                   }
                
                   {                
                    if (data($fML32OUT1/ns1:TR_ID_REF )) then 
						&lt;ns4:transactionRefNumber&gt;{ xs:string( data($fML32OUT1/ns1:TR_ID_REF) ) }&lt;/ns4:transactionRefNumber&gt; 
                    else ()
                   }
                  
                  
                 
                                        &lt;ns4:beErrorCodeList&gt;
                        {
                            for $FML320  in ($fML32OUT1/ns1:TR_KOD_BLEDU_1 union $fML32OUT1/ns1:TR_KOD_BLEDU_2 union $fML32OUT1/ns1:TR_KOD_BLEDU_3 union $fML32OUT1/ns1:TR_KOD_BLEDU_4 union $fML32OUT1/ns1:TR_KOD_BLEDU_5)  
                            return
                                if ((fn:boolean($FML320!="") and fn:boolean($FML320!="000"))) then
                                    (&lt;ns4:BeErrorCode&gt;
                                    &lt;ns4:errorCode&gt;
                                    &lt;ns3:BackendErrorCode&gt;
                                    &lt;ns3:errorCode&gt;{ data($FML320) }&lt;/ns3:errorCode&gt;
                                    &lt;/ns3:BackendErrorCode&gt;
                                    &lt;/ns4:errorCode&gt;
                                    &lt;/ns4:BeErrorCode&gt;)
                                else 
                                    ()
                        }
					&lt;/ns4:beErrorCodeList&gt;
                &lt;/ns4:BackendResponse&gt;
            &lt;/ns0:backendResponse&gt;
        &lt;/ns0:invokeResponse&gt;
};

declare variable $fML32OUT1 as element(ns1:FML32) external;

&lt;soap-env:Body&gt;{
xf:postCloseAccountResponse($fML32OUT1)
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>