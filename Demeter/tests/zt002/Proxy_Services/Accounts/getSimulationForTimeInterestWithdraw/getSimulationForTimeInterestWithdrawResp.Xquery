<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.0
 : @since   2010-02-01
 :
 : wersja WSDLa: 03-12-2009 17:14:48
 :
 : $Proxy Services/Account/getSimulationForTimeInterestWithdraw/getSimulationForTimeInterestWithdrawRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/getSimulationForTimeInterestWithdraw/getSimulationForTimeInterestWithdrawResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:getSimulationForTimeInterestWithdrawResponse($fML321 as element(ns1:FML32))
    as element(ns2:invokeResponse)
{
    &lt;ns2:invokeResponse&gt;
        &lt;ns2:simulationForTimeAccount&gt;
            &lt;ns0:SimulationForTimeAccount?&gt;
            
                &lt;ns0:balance?&gt;{
					data($fML321/ns1:NF_SIMFTA_BALANCE)
				}&lt;/ns0:balance&gt;
				
                &lt;ns0:interestForPay?&gt;{
					data($fML321/ns1:NF_SIMFTA_INTERESTFORPAY)
				}&lt;/ns0:interestForPay&gt;
				
                &lt;ns0:interestCapitalized?&gt;{
					data($fML321/ns1:NF_SIMFTA_INTERESTCAPITALI)
				}&lt;/ns0:interestCapitalized&gt;
				
                &lt;ns0:taxPayed?&gt;{
					data($fML321/ns1:NF_SIMFTA_TAXPAYED)
				}&lt;/ns0:taxPayed&gt;
				
            &lt;/ns0:SimulationForTimeAccount&gt;
        &lt;/ns2:simulationForTimeAccount&gt;
    &lt;/ns2:invokeResponse&gt;
};

&lt;soap-env:Body&gt;{
	xf:getSimulationForTimeInterestWithdrawResponse($fML321)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>