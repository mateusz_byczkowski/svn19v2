<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/icbsmessages/";
declare namespace xf = "http://bzwbk.com/services/icbsmappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapbICBSGetAccountResponse($fml as element(fml:FML32))
	as element(m:bICBSGetAccountResponse) {
		&lt;m:bICBSGetAccountResponse&gt;
			{
				if($fml/fml:B_KOD_RACH)
					then &lt;m:KodRach&gt;{ data($fml/fml:B_KOD_RACH) }&lt;/m:KodRach&gt;
					else ()
			}
			{
				if($fml/fml:B_DL_NR_RACH)
					then &lt;m:DlNrRach&gt;{ data($fml/fml:B_DL_NR_RACH) }&lt;/m:DlNrRach&gt;
					else ()
			}
			{
				if($fml/fml:B_SALDO)
					then &lt;m:Saldo&gt;{ data($fml/fml:B_SALDO) }&lt;/m:Saldo&gt;
					else ()
			}
			{
				if($fml/fml:B_DOST_SRODKI)
					then &lt;m:DostSrodki&gt;{ data($fml/fml:B_DOST_SRODKI) }&lt;/m:DostSrodki&gt;
					else ()
			}
			{
				if($fml/fml:B_WALUTA)
					then &lt;m:Waluta&gt;{ data($fml/fml:B_WALUTA) }&lt;/m:Waluta&gt;
					else ()
			}
			{
				if($fml/fml:B_KOD_WALUTY)
					then &lt;m:KodWaluty&gt;{ data($fml/fml:B_KOD_WALUTY) }&lt;/m:KodWaluty&gt;
					else ()
			}
			{
				if($fml/fml:B_DATA_OST_OPER)
					then &lt;m:DataOstOper&gt;{ data($fml/fml:B_DATA_OST_OPER) }&lt;/m:DataOstOper&gt;
					else ()
			}
			{
				if($fml/fml:B_TYP_RACH)
					then &lt;m:TypRach&gt;{ data($fml/fml:B_TYP_RACH) }&lt;/m:TypRach&gt;
					else ()
			}
			{
				if($fml/fml:B_OPIS_RACH)
					then &lt;m:OpisRach&gt;{ data($fml/fml:B_OPIS_RACH) }&lt;/m:OpisRach&gt;
					else ()
			}
			{
				if($fml/fml:B_SKROT_OPISU)
					then &lt;m:SkrotOpisu&gt;{ data($fml/fml:B_SKROT_OPISU) }&lt;/m:SkrotOpisu&gt;
					else ()
			}
			{
				if($fml/fml:B_ID_ODDZ)
					then &lt;m:IdOddz&gt;{ data($fml/fml:B_ID_ODDZ) }&lt;/m:IdOddz&gt;
					else ()
			}
			{
				if($fml/fml:B_NAZWA_ODDZ)
					then &lt;m:NazwaOddz&gt;{ data($fml/fml:B_NAZWA_ODDZ) }&lt;/m:NazwaOddz&gt;
					else ()
			}
			{
				if($fml/fml:B_RODZAJ_RACH)
					then &lt;m:RodzajRach&gt;{ data($fml/fml:B_RODZAJ_RACH) }&lt;/m:RodzajRach&gt;
					else ()
			}
			{
				if($fml/fml:B_MIKROODDZIAL)
					then &lt;m:Mikrooddzial&gt;{ data($fml/fml:B_MIKROODDZIAL) }&lt;/m:Mikrooddzial&gt;
					else ()
			}
			{
				if($fml/fml:B_PRAWA_KLIENTA)
					then &lt;m:PrawaKlienta&gt;{ data($fml/fml:B_PRAWA_KLIENTA) }&lt;/m:PrawaKlienta&gt;
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KLIENTA)
					then &lt;m:NazwaKlienta&gt;{ data($fml/fml:B_NAZWA_KLIENTA) }&lt;/m:NazwaKlienta&gt;
					else ()
			}
			{
				if($fml/fml:B_UL_ZAM)
					then &lt;m:UlZam&gt;{ data($fml/fml:B_UL_ZAM) }&lt;/m:UlZam&gt;
					else ()
			}
			{
				if($fml/fml:B_M_ZAM)
					then &lt;m:MZam&gt;{ data($fml/fml:B_M_ZAM) }&lt;/m:MZam&gt;
					else ()
			}
			{
				if($fml/fml:B_OPROC1)
					then &lt;m:Oproc1&gt;{ data($fml/fml:B_OPROC1) }&lt;/m:Oproc1&gt;
					else ()
			}
			{
				if($fml/fml:B_D_OTWARCIA)
					then &lt;m:DOtwarcia&gt;{ data($fml/fml:B_D_OTWARCIA) }&lt;/m:DOtwarcia&gt;
					else ()
			}
			{
				if($fml/fml:B_D_ZAMKNIECIA)
					then &lt;m:DZamkniecia&gt;{ data($fml/fml:B_D_ZAMKNIECIA) }&lt;/m:DZamkniecia&gt;
					else ()
			}
			{
				if($fml/fml:B_D_OSTATNIEJ_KAP)
					then &lt;m:DOstatniejKap&gt;{ data($fml/fml:B_D_OSTATNIEJ_KAP) }&lt;/m:DOstatniejKap&gt;
					else ()
			}
			{
				if($fml/fml:B_D_NASTEPNEJ_KAP)
					then &lt;m:DNastepnejKap&gt;{ data($fml/fml:B_D_NASTEPNEJ_KAP) }&lt;/m:DNastepnejKap&gt;
					else ()
			}
			{
				if($fml/fml:B_D_POCZ_WKL)
					then &lt;m:DPoczWkl&gt;{ data($fml/fml:B_D_POCZ_WKL) }&lt;/m:DPoczWkl&gt;
					else ()
			}
			{
				if($fml/fml:B_D_KON_WKL)
					then &lt;m:DKonWkl&gt;{ data($fml/fml:B_D_KON_WKL) }&lt;/m:DKonWkl&gt;
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_AKT)
					then &lt;m:OdsSkapAkt&gt;{ data($fml/fml:B_ODS_SKAP_AKT) }&lt;/m:OdsSkapAkt&gt;
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_WN_POP)
					then &lt;m:OdsSkapWnPop&gt;{ data($fml/fml:B_ODS_SKAP_WN_POP) }&lt;/m:OdsSkapWnPop&gt;
					else ()
			}
			{
				if($fml/fml:B_LIMIT2)
					then &lt;m:Limit2&gt;{ data($fml/fml:B_LIMIT2) }&lt;/m:Limit2&gt;
					else ()
			}
			{
				if($fml/fml:B_D_KONCA_LIMITU)
					then &lt;m:DKoncaLimitu&gt;{ data($fml/fml:B_D_KONCA_LIMITU) }&lt;/m:DKoncaLimitu&gt;
					else ()
			}
			{
				if($fml/fml:B_BLOKADA)
					then &lt;m:Blokada&gt;{ data($fml/fml:B_BLOKADA) }&lt;/m:Blokada&gt;
					else ()
			}
			{
				if($fml/fml:B_RODZAJ_OPROC)
					then &lt;m:RodzajOproc&gt;{ data($fml/fml:B_RODZAJ_OPROC) }&lt;/m:RodzajOproc&gt;
					else ()
			}
			{
				if($fml/fml:B_KAPITALIZACJA)
					then &lt;m:Kapitalizacja&gt;{ data($fml/fml:B_KAPITALIZACJA) }&lt;/m:Kapitalizacja&gt;
					else ()
			}
			{
				if($fml/fml:B_PRZEKS_KAP)
					then &lt;m:PrzeksKap&gt;{ data($fml/fml:B_PRZEKS_KAP) }&lt;/m:PrzeksKap&gt;
					else ()
			}
			{
				if($fml/fml:B_OKR_WKLADU)
					then &lt;m:OkrWkladu&gt;{ data($fml/fml:B_OKR_WKLADU) }&lt;/m:OkrWkladu&gt;
					else ()
			}
			{
				if($fml/fml:B_JDN_OKR_WKLADU)
					then &lt;m:JdnOkrWkladu&gt;{ data($fml/fml:B_JDN_OKR_WKLADU) }&lt;/m:JdnOkrWkladu&gt;
					else ()
			}
			{
				if($fml/fml:B_D_NALICZ_ODS)
					then &lt;m:DNaliczOds&gt;{ data($fml/fml:B_D_NALICZ_ODS) }&lt;/m:DNaliczOds&gt;
					else ()
			}
			{
				if($fml/fml:B_D_NAST_OPER)
					then &lt;m:DNastOper&gt;{ data($fml/fml:B_D_NAST_OPER) }&lt;/m:DNastOper&gt;
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_NMA)
					then &lt;m:ObrotyDzienNma&gt;{ data($fml/fml:B_OBROTY_DZIEN_NMA) }&lt;/m:ObrotyDzienNma&gt;
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_NWN)
					then &lt;m:ObrotyDzienNwn&gt;{ data($fml/fml:B_OBROTY_DZIEN_NWN) }&lt;/m:ObrotyDzienNwn&gt;
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_UMA)
					then &lt;m:ObrotyDzienUma&gt;{ data($fml/fml:B_OBROTY_DZIEN_UMA) }&lt;/m:ObrotyDzienUma&gt;
					else ()
			}
			{
				if($fml/fml:B_OBROTY_DZIEN_UWN)
					then &lt;m:ObrotyDzienUwn&gt;{ data($fml/fml:B_OBROTY_DZIEN_UWN) }&lt;/m:ObrotyDzienUwn&gt;
					else ()
			}
			{
				if($fml/fml:B_OPROC2)
					then &lt;m:Oproc2&gt;{ data($fml/fml:B_OPROC2) }&lt;/m:Oproc2&gt;
					else ()
			}
			{
				if($fml/fml:B_NR_WYCIAGU)
					then &lt;m:NrWyciagu&gt;{ data($fml/fml:B_NR_WYCIAGU) }&lt;/m:NrWyciagu&gt;
					else ()
			}
			{
				if($fml/fml:B_D_POP_WYCIAGU)
					then &lt;m:DPopWyciagu&gt;{ data($fml/fml:B_D_POP_WYCIAGU) }&lt;/m:DPopWyciagu&gt;
					else ()
			}
			{
				if($fml/fml:B_D_NAST_WYCIAGU)
					then &lt;m:DNastWyciagu&gt;{ data($fml/fml:B_D_NAST_WYCIAGU) }&lt;/m:DNastWyciagu&gt;
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KR)
					then &lt;m:NazwaKr&gt;{ data($fml/fml:B_NAZWA_KR) }&lt;/m:NazwaKr&gt;
					else ()
			}
			{
				if($fml/fml:B_NAZWA_STOPY)
					then &lt;m:NazwaStopy&gt;{ data($fml/fml:B_NAZWA_STOPY) }&lt;/m:NazwaStopy&gt;
					else ()
			}
			{
				if($fml/fml:B_STOPA)
					then &lt;m:Stopa&gt;{ data($fml/fml:B_STOPA) }&lt;/m:Stopa&gt;
					else ()
			}
			{
				if($fml/fml:B_OKR_OPROC)
					then &lt;m:OkrOproc&gt;{ data($fml/fml:B_OKR_OPROC) }&lt;/m:OkrOproc&gt;
					else ()
			}
			{
				if($fml/fml:B_JDN_OKR_OPROC)
					then &lt;m:JdnOkrOproc&gt;{ data($fml/fml:B_JDN_OKR_OPROC) }&lt;/m:JdnOkrOproc&gt;
					else ()
			}
			{
				if($fml/fml:B_LIMIT_BIEZ_KR)
					then &lt;m:LimitBiezKr&gt;{ data($fml/fml:B_LIMIT_BIEZ_KR) }&lt;/m:LimitBiezKr&gt;
					else ()
			}
			{
				if($fml/fml:B_KREDYT_PRZYZN)
					then &lt;m:KredytPrzyzn&gt;{ data($fml/fml:B_KREDYT_PRZYZN) }&lt;/m:KredytPrzyzn&gt;
					else ()
			}
			{
				if($fml/fml:B_KOD_RACH_NIEPRZET)
					then &lt;m:KodRachNieprzet&gt;{ data($fml/fml:B_KOD_RACH_NIEPRZET) }&lt;/m:KodRachNieprzet&gt;
					else ()
			}
			{
				if($fml/fml:B_REWOLWING)
					then &lt;m:Rewolwing&gt;{ data($fml/fml:B_REWOLWING) }&lt;/m:Rewolwing&gt;
					else ()
			}
			{
				if($fml/fml:B_AUTO_KAPITAL)
					then &lt;m:AutoKapital&gt;{ data($fml/fml:B_AUTO_KAPITAL) }&lt;/m:AutoKapital&gt;
					else ()
			}
			{
				if($fml/fml:B_AUTO_ODSETKI)
					then &lt;m:AutoOdsetki&gt;{ data($fml/fml:B_AUTO_ODSETKI) }&lt;/m:AutoOdsetki&gt;
					else ()
			}
			{
				if($fml/fml:B_KOD_RACH_ODSETKI)
					then &lt;m:KodRachOdsetki&gt;{ data($fml/fml:B_KOD_RACH_ODSETKI) }&lt;/m:KodRachOdsetki&gt;
					else ()
			}
			{
				if($fml/fml:B_KOD_RACH_KAPITAL)
					then &lt;m:KodRachKapital&gt;{ data($fml/fml:B_KOD_RACH_KAPITAL) }&lt;/m:KodRachKapital&gt;
					else ()
			}
			{
				if($fml/fml:B_D_OST_SPLATY)
					then &lt;m:DOstSplaty&gt;{ data($fml/fml:B_D_OST_SPLATY) }&lt;/m:DOstSplaty&gt;
					else ()
			}
			{
				if($fml/fml:B_NAJBLIZSZE_NAL)
					then &lt;m:NajblizszeNal&gt;{ data($fml/fml:B_NAJBLIZSZE_NAL) }&lt;/m:NajblizszeNal&gt;
					else ()
			}
			{
				if($fml/fml:B_NAJBLIZSZE_ODS)
					then &lt;m:NajblizszeOds&gt;{ data($fml/fml:B_NAJBLIZSZE_ODS) }&lt;/m:NajblizszeOds&gt;
					else ()
			}
			{
				if($fml/fml:B_NAJBLIZSZE_RATY)
					then &lt;m:NajblizszeRaty&gt;{ data($fml/fml:B_NAJBLIZSZE_RATY) }&lt;/m:NajblizszeRaty&gt;
					else ()
			}
			{
				if($fml/fml:B_SUMA_ODS_ZAPL)
					then &lt;m:SumaOdsZapl&gt;{ data($fml/fml:B_SUMA_ODS_ZAPL) }&lt;/m:SumaOdsZapl&gt;
					else ()
			}
			{
				if($fml/fml:B_D_POP_WYMAG)
					then &lt;m:DPopWymag&gt;{ data($fml/fml:B_D_POP_WYMAG) }&lt;/m:DPopWymag&gt;
					else ()
			}
			{
				if($fml/fml:B_ODS_POP_WYMAG)
					then &lt;m:OdsPopWymag&gt;{ data($fml/fml:B_ODS_POP_WYMAG) }&lt;/m:OdsPopWymag&gt;
					else ()
			}
			{
				if($fml/fml:B_NR_ANEKSU)
					then &lt;m:NrAneksu&gt;{ data($fml/fml:B_NR_ANEKSU) }&lt;/m:NrAneksu&gt;
					else ()
			}
			{
				if($fml/fml:B_DOCHODY_DO)
					then &lt;m:DochodyDo&gt;{ data($fml/fml:B_DOCHODY_DO) }&lt;/m:DochodyDo&gt;
					else ()
			}
			{
				if($fml/fml:B_ODS_NIEROZL)
					then &lt;m:OdsNierozl&gt;{ data($fml/fml:B_ODS_NIEROZL) }&lt;/m:OdsNierozl&gt;
					else ()
			}
			{
				if($fml/fml:B_LIMIT_OD)
					then &lt;m:LimitOd&gt;{ data($fml/fml:B_LIMIT_OD) }&lt;/m:LimitOd&gt;
					else ()
			}
			{
				if($fml/fml:B_LIMIT_KWOTA)
					then &lt;m:LimitKwota&gt;{ data($fml/fml:B_LIMIT_KWOTA) }&lt;/m:LimitKwota&gt;
					else ()
			}
			{
				if($fml/fml:B_D_ODS_NALICZ)
					then &lt;m:DOdsNalicz&gt;{ data($fml/fml:B_D_ODS_NALICZ) }&lt;/m:DOdsNalicz&gt;
					else ()
			}
			{
				if($fml/fml:B_D_WYMAGALNOSCI)
					then &lt;m:DWymagalnosci&gt;{ data($fml/fml:B_D_WYMAGALNOSCI) }&lt;/m:DWymagalnosci&gt;
					else ()
			}
			{
				if($fml/fml:B_UL_KORESP)
					then &lt;m:UlKoresp&gt;{ data($fml/fml:B_UL_KORESP) }&lt;/m:UlKoresp&gt;
					else ()
			}
			{
				if($fml/fml:B_M_KORESP)
					then &lt;m:MKoresp&gt;{ data($fml/fml:B_M_KORESP) }&lt;/m:MKoresp&gt;
					else ()
			}
			{
				if($fml/fml:B_KOD_KORESP)
					then &lt;m:KodKoresp&gt;{ data($fml/fml:B_KOD_KORESP) }&lt;/m:KodKoresp&gt;
					else ()
			}
			{
				if($fml/fml:B_NAZWA_KORESP)
					then &lt;m:NazwaKoresp&gt;{ data($fml/fml:B_NAZWA_KORESP) }&lt;/m:NazwaKoresp&gt;
					else ()
			}
		&lt;/m:bICBSGetAccountResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapbICBSGetAccountResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>