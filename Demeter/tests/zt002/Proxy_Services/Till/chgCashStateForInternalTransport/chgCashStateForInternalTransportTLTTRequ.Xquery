<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns0:header" location="chgCashStateForInternalTransport.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns0:invoke" location="chgCashStateForInternalTransport.WSDL" ::)
(:: pragma bea:global-element-parameter parameter="$targetTransactionResponse" element="FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$targetTransactionException" element="FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-parameter parameter="$targetDenomChgException" element="FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)
(:: pragma bea:global-element-return element="ns7:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns2 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns1 = "urn:cash.operations.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:cashdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns0 = "urn:be.services.dcl";
declare namespace ns9 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:internaltransports.operations.entities.be.dcl";
declare namespace ns6 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace xf = "http://tempuri.org/OSB%20Project%201/Till/chgCashStateForInternalTransport_CR124/chgCashStateForInternalTransportTLTTRequest/";
declare namespace ns7 = "http://bzwbk.com/nfe/transactionLog";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";


declare function xf:chgCashStateForInternalTransportTLTTRequest($header1 as element(ns0:header),
    $invoke1 as element(ns0:invoke),
    $targetTransactionResponse as element(soap-env:Body),
    $targetTransactionException as element(FML32),
    $targetDenomChgException as element(FML32))
    as element(ns7:transactionLogEntry) {
        &lt;ns7:transactionLogEntry&gt;
            {
                for $businessTransactionType in $invoke1/ns0:targetTransaction/ns8:Transaction/ns8:businessTransactionType/ns6:BusinessTransactionType/ns6:businessTransactionType
                return
                    &lt;businessTransactionType&gt;{ data($businessTransactionType) }&lt;/businessTransactionType&gt;
            }
            {
                for $csrMessageType in $invoke1/ns0:targetTransaction/ns8:Transaction/ns8:csrMessageType/ns6:CsrMessageType/ns6:csrMessageType
                return
                    &lt;csrMessageType&gt;{ data($csrMessageType) }&lt;/csrMessageType&gt;
            }
            &lt;debit&gt;
                {
                    for $amount in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:amount
                    return
                        &lt;amount&gt;{ xs:decimal( data($amount) ) }&lt;/amount&gt;
                }
                {
                    for $currencyCode in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:currency/ns4:CurrencyCode/ns4:currencyCode
                    return
                        &lt;currencyCode&gt;{ data($currencyCode) }&lt;/currencyCode&gt;
                }
            &lt;/debit&gt;
            {
                for $dtTransactionType in $invoke1/ns0:targetTransaction/ns8:Transaction/ns8:dtTransactionType/ns6:DtTransactionType/ns6:dtTransactionType
                return
                    &lt;dtTransactionType&gt;{ data($dtTransactionType) }&lt;/dtTransactionType&gt;
            }
            &lt;executor&gt;
                {
                    for $branchCode in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:branchCurrentStatus/ns2:BranchCurrentStatus/ns2:branchCode/ns4:BranchCode/ns4:branchCode
                    return
                        &lt;branchNumber&gt;{ xs:int( data($branchCode) ) }&lt;/branchNumber&gt;
                }
                {
                    for $userID in $invoke1/ns0:targetUser/ns9:User/ns9:userID
                    return
                        &lt;executorID&gt;{ data($userID) }&lt;/executorID&gt;
                }
                {
                    for $userFirstName in $invoke1/ns0:targetUser/ns9:User/ns9:userFirstName
                    return
                        &lt;firstName&gt;{ data($userFirstName) }&lt;/firstName&gt;
                }
                {
                    for $userLastName in $invoke1/ns0:targetUser/ns9:User/ns9:userLastName
                    return
                        &lt;lastName&gt;{ data($userLastName) }&lt;/lastName&gt;
                }
                {
                    for $tellerID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:targetTeller/ns2:Teller/ns2:tellerID
                    return
                        &lt;tellerID&gt;{ data($tellerID) }&lt;/tellerID&gt;
                }
                {
                    for $tillID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:targetTill/ns2:Till/ns2:tillID
                    return
                        &lt;tillNumber&gt;{ data($tillID) }&lt;/tillNumber&gt;
                }
                &lt;userID?&gt;{ data($targetTransactionResponse/FML32/TR_UZYTKOWNIK) }&lt;/userID&gt;
            &lt;/executor&gt;
            {
                for $extendedCSRMessageType in $invoke1/ns0:targetTransaction/ns8:Transaction/ns8:extendedCSRMessageType/ns6:ExtendedCSRMessageType/ns6:extendedCSRMessageType
                return
                    &lt;extendedCSRMessageType&gt;{ data($extendedCSRMessageType) }&lt;/extendedCSRMessageType&gt;
            }
            &lt;hlbsName&gt;chgCashStateForInternalTransport&lt;/hlbsName&gt;
            {
                for $putDownDate in $invoke1/ns0:targetTransaction/ns8:Transaction/ns8:putDownDate
                return
                    &lt;putDownDate&gt;{ data($putDownDate) }&lt;/putDownDate&gt;
            }
            &lt;timestamp&gt;{ data($header1/ns0:msgHeader/ns0:timestamp) }&lt;/timestamp&gt;
            &lt;tlCurrencyCashList&gt;
                {
                    for $amount in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:amount
                    return
                        &lt;amount&gt;{ xs:decimal( data($amount) ) }&lt;/amount&gt;
                }
                {
                    for $currencyCode in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:currency/ns4:CurrencyCode/ns4:currencyCode
                    return
                        &lt;currency&gt;{ data($currencyCode) }&lt;/currency&gt;
                }
                {
                    for $DenominationSpecification in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:currencyCashList/ns1:CurrencyCash[1]/ns1:denominationSpecificationList/ns1:DenominationSpecification
                    return
                        &lt;tlDenominationSpecList&gt;
                            {
                                for $denominationID in $DenominationSpecification/ns1:denomination/ns3:DenominationDefinition/ns3:denominationID
                                return
                                    &lt;denomination&gt;{ data($denominationID) }&lt;/denomination&gt;
                            }
                            {
                                for $itemsNumber in $DenominationSpecification/ns1:itemsNumber
                                return
                                    &lt;itemsNumber&gt;{ xs:int(data($itemsNumber)) }&lt;/itemsNumber&gt;
                            }
                        &lt;/tlDenominationSpecList&gt;
                }
            &lt;/tlCurrencyCashList&gt;
            {
				for $errorDesc at $occ in $targetTransactionException/FML_ERROR_DESCRIPTION
				let $errorCode1 := $targetTransactionException/FML_ERROR_CODE1
				let $errorCode2 := $targetTransactionException/FML_ERROR_CODE2
				return
					&lt;tlExceptionDataList?&gt;{
						if (data($errorDesc) != '') then
						(
							&lt;errorCode1?&gt;{ xs:int(data($errorCode1[$occ])) }&lt;/errorCode1&gt;,
							&lt;errorCode2?&gt;{ xs:int(data($errorCode2[$occ])) }&lt;/errorCode2&gt;,
							&lt;errorDescription?&gt;{ data($errorDesc) }&lt;/errorDescription&gt;
						)
						else
						()
					}&lt;/tlExceptionDataList&gt;
			}
            {
				for $errorDesc at $occ in $targetDenomChgException/FML_ERROR_DESCRIPTION
				let $errorCode1 := $targetDenomChgException/FML_ERROR_CODE1
				let $errorCode2 := $targetDenomChgException/FML_ERROR_CODE2
				return
					&lt;tlExceptionDataList?&gt;{
						if (data($errorDesc) != '') then
						(
							&lt;errorCode1?&gt;{ xs:int(data($errorCode1[$occ])) }&lt;/errorCode1&gt;,
							&lt;errorCode2?&gt;{ xs:int(data($errorCode2[$occ])) }&lt;/errorCode2&gt;,
							&lt;errorDescription?&gt;{ data($errorDesc) }&lt;/errorDescription&gt;
						)
						else
						()
					}&lt;/tlExceptionDataList&gt;
			}
           &lt;tlInternalCashTransport&gt;
                {
                    for $sourceName in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:sourceName
                    return
                        &lt;sourceName&gt;{ data($sourceName) }&lt;/sourceName&gt;
                }
                {
                    for $tellerID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:sourceTeller/ns2:Teller/ns2:tellerID
                    return
                        &lt;sourceTeller&gt;{ data($tellerID) }&lt;/sourceTeller&gt;
                }
                {
                    for $tillID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:sourceTill/ns2:Till/ns2:tillID
                    return
                        &lt;sourceTill&gt;{ data($tillID) }&lt;/sourceTill&gt;
                }
                {
                    for $targetName in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:targetName
                    return
                        &lt;targetName&gt;{ data($targetName) }&lt;/targetName&gt;
                }
                {
                    for $tellerID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:targetTeller/ns2:Teller/ns2:tellerID
                    return
                        &lt;targetTeller&gt;{ data($tellerID) }&lt;/targetTeller&gt;
                }
                {
                    for $tillID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:targetTill/ns2:Till/ns2:tillID
                    return
                        &lt;targetTill&gt;{ data($tillID) }&lt;/targetTill&gt;
                }
                {
                    for $transportID in $invoke1/ns0:internalCashTransport/ns5:InternalCashTransport/ns5:transportID
                    return
                        &lt;transportID&gt;{ data($transportID) }&lt;/transportID&gt;
                }
            &lt;/tlInternalCashTransport&gt;
            {
                for $transactionDate in $invoke1/ns0:targetTransaction/ns8:Transaction/ns8:transactionDate
                return
                    &lt;transactionDate&gt;{ data($transactionDate) }&lt;/transactionDate&gt;
            }
            {
                for $transactionGroupID in $invoke1/ns0:targetTransaction/ns8:Transaction/ns8:transactionGroupID
                return
                    &lt;transactionGroupID&gt;{ data($transactionGroupID) }&lt;/transactionGroupID&gt;
            }
            {
                for $transactionID in $invoke1/ns0:targetTransaction/ns8:Transaction/ns8:transactionID
                return
                    &lt;transactionID&gt;{ data($transactionID) }&lt;/transactionID&gt;
            }
            {
				let $tr_status := $targetTransactionResponse/FML32/TR_STATUS
				return
					if (xs:boolean(data($tr_status)!='')) then
					(
						&lt;transactionStatus?&gt;{ data($tr_status) }&lt;/transactionStatus&gt;
					)
					else
					()
			}
        &lt;/ns7:transactionLogEntry&gt;
};

declare variable $header1 as element(ns0:header) external;
declare variable $invoke1 as element(ns0:invoke) external;
declare variable $targetTransactionResponse as element(soap-env:Body) external;
declare variable $targetTransactionException as element(FML32) external;
declare variable $targetDenomChgException as element(FML32) external;
&lt;soap-env:Body&gt;{
xf:chgCashStateForInternalTransportTLTTRequest($header1,
    $invoke1,
    $targetTransactionResponse,
    $targetTransactionException,
    $targetDenomChgException)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>