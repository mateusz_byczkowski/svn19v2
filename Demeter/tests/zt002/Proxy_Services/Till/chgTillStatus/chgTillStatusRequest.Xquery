<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.3
 : @since   2009-12-15
 :
 : wersja WSDLa: 13-01-2010 15:28:58
 :
 : $Proxy Services/Till/chgTillStatus/chgTillStatusRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/chgTillStatus/chgTillStatusRequest_new/";
declare namespace ns0 = "urn:acceptance.entities.be.dcl";
declare namespace ns1 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns6 = "urn:operations.entities.be.dcl";
declare namespace ns7 = "";
declare namespace ns8 = "urn:acceptancedict.dictionaries.be.dcl";
declare namespace ns9 = "urn:be.services.dcl";

declare variable $header1 as element(ns9:header) external;
declare variable $invoke1 as element(ns9:invoke) external;

(:~
 : @param $invoke1 operacja wejściowa
 : @param $header1 nagłówek komunikatu
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:chgTillStatusRequest($header1 as element(ns9:header),
										   $invoke1 as element(ns9:invoke))
    as element(ns7:FML32)
{
	&lt;ns7:FML32&gt;

    	(:
    	 : dane z nagłówka operacji
    	 :)
    	&lt;ns7:TR_MSG_ID?&gt;{
			data($header1/ns9:msgHeader/ns9:msgId)
		}&lt;/ns7:TR_MSG_ID&gt;
    	
    	&lt;ns7:TR_UZYTKOWNIK?&gt;{
    		data($header1/ns9:msgHeader/ns9:userId)
    	}&lt;/ns7:TR_UZYTKOWNIK&gt;
    	
    	&lt;ns7:TR_ID_OPER?&gt;{
    		data($header1/ns9:transHeader/ns9:transId)
    	}&lt;/ns7:TR_ID_OPER&gt;
    	
    	{
    		(:
    		 : konwersja z timestampa ISO do formatu DD-MM-RRRR
    		 :)
			let $timestamp := data($header1/ns9:msgHeader/ns9:timestamp)
			return
	    		if ($timestamp) then
					&lt;ns7:TR_DATA_OPER&gt;{
						fn:concat(fn:substring($timestamp, 9, 2),
								  '-',
								  fn:substring($timestamp, 6, 2),
			    	        	  '-',
			    	        	  fn:substring($timestamp, 1, 4))
					}&lt;/ns7:TR_DATA_OPER&gt;
	    		else
					()
		}
		 
    	(:
    	 : dane z operacji wejściowej
    	 :) 
		&lt;ns7:TR_AKCEPTANT_SKP?&gt;{
			data($invoke1/ns9:acceptTask/ns0:AcceptTask/ns0:acceptor)
		}&lt;/ns7:TR_AKCEPTANT_SKP&gt;
		
		&lt;ns7:TR_TYP_KOM?&gt;{
			data($invoke1/ns9:transaction/ns6:Transaction/ns6:csrMessageType/ns5:CsrMessageType/ns5:csrMessageType)
		}&lt;/ns7:TR_TYP_KOM&gt;
		
		&lt;ns7:TR_ODDZ_KASY?&gt;{
			data($invoke1/ns9:branchCode/ns2:BranchCode/ns2:branchCode)
		}&lt;/ns7:TR_ODDZ_KASY&gt;
		
		{
			(:
			 : konwersja formatu daty z RRRR-MM-DD na DD-MM-RRRR
			 :)
			let $sessionDate := data($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:sessionDate)
			return
	        	if ($sessionDate) then
	    	        &lt;ns7:TR_DATA_WALUTY&gt;{
    	        		fn:concat(fn:substring($sessionDate, 9, 2),
    	        			 	  '-',
	    	        			  fn:substring($sessionDate, 6, 2),
    		        			  '-',
    		        			  fn:substring($sessionDate, 1, 4))
					}&lt;/ns7:TR_DATA_WALUTY&gt;
				else
					()
		}
		
		&lt;ns7:TR_UZYTKOWNIK_SKP?&gt;{
			data($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:user/ns4:User/ns4:userID)
		}&lt;/ns7:TR_UZYTKOWNIK_SKP&gt;
            
		&lt;ns7:TR_KASA?&gt;{
			data($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:till/ns3:Till/ns3:tillID)
		}&lt;/ns7:TR_KASA&gt;
		
		&lt;ns7:TR_KASJER?&gt;{
			data($invoke1/ns9:userTxnSession/ns3:UserTxnSession/ns3:teller/ns3:Teller/ns3:tellerID)
		}&lt;/ns7:TR_KASJER&gt;
		
	&lt;/ns7:FML32&gt;
};

&lt;soap-env:Body&gt;{
	xf:chgTillStatusRequest($header1, $invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>