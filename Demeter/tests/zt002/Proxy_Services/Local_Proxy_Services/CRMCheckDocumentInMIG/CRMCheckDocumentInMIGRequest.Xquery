<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMCheckDocumentInMIGRequest($req as element(m:CRMCheckDocumentInMIGRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:DokTozsamosci)
					then &lt;fml:CI_DOK_TOZSAMOSCI&gt;{ data($req/m:DokTozsamosci) }&lt;/fml:CI_DOK_TOZSAMOSCI&gt;
					else ()
			}
			{
				if($req/m:SeriaNrDok)
					then &lt;fml:CI_SERIA_NR_DOK&gt;{ data($req/m:SeriaNrDok) }&lt;/fml:CI_SERIA_NR_DOK&gt;
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK&gt;{ data($req/m:Uzytkownik) }&lt;/fml:DC_UZYTKOWNIK&gt;
					else ()
			}
			{
				if($req/m:KodJednostki)
					then &lt;fml:DC_KOD_JEDNOSTKI&gt;{ data($req/m:KodJednostki) }&lt;/fml:DC_KOD_JEDNOSTKI&gt;
					else ()
			}
                        &lt;fml:DC_KOD_APLIKACJI&gt;90&lt;/fml:DC_KOD_APLIKACJI&gt;
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMCheckDocumentInMIGRequest($body/m:CRMCheckDocumentInMIGRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>