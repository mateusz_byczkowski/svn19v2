<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.4.1 2009-07-07 PKLI TEET 38316</con:description>
  <con:xquery>(: Log Zmian: 
==================================
 v.1.4.1  2009-07-07  PKLI  TEET 38316 
                                Zmiana mapowania pola IdDefinition z PT_ID_DEFINITION na PT_ID_GROUP
:)

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustProdAddrResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustProdAddrResponse) {
		&lt;m:CRMGetCustProdAddrResponse&gt;
			{
				if($fml/fml:NF_PAGEC_TOTALSIZE)
					then &lt;m:PagecTotalsize&gt;{ data($fml/fml:NF_PAGEC_TOTALSIZE) }&lt;/m:PagecTotalsize&gt;
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
					then &lt;m:PagecNavigationkeyvalu&gt;{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/m:PagecNavigationkeyvalu&gt;
					else ()
			}
			{
				if($fml/fml:NF_PAGEC_HASNEXT)
					then &lt;m:PagecHasnext&gt;{ data($fml/fml:NF_PAGEC_HASNEXT) }&lt;/m:PagecHasnext&gt;
					else ()
			}
			{

				let $PT_ID_DEFINITION := $fml/fml:PT_ID_DEFINITION
                                                                let $PT_ID_GROUP := $fml/fml:PT_ID_GROUP   (: v.1.4.1 T38316 :)
				let $NF_ACCOUN_ACCOUNTIBAN := $fml/fml:NF_ACCOUN_ACCOUNTNUMBER
				let $NF_ADDRES_HOUSEFLATNUMBER := $fml/fml:NF_ACCOUA_HOUSEFLATNUMBER
				let $NF_ADDRES_CITY := $fml/fml:NF_ACCOUA_CITY
				let $NF_STATE_STATE := $fml/fml:NF_ACCOUA_STATECOUNTRY
                                                                let $NF_ACCOUA_STATUS := $fml/fml:NF_ACCOUA_STATUS
				let $NF_ADDRES_VALIDFROM := $fml/fml:NF_ACCOUA_VALIDFROM
				let $NF_ADDRES_VALIDTO := $fml/fml:NF_ACCOUA_VALIDTO
				let $NF_ADDRES_STREET := $fml/fml:NF_ACCOUA_STREET
				let $NF_ADDRES_ZIPCODE := $fml/fml:NF_ACCOUA_ZIPCODE
				let $NF_COUNTC_COUNTRYCODE := $fml/fml:XXX
				let $NF_ADDRES_LOCAL := $fml/fml:NF_ACCOUA_ACCOUNTADDRESSTY
                                                                let $NF_NAME1 := $fml/fml:NF_ACCOUA_NAME1
                                                                let $NF_NAME2 := $fml/fml:NF_ACCOUA_NAME2
                                                                let $NF_DELETE := $fml/fml:NF_ACCOUA_DELETEWHENEXPIRE
				for $it at $p in $fml/fml:NF_ACCOUN_ACCOUNTNUMBER
				return
					&lt;m:Address&gt;
					{
					  (:	if($PT_ID_DEFINITION[$p])                                                                                    v.1.4.1 T38316 :)
					  (:		then &lt;m:IdDefinition&gt;{ data($PT_ID_DEFINITION[$p]) }&lt;/m:IdDefinition&gt;   v.1.4.1 T38316 :)
					  (:	else ()                                                                                                                  v.1.4.1 T38316 :)
                                                                                                if($PT_ID_GROUP[$p])                                                                                        (: v.1.4.1 T38316 :)
                                                                                                   then &lt;m:IdDefinition&gt;{ data($PT_ID_GROUP[$p]) }&lt;/m:IdDefinition&gt;                    (: v.1.4.1 T38316 :)
                                                                                                else ()                                                                                                               (: v.1.4.1 T38316 :)
                                                                                }
					{
						if($NF_ACCOUN_ACCOUNTIBAN[$p])
							then &lt;m:AccounAccountiban&gt;{ data($NF_ACCOUN_ACCOUNTIBAN[$p]) }&lt;/m:AccounAccountiban&gt;
						else ()
					}
					{
						if($NF_ADDRES_HOUSEFLATNUMBER[$p])
							then &lt;m:AddresHouseflatnumber&gt;{ data($NF_ADDRES_HOUSEFLATNUMBER[$p]) }&lt;/m:AddresHouseflatnumber&gt;
						else ()
					}
					{
						if($NF_ADDRES_CITY[$p])
							then &lt;m:AddresCity&gt;{ data($NF_ADDRES_CITY[$p]) }&lt;/m:AddresCity&gt;
						else ()
					}
					{
						if($NF_STATE_STATE[$p])
							then &lt;m:StateState&gt;{ data($NF_STATE_STATE[$p]) }&lt;/m:StateState&gt;
						else ()
					}
                                                                                {
						if($NF_ACCOUA_STATUS[$p])
							then &lt;m:AddresStatus&gt;{ data($NF_ACCOUA_STATUS[$p]) }&lt;/m:AddresStatus&gt;
						else ()
					}

					{
						if($NF_ADDRES_VALIDFROM[$p])
							then &lt;m:AddresValidfrom&gt;{ data($NF_ADDRES_VALIDFROM[$p]) }&lt;/m:AddresValidfrom&gt;
						else ()
					}
					{
						if($NF_ADDRES_VALIDTO[$p])
							then &lt;m:AddresValidto&gt;{ data($NF_ADDRES_VALIDTO[$p]) }&lt;/m:AddresValidto&gt;
						else ()
					}
					{
						if($NF_ADDRES_STREET[$p])
							then &lt;m:AddresStreet&gt;{ data($NF_ADDRES_STREET[$p]) }&lt;/m:AddresStreet&gt;
						else ()
					}
					{
						if($NF_ADDRES_ZIPCODE[$p])
							then &lt;m:AddresZipcode&gt;{ data($NF_ADDRES_ZIPCODE[$p]) }&lt;/m:AddresZipcode&gt;
						else ()
					}
					{
						if($NF_COUNTC_COUNTRYCODE[$p])
							then &lt;m:CountcCountrycode&gt;{ data($NF_COUNTC_COUNTRYCODE[$p]) }&lt;/m:CountcCountrycode&gt;
						else ()
					}
					{
						if($NF_ADDRES_LOCAL[$p])
							then if (data($NF_ADDRES_LOCAL[$p]) = 'K') 
                                                                     then &lt;m:AddresLocal&gt;0&lt;/m:AddresLocal&gt;
                                                               else if (data($NF_ADDRES_LOCAL[$p]) = 'Z') 
                                                                     then &lt;m:AddresLocal&gt;1&lt;/m:AddresLocal&gt;
                                                               else ()
						else ()
					}

                                                                                {
						if($NF_NAME1[$p])
							then &lt;m:Name1&gt;{ data($NF_NAME1[$p]) }&lt;/m:Name1&gt;
						else ()
					}
                                                                                {
						if($NF_NAME2[$p])
							then &lt;m:Name2&gt;{ data($NF_NAME2[$p]) }&lt;/m:Name2&gt;
						else ()
					}
                                                                                {
						if($NF_DELETE[$p])
							then &lt;m:DeleteWhenExpired&gt;{ data($NF_DELETE[$p]) }&lt;/m:DeleteWhenExpired&gt;
						else ()
					}

					&lt;/m:Address&gt;
			}

		&lt;/m:CRMGetCustProdAddrResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCustProdAddrResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>