<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery><![CDATA[declare namespace prim="http://bzwbk.com/services/prime/";
declare namespace cards="http://bzwbk.com/services/cards";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{
	typeswitch($body/*)
		case $req as element(prim:FeeTrxnInfoResponse ) return
				<cards:getHistoryResponse>
	                           	{for $ap in $req/*
	                                return
                                                           for $p in $ap/*
                                                                  return
					<CardChargeHistoryData>
						<pan>{ data($p/prim:pan) }</pan>
						<applyDate>{ data($p/prim:applyDate) }</applyDate>
						<chargecur>{ data($p/prim:chargecur) }</chargecur>
						<recid>{ data($p/prim:recid) }</recid>
						<sysdate>{ data($p/prim:sysdate) }</sysdate>
						<tlgid>{ data($p/prim:recid) }</tlgid>
						<sourcesystem>{ data($p/prim:sourcesystem) }</sourcesystem>
						<credit>{ data($p/prim:credit) }</credit>
						<charge>{ data($p/prim:charge) }</charge>
						<accno>{ data($p/prim:accno) }</accno>
						<descr>{ data($p/prim:descr) }</descr>
					</CardChargeHistoryData>

                                                 }
				</cards:getHistoryResponse>		
		default return <error>Error translating request message</error>
		
}	
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>