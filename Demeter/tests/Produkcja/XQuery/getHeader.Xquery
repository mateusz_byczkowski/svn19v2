<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace hd="urn:be.services.dcl";
declare variable $header as element(soap:Header) external;


&lt;NF_MSHEAD_MSGID&gt;{data($header/hd:header/hd:msgHeader/hd:msgId)}&lt;/NF_MSHEAD_MSGID&gt;</con:xquery>
</con:xqueryEntry>