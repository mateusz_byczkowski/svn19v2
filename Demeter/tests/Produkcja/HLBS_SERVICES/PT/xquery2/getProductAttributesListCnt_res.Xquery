<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://tempuri.org/dcl-bzwbk-pt-alsb/xquery/getProductAttributesListCnt_resp/";
declare namespace srv = "urn:be.services.dcl";
declare namespace n01 ="urn:baseauxentities.be.dcl";

declare function xf:getProductAttributesListCnt_resp($fml as element())
    as element() {
	&lt;srv:invokeResponse&gt;
                    &lt;srv:attrCount&gt;
                        &lt;n01:IntegerHolder&gt;
                            	{if(xs:int(data($fml/PT_PACK_SIZE)) ne 0) then
                            	&lt;n01:value&gt;{ data($fml/PT_PACK_SIZE) }&lt;/n01:value&gt;
                            	else ()}
                         &lt;/n01:IntegerHolder&gt;
                    &lt;/srv:attrCount&gt;
    &lt;/srv:invokeResponse&gt;
};

declare variable $fml as element() external;

xf:getProductAttributesListCnt_resp($fml)</con:xquery>
</con:xqueryEntry>