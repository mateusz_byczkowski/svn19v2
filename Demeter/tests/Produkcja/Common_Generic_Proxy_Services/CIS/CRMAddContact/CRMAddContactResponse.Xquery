<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMAddContactResponse($fml as element(fml:FML32))
	as element(m:CRMAddContactResponse) {
		&lt;m:CRMAddContactResponse&gt;
			{
				if($fml/fml:CI_ID_KONT)
					then &lt;m:IdKont&gt;{ data($fml/fml:CI_ID_KONT) }&lt;/m:IdKont&gt;
					else ()
			}
		&lt;/m:CRMAddContactResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMAddContactResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>