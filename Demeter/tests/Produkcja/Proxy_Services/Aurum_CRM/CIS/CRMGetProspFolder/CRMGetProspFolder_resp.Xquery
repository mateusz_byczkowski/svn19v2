<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetProspFolderResponse($fml as element(fml:FML32))
	as element(m:CRMGetProspFolderResponse) {
		&lt;m:CRMGetProspFolderResponse&gt;
{
if ($fml/fml:DC_TYP_KLIENTA) then
&lt;m:CustType&gt;{data($fml/fml:DC_TYP_KLIENTA)}&lt;/m:CustType&gt;
else ()
}
{
if ($fml/fml:CI_DATA_WPROWADZENIA) then
&lt;m:FolderCreateDate&gt;{data($fml/fml:CI_DATA_WPROWADZENIA)}&lt;/m:FolderCreateDate&gt;
else ()
}
{
if ($fml/fml:CI_UDOSTEP_GRUPA) then
&lt;m:MarketingAgree&gt;{data($fml/fml:CI_UDOSTEP_GRUPA)}&lt;/m:MarketingAgree&gt;
else ()
}
{
if ($fml/fml:CI_ZGODA_INF_HANDLOWA) then
&lt;m:InfoSaleAgree&gt;{data($fml/fml:CI_ZGODA_INF_HANDLOWA)}&lt;/m:InfoSaleAgree&gt;
else ()
}
{
if ($fml/fml:CI_NAZWA_PELNA) then
&lt;m:CustName&gt;{data($fml/fml:CI_NAZWA_PELNA)}&lt;/m:CustName&gt;
else ()
}
{
if ($fml/fml:DC_ULICA_DANE_PODST) then
&lt;m:CustAddressStreet&gt;{data($fml/fml:DC_ULICA_DANE_PODST)}&lt;/m:CustAddressStreet&gt;
else ()
}
{
if ($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST) then
&lt;m:CustAddressHouseFlatNo&gt;{data($fml/fml:DC_NR_POSES_LOKALU_DANE_PODST)}&lt;/m:CustAddressHouseFlatNo&gt;
else ()
}
{
if ($fml/fml:DC_KOD_POCZTOWY_DANE_PODST) then
&lt;m:CustAddressPostCode&gt;{data($fml/fml:DC_KOD_POCZTOWY_DANE_PODST)}&lt;/m:CustAddressPostCode&gt;
else ()
}
{
if ($fml/fml:DC_MIASTO_DANE_PODST) then
&lt;m:CustAddressCity&gt;{data($fml/fml:DC_MIASTO_DANE_PODST)}&lt;/m:CustAddressCity&gt;
else ()
}
{
if ($fml/fml:DC_WOJ_KRAJ_DANE_PODST) then
&lt;m:CustAddressCounty&gt;{data($fml/fml:DC_WOJ_KRAJ_DANE_PODST)}&lt;/m:CustAddressCounty&gt;
else ()
}
{
if ($fml/fml:CI_KOD_KRAJU) then
&lt;m:CustAddressCountry&gt;{data($fml/fml:CI_KOD_KRAJU)}&lt;/m:CustAddressCountry&gt;
else ()
}
{
if ($fml/fml:DC_TYP_ADRESU) then
&lt;m:CustAddressForFlag&gt;{data($fml/fml:DC_TYP_ADRESU)}&lt;/m:CustAddressForFlag&gt;
else ()
}
{
if ($fml/fml:DC_REZ_NIEREZ) then
&lt;m:ResidentFlag&gt;{data($fml/fml:DC_REZ_NIEREZ)}&lt;/m:ResidentFlag&gt;
else ()
}
{
if ($fml/fml:DC_ADRES_E_MAIL) then
&lt;m:Email&gt;{data($fml/fml:DC_ADRES_E_MAIL)}&lt;/m:Email&gt;
else ()
}
{
if ($fml/fml:DC_NR_TELEFONU) then
&lt;m:PhoneNo&gt;{data($fml/fml:DC_NR_TELEFONU)}&lt;/m:PhoneNo&gt;
else ()
}
{
if ($fml/fml:DC_NUMER_FAKSU) then
&lt;m:FaxNo&gt;{data($fml/fml:DC_NUMER_FAKSU)}&lt;/m:FaxNo&gt;
else ()
}
{
if ($fml/fml:DC_NR_TELEF_KOMORKOWEGO) then
&lt;m:MobileNo&gt;{data($fml/fml:DC_NR_TELEF_KOMORKOWEGO)}&lt;/m:MobileNo&gt;
else ()
}
{
if ($fml/fml:DC_NUMER_TELEFONU_PRACA) then
&lt;m:BusinessPhoneNo&gt;{data($fml/fml:DC_NUMER_TELEFONU_PRACA)}&lt;/m:BusinessPhoneNo&gt;
else ()
}
{
if ($fml/fml:DC_NR_DOWODU_REGON) then
     if ($fml/fml:DC_TYP_KLIENTA = "F") then
         &lt;m:CustIdCardNo&gt;{data($fml/fml:DC_NR_DOWODU_REGON)}&lt;/m:CustIdCardNo&gt;
     else
         &lt;m:CustIdCardNo&gt;&lt;/m:CustIdCardNo&gt;
else ()
}
{
if ($fml/fml:DC_NR_DOWODU_REGON) then
     if ($fml/fml:DC_TYP_KLIENTA = "F") then
         &lt;m:RegonNo&gt;&lt;/m:RegonNo&gt;
     else
         &lt;m:RegonNo&gt;{data($fml/fml:DC_NR_DOWODU_REGON)}&lt;/m:RegonNo&gt;
else ()
}
{
if ($fml/fml:DC_NR_PESEL) then
&lt;m:CustPesel&gt;{data($fml/fml:DC_NR_PESEL)}&lt;/m:CustPesel&gt;
else ()
}
{
if ($fml/fml:DC_NIP) then
&lt;m:NipNo&gt;{data($fml/fml:DC_NIP)}&lt;/m:NipNo&gt;
else ()
}
{
if ($fml/fml:CI_DATA_NIP) then
&lt;m:NipGrantDate&gt;{data($fml/fml:CI_DATA_NIP)}&lt;/m:NipGrantDate&gt;
else ()
}
{
if ($fml/fml:DC_SPW) then
&lt;m:Spw&gt;{data($fml/fml:DC_SPW)}&lt;/m:Spw&gt;
else ()
}
{
if ($fml/fml:DC_KARTA_WZOROW_PODPISOW) then
&lt;m:SignCardIcbsNo&gt;{data($fml/fml:DC_KARTA_WZOROW_PODPISOW)}&lt;/m:SignCardIcbsNo&gt;
else ()
}
{
if ($fml/fml:CI_NR_ODDZIALU) then
&lt;m:MainIcbsNo&gt;{data($fml/fml:CI_NR_ODDZIALU)}&lt;/m:MainIcbsNo&gt;
else ()
}
{
if ($fml/fml:CI_PRACOWNIK_WPROW) then
&lt;m:CreateFolderEmpSkpNo&gt;{data($fml/fml:CI_PRACOWNIK_WPROW)}&lt;/m:CreateFolderEmpSkpNo&gt;
else ()
}
{
if ($fml/fml:CI_PRACOWNIK_ZMIEN) then
&lt;m:ModifyFolderEmpSkpNo&gt;{data($fml/fml:CI_PRACOWNIK_ZMIEN)}&lt;/m:ModifyFolderEmpSkpNo&gt;
else ()
}
{
if ($fml/fml:CI_DATA_MODYFIKACJI) then
&lt;m:FolderModificationDate&gt;{data($fml/fml:CI_DATA_MODYFIKACJI)}&lt;/m:FolderModificationDate&gt;
else ()
}
{
if ($fml/fml:DC_ZRODLO_DANYCH) then
&lt;m:DataSrc&gt;{data($fml/fml:DC_ZRODLO_DANYCH)}&lt;/m:DataSrc&gt;
else ()
}
{
if ($fml/fml:DC_JEDNOSTKA_KORPORACYJNA) then
&lt;m:CorpoUnitId&gt;{data($fml/fml:DC_JEDNOSTKA_KORPORACYJNA)}&lt;/m:CorpoUnitId&gt;
else ()
}
{
if ($fml/fml:CI_PROCENT_PODATKU) then
&lt;m:TaxPercentage&gt;{fn:round-half-to-even(data($fml/fml:CI_PROCENT_PODATKU), 5)}&lt;/m:TaxPercentage&gt;
else ()
}
{
if ($fml/fml:CI_PROD_INNE_BANKI) then
&lt;m:OtherBankPrds&gt;{data($fml/fml:CI_PROD_INNE_BANKI)}&lt;/m:OtherBankPrds&gt;
else ()
}
{
if ($fml/fml:CI_SERIA_NR_DOK) then
&lt;m:NiNo&gt;{data($fml/fml:CI_SERIA_NR_DOK)}&lt;/m:NiNo&gt;
else ()
}
{
if ($fml/fml:DC_NUMER_PASZPORTU) then
&lt;m:CustPassportNo&gt;{data($fml/fml:DC_NUMER_PASZPORTU)}&lt;/m:CustPassportNo&gt;
else ()
}
{
if ($fml/fml:CI_POSIADA_SAMOCHOD) then
&lt;m:CarOwnerFlag&gt;{data($fml/fml:CI_POSIADA_SAMOCHOD)}&lt;/m:CarOwnerFlag&gt;
else ()
}
{
if ($fml/fml:DC_PLEC) then
&lt;m:CustSex&gt;{data($fml/fml:DC_PLEC)}&lt;/m:CustSex&gt;
else ()
}
{
if ($fml/fml:DC_IMIE) then
&lt;m:CustFirstName&gt;{data($fml/fml:DC_IMIE)}&lt;/m:CustFirstName&gt;
else ()
}
{
if ($fml/fml:DC_DRUGIE_IMIE) then
&lt;m:CustSecondName&gt;{data($fml/fml:DC_DRUGIE_IMIE)}&lt;/m:CustSecondName&gt;
else ()
}
{
if ($fml/fml:DC_NAZWISKO) then
&lt;m:CustSurname&gt;{data($fml/fml:DC_NAZWISKO)}&lt;/m:CustSurname&gt;
else ()
}
{
if ($fml/fml:DC_IMIE_OJCA) then
&lt;m:FatherName&gt;{data($fml/fml:DC_IMIE_OJCA)}&lt;/m:FatherName&gt;
else ()
}
{
if ($fml/fml:DC_NAZWISKO_PANIENSKIE_MATKI) then
&lt;m:MotherMaidenName&gt;{data($fml/fml:DC_NAZWISKO_PANIENSKIE_MATKI)}&lt;/m:MotherMaidenName&gt;
else ()
}{
if ($fml/fml:CI_OBYWATELSTWO) then
&lt;m:Nationality&gt;{data($fml/fml:CI_OBYWATELSTWO)}&lt;/m:Nationality&gt;
else ()
}
{
if ($fml/fml:DC_DATA_URODZENIA) then
&lt;m:CustBirthDate&gt;{data($fml/fml:DC_DATA_URODZENIA)}&lt;/m:CustBirthDate&gt;
else ()
}
{
if ($fml/fml:DC_MIEJSCE_URODZENIA) then
&lt;m:CustBirthPlace&gt;{data($fml/fml:DC_MIEJSCE_URODZENIA)}&lt;/m:CustBirthPlace&gt;
else ()
}
{
if ($fml/fml:DC_STAN_CYWILNY) then
&lt;m:CivilStatus&gt;{data($fml/fml:DC_STAN_CYWILNY)}&lt;/m:CivilStatus&gt;
else ()
}
{
if ($fml/fml:DC_KOD_JEZYKA) then
&lt;m:Language&gt;{data($fml/fml:DC_KOD_JEZYKA)}&lt;/m:Language&gt;
else ()
}
{
if ($fml/fml:DC_UCZELNIA) then
&lt;m:HighSchoolName&gt;{data($fml/fml:DC_UCZELNIA)}&lt;/m:HighSchoolName&gt;
else ()
}
{
if ($fml/fml:DC_TYP_SZKOLY) then
&lt;m:SchoolType&gt;{data($fml/fml:DC_TYP_SZKOLY)}&lt;/m:SchoolType&gt;
else ()
}
{
if ($fml/fml:DC_PLANOW_DATA_UKON_SZK) then
&lt;m:PlanEndSchoolDate&gt;{data($fml/fml:DC_PLANOW_DATA_UKON_SZK)}&lt;/m:PlanEndSchoolDate&gt;
else ()
}
{
if ($fml/fml:DC_WYKSZTALCENIE) then
&lt;m:EduLevel&gt;{data($fml/fml:DC_WYKSZTALCENIE)}&lt;/m:EduLevel&gt;
else ()
}
{
if ($fml/fml:DC_CHARAKTER_WYKSZTALC) then
&lt;m:EducationProfile&gt;{data($fml/fml:DC_CHARAKTER_WYKSZTALC)}&lt;/m:EducationProfile&gt;
else ()
}
{
if ($fml/fml:DC_SYSTEM_STUDIOW) then
&lt;m:StudyType&gt;{data($fml/fml:DC_SYSTEM_STUDIOW)}&lt;/m:StudyType&gt;
else ()
}
{
if ($fml/fml:DC_SPECJALIZACJA) then
&lt;m:SchoolSpecialization&gt;{data($fml/fml:DC_SPECJALIZACJA)}&lt;/m:SchoolSpecialization&gt;
else ()
}
{
if ($fml/fml:DC_SLUZBA_WOJSKOWA) then
&lt;m:MilitaryService&gt;{data($fml/fml:DC_SLUZBA_WOJSKOWA)}&lt;/m:MilitaryService&gt;
else ()
}
{
if ($fml/fml:DC_WSPOLNOTA_MAJATK) then
&lt;m:JointPropertyFlag&gt;{data($fml/fml:DC_WSPOLNOTA_MAJATK)}&lt;/m:JointPropertyFlag&gt;
else ()
}
{
if ($fml/fml:DC_LICZBA_OS_WE_WSP_GOSP_D) then
&lt;m:HousePeopleCount&gt;{data($fml/fml:DC_LICZBA_OS_WE_WSP_GOSP_D)}&lt;/m:HousePeopleCount&gt;
else ()
}
{
if ($fml/fml:DC_LICZBA_OSOB_NA_UTRZ) then
&lt;m:KeptPeopleCount&gt;{data($fml/fml:DC_LICZBA_OSOB_NA_UTRZ)}&lt;/m:KeptPeopleCount&gt;
else ()
}
{
if ($fml/fml:DC_POSIADA_DOM_MIESZK) then
&lt;m:HouseOwnerFlag&gt;{data($fml/fml:DC_POSIADA_DOM_MIESZK)}&lt;/m:HouseOwnerFlag&gt;
else ()
}
{
if ($fml/fml:CI_TYP_WLASNOSCI) then
&lt;m:HousePropertyType&gt;{data($fml/fml:CI_TYP_WLASNOSCI)}&lt;/m:HousePropertyType&gt;
else ()
}
{
if ($fml/fml:DC_KOD_ZAWODU) then
&lt;m:JobCode&gt;{data($fml/fml:DC_KOD_ZAWODU)}&lt;/m:JobCode&gt;
else ()
}
{
if ($fml/fml:CI_KOD_ZAWODU_WYUCZONEGO) then
&lt;m:Profession&gt;{data($fml/fml:CI_KOD_ZAWODU_WYUCZONEGO)}&lt;/m:Profession&gt;
else ()
}
{
if ($fml/fml:CI_SEKTOR_ZATRUDNIENIA) then
&lt;m:WorkPlace&gt;{data($fml/fml:CI_SEKTOR_ZATRUDNIENIA)}&lt;/m:WorkPlace&gt;
else ()
}
{
if ($fml/fml:DC_MIES_DOCHOD_NETTO_W_PLN) then
&lt;m:MonthIncome&gt;{data($fml/fml:DC_MIES_DOCHOD_NETTO_W_PLN)}&lt;/m:MonthIncome&gt;
else ()
}
{
if ($fml/fml:CI_ZRODLO_DOCHODU) then
&lt;m:IncomeSrc&gt;{data($fml/fml:CI_ZRODLO_DOCHODU)}&lt;/m:IncomeSrc&gt;
else ()
}
{
if ($fml/fml:CI_DOD_DOCHOD_MIES_NETTO) then
&lt;m:ExtraMonthIncome&gt;{data($fml/fml:CI_DOD_DOCHOD_MIES_NETTO)}&lt;/m:ExtraMonthIncome&gt;
else ()
}{
if ($fml/fml:CI_ZRODLO_DODAT_DOCH) then
&lt;m:ExtraIncomeSrc&gt;{data($fml/fml:CI_ZRODLO_DODAT_DOCH)}&lt;/m:ExtraIncomeSrc&gt;
else ()
}
{
if ($fml/fml:CI_RODZAJ_OBCIAZENIA) then
&lt;m:ChargeType&gt;{data($fml/fml:CI_RODZAJ_OBCIAZENIA)}&lt;/m:ChargeType&gt;
else ()
}
{
if ($fml/fml:CI_RODZAJ_OBC_INNE) then
&lt;m:ChargeTypeOther&gt;{data($fml/fml:CI_RODZAJ_OBC_INNE)}&lt;/m:ChargeTypeOther&gt;
else ()
}
{
if ($fml/fml:CI_LACZNA_WYS_OBC) then
&lt;m:SumMonthCharge&gt;{data($fml/fml:CI_LACZNA_WYS_OBC)}&lt;/m:SumMonthCharge&gt;
else ()
}
{
if ($fml/fml:DC_DATA_SMIERCI) then
&lt;m:CustDeathDate&gt;{data($fml/fml:DC_DATA_SMIERCI)}&lt;/m:CustDeathDate&gt;
else ()
}
{
if ($fml/fml:CI_LICZBA_POLIS) then
&lt;m:PolicesCount&gt;{data($fml/fml:CI_LICZBA_POLIS)}&lt;/m:PolicesCount&gt;
else ()
}
{
if ($fml/fml:CI_SUMA_POLIS_NA_ZYCIE) then
&lt;m:PolicesSum&gt;{data($fml/fml:CI_SUMA_POLIS_NA_ZYCIE)}&lt;/m:PolicesSum&gt;
else ()
}
{
if ($fml/fml:DC_UBEZPIECZENIE_NA_ZYCIE) then
&lt;m:LifeInsurranceFlag&gt;{data($fml/fml:DC_UBEZPIECZENIE_NA_ZYCIE)}&lt;/m:LifeInsurranceFlag&gt;
else ()
}
{
if ($fml/fml:DC_UBEZP_OD_NASTEPSTW_NW) then
&lt;m:NwInsurranceFlag&gt;{data($fml/fml:DC_UBEZP_OD_NASTEPSTW_NW)}&lt;/m:NwInsurranceFlag&gt;
else ()
}
{
if ($fml/fml:DC_UBEZPIECZENIE_EMERYTALNE) then
&lt;m:RetireInsurranceFlag&gt;{data($fml/fml:DC_UBEZPIECZENIE_EMERYTALNE)}&lt;/m:RetireInsurranceFlag&gt;
else ()
}
{
if ($fml/fml:DC_UBEZP_MAJATKOWE_MIES) then
&lt;m:WealthInsurranceFlag&gt;{data($fml/fml:DC_UBEZP_MAJATKOWE_MIES)}&lt;/m:WealthInsurranceFlag&gt;
else ()
}
{
if ($fml/fml:DC_UBEZPIECZENIA_INNE) then
&lt;m:OtherInsurranceFlag&gt;{data($fml/fml:DC_UBEZPIECZENIA_INNE)}&lt;/m:OtherInsurranceFlag&gt;
else ()
}
{
if ($fml/fml:CI_KRS) then
&lt;m:RegisterNo&gt;{data($fml/fml:CI_KRS)}&lt;/m:RegisterNo&gt;
else ()
}
{
if ($fml/fml:DC_EKD) then
&lt;m:EkdCode&gt;{data($fml/fml:DC_EKD)}&lt;/m:EkdCode&gt;
else ()
}
{
if ($fml/fml:CI_LICZBA_PLACOWEK) then
&lt;m:AgencyCount&gt;{data($fml/fml:CI_LICZBA_PLACOWEK)}&lt;/m:AgencyCount&gt;
else ()
}
{
if ($fml/fml:DC_LICZBA_ZATRUDNIONYCH) then
&lt;m:EmployedCount&gt;{data($fml/fml:DC_LICZBA_ZATRUDNIONYCH)}&lt;/m:EmployedCount&gt;
else ()
}
{
if ($fml/fml:DC_DATA_BANKRUCTWA) then
&lt;m:BusinessEndDate&gt;{data($fml/fml:DC_DATA_BANKRUCTWA)}&lt;/m:BusinessEndDate&gt;
else ()
}
{
if ($fml/fml:CI_STRONA_WWW) then
&lt;m:WebPage&gt;{data($fml/fml:CI_STRONA_WWW)}&lt;/m:WebPage&gt;
else ()
}
{
if ($fml/fml:CI_DATA_ROZP_DZIAL) then
&lt;m:BusinessInitDate&gt;{data($fml/fml:CI_DATA_ROZP_DZIAL)}&lt;/m:BusinessInitDate&gt;
else ()
}
{
if ($fml/fml:CI_KWOTA_ZADLUZENIA) then
&lt;m:DebtValue&gt;{data($fml/fml:CI_KWOTA_ZADLUZENIA)}&lt;/m:DebtValue&gt;
else ()
}
{
if ($fml/fml:CI_NUMER_SWIFT) then
&lt;m:SwiftNo&gt;{data($fml/fml:CI_NUMER_SWIFT)}&lt;/m:SwiftNo&gt;
else ()
}
{
if ($fml/fml:CI_FOP) then
&lt;m:OrgLawForm&gt;{data($fml/fml:CI_FOP)}&lt;/m:OrgLawForm&gt;
else ()
}
{
if ($fml/fml:CI_NAZWA_ORG_REJESTR) then
&lt;m:RegOrgName&gt;{data($fml/fml:CI_NAZWA_ORG_REJESTR)}&lt;/m:RegOrgName&gt;
else ()
}
{
if ($fml/fml:CI_ULICA_ORG_REJESTR) then
&lt;m:RegOrgStreet&gt;{data($fml/fml:CI_ULICA_ORG_REJESTR)}&lt;/m:RegOrgStreet&gt;
else ()
}
{
if ($fml/fml:CI_NR_POSES_LOKALU_ORG_REJ) then
&lt;m:RegOrgHouseNo&gt;{data($fml/fml:CI_NR_POSES_LOKALU_ORG_REJ)}&lt;/m:RegOrgHouseNo&gt;
else ()
}
{
if ($fml/fml:CI_KOD_POCZT_ORG_REJESTR) then
&lt;m:RegOrgPostCode&gt;{data($fml/fml:CI_KOD_POCZT_ORG_REJESTR)}&lt;/m:RegOrgPostCode&gt;
else ()
}
{
if ($fml/fml:CI_MIASTO_ORG_REJESTR) then
&lt;m:RegOrgCity&gt;{data($fml/fml:CI_MIASTO_ORG_REJESTR)}&lt;/m:RegOrgCity&gt;
else ()
}
{
if ($fml/fml:CI_KRAJ_ORG_REJESTR) then
&lt;m:RegOrgCountry&gt;{data($fml/fml:CI_KRAJ_ORG_REJESTR)}&lt;/m:RegOrgCountry&gt;
else ()
}
{
if ($fml/fml:CI_DATA_WPISU_DO_REJESTR) then
&lt;m:RegOrgInitDate&gt;{data($fml/fml:CI_DATA_WPISU_DO_REJESTR)}&lt;/m:RegOrgInitDate&gt;
else ()
}
{
if ($fml/fml:CI_STATUS_SIEDZIBY) then
&lt;m:HeadquaterFlag&gt;{data($fml/fml:CI_STATUS_SIEDZIBY)}&lt;/m:HeadquaterFlag&gt;
else ()
}
{
if ($fml/fml:CI_AUDYTOR) then
&lt;m:Auditor&gt;{data($fml/fml:CI_AUDYTOR)}&lt;/m:Auditor&gt;
else ()
}
{
if ($fml/fml:CI_KAPITAL_ZALOZYCIELSKI) then
&lt;m:OriginalCapital&gt;{data($fml/fml:CI_KAPITAL_ZALOZYCIELSKI)}&lt;/m:OriginalCapital&gt;
else ()
}

{
if ($fml/fml:CI_WARTOSC_AKCJI) then
&lt;m:SharesValue&gt;{data($fml/fml:CI_WARTOSC_AKCJI)}&lt;/m:SharesValue&gt;
else ()
}
{
if ($fml/fml:CI_WARTOSC_NIERUCHOMOSCI) then
&lt;m:ImmovablesValue&gt;{data($fml/fml:CI_WARTOSC_NIERUCHOMOSCI)}&lt;/m:ImmovablesValue&gt;
else ()
}
{
if ($fml/fml:CI_WARTOSC_SRODKOW_MATER) then
&lt;m:SurfaceValue&gt;{data($fml/fml:CI_WARTOSC_SRODKOW_MATER)}&lt;/m:SurfaceValue&gt;
else ()
}
{
if ($fml/fml:CI_INNE_AKTYWA) then
&lt;m:OtherAssets&gt;{data($fml/fml:CI_INNE_AKTYWA)}&lt;/m:OtherAssets&gt;
else ()
}
{
if ($fml/fml:CI_ZOBOWIAZANIA_PODATKOWE) then
&lt;m:TaxBorrowing&gt;{fn:round-half-to-even(data($fml/fml:CI_ZOBOWIAZANIA_PODATKOWE), 5)}&lt;/m:TaxBorrowing&gt;
else ()
}
{
if ($fml/fml:CI_INNE_ZOBOWIAZANIA) then
&lt;m:OtherBorrowing&gt;{data($fml/fml:CI_INNE_ZOBOWIAZANIA)}&lt;/m:OtherBorrowing&gt;
else ()
}
{
if ($fml/fml:CI_DATA_AKTUAL_DANYCH_FIN) then
&lt;m:FinancialUpdateDate&gt;{data($fml/fml:CI_DATA_AKTUAL_DANYCH_FIN)}&lt;/m:FinancialUpdateDate&gt;
else ()
}
{
if ($fml/fml:CI_PRZYCHODY_INST) then
&lt;m:Income&gt;{data($fml/fml:CI_PRZYCHODY_INST)}&lt;/m:Income&gt;
else ()
}
{
if ($fml/fml:DC_ULICA_ADRES_ALT) then
&lt;m:CustCorrAddressStreet&gt;{data($fml/fml:DC_ULICA_ADRES_ALT)}&lt;/m:CustCorrAddressStreet&gt;
else ()
}
{
if ($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT) then
&lt;m:CustCorrAddressHouseFlatNo&gt;{data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT)}&lt;/m:CustCorrAddressHouseFlatNo&gt;
else ()
}
{
if ($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT) then
&lt;m:CustCorrAddressPostCode&gt;{data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT)}&lt;/m:CustCorrAddressPostCode&gt;
else ()
}
	{
	if ($fml/fml:DC_MIASTO_ADRES_ALT) then
	&lt;m:CustCorrAddressCity&gt;{data($fml/fml:DC_MIASTO_ADRES_ALT)}&lt;/m:CustCorrAddressCity&gt;
	else ()
	}
	{
	if ($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT) then
	&lt;m:CustCorrAddressCounty&gt;{data($fml/fml:DC_WOJEWODZTWO_KRAJ_ADRES_ALT)}&lt;/m:CustCorrAddressCounty&gt;
	else ()
	}
	{
	if ($fml/fml:CI_KOD_KRAJU_KORESP) then
	&lt;m:CustCorrAddressCountry&gt;{data($fml/fml:CI_KOD_KRAJU_KORESP)}&lt;/m:CustCorrAddressCountry&gt;
	else ()
	}
        {
        if ($fml/fml:CI_DATA_OD) then
        &lt;m:CustCorrValidDateFrom&gt;{data($fml/fml:CI_DATA_OD)}&lt;/m:CustCorrValidDateFrom&gt;
        else ()
        }
	{
	if ($fml/fml:CI_DATA_DO) then
	&lt;m:CustCorrValidDateTo&gt;{data($fml/fml:CI_DATA_DO)}&lt;/m:CustCorrValidDateTo&gt;
	else ()
	}
	{
	if ($fml/fml:CI_TYP_ADRESU_KORESP) then
	&lt;m:CustCorrAddressForFlag&gt;{data($fml/fml:CI_TYP_ADRESU_KORESP)}&lt;/m:CustCorrAddressForFlag&gt;
	else ()
	}
	{
	if ($fml/fml:CI_NAZWA_PRACODAWCY) then
	&lt;m:EmpName&gt;{data($fml/fml:CI_NAZWA_PRACODAWCY)}&lt;/m:EmpName&gt;
	else ()
	}
	{
	if ($fml/fml:CI_ULICA_PRACODAWCY) then
	&lt;m:EmpAddressStreet&gt;{data($fml/fml:CI_ULICA_PRACODAWCY)}&lt;/m:EmpAddressStreet&gt;
	else ()
	}
	{
	if ($fml/fml:CI_NUMER_POSESJI_PRAC) then
	&lt;m:EmpAddressHouseFlatNo&gt;{data($fml/fml:CI_NUMER_POSESJI_PRAC)}&lt;/m:EmpAddressHouseFlatNo&gt;
	else ()
	}
	{
	if ($fml/fml:CI_KOD_POCZT_PRAC) then
	&lt;m:EmpAddressPostCode&gt;{data($fml/fml:CI_KOD_POCZT_PRAC)}&lt;/m:EmpAddressPostCode&gt;
	else ()
	}
	{
	if ($fml/fml:CI_MIASTO_PRACODAWCY) then
	&lt;m:EmpAddressCity&gt;{data($fml/fml:CI_MIASTO_PRACODAWCY)}&lt;/m:EmpAddressCity&gt;
	else ()
	}
	{
	if ($fml/fml:CI_WOJEWODZTWO_PRAC) then
	&lt;m:EmpAddressCounty&gt;{data($fml/fml:CI_WOJEWODZTWO_PRAC)}&lt;/m:EmpAddressCounty&gt;
	else ()
	}
	{
	if ($fml/fml:CI_KOD_KRAJU_PRAC) then
	&lt;m:EmpAddressCountry&gt;{data($fml/fml:CI_KOD_KRAJU_PRAC)}&lt;/m:EmpAddressCountry&gt;
	else ()
	}
	{
	if ($fml/fml:CI_TYP_ADRESU_PRAC) then
	&lt;m:EmpAddressForFlag&gt;{data($fml/fml:CI_TYP_ADRESU_PRAC)}&lt;/m:EmpAddressForFlag&gt;
	else ()
	}
	{
	if ($fml/fml:CI_NR_TELEFONU_PRAC) then
	&lt;m:EmpPhone&gt;{data($fml/fml:CI_NR_TELEFONU_PRAC)}&lt;/m:EmpPhone&gt;
	else ()
	}
	{
	if ($fml/fml:DC_STATUS_PRACOWNIKA) then
	&lt;m:EmployeeStatus&gt;{data($fml/fml:DC_STATUS_PRACOWNIKA)}&lt;/m:EmployeeStatus&gt;
	else ()
	}
	{
	if ($fml/fml:DC_DATA_PODJECIA_PRACY) then
	&lt;m:JobStartDate&gt;{data($fml/fml:DC_DATA_PODJECIA_PRACY)}&lt;/m:JobStartDate&gt;
	else ()
	}
	{
	if ($fml/fml:CI_KRAJ_ZAMIESZKANIA) then
	&lt;m:ResidenceCountry&gt;{data($fml/fml:CI_KRAJ_ZAMIESZKANIA)}&lt;/m:ResidenceCountry&gt;
	else ()
	}
	{
	if ($fml/fml:CI_KRAJ_PROWADZENIA_DZIAL) then
	&lt;m:BusinessCountry&gt;{data($fml/fml:CI_KRAJ_PROWADZENIA_DZIAL)}&lt;/m:BusinessCountry&gt;
	else ()
	}
	{
	if ($fml/fml:CI_STRUKTURA_ZLOZONA) then
	&lt;m:ComplexStructFlag&gt;{data($fml/fml:CI_STRUKTURA_ZLOZONA)}&lt;/m:ComplexStructFlag&gt;
	else ()
	}
	{
	if ($fml/fml:CI_RODZ_DZIALALNOSCI) then
	&lt;m:BusinessType&gt;{data($fml/fml:CI_RODZ_DZIALALNOSCI)}&lt;/m:BusinessType&gt;
	else ()
	}
	{
	if ($fml/fml:DC_PODPISANA_UMOWA_LIMIT_W_KO) then
	&lt;m:PodpisanaUmowaLimitWKo&gt;{data($fml/fml:DC_PODPISANA_UMOWA_LIMIT_W_KO)}&lt;/m:PodpisanaUmowaLimitWKo&gt;
	else ()
	}
        {
        if ($fml/fml:CI_DATA_ODDZIAL_OBS) then
        &lt;m:AssignUnitDate&gt;{data($fml/fml:CI_DATA_ODDZIAL_OBS)}&lt;/m:AssignUnitDate&gt;
        else ()
        }
        {
        if ($fml/fml:CI_KRAJ_POCHODZENIA) then
        &lt;m:CustBirthCountry&gt;{data($fml/fml:CI_KRAJ_POCHODZENIA)}&lt;/m:CustBirthCountry&gt;
        else ()
        }
        {
        if ($fml/fml:DC_ROK_STUDIOW) then
        &lt;m:StudyYear&gt;{data($fml/fml:DC_ROK_STUDIOW)}&lt;/m:StudyYear&gt;
        else ()
        }
        {
        if ($fml/fml:CI_ODDZIAL_OBSLUGUJACY) then
        &lt;m:OperationIcbsNo&gt;{data($fml/fml:CI_ODDZIAL_OBSLUGUJACY)}&lt;/m:OperationIcbsNo&gt;
        else ()
        }
        {
        if ($fml/fml:CI_ZYSKI_INST) then
        &lt;m:GainingsValue&gt;{data($fml/fml:CI_ZYSKI_INST)}&lt;/m:GainingsValue&gt;
        else ()
        }
        {
        if ($fml/fml:DC_WYKONYWANA_PRACA) then
        &lt;m:TempJob&gt;{data($fml/fml:DC_WYKONYWANA_PRACA)}&lt;/m:TempJob&gt;
        else ()
        }
        {
        if ($fml/fml:DC_KOD_EKD_PRACODAW) then
        &lt;m:KodEkdPracodaw&gt;{data($fml/fml:DC_KOD_EKD_PRACODAW)}&lt;/m:KodEkdPracodaw&gt;
        else ()
        }
        {
        if ($fml/fml:CI_UBEZP_NA_ZYCIE) then
        &lt;m:LifeInsurerName&gt;{data($fml/fml:CI_UBEZP_NA_ZYCIE)}&lt;/m:LifeInsurerName&gt;
        else ()
        }
        {
        if ($fml/fml:CI_UBEZP_NW) then
        &lt;m:NwInsurerName&gt;{data($fml/fml:CI_UBEZP_NW)}&lt;/m:NwInsurerName&gt;
        else ()
        }
        {
        if ($fml/fml:CI_UBEZP_EMERYTALNE) then
        &lt;m:RetireInsurerName&gt;{data($fml/fml:CI_UBEZP_EMERYTALNE)}&lt;/m:RetireInsurerName&gt;
        else ()
        }
        {
        if ($fml/fml:CI_UBEZP_MAJATKOWE) then
        &lt;m:WealthInsurerName&gt;{data($fml/fml:CI_UBEZP_MAJATKOWE)}&lt;/m:WealthInsurerName&gt;
        else ()
        }
        {
        if ($fml/fml:CI_UBEZP_INNE) then
        &lt;m:OtherInsurerName&gt;{data($fml/fml:CI_UBEZP_INNE)}&lt;/m:OtherInsurerName&gt;
        else ()
        }
        {
        if ($fml/fml:CI_DOK_TOZSAMOSCI) then
        &lt;m:TypNiNo&gt;{data($fml/fml:CI_DOK_TOZSAMOSCI)}&lt;/m:TypNiNo&gt;
        else ()
        }
        {
        if ($fml/fml:DC_KIERUNEK_STUDIOW) then
        &lt;m:KierunekStudiow&gt;{data($fml/fml:DC_KIERUNEK_STUDIOW)}&lt;/m:KierunekStudiow&gt;
        else ()
        }
        {
        if ($fml/fml:CI_KAPITAL_ZALOZ_OPLACONY) then
        &lt;m:KapitalZalozOplacony&gt;{data($fml/fml:CI_KAPITAL_ZALOZ_OPLACONY)}&lt;/m:KapitalZalozOplacony&gt;
        else ()
        }
		&lt;/m:CRMGetProspFolderResponse&gt;

};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetProspFolderResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>