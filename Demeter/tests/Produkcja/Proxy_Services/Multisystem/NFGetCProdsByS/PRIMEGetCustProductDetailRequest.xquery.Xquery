<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductDetail($fml as element(fml:FML32))
	as element(m:PRIMEGetCustProductDetail) {
		&lt;m:PRIMEGetCustProductDetail&gt;
			(:{
				if($fml/fml:CI_ID_WEW_PRAC)
					then &lt;m:CI_ID_WEW_PRAC&gt;{ data($fml/fml:CI_ID_WEW_PRAC) }&lt;/m:CI_ID_WEW_PRAC&gt;
					else ()
			}:)
			{
				if($fml/fml:NF_MSHEAD_COMPANYID)
					then &lt;m:CI_ID_SPOLKI&gt;{ data($fml/fml:NF_MSHEAD_COMPANYID) }&lt;/m:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($fml/fml:NF_CUSTOM_CUSTOMERNUMBER)
					then &lt;m:DC_NUMER_KLIENTA&gt;{ data($fml/fml:NF_CUSTOM_CUSTOMERNUMBER) }&lt;/m:DC_NUMER_KLIENTA&gt;
					else ()
			}
			(:{
				if($fml/fml:DC_NR_DOWODU_REGON)
					then &lt;m:DC_NR_DOWODU_REGON&gt;{ data($fml/fml:DC_NR_DOWODU_REGON) }&lt;/m:DC_NR_DOWODU_REGON&gt;
					else ()
			}
			{
				if($fml/fml:DC_NIP)
					then &lt;m:DC_NIP&gt;{ data($fml/fml:DC_NIP) }&lt;/m:DC_NIP&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_PESEL)
					then &lt;m:DC_NR_PESEL&gt;{ data($fml/fml:DC_NR_PESEL) }&lt;/m:DC_NR_PESEL&gt;
					else ()
			}:)
			{
				if($fml/fml:NF_ACCOUN_ACCOUNTNUMBER)
					then &lt;m:DC_NR_RACHUNKU&gt;{ data($fml/fml:NF_ACCOUN_ACCOUNTNUMBER) }&lt;/m:DC_NR_RACHUNKU&gt;
                                       else ()
			}
			{
				if($fml/fml:NF_CTRL_OPTION)
					then &lt;m:CI_OPCJA&gt;{ data($fml/fml:NF_CTRL_OPTION) }&lt;/m:CI_OPCJA&gt;
					else ()
			}
			{
				if($fml/fml:NF_CTRL_SYSTEMID)
					then &lt;m:CI_ID_SYS&gt;{ data($fml/fml:NF_CTRL_SYSTEMID) }&lt;/m:CI_ID_SYS&gt;
					else ()
			}


		&lt;/m:PRIMEGetCustProductDetail&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapPRIMEGetCustProductDetail($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>