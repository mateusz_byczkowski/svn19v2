<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(: Change Log
v.1.1 2010-01-15 PKLI NP1972_1 Zamiana mapowania dla pola NF_ACCOUN_ACCOUNTOPENDATE (z contract-start na proposal)
v.1.2 2010-03-05 PKLI NP2037_1 CR1 Zaprzestanie wyliczania salda ubezpieczeń JV
v.1.3 2010-05-06 PKLI NP1972_3 CR2 Filtrowanie produktów Streamline na podstawie statusu aktywności 
v.1.4 2010-05-17 PKLI T47597 Brak obsługi numeru wniosku ubezpieczeń Streamline
:)

declare namespace wsdl-jv = "http://jv.channel.cu.com.pl/cmf/wsdl-jv";
declare namespace cmf-holding = "http://jv.channel.cu.com.pl/cmf-holding-types";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";



declare function xf:calculatePageStart ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then 1
else if ($actioncode="N")
   then xs:integer(min(($records , $oldstop + 1)))
else if ($actioncode="P")
   then max((1,$oldstart - $pagesize))
else if ($actioncode="L")
   then max((1,$records  - $pagesize))
else 0
};

declare function xf:calculatePageStop ($pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
																	$actioncode as xs:string , $records as xs:integer) as xs:integer{
if ($actioncode="F")
   then xs:integer(min(($pagesize,$records)))
else if ($actioncode="N")
   then xs:integer(min(($pagesize + $oldstop,$records)))
else if ($actioncode="P")
   then max((1,$oldstart - 1))
else if ($actioncode="L")
   then $records
else xs:integer(0)
};


declare function xf:calculateHasNext( $pagestart as xs:integer,$pagestop as xs:integer, 
                                                     $records as xs:integer, $actioncode as xs:string) as xs:integer{
    if ($actioncode != "P") 
     then  if ($pagestop &lt; $records)
           then 1
           else 0
    else if ($pagestart &gt; 1)
           then 1
           else 0 
};

declare function xf:mapProduct($prod as element(contract),$system_id as xs:string)  as element()* {
	&lt;fml:NF_ACCOUN_ACCOUNTNUMBER  nil="true" /&gt;
        ,
        &lt;NF_PRODUA_CODEPRODUCTAREA&gt;4&lt;/NF_PRODUA_CODEPRODUCTAREA&gt;
	, 
	(: if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))&gt;0)
	  then &lt;fml:NF_ACCOUN_ACCOUNTNUMBER  nil="true" /&gt;
	  else &lt;fml:NF_ACCOUN_ACCOUNTNUMBER &gt;{substring(data($prod/m:DC_NR_RACHUNKU),2)}&lt;/fml:NF_ACCOUN_ACCOUNTNUMBER&gt;
	, 
	if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))&gt;0)
	  then &lt;fml:NF_DEBITC_VIRTUALCARDNBR&gt;{data($prod/m:DC_NR_ALTERNATYWNY)}&lt;/fml:NF_DEBITC_VIRTUALCARDNBR&gt;
	  else &lt;fml:NF_DEBITC_VIRTUALCARDNBR nil="true" /&gt;
	,
	if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))&gt;0)
	  then &lt;fml:NF_DEBITC_CARDNBR&gt;{substring(data($prod/m:DC_NR_RACHUNKU),2)}&lt;/fml:NF_DEBITC_CARDNBR&gt;
	  else &lt;fml:NF_DEBITC_CARDNBR nil="true" /&gt;
	,
    if ($prod/m:DC_NR_ALTERNATYWNY and string-length(data($prod/m:DC_NR_ALTERNATYWNY))&gt;0)
      then &lt;NF_PRODUA_CODEPRODUCTAREA&gt;1&lt;/NF_PRODUA_CODEPRODUCTAREA&gt;
      else &lt;NF_PRODUA_CODEPRODUCTAREA&gt;2&lt;/NF_PRODUA_CODEPRODUCTAREA&gt;
	, :)

      &lt;fml:NF_IS_COOWNER&gt;N&lt;/fml:NF_IS_COOWNER&gt;
	,
    (: if ($prod/m:CI_PRODUCT_ATT1 and string-length(data($prod/m:CI_PRODUCT_ATT1))&gt;0)
      then &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT&gt;{data($prod/m:CI_PRODUCT_ATT1)}&lt;/fml:NF_ATTRPG_FIRSTPRODUCTFEAT&gt;    
      else &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT nil="true"/&gt;
	,
   if ($prod/m:CI_PRODUCT_ATT2 and string-length(data($prod/m:CI_PRODUCT_ATT2))&gt;0)
      then &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA&gt;{data($prod/m:CI_PRODUCT_ATT2)}&lt;/fml:NF_ATTRPG_SECONDPRODUCTFEA&gt;    
      else &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA nil="true"/&gt;
	,			
    if ($prod/m:CI_PRODUCT_ATT3 and string-length(data($prod/m:CI_PRODUCT_ATT3))&gt;0)
      then &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT&gt;{data($prod/m:CI_PRODUCT_ATT3)}&lt;/fml:NF_ATTRPG_THIRDPRODUCTFEAT&gt;    
      else &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/&gt;
	,
    if ($prod/m:CI_PRODUCT_ATT4 and string-length(data($prod/m:CI_PRODUCT_ATT4))&gt;0)
      then &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA&gt;{data($prod/m:CI_PRODUCT_ATT4)}&lt;/fml:NF_ATTRPG_FOURTHPRODUCTFEA&gt;    
      else &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/&gt;
	,
    if ($prod/m:CI_PRODUCT_ATT5 and string-length(data($prod/m:CI_PRODUCT_ATT5))&gt;0)
      then &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT&gt;{data($prod/m:CI_PRODUCT_ATT5)}&lt;/fml:NF_ATTRPG_FIFTHPRODUCTFEAT&gt;    
      else &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT nil="true"/&gt;
    ,:)
     &lt;fml:NF_ATTRPG_FIRSTPRODUCTFEAT&gt;{data($prod/@product-type)}&lt;/fml:NF_ATTRPG_FIRSTPRODUCTFEAT&gt;
     ,
    &lt;fml:NF_ATTRPG_SECONDPRODUCTFEA nil="true"/&gt;
     ,
    &lt;fml:NF_ATTRPG_THIRDPRODUCTFEAT nil="true"/&gt;
     ,
    &lt;fml:NF_ATTRPG_FOURTHPRODUCTFEA nil="true"/&gt;
     ,
    &lt;fml:NF_ATTRPG_FIFTHPRODUCTFEAT&gt;PLN&lt;/fml:NF_ATTRPG_FIFTHPRODUCTFEAT&gt;
     ,  
    if ($prod/level)
     then &lt;fml:NF_PRODUD_SORCEPRODUCTCODE&gt;{concat(data($prod/@product-type), data($prod/level))}&lt;/fml:NF_PRODUD_SORCEPRODUCTCODE&gt;
     else &lt;fml:NF_PRODUD_SORCEPRODUCTCODE&gt;{concat(data($prod/@product-type), "~")}&lt;/fml:NF_PRODUD_SORCEPRODUCTCODE&gt;
     ,
    &lt;fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;1&lt;/fml:NF_ACCOUN_ALTERNATIVEADDRE&gt;
     , 			
    (:1.2   if ($prod/amount)
      then &lt;fml:NF_ACCOUN_CURRENTBALANCE&gt;{data($prod/amount)}&lt;/fml:NF_ACCOUN_CURRENTBALANCE&gt;
      else &lt;fml:NF_ACCOUN_CURRENTBALANCE nil="true" /&gt;
    , 1.2 :)	
     &lt;fml:NF_ACCOUN_CURRENTBALANCE nil="true" /&gt; (:1.2 :)

    ,	
    (:if ($prod/m:B_BLOKADA)
      then &lt;fml:NF_HOLD_HOLDAMOUNT&gt;{data($prod/m:B_BLOKADA)}&lt;/fml:NF_HOLD_HOLDAMOUNT&gt;
      else &lt;fml:NF_HOLD_HOLDAMOUNT&gt;0&lt;/fml:NF_HOLD_HOLDAMOUNT&gt;
    ,			
    if ($prod/m:B_DOST_SRODKI)
      then &lt;fml:NF_ACCOUN_AVAILABLEBALANCE&gt;{data($prod/m:B_DOST_SRODKI)}&lt;/fml:NF_ACCOUN_AVAILABLEBALANCE&gt;
      else &lt;fml:NF_ACCOUN_AVAILABLEBALANCE nil="true" /&gt;
    ,
    if ($prod/m:B_KOD_WALUTY)
      then &lt;fml:NF_CURREC_CURRENCYCODE&gt;{data($prod/m:B_KOD_WALUTY)}&lt;/fml:NF_CURREC_CURRENCYCODE&gt;
      else &lt;fml:NF_CURREC_CURRENCYCODE nil="true"/&gt;
    ,			
    if ($prod/m:B_D_OTWARCIA)
      then &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_D_OTWARCIA))}&lt;/fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;
      else &lt;fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;0001-01-01&lt;/fml:NF_ACCOUN_ACCOUNTOPENDATE&gt;
    ,			
    if ($prod/m:B_DATA_OST_OPER)
      then &lt;fml:NF_TRANA_DATEOFLASTACTIVIT&gt;{fn-bea:date-from-string-with-format("dd-MM-yyyy",data($prod/m:B_DATA_OST_OPER))}&lt;/fml:NF_TRANA_DATEOFLASTACTIVIT&gt;
      else &lt;fml:NF_TRANA_DATEOFLASTACTIVIT&gt;0001-01-01&lt;/fml:NF_TRANA_DATEOFLASTACTIVIT&gt;
    ,		
    if ($prod/m:B_LIMIT1)
      then &lt;fml:NF_TRAACA_LIMITAMOUNT&gt;{data($prod/m:B_LIMIT1)}&lt;/fml:NF_TRAACA_LIMITAMOUNT&gt;
      else &lt;fml:NF_TRAACA_LIMITAMOUNT nil="true" /&gt;
    ,			
    if ($prod/m:CI_PRODUCT_NAME_ORYGINAL)
      then &lt;fml:NF_ACCOUN_ACCOUNTNAME&gt;{data($prod/m:CI_PRODUCT_NAME_ORYGINAL)}&lt;/fml:NF_ACCOUN_ACCOUNTNAME&gt;
      else &lt;fml:NF_ACCOUN_ACCOUNTNAME nil="true" /&gt;
    ,			
    if ($prod/m:B_KOD_RACH)
      then &lt;fml:NF_PRIMAC_ACCOUNTCODE&gt;{data($prod/m:B_KOD_RACH)}&lt;/fml:NF_PRIMAC_ACCOUNTCODE&gt;
      else &lt;fml:NF_PRIMAC_ACCOUNTCODE nil="true" /&gt;
    ,:)
    &lt;NF_CTRL_SYSTEMID&gt;{$system_id}&lt;/NF_CTRL_SYSTEMID&gt;
    ,
    &lt;NF_ACCOUR_RELATIONSHIP nil="true"/&gt;
    ,
    if ($prod[@contract-number])
    then &lt;NF_INSURA_ID&gt;{data($prod/@contract-number)}&lt;/NF_INSURA_ID&gt;
    else  &lt;NF_INSURA_ID nil="true"/&gt;
    ,
    &lt;NF_DEBITC_VIRTUALCARDNBR&gt;{concat(concat(data($prod/@product-type),":"),$prod/@contract-number)}
    &lt;/NF_DEBITC_VIRTUALCARDNBR&gt;
    ,
(: v.1.3 start :)
    if (data($prod/status)='0' or data($prod/status)='1')
    then &lt;NF_PRODUCT_STAT&gt;T&lt;/NF_PRODUCT_STAT&gt;
    else  &lt;NF_PRODUCT_STAT&gt;N&lt;/NF_PRODUCT_STAT&gt;  (: statusy 2, 3, 5, 7, 8 i pozostałe :)
   ,
(: v.1.3 end :)
(: v.1.4 start :)
    if ($prod/proposal-number)
    then &lt;NF_POLICC_PROPOSALID&gt;{data($prod/proposal-number)}&lt;/NF_POLICC_PROPOSALID&gt;
    else ()
    ,
(: v.1.4 end :)
   &lt;NF_CURREC_CURRENCYCODE&gt;PLN&lt;/NF_CURREC_CURRENCYCODE&gt;
   ,
   if ($prod/contract-dates/proposal)
   then &lt;NF_ACCOUN_ACCOUNTOPENDATE&gt;{data($prod/contract-dates/proposal)}&lt;/NF_ACCOUN_ACCOUNTOPENDATE&gt;
   else &lt;NF_ACCOUN_ACCOUNTOPENDATE nil="true" /&gt;
};

declare function xf:mapSTREAMLINEGetContractListResponse ($res as element(wsdl-jv:getContractListResponse),$system_id as xs:string,
  $pagesize as xs:integer,$oldstart as xs:integer,$oldstop as xs:integer ,
								  $actioncode as xs:string )
as element(fml:FML32){
	let $records:=count( $res/contracts/contract)
	let $start:=xf:calculatePageStart($pagesize,$oldstart,$oldstop,$actioncode,$records)
	let $stop:=xf:calculatePageStop($pagesize,$oldstart,$oldstop,$actioncode,$records)
	let $newnavkey:=concat("012:",string($start),":",string($stop))
	let $hasnext:=xf:calculateHasNext($start,$stop,$records,$actioncode)
	return
	   &lt;fml:FML32&gt;
		  &lt;NF_PAGEC_NAVIGATIONKEYVALU&gt;{$newnavkey}&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;
		  &lt;NF_PAGEC_HASNEXT&gt;{$hasnext}&lt;/NF_PAGEC_HASNEXT&gt;
		  &lt;NF_PAGECC_OPERATIONS&gt;{max((0,$stop - $start+1))}&lt;/NF_PAGECC_OPERATIONS&gt;
		  {
			for $idx in ($start to $stop)
			   return
				  xf:mapProduct($res/contracts/contract[$idx], $system_id)
		  }
	   &lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
declare variable $system_id as xs:string external;
declare variable $pagesize as xs:integer external;
declare variable $oldstart  as xs:integer external;
declare variable $oldstop  as xs:integer external;
declare variable $actioncode  as xs:string external;
&lt;soap-env:Body&gt;
{xf:mapSTREAMLINEGetContractListResponse($body/wsdl-jv:getContractListResponse,$system_id,$pagesize,$oldstart,$oldstop,$actioncode)}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>