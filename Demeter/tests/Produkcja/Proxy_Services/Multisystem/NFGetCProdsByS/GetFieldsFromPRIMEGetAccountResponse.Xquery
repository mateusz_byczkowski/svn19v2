<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/prime/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";


declare variable $body as element(FML32) external;

&lt;NF_ACCOUN_CURRENTBALANCE&gt;{data($body/B_SALDO)}&lt;/NF_ACCOUN_CURRENTBALANCE&gt;,
&lt;NF_ACCOUN_AVAILABLEBALANCE&gt;{data($body/B_DOST_SRODKI)}&lt;/NF_ACCOUN_AVAILABLEBALANCE&gt;,
&lt;NF_TRAACA_LIMITAMOUNT&gt;{data($body/B_LIMIT2)}&lt;/NF_TRAACA_LIMITAMOUNT&gt;,
&lt;NF_HOLD_HOLDAMOUNT&gt;{data($body/B_BLOKADA)}&lt;/NF_HOLD_HOLDAMOUNT&gt;,
&lt;NF_CREDIC_CURRENTPAYMENTDATE&gt;{data($body/B_DATA_B_PLAT)}&lt;/NF_CREDIC_CURRENTPAYMENTDATE&gt;,
&lt;NF_CREDIC_LASTPERIODINTREST&gt;{data($body/B_ODS_POP_OKR)}&lt;/NF_CREDIC_LASTPERIODINTREST&gt;,
&lt;NF_CREDIC_LASTSTATEMENTDATE&gt;{data($body/B_D_OST_ZEST)}&lt;/NF_CREDIC_LASTSTATEMENTDATE&gt;,
&lt;NF_CREDIC_OVERDUEMINIMUMPAYMENT&gt;{data($body/B_ZAL_MIN )}&lt;/NF_CREDIC_OVERDUEMINIMUMPAYMENT&gt;,
&lt;NF_CREDIC_CURRENTMINIMUMPAYMENT&gt;{data($body/B_BIEZ_MIN)}&lt;/NF_CREDIC_CURRENTMINIMUMPAYMENT&gt;,
&lt;NF_CREDIC_MINIMUMAMOUNT&gt;{data($body/B_SPLATA_MIN)}&lt;/NF_CREDIC_MINIMUMAMOUNT&gt;,
&lt;NF_CREDIC_LASTSTMTCLOSINGBALANCE&gt;{data($body/B_SPLATA_RAZEM)}&lt;/NF_CREDIC_LASTSTMTCLOSINGBALANCE&gt;,
&lt;NF_CREDIC_NEXTSTATEMENTDATE&gt;{data($body/B_D_NAST_ZEST)}&lt;/NF_CREDIC_NEXTSTATEMENTDATE&gt;,
&lt;NF_ACCOUN_CREDITPRECENTAGE&gt;{data($body/B_OPROC1)}&lt;/NF_ACCOUN_CREDITPRECENTAGE&gt;,
&lt;NF_CURREC_CURRENCYCODE&gt;{data($body/B_KOD_WALUTY)}&lt;/NF_CURREC_CURRENCYCODE&gt;,
&lt;NF_CUSTOM_CUSTOMERNUMBER&gt;{data($body/E_CIF_NUMBER)}&lt;/NF_CUSTOM_CUSTOMERNUMBER&gt;</con:xquery>
</con:xqueryEntry>