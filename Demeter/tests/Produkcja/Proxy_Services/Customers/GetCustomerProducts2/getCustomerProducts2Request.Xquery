<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:Change log 
v.1.0.4  2009-07-01  PKL  TEET 34513 
v.1.0.5  2009-08-04  PKL  TEET 39924 
v.1.0.6  2009-08-11  PKL  TEET 39354 
CR85     2010-01-07  MMa  NP1913_2 CR85
v.1.0.8  2010-01-26  PKL  NP1984 - obsługa filtra accountForDebitCards
v.1.0.9  2010-02-12  PKL   PT 58
v.1.2     2010-05-06  PKLI NP1972_3 CR2 Filtrowanie produktów Streamline na podstawie statusu aktywności
v.1.3     2010-05-14  PKL  NP1696 CR125 Zmiana łączenia filtrów produktowych
:)
declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns0="urn:accounts.entities.be.dcl";
declare namespace ns2="urn:dictionaresrepo.dictionaries.be.dcl";
declare namespace ns1="urn:productstree.entities.be.dcl";
declare namespace ns3="urn:cif.entities.be.dcl";
declare namespace ns5="urn:dictionaries.be.dcl";
declare namespace ns4="urn:errors.hlbsentities.be.dcl";
declare namespace ns6="urn:entities.be.dcl";
declare namespace ns7="http://www.w3.org/2001/XMLSchema";
declare namespace ns8="urn:be.services.dcl";
declare namespace ns9="urn:baseauxentities.be.dcl";
declare namespace fml="";
declare namespace xf="ble";
declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;

declare function boolean2SourceValue ($parm as xs:string*,$trueval as xs:string,$falseval as xs:string) as xs:string* {
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else $falseval
};

declare function boolean2ActiveNActiveValue ($parm as xs:string*,$trueval as xs:string,$falseval as xs:string,$elseval as xs:string) as xs:string* {
    if ($parm  = "true") then $trueval
    else if ($parm  = "1") then $trueval
    else if ($parm  = "false") then $falseval
    else $elseval
};

declare function checkValue ($parm as xs:string*) as xs:string*
{
 if (data($parm)) then 
    if (string(number(data($parm))) != 'NaN') 
  then data($parm)
    else "0"
 else "0"
};

declare function xf:productAreaIdPadding($x as element(ns8:product)) as element(xf:areaIds){
     &lt;xf:areaIds&gt;
       {
          for $el in $x/ns1:ProductDefinition/ns1:productGroup/ns1:ProductGroup/ns1:productCategory/ns1:ProductCategory/ns1:productArea/ns1:ProductArea/ns1:idProductArea
             let $ellen:=string-length(data($el))
             return
                if ($ellen = 1)
                   then &lt;xf:areaId&gt;{concat("0",data($el))}&lt;/xf:areaId&gt;
                else if  ($ellen = 2)
                   then &lt;xf:areaId&gt;{data($el)}&lt;/xf:areaId&gt;
                else ()
       }
     &lt;/xf:areaIds&gt;
};
(: 1.3 Przebudowana funkcja wyznaczająca filtr NF_CTRL_AREAS :)
declare function xf:productAreaIdPadding2($x as element(ns8:product)) as element(xf:areaIds){
     &lt;xf:areaIds&gt;
       {
          for $el at $occ in $x/ns1:ProductDefinition
            let $ctrlAreaFilter:=fn:data($el/ns1:productGroup/ns1:ProductGroup/ns1:productCategory/ns1:ProductCategory/ns1:productArea/ns1:ProductArea/ns1:idProductArea)
            let $ctrlAreaFilterLen:=string-length($ctrlAreaFilter)
            let $ctrlCategoryFilter:=fn:data($el/ns1:productGroup/ns1:ProductGroup/ns1:productCategory/ns1:ProductCategory/ns1:idProductCategory)
            let $ctrlCategoryFilterLen:=fn:string-length($ctrlCategoryFilter)
            let $ctrlGroupFilter:=fn:data($el/ns1:productGroup/ns1:ProductGroup/ns1:idProductGroup)
            let $ctrlGroupFilterLen:=fn:string-length($ctrlGroupFilter)
            let $ctrlProductFilter:=fn:data($el/ns1:idProductDefinition)
            let $ctrlProductFilterLen:=fn:string-length($ctrlProductFilter)
            return
               if ($ctrlAreaFilterLen= 0 and $ctrlCategoryFilterLen + $ctrlGroupFilterLen + $ctrlProductFilterLen &gt; 0) 
                  then &lt;xf:areaId&gt;00&lt;/xf:areaId&gt;
               else if ($ctrlAreaFilterLen= 1)
                  then &lt;xf:areaId&gt;{concat("0",data($ctrlAreaFilter))}&lt;/xf:areaId&gt;
               else if  ($ctrlAreaFilterLen= 2)
                  then &lt;xf:areaId&gt;{data($ctrlAreaFilter)}&lt;/xf:areaId&gt;
               else ()
       }
     &lt;/xf:areaIds&gt;
};
(: 1.3 koniec :) 

declare function xf:getNfCtrlAreas($x as element(ns8:product)) as element(NF_CTRL_AREAS)
{
   (:1.3 let $areaIds:=xf:productAreaIdPadding($x) :)
   (:1.3:) let $areaIds:=xf:productAreaIdPadding2($x)
   let $areasConcatenated:=translate(concat(distinct-values(data($areaIds/xf:areaId)) ," ")," ","")
   return   
        if (string-length($areasConcatenated)&gt;0)
           then &lt;NF_CTRL_AREAS?&gt;{$areasConcatenated}&lt;/NF_CTRL_AREAS&gt;
           else &lt;NF_CTRL_AREAS?&gt;00&lt;/NF_CTRL_AREAS&gt;
};

declare function xf:productIdPadding($x as element(ns8:product)) as element(xf:productIds){
     &lt;xf:productIds&gt;
       {
          for $el in $x/ns1:ProductDefinition/ns1:idProductDefinition
             let $ellen:=string-length(data($el))
             return
                if ($ellen = 1)
                   then &lt;xf:productId&gt;{concat("0000",data($el))}&lt;/xf:productId&gt;
                else if ($ellen = 2)
                   then &lt;xf:productId&gt;{concat("000",data($el))}&lt;/xf:productId&gt;
                else if ($ellen = 3)
                   then &lt;xf:productId&gt;{concat("00",data($el))}&lt;/xf:productId&gt;
                else if ($ellen = 4)
                   then &lt;xf:productId&gt;{concat("0",data($el))}&lt;/xf:productId&gt;
                else if  ($ellen = 5)
                   then &lt;xf:productId&gt;{data($el)}&lt;/xf:productId&gt;
                else ()
       }
     &lt;/xf:productIds&gt;
};

declare function xf:getNfCtrlProducts($x as element(ns8:product)) as element(NF_CTRL_PRODUCTS)
{
   let $productIds:=xf:productIdPadding($x)
   let $productsConcatenated:=translate(concat(distinct-values(data($productIds/xf:productId)) ," ")," ","")
   return   
        if (string-length($productsConcatenated)&gt;0)
           then &lt;NF_CTRL_PRODUCTS?&gt;{$productsConcatenated}&lt;/NF_CTRL_PRODUCTS&gt;
           else &lt;NF_CTRL_PRODUCTS?&gt;00000&lt;/NF_CTRL_PRODUCTS&gt;
};

declare function xf:categoryIdPadding($x as element(ns8:product)) as element(xf:categoryIds){
     &lt;xf:categoryIds&gt;
       {
          for $el in $x/ns1:ProductDefinition/ns1:productGroup/ns1:ProductGroup/ns1:productCategory/ns1:ProductCategory/ns1:idProductCategory
             let $ellen:=string-length(data($el))
             return
                if ($ellen = 1)
                   then &lt;xf:categoryId&gt;{concat("00",data($el))}&lt;/xf:categoryId&gt;
                else if ($ellen = 2)
                   then &lt;xf:categoryId&gt;{concat("0",data($el))}&lt;/xf:categoryId&gt;
                else if ($ellen = 3)
                   then &lt;xf:categoryId&gt;{data($el)}&lt;/xf:categoryId&gt;
                else ()
       }
     &lt;/xf:categoryIds&gt;
};

declare function xf:getNfCtrlCategories($x as element(ns8:product)) as element(NF_CTRL_CATEGORIES)
{
   let $categoryIds:=xf:categoryIdPadding($x)
   let $categoriesConcatenated:=translate(concat(distinct-values(data($categoryIds/xf:categoryId)) ," ")," ","")
   return   
        if (string-length($categoriesConcatenated)&gt;0)
           then &lt;NF_CTRL_CATEGORIES?&gt;{$categoriesConcatenated}&lt;/NF_CTRL_CATEGORIES&gt;
           else &lt;NF_CTRL_CATEGORIES?&gt;000&lt;/NF_CTRL_CATEGORIES&gt;
};

declare function xf:groupIdPadding($x as element(ns8:product)) as element(xf:groupIds){
     &lt;xf:groupIds&gt;
       {
          for $el in $x/ns1:ProductDefinition/ns1:productGroup/ns1:ProductGroup/ns1:idProductGroup
             let $ellen:=string-length(data($el))
             return
                if ($ellen = 1)
                   then &lt;xf:groupId&gt;{concat("000",data($el))}&lt;/xf:groupId&gt;
                else if ($ellen = 2)
                   then &lt;xf:groupId&gt;{concat("00",data($el))}&lt;/xf:groupId&gt;
                else if ($ellen = 3)
                   then &lt;xf:groupId&gt;{concat("0",data($el))}&lt;/xf:groupId&gt;
                else if ($ellen = 4)
                   then &lt;xf:groupId&gt;{data($el)}&lt;/xf:groupId&gt;
                else ()
       }
     &lt;/xf:groupIds&gt;
};

declare function xf:getNfCtrlGroups($x as element(ns8:product)) as element(NF_CTRL_GROUPS)
{
   let $groupIds:=xf:groupIdPadding($x)
   let $groupsConcatenated:=translate(concat(distinct-values(data($groupIds/xf:groupId)) ," ")," ","")
   return   
        if (string-length($groupsConcatenated)&gt;0)
           then &lt;NF_CTRL_GROUPS?&gt;{$groupsConcatenated}&lt;/NF_CTRL_GROUPS&gt;
           else &lt;NF_CTRL_GROUPS?&gt;0000&lt;/NF_CTRL_GROUPS&gt;
};

(:1.3 Funkcja wyznaczająca wartość filtra produktowego NF_CTRL_PRODFILTER :)
(:1.3 Dodatkowo sterujemy filtr NF_CTRL_AREAS, bo musimy go przekazać do NFGetCustProds :)
declare function xf:getNfCtrlProdFilter($x as element(ns8:product)) as element(NF_CTRL_PRODFILTER)
{
     let $ctrlProdFilter:="PT_ID_AREA[0] "
     return
      &lt;NF_CTRL_PRODFILTER?&gt;
         {$ctrlProdFilter, 
            for $el at $occ in $x/ns1:ProductDefinition 
              let $ctrlAreaFilter:=fn:data($el/ns1:productGroup/ns1:ProductGroup/ns1:productCategory/ns1:ProductCategory/ns1:productArea/ns1:ProductArea/ns1:idProductArea)
              let $ctrlAreaFilterLen:=fn:string-length($ctrlAreaFilter)
              let $ctrlCategoryFilter:=fn:data($el/ns1:productGroup/ns1:ProductGroup/ns1:productCategory/ns1:ProductCategory/ns1:idProductCategory)
              let $ctrlCategoryFilterLen:=fn:string-length($ctrlCategoryFilter)
              let $ctrlGroupFilter:=fn:data($el/ns1:productGroup/ns1:ProductGroup/ns1:idProductGroup)
              let $ctrlGroupFilterLen:=fn:string-length($ctrlGroupFilter)
              let $ctrlProductFilter:=fn:data($el/ns1:idProductDefinition)
              let $ctrlProductFilterLen:=fn:string-length($ctrlProductFilter)
              return
              concat( 
                 (: Od 2 bloku wstawiamy łącznik bloków OR jeżeli coś podano w danym bloku:)
                 if($occ &gt; 1 and $ctrlAreaFilterLen + $ctrlCategoryFilterLen + $ctrlGroupFilterLen + $ctrlProductFilterLen &gt; 0)
                    then " OR "
                    else ()
                 ,
                 (: Filtr na obszar :)
                 if($ctrlAreaFilterLen&gt;0)
                    then concat("PT_ID_AREA=", $ctrlAreaFilter)
                    else ()
                 ,
                 (: Filtr na kategorię :)
                 if($ctrlCategoryFilterLen&gt;0)
                    then (
                            if($ctrlAreaFilterLen&gt;0)
                               then " AND "
                               else (),
                            concat("PT_ID_CATEGORY=", $ctrlCategoryFilter)
                           )
                    else ()
                 ,
                 (: Filtr na grupę :)
                 if($ctrlGroupFilterLen&gt;0)
                    then ( 
                            if($ctrlCategoryFilterLen&gt;0 or $ctrlAreaFilterLen&gt;0)
                               then " AND "
                            else (),
                            concat("PT_ID_GROUP=", $ctrlGroupFilter)
                            )
                    else ()
                 ,
                  (: Filtr na produkt :)
                 if($ctrlProductFilterLen&gt;0)
                    then (
                            if($ctrlGroupFilterLen&gt;0 or $ctrlCategoryFilterLen&gt;0 or $ctrlAreaFilterLen&gt;0)
                               then " AND "
                            else (),
                            concat("PT_ID_DEFINITION=", $ctrlProductFilter)
                           )
                    else ()
              )
         }
       &lt;/NF_CTRL_PRODFILTER&gt;
}; 
(:1.3 koniec:)

declare function xf:companyIdPadding($x as element(ns8:customerFolders)) as element(xf:companyIds){
     &lt;xf:companyIds&gt;
       {
          for $el in $x/ns3:CustomerFolders/ns3:companyID/ns5:CompanyType/ns5:companyType
             let $ellen:=string-length(data($el))
             return
                if ($ellen = 1 and data($el) != "0")
                   then &lt;xf:companyId&gt;{concat("0",data($el))}&lt;/xf:companyId&gt;
                 else if ($ellen = 2 and data($el) != "00")
                   then &lt;xf:companyId&gt;{data($el)}&lt;/xf:companyId&gt;
                else ()
       }
     &lt;/xf:companyIds&gt;
};

declare function xf:getNfCtrlCompanies($x as element(ns8:customerFolders)) as element(NF_CTRL_COMPANIES)
{
   let $companyIds:=xf:companyIdPadding($x)
   let $companiesConcatenated:=translate(concat(distinct-values(data($companyIds/xf:companyId)) ," ")," ","")
   return   
        if (string-length($companiesConcatenated)&gt;0)
           then &lt;NF_CTRL_COMPANIES?&gt;{$companiesConcatenated}&lt;/NF_CTRL_COMPANIES&gt;
           else &lt;NF_CTRL_COMPANIES?&gt;01&lt;/NF_CTRL_COMPANIES&gt;
};

declare function xf:getNfCurrecCurrencycode($x as element(ns8:currencies)) as xs:string*
{
   let $currencyCodes:=data($x/ns5:CurrencyCode/ns5:currencyCode)
   let $result:= translate(concat(distinct-values($currencyCodes) ," ")," ","")
   return   
        if (string-length($result)&gt;0) 
           then  $result
           else ()
};

declare function xf:getNfAccourRelationship($x as element(ns8:customer)) as xs:string*
{
   let $relationships:=data($x/ns3:Customer/ns3:accountRelationshipList/ns3:AccountRelationship/ns3:relationship/ns5:CustomerAccountRelationship/ns5:customerAccountRelationship)
   let $result:= translate(concat(distinct-values($relationships) ," ")," ","")
   return   
        if (string-length($result)&gt;0) 
           then  $result
           else ()  
       };


declare function getFieldsFromHeader($parm as element(ns8:header)) as element()*
{
&lt;NF_MSHEAD_COMPANYID?&gt;{data($parm/ns8:msgHeader/ns8:companyId)}&lt;/NF_MSHEAD_COMPANYID&gt;
};

declare function getFieldsFromInvoke($parm as element(ns8:invoke)) as element()*
{
xf:getNfCtrlAreas($parm/ns8:product) 
,
(:1.3 wyłączenie starych funkcji obsługujących filtry produktowe, ale bez filtra na obszar NF_CTRL_AREAS
xf:getNfCtrlProducts($parm/ns8:product)
,
xf:getNfCtrlCategories($parm/ns8:product)
,
xf:getNfCtrlGroups($parm/ns8:product)
,
1.3 koniec wyłączania starych funkcji obsługujących filtry produktowe :)
(:1.3 nowa funkcja budująca filtr produktowy jeżeli podano choć jeden filtr na kategorię, grupę lub produkt 
1.3 a jeżeli podano tylko filtr na obszar, to po staremu ustawiamy tylko NF_CTRL_AREAS w getNfCtrlAreas() :)

if(string-length(concat(
   data($parm/ns8:product/ns1:ProductDefinition/ns1:productGroup/ns1:ProductGroup/ns1:productCategory/ns1:ProductCategory/ns1:idProductCategory),  
   data($parm/ns8:product/ns1:ProductDefinition/ns1:productGroup/ns1:ProductGroup/ns1:idProductGroup),
   data($parm/ns8:product/ns1:ProductDefinition/ns1:idProductDefinition)))&gt;0)
 then  xf:getNfCtrlProdFilter($parm/ns8:product) 
 else()
,
(:1.3 koniec :)

xf:getNfCtrlCompanies($parm/ns8:customerFolders)
,
if(fn:string-length($parm/ns8:extraCharge/ns1:ProductAttributes/ns1:attributeValue)&gt;0)
    (: T34513    then &lt;NF_CTRL_PRODFILTER?&gt;{concat("pt_id_area[10] UPPER(PT_ID_ATTRIBUTE[173]s) = '", data($parm/ns8:extraCharge/ns1:ProductAttributes/ns1:attributeValue), "'")}&lt;/NF_CTRL_PRODFILTER&gt; :)
    then &lt;NF_CTRL_PRODFILTER?&gt;{concat("pt_id_area[10] UPPER(PT_ID_ATTRIBUTE[173]s) = UPPER('", data($parm/ns8:extraCharge/ns1:ProductAttributes/ns1:attributeValue), "')")}&lt;/NF_CTRL_PRODFILTER&gt;
    else ()
,
if(fn:string-length($parm/ns8:flag/ns1:ProductAttributes/ns1:attributeValue)&gt;0)
    (: T34513 then &lt;NF_CTRL_PRODFILTER?&gt;{concat("pt_id_area[13] UPPER(PT_ID_ATTRIBUTE[131]s) = '", data($parm/ns8:flag/ns1:ProductAttributes/ns1:attributeValue), "'")}&lt;/NF_CTRL_PRODFILTER&gt; :)
    then &lt;NF_CTRL_PRODFILTER?&gt;{concat("pt_id_area[13] UPPER(PT_ID_ATTRIBUTE[131]s) = UPPER('", data($parm/ns8:flag/ns1:ProductAttributes/ns1:attributeValue), "')")}&lt;/NF_CTRL_PRODFILTER&gt;
    else ()
,
(: 1.0.8 start:)
if(fn:data($parm/ns8:accountForDebitCards/ns9:IntegerHolder/ns9:value)=1)
    then (
             &lt;NF_CTRL_PRODFILTER?&gt;PT_ID_AREA[2] PT_ATTRIBUTE_NAME[karty_platnicze_identyfikatory] &amp;gt; 0&lt;/NF_CTRL_PRODFILTER&gt;,
             &lt;NF_CTRL_PRODFILTER?&gt;PT_ID_AREA[10] PT_ATTRIBUTE_NAME[karty_platnicze_identyfikatory] &amp;gt; 0&lt;/NF_CTRL_PRODFILTER&gt;
            )
    else ()
,
if(fn:data($parm/ns8:accountForDebitCards/ns9:IntegerHolder/ns9:value)=2)
    then (
             &lt;NF_CTRL_PRODFILTER?&gt;PT_ID_AREA[2] PT_ATTRIBUTE_NAME[karty_platnicze_identyfikatory] = 0&lt;/NF_CTRL_PRODFILTER&gt;,
             &lt;NF_CTRL_PRODFILTER?&gt;PT_ID_AREA[10] PT_ATTRIBUTE_NAME[karty_platnicze_identyfikatory] = 0&lt;/NF_CTRL_PRODFILTER&gt;
            )
    else ()
, (: 1.0.8 koniec:)
&lt;NF_CTRL_SHOWCURRBAL?&gt;{checkValue($parm/ns8:showCurrentBalance/ns9:IntegerHolder/ns9:value)}&lt;/NF_CTRL_SHOWCURRBAL&gt;
,
&lt;NF_CURREC_CURRENCYCODE?&gt;{xf:getNfCurrecCurrencycode($parm/ns8:currencies)}&lt;/NF_CURREC_CURRENCYCODE&gt;
,
&lt;NF_ACCOUR_RELATIONSHIP?&gt;{xf:getNfAccourRelationship($parm/ns8:customer)}&lt;/NF_ACCOUR_RELATIONSHIP&gt;
,
&lt;NF_ACCOUN_ACCOUNTNUMBER?&gt;{data($parm/ns8:accountNumber/ns0:Account/ns0:accountNumber)}&lt;/NF_ACCOUN_ACCOUNTNUMBER&gt;
,
&lt;NF_PAGEC_ACTIONCODE?&gt;{data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns9:PageControl/ns9:actionCode)}&lt;/NF_PAGEC_ACTIONCODE&gt;
,
&lt;NF_PAGEC_PAGESIZE?&gt;{data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns9:PageControl/ns9:pageSize)}&lt;/NF_PAGEC_PAGESIZE&gt;
,
&lt;NF_PAGEC_REVERSEORDER?&gt;{boolean2SourceValue (data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns9:PageControl/ns9:reverseOrder),"1","0")}&lt;/NF_PAGEC_REVERSEORDER&gt;
,
&lt;NF_PAGEC_NAVIGATIONKEYVALU?&gt;{data($parm/ns8:bcd/ns6:BusinessControlData/ns6:pageControl/ns9:PageControl/ns9:navigationKeyValue)}&lt;/NF_PAGEC_NAVIGATIONKEYVALU&gt;
,
(: T39924 &lt;NF_CTRL_ACTIVENONACTIVE?&gt;{boolean2SourceValue (data($parm/ns8:accountActive/ns9:BooleanHolder/ns9:value),"1","0")}&lt;/NF_CTRL_ACTIVENONACTIVE&gt; :)
&lt;NF_CTRL_ACTIVENONACTIVE?&gt;{boolean2ActiveNActiveValue (data($parm/ns8:accountActive/ns9:BooleanHolder/ns9:value),"1","2", "0")}&lt;/NF_CTRL_ACTIVENONACTIVE&gt;
,
&lt;NF_CUSTOM_CUSTOMERNUMBER?&gt;{data($parm/ns8:customer/ns3:Customer/ns3:customerNumber)}&lt;/NF_CUSTOM_CUSTOMERNUMBER&gt;
,
&lt;NF_ACCOUN_ALTERNATIVEADDRE&gt;0&lt;/NF_ACCOUN_ALTERNATIVEADDRE&gt;
,
&lt;NF_CTRL_HOLDCODE?&gt;{data($parm/ns8:holdCode/ns9:IntegerHolder/ns9:value)}&lt;/NF_CTRL_HOLDCODE&gt;
,
&lt;NF_CTRL_SYSTEMS&gt;001002010011&lt;/NF_CTRL_SYSTEMS&gt;
,
&lt;NF_CTRL_OPTION&gt;0&lt;/NF_CTRL_OPTION&gt;
,
 &lt;NF_PRODUG_VISIBILITYDCL&gt;1&lt;/NF_PRODUG_VISIBILITYDCL&gt; 
, (: CR85 - begin :)
&lt;NF_CTRL_RELATEDPRODUCTS?&gt;{boolean2SourceValue (data($parm/ns8:relatedProducts/ns9:IntegerHolder/ns9:value),"1","0")}&lt;/NF_CTRL_RELATEDPRODUCTS&gt;
  (: CR85 - end :)
};

&lt;soap:Body&gt;
  &lt;fml:FML32&gt;
    {getFieldsFromHeader($header/ns8:header)}
    {getFieldsFromInvoke($body/ns8:invoke)}
  &lt;/fml:FML32&gt;
&lt;/soap:Body&gt;</con:xquery>
</con:xqueryEntry>