<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ns4 = "urn:be.services.dcl";
declare namespace xf = "http://tempuri.org/Testy_XQuery/Usługi/CIS/SaveCustomer/RequestTransform2/";
declare namespace ns0 = "urn:cif.entities.be.dcl";
declare namespace ns-1 = "urn:baseentities.be.dcl";
declare namespace ns1 = "http://bzwbk.com/services/cis/messages/";
declare namespace ns3 = "urn:dictionaries.be.dcl";
declare namespace ns2 = "urn:basedictionaries.be.dcl";
declare namespace ns5 = "urn:baseauxentities.be.dcl";
declare namespace ns6 = "urn:entities.be.dcl";
declare namespace ns7 = "urn:insurancedict.dictionaries.be.dcl";
declare namespace ns8 = "urn:cifdict.dictionaries.be.dcl";

declare function local:booleanToInt($tn as xs:boolean?) as xs:integer {
  if($tn) then 1
  else 0
};

declare function local:booleanToString($tn as xs:boolean?) as xs:string {
  if($tn) then '1'
  else '0'
};

declare function local:YMDToDMY($dateYMD as xs:string?) as xs:string {
   if($dateYMD) then
	   if ($dateYMD eq '') then ''
	   else fn:string-join((fn:substring($dateYMD, 9, 2), fn:substring($dateYMD, 6, 2),fn:substring($dateYMD, 1, 4)), '-')
   else ''
};

declare function xf:RequestTransform2($invoke1 as element(ns4:invoke), $header1 as element(ns4:header))
    as element(ns1:pcCustNewRequest) {
        &lt;ns1:pcCustNewRequest&gt;
            &lt;ns1:PracownikWprow?&gt;{ data($header1/ns4:msgHeader/ns4:userId) }&lt;/ns1:PracownikWprow&gt;
            &lt;ns1:NumerKlienta?&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerNumber) }&lt;/ns1:NumerKlienta&gt;
            &lt;ns1:IdSpolki&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:companyID/ns3:CompanyType/ns3:companyType) }&lt;/ns1:IdSpolki&gt;
            {
                for $customerType in $invoke1/ns4:customer/ns0:Customer/ns0:customerType/ns3:CustomerType/ns3:customerType
                return
                    &lt;ns1:TypKlienta&gt;{ data($customerType) }&lt;/ns1:TypKlienta&gt;
            }
            {
                for $agreementMarketing in $invoke1/ns4:customer/ns0:Customer/ns0:processingApproval/ns3:CustomerProcessingApproval/ns3:customerProcessingApproval
                return
                    &lt;ns1:UdostepGrupa?&gt;{ data($agreementMarketing) }&lt;/ns1:UdostepGrupa&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:UlicaDanePodst&gt;{ data($adresy[1]/ns0:street) }&lt;/ns1:UlicaDanePodst&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:NrPosesLokaluDanePodst&gt;{ data($adresy[1]/ns0:houseFlatNumber) }&lt;/ns1:NrPosesLokaluDanePodst&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:KodPocztowyDanePodst&gt;{ data($adresy[1]/ns0:zipCode) }&lt;/ns1:KodPocztowyDanePodst&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:MiastoDanePodst&gt;{ data($adresy[1]/ns0:city) }&lt;/ns1:MiastoDanePodst&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:WojKrajDanePodst&gt;{ data($adresy[1]/ns0:state/ns3:State/ns3:state) }&lt;/ns1:WojKrajDanePodst&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:KodKraju&gt;{ data($adresy[1]/ns0:countryId/ns3:CountryCode/ns3:isoCountryCode) }&lt;/ns1:KodKraju&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:TypAdresu&gt;{ data($adresy[1]/ns0:typeOfLocation/ns3:AddressTypeOfLocation/ns3:addressTypeOfLocation) }&lt;/ns1:TypAdresu&gt;
            }
            &lt;ns1:RezNierez&gt;{ local:booleanToInt($invoke1/ns4:customer/ns0:Customer/ns0:resident) }&lt;/ns1:RezNierez&gt;
            &lt;ns1:NrDowoduRegon&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:identityCardNumber) }&lt;/ns1:NrDowoduRegon&gt;
            &lt;ns1:NrPesel&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:pesel) }&lt;/ns1:NrPesel&gt;
            &lt;ns1:Nip&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:taxID) }&lt;/ns1:Nip&gt;
            {
                for $dateAgreementMarketing in $invoke1/ns4:dateOfAgreement/ns5:DateHolder/ns5:value
                return
                    &lt;ns1:DataUdostepGrupa?&gt;{ local:YMDToDMY( data($dateAgreementMarketing) ) }&lt;/ns1:DataUdostepGrupa&gt;
            }
            {
                for $agreementEmailMarketing in $invoke1/ns4:customer/ns0:Customer/ns0:agreementCode/ns8:CustomerAgreementCode/ns8:customerAgreementCode
                return
                    &lt;ns1:ZgodaInfHandlowa?&gt;{ data($agreementEmailMarketing) }&lt;/ns1:ZgodaInfHandlowa&gt;
            }
            {
                for $dateAgreementEmailMarketing in $invoke1/ns4:dateOfAgreement/ns5:DateHolder/ns5:value
                return
                    &lt;ns1:DataZgodyInfHandlowej?&gt;{ local:YMDToDMY($dateAgreementEmailMarketing) }&lt;/ns1:DataZgodyInfHandlowej&gt;
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:AdresOd&gt;{ local:YMDToDMY(data($adresy[1]/ns0:validFrom)) }&lt;/ns1:AdresOd&gt;
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 1
                                                                	                return $adres
                return
                    &lt;ns1:AdresDo&gt;{ local:YMDToDMY( data($adresy[1]/ns0:validTo) ) }&lt;/ns1:AdresDo&gt;
            }
            &lt;ns1:KrajPochodzenia&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:countryOfOrigin/ns3:CountryCode/ns3:isoCountryCode) }&lt;/ns1:KrajPochodzenia&gt;
            &lt;ns1:AdresEMail&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:email) }&lt;/ns1:AdresEMail&gt;
            &lt;ns1:NrTelefonu&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:phoneNo) }&lt;/ns1:NrTelefonu&gt;
            &lt;ns1:NrTelefKomorkowego&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:mobileNo) }&lt;/ns1:NrTelefKomorkowego&gt;
            &lt;ns1:NumerTelefonuPraca&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:businessNo) }&lt;/ns1:NumerTelefonuPraca&gt;
            &lt;ns1:KartaWzorowPodpisow&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:signatureCardBranchId/ns3:BranchCode/ns3:branchCode) }&lt;/ns1:KartaWzorowPodpisow&gt;
            &lt;ns1:NrOddzialu&gt;{ xs:long( data($invoke1/ns4:customer/ns0:Customer/ns0:branchOfOwnership/ns3:BranchCode/ns3:branchCode) ) }&lt;/ns1:NrOddzialu&gt;
            {
                for $customerDocumentType in $invoke1/ns4:customer/ns0:Customer/ns0:documentList/ns0:Document[1]/ns0:documentType/ns3:CustomerDocumentType/ns3:customerDocumentType
                return
                    &lt;ns1:DokTozsamosci&gt;{ data($customerDocumentType) }&lt;/ns1:DokTozsamosci&gt;
            }
            {
                for $documentNumber in $invoke1/ns4:customer/ns0:Customer/ns0:documentList/ns0:Document[1]/ns0:documentNumber
                return
                    &lt;ns1:SeriaNrDok&gt;{ data($documentNumber) }&lt;/ns1:SeriaNrDok&gt;
            }
            &lt;ns1:NumerPaszportu&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:passportNumber) }&lt;/ns1:NumerPaszportu&gt;
            &lt;ns1:Plec&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:sexCode/ns3:CustomerSexCode/ns3:customerSexCode) }&lt;/ns1:Plec&gt;
            &lt;ns1:Imie&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:firstName) }&lt;/ns1:Imie&gt;
            &lt;ns1:DrugieImie&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:secondName) }&lt;/ns1:DrugieImie&gt;
            &lt;ns1:Nazwisko&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:lastName) }&lt;/ns1:Nazwisko&gt;
            &lt;ns1:NazwiskoPanienskieMatki&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:motherMaidenName) }&lt;/ns1:NazwiskoPanienskieMatki&gt;
            &lt;ns1:Obywatelstwo&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:citizenship/ns3:CitizenshipCode/ns3:isoCitizenshipCode) }&lt;/ns1:Obywatelstwo&gt;
            &lt;ns1:DataUrodzenia&gt;{ local:YMDToDMY( data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:dateOfBirth) ) }&lt;/ns1:DataUrodzenia&gt;
            &lt;ns1:MiejsceUrodzenia&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:birthPlace) }&lt;/ns1:MiejsceUrodzenia&gt;
            &lt;ns1:KodJezyka&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerAdditionalInfo/ns0:CustomerAdditionalInfo/ns0:language/ns2:Language/ns2:languageID) }&lt;/ns1:KodJezyka&gt;
            &lt;ns1:ZrodloDanych&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerAdditionalInfo/ns0:CustomerAdditionalInfo/ns0:dataSource/ns3:DataSource/ns3:dataSource) }&lt;/ns1:ZrodloDanych&gt;
            {
                for $policyNo in $invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyNo
                return
                    &lt;ns1:LiczbaPolis&gt;{ xs:short( data($policyNo) ) }&lt;/ns1:LiczbaPolis&gt;
            }
            {
                for $policyTotalAmount in $invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyTotalAmount
                return
                    &lt;ns1:SumaPolisNaZycie&gt;{ xs:string( data($policyTotalAmount) ) }&lt;/ns1:SumaPolisNaZycie&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:NrPosesLokaluAdresAlt&gt;{ data($adresy[1]/ns0:houseFlatNumber) }&lt;/ns1:NrPosesLokaluAdresAlt&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:KodPocztowyAdresAlt&gt;{ data($adresy[1]/ns0:zipCode) }&lt;/ns1:KodPocztowyAdresAlt&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:WojewodztwoKrajAdresAlt&gt;{ data($adresy[1]/ns0:state/ns3:State/ns3:state) }&lt;/ns1:WojewodztwoKrajAdresAlt&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:KodKrajuKoresp&gt;{ data($adresy[1]/ns0:countryId/ns3:CountryCode/ns3:isoCountryCode) }&lt;/ns1:KodKrajuKoresp&gt;
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:DataOd&gt;{ local:YMDToDMY( data($adresy[1]/ns0:validFrom) ) }&lt;/ns1:DataOd&gt;
            }
            {
                for $adresy in for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:DataDo&gt;{ local:YMDToDMY( data($adresy[1]/ns0:validTo) ) }&lt;/ns1:DataDo&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:TypAdresuKoresp&gt;{ data($adresy[1]/ns0:typeOfLocation/ns3:AddressTypeOfLocation/ns3:addressTypeOfLocation) }&lt;/ns1:TypAdresuKoresp&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:MiastoAdresAlt&gt;{ data($adresy[1]/ns0:city) }&lt;/ns1:MiastoAdresAlt&gt;
            }
            {
                let $adresy := for $adres in $invoke1/ns4:customer/ns0:Customer/ns0:addressList/ns0:Address
                                                                	                where $adres/ns0:addressType/ns3:AddressType/ns3:addressType = 2
                                                                	                return $adres
                return
                    &lt;ns1:UlicaAdresAlt&gt;{ data($adresy[1]/ns0:street) }&lt;/ns1:UlicaAdresAlt&gt;
            }
            &lt;ns1:KrajZamieszkania&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerPersonal/ns0:CustomerPersonal/ns0:countryOfResidence/ns3:CountryCode/ns3:isoCountryCode) }&lt;/ns1:KrajZamieszkania&gt;
            &lt;ns1:ZrodloDochodu&gt;{ xs:string( data($invoke1/ns4:customer/ns0:Customer/ns0:customerFinancialInfo/ns0:CustomerFinancialInfo/ns0:sourceOfIncomeCode/ns3:SourceOfIncomeCode/ns3:sourceOfIncomeCode) ) }&lt;/ns1:ZrodloDochodu&gt;
            {
                let $policyLife := $invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyLife
                return
                    &lt;ns1:UbezpieczenieNaZycie&gt;{ local:booleanToString($policyLife) }&lt;/ns1:UbezpieczenieNaZycie&gt;
            }
            {
                let $policyOther := $invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyOther
                return
                    &lt;ns1:UbezpieczeniaInne&gt;{ local:booleanToString($policyOther) }&lt;/ns1:UbezpieczeniaInne&gt;
            }
            &lt;ns1:UbezpNaZycie?&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyLifeName/ns7:InsurerName/ns7:insurerName) }&lt;/ns1:UbezpNaZycie&gt;
            &lt;ns1:UbezpInne?&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyOtherName/ns7:InsurerName/ns7:insurerName) }&lt;/ns1:UbezpInne&gt;
            {
                for $customerOccupationCode in $invoke1/ns4:customer/ns0:Customer/ns0:customerEmploymentInfoList/ns0:CustomerEmploymentInfo[1]/ns0:occupationCode/ns3:CustomerOccupationCode/ns3:customerOccupationCode
                where $customerOccupationCode != ''
                return
                    &lt;ns1:KodZawoduWyuczonego&gt;{ xs:short(data($customerOccupationCode)) }&lt;/ns1:KodZawoduWyuczonego&gt;
            }
            {
                for $agreementBankConfInfo in $invoke1/ns4:customer/ns0:Customer/ns0:agreementBankInfo
                return
                    &lt;ns1:ZgodaTajemBank?&gt;{ local:booleanToString($agreementBankConfInfo) }&lt;/ns1:ZgodaTajemBank&gt;
            }
            {
                for $dateAgreementBankConfInfo in $invoke1/ns4:dateOfAgreement/ns5:DateHolder/ns5:value
                return
                    &lt;ns1:DataZgodyTajemBank?&gt;{ local:YMDToDMY($dateAgreementBankConfInfo) }&lt;/ns1:DataZgodyTajemBank&gt;
            }
            &lt;ns1:UbezpFundInwest?&gt;{ local:booleanToString($invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyInvest) }&lt;/ns1:UbezpFundInwest&gt;
            &lt;ns1:NazwaUbezpFundInwest?&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyInvestName) }&lt;/ns1:NazwaUbezpFundInwest&gt;
            &lt;ns1:SumaSkladekPolis?&gt;{ data($invoke1/ns4:customer/ns0:Customer/ns0:customerInsuranceList/ns0:CustomerInsurance[1]/ns0:policyPremiumSum) }&lt;/ns1:SumaSkladekPolis&gt;

        &lt;/ns1:pcCustNewRequest&gt;
};

declare variable $invoke1 as element(ns4:invoke) external;
declare variable $header1 as element(ns4:header) external;

xf:RequestTransform2($invoke1, $header1)</con:xquery>
</con:xqueryEntry>