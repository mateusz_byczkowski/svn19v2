<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/csr/messages/";
declare namespace xf = "http://bzwbk.com/services/csr/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappCsrCompRequest($req as element(m:pCsrCompRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:CompPep)
					then &lt;fml:C_COMP_PEP&gt;{ data($req/m:CompPep) }&lt;/fml:C_COMP_PEP&gt;
					else ()
			}
			{
				if($req/m:DataPep)
					then &lt;fml:C_DATA_PEP&gt;{ data($req/m:DataPep) }&lt;/fml:C_DATA_PEP&gt;
					else ()
			}
			{
				if($req/m:Pep)
					then &lt;fml:C_PEP&gt;{ data($req/m:Pep) }&lt;/fml:C_PEP&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mappCsrCompRequest($body/m:pCsrCompRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>