<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/csr/messages/";
declare namespace xf = "http://bzwbk.com/services/csr/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mappCsrCompResponse($fml as element(fml:FML32))
	as element(m:pCsrCompResponse) {
		&lt;m:pCsrCompResponse&gt;
			{
				if($fml/fml:C_STATUS)
					then &lt;m:Status&gt;{ data($fml/fml:C_STATUS) }&lt;/m:Status&gt;
					else ()
			}
		&lt;/m:pCsrCompResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mappCsrCompResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>