<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace cer="http://bzwbk.com/services/cerber";
declare namespace ns1="urn:be.services.dcl";
declare namespace fml="";
declare namespace con="http://www.bea.com/wli/sb/services/security/config";

declare variable $body as element(soap:Body) external;
declare variable $header as element(soap:Header) external;
declare variable $appCredential as element() external;

declare function getOperator() as xs:anyType
{ 
  let $oper := if (data($header/ns1:header/ns1:msgHeader/ns1:userId)) then 
  data($header/ns1:header/ns1:msgHeader/ns1:userId) 
  else data($header/ns1:header/ns1:msgHeader/userId)
  return $oper
};

declare function getElementsForSearchUsersRequest($parm as element(fml:FML32), $head as element(ns1:header), $credential as element()) as element(cer:SearchUsersRequest)
{
  &lt;cer:SearchUsersRequest&gt;
     &lt;cer:Skp&gt;{data($parm/CI_SKP_PRACOWNIKA)}&lt;/cer:Skp&gt;
     &lt;cer:FirstRow&gt;0&lt;/cer:FirstRow&gt;
     &lt;cer:NumRows&gt;1&lt;/cer:NumRows&gt;
     &lt;cer:AppAuthData&gt;
        &lt;ApplicationId&gt;{data($credential/con:UsernamePasswordCredential/con:username)}&lt;/ApplicationId&gt;
        &lt;ApplicationPassword&gt;{data($credential/con:UsernamePasswordCredential/con:password)}&lt;/ApplicationPassword&gt;
        &lt;Operator&gt;{getOperator()}&lt;/Operator&gt;
     &lt;/cer:AppAuthData&gt;
  &lt;/cer:SearchUsersRequest&gt;
};


getElementsForSearchUsersRequest($body/FML32, $header/ns1:header, $appCredential/credential)</con:xquery>
</con:xqueryEntry>