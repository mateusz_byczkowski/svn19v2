<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m  = "urn:be.services.dcl";
declare namespace m1 = "urn:accounts.entities.be.dcl";
declare namespace m2 = "urn:dictionaries.be.dcl";
declare namespace m3 = "urn:accountdict.dictionaries.be.dcl";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare variable $body as element(soap-env:Body) external;
declare variable $header as element(soap-env:Header) external;

declare function xf:convertTo2CharString($value as xs:integer) as xs:string {
  let $string := $value cast as xs:string

  return 
    if ($value &lt; 10) 
      then fn:concat("0",$string)
      else $string
};

declare function xf:convertTo4CharString($value as xs:integer) as xs:string {
   let $string := $value cast as xs:string

   return 
      if  ($value &lt; 10) 
         then fn:concat("000",$string)
      else if ($value &lt; 100) 
         then fn:concat("00",$string)
      else if ($value &lt; 1000) 
         then fn:concat("0",$string)
      else  $string
};

declare function xf:mapDate($dateIn as xs:date) as xs:string {
  fn:concat (xf:convertTo2CharString(fn:day-from-date($dateIn)),"-",
             xf:convertTo2CharString(fn:month-from-date($dateIn)),"-",
             xf:convertTo4CharString(fn:year-from-date($dateIn)))
};

declare function xf:shortAccount($account as xs:string) as xs:string {
    if (fn:string-length($account) &gt; 12)
      then fn:substring($account, (fn:string-length($account))-11)
      else $account
};

declare function xf:booleanTo01($dateIn as xs:boolean) as xs:string {
  if ($dateIn = true()) 
    then "1"
    else "0"
};

&lt;soap-env:Body&gt;
  {
    let $reqh := $header/m:header
    let $req  := $body/m:invoke/m:account/m1:Account
    let $req1 := $body/m:invoke/m:changeParameters/m1:ChangeParameters
    let $req2 := $req/m1:statementParameters/m1:StatementParameters
    let $req3 := $req2/m1:cycle/m2:Period
    let $req4 := $req2/m1:specialDay/m2:SpecialDay
    let $req5 := $req2/m1:snapshotType/m3:SnapshotType
    let $req6 := $req/m1:loanAccount/m1:LoanAccount
    let $req7 := $req2/m1:provideManner/m2:ProvideManner

    let $msgId := $reqh/m:msgHeader/m:msgId
    let $transId := $reqh/m:transHeader/m:transId
    let $userId  := $reqh/m:msgHeader/m:userId
    let $unitId  := $reqh/m:msgHeader/m:unitId
    return
    
    &lt;fml:FML32&gt;
      &lt;fml:DC_MSHEAD_MSGID?&gt;{ data($msgId) }&lt;/fml:DC_MSHEAD_MSGID&gt;
      &lt;fml:DC_TRN_ID?&gt;{ data($transId) }&lt;/fml:DC_TRN_ID&gt;
      &lt;fml:DC_UZYTKOWNIK?&gt;{concat("SKP:", data($userId)) }&lt;/fml:DC_UZYTKOWNIK&gt;

      {if($unitId)
          then 
              if($unitId &lt; 1000)
                then &lt;fml:DC_ODDZIAL&gt;{ data($unitId) }&lt;/fml:DC_ODDZIAL&gt;
                else &lt;fml:DC_ODDZIAL&gt;0&lt;/fml:DC_ODDZIAL&gt;
          else ()
      }
    
      {if($req1/m1:additionalDescription)
          then &lt;fml:DC_DODATKOWY_OPIS_1&gt;{ data($req1/m1:additionalDescription) }&lt;/fml:DC_DODATKOWY_OPIS_1&gt;
          else ()
      }
      {if($req1/m1:newInterestRate)
          then &lt;fml:DC_STOPA_PROCENTOWA&gt;{ data($req1/m1:newInterestRate) }&lt;/fml:DC_STOPA_PROCENTOWA&gt;
          else ()
      }
      {if($req1/m1:faceAmountOfNote)
          then &lt;fml:DC_KWOTA_KREDYTU&gt;{ data($req1/m1:faceAmountOfNote) }&lt;/fml:DC_KWOTA_KREDYTU&gt;
          else ()
      }
      {if($req1/m1:changeRate)
          then &lt;fml:DC_ZMIANA_STOPY&gt;{ xf:booleanTo01($req1/m1:changeRate) }&lt;/fml:DC_ZMIANA_STOPY&gt;
          else ()
      }
      
      {if($req/m1:accountNumber)
          then &lt;fml:DC_NR_RACHUNKU&gt;{ xf:shortAccount($req/m1:accountNumber) }&lt;/fml:DC_NR_RACHUNKU&gt;
          else ()
      }
      {if($req/m1:accountOpenDate and fn:string-length($req/m1:accountOpenDate)&gt;0)
          then &lt;fml:DC_DATA_OTWARCIA_RACHUNKU&gt;{ xf:mapDate($req/m1:accountOpenDate) }&lt;/fml:DC_DATA_OTWARCIA_RACHUNKU&gt;
          else ()
      }
      {if($req6/m1:contractNumber)
          then &lt;fml:DC_NUMER_WNIOSKU_KREDYTOWEGO&gt;{ data($req6/m1:contractNumber) }&lt;/fml:DC_NUMER_WNIOSKU_KREDYTOWEGO&gt;
          else ()
      }
      
      {if($req2/m1:frequency)
          then &lt;fml:B_CZEST_WYCIAGU&gt;{ data($req2/m1:frequency) }&lt;/fml:B_CZEST_WYCIAGU&gt;
          else ()
      }
      {if($req2/m1:specialDay)
          then &lt;fml:B_DZIEN_WYCIAGU&gt;{ data($req2/m1:specialDay) }&lt;/fml:B_DZIEN_WYCIAGU&gt;
          else ()
      }
      {if($req2/m1:provideManner)
          then &lt;fml:B_K_S_P_WYCIAGU&gt;{ data($req2/m1:provideManner) }&lt;/fml:B_K_S_P_WYCIAGU&gt;
          else ()
      }
      {if($req2/m1:nextPrintoutDate and fn:string-length($req2/m1:nextPrintoutDate)&gt;0)
          then &lt;fml:B_D_NAST_WYCIAGU&gt;{ xf:mapDate($req2/m1:nextPrintoutDate) }&lt;/fml:B_D_NAST_WYCIAGU&gt;
          else ()
      }
      {if($req3/m2:period)
          then &lt;fml:B_OKRES_WYCIAGU&gt;{ data($req3/m2:period) }&lt;/fml:B_OKRES_WYCIAGU&gt;
          else ()
      }
      {if($req5/m3:snapshotType)
          then &lt;fml:B_KOD_WYCIAGU&gt;{ data($req5/m3:snapshotType) }&lt;/fml:B_KOD_WYCIAGU&gt;
          else ()
      }
    &lt;/fml:FML32&gt;
  }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>