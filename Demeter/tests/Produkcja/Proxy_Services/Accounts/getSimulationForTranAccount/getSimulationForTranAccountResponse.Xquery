<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-return element="ns2:invokeResponse" location="getSimulationForTranAccount.wsdl" ::)
(:: pragma bea:global-element-return element="ns1:FML32" location="../../Operations/savetransfer/savetransferOUT.xsd" ::)



(:~
 :
 : @author  Grzegorz Strawa
 : @version 1.0
 : @since   2010-03-22
 :
 : wersja WSDLa: 03-12-2009 15:44:24
 :
 : $Proxy Services/Accounts/getSimulationForTranAccount/getSimulationForTranAccountResponse.xq$
 :
 :)  

 
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/getSimulationForTranAccount/getSimulationForTranAccountResponse/";
declare namespace ns0 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns1 = "http://sygnity.pl/saveTransferResponse";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:getSimulationForTranAccountResponse($fMLOUT1 as element(ns1:FML32))
    as element(ns2:invokeResponse) {
        &lt;ns2:invokeResponse&gt;
            &lt;ns2:simulationForTranAccount&gt;
                &lt;ns0:SimulationForTranAccount&gt;
                {
                if (data($fMLOUT1/ns1:TR_KAPITAL)) then 
                    &lt;ns0:currentBalance&gt;{ data($fMLOUT1/ns1:TR_KAPITAL) }&lt;/ns0:currentBalance&gt; else ()
                }
                
  (::
   : Odsetki Wn to wartość zwrócona w wyżej opisanym polu i jest ona ujemna. Odpowiada ona w funkcji ICBS - 202076 Praca z wyliczeniem salda zamknięcia rachunku - wartości w polu Odsetki za TOD/ACA.
   : Odsetki Ma to wartość zwrócona w tym samym polu, tyle że dodatnia. Odpowiada ona w funkcji ICBS - 202076 Praca z wyliczeniem salda zamknięcia rachunku - wartości w polu Odsetki należne.
   : w związku z powyższym proszę o dodanie pola FML - TR_ODSETKI_WN_LUB_MA i wprowadzenie mapowania tego pola z taga 737. Po wprowadzeniu tych zmian proszę przekazać TEET na DCLContact w celu mapowania wartości z tego pola na odpowiednią encję w zależności od tego czy będzie ona dodatnia czy ujemna. 
   ::)           
               
               { 
                if (data($fMLOUT1/ns1:TR_ODSETKI_WN_LUB_MA)) then
                
                if ((data($fMLOUT1/ns1:TR_ODSETKI_WN_LUB_MA))&lt;0) then
                 ( 
                    &lt;ns0:interestDueDebit&gt;{ data($fMLOUT1/ns1:TR_ODSETKI_WN_LUB_MA) }&lt;/ns0:interestDueDebit&gt;,
                    &lt;ns0:interestDueCredit&gt;0&lt;/ns0:interestDueCredit&gt;
                  )
                  else(
                  	if ((data($fMLOUT1/ns1:TR_ODSETKI_WN_LUB_MA))&gt;0) then
                  	(
	                	&lt;ns0:interestDueCredit&gt;{ data($fMLOUT1/ns1:TR_ODSETKI_WN_LUB_MA) }&lt;/ns0:interestDueCredit&gt;,
	                	&lt;ns0:interestDueDebit&gt;0&lt;/ns0:interestDueDebit&gt;
                  	)
                  	else
                  		( 
                  		&lt;ns0:interestDueCredit&gt;0&lt;/ns0:interestDueCredit&gt;,
	                	&lt;ns0:interestDueDebit&gt;0&lt;/ns0:interestDueDebit&gt;
                  		) 
                  )
    			else()
                }
    
    			{
                 if (data($fMLOUT1/ns1:TR_NALEZNA_OPLATA)) then 
                     &lt;ns0:serviceChargeAmount&gt;{ data($fMLOUT1/ns1:TR_NALEZNA_OPLATA) }&lt;/ns0:serviceChargeAmount&gt; else () 
                }  
           	               
               {
                if (data($fMLOUT1/ns1:TR_ODSETKI_POTRACONE)) then
                    &lt;ns0:taxInterestDueCt&gt;{ data($fMLOUT1/ns1:TR_ODSETKI_POTRACONE) }&lt;/ns0:taxInterestDueCt&gt; else ()
                }
           		{
                if (data($fMLOUT1/ns1:TR_SALDO_DO_WYPLATY)) then
                    &lt;ns0:closeBalance&gt;{ data($fMLOUT1/ns1:TR_SALDO_DO_WYPLATY) }&lt;/ns0:closeBalance&gt; else ()
                }
           
              {
              if (data($fMLOUT1/ns1:TR_DATA_OPER)) then
                    &lt;ns0:icbsDateToClose&gt;{  
                        let $transactionDate := $fMLOUT1/ns1:TR_DATA_OPER 	return
						fn:concat(
							fn:substring(data($transactionDate ), 7, 4),
							'-',
							fn:substring(data($transactionDate ), 4, 2),
							'-',
							fn:substring(data($transactionDate ), 1, 2)
							)
							}	&lt;/ns0:icbsDateToClose&gt;	
             else ()
             } 
          &lt;/ns0:SimulationForTranAccount&gt;
         &lt;/ns2:simulationForTranAccount&gt;
        &lt;/ns2:invokeResponse&gt;
};

declare variable $fMLOUT1 as element(ns1:FML32) external;

&lt;soap-env:Body&gt;
{
xf:getSimulationForTranAccountResponse($fMLOUT1)
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>