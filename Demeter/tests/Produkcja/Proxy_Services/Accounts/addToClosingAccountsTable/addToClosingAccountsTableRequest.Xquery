<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.2
 : @since   2010-01-19
 :
 : wersja WSDLa: 08-01-2010 13:09:21
 :
 : $Proxy Services/Accounts/addToClosingAccountsTable/addToClosingAccountsTableRequest.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Accounts/addToClosingAccountsTable/addToClosingAccountsTableRequest/";
declare namespace ns0 = "urn:accountdict.dictionaries.be.dcl";
declare namespace ns1 = "urn:accounts.entities.be.dcl";
declare namespace ns2 = "urn:be.services.dcl";
declare namespace ns3 = "";

declare variable $header1 as element(ns2:header) external;
declare variable $invoke1 as element(ns2:invoke) external;

(:~
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacja wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:addToClosingAccountsTableRequest($header1 as element(ns2:header),
													   $invoke1 as element(ns2:invoke))
    as element(ns3:FML32)
{
	&lt;ns3:FML32&gt;

		&lt;ns3:DC_TRN_ID?&gt;{
    	data($header1/ns2:transHeader/ns2:transId)
		}&lt;/ns3:DC_TRN_ID&gt;

	    &lt;ns3:DC_UZYTKOWNIK?&gt;{
			data($header1/ns2:msgHeader/ns2:userId)
		}&lt;/ns3:DC_UZYTKOWNIK&gt;
	
	    &lt;ns3:DC_ODDZIAL?&gt;{
			data($header1/ns2:msgHeader/ns2:unitId)
		}&lt;/ns3:DC_ODDZIAL&gt;
	
		&lt;ns3:NF_CTRL_OPTION&gt;
			1
		&lt;/ns3:NF_CTRL_OPTION&gt;
	
	    &lt;ns3:NF_ACCOCT_ENDDATE?&gt;{
			data($invoke1/ns2:accountClosingTask/ns1:AccountClosingTask/ns1:endDate)
		}&lt;/ns3:NF_ACCOCT_ENDDATE&gt;
	
	    &lt;ns3:NF_ACCOCT_REASON?&gt;{
			data($invoke1/ns2:accountClosingTask/ns1:AccountClosingTask/ns1:reason)
		}&lt;/ns3:NF_ACCOCT_REASON&gt;
	
	    &lt;ns3:NF_ACCOCT_REGISTERUSER?&gt;{
			data($invoke1/ns2:accountClosingTask/ns1:AccountClosingTask/ns1:registerUser)
		}&lt;/ns3:NF_ACCOCT_REGISTERUSER&gt;
	
		{
			let $issueDate := $invoke1/ns2:accountClosingTask/ns1:AccountClosingTask/ns1:issueDate
			return
				if (data($issueDate)) then
		    		&lt;ns3:NF_ACCOCT_ISSUEDATE&gt;{
						fn:substring($issueDate, 1, 10)
					}&lt;/ns3:NF_ACCOCT_ISSUEDATE&gt;					
				else ()
		}
	
	    &lt;ns3:NF_ACCOCR_ACCOUNTCLOSINGRE?&gt;{
			data($invoke1/ns2:accountClosingTask/ns1:AccountClosingTask/ns1:reasonType/ns0:AccountClosingReason/ns0:accountClosingReason)
		}&lt;/ns3:NF_ACCOCR_ACCOUNTCLOSINGRE&gt;
	
	    &lt;ns3:NF_ACCCRB_ACCOUNTCLOSINGRE?&gt;{
			data($invoke1/ns2:accountClosingTask/ns1:AccountClosingTask/ns1:closingReasonBank/ns0:AccountClosingReasonBank/ns0:accountClosingReasonBank)
		}&lt;/ns3:NF_ACCCRB_ACCOUNTCLOSINGRE&gt;
	
	    &lt;ns3:NF_DSFTAC_DISPSTATFORTRANA?&gt;{
			data($invoke1/ns2:accountClosingTask/ns1:AccountClosingTask/ns1:closingTaskStatus/ns0:DispStatForTranAccClosTask/ns0:dispStatForTranAccClosTask)
		}&lt;/ns3:NF_DSFTAC_DISPSTATFORTRANA&gt;
	
	{
	    (: przycinami IBAN do 12 osttatnich:)
		let $accountNumber := data($invoke1/ns2:account/ns1:Account/ns1:accountNumber)
			return
				if ($accountNumber ) then
		    		&lt;ns3:NF_ACCOUN_ACCOUNTNUMBER?&gt;{
						fn:substring($accountNumber , (fn:string-length($accountNumber))-11)
					}&lt;/ns3:NF_ACCOUN_ACCOUNTNUMBER&gt;					
				else
					()
		}
	(:~    &lt;ns3:NF_ACCOUN_ACCOUNTNUMBER?&gt;{
	:		data($invoke1/ns2:account/ns1:Account/ns1:accountNumber)
	:	}&lt;/ns3:NF_ACCOUN_ACCOUNTNUMBER&gt;
	:)		
	&lt;/ns3:FML32&gt;
};

&lt;soap-env:Body&gt;{
	xf:addToClosingAccountsTableRequest($header1, $invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>