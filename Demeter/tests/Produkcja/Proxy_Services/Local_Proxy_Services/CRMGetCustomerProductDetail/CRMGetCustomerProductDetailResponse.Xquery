<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.2.4 2010-01-12 PKL NP2036 CR2 Dodatkowe pola systemu EGERIA
v.1.2.3 2009-11-10 PKL NP2036 Dodanie pól systemu EGERIA
v.1.2.2 2009-08-03 PKL NP1995 Dodanie pól CI_STATUS i CI_STATUS_AKTYWNOSCI</con:description>
  <con:xquery>(: Change Log 
 v.1.2.2 2009-08-03 PKL NP1995 Dodanie pól ProductStat i ProductStatusCode 
 v.1.2.3 2009-11-10 PKL NP2036 Dodanie pól systemu EGERIA
 v.1.2.4 2010-01-12 PKL NP2036 CR2 Dodatkowe pola systemu EGERIA
:)

declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetCustomerProductDetailResponse($fml as element(fml:FML32))
	as element(m:CRMGetCustomerProductDetailResponse) {
		&lt;m:CRMGetCustomerProductDetailResponse&gt;
			{
				if($fml/fml:CI_PRODUCT_CATEGORY_ID)
					then &lt;m:ProductCategoryId&gt;{ data($fml/fml:CI_PRODUCT_CATEGORY_ID) }&lt;/m:ProductCategoryId&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_CATEGORY_CODE)
					then &lt;m:ProductCategoryCode&gt;{ data($fml/fml:CI_PRODUCT_CATEGORY_CODE) }&lt;/m:ProductCategoryCode&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_NAME_PL)
					then &lt;m:ProductNamePl&gt;{ data($fml/fml:CI_PRODUCT_NAME_PL) }&lt;/m:ProductNamePl&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_NAME_EN)
					then &lt;m:ProductNameEn&gt;{ data($fml/fml:CI_PRODUCT_NAME_EN) }&lt;/m:ProductNameEn&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ID)
					then &lt;m:ProductId&gt;{ data($fml/fml:CI_PRODUCT_ID) }&lt;/m:ProductId&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_CODE)
					then &lt;m:ProductCode&gt;{ data($fml/fml:CI_PRODUCT_CODE) }&lt;/m:ProductCode&gt;
					else ()
			}
			{
				if($fml/fml:B_POJEDYNCZY)
					then &lt;m:Pojedynczy&gt;{ data($fml/fml:B_POJEDYNCZY) }&lt;/m:Pojedynczy&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_RACHUNKU)
					then &lt;m:NrRachunku&gt;{ data($fml/fml:DC_NR_RACHUNKU) }&lt;/m:NrRachunku&gt;
					else ()
			}
			{
				if($fml/fml:B_KOD_RACH)
					then &lt;m:KodRach&gt;{ data($fml/fml:B_KOD_RACH) }&lt;/m:KodRach&gt;
					else ()
			}
			{
				if($fml/fml:DC_PAKIET)
					then &lt;m:Pakiet&gt;{ data($fml/fml:DC_PAKIET) }&lt;/m:Pakiet&gt;
					else ()
			}
			{
				if($fml/fml:DC_IS_RACH_POWIAZANY)
					then &lt;m:RachPowiazany&gt;{ data($fml/fml:DC_IS_RACH_POWIAZANY) }&lt;/m:RachPowiazany&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_RACHUNKU_W_SYSTEMIE)
					then &lt;m:NrAlternatywny&gt;{ data($fml/fml:DC_NR_RACHUNKU_W_SYSTEMIE) }&lt;/m:NrAlternatywny&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT1)
					then &lt;m:ProductAtt1&gt;{ data($fml/fml:CI_PRODUCT_ATT1) }&lt;/m:ProductAtt1&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT2)
					then &lt;m:ProductAtt2&gt;{ data($fml/fml:CI_PRODUCT_ATT2) }&lt;/m:ProductAtt2&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT3)
					then &lt;m:ProductAtt3&gt;{ data($fml/fml:CI_PRODUCT_ATT3) }&lt;/m:ProductAtt3&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT4)
					then &lt;m:ProductAtt4&gt;{ data($fml/fml:CI_PRODUCT_ATT4) }&lt;/m:ProductAtt4&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_ATT5)
					then &lt;m:ProductAtt5&gt;{ data($fml/fml:CI_PRODUCT_ATT5) }&lt;/m:ProductAtt5&gt;
					else ()
			}
			{
				if($fml/fml:CI_RACHUNEK_ADR_ALT)
					then &lt;m:RachunekAdrAlt&gt;{ data($fml/fml:CI_RACHUNEK_ADR_ALT) }&lt;/m:RachunekAdrAlt&gt;
					else ()
			}
			{
				if($fml/fml:DC_IMIE_I_NAZWISKO_ALT)
					then &lt;m:ImieINazwiskoAlt&gt;{ data($fml/fml:DC_IMIE_I_NAZWISKO_ALT) }&lt;/m:ImieINazwiskoAlt&gt;
					else ()
			}
			{
				if($fml/fml:DC_ULICA_ADRES_ALT)
					then &lt;m:UlicaAdresAlt&gt;{ data($fml/fml:DC_ULICA_ADRES_ALT) }&lt;/m:UlicaAdresAlt&gt;
					else ()
			}
			{
				if($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT)
					then &lt;m:NrPosesLokaluAdresAlt&gt;{ data($fml/fml:DC_NR_POSES_LOKALU_ADRES_ALT) }&lt;/m:NrPosesLokaluAdresAlt&gt;
					else ()
			}
			{
				if($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT)
					then &lt;m:KodPocztowyAdresAlt&gt;{ data($fml/fml:DC_KOD_POCZTOWY_ADRES_ALT) }&lt;/m:KodPocztowyAdresAlt&gt;
					else ()
			}
(: 1.2.2 NP1995 początek :)         {
				if($fml/fml:CI_STATUS_AKTYWNOSCI)
					then &lt;m:ProductStat&gt;{ data($fml/fml:CI_STATUS_AKTYWNOSCI) }&lt;/m:ProductStat&gt;
					else ()
			}
                                                {
			                if($fml/fml:CI_STATUS)
					then &lt;m:ProductStatusCode&gt;{ data($fml/fml:CI_STATUS) }&lt;/m:ProductStatusCode&gt;
					else ()
(: 1.2.2 NP1995 koniec :)             }
			{
				if($fml/fml:B_SALDO)
					then &lt;m:Saldo&gt;{ data($fml/fml:B_SALDO) }&lt;/m:Saldo&gt;
					else ()
			}
			{
				if($fml/fml:B_DOST_SRODKI)
					then &lt;m:DostSrodki&gt;{ data($fml/fml:B_DOST_SRODKI) }&lt;/m:DostSrodki&gt;
					else ()
			}
			{
				if($fml/fml:B_BLOKADA)
					then &lt;m:Blokada&gt;{ data($fml/fml:B_BLOKADA) }&lt;/m:Blokada&gt;
					else ()
			}
			{
				if($fml/fml:B_KOD_WALUTY)
					then &lt;m:KodWaluty&gt;{ data($fml/fml:B_KOD_WALUTY) }&lt;/m:KodWaluty&gt;
					else ()
			}
			{
				if($fml/fml:B_D_OTWARCIA)
					then &lt;m:DOtwarcia&gt;{ data($fml/fml:B_D_OTWARCIA) }&lt;/m:DOtwarcia&gt;
					else ()
			}
			{
				if($fml/fml:B_DATA_OST_OPER)
					then &lt;m:DataOstOper&gt;{ data($fml/fml:B_DATA_OST_OPER) }&lt;/m:DataOstOper&gt;
					else ()
			}
			{
				if($fml/fml:B_LIMIT1)
					then &lt;m:Limit1&gt;{ data($fml/fml:B_LIMIT1) }&lt;/m:Limit1&gt;
					else ()
			}
			{
				if($fml/fml:CI_PRODUCT_NAME_ORYGINAL)
					then &lt;m:ProductNameOryginal&gt;{ data($fml/fml:CI_PRODUCT_NAME_ORYGINAL) }&lt;/m:ProductNameOryginal&gt;
					else ()
			}
			{
				if($fml/fml:B_OPROC1)
					then &lt;m:Oproc1&gt;{ data($fml/fml:B_OPROC1) }&lt;/m:Oproc1&gt;
					else ()
			}
			{
				if($fml/fml:DC_ODDZIAL)
					then &lt;m:Oddzial&gt;{ data($fml/fml:DC_ODDZIAL) }&lt;/m:Oddzial&gt;
					else ()
			}
			{
				if($fml/fml:CI_WLASCICIEL)
					then &lt;m:Wlasciciel&gt;{ data($fml/fml:CI_WLASCICIEL) }&lt;/m:Wlasciciel&gt;
					else ()
			}
			{
				if($fml/fml:CI_WSPOLPOSIADACZ)
					then &lt;m:Wspolposiadacz&gt;{ data($fml/fml:CI_WSPOLPOSIADACZ) }&lt;/m:Wspolposiadacz&gt;
					else ()
			}
			{
				if($fml/fml:CI_PELNOMOCNIK)
					then &lt;m:Pelnomocnik&gt;{ data($fml/fml:CI_PELNOMOCNIK) }&lt;/m:Pelnomocnik&gt;
					else ()
			}
			{
				if($fml/fml:B_D_OSTATNIEJ_KAP)
					then &lt;m:DOstatniejKap&gt;{ data($fml/fml:B_D_OSTATNIEJ_KAP) }&lt;/m:DOstatniejKap&gt;
					else ()
			}
			{
				if($fml/fml:B_D_NASTEPNEJ_KAP)
					then &lt;m:DNastepnejKap&gt;{ data($fml/fml:B_D_NASTEPNEJ_KAP) }&lt;/m:DNastepnejKap&gt;
					else ()
			}
			{
				if($fml/fml:B_D_KONCA_LIMITU)
					then &lt;m:DKoncaLimitu&gt;{ data($fml/fml:B_D_KONCA_LIMITU) }&lt;/m:DKoncaLimitu&gt;
					else ()
			}
			{
				if($fml/fml:B_ODS_POP_OKR)
					then &lt;m:OdsPopOkr&gt;{ data($fml/fml:B_ODS_POP_OKR) }&lt;/m:OdsPopOkr&gt;
					else ()
			}
			{
				if($fml/fml:B_D_OST_ZEST)
					then &lt;m:DOstZest&gt;{ data($fml/fml:B_D_OST_ZEST) }&lt;/m:DOstZest&gt;
					else ()
			}
			{
				if($fml/fml:B_ZAL_MIN)
					then &lt;m:ZalMin&gt;{ data($fml/fml:B_ZAL_MIN) }&lt;/m:ZalMin&gt;
					else ()
			}
			{
				if($fml/fml:B_BIEZ_MIN)
					then &lt;m:BiezMin&gt;{ data($fml/fml:B_BIEZ_MIN) }&lt;/m:BiezMin&gt;
					else ()
			}
			{
				if($fml/fml:B_SPLATA_MIN)
					then &lt;m:SplataMin&gt;{ data($fml/fml:B_SPLATA_MIN) }&lt;/m:SplataMin&gt;
					else ()
			}
			{
				if($fml/fml:B_DATA_B_PLAT)
					then &lt;m:DataBPlat&gt;{ data($fml/fml:B_DATA_B_PLAT) }&lt;/m:DataBPlat&gt;
					else ()
			}
			{
				if($fml/fml:B_D_NAST_ZEST)
					then &lt;m:DNastZest&gt;{ data($fml/fml:B_D_NAST_ZEST) }&lt;/m:DNastZest&gt;
					else ()
			}
			{
				if($fml/fml:B_NR_RACH)
					then &lt;m:NrRach&gt;{ data($fml/fml:B_NR_RACH) }&lt;/m:NrRach&gt;
					else ()
			}
			{
				if($fml/fml:B_KARTA)
					then &lt;m:Karta&gt;{ data($fml/fml:B_KARTA) }&lt;/m:Karta&gt;
					else ()
			}
			{
				if($fml/fml:B_KK_EXP_DATE)
					then &lt;m:KkExpDate&gt;{ data($fml/fml:B_KK_EXP_DATE) }&lt;/m:KkExpDate&gt;
					else ()
			}
			{
				if($fml/fml:B_KREDYT_PRZYZN)
					then &lt;m:KredytPrzyzn&gt;{ data($fml/fml:B_KREDYT_PRZYZN) }&lt;/m:KredytPrzyzn&gt;
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_AKT)
					then &lt;m:OdsSkapAkt&gt;{ data($fml/fml:B_ODS_SKAP_AKT) }&lt;/m:OdsSkapAkt&gt;
					else ()
			}
			{
				if($fml/fml:B_D_OST_SPLATY)
					then &lt;m:DOstSplaty&gt;{ data($fml/fml:B_D_OST_SPLATY) }&lt;/m:DOstSplaty&gt;
					else ()
			}
			{
				if($fml/fml:DC_DATA_URUCHOMIENIA)
					then &lt;m:DataUruchomienia&gt;{ data($fml/fml:DC_DATA_URUCHOMIENIA) }&lt;/m:DataUruchomienia&gt;
					else ()
			}
			{
				if($fml/fml:DC_DATA_PLATNOSCI)
					then &lt;m:DataPlatnosci&gt;{ data($fml/fml:DC_DATA_PLATNOSCI) }&lt;/m:DataPlatnosci&gt;
					else ()
			}
			{
				if($fml/fml:B_PRZEKS_KAP)
					then &lt;m:PrzeksKap&gt;{ data($fml/fml:B_PRZEKS_KAP) }&lt;/m:PrzeksKap&gt;
					else ()
			}
			{
				if($fml/fml:B_JDN_OKR_WKLADU)
					then &lt;m:JdnOkrWkladu&gt;{ data($fml/fml:B_JDN_OKR_WKLADU) }&lt;/m:JdnOkrWkladu&gt;
					else ()
			}
			{
				if($fml/fml:B_D_KON_WKL)
					then &lt;m:DKonWkl&gt;{ data($fml/fml:B_D_KON_WKL) }&lt;/m:DKonWkl&gt;
					else ()
			}
			{
				if($fml/fml:B_ODS_SKAP_POP)
					then &lt;m:OdsSkapPop&gt;{ data($fml/fml:B_ODS_SKAP_POP) }&lt;/m:OdsSkapPop&gt;
					else ()
			}
			{
				if($fml/fml:B_KAPITALIZACJA)
					then &lt;m:Kapitalizacja&gt;{ data($fml/fml:B_KAPITALIZACJA) }&lt;/m:Kapitalizacja&gt;
					else ()
			}
			{
				if($fml/fml:B_OKR_WKLADU)
					then &lt;m:OkrWkladu&gt;{ data($fml/fml:B_OKR_WKLADU) }&lt;/m:OkrWkladu&gt;
					else ()
			}
			{
				if($fml/fml:B_D_POCZ_WKL)
					then &lt;m:DPoczWkl&gt;{ data($fml/fml:B_D_POCZ_WKL) }&lt;/m:DPoczWkl&gt;
					else ()
			}
			{
				if($fml/fml:B_SPLATA_RAZEM)
					then &lt;m:SplataRazem&gt;{ data($fml/fml:B_SPLATA_RAZEM) }&lt;/m:SplataRazem&gt;
					else ()
			}   

			{
				if($fml/fml:B_D_NAST_OPER)
					then &lt;m:DNastOper&gt;{ data($fml/fml:B_D_NAST_OPER) }&lt;/m:DNastOper&gt;
					else ()
			}   

			{
				if($fml/fml:B_ODS_POP_WYMAG)
					then &lt;m:OdsPopWymag&gt;{ data($fml/fml:B_ODS_POP_WYMAG) }&lt;/m:OdsPopWymag&gt;
					else ()
			}   

(: NP2036 start :)
{
if($fml/fml:CI_ID_SYS and data($fml/fml:CI_ID_SYS)=3)
then
  &lt;m:Egeria&gt; 
  {
  if ($fml/fml:EG_ID_SPOLKI)
    then if (data($fml/fml:EG_ID_SPOLKI)="L")
      then &lt;m:nazwaSpolki&gt;BZWBK Leasing S.A.&lt;/m:nazwaSpolki&gt;
      elseif (data($fml/fml:EG_ID_SPOLKI)="FL")
      then &lt;m:nazwaSpolki&gt;BZWBK Finanse &amp; Leasing S.A.&lt;/m:nazwaSpolki&gt;
    else ()
  else ()
  }  
  {
  if($fml/fml:EG_NR_UMOWY)
    then &lt;m:nrUmowy&gt;{ data($fml/fml:EG_NR_UMOWY) }&lt;/m:nrUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_BIEZ_ETAP_UMOWY)
    then &lt;m:biezEtapUmowy&gt;{data($fml/fml:EG_BIEZ_ETAP_UMOWY) }&lt;/m:biezEtapUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_DATA_OTWARCIA_UMOWY)
    then &lt;m:dataOtwUmowy&gt;{data($fml/fml:EG_DATA_OTWARCIA_UMOWY) }&lt;/m:dataOtwUmowy&gt; 
    else ()	
  }
  
  {
  if($fml/fml:EG_DATA_KONCA_UMOWY)
    then &lt;m:dataKoncaUmowy&gt;{data($fml/fml:EG_DATA_KONCA_UMOWY) }&lt;/m:dataKoncaUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_OKRES_UMOWY)
    then &lt;m:okresUmowy&gt;{data($fml/fml:EG_OKRES_UMOWY) }&lt;/m:okresUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_KOD_WALUTY)
    then &lt;m:kodWalutyUmowy&gt;{data($fml/fml:EG_KOD_WALUTY) }&lt;/m:kodWalutyUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_RODZAJ_UMOWY)
    then &lt;m:rodzajUmowy&gt;{data($fml/fml:EG_RODZAJ_UMOWY) }&lt;/m:rodzajUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PROCEDURA)
    then &lt;m:procedura&gt;{data($fml/fml:EG_PROCEDURA) }&lt;/m:procedura&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PRZEDMIOT_UMOWY)
    then &lt;m:przedmiotUmowy&gt;{data($fml/fml:EG_PRZEDMIOT_UMOWY) }&lt;/m:przedmiotUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_TYP_RATY)
    then &lt;m:typRaty&gt;{data($fml/fml:EG_TYP_RATY) }&lt;/m:typRaty&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_POCZ_PORTFEL_UMOWY_NET)
    then &lt;m:poczPortfelUmowyNet&gt;{data($fml/fml:EG_POCZ_PORTFEL_UMOWY_NET) }&lt;/m:poczPortfelUmowyNet&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SALDO_UMOWY_NET)
    then &lt;m:saldoUmowyNet&gt;{data($fml/fml:EG_SALDO_UMOWY_NET) }&lt;/m:saldoUmowyNet&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PORTFEL_NET_1Q)
    then &lt;m:portfelNet1Q&gt;{data($fml/fml:EG_PORTFEL_NET_1Q) }&lt;/m:portfelNet1Q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PORTFEL_NET_2Q)
    then &lt;m:portfelNet2Q&gt;{data($fml/fml:EG_PORTFEL_NET_2Q) }&lt;/m:portfelNet2Q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PORTFEL_NET_3Q)
    then &lt;m:portfelNet3Q&gt;{data($fml/fml:EG_PORTFEL_NET_3Q) }&lt;/m:portfelNet3Q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PORTFEL_NET_4Q)
    then &lt;m:portfelNet4Q&gt;{data($fml/fml:EG_PORTFEL_NET_4Q) }&lt;/m:portfelNet4Q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_P_PORTFEL_NET_1Q)
    then &lt;m:portfelNetPoprz1Q&gt;{data($fml/fml:EG_P_PORTFEL_NET_1Q) }&lt;/m:portfelNetPoprz1Q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_P_PORTFEL_NET_2Q)
    then &lt;m:portfelNetPoprz2Q&gt;{data($fml/fml:EG_P_PORTFEL_NET_2Q) }&lt;/m:portfelNetPoprz2Q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_P_PORTFEL_NET_3Q)
    then &lt;m:portfelNetPoprz3Q&gt;{data($fml/fml:EG_P_PORTFEL_NET_3Q) }&lt;/m:portfelNetPoprz3Q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_P_PORTFEL_NET_4Q)
    then &lt;m:portfelNetPoprz4Q&gt;{data($fml/fml:EG_P_PORTFEL_NET_4Q) }&lt;/m:portfelNetPoprz4Q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_WART_NET_PRZEDM_UMOWY)
    then &lt;m:wartNetPrzedmUmowy&gt;{data($fml/fml:EG_WART_NET_PRZEDM_UMOWY) }&lt;/m:wartNetPrzedmUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PROC_WPLATA_WLASNA)
    then &lt;m:procWplataWlasna&gt;{data($fml/fml:EG_PROC_WPLATA_WLASNA) }&lt;/m:procWplataWlasna&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PROC_WART_KONCOWA)
    then &lt;m:procWartKoncowa&gt;{data($fml/fml:EG_PROC_WART_KONCOWA) }&lt;/m:procWartKoncowa&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PROC_MARZA_SWWR)
    then &lt;m:procMarzaSWWR&gt;{data($fml/fml:EG_PROC_MARZA_SWWR) }&lt;/m:procMarzaSWWR&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PROC_SPL_UMOWY)
    then &lt;m:procSplUmowy&gt;{data($fml/fml:EG_PROC_SPL_UMOWY) }&lt;/m:procSplUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_PROC_SPL_PRZEDM_UMOWY)
    then &lt;m:procSplPrzedmUmowy&gt;{data($fml/fml:EG_PROC_SPL_PRZEDM_UMOWY) }&lt;/m:procSplPrzedmUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_KURS_WAL_DATA)
    then &lt;m:kursWalData&gt;{data($fml/fml:EG_KURS_WAL_DATA) }&lt;/m:kursWalData&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_OBC_NET_POPRZ_ROK_KAP)
    then &lt;m:obcNetPoprzRokKap&gt;{data($fml/fml:EG_OBC_NET_POPRZ_ROK_KAP) }&lt;/m:obcNetPoprzRokKap&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_OBC_NET_POPRZ_ROK_ODS)
    then &lt;m:obcNetPoprzRokOds&gt;{data($fml/fml:EG_OBC_NET_POPRZ_ROK_ODS) }&lt;/m:obcNetPoprzRokOds&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_OBC_NET_POPRZ_ROK_SUMA)
    then &lt;m:obcNetPoprzRokSuma&gt;{data($fml/fml:EG_OBC_NET_POPRZ_ROK_SUMA) }&lt;/m:obcNetPoprzRokSuma&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_OBC_NET_BIEZ_ROK_KAP)
    then &lt;m:obcNetBiezRokKap&gt;{data($fml/fml:EG_OBC_NET_BIEZ_ROK_KAP) }&lt;/m:obcNetBiezRokKap&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_OBC_NET_BIEZ_ROK_ODS)
    then &lt;m:obcNetBiezRokOds&gt;{data($fml/fml:EG_OBC_NET_BIEZ_ROK_ODS) }&lt;/m:obcNetBiezRokOds&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_OBC_NET_BIEZ_ROK_SUMA)
    then &lt;m:obcNetBiezRokSuma&gt;{data($fml/fml:EG_OBC_NET_BIEZ_ROK_SUMA) }&lt;/m:obcNetBiezRokSuma&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_OBC_NET_NAST_ROK_KAP)
    then &lt;m:obcNetNastRokKap&gt;{data($fml/fml:EG_OBC_NET_NAST_ROK_KAP) }&lt;/m:obcNetNastRokKap&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_OBC_NET_NAST_ROK_ODS)
    then &lt;m:obcNetNastRokOds&gt;{data($fml/fml:EG_OBC_NET_NAST_ROK_ODS) }&lt;/m:obcNetNastRokOds&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_OBC_NET_NAST_ROK_SUMA)
    then &lt;m:obcNetNastRokSuma&gt;{data($fml/fml:EG_OBC_NET_NAST_ROK_SUMA) }&lt;/m:obcNetNastRokSuma&gt; 
    else ()	
  }
  
  {
  if($fml/fml:EG_SY_KOD_KLIENTA)
    then &lt;m:synKodKlienta&gt;{data($fml/fml:EG_SY_KOD_KLIENTA) }&lt;/m:synKodKlienta&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_OPIEKUN_KLIENTA)
    then &lt;m:synOpiekunKlienta&gt;{data($fml/fml:EG_SY_OPIEKUN_KLIENTA) }&lt;/m:synOpiekunKlienta&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_SUMA_POCZ_POR_NET)
    then &lt;m:synSuma_pocz_portf_net&gt;{data($fml/fml:EG_SY_SUMA_POCZ_POR_NET) }&lt;/m:synSuma_pocz_portf_net&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_SUMA_BIEZ_POR_NET)
    then &lt;m:synSumaBiezPortf_net&gt;{data($fml/fml:EG_SY_SUMA_BIEZ_POR_NET) }&lt;/m:synSumaBiezPortf_net&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_SUMA_PORTF_NET_1Q)
    then &lt;m:synSumaPortfNet1q&gt;{data($fml/fml:EG_SY_SUMA_PORTF_NET_1Q) }&lt;/m:synSumaPortfNet1q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_SUMA_PORTF_NET_2Q)
    then &lt;m:synSumaPortfNet2q&gt;{data($fml/fml:EG_SY_SUMA_PORTF_NET_2Q) }&lt;/m:synSumaPortfNet2q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_SUMA_PORTF_NET_3Q)
    then &lt;m:synSumaPortfNet3q&gt;{data($fml/fml:EG_SY_SUMA_PORTF_NET_3Q) }&lt;/m:synSumaPortfNet3q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_SUMA_PORTF_NET_4Q)
    then &lt;m:synSumaPortfNet4q&gt;{data($fml/fml:EG_SY_SUMA_PORTF_NET_4Q) }&lt;/m:synSumaPortfNet4q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_P_SUMA_PORTF_NET_1Q)
    then &lt;m:synSumaPortfNetPoprz1q&gt;{data($fml/fml:EG_SY_P_SUMA_PORTF_NET_1Q) }&lt;/m:synSumaPortfNetPoprz1q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_P_SUMA_PORTF_NET_2Q)
    then &lt;m:synSumaPortfNetPoprz2q&gt;{data($fml/fml:EG_SY_P_SUMA_PORTF_NET_2Q) }&lt;/m:synSumaPortfNetPoprz2q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_P_SUMA_PORTF_NET_3Q)
    then &lt;m:synSumaPortfNetPoprz3q&gt;{data($fml/fml:EG_SY_P_SUMA_PORTF_NET_3Q) }&lt;/m:synSumaPortfNetPoprz3q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_P_SUMA_PORTF_NET_4Q)
    then &lt;m:synSumaPortfNetPoprz4q&gt;{data($fml/fml:EG_SY_P_SUMA_PORTF_NET_4Q) }&lt;/m:synSumaPortfNetPoprz4q&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_SUM_WAR_NET_PRZ_UM)
    then &lt;m:synSumaWartNetPrzedmUmowy&gt;{data($fml/fml:EG_SY_SUM_WAR_NET_PRZ_UM) }&lt;/m:synSumaWartNetPrzedmUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_PROC_SREDNI_SPL_UM)
    then &lt;m:synProcSredniSplUmowy&gt;{data($fml/fml:EG_SY_PROC_SREDNI_SPL_UM) }&lt;/m:synProcSredniSplUmowy&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_OBC_NET_POP_ROK_KAP)
    then &lt;m:synObcNetPoprzRokKap&gt;{data($fml/fml:EG_SY_OBC_NET_POP_ROK_KAP) }&lt;/m:synObcNetPoprzRokKap&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_OBC_NET_POP_ROK_ODS)
    then &lt;m:synObcNetPoprzRokOds&gt;{data($fml/fml:EG_SY_OBC_NET_POP_ROK_ODS) }&lt;/m:synObcNetPoprzRokOds&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_OBC_NET_POP_ROK_SUM)
    then &lt;m:synObcNetPoprzRokSuma&gt;{data($fml/fml:EG_SY_OBC_NET_POP_ROK_SUM) }&lt;/m:synObcNetPoprzRokSuma&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_OBC_NET_BIE_ROK_KAP)
    then &lt;m:synObcNetBiezRokKap&gt;{data($fml/fml:EG_SY_OBC_NET_BIE_ROK_KAP) }&lt;/m:synObcNetBiezRokKap&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_OBC_NET_BIE_ROK_ODS)
    then &lt;m:synObcNetBiezRokOds&gt;{data($fml/fml:EG_SY_OBC_NET_BIE_ROK_ODS) }&lt;/m:synObcNetBiezRokOds&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_OBC_NET_BIE_ROK_SUM)
    then &lt;m:synObcNetBiezRokSuma&gt;{data($fml/fml:EG_SY_OBC_NET_BIE_ROK_SUM) }&lt;/m:synObcNetBiezRokSuma&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_OBC_NET_NAS_ROK_KAP)
    then &lt;m:synObcNetNastRokKap&gt;{data($fml/fml:EG_SY_OBC_NET_NAS_ROK_KAP) }&lt;/m:synObcNetNastRokKap&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_OBC_NET_NAS_ROK_ODS)
    then &lt;m:synObcNetNastRokOds&gt;{data($fml/fml:EG_SY_OBC_NET_NAS_ROK_ODS) }&lt;/m:synObcNetNastRokOds&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_OBC_NET_NAS_ROK_SUM)
    then &lt;m:synObcNetNastRokSuma&gt;{data($fml/fml:EG_SY_OBC_NET_NAS_ROK_SUM) }&lt;/m:synObcNetNastRokSuma&gt; 
    else ()	
  }
  {
  if($fml/fml:EEG_SY_LFPO_OBCIAZ_KAP_P)
    then &lt;m:synObcLFPOPoprzRokKap&gt;{data($fml/fml:EG_SY_LFPO_OBCIAZ_KAP_P) }&lt;/m:synObcLFPOPoprzRokKap&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_LFPO_OBCIAZ_ODS_P)
    then &lt;m:synObcLFPOPoprzRokOds&gt;{data($fml/fml:EG_SY_LFPO_OBCIAZ_ODS_P) }&lt;/m:synObcLFPOPoprzRokOds&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_LFPO_OBCIAZ_SUMA_P)
    then &lt;m:synObcLFPOPoprzRokSuma&gt;{data($fml/fml:EG_SY_LFPO_OBCIAZ_SUMA_P) }&lt;/m:synObcLFPOPoprzRokSuma&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_LFPO_OBCIAZ_KAP_C)
    then &lt;m:synObcLFPOBiezRokKap&gt;{data($fml/fml:EG_SY_LFPO_OBCIAZ_KAP_C) }&lt;/m:synObcLFPOBiezRokKap&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_LFPO_OBCIAZ_ODS_C)
    then &lt;m:synObcLFPOBiezRokOds&gt;{data($fml/fml:EG_SY_LFPO_OBCIAZ_ODS_C) }&lt;/m:synObcLFPOBiezRokOds&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_LFPO_OBCIAZ_SUMA_C)
    then &lt;m:synObcLFPOBiezRokSuma&gt;{data($fml/fml:EG_SY_LFPO_OBCIAZ_SUMA_C) }&lt;/m:synObcLFPOBiezRokSuma&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_LFPO_OBCIAZ_KAP_N)
    then &lt;m:synObcLFPONastRokKap&gt;{ data($fml/fmlEG_SY_LFPO_OBCIAZ_KAP_N) }&lt;/m:synObcLFPONastRokKap&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_LFPO_OBCIAZ_ODS_N)
    then &lt;m:synObcLFPONastRokOds&gt;{ data($fml/fml:EG_SY_LFPO_OBCIAZ_ODS_N) }&lt;/m:synObcLFPONastRokOds&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_LFPO_OBCIAZ_SUMA_N)
    then &lt;m:synObcLFPONastRokSuma&gt;{ data($fml/fml:EG_SY_LFPO_OBCIAZ_SUMA_N) }&lt;/m:synObcLFPONastRokSuma&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_PROC_SRED_SPL_PRZED)
    then &lt;m:synProcSredSplPrzedm&gt;{ data($fml/fml:EG_SY_PROC_SRED_SPL_PRZED) }&lt;/m:synProcSredSplPrzedm&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_PROC_SRED_MARZ_SWWR)
    then &lt;m:synProcSredMarzaSWWR&gt;{ data($fml/fml:EG_SY_PROC_SRED_MARZ_SWWR) }&lt;/m:synProcSredMarzaSWWR&gt; 
	else ()
  }
  {
  if($fml/fml:EG_SY_BIEZ_ZALEGL_KWOTA)
    then &lt;m:synBiezZaleglKwota&gt;{ data($fml/fml:EG_SY_BIEZ_ZALEGL_KWOTA) }&lt;/m:synBiezZaleglKwota&gt;
    else ()	
  }
  {
  if($fml/fml:EG_SY_BIEZ_ZALEGL_DNI)
    then &lt;m:synBiezZaleglDni&gt;{ data($fml/fml:EG_SY_BIEZ_ZALEGL_DNI) }&lt;/m:synBiezZaleglDni&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_MAX_OPOZ_RATING_DNI)
    then &lt;m:synMaxOpoznRatingDni&gt;{ data($fml/fml:EG_SY_MAX_OPOZ_RATING_DNI) }&lt;/m:synMaxOpoznRatingDni&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_MAX_OPOZ_DL_REZ_DNI)
    then &lt;m:synMaxOpoznDlaRezDni&gt;{ data($fml/fml:EG_SY_MAX_OPOZ_DL_REZ_DNI) }&lt;/m:synMaxOpoznDlaRezDni&gt; 
    else ()	
  }
  {
  if($fml/fml:EG_SY_KURSY_WAL_PLN)
    then &lt;m:synKursyWalPln&gt;{ data($fml/fml:EG_SY_KURSY_WAL_PLN) }&lt;/m:synKursyWalPln&gt; 
    else ()	
  }
  &lt;/m:Egeria&gt; 
else ()
}   



(: NP2036 end :)


		&lt;/m:CRMGetCustomerProductDetailResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetCustomerProductDetailResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>