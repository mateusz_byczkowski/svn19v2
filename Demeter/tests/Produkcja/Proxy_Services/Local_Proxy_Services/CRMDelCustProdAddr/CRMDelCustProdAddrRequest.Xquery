<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function cutStr($sourceString as xs:string, $count as xs:decimal) as xs:string
{
   let $length:= string-length($sourceString)
   let $startPos := $length - $count + 1 (:pozycja znaku od ktorego wycinac :)
   let $accountNumber :=  substring($sourceString, $startPos)
   return
     $accountNumber
};

declare function xf:mapCRMDelCustProdAddrRequest($req as element(m:CRMDelCustProdAddrRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:TrnId)
					then &lt;fml:DC_TRN_ID&gt;{ data($req/m:TrnId) }&lt;/fml:DC_TRN_ID&gt;
					else ()
			}
			{
				if($req/m:Uzytkownik)
					then &lt;fml:DC_UZYTKOWNIK&gt;{ concat("SKP:",data($req/m:Uzytkownik)) }&lt;/fml:DC_UZYTKOWNIK&gt;
					else ()
			}
			{
				if($req/m:Oddzial)
					then &lt;fml:DC_ODDZIAL&gt;{ data($req/m:Oddzial) }&lt;/fml:DC_ODDZIAL&gt;
					else ()
			}
			{
				if($req/m:NumerProduktu)
					then &lt;fml:DC_NUMER_PRODUKTU&gt;{ data($req/m:NumerProduktu) }&lt;/fml:DC_NUMER_PRODUKTU&gt;
					else ()
			}
			{
				if($req/m:NrRachunku)
					then &lt;fml:DC_NR_RACHUNKU&gt;{ cutStr(data($req/m:NrRachunku),10) }&lt;/fml:DC_NR_RACHUNKU&gt;
					else ()
			}
                        &lt;DC_TYP_ZMIANY&gt;D&lt;/DC_TYP_ZMIANY&gt;
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMDelCustProdAddrRequest($body/m:CRMDelCustProdAddrRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>