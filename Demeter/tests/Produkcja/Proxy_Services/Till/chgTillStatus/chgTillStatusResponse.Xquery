<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.2
 : @since   2009-12-15
 :
 : wersja WSDLa: 13-01-2010 15:28:58
 :
 : $Proxy Services/Till/chgTillStatus/chgTillStatusResponse.xq$
 :
 :)

declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Till/chgTillStatus/chgTillStatusResponse/";
declare namespace ns0 = "urn:dictionaries.be.dcl";
declare namespace ns1 = "";
declare namespace ns2 = "urn:operations.entities.be.dcl";
declare namespace ns3 = "urn:be.services.dcl";

declare variable $fML321 as element(ns1:FML32) external;

(:~
 : @param $fML321 bufor XML/FML
 :
 : @return invokeResponse operacja wyjściowa
 :)
declare function xf:chgTillStatusResponse($fML321 as element(ns1:FML32))
    as element(ns3:invokeResponse)
{
    &lt;ns3:invokeResponse&gt;
        &lt;ns3:transactionOut&gt;
            &lt;ns2:Transaction&gt;
                &lt;ns2:transactionStatus?&gt;
                    &lt;ns0:TransactionStatus?&gt;
                        &lt;ns0:transactionStatus?&gt;{
							data($fML321/ns1:TR_STATUS)
						}&lt;/ns0:transactionStatus&gt;
                    &lt;/ns0:TransactionStatus&gt;
                &lt;/ns2:transactionStatus&gt;
            &lt;/ns2:Transaction&gt;
        &lt;/ns3:transactionOut&gt;
    &lt;/ns3:invokeResponse&gt;
};

&lt;soap-env:Body&gt;{
	xf:chgTillStatusResponse($fML321)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>