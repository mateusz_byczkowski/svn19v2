<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="urn:errors.hlbsentities.be.dcl";
declare variable $fault external;
 
declare function local:fault($faultString as xs:string, $detail as xs:anyType) as element(soap-env:Fault) {
  &lt;soap-env:Fault&gt;
   &lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
   &lt;faultstring&gt;{ $faultString }&lt;/faultstring&gt; 
   &lt;detail&gt;{ $detail }&lt;/detail&gt;
  &lt;/soap-env:Fault&gt;
};
 
declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string, $errorDescription as xs:string) as element()* {
 &lt;f:exceptionItem&gt;
 &lt;f:errorCode1&gt;{ $errorCode1 }&lt;/f:errorCode1&gt;
 &lt;f:errorCode2&gt;{ $errorCode2 }&lt;/f:errorCode2&gt;
 &lt;f:errorDescription&gt;{ $errorDescription }&lt;/f:errorDescription&gt;
 &lt;/f:exceptionItem&gt;
};
 
&lt;soap-env:Body&gt;
{
 let $reason := fn:substring-after(fn:substring-before($fault/ctx:reason, "):"), "(")
 let $urcode := fn:substring-before(fn:substring-after(fn:substring-after($fault/ctx:reason, ":"), ":"), ":")
 return
  if($reason = "13") then
   local:fault("errors.hlbsentities.be.dcl.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode, "Timeout") })
  else if($reason = "11") then
    local:fault("errors.hlbsentities.be.dcl.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode, "Blad uslugi") })
  else
   local:fault("errors.hlbsentities.be.dcl.ServiceException", element f:ServiceException { local:errors($reason, $urcode, "Service Exception") })
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>