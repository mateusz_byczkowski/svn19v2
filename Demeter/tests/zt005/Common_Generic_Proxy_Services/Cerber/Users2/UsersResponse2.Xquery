<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace cer="http://bzwbk.com/services/cerber";
declare namespace xsi="http://www.w3.org/2001/XMLSchema-instance";

declare variable $body as element(soap-env:Body) external;

declare function local:UnitBaseContents($unit as element()) as element()* {
    <AddressCity>{ data($unit/addressCity) }</AddressCity>,
    <AddressNo>{ data($unit/addressNo) }</AddressNo>,
    <AddressState>{ data($unit/addressState) }</AddressState>,
    <AddressStreet>{ data($unit/addressStreet) }</AddressStreet>,
    <AddressZip>{ data($unit/addressZip) }</AddressZip>,
    <IcbsNo?>{ data($unit/icbsNo) }</IcbsNo>, 
    <KirNo?>{ data($unit/kirNo) }</KirNo>,
    <Name>{ data($unit/name) }</Name>,
    <FunctionId?>{ data($unit/functionId) }</FunctionId>,
    <Function>{ data($unit/function) }</Function>,
    <ChiefId?>{ data($unit/chiefId) }</ChiefId>,
    <CompanyId?>{ data($unit/companyId) }</CompanyId>,
    <ParentIcbsNo?>{ data($unit/parentIcbsNo) }</ParentIcbsNo>
};

declare function local:Units($unit as element()) as element() {
    <Units>
        {
            local:UnitBaseContents($unit)
        }
        <CashierNo?>{ data($unit/cashierNo)}</CashierNo>
        <TeamId?>{data($unit/teamId)}</TeamId>
        <IsDelegation>{data($unit/isDelegation)}</IsDelegation>
        <Since?>{data($unit/since)}</Since>
        <Until?>{data($unit/until)}</Until>
        <IsMasterUnit>{data($unit/isMasterUnit)}</IsMasterUnit>
        <WorkWithOwnWallet>{data($unit/workWithOwnWallet)}</WorkWithOwnWallet>
        {
            for $a in $unit/attributes/attribute
            return
                <Attributes>
                    <Name>{data($a/name)}</Name>
                    <Value>{data($a/value)}</Value>
                </Attributes>
        }
        {
            for $p in $unit/permissions/string
            return
                <Permissions>{data($p)}</Permissions>
        }
        {
            for $s in $unit/substitutions/substitution
            return
                <Substitutions>
                    <Id>{ data($s/id) }</Id>
                    <Since?>{data($s/since)}</Since>
                    <SubstitutedUserId>{data($s/substitutedUserId)}</SubstitutedUserId>
                    <SubstitutionForApplication>{data($s/substitutionForApplication)}</SubstitutionForApplication>
                    <UnitId>{data($s/unitId)}</UnitId>
                    <Until?>{data($s/until)}</Until>
                    <UserId>{data($s/userId)}</UserId>
                    <CreatedBy>{ data($s/createdBy) }</CreatedBy>
                </Substitutions>
        }
    </Units>
};

declare function local:UserBaseContents($user as element()) as element()* {
    <Email>{ data($user/email) }</Email>,
    <Func>{ data($user/function) }</Func>,
    <Gender>{ data($user/gender) }</Gender>,
    <LastIncorrectLoginApp?>{ data($user/lastIncorrectLoginApp) }</LastIncorrectLoginApp>,
    <LastIncorrectLoginDate?>{ data($user/lastIncorrectLoginDate) }</LastIncorrectLoginDate>,
    <LastLoggedInApplication?>{ data($user/lastLoggedInApplication) }</LastLoggedInApplication>,
    <LastLoggedInDate?>{ data($user/lastLoggedInDate) }</LastLoggedInDate>,
    <Login>{ data($user/login) }</Login>,
    <MobileNo>{ data($user/mobileNo) }</MobileNo>,
    <Name>{ data($user/name) }</Name>,
    <Password?>{ data($user/password) }</Password>,
    <PhoneNo>{ data($user/phoneNo) }</PhoneNo>,
    <Position?>{ data($user/position) }</Position>,
    <PositionId?>{ data($user/positionId) }</PositionId>,
    <Skp>{ data($user/skp) }</Skp>,
    <Surname>{ data($user/surename) }</Surname>
};

declare function local:UserContents($user as element()) as element()* {
    local:UserBaseContents($user),
    for $a in $user/attributes/attribute
    return
        <Attributes>
            <Name>{data($a/name)}</Name>
            <Value>{data($a/value)}</Value>
        </Attributes>,
    for $a in $user/positionAttributes/attribute
    return
        <PositionAttributes>
            <Name>{data($a/name)}</Name>
            <Value>{data($a/value)}</Value>
        </PositionAttributes>,
    for $p in $user/permissions/string
    return
        <Permissions>{data($p)}</Permissions>,
    for $p in $user/positionPermissions/string
    return
        <PositionPermissions>{data($p)}</PositionPermissions>,
    for $u in $user/units/unit
    return
        local:Units($u)
};

declare function local:UserExtendedContents($user as element()) as element()* {
    local:UserContents($user),
    for $g in $user/groups/group
    return
        <Groups>
            {
                for $a in $g/attributes/attribute
                return
                    <Attributes>
                        <Name>{data($a/name)}</Name>
                        <Value>{data($a/value)}</Value>
                    </Attributes>
            }
            <Name>{data($g/name)}</Name>
            {
                for $p in $g/permissions/string
                return
                    <Permissions>{data($p)}</Permissions>
            }
        </Groups>,
    for $g in $user/positionGroups/group
    return
        <PositionGroups>
            {
                for $a in $g/attributes/attribute
                return
                    <Attributes>
                        <Name>{data($a/name)}</Name>
                        <Value>{data($a/value)}</Value>
                    </Attributes>
            }
            <Name>{data($g/name)}</Name>
            {
                for $p in $g/permissions/string
                return
                    <Permissions>{data($p)}</Permissions>
            }
        </PositionGroups>
};

declare function local:GetUserDataResponse($resp as element(cer:getUserDataResponse)) as element() {
    if($resp/user) then
        <cer:GetUserDataResponse>
        {
            local:UserContents($resp/user)
        }
        </cer:GetUserDataResponse>
    else
        $resp
};

declare function local:GetUserDataWithHashResponse($resp as element(cer:getUserDataWithHashResponse)) as element() {
    if($resp/user) then
        <cer:GetUserDataWithHashResponse>
        {
            local:UserContents($resp/user)
        }
        </cer:GetUserDataWithHashResponse>
    else
        $resp
};


declare function local:GetUserDataExtendedResponse($resp as element(cer:getUserDataExtendedResponse)) as element() {
    if($resp/userExtended) then
        <cer:GetUserDataExtendedResponse>
        {
            local:UserExtendedContents($resp/userExtended)
        }
        </cer:GetUserDataExtendedResponse>
    else
        $resp
};

declare function local:GetUserDataWithoutPassResponse($resp as element(cer:getUserDataWithoutPassResponse)) as element() {
    if($resp/user) then
        <cer:GetUserDataWithoutPassResponse>
        {
            local:UserContents($resp/user)
        }
        </cer:GetUserDataWithoutPassResponse>
    else
        $resp
};

declare function local:GetUserDataWithoutPassForAppResponse($resp as element(cer:getUserDataWithoutPassForAppResponse)) as element() {
    if($resp/user) then
        <cer:GetUserDataWithoutPassForAppResponse>
        {
            local:UserContents($resp/user)
        }
        </cer:GetUserDataWithoutPassForAppResponse>
    else
        $resp
};

declare function local:GetUserDataWithoutPassForAppsResponse($resp as element(cer:getUserDataWithoutPassForAppsResponse)) as element() {
    if($resp/user-array/user) then
        <cer:GetUserDataWithoutPassForAppsResponse>
        {
            local:UserContents($resp/user-array/user)
        }
        </cer:GetUserDataWithoutPassForAppsResponse>
    else
        $resp
};

declare function local:GetUserDataForAppsResponse($resp as element(cer:getUserDataForAppsResponse)) as element() {
    if($resp/user-array/user) then
        <cer:GetUserDataForAppsResponse>
        {
            local:UserContents($resp/user-array/user)
        }
        </cer:GetUserDataForAppsResponse>
    else
        $resp
};

declare function local:GetUsersDataWithoutPassResponse($resp as element()) as element() {
    if($resp/user-array) then
        <cer:GetUsersDataWithoutPassResponse>
        {
            for $user in $resp/user-array/user
            return
                <cer:User>{ local:UserContents($user) }</cer:User>
        }
        </cer:GetUsersDataWithoutPassResponse>
    else
        $resp
};

declare function local:FindUsersMasterUnitsICBSNoResponse($resp as element()) as element() {
    if($resp/list) then
        <cer:FindUsersMasterUnitsICBSNoResponse>
        {
            for $icbsNo in $resp/list/*
            return
            typeswitch($icbsNo)
                case $val as element(int) return element cer:IcbsNumber { data($val) }
                default return <cer:IcbsNumber xsi:nil="true" />
        }
        </cer:FindUsersMasterUnitsICBSNoResponse>
    else
        $resp

};

declare function local:SearchUsersResponse($resp as element()) as element() {
    if($resp/list) then
        <cer:SearchUsersResponse>
        {
            for $user in $resp/list/userBaseImpl
            return 
                <cer:User>{ local:UserBaseContents($user) }</cer:User>
        }
        </cer:SearchUsersResponse>
    else
        $resp
};

declare function local:GetDirectSuperiorUserResponse($resp as element()) as element() {
    <cer:GetDirectSuperiorUserResponse>
    {
        if($resp/userBaseImpl) then
            local:UserBaseContents($resp/userBaseImpl)
        else
            ()
    }
    </cer:GetDirectSuperiorUserResponse>
};

declare function local:GetDirectSuperiorUsersResponse($resp as element()) as element() {
    if($resp/list) then
        <cer:GetDirectSuperiorUsersResponse>
        {
            for $user in $resp/list/(userBaseImpl|null)
            return
            (
                if(name($user) = "null") then
                    <cer:User xsi:nil="true" />
                else
                    <cer:User>{ local:UserContents($user) }</cer:User>
            )
        }
        </cer:GetDirectSuperiorUsersResponse>
    else
        $resp
};

declare function local:GetUserPermissionUnitInfoResponse($resp as element()) as element() {
    if($resp/linked-list) then
        <cer:GetUserPermissionUnitInfoResponse>
        {
            for $upui in $resp/linked-list/userPermissionUnitInfo
            return
                <cer:userPermissionUnitInfo>
                    <SKP>{ data($upui/skp) }</SKP>
                    <PermissionName>{ data($upui/permissionName) }</PermissionName>
                    <IcbsNo?>{ data($upui/icbsNo) }</IcbsNo>
                </cer:userPermissionUnitInfo>
        }
        </cer:GetUserPermissionUnitInfoResponse>
    else
        $resp
};


<soap-env:Body>
{
    typeswitch($body/*)
        case $resp as element(cer:getUserDataResponse) return local:GetUserDataResponse($resp)
        case $resp as element(cer:getUserDataWithHashResponse) return local:GetUserDataWithHashResponse($resp)
        case $resp as element(cer:getUserDataExtendedResponse) return local:GetUserDataExtendedResponse($resp)
        case $resp as element(cer:getUserDataWithoutPassResponse) return local:GetUserDataWithoutPassResponse($resp)
        case $resp as element(cer:getUserDataWithoutPassForAppResponse) return local:GetUserDataWithoutPassForAppResponse($resp)
        case $resp as element(cer:getUserDataWithoutPassForAppsResponse) return local:GetUserDataWithoutPassForAppsResponse($resp)
        case $resp as element(cer:getUserDataForAppsResponse) return local:GetUserDataForAppsResponse($resp)
        case $resp as element(cer:getUsersDataWithoutPassResponse) return local:GetUsersDataWithoutPassResponse($resp)
        case $resp as element(cer:findUsersMasterUnitsICBSnoResponse) return local:FindUsersMasterUnitsICBSNoResponse($resp)
        case $resp as element(cer:searchUsersResponse) return local:SearchUsersResponse($resp)
        case $resp as element(cer:getDirectSuperiorUserResponse) return local:GetDirectSuperiorUserResponse($resp) 
        case $resp as element(cer:getDirectSuperiorUsersResponse) return local:GetDirectSuperiorUsersResponse($resp)        
        case $resp as element(cer:getUserPermissionUnitInfoResponse) return local:GetUserPermissionUnitInfoResponse($resp)      
        default $resp return $resp
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>