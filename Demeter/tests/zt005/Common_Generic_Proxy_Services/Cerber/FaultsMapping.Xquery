<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery><![CDATA[declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace cer="http://bzwbk.com/services/cerber";
declare namespace f="http://bzwbk.com/services/cerber/faults";

declare variable $body external;

declare function local:fault($faultString as xs:string, $detail as element()) as element(soap-env:Fault) {
		<soap-env:Fault>
			<faultcode>soapenv:Server.userException</faultcode> 
			<faultstring>{ $faultString }</faultstring> 
			<detail>{ $detail }</detail>
		</soap-env:Fault>
};


<soap-env:Body>
{
	typeswitch($body/*/*[1])
		case element(com.bzwbk.cerber.services.exceptions.UserUnitNotFoundException)
			return local:fault("com.bzwbk.services.cerber.faults.UserUnitNotFoundException", element f:UserUnitNotFoundException {})
		case element(com.bzwbk.cerber.services.exceptions.DelegationExistException)
			return local:fault("com.bzwbk.services.cerber.faults.DelegationExistException", element f:DelegationExistException {})
		case element(com.bzwbk.cerber.services.exceptions.DelegationNotFoundExcepton)
			return local:fault("com.bzwbk.services.cerber.faults.DelegationNotFoundExcepton", element f:DelegationNotFoundExcepton {})
		case element(com.bzwbk.cerber.services.exceptions.IllegalApplicationAccessException)
			return local:fault("com.bzwbk.services.cerber.faults.IllegalApplicationAccessException", element f:IllegalApplicationAccessException {})
		case element(com.bzwbk.cerber.services.exceptions.InputBufferException)
			return local:fault("com.bzwbk.services.cerber.faults.InputBufferException", element f:InputBufferException {})
		case element(com.bzwbk.cerber.services.exceptions.InvalidUserPasswordException)
			return local:fault("com.bzwbk.services.cerber.faults.InvalidUserPasswordException", element f:InvalidUserPasswordException {})
		case element(com.bzwbk.cerber.services.exceptions.SubstitutionExistException)
			return local:fault("com.bzwbk.services.cerber.faults.SubstitutionExistsException", element f:SubstitutionExistsException {})
		case element(com.bzwbk.cerber.services.exceptions.SubstitutionNotFoundException)
			return local:fault("com.bzwbk.services.cerber.faults.SubstitutionNotFoundException", element f:SubstitutionNotFoundException {})
		case element(com.bzwbk.cerber.services.exceptions.SubstitutionRemovedException)
			return local:fault("com.bzwbk.services.cerber.faults.SubstitutionRemovedException", element f:SubstitutionRemovedException {})
		case element(com.bzwbk.cerber.services.exceptions.TeamBusinessRuleViolationException)
			return local:fault("com.bzwbk.services.cerber.faults.TeamBusinessRuleViolationException", element f:TeamBusinessRuleViolationException {})
		case element(com.bzwbk.cerber.services.exceptions.TeamNotFoundException)
			return local:fault("com.bzwbk.services.cerber.faults.TeamNotFoundException", element f:TeamNotFoundException {})
		case element(com.bzwbk.cerber.services.exceptions.TeamRemovedException)
			return local:fault("com.bzwbk.services.cerber.faults.TeamRemovedException", element f:TeamRemovedException {})
		case element(com.bzwbk.cerber.services.exceptions.UnitNotFoundException)
			return local:fault("com.bzwbk.services.cerber.faults.UnitNotFoundException", element f:UnitNotFoundException {})
		case element(com.bzwbk.cerber.services.exceptions.UserBlockedException)
			return local:fault("com.bzwbk.services.cerber.faults.UserBlockedException", element f:UserBlockedException {})
		case element(com.bzwbk.cerber.services.exceptions.UserNotFoundException)
			return local:fault("com.bzwbk.services.cerber.faults.UserNotFoundException", element f:UserNotFoundException {})
		case element(com.bzwbk.cerber.services.exceptions.UserPasswordExpiredException)
			return local:fault("com.bzwbk.services.cerber.faults.UserPasswordExpiredException", element f:UserPasswordExpiredException {})
		default
			return $body/*
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>