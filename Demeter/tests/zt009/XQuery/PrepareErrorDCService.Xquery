<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err="urn:errors.hlbsentities.be.dcl";
declare variable $body external;
declare variable $headerCache external;
declare namespace urn3="urn:be.services.dcl";

declare function local:fault($faultString as xs:string, $detail as element()?) as element(soap-env:Fault) {
		&lt;soap-env:Fault&gt;
			&lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
			&lt;faultstring&gt;{ $faultString }&lt;/faultstring&gt; 
			&lt;detail&gt;{ $detail }&lt;/detail&gt;
		&lt;/soap-env:Fault&gt;
};

declare function Itemerr($errorCode1 as xs:string, $errorCode2 as xs:string,
        $error as element()?,$errorCodeFromBS as xs:string, $messageID as xs:string) as element()
{
  for $x in $error
  return		
  &lt;err:exceptionItem&gt;
	&lt;err:errorCode1&gt;{ $errorCode1 }&lt;/err:errorCode1&gt;
	&lt;err:errorCode2&gt;{ $errorCode2 }&lt;/err:errorCode2&gt;
        {
           if (string-length($errorCodeFromBS)&gt;0) 
               then   &lt;err:errorDescription&gt;{ concat($errorCodeFromBS," : ",concat(translate($x, "za§˘’Ťg‘žl†ja¦¤", "zażółćgęśląjaźń"), concat(' (',concat($messageID, ')')))) }&lt;/err:errorDescription&gt;
               else    
              &lt;err:errorDescription&gt;{ concat(translate($x, "za§˘’Ťg‘žl†ja¦¤", "zażółćgęśląjaźń"), concat(' (',concat($messageID, ')'))) }&lt;/err:errorDescription&gt;
        }
   &lt;/err:exceptionItem&gt;
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string,
        $errorDescription as xs:string?, $errorCodeFromBS as xs:string, $messageID as xs:string) as element(soap-env:Fault) 
{
    &lt;soap-env:Fault&gt;
			&lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
			&lt;faultstring&gt;err:ServiceFailException&lt;/faultstring&gt; 
			&lt;detail&gt;
(: PT058 begin :)
	(:		{	
                element err:ServiceFailException {Itemerr($errorCode1, $errorCode2, $errorDescription, $errorCodeFromBS, $messageID)} &lt;/detail&gt;}
:)
&lt;err:ServiceFailException&gt;
{
  for $x in $errorDescription
  return	
  &lt;err:exceptionItem&gt;
	&lt;err:errorCode1&gt;{ $errorCode1 }&lt;/err:errorCode1&gt;
	&lt;err:errorCode2&gt;{ $errorCode2 }&lt;/err:errorCode2&gt;
        {
           if (string-length($errorCodeFromBS)&gt;0) 
               then   &lt;err:errorDescription&gt;{ concat($errorCodeFromBS," : ",concat(translate($x, "za§˘’Ťg‘žl†ja¦¤", "zażółćgęśląjaźń"), concat(' (',concat($messageID, ')')))) }&lt;/err:errorDescription&gt;
               else    
              &lt;err:errorDescription&gt;{ concat(translate($x, "za§˘’Ťg‘žl†ja¦¤", "zażółćgęśląjaźń"), concat(' (',concat($messageID, ')'))) }&lt;/err:errorDescription&gt;
        }
   &lt;/err:exceptionItem&gt;
}

&lt;/err:ServiceFailException&gt;
&lt;/detail&gt;
(: PT058 end :)
	&lt;/soap-env:Fault&gt;
};

&lt;soap-env:Body&gt;
	{
		let $reason := "11"
		let $urcode := "1000"
		
		let $errorDescription:=data($body/FML32/NF_ERROR_DESCRIPTION[1])
		let $errorCode:=data($body/FML32/NF_ERROR_CODE[1])
        let $messageID := data($headerCache/urn3:header/urn3:msgHeader/urn3:msgId)

		return
                       if (string-length($errorDescription)&gt;0 or string-length($errorCode)&gt;0) then
                         local:errors($reason, $urcode, $errorDescription,$errorCode, $messageID)
                       else if (string-length(data($body/FML32/DC_OPIS_BLEDU[1]))&gt;0) then
                         local:errors($reason, $urcode, data($body/FML32/DC_OPIS_BLEDU),"", $messageID)
                       else()
                         }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>