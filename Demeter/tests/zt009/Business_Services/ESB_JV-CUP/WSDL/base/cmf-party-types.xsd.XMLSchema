<?xml version="1.0" encoding="UTF-8"?>
<con:schemaEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Typy dotyczace klientow i agentow
Version: 3.009</con:description>
  <con:schema><![CDATA[<?xml version="1.0" encoding="UTF-8"?>
<!--
        Typy dotyczace klientow i agentow
        $Revision: 3.009 $
	$Date: 2010-03-10 12:00:00 $
-->
<xs:schema
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:cmf-tech="http://jv.channel.cu.com.pl/cmf-technical-types" 
xmlns:cmf-biz="http://jv.channel.cu.com.pl/cmf-biz-types" 
xmlns:cmf-party="http://jv.channel.cu.com.pl/cmf-party-types"
targetNamespace="http://jv.channel.cu.com.pl/cmf-party-types">
   <xs:import namespace="http://jv.channel.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd"/>
   <xs:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd"/>
	<xs:complexType name="AddressListType">
		<xs:annotation>
			<xs:documentation>Lista adresów</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="address" type="cmf-biz:AddressType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="PhoneListType">
		<xs:annotation>
			<xs:documentation>Lista Telefonów</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="phone" type="cmf-biz:PhoneType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="PersonalDataType">
		<xs:annotation>
			<xs:documentation>Dane osoby</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:NameType"/>
			<xs:element name="dob" type="xs:date" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Data urodzenia</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="dod" type="xs:date" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Data śmierci</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="age" type="xs:int" minOccurs="0"/>
			<xs:element name="calculated-age-date" type="xs:date" minOccurs="0"/>
			<xs:element name="pob" type="cmf-tech:CmfString" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Miejsce urodzenia</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="cob" type="cmf-tech:CmfString" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Kraj urodzenia</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="title" type="cmf-biz:PersonTitleType" minOccurs="0"/>
			<xs:element name="sex" type="cmf-biz:SexType" minOccurs="0"/>
			<xs:element name="maritial-status" type="cmf-biz:MaritalStatusType" minOccurs="0"/>
			<xs:element name="citizenship" type="cmf-tech:CmfString" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Obywatelstwo</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="country" type="cmf-tech:CmfString" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Kraj zamieszkania</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="identifiers" type="cmf-biz:IdentifiersType" minOccurs="0"/>
			<xs:element name="email" type="cmf-biz:EmailType" minOccurs="0"/>
			<xs:element name="address-list" type="cmf-party:AddressListType" minOccurs="0"/>
			<xs:element name="phone-list" type="cmf-party:PhoneListType" minOccurs="0"/>
			<xs:element name="no-of-children" type="xs:int" minOccurs="0"/>
			<xs:element name="no-of-vehicles" type="xs:int" minOccurs="0"/>
			<xs:element name="occupation-code" type="cmf-biz:OccupationCodeType" minOccurs="0"/>
			<xs:element name="segmentation" type="xs:int" minOccurs="0"/>
			<xs:element name="community-property" type="cmf-biz:CommunityPropertyType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Wspólnota majątkowa</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="resident" type="xs:boolean" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Rezydent czy osoba ma PESEL</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgreementSourceType">
		<xs:sequence>
			<xs:element name="system" type="cmf-biz:SourceSystemNameType"/>
			<xs:element name="type" type="cmf-biz:AgreementTypeType"/>
			<xs:element name="id" type="cmf-biz:AgreementIdType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgreementType">
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:AgreementNameType"/>
			<xs:element name="version" type="cmf-biz:AgreementVersionType" minOccurs="0"/>
			<xs:element name="validFrom" type="xs:date" minOccurs="0"/>
			<xs:element name="validTo" type="xs:date" minOccurs="0"/>
			<xs:element name="status" type="cmf-biz:AgreementStatusType" minOccurs="0"/>
			<xs:element name="source" type="cmf-party:AgreementSourceType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgreementListType">
		<xs:sequence>
			<xs:element name="agreement" type="cmf-party:AgreementType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="SourceIdentifierType">
		<xs:sequence>
			<xs:element name="person-id" type="cmf-tech:CmfString"/>
			<xs:element name="source-system" type="cmf-biz:SourceSystemNameType"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="SourceIdListType">
		<xs:sequence>
			<xs:element name="identifier" type="cmf-party:SourceIdentifierType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ContactDataType">
		<xs:annotation>
			<xs:documentation>
			  Dane kontaktowe
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:NameType" minOccurs="0"/>
			<xs:element name="phones" type="cmf-party:PhoneListType" minOccurs="0"/>
			<xs:element name="email" type="cmf-biz:EmailType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ClientType">
		<xs:annotation>
			<xs:documentation>Obiekt reprezentujący dane klienta, który może być osobą lub firmą</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="agreements" type="cmf-party:AgreementListType" minOccurs="0"/>
			<xs:element name="contact" type="cmf-biz:ContactType" minOccurs="0"/>
			<xs:element name="factories" type="cmf-biz:FactoryNameListType" minOccurs="0"/>
			<xs:element name="source-ids" type="cmf-party:SourceIdListType" minOccurs="0"/>
			<xs:element name="company-ind" type="cmf-biz:CompanyIndType" minOccurs="0"/>
			<xs:element name="company-contact-data" type="cmf-party:ContactDataType" minOccurs="0"/>
			<xs:element name="moto-data" type="cmf-party:MotoDataType" minOccurs="0"/>
			<xs:element name="jv-data" type="cmf-party:JVDataType" minOccurs="0"/>
			<xs:element name="loyalty-program-data" type="cmf-party:LoyaltyMemberDataType" minOccurs="0"/>
			<xs:element name="payback-details" type="cmf-party:PaybackDetailsType" minOccurs="0"/>
			<xs:element name="person" type="cmf-party:PersonalDataType" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="client-code" type="cmf-biz:ClientCodeType" use="optional"/>
	</xs:complexType>
	<xs:complexType name="ClientShortType">
		<xs:sequence>
			<xs:element name="client-code" type="cmf-biz:ClientCodeType"/>
			<xs:element name="last-name" type="cmf-biz:PersonSurnameType"/>
			<xs:element name="first-name" type="cmf-biz:PersonNameType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ClientListType">
		<xs:annotation>
			<xs:documentation>Lista klientów</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="client" type="cmf-party:ClientType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ClientShortListType">
		<xs:sequence>
			<xs:element name="client" type="cmf-party:ClientShortType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ProspectContactType">
		<xs:annotation>
			<xs:documentation>
			  Dane dotyczące kontaktu
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="priority" type="xs:int" minOccurs="0"/>
			<xs:element name="contact-date-start" type="xs:dateTime" minOccurs="0"/>
			<xs:element name="contact-date-end" type="xs:dateTime" minOccurs="0"/>
			<xs:element name="valid-period-start" type="xs:dateTime" minOccurs="0"/>
			<xs:element name="valid-period-end" type="xs:dateTime" minOccurs="0"/>
			<xs:element name="registration-date" type="xs:dateTime" minOccurs="0"/>
			<xs:element name="registration-source" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="referer" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="other-information" type="cmf-tech:CmfString" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ProspectType">
		<xs:sequence>
			<xs:element name="person" type="cmf-party:PersonalDataType" minOccurs="0"/>
			<xs:element name="agreements" type="cmf-party:AgreementListType" minOccurs="0"/>
			<xs:element name="factories" type="cmf-biz:FactoryNameListType" minOccurs="0"/>
			<xs:element name="comment" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="client-code" type="cmf-biz:ClientCodeType" minOccurs="0"/>
			<xs:element name="prospect-contact" type="cmf-party:ProspectContactType" minOccurs="0"/>
			<xs:element name="employee" type="cmf-party:EmployeeType" minOccurs="0"/>
			<xs:element name="vehicle" type="cmf-party:VehicleType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="OrganizationLevelType">
		<xs:annotation>
			<xs:documentation>Nazwa organizacyjna</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="function" type="xs:int">
				<xs:annotation>
					<xs:documentation>Funkcja: 
                        0 - agent, 
                        1 - kierownik. 
                        2 - dyrektor,
                        3 - struktura</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="description" type="cmf-biz:DescriptionType"/>
		</xs:sequence>
		<xs:attribute name="name" type="cmf-biz:OrganizationLevelNameType"/>
	</xs:complexType>
	<xs:complexType name="OrganizationLevelListType">
		<xs:sequence>
			<xs:element name="organization-level" type="cmf-party:OrganizationLevelType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="BusinessDataType">
		<xs:annotation>
			<xs:documentation>
				Dane biznesowe agenta:
				manager: Przelozony agenta
				real-manager: Przelozony agenta osoba fizyczna
				start-date: data rozpoczecia dzialalnosci agenta
				knf-registration-date: data rejestracji w KNF
				elearning-member: flaga czy agent jest uprawniony do korzystania z eLearningu
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="status" type="cmf-biz:AgentStatusType" minOccurs="0"/>
			<xs:element name="organization-level" type="cmf-biz:OrganizationLevelNameType" minOccurs="0"/>
			<xs:element name="basic-level" type="cmf-biz:SalesLevelType" minOccurs="0"/>
			<xs:element name="override-level" type="cmf-biz:SalesLevelType" minOccurs="0"/>
			<xs:element name="distrib-source" type="cmf-biz:DistributionSourceNameType" minOccurs="0"/>
			<xs:element name="manager" type="cmf-biz:AgentIdType" minOccurs="0"/>
			<xs:element name="real-manager" type="cmf-biz:AgentIdType" minOccurs="0"/>
			<xs:element name="structure" type="cmf-biz:AgentIdType" minOccurs="0"/>
			<xs:element name="structure-name" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="factory" type="cmf-biz:FactoryNameType" minOccurs="0"/>
			<xs:element name="start-date" type="xs:date" minOccurs="0"/>
			<xs:element name="knf-registration-date" type="xs:date" minOccurs="0"/>
			<xs:element name="elearning-member" type="xs:boolean" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgentHistoryItemType">
		<xs:sequence>
			<xs:element name="effective-date" type="xs:date"/>
			<xs:element name="change-date" type="xs:date"/>
			<xs:element name="status" type="cmf-biz:AgentStatusType"/>
			<xs:element name="organization-level" type="cmf-biz:OrganizationLevelNameType"/>
			<xs:element name="basic-level" type="cmf-biz:SalesLevelType"/>
			<xs:element name="override-level" type="cmf-biz:SalesLevelType" minOccurs="0"/>
			<xs:element name="distrib-source" type="cmf-biz:DistributionSourceNameType"/>
			<xs:element name="manager" type="cmf-biz:AgentIdType" minOccurs="0"/>
			<xs:element name="structure" type="cmf-biz:AgentIdType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgentHistoryType">
		<xs:sequence>
			<xs:element name="history" type="cmf-party:AgentHistoryItemType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgentLicenceType">
		<xs:sequence>
			<xs:element name="factory" type="cmf-biz:FactoryNameType"/>
			<xs:element name="name" type="cmf-tech:CmfString"/>
			<xs:element name="number" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="active" type="xs:int"/>
			<xs:element name="created" type="xs:date"/>
			<xs:element name="expired" type="xs:date" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgentLicenceListType">
		<xs:sequence>
			<xs:element name="licence" type="cmf-party:AgentLicenceType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgentType">
		<xs:annotation>
			<xs:documentation>Obiekt reprezentujący dane agenta</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="person-data" type="cmf-party:PersonalDataType"/>
			<xs:element name="business-data" type="cmf-party:BusinessDataType" minOccurs="0"/>
			<xs:element name="agent-history" type="cmf-party:AgentHistoryType" minOccurs="0"/>
			<xs:element name="licence-list" type="cmf-party:AgentLicenceListType" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="agent-id" type="cmf-biz:AgentIdType"/>
		<xs:attribute name="person-id" type="cmf-biz:AgentIdType"/>
	</xs:complexType>
	<xs:complexType name="GroupAdminType">
		<xs:annotation>
			<xs:documentation>Obiekt reprezentujący dane administratora grupowego</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="person-data" type="cmf-party:PersonalDataType"/>
		</xs:sequence>
		<xs:attribute name="person-id" type="cmf-biz:AgentIdType"/>
	</xs:complexType>
	<xs:complexType name="AgentSplitType">
		<xs:sequence>
			<xs:element name="agent-id" type="cmf-biz:AgentIdType"/>
			<xs:element name="share" type="cmf-biz:SplitType"/>
			<xs:element name="main" type="xs:int"/>
			<xs:element name="comm-ratio" type="xs:double"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgentIdListType">
		<xs:sequence>
			<xs:element name="agent-id" type="cmf-biz:AgentIdType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgentSplitListType">
		<xs:sequence>
			<xs:element name="split" type="cmf-party:AgentSplitType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgentListType">
		<xs:sequence>
			<xs:element name="agent" type="cmf-party:AgentType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgencyType">
		<xs:annotation>
			<xs:documentation>Obiekt reprezentujący strukturę agenta</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:CompanyNameType"/>
			<xs:element name="address-list" type="cmf-party:AddressListType" minOccurs="0"/>
			<xs:element name="factory" type="cmf-biz:FactoryNameType" minOccurs="0"/>
			<xs:element name="region" type="cmf-biz:AgentIdType" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="agency-id" type="cmf-biz:AgentIdType"/>
	</xs:complexType>
	<xs:complexType name="BeneficiaryType" abstract="false">
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:NameType"/>
			<xs:element name="company-ind" type="cmf-biz:CompanyIndType" minOccurs="0"/>
			<xs:element name="sex" type="cmf-biz:SexType" minOccurs="0"/>
			<xs:element name="dob" type="xs:date" minOccurs="0"/>
			<xs:element name="identifiers" type="cmf-biz:IdentifiersType" minOccurs="0"/>
			<xs:element name="kinship" type="cmf-biz:KinshipType" minOccurs="0"/>
			<xs:element name="main" type="xs:int" minOccurs="0">
				<xs:annotation>
					<xs:documentation>1 - primery beneficiary
					  0 - secondary</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="split" type="xs:float"/>
			<xs:element name="address" type="cmf-biz:AddressType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="BeneficiaryListType">
		<xs:sequence>
			<xs:element name="beneficiary" type="cmf-party:BeneficiaryType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="MotoDataType">
		<xs:annotation>
			<xs:documentation>Client data for Moto project
				no-damage-ac - period with no AC damage
				no-damage-oc - period with no OC damage
				damage-list  - list of previous damages</xs:documentation>
		</xs:annotation>
		<xs:sequence minOccurs="0">
			<xs:element name="no-damage-ac" type="xs:int"/>
			<xs:element name="no-damage-oc" type="xs:int"/>
			<xs:element name="damage-list" type="cmf-biz:DamageListType"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="JVDataType">
		<xs:annotation>
			<xs:documentation>
			  Dane specyficzne dla JV
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="giodo-status" type="cmf-biz:JVGiodoStatusType" minOccurs="0"/>
			<xs:element name="branch-id" type="cmf-biz:JVBranchIdType" minOccurs="0"/>
			<xs:element name="facsimile-signature" type="cmf-biz:FacsimileSignatureType" minOccurs="0"/>
			<xs:element name="language-code" type="cmf-biz:LanguageCodeType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="UserType">
		<xs:sequence>
			<xs:element name="name" type="cmf-biz:NameType"/>
			<xs:element name="principal" type="cmf-biz:UserIdType" minOccurs="0"/>
			<xs:element name="position" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="cost-center" type="cmf-biz:CostCenterType" minOccurs="0"/>
			<xs:element name="email" type="cmf-biz:EmailType" minOccurs="0"/>
			<xs:element name="phone-list" type="cmf-party:PhoneListType" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="user-id" type="cmf-biz:UserIdType"/>
	</xs:complexType>
	<xs:complexType name="ContactHistoryType">
		<xs:sequence>
			<xs:element name="contact" type="cmf-biz:ContactType"/>
			<xs:element name="external-code" type="xs:string" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					   kod operacji (podawany w interfejsie przez 3r)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="description" type="cmf-biz:DescriptionType" minOccurs="0"/>
			<xs:element name="barcode" type="cmf-biz:BarcodeType" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  barkod listu lub emaila albo id SMS z USP
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="status" type="cmf-biz:ContactStatusType"/>
			<xs:element name="required" type="xs:dateTime" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  data żadania (domyślnie sysdate)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="applied" type="xs:dateTime" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  data wykonania domyślnie sysdate)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="additional-info" type="cmf-tech:CmfString" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  dodatkowa informacja (w przyapdku SMSa tu ma być podana treść SMSa)
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="operator" type="cmf-biz:UserIdType" minOccurs="0"/>
			<xs:element name="ref-number" type="cmf-tech:CmfString" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  id SMSa z USP, z którym związany jest dany SMS
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="letter-number" type="cmf-tech:CmfString" minOccurs="0">
				<xs:annotation>
					<xs:documentation>
					  Numer listu poleconego
					</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="email" type="cmf-biz:EmailType" minOccurs="0"/>
			<xs:element name="source-system" type="cmf-biz:SourceSystemNameType" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="client-code" type="cmf-biz:ClientCodeType"/>
		<xs:attribute name="agent-id" type="cmf-biz:AgentIdType"/>
		<xs:attribute name="contract-number" type="cmf-biz:ContractNumberType"/>
		<xs:attribute name="product-type" type="cmf-biz:ProductNameType"/>
	</xs:complexType>
	<xs:complexType name="LoyaltyTransactionElementType">
		<xs:annotation>
			<xs:documentation>
                Element opisuje transakcje/zakup
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="card-number" type="cmf-tech:CmfString">
				<xs:annotation>
					<xs:documentation>
                        Numer karty Payback
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="client-code" type="cmf-biz:ClientCodeType">
				<xs:annotation>
					<xs:documentation>
                        identyfikator klienta
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="channel-id" type="cmf-biz:LoyaltyChannelIdType"/>
			<xs:element name="pos-code" type="cmf-tech:CmfString">
				<xs:annotation>
					<xs:documentation>
                        Kod POS w którym nastąpiła transakcja
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="transaction-date" type="xs:dateTime">
				<xs:annotation>
					<xs:documentation>
                        Data transakcji
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="transaction-type" type="cmf-biz:LoyaltyTransactionTypeType"/>
			<xs:element name="contract-number" type="cmf-biz:ContractNumberType">
				<xs:annotation>
					<xs:documentation>
                        Numer kontraktu
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="product-type" type="cmf-biz:ProductNameType">
				<xs:annotation>
					<xs:documentation>
                        Typ produktu
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="points" type="xs:int">
				<xs:annotation>
					<xs:documentation>
                        Wydane punkty (tylko dodatnie wartości)
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="block-date" type="xs:dateTime">
				<xs:annotation>
					<xs:documentation>
                        Data do ktorej przyznane punkty beda chronione przez system przed wykorzystaniem
                    </xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="LoyaltyTransactionElementListType">
		<xs:sequence>
			<xs:element name="transation" type="cmf-party:LoyaltyTransactionElementType" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="LoyaltyTransactionsPackageType">
		<xs:annotation>
			<xs:documentation>
                Element zawiera paczke transakcji do eksportu
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="export-number" type="xs:long"/>
			<xs:element name="file-name" type="cmf-tech:CmfString"/>
			<xs:element name="source-system" type="cmf-biz:SourceSystemNameType"/>
			<xs:element name="transactions" type="cmf-party:LoyaltyTransactionElementListType"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="LoyaltyMemberDataType">
		<xs:annotation>
			<xs:documentation>
                Element zawiera dane uczestnika programu lojalnosciowego
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="card-number" type="cmf-tech:CmfString"/>
			<xs:element name="client-code" type="cmf-biz:ClientCodeType"/>
			<xs:element name="scan-date" type="xs:dateTime"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="LoyaltyMembersPackageType">
		<xs:annotation>
			<xs:documentation>
                Element zawiera paczke uczestnikow programu lojalnosciowego
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="export-number" type="xs:long"/>
			<xs:element name="file-name" type="cmf-tech:CmfString"/>
			<xs:element name="source-system" type="cmf-biz:SourceSystemNameType"/>
			<xs:element name="members" type="cmf-party:ClientListType"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="PaybackDetailsType">
		<xs:sequence>
			<xs:element name="card-number" type="cmf-biz:CardNumberType" minOccurs="0"/>
			<xs:element name="scan-date" type="xs:date"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="EmployeeType">
		<xs:annotation>
			<xs:documentation>
			  Dane pracownika
			</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="employee-id" type="cmf-biz:EmployeeIdType" minOccurs="0"/>
			<xs:element name="name" type="cmf-biz:NameType" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="EngineDataType">
		<xs:annotation>
			<xs:documentation>
                Vehicle engine details
            </xs:documentation>
		</xs:annotation>
		<xs:sequence minOccurs="0">
			<xs:element name="capacity" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="fuel-type" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="net-power" type="cmf-tech:CmfString" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="VehicleDataType">
		<xs:annotation>
			<xs:documentation>
                Vehicle details
            </xs:documentation>
		</xs:annotation>
		<xs:sequence minOccurs="0">
			<xs:element name="make" type="cmf-biz:VehicleMakeType" minOccurs="0"/>
			<xs:element name="model-type" type="cmf-biz:VehicleModelType" minOccurs="0"/>
			<xs:element name="variant" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="version" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="commercial_desc" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="vehicle-category" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="usage-category" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="no-of-doors" type="xs:int" minOccurs="0"/>
			<xs:element name="no-of-seats" type="xs:int" minOccurs="0"/>
			<xs:element name="body-type" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="engine-details" type="cmf-party:EngineDataType" minOccurs="0"/>
		</xs:sequence>
		<xs:attribute name="vehicle-code" type="cmf-biz:VehicleCodeType"/>
	</xs:complexType>
	<xs:complexType name="VehicleModifListType">
		<xs:annotation>
			<xs:documentation>
                Vehicles modifications list
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="modification" type="cmf-tech:CmfString" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="VehicleType">
		<xs:annotation>
			<xs:documentation>
                Vehicle details
            </xs:documentation>
		</xs:annotation>
		<xs:sequence minOccurs="0">
			<xs:element name="vehicle-details" type="cmf-party:VehicleDataType" minOccurs="0"/>
			<xs:element name="registration-no" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="VIN" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="year-of-manufacture" type="cmf-biz:YearOfManufactureType" minOccurs="0"/>
			<xs:element name="date-of-first-reg" type="xs:gYearMonth" minOccurs="0"/>
			<xs:element name="ownership" type="xs:int" minOccurs="0"/>
			<xs:element name="ownership-length" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="mileage" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="annual-mileage" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="theft-protection" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="kept-postcode" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="kept-place" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="tracker-device" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="damages" type="cmf-tech:CmfString" minOccurs="0"/>
			<xs:element name="modifications-list" type="cmf-party:VehicleModifListType" minOccurs="0"/>
			<xs:element name="additional-equipment" type="cmf-tech:CmfString"/>
			<xs:element name="abroad-using" type="xs:int" minOccurs="0"/>
			<xs:element name="no-damage-ac" type="xs:int"/>
			<xs:element name="no-damage-oc" type="xs:int"/>
			<xs:element name="damage-list" type="cmf-biz:DamageListType"/>
		</xs:sequence>
		<xs:attribute name="vehicle-code" type="cmf-biz:VehicleCodeType"/>
	</xs:complexType>
	<xs:complexType name="VehicleListType">
		<xs:annotation>
			<xs:documentation>
                Vehicles list
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="vehicle" type="cmf-party:VehicleType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="VehicleCodeListType">
		<xs:annotation>
			<xs:documentation>
                Vehicles owned by person on contract
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="vehicle" type="cmf-biz:VehicleCodeType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="AgentActivityNameType">
		<xs:annotation>
			<xs:documentation>
				Rodzaj (nazwa) aktywnosci agenta:
				CONTACTS - Ilosc kontaktow(np. telefonow ale nie spotkan)
				SPI - Spotkanie Prospekt
				SKI - Spotkanie Klient
				FP - Folder Potrzeb
				CUZI - Ilosc spisanych umow TUZI
				CUZG - Ilosc spisanych umow TUZG
				CUO - Ilosc spisanych umow TUO
				PTE - Ilosc spisanych umow PTE
				TFI - Ilosc spisanych umow TFI
				CALENDAR - czy dane zostaly pobrane z kalendarza i dla ilu kolumn nie
				RECOMMENDATIONS - Polecenia (namiary uzyskane na spotkaniu na  nowych klientow )
				SKC - Spotkania konwersyjne				
			</xs:documentation>
		</xs:annotation>
		<xs:restriction base="cmf-tech:CmfString">
			<xs:enumeration value="CONTACTS"/>
			<xs:enumeration value="SPI"/>
			<xs:enumeration value="SKI"/>
			<xs:enumeration value="FP"/>
			<xs:enumeration value="CUZI"/>
			<xs:enumeration value="CUZG"/>
			<xs:enumeration value="CUO"/>
			<xs:enumeration value="PTE"/>
			<xs:enumeration value="TFI"/>
			<xs:enumeration value="CALENDAR"/>
			<xs:enumeration value="RECOMMENDATIONS"/>
			<xs:enumeration value="SKC"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="AgentActivityType">
		<xs:annotation>
			<xs:documentation>
                Liczba aktywnosci danego typu
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="activityName" type="cmf-party:AgentActivityNameType"/>
			<xs:element name="number" type="xs:int"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgentActivityListType">
		<xs:annotation>
			<xs:documentation>
                Liczba aktywnosci danego typu
            </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="agentId" type="cmf-biz:AgentIdType"/>
			<xs:element name="year" type="xs:gYear"/>
			<xs:element name="month" type="xs:gYearMonth"/>
			<xs:element name="activity" type="cmf-party:AgentActivityType" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="AgentRateType">
		<xs:annotation>
			<xs:documentation>Wskaznik agenta</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="agent-id" type="cmf-biz:AgentIdType"/>
			<xs:element name="rate-type" type="cmf-biz:AgentRateKindType"/>
			<xs:element name="rate" type="xs:float"/>
			<xs:element name="rate-date" type="xs:date"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>]]></con:schema>
  <con:dependencies>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-technical-types" schemaLocation="cmf-technical-types.xsd" ref="Business Services/ESB JV-CUP/WSDL/base/cmf-technical-types.xsd"/>
    <con:import namespace="http://jv.channel.cu.com.pl/cmf-biz-types" schemaLocation="cmf-biz-types.xsd" ref="Business Services/ESB JV-CUP/WSDL/base/cmf-biz-types.xsd"/>
  </con:dependencies>
  <con:targetNamespace>http://jv.channel.cu.com.pl/cmf-party-types</con:targetNamespace>
</con:schemaEntry>