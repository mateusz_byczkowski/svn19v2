<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>xquery version "1.0";

(:~
 :
 : @author  Kacper Pawlaczyk
 : @version 1.4
 : @since   2009-12-15
 :
 : wersja WSDLa: 25-05-2009 13:36:55
 :
 : $Proxy Services/Branches/chgBranchStatus/chgBranchStatusRequest.xq$
 :
 :)
 
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace xf = "http://tempuri.org/Branches/chgBranchStatus/chgBranchStatusRequest/";
declare namespace ns0 = "";
declare namespace ns1 = "urn:branchmanagmentdict.operationsdictionary.dictionaries.be.dcl";
declare namespace ns2 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns4 = "urn:entities.be.dcl";
declare namespace ns5 = "urn:be.services.dcl";

declare variable $invoke1 as element(ns5:invoke) external;
declare variable $header1 as element(ns5:header) external;

(:~
 :
 : @param $header1 nagłówek komunikatu
 : @param $invoke1 operacje wejściowa
 :
 : @return FML32 bufor XML/FML
 :)
declare function xf:chgBranchStatusRequest($header1 as element(ns5:header),
											 $invoke1 as element(ns5:invoke))
    as element(ns0:FML32)
{
	&lt;ns0:FML32&gt;
	
		(:
		 : dane z nagłówka
		 :)
		&lt;ns0:TR_MSG_ID?&gt;{
			data($header1/ns5:msgHeader/ns5:msgId)
		}&lt;/ns0:TR_MSG_ID&gt;
		
		&lt;ns0:TR_ID_OPER?&gt;{
			data($header1/ns5:transHeader/ns5:transId)
		}&lt;/ns0:TR_ID_OPER&gt;
    
    	{
    		(:
    		 : konwersja z timestampa ISO do formatu DD-MM-RRRR
    		 :)
			let $timestamp := data($header1/ns5:msgHeader/ns5:timestamp)
			return
	    		if ($timestamp) then
					&lt;ns0:TR_DATA_OPER&gt;{
		    	        fn:concat(fn:substring(data($timestamp), 9, 2),
		    	        		  '-',
		    	        		  fn:substring(data($timestamp), 6, 2),
		    	        		  '-',
		    	        		  fn:substring(data($timestamp), 1, 4))
					}&lt;/ns0:TR_DATA_OPER&gt;
	    		else
					()
		}

		(:
		 : dane z operacji wejściowej
		 :)
		&lt;ns0:TR_ODDZ_KASY?&gt;{
			data($invoke1/ns5:branchCurrentStatus/ns3:BranchCurrentStatus/ns3:branchCode/ns2:BranchCode/ns2:branchCode)
		}&lt;/ns0:TR_ODDZ_KASY&gt;
		
		&lt;ns0:TR_KASA&gt;0&lt;/ns0:TR_KASA&gt;		(: wartość 'zero' :)
		&lt;ns0:TR_KASJER&gt;0&lt;/ns0:TR_KASJER&gt;	(: wartość 'zero' :) 

		{
			let $user := data($invoke1/ns5:branchCurrentStatus/ns3:BranchCurrentStatus/ns3:lastChangeUser/ns4:User/ns4:userID)
			return
				if ($user) then
					&lt;ns0:TR_UZYTKOWNIK&gt;{
						fn:concat("SKP:", $user)
					}&lt;/ns0:TR_UZYTKOWNIK&gt;
				else
					()
		}
		
		{
			let $acceptor := data($invoke1/ns5:branchCurrentStatus/ns3:BranchCurrentStatus/ns3:lastChangeAcceptor/ns4:User/ns4:userID)
			return
				if ($acceptor) then
					&lt;ns0:TR_AKCEPTANT&gt;{
						fn:concat("SKP:", $acceptor)
					}&lt;/ns0:TR_AKCEPTANT&gt;
				else
					()
		}
		
		(:
		 : 'O' --&gt; 400		otwarcie oddziału	
		 : 'C' --&gt; 401		zamknięcie oddziału	
		 :)
		{
			let $branchStatus := data($invoke1/ns5:branchCurrentStatus/ns3:BranchCurrentStatus/ns3:status/ns1:BranchStatus/ns1:branchStatus)
			return
				if ($branchStatus) then
					if ($branchStatus eq 'O') then
						&lt;ns0:TR_TYP_KOM&gt;
							400
						&lt;/ns0:TR_TYP_KOM&gt;
					else if ($branchStatus eq 'C') then
						&lt;ns0:TR_TYP_KOM&gt;
							401
						&lt;/ns0:TR_TYP_KOM&gt;
					else
						()
				else
					()
		}

		{
			(:
			 : konwersja formatu daty z RRRR-MM-DD na DD-MM-RRRR
			 :)
			let $workDate := data($invoke1/ns5:branchCurrentStatus/ns3:BranchCurrentStatus/ns3:workDate)
			return
				if ($workDate) then
					&lt;ns0:TR_DATA_PRACY&gt;{
						fn:concat(
							fn:substring(data($workDate), 9, 2),
							'-',
							fn:substring(data($workDate), 6, 2),
							'-',
							fn:substring(data($workDate), 1, 4)
						)
					}&lt;/ns0:TR_DATA_PRACY&gt;
				else
					()
		}
		
		&lt;ns0:TR_UZYTKOWNIK_SKP?&gt;{
			data($invoke1/ns5:branchCurrentStatus/ns3:BranchCurrentStatus/ns3:lastChangeUser/ns4:User/ns4:userID)
		}&lt;/ns0:TR_UZYTKOWNIK_SKP&gt;
		
	    &lt;ns0:TR_AKCEPTANT_SKP?&gt;{
			data($invoke1/ns5:branchCurrentStatus/ns3:BranchCurrentStatus/ns3:lastChangeAcceptor/ns4:User/ns4:userID)
		}&lt;/ns0:TR_AKCEPTANT_SKP&gt;
		
	&lt;/ns0:FML32&gt;
};

&lt;soap-env:Body&gt;{
	xf:chgBranchStatusRequest($header1, $invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>