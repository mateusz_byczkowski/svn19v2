<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapeEGERIAGetCustProds($fml as element(fml:FML32))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($fml/fml:NF_CUSTOM_CUSTOMERNUMBER)
					then &lt;fml:EG_KOD_KLIENTA&gt;{ data($fml/fml:NF_CUSTOM_CUSTOMERNUMBER) }&lt;/fml:EG_KOD_KLIENTA&gt;
					else  ()
                         }
                         {
				if($fml/fml:NF_CTRL_ACTIVENONACTIVE)
					then &lt;fml:EG_AKTYWNE_NIEAKTYWNE&gt;{ data($fml/fml:NF_CTRL_ACTIVENONACTIVE) }&lt;/fml:EG_AKTYWNE_NIEAKTYWNE&gt;
					else  &lt;fml:EG_AKTYWNE_NIEAKTYWNE&gt;0&lt;/fml:EG_AKTYWNE_NIEAKTYWNE&gt;

			 }
                         {
				if($fml/fml:NF_PAGEC_ACTIONCODE)
					then &lt;fml:EG_KOD_STRONY&gt;{ data($fml/fml:NF_PAGEC_ACTIONCODE) }&lt;/fml:EG_KOD_STRONY&gt;
					else  ()
			 }
                         {
				if($fml/fml:NF_PAGEC_PAGESIZE)
					then &lt;fml:EG_PAGEC_PAGESIZE&gt;{ data($fml/fml:NF_PAGEC_PAGESIZE) }&lt;/fml:EG_PAGEC_PAGESIZE&gt;
					else  ()
			 }
                         {
				if($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU)
					then &lt;fml:EG_KLUCZ_NAWIG&gt;{ data($fml/fml:NF_PAGEC_NAVIGATIONKEYVALU) }&lt;/fml:EG_KLUCZ_NAWIG&gt;
					else  ()
			 }
                         {
				if($fml/fml:NF_PAGEC_REVERSEORDER)
					then &lt;fml:EG_SORT_ODWROTNE&gt;{ data($fml/fml:NF_PAGEC_REVERSEORDER) }&lt;/fml:EG_SORT_ODWROTNE&gt;
					else  ()
			 }
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapeEGERIAGetCustProds($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>