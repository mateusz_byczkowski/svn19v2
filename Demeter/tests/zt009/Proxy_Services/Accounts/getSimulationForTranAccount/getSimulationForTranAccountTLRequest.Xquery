<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>(:: pragma bea:global-element-parameter parameter="$header1" element="ns9:header" ::)
(:: pragma bea:global-element-parameter parameter="$invoke1" element="ns9:invoke" ::)
(:: pragma bea:global-element-return element="ns0:transactionLogEntry" location="../../Operations/lpAddTLEntry/AddTLEntry.wsdl" ::)

declare namespace ns9 = "urn:be.services.dcl";
declare namespace ns5 = "urn:branchmanagement.operations.entities.be.dcl";
declare namespace ns6 = "urn:entities.be.dcl";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace ns7 = "urn:operationsdictionary.dictionaries.be.dcl";
declare namespace ns10 = "urn:baseauxentities.be.dcl";
declare namespace ns8 = "urn:operations.entities.be.dcl";
declare namespace ns2 = "urn:accounts.entities.be.dcl";
declare namespace ns1 = "urn:filtersandmessages.entities.be.dcl";
declare namespace ns4 = "urn:dictionaries.be.dcl";
declare namespace ns3 = "urn:productstree.entities.be.dcl";
declare namespace ns0 = "http://bzwbk.com/nfe/transactionLog";
declare namespace xf = "http://tempuri.org/Proxy%20Services/Accounts/getSimulationForTranAccount/getSimulationForTranAccountTLRequest/";

declare function xf:getSimulationForTranAccountTLRequest($header1 as element(ns9:header),
    $invoke1 as element(ns9:invoke))
    as element(ns0:transactionLogEntry) {
        &lt;ns0:transactionLogEntry&gt;
            {
                for $businessTransactionType in $invoke1/ns9:transaction/ns8:Transaction/ns8:businessTransactionType/ns7:BusinessTransactionType/ns7:businessTransactionType
                return
                    &lt;businessTransactionType&gt;{ data($businessTransactionType) }&lt;/businessTransactionType&gt;
            }
            {
                for $csrMessageType in $invoke1/ns9:transaction/ns8:Transaction/ns8:csrMessageType/ns7:CsrMessageType/ns7:csrMessageType
                return
                    &lt;csrMessageType&gt;{ data($csrMessageType) }&lt;/csrMessageType&gt;
            }
            {
                for $dtTransactionType in $invoke1/ns9:transaction/ns8:Transaction/ns8:dtTransactionType/ns7:DtTransactionType/ns7:dtTransactionType
                return
                    &lt;dtTransactionType&gt;{ data($dtTransactionType) }&lt;/dtTransactionType&gt;
            }
            &lt;executor&gt;
                {
                    for $branchCode in $invoke1/ns9:branchCode/ns4:BranchCode/ns4:branchCode
                    return
                        &lt;branchNumber&gt;{ xs:int( data($branchCode) ) }&lt;/branchNumber&gt;
                }
                &lt;executorID&gt;{ data($header1/ns9:msgHeader/ns9:userId) }&lt;/executorID&gt;
                {
                    for $userFirstName in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:user/ns6:User/ns6:userFirstName
                    return
                        &lt;firstName&gt;{ data($userFirstName) }&lt;/firstName&gt;
                }
                {
                    for $userLastName in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:user/ns6:User/ns6:userLastName
                    return
                        &lt;lastName&gt;{ data($userLastName) }&lt;/lastName&gt;
                }
                {
                    for $tellerID in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:teller/ns5:Teller/ns5:tellerID
                    return
                        &lt;tellerID&gt;{ data($tellerID) }&lt;/tellerID&gt;
                }
                {
                    for $tillID in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:till/ns5:Till/ns5:tillID
                    return
                        &lt;tillNumber&gt;{ data($tillID) }&lt;/tillNumber&gt;
                }
                {
                    for $userID in $invoke1/ns9:userTxnSession/ns5:UserTxnSession/ns5:user/ns6:User/ns6:userID
                    return
                        &lt;userID&gt;{ data($userID) }&lt;/userID&gt;
                }
            &lt;/executor&gt;
            {
                for $extendedCSRMessageType in $invoke1/ns9:transaction/ns8:Transaction/ns8:extendedCSRMessageType/ns7:ExtendedCSRMessageType/ns7:extendedCSRMessageType
                return
                    &lt;extendedCSRMessageType&gt;{ data($extendedCSRMessageType) }&lt;/extendedCSRMessageType&gt;
            }
            &lt;hlbsName&gt;getSimulationForTranAccount&lt;/hlbsName&gt;
            {
                for $orderedBy in $invoke1/ns9:transaction/ns8:Transaction/ns8:orderedBy
                return
                    &lt;orderedBy&gt;{ data($orderedBy) }&lt;/orderedBy&gt;
            }
            {
                for $putDownDate in $invoke1/ns9:transaction/ns8:Transaction/ns8:putDownDate
                return
                    &lt;putDownDate&gt;{ xs:date( data($putDownDate)) }&lt;/putDownDate&gt;
            }
            &lt;timestamp&gt;{ data($header1/ns9:msgHeader/ns9:timestamp) }&lt;/timestamp&gt;
            {
                let $Account := $invoke1/ns9:account/ns2:Account
                return
                    &lt;tlAccountList&gt;
                        {
                            for $accountNumber in $Account/ns2:accountNumber
                            return
                                &lt;accountNumber&gt;{ data($accountNumber) }&lt;/accountNumber&gt;
                        }
                        {
                            for $idProductDefinition in $Account/ns2:productDefinition/ns3:ProductDefinition/ns3:idProductDefinition
                            return
                                &lt;productId&gt;{ data($idProductDefinition) }&lt;/productId&gt;
                        }
                    &lt;/tlAccountList&gt;
            }
            {
                for $transactionDate in $invoke1/ns9:transaction/ns8:Transaction/ns8:transactionDate
                return
                    &lt;transactionDate&gt;{ xs:date( data($transactionDate)) }&lt;/transactionDate&gt;
            }
            {
                for $transactionGroupID in $invoke1/ns9:transaction/ns8:Transaction/ns8:transactionGroupID
                return
                    &lt;transactionGroupID&gt;{ data($transactionGroupID) }&lt;/transactionGroupID&gt;
            }
            &lt;transactionID&gt;{ data($header1/ns9:transHeader/ns9:transId) }&lt;/transactionID&gt;
        &lt;/ns0:transactionLogEntry&gt;
};

declare variable $header1 as element(ns9:header) external;
declare variable $invoke1 as element(ns9:invoke) external;

&lt;soap-env:Body&gt;{
xf:getSimulationForTranAccountTLRequest($header1,
    $invoke1)
}&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>