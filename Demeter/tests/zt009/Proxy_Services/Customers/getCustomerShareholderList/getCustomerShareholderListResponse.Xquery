<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace m="urn:be.services.dcl";
declare namespace m1="urn:cif.entities.be.dcl";
declare namespace xf="urn:be.services.dcl/mappings";

declare variable $body external;
declare variable $fml as element(FML32):=$body/FML32;

declare function m:isData($dateIn as xs:string) as xs:boolean {
  if(string-length(normalize-space(data($dateIn)))&gt;0)
    then true()
    else false()
};

declare function xf:mapDate($dateIn as xs:string) as xs:string {
     fn:concat (fn:substring($dateIn,7,4),"-",fn:substring($dateIn,4,2),"-",fn:substring($dateIn,1,2))
};

&lt;soap-env:Body&gt; 
  &lt;m:invokeResponse xmlns:urn="urn:be.services.dcl"&gt; 
    &lt;m:shareholderList xmlns:urn1="urn:cif.entities.be.dcl"&gt; 
      {if($fml/DC_NUMER_KLIENTA)
        then 
          for $x at $i in $fml/DC_NUMER_KLIENTA
            return
              &lt;m1:Customer&gt;
                {if($fml/CI_NAZWA_PELNA[$i] and m:isData($fml/CI_NAZWA_PELNA[$i]))
                    then &lt;m1:companyName&gt;{data($fml/CI_NAZWA_PELNA[$i])}&lt;/m1:companyName&gt;
                    else ()
                }
                {if($fml/DC_NUMER_KLIENTA[$i] and m:isData($fml/DC_NUMER_KLIENTA[$i]))
                    then &lt;m1:customerNumber&gt;{data($fml/DC_NUMER_KLIENTA[$i])}&lt;/m1:customerNumber&gt;
                    else ()
                }
                {if($fml/DC_DATA_WPROWADZENIA[$i] and m:isData($fml/DC_DATA_WPROWADZENIA[$i]))
                    then &lt;m1:dateCustomerOpened&gt;{xf:mapDate($fml/DC_DATA_WPROWADZENIA[$i])}&lt;/m1:dateCustomerOpened&gt;
                    else ()
                }
                {if($fml/CI_DATA_AKTUALIZACJI[$i] and m:isData($fml/CI_DATA_AKTUALIZACJI[$i]))
                    then &lt;m1:fileMaintenanceLastDate&gt;{xf:mapDate($fml/CI_DATA_AKTUALIZACJI[$i])}&lt;/m1:fileMaintenanceLastDate&gt;
                    else ()
                }
                {if($fml/DC_REZ_NIEREZ[$i] and m:isData($fml/DC_REZ_NIEREZ[$i]))
                    then &lt;m1:resident&gt;{fn:boolean(xs:integer($fml/DC_REZ_NIEREZ[$i]))}&lt;/m1:resident&gt;
                    else ()
                }
                {if($fml/DC_NIP[$i] and m:isData($fml/DC_NIP[$i]))
                    then &lt;m1:taxID&gt;{data($fml/DC_NIP[$i])}&lt;/m1:taxID&gt;
                    else ()
                }
                {if($fml/DC_NAZWA_SKROCONA[$i] and m:isData($fml/DC_NAZWA_SKROCONA[$i]))
                    then &lt;m1:shortName&gt;{data($fml/DC_NAZWA_SKROCONA[$i])}&lt;/m1:shortName&gt;
                    else (if($fml/CI_NAZWA_PELNA[$i] and m:isData($fml/CI_NAZWA_PELNA[$i]))
                          then &lt;m1:shortName&gt;{data($fml/CI_NAZWA_PELNA[$i])}&lt;/m1:shortName&gt;
                          else ()
                         )
                }
                {if($fml/CI_DECYZJA[$i] and m:isData($fml/CI_DECYZJA[$i]))
                    then &lt;m1:crmDecision&gt;{data($fml/CI_DECYZJA[$i])}&lt;/m1:crmDecision&gt;
                    else ()
                }
                {if($fml/CI_DATA_NIP[$i] and m:isData($fml/CI_DATA_NIP[$i]))
                    then &lt;m1:taxIdDate&gt;{xf:mapDate($fml/CI_DATA_NIP[$i])}&lt;/m1:taxIdDate&gt;
                    else ()
                }
                &lt;m1:certificate&gt;false&lt;/m1:certificate&gt;
              &lt;/m1:Customer&gt; 
        else ()
      }
    &lt;/m:shareholderList&gt;
  &lt;/m:invokeResponse&gt; 
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>