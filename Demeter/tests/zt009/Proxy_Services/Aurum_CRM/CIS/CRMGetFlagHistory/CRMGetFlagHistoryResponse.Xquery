<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetFlagHistoryResponse($fml as element(fml:FML32))
	as element(m:CRMGetFlagHistoryResponse) {
		&lt;m:CRMGetFlagHistoryResponse&gt;
					{
                                let $CI_SKP_PRACOWNIKA_REJ := $fml/fml:CI_SKP_PRACOWNIKA_REJ
                                let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
                                let $CI_DATA_AKTUALIZACJI := $fml/fml:CI_DATA_AKTUALIZACJI
                                let $CI_TYP := $fml/fml:CI_TYP
                                let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
                                let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
                                let $DC_IMIE := $fml/fml:DC_IMIE
                                let $DC_DRUGIE_IMIE := $fml/fml:DC_DRUGIE_IMIE
                                let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
                                let $CI_NUMER_ODDZIALU := $fml/fml:CI_NUMER_ODDZIALU

				for $it at $p in $fml/fml:DC_NUMER_KLIENTA
                                      return
                                            &lt;m:FlagHistory&gt;

			{
				if($CI_SKP_PRACOWNIKA_REJ[$p])
					then &lt;m:SetUserId&gt;{ data($CI_SKP_PRACOWNIKA_REJ[$p]) }&lt;/m:SetUserId&gt;
					else ()
			}
			{
				if($CI_SKP_PRACOWNIKA[$p])
					then &lt;m:AccUserId&gt;{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:AccUserId&gt;
					else ()
			}
			{
				if($CI_DATA_AKTUALIZACJI[$p])
					then &lt;m:AccDate&gt;{ data($CI_DATA_AKTUALIZACJI[$p]) }&lt;/m:AccDate&gt;
					else ()
			}
			{
				if($CI_TYP[$p])
					then &lt;m:FlagType&gt;{ data($CI_TYP[$p]) }&lt;/m:FlagType&gt;
					else ()
			}
			{
				if($DC_NUMER_KLIENTA[$p])
					then &lt;m:CustCif&gt;{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:CustCif&gt;
					else ()
			}
			{
				if($CI_NAZWA_PELNA[$p])
					then &lt;m:CustName&gt;{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:CustName&gt;
					else ()
			}
			{
				if($DC_IMIE[$p])
					then &lt;m:CustFirstName&gt;{ data($DC_IMIE[$p]) }&lt;/m:CustFirstName&gt;
					else ()
			}
			{
				if($DC_DRUGIE_IMIE[$p])
					then &lt;m:CustSecondName&gt;{ data($DC_DRUGIE_IMIE[$p]) }&lt;/m:CustSecondName&gt;
					else ()
			}
			{
				if($DC_NAZWISKO[$p])
					then &lt;m:CustSurname&gt;{ data($DC_NAZWISKO[$p]) }&lt;/m:CustSurname&gt;
					else ()
			}
			{
				if($CI_NUMER_ODDZIALU[$p])
					then &lt;m:IcbsNo&gt;{ data($CI_NUMER_ODDZIALU[$p]) }&lt;/m:IcbsNo&gt;
					else ()
			}
  &lt;/m:FlagHistory&gt;
                        }
		&lt;/m:CRMGetFlagHistoryResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetFlagHistoryResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>