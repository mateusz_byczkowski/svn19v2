<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>v.1.1 2009-11-05 LK PT58</con:description>
  <con:xquery>declare namespace ctx="http://www.bea.com/wli/sb/context";
declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace f="http://bzwbk.com/services/faults/";
declare variable $fault external;
declare variable $body external;

declare function local:fault($faultString as xs:string, $detail as element()) as element(soap-env:Fault) {
		&lt;soap-env:Fault&gt;
			&lt;faultcode&gt;soapenv:Server.userException&lt;/faultcode&gt; 
			&lt;faultstring&gt;{ $faultString }&lt;/faultstring&gt; 
			&lt;detail&gt;{ $detail }&lt;/detail&gt;
		&lt;/soap-env:Fault&gt;
};

declare function local:errors($errorCode1 as xs:string, $errorCode2 as xs:string,$desc as xs:string) as element()* {
	&lt;f:errorCode1&gt;{ $errorCode1 }&lt;/f:errorCode1&gt;,
	&lt;f:errorCode2&gt;{ $errorCode2 }&lt;/f:errorCode2&gt;, 
        &lt;f:errorDescription&gt;{$desc}&lt;/f:errorDescription&gt;
};



declare function local:getErrorDescription($fml as element()*) as xs:string { 
    if ($fml/DC_OPIS_BLEDU[1])
       then  data($fml/DC_OPIS_BLEDU[1])
    else  ""
       
};


declare function local:getDefaultDescription($a as xs:string*, $def as xs:string) as xs:string{
   if (string-length($a) = 0)
       then $def
   else $a
};

&lt;soap-env:Body&gt;
	{
		let $reason := fn:substring-before(fn:substring-after(fn:substring-before(data($fault/ctx:reason), ":"), "("), ")") 
                
		let $urcode := fn:substring-before(fn:substring-after(fn:substring-after(data($fault/ctx:reason), ":"), ":"), ":")
 
                let $errorDescription := local:getErrorDescription($body/FML32)
		return
			if($reason = "13") then
				local:fault("com.bzwbk.services.cis.faults.TimeoutException", element f:TimeoutException { local:errors($reason, $urcode,"Przekroczenie dopuszczalnego czasu odpowiedzi") })
		        else if($reason = "6") then
				local:fault("com.bzwbk.services.cis.faults.ServiceException", element f:ServiceException{ local:errors($reason, $urcode,"Brak usługi źródłowej w domenie tuxedo") })
			else if($reason = "11") then
				if($urcode = "101") then
					local:fault("com.bzwbk.services.cis.faults.FMLBufferException", element f:FMLBufferException { local:errors($reason, $urcode,"Błąd bufora FML") })
				else if($urcode = "102") then
					local:fault("com.bzwbk.services.cis.faults.WrongInputException", element f:WrongInputException { local:errors($reason, $urcode,local:getDefaultDescription($errorDescription,"Błędny komunikat wejściowy")) })
				else if($urcode = "103") then
					local:fault("com.bzwbk.services.cis.faults.NoDataException", element f:NoDataException { local:errors($reason, $urcode,"Brak danych") })
				else
					local:fault("com.bzwbk.services.cis.faults.ServiceFailException", element f:ServiceFailException { local:errors($reason, $urcode,$errorDescription) })
			else
				local:fault("com.bzwbk.services.cis.faults.ServiceException", element f:ServiceException { local:errors($reason, $urcode, "") })
	}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>