<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace soap-env="http://schemas.xmlsoap.org/soap/envelope/";
declare namespace cer="http://bzwbk.com/services/cerber";
declare namespace auth="java:com.bzwbk.cerber.services.auth";
declare namespace data="java:com.bzwbk.cerber.services.data";

declare variable $body external;

declare function local:authData($appAuthData as element(cer:AppAuthData)) as element() {
    &lt;cer:authData&gt;
        &lt;auth:ApplicationId&gt;{ data($appAuthData/ApplicationId) }&lt;/auth:ApplicationId&gt;
        &lt;auth:ApplicationPassword&gt;{ data($appAuthData/ApplicationPassword) }&lt;/auth:ApplicationPassword&gt;
        &lt;auth:Operator&gt;{ data($appAuthData/Operator) }&lt;/auth:Operator&gt;
    &lt;/cer:authData&gt; 
};

&lt;soap-env:Body&gt;
{
    typeswitch($body/*)
        case $req as element(cer:GetUserDataRequest) return
            &lt;cer:getUserData&gt;
                &lt;cer:userId&gt;{ data($req/cer:UserId) }&lt;/cer:userId&gt;
                &lt;cer:password&gt;{ data($req/cer:Password) }&lt;/cer:password&gt;            
                { local:authData($req/cer:AppAuthData) }
            &lt;/cer:getUserData&gt;
        case $req as element(cer:GetUserDataWithHashRequest) return
            &lt;cer:getUserDataWithHash&gt;
                &lt;cer:userId&gt;{ data($req/cer:UserId) }&lt;/cer:userId&gt;
                &lt;cer:passwordHash&gt;{ data($req/cer:PasswordHash) }&lt;/cer:passwordHash&gt;            
                { local:authData($req/cer:AppAuthData) }
            &lt;/cer:getUserDataWithHash&gt;          
        case $req as element(cer:GetUserDataExtendedRequest) return
            &lt;cer:getUserDataExtended&gt;
                &lt;cer:userId&gt;{ data($req/cer:UserId) }&lt;/cer:userId&gt;
                &lt;cer:password&gt;{ data($req/cer:Password) }&lt;/cer:password&gt;            
                { local:authData($req/cer:AppAuthData) }                
            &lt;/cer:getUserDataExtended&gt;
        case $req as element(cer:GetUserDataWithoutPassRequest) return
            &lt;cer:getUserDataWithoutPass&gt;
                &lt;cer:userId&gt;{ data($req/cer:UserId) }&lt;/cer:userId&gt;
                { local:authData($req/cer:AppAuthData) }                
            &lt;/cer:getUserDataWithoutPass&gt;
        case $req as element(cer:GetUsersDataWithoutPassRequest) return
            &lt;cer:getUsersDataWithoutPass&gt;
                &lt;cer:userIds&gt;
                {
                    for $userId in $req/cer:UserIds
                        return &lt;cer:JavaLangint&gt;{ data($userId) }&lt;/cer:JavaLangint&gt;
                }
                &lt;/cer:userIds&gt;
                { local:authData($req/cer:AppAuthData) }                
            &lt;/cer:getUsersDataWithoutPass&gt;
        case $req as element(cer:FindUsersMasterUnitsICBSNoRequest) return
            &lt;cer:findUsersMasterUnitsICBSno&gt;
                &lt;cer:skps&gt;
                {
                    for $skp in $req/cer:Skps
                        return &lt;cer:JavaLangint&gt;{ data($skp) }&lt;/cer:JavaLangint&gt;
                }
                &lt;/cer:skps&gt;
                { local:authData($req/cer:AppAuthData) }                
            &lt;/cer:findUsersMasterUnitsICBSno&gt;
        case $req as element(cer:GetDirectSuperiorUserRequest) return
            &lt;cer:getDirectSuperiorUser&gt;
                &lt;cer:skp&gt;{ data($req/cer:Skp) }&lt;/cer:skp&gt;
                { local:authData($req/cer:AppAuthData) }
            &lt;/cer:getDirectSuperiorUser&gt;
        case $req as element(cer:SearchUsersRequest) return
            &lt;cer:searchUsers&gt;
            {
                if($req/cer:UnitId) then 
                    &lt;cer:unitId?&gt;{ data($req/cer:UnitId) }&lt;/cer:unitId&gt;
                else
                    &lt;cer:unitId xsi:nil="true" /&gt;,
                if($req/cer:Skp) then 
                    &lt;cer:skp&gt;
                    {
                        for $skp in $req/cer:Skp
                            return &lt;cer:JavaLangint&gt;{ data($skp) }&lt;/cer:JavaLangint&gt;
                    }
                    &lt;/cer:skp&gt;
                else
                    &lt;cer:skp xsi:nil="true" /&gt;,
                if($req/cer:Name) then 
                    &lt;cer:name&gt;{ data($req/cer:Name) }&lt;/cer:name&gt;
                else
                    &lt;cer:name xsi:nil="true" /&gt;,
                if($req/cer:Surname) then 
                    &lt;cer:surname&gt;{ data($req/cer:Surname) }&lt;/cer:surname&gt;
                else
                    &lt;cer:surname xsi:nil="true" /&gt;,
                if($req/cer:PositionName) then 
                    &lt;cer:positionName&gt;{ data($req/cer:PositionName) }&lt;/cer:positionName&gt;
                else
                    &lt;cer:positionName xsi:nil="true" /&gt;,
                if($req/cer:StartDate) then 
                    &lt;cer:startDate&gt;{ data($req/cer:StartDate) }&lt;/cer:startDate&gt;
                else
                    &lt;cer:startDate xsi:nil="true" /&gt;,
                if($req/cer:EndDate) then 
                    &lt;cer:endDate&gt;{ data($req/cer:EndDate) }&lt;/cer:endDate&gt;
                else
                    &lt;cer:endDate xsi:nil="true" /&gt;,
                    
                if($req/cer:Function) then
                    &lt;cer:functions&gt;
                    {
                        for $func in $req/cer:Function
                            return &lt;cer:JavaLangstring&gt;{ data($func) }&lt;/cer:JavaLangstring&gt;
                    }
                    &lt;/cer:functions&gt;
                else
                    &lt;cer:functions xsi:nil="true" /&gt;
            }
                    
                { local:authData($req/cer:AppAuthData) }                
                
                &lt;cer:firstRow&gt;{ data($req/cer:FirstRow) }&lt;/cer:firstRow&gt;
                &lt;cer:numRows&gt;{ data($req/cer:NumRows) }&lt;/cer:numRows&gt;
            &lt;/cer:searchUsers&gt;
        case $req as element(cer:GetDirectSuperiorUsersRequest) return
            &lt;cer:getDirectSuperiorUsers&gt;
                &lt;cer:userIds&gt;
                {
                    for $userId in $req/cer:UserIds
                        return &lt;cer:JavaLangint&gt;{ data($userId) }&lt;/cer:JavaLangint&gt;
                }
                &lt;/cer:userIds&gt;
                { local:authData($req/cer:AppAuthData) }
            &lt;/cer:getDirectSuperiorUsers&gt;
        case $req as element(cer:GetUserPermissionUnitInfoRequest) return
            &lt;cer:getUserPermissionUnitInfo&gt;
                { local:authData($req/cer:AppAuthData) }
            &lt;/cer:getUserPermissionUnitInfo&gt;
        default return &lt;error&gt;Error translating request message&lt;/error&gt;     
}
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>