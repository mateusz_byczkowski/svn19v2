<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetProspectRequest($req as element(m:CRMGetProspectRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdKlienta)
					then &lt;fml:CI_ID_KLIENTA&gt;{ data($req/m:IdKlienta) }&lt;/fml:CI_ID_KLIENTA&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetProspectRequest($body/m:CRMGetProspectRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>