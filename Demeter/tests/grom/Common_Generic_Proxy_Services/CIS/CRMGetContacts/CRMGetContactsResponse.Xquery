<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetContactsResponse($fml as element(fml:FML32))
	as element(m:CRMGetContactsResponse) {
		&lt;m:CRMGetContactsResponse&gt;
			{
				let $CI_NUMER_PACZKI := $fml/fml:CI_NUMER_PACZKI return
					if($CI_NUMER_PACZKI)
						then &lt;m:NumerPaczki&gt;{ data($CI_NUMER_PACZKI) }&lt;/m:NumerPaczki&gt;
						else ()
			}
			{
				let $DC_NUMER_KLIENTA := $fml/fml:DC_NUMER_KLIENTA
				let $CI_NAZWA_PELNA := $fml/fml:CI_NAZWA_PELNA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $CI_STATUS_GIODO := $fml/fml:CI_STATUS_GIODO
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $CI_RODZAJ_KONT := $fml/fml:CI_RODZAJ_KONT
				let $CI_CEL_KONT := $fml/fml:CI_CEL_KONT
				let $CI_SPOS_KONT := $fml/fml:CI_SPOS_KONT
				let $CI_ID_KAMP := $fml/fml:CI_ID_KAMP
				let $CI_PRODUKTY := $fml/fml:CI_PRODUKTY
				let $CI_SKP_PRACOWNIKA := $fml/fml:CI_SKP_PRACOWNIKA
				let $CI_ID_KONT := $fml/fml:CI_ID_KONT
				let $CI_DATA_KONT := $fml/fml:CI_DATA_KONT
				let $CI_WYNIK_KONTAKTU := $fml/fml:CI_WYNIK_KONTAKTU
				let $CI_ID_ZADANIA := $fml/fml:CI_ID_ZADANIA
				let $CI_ID_PORTFELA := $fml/fml:CI_ID_PORTFELA
				for $it at $p in $fml/fml:CI_ID_KONT
				return
					&lt;m:CRMGetContactsKontakt&gt;
					{
						if($DC_NUMER_KLIENTA[$p])
							then &lt;m:NumerKlienta&gt;{ data($DC_NUMER_KLIENTA[$p]) }&lt;/m:NumerKlienta&gt;
						else ()
					}
					{
						if($CI_NAZWA_PELNA[$p])
							then &lt;m:NazwaPelna&gt;{ data($CI_NAZWA_PELNA[$p]) }&lt;/m:NazwaPelna&gt;
						else ()
					}
					{
						if($DC_IMIE[$p])
							then &lt;m:Imie&gt;{ data($DC_IMIE[$p]) }&lt;/m:Imie&gt;
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then &lt;m:Nazwisko&gt;{ data($DC_NAZWISKO[$p]) }&lt;/m:Nazwisko&gt;
						else ()
					}
					{
						if($CI_STATUS_GIODO[$p])
							then &lt;m:StatusGiodo&gt;{ data($CI_STATUS_GIODO[$p]) }&lt;/m:StatusGiodo&gt;
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then &lt;m:UdostepGrupa&gt;{ data($CI_UDOSTEP_GRUPA[$p]) }&lt;/m:UdostepGrupa&gt;
						else ()
					}
					{
						if($CI_RODZAJ_KONT[$p])
							then &lt;m:RodzajKont&gt;{ data($CI_RODZAJ_KONT[$p]) }&lt;/m:RodzajKont&gt;
						else ()
					}
					{
						if($CI_CEL_KONT[$p])
							then &lt;m:CelKont&gt;{ data($CI_CEL_KONT[$p]) }&lt;/m:CelKont&gt;
						else ()
					}
					{
						if($CI_SPOS_KONT[$p])
							then &lt;m:SposKont&gt;{ data($CI_SPOS_KONT[$p]) }&lt;/m:SposKont&gt;
						else ()
					}
					{
						if($CI_ID_KAMP[$p])
							then &lt;m:IdKamp&gt;{ data($CI_ID_KAMP[$p]) }&lt;/m:IdKamp&gt;
						else ()
					}
					{
						if($CI_PRODUKTY[$p])
							then &lt;m:Produkty&gt;{ data($CI_PRODUKTY[$p]) }&lt;/m:Produkty&gt;
						else ()
					}
					{
						if($CI_SKP_PRACOWNIKA[$p])
							then &lt;m:SkpPracownika&gt;{ data($CI_SKP_PRACOWNIKA[$p]) }&lt;/m:SkpPracownika&gt;
						else ()
					}
					{
						if($CI_ID_KONT[$p])
							then &lt;m:IdKont&gt;{ data($CI_ID_KONT[$p]) }&lt;/m:IdKont&gt;
						else ()
					}
					{
						if($CI_DATA_KONT[$p])
							then &lt;m:DataKont&gt;{ data($CI_DATA_KONT[$p]) }&lt;/m:DataKont&gt;
						else ()
					}
					{
						if($CI_WYNIK_KONTAKTU[$p])
							then &lt;m:WynikKontaktu&gt;{ data($CI_WYNIK_KONTAKTU[$p]) }&lt;/m:WynikKontaktu&gt;
						else ()
					}
					{
						if($CI_ID_ZADANIA[$p])
							then &lt;m:IdZadania&gt;{ data($CI_ID_ZADANIA[$p]) }&lt;/m:IdZadania&gt;
						else ()
					}
					{
						if($CI_ID_PORTFELA[$p])
							then &lt;m:IdPortfela&gt;{ data($CI_ID_PORTFELA[$p]) }&lt;/m:IdPortfela&gt;
						else ()
					}
					&lt;/m:CRMGetContactsKontakt&gt;
			}
		&lt;/m:CRMGetContactsResponse&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetContactsResponse($body/fml:FML32) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>