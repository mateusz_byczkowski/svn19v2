<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:xquery>declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMGetPortCustsRequest($req as element(m:CRMGetPortCustsRequest))
	as element(fml:FML32) {
		&lt;fml:FML32&gt;
			{
				if($req/m:IdWewPrac)
					then &lt;fml:CI_ID_WEW_PRAC&gt;{ data($req/m:IdWewPrac) }&lt;/fml:CI_ID_WEW_PRAC&gt;
					else ()
			}
			{
				if($req/m:IdSpolki)
					then &lt;fml:CI_ID_SPOLKI&gt;{ data($req/m:IdSpolki) }&lt;/fml:CI_ID_SPOLKI&gt;
					else ()
			}
			{
				if($req/m:SkpPracownika)
					then &lt;fml:CI_SKP_PRACOWNIKA&gt;{ data($req/m:SkpPracownika) }&lt;/fml:CI_SKP_PRACOWNIKA&gt;
					else ()
			}
			{
				if($req/m:IdPortfela)
					then &lt;fml:CI_ID_PORTFELA&gt;{ data($req/m:IdPortfela) }&lt;/fml:CI_ID_PORTFELA&gt;
					else ()
			}
			{
				if($req/m:TypKlienta)
					then &lt;fml:DC_TYP_KLIENTA&gt;{ data($req/m:TypKlienta) }&lt;/fml:DC_TYP_KLIENTA&gt;
					else ()
			}
			{
				if($req/m:KlasaObslugi)
					then &lt;fml:CI_KLASA_OBSLUGI&gt;{ data($req/m:KlasaObslugi) }&lt;/fml:CI_KLASA_OBSLUGI&gt;
					else ()
			}
			{
				for $v in $req/m:StatusGiodo
				return
					&lt;fml:CI_STATUS_GIODO&gt;{ data($v) }&lt;/fml:CI_STATUS_GIODO&gt;
			}
			{
				for $v in $req/m:UdostepGrupa
				return
					&lt;fml:CI_UDOSTEP_GRUPA&gt;{ data($v) }&lt;/fml:CI_UDOSTEP_GRUPA&gt;
			}
			{
				if($req/m:InnePortfele)
					then &lt;fml:CI_INNE_PORTFELE&gt;{ data($req/m:InnePortfele) }&lt;/fml:CI_INNE_PORTFELE&gt;
					else ()
			}
			{
				if($req/m:Kontakty)
					then &lt;fml:CI_KONTAKTY&gt;{ data($req/m:Kontakty) }&lt;/fml:CI_KONTAKTY&gt;
					else ()
			}
			{
				if($req/m:SkpPracownikaUpr)
					then &lt;fml:CI_SKP_PRACOWNIKA_UPR&gt;{ data($req/m:SkpPracownikaUpr) }&lt;/fml:CI_SKP_PRACOWNIKA_UPR&gt;
					else ()
			}
			{
				if($req/m:TypKont)
					then &lt;fml:CI_TYP_KONT&gt;{ data($req/m:TypKont) }&lt;/fml:CI_TYP_KONT&gt;
					else ()
			}
			{
				if($req/m:CelKont)
					then &lt;fml:CI_CEL_KONT&gt;{ data($req/m:CelKont) }&lt;/fml:CI_CEL_KONT&gt;
					else ()
			}
			{
				if($req/m:DataOstKontaktu)
					then &lt;fml:CI_DATA_OST_KONTAKTU&gt;{ data($req/m:DataOstKontaktu) }&lt;/fml:CI_DATA_OST_KONTAKTU&gt;
					else ()
			}
			{
				if($req/m:LiczbaOper)
					then &lt;fml:CI_LICZBA_OPER&gt;{ data($req/m:LiczbaOper) }&lt;/fml:CI_LICZBA_OPER&gt;
					else ()
			}
			{
				if($req/m:NumerPaczki)
					then &lt;fml:CI_NUMER_PACZKI&gt;{ data($req/m:NumerPaczki) }&lt;/fml:CI_NUMER_PACZKI&gt;
					else ()
			}
			{
				if($req/m:Opcja)
					then &lt;fml:CI_OPCJA&gt;{ data($req/m:Opcja) }&lt;/fml:CI_OPCJA&gt;
					else ()
			}
		&lt;/fml:FML32&gt;
};

declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
{ xf:mapCRMGetPortCustsRequest($body/m:CRMGetPortCustsRequest) }
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>