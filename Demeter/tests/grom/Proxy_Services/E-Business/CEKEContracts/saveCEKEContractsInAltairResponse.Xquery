<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2010-08-02</con:description>
  <con:xquery>declare namespace m = "http://bzwbk.com/services/messages/";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
declare namespace urn = "http://bzwbk.com/services/ceke/messages/";


declare variable $body as element(soap-env:Body) external;
&lt;soap-env:Body&gt;
&lt;/soap-env:Body&gt;</con:xquery>
</con:xqueryEntry>