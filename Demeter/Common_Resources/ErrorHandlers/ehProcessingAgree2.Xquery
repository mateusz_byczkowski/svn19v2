<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-07-12</con:description>
  <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err = "urn:errors.hlbsentities.be.dcl";
declare namespace pdk = "http://bzwbk.com/services/pdk/";

declare function local:response($fault as element(soap-env:Fault)) as element(soap-env:Body) {
    <soap-env:Body>
        <soap-env:Fault>
            <faultcode>Server</faultcode>
            <faultstring>{ data($fault/faultstring) }</faultstring>
            <detail>
                <err:ServiceFailException>
                    <err:exceptionItem>
                        <err:errorCode1>-1</err:errorCode1>
                        <err:errorCode2>0</err:errorCode2>
                        <err:errorDescription>{ data($fault/faultstring) }</err:errorDescription>
                    </err:exceptionItem>
                </err:ServiceFailException>
            </detail>
        </soap-env:Fault>
    </soap-env:Body>
};

declare variable $body as element(soap-env:Body) external;

local:response($body/soap-env:Fault)]]></con:xquery>
</con:xqueryEntry>