<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>error handler dla usług chgProcessingAgreeParameters i getProcessingAgreeParametersVersion.$2.2011-07-12</con:description>
  <con:xquery><![CDATA[declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace err = "urn:errors.hlbsentities.be.dcl";
declare namespace pdk = "http://bzwbk.com/services/pdk/";

declare function local:response($fault as element(soap-env:Fault)) as element() {
        let $se := $fault/detail/pdk:ServiceException
        let $faultstring := fn:concat(data($se/pdk:Description), ", ", data($se/pdk:ErrDetails))
        return
            <soap-env:Fault>
                <faultcode>Server</faultcode>
                <faultstring>{ $faultstring }</faultstring>
                <detail>
                    <err:ServiceFailException>
                        <err:exceptionItem>
                            <err:errorCode1>{ data($se/pdk:ErrorCode) }</err:errorCode1>
                            <err:errorCode2>0</err:errorCode2>
                            <err:errorDescription>{ $faultstring }</err:errorDescription>
                        </err:exceptionItem>
                    </err:ServiceFailException>
                </detail>
        </soap-env:Fault>
};

declare variable $body as element(soap-env:Body) external;

<soap-env:Body>{ local:response($body/soap-env:Fault) }</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>