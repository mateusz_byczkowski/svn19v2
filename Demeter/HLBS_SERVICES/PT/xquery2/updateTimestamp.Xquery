<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Aktualizacja pola timestamp w nagłówku.Version.$1.2011-04-06</con:description>
  <con:xquery><![CDATA[xquery version "1.0";

(:~
 :
 : @author	Tomasz Krajewski  
 : @version	1.0
 : @since	2011-02-22
 :
 : $HLBS_SERVICES/PT/xquery2/updateTimestamp.xq$
 :
 :)

declare namespace xf = "http://tempuri.org/PT/xquery2/updateTimestamp/";
declare namespace dcl = "urn:be.services.dcl";
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";

declare function xf:updateTimestamp($header as element(dcl:header))
as element(dcl:header)
{
	<dcl:header>
		<dcl:msgHeader>
		{
			for $elt in $header/dcl:msgHeader/*
			return
			(
				if (fn:local-name($elt) eq 'timestamp')
				then element {node-name($elt)} {$elt/@*, fn:current-dateTime()}
				else element {node-name($elt)} {$elt/@*, data($elt)}
			)
		}
		</dcl:msgHeader>
		{
			if (data($header/dcl:transHeader/*))
			then
			(
				<dcl:transHeader>
				{
					for $elt in $header/dcl:transHeader/*
					return
						element {node-name($elt)} {$elt/@*, data($elt)}
				}
				</dcl:transHeader>
			)
			else
			()
		}
	</dcl:header>
};

declare variable $header as element(dcl:header) external;

<soapenv:Header>
{
	xf:updateTimestamp($header)
}
</soapenv:Header>]]></con:xquery>
</con:xqueryEntry>