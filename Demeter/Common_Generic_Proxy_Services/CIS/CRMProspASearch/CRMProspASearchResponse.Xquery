<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-07-11</con:description>
  <con:xquery><![CDATA[declare namespace m = "http://bzwbk.com/services/cis/messages/";
declare namespace xf = "http://bzwbk.com/services/cis/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapCRMProspASearchResponse($fml as element(fml:FML32))
	as element(m:CRMProspASearchResponse) {
		<m:CRMProspASearchResponse>
			{

				let $CI_ID_KLIENTA := $fml/fml:CI_ID_KLIENTA
				let $DC_IMIE := $fml/fml:DC_IMIE
				let $DC_NAZWISKO := $fml/fml:DC_NAZWISKO
				let $DC_NAZWA := $fml/fml:DC_NAZWA
				let $CI_UDOSTEP_GRUPA := $fml/fml:CI_UDOSTEP_GRUPA
				let $DC_NR_PESEL := $fml/fml:DC_NR_PESEL
				let $DC_NR_DOWODU_REGON := $fml/fml:DC_NR_DOWODU_REGON
				let $DC_KRAJ := $fml/fml:DC_KRAJ
				let $DC_WOJEWODZTWO := $fml/fml:DC_WOJEWODZTWO
				let $DC_KOD_POCZTOWY_DANE_PODST := $fml/fml:DC_KOD_POCZTOWY_DANE_PODST
				let $DC_MIASTO_DANE_PODST := $fml/fml:DC_MIASTO_DANE_PODST
				let $DC_ULICA_DANE_PODST := $fml/fml:DC_ULICA_DANE_PODST
				let $DC_NR_POSES_LOKALU_DANE_PODST := $fml/fml:DC_NR_POSES_LOKALU_DANE_PODST
				let $CI_ADRES_DOD := $fml/fml:CI_ADRES_DOD
				let $DC_NR_TELEFONU := $fml/fml:DC_NR_TELEFONU
				let $DC_NR_TELEF_KOMORKOWEGO := $fml/fml:DC_NR_TELEF_KOMORKOWEGO
				let $DC_JEDNOSTKA_KORPORACYJNA := $fml/fml:DC_JEDNOSTKA_KORPORACYJNA
				let $DC_DATA_URODZENIA := $fml/fml:DC_DATA_URODZENIA
                                let $CI_ID_SPOLKI := $fml/fml:CI_ID_SPOLKI

				for $it at $p in $fml/fml:CI_ID_KLIENTA
				return
					<m:CRMProspASearchKlient>
					{
						if($CI_ID_KLIENTA[$p])
							then <m:IdKlienta>{ data($CI_ID_KLIENTA[$p]) }</m:IdKlienta>
						else ()
					}
					{
						if($DC_IMIE[$p])
							then <m:Imie>{ data($DC_IMIE[$p]) }</m:Imie>
						else ()
					}
					{
						if($DC_NAZWISKO[$p])
							then <m:Nazwisko>{ data($DC_NAZWISKO[$p]) }</m:Nazwisko>
						else ()
					}
					{
						if($DC_NAZWA[$p])
							then <m:Nazwa>{ data($DC_NAZWA[$p]) }</m:Nazwa>
						else ()
					}
					{
						if($CI_UDOSTEP_GRUPA[$p])
							then <m:UdostepGrupa>{ data($CI_UDOSTEP_GRUPA[$p]) }</m:UdostepGrupa>
						else ()
					}
					{
						if($DC_NR_PESEL[$p])
							then <m:NrPesel>{ data($DC_NR_PESEL[$p]) }</m:NrPesel>
						else ()
					}
					{
						if($DC_NR_DOWODU_REGON[$p])
							then <m:NrDowoduRegon>{ data($DC_NR_DOWODU_REGON[$p]) }</m:NrDowoduRegon>
						else ()
					}
					{
						if($DC_KRAJ[$p])
							then <m:Kraj>{ data($DC_KRAJ[$p]) }</m:Kraj>
						else ()
					}
					{
						if($DC_WOJEWODZTWO[$p])
							then <m:Wojewodztwo>{ data($DC_WOJEWODZTWO[$p]) }</m:Wojewodztwo>
						else ()
					}
					{
						if($DC_KOD_POCZTOWY_DANE_PODST[$p])
							then <m:KodPocztowyDanePodst>{ data($DC_KOD_POCZTOWY_DANE_PODST[$p]) }</m:KodPocztowyDanePodst>
						else ()
					}
					{
						if($DC_MIASTO_DANE_PODST[$p])
							then <m:MiastoDanePodst>{ data($DC_MIASTO_DANE_PODST[$p]) }</m:MiastoDanePodst>
						else ()
					}
					{
						if($DC_ULICA_DANE_PODST[$p])
							then <m:UlicaDanePodst>{ data($DC_ULICA_DANE_PODST[$p]) }</m:UlicaDanePodst>
						else ()
					}
					{
						if($DC_NR_POSES_LOKALU_DANE_PODST[$p])
							then <m:NrPosesLokaluDanePodst>{ data($DC_NR_POSES_LOKALU_DANE_PODST[$p]) }</m:NrPosesLokaluDanePodst>
						else ()
					}
					{
						if($CI_ADRES_DOD[$p])
							then <m:AdresDod>{ data($CI_ADRES_DOD[$p]) }</m:AdresDod>
						else ()
					}
					{
						if($DC_NR_TELEFONU[$p])
							then <m:NrTelefonu>{ data($DC_NR_TELEFONU[$p]) }</m:NrTelefonu>
						else ()
					}
					{
						if($DC_NR_TELEF_KOMORKOWEGO[$p])
							then <m:NrTelefKomorkowego>{ data($DC_NR_TELEF_KOMORKOWEGO[$p]) }</m:NrTelefKomorkowego>
						else ()
					}
					{
						if($DC_JEDNOSTKA_KORPORACYJNA[$p])
							then <m:JednostkaKorporacyjna>{ data($DC_JEDNOSTKA_KORPORACYJNA[$p]) }</m:JednostkaKorporacyjna>
						else ()
					}
					{
						if($DC_DATA_URODZENIA[$p])
							then <m:DataUrodzenia>{ data($DC_DATA_URODZENIA[$p]) }</m:DataUrodzenia>
						else ()
					}
                                        {
                                                if($CI_ID_SPOLKI[$p])
                                                        then <m:IdSpolki>{ data($CI_ID_SPOLKI[$p]) }</m:IdSpolki>
                                                else ()
                                        }
					</m:CRMProspASearchKlient>
			}

		</m:CRMProspASearchResponse>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ xf:mapCRMProspASearchResponse($body/fml:FML32) }
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>