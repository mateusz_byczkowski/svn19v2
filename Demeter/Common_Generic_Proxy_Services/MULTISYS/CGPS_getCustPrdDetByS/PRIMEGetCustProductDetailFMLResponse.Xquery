<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
  <con:description>Version.$1.2011-06-22</con:description>
  <con:xquery><![CDATA[(: Change Log
v.1.0 2011-03-25 PKL PT58 Content Streaming

 :)

declare namespace x = "http://bzwbk.com/services/prime/";
declare namespace m = "http://schemas.datacontract.org/2004/07/SzrekLibrary.Struktures";
declare namespace xf = "http://bzwbk.com/services/mappings/";
declare namespace soap-env = "http://schemas.xmlsoap.org/soap/envelope/";
declare namespace fml = "";

declare function xf:mapPRIMEGetCustProductDetailResponse($bdy as element(x:PRIMEGetCustProductDetailResponse))
	as element(fml:FML32) {
		<fml:FML32>
			{
				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_KARTA)
					then <fml:B_KARTA>{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_KARTA) }</fml:B_KARTA>
					else ()
			}
                        {
 				if($bdy/x:PRIMEGetCustProductDetailResult/m:B_KOD_RACH)
					then <fml:B_KOD_RACH>{ data($bdy/x:PRIMEGetCustProductDetailResult/m:B_KOD_RACH) }</fml:B_KOD_RACH>
					else ()
                        }
                </fml:FML32>
};

declare variable $body as element(soap-env:Body) external;
<soap-env:Body>
{ 
 if ($body/x:PRIMEGetCustProductDetailResponse)
    then xf:mapPRIMEGetCustProductDetailResponse($body/x:PRIMEGetCustProductDetailResponse) 
    else  
 if ($body/fml:FML32)
    then $body/fml:FML32
    else  ()
}
</soap-env:Body>]]></con:xquery>
</con:xqueryEntry>